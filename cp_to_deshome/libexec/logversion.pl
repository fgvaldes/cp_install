#!/usr/bin/perl
########################################################################
#  $Id: logversion.pl 6505 2011-02-21 17:44:09Z mgower $
#
#  $Rev:: 6505                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2011-02-21 10:44:09 #$:  # Date of last commit.
#
#  Authors: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#      
########################################################################

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
#use DBD::Oracle qw(:ora_types);
use DB::DESUtil;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($deshome, $id, $system, $verbose, $help, $man) = undef;
Getopt::Long::GetOptions(
    'deshome|des_home|des-home=s' => \$deshome,
    'system=s' => \$system,
    'id=s' => \$id,
    'verbose|V=i' => \$verbose,
    'help|h|?' => \$help,
    'man' => \$man

);

# Display documentation and exit if requested:
if ($help) {
    Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($man) {
    Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($id)) {
    Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
      -message => 'Must provide id.'
    );
}
if (!defined($deshome)) {
    Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
      -message => 'Must provide deshome.'
    );
}
if (!defined($system)) {
    Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
      -message => 'Must provide system=target or submit'
    );
}
my $sys_prefix;
if ($system =~ /submit/i) {
    $sys_prefix = 'SUBMIT';
}
elsif ($system =~ /target/i) {
    $sys_prefix = 'TARGET';
}
else {
    Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
      -message => 'Must provide system=target or submit'
    );
}


die "$deshome does not exist.\n" if (! -d $deshome);

print "\nRetrieving version and build/installation information for a $sys_prefix system.\n";
print "DES_HOME: $deshome\n";
print "id: $id\n";
print "\n";

my %update = (
    $sys_prefix.'_RELEASE' => {'file' => "$deshome/VERSION", 'type' =>'VARCHAR2'},
    $sys_prefix.'_BASEVERSIONS' => {'file' => "$deshome/BASEVERSIONS", 'type' => 'CLOB'},
    $sys_prefix.'_ENV' => {'file' => "$deshome/ENV.log", 'type' => 'CLOB'},
    $sys_prefix.'_BUILD' => {'file' => "$deshome/BUILD.log", 'type' => 'CLOB'},
    $sys_prefix.'_INSTALL' => {'file' => "$deshome/INSTALL.log", 'type' => 'CLOB'}
);

while ((my $key, my $vals) = each %update) {
    my $file = $vals->{'file'};
    next if ($key eq $sys_prefix.'_SVN_REVISION');
    if (-r $file) {
        if ($key eq $sys_prefix.'_RELEASE') {
            my $str = gulp($file);
            $str =~/Release:\s*(\S+)/ ;
            my $rel = $1;
            $str =~/Rev:\s*(\S+)/;
            my $rev = $1;
            $update{$sys_prefix.'_SVN_REVISION'} = {'content' => $rev, 'type' => 'VARCHAR2'};
            $vals->{'content'} = $rel;
        }
        else {
            $vals->{'content'} = gulp($file);
        }
    }
}

my $db = DB::DESUtil->new(verbose=>3,debug=>3);
my ($table, $where);

if ($sys_prefix eq 'TARGET') {
    $table='BLOCK';
    $where = "BLOCK_ID=$id";
}
else {
    $table='RUN';
    $where = "ID=$id";
}

foreach my $field (keys %update) {
    my $sql = "UPDATE $table SET $field=? WHERE $where";
    my $type = $update{"$field"}->{'type'};
    my $sth = $db->prepare($sql);
    if ($update{"$field"}->{'content'}) {
        print "Updating $field\n";
        if ($type eq 'CLOB') {
#            $sth->bind_param(1,$update{"$field"}->{'content'},{'ora_type'=>SQLT_CHR})
            $sth->bind_param(1,$update{"$field"}->{'content'})
        }
        else {
            $sth->bind_param(1,$update{"$field"}->{'content'})
        }
        $sth->execute();
    }
    else {
        print "No data for $field\n";
    }
    $sth->finish;
}


$db->commit();
$db->disconnect;

sub gulp {
    my $file = shift;

    open (FH, "<$file") or die "Cannot open $file\n";
    my $lines = '';
    while (my $line = <FH>) {
      $lines = $lines.$line;
    }
    close FH;

    return $lines;
}


########################################################################
# DOCUMENTATION
########################################################################

=head1 NAME B<logversion.pl>

=head1 SYNOPSIS

B<logversion.pl>  -system <TARGET|SUBMIT> -deshome <installation home> -id <id for DB>

=head1 DESCRIPTION

Log version and build information into DB tables.

=head1 OPTIONS


