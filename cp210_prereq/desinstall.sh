#! /bin/bash
#
# Usage: sh newinstall.sh [package ...]
#
#  Set up some initial environment variables
#
# set -x
SHELL=/bin/bash
DES_PREREQ=$PWD
# export EUPS_PKGROOT=http://lsstdev.ncsa.uiuc.edu/dmspkgs
export EUPS_PKGROOT=http://deslogin.cosmology.illinois.edu/desdmpkgs
owneups=
httpget=

while [ $# -gt 0 ]; do
    case "$1" in 
        -H) DES_PREREQ="$2"; shift;;
        -E) owneups="1";;
        -w) httpget="$2"; shift;;
        -r) EUPS_PKGROOT="$2"; shift;;
        *)  break;;
    esac
    shift
done
cd $DES_PREREQ

if [ -z "$httpget" ]; then
    httpget=`/usr/bin/which curl` || httpget=`/usr/bin/which wget`
    if [ $? -ne 0 -o -z "$httpget" ]; then
        echo "Can't find curl or wget on your system; try to give path via -w"
        exit 1
    fi
fi
if [ `basename $httpget` = 'wget' ]; then
    httpget="$httpget -O -"
fi


# Create the initial set of directories 
#
mkdir -p eups EupsBuildDir
cat > EupsBuildDir/README <<EOF
This directory is used to build packages, 
EOF

# Download and install EUPS
# 
if [ -z "$owneups" ]; then
    cd EupsBuildDir && mkdir eups-default && cd eups-default

    echo "Pulling down EUPS..."
    $httpget $EUPS_PKGROOT/external/eups/eups-default.tar.gz >eups-default.tar.gz
    eupsdir=`tar tzf eups-default.tar.gz | grep / | head -1 | sed -e 's/\/.*$//'`
    tar xzf eups-default.tar.gz

    if [ -n "$eupsdir" ]; then
        cd $eupsdir
        echo "Installing EUPS..."
        eupsver=`echo $eupsdir | sed -e 's/^eups-//'`
        echo ./configure --prefix=$DES_PREREQ/eups/$eupsver --with-eups=$DES_PREREQ --with-eups_dir=$DES_PREREQ/eups/$eupsver
        ls Makefile.in
        ./configure --prefix=$DES_PREREQ/eups/$eupsver --with-eups=$DES_PREREQ --with-eups_dir=$DES_PREREQ/eups/$eupsver || {
            echo "Failed to configure EUPS"
            exit 2
        }
        echo make install
        make install > make-install.log 2>&1 || {
            cat make-install.log
            echo "Failed to install EUPS"
            exit 2
        }
        head -4 make-install.log
    else
        echo "I don't see what EUPS unpacked into"
        exit 2
    fi

    cd $DES_PREREQ/eups || exit 2
    [ -e default ] && rm default
    ln -s $eupsver default
    cd $DES_PREREQ || exit 2
    rm -rf EupsBuildDir/eups-default

    # load EUPS into the environment
    source eups/default/bin/setups.sh

else

    if [ -z "$EUPS_DIR" ]; then
        echo "User's EUPS_DIR is not set; set up EUPS or do not use -E"
        exit 1
    fi
    echo "Warning: Using local EUPS installation at $EUPS_DIR"
    mkdir -p ups_db

fi

mkdir `eups flavor`

# install the LSST EUPS extension package

eups distrib install -v -r $EUPS_PKGROOT/bootstrap lssteups || {
    echo "Failed to install lssteups, LSST's EUPS extension package"
    exit 2
}
[ -d "EupsBuildDir" ] && rm -rf EupsBuildDir
setup lssteups

echo "Installation complete"
echo "Now type "
echo 
echo "  source ${DES_PREREQ}/eups/1.1.1/bin/setups.sh "
echo 
echo "or"
echo 
echo "  source ${DES_PREREQ}/eups/1.1.1/bin/setups.csh" 
echo 
echo "to load version management with EUPS"
echo 
