package Sys::Info::Driver::Linux::Device;
use strict;
use warnings;
use vars qw( $VERSION );

$VERSION = '0.74';

1;

__END__

=head1 NAME

Sys::Info::Driver::Linux::Device - Base class for Linux device drivers

=head1 SYNOPSIS

    use base qw( Sys::Info::Driver::Linux::Device );

=head1 DESCRIPTION

This document describes version C<0.74> of C<Sys::Info::Driver::Linux::Device>
released on C<15 January 2010>.

Base class for Linux device drivers.

=head1 METHODS

None.

=head1 AUTHOR

Burak Gursoy <burak@cpan.org>.

=head1 COPYRIGHT

Copyright 2006 - 2010 Burak Gursoy. All rights reserved.

=head1 LICENSE

This library is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself, either Perl version 5.10.0 or, 
at your option, any later version of Perl 5 you may have available.

=cut
