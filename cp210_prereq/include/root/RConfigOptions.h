#ifndef ROOT_RConfigOptions
#define ROOT_RConfigOptions

#define R__CONFIGUREOPTIONS   "QTDIR=/usr/lib64/qt-3.3 --build=debug --enable-mathmore --enable-minuit2 --disable-python --enable-qt --enable-roofit --enable-rpath --enable-shadowpw --enable-shared --enable-ssl --enable-xml --enable-xrootd --with-gsl-incdir=/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/include --with-gsl-libdir=/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/lib --enable-oracle --with-oracle-libdir=/lib --with-oracle-incdir=/rdbms/public --prefix=/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq --enable-pgsql --with-pgsql-libdir=/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/lib --with-pgsql-incdir=/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/include --etcdir=/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/etc"
#define R__CONFIGUREFEATURES  "asimage astiff builtin_afterimage builtin_ftgl builtin_freetype builtin_pcre builtin_zlib cint5 cintex editline exceptions genvector mathmore memstat minuit2 pgsql reflex roofit rpath shadowpw shared tmva winrtdebug xft xml xrootd thread"

#endif
