#ifndef ROOT_RConfigure
#define ROOT_RConfigure

/* Configurations file for linuxx8664gcc */
#ifdef R__HAVE_CONFIG
#define ROOTPREFIX    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq"
#define ROOTBINDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/bin"
#define ROOTLIBDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/lib/root"
#define ROOTINCDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/include/root"
#define ROOTETCDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/etc"
#define ROOTDATADIR   "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/share/root"
#define ROOTDOCDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/share/doc/root"
#define ROOTMACRODIR  "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/share/root/macros"
#define ROOTSRCDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/share/root/src"
#define ROOTICONPATH  "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/share/root/icons"
#define EXTRAICONPATH ""
#define TTFFONTDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/share/root/fonts"
#define CINTINCDIR    "/data/sharedfs/deccptest/CP_INSTALL/cp210_prereq/lib/root/cint"
#endif

#define R__HAS_SETRESUID   /**/
#define R__HAS_MATHMORE   /**/
#define R__HAS_PTHREAD    /**/
#define R__HAS_XFT    /**/

#endif
