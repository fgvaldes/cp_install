#ifndef ATLAS_MVN_H
#define ATLAS_MVN_H

#include "atlas_misc.h"
#include "atlas_dNCmm.h"
#include "atlas_lvl3.h"

#define ATL_mvNMU ATL_mmMU
#define ATL_mvNNU NB
#define ATL_mvNCallsGemm
#ifndef ATL_L1mvelts
   #define ATL_L1mvelts ((3*ATL_L1elts)>>2)
#endif
#ifndef ATL_mvpagesize
   #define ATL_mvpagesize ATL_DivBySize(4096)
#endif
#ifndef ATL_mvntlb
   #define ATL_mvntlb 56
#endif

#define ATL_GetPartMVN(A_, lda_, mb_, nb_) \
{ \
   *(mb_) = (ATL_L1mvelts - (ATL_mmMU+1)*NB) / (NB + 2); \
   if (*(mb_) > NB) *(mb_) = ATL_MulByNB(ATL_DivByNB(*(mb_))); \
   else if (*(mb_) < ATL_mmMU) *(mb_) = NB; \
   else  *(mb_) = (*(mb_) / ATL_mmMU) * ATL_mmMU; \
   *(nb_) = NB; \
}

#endif
