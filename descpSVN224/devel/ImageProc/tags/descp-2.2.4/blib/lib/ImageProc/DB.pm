########################################################################
#  $Id: DB.pm 6837 2011-06-01 15:03:45Z mgower $
#
#  $Rev:: 6837                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-06-01 08:03:45 #$:  # Date of last commit.
#
#  Authors: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#         Choong Ngeow (cngeow@uiuc.edu)
#         joe Mohr (jmohr@uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION: 
#  
#######################################################################

package ImageProc::DB;
use strict;
use warnings;
use Carp;
use FindBin;
use Exception::Class('bail');
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use base qw(DB::DESUtil);
use Math::Trig qw(deg2rad);



#######################################################################
#######################################################################
sub new {
  my ($this, %Args)  = @_;
   
  my $class = ref($this) || $this;
  my $self  = $class->SUPER::new(%Args);
  $self->verbose($Args{'verbose'}) if ($Args{'verbose'});
  return $self;
}


#######################################################################
#######################################################################
package ImageProc::DB::db;
use Carp;
use Data::Dumper;
our @ISA = qw(DB::DESUtil::db);



#######################################################################
# 
#######################################################################
sub getZeroPoints {
  my $self = shift;
  my $Args = shift;
  my $tile = shift;

  my $FileList = $Args->{'remap_list'} if ($Args->{'remap_list'});
  my ($dbq, $sth);

  bail->throw("Must provide list of remaps") if (!$FileList);

  my $lt = $self->LOCATION_TABLE->{'table_name'};
  my $it = $self->IMAGE_META_TABLE->{'table_name'};
  my $zpt = $self->ZEROPOINT_TABLE->{'table_name'};

  # Get red parents:
  $dbq = "SELECT $lt.id,$lt.filetype FROM $lt,$it WHERE $lt.id = $it.parentid AND $it.id=?";
  $sth = $self->prepare($dbq);

  my $N_files = scalar keys %$FileList;
  print "N_files: $N_files\n";
  my $N_reds = 0;

  while ($N_reds < $N_files) {
    foreach my $file (values %$FileList) {
      my $row;
      if (exists $file->{'parentid'}) {
        if ($file->{'parent_filetype'} eq 'red') {
          next;
        }
        else {
          $sth->execute($file->{'parentid'})
        }
      }
      else {
        $sth->execute($file->{'id'});
      }
      $row = $sth->fetchrow_hashref();
      die "parent link broken.\n"if (!$row);
      $file->{'parentid'} = $row->{'id'};
      $file->{'parent_filetype'} = $row->{'filetype'};
      $N_reds++ if ($row->{'filetype'} eq 'red');
    }
    print "N_reds: $N_reds\n";
  }

  $sth->finish;


  # Get latest zp data:
  $dbq = "SELECT IMAGEID, MAG_ZERO, SIGMA_MAG_ZERO FROM $zpt WHERE $zpt.imageid=? "; 

  if (!defined($Args->{'caltype'}) || ($Args->{'caltype'} ne 'global')) {
print "caltype = '", $Args->{'caltype'},"'\n";
      $dbq .= " AND $zpt.tilename='$tile' ";
  }
  $dbq .= " ORDER BY ID";
  print "$dbq\n";
  $sth = $self->prepare($dbq);

  # This should only get one row -- the one with max zp_n:
  foreach my $file (values %$FileList) {
    die "Parent is not a red file.\n" if ($file->{'parent_filetype'} ne 'red');
    $sth->execute($file->{'parentid'});
    my $Row = $sth->fetchall_hashref('imageid');
    my $N_Rows = scalar keys %$Row;
    if ($N_Rows > 0) {    
      bail->throw ('More than one zp row--how can this be??') if $N_Rows > 1;
      my @vals = values %$Row; 
      $file = {%$file,%{$vals[0]}};
    }
    else {
      #bail->throw("No zeropoint found for remap:$id");
      warn "No zeropoint found for remap: $file->{'id'}\n";
    }
    
  }
  $sth->finish;

}

#######################################################################
# SUBROUTINE: getTileMatchInfo
#
# DESCRIPTION:
#   For a given list of images, find matching tiles and associate tile
#   information with the image.
#
# INPUT:
#
# OUTPUT:
# 
#######################################################################
sub getTileMatchInfo {
  my $self = shift;
  my $Args = shift;

  my ($coadd_project, $ImageList);

  $coadd_project = $Args->{'coadd_project'} if ($Args->{'coadd_project'});
  $ImageList = $Args->{'images'} if ($Args->{'images'});

  my $List;
  if (ref $ImageList eq 'ARRAY') {
    $List = $ImageList;
  }
  elsif (ref $ImageList eq 'HASH') {
    $List = [values %$ImageList];
  }
  my $dbq = <<STR;
SELECT * FROM coaddtile WHERE 
  project = '$coadd_project' AND 
  (ra BETWEEN ? AND ?) AND
  (dec BETWEEN ? AND ?)
STR

  my $sth = $self->prepare($dbq);

  # Local subroutine to execute the query and push the results
  # into an array
  local * executeAndPush = sub {
    my $vals = shift;
    my $Aref = shift;

    $sth->execute(@$vals);
    my $Rows = $sth->fetchall_arrayref({});
    if ($#$Rows < 0) {
      warn(
        qq(STATUS4BEG\n),
        qq(  Error, No matching tile in: getTileMatchInfo\n),
        qq(  minra         maxra         mindec         maxdec\n),
        qq(  @$vals\n),
        qq(STATUS4END\n),
      );
    }
    else {
      foreach my $Row (@$Rows) {
        push(@$Aref, $Row);
        print "   Matched tile: ",$Row->{'tilename'},' ', $Row->{'ra'}, ' ', $Row->{'dec'},"\n" if (! defined $Args->{'quiet'});
      }
    }
  };


  # For every image execute the coaddtile query for the given
  # range.
  foreach my $Image (@$List) {
    my @Tiles;
    my $maxra = $Image->{'maxra_look'};
    my $minra = $Image->{'minra_look'};
    my $mindec = $Image->{'mindec_look'};
    my $maxdec = $Image->{'maxdec_look'};

    if($minra>360) {$minra=$minra-360;}
    if($mindec>360) {$mindec=$mindec-360};
    if($maxra>360) {$maxra=$maxra-360;}
    if($maxdec>360) {$maxdec=$maxdec-360};
      


    print "\nLooking for tile matches for image: ", $Image->{'filename'},"\n" if (! defined $Args->{'quiet'});
    if (! defined $Args->{'quiet'}) {
      if (defined $Args->{'debug'}) { 
        foreach my $k (sort keys %$Image) {
          print "$k = ",$Image->{$k},"\n";
        }
      }
    }
    print "minra             maxra             mindec             maxdec\n" if (! defined $Args->{'quiet'});
    if ($minra > $maxra) {
      print "$minra  360.0  $mindec  $maxdec\n" if (! defined $Args->{'quiet'});
      executeAndPush ([$minra, 360.0, $mindec, $maxdec], \@Tiles);
      print "0.0  $maxra  $mindec  $maxdec\n" if (! defined $Args->{'quiet'});
      executeAndPush ([0.0, $maxra, $mindec, $maxdec], \@Tiles);
    }
    else {
      print "$minra  $maxra  $mindec  $maxdec\n" if (! defined $Args->{'quiet'});
      executeAndPush ([$minra, $maxra, $mindec, $maxdec], \@Tiles);
    }
    $Image->{'tiles_info'} = \@Tiles;
  }  


}




#######################################################################
# SUBROUTINE: getTileMatchInfo
#
# DESCRIPTION:
#   For a given list of images, find matching tiles and associate tile
#   information with the image.
#
# INPUT:
#
# OUTPUT:
# 
#######################################################################
sub getNewTileMatchInfo {
  my $self = shift;
  my $Args = shift;

  my ($coadd_project, $ImageList,$min_overlap) ;

  $coadd_project = $Args->{'coadd_project'} if ($Args->{'coadd_project'});
  $ImageList = $Args->{'images'} if ($Args->{'images'});
  $min_overlap = $Args->{'min_overlap'}  if ($Args->{'min_overlap'});;

  my $List;
  if (ref $ImageList eq 'ARRAY') {
    $List = $ImageList;
  }
  elsif (ref $ImageList eq 'HASH') {
    $List = [values %$ImageList];
  }

  my @tile_info;

  my $dbq = <<STR;
SELECT * FROM coaddtile WHERE 
project = '$coadd_project'
STR

  my $sth = $self->prepare($dbq);
  $sth->execute();
  while (my $data = $sth->fetchrow_hashref) {
      push @tile_info,$data;
  }


  foreach my $Image (@$List) {

      my @Tiles;
      my $maxra = $Image->{'maxra'};
      my $minra = $Image->{'minra'};
      my $mindec = $Image->{'mindec'};
      my $maxdec = $Image->{'maxdec'};
      

      if($minra>360) {$minra=$minra-360;}
      if($mindec>360) {$mindec=$mindec-360};
      if($maxra>360) {$maxra=$maxra-360;}
      if($maxdec>360) {$maxdec=$maxdec-360};

      
      # Look for coordinates that stradle zero
      if(($maxra-$minra)>300) {
          my $newmin=$maxra-360;
          $maxra=$minra;
          $minra=$newmin;
      }
      if(($maxdec-$mindec)>300) {
          my $newmin=$maxdec-360;
          $maxdec=$mindec;
          $mindec=$newmin;
      }
       
      printf ("\nLooking for tile matches for image: %s -> %0.4f %0.4f %0.4f %0.4f\n",$Image->{'filename'},$minra,$maxra,$mindec,$maxdec) if (! defined $Args->{'quiet'});
      


      for my $data (@tile_info) {
          # find min max for each tile
          my $tile=$data->{'tilename'};
          my $npix_ra=$data->{'npix_ra'};
          my $npix_dec=$data->{'npix_dec'};
          my $tile_ra=$data->{'ra'};
          my $tile_dec=$data->{'dec'};
          my $pixscale=$data->{'pixelsize'};

          
          my $tile_dec_max=$tile_dec+($npix_dec/2.)*$pixscale/3600.0;
          my $tile_dec_min=$tile_dec-($npix_dec/2.)*$pixscale/3600.0;
          my $scale = cos(3.14159265/180*$tile_dec);
          
          my $tile_ra_max=$tile_ra+($npix_ra/2.)*$pixscale/3600.0*$scale;
          my $tile_ra_min=$tile_ra-($npix_ra/2.)*$pixscale/3600.0*$scale;
          

          # check if any overlap at all
          if( $maxra< $tile_ra_min ||
              $minra> $tile_ra_max ||
              $maxdec< $tile_dec_min ||
              $mindec> $tile_dec_max) {next;}

          


          my ($overlap_ra,$overlap_dec );

          if($maxra>$tile_ra_max) {
              $overlap_ra=($tile_ra_max-$minra)*$scale;
          } elsif($minra<$tile_ra_min) {
              $overlap_ra=($maxra-$tile_ra_min)*$scale;
          } else {
              $overlap_ra=($maxra-$minra)*$scale;
          }
          
          if($maxdec>$tile_dec_max) {
              $overlap_dec=$tile_dec_max-$mindec;
          } elsif($mindec<$tile_dec_min) {
              $overlap_dec=$maxdec-$tile_dec_min;
          } else {
              $overlap_dec=$maxdec-$mindec;
          }

          
          my $overlap_area=$overlap_ra*$overlap_dec*3600;#convert to arcmin^2

          if($overlap_area>$min_overlap) {
              push @Tiles,$data;
              printf ("  Adding $tile %0.4f %0.4f %0.4f %0.4f image=$Image->{'imagename'}   %0.4f %0.4f %0.4f %0.4f overlap=%0.4f\n",$tile_ra_min,$tile_ra_max,$tile_dec_min,$tile_dec_max,$minra,$maxra,$mindec,$maxdec,$overlap_area) if (! defined $Args->{'quiet'}) ;
          } else {
              printf( "  Not including $tile, overlap=%0.4f (min=$min_overlap)\n",$overlap_area) if (! defined $Args->{'quiet'});
          }
      }
      
      if ($#Tiles<0) {
          warn(
              qq(STATUS4BEG\n),
              qq(  Error, No matching tile in: getNewTileMatchInfo),
              qq( for $Image->{'filename'}\n),
              qq(STATUS4END\n),
              );
      }

      $Image->{'tiles_info'} = \@Tiles;
  }
  
  
}

#######################################################################
#
# DESCRIPTION:
#  Select ra,dec,ccd and band for a particular filetype
#
#######################################################################
sub getRedImageListInfo {
  my $self = shift;
  my $List = shift;

  my $dbq = <<STR;
SELECT b.telra, b.teldec, a.ccd, a.band,
c.equinox, c.naxis1, c.naxis2, c.ctype1, c.ctype2, c.crval1, c.crval2,
c.crpix1, c.crpix2, c.cd1_1, c.cd1_2, c.cd2_1, c.cd2_2, 
c.pv1_0,c.pv1_1,c.pv1_2,c.pv1_3,c.pv1_4,c.pv1_5,c.pv1_6,c.pv1_7,
c.pv1_8,c.pv1_9,c.pv1_10,
c.pv2_0,c.pv2_1,c.pv2_2,c.pv2_3,c.pv2_4,c.pv2_5,c.pv2_6,c.pv2_7,
c.pv2_8,c.pv2_9,c.pv2_10 ,c.id,c.imagename
FROM location a,exposure b,image c
WHERE a.id=c.id AND
c.exposureid=b.id AND
a.filetype='red' AND
a.fileclass='red' AND
a.run=? AND
a.exposurename=? AND
a.filename=?
STR

  my $sth = $self->prepare($dbq);

  foreach my $Image (@$List) {
    $sth->execute(
      $Image->{'run'},
      $Image->{'exposurename'},
      $Image->{'filename'},
    );


    my $Row = $sth->fetchrow_hashref();
    if (! defined($Row)) {
      warn $Image->{'run'}, ' ', $Image->{'exposurename'}, ' ', $Image->{'filename'},"\n";
      croak("Unable to find image in DB");
    }
    while ((my $k, my $v) = each %$Row) {
      $Image->{$k} = $v;
    }

  }
  $sth->finish;

}

sub getImageDataForList {
  my $self = shift;
  my $List = shift;

  my $dbq = 'SELECT ra,dec FROM image WHERE id=?';

  my $sth = $self->prepare($dbq);

  foreach my $Image (@$List) {
    $sth->execute( $Image->{'fileid'} ); 
    my $Row = $sth->fetchrow_hashref();
    if (! defined($Row)) {
      warn $Image->{'fileid'},"\n";
      croak("Unable to find image in DB");
    }
    while ((my $k, my $v) = each %$Row) {
      $Image->{$k} = $v;
    }

  }
  $sth->finish;

}

#######################################################################
#######################################################################
sub getCCDOffsets {
  my $self = shift;
  my $Args = shift;

  my ($detector, $table);  

  $detector = $Args->{'detector'} if ($Args->{'detector'});
  $table = $Args->{'table'} if ($Args->{'table'});
  
  croak("Must provide detector.\n") if (! $detector);
  $table = 'wcsoffset' if (! $table);
  
  my $dbq = "SELECT chipid, raoffset, decoffset, rahwidth, dechwidth FROM $table WHERE DETECTOR = '$detector'";

  my $Rows = $self->selectall_hashref($dbq, 'chipid');

  if ($self->verbose() >= 1) {
    print "\n";
#    foreach my $Row (keys %$Rows) {
#      print "Row: $Row ", keys %{$Rows->{$Row}},"\n";
#    }
  }
  return $Rows; 
}

#######################################################################
#######################################################################
sub getTileSizeInfo {
  my $self = shift;
  my $Args = shift;

  my ($project, $table);

  $project = $Args->{'project'} if ($Args->{'project'});
  $table = $Args->{'table'} if ($Args->{'table'});

  croak("Must provide project.\n") if (! $project);
  $table = 'coaddtile' if (! $table);

  # Not used yet:
  my %db_fields = (
    'pixelsize' => 'arcsec',
    'npix_ra' => 'pixels',
    'npix_dec' => 'pixels'
  );

  my $dbq = "SELECT DISTINCT pixelsize, npix_ra, npix_dec FROM $table where project='$project'";

  my $Rows = $self->selectall_arrayref($dbq,{'Slice' => {}});

  if (scalar @$Rows > 1) {
    croak("Recieved more than on set of Tile pixel data for project: $project");
  }

  if ($self->verbose() >= 1) {
    print "\nTile Size Info for project $project:\n";
    while ((my $k, my $v) = each %{$Rows->[0]}) {
      print "$k = $v \n";
    }
    print "\n"
  }

  $Rows->[0]->{'pixelsize'} /= 3600.0;

  return $Rows->[0];
}

#######################################################################
#######################################################################
sub getPointingData {
  my $db = shift;
  my $Args = shift;

  my ($exposureList, $project, $nite,$err);

  $exposureList = $Args->{'exposure_list'}  if ($Args->{'exposure_list'});
  $project = $Args->{'project'} if ($Args->{'project'});
  $nite = $Args->{'nite'} if ($Args->{'nite'});

  croak("Must provide an exposure list hash ref.\n") if (! $exposureList);
  croak("Must provide a project string.\n") if (! $project);
  croak("Must provide a nite string.\n") if (! $nite);

  my $dbq = <<STR;
SELECT DISTINCT exposure.telra,exposure.teldec,exposure.telequin 
FROM exposure,location 
WHERE location.project='$project' 
AND location.nite='$nite' 
AND location.filetype='src' 
AND exposure.id=location.id 
AND location.exposurename=?
STR

  my $sth = $db->prepare($dbq);

  foreach my $exposurename (keys %$exposureList) {
    $sth->execute($exposurename);
    my $Result = $sth->fetchall_arrayref({});
    if (scalar @$Result > 0) {
      $exposureList->{"$exposurename"} = $Result->[0];
    }
    else {
      $err = "Unable to retrive some telra, tedec data for the given exposure list.\n";
      warn "No telra teldec data found for $exposurename\n";
    }
  }
  $sth->finish();

  if ($err) {
    croak("$err");
  }


}

#######################################################################
# SUBROUTINE: getStandardsData
#
# INPUT:
#   hashref - $PointingData: Pointing data with search regions
#     minra, maxra, mindec, maxdec
#
# OUTPUT:
#   hashref - standards catalog
#     {ra => [], dec => [], sra => [], sde => [], r2 => []}
#
#######################################################################
sub getStandardsData {
  my $db = shift;
  my $PointingData = shift;
  my $catalog_table = shift;
  my $catalog_table_IDfield = shift;

  my %standards;
  my $dbq ;

if ($catalog_table eq 'USNOB_CAT1')
{
   $dbq = <<STR;
SELECT  $catalog_table_IDfield,ra,dec,sra,sde,r2,b2,i2
FROM $catalog_table
WHERE r2<20.5 AND
(ra BETWEEN ? AND ?) AND
(dec BETWEEN ? AND ?)
STR
}
else 
{
     $dbq = <<STR;
SELECT    $catalog_table_IDfield,ra_dbl,dec_dbl,sra,sde,r_mag,g_mag,i_mag 
FROM $catalog_table 
WHERE (ra BETWEEN ? AND ?) AND
(dec BETWEEN ? AND ?) 
STR
}


  my $sth = $db->prepare($dbq);

  # Execute query and fetch results for each region of sky.  Join result into string 
  # and insure a distinct set by filling a hash with these strings:
  foreach my $pdata (values %$PointingData) {
   START:
       my $rerun= 0;
   print "Executing standards query for: ",$pdata->{'minra'},' < ra < ',$pdata->{'maxra'},',',$pdata->{'mindec'},' <  dec < ',$pdata->{'maxdec'},"\n";
if ($pdata->{'maxra'} > 360) {$rerun=1;}
  $sth->execute($pdata->{'minra'},$pdata->{'maxra'},$pdata->{'mindec'},$pdata->{'maxdec'}); 

    my ($ID,$ra,$dec,$sra,$sde,$r2,$b2,$i2);

$sth->bind_columns(\$ID,\$ra,\$dec,\$sra,\$sde,\$r2,\$b2,\$i2);
    my $ctr = 0;
    my $start = 0;
if (%standards) {
     $start = scalar keys %standards;
    }
    while ($sth->fetch) {
      my $key = $ID;
      my $val = join('_',$ra,$dec,$sra,$sde,$r2,$b2,$i2);
      $standards{"$key"} = $val;
      $ctr++;
    }
    my $end = scalar keys %standards;
    print "$ctr results, ",$end - $start," new objects appended, ",$end,"  total objects collected.\n";
if ($rerun==1){$pdata->{'maxra'} -= 360; $pdata->{'minra'} = 0; goto START;}
}

  # Repackage data into a single hash of array references, one for each column to
  # be created in the FITS astro standards catalog:
  my %results;
  foreach my $val(values %standards) {
      my ($ra,$dec,$sra,$sde,$r2,$b2,$i2) = split (/_/, $val,7);
      push(@{$results{'ra'}},$ra);
      push(@{$results{'dec'}},$dec);
      push(@{$results{'sra'}},$sra);
      push(@{$results{'sde'}},$sde);
      push(@{$results{'r2'}},$r2);
      push(@{$results{'b2'}},$b2);
      push(@{$results{'i2'}},$i2);
  }

  return \%results;

}

#######################################################################
#######################################################################
package ImageProc::DB::st;
our @ISA = qw(DB::DESUtil::st);

