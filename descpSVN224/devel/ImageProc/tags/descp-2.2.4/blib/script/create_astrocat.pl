#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#  $Id: create_astrocat.pl 8731 2012-09-26 05:15:42Z donaldp $
#
#  $Rev:: 8731                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-25 22:15:42 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#   
#   
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use warnings;
use strict;
    
use Getopt::Long;
use File::Basename;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
    
use ImageProc::DB;
use ImageProc::SCAMP qw(createAstroStds);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my $verbose = 1;
my (%opts);

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
my $stat = Getopt::Long::GetOptions(\%opts,
      'tilename=s',
      'coadd_project|project=s',
      'outdir|output-dir|od|outputpath|output-path=s',
      'run=s',
      'astrostd=s',
      'verbose|V=s',
      'help|h|?',
      'man'
);

# Display documentation and exit if requested:
if ($opts{'help'}) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($opts{'man'}) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

if (!defined($opts{'tilename'})) {
    print STDERR "Must specify tilename.\n";
    exit 1;
}

if (!defined($opts{'coadd_project'})) {
    print STDERR "Must specify (coadd) project.\n";
    exit 1;
}

if (!defined($opts{'run'}) && !defined($opts{'astrostd'})) {
    print STDERR "Must specify astrostd or run.\n";
    exit 1;
}

if (! defined($opts{'outdir'})) {
    $opts{'outdir'} = '.';
}

print "\n";

# Set file name for archive astrostds file:
my ($outdir, $outfile);

if (defined($opts{'astrostd'})) {
    my ($filename, $path, $suffix) = fileparse($opts{'astrostd'});
    chop($path);   # remove trailing / so next fileparse will get filetype
    my ($filetype, $path2, $suffix2) = fileparse($path);
    $outdir = $outdir.'/'.$filetype;
    $outfile = $outdir.'/'.$filename.$suffix;
}
else {
    $outdir = $outdir.'/'.'aux';
    $outfile = $outdir.'/'.$opts{'run'}.'_'.'astrostds.fits';
}
mkdir $outdir;

# Get database connection and query for tile info:
my $db = ImageProc::DB->new();
my $sql = "SELECT * FROM coaddtile WHERE project='".$opts{'coadd_project'}."' AND tilename='".$opts{'tilename'}."'";
print "$sql\n";
my $TileRows = $db->selectall_arrayref($sql,{'Slice'=>{}});
my $N_TileRows = scalar @$TileRows;

print "\nFound $N_TileRows rows for ",$opts{'tilename'},"\n";
print "\n";

my $tiledata = $TileRows->[0];
$tiledata->{'pixelsize'} /= 3600.0;

print $tiledata->{'ra'},"\n";

# Set dra and ddec based on the tile definition
my $MINIMUM_OVERLAP = 0.5/60.0;    # 1/2 of an arcmin.
my $dra = $tiledata->{'npix_ra'}/2.0*$tiledata->{'pixelsize'} + $MINIMUM_OVERLAP;
my $ddec = $tiledata->{'npix_dec'}/2.0*$tiledata->{'pixelsize'} + $MINIMUM_OVERLAP;

print "Tile: ",$opts{'tilename'},"\n";
print "ra: ",$tiledata->{'ra'},"  dec: ",$tiledata->{'dec'},"  dra: $dra  ddec: $ddec\n";
print "\n";

# Create a new "pointing" centered on the tile
my %pointings = ('1' => {'telra' => $tiledata->{'ra'}, 'teldec' => $tiledata->{'dec'} });

# Call routine to execute DB query and fits file creation:
createAstroStds( {
  'pointing_list' => \%pointings,
  'delta_ra' => $dra,
  'delta_dec' => $ddec,
  'dbh' => $db,
  'outfile' => $outfile,
  'std_table_name' => 'USNOB_CAT1',
  'std_table_IDfield' => 'USNOB_CAT1_ID'
});


$db->disconnect();

