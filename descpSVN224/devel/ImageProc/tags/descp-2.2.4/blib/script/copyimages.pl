#!/usr/bin/env perl
########################################################################
#  $Id                                     $
#
#  $Rev:: 8754                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-26 19:02:29 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  DESCRIPTION:
#      Given a list of images, checks the run to see if different from current
#      run.  If different copies to current run.
#
#######################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking



use strict;
use warnings;

use Data::Dumper;

use Getopt::Long;
use File::Basename;
use File::Path;
use FindBin;

use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::FileUtils qw(filenameResolve);
use DAF::Archive::Tools qw(getArchiveLocation);

my $verbose = 1;
my ($input_list, $output_list, $run, $archive_root) = undef;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'inputlist|i=s'  => \$input_list,
      'outputlist|o=s'  => \$output_list,
      'archiveroot|a=s'  => \$archive_root,
      'run|r=s' => \$run,
      'verbose|v=s' => \$verbose,
);

if (scalar(@ARGV) == 4) {
    $input_list = $ARGV[0];
    $output_list = $ARGV[1];
    $archive_root = $ARGV[2];
    $run = $ARGV[3];
}

if (!defined($input_list) || !defined($output_list) || !defined($run)) {
    print STDERR "Usage: imcorrect_prep.pl inputlist outputlist archiveroot run\n";
    print STDERR "\n";
    print STDERR "or\n";
    print STDERR "Usage: copyimages.pl [args]\n";
    print STDERR "       -inputlist <filename> (required)\n"; 
    print STDERR "       -outputlist <filename> (required)\n"; 
    print STDERR "       -archiveroot <archiveroot> (required)\n"; 
    print STDERR "       -run <run> (required)\n"; 
    print STDERR "       -verbose <0-3> (optional)\n"; 
    exit 1;
}

my $exit = 0;
my $suffix;
my ($filename,$newfilename);
open INPUTLIST, "< $input_list";
open OUTPUTLIST, "> $output_list";
while ($filename = <INPUTLIST>) {
    chomp($filename);
    if ($filename =~ /\S/) {
        $suffix="";
        if (! -r $filename) {
            if (-r "$filename.fz") {
                $suffix = '.fz';
            }
            elsif (-r "$filename.gz") {
                $suffix = '.gz';
            }
            else {
                print "STAT5BEG\n" if $verbose >= 2;
                print "Error:  Cannot read file from input list: \n";
                print "\t$filename\n";
                print "\t$filename.fz\n";
                print "\t$filename.gz\n";
                print "STAT5END\n" if $verbose >= 2;
                exit 1;
            }
        }
        my ($basename,$path,$suf) = fileparse($filename,'.gz','.fz');
        
        $path =~ s/\/$//;
        if ($path !~ /$run/) {
            # get archive path pieces - make sure lowercase keys
            my $relpathname = $filename;
            $relpathname =~ s/$archive_root//;
            $relpathname =~ s/^\///;
            my $info = filenameResolve($relpathname);
            while (my ($k, $v) = each(%$info)) {
                $info->{lc($k)} = $v;
            }

            # moving to new run 
            $info->{'run'} = $run; 

            # create new filename with new archive path
            $newfilename = $archive_root.'/'.getArchiveLocation($info).'/'.$info->{'FILENAME'};

            # make paths before cp if needed
            my ($basename,$path,$suf) = fileparse($newfilename,('.fits','.fits.fz','.fits.gz'));
            if (! -d $path) {
                my $out = `mkdir -p $path 2>&1`;
                if (($?>>8) != 0) {
                    print "\tError making directory\n";
                    print "$out\n";
                    $exit++;
                }
            }
            
            print "Copying $filename$suffix to $newfilename$suffix\n";
            my $out = `cp $filename$suffix $newfilename$suffix 2>&1`;
            if (($?>>8) != 0) {
                print "\tError copying file $filename$suffix\n";
                print "$out\n";
                $exit++;
            }
        }
        else {
            $newfilename = $filename;
        }

        print OUTPUTLIST "$newfilename\n";
    }
}
close INPUTLIST;
close OUTPUTLIST;

exit $exit;
