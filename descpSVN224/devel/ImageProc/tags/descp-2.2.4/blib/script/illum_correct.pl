#!/usr/bin/env perl
########################################################################
#  $Id: illum_correct.pl 9326 2012-10-19 21:06:25Z rgruendl $
#
#  $Rev:: 9326                             $:  # Revision of last commit.
#  $LastChangedBy:: rgruendl               $:  # Author of last commit. 
#  $LastChangedDate:: 2012-10-19 14:06:25 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  DESCRIPTION:
#       Either fringebands or detector must be defined (with fringebands
#       taking precedenct).   These determine for which bands imcorrect
#       is called with -fringe.
#
#       If wrapper needs to call imcorrect with -fringe, -fringe is required for
#       this wrapper.
#       
#
#######################################################################

use strict;
use warnings;

use Getopt::Long;
use FindBin;
use File::Basename;
use File::Path;

my ($input_list, $output_list, $archive_root, $run, $output_path) = undef; 
my ($illum, $fringe,$bpm) = undef;
my ($band, $fringebands, $detector, $binpath, $imcorrect, $interpolate_varscale, $verbose) = undef;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'inputlist|il=s'  => \$input_list,
      'outputlist|ol=s'  => \$output_list,
      'archiveroot|a=s'  => \$archive_root,
      'run|r=s' => \$run,
      'illum|i=s'  => \$illum,
      'binpath|x=s'  => \$binpath,
      'fringe|f=s'  => \$fringe,
      'fringebands|f=s'  => \$fringebands,
      'band|b=s'  => \$band,
      'detector=s'  => \$detector,
      'verbose|v=s'  => \$verbose,
      'interpolate_col=s'  => \$interpolate_varscale,
      'bpm=s' => \$bpm,
);

if (!defined($verbose)) {
    $verbose = 1;
}

if (!defined($input_list) || !defined($output_list) || !defined($illum) || !defined($run) || !defined($archive_root)) {
    print "STATUS5BEG\n" if ($verbose >= 2);
    print STDERR "Usage: illum_correct.pl [args]\n";
    print STDERR "       -inputlist <filename> (required)\n"; 
    print STDERR "       -outputlist <filename> (required)\n"; 
    print STDERR "       -archiveroot <archiveroot> (required)\n"; 
    print STDERR "       -run <run> (required)\n"; 
    print STDERR "       -illum <illumination> (required)\n";
    print STDERR "       -detector <detector>  OR -fringebands <b1,b2,b3> (required)\n";
    print STDERR "       -bpm <bpm filename> (required)\n";
    print STDERR "       -band <band> (optional - used to determine whether to use fringe)\n";
    print STDERR "       -fringe <fringe> (required for fringe bands)\n";
    print STDERR "       -binpath <path> (optional - defaults to same dir as this script)\n";
    print STDERR "       -interpolate_col <variance scaling> (requiered)\n";
    print STDERR "       -verbose <0-3> (optional)\n";
    print "STATUS5END\n" if ($verbose >= 2);
    exit 1;
}

# Check for imcorrect executable
if (!defined($binpath)) {
    $binpath = $FindBin::Bin;
}
$imcorrect = "$binpath/imcorrect";
if (! -x $imcorrect) {
    print "STATUS5BEG\n" if ($verbose >= 2);
    print "Error: could not find imcorrect executable: '$imcorrect'\n";
    print "STATUS5END\n" if ($verbose >= 2);
    exit 1;
}

if ((! -r $illum) && (! -r $illum.".fz"))  {
    print "STATUS5BEG\n" if ($verbose >= 2);
    print "Error: could not read illumination file '$illum'\n";
    print "STATUS5END\n" if ($verbose >= 2);
    exit 1;
}

if (!defined($fringebands)) {
    if (defined($detector)) {
        if (lc($detector) eq 'decam') {
            $fringebands = 'z,Y';
        }
        elsif (lc($detector) eq 'mosaic2') {
            $fringebands = 'i,z';
        }
	elsif (lc($detector) eq 'megacam') {
	    $fringebands = 'i,z';
	}    
        else {
            print "STATUS5BEG\n" if ($verbose >= 2);
            print "Error: Unknown detector '$detector'\n";
            print "STATUS5END\n" if ($verbose >= 2);
            exit 1;
        }
    }
    else {
        print "STATUS5BEG\n" if ($verbose >= 2);
        print "Error: either detector or fringebands must be specified\n";
        print "STATUS5END\n" if ($verbose >= 2);
        exit 1;
    }
}

my ($name,$path,$suffix) = undef;

# copy images to current run if needed
my $copycmd = $FindBin::Bin."/copyimages.pl -inputlist $input_list -outputlist $output_list -archiveroot $archive_root -run $run";
print "Copycmd = '$copycmd'\n";
system($copycmd);
if ($?>>8 != 0) {
    exit $?;
}

if (!defined($band)) {
    open LIST, "< $output_list";
    my $line = <LIST>;
    chomp($line);
    ($name,$path,$suffix) = fileparse($line,('.fits','.fits.gz'));
    if ($name =~ /-(\w)-\d+_\d\d/) {
        $band = $1;
    }
    else {
        print "Error: Could not determine band\n";
    }
}

my $cmd = $imcorrect." ${input_list} -MEF -output ${output_list} -illumination ${illum}";

if (!defined($interpolate_varscale)) {
    print "STATUS5BEG\n" if ($verbose >= 2);
    print "Error: interpolate_col must be specified\n";
    print "STATUS5END\n" if ($verbose >= 2);
    exit 1;
}
else {
    $cmd .= " -interpolate_col $interpolate_varscale";
}

if (!defined($bpm)) {
    print "STATUS5BEG\n" if ($verbose >= 2);
    print "Error: bpm file must be specified\n";
    print "STATUS5END\n" if ($verbose >= 2);
    exit 1;
}
else {
    $cmd .= " -bpm $bpm";
}

if (defined($band) && defined($fringebands) && ($fringebands =~ /$band/)) {
    if (!defined($fringe)) {
        print "STATUS5BEG\n" if ($verbose >= 2);
        print "Error: fringe file not specified (band = $band, fringebands = $fringebands)\n";
        print "STATUS5END\n" if ($verbose >= 2);
        exit 1;
    }
    $cmd .= " -fringe $fringe";
}
elsif (!defined($band) && defined($fringe)) {
    $cmd .= " -fringe $fringe";
}
else {
    print "Note: not using fringe option to imcorrect\n";
}
# Removed because option is depricated
#
#$cmd .= " -nooverscan";
if (defined($verbose)) {
    $cmd .= " -verbose $verbose";
}

print "STATUS1BEG\n" if ($verbose >= 2);
print "Executing command '$cmd'\n";
print "STATUS1END\n" if ($verbose >= 2);
system($cmd);
exit ($?>>8);
