#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell


$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking

use strict;
use warnings;

use Getopt::Long;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
use FindBin qw($Bin);

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use Astro::VO::VOTable::Document;
use DB::DESUtil;
use DB::EventUtils;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($project, $run, $nite, $file, $list, $noingest, $debug, $type) = undef;
my $verbose = 0;
Getopt::Long::GetOptions(
    "project=s" => \$project,
    "run=s" => \$run,
    "nite=s" => \$nite,
    "list=s" => \$list,
    "debug" => \$debug,
    "noingest" => \$noingest,
    "type=s" => \$type,
    "verbose=i" => \$verbose
); #or usage("Invalid command line options\n");


### input checks
if (!defined($project)) {
    reportEvent( $verbose, 'STATUS', 5, "Missing project on command line");
    exit 1;
}


if (!defined($list) && scalar(@ARGV) == 0) {
    reportEvent( $verbose, 'STATUS', 5, "Missing list or file on command line");
    exit 1;
}

if (defined($list) && scalar(@ARGV) > 0) {
    reportEvent( $verbose, 'STATUS', 5, "Cannot have list and file on command line");
    exit 1;
}

if (defined($list) && !-r $list) {
    reportEvent( $verbose, 'STATUS', 5, "Cannot open list $list");
    exit 1;
}

if (!defined($list) && !-r $ARGV[0]) {
    reportEvent( $verbose, 'STATUS', 5, "Cannot open file $ARGV[0]");
    exit 1;
}

if ($type ne "red" && $type ne "coadd") {
    reportEvent( $verbose, 'STATUS', 5, "Invalid type: $type  type must be coadd or red");
    exit 1;
}

# get exposure ids from DB for this nite
my $dbh = new DB::DESUtil(verbose=>$verbose,debug=>$debug);

# get columns for table in which we will be inserting
my $colnames;
eval {
   $colnames = $dbh->getColumnNames('psf_qa');

   # the following aren't stored in file as named
   delete $colnames->{'run'} if defined($colnames->{'run'});
   delete $colnames->{'type'} if defined($colnames->{'type'});
};
if ($@) {
    print "Error getting column names\n";
    print "$@\n";
    exit 1;
}
my %nameshash;
foreach my $c (keys %$colnames) {
    $c =~ s/_\d$//;
    $nameshash{$c} = 1;
}
my @varnames = sort keys %nameshash;


# grab image id
my $run_string="";
my $bcs_string;
my $dbq;
if ( !defined($run)) {
$dbq = "select max(run) as maxrun from image where project='$project' and nite='$nite' and imagetype='red' " ;
my $runval = $dbh->selectall_hashref($dbq,'maxrun');
my @runno =  keys %$runval ;
$run=$runno[0] ;
 }


 

if(defined($run)) {$run_string=" and run='$run'";}


#if ($project ='BCS') $bcs_string=" and run>'201012'";

if ($type eq "red") {
    $dbq = "select id,imagename from image where project='$project' $run_string and imagetype='red' ";
}
else {
    $dbq = "select id,imagename from coadd where project='$project' $run_string";
}
print "dbq = $dbq\n" if $debug;

my $images = $dbh->selectall_hashref($dbq, 'imagename');
if ($debug) {
    print "Images from DB\n";
    print Dumper($images),"\n\n";
}

# grab existing entries for this project+run
#$dbq = "select id,exposureid from exposure_qa where project='$project' and run='$run'";


$dbq = "select imageid from psf_qa where run='$run' and type='$type'";
print "dbq = $dbq\n" if $debug;

my $existingQA = $dbh->selectall_hashref($dbq, 'imageid');
if ($debug) {
    print "Image QA from DB\n";
    print Dumper($existingQA),"\n\n";
}
if (scalar keys %$existingQA > 0) {
    reportEvent( $verbose, 'STATUS', 5, "Information exists in DB for $project $run");
    print "If rerunning, delete information by hand first\n";
    print Dumper($existingQA), "\n";
    exit 1;
}

# done with database until mass ingest
$dbh->disconnect();

my %alldbinfo = ();
if (defined($file)) {
    parseFile($file, \%alldbinfo, $images, \@varnames);
}
elsif (defined($list)) {
    open LISTFH, "< $list";
    while (my $line = <LISTFH>) {
        $line =~ s/\s+//;
        parseFile($line, \%alldbinfo, $images, \@varnames);
    }
    close LISTFH;
}

if (!$noingest) {
    print Dumper(\%alldbinfo), "\n" if ($debug);
    ingest($project, $run, $type, \%alldbinfo, sort keys %$colnames);
}
else {
    print Dumper(\%alldbinfo), "\n";
}


######################################################
sub ingest {
    my ($project, $run, $type, $alldbinfo, @cols) = @_;
    my $dbh = new DB::DESUtil(verbose=>$verbose,debug=>$debug);

    my $dbp = "insert into psf_qa (run,type";
    for (my $i = 0; $i < scalar(@cols); $i++) {
        $dbp .= ", ".$cols[$i];
    }

    $dbp .= ") values ('$run','$type'";
    for (my $i = 0; $i < scalar(@cols); $i++) {
        $dbp .= ",?";
    }
    $dbp .= ")";
    print "dbp: $dbp\n" if $debug;

    my $sth = $dbh->prepare($dbp);


    for my $href (values %$alldbinfo) {
        my @valarray;

    for (my $i = 0; $i < scalar(@cols); $i++) {
            if (defined($href->{$cols[$i]})) {
                $valarray[$i] = $href->{$cols[$i]};
            }
            else {
                $valarray[$i] = undef;
            }
        }

        if ($debug) {
            print "dbp: $dbp\n";
            print Dumper(\@valarray), "\n";
        }
        
        $sth->execute(@valarray);
    }
    $dbh->commit();
    $dbh->disconnect();
}


sub parseFile {
    my ($file, $alldbinfo, $images, $varnames ) = @_;

    my $dbinfo = {};
    my $imageid;

    # read information from PSF XML file
    my @info=getPsfInfo($file,0);

    # only need first
    my $firstinfo = $info[0];

    if ($debug) {
        dumpPSFInfo($firstinfo, $varnames);
    }

    ## get exposure id 
    print "catalog_name = '", $firstinfo->{'catalog_name'}, "'\n";
    my $exposurename;
    if (!defined($firstinfo->{'catalog_name'})) {
        reportEvent( $verbose, 'STATUS', 5, "Could not find catalog_name");
        print Dumper($firstinfo), "\n";
        exit 1;
    }
    if ($firstinfo->{'catalog_name'} !~ /\w/) {
        reportEvent( $verbose, 'STATUS', 5, "Empty catalog_name");
        print Dumper($firstinfo), "\n";
        exit 1;
    }
    if ($firstinfo->{'catalog_name'} =~ m/^(\S+)_psfcat.fits/) { 
        my ($imagename) = $firstinfo->{'catalog_name'} =~ m/^(\S+)_psfcat.fits/;
        print "imagename = $imagename\n";

        # imagename 
        $imagename .= '.fits';
        if (defined($images->{$imagename})) {
            $imageid = $images->{$imagename}->{'id'};
            print "found imageid = $imageid\n";
            $firstinfo->{'imageid'} = $imageid;
        }
        else {
            reportEvent( $verbose, 'STATUS', 5, "Could not find image id for imagename '$imagename'");
            exit 1;
        }

        foreach my $key (@$varnames) {
            my $val = "";
            if (defined($firstinfo->{$key})) {
                $val=$firstinfo->{$key};
            }
            else {
                reportEvent( $verbose, 'STATUS', 5, "Missing value for key $key");
                exit 1;
            }

            # remove leading and trailing spaces
            $val =~ s/^\s+//;
            $val =~ s/\s+$//;

            # check for multiple values
            my @vals=split /\s+/, $val;
            if (scalar(@vals) == 1) {
               $alldbinfo->{$imageid}->{$key} = $vals[0];
            }
            else {
                for (my $i = 1; $i <= scalar(@vals); $i++) {
                    $alldbinfo->{$imageid}->{${key}."_$i"} = $vals[$i-1];
                }
            }
        }
    }
    else {
        reportEvent( $verbose, 'STATUS', 5, "catalog_name ".$firstinfo->{'catalog_name'}." doesn't match pattern");
        print Dumper($firstinfo), "\n";
        exit 1;
    }
}

############################################################
sub getPsfInfo {
    my $file=shift; 
    my $which_table=shift; 

    # Read the VOTABLE document.
    my $doc = Astro::VO::VOTable::Document->new_from_file($file) or die;
    
    # Get the (only) VOTABLE element.
    my $votable = $doc->get_VOTABLE or die;

    my $res=$votable->get_RESOURCE(0);

    my @info;
    foreach my $res2 ($res->get_RESOURCE) {
        
        # Table 0 contains the image info
        my @tables;
        my $table=$res2->get_TABLE($which_table);
        
        # Get the FIELD elements.
        my(@field_names) = ();
        my($field);
        foreach $field ($table->get_FIELD) {
            push(@field_names, $field->get_name);
        }
        
        
        my $data = $table->get_DATA or die;
        my $tabledata = $data->get_TABLEDATA or die;
        
        # Get each TR element, then each TD element, and print its contents.
        my($tr, $td);
        my($i, $j);
        $i = 0;
        foreach $tr ($tabledata->get_TR) {
            #print "Data for target $i:\n";
            $j = 0;
            foreach $td ($tr->get_TD) {
                #print "  $field_names[$j] = ", $td->get, "\n";
                my $var=$field_names[$j];
                $info[$i]{lc($var)}=$td->get;
                $j++;
            }
            $i++;
        }
    }
    
    return @info;
}

##################################################
sub dumpPSFInfo {
    my ($firstinfo, $varnames) = @_;

    print "========== dumpPSFInfo ==========\n";
    printf "%30s [%s] [%s]\n", "KEY", "INFO", "STAT";
    foreach my $key (@$varnames) {
        my $fstat="undef";
        my $finfo="undef";

        if (defined($firstinfo->{$key})) {
            $finfo=$firstinfo->{$key};
        }
        printf "%30s [%s] \n", $key, $finfo;
    }
}
