#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# SVN Info:
#
# $Rev:: 3199                                           $: Rev of last commit
# $LastChangedBy:: dadams                               $: Author of last commit
# $LastChangedDate:: 2009-02-23 13:43:37 -0700 (Mon, 23#$: Date of last commit
#

use strict;
use Astro::FITS::CFITSIO qw( :constants );
use Astro::FITS::CFITSIO qw( :longnames );
use DBI;
use File::Basename;
use File::Path;
use Getopt::Long;
use Cwd;
use POSIX qw(floor);

my $binDir = "/home/bcs/des_prereq/bin";

my ($fileName);

Getopt::Long::GetOptions(
   "filename=s" => \$fileName,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply a filename") unless defined $fileName;

  my $path = dirname($fileName);
  my $baseName = basename($fileName);


  if (!-e $fileName){
    print "$baseName not in $path\n";
    next;
  }

  my $status;
  my $fptr = Astro::FITS::CFITSIO::open_file(
      "$path/$baseName",
      READWRITE,
      $status
      );

  my ($naxis1,$naxis2,$xcen,$ycen);
  $fptr->read_key_str('NAXIS1',$naxis1,undef,$status);
  $fptr->read_key_str('NAXIS2',$naxis2,undef,$status);
  $xcen = $naxis1 / 2;
  $ycen = $naxis2 / 2;

#
# xy2sky command line:
#  j = print sky coords in J2000 (FK5) ra/dec
#  g = Print sky coordinates as galactic longitude and latitude
#
  my $output = `xy2sky -j $fileName $xcen $ycen`;
  
  my @output = split / /,$output;
  my ($ra,$dec) = @output;
  
  $fptr->update_key_str('RA', $ra, "RA of central pixel ($xcen,$ycen)", $status);
  $fptr->update_key_str('DEC', $dec, "DEC of central pixel($xcen,$ycen)", $status);

exit(0);

#
# Subroutines
#

sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -archiveroot archivedir -tilename tilename -band g|r|i|z [-run runId]".
      " [-psfoff] [-psfsize psfsize] [-fwhmFrac (0.0 - 1.0)]\n" .
      "       tilename is the name of the tile you want to use\n" .
      "       band is the color of the filter\n" .
      "       run is optional\n" .
      "       psfoff turns off the psf normalization step\n" .
      "       psfsize is the desired size of psf kernel in pixels\n" .
      "       fwhmFrac is the point in the sorted distribution of FHWM's \n" .
      "       you want to use, where the median is 0.5, 0.0 is the lowest,\n" .
      "       1.0 is the highest.\n"
   );

   die("\n")

}
