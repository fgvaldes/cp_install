#!/usr/bin/env perl
########################################################################
#  $Id: crosstalk.pl 14589 2013-09-12 20:29:44Z ankitc $
#
#  $Rev:: 14589                            $:  # Revision of last commit.
#  $LastChangedBy:: ankitc                 $:  # Author of last commit. 
#  $LastChangedDate:: 2013-09-12 13:29:44 #$:  # Date of last commit.
#
#  Authors: 
#         Joe Mohr (jmohr@uiuc.edu)
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#       Replacement for deprecated convert_ingest.c.   Given a list of 
#       images, run the appropriate convert program on each image to split
#       the image and do crosstalk correction.  File ingestion into DB 
#       has been separated into a different script.
#
#      
#
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking

use strict;
use warnings;

use Getopt::Long;
use File::Basename;
use File::Path;
use FindBin qw($Bin);
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5" );

use DB::EventUtils;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($image_list, $crosstalk, $fixim, $detector, $gzip, $photflag) = undef;
my ($quiet, $verbose, $bin_path, $output_path, $archive_root) = undef;
my ($oscanorder, $oscanfunction, $oscansample, $oscantrim,$presatmask,$postsatmask,$replace) = undef;
Getopt::Long::GetOptions(
    "archiveroot=s" => \$archive_root,
    "list=s" => \$image_list,
    "crosstalk=s" => \$crosstalk,
    "fixim=s" =>  \$fixim,
    "detector=s" => \$detector,
    "binpath=s" => \$bin_path,
    "outputpath=s" => \$output_path,
    "photflag=i" => \$photflag,
    "presatmask" => \$presatmask,
    "postsatmask" => \$postsatmask,
    "replace=s" => \$replace,
    "gzip" => \$gzip,
    "quiet" => \$quiet,
    "verbose=s" => \$verbose,
    "overscanorder=s" => \$oscanorder,
    "overscanfunction=s" => \$oscanfunction,
    "overscansample=s" => \$oscansample,
    "overscantrim=s" => \$oscantrim,
);

if (defined($quiet)) {
    $verbose = 0;
}

if (!defined($verbose)) {
    $verbose = 1;
}

# check valid detector value
if (!defined($detector) || 
    (($detector !~ /mosaic2/i) && ($detector !~ /decam/i)) && ($detector !~ /megacam/i)) {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2); 
    print STDERR "Error: must specify a valid detector\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}

# check valid photflag value
if (!defined($photflag) || 
    (($photflag != 0) && ($photflag != 1))) {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: must specify a valid photflag (0 or 1)\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}

# check valid archive_root value
if (defined($archive_root) && (! -d $archive_root)) {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: archive_root is not a valid directory: $archive_root\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}

my @images = ();

# get list of src files
if (defined($image_list)) {
    if (($image_list !~ /^\//) && defined($archive_root)) {
        $image_list = $archive_root."/".$image_list;
    }
    if (! -r $image_list) {
        print STDERR "STATUS4BEG\n" if ($verbose >= 2);
        print STDERR "Error: Cannot read image list '$image_list'\n";
        print STDERR "STATUS4END\n" if ($verbose >= 2);
        exit 1;
    }
    open FILE, "< $image_list" || die "Error: Could not open list file '$image_list'\n";
    while (my $line = <FILE>) {
        my $image_name = $line;
        chomp($image_name);
        push (@images, $image_name);
    }
    close FILE
}
else {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: must provide a file containing a list of images.\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}

# if relative path, prepend archive root
if (defined($output_path)) {
    if (($output_path !~ /^\//) && defined($archive_root)) {
        $output_path = $archive_root."/".$output_path;
    }
}
else {
    $output_path = ".";
}


printf "  Discovered %d images\n", scalar(@images) if ($verbose > 0);


if (scalar(@images) == 0) {
    print "STATUS4BEG\n" if ($verbose >= 2);
    print "Error: 0 images to split and crosstalk correct\n";
    print "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}


# for each image
foreach my $image (@images) {
    if ($verbose > 0) {
        print "Creating command line for image: $image\n";
    }

    my $exposure_name = basename($image,(".fits",".fits.gz",".fits.fz"));

    my $exposure_path = $output_path."/".$exposure_name;

    # call convert program to split and do crosstalk correction
    my $cmd = "";

    # include path to convert program if specified
    if (defined($bin_path)) {
        $cmd .= "$bin_path/";
    }

    # pick convert program based upon detector
    if ($detector =~ /mosaic2/i) {
        $cmd .= "Mosaic2_crosstalk";
    }
    elsif ($detector =~ /decam/i) {
        $cmd .= "DECam_crosstalk";
    }
    else {
	$cmd .= "MegaCam_crosstalk" ;
    }
    if (($image !~ /^\//) && defined($archive_root)) {
        $image = $archive_root."/".$image;
    }

    # add arguments 
    my $output_prefix = "$exposure_path/$exposure_name";
    $cmd .= " $image $output_prefix";

    $cmd .= " -verbose $verbose"; 

    if (defined($crosstalk)) {
        if (($crosstalk !~ /^\//) && defined($archive_root)) {
            $crosstalk = $archive_root."/".$crosstalk;
        }

        $cmd .= " -crosstalk $crosstalk";
    }
    if (defined($fixim)) {
        if (($fixim !~ /^\//) && defined($archive_root)) {
            $fixim = $archive_root."/".$fixim;
        }
        $cmd .= " -fixim $fixim";
    }
    if (defined($gzip)) {
        $cmd .= " -gzip";
    }
    if (defined($presatmask)) {
        $cmd .= " -presatmask";
    }
    if (defined($postsatmask)) {
        $cmd .= " -postsatmask";
    }
    if (defined($replace)) {
        if (($replace !~ /^\//) && defined($archive_root)) {
            $replace = $archive_root."/".$replace;
        }

        $cmd .= " -replace $replace";
    }

    if (defined($photflag)) {
        $cmd .= " -photflag $photflag";
    }

    if (defined($oscansample)) {
        $cmd .= " -overscansample $oscansample";
    }
    if (defined($oscanfunction)) {
        $cmd .= " -overscanfunction $oscanfunction";
    }
    if (defined($oscanorder)) {
        $cmd .= " -overscanorder $oscanorder";
    }
    if (defined($oscantrim)) {
        $cmd .= " -overscantrim $oscantrim";
    }
    print "Crosstalk command: $cmd\n" if ($verbose > 0);

    system($cmd);
    if ($? == -1) {
        my $eventstr = "Crosstalk failed to execute: $!";
        reportEvent($verbose, "STATUS", 5, $eventstr);
    }
    elsif ($? & 127) {
        my $eventstr = sprintf("Crosstalk died with signal %d, %s coredump", ($? & 127),  ($? & 128) ? 'with' : 'without');
        reportEvent($verbose, "STATUS", 5, $eventstr);
    }
    elsif ($? != 0) {
        my $eventstr = sprintf("Crosstalk exited with value %d", $? >> 8);
        reportEvent($verbose, "STATUS", 5, $eventstr);
    }
}

print "\n" if ($verbose >= 0);

exit 0;
