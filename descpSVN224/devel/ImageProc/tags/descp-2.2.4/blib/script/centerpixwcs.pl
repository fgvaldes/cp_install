#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# centerpixwcs.pl
#
# DESCRIPTION:
#   Driver script for pix2wcs.c
#
# AUTHOR:  
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev $
# $LastChangedBy: donaldp $
# $LastChangedDate: 2012-09-26 19:02:29 -0700 (Wed, 26 Sep 2012) $
#

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use Data::Dumper;
use Getopt::Long;
use FindBin;

use lib ("$FindBin::Bin/../../../Database/trunk/lib");
use DB::DESUtil;
use DB::IngestUtils;

my ($fileList,$outFile,$metaTable);

$outFile="test.out";

Getopt::Long::GetOptions(
   "outfile=s" => \$outFile,
   "table=s" => \$metaTable,
) or die("Invalid command line options\n");

#exit("You must supply an input filelist\n") if not $fileList;
#exit("You must supply an output filename\n") if not $outFile;

my $binPath = "~/desdm/devel/ImageProc/trunk/src";

#
# Make a database connection
#
my $dbh = DB::DESUtil->new
  (
             DBIattr => {
              AutoCommit => 0,
              RaiseError => 1,
              PrintError => 0
             }
  );

#
# Get ids from database
#

#open (my $FL, "<$fileList");
#my @ids = <$FL>;
#close($FL);

#my $idSql = qq{select id from image where project='DES' and (imagetype='remap' or imagetype='red') and nite='20081003'};
my $idSql = qq{
    SELECT location.id FROM location,$metaTable
    WHERE location.id = $metaTable.id AND 
          location.run='20090127154015_DES2226-4230'
   };

my $idSth = $dbh->prepare($idSql);
$idSth->execute();
my $ids = $idSth->fetchall_arrayref();
$idSth->finish();


open(my $FH, ">$outFile");

#
# Get the information from the image table
#
my $sql = qq{ SELECT * FROM $metaTable WHERE ID = ?};

my $sth = $dbh->prepare($sql);

foreach my $id (@{$ids}){
#foreach my $id (@ids){

  $id = $id->[0];
  #chomp($id);

  $sth->execute($id);

  my $result = $sth->fetchall_arrayref({});
  $sth->finish();
  $result = $result->[0];

  #my $mjdId = $result->{'exposureid'};

  #my ($mjdObs,$epoch);
  #$mjdSth->execute($mjdId);
  #$mjdSth->bind_columns(\$mjdObs,\$epoch);
  #$mjdSth->fetch();
  #$mjdSth->finish();
  #$mjdObs = $mjdObs;

  #$mjdObs = 54038.38108609 if ($id == 5096904);
  #$mjdObs = 54038.38108609 if ($id == 5096710);

  #$mjdObs = 2000.0 - (51544.50000 - $mjdObs)/365.25;
  #$mjdObs = 0.0;

#
# Print out the results to a file
#
  my @keys = (
      'naxis',
      'naxis1',
      'naxis2',
      'crval1',
      'crval2',
      'crpix1',
      'crpix2',
      'cd1_1',
      'cd1_2',
      'cd2_1',
      'cd2_2',
      'obsdate',
      'equinox',
      'pv1_0',
      'pv1_1',
      'pv1_2',
      'pv1_3',
      'pv1_4',
      'pv1_5',
      'pv1_6',
      'pv1_7',
      'pv1_8',
      'pv1_9',
      'pv1_10',
      'pv2_0',
      'pv2_1',
      'pv2_2',
      'pv2_3',
      'pv2_4',
      'pv2_5',
      'pv2_6',
      'pv2_7',
      'pv2_8',
      'pv2_9',
      'pv2_10',
      );

#
# Get the center pixel
#
  my $xpix = $result->{'naxis1'} / 2;
  my $ypix = $result->{'naxis2'} / 2;

  print $FH "$id,$xpix,$ypix,";
  my $epoch=2000.0;

#
# Need to write these out in a very specific order (as outlined in the
# order of elements in the array @keys).
#
  foreach my $key (@keys){
    if ($key eq 'naxis'){
      print $FH "2,";
    } elsif ($key eq 'obsdate'){
      #print $FH "$mjdObs,";
      print $FH "0.0,";
    } else {
      if ($result->{$key}){
        print $FH $result->{$key} . "," ;
      } else {
        print $FH "0.0,";
      }
    }
  }
  print $FH "$epoch,";
  print $FH "$metaTable";
  print $FH "\n";

}

close($FH);

#
# Now, run pix2wcs, sending it the param file just created
#

my @allLines = `$binPath/pix2wcs $outFile`;

my $updateHashRef;
foreach my $line (@allLines){
  chomp($line);
  my ($imageid,$ra,$dec,$metaTable) = split /,/, $line;
  push @{$updateHashRef->{$metaTable}->{'ID'}},$imageid;
  push @{$updateHashRef->{$metaTable}->{'RA'}},$ra;
  push @{$updateHashRef->{$metaTable}->{'DEC'}},$dec;
  print "updating $imageid:  $ra,$dec\n";
}

my $numRowsUpdated = batchIngest($dbh,$updateHashRef,1);

$dbh->commit();
$dbh->disconnect();

exit(0);
