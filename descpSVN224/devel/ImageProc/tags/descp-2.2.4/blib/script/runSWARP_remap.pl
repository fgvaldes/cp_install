#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#
#  $Id: runSWARP_remap.pl 8754 2012-09-27 02:02:29Z donaldp $
#
#  $Rev:: 8754                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-26 19:02:29 #$:  # Date of last commit.
#
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use warnings;

use Carp;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use File::Path;
use Astro::FITS::CFITSIO qw(:constants);
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use ImageProc::SWARP qw(:all);
use ImageProc::Util qw(appendArgs readFitsHeaderKeys getDesHdus);

$|=1;

my $verbose = 1;
my $swarp = 'swarp';
my $funpack = 'funpack';
my $outdir = '.';
my ($help,$man);
my ($list,$bindir,$stat,$image_mode,$bpm_mode,$nthread,$config)=undef;

# Definitions of FITS header keys that need to be copied from the image HDU to
# the bpm HDU:
my %WCS_keys = (
  'EQUINOX' =>  {'dtype' => TFLOAT,  'comment' => 'Equinox'},
  'RADECSYS' => {'dtype' => TSTRING, 'comment' => 'Astrometric system'},
  'CTYPE1' =>   {'dtype' => TSTRING, 'comment' => 'WCS projection type for this axis'},
  'CTYPE2' =>   {'dtype' => TSTRING, 'comment' => 'WCS projection type for this axis'},
  'CUNIT1' =>   {'dtype' => TSTRING, 'comment' => 'Axis unit'},
  'CUNIT2' =>   {'dtype' => TSTRING, 'comment' => 'Axis unit'}, 
  'CRVAL1' =>   {'dtype' => TDOUBLE, 'comment' => 'World coordinate on this axis'},
  'CRVAL2' =>   {'dtype' => TDOUBLE, 'comment' => 'World coordinate on this axis'},
  'CRPIX1' =>   {'dtype' => TDOUBLE, 'comment' => 'Reference pixel on this axis'},
  'CRPIX2' =>   {'dtype' => TDOUBLE, 'comment' => 'Reference pixel on this axis'},
  'CD1_1' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'CD1_2' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'CD2_1' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'CD2_2' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'PV1_0' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_1' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_2' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_3' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_4' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_5' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_6' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_7' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_8' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_9' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_10' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_0' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_1' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_2' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_3' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_4' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_5' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_6' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_7' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_8' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_9' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_10' =>      {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'WCSDIM' =>      {'dtype' => TINT,    'comment' => 'WCS Dimensionality'}
);


# Grab the wrapper options and the SWARP options that are also
# handles and set by this code.  the rest of the command line
# (whatever's left) will be assumed to be additional SWARP arguments:
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
$stat = Getopt::Long::GetOptions(
      'list=s' => \$list,
      'bindir=s' => \$bindir,
      'image|im' => \$image_mode,
      'bpm' => \$bpm_mode,
      'outdir|od=s' => \$outdir,
      'config|c=s' => \$config,
      'nthread=i' => \$nthread,
      'verbose|V=s' => \$verbose,
      'help|h|?' => \$help,
      'man' => \$man
);

# Display documantation and exit if requested:
if ($help) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($man) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($list)) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide an input list of images.'
  );
}

if (! -r $list) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2,-output => \*STDERR,
    -message => "Could not read image list: '$list'."
  );
}

if (defined ($image_mode) && defined ($bpm_mode)) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => 'runSWARP_remap will only operate in image OR bpm mode'
  );
}

if (! defined ($image_mode) && !defined ($bpm_mode)) {
Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => 'Must specify image or bpm mode'
  );

}

if ($bindir) {
  $swarp = join('/',$bindir,$swarp);
  $funpack = join('/',$bindir,$funpack);
}

if (! -x $swarp) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => "Cannot execute $swarp"
  );
}

if (! -x $funpack) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => "Cannot execute $funpack"
  );
}

if (! -d $outdir) {
  mkdir $outdir;
}


# Parse Input file:
my $value_list = readSWARPInputFile($list);

foreach my $set (@$value_list) {

  my %Args;
  my $swarp_cmd = undef;
  # Better hope the input file is right...:
  my $infile = $set->[0];
  my $tilename = $set->[1];
  my $pixelsize = $set->[2];
  my $tile_ra = $set->[3];
  my $tile_dec = $set->[4];
  my $tile_npixra = $set->[5];
  my $tile_npixdec = $set->[6];

#  $infile =~ /([^\/]+/)$/;
#  my $infile_basename = $1;
#  $infile_basename =~ s/\.fits//;
#  my $var_out = $infile_basename.'_'.$tilename.'_var.fits';


  # Get remap location from red location:
  my ($basename,$path,$suffix) = fileparse($infile,('.fits','.fits.gz','.fits.fz'));
  $path =~ s/\/\s*$//g;
  $path =~ /\/([^\/]+)$/;
  my $exposurename = $1;
  $path =~ s/\/[^\/]+$//;
  $path =~ s/\/[^\/]+$//;
  $path = "$outdir/$exposurename";
  if (! -d $path) {
    mkdir $path;
  }
  my $var_out = $basename.'_'.$tilename.'_var.fits';


  $Args{'-CENTER_TYPE'} = 'MANUAL';
  $Args{'-PIXEL_SCALE'} = $set->[2];
  $Args{'-CENTER'} = join(',',$set->[3],$set->[4]);
  $Args{'-IMAGE_SIZE'} = join(',',$set->[5],$set->[6]);
  $Args{'-RESAMPLE'} = 'Y';
  $Args{'-RESAMPLE_DIR'} = $path;


  # For "image_mode", use the first HDU as input and utilize the third
  # as the weight map, For bpm use the 2nd HDU as input and no weight map  

  my $rm = 0;
  # If the input file does not exist, look for the fz compressed version.
  # When this is a bpm run copy headers from image to bpm extension
  # if needed, before unpacking.
  if (!  -r $infile) {
    if (-r "$infile.fz") {
      print "Unpacking fz file...";
      system ("$funpack $infile.fz");
      $rm = 1;
    }
    else {
      croak("Cannot read $infile or $infile.fz\n");
    }
  }



  if ($image_mode) {
    $swarp_cmd  = join(' ', $swarp, join('',$infile,'[0]'));
    $Args{'-WEIGHT_TYPE'} = 'MAP_WEIGHT';
    $Args{'-WEIGHT_IMAGE'} = join('',$infile,'[2]');
    $Args{'-RESAMPLE_SUFFIX'} = '_'.$tilename.'_im.fits';
    #$Args{'-WEIGHTOUT_NAME'} = join('/',$path,$var_out);
  }
  if ($bpm_mode) {
    $swarp_cmd  = join(' ', $swarp, join('',$infile,'[1]'));
    $Args{'-WEIGHT_TYPE'} = 'NONE';
    $Args{'-RESAMPLE_SUFFIX'} = '_'.$tilename.'_bpm.fits';
  }

    if (defined($config)) {
        if (! -r $config) {
            print "\nSTATUS4BEG\n" if ($verbose >= 2); 
            print "WARNING: Could not read config file '$config'.   Calling swarp without -c option.\n";
            print "STATUS4BEG\n\n" if ($verbose >= 2); 
        }
        else {
            $swarp_cmd .= " -c $config ";
        }
    }

    if (defined($nthread)) {
        $swarp_cmd .= " -NTHREADS $nthread ";
    }

  $swarp_cmd = appendArgs($swarp_cmd, \%Args);
  $swarp_cmd = join(' ',$swarp_cmd, @ARGV);



  print "\nSTATUS1BEG\n" if ($verbose >= 2); 
  print "\nRunning:\n$swarp_cmd\n\n";
  print "STATUS1BEG\n\n" if ($verbose >= 2); 


  runSWARP($swarp_cmd, $verbose);

  if ($rm) {
    print "Removing temporary uncompressed input file: $infile\n";
    system("rm -f $infile");
  }

}



=head1 NAME B<runSWARP_remap.pl> - Wrapper to run TeraPix SWARP software on DESDM data sets.

=head1 SYNOPSIS

B<runSWARP_remap.pl> -list <file list> [-bindir <bin directory with swarp>] [-- [Additional swarp options] ]

=head1 DESCRIPTION

=head1 OPTIONS

=over 4

=item B<-bindir> I<directory>

The Directory containing the Terapix B<swarp> executable.

=item B<-bpm>

Indicates that the remapping is to be done on the FITS extension containing the bpm data.

=item B<-help>, B<-h>, B<-?>

Display brief help and exit.

=item B<-image>, B<-im>

Indicates that the remapping is to be done on the FITS extension containing the image data.

=item B<-man>

Display full manual page and exit.

=item B<-verbose>, B<-V> I<Number>

Verbose level 0-5. Default is 1.  Level 2 and above will turn on status event syntax.


=back

=head1 EXAMPLES

=over 4

=item Example running on I<image> extension for remap in nightly processing:

runSWARP_remap.pl -bindir $DES_PREREQ/bin -outdir /gpfs_scratch1/bcs/Archive/DES/red/20080821151321_20071002/remap 
-list aux/swarp_input_0001 -image -- -SUBTRACT_BACK Y -FSCALASTRO_TYPE NONE -RESAMPLING_TYPE LANCZOS3 
-COMBINE N -COMBINE_TYPE WEIGHTED -COPY_KEYWORDS OBJECT,OBSERVER,CCDNUM,FILTER,OBSTYPE,FILENAME,TELRA,TELDEC,TELEQUIN,AIRMASS,DATE-OBS,EXPTIME,SATURATE,DETECTOR,TELESCOP,OBSERVAT,SKYBRITE,SKYSIGMA,PHOTFLAG,FWHM,ELLIPTIC,SCAMPCHI,SCAMPFLG,SCAMPNUM
-DELETE_TMPFILES Y  -HEADER_ONLY N -VERBOSE_TYPE FULL

=item Example running on I<bpm> extension for remap in nightly processing:

runSWARP_remap.pl -bindir $DES_PREREQ/bin -outdir /gpfs_scratch1/bcs/Archive/DES/red/20080821151321_20071002/remap 
-list aux/swarp_input_0001 -bpm -- -SUBTRACT_BACK N -FSCALASTRO_TYPE NONE -RESAMPLING_TYPE NEAREST -COMBINE N
-DELETE_TMPFILES Y  -HEADER_ONLY N -VERBOSE_TYPE FULL

=back
