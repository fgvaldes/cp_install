#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#
#  $Id: remap.pl 3576 2009-05-15 18:24:28Z mgower $
#
#  $Rev:: 3576                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-05-15 13:24:28 #$:  # Date of last commit.
#
########################################################################

use strict;
use warnings;

use Carp;
use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use File::Path;
use Astro::FITS::CFITSIO qw(:constants);
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::EventUtils;
use ImageProc::SWARP qw(:all);
use ImageProc::Util qw(appendArgs readFitsHeaderKeys updateFitsHeaderKeys);

$|=1;

# Define DES header keywords to be inserted into remap MEF:
my %key_defs = (
  'DES_EXT' => {'dtype' => TSTRING, 'comment' => 'Image Extension'},
  'OBSTYPE' => {'dtype' => TSTRING, 'comment' => 'Observation type'}
);


my $verbose = 1;
my $swarp = 'swarp';
my $funpack = 'funpack';
my $fitscombine = 'fitscombine';
my $outdir = '.';

my ($help,$man,$exit);
my ($list,$prereqbin,$bindir,$stat,$image_mode,$bpm_mode,$nthread,$fscaleastrovariable,$config,$cleanup)=undef;

# Grab the wrapper options and the SWARP options that are also
# handles and set by this code.  the rest of the command line
# (whatever's left) will be assumed to be additional SWARP arguments:
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
$stat = Getopt::Long::GetOptions(
      'list=s' => \$list,
      'prereqbin=s' => \$prereqbin,
      'binpath|bindir=s' => \$bindir,
      'outpath|outdir|od=s' => \$outdir,
      'cleanup' => \$cleanup,
      'config|c=s' => \$config,
      'nthread=i' => \$nthread,
      'verbose|V=s' => \$verbose,
      'fscaleastrovariable' => \$fscaleastrovariable, 
      'help|h|?' => \$help,
      'man' => \$man
);

# Display documantation and exit if requested:
if ($help) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($man) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($list)) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide an input list of images.'
  );
}

if (! -r $list) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2,-output => \*STDERR,
    -message => "Could not read image list: '$list'."
  );
}

if (defined($bindir)) {
  $swarp = $prereqbin.'/'.$swarp;
  $funpack = $prereqbin.'/'.$funpack;
}

if (defined($bindir)) {
  $fitscombine = $bindir.'/'.$fitscombine;
}

if (! -x $swarp) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => "Cannot execute $swarp"
  );
}

if (! -x $funpack) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => "Cannot execute $funpack"
  );
}

if (! -x $fitscombine) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => "Cannot execute $fitscombine"
  );
}
if (defined($cleanup)) {
    $fitscombine .= ' -cleanup ';
}

if (! -d $outdir) {
    eval { mkpath($outdir) };
    if ($@) {
        reportEvent($verbose, "STATUS", 5, "Error:  Could not make dir $outdir");
        exit 1;
    }
}

my $base_swarp_cmd = $swarp;
if (defined($config)) {
    if (! -r $config) {
        reportEvent($verbose, "STATUS", 4, "WARNING: Could not read config file '$config'.   Calling swarp without -c option.");
    }
    else {
        $base_swarp_cmd .= " -c $config ";
    }
}

if (defined($nthread)) {
    $base_swarp_cmd .= " -NTHREADS $nthread ";
}



my $value_list = readSWARPInputFile($list);
my $errflag = 0;

my $linecnt = 1;
foreach my $line (@$value_list) {
    print "Starting remap work on line $linecnt\n";
    my %Args;
    my $swarp_cmd = $base_swarp_cmd;
    
    if (scalar(@$line) < 7) {
        reportEvent($verbose, "STATUS",5,"Error: line $linecnt does not have enough values ".Dumper($line)); 
        $linecnt++;
        next;
    }
    my $infile = $line->[0];
    my $tilename = $line->[1];
    my $pixelsize = $line->[2];
    my $tile_ra = $line->[3];
    my $tile_dec = $line->[4];
    my $tile_npixra = $line->[5];
    my $tile_npixdec = $line->[6];

    # Get exposure from red location to use in remap location
    my ($basename,$path,$suffix) = fileparse($infile,('.fits','.fits.gz','.fits.fz'));
    $path =~ s/\/\s*$//g;
    $path =~ /\/([^\/]+)$/;
    my $exposurename = $1;
    $path = "$outdir/$exposurename";

    my $remap_name = $basename.'_'.$tilename.'.fits';
    if (-r "$path/$remap_name") {
        reportEvent($verbose,"STATUS",4,"WARNING: skipping image $linecnt.  Remap image already exists $path/$remap_name");
        $linecnt++;
        next;
    }

    # If the input file does not exist, look for the fz compressed version.
    my $uncompressed = 0;
    if (!  -r $infile) {
        if (-r "$infile.fz") {
            print "Unpacking fz file...";
            system ("$funpack $infile.fz");
            $uncompressed = 1;
        }
        else {
            reportEvent($verbose, "STATUS", 5, "$linecnt: Cannot read input file $infile or $infile.fz");
            $errflag=1;
            $linecnt++;
            next;
        }
    }

    if (! -d $path) {
        mkdir $path;
    }

    $Args{'-CENTER_TYPE'} = 'MANUAL';
    $Args{'-PIXEL_SCALE'} = $pixelsize;
    $Args{'-CENTER'} = $tile_ra.','.$tile_dec;
    $Args{'-IMAGE_SIZE'} = $tile_npixra.','.$tile_npixdec;
    $Args{'-RESAMPLE'} = 'Y';
    $Args{'-RESAMPLE_DIR'} = $path;


    ##### IMAGE
    # For "image_mode", use the first HDU as input and utilize the third as the weight map 
    $swarp_cmd  = $base_swarp_cmd.' '.$infile.'[0] ';
    $swarp_cmd .= ' -WEIGHT_TYPE MAP_WEIGHT ';
    $swarp_cmd .= ' -WEIGHT_IMAGE  '.$infile.'[2] ';
    $swarp_cmd .= ' -RESAMPLE_SUFFIX _'.$tilename.'_im.fits ';
    
    $swarp_cmd = appendArgs($swarp_cmd, \%Args);
    if (defined($fscaleastrovariable)) {
        $swarp_cmd .= " -FSCALASTRO_TYPE VARIABLE ";
    }
    else {
        $swarp_cmd .= " -FSCALASTRO_TYPE NONE ";
    }
    $swarp_cmd .= ' -SUBTRACT_BACK Y -RESAMPLING_TYPE LANCZOS3 -COMBINE N -COMBINE_TYPE WEIGHTED -COPY_KEYWORDS OBJECT,OBSERVER,CCDNUM,FILTER,OBSTYPE,FILENAME,TELRA,TELDEC,TELEQUIN,AIRMASS,DATE-OBS,EXPTIME,SATURATE,DETECTOR,TELESCOP,OBSERVAT,SKYBRITE,SKYSIGMA,PHOTFLAG,FWHM,ELLIPTIC,SCAMPCHI,SCAMPFLG,SCAMPNUM -DELETE_TMPFILES Y  -HEADER_ONLY N -VERBOSE_TYPE FULL ';
    $swarp_cmd = join(' ',$swarp_cmd, @ARGV);

    reportEvent($verbose, "STATUS", 1, "$linecnt: Running $swarp_cmd");
    $exit = runSWARP($swarp_cmd, $verbose);
    if ($exit != 0) {
        reportEvent($verbose, "STATUS", 5, "$linecnt: first swarp exited with non-zero exit code ($exit)");
        $linecnt++;
        next;
    }
    


    ##### BPM
    # For bpm use the 2nd HDU as input and no weight map  
    $swarp_cmd  = $base_swarp_cmd.' '.$infile.'[1] ';
    $swarp_cmd = appendArgs($swarp_cmd, \%Args);
    $swarp_cmd .= ' -WEIGHT_TYPE NONE ';
    $swarp_cmd .= ' -RESAMPLE_SUFFIX _'.$tilename.'_bpm.fits ';
    $swarp_cmd .= ' -SUBTRACT_BACK N -FSCALASTRO_TYPE NONE -RESAMPLING_TYPE NEAREST -COMBINE N -DELETE_TMPFILES Y  -HEADER_ONLY N -VERBOSE_TYPE FULL ';
    $swarp_cmd = join(' ',$swarp_cmd, @ARGV);

    reportEvent($verbose, "STATUS", 1, "$linecnt: Running $swarp_cmd");
    $exit = runSWARP($swarp_cmd, $verbose);
    if ($exit != 0) {
        reportEvent($verbose, "STATUS", 5, "$linecnt: second swarp exited with non-zero exit code ($exit)");
        $linecnt++;
        next;
    }

    ##### create MEF
    my $im_name = $basename.'_'.$tilename.'_im.fits';  
    my $imvar_name = $basename.'_'.$tilename.'_im.weight.fits';   
    my $bpm_name = $basename.'.0001_'.$tilename.'_bpm.fits';  
    my $bpmvar_name = $basename.'.0001_'.$tilename.'_bpm.weight.fits';  

    if (! -r "$path/$im_name" ) {
        reportEvent($verbose,"STATUS",4,"Unable to read: $path/$im_name");
        $errflag = 1;
    }
    if (! -r "$path/$bpm_name" ) {
        reportEvent($verbose,"STATUS",4,"Unable to read: $path/$bpm_name");
        $errflag = 1;
    }
    if (! -r "$path/$imvar_name") {
        reportEvent($verbose,"STATUS",4,"Unable to read: $path/$imvar_name");
        $errflag = 1;
    }
    if ((-r "$path/$im_name") && (-r "$path/$bpm_name") && (-r "$path/$imvar_name") ) {

        my $this_command = "$fitscombine $path/$im_name $path/$bpm_name $path/$imvar_name $path/$remap_name";
        print "\n$linecnt: Combining remapped image, mask and weight";
        print "$this_command";
        my $stat = system ("$this_command");
        if ($stat) {
            $errflag = 1;
            print STDERR "Error: $this_command exited with non-zero exit code ($stat, $infile, $tilename)\n";
        }
        else {
            # Update DES_EXT, OBSTYPE header keywords
            updateFitsHeaderKeys ({
                'fits_file' => "$path/$remap_name",
                'key_defs' => \%key_defs,
                'values' => {'DES_EXT'=>'IMAGE','OBSTYPE'=>'remap'},
                'hdu' => 1,
                'verbose' => $verbose
            });
            updateFitsHeaderKeys ({
                'fits_file' => "$path/$remap_name",
                'key_defs' => \%key_defs,
                'values' => {'DES_EXT'=>'MASK','OBSTYPE'=>'remap'},
                'hdu' => 2,
                'verbose' => $verbose
            });
            updateFitsHeaderKeys ({
                'fits_file' => "$path/$remap_name",
                'key_defs' => \%key_defs,
                'values' => {'DES_EXT'=>'WEIGHT','OBSTYPE'=>'remap'},
                'hdu' => 3,
                'verbose' => $verbose
            });
        }
    }
    else {
        print "$linecnt: Skipping fitscombine\n";
    }

    ##### CLEANUP
    print "$linecnt: Removing intermediate files\n";
    if ($uncompressed) {
        print "Removing temporary uncompressed input file: $infile\n";
        system("rm -f $infile");
    }
    
    # Remove bpm weight map
    if ($cleanup) {
        if (-r "$path/$im_name") {
            unlink "$path/$im_name";
        }
        if (-r "$path/$imvar_name") {
            unlink "$path/$imvar_name";
        }
        if (-r "$path/$bpm_name") {
            unlink "$path/$bpm_name";
        }
        if (-r "$path/$bpmvar_name") {
            unlink "$path/$bpmvar_name";
        }
        if (! -r "$path/$remap_name") {
            rmdir "$path";
        }
    }
    $linecnt++;
}

exit $errflag;



=head1 NAME B<remap.pl> - Wrapper to run TeraPix SWARP software on DESDM data sets.

=head1 SYNOPSIS

B<remap.pl> -list <file list> [-bindir <bin directory with swarp>] [-- [Additional swarp options] ]

=head1 DESCRIPTION

=head1 OPTIONS

=over 4

=item B<-bindir> I<directory>

The Directory containing the Terapix B<swarp> executable.

=item B<-bpm>

Indicates that the remapping is to be done on the FITS extension containing the bpm data.

=item B<-help>, B<-h>, B<-?>

Display brief help and exit.

=item B<-image>, B<-im>

Indicates that the remapping is to be done on the FITS extension containing the image data.

=item B<-man>

Display full manual page and exit.

=item B<-verbose>, B<-V> I<Number>

Verbose level 0-5. Default is 1.  Level 2 and above will turn on status event syntax.


=back
