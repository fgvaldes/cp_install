#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#  $Id:                                            $
#
#  $Rev:: 3202                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-02-23 15:59:53 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use warnings;

use Data::Dumper;
use Getopt::Long;
use File::Basename;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

use DB::EventUtils;

$|=1;

my ($binpath, $logprefix, $list, $writedb, $verbose) = undef;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'list=s' => \$list,
      'binpath=s' => \$binpath,
      'logprefix=s' => \$logprefix,
      'writedb=i' => \$writedb,
      'verbose|V=i' => \$verbose,
    
);

# Check inputs
if (!defined($binpath) || !defined($list)) {
    reportEvent($verbose,"STATUS",5,"Error: Missing required arguments");
    Usage();
    exit 1;
}

# set defaults
if (!defined($logprefix)) {
    $logprefix = './filterObj_';
}
if (!defined($verbose)) {
    $verbose = 1;
}
if (!defined($writedb)) {
    $writedb = 1;
}


my $opensuccess = open FH, "< $list";
if (!$opensuccess) {
    print STDERR "Could not open file '$list'\n";
    exit 1;
}
foreach my $line (<FH>) {
    if ($line =~ /\S/) {
        $line =~ s/^\s+//;
        $line =~ s/\s+$//;
        if ($line =~ /^(\S+)\s+(\S+)$/) {
            my $cat = $1;
            my $id = $2;

            my ($name,$path,$suffix) = fileparse($cat,'.cat');
            $name =~ s/_(nc)*cat$//;
            my $log = $logprefix.$name.'.log';

            my $cmd = "$binpath/filterObj file=$cat comb=$id writedb=$writedb log=$log";
            reportEvent($verbose,"STATUS",1,"Executing: '$cmd'");
            reportEvent($verbose, "STATUS", 1, "Start application filterObj") if $verbose > 3;
            system("$cmd 2>&1");
            my $stat = $?;
            reportEvent($verbose, "STATUS", 1, "End application filterObj") if $verbose > 3;
            if ($stat != 0) {
                reportEvent($verbose, "STATUS", 5, "$cat: ERR non-zero exit code: $stat(".($stat>>8).")");
                exit $stat;
            }
        }
        else {
            reportEvent($verbose, "STATUS", 4, "Warning: skipping bad input line: $line");
        }
    }
}
close FH;

exit 0;

sub Usage {
    print STDERR  "runFilterObj.pl -binpath <path> -list <list>\n";
}


