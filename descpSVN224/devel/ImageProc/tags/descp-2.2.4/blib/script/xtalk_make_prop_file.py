#!/usr/bin/python

# creates input configuration file for xtalk.
# to mark a source to be skipped use -s (can be used multiple times).
#
# whanlon@illinois.edu
# 9 jun 2011

import sys

checkList = [1]*62

skipList = []

# use -s to skip as a source

for i in range(1, len(sys.argv)):
    ind = sys.argv[i].find("-s")  
    if (ind == -1):
        #print("skipping arg: %s" % sys.argv[i])
        continue

    try:
        if (sys.argv[i].strip() == "-s"):
            if (sys.argv[i + 1][0] != "-"):
                arg = sys.argv[i + 1]
            else:
                continue
        else:
            arg = sys.argv[i][ind + 2:]
    except Exception:
        continue

    chip = int(arg)
    if (chip < 1 or chip > 62):
        continue
    skipList.append(chip)


print("# %4s %6s %5s %9s %9s" % ('ampA', 'ampB', 'check', 'lrReflect',
                                 'udReflect'))

for ccd1 in range(1, 63):
    for amp1 in ['A', 'B']:
        for ccd2 in range(1, 63):
            for amp2 in ['A', 'B']:
                samp = ("ccd%02d%s" % (ccd1, amp1))
                vamp = ("ccd%02d%s" % (ccd2, amp2))
                if (samp == vamp):
                    check = 0
                    lr = 0
                else:
                    check = 1

                    for i in skipList:
                        if (i == ccd1):
                            check = 0

                    lr = 1
                ud = 0
                print("%6s %6s %5d %9d %9d" % (samp, vamp, check, lr, ud))


