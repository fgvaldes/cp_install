#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
#$Id: runWL_multishear.pl 8754 2012-09-27 02:02:29Z donaldp $#

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking

use strict;
use warnings;
use File::Basename;
use File::Path;
use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib ("$FindBin::Bin/../lib",
         "$FindBin::Bin/../lib/perl5",
         "$FindBin::Bin/../../../Database/trunk/lib"
    );
use DB::FileUtils;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

$|=1;

my $verbose = 1;
my (%opts, $multishear,$output,$config_dir);

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
my $status = Getopt::Long::GetOptions(\%opts,
      'list=s',
      'coadd|coaddimage_file=s',			    
      'coaddcat|coaddcat_file=s',			    
      'bindir=s',
      'outdir|output-dir|od=s',
      'output|output_file=s',
      # 'config_science=s',
      'config_dict=s',
      'config_desdm=s',
      'config_ops=s',
      'debug_wl=s',
      'verbose|V=s',
      'help|h|?',
      'man'
);

# Display documentation and exit if requested:
if ($opts{'help'}) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($opts{'man'}) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($opts{'list'})) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide an input list of images.'
  );
}

if (!defined($opts{'coadd'})) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide a coadd image'
  );
}

if (!defined($opts{'coaddcat'})) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide a coadd catalog'
  );
}

if (!defined($opts{'output'})) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide a multishear output filename'
  );
}

if (! $opts{'config_dict'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide WL dictionary configuration file '-config_dict'."
  );
}

if (! $opts{'config_desdm'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide DESDM-specific configuration file '-config_desdm'."
  );
}

if (! $opts{'config_ops'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide operations level WL configuration file '-config_ops'."
  );
}


if (! -r $opts{'list'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Could not read image list: '".$opts{'list'}."'."
  );
}


if ($opts{'outdir'}) {
  if (! -d $opts{'outdir'}) {
    mkpath($opts{'outdir'});
  }
  $output=$opts{'outdir'}."/".$opts{'output'};
}
else {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide output directory '-outdir'."
  );
}

if (! $opts{'bindir'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide bin directory '-bindir'."
  );
}
$multishear = join('/',$opts{'bindir'},'multishear');
die "Cannot execute $multishear" if (! -x $multishear);

open (FH,"<".$opts{'list'});
my $file_list=$opts{'list'}.".fz";
open (OUT,">".$file_list);
my @list = <FH>;
close FH;

foreach my $line (@list) {
  # Should make sure no whitespace creaps in to the values:
  chomp $line;
  my @args = split(/\s+/,$line);
  #die "\nERROR:Invalid input list provided.\n"if ($#args != 4);
  my $abs_red = $args[0];
  my $abs_shear = $args[1];
  my $abs_psf = $args[2];
  my $abs_bkg = $args[3];
  my ($base_imagename, $image_prefix, $image_ext) = fileparse($abs_red,'.fits','.fits.fz','.fits.gz');
  my $compressed = sniffForCompressedFile($abs_red);
  my $compressionType = ($compressed =~ m/(gz$|fz$)/) ? $1 : $2;

  $abs_red=$compressed if($compressed);

  ($base_imagename, $image_prefix, $image_ext) = fileparse($abs_bkg,'.fits','.fits.fz','.fits.gz');
  $compressed = sniffForCompressedFile($abs_bkg);
  $compressionType = ($compressed =~ m/(gz$|fz$)/) ? $1 : $2;

  $abs_bkg=$compressed if($compressed);
  print OUT "$abs_red $abs_shear $abs_psf $abs_bkg\n";
}
close OUT;

 my $configstr = sprintf(" %s +%s +%s", $opts{'config_dict'}, $opts{'config_desdm'}, $opts{'config_ops'});


 $multishear .= " $configstr coadd_srclist=$file_list";
 $multishear .= " coaddimage_file=$opts{'coadd'}";
 $multishear .= " coaddcat_file=$opts{'coaddcat'}";
 $multishear .= " multishear_file=$output";
 if (defined($opts{'debug_wl'})) {
    $multishear .= "verbose=$opts{'debug_wl'}";
 }

  print "\nExecuting:\n$multishear\n";
  my $stat = system("$multishear");

  # if output file doesn't exist or non-zero exit status, print message
  if (! -e $output || $stat != 0) {
    warn "\n\nSTATUS4BEG\n";
    warn "WARNING: multishear failed using list:$file_list\n".
	"using coaddimage:$opts{'coadd'}\nusing coaddcat:".
	"$opts{'coaddcat'}\n";
    warn "STATUS4END\n\n";
  }


exit 0;

########################################################################
# Documentation
########################################################################


=head1 NAME B<runWL_multishear.pl> - Wrapper to run multi epoch Weak Lensing software on DESDM data sets.

=head1 SYNOPSIS

runWL_multishear.pl [options] -list <inputlist> -coadd <input coadd image> -coaddcat <input coadd catalog> -outdir <output dir> -output <output file>

=head1 DESCRIPTION

=head1 OPTIONS

=over 4

=item B<-bindir> <path>

Path to WL executable multishear

=item B<-config_science> <configfile> 

Full path to WL config file (typically the one in the WL install)

=item B<-config_desdm> <configfile> 

Full path to DESDM-specific config file (i.e. operator and framework settings) 

=item B<-help>, B<-h>, B<-?>

Display brief help and exit.

=item B<-man>

Display full manual page and exit.

=item B<-debug_wl> <level>

Value passed as verbose to wl codes (needed because can be different than desdm framework verbose level)

=item B<-verbose>, B<-V> I<Number>

Verbose level 0-3. Default is 1.  Level 2 and above will turn on status event syntax.

=back

=head1 EXAMPLES
