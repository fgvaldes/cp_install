#!/usr/bin/env perl
########################################################################
#  $Id: create_CPREMAP_inputs.pl   1.0     $:
#
#  $Rev:: 1                                $:  # Revision of last commit.
#  $LastChangedBy:: kuropat                $:  # Author of last commit. 
#  $LastChangedDate:: 2012 02-10 11:20     #$:  # Date of last commit.
#
#  Authors:
#         Darren Adams (dadams@ncsa.uiuc.edu)
#         Michelle Gower (mgower@ncsa.uiuc.edu)  
#         Nikolay Kuropatkin (kuropat@fnal.gov)
#         
#
#  Developed at: 
#  Fermi National Accelerator Laboratory (FNAL)
#
#  
#  
#
#  DESCRIPTION:
#     The program is adoption of DESDM create_SWARP_inputs.pl
#     Creates input lists for cp_remap.pl program. The difference is
#     in tile assignment. Instead of DESDM fixed tiles the program read
#     file with tangent points which include point name, RA, Dec. Then
#	  the program detect for each input image bore sight the closest
#	  tangent point which is used as a re-map point creating output files
#     containing the point name instead of the tile name in DESDM.
#      
#
########################################################################

use warnings;
use strict;

#use Math::Round qw( :all);

use POSIX;

#use Astro::FITS::CFITSIO;
use Astro::FITS::CFITSIO qw(:constants);
use Astro::FITS::CFITSIO qw( :longnames );
use Astro::FITS::CFITSIO qw( :shortnames );
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use ImageProc::Util qw(readFitsHeaderKeys );
# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($stat);
my ($man, $help, $version, $verbose,$debug);
my ($detector, $coadd_project);
my ($output_path, $input_path, $input_base, $output_base, $minimum_overlap);
my ($tpointfile, $first);
my (@ImageList, @radec);
my ($bin_path);
my ($file, $imfile, $imkey);
my ($headers);
my (%tangent_hash, %filelists, %tangent_list);

# Set defaults:
$verbose = 1;
$first = 1;
$input_base = 'reduced_science';
$output_base = 'swarp_input';
$input_path = 'aux';
$output_path = 'aux';
$tpointfile = 'tangent_grid_test.txt';
#
# *******************************************
#  Subroutine to read TELRA and TELDEC from 
#  image header                              
#  return list (telra,teldec, key )
# *******************************************
sub getHeadKey {
my $file= $_[0];
my    $keyword = 'TELRA';
my    $dtype = 16;   # TSTRING
my    $image_hdu = 1;
my 	  $key = 0;
my @res =();
    $headers = readFitsHeaderKeys({'fits_file'=>$file, 'hdu'=>$image_hdu, 
                                  'key_defs'=>{$keyword=>{'dtype'=>$dtype}}});
  my $telra = $headers->{'TELRA'};
#
  
  $keyword = 'TELDEC';
  $headers = {};
      $headers = readFitsHeaderKeys({'fits_file'=>$file, 'hdu'=>$image_hdu, 
                                  'key_defs'=>{$keyword=>{'dtype'=>$dtype}}});
  my $teldec = $headers->{'TELDEC'};
#
  my $sign = 1;
  if ($teldec =~ /^\-/ )  { $sign = -1;}
  my @dectok = split ":", $teldec;
  
  $teldec = ((abs $dectok[0]) + $dectok[1]/60. + $dectok[2]/3600.);
  #
  # Would be better to use Math::Round
  #
  if (($teldec - int $teldec) >= 0.5) { 
  	$teldec = (int $teldec) + 1;
  } else { 
  	$teldec = int $teldec;
  }

  #
  my @ratok = split ":", $telra;
  $telra = ($ratok[0] + $ratok[1]/60. + $ratok[2]/3600.)*15.;
  if ($teldec == 0) { $sign = 1; }
  $key = $sign*((int($teldec))*1000 + $telra);
  $teldec *= $sign;

  @res = ($telra,$teldec, $key );
}
#
#  search for minimal distance
#

sub searchMin {
    my ($array, $word) = @_;
    my $low = 0;
    my $ind = 0;
    my @keyarr =  @$array;
    my $high = $#keyarr;
    print "Max index=",$high," search index=",$word,"\n";
    my $middle = $high/2;

    my $min = abs ($keyarr[0] - $word);
    foreach (1..$high){
    	my $diff= abs($keyarr[$_] - $word); 
    	if ($diff < $min) { $min = $diff; $ind = $_; }
    }

    return $ind;
}
#
# Processing input parameters
#
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev'); # set working flags for Getopt
$stat = Getopt::Long::GetOptions(
      'detector=s'  => \$detector,
      'input-base=s' => \$input_base,
      'output-base=s' => \$output_base,
      'coadd-project=s' => \$coadd_project,
      'output-path=s' => \$output_path,
      'bin-path=s' => \$bin_path,
      'input-path=s' => \$input_path,
      'tpointfile=s' => \$tpointfile,
      'debug|d' => \$debug,
      'help|h'  => \$help,
      'man' => \$man,
      'version|v' => \$version,
      'verbose|V=i' => \$verbose
);

# Check inputs
if (!defined($detector)) {
    print STDERR "Must be define detector (either DECam or Mosaic2)\n";
    exit 1;
}

if (($detector ne 'DECam') && ($detector ne 'Mosaic2')) {
    print STDERR "Invalid detector '$detector'\n";
    print STDERR "Must be either DECam or Mosaic2\n";
    exit 1;
}

if (!defined($coadd_project)) {
    print STDERR "Must specify coadd project\n";
    exit 1;
}

#
# Read tangent point list and create hash
#
#
  open FH, "<$tpointfile" or die "\nUnable to open $tpointfile\n";
   while (my $record = <FH>) {            #start block of list processing
   	if ($record =~ /^#/){ next };              # skip comments
   		chomp $record;
   		my @tokens = split ' ', $record; # split on tokens
   		my $name = $tokens[0];
   		my $tpra = $tokens[1];
   		my $tpdec = $tokens[2];
   		my $sign = 1;
   		if ($tpdec < 0) { $sign = -1; }   		
   		my $pkey = $sign*( (abs $tpdec)*1000.0 +$tpra);

   		$tangent_list{$pkey} = [$name, $tpra, $tpdec]; 

   }
   close FH;

# Read image file lists: The directory should contain a number of image lists
# prepared by orchestration
opendir DH, "$input_path";
my @imagelists = grep { /$input_base.\d+/ && -f "$input_path/$_" } readdir DH;
close DH;

print "\nFound ",scalar @imagelists, " image lists in the $input_path directory.\n";

# Parse each list of science files for info and place info
# (as a hashref) into the @ImageList array:
foreach my $listfile (@imagelists) {
  # Grab job number from filename:
  $listfile =~ /$input_base.(\d+)/;
  my $jobnum = $1;                # first match from he previous commad
  open FH, "<$input_path/$listfile" or die "\nUnable to open $input_path/$listfile\n";

  # Grab keys (from right up to project) from each full path archive 
  # file name entry (line) in the file:
  while (my $line = <FH>) {;            #start block of list processing
    my @toklist = split " ", $line;
    my $file = $toklist[0];
    my $telra = chomp $toklist[1];
    my $teldec = chomp $toklist[2];
    chomp $file;
    $file =~/\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)$/; # split on tokens
    my $coadd_project = $1;
    my $fileclass = $2;
    my $run =$3;
    my $filetype = $4;
    my $exposurename = $5;
    my $filename = $6;
    print $file,"\n";
    print "job_number=",$jobnum," project=",$coadd_project," class=",$fileclass," run=",$run," type=",$filetype;
    print " exposure=",$exposurename," filename=",$filename,"\n";

  my $sign = 1;
  if ($teldec < 0 )  { $sign = -1;}
  $teldec = abs $teldec;
  if (($teldec - int $teldec) >= 0.5) { 
  	$teldec = (int $teldec) + 1;
  } else { 
  	$teldec = int $teldec;
  }

  #

  if ($teldec == 0) { $sign = 1; }
  $imkey = $sign*(($teldec)*1000 + $telra);
	$teldec *=$sign;
#
    my @tangent_keys = keys %tangent_list;
   	my $index = &searchMin( \@tangent_keys, $imkey);
   	my $key_sel = $tangent_keys[$index];
#
#    

    
    # put the image info in the list
      push(@ImageList, {
      'jobnum' =>$jobnum,
      'filename' => $filename,
      'exposurename' => $exposurename,
      'run' => $run,
      'absfile' => $file,
      'tpra' => $tangent_list{$key_sel}[1],
      'tpdec' => $tangent_list{$key_sel}[2],
      'tilename' => $tangent_list{$key_sel}[0],
    }
    );
  }
  close FH; 
}  # end block of list processing 

  my %jobids;
  foreach my $Image (@ImageList) {
    $jobids{$Image->{'jobnum'}} = 1;
  }
foreach my $jobid (keys %jobids) {    # start loop on jobs
    my $outfile = join('_',$output_base, $jobid);
    $outfile = join('/',$output_path,"$outfile.list");
    print "OutputFile=",$outfile,"\n";
    open FH, ">$outfile" or die "Unable to open $outfile\n";

    foreach my $Image (@ImageList) {  # start loop on images
      next if ($Image->{'jobnum'} ne $jobid);
		my $inputfile = $Image->{'absfile'};
        my $TileName = $Image->{'tilename'};
        my $PixelSize = 0.27;
        my $ra = $Image->{'tpra'}; 
        my $dec = $Image->{'tpdec'}; 
        print FH "$inputfile $TileName $PixelSize $ra $dec\n"; 

  
    }   # end loop in images
    close FH;

  } # end loop on jobs




# Output a summary of image info:
if ($verbose >= 3) {
  foreach (@ImageList) {
    while((my $k, my $v) = each %$_) {
      if ($k eq 'tiles_info') {
        print "$k\n";
        foreach my $tile (@$v) {
          print "$tile ";
          while((my $k2, my $v2) = each %$tile) {
            print "$k2 = $v2 ";
          }
          print "\n";
        }
      }
      #if ($k eq 'ccd') {
      #  print "Offset info:\n";
      #  while((my $k2, my $v2) = each %{$Offsets->{$v}}) {
      #      print "$k2 = $v2 ";
      #    }
      #  print "End offset info\n";
      #}
      #else {
        print "$k = $v\n";
      #}
    }
    print "\n";
  }
}




exit 0;


=head1 