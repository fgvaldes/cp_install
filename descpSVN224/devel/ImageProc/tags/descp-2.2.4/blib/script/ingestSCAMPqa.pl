#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

use strict;
use warnings;

use Getopt::Long;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;
use FindBin qw($Bin);

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use Astro::VO::VOTable::Document;
use DB::DESUtil;
use DB::EventUtils;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($project, $run, $nite, $file, $list, $noingest, $debug) = undef;
my $verbose = 0;
Getopt::Long::GetOptions(
    "project=s" => \$project,
    "run=s" => \$run,
    "nite=s" => \$nite,
    "list=s" => \$list,
    "debug" => \$debug,
    "noingest" => \$noingest,
    "verbose=i" => \$verbose
) or usage("Invalid command line options\n");


### input checks
if (!defined($project)) {
    reportEvent( $verbose, 'STATUS', 5, "Missing project on command line");
    exit 1;
}

if (!defined($run)) {
    reportEvent( $verbose, 'STATUS', 5, "Missing run on command line");
    exit 1;
}

if (!defined($nite)) {
    reportEvent( $verbose, 'STATUS', 5, "Missing run on command line");
    exit 1;
}

if (!defined($list) && scalar(@ARGV) == 0) {
    reportEvent( $verbose, 'STATUS', 5, "Missing list or file on command line");
    exit 1;
}

if (defined($list) && scalar(@ARGV) > 0) {
    reportEvent( $verbose, 'STATUS', 5, "Cannot have list and file on command line");
    exit 1;
}

if (defined($list) && !-r $list) {
    reportEvent( $verbose, 'STATUS', 5, "Cannot open list $list");
    exit 1;
}

if (!defined($list) && !-r $ARGV[0]) {
    reportEvent( $verbose, 'STATUS', 5, "Cannot open file $ARGV[0]");
    exit 1;
}

# get exposure ids from DB for this nite
my $dbh = new DB::DESUtil(verbose=>$verbose,debug=>$debug);

# get columns for table in which we will be inserting
my $colnames;
eval {
   $colnames = $dbh->getColumnNames('exposure_qa');

   # the following aren't stored in file as named
   delete $colnames->{'id'} if defined($colnames->{'id'});
   delete $colnames->{'project'} if defined($colnames->{'project'});
   delete $colnames->{'run'} if defined($colnames->{'run'});
   delete $colnames->{'nite'} if defined($colnames->{'nite'});
};
if ($@) {
    print "Error getting column names\n";
    print "$@\n";
    exit 1;
}

# Remove numbers from column names to match names in xml file
my %nameshash;
foreach my $c (keys %$colnames) {
    $c =~ s/_\d$//;
    $nameshash{$c} = 1;
}
my @varnames = sort keys %nameshash;

# grab exposure id
my $dbq = "select id,exposurename from exposure where project='$project' and nite='$nite'";
print "dbq = $dbq\n" if $debug;

my $exposures = $dbh->selectall_hashref($dbq, 'exposurename');
if ($debug) {
    print "Exposures from DB\n";
    print Dumper($exposures),"\n\n";
}

# grab existing entries for this project+run
$dbq = "select id,exposureid from exposure_qa where project='$project' and run='$run'";
print "dbq = $dbq\n" if $debug;

my $existingQA = $dbh->selectall_hashref($dbq, 'exposureid');
if ($debug) {
    print "Exposure QA from DB\n";
    print Dumper($existingQA),"\n\n";
}
if (scalar keys %$existingQA > 0) {
    reportEvent( $verbose, 'STATUS', 5, "Information exists in DB for $project $run");
    print "If rerunning, delete information by hand first\n";
    print Dumper($existingQA), "\n";
    exit 1;
}

# done with database until mass ingest
$dbh->disconnect();

my %alldbinfo = ();
if (defined($file)) {
    parseFile($file, \%alldbinfo, $exposures, \@varnames);
}
elsif (defined($list)) {
    open LISTFH, "< $list";
    while (my $line = <LISTFH>) {
        $line =~ s/\s+//;
        parseFile($line, \%alldbinfo, $exposures, \@varnames);
    }
    close LISTFH;
}

if (!$noingest) {
    print Dumper(\%alldbinfo), "\n" if ($debug);
    ingest($project, $run, $nite, \%alldbinfo, sort keys %$colnames);
}
else {
    print Dumper(\%alldbinfo), "\n";
}


######################################################
sub ingest {
    my ($project, $run, $nite, $alldbinfo, @cols) = @_;
    my $dbh = new DB::DESUtil(verbose=>$verbose,debug=>$debug);

    my $dbp = "insert into exposure_qa (project, run, nite, id";
    for (my $i = 0; $i < scalar(@cols); $i++) {
        $dbp .= ", ".$cols[$i];
    }
    $dbp .= ") values ('$project','$run','$nite',?";
    for (my $i = 0; $i < scalar(@cols); $i++) {
        $dbp .= ",?";
    }
    $dbp .= ")";
    print "dbp: $dbp\n" if $debug;

    my $sth = $dbh->prepare($dbp);

    for my $href (values %$alldbinfo) {
        my $id = $dbh->getNextSequenceID('exposure_qa_seq');
        my @valarray;
        $valarray[0] = $id;
        for (my $i = 0; $i < scalar(@cols); $i++) {
            if (defined($href->{$cols[$i]})) {
                $valarray[$i+1] = $href->{$cols[$i]};
            }
            else {
                $valarray[$i+1] = undef;
            }
        }

        if ($debug) {
            print "dbp: $dbp\n";
            print Dumper(\@valarray), "\n";
        }

        $sth->execute(@valarray);
    }
    $dbh->commit();
    $dbh->disconnect();
}


sub parseFile {
    my ($file, $alldbinfo, $exposures, $varnames ) = @_;

    my $dbinfo = {};
    my $exposureid;

    # read information from SCAMP XML file
    my @info=getScampInfo($file,0);
    my @stats=getScampInfo($file,1);

    # only need first
    my $firstinfo = $info[0];
    my $firststats = $stats[0];

    if ($debug) {
        dumpScampInfo($firstinfo, $firststats, $varnames);
    }

    ## get exposure id 
    print "catalog_name = '", $firstinfo->{'catalog_name'}, "'\n";
    my $exposurename;
    if (!defined($firstinfo->{'catalog_name'})) {
        reportEvent( $verbose, 'STATUS', 5, "Could not find catalog_name");
        print Dumper($firstinfo), "\n";
        exit 1;
    }
    if ($firstinfo->{'catalog_name'} !~ /\w/) {
        reportEvent( $verbose, 'STATUS', 5, "Empty catalog_name");
        print Dumper($firstinfo), "\n";
        exit 1;
    }
    if ($firstinfo->{'catalog_name'} =~ m/^(\S+)_scamp.fits/) { 
        my ($exposurename) = $firstinfo->{'catalog_name'} =~ m/^(\S+)_scamp.fits/;
        print "exposurename = $exposurename\n";

        # exposurename 
        $exposurename .= '.fits';
        if (defined($exposures->{$exposurename})) {
            $exposureid = $exposures->{$exposurename}->{'id'};
        print "found exposureid = $exposureid\n";
            $firststats->{'exposureid'} = $exposureid;
        }
        else {
            reportEvent( $verbose, 'STATUS', 5, "Could not find exposure id for exposurename '$exposurename'");
            exit 1;
        }

        foreach my $key (@$varnames) {
            my $val = "";
            if (defined($firststats->{$key})) {
                $val=$firststats->{$key};
            }
            else {
                reportEvent( $verbose, 'STATUS', 5, "Missing value for key $key");
                exit 1;
            }

            # remove leading and trailing spaces
            $val =~ s/^\s+//;
            $val =~ s/\s+$//;

            # check for multiple values
            my @vals=split /\s+/, $val;
            if (scalar(@vals) == 1) {
               $alldbinfo->{$exposureid}->{$key} = $vals[0];
            }
            else {
                for (my $i = 1; $i <= scalar(@vals); $i++) {
                    $alldbinfo->{$exposureid}->{${key}."_$i"} = $vals[$i-1];
                }
            }
        }
    }
    else {
        reportEvent( $verbose, 'STATUS', 5, "catalog_name ".$firstinfo->{'catalog_name'}." doesn't match pattern");
        print Dumper($firstinfo), "\n";
        exit 1;
    }
}

############################################################
sub getScampInfo {
    my $file=shift; 
    my $which_table=shift; 

    # Read the VOTABLE document.
    my $doc = Astro::VO::VOTable::Document->new_from_file($file) or die;
    
    # Get the (only) VOTABLE element.
    my $votable = $doc->get_VOTABLE or die;

    my $res=$votable->get_RESOURCE(0);

    my @info;
    foreach my $res2 ($res->get_RESOURCE) {
        
        # Table 0 contains the image info
        my @tables;
        my $table=$res2->get_TABLE($which_table);
        
        # Get the FIELD elements.
        my(@field_names) = ();
        my($field);
        foreach $field ($table->get_FIELD) {
            push(@field_names, $field->get_name);
        }
        
        
        my $data = $table->get_DATA or die;
        my $tabledata = $data->get_TABLEDATA or die;
        
        # Get each TR element, then each TD element, and print its contents.
        my($tr, $td);
        my($i, $j);
        $i = 0;
        foreach $tr ($tabledata->get_TR) {
            #print "Data for target $i:\n";
            $j = 0;
            foreach $td ($tr->get_TD) {
                #print "  $field_names[$j] = ", $td->get, "\n";
                my $var=$field_names[$j];
                $info[$i]{lc($var)}=$td->get;
                $j++;
            }
            $i++;
        }
    }
    
    return @info;
}

##################################################
sub dumpScampInfo {
    my ($firstinfo, $firststats, $varnames) = @_;

    print "========== dumpScampInfo ==========\n";
    printf "%30s [%s] [%s]\n", "KEY", "INFO", "STAT";
    foreach my $key (@$varnames) {
        my $fstat="undef";
        my $finfo="undef";

        if (defined($firstinfo->{$key})) {
            $finfo=$firstinfo->{$key};
        }
        if (defined($firststats->{$key})) {
            $fstat=$firststats->{$key};
        }
        printf "%30s [%s] [%s]\n", $key, $finfo, $fstat;
    }
}
