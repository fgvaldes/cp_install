#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# This script will perform all tasks necessary to do a 
# psf homogenization run on a tile.
#
# 1.  Gets all imagenames associated with a given tile, date and run
# 2.  Gets all FWHM's for each image from the database and calculates a median.
# 3.  Runs sextractor on each image to create a catalog
# 4.  Runs psfex on that catalog to create a psf kernel using median fwhm.
# 5.  Runs psfnormalize to convolve image with kernel.
# 6.  Run coadd_fluxscale on all psf normalized images to build the coadd
#
# AUTHOR:  Tony Darnell (tdarnell@uiuc.edu)
#
# SVN Info:
#
# $Rev: 8754 $: Rev of last commit
# $LastChangedBy: donaldp $: Author of last commit
# $Date: 2012-09-26 19:02:29 -0700 (Wed, 26 Sep 2012) $: Date of last commit
#

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use File::Basename;
use File::Path;
use FindBin;
use Getopt::Long;
use Cwd;
use POSIX qw(floor);

use lib ("$FindBin::Bin/../../../Database/trunk/lib");
#use DB::desDBI;
#use DB::IngestUtils;

my $VERSION = '$Rev: 8754 $';

#
# Open database connection
#
#my $dbh = new DB::desDBI
#  (
#            {verbose   => 1,
#             DBIattr => {
#              AutoCommit => 0,
#              RaiseError => 1,
#              PrintError => 0,
#             }
#            }
#  );

my $desHome = $ENV{'DES_HOME'};
#my $binPath = ($desHome) ? "$desHome/bin" : 
#              "/home/bcs/desdm/devel/ImageProc/trunk/bin_WS";
my $desBinPath = "/home/tdarnell/desdm/devel/ImageProc/trunk/bin";
my $tpBinPath = "/home/bcs/des_prereq/bin";
my $etcPath = "/home/tdarnell/desdm/devel/ImageProc/trunk/etc";

my $archiveNode;
my $tileName;
my $band;
my $runFilter = undef;
my $niteFilter = undef;
my $psfOff = 0;
my $psfSize = 26;
my $fwhmFrac = undef;
my $detectThresh = undef;
my $tileList = q{};

Getopt::Long::GetOptions(
   "tilename=s" => \$tileName,
   "band=s" => \$band,
   "archivenode=s" => \$archiveNode,
   "run:s" => \$runFilter,
   "nite:s" => \$niteFilter,
   "psfoff" => \$psfOff,
   "psfsize:i" => \$psfSize,
   "fwhmFrac:f" => \$fwhmFrac,
   "detect_thresh:f" => \$detectThresh,
   "tilelist:s" => \$tileList,
) or usage("Invalid command line options\n");

#usage("\n\nYou must supply an archive node") unless defined $archiveNode;
usage("\n\nYou must supply a tilename") unless defined $tileName;
usage("\n\nYou must supply a band") unless defined $band;
usage("\n\nYou must supply a detect_thresh") unless defined $detectThresh;

#
# Get info about this node
#
my $archiveRoot = "/data2/Archive";

#
# Allow the $niteFilter and $runFilter command line options to be
# a comma separated list.
#
my @nites = split(/,/,$niteFilter);
my @runs = split(/,/,$runFilter);

my $pixelScale = 0.27; #arcsec/pixel

my $dataDir = qq{$archiveRoot/coadd/$tileName};
#mkpath ("$dataDir/$band");
chdir("$dataDir/$band");

#
# Get the location of the tilefiles and build directory and filenames
#
my @tileFiles=();

open (my $FILES, "< $tileList");
@tileFiles = <$FILES>;
close ($FILES);

open (my $LOGFILE, "> psfnormalize.log");
my $numFiles = scalar (@tileFiles);
print ($LOGFILE "\n\nThere are $numFiles files to do...\n");

#
# Get FWHM of images 
#
my $medianFWHM = 3.0;

$medianFWHM = sprintf "%2.2f",$medianFWHM;

#
# Go through each image, apply sextractor
# using FWHM
# Make kernel from catalog
# convolve each image with its kernel
#

chdir ("$dataDir/$band");

my $retVal;
foreach my $file (@tileFiles){

  chomp($file);
  print "$file\n";
  my $tilePath = dirname($file);
  my $baseName = basename($file);

  my $homoDir = "/data2/Archive/coadd/DES2237-4522/g/homoCR";
  #my $homoDir = ($psfOff) ? "$tilePath" : "$tilePath/homoCR";
  mkpath ("$homoDir") if (!-d $homoDir);

  if (!-e $file){
    print ($LOGFILE "$baseName not in $tilePath\n");
    next;
  }

  $file =~ s/\.fits//;
  $baseName =~ s/\.fits//;
  print "$baseName\n";

#
# Comment this out for now.  Want to manually use list that exists
#
  #open (my $HOMOLIST, ">>$dataDir/$band/homoList");
  #print $HOMOLIST "$homoDir/$baseName.fits\n";
  #close ($HOMOLIST);

  next if ($psfOff);

  print ($LOGFILE "=============== $baseName ===============\n\n");

#
# Detect cosmic rays
#
  my $sex = qq{
    $tpBinPath/sex -FILTER_NAME $etcPath/descosmicray.ret ../$baseName.fits[0] -CHECKIMAGE_TYPE OBJECTS -CHECKIMAGE_NAME CRMask.fits -STARNNW_NAME $etcPath/sex.nnw -PARAMETERS_NAME $etcPath/sex.param_nopsfex -c $etcPath/sexdescosmic.config
  };

  print "$sex\n";
  print ($LOGFILE "\n\n****** RUNNING sex for CR detection ******\n\n");
  print ($LOGFILE "$sex\n");
  #$retVal = system($sex);

#
# Apply cosmic ray mask to image
#
  my $mkMask = qq{
    $desBinPath/mkmask $dataDir/$baseName.fits -crays CRMask.fits -output $homoDir/$baseName\_nocrs.fits 
  };
  #print "$mkMask\n";
  #$retVal = system($mkMask);

#
# Run sextractor
#
  my $sextractor = qq{
    $tpBinPath/sex "homoCR/$baseName\_nocrs.fits[0]"  -c $etcPath/default.sex -DETECT_THRESH $detectThresh -CATALOG_NAME homoCR/$baseName\_vig.fits -CATALOG_TYPE FITS_LDAC -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE "homoCR/$baseName\_nocrs.fits[2]" -FLAG_IMAGE "homoCR/$baseName\_nocrs.fits[1]" -PARAMETERS_NAME $etcPath/sex.param_psfex -FILTER_NAME $etcPath/sex.conv -STARNNW_NAME $etcPath/sex.nnw -DETECT_MINAREA 3 -PHOT_APERTURES 15.0
  };

  print ($LOGFILE "\n\n****** RUNNING sextractor ******\n\n");
  print ($LOGFILE "$sextractor\n");
  #$retVal = system($sextractor);

#
# Run PSFex to make kernel
#
  my $psfex = qq{
    $tpBinPath/psfex homoCR/$baseName\_vig.fits -c $etcPath/default.psfex -HOMOBASIS_TYPE GAUSS-LAGUERRE -HOMOPSF_PARAMS $medianFWHM,3.0 -PSF_SIZE $psfSize -HOMOKERNEL_NAME homoCR/$baseName.psf.fits 2>&1
  };
  print ($LOGFILE "\n\n****** RUNNING psfex ******\n\n");
  print ($LOGFILE "$psfex\n");
  #$retVal = `$psfex`;
  print "$psfex\n";

  next if ( $retVal =~ m/not enough samples/gis);

#
# Run psfnormalize
#
  my $psfnormalize = qq{
    $desBinPath/psfnormalize homoCR/$baseName\_nocrs.fits -v -kernel homoCR/$baseName.psf.fits -output homoCR/$baseName\_psfnorm.fits
  };
  print ($LOGFILE "\n\n****** RUNNING psfnormalize ******\n\n");
  print ($LOGFILE "$psfnormalize\n");
  #$retVal = system($psfnormalize);
  print "$psfnormalize\n";

#
# Run sextractor on psfnorm files with FWHM_WORLD set.
#
  
  $sextractor = qq{
    $tpBinPath/sex "homoCR/$baseName\_psfnorm.fits[0]"  -c $etcPath/default.sex -DETECT_THRESH $detectThresh -CATALOG_NAME homoCR/$baseName\_psfnorm_cat.fits -CATALOG_TYPE FITS_LDAC -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE "homoCR/$baseName\_psfnorm.fits[2]" -FLAG_IMAGE "homoCR/$baseName\_psfnorm.fits[1]" -PARAMETERS_NAME $etcPath/sex.param_psfex -FILTER_NAME $etcPath/sex.conv -STARNNW_NAME $etcPath/sex.nnw -DETECT_MINAREA 3 -PHOT_APERTURES 15.0 
  };
  print "$sextractor\n";
  #$retVal = system($sextractor);

#
# Insert the new FWHM's in the header
#
  my $fwhm = qq{$desBinPath/fwhm homoCR/$baseName\_psfnorm_cat.fits homoCR/$baseName\_nocrs.fits};
  print "$fwhm\n";

  #$retVal = system($fwhm);
}

#
# Run coadd_fluxscale
#

my $coaddFluxScaleOpts = q{};
$coaddFluxScaleOpts .= qq{ -nite $niteFilter } if defined $niteFilter;
$coaddFluxScaleOpts .= qq{ -runid $runFilter } if defined $runFilter;

my $coaddFluxScale = qq{
$desBinPath/coadd_fluxscale -project DES -tilename $tileName -band $band -detector DECam -binpath $desBinPath -etcpath $etcPath $coaddFluxScaleOpts -output $dataDir/$tileName\_$band.fits -class_star 0.75 1.00 -magtype 5 -sigmaclip 2.5 -weightmean -list $dataDir/$band/homoList
};
  

print ($LOGFILE "\n\n****** RUNNING coadd_fluxscale ******\n\n");
print ($LOGFILE "$coaddFluxScale\n");
#$retVal = system($coaddFluxScale);

close ($LOGFILE);

#$dbh->disconnect();

exit(0);

#
# Subroutines
#

sub median {

      my $rpole = shift;
      my @pole = @$rpole;

      my $nelem = scalar(@pole);

      my $median;

      @pole = sort(@pole);

      if( $nelem % 2 ) {
        # nelem is odd
        $median = $pole[(($nelem+1) / 2)-1];
      } else {
        # nelem is even
        $median = ($pole[($nelem / 2)-1] + $pole[$nelem / 2]) / 2;
      }

      return $median;
}

sub getFWHM {

      my ($frac, $rpole) = @_;
      my @pole = sort(@$rpole);

      my $nelem = scalar(@pole);

      my $fwhm;

      my $index=floor((($nelem+1) * $frac))-1;
      $index = ($index < 0) ? 0 : $index;
      $index = ($index >= $nelem) ? $nelem-1 : $index;
      $fwhm = $pole[$index];

      return $fwhm;
}

sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -archiveroot archivedir -tilename tilename -band g|r|i|z [-run runId]".
      " [-psfoff] [-psfsize psfsize] [-fwhmFrac (0.0 - 1.0)]\n" .
      "       tilename is the name of the tile you want to use\n" .
      "       band is the color of the filter\n" .
      "       run is optional\n" .
      "       psfoff turns off the psf normalization step\n" .
      "       psfsize is the desired size of psf kernel in pixels\n" .
      "       fwhmFrac is the point in the sorted distribution of FHWM's \n" .
      "       you want to use, where the median is 0.5, 0.0 is the lowest,\n" .
      "       1.0 is the highest.\n"
   );

   die("\n")

}
