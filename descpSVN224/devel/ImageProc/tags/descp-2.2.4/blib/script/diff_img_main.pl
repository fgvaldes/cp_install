#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#
#  $Id: runWL.pl 2817 2009-01-05 18:59:24Z dadams $
#
#  $Rev:: 2817                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-01-05 12:59:24 #$:  # Date of last commit.
#
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use warnings;

use Data::Dumper;
use File::Path;
use File::Copy;
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

use DB::FileUtils;
use DB::EventUtils;
use ImageProc::Util qw(:all);

$|=1;

my ($binpath, $etcpath, $terapixpath, $outpath, $auxpath) = undef;
my ($list, $difftype, $filter, $threshold, $conpix, $fcarith, $nthread) = undef;
my ($verbose, $nocleanup) = undef;
my ($stat, $file, $keyword, $dtype, $image_hdu, $headers);

my @INORDER = ('number', 'flux_iso', 'fluxerr_iso', 'x_image', 'y_image', 'alpha_sky', 'delta_sky', 'theta_image', 'ellipticity', 'flags');

my @OUTORDER = ('number', 'flux_iso', 'fluxerr_iso', 'x_image', 'y_image', 'alpha_sky', 'delta_sky', 'theta_image',
'ellipticity', 'flags', 'pos_neg', 'dipole', 'cluster', 'mag', 'mag_err');

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'list=s' => \$list,
      'binpath=s' => \$binpath,
      'etcpath=s' => \$etcpath,
      'terapixpath=s' => \$terapixpath,
      'outpath=s' => \$outpath,
      'auxpath=s' => \$auxpath,
      'difftype=s' => \$difftype,
      'filter=s' => \$filter,
      'threshold=s' => \$threshold,
      'conpix' => \$conpix,
      'fcarith' => \$fcarith,
      'nthread' => \$nthread,
      'nocleanup' => \$nocleanup,
      'verbose|V=i' => \$verbose,
);

print "INPUT VALUES\n";
print "binpath = $binpath\n" if defined($binpath);
print "terapixpath = $terapixpath\n" if defined($terapixpath);
print "etcpath = $etcpath\n" if defined($etcpath);
print "outpath = $outpath\n" if defined($outpath);
print "auxpath = $auxpath\n" if defined($auxpath);
print "difftype = $difftype\n" if defined($difftype);
print "list = $list\n" if defined($list);
print "\n\n";

# Check inputs
if (!defined($binpath) || !defined($terapixpath) || !defined($etcpath) || 
    !defined($outpath) || !defined($auxpath) || 
    !defined($list) || !defined($difftype)) {
    reportEvent($verbose,"STATUS",5,"Error: Missing required arguments");
    Usage();
    exit 1;
}
if (!defined($nocleanup)) {
    $nocleanup = 0;
}
else {
    $nocleanup = 1;
}

$difftype = lc($difftype);
if (($difftype ne 'red') && ($difftype ne 'nitecmb')) {
    reportEvent($verbose,'STATUS',5,"Error: Invalid difftype value.  Must be either 'red' or 'nitecmb'");
    exit 1;
}

my $extra = '';
my $extraUnd = '';
if ($difftype eq 'nitecmb') {
    $extra = 'nc';
    $extraUnd = '_nc';
}


# set defaults
if (!defined($verbose)) {
    $verbose = 1;
}
if (!defined($filter)) {
    $filter = "$etcpath/gauss_3.0_5x5.conv";
}
if (!defined($threshold)) {
    $threshold = 3.5;
}
if (!defined($conpix) && !defined($fcarith)) {
    $conpix = 1;
}

if (! -d $outpath) {
    eval { mkpath($outpath) };
    if ($@) {
        reportEvent($verbose, "STATUS", 5, "Error\nCouldn’t create $outpath: $@");
        exit 1;
    }
}
if (! -d $auxpath) {
    eval { mkpath($auxpath) };
    if ($@) {
        reportEvent($verbose, "STATUS", 5, "Error\nCouldn’t create $auxpath: $@");
        exit 1;
    }
}


my $opensuccess = open FH, "< $list";
if (!$opensuccess) {
    print STDERR "Could not open file '$list'\n";
    exit 1;
}
foreach my $line (<FH>) {
    my ($image, $exposurename, $ccd) = undef;
    ($image, $exposurename, $ccd) = split /\s+/, $line;
    if (!defined($image)) {
        reportEvent($verbose, "STATUS", 5, "ERR undefined image name from file"); 
        exit 1;
    }
    if (!defined($exposurename)) {
        reportEvent($verbose, "STATUS", 5, "$image: ERR undefined exposure name from file"); 
        exit 1;
    }
    if (!defined($ccd)) {
        reportEvent($verbose, "STATUS", 5, "$image: ERR undefined ccd from file"); 
        exit 1;
    }

    # zero-pad ccd
    $ccd = sprintf("%02d", $ccd);

    print "--------------------------------------------------------------------------------\n";
    reportEvent($verbose, "STATUS", 1, "Difference imaging $image");
    print "--------------------------------------------------------------------------------\n\n";


    if (! -d "$outpath/$exposurename") {
        eval { mkpath("$outpath/$exposurename") };
        if ($@) {
            reportEvent($verbose, "STATUS", 5, "$image: Error\nCouldn’t create $outpath/$exposurename: $@");
            exit 1;
        }
    }

    my $coaddlist = "${auxpath}/diffcoadds_${exposurename}_${ccd}.list";
    my $distort_template = "${outpath}/${exposurename}/${exposurename}_${ccd}_${extra}distmp.fits";

    # sanity check
    if (-s $distort_template) {
        reportEvent($verbose, "STATUS", 4, "$image: distmp already exists: $distort_template");
        next;
    }

    reportEvent($verbose,"STATUS",1,"$image: Creating template image (makeTemp) BEG") if $verbose > 3;
    my $negative_image = "${exposurename}_${ccd}${extraUnd}e.fits";
    my $makeTcmd = "$binpath/makeTemp $image -coaddlist $coaddlist -terapixpath $terapixpath -outauxpath $auxpath -binpath $binpath -etcpath $etcpath -verbose $verbose -outpath $outpath/$exposurename";
    if (defined($nthread)) {
        $makeTcmd .= " -nthread $nthread";
    }
    $stat = runCmd($makeTcmd, 'makeTemp', $verbose);
    if (($stat>>8) == 1) {
        reportEvent($verbose, "STATUS", 4, "$image: Creating template image (makeTemp) exit code = 1, skipping to next image");
        next;
    }
    elsif ($stat != 0) {
        reportEvent($verbose, "STATUS", 5, "$image: Creating template image (makeTemp) ERR non-zero exit code: $stat");
        exit $stat;
    }
    reportEvent($verbose,"STATUS",1,"$image: Creating template image (makeTemp) END") if $verbose > 3;


    if (! -s $distort_template) {
        reportEvent($verbose, "STATUS", 4, "$image: Didn't create template image (makeTemp: $stat, $distort_template), skipping to next image");
        next;
    }

    $dtype = 16;   # TSTRING
    $image_hdu = 1;

    $file = $distort_template;
    $headers = readFitsHeaderKeys({'fits_file'=>$file, 'hdu'=>$image_hdu, 
                                   'key_defs'=>{'SEXMGZPT'=>{'dtype'=>$dtype},
                                                'SATURATE'=>{'dtype'=>$dtype}}});
    if (!defined($headers->{'SEXMGZPT'})) {
        reportEvent($verbose,"STATUS",5,"$file: Could not find 'SEXMGZPT' keyword in template");
        next; 
    }
    my $sexmgzpt = $headers->{'SEXMGZPT'};

    if (!defined($headers->{'SATURATE'})) {
        reportEvent($verbose,"STATUS",5,"$file: Could not find 'SATURATE' keyword in template");
        next; 
    }
    my $saturate_template = $headers->{'SATURATE'};


    $file = $image;
    $headers = {};
    $headers = readFitsHeaderKeys({'fits_file'=>$file, 'hdu'=>$image_hdu, 
                                   'key_defs'=>{'SATURATE'=>{'dtype'=>$dtype}}});
    if (!defined($headers->{'SATURATE'})) {
        reportEvent($verbose,"STATUS",5,"$file: Could not find 'SATURATE' keyword in search image");
        next; 
    }
    my $saturate_image = $headers->{'SATURATE'};
    print "\n\n\n";

    print "\nsexmgzpt = $sexmgzpt, iupthresh = $saturate_image, tupthresh = $saturate_template\n\n";

    reportEvent($verbose,"STATUS",1,"$image: Creating differenced image (hotpants) BEG") if $verbose > 3;

    my $differenced_image = "${outpath}/${exposurename}/${exposurename}_${ccd}";
    if ($difftype eq 'red') {
        $differenced_image .= ".fits";
    }
    else {
        $differenced_image .= "${extraUnd}.fits";
    }
    $stat = runCmd("$terapixpath/hotpants -inim $image -tmplim $distort_template -outim $differenced_image -il 0 -tl -100 -iu $saturate_image -tu $saturate_template 2>&1", 'hotpants', $verbose);
    if ($stat != 0) {
        reportEvent($verbose, "STATUS", 5, "$image: Creating differenced image (hotpants) ERR non-zero exit code: $stat");
        exit $stat;
    }
    reportEvent($verbose,"STATUS",1,"$image: Creating differenced image (hotpants) END") if $verbose > 3;


    $file = $differenced_image;
    $keyword = 'KSUM00';
    $dtype = 16;   # TSTRING
    $image_hdu = 1;
    $headers = readFitsHeaderKeys({'fits_file'=>$file, 'hdu'=>$image_hdu, 
                                   'key_defs'=>{$keyword=>{'dtype'=>$dtype}}});
    if (!defined($headers->{$keyword})) {
        reportEvent($verbose,"STATUS",5,"$file: Could not find $keyword keyword");
        next;
    }
    my $ksum00 = $headers->{$keyword};

    print "\nksum00=$ksum00\n\n";

    if ($ksum00 <= 0) {
        reportEvent($verbose, "STATUS", 4, "$file: Error KSUM00 <= 0  ($ksum00)");
        next;
    }

    


    print "\n\n\n";
    reportEvent($verbose,"STATUS",1,"$image: Creating pos cat (sex) BEG") if $verbose > 3;
    my $ascii_cat = "${exposurename}_${ccd}_pos.cat";
    $stat = runCmd("$terapixpath/sex $differenced_image -c $etcpath/default.sex -PARAMETERS_NAME $etcpath/difference.param -FILTER_NAME $filter -DETECT_THRESH $threshold -DETECT_MINAREA 5 -ANALYSIS_THRESH 4.5 -CATALOG_NAME $ascii_cat -CATALOG_TYPE ASCII", 'possex', $verbose);
    if ($stat != 0) {
        reportEvent($verbose, "STATUS", 5, "$image: Creating pos cat (sex) ERR non-zero exit code: $stat");
        exit $stat;
    }
    reportEvent($verbose,"STATUS",1,"$image: Creating pos cat (sex) END") if $verbose > 3;



    print "\n\n\n";
    reportEvent($verbose,"STATUS",1,"$image: Creating negative differenced image (conpix/fcarith) BEG") if $verbose > 3;
    my ($multcmd, $multname);
    if (defined($conpix)) {
        $multcmd = "$terapixpath/conpix -n -m -1 $differenced_image";
        $multname = 'conpix';
    }
    else {
        $multcmd =  "$terapixpath/fcarith $differenced_image -1 $negative_image MUL";
        $multname = 'fcarith';
    }
    $stat = runCmd($multcmd, $multname, $verbose);
    if ($stat != 0) {
        reportEvent($verbose,"STATUS",5,"$image: Creating negative differenced image (conpix/fcarith) ERR non-zero exit code: $stat");
        exit $stat;
    }
    reportEvent($verbose,"STATUS",1,"$image: Creating negative differenced image (conpix/fcarith) END") if $verbose > 3;



    print "\n\n\n";
    reportEvent($verbose,"STATUS",1,"$image: Creating neg cat (sex) BEG") if $verbose > 3;
    my $ascii_neg_cat = "${exposurename}_${ccd}_neg.cat";
    $stat = runCmd("$terapixpath/sex $negative_image -c $etcpath/default.sex -PARAMETERS_NAME $etcpath/difference.param -FILTER_NAME $filter -DETECT_THRESH $threshold -DETECT_MINAREA 5 -ANALYSIS_THRESH 4.5 -CATALOG_NAME $ascii_neg_cat -CATALOG_TYPE ASCII", 'negsex', $verbose);
    if ($stat != 0) {
        reportEvent($verbose, "STATUS", 5, "$image: Creating neg cat (sex) ERR non-zero exit code: $stat");
        exit $stat;
    }
    reportEvent($verbose,"STATUS",1,"$image: Creating neg cat (sex) END") if $verbose > 3;
    


    print "\n\n\n";
    reportEvent($verbose, "STATUS", 1, "Start application setFlags") if $verbose > 3;
    setFlagsObjDet($ascii_cat, $ascii_neg_cat, $outpath, $auxpath, $exposurename, $ccd, $sexmgzpt, $ksum00);
    reportEvent($verbose, "STATUS", 1, "End application setFlags") if $verbose > 3;

    my $compressed = sniffForCompressedFile($image);
    if (($compressed =~ /\S/) && (-r $compressed) && (-r $image)) {
        unlink $image;
    }

    if (!$nocleanup) {
        unlink <*$exposurename*>;
    }
}
close FH;


#####################################################################


sub readCat {
    my ($file, $cat, $posneg, $zpt, $ksum) = @_;

    if ($ksum <= 0) {
        reportEvent($verbose,"STATUS",4,"Error: ksum <= 0 ($ksum)");
        return;
    }
    my $opensuccess = open FH, "< $file";
    if (!$opensuccess) {
        reportEvent($verbose,"STATUS",5,"Error: Could not open cat file '$file'");
        exit 1;
    }
    foreach my $line (<FH>) {
        # remove space at beginning and end of line
        $line =~ s/^\s+//;
        $line =~ s/\s+$//;

        my %obj = ();
        
        my @info = split /\s+/, $line;
        if (scalar(@info) != scalar(@INORDER)) {
            reportEvent($verbose,"STATUS",5,"Error: line missing information in file '$file'\nline: $line");
            exit 1;
        }
        for (my $i = 0; $i < scalar(@INORDER); $i++) {
            $obj{$INORDER[$i]} = $info[$i];  
        }
        $obj{'pos_neg'} = $posneg;
        $obj{'dipole'} = 0;
        $obj{'cluster'} = 0;
        if ($obj{'flux_iso'} <= 0) {
            reportEvent($verbose,"STATUS",4,"Error: flux_iso <= 0 in file '$file'\nline: $line");
            next;
        }
        my $zp = -2.5*log($ksum)+$zpt;
        $obj{'mag'} = -2.5*log($obj{'flux_iso'}) + $zp; 
        $obj{'mag_err'} = -2.5/log(10)* $obj{'fluxerr_iso'}/$obj{'flux_iso'};
        push (@$cat, \%obj);
    }
    close FH;
}

sub setFlagsObjDet {
    my ($posfile, $negfile, $outpath, $auxpath, $exposure, $ccd, $zpt, $ksum) = @_;
    
    my @cat = (); 
    readCat($posfile, \@cat, 1, $zpt, $ksum);
    readCat($negfile, \@cat, 2, $zpt, $ksum);

    my $num = scalar(@cat);
    for (my $i=0; $i<$num; $i++) {
        my $objI = $cat[$i];
        for (my $j=$i+1; $j<$num;$j++) {
            my $objJ = $cat[$j];

            # if a "positive" is close to "negative" by 17 pixel
            if ((($objI->{'x_image'}-$objJ->{'x_image'})**2+($objI->{'y_image'}-$objJ->{'y_image'})**2)<=289 && 
                ($objI->{'pos_neg'} != $objJ->{'pos_neg'}) ) {
                $objI->{'dipole'} = 1; 
                $objJ->{'dipole'} = 1; 
            }

            # if an object is close to another object by 30 pixel
            if ((($objI->{'x_image'}-$objJ->{'x_image'})**2+($objI->{'y_image'}-$objJ->{'y_image'})**2)<=900 ) {
                $objI->{'cluster'}++; 
                $objJ->{'cluster'}++; 
            }
        }
    }

    if (scalar(@cat) == 0) {  
        reportEvent($verbose,"STATUS",3,"Warning: 0 catalog entries");
    }
    else {
        # divide into separate cats
        my @poscat = ();
        my @negcat = ();
        my @remcat = ();
        foreach my $obj (@cat) {
            if (($obj->{'dipole'} == 0) && ($obj->{'ellipticity'} < 0.3) && ($obj->{'cluster'} < 4)) {
                if ($obj->{'pos_neg'} == 1) {
                    push(@poscat, $obj);
                }
                elsif ($obj->{'pos_neg'} == 2) {
                    push(@negcat, $obj);
                }
                else {
                    reportEvent($verbose,"STATUS",3,"Warning: bad pos_neg value '".$obj->{'pos_neg'}."', ignoring obj");
                }
            }
            else {
                push(@remcat, $obj);
            }
        }

        if (scalar(@poscat) > 0) {  
            open POSFH, "> ${outpath}/${exposure}/${exposure}_${ccd}_${extra}cat.cat";
            foreach my $obj (@poscat) {
                my $str = toStringObj($obj);
                print POSFH "$str\n";
            }
            close POSFH;
        }
        else {
            reportEvent($verbose, "STATUS", 3, "Warning: 0 entries for pos cat");
        }

        if (scalar(@negcat) > 0) {  
            open NEGFH, "> ${auxpath}/${exposure}_${ccd}_NEGATIVE_${extra}cat.cat";
            foreach my $obj (@negcat) {
                my $str = toStringObj($obj);
                print NEGFH "$str\n";
            }
            close NEGFH;
        }
        else {
            reportEvent($verbose, "STATUS", 3, "Warning: 0 entries for neg cat");
        }

        if (scalar(@remcat) > 0) {  
            open REMFH, "> ${auxpath}/${exposure}_${ccd}_REMOVE_${extra}cat.cat";
            foreach my $obj (@remcat) {
                my $str = toStringObj($obj);
                print REMFH "$str\n";
            }
            close REMFH;
        }
        else {
            reportEvent($verbose, "STATUS", 3, "Warning: 0 entries for rem cat");
        }
    }
}


sub toStringObj {
    my ($obj) = @_;
    my $str = "";
    foreach my $k (@OUTORDER) {
        $str .= $obj->{$k}." ";
    }
    chomp($str);
    return $str;
}

sub runCmd {
    my ($cmd,$name,$verbose) = @_;
    reportEvent($verbose,"STATUS",1,"Executing $name: '$cmd'") if $verbose > 3;
    reportEvent($verbose, "STATUS", 1, "Start application $name") if $verbose > 3;
    system("$cmd 2>&1");
    my $stat = $?;
    reportEvent($verbose, "STATUS", 1, "End application $name") if $verbose > 3;
    return $stat;
}

sub Usage {
    print STDERR  "DiffImg.pl -binpath <path> -terapixpath <path> -etcpath <path> -outpath <path> -auxpath <path> -list <list> -difftype <difftype>\n";
}


sub uncompressFile {
    my ($compFilename, $teraDir) = @_;

    my $compressionType = ""; 
    if ($compFilename =~ m/fz$/) {
        reportEvent($verbose,"STATUS",1,"funpacking: $compFilename") if $verbose > 3;
        system("$teraDir/funpack $compFilename");
    } elsif ($compFilename =~ m/gz$/) {
        system("gunzip $compFilename");
    }
}
