#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# AUTHOR:  Tony Darnell (tdarnell@uiuc.edu)
#
# SVN Info:
#
# $Rev:: 8754                                          $: Rev of last commit
# $LastChangedBy:: donaldp                             $: Author of last commit
# $Date:: 2012-09-26 19:02:29 -0700 (Wed, 26 Sep 2012) $: Date of last commit
#

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use Cwd;
use Data::Dumper;
use DBI;
use Getopt::Long;
use Graphics::PLplot;
use FindBin;

use lib ("$FindBin::Bin/../../../Database/trunk/lib");
use DB::DESUtil;

my $plotName = q{};

#
# Handle command lines options here
#
Getopt::Long::GetOptions(
   "plotname=s" => \$plotName,
) or usage("Invalid command line options\n");

#
# Open a DB connection
#
my $dbh =DB::desDBI->new
  (
             DBIattr => {
              AutoCommit => 0,
              RaiseError => 1,
              PrintError => 0,
             }
  );

#
# Get the FWHMS
#
my $sql = qq{select FWHM from IMAGE where imagetype='red'};
my $sth = $dbh->prepare($sql);
$sth->execute();
my @fwhms;
#my @fwhms = map { $_->[0] } @{ $sth->fetchall_arrayref };
while (my $data = $sth->fetchrow_arrayref()){
#
# Get rid of undefined values
#
  next if not defined @$data[0];
  push @fwhms,@$data[0];
}
$sth->finish();
$dbh->disconnect();

my @sortedfwhms = sort(@fwhms);
my $numPoints = scalar (@sortedfwhms);
my $minData = $sortedfwhms[0];
my $maxData = $sortedfwhms[-1];

print "numPoints:  $numPoints\n";
print "min:  ",$sortedfwhms[0],"\n";
print "max:  ",$sortedfwhms[-1],"\n";

#
# Plot the results
#
my $plWin = 1;
my $numBins = 50;
#
# Need to recompile libplplot to give us more devices, like 'jpeg' and 'png'
#
plsdev("psc");
plsfnam("$plotName");

#
# These were taken from some settings I found in psfex code.  It looks
# terrible, but you get the idea.
#
plfontld(1);
plscol0(15, 0,0,0);           # Force the foreground colour to black
plscol0(3, 0,200,0);          # Force the green colour to darken 
plscol0(7, 128,128,128);      # Force the midground colour to grey 
plscol0(8, 64,0,0);           # Force the brown colour to darken 
plinit();
plenv($minData,$maxData,0,10000,0,0);
plhist(\@sortedfwhms,$minData,$maxData,$numBins,$plWin);

exit(0);

sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -plotname plot.ps \n"
   );

   die("\n")

}
