#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# makecutout.pl
#
# DESCRIPTION:
#
# AUTHOR:  
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev: 8755 $
# $LastChangedBy: donaldp $
# $LastChangedDate: 2012-09-26 19:29:29 -0700 (Wed, 26 Sep 2012) $
#

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;

use Cwd;
use Data::Dumper;
use Getopt::Long;
use FindBin qw($Bin);
use Math::Trig;
use Pod::Usage;

use lib ("$Bin/../lib/perl5","$Bin/../../../Database/trunk/lib");

use DB::DESUtil;
use DB::IngestUtils;
use DB::FileUtils;


$| = 1;

my $VERSION = '$Rev: 8755 $';

my $displayNumber = getDisplayNumber();
print "Last unused display: $displayNumber\n";

$ENV{DISPLAY}=':' . $displayNumber;

my %options = ();
Getopt::Long::GetOptions(\%options,
   'help!','man!',
   'project=s','rchan=s','gchan=s','bchan=s','regiontype=s','overlaycolor=s',
   'overlayfont=s','overlayweight=i','zoom=f','width=s','height=s',
   'zr1=i','zr2=i','zg1=i','zg2=i','zb1=i','zb2=i',
   'regionfile=s','objectfile=s','regionscale=f','outputdir=s',
   'archivenode=s','scale=s','function=s','radius=i','smoothon'
);

Pod::Usage::pod2usage (1) if exists($options{'help'});

Pod::Usage::pod2usage (
  -verbose => 2,
  -exitval => 2,
) if exists($options{'man'});

#
# Get some command line options and fill in defaults
#
my $project;
my $archiveNode;

if (exists ($options{'project'})){
   $project = $options{'project'};
} else {

  Pod::Usage::pod2usage (
    -verbose => 2,
    -exitval => 2,
    -message => 'No project Specified'
  );

}

if (exists ($options{'archivenode'})){
   $archiveNode = $options{'archivenode'};
} else {

  Pod::Usage::pod2usage (
    -verbose => 2,
    -exitval => 2,
    -message => 'No archivenode Specified'
  );

}

my $rchan = (exists($options{'rchan'}) ) ? $options{'rchan'} : 'i';
my $gchan = (exists($options{'gchan'}) ) ? $options{'gchan'} : 'r';
my $bchan = (exists($options{'bchan'}) ) ? $options{'bchan'} : 'g';

my $regionScale = 
   (exists($options{'regionscale'}) ) ? $options{'regionscale'} : 5;

my $regionType = (exists($options{'regiontype'})) ? $options{'regiontype'}
                                                     : 'circle';

my $overlayColor = (exists($options{'overlaycolor'})) ? $options{'overlaycolor'}
                                                     : 'green';
my $overlayFont = (exists($options{'overlayfont'})) ? $options{'overlayfont'}
                                                     : 'helvetica 10 normal';
my $overlayWeight = 
   (exists($options{'overlayweight'})) ? $options{'overlayweight'} : 2;

my $outputDir = (exists($options{'outputdir'})) ? $options{'outputdir'} 
                                                : cwd();

my $zoom = (exists($options{'zoom'})) ? $options{'zoom'} : 1.0;

my $width = (exists($options{'width'})) ? $options{'width'} : '500';
my $height = (exists($options{'height'})) ? $options{'height'} : '500';

my $zr1 = (exists($options{'zr1'})) ? $options{'zr1'} : 0;
my $zr2 = (exists($options{'zr2'})) ? $options{'zr2'} : 2000;
my $zg1 = (exists($options{'zg1'})) ? $options{'zg1'} : 0;
my $zg2 = (exists($options{'zg2'})) ? $options{'zg2'} : 2000;
my $zb1 = (exists($options{'zb1'})) ? $options{'zb1'} : 0;
my $zb2 = (exists($options{'zb2'})) ? $options{'zb2'} : 2000;

my $scale = (exists($options{'scale'})) ? $options{'scale'} : 'log';
my $function = (exists($options{'function'})) ? 
               $options{'function'} : 
               'gaussian';

my $radius = (exists($options{'radius'})) ? $options{'radius'} : 2;
my $smoothOn = (exists($options{'smoothon'})) ? 'yes' : 'no';

#
# Open a DB Connection
#
my $dbh = DB::DESUtil->new
 (
             DBIattr => {
               AutoCommit => 0,
               RaiseError => 1
             }
  );

my $objectList = $options{'objectfile'};
my @objects = ();

my $outputRegionFile = "$outputDir/cutout.region";

#
# Read in the formatted object list
#
if (-e $objectList){
  open(my $OBJFILE, "<$objectList") or die "Can't open $objectList: ";
  @objects = <$OBJFILE>;
  close $OBJFILE;
} else {
  die "$objectList doesn't exist"
}

#
# Loop through each object and write entry in DS9 Region file
#
open(my $REGIONFILE,"> $outputRegionFile");

print $REGIONFILE qq{global color=$overlayColor font="$overlayFont"\nfk5\n};

my @rimgs;
my @gimgs;
my @bimgs;
my @remainingObjs;

foreach my $object (@objects){

  chomp($object);
  next if not $object;
  $object =~ s/\s+/ /gis;
  print "Processing object:  $object\n";
  my ($tag,$ra,$dec,$equinox) = split / /,$object;
  print $REGIONFILE
    qq/$regionType($ra,$dec,$regionScale") #width=$overlayWeight text={$tag}\n/;

#
# Get the coadd tile for this object
#
  my $tileName = getCoaddTile($dbh,$project,$ra,$dec);

  if (not defined $tileName){
    print "WARNING: tile not found.\n";
    next;
  }

#
# Find out where the images are
#
  my $rimg = getImageLocation($dbh,$project,$tileName,$rchan,$archiveNode);
  next if (not defined $rimg);
  my $gimg = getImageLocation($dbh,$project,$tileName,$gchan,$archiveNode);
  next if (not defined $gimg);
  my $bimg = getImageLocation($dbh,$project,$tileName,$bchan,$archiveNode);
  next if (not defined $bimg);

  push @remainingObjs,$object;
  push @rimgs,$rimg;
  push @gimgs,$gimg;
  push @bimgs,$bimg;

}

# 
# Append any region file given
#
if (exists($options{'regionfile'})){

  my @files = split /,/, $options{'regionfile'};

  my @lines;
  foreach my $file (@files){

    open my $MYFILE, "<$file";
    my @all_lines = <$MYFILE>;
    push @lines,@all_lines;

  }

  foreach my $line (@lines){
    print $REGIONFILE "$line";
  }

} 

close($REGIONFILE);

#
# Check for a running Xvfb buffer for this user.  Need to kill if it exists
# because there's only one allowed per user.  This will also kill any running
# ds9 process that may be connected to it.
#

my $user = `whoami`;
chomp($user);

my @userpids = `ps -U $user`;
my @xpids = grep /Xvfb/,@userpids;
my @ds9pids = grep /ds9/,@userpids;

if (scalar(@xpids)) {
  foreach my $line (@xpids){
    chomp($line);
    $line =~ s/^\s+//g;
    $line =~ s/\s+/ /gis;
    my ($pid,$pts,$time,$process) = split / /,$line;
    print "$line\n";
    print "Xvfb framebuffer found for user $user\n";
    print "Killing $pid...\n";
    system("kill $pid");
  }
}

print "Starting Xvfb buffer...\n\n";
my $xbuffPID = open(my $XVFB,"| /usr/bin/Xvfb :$displayNumber -pixdepths 24 -screen 0 1600x1200x24 -shmem &") or die "Can't fork: $!";
close($XVFB);

print "Starting DS9 to use that buffer...\n\n";
my $ds9PID = open(my $DS9,"| ds9 -xpa &") or die "Can't fork: $!";
close($DS9);

sleep 2;

$xbuffPID++;
$ds9PID++;

my $ind = 0;
foreach my $object (@remainingObjs){

  open my $SHELL,">$outputDir/cmd.csh" or die "Can't open csh file: $!\n";;

  chomp($object);
  print "$object\n";
  $object =~ s/\s+/ /gis;
  my ($tag,$ra,$dec,$equinox) = split / /,$object;
  my $tileName = getCoaddTile($dbh,$project,$ra,$dec);
  print "Creating jpg for tile $tileName:\n";
  my $rimg = $rimgs[$ind];
  my $gimg = $gimgs[$ind];
  my $bimg = $bimgs[$ind];
  next if not $rimg;
  next if not $gimg;
  next if not $bimg;

  print $SHELL <<DONE;
/usr/local/bin/xpaset -p ds9 view colorbar no
/usr/local/bin/xpaset -p ds9 rgb
/usr/local/bin/xpaset -p ds9 rgb channel red
/usr/local/bin/xpaset -p ds9 file $rimg
/usr/local/bin/xpaset -p ds9 scale $function
/usr/local/bin/xpaset -p ds9 scale limits $zr1 $zr2
/usr/local/bin/xpaset -p ds9 smooth $smoothOn
/usr/local/bin/xpaset -p ds9 smooth $radius
/usr/local/bin/xpaset -p ds9 rgb channel green
/usr/local/bin/xpaset -p ds9 file $gimg
/usr/local/bin/xpaset -p ds9 scale $function
/usr/local/bin/xpaset -p ds9 scale limits $zg1 $zg2
/usr/local/bin/xpaset -p ds9 smooth $smoothOn
/usr/local/bin/xpaset -p ds9 smooth $radius
/usr/local/bin/xpaset -p ds9 rgb channel blue
/usr/local/bin/xpaset -p ds9 file $bimg
/usr/local/bin/xpaset -p ds9 scale $function
/usr/local/bin/xpaset -p ds9 scale limits $zb1 $zb2
/usr/local/bin/xpaset -p ds9 smooth $smoothOn
/usr/local/bin/xpaset -p ds9 smooth $radius
/usr/local/bin/xpaset -p ds9 pan to $ra $dec wcs fk5
/usr/local/bin/xpaset -p ds9 zoom $zoom
/usr/local/bin/xpaset -p ds9 regions $outputRegionFile
/usr/local/bin/xpaset -p ds9 width $width
/usr/local/bin/xpaset -p ds9 height $height
/usr/local/bin/xpaset -p ds9 saveimage jpeg $outputDir/$tag\_$tileName.jpg
/usr/local/bin/xpaset -p ds9 frame delete
DONE

  $ind++;

  close($SHELL);

  system("/bin/csh $outputDir/cmd.csh");
  #unlink("$outputDir/cmd.csh");
}

#close(DS9);

$dbh->disconnect();

#
# Kill the Xvfb and ds9 processes
#
system("kill $xbuffPID");
system("kill $ds9PID");

exit(0);

#
# Subroutines
#

sub getCoaddTile{

  my ($dbh,$project,$ra,$dec) = @_;

  my $sql = qq{

    select tilename,ra,dec,equinox,npix_ra,npix_dec,pixelsize
      FROM coaddtile
      WHERE project = '$project'
        AND (ra between ($ra-pixelsize*npix_ra/7200/cos($dec*3.141593/180.0))
        AND ($ra+pixelsize*npix_ra/7200/cos($dec*3.141593/180.0)))
        AND (dec between ($dec-pixelsize*npix_dec/7200) 
        AND ($dec+pixelsize*npix_dec/7200))

  };

  my $sth = $dbh->prepare($sql);
  $sth->execute();

  my $results = $sth->fetchall_arrayref({});

  my $numTiles = scalar @$results;

  if ($numTiles > 1){

    print "More than one tile found for this object,\nfinding tile where object is closest to center...\n";
    my $dist = 0;
    my $ind = 0;
    my $largestInd = 0;
    my $lastDist = 0;
   
    foreach my $result (@$results){

      my $tileRa  = $result->{'ra'};
      my $tileDec = $result->{'dec'};

      $dist = 
         ( ($dec-$tileDec) * ($dec-$tileDec) ) +
         ( (cos(0.5*($dec+$tileDec)*pi/180.0)*($ra-$tileRa)) *
           (cos(0.5*($dec+$tileDec)*pi/180.0)*($ra-$tileRa)) );

      $largestInd = $ind if ($dist > $lastDist);
      $lastDist = $dist;
      $ind++;
    }

    print "Using Tile: ",$results->[$largestInd]->{'tilename'},"\n\n";
    return $results->[$largestInd]->{'tilename'};

  }

  return $results->[0]->{'tilename'};

}

sub getImageLocation{

  my ($dbh,$project,$tileName,$chan,$archiveNode) = @_;

  my $sql = qq{
    SELECT id,run FROM coadd 
    WHERE project='$project' AND tilename='$tileName' AND band='$chan'
    ORDER BY run
  };

  my $sth = $dbh->prepare($sql);
  $sth->execute();

  my $results = $sth->fetchall_arrayref({});

  foreach my $res (@$results){
    pop @$results if ($res->{'run'} =~ m/^$project/);
  }

  my $result = pop @$results;
  my $id = $result->{'id'};

  if (!$id){
    print "WARNING: image not found. Project = $project, tilename=$tileName\n";
    #print "Here is the sql:\n$sql";
    return (undef);
  }

  $sth->finish();

  $sql = qq{select fileclass,run,filetype,filename,archivesites from location where id=$id};

  $sth = $dbh->prepare($sql);
  $sth->execute();
  $results = $sth->fetchall_arrayref({});
  my $fileClass = $results->[0]->{'fileclass'};
  my $run = $results->[0]->{'run'};
  my $fileType = $results->[0]->{'filetype'};
  my $fileName = $results->[0]->{'filename'};
  my $archiveSites = $results->[0]->{'archivesites'};

  my ($locationId,$archiveHost,$archiveRoot,$archiveSiteStr) =
     getArchiveNodeInfo($dbh, $archiveNode);

  my $fullImageName = 
     qq{$archiveRoot/$project/$fileClass/$run/$fileType/$fileName};

  if (substr($archiveSites,$locationId-1,1) eq 'F'){
    $fullImageName .= '.fz';
  } elsif (substr($archiveSites,$locationId-1,1) eq 'G'){
    $fullImageName .= '.gz';
  }

  return $fullImageName;

}

sub getArchiveRoot{

  my ($dbh,$locationId) = @_;

  my $sql = qq{select archive_root from archive_sites where location_id = ?};
  my $sth = $dbh->prepare($sql);
  $sth->execute($locationId);

  return $sth->fetchrow();

}

sub getDisplayNumber {

  my $minDisplayNumber = 50;
  my $lastUsedDisplay = `/bin/ls /tmp/.X11-unix/X* | \
  sed -e 's|.*X||' | sort -n | tail -1`;
  chomp($lastUsedDisplay);

  if (not $lastUsedDisplay){
    return $minDisplayNumber;
  } elsif ($lastUsedDisplay < $minDisplayNumber){
    return $minDisplayNumber;
  } else {
    return $lastUsedDisplay + 1;
  }

}

__END__

=head1 NAME

B<makecutout.pl> - Save cutout of DES coadd as an RGB image

=head1 SYNOPSIS

B<makecutout.pl> [Options] I<objectlist...>

=head1 OPTIONS

=over 10

=item B<-project>

project data reside in

=item B<-archivenode>

archivenode coadds reside on

=item B<-objectfile>

Text file containing objects of interest.  One object per line.  Each line should have the form: TAG RA DEC EQUINOX (space delimited)

=item B<-regionfile>

DS9 region file.  This will be appended to the end of the one created by this script.  Enter multiple regions files by using a comma separated list 
(e.g., -regionfile file1.reg,file2.reg,file3.reg)

=item B<-rchan>

image to put in the red channel

=item B<-gchan>

image to put in the green channel

=item B<-bchan>

image to put in the blue channel

=item B<-regiontype >

DS9 region type (default: circle)

=item B<-overlaycolor>

DS9 color for plottin (default: green)

=item B<-overlayfont>

DS9 font specification (default: "helvetica 10 normal")

=item B<-overlayweight>

DS9 line weight in te overlay (default: 2)

=item B<-zoom>

DS9 zoom factor (default: 1.0)

=item B<-width>

width of jpg image in pixels

=item B<-height>

height of jpg image in pixels

=item B<-z[rgb]1>

Min image scaling parameter for rgb channel(default: 0);

=item B<-z[rgb]2>

Max image scaling parameter for rgb channel (default: 2000)

=item B<-outputdir>

Directory to use to write output to.  Must already exist.

=item B<-help>

Print a brief help message and exit

=item B<-man>

Print the manual page and exits

=back

=head1 DESCRIPTION

B<makecutout.pl> reads in an object list and creates an RGB image with DS9 
region files overlaid.

=cut


