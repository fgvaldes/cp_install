#!/usr/bin/env perl
########################################################################
#  $Id: imcorrect_prep.pl 3199 2009-02-23 20:43:37Z dadams $
#
#  $Rev:: 3199                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-02-23 14:43:37 #$:  # Date of last commit.
#
#  DESCRIPTION:
#
#######################################################################


use strict;
use warnings;

print "\n$0 @ARGV\n\n";

use Getopt::Long;
use File::Basename;
use File::Path;
use FindBin;

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use DB::EventUtils;

my ($cmd, $out, $stat, $eventstr);
my ($list, $outdir) = undef;
my $zoom = 0.25;
my $geometry = "600x1300";
my $verbose = 1;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'list|i=s'  => \$list,
    'zoom|z=s'  => \$zoom,
    'geometry|g=s' => \$geometry,    
    'output_rundir|o=s' => \$outdir,
    'verbose|v=i' => \$verbose
);

if (!defined($list)) {
    print "Error:  must specify list containing fits files\n";
    exit 1;
}

if (!defined($outdir)) {
    print "Error:  must specify output_rundir\n";
    exit 1;
}

# outdir = output run directory, need to append QA (and exposurename if nightly) 
$outdir .= '/' if ($outdir !~ /\/$/);
$outdir .= 'QA';

$geometry =~ s/\s//g;   # remove all spaces
if ($geometry !~ m/\d+x\d+/i) {
    print "Error: geometry wrong format.  Should be ###x###, given '$geometry'\n";
    exit 1;
}

open LIST, "< $list" or die "Cannot open input list: $list";
while (my $origfile = <LIST>) {
    chomp($origfile);
    print "origfile = $origfile\n";
    if ($origfile =~ /\S/) {
        # get exposurename
        my ($name,$path,$suffix) = fileparse($origfile,('.fits','.fits.fz','.fits.gz'));
        print "name = $name\n";
        print "path = $path\n";
        print "suffix = $suffix\n";

        $path =~ s/\/$//;  # remove trailing / so fileparse will work
        my ($exposurename, $restpath) = fileparse($path); 

        print "restpath = $restpath\n";
        print "exposurename = $exposurename\n";

        my $file = undef;   # file will contain uncompressed filename
        if (-r $origfile) {
            $file = $origfile; 
        }
        elsif (-r "$origfile.fz") {
            $origfile .= ".fz";
            $file = "$name.fits";
            $cmd = "funpack -O $file $origfile";
            print "\n\ncmd> $cmd\n";
            $out = `$cmd 2>&1`;
            $stat = $?;
            print "$out\n";
            print "exit status = $stat\n\n";
            if ($stat != 0) {
                $eventstr = "Could not funpack file: $origfile\n";
                reportEvent($verbose,"STATUS",4,$eventstr);
                next;
            }
        }
        elsif (-r "$origfile.gz") {
            $origfile .= "gz";
            $file = "$name.fits";
            $eventstr = "Currently does not handle gzip'd files";
            reportEvent($verbose, "STATUS", 4, $eventstr); 
            next;

            #### TODO figure out how to gunzip to a different file leaving orig
            #### $cmd = "(gunzip -c $origfile > $file)";
            #### print "\n\ncmd> $cmd\n";
            #### $out = `$cmd 2>&1`;
            #### $stat = $?;
            #### print "$out\n";
            #### print "exit status = $stat\n\n";
            #### if ($stat != 0) {
            ####     $eventstr = "Could not gunzip file: $origfile\n";
            ####     reportEvent($verbose,"STATUS",4,$eventstr);
            ####     next;
            #### }
        }
        else {
            $eventstr = "Could not find file $origfile";
            reportEvent($verbose,"STATUS",4,$eventstr);
            next;
        }
        
        print "uncompressed input file = $file\n";

        my $fulloutdir = $outdir.'/'.$exposurename; 

        print "outdir = $outdir\n";
        print "fulloutdir = $fulloutdir\n";

        # mkdir exposure subdir
        if (! -d $fulloutdir) {
            eval { mkpath($fulloutdir) };
            if ($@) {
                $eventstr = "Error\nCouldn’t create directory $fulloutdir: $@";
                reportEvent($verbose, "STATUS", 5, $eventstr);
                exit 1;
            }
        }

        $cmd = "stiff $file -OUTFILE_NAME=$name.tif";
        print "\n\ncmd> $cmd\n";
        $out = `$cmd 2>&1`;
        $stat = $?;
        print "$out\n";
        print "exit status = $stat\n\n";

        if ($stat != 0) {
            $eventstr = "stiff $file failed\n";
            reportEvent($verbose,"STATUS",4,$eventstr);
        }
        else {
            my $pngfile = "$fulloutdir/$name.png"; 
            print "pngfile = $pngfile\n";

            if (! -r "$name.tif") {
                $eventstr = "Could not find $name.tif\n";
                reportEvent($verbose,"STATUS",4,$eventstr);
            }
            else {
                $cmd = "convert -resize 10% $name.tif $pngfile";
                print "\n\ncmd> $cmd\n";
                $out = `$cmd 2>&1`;
                $stat = $?;
                print "$out\n";
                print "exit status = $stat\n\n";
                if ($stat != 0) {
                    $eventstr = "convert $name.tif failed\n";
                    reportEvent($verbose,"STATUS",4,$eventstr);
                }
            }
        }

        # clean up temp files
        if (-r "$name.tif") {
            unlink "$name.tif";
        }

        if (-r "stiff.xml") {
            unlink "stiff.xml";
        }

        # if had to uncompress the original file, 
        # delete the uncompressed version
        if (($file ne $origfile) && ($file !~ /\//) && -r $file) {
            unlink $file;
        }
        print "\n\n\n";
    }
}
close LIST;
