#######################################################################
#  $Id: SCAMP.pm 14598 2013-09-13 20:43:53Z ankitc $
#
#  $Rev:: 14598                            $:  # Revision of last commit.
#  $LastChangedBy:: ankitc                 $:  # Author of last commit. 
#  $LastChangedDate:: 2013-09-13 13:43:53 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#    Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package ImageProc::SCAMP;

use strict;
use warnings;
use File::Path;
use Carp;
use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use ImageProc::Util qw(getSkyWindow writeStandardsCat);

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&createAhead &createAstroStds &runSCAMP);
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}

#######################################################################
# SUBROUTINE: createAhead
#
# INPUT:
#   string - $template:  .ahead template.
#   hasref - $exposures: Exposurelist with pointing data.
#   string - $outdir:    Location to write .ahaed file. 
#
# OUTPUT:
#   File - .ahead input file for SCAMP
#
#######################################################################
sub createAhead {
    my ($template, $exposures, $outdir) = @_;

    # read template
    my @templatelines;
    open FH, "< $template" or die "Could not open template: $template\n";
    while (my $line = <FH>) {
        push (@templatelines,$line);
    }

    mkdir "$outdir/red";

    # write ahead files
    foreach my $e (sort keys %$exposures) {
        my $ra = $exposures->{$e}->{'telra'};
        my $dec = $exposures->{$e}->{'teldec'};
        mkpath "$outdir/red/$e";
        my $aheadfile = "$outdir/red/$e/$e"."_scamp.ahead";
        open FH, "> $aheadfile" or die "Cannot open aheadfile ($aheadfile)";
        foreach my $line (@templatelines) {
            print FH $line;
            if ($line =~ /COMMENT/i) {
                printf FH "CRVAL1  =           %3.6f\n",$ra;
                printf FH "CRVAL2  =           %3.6f\n",$dec;
            }
        }
        close FH;
    }
}

#######################################################################
# SUBROUTINE: createAstroStds
#
# INPUT:
#   hasref: Args = {
#     hashref - 'pointing_list':
#     object -  'dbh':            Database connecton handle.
#     string -  'outfile':        Standards file name to create.
#     string -  'catalog_table':  Name of standrds DB table.
#     float  -  'delta_ra':       ra search half width. [OPTIONAL]
#     float  -  'delta_dec':      dec search half width. [OPTIONAL]
#     integer  -  'useDB':        whether to query DB (1) or use catalog files (0) [OPTIONAL]
#   }
#
# OUTPUT:
#   File - Astro standards catalog.
# 
#######################################################################
sub createAstroStds {
  my $Args = shift;

  print "createAstroStds BEG ", time, "\n";
  print Dumper($Args),"\n";

  my ($PointingList, $dbh, $outfile, $catalog_table, $catalog_table_IDfield);
  my ($dra, $ddec);
  my $useDB = 1;
  my ($ttype, $tform, $tunit);
  my $status = 0;

  # Grab inputs and test for required
  $PointingList = $Args->{'pointing_list'}  if ($Args->{'pointing_list'});
  $dbh = $Args->{'dbh'} if ($Args->{'dbh'});
  $outfile = $Args->{'outfile'} if($Args->{'outfile'});
  $catalog_table = $Args->{'std_table_name'} if($Args->{'std_table_name'});
  $catalog_table_IDfield = $Args->{'std_table_IDfield'} if($Args->{'std_table_IDfield'});
  $dra = $Args->{'delta_ra'} if ($Args->{'delta_ra'});
  $ddec = $Args->{'delta_dec'} if ($Args->{'delta_dec'});
  $useDB = $Args->{'useDB'} if (defined($Args->{'useDB'}));

  croak("Must provide a pointing list hash ref.\n") if (! $PointingList);
  croak("Must provide a live database handle object.\n") if (! $dbh);
  croak("Must provide a output file name for fits standards catalog.\n") if (! $outfile);

  # Default window:
  $dra = 1.1 if (! $dra);
  $ddec = 1.1 if (! $ddec);

  # Default catalog:
  if (! $catalog_table) {
    $catalog_table = 'usnob_cat1';
    $catalog_table_IDfield = 'usnob_cat1_id';
  }

  my $standards;

  if ($useDB) {
        print "Using database\n";
        # For each pair get bounding box search region:
        foreach my $pointing (values %$PointingList) {
            my $window_coords = getSkyWindow($pointing->{'telra'}, $pointing->{'teldec'}, $dra, $ddec);
            $pointing->{'maxra'} = $window_coords->{'maxra'};
            $pointing->{'minra'} = $window_coords->{'minra'};
            $pointing->{'maxdec'} = $window_coords->{'maxdec'};
            $pointing->{'mindec'} = $window_coords->{'mindec'};
        }
        # Get hash of array refs of standards data (ra,dec,sra,sde,r2) from database:
        $standards = $dbh->getStandardsData($PointingList, $catalog_table, $catalog_table_IDfield);
  }
  else {
        print "Using wcstools\n";
        $standards = readStdsFromFile($PointingList, $catalog_table, $dra);
        $standards = repackageStdsHash($standards);
  }
 

  # Write fits catalog of standards:
  writeStandardsCat($standards, $outfile);

  print "createAstroStds END ", time, "\n";
}


#######################################################################
#
# Assumes wcstools' scat command is in path
#
#######################################################################
sub readStdsFromFile {
    my ($PointingData, $catalog, $delta) = @_;

    my $arcsecs = 3600 * $delta;

    my %standards;
    foreach my $pdata (values %$PointingData) {
        my $telra = $pdata->{'telra'};
        my $teldec = $pdata->{'teldec'};
        my $cmd = "scat1 $arcsecs $telra $teldec";
        print "Executing scat1 for: $telra, $teldec, $delta\n";
        print "cmd: $cmd\n";
        my $out = `$cmd 2>&1`;
        my $stat = $?;
        if ($stat == 0) {
            my $cnt = 0;
            my $uniqcnt = 0;
            my @lines = split /\n/, $out;
            foreach my $line (@lines) { 
                if ($line =~ /\d/) {  # ignore empty lines
                    $cnt++;
                    #USNO_B1_number    RA2000     Dec2000   MagB1  MagR1  MagB2  MagR2  MagN  PM NI SG  Arcsec
                    my @linearr = split /\s+/, $line;
                    my $ID = $linearr[0];
                    if (!defined($standards{$ID})) {
                        $uniqcnt++;
                        $standards{$ID}{'ra'} = $linearr[1];
                        $standards{$ID}{'dec'} = $linearr[2];
                        $standards{$ID}{'r2'} = $linearr[6];
                        $standards{$ID}{'b2'} = $linearr[5];
                        $standards{$ID}{'i2'} = $linearr[7];

                        # missing from scat output, defaulting to nominal 
                        $standards{$ID}{'sra'} = 0.2;
                        $standards{$ID}{'sde'} = 0.2;
                    }
                }
            }
            print "$cnt results, ",$uniqcnt," new objects appended\n";
        }
        else {
            print "scat error ($?)\n";
            print "$out\n";
        }
        print "\n";
    }
    print scalar(keys(%standards)),"  total objects collected.\n";

    return \%standards;
}


#######################################################################
# Repackage data into a single hash of array references, one for each column to
# be created in the FITS astro standards catalog:
#######################################################################
sub repackageStdsHash {
  my ($standards) = @_;
  my %results;

  foreach my $val (values %$standards) {
      push(@{$results{'ra'}},$val->{'ra'});
      push(@{$results{'dec'}},$val->{'dec'});
      push(@{$results{'sra'}},$val->{'sra'});
      push(@{$results{'sde'}},$val->{'sde'});
      push(@{$results{'r2'}},$val->{'r2'});
      push(@{$results{'b2'}},$val->{'b2'});
      push(@{$results{'i2'}},$val->{'i2'});
  }

  return \%results;
}



#######################################################################
#
#######################################################################
sub runSCAMP {
  my $scamp = shift;
  my $astromSigma1 = shift;
  my $astromSigma2 = shift;
  my $astromChi    = shift;
  my $nstars_ref = shift;
  my $astromSigmaH1 = shift;
  my $astromSigmaH2 = shift;
  my $chi2_ref = shift;
  my $nstarsH  = shift;
  my $verbose = shift;

  
  my $retry = 0;
  open (OUT, "$scamp 2>&1 |");
  while (<OUT>) {
    if (m/WARNING:/) {
      s/>//;
      warn "\nSTATUS3BEG\n" if ($verbose >= 2);
      warn $_;
      if (m/WARNING: Not enough matched detections in set/) {
        $retry = 1;
      }
      elsif (m/WARNING: Significant inaccuracy likely to occur in projection/) {
        $retry = 1;
      }
      elsif (m/No source found in reference catalog/) {
        $retry = 1;
      }
      warn "STATUS3END\n\n" if ($verbose >= 2);
    }
    elsif(m/Astrometric stats \(external\):/) {
      print;
      my $line;
      while ($line = <OUT>) {
        print $line;
        last if ($line =~ /Group/);
      }
      my @vals = split(/\s+/, $line);

      # Counting 1 thru 10
      # $3 = astromsigma_ref_1
      # $4 = astromsigma_ref_2
      # $5 = astromchi2_ref
      # $6 = astromndets_ref
      # $7 = astromsigma_ref_highsn_1
      # $8 = astromsigma_ref_highsn_2
      # $9 = astromchi2_ref_highsn
      # $10 = astromndets_ref_highsn
      # array 0 thru 9
      $$astromSigma1 = $vals[2];
      $$astromSigma2 = $vals[3];
      $$astromChi    = $vals[4];
      $$nstars_ref = $vals[5];
      $$astromSigmaH1 = $vals[6];
      $$astromSigmaH2 = $vals[7];
      $$chi2_ref = $vals[8];
      $$nstarsH  = $vals[9];

    }
    else {
      print $_;
    }
  }
  close OUT; # to get $?
  my $exit = $?;
  if ($exit & 128) {
      $retry = 1;
      warn "\nSTATUS4BEG\n" if ($verbose >= 2);
      warn "SCAMP died due to a segmentation fault\n";
      warn "STATUS4END\n\n" if ($verbose >= 2);
  }
  elsif ($exit & 127) {
      $retry = 1;
      warn "\nSTATUS4BEG\n" if ($verbose >= 2);
      warn "SCAMP died with signal ".($exit & 127)."\n";
      warn "STATUS4END\n\n" if ($verbose >= 2);
  }
  elsif ($exit != 0) {
      $retry = 1;
      warn "\nSTATUS4BEG\n" if ($verbose >= 2);
      warn "SCAMP exited with non-zero exit value (".($exit>>8).")\n";
      warn "STATUS4END\n\n" if ($verbose >= 2);
  }

  return $retry;
}



1;
