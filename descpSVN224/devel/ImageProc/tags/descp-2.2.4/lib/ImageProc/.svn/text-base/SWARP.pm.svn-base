#######################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#    Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package ImageProc::SWARP;

use strict;
use warnings;
use Carp;
use Math::Trig;

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&updateImageCoordsForOffset &getTileSearchRange &createSWARPInputFiles &readSWARPInputFile &runSWARP &getMinMax);
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}


#######################################################################
#
# SUBROUTINE: updateImageCoordsForOffset
#
# DESCRIPTION:
#  Will adjust the recorded image ra and dec from the image metadata 
#  table (and header) for the detector offset.  This adjusted value
#  is then ready to use to build a search region for surrounding tile 
#  (ra and dec) centers.
#
# INPUT:
#  $Args = {
#    images = {
#      {ccd, ra, dec, ...}
#    },
#    offsets = {
#      {raoffset, decoffset, rahwidth, dechwidth}
#    }
#   }
#
# OUTPUT:
#  images = {
#    {ra_off, dec_off, maxra, minra, maxdec, mindec}
#  }
#
#######################################################################
sub updateImageCoordsForOffset {
  my $Args = shift;
  
  my $Offsets = $Args->{'offsets'} if ($Args->{'offsets'});
  my $ImageList = $Args->{'images'} if ($Args->{'images'});

  my $List;
  if (ref $ImageList eq 'ARRAY') {
    $List = $ImageList;
  }
  elsif (ref $ImageList eq 'HASH') {
    $List = [values %$ImageList];
  }

  # For every element of the input image hashref, update it
  # with offset ra,dec and ccd offsets:
  foreach my $Image (@$List) {
    # Grab off set values for this CCD:
    my $ccd = $Image->{'ccd'};
    my $ra_offset = $Offsets->{$ccd}->{'raoffset'};
    my $dec_offset = $Offsets->{$ccd}->{'decoffset'};
    my $ra_hwidth = $Offsets->{$ccd}->{'rahwidth'};
    my $dec_hwidth = $Offsets->{$ccd}->{'dechwidth'};

    # Set min & max ra, dec translating from offset center
    # by CCD half widths:
    #my $scale = 1.0/cos(deg2rad($Image->{'teldec'}));
    my $dec = $Image->{'teldec'} + $dec_offset;     
    my $scale = 1.0/cos(deg2rad($dec));
    my $ra = $Image->{'telra'} + $ra_offset*$scale;
    my $maxra = $ra + $ra_hwidth*$scale;
    my $minra = $ra - $ra_hwidth*$scale;
    $maxra -= 360.0 if ($maxra>360.0);
    $minra += 360.0 if ($minra<0.0);
    my $maxdec = $dec + $dec_hwidth;
    my $mindec = $dec - $dec_hwidth;

    #print "test: ",$Image->{'dec'}, ' ', $dec_offset, ' ', $dec_hwidth, ' ',  $maxdec,"\n";
    #print "File: $Image->{'filename'}  CCD: $ccd\n";
    #print "Dec: ",$Image->{'teldec'}, " ", $dec_offset, " ", $dec_hwidth, " ",  $mindec," ", $maxdec,"\n";
    #print "RA: ",$Image->{'telra'}, " ", $ra_offset, " ", $ra_hwidth, " ",  $minra," ", $maxra," ",$scale,"\n";
    # Update Image info hashref:
    $Image->{'ra_off'} =  $ra;
    $Image->{'dec_off'} = $dec;
    $Image->{'maxra'} = $maxra;
    $Image->{'minra'} = $minra;
    $Image->{'maxdec'} = $maxdec;
    $Image->{'mindec'} = $mindec;
    $Image->{'scale'} = $scale;
  }

}



#######################################################################
#
# SUBROUTINE: getMinMax
#
# DESCRIPTION:
#
# INPUT:
#  $Args = {
#    images = {
#      {ccd, ra, dec, ...}
#    },
#    offsets = {
#      {raoffset, decoffset, rahwidth, dechwidth}
#    }
#   }
#
# OUTPUT:
#  images = {
#    {ra_off, dec_off, maxra, minra, maxdec, mindec}
#  }
#
#######################################################################
sub getMinMax {
  my $Args = shift;
  my $dir = shift;
  
  my $ImageList = $Args->{'images'} if ($Args->{'images'});

  my $List;
  if (ref $ImageList eq 'ARRAY') {
    $List = $ImageList;
  }
  elsif (ref $ImageList eq 'HASH') {
    $List = [values %$ImageList];
  }
  
  #check for existance  of get_corners routing
  if( !-f $dir."/get_corners") {
      die "could not find executable $dir/get_corners\n";
      exit 1;
  }
      

  # For every element of the input image hashref, update it
  # with offset ra,dec and ccd offsets from the get_corners routine:
  foreach my $Image (@$List) {
      # create commandline
     #print $Image->{'id'}," ",$Image->{'imagename'},"\n";
    my $cmd = "$dir/get_corners -equinox $Image->{'equinox'} -axis $Image->{'naxis1'} $Image->{'naxis2'} -ctype $Image->{'ctype1'} $Image->{'ctype2'} -crval $Image->{'crval1'} $Image->{'crval2'} -cd1 $Image->{'cd1_1'} $Image->{'cd1_2'} -cd2 $Image->{'cd2_1'} $Image->{'cd2_2'} -pv1 $Image->{'pv1_0'} $Image->{'pv1_1'} $Image->{'pv1_2'} $Image->{'pv1_3'} $Image->{'pv1_4'} $Image->{'pv1_5'} $Image->{'pv1_6'} $Image->{'pv1_7'} $Image->{'pv1_8'} $Image->{'pv1_9'} $Image->{'pv1_10'} -pv2  $Image->{'pv2_0'} $Image->{'pv2_1'} $Image->{'pv2_2'} $Image->{'pv2_3'} $Image->{'pv2_4'} $Image->{'pv2_5'} $Image->{'pv2_6'} $Image->{'pv2_7'} $Image->{'pv2_8'} $Image->{'pv2_9'} $Image->{'pv2_10'} -crpix $Image->{'crpix1'} $Image->{'crpix2'}";
    my $output=`$cmd`;
     #print $cmd,"\n";
    #print $output,"\n\n";
    my ($ra_off,$minra,$maxra,$dec_off,$mindec,$maxdec)=
        $output=~/RA:(.*) (.*) (.*) Dec:(.*) (.*) (.*)/;

    croak("Could not get ra/dec info from get_corners.\n") 
        if (!$minra  ||  !$maxra  || !$ra_off ||
            !$mindec ||  !$maxdec || !$dec_off );

    $Image->{'ra_off'} =  $ra_off;
    $Image->{'dec_off'} = $dec_off;
    $Image->{'maxra'} = $maxra;
    $Image->{'minra'} = $minra;
    $Image->{'maxdec'} = $maxdec;
    $Image->{'mindec'} = $mindec;
    $Image->{'scale'} = 1.0/cos(deg2rad($dec_off));
  }

}




#######################################################################
#
# SUBROUTINE: getTileSearchRange
#
# DESCRIPTION: 
#   Uses min/max ra/dec calculated for a particular image to 
#   determine the min/max ra/dec to search for a overlapping tile.
#
# INPUT:
#
#
# OUTPUT:
#   images = {
#     {maxra_look, minra_look, maxdec_look, mindec_look}
#    }
#
# 
#######################################################################
sub getTileSearchRange {
  my $Args = shift;

  my $MINIMUM_OVERLAP;

  my $ImageList = $Args->{'images'} if ($Args->{'images'});
  my $npix_ra = $Args->{'npix_ra'} if ($Args->{'npix_ra'});
  my $npix_dec = $Args->{'npix_dec'} if ($Args->{'npix_dec'});
  my $pixelsize = $Args->{'pixelsize'} if ($Args->{'pixelsize'});
  my $tiledata = $Args->{'tiledata'} if ($Args->{'tiledata'});

  if ($Args->{'minimum_overlap'}) {
    $MINIMUM_OVERLAP = $Args->{'minimum_overlap'};
  }
  else {
    $MINIMUM_OVERLAP = 0.5/60.0;    # 1/2 of an arcmin.
  }

  croak("Must provide npix_ra.\n") if (! $npix_ra);
  croak("Must provide npix_dec.\n") if (! $npix_dec);
  croak("Must provide pixelsize.\n") if (! $pixelsize);
  croak("Must provide arrayref of image info hash refs.\n") if (! $ImageList);

  my $List;
  if (ref $ImageList eq 'ARRAY') {
    $List = $ImageList;
  }
  elsif (ref $ImageList eq 'HASH') {
    $List = [values %$ImageList];
  }

  # Operate on a list of image hasrefs that already have min/max ra/dec and scale
  # factors defined.
  foreach my $Image (@$List) {
    my $maxra = $Image->{'maxra'};
    my $minra = $Image->{'minra'};
    my $maxdec = $Image->{'maxdec'};
    my $mindec = $Image->{'mindec'};
    my $scale = $Image->{'scale'};

    # Set min.max range to look for tile centers:
    my $maxra_look = $maxra + $scale*($npix_ra/2.0*$pixelsize - $MINIMUM_OVERLAP);
    my $minra_look = $minra - $scale*($npix_ra/2.0*$pixelsize - $MINIMUM_OVERLAP);
    my $maxdec_look = $maxdec + $npix_dec/2.0*$pixelsize - $MINIMUM_OVERLAP;
    my $mindec_look = $mindec - $npix_dec/2.0*$pixelsize + $MINIMUM_OVERLAP;
    $maxra_look -= 360.0 if ($maxra>360.0);
    $minra_look += 360.0 if ($minra<0.0);

    # Update Image Info hash with "look" ranges:
    $Image->{'maxra_look'} = $maxra_look;
    $Image->{'minra_look'} = $minra_look;
    $Image->{'maxdec_look'} = $maxdec_look;
    $Image->{'mindec_look'} = $mindec_look;
  }
  

}

#######################################################################
#
#######################################################################
sub readSWARPInputFile {
  my $File = shift;

  open (FH, "<$File") or croak("Unable to open $File\n");
  my @vals;
    
  while (<FH>) {
    if (/\S/) {
      push(@vals,[split(/\s+/)]);
    }
  }

  close FH;

  return \@vals
}


#######################################################################
#
#######################################################################
sub createSWARPInputFiles {
  my $Args = shift;

  my $output_base = 'SWARP_input';
  my $output_path = '.';
  my $List = $Args->{'images'} if ($Args->{'images'});
  $output_base = $Args->{'output_base'} if ($Args->{'output_base'});
  $output_path = $Args->{'output_path'} if ($Args->{'output_path'});

  croak("Must provide a Image Info list\n") if (! $List); 

  # Get jobids:
  my %jobids;
  foreach my $Image (@$List) {
    $jobids{$Image->{'jobnum'}} = 1;
  }

  foreach my $jobid (keys %jobids) {
    my $outfile = join('_',$output_base, $jobid);
    $outfile = join('/',$output_path,"$outfile.list");
    open FH, ">$outfile" or die "Unable to open $outfile\n";

    foreach my $Image (@$List) {
      next if ($Image->{'jobnum'} ne $jobid);
      my $TileInfo = $Image->{'tiles_info'};
      my $ccd = $Image->{'ccd'};
      my $padccd = sprintf("%0*d",2,$ccd);
      my $exposurename = $Image->{'exposurename'};
      my $inputfile = $Image->{'absfile'};

      foreach my $Tile (@$TileInfo) {
        my $TileName = $Tile->{'tilename'};
        my $PixelSize = $Tile->{'pixelsize'};
        my $ra = $Tile->{'ra'}; 
        my $dec = $Tile->{'dec'}; 
        my $npix_ra = $Tile->{'npix_ra'}; 
        my $npix_dec = $Tile->{'npix_dec'};

        #my $outputfile = join('_',$exposurename, $padccd, $TileName);
        #$outputfile .= '.fits';
        #my $abs_outputfile = $inputfile;
       # 
       # $abs_outputfile =~  s/\/[^\/]+$//;  #chop filename
       # $abs_outputfile =~  s/\/[^\/]+$//;  #chop exposurename dir
       # $abs_outputfile =~  s/\/[^\/]+$//;  #chop filetype dir
       # $abs_outputfile =  join('/',$abs_outputfile,'remap',$exposurename,$outputfile);

        print FH "$inputfile $TileName $PixelSize $ra $dec $npix_ra $npix_dec\n"; 

      }  
  
    }
    close FH;

  }

}


#######################################################################
#
#######################################################################
sub runSWARP {
  my $swarp = shift;
  my $verbose = shift;

  open (OUT, "$swarp 2>&1 |");
  while (<OUT>) {
    if (m/WARNING:/) {
      s/>//;
      warn "\nSTATUS3BEG\n" if ($verbose >= 2);
      warn $_;
      warn "STATUS3END\n\n" if ($verbose >= 2);
    }
    elsif (m/Not enough valid FITS image extensions/i) {
      print "\nSTATUS5BEG\n" if ($verbose >= 2);
      print $_;
      print "STATUS5END\n\n" if ($verbose >= 2);
    }
    else {
      print $_;
    }
  }
  close OUT;
  my $exit = $?;
  return $exit;
}


1;
