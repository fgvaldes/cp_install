#!/usr/bin/perl

use strict;
use warnings;
use Test::More tests => 1;
use Astro::FITS::CFITSIO qw (:constants);
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use lib "../../../Database/trunk/lib";
use ImageProc::DB;
use ImageProc::SCAMP qw(:all);

my $db = new ImageProc::DB();

ok($db->ping() == 1,"DB connection test");


my %exposure_list = (
  'decam-6--43-z-4' => 1,
  'decam-6--43-r-4' => 1,
  'decam-6--41-z-4' => 1,
  'decam-6--41-r-4' => 1
);

$db->getPointingData({'project' => 'DES', 'nite' => '20071011','exposure_list' => \%exposure_list});

my $pointings = getDistinctPointings(\%exposure_list);

my $detector = 'DECam';



foreach my $exp (keys %exposure_list) {
  my $edata = $exposure_list{"$exp"};
  print "exposure: $exp\n";
   while (my ($key, $val) = each %$edata) {
     print "$key = $val\n";
   }
  print "\n";
}
foreach my $exp (keys %$pointings) {
  my $edata = $pointings->{"$exp"};
  print "exposure: $exp\n";
   while (my ($key, $val) = each %$edata) {
     print "$key = $val\n";
   }
  print "\n";
}


createAstroStds( {
  'pointing_list' => $pointings,
  'detector' => $detector, 
  'catalog_table' => 'usnob_cat1',
  'dbh' => $db,
  'outfile' => 'std.fits'
});

my $status = 0;
my $hdunum;
my $nrows;
my $fits = Astro::FITS::CFITSIO::open_file('std.fits',READONLY,$status);
die "$status\n" if ($status);
$fits->get_num_hdus($hdunum,$status);
die "$status\n" if ($status);
print "nhdus: $hdunum\n";
$fits->movabs_hdu(3,BINARY_TBL,$status);
die "$status\n" if ($status);
$fits->get_num_rows($nrows, $status);
die "$status\n" if ($status);
print "nrows: $nrows\n";

my @col;
my $anynull;


$fits->read_col(TFLOAT, 6, 1, 1, $nrows, 0,\@col,$anynull,$status);
die "$status\n" if ($status);

print $col[100], "\n";

print "\n";

$fits->movabs_hdu(1,ANY_HDU,$status);
die "$status\n" if ($status);
my $head = $fits->read_header;


while ((my $k, my $v) = each %$head) {
  print "$k = $v\n";
}
