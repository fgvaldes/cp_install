#include "TileFinder.h"
/*
 * The program  detects natural crowding of observation bore sights and indicate them
 *  as tile pointing. The input list of data files and their bore sight Ra,Dec coordinates
 *  is processed to produce a new list containing information necessary for cp_remap program
 *  The accuracy of the tile positioning is rough (about +/- 0.5 degree). This can be improved
 *  if corresponding request will be given.
 *
 *   Input parameters: -input <input list of files: format <file name> <Ra> <Dec>
 *                                                        <exp.time> <seeing.fwhm> <Zero Point> >
 *                     -output <filename including path where result will be stored >
 *                     -tpfile < filename including path where tangent point grid is stored >
 *                     -verbose <verbose level 0 - 3 >
 *                     -config < file containing configuration parameters as key,value pairs >
 *                             < radius 1.0  - radius of search>
 *                             <exp_cut 0.1 - low exposure cut as fraction of maximum exposure >
 *                             <seeing_cut 2.0  - seeing level to cut from median fwhm >
 *                             <zp_cut 2.0 - zero_point level  to cut from median zero point >
 *       Note: exposures selected if higher than minimal value
 *             seeing selected if fwhm is less than maximal value
 *             zero popint selected if higher than minimal value
 *
 *   !!!      To switch off selection on a given parameter set corresponding
 *            cut to negative value.
 *
 *
 * Author: Nikolay Kuropatkin    03/23/2012
 * Modified by N. Kuropatkin     12/03/2012
 *
 */

using namespace std;

/* function to remove proper key value from list */

 void removeMember(std::vector<int>  *list, int key) {
	int i, nk =0;
	nk = list->size();
	for ( i=0; i<nk; i++) {
		if (list->operator [](i) == key) {
			list->erase(list->begin()+i);
			break;
		}
	}


}
/* string trimming functions */

 const std::string trim(const std::string& pString,
                        const std::string& pWhitespace = " \t")
 {
     const size_t beginStr = pString.find_first_not_of(pWhitespace);
     if (beginStr == std::string::npos)
     {
         // no content
         return "";
     }

     const size_t endStr = pString.find_last_not_of(pWhitespace);
     const size_t range = endStr - beginStr + 1;

     return pString.substr(beginStr, range);
 }

 const std::string reduce(const std::string& pString,
                          const std::string& pFill = " ",
                          const std::string& pWhitespace = " \t")
 {
     // trim first
     std::string result(trim(pString, pWhitespace));

     // replace sub ranges
     size_t beginSpace = result.find_first_of(pWhitespace);
     while (beginSpace != std::string::npos)
     {
         const size_t endSpace =
                         result.find_first_not_of(pWhitespace, beginSpace);
         const size_t range = endSpace - beginSpace;

         result.replace(beginSpace, range, pFill);

         const size_t newStart = beginSpace + pFill.length();
         beginSpace = result.find_first_of(pWhitespace, newStart);
     }

     return result;
 }

/* double rounding  */

double dround(double input) {

	 double val = input;
	 double res;
	 res = (double)floor(val + 0.5);
	 return res;
}

/* convert tangent point index into RA,Dec  */

skyPoint getRaDec(int index) {
	skyPoint res;
	double shiftD = 90.;
	double myind = (double)index;
	double int_part, part, dec, ra;
	double divider;
	int deci;
	deci = (int)(myind/1000.);
	dec = (double) deci;  /* drop RA from index */
	divider = dec*1000.;
	if (dec > 0.) {
	  part  = (modf(myind/divider,&int_part));
	  ra =  (part*divider);
	} else {
		ra = myind;
	}
	res.Dec = dec - shiftD;
	res.RA = ra;
	return res;
}
/*  convert Ra,Dec into tangent point index */

int getIndex(skyPoint point) {
	double shiftD = 90.;

	   int val = (int)((dround(point.Dec + shiftD))*1000. + dround(point.RA));
//	   cout<<" val="<<val<<endl;
	return val;

}

/*
 *  Search in the List closest key  to given value
 *
 */

int findPoint(vector<int>* list, int *key) {
	int res = 0;
	int n = list->size();
	int dist = 0;
	int min = 10000000;
	int t;
	int i = 0;
	int val;
	int indMin = 0;
	int indMax = n;
	int indMed = 0;
	if (n >=4) {
		indMed = n/2;


	while ((indMax - indMin) > 4 ){
		val =	list->at(indMed);

		if (val >= *key) {
			indMax = indMed;
			indMed = indMin + (indMax - indMin)/2;
		} else {
			indMin = indMed;
			indMed = indMin + (indMax - indMin) /2;
		}
	}
    	min = 10000000;
	for (i=indMin; i<=indMax; i++) {
		t = (list->at(i));
			dist = abs((t - *key));
			if (dist <= min) {
				min = dist;
				res = t;

			}
	}
	}else {
		min = 10000000;
		for (i=indMin; i<=indMax; i++) {
			t = (list->at(i));
				dist = abs((t - *key));
				if (dist <= min) {
					min = dist;
					res = t;

				}
		}
	}
	return res;
}

/*
 * Search in +/- radius square neighbor clusters and sum entries
 * then search maximum one, put it in result table and remove from search.
 * repeat search untill number of entries in remaining clusters will not be <=1
 *
 */

std::vector<Tile*>*  findTile(std::map<int,int> pointTab, std::vector<int> keysTP,std::vector<int>cand, std::map<int,TangentPoint>skyGrid, double radius){
	std::vector<Tile*> *res = new std::vector<Tile*>();
	 map<int,int>::iterator it;


	Tile *tile = NULL;
	int key,  ctInd,  selKey,  tileInd ;
	double  ra, dec,  avRa, avDec, sumTp, sumRa, sumDec;
	int i,nk, sum,max,  nsel, point;
	skyPoint sp, center, currCent;
	center.RA=0.0;  center.Dec = 0.0;


	nk = cand.size();


	/*
	 *  Loop on the table untill all tiles are selected
	 *
	 */
//	cout<<" search for tile with radius="<<radius<<" nk="<<nk<<endl;
	 while ( cand.size() > 0) {
		 nk = cand.size();
		 selKey = 0;
//
		 /* create candidate tile */
		 tile = new Tile();


		 max = 0;
		 for (i=0; i<nk; i++){    /* loop on candidates */
			 sum = 0;
			 nsel = 0;


			 ctInd = cand[i];
//			 printf(" processing tile %d \n",ctInd);

			 currCent = getRaDec(ctInd);           /* get Ra,Dec from index */
//			 printf(" current index %d  Current RA =%f\n",ctInd, currCent.RA);
             

			 for (ra = currCent.RA - radius; ra<= currCent.RA + radius; ra++) {
				 for (dec = currCent.Dec - radius; dec <= currCent.Dec + radius; dec++) {
					 sp.Dec = dec;
					 sp.RA = ra;
					 
					 if( ra < 0.) sp.RA = ra +360.;
					 if(ra >360.) sp.RA = ra - 360.;
					 if (dec <-90.) sp.Dec = -90.;
					 if (dec >90.) sp.Dec = 90.;
					 if (fabs(ra) <= 0.5) sp.RA = 0.0;

					 key = getIndex(sp);

					 it=pointTab.find(key);

					 if (it != pointTab.end() && pointTab[key] > 0) {
						// printf(" ra=%f dec=%f key=%d entries=%d\n",sp.RA,sp.Dec,key,pointTab[key]);
						 sum += pointTab[key];

						 nsel++;

					 }
				 }
			 }

			 if (sum >= max) {
				 max = sum;
				 selKey = ctInd;
				 center.RA = currCent.RA;
				 center.Dec = currCent.Dec;
//				 printf("got max %d for index %d \n",max,ctInd);
			 }

		 }                         /* end loop on cells  */
		 if (max > 1) {

			 sumRa = 0.;
			 sumDec = 0.;
			 sumTp =0.;
//			 printf("tile name DECam_%ld\n",tileInd);
			 /* now search for neighbors of the candidate and add them to array */
			 /* here we need to find weighted average as a center of the tile   */
			 /* to make it more precise we need to use actual center of pixel not from index */
//
			 for (ra = center.RA - radius; ra<= center.RA + radius; ra++) {
 				for (dec = center.Dec - radius; dec <= center.Dec + radius; dec++) {
					 sp.Dec = dec;
					 sp.RA = ra;
					 if( ra < 0.) sp.RA = ra +360.;
					 if(ra >360.) sp.RA = ra - 360.;
					 if (dec <-90.) sp.Dec = -90.;
					 if (dec >90.) sp.Dec = 90.;
					 if (fabs(ra) <= 0.5) sp.RA = 0.0;
 					 key = getIndex(sp);

 					 it = pointTab.find(key);

 					if (it != pointTab.end() ) {
 						point = pointTab[key];
//					 printf("sel Index to put is %d \n",key);
 						sumRa += ra*point;
 						sumDec += dec*point;
 						sumTp += point;
 						tile->neighbors.push_back(key);
 					    removeMember(&cand,key);
 						nsel++;

 					}
 				}
			 }
			 avRa = sumRa/sumTp;
			 avDec = sumDec/sumTp;
			 sp.Dec =avDec;
			 sp.RA = avRa;
			 selKey = getIndex(sp);
			 tileInd = findPoint(&keysTP,&selKey);
			 /* find real Ra,Dec for the tangent point */

			 tile->raC = skyGrid[tileInd].Ra;
			 tile->decC = skyGrid[tileInd].Dec;
			 tile->tileID = tileInd;
			 sprintf(tile->name,"%s",skyGrid[tileInd].name_);
			 tile->nP = max;
			 res->push_back(tile);
		 } else {							/* just remove those unused entries */


			 for (ra = center.RA - radius; ra<= center.RA + radius; ra++) {
 				for (dec = center.Dec - radius; dec <= center.Dec + radius; dec++) {
					 sp.Dec = dec;
					 sp.RA = ra;
					 
					 if( ra < 0.) sp.RA = ra +360.;
					 if(ra >360.) sp.RA = ra - 360.;
					 if (dec <-90.) sp.Dec = -90.;
					 if (dec >90.) sp.Dec = 90.;
					 if (fabs(ra) <= 0.5) sp.RA = 0.0;
 					key = getIndex(sp);
 					it = pointTab.find(key);

 					if (it != pointTab.end() ){


 						removeMember(&cand,key);
 						nsel++;

 					}
 				}
			 }
			 delete(tile);

		 }

	 }  /* end of while loop */

//	cout<<"found tile "<<endl;
	return res;
}




/* search for tile that contain given cell index */

Tile* getTile(std::vector<Tile*> * tiles, int index){
	Tile * tile=NULL;
	int ntiles = tiles->size();
	int found = 0;
	int i, k, ncells;

	for (i=0; i<ntiles; i++) {
		found = 0;
		tile = tiles->operator [](i);
		ncells = tile->neighbors.size();
		for (k=0; k<ncells; k++) {
			if (index == tile->neighbors[k]) {
				found = 1;
				break;
			}
		}
		if (found) break;
	}
	if (!found) tile = NULL;
	return tile;
}
 skyPoint getRaDecFromIndex(std::vector<Tile*> *tiles, int index) {
	 skyPoint res;
		res.RA = -1.0;
		res.Dec = -100.0;
		Tile * tile=NULL;
		int ntiles = tiles->size();
		int found = 0;
		int i, k, ncells;
		for (i=0; i<ntiles; i++) {
			found = 0;
			tile = (Tile*) tiles->operator [](i);
			ncells = tile->neighbors.size();
			for (k=0; k<ncells; k++) {
				if (index == tile->neighbors[k]) {
//					printf(" index= %lf tile index= %lf \n",index,*(double*)g_array_index(tile->neighbors,double*,k));
					res.RA = tile->raC;
					res.Dec = tile->decC;
//					printf(" res RA= %f Dec= %f \n",res.RA, res.Dec);
					found = 1;
					break;
				}
			}
			if (found) break;
		}
//		printf("res RA= %f Dec= %f \n",res.RA, res.Dec);
		return res;
 }

  static const char *svn_id = "$Id: TileFinder.cpp 14768 2013-09-26 15:56:21Z kuropat $";

  int main(int argc, char *argv[]) {

    	 	std::map <int, TangentPoint> skyGrid;
    	 	map<int,TangentPoint>::iterator it;
    	 	pair<map<int,TangentPoint>::iterator,bool> ret;
    	 	std::vector<int> keysTP;
    	 	vector<int>::iterator itv;
    	 	std::vector<int> cand;
    	 	vector<int>::iterator cit;
    	 	std::vector<Tile*> *tiles;
    	 	vector<int>::iterator tit;
    	 	vector<double> exptms;
    	 	vector<double> fwhms;
    	 	vector<double> zps;
    	 	vector<skyPoint*> bss;
    	 int N, Ntp,i, flag_verbose=2,flag_input=0,flag_tpfile=1, config_flag=1;
    	 double exp_cut = 0.1, seeing_cut = 0.5, zp_cut = 0.5;
    	 double exptime, fwhm, zp, sum_bright, sum_faint;
    	 double expmax = -1.0, zpmed = 0.0,fwhmmed = 0.0;
     	 double exp_min_cut,fwhm_max_cut,zp_min_cut;
    	 int flag_outpath=1;
    	 int npoint;
    	 int newKey;
    	 double radius=2.0;
    	 int val;

    	 double PixSize = 0.27;
    	 skyPoint  pointing;
    	 Tile* tile;

    	 FILE *tpfile, *pip,*fin, *fout, *conf;
    	 char command[1000], tpfilename[1000],configfile[1000],inpline[100],event[2000],inlist[1000],
    	 outfile[1000],inputimage[1000], par_name[50],par_val[50];
    	 float RA,Dec, remapRA, remapDec;
    	  sprintf(tpfilename,"tangent_grid_test.txt");

	  printf("Version: %s \n",svn_id);

    	  if (argc<2) {
    	    printf("Usage: %s <required inputs> \n",argv[0]);
    	    printf("    Required Inputs:\n");
    	    printf("       -input <list (format: FileNameIncluding Path RA  Dec>  \n");
    	    printf("       -tpfile <tpfilename including path>  \n");
    	    printf("       -output <output file including path>  \n");
    	    printf("       -config <config file name including path>  \n");
    	    printf("       -verbose <verbose level 0 - 3 \n");
    	    printf("\n");
    	        exit(0);
    	  }


    	  // Process command line
    	  for (i=2;i<argc;i++) {
    	    if (!strcmp(argv[i],"-verbose")) {
    	      sscanf(argv[++i],"%d",&flag_verbose);
    	      if (flag_verbose<0 || flag_verbose>3) {
    	        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
    	        flag_verbose=2;
    	      }
    	    }
    	  }
    	  // now go through again
    	  for(i=1;i<argc;i++) {
    	    if (!strcmp(argv[i],"-input")) {
    	      flag_input=1;
    	      sscanf(argv[i+1],"%s",inlist);

    	    }


    	    if (!strcmp(argv[i],"-tpfile")) {
    	      flag_tpfile=1;
    	      sscanf(argv[i+1],"%s",tpfilename);
    	    }

    	    if (!strcmp(argv[i],"-output")) {
    	      flag_outpath=1;
    	      sscanf(argv[i+1],"%s",outfile);

    	    }

    	    if (!strcmp(argv[i],"-config")) {
    	    	sscanf(argv[i+1],"%s",configfile);
//    	    	radius = atof(argv[i+1]);

  	    }

  	  }
    	    // quit if required inputs are not set
    	    if(!flag_input || !flag_tpfile || !flag_outpath ) {
    	      printf("Required inputs are not set \n");

    	      exit(0);
    	    }
    	    if (!config_flag) {
    	    	cout<<"TileFinder -- configuration file is not set using default values "<<endl;
    	    }
    	    /* read config file to get selection parameters   */
    	    conf=fopen(configfile,"r");
    	        	  if (conf==NULL) {
    	        		  printf("File %s not found  using default selection parameters\n",configfile);
    	        		  printf(" radius=%lf : exposure cut= %lf : seeing cut= %lf : zero point cut= %lf \n",
    	        				  radius, exp_cut, seeing_cut, zp_cut);

    	        	    exit(0);
    	        	  }

    	     while (fgets ( inpline, sizeof inpline, conf ) != NULL) {

    	      if (!strncmp((char *)&inpline,"#",1)){
    	       	printf("Skip comment \n");
    	       	continue;
    	       }    // skip comments
    	      sscanf(inpline,"%s %s ",par_name, par_val);
	      
    	      string par = trim(par_name);
    	      if (!par.compare("radius")) {
    	    	  radius = atof(par_val);
    	      }
    	      if (!par.compare("exp_cut")) {
    	    	  exp_cut = atof(par_val);
    	      }
    	      if (!par.compare("seeing_cut")) {
    	    	  seeing_cut = atof(par_val);
    	      }
    	      if (!par.compare("zp_cut")) {
    	    	  zp_cut = atof(par_val);
    	      }
    	     }
    	  fclose(conf);
    	  if (flag_verbose >= 2) {
    		  cout<<"TileFinder using selection parameters: radius="<<radius<<" : exposure_cut="<<exp_cut;
    		  cout<<" : seeing_cut="<<seeing_cut<<" : Zero_Point_cut ="<<zp_cut<<endl;
    	  }
    	  // *** Find out the number of lines from tangent points file ***
    	  sprintf(command,"wc -l %s",tpfilename);
    	  pip=popen(command,"r");
    	  if (pip==NULL) {
    	    printf("Pipe failed: %s ",command);

    	    exit(0);
    	  }
    	  fscanf(pip,"%d",&Ntp);
    	  pclose(pip);
          cout<<" number of points in tangent points file is:"<<Ntp<<endl;

    	  //    read the tangent points file

    	  tpfile=fopen(tpfilename,"r");
	  printf("tpfilename=%s\n",tpfilename);
    	  if (tpfile==NULL) {
    		  printf("File %s not found \n",tpfilename);

    	    exit(0);
    	  }
    	  i = 0;

    	//
    	  cout<<"start reading tangent points"<<endl;
    	  TangentPoint tp[Ntp];


    	   while (fgets ( inpline, sizeof inpline, tpfile ) != NULL) {

    		if (!strncmp((char *)&inpline,"#",1)){
    			printf("Skip comment \n");
    			continue;
    		}    // skip comments
    		double Ra=0., Dec=0.;
    		char name[50];
    		sscanf(inpline,"%s %lf %lf",name, &Ra, &Dec);
    		tp[i].set(name,Ra,Dec);
//    		tp[i].print();
    		 pointing.RA = tp[i].Ra;
    		 pointing.Dec = tp[i].Dec;
    		 val =getIndex(pointing);
    		skyGrid.insert(pair<int,TangentPoint>(val,tp[i]));
    		keysTP.push_back(val);                                    // sorted by construction
    		i++;
    		if (i >=Ntp ) break;
    	  }
    	  fclose(tpfile);
          printf("Ntp=%d\n",Ntp);
//    	  cout<<"sorting Keys"<<endl;
    	  sort (keysTP.begin(), keysTP.end());

//

    	  //
    	  // Now we have tangent points let read input file and build
    	  //  hash with tangent point keys and number of pointings in each
    	  //
  	    // *** Find out the number of lines from input file ***
  	    sprintf(command,"wc -l %s",inlist);
  	    pip=popen(command,"r");
  	    if (pip==NULL) {
  	    	printf("Pipe failed: %s \n",command);
  	      exit(0);
  	    }
  	    fscanf(pip,"%d",&N);
  	    pclose(pip);
  	  printf("Number of images in %s = %d \n",inlist,N);
  	  // *** Input data  ***
  	  fin=fopen(inlist,"r");
  	  if (fin==NULL) {
    	    printf("File %s not found \n",inlist);
  	    exit(0);
  	  }
 	  for(i=0;i<N;i++) {
 	    fscanf(fin,"%s %f %f %lf %lf %lf",inputimage,&RA,&Dec,&exptime,&fwhm,&zp);
  	   exptms.push_back(exptime);
  	   fwhms.push_back(fwhm);
  	   zps.push_back(zp);
  	   skyPoint *spi = new skyPoint();
  	   spi->RA = RA;
  	   spi->Dec = Dec;
  	   bss.push_back(spi);
 	  }

 	  /*
 	   * here we will calculate parameter limits to select usefull data
 	   */


 	  for (i=0; i< N; i++) {
 		  zpmed += zps[i];
 		  fwhmmed += fwhms[i];
 		  if(expmax <= exptms[i]) {
 			  expmax = exptms[i];
 		  }
 	  }
 	  fwhmmed /= (double)N;  // Calculated mediand in seeing and zp
 	  zpmed /= (double)N;
 	  if (flag_verbose >= 2) cout<<"exp time max ="<<expmax<<" fwhmmed="<<fwhmmed<<" zpmed="<<zpmed<<" vectlength="<<zps.size()<<endl;
 	  exp_min_cut = exp_cut*expmax;
 	  fwhm_max_cut = 2.0*fwhmmed  - seeing_cut*fwhmmed;
 	  zp_min_cut = zp_cut*zpmed;
 	  if (flag_verbose >= 2) cout<<"exp_cut="<<exp_min_cut<<" seeing cut="<<fwhm_max_cut<<" zp cut="<<zp_min_cut<<endl;

 	  fclose(fin);

 	  /*
 	   * calculate sum of faint and bright images
 	   */
 	  sum_faint = 0.0;
 	  sum_bright = 0.0;
 	  for (i=0; i<N; i++) {

 		 if (exptms[i] <= exp_min_cut ) {
 			 sum_faint += exptms[i];
 		 } else {
 			 sum_bright += exptms[i];
 		 }
 	  }
 	  if (flag_verbose >= 2) cout<<"TileFinder sum of faint exposures ="<<sum_faint<<" vs sum of bright ="<<sum_bright<<endl;

  	  // create map with tp index key and number of pointings in it
     	std::map<int,int> pointTab; // 
	 	map<int,int>::iterator itpT;
	 	std::pair<map<int,int>::iterator,bool> retTp;
//
//	cout<<" Creating table of tiles and entries in them "<<endl;
     	int first = 1;
     	for(i=0;i<N;i++) {
 		  val = getIndex(*bss[i]);
 		  newKey = findPoint((std::vector<int>*) &keysTP, &val);
//		  cout<<" created index="<<val<<" newKey="<<newKey<<endl;
		//
		// selection
		//
	  if ((exp_min_cut <0. || exptms[i]>= exp_min_cut) &&
              (fwhm_max_cut <0. || fwhms[i]<= fwhm_max_cut) &&
	      (zp_min_cut < 0. || zps[i] >= zp_min_cut)) {
                  
     		  if (first == 1){
     			  npoint = 1;
     			  pointTab.insert(pair<int,int>(newKey,npoint));
//     	   	   printf(" insert first key %d  key= %d \n",npoint, newKey );
     			  first = 0;
     			  continue;
     		  }



//     	  	  	npoint = pointTab[newKey];

     		  if (pointTab.count(newKey) == 0) {
     			  npoint = 1;
     			  pointTab.insert(pair<int,int>(newKey,npoint));
//     	    	printf(" strat insertion in table %d  key= %d \n",npoint, newKey );
     		  } else {
     			  npoint = pointTab[newKey];
     			  npoint++;
     			  itpT=pointTab.find(newKey);
     			  pointTab.erase(itpT);
     			  pointTab.insert(pair<int,int>(newKey,npoint));
//    	    	printf("Increment tab value %d for key= %d \n",npoint,newKey);


     		  }
        	 }
     	  }



     	 for ( itpT=pointTab.begin() ; itpT != pointTab.end(); itpT++ ) {
     		cand.push_back(itpT->first);
     	 }
     	 sort (cand.begin(), cand.end());
     	  // Now we have a table with pixel numbers and number of pointings in each
     	  //  lets find clusters of given raduis starting from most significant one
     	  //
     	  tiles = findTile(pointTab,keysTP,cand, skyGrid, radius);

     	  //
     	  //  Now reopen the input file read entries find tile number it belongs to
     	  //  and write output file with RA,Dec of the remapping pointing
     	  //  that will be input for cp_remap.c
     	  //
     	  //

     	 fin=fopen(inlist,"r");
    	 if (fin==NULL) {
    		 cout<<"File "<<inlist<<" open failed"<<endl;
    	    exit(-1);
    	 }

     	 fout = fopen(outfile,"w");
     	 if (fout==NULL) {
     		 cout<<"File "<<outfile<<" open failed"<<endl;
     	    exit(-1);
     	 }
     	for(i=0;i< N;i++) {
     		fscanf(fin,"%s %f %f %lf %lf %lf",inputimage,&RA,&Dec,&exptime,&fwhm, &zp);
//     	 printf("processing %s %f %f %6.1f %6.2f %8.3f  \n",inputimage,RA,Dec,exptime,fwhm,zp);

     		 pointing.RA = RA;
     		 pointing.Dec = Dec;
     		 int index = getIndex(pointing);
//     	 printf("index %d \n",index);
     		 newKey = findPoint(&keysTP,&index);
//     	 printf("newKey %d \n",newKey);
//
     		 tile = getTile(tiles, newKey);
//     	 printf("tile %d \n",tile->tileID);
     		 if (tile != NULL) {
     			 remapRA = tile->raC;
     			 remapDec = tile->decC;
     			 fprintf(fout,"%s %s %f %f %f \n",inputimage,tile->name, PixSize, remapRA, remapDec);
     		 } else {
     			 printf("file %s has no associated tile, skipping \n",inputimage);
     		 }

     	}
     	fclose(fin);
     	fclose(fout);
     	size_t l = 0;
    	  // free memory
     	  for(l=0; l< tiles->size(); l++) {
     		  delete(tiles->operator [](l));
     	  }


    	  delete(tiles);
    	  return 0;
}

