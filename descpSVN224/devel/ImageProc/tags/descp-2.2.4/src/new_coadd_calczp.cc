#include "TSystem.h"
#include "TLine.h"
#include <fstream>
#include "TMatrixD.h"
#include "Object.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TCut.h"
#include "FitSLR.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TMath.h"
#include <string>
#include <iostream>
#include "TDirectory.h"
#include "TEventList.h"
#include "TFile.h"
#include "TH2D.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TGraph.h"
#include "TLatex.h"
#include "TDirectory.h"
#include "TSQLServer.h"
#include "TSQLStatement.h"
#include "TOracleStatement.h"
#include "ExtinctionMap.h"
#include "kdtree++/kdtree.hpp"
#include "TDecompSVD.h"
#include "TVector.h"
using namespace std;

void LoadStyle();
void report_evt(bool verbose,string type,int level,string mess);
void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,double &tol);
void PrintSLRHelp();
typedef KDTree::KDTree<3,Object> KD_Tree;
typedef std::pair<KD_Tree::const_iterator,double> KD_Match;

struct ObjPair{

  int id1;
  int id2;
  Object obj1;
  Object obj2;
};

struct ObjTri{

  Object obj1;
  Object obj2;
  Object obj3;
};

class Exposure{
public:
  
  Exposure() {}
  bool operator==(const Exposure& other) const {
    if( name==other.name && band==other.band)
      return true;
    else return false;
  }
  string name;
  string band;
  string run;
  int ccd;
  double skybrite;

};

struct Tile{

  string name;
  string run;
  double ra;
  double dec;
  double pix_ra;
  double pix_dec;
  double pixelsize;
  
};





class ImagePair{
public:
  ImagePair():id1(-999),id2(-999),
              mean(-999),rms(-999) {}
  ImagePair(int _id1,int _id2):id1(_id1),id2(_id2),
                               mean(-999),rms(-999) {}
  
  bool operator==(const ImagePair& other) const {
    if( (id1==other.id1 && id2==other.id2) ||
        (id1==other.id2 && id2==other.id1)) return true;
    else return false;
  }
        
   int id1;
   int id2;
   double mean;
   double rms;
   vector<ObjPair> matches;

};



class ExpPair{
public:
  ExpPair():exp1(""),exp2(""),
    mean(-999),rms(-999) {}
  ExpPair(string _exp1,string _exp2):exp1(_exp1),exp2(_exp2),
                                       mean(-999),rms(-999) {}
  
  bool operator==(const ExpPair& other) const {
    if( (exp1==other.exp1 && exp2==other.exp2) ||
        (exp1==other.exp2 && exp2==other.exp1)) return true;
    else return false;
  }
   
   string exp1;
   string exp2;
   double mean;
   double rms;
   vector<ImagePair> matches;
};



void Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters = " ");



int main(int argc, char*argv[])
{
    LoadStyle();
    TStopwatch timer;
    TStopwatch timer_db;

    // really bad way of creating dummy pointer to pointer
    char**argv_dummy;
    char char_dummy='p';
    char *char_dummy2=&char_dummy;
    argv_dummy=&char_dummy2;

    int int_dummy=1;
    //TApplication app("app",&int_dummy,argv_dummy);

    string project="BCS";
    string detector="Mosiac2";
    string tile="";
    string color_datafile("/home/rearmstr/slr_data/covey_extinction.root");
    string mag="auto";
    double err_cut=0.1;
    double min_err=0.000001;
    string zpdiff_filename;
    double svd_tol=1e-6;
    double cut_range=-1;
    double min_sky_error=0.01;
    bool extinction=false;
    bool applycolor=false;
    bool use_exp_ave=false;
    int debug=0;
    string zp_file;
    string bcs_zpdir;
    double tol=2;
    int min_pair=10;
    double star_cut_min=0;
    double star_cut_max=0.1;
    bool use_clip=true;
    bool use_weighted=false;
    string exp;
    double thresh=2.5;
    int max_iter=1000;
    int max_image=-1;
    double clip_tol=1e-6;
    bool use_weighted_mean=false;
    bool neighbor=false;
    bool more_neighbor=false;
    double max_fwhm=-1;
    double max_mag=100;
    double min_mag=0;
    double max_rms=10;
    double max_mag_diff=6.25; 
    int flag_cut=3;
    string rootfile="";
    string debugfile="";
    string band;
    bool insert_db=false;
    string star_sel="spread";
    double zp_max=100;
    double zp_min=-100;
    bool use_best=false;
    bool use_same_exp=false;
    bool use_rel=false;
    bool use_psm=false;
    bool use_sky=false;
    bool use_missing_ccd=false;
    bool compare_psm=false;
    bool add_missing=false;
    string which_db="fm";
    string nite_list;
    string run_list;
    string object_table="objects";
    bool use_object_current=false;
    bool remove_zp=false;
    double min_exptime=0;
    bool no_photo_req=false;
    bool psm_cal=false;
    double psm_sys=0;
    bool scale_rms=false;
    int use_ccd=-1;
    bool remove_solo=false;
    if(argc==1) {
      exit(1);
    }
    
    for(int ii=1;ii<argc;ii++) {
      string arg=argv[ii];
      if(arg=="-tile")            {tile=argv[ii+1];ii++;}
      else if(arg=="-project")        {project=argv[ii+1];ii++;}
      else if(arg=="-extinction") {extinction=true;}
      else if(arg=="-compare_psm") {compare_psm=true;}
      else if(arg=="-debug")      {debug=atoi(argv[ii+1]);ii++;}
      else if(arg=="-color")      {applycolor=true;}
      else if(arg=="-psm_cal")      {psm_cal=true;}
      else if(arg=="-psm_sys")      {psm_sys=atof(argv[ii+1]);ii++;}
      else if(arg=="-use_exp_ave")      {use_exp_ave=true;}
      else if(arg=="-use_rel")      {use_rel=true;}
      else if(arg=="-obj_current")      {use_object_current=true;}
      else if(arg=="-object_table")      {object_table=argv[ii+1];ii++;}
      else if(arg=="-use_psm")      {use_psm=true;}
      else if(arg=="-scale_rms")      {scale_rms=true;}
      else if(arg=="-use_sky")      {use_sky=true;}
      else if(arg=="-use_same_exp")      {use_same_exp=true;}
      else if(arg=="-use_miss_ccd")      {use_missing_ccd=true;}
      else if(arg=="-add_missing")      {add_missing=true;}
      else if(arg=="-star_sel")      {star_sel=argv[ii+1];ii++;}
      else if(arg=="-use_best")      {use_best=true;}
      else if(arg=="-weighted_mean") {use_weighted_mean=true;}
      else if(arg=="-weighted_fit") {use_weighted=true;}
      else if(arg=="-weighted") {use_weighted_mean=true;use_weighted=true;}
      else if(arg=="-neighbor")   {neighbor=true;}
      else if(arg=="-more_neighbor") {more_neighbor=true;}
      else if(arg=="-no_photo_req") {no_photo_req=true;}
      else if(arg=="-mag")        {mag=argv[ii+1];ii++;}
      else if(arg=="-band")        {band=argv[ii+1];ii++;}
      else if(arg=="-ccd")        {use_ccd=atoi(argv[ii+1]);ii++;}
      else if(arg=="-exp")        {exp=argv[ii+1];ii++;}
      else if(arg=="-star_cut")   {
         star_cut_min=atof(argv[ii+1]);
         star_cut_max=atof(argv[ii+2]);
         ii+=2;
      }
      else if(arg=="-zp_range")   {
         zp_min=atof(argv[ii+1]);
         zp_max=atof(argv[ii+2]);
         ii+=2;
      }
      else if(arg=="-fwhm_cut")   {max_fwhm=atof(argv[ii+1]);ii++;}
      else if(arg=="-max_rms")   {max_rms=atof(argv[ii+1]);ii++;}
      else if(arg=="-max_mag")   {max_mag=atof(argv[ii+1]);ii++;}
      else if(arg=="-min_mag")   {min_mag=atof(argv[ii+1]);ii++;}
      else if(arg=="-mag_range")   {
        min_mag=atof(argv[ii+1]);
        max_mag=atof(argv[ii+2]);
        ii+=2;
      }
      else if(arg=="-min_exptime")   {min_exptime=atof(argv[ii+1]);ii++;}
      else if(arg=="-max_image")   {max_image=atoi(argv[ii+1]);ii++;}
      else if(arg=="-max_iter")   {max_iter=atoi(argv[ii+1]);ii++;}
      else if(arg=="-min_pair")   {min_pair=atoi(argv[ii+1]);ii++;}
      else if(arg=="-flag_cut")   {flag_cut=atoi(argv[ii+1]);ii++;}
      //else if(arg=="-max_matches")   {MaxMatches=atoi(argv[ii+1]);ii++;}
      else if(arg=="-tol")        {tol=atof(argv[ii+1]);ii++;}
      else if(arg=="-svd_tol")    {svd_tol=atof(argv[ii+1]);ii++;}
      else if(arg=="-err_cut")    {err_cut=atof(argv[ii+1]);ii++;}
      else if(arg=="-min_err")    {min_err=atof(argv[ii+1]);ii++;}
      else if(arg=="-rootfile")    {rootfile=argv[ii+1];ii++;}
      else if(arg=="-debugfile")    {debugfile=argv[ii+1];ii++;}
      else if(arg=="-nite")    {nite_list=argv[ii+1];ii++;}
      else if(arg=="-run")    {run_list=argv[ii+1];ii++;}
      else if(arg=="-db")    {which_db=argv[ii+1];ii++;}
      else if(arg=="-zpdiff_file")    {zpdiff_filename=argv[ii+1];ii++;}
      else if(arg=="-zp_file")    {zp_file=argv[ii+1];ii++;}
      else if(arg=="-bcs_zpdir")    {bcs_zpdir=argv[ii+1];ii++;}
      else if(arg=="-insert_db")      {insert_db=true;}
      else if(arg=="-rm_nomatches")      {remove_solo=true;}
      else {
        cerr<<"Not a valid command line entry: "<<arg<<endl;
        exit(1);
      }
    }
    if(compare_psm && rootfile.empty()) rootfile="out.root";
    if(tile.empty()) {
      cout<<"Must give a valid tilename"<<endl;
    }

    if(project=="DES") {
      
    }

    if(star_sel!="spread" && star_sel !="class") {
      cout<<"Not a valid star selector: "<<star_sel<<endl;
      cout<<"Must be either 'spread' or 'class'"<<endl;
      exit(1);

    }
    
    if(use_object_current) object_table="objects_current";
    //if(project=="SPT")remove_zp=true;
    remove_zp=true;
    //else if(project=="BCS" && !use_psm) remove_zp=true;

    vector<string> match_exp;
    if(!exp.empty()) {
      Tokenize(exp,match_exp,",");
    }

    vector<string> nites;
    if(!nite_list.empty()) {
      Tokenize(nite_list,nites,",");
    }

    vector<string> runs;
    if(!run_list.empty()) {
      Tokenize(run_list,runs,",");
    }



    tol*=1./3600*TMath::DegToRad();
    
    
    int N_ccd;
    if(detector=="Mosiac2") N_ccd=8;
    else if(detector=="DECam") N_ccd=62;

    
    ExtinctionMap ExtMap("/home/zenteno/REDSHIFT_SURVEYS/EXTINCTION/MAPS_FITS/");
    

    TSQLServer *db;// = TSQLServer::Connect("oracle://decam-devdb.fnal.gov/fmdes",
    //                    "pipeline", "dc01user");
    string db_cmd;
    TString db_string;
    string db_user="pipeline";
    string db_passwd="dc01user";

    if(which_db=="fm") {
      db_string="oracle://decam-devdb.fnal.gov/fmdes";
    }
    else if(which_db=="pr") {
      db_string="oracle://desdb/prdes";
    }
    else if(which_db=="st") {
      db_string="oracle://desdb/stdes";
    }
    else if(which_db=="pg") {
      db_string="pgsql://desproc.cosmology.illinois.edu/descp";
      db_user="yyang";
      db_passwd="Yyang0084";
    }
    TSQLStatement *stmt;
    
    
    
    if(debug)cout<<"Processing "<<tile<<" "<<endl<<endl;
    
    string tile_string("");
    vector<Tile> tile_neighbors;
    // add to the list of neighboring tiles
    if(neighbor || more_neighbor) {
      db = TSQLServer::Connect(db_string,db_user.c_str(), db_passwd.c_str());
      double tilebuff=0.8;

      if(more_neighbor) tilebuff*=2;

      db_cmd=Form("select ra,dec from coaddtile where "
                  " project='%s' and tilename='%s'",
                  project.c_str(),tile.c_str());
      if(debug>2) cout<<db_cmd<<endl;
      
      stmt=db->Statement(db_cmd.c_str());
      
      stmt->Process();
      stmt->StoreResult();
      stmt->NextResultRow();
      double tile_ra,tile_dec;
      tile_ra=stmt->GetDouble(0);
      tile_dec=stmt->GetDouble(1);


      double ra_min=tile_ra-tilebuff/cos(tile_dec*TMath::DegToRad());
      double ra_max=tile_ra+tilebuff/cos(tile_dec*TMath::DegToRad());
      double dec_min=tile_dec-tilebuff;
      double dec_max=tile_dec+tilebuff;
      db_cmd=Form("select tilename,ra,dec,npix_ra,npix_dec,pixelsize "
                  "from coaddtile where project='%s' and "
                  "(ra between %f and %f) and "
                  "(dec between %f and %f)",
                  project.c_str(),ra_min,ra_max,dec_min,dec_max);
      if(debug>2) cout<<db_cmd<<endl;
      stmt=db->Statement(db_cmd.c_str());
      
      // Check for errors in the statement
      if(!stmt->Process()) {
        cout<<stmt->GetErrorMsg()<<endl;
        exit(1);
      }
      stmt->StoreResult();

      
      if(debug)cout<<"Using the following neighboring tiles"<<endl;

      
      while(stmt->NextResultRow()) {

        Tile neighbor;
        neighbor.name=stmt->GetString(0);
        neighbor.ra=stmt->GetDouble(1);
        neighbor.dec=stmt->GetDouble(2);
        neighbor.pix_ra=stmt->GetDouble(3);
        neighbor.pix_dec=stmt->GetDouble(4);
        neighbor.pixelsize=stmt->GetDouble(5);
        tile_neighbors.push_back(neighbor);
        
        
      }
      
      // put the first tile into the search string
      tile_string+=" and ( b.tilename='"+tile+"";      
      vector<Tile>::iterator tile_iter=tile_neighbors.begin();
      for(; tile_iter!=tile_neighbors.end(); ++tile_iter) {
        tile_string+=" or b.tilename='"+tile_iter->name ;
      }
      tile_string+=") ";
      if(debug)cout<<endl;
      delete stmt;
      db->Close();

    }
    else {
      tile_string+=" and (b.tilename='"+tile+"') ";
    }

    

    // Get a list of red objects that correspond to this
    // tile through the remap parentid. Must account
    // the posibility of more than one run per
    
    //build tile string
    if(neighbor||more_neighbor){

      tile_string=" and (a.tilename='"+tile+"'";
      vector<Tile>::iterator tile_iter=tile_neighbors.begin();
      for(; tile_iter!=tile_neighbors.end(); ++tile_iter) {
        tile_string+=" or a.tilename='"+tile_iter->name+"'";
      }
      tile_string+=")";
    }
    else tile_string=" and a.tilename='"+tile+"' ";

    db = TSQLServer::Connect(db_string,
                             db_user.c_str(), db_passwd.c_str());
    
    db_cmd=Form("select a.parentid, a.ccd, a.band, c.exposurename, "
                "a.nite, c.run, a.exptime, a.fwhm ,a.skybrite, a.ra, "
                "a.dec,b.photflag from "
                "image a, exposure b,location c where a.project='%s' and "
                "a.imagetype='remap' and a.id=c.id and a.band='%s' and "
                "a.exposureid=b.id"
                " and regexp_like(archivesites,'[^N]')",
                project.c_str(),band.c_str());
    db_cmd+=tile_string;


    if(!nite_list.empty()) {
      
      string nite_string="and a.nite in (";
      vector<string>::iterator st_iter=nites.begin();
      for(; st_iter!=nites.end(); ++st_iter) {
        nite_string+="'"+*st_iter+"',";
      }
      // delete last comma
      nite_string.erase(nite_string.length()-1);
      nite_string+=") ";
      db_cmd+=nite_string;
    } 
    cout<<run_list<<endl;
     if(!run_list.empty()) {
      
      string run_string="and (";
      vector<string>::iterator st_iter=runs.begin();
      for(; st_iter!=runs.end(); ++st_iter) {
        run_string+="a.run like '"+*st_iter+"%' or ";
      }
      // delete last or
      run_string.erase(run_string.length()-3);
      run_string+=") ";
      db_cmd+=run_string;
    } 



     if(debug>2) cout<<db_cmd<<endl;
     
     stmt=db->Statement(db_cmd.c_str());
    
     // Check for errors in the statement
     if(!stmt->Process()) {
       cout<<stmt->GetErrorMsg()<<endl;
       exit(1);
     }
     stmt->StoreResult();
     

     map<int,ImageInfo> ImageList,ZeroList;

     int dups=0;
    int ccd,nite,id;                                   
    string exp_name,im_run,im_band;
    double fwhm,skybrite,exp_time;
    int photflag;
    while(stmt->NextResultRow()) {

      if(max_image>0 && ImageList.size()>max_image) continue;

      id=stmt->GetInt(0);
      ccd=stmt->GetInt(1);
      im_band=stmt->GetString(2);
      exp_name=stmt->GetString(3);
      nite=atoi(stmt->GetString(4));
      im_run=stmt->GetString(5);
      exp_time=stmt->GetDouble(6);
      fwhm=stmt->GetDouble(7);
      skybrite=stmt->GetDouble(8);
      photflag=stmt->GetInt(9);
      

      if( fwhm > max_fwhm && max_fwhm>0) {
        if(debug>2) cout<<" image "<<id<<" will not be used: FWHM="<<fwhm<<endl;
        continue;
      }

      if( exp_time <min_exptime) {
        if(debug>2)cout<<" image "<<id<<" will not be used: exptime to low="
                       <<exp_time<<endl;
        continue;
      }

      if(match_exp.size()>0) {
        bool match=false;
        for(int i=0;i<match_exp.size();i++) {
          if(((TString)exp_name).BeginsWith(match_exp[i].c_str())) match=true;
        }
        if(!match) continue;
      }

      //if(use_ccd>0) {
      //  if(ccd!=use_ccd) continue;
        
      // }

      ImageInfo im(id,exp_name,exp_time,ccd,nite,im_run,fwhm,skybrite);
      im.band=im_band;
      im.photometric=photflag;
      // Check to see if we already have this particular nite/exposure
      // if multiple entries use the latest one.
      map<int,ImageInfo>::iterator iter=ImageList.begin();;

      bool insert=true;
      while (iter!=ImageList.end()) {
         
        if(exp_name==(*iter).second.exp_name && nite==(*iter).second.nite &&
           ccd==(*iter).second.ccd ) {

          if(id>(*iter).second.id) {
            ImageList.erase(iter++);
          }
          else {
            insert=false;
            break;
          }
        }
        else ++iter;
      }
      

      if( fwhm > max_fwhm && max_fwhm>0) {
        if(debug>2) cout<<" image "<<id<<" will not be used: FWHM="<<fwhm<<endl;
        if(insert) ZeroList[id]=im;
      }

      else  if( exp_time <min_exptime) {
        if(debug>2)cout<<" image "<<id<<" will not be used: exptime to low="
                       <<exp_time<<endl;
        if(insert) ZeroList[id]=im;
      }

      else if(match_exp.size()>0) {
        bool match=false;
        for(int i=0;i<match_exp.size();i++) {
          if(((TString)exp_name).BeginsWith(match_exp[i].c_str())) match=true;
        }
        if(!match && insert) {
          if(debug>2)cout<<" image "<<id<<" will not be used: not a valid exposure, "
                         <<" must match: "<<exp_name<<endl;
          ZeroList[id]=im;
        }
          
      }

      else if(use_ccd>0) {
        if(insert && ccd!=use_ccd){
          if(debug>2)cout<<" image "<<id<<" will not be used: not a valid ccd, "
                         <<" must be: "<<use_ccd<<endl;
          else ZeroList[id]=im;
        }
      }
      else {
        if(insert) ImageList[id]=im;     
      }
      
    }
    delete stmt;
    db->Close();
    
    

    
    cout<<"**************************************************"<<endl;
    cout<<"Found "<<ImageList.size()
        <<" distinct single epoch images"<<endl;
    cout<<"**************************************************"<<endl;
    
    if(ZeroList.size()>0) {
      cout<<"Will put zp=0 for "<<ZeroList.size()<<" images"<<endl;
    }

    
    map<int,ImageInfo>::iterator image_iter=ImageList.begin();
    for(; image_iter!=ImageList.end(); ++image_iter) {
      cout<<image_iter->second.id<<" "<<image_iter->second.run<<" "
          <<image_iter->second.exp_name<<" "
          <<image_iter->second.ccd<<endl;
    }



    if(project=="DES") {
  
      if(debug>2) cout<<endl<<"Getting truth info for images"<<endl;
      map<int,ImageInfo>::iterator image_iter=ImageList.begin();
      for(; image_iter!=ImageList.end(); ++image_iter) {
        
        db = TSQLServer::Connect(db_string,
                                 db_user.c_str(), db_passwd.c_str());
        db_cmd=Form("select a.zp,a.gaina,a.gainb,a.zero,a.exptime from "
                    "image a,image b where "
                    " a.id=b.parentid and b.id=%d",image_iter->second.id);
        
        if(debug>2) cout<<db_cmd<<endl;
        stmt=db->Statement(db_cmd.c_str());
        
        // Check for errors in the statement
        if(!stmt->Process()) {
          cout<<stmt->GetErrorMsg()<<endl;
          exit(1);
        }
        stmt->StoreResult();
        
        double exp_time,zp_db,gaina,gainb,zero;
        image_iter->second.true_zp=-1;
        while(stmt->NextResultRow()) {
          
          zp_db=stmt->GetDouble(0);
          gaina=stmt->GetDouble(1);
          gainb=stmt->GetDouble(2);
          zero=stmt->GetDouble(3);
          exp_time=stmt->GetDouble(4);
          double gain=(gaina+gainb)/2;
          if(gain>0 && exp_time>0 && zero>0) { 
            image_iter->second.true_zp=zp_db+2.5*log10(zero*exp_time/gain);
          }
          else image_iter->second.true_zp=-999;
          
        }
        delete stmt;
        db->Close();

      }
    }




    
    int n_psm=0;
    if(use_psm || psm_cal) {
      
      cout<<endl;
      cout<<"**************************************************"<<endl;
      cout<<" PSM Zeropoints"<<endl;
      cout<<"**************************************************"<<endl;
    }


    image_iter=ImageList.begin();
    for(; image_iter!=ImageList.end(); ++image_iter) {
        
        db = TSQLServer::Connect(db_string,
                                 db_user.c_str(), db_passwd.c_str());
        
        db_cmd=Form("select a.mag_zero,a.sigma_mag_zero,c.photflag from "
                    "zeropoint a, image b, exposure c where a.imageid=%d and "
                    " b.id=a.imageid and a.source='PSM' and "
                    " b.exposureid=c.id and c.photflag=1 order by insert_date"
                    ,image_iter->second.id);
        if(debug>3) cout<<db_cmd<<endl;
        stmt=db->Statement(db_cmd.c_str());
        
        // Check for errors in the statement
        if(!stmt->Process()) {
          cout<<stmt->GetErrorMsg()<<endl;
          exit(1);
        }
        stmt->StoreResult();
        
        // check how many results
        image_iter->second.zp=-1;
        while(stmt->NextResultRow()) {
          
          image_iter->second.zp=stmt->GetDouble(0);
          image_iter->second.zp_err=stmt->GetDouble(1);
          image_iter->second.photometric=1;
        }
        
        if(image_iter->second.zp!=-1) {
          if(use_psm || psm_cal) {
            cout<<image_iter->second.id<<" zp="<<image_iter->second.zp<<endl;
            n_psm++;
          }
        }

        delete stmt;
        db->Close();
      }

    
    // Tree to store potential objects
    TFile *tdebug;
    TTree *tree_all;
    char c_exp[50],c_ccd[50],c_id[50],c_nite[50];
    int d_id,d_ccd,d_nite,i_exp;
    Object *ob=new Object;
    if(!debugfile.empty()) {

      tdebug=new TFile(debugfile.c_str(),"recreate");

      tree_all=new TTree("tree_all","");
      
      tree_all->Branch("ob",&ob);
      tree_all->Branch("c_exp",&c_exp,"c_exp/C");
      tree_all->Branch("i_exp",&i_exp,"i_exp/I");
      tree_all->Branch("c_nite",&c_nite,"c_nite/C");
      tree_all->Branch("c_ccd",&c_ccd,"c_ccd/C");
      tree_all->Branch("ccd",&d_ccd,"ccd/I");
      tree_all->Branch("c_id",&c_id,"c_id/C");
      tree_all->Branch("id",&d_id,"id/I");
      tree_all->Branch("nite",&d_nite,"nite/I");
    }

    // get a list of objects from each red image of of the 
    // objects current table
    map<int,vector<Object> > ImageObjs;
    image_iter=ImageList.begin();

    string prev_exp=image_iter->second.exp_name;
    i_exp=0;
    for(; image_iter!=ImageList.end(); ++image_iter) {
       if(debug>2)cout<<"Getting objects from image "
                      <<image_iter->second.id<<endl;
       
       sprintf(c_exp,"%s",image_iter->second.exp_name.c_str());
       sprintf(c_ccd,"%d",image_iter->second.ccd);
       sprintf(c_id,"%d",image_iter->second.id);
       sprintf(c_nite,"%d",image_iter->second.nite);
       d_id=image_iter->second.id;
       d_ccd=image_iter->second.ccd;
       d_nite=image_iter->second.nite;
       if(prev_exp!=image_iter->second.exp_name) {
         i_exp++;
         prev_exp=image_iter->second.exp_name;
       }

       double max_ra=-1e6,max_dec=-1e6,min_ra=1e6,min_dec=1e6;
       vector<Object> vec;
       db = TSQLServer::Connect(db_string,
                                db_user.c_str(), db_passwd.c_str());
       
       if(star_sel=="spread") {
          
          db_cmd=Form("select a.ra,a.dec,"
                      "a.band,"
                      "a.catalogid,a.imageid,a.mag_%s, " 
                      "a.magerr_%s,a.spread_model,a.flags,a.zeropoint from %s a,"
                      "exposure b,image c "
                      " where "
                      " a.imageid=%d and c.exposureid=b.id and a.imageid=c.id",
                      mag.c_str(),mag.c_str(),object_table.c_str(),
                      image_iter->second.id);
        } else {
         db_cmd=Form("select a.ra,a.dec,"
                      "a.band,"
                      "a.catalogid,a.imageid,a.mag_%s, " 
                      "a.magerr_%s,a.class_star,a.flags,a.zeropoint from %s a,"
                      "exposure b,image c "
                      " where "
                      " a.imageid=%d and c.exposureid=b.id and a.imageid=c.id",
                      mag.c_str(),mag.c_str(),object_table.c_str(),
                      image_iter->second.id);
        }
        if(debug>2) cout<<"Using following command to get objects from images:"
                      <<db_cmd<<endl;
        stmt=db->Statement(db_cmd.c_str());
       
        // Check for errors in the statement
        if(!stmt->Process()) {
          cout<<stmt->GetErrorMsg()<<endl;
          exit(1);
        }
        stmt->StoreResult();
       
       
        double ra,dec;
        int catid,imageid;
        double obj_mag,obj_err,obj_class,zeropoint;
        int obj_flag;
        string im_band;
       

        while(stmt->NextResultRow()) {
          ra=stmt->GetDouble(0);       
          dec=stmt->GetDouble(1);       
          im_band=stmt->GetString(2);       
          catid=stmt->GetInt(3);       
          imageid=stmt->GetInt(4);       
          obj_mag=stmt->GetDouble(5);       
          obj_err=stmt->GetDouble(6);       
          obj_class=stmt->GetDouble(7);       
          obj_flag=stmt->GetInt(8);       
          zeropoint=stmt->GetDouble(9);
          
          //if (obj_class<star_cut || obj_mag>98 || obj_err>err_cut) continue;   
          if (obj_mag>98 || obj_err>err_cut || obj_err<min_err || 
              obj_class<star_cut_min || obj_class>star_cut_max ||
              obj_flag>flag_cut) continue;   

          if(extinction) {
           
            double R_g=3.793;
            double R_r=2.751;
            double R_i=2.086;
            double R_z=1.479;
           
            double EBV=ExtMap.GetExtinction(ra,dec);
           
            /* apply extinction correction in each band */
            if(im_band=="g") obj_mag -= R_g * EBV;
            else if(im_band=="r") obj_mag -= R_r * EBV;
            else if(im_band=="i") obj_mag -= R_i * EBV;
            else if(im_band=="z") obj_mag -= R_z * EBV;
          }
         
          if(ra>max_ra) max_ra=ra;
          if(dec>max_dec) max_dec=dec;
          
          if(ra<min_ra) min_ra=ra;
          if(dec<min_dec) min_dec=dec;
          
          if(remove_zp) {
            if(zeropoint!=25) obj_mag-= (image_iter->second.zp-25);
          }
          
          Object obj(ra,dec,obj_mag,obj_err,im_band,
                     catid,imageid);
          obj.class_star=obj_class;
          obj.flag=obj_flag;
          

          
         
          double cd=cos(obj.Dec*TMath::DegToRad());
          obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
          obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
          obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());

          vec.push_back(obj);
          
          if(!debugfile.empty()) {
            *ob=obj;
            tree_all->Fill();
          }
          
        }

        ImageObjs[image_iter->first]=vec;
        ImageList[image_iter->first].max_RA=max_ra;
        ImageList[image_iter->first].min_RA=min_ra;
        ImageList[image_iter->first].max_Dec=max_dec;
        ImageList[image_iter->first].min_Dec=min_dec;

        if(debug>2)cout<<"  Found "<<vec.size()<<" objects"<<endl;
        
        delete stmt;
        db->Close();

    }
    cout<<"Database interaction done"<<endl;
    timer_db.Print();
    
    if(!debugfile.empty()) {
      tree_all->Write();
      tdebug->Close();
    }


    // Remove images with no objects
   //  image_iter=ImageList.begin();
//     for(; image_iter!=ImageList.end(); ) {
//       if( ImageObjs[image_iter->first].size()==0) {
//          cout<<"Removing image "<<image_iter->second.id
//              <<" because no objects were found"<<endl;
//         ImageList.erase(image_iter++) ;

//       }
//       else ++image_iter;
//     }



    vector<ImagePair> ImagePairs;
    vector<ImagePair>::iterator pair_iter;
    int n=ImageList.size();
    ImagePairs.reserve(n*(n-1)/2);

    image_iter=ImageList.begin();

    for(; image_iter!=ImageList.end(); image_iter++) {
       
      map<int,ImageInfo>::iterator image_iter2=ImageList.begin();
      for(; image_iter2!=ImageList.end(); image_iter2++) {
          
        ImagePair pa(image_iter->first,image_iter2->first);
         
        // check to see if pair already exists
        pair_iter=find(ImagePairs.begin(),ImagePairs.end(),pa);
        if(pair_iter==ImagePairs.end() && image_iter!=image_iter2 &&
           image_iter->first!=image_iter2->first) {
          ImagePairs.push_back(pa);
        }
      }
    }
    
    // cout<<endl;
//     cout<<"**************************************************"<<endl;
//     cout<<" Found "<<ImagePairs.size()<<" posible pairs of images"<<endl;
//     cout<<"**************************************************"<<endl;
    cout<<endl;
    cout<<"**************************************************"<<endl;
    cout<<" Overlapping pairs of images"<<endl;
    cout<<"**************************************************"<<endl;

    // for all pairs of images find matching objects
    vector<ImagePair> ImageMatches(ImagePairs);
    TTree *pair_tree=new TTree("tree","");
    int id1,id2,ccd1,ccd2;
    double mean,rms;
    double psm_zp1,psm_zp2,true_zp1,true_zp2;
    int good,bad;
    char exp1[100],exp2[100],oid1[20],oid2[20];
    pair_tree->Branch("id1",&id1,"id1/I");
    pair_tree->Branch("id2",&id2,"id2/I");
    pair_tree->Branch("ccd1",&ccd1,"ccd1/I");
    pair_tree->Branch("ccd2",&ccd2,"ccd2/I");
    pair_tree->Branch("oid1",&oid1,"oid1/C");
    pair_tree->Branch("oid2",&oid2,"oid2/C");
    pair_tree->Branch("exp1",&exp1,"exp1/C");
    pair_tree->Branch("exp2",&exp2,"exp2/C");
    pair_tree->Branch("mean",&mean,"mean/D");
    pair_tree->Branch("rms",&rms,"rms/D");
    pair_tree->Branch("good",&good,"good/I");
    pair_tree->Branch("bad",&bad,"bad/I");
    pair_tree->Branch("true_zp1",&true_zp1,"true_zp1/D");
    pair_tree->Branch("true_zp2",&true_zp2,"true_zp2/D");
    pair_tree->Branch("psm_zp1",&psm_zp1,"psm_zp1/D");
    pair_tree->Branch("psm_zp2",&psm_zp2,"psm_zp2/D");
    
    TTree *ob_tree=new TTree("ob_tree","");
    Object ob1,ob2;
    ob_tree->Branch("ob1",&ob1);
    ob_tree->Branch("ob2",&ob2);
    ob_tree->Branch("id1",&id1,"id1/I");
    ob_tree->Branch("id2",&id2,"id2/I");
    ob_tree->Branch("ccd1",&ccd1,"ccd1/I");
    ob_tree->Branch("ccd2",&ccd2,"ccd2/I");
    ob_tree->Branch("oid1",&oid1,"oid1/C");
    ob_tree->Branch("oid2",&oid2,"oid2/C");
    ob_tree->Branch("exp1",&exp1,"exp1/C");
    ob_tree->Branch("exp2",&exp2,"exp2/C");
    ob_tree->Branch("mean",&mean,"mean/D");
    ob_tree->Branch("rms",&rms,"rms/D");
    ob_tree->Branch("good",&good,"good/I");
    ob_tree->Branch("bad",&bad,"bad/I");
    ob_tree->Branch("psm_zp1",&psm_zp1,"psm_zp1/D");
    ob_tree->Branch("psm_zp2",&psm_zp2,"psm_zp2/D");

    pair_iter=ImageMatches.begin();
    for(; pair_iter!=ImageMatches.end(); ++pair_iter) {
       
      id1=pair_iter->id1;
      id2=pair_iter->id2;
      ccd1=ImageList[id1].ccd;
      ccd2=ImageList[id2].ccd;
      sprintf(exp1,"%s",ImageList[id1].exp_name.c_str());
      sprintf(exp2,"%s",ImageList[id2].exp_name.c_str());
      sprintf(oid1,"%d",id1);
      sprintf(oid2,"%d",id2);
      if(ImageList[id1].max_RA<ImageList[id2].min_RA && 
         ImageList[id1].max_RA>ImageList[id2].max_RA) {
        
         pair_iter->mean=-999;
         pair_iter->rms=-999;
         continue;
      }

      if(ImageList[id1].max_Dec<ImageList[id2].min_Dec && 
         ImageList[id1].max_Dec>ImageList[id2].max_Dec){
        
        pair_iter->mean=-999;
        pair_iter->rms=-999;
        continue;
      }
      if(ImageList[id1].zp>-1) psm_zp1=ImageList[id1].zp;
      else psm_zp1=-1;
      if(ImageList[id2].zp>-1) psm_zp2=ImageList[id2].zp;
      else psm_zp2=-1;

      true_zp1=ImageList[id1].true_zp;
      true_zp2=ImageList[id2].true_zp;

      KD_Tree obj_tree; // store first list in kd tree
      
      for(int i=0;i<ImageObjs[id1].size();i++) {
        obj_tree.insert(ImageObjs[id1][i]);
      }
      //if(debug)cout<<"Matching "<<id1<<" with "<<ImageObjs[id1].size()
      //           <<" to "<<id2<<" with "<<ImageObjs[id2].size()<<endl;
      
      vector<Object>::iterator obj_iter=ImageObjs[id2].begin();
      
      // Loop through objects from second image and
      // try to match from the first image
      vector<ObjPair> matches;
      for(; obj_iter!=ImageObjs[id2].end(); ++obj_iter) {
        
        // find closest match within tolerance
        KD_Match match=obj_tree.find_nearest(*obj_iter,tol);
        
        if(match.first!=obj_tree.end()) {
          //cout<<" Distance = "<<match.second;
          ObjPair pa;
          //cout<<match.second<<endl;
          pa.obj1=*match.first;
          pa.obj2=*obj_iter;
          pa.id1=id1;
          pa.id2=id2;
          matches.push_back(pa);
          ob1=pa.obj1;
          ob2=pa.obj2;
          if(!rootfile.empty())ob_tree->Fill();
        }
      }
      
      
      if(matches.size()<min_pair) {
        
        pair_iter->mean=-999;
        pair_iter->rms=-999;
        continue;
      }
      
      double dd;
      TTree ttree("tree","");
      ttree.Branch("dd",&dd);
      vector<double> diff, diff_err;
      vector<ObjPair>::iterator objpair_iter=matches.begin();
      for( ; objpair_iter!=matches.end(); ++objpair_iter) {
        
        if( (objpair_iter->obj1.mag<min_mag ||
             objpair_iter->obj1.mag>max_mag) &&
            (objpair_iter->obj2.mag<min_mag ||
             objpair_iter->obj2.mag<max_mag)
            ) continue;
        
        
        double magdiff=(objpair_iter->obj1.mag-
                        objpair_iter->obj2.mag);
        
        if(magdiff>max_mag_diff) continue;
        
        dd=magdiff;
        ttree.Fill();
        diff.push_back(magdiff);
        diff_err.push_back(1./(objpair_iter->obj1.mag_err*
                               objpair_iter->obj1.mag_err+
                               objpair_iter->obj2.mag_err*
                               objpair_iter->obj2.mag_err));
      }
      
     
      TH1D *hist=(TH1D*)gSystem->FindObject("hist");
      TCanvas c1;
      // ttree.Draw("dd>>hist(40)","","");

      
      
      

      sigma_clip(max_iter,thresh,diff,diff_err,mean,
                 good,bad,rms,use_clip,use_weighted_mean,clip_tol);
      
      //       TLine *hi=new TLine(mean,0,
      //                           mean,100);
      //       hi->SetLineColor(2);
      //       hi->Draw("same");
      //       c1.Update();
      
      mean=-mean;
      if(scale_rms) rms=rms/TMath::Sqrt(good);
       if(good<min_pair || rms>max_rms) {
         pair_iter->mean=-999;
         pair_iter->rms=-999;
         continue;
       }
       if(!rootfile.empty())pair_tree->Fill();
       cout<<ImageList[pair_iter->id1].exp_name<<" "<<ImageList[pair_iter->id1].ccd
           <<" ("<<pair_iter->id1<<") with "
           <<ImageList[pair_iter->id2].exp_name<<" "<<ImageList[pair_iter->id2].ccd
           <<" ("<<pair_iter->id2<<")"<<endl;
       // cout<<"Matched "<<pair_iter->id1<<" with "<<pair_iter->id2<<endl;

       cout<<" Mean: "<<mean<<" RMS: "<<rms<<" Good pairs: "<<good<<" Bad: "<<bad<<endl;
       //c1.WaitPrimitive();
              
       
       pair_iter->mean=mean;
       pair_iter->rms=rms;

    //Remove pairs of images that do not have enough matches
//          pair_iter=ImageMatches.begin();
//          while ( pair_iter!=ImageMatches.end()) {
//        if(pair_iter->matches.size()<min_pair) {
//           ImageMatches.erase(pair_iter);
//        }
//        else pair_iter++;
//     }
    
//     cout<<endl<<endl;
//     cout<<"**************************************************"<<endl;
//     cout<<"Found the following image pair matches"<<endl;
//     cout<<"**************************************************"<<endl;
//     pair_iter=ImageMatches.begin();
//     while ( pair_iter!=ImageMatches.end()) {
       
       // make a vector of differences
         // vector<double> diff, diff_err;
//        vector<ObjPair>::iterator objpair_iter=pair_iter->matches.begin();
//        for( ; objpair_iter!=pair_iter->matches.end(); ++objpair_iter) {
         
//           if( (objpair_iter->obj1.mag<min_mag ||
//                objpair_iter->obj1.mag>max_mag) &&
//               (objpair_iter->obj2.mag<min_mag ||
//                objpair_iter->obj2.mag<max_mag)
//             ) continue;
          
        
//           double magdiff=(objpair_iter->obj1.mag-
//                           objpair_iter->obj2.mag);
          
//           if(magdiff>max_mag_diff) continue;
          
//           diff.push_back(magdiff);
//           diff_err.push_back(sqrt(objpair_iter->obj1.mag_err*
//                                   objpair_iter->obj1.mag_err+
//                                   objpair_iter->obj2.mag_err*
//                                   objpair_iter->obj2.mag_err));
//        }
       
       
//        int good,bad;
//        double mean,rms;
//        sigma_clip(max_iter,thresh,diff,diff_err,mean,
//                   good,bad,rms,use_clip,use_weighted,clip_tol);

//        cout<<ImageList[pair_iter->id1].exp_name<<" "<<ImageList[pair_iter->id1].ccd
//            <<" with "
//            <<ImageList[pair_iter->id2].exp_name<<" "<<ImageList[pair_iter->id2].ccd
//            <<endl;
//        cout<<mean<<" "<<rms<<" "<<good<<" "<<bad<<endl;

//        // If only one good match or rms too small remove from the list
//        if(good<min_pair || rms>max_rms) {
//          cout<<" Not enough matches passed the cuts or rms too high"<<endl;
//           ImageMatches.erase(pair_iter);
//           continue;
//        }
       
//        pair_iter->mean=mean;
//        pair_iter->rms=rms;

         // pair_iter++;
    }


    // remove bad pair constraints
    pair_iter=ImageMatches.begin();
    while ( pair_iter!=ImageMatches.end()) {
      if(pair_iter->mean<-998 || pair_iter->rms==0 ||
         TMath::IsNaN(pair_iter->mean)) {
        ImageMatches.erase(pair_iter);
      }
      else pair_iter++;
    }
    
    
    
    vector<ImagePair> image_pairs_sky(ImagePairs);
    //image_pairs_sky.assign(ImagePairs.begin(),ImagePairs.end());

    if(use_sky) {
      
      // Build unique list of nites for sky brightness
      vector<int> unique_nites;
      image_iter=ImageList.begin();
      for(; image_iter!=ImageList.end(); image_iter++) {
        if(find(unique_nites.begin(),unique_nites.end(),
                image_iter->second.nite)==unique_nites.end()) {
          unique_nites.push_back(image_iter->second.nite);
        }
      }
      
      if(debug)cout<<endl<<"\nFound "<<unique_nites.size()
                   <<" unique nites"<<endl;
      
      vector<int>::iterator nite_iter=unique_nites.begin();
      
      for(; nite_iter!=unique_nites.end();nite_iter++) {
        
        vector<ImageInfo> nite_imgs;
        vector<string> unique_exp;
        vector<Exposure> exp_sky;
        double ccd_median[N_ccd][N_ccd];
        double ccd_median_err[N_ccd][N_ccd];
        
        db = TSQLServer::Connect(db_string,
                                 db_user.c_str(), db_passwd.c_str());
        
        // get list of exposure
        db_cmd=Form(" select b.exposurename,a.ccd,a.band,"
                    " a.skybrite, a.run from image a,"
                    " exposure b where a.imagetype='red'  "
                    " and a.run like '%%%d%%' and a.exposureid=b.id "
                    " AND b.photflag=1 and a.band='%s'",
                    *nite_iter,band.c_str());
        if(debug>1) cout<<db_cmd<<endl;
        stmt=db->Statement(db_cmd.c_str());
        
        // Check for errors in the statement
        if(!stmt->Process()) {
          cout<<stmt->GetErrorMsg()<<endl;
          exit(1);
        }
        stmt->StoreResult();
        
        string exp_name,im_band,im_run;
        int im_ccd;
        double im_skybrite;
        while(stmt->NextResultRow()) {
          exp_name=stmt->GetString(0);
          im_ccd=stmt->GetInt(1);
          im_band=stmt->GetString(2);
          im_skybrite=stmt->GetDouble(3);
          im_run=stmt->GetString(4);
        
          Exposure exp;
          exp.name=exp_name;
          exp.band=im_band;
          exp.ccd=im_ccd;
          exp.skybrite=im_skybrite;
          exp.run=im_run;
          exp_sky.push_back(exp);
          
          if(find(unique_exp.begin(),unique_exp.end(),exp_name)==
             unique_exp.end()) unique_exp.push_back(exp_name);
        }
        delete stmt;
        db->Close();
        
        if(unique_exp.size()==0||exp_sky.size()==0) {
          if(debug)cout<<"\nCouldn't find any exposures for nite "
                       <<*nite_iter<<endl;
          continue;
        }
        
        if(debug)cout<<"\nFor nite "<<*nite_iter<<" found "
                     <<unique_exp.size()
                     <<" distinct exposures and a total of "
                     <<exp_sky.size()<<" images"<<endl;
        


        // Calculate the median between each pair of ccds
        for(int i=1;i<=N_ccd;i++) {
          for(int j=1;j<=N_ccd;j++) {
            if(i==j) continue;

            
            vector<double> ratios;
            
            // Loop over all the exposures/ccd combos 
            // to get the sky brite values for the particular
            // ccd combination
            vector<Exposure>::iterator exp_iter=exp_sky.begin();
            
            for(; exp_iter!=exp_sky.end();++exp_iter) {
              vector<Exposure>::iterator exp_iter2=exp_sky.begin();
              for(; exp_iter2!=exp_sky.end();++exp_iter2) {
                
                if(exp_iter->ccd==i && exp_iter2->ccd==j &&
                   exp_iter->name==exp_iter2->name
                   ) {
                  ratios.push_back(exp_iter->skybrite/exp_iter2->skybrite);
                }
              }
            } 
            
            double median=TMath::Median(ratios.size(),&ratios[0]);
            double rms=TMath::RMS(ratios.size(),&ratios[0]);
            double median_err=1.253*rms/sqrt(ratios.size());
            
            ccd_median_err[i-1][j-1]=(2.5*TMath::LogE()/
                                      median)*median_err;
            if(ccd_median_err[i-1][j-1]<min_sky_error) {
              ccd_median_err[i-1][j-1]=min_sky_error;
            }
            ccd_median[i-1][j-1]=2.5*TMath::Log10(median);
          }
        }

        
        // Loop over all image pairs and assign a 
        // a sky brightness ratio if they are from
        // the correct nite and same exposure
        // print them out
        pair_iter=image_pairs_sky.begin();
        if(debug)cout<<"Using the following sky constraints"<<endl;
        for(; pair_iter!=image_pairs_sky.end(); ++pair_iter) {
          ImageInfo im1=ImageList[pair_iter->id1];
          ImageInfo im2=ImageList[pair_iter->id2];
          
          if(im1.nite==*nite_iter && im2.nite==*nite_iter &&
             im1.exp_name==im2.exp_name) {

            pair_iter->mean=ccd_median[im1.ccd-1][im2.ccd-1];
            pair_iter->rms=ccd_median_err[im1.ccd-1][im2.ccd-1];
              
              if(debug)cout<<pair_iter->id1<<" "
                           <<pair_iter->id2<<" "
                           <<pair_iter->mean<<" "
                           <<pair_iter->rms<<endl;
              
          }
          
        }
        

        //Remove pairs of images that do not have sky constraints
        pair_iter=image_pairs_sky.begin();
        while ( pair_iter!=image_pairs_sky.end()) {
          if(pair_iter->mean<-998 || pair_iter->rms==0 ||
             pair_iter->mean) {
            image_pairs_sky.erase(pair_iter);
          }
          else pair_iter++;
        }
      }
    }

      
      
      
    vector<ExpPair> SameNiteExpPairs;
    vector<ExpPair> DiffNiteExpPairs;
    vector<ExpPair> BestExpPairs;
    vector<Exposure> Exps;

    // build unique exposure list
    vector<Exposure>::iterator exp_iter;
    image_iter=ImageList.begin();
    for(;image_iter!=ImageList.end();image_iter++) {
      Exposure exp;
      exp.name=image_iter->second.exp_name;
      exp.band=image_iter->second.band;
      exp.run=image_iter->second.run;
      
      exp_iter=find(Exps.begin(),
                    Exps.end(),
                    exp);
      if(exp_iter==Exps.end()) {
        
        Exps.push_back(exp);
      }
    }
    
    if(debug>4) {
      cout<<endl<<"**************************************************"<<endl;
      cout<<"Unique exposures"<<endl;
      cout<<"**************************************************"<<endl;
      exp_iter=Exps.begin();
      for(;exp_iter!=Exps.end();exp_iter++) {
        cout<<exp_iter->name<<endl;
      }
    }
      
    vector<ImagePair> AvePairs;
    if(use_exp_ave) {

      //remove pair constraints from overlapping exposures
      // that are from different ccds
       pair_iter=ImageMatches.begin();
       while ( pair_iter!=ImageMatches.end()) {
         if(ImageList[pair_iter->id1].ccd==ImageList[pair_iter->id2].ccd &&
            ImageList[pair_iter->id1].nite==ImageList[pair_iter->id2].nite
            ) {
           AvePairs.push_back(*pair_iter);
           ImageMatches.erase(pair_iter);
         }
         else pair_iter++;
       }
       
       
      // combine ccd constraints to exposure based
      // build unique exposure pair list, exposure list

      pair_iter=AvePairs.begin();
      for(; pair_iter!=AvePairs.end();pair_iter++) {
        
        ExpPair pair(ImageList[pair_iter->id1].exp_name,
                     ImageList[pair_iter->id2].exp_name);
        
        vector<ExpPair>::iterator iter=find(SameNiteExpPairs.begin(),
                                            SameNiteExpPairs.end(),
                                            pair);
        if(iter==SameNiteExpPairs.end()) {
          pair.matches.push_back(*pair_iter);
          SameNiteExpPairs.push_back(pair);
          continue;
        }
        else {
          iter->matches.push_back(*pair_iter);
        }
        
      }
      
      // vector<ExpPair>::iterator exp_pair_iter=SameNiteExpPairs.begin();
//       while(exp_pair_iter!=SameNiteExpPairs.end()) {
//         if(exp_pair_iter->matches.size()<5) SameNiteExpPairs.erase(exp_pair_iter);
//         else exp_pair_iter++;
//       }
      // loop over exposure pairs and calculate mean rms
      vector<ExpPair>::iterator exp_pair_iter=SameNiteExpPairs.begin();
      //exp_pair_iter=SameNiteExpPairs.begin();
      cout<<endl;
      cout<<"**************************************************"<<endl;
      cout<<"Average difference from same ccds but different exposures"<<endl;
      cout<<"**************************************************"<<endl;
      for(; exp_pair_iter!=SameNiteExpPairs.end();exp_pair_iter++) {
        
        
        vector<double> diff,diff_err;
        for(pair_iter=exp_pair_iter->matches.begin();
            pair_iter!=exp_pair_iter->matches.end();pair_iter++) {
          diff.push_back(pair_iter->mean);
          diff_err.push_back(1./pair_iter->rms);
        }
        
        int good,bad;
        double mean,rms;
        sigma_clip(max_iter,thresh,diff,diff_err,mean,
                   good,bad,rms,use_clip,use_weighted_mean,clip_tol);
        
        cout<<exp_pair_iter->exp1<<" "<<exp_pair_iter->exp2<<endl;
        cout<<" Mean:"<<mean<<"  RMS"<<rms<<endl;
        for(pair_iter=exp_pair_iter->matches.begin();
            pair_iter!=exp_pair_iter->matches.end();pair_iter++) {
          cout<<"    "<<ImageList[pair_iter->id1].ccd<<"<->"<<
            ImageList[pair_iter->id2].ccd<<" "<<pair_iter->mean<<" "
              <<pair_iter->rms<<endl;
        }
        exp_pair_iter->mean=mean;
        exp_pair_iter->rms=rms;
      }
    
     
    // build list of exposure pairs that will result in the
    // best combination of rms's this should elimate the
    // noise caused by the averaging

    
//     vector<Exposure> search_list;
//     vector<Exposure> found_list;

   //  if(use_best) {
//       copy(Exps.begin(),Exps.end(),back_inserter(search_list));
      
//       // put the first exposure on the list
//       // I don't know if it matters which one is the first
      
//       found_list.push_back(Exps[0]);
      
//       // remove the first one from the search list
//       exp_iter=remove(search_list.begin(),search_list.end(),Exps[0]);
//       search_list.erase(exp_iter,search_list.end());
      
//       //vector<Exposure>::iterator found_iter=found_list.begin();
//       //for(;found_iter!=found_list.end();found_iter++) {
//       for( int ii=0;ii<found_list.size();ii++) {
        
//         cout<<"Found "<<found_list[ii].name<<endl;
//         double min_rms=1e6;
//         ExpPair min_exp_pair;
//         Exposure min_exp;
//         bool found=false;
        
                
//         // loop over search list and find pair with the minimum rms
//         vector<Exposure>::iterator search_iter=search_list.begin();
//         for(; search_iter!=search_list.end();search_iter++) {


//           cout<<"Seach "<<found_list[ii].name<<" "<<search_iter->name;
//           ExpPair exp(found_list[ii].name,search_iter->name);
//           vector<ExpPair>::iterator exp_pair_iter=find(SameNiteExpPairs.begin(),
//                                                        SameNiteExpPairs.end(),
//                                                        exp);
          
//           if(exp_pair_iter!=SameNiteExpPairs.end()) {
//             cout<<"  "<<exp_pair_iter->mean<<" "
//                 <<exp_pair_iter->rms<<endl;
//             if(exp_pair_iter->rms<min_rms) {
//               min_rms=exp_pair_iter->rms;
//               min_exp=*search_iter;
//               min_exp_pair=*exp_pair_iter;
//               found=true;
//             }
//           }
//           else cout<<endl;
//         }
        
//         if(found) {
//           cout<<"Min "<<min_exp.name<<" "<<min_rms<<endl;
//           // add lowest rms exposure match to the found list
//           // and remove from search list

//           exp_iter=remove(search_list.begin(),search_list.end(),min_exp);
//           search_list.erase(exp_iter,search_list.end());
//           found_list.push_back(min_exp);          
//           // add exposure pair to best pair
//           BestExpPairs.push_back(min_exp_pair);
//         }
//         else {
//           cout<<"Couldn't find a match for "<<found_list[ii].name<<endl;
//         }
        
//       }
    
      
//       // filter out all pair constraints that do not
//       // match one of the best pair exposures
//       pair_iter=AvePairs.begin();
//       while ( pair_iter!=AvePairs.end()) {
//         ExpPair exp(ImageList[pair_iter->id1].exp_name,
//                     ImageList[pair_iter->id2].exp_name);
        
//         // find the particular exposure pair
//         vector<ExpPair>::iterator iter=find(BestExpPairs.begin(),
//                                             BestExpPairs.end(),
//                                             exp);
//         if(iter==BestExpPairs.end()) {
//           AvePairs.erase(pair_iter);
//         }
//         else pair_iter++;
//       }
    
//       cout<<"**************************************************"<<endl;
//       cout<<"Average difference from best exposures"<<endl;
//       cout<<"**************************************************"<<endl;
//       exp_pair_iter=BestExpPairs.begin();
//       for(; exp_pair_iter!=BestExpPairs.end();exp_pair_iter++) {
        
//         cout<<exp_pair_iter->exp1<<" "<<exp_pair_iter->exp2<<endl;
//         cout<<" Mean:"<<exp_pair_iter->mean<<"  RMS"<<exp_pair_iter->rms<<endl;
//         for(pair_iter=exp_pair_iter->matches.begin();
//             pair_iter!=exp_pair_iter->matches.end();pair_iter++) {
//           cout<<"    "<<ImageList[pair_iter->id1].ccd<<"<->"<<
//             ImageList[pair_iter->id2].ccd<<" "<<pair_iter->mean<<" "
//               <<pair_iter->rms<<endl;
//         }
//       }
//     }
    }

        
   //  vector<ImagePair> NiteAvePairs;
//     if(use_exp_nite_ave) {


//       //remove pair constraints from overlapping exposures
//       // that are from different nites
//       pair_iter=ImageMatches.begin();
//       while ( pair_iter!=ImageMatches.end()) {
//          if(ImageList[pair_iter->id1].nite!=ImageList[pair_iter->id2].nite) {
//            NiteAvePairs.push_back(*pair_iter);
//            ImageMatches.erase(pair_iter);
//          }
//          else pair_iter++;
//       }
      

//       // combine ccd constraints to exposure based
//       // build unique exposure pair list, exposure list

//       pair_iter=NiteAvePairs.begin();
//       for(; pair_iter!=NiteAvePairs.end();pair_iter++) {
        
//         if(ImageList[pair_iter->id1].exp_name==
//            ImageList[pair_iter->id2].exp_name ||
//            ImageList[pair_iter->id1].nite==
//            ImageList[pair_iter->id2].nite          
//            ) continue;
//         ExpPair pair(ImageList[pair_iter->id1].exp_name,
//                      ImageList[pair_iter->id2].exp_name);
        
//         vector<ExpPair>::iterator iter=find(DiffNiteExpPairs.begin(),
//                                             DiffNiteExpPairs.end(),
//                                             pair);
//         if(iter==DiffNiteExpPairs.end()) {
//           pair.matches.push_back(*pair_iter);
//           DiffNiteExpPairs.push_back(pair);
//           continue;
//         }
//         else {
//           iter->matches.push_back(*pair_iter);
//         }
        
//       }
      
//       // loop over exposure pairs and calculate mean rms
//       vector<ExpPair>::iterator exp_pair_iter=DiffNiteExpPairs.begin();

//       cout<<endl;
//       cout<<"**************************************************"<<endl;
//       cout<<"Average difference from exposures across nites"<<endl;
//       cout<<"**************************************************"<<endl;
//       for(; exp_pair_iter!=DiffNiteExpPairs.end();exp_pair_iter++) {
        
//         vector<double> diff,diff_err;
//         for(pair_iter=exp_pair_iter->matches.begin();
//             pair_iter!=exp_pair_iter->matches.end();pair_iter++) {
//           diff.push_back(pair_iter->mean);
//           diff_err.push_back(pair_iter->rms);
//         }
        
//         int good,bad;
//         double mean,rms;
//         sigma_clip(max_iter,thresh,diff,diff_err,mean,
//                    good,bad,rms,use_clip,use_weighted,clip_tol);
        
//         cout<<exp_pair_iter->exp1<<" "<<exp_pair_iter->exp2<<endl;
//         cout<<" Mean:"<<mean<<"  RMS"<<rms<<endl;
//         for(pair_iter=exp_pair_iter->matches.begin();
//             pair_iter!=exp_pair_iter->matches.end();pair_iter++) {
//           cout<<"    "<<ImageList[pair_iter->id1].ccd<<"<->"<<
//             ImageList[pair_iter->id2].ccd<<" "<<pair_iter->mean<<" "
//               <<pair_iter->rms<<endl;
//         }
//         exp_pair_iter->mean=mean;
//         exp_pair_iter->rms=rms;
//       }
      
      
//       // build list of exposure pairs that will result in the
//       // best combination of rms's this should elimate the
//       // noise caused by the averaging
      
      
// //       vector<Exposure> search_list;
// //       vector<Exposure> found_list;

// //     if(use_best) {
// //       copy(Exps.begin(),Exps.end(),back_inserter(search_list));
      
// //       // put the first exposure on the list
// //       // I don't know if it matters which one is the first
      
// //       found_list.push_back(Exps[0]);
      
// //       // remove the first one from the search list
// //       exp_iter=remove(search_list.begin(),search_list.end(),Exps[0]);
// //       search_list.erase(exp_iter,search_list.end());
      
// //       //vector<Exposure>::iterator found_iter=found_list.begin();
// //       //for(;found_iter!=found_list.end();found_iter++) {
// //       for( int ii=0;ii<found_list.size();ii++) {
        
// //         cout<<"Found "<<found_list[ii].name<<endl;
// //         double min_rms=1e6;
// //         ExpPair min_exp_pair;
// //         Exposure min_exp;
// //         bool found=false;
        
                
// //         // loop over search list and find pair with the minimum rms
// //         vector<Exposure>::iterator search_iter=search_list.begin();
// //         for(; search_iter!=search_list.end();search_iter++) {


// //           cout<<"Seach "<<found_list[ii].name<<" "<<search_iter->name;
// //           ExpPair exp(found_list[ii].name,search_iter->name);
// //           vector<ExpPair>::iterator exp_pair_iter=find(ExpPairs.begin(),
// //                                                        ExpPairs.end(),
// //                                                        exp);
          
// //           if(exp_pair_iter!=ExpPairs.end()) {
// //             cout<<"  "<<exp_pair_iter->mean<<" "
// //                 <<exp_pair_iter->rms<<endl;
// //             if(exp_pair_iter->rms<min_rms) {
// //               min_rms=exp_pair_iter->rms;
// //               min_exp=*search_iter;
// //               min_exp_pair=*exp_pair_iter;
// //               found=true;
// //             }
// //           }
// //           else cout<<endl;
// //         }
        
// //         if(found) {
// //           cout<<"Min "<<min_exp.name<<" "<<min_rms<<endl;
// //           // add lowest rms exposure match to the found list
// //           // and remove from search list

// //           exp_iter=remove(search_list.begin(),search_list.end(),min_exp);
// //           search_list.erase(exp_iter,search_list.end());
// //           found_list.push_back(min_exp);          
// //           // add exposure pair to best pair
// //           BestExpPairs.push_back(min_exp_pair);
// //         }
// //         else {
// //           cout<<"Couldn't find a match for "<<found_list[ii].name<<endl;
// //         }
        
// //       }
    
      
// //       // filter out all pair constraints that do not
// //       // match one of the best pair exposures
// //       pair_iter=AvePairs.begin();
// //       while ( pair_iter!=AvePairs.end()) {
// //         ExpPair exp(ImageList[pair_iter->id1].exp_name,
// //                     ImageList[pair_iter->id2].exp_name);
        
// //         // find the particular exposure pair
// //         vector<ExpPair>::iterator iter=find(BestExpPairs.begin(),
// //                                             BestExpPairs.end(),
// //                                             exp);
// //         if(iter==BestExpPairs.end()) {
// //           ImageMatches.erase(pair_iter);
// //         }
// //         else pair_iter++;
// //       }
    
// //       cout<<"**************************************************"<<endl;
// //       cout<<"Average difference from best exposures"<<endl;
// //       cout<<"**************************************************"<<endl;
// //       exp_pair_iter=BestExpPairs.begin();
// //       for(; exp_pair_iter!=BestExpPairs.end();exp_pair_iter++) {
        
// //         cout<<exp_pair_iter->exp1<<" "<<exp_pair_iter->exp2<<endl;
// //         cout<<" Mean:"<<exp_pair_iter->mean<<"  RMS"<<exp_pair_iter->rms<<endl;
// //         for(pair_iter=exp_pair_iter->matches.begin();
// //             pair_iter!=exp_pair_iter->matches.end();pair_iter++) {
// //           cout<<"    "<<ImageList[pair_iter->id1].ccd<<"<->"<<
// //             ImageList[pair_iter->id2].ccd<<" "<<pair_iter->mean<<" "
// //               <<pair_iter->rms<<endl;
// //         }
// //       }
// //     }
//     }

        
     // read in text file of zeropoint differences
    // Need to move this to a better solution
    

    
    double zp_diff[8][8],zprms_diff[8][8];    

    double zp_diff1[8][8],zprms_diff1[8][8];    
    double zp_diff2[8][8],zprms_diff2[8][8];    
    vector<ImagePair> ImageCCDs(ImagePairs);
    int n_zp_diff=0;
    if(use_same_exp || add_missing) {

      cout<<endl;
      cout<<"**************************************************"<<endl;
      cout<<"CCD same exposure constraints"<<endl;
      cout<<"**************************************************"<<endl;

      if(project=="SPT") {
        
        if(bcs_zpdir.empty()) {
          cout<<"Must specify zeropoint directory to find bcs_zp_e1_"
              <<band<<" and bcs_zp_e2_"<<band<<endl;
          exit(1);
         }
         zpdiff_filename=bcs_zpdir+"/bcs_zp_e3_"+band;
         
         if(zpdiff_filename.empty()) {
           cout<<"Must specify zeropoint file"<<endl;
           exit(1);
         }
         
         ifstream zpfile(zpdiff_filename.c_str());
         
         while(!zpfile.eof()) {
           for(int i=0;i<8;i++) {
             zpfile>>zp_diff[i][0]>>zp_diff[i][1]>>zp_diff[i][2]>>zp_diff[i][3]
                   >>zp_diff[i][4]>>zp_diff[i][5]>>zp_diff[i][6]>>zp_diff[i][7];
           }
           for(int i=0;i<8;i++) {
             zpfile>>zprms_diff[i][0]>>zprms_diff[i][1]>>zprms_diff[i][2]
                   >>zprms_diff[i][3]>>zprms_diff[i][4]>>zprms_diff[i][5]
                    >>zprms_diff[i][6]>>zprms_diff[i][7];
            }
          }
          
          pair_iter=ImageCCDs.begin();
          for(; pair_iter!=ImageCCDs.end(); pair_iter++) {
            
            
            int ccd1= ImageList[pair_iter->id1].ccd-1;
            int ccd2= ImageList[pair_iter->id2].ccd-1;
            
            if(ImageList[pair_iter->id1].exp_name!=
               ImageList[pair_iter->id2].exp_name) {
              pair_iter->mean=-999;
              pair_iter->rms=-999;
              continue;
            }
            
            if(fabs(zp_diff[ccd1][ccd2])>1e-12) {
              pair_iter->mean=zp_diff[ccd1][ccd2];
              pair_iter->rms=zprms_diff[ccd1][ccd2];
              //n_zp_diff++;
            //   cout<<ImageList[pair_iter->id1].exp_name<<" "<<ImageList[pair_iter->id1].ccd
//                   <<" with "
//                   <<ImageList[pair_iter->id2].exp_name<<" "<<ImageList[pair_iter->id2].ccd
//                   <<endl;
              cout<<pair_iter->id1<<" with"<<pair_iter->id2<<endl;
              cout<<" "<<pair_iter->mean<<" "<<pair_iter->rms<<endl;
              
            } else {
              pair_iter->mean=-999;
              pair_iter->rms=-999;
            }
          }
      }
      else if(project=="BCS" || project=="SCS") {

        if(bcs_zpdir.empty()) {
          cout<<"Must specify zeropoint directory to find bcs_zp_e1_"
              <<band<<" and bcs_zp_e2_"<<band<<endl;
          exit(1);
         }
         zpdiff_filename=bcs_zpdir+"/bcs_zp_e1_"+band;
         ifstream zpfile1(zpdiff_filename.c_str());
          
         while(!zpfile1.eof()) {
           for(int i=0;i<8;i++) {
             zpfile1>>zp_diff1[i][0]>>zp_diff1[i][1]>>zp_diff1[i][2]>>zp_diff1[i][3]
                   >>zp_diff1[i][4]>>zp_diff1[i][5]>>zp_diff1[i][6]>>zp_diff1[i][7];
           }
           for(int i=0;i<8;i++) {
             zpfile1>>zprms_diff1[i][0]>>zprms_diff1[i][1]>>zprms_diff1[i][2]
                   >>zprms_diff1[i][3]>>zprms_diff1[i][4]>>zprms_diff1[i][5]
                   >>zprms_diff1[i][6]>>zprms_diff1[i][7];
           }
         }
         

         


         zpdiff_filename=bcs_zpdir+"/bcs_zp_e2_"+band;
         ifstream zpfile2(zpdiff_filename.c_str());
          
         while(!zpfile2.eof()) {
           for(int i=0;i<8;i++) {
             zpfile2>>zp_diff2[i][0]>>zp_diff2[i][1]>>zp_diff2[i][2]>>zp_diff2[i][3]
                   >>zp_diff2[i][4]>>zp_diff2[i][5]>>zp_diff2[i][6]>>zp_diff2[i][7];
           }
           for(int i=0;i<8;i++) {
             zpfile2>>zprms_diff2[i][0]>>zprms_diff2[i][1]>>zprms_diff2[i][2]
                   >>zprms_diff2[i][3]>>zprms_diff2[i][4]>>zprms_diff2[i][5]
                   >>zprms_diff2[i][6]>>zprms_diff2[i][7];
           }
         }
    
          
         
         pair_iter=ImageCCDs.begin();
         for(; pair_iter!=ImageCCDs.end(); pair_iter++) {

           // check for same exposure and photometric night
           if(ImageList[pair_iter->id1].exp_name!=
              ImageList[pair_iter->id2].exp_name ||
              (ImageList[pair_iter->id1].photometric==0 &&
               !no_photo_req)) {

             pair_iter->mean=-999;
             pair_iter->rms=-999;
             continue;
           }
           
           
                    
           int ccd1= ImageList[pair_iter->id1].ccd-1;
           int ccd2= ImageList[pair_iter->id2].ccd-1;
           
           // determin epoch
           int epoch;
           if(ImageList[pair_iter->id1].nite<2008) epoch=1;
           else epoch=2;
           
           if(epoch==1) {
             if(fabs(zp_diff1[ccd1][ccd2])>1e-12) {
               pair_iter->mean=zp_diff1[ccd1][ccd2];
               pair_iter->rms=zprms_diff1[ccd1][ccd2];
               //n_zp_diff++;
               cout<<ImageList[pair_iter->id1].exp_name<<" "<<ImageList[pair_iter->id1].ccd
                   <<" with "
                   <<ImageList[pair_iter->id2].exp_name<<" "<<ImageList[pair_iter->id2].ccd
                   <<endl;
               cout<<" "<<pair_iter->mean<<" "<<pair_iter->rms<<endl;
               
             } else {
               pair_iter->mean=-999;
               pair_iter->rms=-999;
             }
           }
           else if(epoch==2) {
             if(fabs(zp_diff2[ccd1][ccd2])>1e-12) {
               pair_iter->mean=zp_diff2[ccd1][ccd2];
               pair_iter->rms=zprms_diff2[ccd1][ccd2];
               //n_zp_diff++;
               cout<<ImageList[pair_iter->id1].exp_name<<" "<<ImageList[pair_iter->id1].ccd
                   <<" with "
                   <<ImageList[pair_iter->id2].exp_name<<" "<<ImageList[pair_iter->id2].ccd
                   <<endl;
               cout<<" "<<pair_iter->mean<<" "<<pair_iter->rms<<endl;
               
             } else {
               pair_iter->mean=-999;
                pair_iter->rms=-999;
             }
           }
         }
      }
           



      // remove bad ccd constraints
      pair_iter=ImageCCDs.begin();
      while ( pair_iter!=ImageCCDs.end()) {
        if(pair_iter->mean<-998) {
          ImageCCDs.erase(pair_iter);
        }
        else pair_iter++;
      }
    }    
    


    // Build the column index for the matrix
    map<int,int> imageid_col;
    int counter=0;
    int n_photo;
    int n_sky=0;

    // remove images with no overlapping tiles
    // may not want to have this on by default
    if(remove_solo) {
      image_iter=ImageList.begin();
      
      for(; image_iter!=ImageList.end();) {
     
        bool found=false;   
        // loop throuth and see if there is a match to overlaps
        pair_iter=ImageMatches.begin();
        while ( pair_iter!=ImageMatches.end()) {
          if(image_iter->second.id==pair_iter->id1 || 
             image_iter->second.id==pair_iter->id2)
           found=true;
          pair_iter++;
        }

        // remove from Image list and to zero list
        if(!found) {
          ZeroList[image_iter->second.id]=image_iter->second;
          ImageList.erase(image_iter++);
        }
        else ++image_iter;
      
        
      }
      

    }
      
    



    if(!use_psm) n_photo=1;
    else n_photo=n_psm;

    if(use_sky) n_sky=image_pairs_sky.size();

      
    image_iter=ImageList.begin();
    for(; image_iter!=ImageList.end(); ++image_iter) {
      imageid_col[image_iter->second.id]=counter;
      counter++;
    }
    
    int n_match=0;
    int n_exp_ave=0;
    if(use_rel) n_match=ImageMatches.size();
    if(use_exp_ave) n_exp_ave=AvePairs.size();
    if(use_same_exp)n_zp_diff=ImageCCDs.size();

      
    // Check if there are no matching pairs, but psm solutions exist
    // and using psm_cal just assign the psm solutions and set
    // psm_cal = false;
    if(n_match==0 && psm_cal) {
      n_photo=n_psm;
      use_psm=true;
      psm_cal=false;
    }
    

    int ncol=ImageList.size();
    int nrow=n_photo+n_match+n_exp_ave+n_zp_diff+n_sky;

    // Check if sufficient constraints.  If not and we are using
    // psm_call add them to the matrix
    if(nrow<ncol && psm_cal) {
      use_psm=true;
      psm_cal=false;
      n_photo=n_psm;
      nrow=n_photo+n_match+n_exp_ave+n_zp_diff+n_sky;
      
      cout<<"Not enough matching pairs.  Adding psm solutions to matrix"
          <<endl;
      
    }
   
    // Add same exposure constraints if they were going to be
    // added afterward if there are not enough constraints
    bool skip_second=false;
    if(nrow<ncol && add_missing && !use_same_exp) {
      use_same_exp=true;
      n_zp_diff=ImageCCDs.size();
      nrow=n_photo+n_match+n_exp_ave+n_zp_diff+n_sky;
      cout<<endl<<"Not enough matching pairs.  "
          <<"Adding same exposure constraints"<<endl;
      skip_second=true;
    }
    






    
//     if(nrow==1 ) {
//       if( !add_missing || (add_missing && ImageCCDs.size()==0)) {
        
//         string evt="Found no constraints.  Exiting...";
//         report_evt(true,"STATUS",5,evt);
//         exit(1);
//       }
//     }
    
    cout<<"\n\n***** Using the Following Constraints***********"<<endl;
    cout<<"Direct ZP: "<<n_photo;
    cout<<"    Relative Pairs: "<<n_match;
    cout<<"    Averaged Pairs: "<<n_exp_ave;
    cout<<"    CCD Diff: "<<n_zp_diff;
    if(use_sky) cout<<"    Sky Pairs: "<<n_sky<<endl;
    else cout<<endl;
    TMatrixD A(nrow,ncol);
    TVectorD b(nrow);
    // Assign the zeropoints
         


    counter=0;

    // If there is only one ccd match using relative and nothing
    // else give it a zeropoint of 25
    if(n_photo==1 && n_match==0 && n_exp_ave==0 && 
       n_zp_diff==0 && n_sky==0) {
      if(use_weighted) {
        double err=0.001;
        b(counter)=25./err;
        A(counter,imageid_col[0])=1/err;
      }
      else {
        b(counter)=25;
        A(counter,imageid_col[0])=1;
      }
      counter++;
    }

    // assign a zeropoint of 25 to first image if only one or 
    // the first image of a matching pair
    if(!use_psm ) {
      int image_id;
      
      if(n_match>0) {
        image_id=ImageMatches[0].id1;
      }
      else {
        image_id=(*ImageList.begin()).second.id;
      }
      // assign 25 to the first zeropoint that has a relative
      // constraint
      if(use_weighted) {
        double err=0.001;
        b(counter)=25./err;
        A(counter,imageid_col[image_id])=1/err;
      }
      else {
        b(counter)=25;
        A(counter,imageid_col[image_id])=1;
      }
      counter++;
    }
    else if(use_psm) {
      
      map<int,ImageInfo>::iterator image_iter=ImageList.begin();
      for(; image_iter!=ImageList.end(); ++image_iter) {
        
        if(image_iter->second.zp!=-1) {
          double zp,zperr;
          zp=image_iter->second.zp;
          if(use_weighted) zperr=image_iter->second.zp_err+psm_sys;
          else zperr=1;

          b(counter)=zp/zperr;
          A(counter,imageid_col[image_iter->second.id])=1/zperr;
          counter++;
        }
      }
    }

      
    
    if(use_rel) {
      //Assign the pair constraints
      pair_iter=ImageMatches.begin();
      for( ; pair_iter!=ImageMatches.end();pair_iter++) {
        // if not using the exposure average or the ccd numbers
        // are different use the regular constraint.
        if(use_weighted) {
          b[counter]=pair_iter->mean/pair_iter->rms;
          A[counter][imageid_col[pair_iter->id1]]=1/pair_iter->rms;
          A[counter][imageid_col[pair_iter->id2]]=-1/pair_iter->rms;
        }
        else {
          b[counter]=pair_iter->mean;
          A[counter][imageid_col[pair_iter->id1]]=1;
          A[counter][imageid_col[pair_iter->id2]]=-1;
        }
        counter++;
      }
    }

    if(use_exp_ave) {
      pair_iter=AvePairs.begin();
      for( ; pair_iter!=AvePairs.end();pair_iter++) {
        
        ExpPair exp(ImageList[pair_iter->id1].exp_name,
                    ImageList[pair_iter->id2].exp_name);
        vector<ExpPair>::iterator iter;
        
        
        if(!use_best) {
          // find the particular exposure pair
          iter=find(SameNiteExpPairs.begin(),SameNiteExpPairs.end(),
                    exp);
          if(iter==SameNiteExpPairs.end()) {
            cout<<"Probelm finding average exposure difference for images: "
                <<pair_iter->id1<<" "<<pair_iter->id2<<endl;
            continue;
          }
        }
        else {
          iter=find(BestExpPairs.begin(),BestExpPairs.end(),
                    exp);
          if(iter==BestExpPairs.end()) {
            cout<<"Probelm finding average exposure difference for images: "
                <<pair_iter->id1<<" "<<pair_iter->id2<<endl;
            continue;
          }
        }
        
        double mean;
        double err;
        
        mean=iter->mean;
        err=iter->rms;
        
        if(use_weighted) {
          b[counter]=mean/err;
          A[counter][imageid_col[pair_iter->id1]]=1/err;
          A[counter][imageid_col[pair_iter->id2]]=-1/err;
        }
        else {
          b[counter]=mean;
          A[counter][imageid_col[pair_iter->id1]]=1;
          A[counter][imageid_col[pair_iter->id2]]=-1;
        }
        
        counter++;
      }
    }

    if(use_same_exp) {
    
    // Assign zp diff constraints
      pair_iter=ImageCCDs.begin();
      for( ; pair_iter!=ImageCCDs.end();pair_iter++) {

      if(use_weighted) {
        double err=pair_iter->rms;
        b[counter]=pair_iter->mean/err;
        A[counter][imageid_col[pair_iter->id1]]=1/err;
        A[counter][imageid_col[pair_iter->id2]]=-1/err;
      }
      else {
        b[counter]=pair_iter->mean;
        A[counter][imageid_col[pair_iter->id1]]=1;
        A[counter][imageid_col[pair_iter->id2]]=-1;
      }
      counter++;
    }
    }



    if(use_sky) {
      pair_iter=image_pairs_sky.begin();
      for( ; pair_iter!=image_pairs_sky.end();pair_iter++) {
        double mean=pair_iter->mean;
        double err=1;
        if(use_weighted) err=pair_iter->rms;
        
        b[counter]=pair_iter->mean/err;
        A[counter][imageid_col[pair_iter->id1]]=1/err;
        A[counter][imageid_col[pair_iter->id2]]=-1/err;
        counter++;
      }
    }
      
    
      
    
    if(debug>3) {
      cout<<"A:="<<endl;
      A.Print();
      cout<<endl<<"b:="<<endl;
      b.Print();
    }
    TDecompSVD svd(A,svd_tol);
    bool ok;
    TVector solution=svd.Solve(b,ok);

    
      

    if(!ok && !add_missing) {
      string evt="Error in SVD solove.  Exiting...";
      report_evt(true,"STATUS",5,evt);
      exit(1);
    }
    // vectors
    vector<int> bad_id_list;
    TMatrixD Cov(ncol,ncol);
    TVectorD w_inv(ncol);
    if(ok) {
      
      if(debug >3) {
        cout<<"Solution:="<<endl;
        solution.Print();
      }
    
    
      cout<<endl;
      cout<<"**************************************************"<<endl;
      cout<<"Results:"<<endl;
      cout<<"**************************************************"<<endl;
      
      // caluate error array by V*1/w*VT
      TMatrixD V(svd.GetV());
      TVectorD w(svd.GetSig());
      
      if(debug>3) {
        cout<<"w:="<<endl;
        w.Print();
        cout<<endl<<"V:="<<endl;
        V.Print();
      }

      
      for(int i=0;i<ncol;i++) {
        if(w[i]>1e-12) {
          w_inv[i]=1./(w[i]*w[i]);
        }
      };
      
      //TMatrixD Vw(V,TMatrixD::kMult,w_inv);
      //TMatrixD Vw=V*w_inv;
      //TMatrixD Err(Vw,TMatrixD::kTransposeMult,V);
      
      // Calculate the covariance Matrix
      

      for(int i=0;i<ncol;i++){ 
        for(int j=0;j<=i;j++){ 
          double sum;
          int k;
          for(sum=0.0,k=0;k<ncol;k++) sum+=V(i,k)*V(j,k)*w_inv(k); 
          Cov(j,i)=sum;
          Cov(i,j)=sum;
        } 
      } 
      

      // Case where only one image and using psm zeropoints
      if(n_photo==1 && n_match==0 && use_psm) {
        map<int,ImageInfo>::iterator image_iter=ImageList.begin();
        solution(0)=image_iter->second.zp;
        Cov(0,0)=TMath::Power(image_iter->second.zp_err,2);
      }


      // Case where only one image and using relative calibration
      if(n_match==0 && n_exp_ave==0 && 
       n_zp_diff==0 && n_sky==0) {
        map<int,ImageInfo>::iterator image_iter=ImageList.begin();
        solution(0)=25;
        Cov(0,0)=0.01;
      }

      counter=0;
      int n_fail=0;
      image_iter=ImageList.begin();

      for(; image_iter!=ImageList.end(); ++image_iter) {
        
        image_iter->second.coadd_zp=solution(counter);
        image_iter->second.coadd_zp_err=Cov(counter,counter);
        
        cout<<image_iter->second.id<<"  ZP = "
            <<image_iter->second.coadd_zp<<" +- "
            <<sqrt(Cov(counter,counter))<<endl;       
        // check for failed images
        if(image_iter->second.coadd_zp<10 ) {
          
          bad_id_list.push_back(image_iter->second.id);
          string evt=Form("ZP failed on image : %d",
                          image_iter->second.id);  
          n_fail++;
          if(!add_missing) cout<<evt<<endl;
          else {
            report_evt(true,"STATUS",4,evt);
          }

        }
        
        
        counter++;
      }
      
      cout<<endl<<"ZP failed on "<<n_fail<<" images"<<endl;

    }

    bool redo=false;
    
    if(add_missing && (bad_id_list.size()>0 || !ok) && !skip_second) {

      
      // remove all the same exposure constraints that are not in the
      // bad id list if there are more than ~6 just add in all of the
      // constraints
      if(bad_id_list.size()>0) {
         pair_iter=ImageCCDs.begin();
         while ( pair_iter!=ImageCCDs.end()) {
             
           vector<int>::iterator iter1=find(bad_id_list.begin(),
                                            bad_id_list.end(),
                                            pair_iter->id1);
           vector<int>::iterator iter2=find(bad_id_list.begin(),
                                            bad_id_list.end(),
                                            pair_iter->id2);
           
           if( (iter1==bad_id_list.end() && iter2==bad_id_list.end()) ||
               (project=="SPT" && (ImageList[pair_iter->id1].ccd==4
                                   || ImageList[pair_iter->id2].ccd==4))) {
             
             ImageCCDs.erase(pair_iter);
           }
           else pair_iter++;
         }
       }
      if(ImageCCDs.size()>0) redo=true;
    }
    
    if(redo) {
      cout<<"Found failed images or SVD failed.  Using good seeing average to"
          << " bring them back"<<endl;
    // reset the zeropoints and build the matrix again
       image_iter=ImageList.begin();
       for(; image_iter!=ImageList.end(); ++image_iter) {
          image_iter->second.coadd_zp=0;
       }
       
       n_zp_diff=ImageCCDs.size();

              
       nrow=n_photo+n_match+n_exp_ave+n_zp_diff;

              
       cout<<"\n\n***** Using the Following Constraints***********"<<endl;
       cout<<"Direct ZP: "<<n_photo;
       cout<<"    Relative Pairs: "<<n_match;
       cout<<"    Averaged Pairs: "<<n_exp_ave;
       cout<<"    CCD Diff: "<<n_zp_diff<<endl;
    
       A.Clear();
       b.Clear();
       A.ResizeTo(nrow,ncol);
       b.ResizeTo(nrow);
       // Assign the zeropoints
       image_iter=ImageList.begin();

       counter=0;
       
       if(!use_psm) {

         int image_id;
         
         if(ImageMatches.size()>0) {
           image_id=ImageMatches[0].id1;
         }
         else {
           image_id=(*ImageList.begin()).second.id;
         }


         if(use_weighted) {
           double err=0.001;
           b(counter)=25./err;
           A(counter,imageid_col[image_id])=1/err;
         }
         else {
           b(counter)=25;
           A(counter,imageid_col[image_id])=1;
         }
         counter++;
       }
       else {
         map<int,ImageInfo>::iterator image_iter=ImageList.begin();
         for(; image_iter!=ImageList.end(); ++image_iter) {
           
           if(image_iter->second.zp!=-1) {
             double zp,zperr;
             zp=image_iter->second.zp;
             if(use_weighted) zperr=image_iter->second.zp_err+psm_sys;
             else zperr=1;
             
             b(counter)=zp/zperr;
             A(counter,imageid_col[image_iter->second.id])=1/zperr;
             counter++;
           }
         }
       }
       
       if(use_rel) {
         //Assign the pair constraints
         pair_iter=ImageMatches.begin();
         for( ; pair_iter!=ImageMatches.end();pair_iter++) {
           // if not using the exposure average or the ccd numbers
           // are different use the regular constraint.
           if(use_weighted) {
             b[counter]=pair_iter->mean/pair_iter->rms;
             A[counter][imageid_col[pair_iter->id1]]=1/pair_iter->rms;
             A[counter][imageid_col[pair_iter->id2]]=-1/pair_iter->rms;
           }
           else {
             b[counter]=pair_iter->mean;
             A[counter][imageid_col[pair_iter->id1]]=1;
             A[counter][imageid_col[pair_iter->id2]]=-1;
           }
           counter++;
         }
       }
       
       if(use_exp_ave) {
          pair_iter=AvePairs.begin();
          for( ; pair_iter!=AvePairs.end();pair_iter++) {
             
             ExpPair exp(ImageList[pair_iter->id1].exp_name,
                         ImageList[pair_iter->id2].exp_name);
             vector<ExpPair>::iterator iter;
             
             
             if(!use_best) {
                // find the particular exposure pair
                iter=find(SameNiteExpPairs.begin(),SameNiteExpPairs.end(),
                          exp);
                if(iter==SameNiteExpPairs.end()) {
                   cout<<"Probelm finding average exposure difference for images: "
                       <<pair_iter->id1<<" "<<pair_iter->id2<<endl;
                   continue;
                }
             }
             else {
                iter=find(BestExpPairs.begin(),BestExpPairs.end(),
                          exp);
                if(iter==BestExpPairs.end()) {
                   cout<<"Probelm finding average exposure difference for images: "
                       <<pair_iter->id1<<" "<<pair_iter->id2<<endl;
                   continue;
                }
             }
             
             double mean;
             double err;
             
             mean=iter->mean;
             err=iter->rms;
             
             if(use_weighted) {
                b[counter]=mean/err;
                A[counter][imageid_col[pair_iter->id1]]=1/err;
                A[counter][imageid_col[pair_iter->id2]]=-1/err;
             }
             else {
                b[counter]=mean;
                A[counter][imageid_col[pair_iter->id1]]=1;
                A[counter][imageid_col[pair_iter->id2]]=-1;
             }
             
             counter++;
          }
       }
       
       
       // Add these in
       // Assign zp diff constraints
       pair_iter=ImageCCDs.begin();
       for( ; pair_iter!=ImageCCDs.end();pair_iter++) {
         if(use_weighted) {
             double err=pair_iter->rms;
             b[counter]=pair_iter->mean/err;
             A[counter][imageid_col[pair_iter->id1]]=1/err;
             A[counter][imageid_col[pair_iter->id2]]=-1/err;
          }
          else {
             b[counter]=pair_iter->mean;
             A[counter][imageid_col[pair_iter->id1]]=1;
             A[counter][imageid_col[pair_iter->id2]]=-1;
          }
          counter++;
       }
       
       
       if(use_sky) {
         pair_iter=image_pairs_sky.begin();
         for( ; pair_iter!=image_pairs_sky.end();pair_iter++) {
           double mean=pair_iter->mean;
           double err=1;
           if(use_weighted) err=pair_iter->rms;
           
           b[counter]=pair_iter->mean/err;
           A[counter][imageid_col[pair_iter->id1]]=1/err;
           A[counter][imageid_col[pair_iter->id2]]=-1/err;
           counter++;
         }
       }
       

       
       if(debug>3) {
          cout<<"A:="<<endl;
          A.Print();
          cout<<endl<<"b:="<<endl;
          b.Print();
       }

       svd.Clear();
       svd.SetMatrix(A);
       
       
       TVector solution2=svd.Solve(b,ok);

       if(!ok) {
         string evt="Error in SVD solove.  Exiting...";
         report_evt(true,"STATUS",5,evt);
          exit(1);
       }
       
       
       if(debug>3) {
          cout<<"Solution:="<<endl;
          solution2.Print();
       }
       
              
       // caluate error array by V*1/w*VT
       TMatrixD V(svd.GetV());
       TVectorD w(svd.GetSig());
      
       
       if(debug>4) {
          cout<<"w:="<<endl;
          w.Print();
          cout<<endl<<"V:="<<endl;
          V.Print();
       }
       w_inv.Clear();
       w_inv.ResizeTo(ncol);
       //TVectorD w_inv(ncol);
       for(int i=0;i<ncol;i++) {
          if(w[i]>1e-12) {
             w_inv[i]=1./(w[i]*w[i]);
          }
       };
       
    // Calculate the covariance Matrix
       Cov.Clear();
       Cov.ResizeTo(ncol,ncol);
       
       for(int i=0;i<ncol;i++){ 
         for(int j=0;j<=i;j++){ 
           double sum;
           int k;
           for(sum=0.0,k=0;k<ncol;k++) sum+=V(i,k)*V(j,k)*w_inv(k); 
           Cov(j,i)=sum;
           Cov(i,j)=sum;
         } 
       } 
       
    
    
       counter=0;
       for(; image_iter!=ImageList.end(); ++image_iter) {
         cout<<image_iter->second.exp_name<<"_"<<image_iter->second.ccd;
         
         // if(image_iter->second.ccd==4 && project=="SPT") {
//            cout<<"   Changing zeropoint to 0 for ccd 4"<<endl;
//            solution2(counter)=0;
//            Cov(counter,counter)=0;
         //}
         cout<<"  ZP = "<<solution2(counter)<<" +- "
             <<sqrt(Cov(counter,counter))<<endl;
         image_iter->second.coadd_zp=solution2(counter);
         image_iter->second.coadd_zp_err=Cov(counter,counter);

         if(image_iter->second.coadd_zp<10 ) {
          
           string evt=Form("ZP failed on image : %d",
                           image_iter->second.id);  
           report_evt(true,"STATUS",4,evt);
         }
         counter++;
       }
       
       solution.Clear();
       solution.ResizeTo(solution2.GetNrows());
       solution=solution2;
    }
    
//     cout<<"***************************"<<endl;
//     cout<<endl<<"Check "<<endl;
//     cout<<"***************************"<<endl;
//     pair_iter=ImageMatches.begin();
//     for( ; pair_iter!=ImageMatches.end();pair_iter++) {
      
//       int id1=pair_iter->id1;
//       int id2=pair_iter->id2;
//       double p_mean=pair_iter->mean;
//       double p_rms=pair_iter->rms;
//       double zp_diff=ImageList[id1].coadd_zp-ImageList[id2].coadd_zp;

//       if(abs(zp_diff-p_mean) > 3*p_rms) {
//         cout<<"Warning: Zeropoint confict with relative constraint diff:="
//             <<zp_diff-p_mean<<endl;
        
//       }
//         cout<<ImageList[id1].exp_name<<"_"<<ImageList[id1].ccd<<"  "
//             <<ImageList[id2].exp_name<<"_"<<ImageList[id2].ccd<<endl;
//         cout<<"  "<<zp_diff-p_mean<<" --  zp:"
//             <<zp_diff<<" rel mean:"
//             <<p_mean<<" rel rms:"<<p_rms<<endl;
      
      
//     }

       if(psm_cal) {
          vector<double> zp_diff;
          double zp_coadd,zp_psm,diff;
          image_iter=ImageList.begin();
          for(; image_iter!=ImageList.end(); ++image_iter) {
            // make sure they have a valid zeropoint
            // 10 should be sufficient to catch errors
            // and psm which should be > -1
             if(image_iter->second.coadd_zp>10 && 
                image_iter->second.zp>0) {
                zp_coadd=image_iter->second.coadd_zp;
                zp_psm=image_iter->second.zp;
                diff=zp_psm-zp_coadd;
                zp_diff.push_back(diff);
             }
          }
          
          if(zp_diff.size()==0) {
            string evt="No PSM zeropoints found.  Not applying PSM calibration correction";

            report_evt(true,"STATUS",4,evt);
          }
          else {
             double mean=TMath::Mean(zp_diff.size(),&zp_diff[0]);
             double rms=TMath::RMS(zp_diff.size(),&zp_diff[0]);
             double clip_tol=1e-6;
             int max_iter=1000;
             double thresh=2.5;
             double clipped_mean,clipped_rms;
             int good,bad;
             bool clip=true,weight=false;
             vector<double> zp_diff_err(zp_diff.size());
             sigma_clip(max_iter,thresh,zp_diff,zp_diff_err,clipped_mean,
                        good,bad,clipped_rms,clip,weight,clip_tol);
             
             cout<<endl<<"After Calibration with PSM"<<endl;
             cout<<"Mean Difference from PSM: "<<mean<<endl;
             cout<<"RMS from PSM: "<<rms<<endl;
             cout<<"Clipped Mean Difference from PSM: "<<clipped_mean<<endl;
             cout<<"Clipped RMS from PSM: "<<clipped_rms<<endl;
             
             image_iter=ImageList.begin();
             
             for(; image_iter!=ImageList.end(); ++image_iter) {
                // only apply correction if solution was found
                if(image_iter->second.coadd_zp>1) {
                   image_iter->second.coadd_zp+=clipped_mean;
                }
                cout<<image_iter->second.id<<"  ZP = "
                    <<image_iter->second.coadd_zp<<" +- "
                    <<image_iter->second.coadd_zp_err<<endl;
             }
          }
          

       }


       cout<<endl;
       cout<<"**************************************************"<<endl;
       cout<<"Images that were thrown out:"<<endl;
       cout<<"**************************************************"<<endl;
       image_iter=ZeroList.begin();
       
       for(; image_iter!=ZeroList.end(); ++image_iter) {
         cout<<image_iter->second.id<<"  ZP = 0"<<endl;

       }


    
    if(!zp_file.empty()) {
       ofstream zp(zp_file.c_str());
       counter=0;
       image_iter=ImageList.begin();
       for(; image_iter!=ImageList.end(); ++image_iter) {
          zp<<image_iter->second.id<<" "<<image_iter->second.coadd_zp<<endl;
          counter++;
       }
       
    }
       
       
       if(insert_db) {

          db = TSQLServer::Connect(db_string,db_user.c_str(), db_passwd.c_str());
          db->StartTransaction();

          image_iter=ImageList.begin();
          int counter=0;
          for(; image_iter!=ImageList.end(); ++image_iter) {
             int id=image_iter->second.id;
             double zp=image_iter->second.coadd_zp;
             double zp_err=image_iter->second.coadd_zp_err;
             counter++;
             
             // insert all zeropoints into the table for SPT and 
             // then filter images based on the zeropoint in coadd_swarp
             //if(zp<zp_min || zp>zp_max && project!="SPT") {
             //   cout<<"Not ingesting id="<<id<<" zeropoint "
             //       <<zp<<" out of range\n";
             //   continue;
             // }
             
             // Oracle and Postgres uses different sequencer and sysdate syntax
             string nextval = "zeropoint_seq.nextval";
             string sysdate = "sysdate";
             if (which_db=="pg") {
                nextval = "nextval('zeropoint_seq')";
                sysdate = "current_timestamp";
             }
             string db_cmd=Form("insert into zeropoint (id,imageid,mag_zero,"
                                "sigma_mag_zero,source,insert_date,tilename)"
                                "values (%s,%d,%f,%f,'COADD',%s,'%s')",
                                nextval.c_str(),id,zp,zp_err,sysdate.c_str(),tile.c_str());
             if(debug) cout<<db_cmd<<endl;
             stmt=db->Statement(db_cmd.c_str());
             stmt->Process();
          }
          
          image_iter=ZeroList.begin();
          for(; image_iter!=ZeroList.end(); ++image_iter) {
             int id=image_iter->second.id;
             double zp=0;
             double zp_err=0;
             
             string db_cmd=Form("insert into zeropoint (id,imageid,mag_zero,"
                                "sigma_mag_zero,source,insert_date,tilename)"
                                "values (zeropoint_seq.nextval,%d,%f,%f,"
                                "'COADD',sysdate,'%s')",
                                id,zp,zp_err,tile.c_str());
             if(debug) cout<<db_cmd<<endl;
             stmt=db->Statement(db_cmd.c_str());
             stmt->Process();
             
          }
          db->Commit();
          db->Close();  
       }
       // now go back and see how different the constraints are
       
       if(!rootfile.empty()) {
          TFile outfile(rootfile.c_str(),"recreate");
          A.Write("A");
          b.Write("b");
          svd.Write("svd");
          Cov.Write("Cov");
          pair_tree->Write();
          ob_tree->Write();
          
          if(compare_psm) {
             double coadd_zp,psm_zp,psm_zp_err,old_zp,old_zp_err,true_zp;
             char id[20],exp_ccd[50];
             TTree *tree=new TTree("tree","");
             tree->Branch("coadd_zp",&coadd_zp,"coadd_zp/D");
             if(project=="BCS" || project=="DES" || project=="SCS") {
                tree->Branch("old_zp",&old_zp,"old_zp/D");
                tree->Branch("old_zp_err",&old_zp_err,"old_zp_err/D");
             }
             tree->Branch("psm_zp",&psm_zp,"psm_zp/D");
             tree->Branch("true_zp",&true_zp,"true_zp/D");
             tree->Branch("psm_zp_err",&psm_zp_err,"psm_zp_err/D");
             tree->Branch("id",&id,"id/C");
             tree->Branch("exp",&exp_ccd,"exp/C");
             map<int,ImageInfo>::iterator image_iter=ImageList.begin();
             for(; image_iter!=ImageList.end(); ++image_iter) {
                
                db = TSQLServer::Connect(db_string,
                                         db_user.c_str(), db_passwd.c_str());
                
                db_cmd=Form("select a.mag_zero,a.sigma_mag_zero,c.photflag from "
                            "zeropoint a, image b, exposure c where a.imageid=%d and "
                            " b.id=a.imageid and a.source='PSM' and "
                            " b.exposureid=c.id and c.photflag=1 order by insert_date"
                            ,image_iter->second.id);
                if(debug)cout<<db_cmd<<endl;
                stmt=db->Statement(db_cmd.c_str());
                
                // Check for errors in the statement
                if(!stmt->Process()) {
                   cout<<stmt->GetErrorMsg()<<endl;
                   exit(1);
                }
                stmt->StoreResult();
                
                // check how many results
                image_iter->second.zp=-1;
                while(stmt->NextResultRow()) {
                   
                   image_iter->second.zp=stmt->GetDouble(0);
                   image_iter->second.zp_err=stmt->GetDouble(1);
                }
                
                true_zp=image_iter->second.true_zp;
                psm_zp=image_iter->second.zp;
                psm_zp_err=image_iter->second.zp_err;
                coadd_zp=image_iter->second.coadd_zp;
                
                
                if(project=="BCS" || project=="DES" || project=="SCS") {
                   
                   db_cmd=Form("select a.mag_zero,a.sigma_mag_zero,c.photflag from "
                               "zeropoint a, image b, exposure c where a.imageid=%d and "
                               " b.id=a.imageid and a.source='COADD' and "
                               " b.exposureid=c.id order by insert_date"
                               ,image_iter->second.id);
                   if(debug)cout<<db_cmd<<endl;
                   stmt=db->Statement(db_cmd.c_str());
                   
                   // Check for errors in the statement
                   if(!stmt->Process()) {
                      cout<<stmt->GetErrorMsg()<<endl;
                      exit(1);
                   }
                   stmt->StoreResult();
                   
                   // check how many results
                   old_zp=-1;
                   old_zp_err=-1;
                   while(stmt->NextResultRow()) {
                      old_zp=stmt->GetDouble(0);
                      old_zp_err=stmt->GetDouble(1);
                   }
                   
                   
                }
                
                sprintf(id,Form("%d",image_iter->second.id));
                sprintf(exp_ccd,Form("%s_%d",image_iter->second.exp_name.c_str(),image_iter->second.ccd));
                cout<<psm_zp<<" "<<coadd_zp<<" "<<old_zp<<endl;
                tree->Fill();
             }
             
             
             db->Close();
             tree->Write("tree");
             
          }
          
          
          solution.Write("sol");
       
          vector<int> n_ov;
          vector<int>::iterator vint_iter;
          pair_iter=ImageMatches.begin();
          
          for( ; pair_iter!=ImageMatches.end();pair_iter++) {

            vint_iter=find(n_ov.begin(),n_ov.end(),pair_iter->id1);
            if(vint_iter==n_ov.end()) n_ov.push_back(pair_iter->id1);

            vint_iter=find(n_ov.begin(),n_ov.end(),pair_iter->id2);
            if(vint_iter==n_ov.end()) n_ov.push_back(pair_iter->id2);
          }

          int nimages=n_ov.size();
          TH1D *h1=new TH1D("h1","",100,1,-1);
          TH2D *h2=new TH2D("h2","",nimages,0,nimages,
                            nimages,0,nimages);


          // Compare pair constraints to the zeropoint corrected values
          cout<<"\n**************************************************"<<endl;
          cout<<"Comparison"<<endl;
          cout<<"**************************************************"<<endl;
          pair_iter=ImageMatches.begin();
          vector<double> v_diff;
          for( ; pair_iter!=ImageMatches.end();pair_iter++) {
            
            int id1=pair_iter->id1;
            int id2=pair_iter->id2;
            double new_zp1=ImageList[id1].coadd_zp;
            double new_zp2=ImageList[id2].coadd_zp;
            double new_zp1_err=ImageList[id1].coadd_zp_err;
            double new_zp2_err=ImageList[id2].coadd_zp_err;
            
            double diff=pair_iter->mean-(new_zp1-new_zp2);
            v_diff.push_back(diff);
            h1->Fill(diff);
            h2->Fill(Form("%d",id1),Form("%d",id2),diff);
          }

          double min=h2->GetMinimum()-0.02;
        
          for(int i=0;i<nimages;i++) {
            for(int j=0;j<nimages;j++) {
              if(fabs(h2->GetBinContent(i+1,j+1))==0) {
                
                h2->SetBinContent(i+1,j+1,-999);
              }
            }
          } 



          
          // Identify mean rms
          vector<double> diff_err(v_diff.size());
          double mean,rms;
          int good,bad;
          
          sigma_clip(max_iter,3,v_diff,diff_err,mean,
                     good,bad,rms,true,false,clip_tol);

          string evt=Form("Residual Scatter=%0.3f, Number Clipped=%d/%d",
                          rms,bad,(good+bad));
          report_evt(true,"QA",3,evt);

          map<int,int> n_bad;

          pair_iter=ImageMatches.begin();
          for( ; pair_iter!=ImageMatches.end();pair_iter++) {

            int id1=pair_iter->id1;
            int id2=pair_iter->id2;
            double new_zp1=ImageList[id1].coadd_zp;
            double new_zp2=ImageList[id2].coadd_zp;
            double diff=pair_iter->mean-(new_zp1-new_zp2);
            
            if( fabs(diff-mean) > thresh*rms) {
              n_bad[id1]++;
              n_bad[id2]++;
            }
          }

          map<int,int>::iterator i_i_iter=n_bad.begin();            
          for(;i_i_iter!=n_bad.end();i_i_iter++) {

            if(i_i_iter->second>2) {
              string evt=Form("id=%d Number of clipped overlap constraints"
                              "=%d",i_i_iter->first,i_i_iter->second);
              report_evt(true,"STATUS",3,evt);
            }
          }




          h2->SetMinimum(min);
          h1->Write();
          h2->Write();
          outfile.Close();
          
       }
       
       
       timer.Print();
}

 
// Set style

void LoadStyle()
{


  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  TStyle* miStyle = new  TStyle("miStyle", "MI Style");

  // Colors


  //  set the background color to white
  miStyle->SetFillColor(10);


  miStyle->SetFrameFillColor(10);
  miStyle->SetCanvasColor(10);
  miStyle->SetCanvasDefH(680);
  miStyle->SetCanvasDefW(700);
  miStyle->SetPadColor(10);
  miStyle->SetTitleFillColor(0);
  miStyle->SetStatColor(10);

  //  //dont put a colored frame around the plots
  miStyle->SetFrameBorderMode(0);
  miStyle->SetCanvasBorderMode(0);
  miStyle->SetPadBorderMode(0);

  //use the primary color palette
  miStyle->SetPalette(1);

  //set the default line color for a histogram to be black
  miStyle->SetHistLineColor(kBlack);

  //set the default line color for a fit function to be red
  miStyle->SetFuncColor(kBlue);

  //make the axis labels black
  miStyle->SetLabelColor(kBlack,"xyz");

  //set the default title color to be black
  miStyle->SetTitleColor(kBlack);

  // Sizes
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);


  //set the margins
  miStyle->SetPadBottomMargin(0.12);
  miStyle->SetPadTopMargin(0.1);
  miStyle->SetPadLeftMargin(0.14);
  miStyle->SetPadRightMargin(0.14);

  //set axis label and title text sizes
  miStyle->SetLabelSize(0.04,"x");
  miStyle->SetLabelSize(0.04,"y");
  miStyle->SetTitleSize(0.05,"xyz");
  miStyle->SetTitleOffset(1.1,"x");
  miStyle->SetTitleOffset(1.3,"yz");
  miStyle->SetLabelOffset(0.012,"y");
  miStyle->SetStatFontSize(0.025);
  miStyle->SetTextSize(0.02);
  miStyle->SetTitleBorderSize(0);

  //set line widths
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);

  // Misc

  //align the titles to be centered
  miStyle->SetTextAlign(22);

  //set the number of divisions to show
  // miStyle->SetNdivisions(506, "xy");

  //turn off xy grids
  miStyle->SetPadGridX(0);
  miStyle->SetPadGridY(0);

  //set the tick mark style
  miStyle->SetPadTickX(1);
  miStyle->SetPadTickY(1);

  //show the fit parameters in a box
  miStyle->SetOptFit(0111);
  miStyle->SetOptTitle(0);

  //turn off all other stats
  miStyle->SetOptStat(0);
  miStyle->SetStatW(0.20);
  miStyle->SetStatH(0.15);
  miStyle->SetStatX(0.94);
  miStyle->SetStatY(0.92);


  miStyle->SetFillStyle(0);

  //  // Fonts
  miStyle->SetStatFont(42);
  miStyle->SetLabelFont(42,"xyz");
  miStyle->SetTitleFont(42,"xyz");
  // miStyle->SetTextFont(40);

  //done

  miStyle->cd();


  // gROOT->ForceStyle(1);
 

}

void PrintSLRHelp()
{
  cout<<"Usage: slr_cal -tile (tile) (options)\n";
  cout<<"Options (default):\n";
  cout<<"  -project    (none)       For looping over all tiles in BCS/DES.\n";
  cout<<"  -run        (latest)     Use a specific run.   \n";
  cout<<"  -color      (no)         Apply BCS color corrections\n";
  cout<<"  -debug      (no)         Print out debug information\n";
  cout<<"  -deug_color (no)         Put to screen each color-color fit\n";
  cout<<"  -mag        (auto)       Which magnitude to use\n";
  cout<<"  -star_cut   (0.7)        Value of class_star to determine stars\n";
  cout<<"  -err_cut    (0.2)        Maximum flux error\n";
  cout<<"  -outfile    (out.root)   Output file (only if using -project)\n";
  cout<<"  -extinction (no)         Apply extinction correction\n";

}



void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,double &tol)
{
  int N=array.size();
  if(!weighted) {
    mean=TMath::Mean(N,&array[0]);
  }
  else {
    mean=TMath::Mean(N,&array[0],&err[0]);
  }
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }
  good=N-bad;
  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;
  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    

    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();
    newerr_array.clear();

    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    curerr_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;


    
  }
 

  
  return;

}

void Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters)
{
  // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}

void report_evt(bool verbose,string type,int level,string mess)
{

  if( verbose) {
    if(type=="STATUS") {
      cout<<"STATUS"<<level<<"BEG "<<mess<<" STATUS"<<level<<"END"<<endl;
    }
    else if(type=="QA") {
      cout<<"QA"<<level<<"BEG "<<mess<<" QA"<<level<<"END"<<endl;
    }
    else {
      cout<<"STATUS5BEG Uknown event type "<<type<<endl;
    }
  }
  else {
    if(level==5) cout<<"** "<<mess<<" **"<<endl;
    else cout<<"  "<<mess<<endl;
  }
}

