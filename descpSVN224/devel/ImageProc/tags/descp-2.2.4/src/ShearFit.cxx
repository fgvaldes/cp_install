///
/// \file    ShearFit.cxx
/// \brief   Vertex and n-prong fitting
/// \version $Id: ShearFit.cxx,v 1.3 2009/06/29 19:09:45 messier Exp $
/// \author  messier@indiana.edu
///
#include "ShearFit.h"
#include <iostream>
#include <cmath>
#include "Minuit2/Minuit2Minimizer.h"
#include <vector>
#include "TFile.h"
#include "TGraph.h"

using namespace std;
double ShearFit::DoEval(const double* par) const
{
  double sum=0;
  if(!quad) {
    double a=par[0];
    double b=par[1];
    double c=par[2];
    double difference;
    sum=0;
    for(int i=0;i<entries; i++) {
      difference=( (a*x[i]+b*y[i]+c)-diff[i]);
      sum+=difference*difference;
    }
    return sum;
  }
  else {
    double a=par[0];
    double b=par[1];
    double c=par[2];
    double d=par[3];
    double e=par[4];
    double f=par[5];
    double sum=0;
    
    double difference;
    
    for(int i=0;i<entries; i++) {
      
      difference=( (a*x[i]+b*y[i]+c+d*x[i]*x[i]+e*x[i]*y[i]+f*y[i]*y[i])
		   -(diff[i]));
      sum+=difference*difference;
    }    
    return sum;
  }
  
}

 
vector<double> ShearFit::Fit(const vector<double> *_diff, 
			     const vector<double> *_x,
			     const vector<double> *_y,
			     vector<double> &errs)
{
  // Set data

  entries=(*_x).size();

  //ROOT::Math::GSLMinimizer mini(ROOT::Math::kVectorBFGS);

  ROOT::Minuit2::Minuit2Minimizer mini(ROOT::Minuit2::kCombined);
  //ROOT::Minuit2::Minuit2Minimizer mini;
  mini.SetPrintLevel(0);
  mini.SetTolerance(0.00001);
  mini.SetStrategy(2);
  mini.SetMaxFunctionCalls(10000);
  
  if(!quad) {
    x=*_x;
    y=*_y;
    diff=*_diff;
    
    mini.SetLimitedVariable(0, "a", 0,0.0001, -1, 1);
    mini.SetLimitedVariable(1, "b", 0,0.0001, -1, 1);
    mini.SetLimitedVariable(2, "c", 0,0.0001, -1, 1);
    
    mini.SetFunction(*this);
    mini.Minimize();
    
    double a=mini.X()[0];
    double b=mini.X()[1];
    double c=mini.X()[2];
    
    const double *errors=mini.Errors();

    double err_low,err_hi;
        

    double chi2=mini.MinValue();
    double ndf=entries-2;
    for(int i=0;i<3;i++) {
      double err=errors[i]*sqrt(chi2/(ndf-1));
      errs.push_back(err);
    }
    
    par.push_back(a);
    par.push_back(b);
    par.push_back(c);
    
    
    
    //double *xx = new double[100];
    //double *yy = new double[100];
   //  vector<double> xx(100),yy(100);
//     unsigned int pp=0,nstep=100;
//     mini.Scan(pp,nstep,&xx[0],&yy[0],0.,0.);
    
//     TGraph gr;
//     for(int i=0;i<100;i++) gr.SetPoint(i,xx[i],yy[i]);
    
//     TFile t("t.root","update");
//     gr.Write("gr");
//     t.Close();
    
    
    
    
    
    return par;
  }
  


  // Test quadratic
 else {
   quad=true;
   x=*_x;
   y=*_y;
   diff=*_diff;
   mini.SetLimitedVariable(0, "a", 0,0.0001, -1, 1);
   mini.SetLimitedVariable(1, "b", 0,0.0001, -1, 1);
   mini.SetLimitedVariable(2, "c", 0,0.0001, -1, 1);
   mini.SetLimitedVariable(3, "g", 0,0.0001, -1, 1);
   mini.SetLimitedVariable(4, "h", 0,0.0001, -1, 1);
   mini.SetLimitedVariable(5, "i", 0,0.0001, -1, 1);
   
   mini.SetFunction(*this);
   mini.Minimize();
   const double *errors=mini.Errors();  
   float a=mini.X()[0];
   float b=mini.X()[1];
   float c=mini.X()[2];
   double g=mini.X()[3];
   double h=mini.X()[4];
   double i=mini.X()[5];
   
   par.push_back(a);
   par.push_back(b);
   par.push_back(c);
   par.push_back(g);
   par.push_back(h);
   par.push_back(i);


   
   double chi2=mini.MinValue();
   double ndf=entries-2;
   for(int i=0;i<6;i++) {
     double err=errors[i]*sqrt(chi2/(ndf-1));
     errs.push_back(err);
   }
   
   
   return par;
   
 }
  // Test quadratic

 


//   //double *xx = new double[100];
//   //double *yy = new double[100];
//   vector<double> xx(100),yy(100);
//   unsigned int pp=0,nstep=100;
//   mini.Scan(pp,nstep,&xx[0],&yy[0],0.,0.);

//   TGraph gr;
//   for(int i=0;i<100;i++) gr.SetPoint(i,xx[i],yy[i]);

//   TFile t("t.root","recreate");
//   gr.Write("gr");
//   t.Close();

//return mini.MinValue();


  
}

