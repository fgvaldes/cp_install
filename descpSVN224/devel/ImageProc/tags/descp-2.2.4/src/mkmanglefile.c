/*
** mkmanglefile.c
**
** Program to produce input files for mangle
**
** usage:  mkmanglefile --tile <TILENAME> --run <RUNID> --outfile <OUTFILE>
**
** If outfile is not supplied, the a default file named mangle.ccd
** is written in the current directory
**
** OUTPUTS:
**   mangle.ccd
**
** The mangle.tile file contains a list of all DES tile in the following
** format:
**   TILENAME|CENTERRA|CENTERDEC
**
** The first line in the mangle.ccd file explains each row.
**
** AUTHOR:  Tony Darnell (tdarnell@illinois.edu)
** DATE:  20 July 2009
**
** $Rev:: 6763                                        $ Revision of last commit
** $Author:: mgower                                   $ Author of last commit
** $Date:: 2011-05-18 09:46:06 -0700 (Wed, 18 May 201#$ Date of last commit
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>

#include "ora_utils.h"
#include "fitswcs.h"

struct globalArgs_t {
        int  year;
        char *tile;
        char *run;
        char *range;
        int  verbosity;
        char *outfile;
} globalArgs;

static const char *optString = "y:t:r:vh?";

static const struct option longOpts[] = {
        { "year", required_argument, NULL, 'y' },
        { "tile", required_argument, NULL, 't' },
        { "run", required_argument, NULL, 'r' },
        { "range", required_argument, NULL, 'a' },
        { "verbose", no_argument, NULL, 'v' },
        { "help", no_argument, NULL, 'h' },
        { "outfile", required_argument, NULL, 'o' },
        { NULL, no_argument, NULL, 0 }
};

//
// Prototypes
//
void usage();

int main(int argc, char* argv[]) 
{
  sword status;
  int opt,i,j,k, longIndex = 0;
  int coaddTileId, coaddId, imageId, reducedId, expId, rawId, normId, numCoadds = 0, 
      numTiles = 0, numImgs = 0;
  char errbuf[100],tileoutname[100],ccdoutname[100];
  char *currentType, *tag;
  int errcode;
  double ra, dec, zeroPoint = 0.0, coaddZeropoint;
  double wcspos[2],rawpos[2];

  FILE *foutp;

  parentStruct  *parent;
  parentStruct  *parentForRaw;
  tileStruct    *desTileArr;
  imageIdStruct *coaddIdArr;
  imageIdStruct *imageIdArr;
  imageInfo     *imageHeader;
  imageInfo     *rawHeader;
  imageInfo     *normHeader;
  //struct WorldCoor     *wcs;
  double *corners;

  coaddIdArr  = (imageIdStruct*) malloc(sizeof(imageIdStruct));
  imageIdArr  = (imageIdStruct*) malloc(sizeof(imageIdStruct));
  imageHeader = (imageInfo*) malloc(sizeof(imageInfo));
  rawHeader   = (imageInfo*) malloc(sizeof(imageInfo));
  normHeader   = (imageInfo*) malloc(sizeof(imageInfo));
  parent      = (parentStruct*) malloc(sizeof(parentStruct));
  parentForRaw = (parentStruct*) malloc(sizeof(parentStruct));
  corners = (double *) malloc(sizeof(position) * 4);

  //
  // Bind handles
  //
  static OCIBind *bnd1p = (OCIBind *) 0;
  
  //
  // Output variable definitions
  //
  static OCIDefine *dfnhp[2];

  //
  // initialize command line args
  //
  globalArgs.year = 0;
  globalArgs.tile = NULL;
  globalArgs.run = NULL;
  globalArgs.outfile = NULL;
  globalArgs.range = NULL;
  globalArgs.verbosity = 0;

  //
  // Process command line args
  //
  opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
  while( opt != -1 ) {
     switch( opt ) {
        case 'y':
                globalArgs.year = atoi(optarg);
                break;

        case 't':

                globalArgs.tile = (char *) malloc(sizeof(optarg));
                strcpy(globalArgs.tile,optarg);
                char *mystr = strtok(optarg,",");
                while (mystr != NULL){
                  mystr = strtok(NULL,",");
                  numTiles++;
                }
                break;

        case 'r':
                globalArgs.run = optarg;
                break;

        case 'a':
                globalArgs.range = optarg;
                break;

        case 'o':
                globalArgs.outfile = optarg;
                break;

        case 'v':
                globalArgs.verbosity++;
                break;

        case 'h':

        case '?':
                usage();
                break;
        case 0:

        default:
               break;
     }
     opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
  }

  //
  // End of command line arg processing
  //
  printf("Using tile:  %s\n",globalArgs.tile);
  printf("Using run:  %s\n",globalArgs.run);
  printf("There are %d tiles to get: %s\n",numTiles,globalArgs.tile);

  //
  // Establish a DB connection
  //

  status = dbconnect();
  if (status){
    printf("DB ERROR:  Database connection failed.\n");
    exit(4);
  }

  //
  // Allocate statement handle
  //
  status = OCIHandleAlloc(envhp, (dvoid **) &sthp, OCI_HTYPE_STMT, 
                          (size_t) 0, (dvoid **) 0);
  checkDBerr(errhp,status);


  //
  // Get coadd ids in tile
  //
  coaddIdArr = getCoaddIdsForTile(globalArgs.tile,globalArgs.run);

  //
  // Get number of coadds from the statement handle
  //
  status = OCIAttrGet(sthp, OCI_HTYPE_STMT, &numCoadds, (ub4 *) 0,
      OCI_ATTR_ROW_COUNT,errhp);

  //
  // Get DES Tiles
  //
  numTiles = getNumDESTiles();
  desTileArr = (tileStruct *) malloc(sizeof(tileStruct) * numTiles);
  getDESTiles(desTileArr, numTiles);
  status = OCIAttrGet(sthp, OCI_HTYPE_STMT, &numTiles, (ub4 *) 0,
      OCI_ATTR_ROW_COUNT,errhp);

  /*
  ** Write the tile file
  ** TODO:  Only write the tiles relevant on the command line
  ** This section needs to be a for loop going through whatever tiles
  ** are listed on the command line.
  */
  coaddTileId = getDESTileId(globalArgs.tile);

  imageIdArr = (imageIdStruct*) malloc(sizeof(imageIdStruct) * MAXIMGS);

  if (globalArgs.outfile == NULL){
    foutp = fopen("mangle.ccd","w");
  }else{
    foutp = fopen(globalArgs.outfile,"w");
  }

  fprintf(foutp,"COADDTILEID|COADDID|REDUCEDID|RAWID|EXPOSUREID|CCD|BAND|EXPTIME|RA1,1|DEC1,1|RA1024,1|DEC1024,1|RA1024,4096|DEC1024,4096|RA1,4096|DEC1,4096|RA1025,1|DEC1025,1|RA2048,1|DEC2048,1|RA2048,4096|DEC2048,4096|RA1025,4096|DEC1025,4096|SKYBRITE|SKYSIGMA|GAINA|GAINB|RDNOISEA|RDNOISEB|ZEROPOINT|COADDZEROPOINT|PSFSCALE|FWHM\n");
                 


  for (i = 0; i < numCoadds; i++){

    coaddId = coaddIdArr[i].imageId;

    printf("Processing coadd %d\n",coaddId);

    // 
    // Get the image ids for current coadd
    //
    getImageZeropoint(coaddId,&coaddZeropoint);
    printf("coaddZeropoint: %f\n",coaddZeropoint);
    //coaddZeropoint = 10.0;

    getImageIdsForCoadd(coaddId,imageIdArr);
    status = OCIAttrGet(sthp, OCI_HTYPE_STMT, &numImgs, (ub4 *) 0,
      OCI_ATTR_ROW_COUNT,errhp);
    checkDBerr(errhp,status);
    printf("There are %d images in this coadd: %d\n",numImgs,coaddId);

    if (numImgs > 0){
      for (j = 0; j < numImgs; j++){
        //
        // These are remaps, we want reduced images.
        //
        getImageParentId(imageIdArr[j].imageId, parent);
        reducedId = parent->parentId;
        getImageParentId(reducedId,parentForRaw);
        rawId = parentForRaw->parentId;
        currentType = parent->parentType;
        zeroPoint = imageIdArr[j].magZP;

        /*
        ** If we have a norm file, go again because we want the reduced and
        ** raw ids
        */
        if (strcmp(currentType,"norm") == 0){
          getImageParentId(reducedId, parent);
          reducedId = parent->parentId;
          getImageParentId(reducedId,parentForRaw);
          rawId = parentForRaw->parentId;
        }

        fprintf(foutp,"%d|%d|%d|%d|",coaddTileId,coaddId,reducedId,rawId);

        //
        // Get the exposure id
        //
        expId = getExposureId(reducedId);
        fprintf(foutp,"%d|",expId);

        //reducedId = 12259035; /* for testing */

        //
        // Get image header information from DB
        //
        getImageInfoFromDB(reducedId,imageHeader);
        fprintf(foutp,"%d|%s|%f|",
            imageHeader->ccd,
            imageHeader->band,
            imageHeader->exptime);

        getImageInfoFromDB(rawId,rawHeader);
	normId=imageIdArr[j].imageId;
        getImageInfoFromDB(normId,normHeader);

        //
        // Do the WCS transformation
        // Here, we're getting the RA/DEC of all four corners of each 
        // amplifier.  
        // Amp A:  [1:1024,1:4096] and Amp B:  [1025:2048,1:4096]
        //
        //struct WorldCoor *wcs = deswcsinit(imageHeader);
        wcsstruct *wcs = deswcsinit(imageHeader);

        /*
        rawpos[0] = 1421.5166;
        rawpos[1] = 3571.9641;
        raw_to_wcs(wcs,rawpos,wcspos);
        printf("%f,%f\n",wcspos[0],wcspos[1]);
        //printf("%s\n",wcs->ptype);
        exit(0);
        */

        /* Amp A */
        rawpos[0] = 1;
        rawpos[1] = 1;
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);
        rawpos[0] = 1024;
        rawpos[1] = 1;
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);
        rawpos[0] = 1024;
        rawpos[1] = 4096;
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);
        rawpos[0] = 1;
        rawpos[1] = 4096;
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);

        /*
        pix2wcs(wcs,1,1,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        pix2wcs(wcs,1024,1,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        pix2wcs(wcs,1024,4096,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        pix2wcs(wcs,1,4096,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        */

        /* Amp B */
        rawpos[0] = 1025;
        rawpos[1] = 1;
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);
        rawpos[0] = 2048;
        rawpos[1] = 1; 
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);
        rawpos[0] = 2048;
        rawpos[1] = 4096;
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);
        rawpos[0] = 1025;
        rawpos[1] = 4096;
        raw_to_wcs(wcs,rawpos,wcspos);
        fprintf(foutp,"%6.12f|%6.12f|",wcspos[0],wcspos[1]);

        /*
        pix2wcs(wcs,1025,1,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        pix2wcs(wcs,2048,1,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        pix2wcs(wcs,2048,4096,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        pix2wcs(wcs,1025,4096,&ra,&dec);
        fprintf(foutp,"%6.12f|%6.12f|",ra,dec);
        */

        fprintf(foutp,"%.12f|",imageHeader->skybrite);
        fprintf(foutp,"%.12f|",imageHeader->skysigma);
        fprintf(foutp,"%.12f|%.12f|",rawHeader->gaina,rawHeader->gainb);
        fprintf(foutp,"%.12f|%.12f|",rawHeader->rdnoisea,rawHeader->rdnoiseb);
        fprintf(foutp,"%.12f|",zeroPoint);
        fprintf(foutp,"%.12f|",coaddZeropoint);
        fprintf(foutp,"%.12f|",normHeader->psfscale);
        fprintf(foutp,"%.12f\n",imageHeader->fwhm);

        //printf("imageHeader RA/DEC:  %f,%f\n",imageHeader->ra, imageHeader->dec);
        //printf("%f,%f\n",rawHeader->gaina, rawHeader->gainb);
        //printf("%f,%f\n",imageHeader->exptime,imageHeader->skysigma);
        //printf("Returned RA/DEC: %f,%f\n",ra,dec);
        //exit(0);

        //printf("%s\n",imageHeader->band);
        //printf("%d\n",imageHeader->ccd);
        //free(wcs);
      }
    }
    
  } // coadd for loop

  dbdisconnect();
  fclose(foutp);

  //
  // Free DB Handles
  //
  status = OCIHandleFree((dvoid *) sthp, OCI_HTYPE_STMT);
  status = OCIHandleFree((dvoid *) svchp, OCI_HTYPE_SVCCTX);
  status = OCIHandleFree((dvoid *) errhp, OCI_HTYPE_ERROR);

  //
  // Free memory
  //
  free(coaddIdArr);
  free(imageIdArr);
  free(imageHeader);
  free(rawHeader);
  free(parent);
  free(parentForRaw);
  free(desTileArr);

  return(0);

}

void usage( void )
{
        puts( "mkmanglefile:  Makes an input polygon file for mangle" );
        /* ... */
        exit( EXIT_FAILURE );
}

