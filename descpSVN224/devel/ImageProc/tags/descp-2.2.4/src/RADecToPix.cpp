#include "RADecToPix.h"
/*
 *The program call PixTools method to calculate Pixel number from WCS coordinates RA,Dec
 * and given nside parameter (map resolution)
 *
 * Author: Nikolay Kuropatkin    10/02/2012
 *
 */

using namespace std;



  int main(int argc, char *argv[]) {
	  	  double *radec = new double[2];
	      double *ang = new double[2];
	  	  PixTools *pt = new PixTools();
	  	  int nside;
	  	  int pixel;
    	  float RA,Dec;

    	  if (argc<3) {
    	    cout<<"Usage: "<<argv[0]<<" required inputs> "<<endl;
    	    cout<<"    Required Inputs:"<<endl;
    	    cout<<"       <RA>  <Dec> <nside> "<<endl;
    	    cout<<"       Output: print pixel number containing given RA,Dec "<<endl;
    	    cout<<endl;
    	        exit(0);
    	  }


    	  // now go through arguments


    	   RA = atof(argv[1]);
    	   Dec = atof(argv[2]);
    	   nside = atoi(argv[3]);
//    	   cout<<" RA="<<RA<<" "<<"Dec="<<Dec<<" "<<"nside="<<nside<<endl;


    	  	radec[0] = RA;
    	  	radec[1] = Dec;

    	  	ang = pt->RaDecToPolar(radec);
    	  	pixel = pt->ang2pix_ring(nside,ang[0],ang[1]);
    	  	cout<<pixel<<endl;
    	  	delete(ang);
    	  	delete(radec);
    	  	delete(pt);
}

