///
/// \file    SysErrFit.cxx
/// \brief   Vertex and n-prong fitting
/// \version $Id: SysErrFit.cxx,v 1.3 2009/06/29 19:09:45 messier Exp $
/// \author  messier@indiana.edu
///
#include "SysErrFit.h"
#include <iostream>
#include <cmath>
#include "Minuit2/Minuit2Minimizer.h"
#include <vector>
#include "TFile.h"
#include "TGraph.h"
#include "TMath.h"
using namespace std;

double SysErrFit::DoEval(const double* par) const
{
  
  double x=par[0];
    
  double sum=0;

  for(int i=0;i<entries; i++) {
    sum+= (diff[i]-mean)*(diff[i]-mean)/
      (x*x+m_err[i]*m_err[i]+t_err[i]*t_err[i]);

  }


  return (sum/(entries-1)-1)*(sum/(entries-1)-1);

}

 
double SysErrFit::Fit(const std::vector<double> *_diff,
		      const std::vector<double> *_m_err,
		      double &err)
	
{

  vector<double> _t_err;
  for(int i=0;i<(*_m_err).size();i++) _t_err.push_back(0.);
  
  this->Fit(_diff,_m_err,err,&_t_err);
}

double SysErrFit::Fit(const std::vector<double> *_diff,
		      const std::vector<double> *_m_err,
		      double &err,
		      const std::vector<double> *_t_err)
{
  // Set data
  diff=*_diff;
  m_err=*_m_err;
  t_err=*_t_err;

  mean=mean=TMath::Mean(diff.size(),&diff[0]);

  entries=diff.size();

  
  ROOT::Minuit2::Minuit2Minimizer mini(ROOT::Minuit2::kSimplex);
  //ROOT::Minuit2::Minuit2Minimizer mini;
  mini.SetPrintLevel(0);
  mini.SetTolerance(0.0000001);
  mini.SetStrategy(2);
  mini.SetMaxFunctionCalls(30000);
  

  mini.SetLimitedVariable(0, "#sigma_{sys}", 0,0.001,0, 1);
  
  
  mini.SetFunction(*this);
  mini.Minimize();
  
  //cout<<mini.MinValue()<<endl;

  //const double *errors=mini.Errors();
  //_fit_error=errors[0];
  return mini.X()[0];


  
}

