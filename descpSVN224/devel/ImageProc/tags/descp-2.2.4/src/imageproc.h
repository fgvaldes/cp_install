#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
//#include "fitsio.h"
#include "imsupport.h"

#if 0
#define Squ(x) ((x)*(x))
#define Cube(x) ((x)*(x)*(x))
#define Quad(x) (Squ(x)*Squ(x))
#define FIT 1
#define CONSTANT 0
#define FAST 1
#define NO 0
#define YES 1
#define SLOW 0
#define RAD2DEG 57.29578
#define MEM(st) printf("%s\n",st);system("ps aux|grep sim_ensemble");fflush(stdout);

#define GBAND 0
#define RBAND 1
#define IBAND 2
#define ZBAND 3
#define YBAND 4

#endif 

#if 0 
#define DES_IMAGE 1	/* "IMAGE" */
#define DES_VARIANCE 2  /* "VARIANCE" now deprecated */
#define DES_MASK 3	/* "BPM" also "MASK" but that is deprecated */
#define DES_SIGMA 4	/* "SIGMA" */
#define DES_WEIGHT 5	/* "WEIGHT" */
#define DES_SKYONLY 6	/* noisemodel uses only sky noise */
#define DES_FULL 7	/* noisemodel uses both sky and object noise */
#define AVERAGE 0
#define MEDIAN 1
#define AVSIGCLIP 2
#define CLIPPEDMEDIAN 3
#define CLIPPEDAVERAGE 4
#define AVSIGCLIPMAXNUM 10000  /* number of sigmas to calculate for the average value */
#define VARIANCE_DELTAPIXEL 1  /* use 3X3 square centered on each pixel               */
			       /* for variance                                        */
#define VARIANCE_DIRECT 1      /* calculate variance directly                         */
#define VARIANCE_CCD 2	       /* assume CCD noise is dominating variance             */
#endif 

#if 0
/* define event types */
#define STATUS 33
#define QA 34
#endif 

#if 0
/* define BPM bit mappings */
#define BADPIX_BPM 1          /* set in bpm (hot/dead pixel/column)        */
#define BADPIX_SATURATE 2     /* saturated pixel                           */
#define BADPIX_INTERP 4	      /* interpolated pixel                        */
#define BADPIX_THRESHOLD 0.10 /* pixels less than this fraction of sky     */
                              /* are filtered -- helps remove failed reads */
#define BADPIX_LOW    8       /* too little signal- i.e. poor read         */
#define BADPIX_CRAY   16      /* cosmic ray pixel                          */
#define BADPIX_STAR   32      /* bright star pixel                         */
#define BADPIX_TRAIL  64      /* bleed trail pixel                         */
#endif

#if 0 
/* define DataBase access modes */
#define DB_READONLY 1
#define DB_READWRITE 2
#define DB_FNAL 3

/* define Image FLAVOR Check modes */
#define CHECK_FITS 1
#define REQUIRE_FITS 2
#define FLAVOR_FITS 1
#define FLAVOR_FZ 2
#define FLAVOR_GZ 3
#endif


#if 0
/* Overscan configuration (temporary home) */
typedef struct {
  int choice;
  int sample;
  int function;
  int order;
  int trim;
} overscan_config;
#endif
#if 0
/* Image structure */
typedef struct {
  char     name[1000];
  char     biasseca[100],biassecb[100],
           ampseca[100], ampsecb[100],
           trimsec[100], datasec[100],
           dataseca[100],datasecb[100];
  long     axes[7],bscale,bzero,bitpix,npixels,fpixel;
  float	   saturateA,saturateB,
           gainA,gainB,
           rdnoiseA,rdnoiseB,
    exptime,crpix1,crpix2;
  int      nfound,hdunum,unit,varunit,maskunit;
  int      biassecan[4],biassecbn[4],
           ampsecan[4], ampsecbn[4],
           trimsecn[4], datasecn[4],
           datasecan[4],datasecbn[4];
  fitsfile *fptr;
  float	   *image,*varim,nullval;
  int      variancetype;
  short    *mask,shnullval;
} desimage;

/* FITS Catalog structure */
typedef struct {
  char    name[200];
  long    axes[7],bscale,bzero,bitpix,npixels,fpixel;
  float   *image,*varim,nullval;
  short   *mask;
  int     nfound,hdunum;
  int     biassec0n[4],biassec1n[4],
    ampsec0n[4], ampsec1n[4],
    trimsecn[4], datasecn[4];
  long    nrows,*repeat,*width;
  int     ncols,*typecode,hdutype;
} descat;

#endif 
typedef struct {
  char    nite[20],band[10],psmversion[20],date[15],time[15],pm[5];
  float   mjdlo,mjdhi;
  int     fitid,ccd,dof,photflag;
  float   a,aerr,b,berr,k,kerr,rms,chi2,color;
} db_psmfit;

typedef struct {
  int     imageid,ccd_number,devid,ccd,parentid,zeropointlink;
  char    band[10],detector[20],telescope[20],imagename[80],
    runiddesc[80],nite[80],tilename[100],imagetype[20];
  float   airmass,exptime,radeceq;
  double  ra,dec;
} db_files;

typedef struct {
  int     id,imageid,fitid,originid;
  float   mag_zero,sigma_mag_zero;
  char	source[10],date[100];
} db_zeropoint;

typedef struct {
  char    telescope[20],detector[20];
  int     chipid;
  float   raoffset,decoffset,rahwidth,dechwidth;
} db_wcsoffset;

typedef struct {
  char    project[25],tilename[50],runiddesc[300];
  char    nite[50],band[10],imagename[1024],basedir[1000];
  int     coaddtile_id,npix_ra,npix_dec,id,ccdnum,imageid;
  double  ra,dec;
  double  ra_lo,ra_hi,dec_lo,dec_hi;
  float   equinox,pixelsize;
  float   exptime,airmass;
} db_tiles;

typedef struct {
  double ra,dec;
  float mag,magerr;
  float class_star,flags;
} db_obj;

typedef struct {
  char	pointname[50];
  double ra,dec;
} tangent_point;

