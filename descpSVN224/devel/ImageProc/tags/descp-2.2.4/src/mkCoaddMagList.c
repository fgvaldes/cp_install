/*
**
** mkCoaddMagList.c
**
** Program to produce a list of magnitudes for objects on Coadds
**
** usage: mkCoaddMagList --l <outfile>
**
** Author: Gurvan Bazin (gbazin@usm.uni-muenchen.de)
** Date: 13 January 2010
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>

#include "ora_utils.h"



void usage(){
	fprintf(stderr, "mkCoaddMagList -l <outfile>\n");
}


int main(int argc, char** argv) {

	char* filename = NULL;
	int index;
	int c;
	int i;

	sword status;
	int numCoaddObj;
	mag_modelStruct* mag_model;

	FILE* fileout;

	//
	// Output variable definitions
	//
	//static OCIDefine *dfnhp[13];






	if(argc<2){
		usage();
		exit(2);
	}

	opterr = 0;
	while ((c = getopt (argc, argv, "l:")) != -1)
         switch (c)
           {
           case 'l':
             filename = optarg;
             break;
           case '?':
             if (optopt == 'l')
               fprintf (stderr, "Option -%c requires an argument.\n", optopt);
             else if (isprint (optopt))
               fprintf (stderr, "Unknown option `-%c'.\n", optopt);
             else
               fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
             return 1;
           default:
             abort ();
           }
     
       for (index = optind; index < argc; index++)
         printf ("Non-option argument %s\n", argv[index]);



	//
	// Establish a DB connection
	//
	status = dbconnect();
	if (status){
	  fprintf(stderr, "DB ERROR:  Database connection failed.");
	  exit(4);
	}

	//
	// Allocate statement handle
	//
	status = OCIHandleAlloc(envhp, (dvoid **) &sthp, OCI_HTYPE_STMT,
        	                (size_t) 0, (dvoid **) 0);
	checkDBerr(errhp,status);


	// Get DES Coadd Magnitudes
 	numCoaddObj = getNumCoaddObj();
	printf("%d objects in the coadd_objects table\n", numCoaddObj);
	mag_model = (mag_modelStruct *) malloc(sizeof(mag_modelStruct) * numCoaddObj);
	getMagModel(mag_model, numCoaddObj);
	status = OCIAttrGet(sthp, OCI_HTYPE_STMT, &numCoaddObj, (ub4 *) 0,
      				OCI_ATTR_ROW_COUNT,errhp);

	dbdisconnect();

	if((fileout = fopen(filename, "w")) == NULL) {
   	  fprintf(stderr, "Cannot open file.\n");
  	  exit(2);
  	}

	fprintf(fileout, "#COADD_OBJECTS_ID ALPHAMODEL_J2000_I DELTAMODEL_J2000_I MAG_MODEL_G MAG_MODEL_R MAG_MODEL_I MAG_MODEL_Z MAG_MODEL_Y MAGERR_MODEL_G MAGERR_MODEL_R MAGERR_MODEL_I MAGERR_MODEL_Z MAGERR_MODEL_Y\n");
	for(i = 0; i < numCoaddObj; i++) fprintf(fileout, "%d %22.16lf %22.16lf %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f\n", mag_model[i].objId, mag_model[i].ra, mag_model[i].dec, mag_model[i].g, mag_model[i].r, mag_model[i].i, mag_model[i].z, mag_model[i].y, mag_model[i].gerr, mag_model[i].rerr, mag_model[i].ierr, mag_model[i].zerr, mag_model[i].yerr);

	if( fclose( fileout )) fprintf(stderr, "File close error.\n");

	//
	// Free DB Handles
	//
	status = OCIHandleFree((dvoid *) sthp, OCI_HTYPE_STMT);
	status = OCIHandleFree((dvoid *) svchp, OCI_HTYPE_SVCCTX);
	status = OCIHandleFree((dvoid *) errhp, OCI_HTYPE_ERROR);

	//
	// Free memory
	//
	free(mag_model);

	return 0;	

}

