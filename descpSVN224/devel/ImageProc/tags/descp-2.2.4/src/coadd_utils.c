/*
**
** coadd_utils.c
**
** DESCRIPTION:
**     Utility subroutine for coaddition codes
**     
** OUTPUT FILE FORMAT:
**    ImageID  
**
** Last commit:
**     $Rev: 3199 $
**     $LastChangedBy: dadams $
**     $LastChangedDate: 2009-02-23 13:43:37 -0700 (Mon, 23 Feb 2009) $
**
*/

#include "imageproc.h"

void get_input(char *input1, char *input2, char *key, char *output, int flag_verbose)
{
  char event[1000];
  void reportevt();

  if (!strcmp(input1,key))  {
    if(input2==NULL) {
      sprintf(event,"Input for %s option is not set ",key);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }
    else {
      sprintf(output,"%s",input2);
      if (!strncmp(&output[0],"-",1)) {
	sprintf(event,"Wrong input for %s option ",key);
	reportevt(flag_verbose,STATUS,5,event);
	exit(0);
      }
    }
  }
}

void get_input_flt(char *input1, char *input2, char *key, float *value, int flag_verbose)
{
  char event[1000],temp[1000];
  void reportevt();

  if(input2==NULL) {
    sprintf(event,"Input for %s option is not set ",key);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else {
    sprintf(temp,"%s",input2);
    if (!strncmp(&temp[0],"-",1)) {
      sprintf(event,"Wrong input for %s option ",key);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }
    sscanf(input2,"%f",value);
  }
}

void get_input_array(char *inputlist, char *arrayout[], int *N, int flag_verbose)
{
  char event[1000],temp[1000];
  int j,k,s,ncomma,len;
  void reportevt(); 

  ncomma=0;
  len=strlen(inputlist);
  for (j=len;j>0;j--) {
    if (!strncmp(&(inputlist[j]),",",1)) { 
      ncomma++;
      inputlist[j]=32;
    }
  }
  *N=ncomma+1;	
 
  s=0;
  for(k=1;k<=*N;k++) {
    sscanf(inputlist+s,"%s%[\0]",temp);
    sprintf(arrayout[k],"%s",temp);
    len=strlen(temp);
    s+=len+1;

  }	
}

void check_fits_extension(char *inputimage, char *terapixpath, int flag_verbose)
{
  char command[1000],event[1000],checkimage[1000];
  int flag_checkimage;
  void reportevt(); 
  FILE *fcheck;

  /* check the input image */
  flag_checkimage=0;
  if (!strncmp(&(inputimage[strlen(inputimage)-5]),".fits",5)) {
    flag_checkimage=1;
  } 
  /* strip off the .fz  */
  else if(!strncmp(&(inputimage[strlen(inputimage)-8]),".fits.fz",8)) {
    flag_checkimage=1;
    inputimage[strlen(inputimage)-3]=0;
  }
  /* strip off the .gz  */
  else if(!strncmp(&(inputimage[strlen(inputimage)-8]),".fits.gz",8)) {
    flag_checkimage=1;
    inputimage[strlen(inputimage)-3]=0;
  }
  
  if(!flag_checkimage) {
    sprintf(event,"Input fits file %s must contain only FITS images",inputimage);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0); 
  } 

  /* ************************************************** */
  /* ********** uncompress image if needed ************ */
  /* ************************************************** */
  sprintf(checkimage,"%s",inputimage);
  fcheck=fopen(checkimage,"r");
  if (fcheck==NULL) { /* check for fpacked flavor...*/
    sprintf(checkimage,"%s.fz",inputimage);
      fcheck=fopen(checkimage,"r");
      if (fcheck==NULL) { /* check for gzipped flavor...*/
        sprintf(checkimage,"%s.gz",inputimage);
        fcheck=fopen(checkimage,"r");
        if (fcheck==NULL) { /* no flavor exists*/
          sprintf(event,"File %s not found in any flavor",inputimage);
          reportevt(flag_verbose,STATUS,5,event);
          exit(0);
        }
        else fclose(fcheck); /* flavor G exists */
        sprintf(command,"gunzip %s.gz",inputimage);
        sprintf(event,"Gunzip'ing  %s.gz",inputimage);
        reportevt(flag_verbose,STATUS,3,event);
        system(command);
      }
      else fclose(fcheck); /* flavor F exists */
      sprintf(command,"%s/funpack %s.fz",terapixpath,inputimage);
      sprintf(event,"Funpack'ing  %s.fz",inputimage);
      reportevt(flag_verbose,STATUS,3,event);
      system(command);
  }
  else fclose(fcheck); /* uncompressed flavor exists */ 
}

int count_inputfile(char *filename, int flag_verbose)
{
  char command[1000],event[1000];
  void reportevt(); 
  int N=0;
  FILE *pip;

  sprintf(command,"wc -l %s",filename);
  pip=popen(command,"r");
  if (pip==NULL) {
    sprintf(event,"Pipe failed: %s ",command);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  fscanf(pip,"%d",&N);
  pclose(pip);

  return (N);
}
