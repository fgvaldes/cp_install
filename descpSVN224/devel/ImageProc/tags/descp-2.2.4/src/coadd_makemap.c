/*
**
** coadd_makemap.c
**
** NOTE:
**   Current version using a dump way to make plot: create a sm script and run the script within sm to make the plot.
**
**   Need to update it to use PLPLOT (or Tony can covert to perl?)  
**     
**
** Last commit:
**     $Rev: 4567 $
**     $LastChangedBy: desai $
**     $LastChangedDate: 2009-10-15 11:16:14 -0700 (Thu, 15 Oct 2009) $
**
*/

#include "imageproc.h"

//#define PIXSCALE 0.27
//#define NPIX 10000.0

main(int argc, char *argv[])
{
  char project[10],tilename[1000],command[1000],event[1000];
  char dblogin[1000],sqlcall[1000],sqlcallA[1000],sqlHEAD[1000];

  double ra,dec;
  double ra_lo,ra_hi,dec_lo,dec_hi;
  
  float inra_lo,inra_hi;
  float indec_lo,indec_hi;
  float PIXSCALE,temp;
  
  int i,count,counta,N,imageid,num=0;
  int flag_verbose=0,flag_rarange=0,flag_decrange=0;
  int NPIX_RA,NPIX_DEC;

  FILE *fin,*fsm,*pip,*pip1,*fsql;

  /* external functions call */
  void select_dblogin(),reportevt();

  /* default values */
  PIXSCALE=0.27;
  NPIX_RA=NPIX_DEC=10000;
  sprintf(project,"DES");

  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <project> <options>\n",argv[0]);
    printf("    Options:\n");
    printf("       -rarange  <lower_RA>  <upper_RA>\n");
    printf("       -decrange <lower_DEC> <upper_DEC>\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }

  if(argv[1]==NULL) {
    sprintf(event,"required <project> is not given");
    flag_verbose=2;
    reportevt(flag_verbose,STATUS,3,event);
  }
  else
    sprintf(project,"%s",argv[1]);
  
  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }


  /* now go through again */
  for(i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-rarange")) {
      flag_rarange=1;
      sscanf(argv[i+1],"%f",&inra_lo);
      sscanf(argv[i+2],"%f",&inra_hi);
    }
    if (!strcmp(argv[i],"-decrange")) {
      flag_decrange=1;
      sscanf(argv[i+1],"%f",&indec_lo);
      sscanf(argv[i+2],"%f",&indec_hi);
    }
  }

  /* *** Setup DB login and make generic sqlcall *** */
  select_dblogin(dblogin,DB_READWRITE);
  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < temp.sql ",dblogin);
  sprintf(sqlcallA, "${ORACLE_HOME}/bin/sqlplus -S %s < tempA.sql ",dblogin);
 
  /* *** Find out the PIXSCALE and NPIX_RA/DEC from coaddtile table *** */
  fsql=fopen("temp.sql","w");
  fprintf(fsql,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;\n");
  fprintf(fsql,"select distinct(PIXELSIZE) from coaddtile where project='%s';\n",project);
  fprintf(fsql,"select distinct(NPIX_RA) from coaddtile where project='%s';\n",project);
  fprintf(fsql,"select distinct(NPIX_DEC) from coaddtile where project='%s';\n",project);
  fprintf(fsql,"exit;\n");
  fclose(fsql);

  pip=popen(sqlcall,"r");
  fscanf(pip,"%f",&PIXSCALE);
  fscanf(pip,"%d",&NPIX_RA);
  fscanf(pip,"%d",&NPIX_DEC);
  pclose(pip);

  sprintf(event,"For %s project, use PIXSCALE=%2.2f NPIX_RA=%d NPIX_DEC=%d\n",project,PIXSCALE,NPIX_RA,NPIX_DEC);
  reportevt(flag_verbose,STATUS,2,event);

  /* *** Find out all the coadd tiles in the coadd table *** */  
  fsql=fopen("tempA.sql","w");
  fprintf(fsql,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;\n");
  fprintf(fsql,"select ra,dec,id,tilename from coadd where band='g' and project='DES' "); 
  if(flag_rarange)
    fprintf(fsql,"and (ra between %2.3f and %2.3f) ",inra_lo,inra_hi);
  if(flag_decrange)
    fprintf(fsql,"and (dec between  %2.3f and %2.3f) ",indec_lo,indec_hi);
  fprintf(fsql,"order by tilename;\nexit;\n");
  fclose(fsql);

 
  fsm=fopen("plot.sm","w");

  pip=popen(sqlcallA,"r");

  counta=1;
  while(fscanf(pip,"%lg %lg %d %s",&ra,&dec,&imageid,tilename)!=EOF) {

    /* find out the (g band) coadd images that do not have count in coadd_objects table */ 
    count=0;
    fsql=fopen("temp.sql","w");
    fprintf(fsql,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;\n");
    fprintf(fsql,"select count(*) from coadd_objects where  imageid_g=%d;\n",imageid);
    fprintf(fsql,"exit;\n");
    fclose(fsql);

    pip1=popen(sqlcall,"r");
    fscanf(pip1,"%d",&count);
    pclose(pip1);

    sprintf(event,"tilename = %s\tcoadd_id = %d\tcount = %d",tilename,imageid,count);
    reportevt(flag_verbose,STATUS,1,event);
    
    if(count) {
    
      dec_lo=dec-(NPIX_DEC/2.0)*PIXSCALE/3600.0;
      dec_hi=dec+(NPIX_DEC/2.0)*PIXSCALE/3600.0;
    
      ra_lo=ra-(NPIX_RA/2.0)*PIXSCALE/cos(dec_lo*M_PI/180.0)/3600.0;
      ra_hi=ra+(NPIX_RA/2.0)*PIXSCALE/cos(dec_hi*M_PI/180.0)/3600.0;

      fprintf(fsm,"p%d\n",counta);
      fprintf(fsm,"\tctype red\n");
      fprintf(fsm,"\tset a={%2.6f %2.6f}\n",ra_lo,ra_hi);
      fprintf(fsm,"\tset b={%2.6f %2.6f}\n",dec_lo,dec_lo);
      fprintf(fsm,"\tconnect a b\n");
      fprintf(fsm,"\tset a={%2.6f %2.6f}\n",ra_lo,ra_hi);
      fprintf(fsm,"\tset b={%2.6f %2.6f}\n",dec_hi,dec_hi);
      fprintf(fsm,"\tconnect a b\n");
      fprintf(fsm,"\tset a={%2.6f %2.6f}\n",ra_lo,ra_lo);
      fprintf(fsm,"\tset b={%2.6f %2.6f}\n",dec_lo,dec_hi);
      fprintf(fsm,"\tconnect a b\n");
      fprintf(fsm,"\tset a={%2.6f %2.6f}\n",ra_hi,ra_hi);
      fprintf(fsm,"\tset b={%2.6f %2.6f}\n",dec_lo,dec_hi);
      fprintf(fsm,"\tconnect a b\n");
      fprintf(fsm,"\tset c={%2.6f}\n",ra);
      fprintf(fsm,"\tset d={%2.6f}\n",dec);
      fprintf(fsm,"\tctype green\n");
      fprintf(fsm,"\tptype 36 0\n");
      fprintf(fsm,"\tpoints c d\n");
      fprintf(fsm,"\n");

      counta++;
    }
  }
  pclose(pip);


  fprintf(fsm,"\n\npt\terase\n");
  fprintf(fsm,"\texpand 1.1\n");
  fprintf(fsm,"\tctype default\n");
  
  fprintf(fsm,"\tlimits 355 315 -46 -6\n");
  fprintf(fsm,"\tbox\n");
  fprintf(fsm,"\txlabel RA(J2000) [Deg.]\n");
  fprintf(fsm,"\tylabel DEC(J2000) [Deg.]\n");
  
  for(i=1;i<counta;i++)
    fprintf(fsm,"\tp%d\n",i);
  fprintf(fsm,"\n");

  fclose(fsm);

  system ("rm temp*.sql");  
}

//#undef PIXSCALE
//#undef NPIX
