#include "DatabaseInterface.h"
#include "DBSObject.h"
#include "DBTruth.h"
#include "DBSCoaddObject.h"
#include "DBSCoaddShear.h"
#include "DBCatalog.h"
#include "DBImage.h"
#include "DBShear.h"
#include "DatabaseUtils.h"
#include <iostream>
#include <fstream>
#include <kdtree++/kdtree.hpp>

#include "TTree.h"
#include "TFile.h"
#include "TStopwatch.h"
using namespace std;
using namespace Db;


typedef KDTree::KDTree<3,DBSObject > DB_KDTree;
typedef std::pair<DB_KDTree::iterator,double> DB_Match;

int main(int argc,char* argv[])
{
   
   double tol=2;
   tol*=1./3600*TMath::DegToRad();
   TStopwatch timer;
  //  string run="20110111100523_BCS2335-5421";
    string tile="BCS2336-5602";
//   string run="20101231234611_BCS0547-5332";//20090803201006_BCS0547-5332";
   string run="";//"20100820122536_BCS0547-5332";//20090803201006_BCS0547-5332";
   //string tile="BCS0547-5332";

    DatabaseInterface db("st");
    if(run.empty()) {
      run=GetMaxCatRun(db,tile);
    }
    cout<<"Using run: "<<run<<endl;

    string band="i";

    // Get Coadd objects
    string where=Form("from coadd_objects,catalog where "
                      "catalog.tilename='%s' and "
                      "catalog.id=coadd_objects.catalogid_g and "
                      "catalog.run='%s'",
                      tile.c_str(),run.c_str());

    vector<DBSCoaddObject> vec_coadd;
    DBSCoaddObject *coadd=new DBSCoaddObject;

    bool inc_table=true;
    bool ok=FillVec(db,where,vec_coadd,inc_table);
    cout<<"coadd objects: "<<vec_coadd.size()<<" objects"<<endl;
    if(vec_coadd.size()==0) {
      exit(1);
    }
    
    
    // create new file
    TFile newfile("BCS2336-5602_new.root","recreate");

    
    TTree *imtree=new TTree("imtree","");
    DBImage *image=new DBImage;
    imtree->Branch("image",&image);

    vector<DBImage> vec_im;
    GetSEImages(db,vec_im,tile,run);
    cout<<"Found "<<vec_im.size()<<" images in the tile"<<endl;


    bool cal=true;
    DB_KDTree obs;
    for(int i=0;i<vec_im.size();++i) {
     vector<DBSObject> vec_ob;
     bool ok=GetSEObjects(db,vec_im[i],vec_ob,tile,cal);
     cout<<"Got image with "<<vec_ob.size()<<" objects"<<endl;
     for(int j=0;j<vec_ob.size();j++) {
       vec_ob[j].SetRADec(vec_ob[j].alphawin_j2000,
                          vec_ob[j].deltawin_j2000);
       obs.insert(vec_ob[j]);
       
     }
   }
    
    
    // Fill the image tree
    for(int i=0;i<vec_im.size();++i) {
      *image=vec_im[i];
      imtree->Fill();
    }
    
    TTree *ntree=new TTree("tree","");
    int n_match;
    vector<DBImage> vec_imm;
    vector<DBSObject> vec_ob;
    vector<int> se_band;
    ntree->Branch("coadd",&coadd);
    ntree->Branch("n_match",&n_match,"n_match/I");
    ntree->Branch("im",&vec_imm);
    ntree->Branch("se",&vec_ob);
    ntree->Branch("se_band",&se_band);

    
    int i=0;
    int m=0,n=0;
    for(int i=0;i<vec_coadd.size();++i) {
      i++;
      vector<DBSObject> matches;
    
      // reset vectors
      vec_ob.clear();
      vec_imm.clear();
      se_band.clear();
      *coadd=vec_coadd[i];

      // use i band to match
      DBSObject dummy;
      dummy.SetRADec(vec_coadd[i].alphawin_j2000_i,
                     vec_coadd[i].deltawin_j2000_i);
      obs.find_within_range(dummy,tol,std::back_inserter(matches));
      
      n_match=matches.size();
      vector<DBSObject>::iterator match_iter=matches.begin();
      for( ; match_iter!=matches.end(); ++match_iter) {
        vec_ob.push_back(*match_iter);

        // need seperate vector for band info since
        // ROOT has problem with the strings
        if(match_iter->band=="g") se_band.push_back(0);
        else if(match_iter->band=="r") se_band.push_back(1);
        else if(match_iter->band=="i") se_band.push_back(2);
        else if(match_iter->band=="z") se_band.push_back(3);
        else se_band.push_back(-1);

        // find single epoch image
        int id=match_iter->imageid;
        bool found=false;
        for(int j=0;j<vec_im.size();j++) {
          if(vec_im[j].parentid==id) {
            vec_imm.push_back(vec_im[j]);
            found=true;
            break;
          }
        }
        if(!found) {
          // if none was found create an empty one
          cout<<"Not found"<<endl;
          DBImage im;
          vec_imm.push_back(im);
        }
        
      }

      if(n_match>0) {
        ntree->Fill();
      }
      
    }

    imtree->Write();
    ntree->Write();
    newfile.Close();
    timer.Print();
}
  

