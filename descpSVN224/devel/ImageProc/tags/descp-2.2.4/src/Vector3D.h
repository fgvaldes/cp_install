//Vector3D.h
/*
 * Simple 3D vector class used in PixTools
 */
#ifndef VECTOR3D
#define VECTOR3D

#include <iostream>
#include <math.h>

//#include "defines.h"

#define X (Vector3D(1, 0, 0))
#define Y (Vector3D(0, 1, 0))
#define Z (Vector3D(0, 0, 1))
namespace std {
class Vector3D
{
 public:
  Vector3D(void);
  Vector3D(double nx, double ny, double nz);
  Vector3D(bool v);

  //double get_x(void);
  //double get_y(void);
  //double get_z(void);
  //bool get_valid(void);

  //void set_x(double nx);
  //void set_y(double ny);
  //void set_z(double nz);
  //void set_xyz(double nx, double ny, double nz);
  //void set_valid(bool v);
  double getInd(int);
  void normalize(void);
  double magnitude(void);
  double angleBetween(
   	Vector3D vector1,
   	Vector3D vector2
   );
  Vector3D cross(Vector3D v, Vector3D w);
  double dot(Vector3D v, Vector3D w);
  Vector3D rotate(Vector3D v, Vector3D axis, double angle);
  void full_dump(void) const;

  bool valid;
  double x,y,z;
 protected:
 private:
};

//Vector3D normalize(Vector3D v);
//double magnitude(Vector3D v);
Vector3D rotate(Vector3D v, Vector3D axis, double angle);
Vector3D cross(Vector3D v, Vector3D w);
double dot(Vector3D v, Vector3D w);

Vector3D operator+(Vector3D v, Vector3D w);
Vector3D operator-(Vector3D v, Vector3D w);
Vector3D operator*(Vector3D v, Vector3D w);
 Vector3D operator*(Vector3D v, double w);
 Vector3D operator*(double w, Vector3D v);
Vector3D operator/(Vector3D v, Vector3D w);
 Vector3D operator/(Vector3D v, double w);
 Vector3D operator/(double w, Vector3D v);
 double angleBetween(Vector3D v1, Vector3D v2);
//****************
//* Constructors *
//****************

inline Vector3D::Vector3D(void)
{
	z = 0.;
	y = 0.;
	x = 0.;
 valid=true;
}

inline Vector3D::Vector3D(double nx, double ny, double nz)
{
 x=nx;
 y=ny;
 z=nz;
 valid=true;

}

inline Vector3D::Vector3D(bool v)
{
 x=y=z=0;
 valid=v;
}

inline double Vector3D::getInd(int i) {
	if (i==0) return x;
	if (i == 1) return y;
	if ( i == 2) return z;
	return 0.0;
}
//******************************
//* More complicated functions *
//******************************

//Basic arithmetic (+, -, *, /)

inline Vector3D operator+(Vector3D v, Vector3D w)
{
 return Vector3D(
  v.x + w.x,
  v.y + w.y,
  v.z + w.z
 );
}

inline Vector3D operator-(Vector3D v, Vector3D w)
{
 return Vector3D(
  v.x - w.x,
  v.y - w.y,
  v.z - w.z
 );
}

inline Vector3D operator*(Vector3D v, Vector3D w)
{
 return Vector3D(
  v.x * w.x,
  v.y * w.y,
  v.z * w.z
 );
}

 inline Vector3D operator*(Vector3D v, double w)
 {
  return Vector3D(
   v.x * w,
   v.y * w,
   v.z * w
  );
 }

 inline Vector3D operator*(double w, Vector3D v)
 {
  return v*w;
 }

inline Vector3D operator/(Vector3D v, Vector3D w)
{
 return Vector3D (
  v.x / w.x,
  v.y / w.y,
  v.z / w.z
 );
}

 inline Vector3D operator/(Vector3D v, double w)
 {
  return Vector3D (
   v.x / w,
   v.y / w,
   v.z / w
  );
 }

 inline Vector3D operator/(double w, Vector3D v)
 {
  return Vector3D (
   w / v.x,
   w / v.y,
   w / v.z
  );
 }

//Normalization

inline void Vector3D::normalize(void)
{
 double mag=magnitude();
 x/=mag;
 y/=mag;
 z/=mag;
}

//Finding the magnitude

inline double Vector3D::magnitude(void)
{
 return sqrt(x*x+y*y+z*z);
}

//Cross product

inline Vector3D Vector3D::cross(Vector3D v, Vector3D w)
{
 return Vector3D(
  v.y*w.z - v.z*w.y,
  v.z*w.x - v.x*w.z,
  v.x*w.y - v.y*w.x
 );
}

//Dot product

inline double Vector3D::dot(Vector3D v, Vector3D w)
{
 Vector3D x=w*v;
 return x.x + x.y + x.z;
}

//Rotation

inline Vector3D Vector3D::rotate(Vector3D v, Vector3D axis, double angle)
{
 //Working for this function by:
 // "John L. Richardson" <john@real3d.com>

 //axis must be normalized
 Vector3D n;
 axis.normalize();
 n=axis*dot(axis,v);
 return n + cos(angle)*(v-n) + sin(angle)*cross(axis, v);
}
inline double Vector3D::angleBetween(Vector3D v1, Vector3D v2) {
	double val = dot(v1,v2);
	return acos(val/(v1.magnitude()*v2.magnitude()));
}

inline void Vector3D::full_dump(void) const
{
 cout << ((valid==true)?"valid":"invalid") << ", " << x << ", " << y << ", " << z << "\n";
}
} /* namespace std */
#endif
