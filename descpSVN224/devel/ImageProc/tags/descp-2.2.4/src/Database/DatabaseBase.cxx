#include "DatabaseBase.h"
#include "DatabaseUtils.h"
using namespace Db;
ClassImp(DatabaseBase)

DatabaseBase::DatabaseBase(const string &_tables, const string &_columns,
                           DatabaseInterface *db):
  GenericFill(true)
{
  

  Tokenize(_tables,tables,",");

  // if columns empty fill get everything from the database
  if(columns.empty() && db) {

    for(int i=0;i<tables.size();++i) {

      vector<string> tb_col;

      MetaTable m_table;
      db->GetMeta(tables[i],m_table);
      map<string,ColType> cols=m_table.col_map;
      map<string,ColType>::iterator iter= cols.begin();
      
      for(; iter!=cols.end();iter++) {

        TString low(iter->first.c_str());
        low.ToLower();
        tb_col.push_back(low.Data());
      }
      columns.push_back(tb_col);
    }
  }
  else {
    vector<string> tb_col;
    Tokenize(_columns,tb_col,":");
    
    for(int i=0;i<tb_col.size();i++) {
      vector<string> tbs_col;
      
      Tokenize(tb_col[i],tbs_col,",");
      columns.push_back(tbs_col);
    }
  }

  
}  

void DatabaseBase::AddTable(const string &table,const string &cols)
{
  tables.push_back(table);
  vector<string> tmp;
  Tokenize(cols,tmp,",");

  columns.push_back(tmp);
}
   
// bool DatabaseBase::Fill(DatabaseInterface &db,const string &where)
// {
//   //build the query
//   string query="select ";
//   for(int i=0;i<tables.size();++i) {
    
//     for(int j=0;j<columns[i].size();++j) {

//       query+=tables[i]+"."+columns[i][j]+",";
//     }
//   }
//   query=query.substr(0,query.size()-1);
 
//   query+=" from ";
//   for(int i=0;i<tables.size();++i) {
//     query+=tables[i]+",";
//   }
//   query=query.substr(0,query.size()-1);
//   query+=" where "+where;
//   cout<<query<<endl;
//   DatabaseResult result;
//   bool ok=result.Query(db,query);
//   if(!ok) return ok;

//   while(result.NextRow()) {


//     SetVals(result);
//   }

//   return true;
  
// }
  
// bool DatabaseBase::SetVals(DatabaseRecord &rec)
// {

  

// }


