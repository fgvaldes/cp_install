#ifndef DBEXPOSURE_H
#define DBEXPOSURE_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class DBExposure: public DatabaseBase {

public:

 DBExposure();
 ~DBExposure() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 float airmass;
 string altitude;
 string ambtemp;
 string band;
 string camshut;
 double cx;
 double cy;
 double cz;
 float darktime;
 string date_obs;
 string detector;
 string detsize;
 float dimmsee;
 string domelamp;
 string domeshut;
 string domestat;
 float epoch;
 int expnum;
 string exposurename;
 string exposuretype;
 float expreq;
 float exptime;
 int exp_exclude;
 int filpos;
 string filtid;
 float fluxvar;
 string ha;
 int htmid;
 string humidity;
 int id;
 string imagehwv;
 string imageswv;
 string instrument;
 string latitude;
 string longitude;
 double mjd_obs;
 float moonangle;
 int naxis1;
 int naxis2;
 int nextend;
 string nite;
 string object;
 string observatory;
 string observer;
 string obstype;
 int photflag;
 float pixscal1;
 float pixscal2;
 string pressure;
 string proctype;
 string prodtype;
 string project;
 string propid;
 string proposer;
 string radesys;
 float skyvar;
 int teldec;
 float telequin;
 string telescope;
 float telfocus;
 int telra;
 string telradec;
 string telstat;
 string timesys;
 string time_obs;
 string winddir;
 string windspd;
 float zd;


ClassDef(DBExposure,1);
};
}
#endif
