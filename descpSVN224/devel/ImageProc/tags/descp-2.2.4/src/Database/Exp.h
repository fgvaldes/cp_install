#ifndef EXP_H
#define EXP_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class Exp: public DatabaseBase {

public:

 Exp();
 ~Exp() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 double airmass;
 string altitude;
 string ambtemp;
 string band;
 string camshut;
 double cx;
 double cy;
 double cz;
 double darktime;
 string date_obs;
 string detector;
 string detsize;
 double dimmsee;
 string domelamp;
 string domeshut;
 string domestat;
 double epoch;
 double exp_exclude;
 double expnum;
 string exposurename;
 string exposuretype;
 double expreq;
 double exptime;
 double filpos;
 string filtid;
 double fluxvar;
 string ha;
 double htmid;
 string humidity;
 int id;
 string imagehwv;
 string imageswv;
 string instrument;
 string latitude;
 string longitude;
 double mjd_obs;
 double moonangle;
 double naxis1;
 double naxis2;
 double nextend;
 string nite;
 string object;
 string observatory;
 string observer;
 string obstype;
 double photflag;
 double pixscal1;
 double pixscal2;
 string pressure;
 string proctype;
 string prodtype;
 string project;
 string propid;
 string proposer;
 string radesys;
 double skyvar;
 double teldec;
 double telequin;
 string telescope;
 double telfocus;
 double telra;
 string telradec;
 string telstat;
 string time_obs;
 string timesys;
 string winddir;
 string windspd;
 double zd;


ClassDef(Exp,1);
};
}
#endif
