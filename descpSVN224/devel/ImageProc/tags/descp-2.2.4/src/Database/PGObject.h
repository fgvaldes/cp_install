#ifndef PGOBJECT_H
#define PGOBJECT_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"
#include "XYZTree.h"

namespace Db {
  class PGObject: public DatabaseBase,public XYZTree {

public:

 PGObject();
 ~PGObject() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 double alpha_j2000;
 string band;
 double catalogid;
 double class_star;
 double dec;
 double delta_j2000;
 double errx2_world;
 double erry2_world;
 double flags;
 double imageid;
 double mag_aper_8;
 double mag_auto;
 double magerr_aper_8;
 double magerr_auto;
 double object_id;
 double ra;
 double x_image;
 double y_image;
 double zeropoint;


ClassDef(PGObject,1);
};
}
#endif
