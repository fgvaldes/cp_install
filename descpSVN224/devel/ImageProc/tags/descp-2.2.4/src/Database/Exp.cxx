#include "Exp.h"

using namespace Db;
ClassImp(Exp)

Exp::Exp():
  DatabaseBase()
{
 this->AddTable("exposure","airmass,altitude,ambtemp,band,camshut,cx,cy,cz,darktime,date_obs,detector,detsize,dimmsee,domelamp,domeshut,domestat,epoch,exp_exclude,expnum,exposurename,exposuretype,expreq,exptime,filpos,filtid,fluxvar,ha,htmid,humidity,id,imagehwv,imageswv,instrument,latitude,longitude,mjd_obs,moonangle,naxis1,naxis2,nextend,nite,object,observatory,observer,obstype,photflag,pixscal1,pixscal2,pressure,proctype,prodtype,project,propid,proposer,radesys,skyvar,teldec,telequin,telescope,telfocus,telra,telradec,telstat,time_obs,timesys,winddir,windspd,zd");
}


bool Exp::SetVals(DatabaseResult &rec,const int &first) 
{

  airmass=rec.GetDouble(first+0);
  altitude=rec.GetString(first+1);
  ambtemp=rec.GetString(first+2);
  band=rec.GetString(first+3);
  camshut=rec.GetString(first+4);
  cx=rec.GetDouble(first+5);
  cy=rec.GetDouble(first+6);
  cz=rec.GetDouble(first+7);
  darktime=rec.GetDouble(first+8);
  date_obs=rec.GetString(first+9);
  detector=rec.GetString(first+10);
  detsize=rec.GetString(first+11);
  dimmsee=rec.GetDouble(first+12);
  domelamp=rec.GetString(first+13);
  domeshut=rec.GetString(first+14);
  domestat=rec.GetString(first+15);
  epoch=rec.GetDouble(first+16);
  exp_exclude=rec.GetDouble(first+17);
  expnum=rec.GetDouble(first+18);
  exposurename=rec.GetString(first+19);
  exposuretype=rec.GetString(first+20);
  expreq=rec.GetDouble(first+21);
  exptime=rec.GetDouble(first+22);
  filpos=rec.GetDouble(first+23);
  filtid=rec.GetString(first+24);
  fluxvar=rec.GetDouble(first+25);
  ha=rec.GetString(first+26);
  htmid=rec.GetDouble(first+27);
  humidity=rec.GetString(first+28);
  id=rec.GetInt(first+29);
  imagehwv=rec.GetString(first+30);
  imageswv=rec.GetString(first+31);
  instrument=rec.GetString(first+32);
  latitude=rec.GetString(first+33);
  longitude=rec.GetString(first+34);
  mjd_obs=rec.GetDouble(first+35);
  moonangle=rec.GetDouble(first+36);
  naxis1=rec.GetDouble(first+37);
  naxis2=rec.GetDouble(first+38);
  nextend=rec.GetDouble(first+39);
  nite=rec.GetString(first+40);
  object=rec.GetString(first+41);
  observatory=rec.GetString(first+42);
  observer=rec.GetString(first+43);
  obstype=rec.GetString(first+44);
  photflag=rec.GetDouble(first+45);
  pixscal1=rec.GetDouble(first+46);
  pixscal2=rec.GetDouble(first+47);
  pressure=rec.GetString(first+48);
  proctype=rec.GetString(first+49);
  prodtype=rec.GetString(first+50);
  project=rec.GetString(first+51);
  propid=rec.GetString(first+52);
  proposer=rec.GetString(first+53);
  radesys=rec.GetString(first+54);
  skyvar=rec.GetDouble(first+55);
  teldec=rec.GetDouble(first+56);
  telequin=rec.GetDouble(first+57);
  telescope=rec.GetString(first+58);
  telfocus=rec.GetDouble(first+59);
  telra=rec.GetDouble(first+60);
  telradec=rec.GetString(first+61);
  telstat=rec.GetString(first+62);
  time_obs=rec.GetString(first+63);
  timesys=rec.GetString(first+64);
  winddir=rec.GetString(first+65);
  windspd=rec.GetString(first+66);
  zd=rec.GetDouble(first+67);
}

