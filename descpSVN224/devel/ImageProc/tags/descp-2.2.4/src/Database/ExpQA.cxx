#include "ExpQA.h"

using namespace Db;
ClassImp(ExpQA)

ExpQA::ExpQA():
  DatabaseBase()
{
 this->AddTable("exposure_qa","astromchi2_reference,astromchi2_reference_highsn,astromcorr_reference,astromcorr_reference_highsn,astromndets_reference,astromndets_reference_highsn,astromoffset_reference_1,astromoffset_reference_2,astromoffset_reference_highsn_1,astromoffset_reference_highsn_2,astromsigma_reference_1,astromsigma_reference_2,astromsigma_reference_highsn_1,astromsigma_reference_highsn_2,exposureid,id,nite,project,run");
}


bool ExpQA::SetVals(DatabaseResult &rec,const int &first) 
{

  astromchi2_reference=rec.GetDouble(first+0);
  astromchi2_reference_highsn=rec.GetDouble(first+1);
  astromcorr_reference=rec.GetDouble(first+2);
  astromcorr_reference_highsn=rec.GetDouble(first+3);
  astromndets_reference=rec.GetDouble(first+4);
  astromndets_reference_highsn=rec.GetDouble(first+5);
  astromoffset_reference_1=rec.GetDouble(first+6);
  astromoffset_reference_2=rec.GetDouble(first+7);
  astromoffset_reference_highsn_1=rec.GetDouble(first+8);
  astromoffset_reference_highsn_2=rec.GetDouble(first+9);
  astromsigma_reference_1=rec.GetDouble(first+10);
  astromsigma_reference_2=rec.GetDouble(first+11);
  astromsigma_reference_highsn_1=rec.GetDouble(first+12);
  astromsigma_reference_highsn_2=rec.GetDouble(first+13);
  exposureid=rec.GetDouble(first+14);
  id=rec.GetDouble(first+15);
  nite=rec.GetString(first+16);
  project=rec.GetString(first+17);
  run=rec.GetString(first+18);
}

