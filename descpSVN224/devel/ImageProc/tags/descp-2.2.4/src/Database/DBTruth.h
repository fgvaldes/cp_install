#ifndef DBTRUTH_H
#define DBTRUTH_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"
#include "XYZTree.h"

namespace Db {
  class DBTruth: public DatabaseBase,public XYZTree {

public:

 DBTruth();
 ~DBTruth() {}
 DBTruth(const DBTruth &cp);
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 string sclass;
 string datafile_name;
 float dec;
 double dec_dbl;
 float e;
 double gamma1;
 double gamma2;
 int gsn_aug10_truth_id;
 float g_mag;
 float i_mag;
 double kappa;
 double magn;
 int mudec;
 int mura;
 float pa;
 float ra;
 double ra_dbl;
 float re;
 float redshift;
 float r_mag;
 int sde;
 float sersic_n;
 int sra;
 int star_galaxy_id;
 float u_mag;
 float y_mag;
 float z_mag;


ClassDef(DBTruth,2);
};
}
#endif
