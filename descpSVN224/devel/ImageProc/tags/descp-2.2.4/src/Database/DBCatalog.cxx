#include "DBCatalog.h"

using namespace Db;
ClassImp(DBCatalog)

DBCatalog::DBCatalog():
  DatabaseBase()
{
 this->AddTable("catalog","band,catalogname,catalogtype,ccd,exposureid,id,nite,objects,parentid,project,run,tilename");
}


bool DBCatalog::SetVals(DatabaseResult &rec,const int &first) 
{

    if(!rec.IsNull(first+0))band=rec.GetString(first+0);
    else band="";
    if(!rec.IsNull(first+1))catalogname=rec.GetString(first+1);
    else catalogname="";
    if(!rec.IsNull(first+2))catalogtype=rec.GetString(first+2);
    else catalogtype="";
    if(!rec.IsNull(first+3))ccd=rec.GetInt(first+3);
    if(!rec.IsNull(first+4))exposureid=rec.GetInt(first+4);
    if(!rec.IsNull(first+5))id=rec.GetInt(first+5);
    if(!rec.IsNull(first+6))nite=rec.GetString(first+6);
    else nite="";
    objects=rec.GetInt(first+7);
    parentid=rec.GetInt(first+8);
    if(!rec.IsNull(first+9))project=rec.GetString(first+9);
    else project="";
    run=rec.GetString(first+10);
    if(!rec.IsNull(first+11))tilename=rec.GetString(first+11);
    else tilename="";

}

