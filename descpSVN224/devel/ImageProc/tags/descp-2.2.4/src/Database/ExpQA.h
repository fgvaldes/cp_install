#ifndef EXPQA_H
#define EXPQA_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class ExpQA: public DatabaseBase {

public:

 ExpQA();
 ~ExpQA() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 double astromchi2_reference;
 double astromchi2_reference_highsn;
 double astromcorr_reference;
 double astromcorr_reference_highsn;
 double astromndets_reference;
 double astromndets_reference_highsn;
 double astromoffset_reference_1;
 double astromoffset_reference_2;
 double astromoffset_reference_highsn_1;
 double astromoffset_reference_highsn_2;
 double astromsigma_reference_1;
 double astromsigma_reference_2;
 double astromsigma_reference_highsn_1;
 double astromsigma_reference_highsn_2;
 double exposureid;
 double id;
 string nite;
 string project;
 string run;


ClassDef(ExpQA,1);
};
}
#endif
