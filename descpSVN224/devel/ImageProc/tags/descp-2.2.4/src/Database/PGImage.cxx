#include "PGImage.h"

using namespace Db;
ClassImp(PGImage)

PGImage::PGImage():
  DatabaseBase()
{
 this->AddTable("image","airmass,ampseca,ampsecb,band,biasseca,biassecb,bscale,bunit,bzero,ccd,ccdname,ccdsum,cd1_1,cd1_2,cd2_1,cd2_2,crpix1,crpix2,crval1,crval2,ctype1,ctype2,cunit1,cunit2,cx,cy,cz,datasec,dec,detsec,device_id,elliptic,equinox,exposureid,exptime,fwhm,gaina,gainb,gcount,htmid,id,imagename,imagetype,ltm1_1,ltm2_2,ltv1,ltv2,naxis1,naxis2,nextend,nite,parentid,pcount,project,pv1_0,pv1_1,pv1_10,pv1_2,pv1_3,pv1_4,pv1_5,pv1_6,pv1_7,pv1_8,pv1_9,pv2_0,pv2_1,pv2_10,pv2_2,pv2_3,pv2_4,pv2_5,pv2_6,pv2_7,pv2_8,pv2_9,ra,radesys,rdnoisea,rdnoiseb,run,saturata,saturatb,scampchi,scampflg,scampnum,seesiga,sky,skybrite,skysigma,tilename,trimsec,wcsaxes,wcsdim,zero,zp");
}


bool PGImage::SetVals(DatabaseResult &rec,const int &first) 
{

  airmass=rec.GetDouble(first+0);
  if(!rec.IsNull(first+1)) ampseca=rec.GetString(first+1);
  else ampseca="";
  if(!rec.IsNull(first+2)) ampsecb=rec.GetString(first+2);
  else ampsecb="";
  if(!rec.IsNull(first+3)) band=rec.GetString(first+3);
  else band="";
  if(!rec.IsNull(first+4)) biasseca=rec.GetString(first+4);
  else biasseca="";
  if(!rec.IsNull(first+5)) biassecb=rec.GetString(first+5);
  else biassecb="";
  bscale=rec.GetDouble(first+6);
  if(!rec.IsNull(first+7)) bunit=rec.GetString(first+7);
  else bunit="";
  bzero=rec.GetDouble(first+8);
  ccd=rec.GetDouble(first+9);
  if(!rec.IsNull(first+10)) ccdname=rec.GetString(first+10);
  else ccdname="";
  if(!rec.IsNull(first+11)) ccdsum=rec.GetString(first+11);
  else ccdsum="";
  cd1_1=rec.GetDouble(first+12);
  cd1_2=rec.GetDouble(first+13);
  cd2_1=rec.GetDouble(first+14);
  cd2_2=rec.GetDouble(first+15);
  crpix1=rec.GetDouble(first+16);
  crpix2=rec.GetDouble(first+17);
  crval1=rec.GetDouble(first+18);
  crval2=rec.GetDouble(first+19);
  if(!rec.IsNull(first+20)) ctype1=rec.GetString(first+20);
  else ctype1="";
  if(!rec.IsNull(first+21)) ctype2=rec.GetString(first+21);
  else ctype2="";
  if(!rec.IsNull(first+22)) cunit1=rec.GetString(first+22);
  else cunit1="";
  if(!rec.IsNull(first+23)) cunit2=rec.GetString(first+23);
  else cunit2="";
  cx=rec.GetDouble(first+24);
  cy=rec.GetDouble(first+25);
  cz=rec.GetDouble(first+26);
  if(!rec.IsNull(first+27)) datasec=rec.GetString(first+27);
  else datasec="";
  dec=rec.GetDouble(first+28);
  if(!rec.IsNull(first+29)) detsec=rec.GetString(first+29);
  else detsec="";
  device_id=rec.GetInt(first+30);
  elliptic=rec.GetDouble(first+31);
  equinox=rec.GetDouble(first+32);
  exposureid=rec.GetInt(first+33);
  exptime=rec.GetDouble(first+34);
  fwhm=rec.GetDouble(first+35);
  gaina=rec.GetDouble(first+36);
  gainb=rec.GetDouble(first+37);
  gcount=rec.GetDouble(first+38);
  htmid=rec.GetDouble(first+39);
  id=rec.GetInt(first+40);
  if(!rec.IsNull(first+41)) imagename=rec.GetString(first+41);
  else imagename="";
  if(!rec.IsNull(first+42)) imagetype=rec.GetString(first+42);
  else imagetype="";
  ltm1_1=rec.GetDouble(first+43);
  ltm2_2=rec.GetDouble(first+44);
  ltv1=rec.GetDouble(first+45);
  ltv2=rec.GetDouble(first+46);
  naxis1=rec.GetInt(first+47);
  naxis2=rec.GetInt(first+48);
  nextend=rec.GetDouble(first+49);
  if(!rec.IsNull(first+50)) nite=rec.GetString(first+50);
  else nite="";
  parentid=rec.GetInt(first+51);
  pcount=rec.GetInt(first+52);
  if(!rec.IsNull(first+53)) project=rec.GetString(first+53);
  else project="";
  pv1_0=rec.GetDouble(first+54);
  pv1_1=rec.GetDouble(first+55);
  pv1_10=rec.GetDouble(first+56);
  pv1_2=rec.GetDouble(first+57);
  pv1_3=rec.GetDouble(first+58);
  pv1_4=rec.GetDouble(first+59);
  pv1_5=rec.GetDouble(first+60);
  pv1_6=rec.GetDouble(first+61);
  pv1_7=rec.GetDouble(first+62);
  pv1_8=rec.GetDouble(first+63);
  pv1_9=rec.GetDouble(first+64);
  pv2_0=rec.GetDouble(first+65);
  pv2_1=rec.GetDouble(first+66);
  pv2_10=rec.GetDouble(first+67);
  pv2_2=rec.GetDouble(first+68);
  pv2_3=rec.GetDouble(first+69);
  pv2_4=rec.GetDouble(first+70);
  pv2_5=rec.GetDouble(first+71);
  pv2_6=rec.GetDouble(first+72);
  pv2_7=rec.GetDouble(first+73);
  pv2_8=rec.GetDouble(first+74);
  pv2_9=rec.GetDouble(first+75);
  ra=rec.GetDouble(first+76);
  if(!rec.IsNull(first+77)) radesys=rec.GetString(first+77);
  else radesys="";
  rdnoisea=rec.GetDouble(first+78);
  rdnoiseb=rec.GetDouble(first+79);
  if(!rec.IsNull(first+80)) run=rec.GetString(first+80);
  else run="";
  saturata=rec.GetDouble(first+81);
  saturatb=rec.GetDouble(first+82);
  scampchi=rec.GetDouble(first+83);
  scampflg=rec.GetDouble(first+84);
  scampnum=rec.GetInt(first+85);
  seesiga=rec.GetDouble(first+86);
  sky=rec.GetDouble(first+87);
  skybrite=rec.GetDouble(first+88);
  skysigma=rec.GetDouble(first+89);
  if(!rec.IsNull(first+90)) tilename=rec.GetString(first+90);
  else tilename="";
  if(!rec.IsNull(first+91)) trimsec=rec.GetString(first+91);
  else trimsec="";
  wcsaxes=rec.GetDouble(first+92);
  wcsdim=rec.GetDouble(first+93);
  zero=rec.GetDouble(first+94);
  zp=rec.GetDouble(first+95);
}

