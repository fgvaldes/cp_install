#ifndef DBCATALOG_H
#define DBCATALOG_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class DBCatalog: public DatabaseBase {

public:

 DBCatalog();
 ~DBCatalog() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 string band;
 string catalogname;
 string catalogtype;
 int ccd;
 int exposureid;
 int id;
 string nite;
 int objects;
 int parentid;
 string project;
 string run;
 string tilename;


ClassDef(DBCatalog,1);
};
}
#endif
