#ifndef DBSHEAR_H
#define DBSHEAR_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class DBShear: public DatabaseBase {

public:

 DBShear();
 ~DBShear() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 int catalogid;
 int gal_order;
 int id;
 long object_id;
 int object_number;
 float shapelet_sigma;
 double shear1;
 double shear2;
 double shear_cov00;
 double shear_cov01;
 double shear_cov11;
 int shear_flags;
 float shear_signal_to_noise;


ClassDef(DBShear,1);
};
}
#endif
