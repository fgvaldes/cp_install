#include "DatabaseInterface.h"

#include <iostream>
#include <fstream>
#include <stdlib.h>

//#include "TSQLColumnInfo.h"
//#include "TList.h"

using std::cout;
using std::cerr;
using std::endl;
using namespace Db;
ClassImp(DatabaseInterface)

DatabaseInterface::DatabaseInterface(const string &db,string file,
                                     const bool &load):
  server(0),
  load_tables(load)
{
  if(file.empty()) file=Form("%s/.desdm",getenv("HOME"));

  // read in key values from desdm file
  map<string,string> keys;

  ifstream infile(file.c_str());
  string s1,s2;
  while(infile>>s1>>s2) {
    keys[s1]=s2;
  }

  db_user=keys["DB_USER"];
  db_passwd=keys["DB_PASSWD"];

  if(db.empty()) {
    string dbtype=keys["DB_TYPE"];
    if(dbtype=="oracle") {
      db_type=kDBOracle;
      db_name="oracle://"+keys["DB_SERVER"]+"/"+keys["DB_NAME"];
    } 
    else if (dbtype=="postgres" || dbtype=="POSTGRES") {
     db_type= kDBPsql;
     db_name="pgsql://"+keys["DB_SERVER"]+"/"+keys["DB_NAME"];
    }
    else {
      cout<<"Not given a valid db_type:"<<db_type<<endl;

      exit(1);
    }
    
  }
  else {
    
    if(db=="pr") {
      db_name="oracle://"+keys["DB_SERVER"]+"/"+keys["DB_NAME"];
      db_type=kDBOracle;
    }
    else if(db=="st") {
      db_name="oracle://"+keys["DB_SERVER_STANDBY"]+"/"+
        keys["DB_NAME_STANDBY"];
      db_type=kDBOracle;
    }
  else if(db=="fm") {
    db_name="oracle://"+keys["DB_SERVER_FM"]+"/"+
      keys["DB_NAME_FM"];
    db_type=kDBOracle;
  }
  else if(db=="pg") {
    db_user=keys["DB_USER_PG"];
    db_passwd=keys["DB_PASSWD_PG"];
    db_name="pgsql://"+keys["DB_SERVER_PG"]+"/"+
      keys["DB_NAME_PG"];
    db_type=kDBPsql;
  }
  }
  bool success=this->Open();
  
  if(!success) exit(1);
  if(load_tables && db_type==kDBOracle) {
    bool loaded=LoadTable("OBJECTS");
    loaded=LoadTable("COADD_OBJECTS");
    loaded=LoadTable("IMAGE");
    loaded=LoadTable("EXPOSURE");
    loaded=LoadTable("ZEROPOINT");
    loaded=LoadTable("CATALOG");
    loaded=LoadTable("LOCATION");
    loaded=LoadTable("GSN_OCT10_TRUTH");
    loaded=LoadTable("DC5_TRUTH");
  }
  
}


bool DatabaseInterface::Open()
{

  if(!this->IsClosed()) return true;

  
  server = TSQLServer::Connect(db_name.c_str(),db_user.c_str(),
                               db_passwd.c_str());
  
  if(!server) {
    cerr<<"Could not open database to: "<<db_name<<endl;
    return false;
  }

  return true;
}
 

bool DatabaseInterface::Commit()
{
  if ( !this->Open() ) return server->Commit();
  else return false;
}


   bool DatabaseInterface::Close()
{
  delete server;
  server = 0;
  return 1;
}

TSQLStatement* DatabaseInterface::CreateStatement(const std::string& sql) 
{

 //  Purpose:  Open if necessary and get a prepared statment.
 //
 //  Return:    Statement - Caller must take ownership.
 //             will be 0 if failure.
  // close the database before each call
  this->Close();


  TSQLStatement* stmt = 0;
  if ( !this->Open() ) return stmt;
  stmt = server->Statement(sql.c_str());
  if ( !stmt ) {
    return 0;
  }
 
   return stmt;
}


bool DatabaseInterface::LoadTable(const string &table)
{

  TString tablename(table);
  tablename.ToUpper();

  if(this->IsLoaded(tablename.Data())) return true;

  MetaTable mt(tablename.Data());
  
  string db_cmd;
  if(db_type==kDBOracle) {
    db_cmd=Form("SELECT COLUMN_NAME,DATA_TYPE FROM "
                "ALL_TAB_COLUMNS where table_name='%s' order by column_name",table.c_str());
  }
  else {
    db_cmd=Form("SELECT COLUMN_NAME,DATA_TYPE FROM "
                "INFORMATION_SCHEMA.COLUMNS where table_name='%s' order by column_name",table.c_str());
  }
  TSQLStatement *stmt=this->CreateStatement(db_cmd);
  
  bool ok=stmt->Process();
  if(!ok) {
    delete stmt;
    return false;
  }

  ok=stmt->StoreResult();
  if(!ok) {
    delete stmt;
    return false;
  }
  
  while(stmt->NextResultRow()) {
    string name=stmt->GetString(0);
    string type=stmt->GetString(1);
    mt.AddCol(name,this->StringToDB(type));
  }
  delete stmt;

  if(mt.Size()>0) meta_table.push_back(mt);
  else return false;
  
  return true;
}


bool DatabaseInterface::IsLoaded(const string &table)
{
  TString tablename(table);
  tablename.ToUpper();
  string s=tablename.Data();

  vector<MetaTable>::iterator iter=meta_table.begin();
  for(; iter!=meta_table.end(); ++iter) {
    if(iter->Name==s) return true;
  }

  return false;
}

bool DatabaseInterface::GetMeta(const string &table,MetaTable &meta)
{
  if(!this->IsLoaded(table)) {
    
    bool found=this->LoadTable(table);
    if(!found) return false;
  }

  TString tablename(table);
  tablename.ToUpper();
  string s=tablename.Data();

  vector<MetaTable>::iterator iter=meta_table.begin();
  for(; iter!=meta_table.end(); ++iter) {
    if(iter->Name==s) {
      meta=*iter;
      return true;
    }
  }
  
  // shouldn't reach here
  return false;
}

ColType DatabaseInterface::StringToDB(const string &val)
{
  TString ts(val.c_str());
  if(ts.BeginsWith("VAR") || ts.BeginsWith("var") || ts.BeginsWith("char")) return kDBString;
  else if(ts.BeginsWith("NUMBER") || ts.BeginsWith("number") ||  ts.BeginsWith("int")) {
    return kDBInt;
  }
  else if(ts.BeginsWith("BINARY_F") || ts.BeginsWith("binary_f") || ts.BeginsWith("float")) {
    return kDBFloat;
  }
  else if(ts.BeginsWith("BINARY_F") || ts.BeginsWith("binary_f") || ts.BeginsWith("double")) {
    return kDBDouble;
  }
}


