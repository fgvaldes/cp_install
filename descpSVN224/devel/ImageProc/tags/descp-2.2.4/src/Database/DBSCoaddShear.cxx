#include "DBSCoaddShear.h"

using namespace Db;
ClassImp(DBSCoaddShear)

DBSCoaddShear::DBSCoaddShear():
  DatabaseBase()
{
 this->AddTable("WL_ME_SHEAR","coadd_object_id,dec,id,nimages_found_g,nimages_found_i,nimages_found_r,nimages_found_y,nimages_found_z,ra,shapelet_sigma_g,shapelet_sigma_i,shapelet_sigma_r,shapelet_sigma_y,shapelet_sigma_z,shear1_g,shear1_i,shear1_r,shear1_y,shear1_z,shear2_g,shear2_i,shear2_r,shear2_y,shear2_z,shear_cov00_g,shear_cov00_i,shear_cov00_r,shear_cov00_y,shear_cov00_z,shear_cov01_g,shear_cov01_i,shear_cov01_r,shear_cov01_y,shear_cov01_z,shear_cov11_g,shear_cov11_i,shear_cov11_r,shear_cov11_y,shear_cov11_z,shear_flags_g,shear_flags_i,shear_flags_r,shear_flags_y,shear_flags_z,shear_signal_to_noise_g,shear_signal_to_noise_i,shear_signal_to_noise_r,shear_signal_to_noise_y,shear_signal_to_noise_z");
}


bool DBSCoaddShear::SetVals(DatabaseResult &rec,const int &first) 
{

  coadd_object_id=rec.GetInt(first+0);
  dec=rec.GetDouble(first+1);
  id=rec.GetInt(first+2);
  nimages_found_g=rec.GetInt(first+3);
  nimages_found_i=rec.GetInt(first+4);
  nimages_found_r=rec.GetInt(first+5);
  nimages_found_y=rec.GetInt(first+6);
  nimages_found_z=rec.GetInt(first+7);
  ra=rec.GetDouble(first+8);
  shapelet_sigma_g=rec.GetDouble(first+9);
  shapelet_sigma_i=rec.GetDouble(first+10);
  shapelet_sigma_r=rec.GetDouble(first+11);
  shapelet_sigma_y=rec.GetDouble(first+12);
  shapelet_sigma_z=rec.GetDouble(first+13);
  shear1_g=rec.GetDouble(first+14);
  shear1_i=rec.GetDouble(first+15);
  shear1_r=rec.GetDouble(first+16);
  shear1_y=rec.GetDouble(first+17);
  shear1_z=rec.GetDouble(first+18);
  shear2_g=rec.GetDouble(first+19);
  shear2_i=rec.GetDouble(first+20);
  shear2_r=rec.GetDouble(first+21);
  shear2_y=rec.GetDouble(first+22);
  shear2_z=rec.GetDouble(first+23);
  shear_cov00_g=rec.GetDouble(first+24);
  shear_cov00_i=rec.GetDouble(first+25);
  shear_cov00_r=rec.GetDouble(first+26);
  shear_cov00_y=rec.GetDouble(first+27);
  shear_cov00_z=rec.GetDouble(first+28);
  shear_cov01_g=rec.GetDouble(first+29);
  shear_cov01_i=rec.GetDouble(first+30);
  shear_cov01_r=rec.GetDouble(first+31);
  shear_cov01_y=rec.GetDouble(first+32);
  shear_cov01_z=rec.GetDouble(first+33);
  shear_cov11_g=rec.GetDouble(first+34);
  shear_cov11_i=rec.GetDouble(first+35);
  shear_cov11_r=rec.GetDouble(first+36);
  shear_cov11_y=rec.GetDouble(first+37);
  shear_cov11_z=rec.GetDouble(first+38);
  shear_flags_g=rec.GetInt(first+39);
  shear_flags_i=rec.GetInt(first+40);
  shear_flags_r=rec.GetInt(first+41);
  shear_flags_y=rec.GetInt(first+42);
  shear_flags_z=rec.GetInt(first+43);
  shear_nu_g=rec.GetDouble(first+44);
  shear_nu_i=rec.GetDouble(first+45);
  shear_nu_r=rec.GetDouble(first+46);
  shear_nu_y=rec.GetDouble(first+47);
  shear_nu_z=rec.GetDouble(first+48);
}

