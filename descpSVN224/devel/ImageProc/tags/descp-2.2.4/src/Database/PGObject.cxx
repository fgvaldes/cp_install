#include "PGObject.h"

using namespace Db;
ClassImp(PGObject)

PGObject::PGObject():
  DatabaseBase()
{
 this->AddTable("objects_template","alpha_j2000,band,catalogid,class_star,dec,delta_j2000,errx2_world,erry2_world,flags,imageid,mag_aper_8,mag_auto,magerr_aper_8,magerr_auto,object_id,ra,x_image,y_image,zeropoint");
}


bool PGObject::SetVals(DatabaseResult &rec,const int &first) 
{

  alpha_j2000=rec.GetDouble(first+0);
  if(!rec.IsNull(first+1)) band=rec.GetString(first+1);
  else band="";
  catalogid=rec.GetDouble(first+2);
  class_star=rec.GetDouble(first+3);
  dec=rec.GetDouble(first+4);
  delta_j2000=rec.GetDouble(first+5);
  errx2_world=rec.GetDouble(first+6);
  erry2_world=rec.GetDouble(first+7);
  flags=rec.GetDouble(first+8);
  imageid=rec.GetDouble(first+9);
  mag_aper_8=rec.GetDouble(first+10);
  mag_auto=rec.GetDouble(first+11);
  magerr_aper_8=rec.GetDouble(first+12);
  magerr_auto=rec.GetDouble(first+13);
  object_id=rec.GetDouble(first+14);
  ra=rec.GetDouble(first+15);
  x_image=rec.GetDouble(first+16);
  y_image=rec.GetDouble(first+17);
  zeropoint=rec.GetDouble(first+18);
  this->SetRADec(alpha_j2000,delta_j2000);
}

