#include "DatabaseResult.h"
#include <iostream>
using namespace Db;
ClassImp(DatabaseResult)

DatabaseResult::DatabaseResult():
  stmt(0),
  num_fields(0)
{
  
}

bool DatabaseResult::Query(DatabaseInterface &db,const string &cmd)
{
  stmt=db.CreateStatement(cmd);
  bool ok=stmt->Process();
  if(!ok) return false;
  
  

  stmt->StoreResult();
  num_fields=stmt->GetNumFields();

  columns.clear();

  for(int i=0;i<num_fields; i++) {
    columns.push_back(stmt->GetFieldName(i));
  }
  


  return true;
}


bool DatabaseResult::Process(DatabaseInterface &db,const string &cmd)
{
  stmt=db.CreateStatement(cmd);
  return stmt->Process();
}
bool DatabaseResult::NextRow()
{
  if (stmt->NextResultRow()) {
    return true;
  }
  return false;
}

double DatabaseResult::GetDouble(const string &col)
{
  int index=-1;
  if(stmt) {
    // find the index
    for(int i=0;i<num_fields;++i) {
      if(col==columns[i]) index=i;
    }
  }

  if(index>0) {
    return this->GetDouble(index);
  }

  else return -999.;
   
}

int DatabaseResult::GetInt(const string &col)
{
  int index=-1;
  if(stmt) {
    // find the index
    for(int i=0;i<num_fields;++i) {
      if(col==columns[i]) index=i;
    }
  }

  if(index>0) {
    return this->GetInt(index);
  }
   
  else return -999;
}

string DatabaseResult::GetString(const string &col)
{
  int index=-1;
  if(stmt) {
    // find the index
    for(int i=0;i<num_fields;++i) {
      if(col==columns[i]) index=i;
    }
  }

  if(index>0) {
    return this->GetString(index);
  }
   
  else return "";
}

long DatabaseResult::GetLong(const string &col)
{
  int index=-1;
  if(stmt) {
    // find the index
    for(int i=0;i<num_fields;++i) {
      if(col==columns[i]) index=i;
    }
  }

  if(index>0) {
    return this->GetLong(index);
  }

  else return -999;
  
}


