/*
 * PixTools.h
 *
 *  Created on: Apr 3, 2012
 *      Author: kuropat
 */

#ifndef PIXTOOLS_H_
#define PIXTOOLS_H_
#define _USE_MATH_DEFINES
//#include "BitManip.h"
#include "Vector3D.h"
#include <vector>
#include <math.h>
#include "PixInfo.h"
#include <algorithm>


namespace std {

class PixTools {
	/**
	 * magic1 odd bits set constant
	 */
	static const int magic1 = 89478485; //  101010101010101010101010101 LSB
	/**
	 * magic2 - even bits set constant
	 */
	static const int magic2 = 178956970; //1010101010101010101010101010 MSB

	static const double twothird = 0.666666667;

	static const  double PI =  M_PI;


	static  const double TWOPI = 2. * M_PI;

	static const double RadToDeg = 180./M_PI;

	static const double DegToRad = M_PI/180.;

    static const double HALFPI = M_PI / 2.0;

//    static const int ns_max = 1048576; // 2^20
//    static const int ns_max = 8192; // 2^13

//
//    static const int xmax = 4096;
//
//    static const int pixmax = 262144;
//    static const int pixmax = 50331648;

//
//    static const int xmid = 512;



//    static BitManip* bm;

    static PixInfo pixinfo;

public:
//    static const int ns_max = 1048576; // 2^20
//    static const int ns_max = 65536; // 2^16
//    static const int ns_max =262144;
    static const int ns_max =1024;
//
    static const int xmax = 4096;
//
//    static const int pixmax = 262144;
//    static const int pixmax = 50331648;
    static const int pixmax = 524288;

//
    static const int xmid = 512;
	double dround(double );
	int swapLSBMSB(int );
	int invswapLSBMSB(int );
	int invLSB(int );
	int invMSB(int );
	float MODULO(float , float );
	vector<int>* query_triangle(int , Vector3D , Vector3D ,Vector3D ,int , int );
	vector<int>* query_polygon(int , vector<Vector3D> , int ,int );
	vector<int>* query_strip(int , double , double ,int nest);
	vector<double>* intrs_intrv(vector<double>* d1, vector<double>* d2);
	vector<int> *getDisc_ring(int nside, Vector3D vector0, double radius);
	vector<int>* query_disc(int , Vector3D , double ,int , int );
	double* pix2ang_ring(int nside, int ipix);
	Vector3D pix2vect_ring(int nside, int ipix);
	double** pix2vertex_ring(int nside, int ipix);
	PixInfo makePix2Vect_ring(int nside, int ipix);
	int ang2pix_ring(int nside, double theta, double phi);
	int vect2pix_ring(int nside, Vector3D vector);
	double* pix2ang_nest(int nside, int ipix);
	Vector3D pix2vect_nest(int nside, int ipix);
	double** pix2vertex_nest(int nside, int ipix);
	PixInfo makePix2Vect_Nest(int nside, int ipix);
	int ang2pix_nest(int nside, double theta, double phi);
	int* convert_nest2ring(int nside, int* map);
	int* convert_ring2nest(int nside, int* map);
	vector<int>* neighbours_nest(int nside, int ipix);
	vector<int>* InRing(int nside, int iz, double phi0, double dphi,bool nest);
	int next_in_line_nest(int nside, int ipix);
	int vect2pix_nest(int nside, Vector3D vector);
	int xy2pix_nest(int nside, int ix, int iy, int face_num);
	int* pix2xy_nest(int nside, int ipf);
	void mk_pix2xy();
	int nest2ring(int nside, int ipnest);
	int ring2nest(int nside, int ipring);
	void mk_xy2pix();
	int RingNum(int nside, double z);
	Vector3D Ang2Vec(double theta, double phi);
	int Npix2Nside(int npix);
	int Nside2Npix(int nside);
	double SurfaceTriangle(Vector3D v1, Vector3D v2, Vector3D v3);
	double AngDist(Vector3D v1, Vector3D v2);
	Vector3D VectProd(Vector3D v1, Vector3D v2);
	double dotProduct(Vector3D v1, Vector3D v2);
	Vector3D crossProduct(Vector3D v1, Vector3D v2);
	int GetNSide(double pixsize);
	double PixRes(int nside);
	double* xyzToPolar(double , double , double );
	double* PolarToRaDec(double polar[]);
	double* RaDecToPolar(double radec[]);
	double* Vect2Ang(Vector3D );
	bool contains(vector<int>, int);
	vector<int>  *addAll(vector<int> *a, vector<int> *b);
	void setUp();
	PixTools();
	~PixTools();
};

} /* namespace std */
#endif /* PIXTOOLS_H_ */
