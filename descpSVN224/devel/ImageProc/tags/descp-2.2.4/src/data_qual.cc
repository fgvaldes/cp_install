#include "SysErrFit.h"
#include "ShearFit.h"
#include "TTree.h"
#include "TMath.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "TDirectory.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TVirtualFitter.h"
#include "TMath.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TF1.h"
#include "TLine.h"
#include "TCanvas.h"
#include "TPaveStats.h"
#include "TPad.h"
#include "TLatex.h"
#include "TObject.h"
#include "TSystem.h"
#include "TFile.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TGaxis.h"
#include "TPaveLabel.h"
#include "TROOT.h"
#include "TRandom3.h"
#include "kdtree++/kdtree.hpp"
#include "Object.h"
#include "Math/Minimizer.h"
#include "Minuit2/Minuit2Minimizer.h"
using namespace std;

void LoadStyle();


// Quick class to minimize to a value of the data
class FitGraph: public ROOT::Math::IMultiGenFunction {

public:
  FitGraph(TGraph &_g):graph(_g) { }
  double Fit(const double &val);
  double DoEval(const double* par) const
  {
    
    double diff=graph.Eval(par[0],0,"s")-value;
    return diff*diff;
  }

  // Complete the IMultiGenFunction interface
  unsigned int                   NDim() const {return 1;}
  ROOT::Math::IMultiGenFunction* Clone() const { return 0;}
  
private:
  TGraph graph;
  double value;
  
};


double FitGraph::Fit(const double &val)
{
  value=val;
  
  ROOT::Minuit2::Minuit2Minimizer mini(ROOT::Minuit2::kCombined);
  mini.SetPrintLevel(0);
  mini.SetTolerance(0.0000001);
  mini.SetStrategy(2);
  mini.SetMaxFunctionCalls(30000);
  mini.SetLimitedVariable(0, "par", 16,0.01,16, 26);
    
  mini.SetFunction(*this);
  mini.Minimize();
    
  return mini.X()[0];
}


// Plotting functions
void plot_phot(string inputfile,string title="",string dir=".",
	       string which_band="all",
	       string which_mag="auto",string which_truth="true",
	       bool coadd=true,bool bcs=false,
	       bool debug=false,bool draw_text=true,double err_cut=0.1,
               int phot_flag=3,
	       double sigma=3,int N_bin=1000,int max_scatter=5000,
               bool fixed_bins = true);

void plot_color_true(string inputfile,string title="",string dir=".",
                     string which_mag="auto",string which_truth="true",
                     bool coadd=true,bool bcs=false,
                     bool debug=false,bool draw_text=true,double err_cut=0.1,
                     int phot_flag=3,
                     double sigma=3,int N_bin=1000,int max_scatter=5000,
                     bool fixed_bins = true);

void plot_color(string inputfile,string title="",string dir=".",
                string which_mag="auto",string which_truth="true",
                bool coadd=true,bool bcs=false,
                bool debug=false,bool draw_text=true,double err_cut=0.1,
                int phot_flag=3,
                double sigma=3,int N_bin=1000,int max_scatter=5000);

int plot_astro(string input_file,string title,string dir=".",
	       string which_band="all",
	       bool coadd=true,bool bcs=false,bool debug=false);

int plot_diffxy(string inputfile,string title,
		string dir=".",bool coadd=true,
		string band="g",string which_mag="auto",
		string which_truth="true",
		int bins=150,int neighbors=20,
		bool bcs=false,bool debug=false);

void plot_comp(string inputfile,string truthfile,double tol,double max_mag,
               string title="",string dir=".",
	       string which_band="all",
	       string which_mag="auto",string which_truth="true",
               bool coadd=true,bool bcs=false,
               bool debug=false,double err_cut=0.1);



string get_branches(bool coadd,string which_band,bool bcs=false);
void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol);

// functions used in plotting
double BifurGaus(double *x, double *par);
typedef KDTree::KDTree<3,Object> KD_Tree;
typedef std::pair<KD_Tree::iterator,double> KD_Match;
using namespace std;

int main(int argc, char **argv)
{
  
    

  string inputfile,truthfile,title="",which_band="all",which_obs="auto",dir=".",
    which_truth="true",algs="all";
  bool debug=false,coadd=false,bcs=false,draw_text=true;
  bool flag_phot=0,flag_astro=0,flag_diff=0,flag_comp=0,flag_color=0;
  bool flag_color_true=0;
  bool fixed_bins=true;
  int N_bin=1000,sigma=3,diff_bins=150,diff_neighbors=20;
  int max_scatter_points=4500;
  double err_cut=0.1;
  double color_err_cut=0.1;
  double tol=2;
  double max_mag=2.5;
  int phot_flags_cut=3;
    
  for(int i=1;i<argc;i++) {
    string arg=argv[i];
    if(arg=="-inputfile")    {inputfile=argv[i+1];i++;}
    else if(arg=="-truthfile")    {truthfile=argv[i+1];i++;}
    else if(arg=="-dir")     {dir=argv[i+1];i++;}
    else if(arg=="-title")   {title=argv[i+1];i++;}
    else if(arg=="-algs")   {algs=argv[i+1];i++;}
    else if(arg=="-band")    {which_band=argv[i+1];i++;}
    else if(arg=="-label")   {which_obs=argv[i+1];i++;}
    else if(arg=="-truth")   {which_truth=argv[i+1];i++;}
    else if(arg=="-debug")   {debug=atoi(argv[i+1]);i++;}
    else if(arg=="-coadd")   {coadd=atoi(argv[i+1]);i++;}
    else if(arg=="-err_cut")   {err_cut=atof(argv[i+1]);i++;}
    else if(arg=="-color_err_cut")   {color_err_cut=atof(argv[i+1]);i++;}
    else if(arg=="-phot_flags")   {phot_flags_cut=atoi(argv[i+1]);i++;}
    else if(arg=="-tol")   {tol=atof(argv[i+1]);i++;}
    else if(arg=="-max_mag") {max_mag=atof(argv[i+1]);i++;}
    else if(arg=="-bcs")   {bcs=atoi(argv[i+1]);i++;}
    else if(arg=="-max_scatter_points"){max_scatter_points=atoi(argv[i+1]);i++;}
    else if(arg=="-PhotBin") {N_bin=atoi(argv[i+1]);i++;}
    else if(arg=="-PhotSig") {sigma=atoi(argv[i+1]);i++;}
    else if(arg=="-DiffBin") {diff_bins=atoi(argv[i+1]);i++;}
    else if(arg=="-DiffNei") {diff_neighbors=atoi(argv[i+1]);i++;}
    else if(arg=="-draw_text") {draw_text=atoi(argv[i+1]);i++;}
    else if(arg=="-fixed_bins") {fixed_bins=atoi(argv[i+1]);i++;}
    else {
      cerr<<"Not a valid command line entry: "<<arg<<endl;
      return 1;
    }
  }      
    
  tol*=1./3600*TMath::DegToRad();

  // Check to see if directory exists
  if(gSystem->Exec(Form("ls %s&>/dev/null",dir.c_str()))) {
    cout<<"Directiry: "<<dir<<" does not exist.  Exiting..."<<endl;
    return 1;
  }


  if(algs=="all") {
    flag_phot=1;flag_astro=1;flag_diff=1;flag_comp=1;
  }
  else {
    if(algs.find("phot") != string::npos) flag_phot=1;
    if(algs.find("astro") != string::npos) flag_astro=1;
    if(algs.find("diff") != string::npos) flag_diff=1;
    if(algs.find("comp") != string::npos) flag_comp=1;
    if(algs.find("color") != string::npos) flag_color=1;
    if(algs.find("delta_col_v_true_mag") != string::npos) flag_color_true=1;
  }
  
    
  if(bcs && which_truth=="true") {
    which_truth="modelmag";
  }
  
  TApplication *app;
  if(debug) app=new TApplication("app", &argc, argv);
  LoadStyle();

  if(flag_phot) plot_phot(inputfile,title,dir,which_band,which_obs,
                          which_truth,coadd,bcs,debug,draw_text,
                          err_cut,phot_flags_cut,sigma,N_bin,
                          max_scatter_points, fixed_bins);
  if(flag_color_true) plot_color_true(inputfile,title,dir,which_obs,
                          which_truth,coadd,bcs,debug,draw_text,
                          color_err_cut,phot_flags_cut,sigma,N_bin,
                          max_scatter_points, fixed_bins);
  if(flag_color) plot_color(inputfile,title,dir,which_obs,
                            which_truth,coadd,bcs,debug,draw_text,
                            color_err_cut,phot_flags_cut,sigma,N_bin,
                           max_scatter_points);
  if(flag_astro) plot_astro(inputfile,title,dir,which_band,coadd,bcs,debug);
  if(flag_diff) plot_diffxy(inputfile,title,dir,coadd,which_band,which_obs,which_truth,
                            diff_bins,diff_neighbors,bcs,debug);
  if(flag_comp) {

    if(truthfile.empty()) {
      cout<<"truth file was not specified"<<endl;
      exit(1);
    }
    
    plot_comp(inputfile,truthfile,tol,max_mag,title,dir,which_band,
              which_obs,which_truth,
              coadd,bcs,debug,err_cut);
  }
    
}


string get_branches(bool coadd,string which_band,bool bcs)
{
  string branch;
  if(coadd){
    
    if(!bcs) {
      branch="tmp1/D:tmp2:classflag/I:RA/D:RA_err:Dec:Dec_err:g_mag_obs:g_obs_err:g_code:gerr_code:g_flags/I:r_mag_obs/D:r_obs_err:r_code:rerr_code:r_flags/I:i_mag_obs/D:i_obs_err:i_code:ierr_code:i_flags/I:z_mag_obs/D:z_obs_err:z_code:zerr_code:z_flags/I:Y_mag_obs/D:Y_obs_err:Y_code:Yerr_code:Y_flags/I:x_pos/D:y_pos:tmp4:RA_true:Dec_true:g_true:r_true:i_true:z_true:Y_true:class/C";
    }
    else {
      branch="tmp1/D:tmp2:classflag/I:RA/D:RA_err:Dec:Dec_err:g_mag_obs:g_obs_err:g_code:gerr_code:g_flags/I:r_mag_obs/D:r_obs_err:r_code:rerr_code:r_flags/I:i_mag_obs/D:i_obs_err:i_code:ierr_code:i_flags/I:z_mag_obs/D:z_obs_err:z_code:zerr_code:z_flags/I:Y_mag_obs/D:Y_obs_err:Y_code:Yerr_code:Y_flags/I:x_pos/D:y_pos:tmp4:RA_true:Dec_true:";
      
      string indexes[12]={"","err","modelmag","modelmagerr","petromag",
			  "petromagerr","petror50","petror50err","petror90",
			  "petror90err","probpsf","extinction"};
      string bcs_bands[5]={"u","g","r","i","z"};
      
      for(int i=0;i<12;i++) {
	for(int j=0;j<5;j++) {
	  if(i==0) branch+=bcs_bands[j]+":";
	  else if(i==1) branch+=bcs_bands[j]+indexes[i]+":";
	  else branch+=indexes[i]+"_"+bcs_bands[j]+":";
	}
      }
    // remove last : from string
    branch=branch.substr(0,branch.size()-1);
    }

  }
  else {
    branch="RA_true/D:Dec_true:mag_true:tmp1:tmp2:RA:RA_err:Dec:Dec_err:mag_obs:mag_obs_err:band/C:tmp3/D:tmp4:class/C:ccd/I:x_pos/D:y_pos:flags/I";
  }

  return branch;

}


string get_truthbranches(bool coadd,string which_band,bool bcs)
{
  string branch;
  if(coadd){
    
    if(!bcs) {
    branch="RA_true/D:Dec_true:g_mag:r_mag:i_mag:z_mag:Y_mag:class/C";
    }
    else {
      branch="tmp1/D:tmp2:classflag/I:RA/D:RA_err:Dec:Dec_err:g_mag_obs:g_obs_err:g_code:g_flags/I:r_mag_obs/D:r_obs_err:r_code:r_flags/I:i_mag_obs/D:i_obs_err:i_code:i_flags/I:z_mag_obs/D:z_obs_err:z_code:z_flags/I:Y_mag_obs/D:Y_obs_err:Y_code:Y_flags/I:x_pos/D:y_pos:tmp4:RA_true:Dec_true:";
      
      string indexes[12]={"","err","modelmag","modelmagerr","petromag",
			  "petromagerr","petror50","petror50err","petror90",
			  "petror90err","probpsf","extinction"};
      string bcs_bands[5]={"u","g","r","i","z"};
      
      for(int i=0;i<12;i++) {
	for(int j=0;j<5;j++) {
	  if(i==0) branch+=bcs_bands[j]+":";
	  else if(i==1) branch+=bcs_bands[j]+indexes[i]+":";
	  else branch+=indexes[i]+"_"+bcs_bands[j]+":";
	}
      }
    // remove last : from string
    branch=branch.substr(0,branch.size()-1);
    }

  }
  else {
    branch="RA_true/D:Dec_true:g_mag:r_mag:i_mag:z_mag:Y_mag:class/C:mura/D:mud";
  }

  return branch;

}
  

void plot_phot(string inputfile,string title, string dir, 
	       string which_band,string which_obs,string which_truth,
	       bool coadd,bool bcs,bool debug,bool draw_text,
               double err_cut,int phot_flags,
	       double sigma,int N_bin,int max_scatter,bool fixed_bins)
{
  int min_entries=50; //minimum entries per bin
  int func_min=0;    // min function limit
  int func_max=1;   // max function limit
  double hist_fit_max; // max for histogram fitting the gaussian
  int func_fit=3; // this*rms is max for function fitting the gaussian
  // use Minuit2 to do the fitting
  // I found that it did better in some cases
  TVirtualFitter::SetDefaultFitter("Minuit2" );

  string bands[5]={"g","r","i","z","Y"};
  string types[2]={"S","G"};
  string int_types[2]={"2","1"};
  string type_labels[2]={"stars","galaxies"};

  string band,type,type_label;

  TCanvas *c1=new TCanvas("c1","");

  
  
  // create tree to hold variables
  string branch=get_branches(coadd,which_band,bcs);
  TTree *tree=new TTree("tree","");
  tree->ReadFile(inputfile.c_str(),branch.c_str());
  
  double RA,RA_true,Dec,Dec_true,x_pos,y_pos;
  int classflag;
  
  // loop over galaxies and stars
  for(int cur_type=0;cur_type<2;cur_type++) {
    type=types[cur_type];
    type_label=type_labels[cur_type];
    
    if(debug)cout<<"Type :"<<type_label<<endl;
    
    // loop over bands
    for(int cur_band=0;cur_band<5;cur_band++) {
      band=bands[cur_band];
      
      if(bcs &&  band=="Y") continue;
      
      if(which_band=="all" || which_band==band) {
   
	string cur_title=band+" band "+title+" "+type_label;
	if(debug) cout<<"Band :"<<band<<endl;
      
	if(!bcs) {
	  if(type=="G") hist_fit_max=0.9*1.5;
	  else hist_fit_max=0.45*1.5;
	}
	else {
  	  if(type=="G") hist_fit_max=0.4*2.5;
	  else hist_fit_max=0.2*2.5;
	}
	c1->Clear();
	c1->cd();
	//hist_fit_max=10;
	// do any pre-selection cuts by producing an event list
	string basic_cut;
	if(!coadd) {
          basic_cut=Form("mag_obs_err<%f && band==\"%s\"",err_cut,
                         band.c_str());
        } 
        else {
          
          basic_cut=Form("%s_obs_err<%f",
                         band.c_str(),err_cut,band.c_str());
        }
          
	if(!bcs) basic_cut+=" && class==\""+type+"\"";
	else basic_cut+=" && classflag=="+int_types[cur_type];
        if(!coadd) basic_cut+=Form(" && flags <%d",phot_flags);
  
	TEventList *elist=0;
	tree->Draw(">>elist",basic_cut.c_str());
	elist = (TEventList*)gDirectory->Get("elist");
	if(!elist) continue;
	
	if(elist->GetN()==0) continue;

	// load the vectors with the data
	double obs_mag,true_mag,diff,err,true_err;


	// allow for different truth labels
	string true_label;
	if(which_truth=="true") true_label=band+"_true";
	else true_label=which_truth+"_"+band;

	tree->ResetBranchAddresses();
        if(!coadd) {
          tree->SetBranchAddress(("mag_obs"),&obs_mag);
          tree->SetBranchAddress("mag_true",&true_mag);
          tree->SetBranchAddress(("mag_obs_err"),&err);
        }
        else {
          tree->SetBranchAddress(Form("%s_mag_obs",band.c_str()),&obs_mag);
          if(!bcs) {
            tree->SetBranchAddress(Form("%s_true",band.c_str()),&true_mag);
          }
          else {
            tree->SetBranchAddress(true_label.c_str(),&true_mag);
          }
          tree->SetBranchAddress(Form("%s_obs_err",band.c_str()),&err);
        }
        
	if(bcs)tree->SetBranchAddress("classflag",&classflag);

	string true_errlabel=which_truth+"err_"+band;
	if(bcs) {
	  tree->SetBranchAddress(true_errlabel.c_str(),&true_err);
	}
	else true_err=0;
	
	
	vector<double> full_obs_mag,full_true_mag,full_diff,full_err,full_true_err;  
	true_err=0;
	int i=0;
	while(tree->GetEntry(elist->GetEntry(i))>0) {
	  full_obs_mag.push_back(obs_mag);
	  full_true_mag.push_back(true_mag);
	  full_diff.push_back(obs_mag-true_mag);
	  full_err.push_back(err);
	  full_true_err.push_back(true_err);
	  i++;
	}
	int N_entries = full_diff.size();

	if(debug) {
	  cout<<"Total Objects: "<<N_entries<<endl;
	}
	
	if(N_entries==0) {delete elist;continue;}

	// Sort tree into index according to true mag
	int *index = new int[N_entries];
	TMath::Sort(N_entries,&full_true_mag[0],index,false);

	// Fill ordered vectors
	vector<double> order_obs_mag,order_true_mag,
	  order_diff,order_err,order_true_err;  
        
        int desired_bins;

        if (fixed_bins)
          N_bin = 50000;  // not really sure here... make it huge.
        else
        {
          desired_bins =8;
          if(1.*N_entries/N_bin> desired_bins) {
            N_bin=N_entries/desired_bins*1.;
          }
        }

	for(int i=0;i<N_entries;i++) {

	  order_obs_mag.insert(order_obs_mag.end(),full_obs_mag[index[i]]);
	  order_true_mag.insert(order_true_mag.end(),full_true_mag[index[i]]);
	  order_diff.insert(order_diff.end(),full_diff[index[i]]);
	  order_err.insert(order_err.end(),full_err[index[i]]);
	  order_true_err.insert(order_true_err.end(),full_true_err[index[i]]);

	}
      

	TGraph *sigma_scatter=new TGraph;  
	TGraph *sigma_gr_scatter=new TGraph;  
        vector<double> bins,sys_error,tot_error,mean_error,mean_vec,frac_passed;
        // number of events in tot_error and sys_error.
        vector<long> nbin_tot, nbin_sys;
	int global_counter=0,sigma_counter=0,sigma_gr_counter=0;

	int bin=0;


        const double histmin   = 13.75;
        const double histmax   = 26.75;
        // for use with all bins fixed the same width
        //const double bin_width = 0.5;
        //const int nfixed_bins = (int)((histmax - histmin)/bin_width);

        // fixed width bins, but with 2 different sizes.
        double tbin[18] = {13.75, 14.75, 15.75, 16.75,
          17.75, 18.75, 19.75, 20.75, 21.75, 22.75, 23.25, 23.75, 24.25,
          24.75, 25.25, 25.75, 26.25, 26.75};
        const int nfixed_bins = 17;
          
        if (fixed_bins)
        {
          if (debug)
          {
            cout << "Using FIXED width bins." << endl;
            cout << "hist range: " << histmin << " " << histmax << endl;
            cout << "nbins: " << nfixed_bins << endl;
          }

          // bins defines the low edge of the histogram bins. It must be length
          // of nbins + 1.
          bins.assign(nfixed_bins + 1, 0.);
          sys_error.assign(nfixed_bins, 0.);
          tot_error.assign(nfixed_bins, 0.);
          mean_error.assign(nfixed_bins, 0.);
          mean_vec.assign(nfixed_bins, 0.);
          frac_passed.assign(nfixed_bins, 0.);
          nbin_tot.assign(nfixed_bins, 0);
          nbin_sys.assign(nfixed_bins, 0);
        }
        else
          cout << "Using VARIABLE width bins." << endl;

        double bin_le;

	// Loop until the end of the vectors have been reached
	bool finished=false;
	while(!finished) {

          // all same size fixed bins
          //bin_le = histmin + bin*bin_width;
          // mixed bin sizes with fixed bins
          bin_le = tbin[bin];

	  if (debug)
            if (!fixed_bins)
              cout << "Bin " << bin << endl;
	  TGraph scatter,scatter_clipped;

	  // create sub list of vectors with either N_bin entries or
	  // the number in a mag difference of 1
	  vector<double> sub_obs_mag,sub_true_mag,sub_diff,sub_err,sub_true_err;

	  int current_min=global_counter;
	  for(int entry=0; entry<N_bin ; entry++) {
      
	    // check if at the end of the vector;
	    if(global_counter  >= N_entries) {
	      finished=true;
	      break;
	    }
	    if(debug)cout<<"  entry "<<entry<<" "<<global_counter<<" "
			 <<order_true_mag[global_counter]<<" "
			 <<order_obs_mag[global_counter]<<endl;

            if (fixed_bins)
            {
              if (order_true_mag[global_counter] >= histmax ||
                  bin_le >= histmax || bin >= nfixed_bins)
              {
                finished=true;
                break;
              }

              // all same size fixed bins
              //double bin_he = bin_le + bin_width;
              // mixed bin sizes with fixed bins
              double bin_he = tbin[bin + 1];
              if (debug)
                if (fixed_bins)
                  cout << "Bin " << bin << ", [" << bin_le << ", " << bin_he <<
                    ")" << endl;

              if (order_true_mag[global_counter] < bin_le)
              {
                global_counter++;
                continue;
              }
              else if (order_true_mag[global_counter] >= bin_he)
                break;
            }
            
	    // check if the mag difference is greater than 1 between the
	    // current and first of the bin
            if (!fixed_bins)
              if (entry>2) {
                if (order_true_mag[global_counter] -
                    order_true_mag[current_min] >1.) {
                  global_counter++;
                  break;
                }
              }

	    scatter.SetPoint(entry,order_true_mag[global_counter],
			     order_diff[global_counter]);

	    sub_obs_mag.insert(sub_obs_mag.end(),order_obs_mag[global_counter]);
	    sub_true_mag.insert(sub_true_mag.end(),order_true_mag[global_counter]);
	    sub_diff.insert(sub_diff.end(),order_diff[global_counter]);
	    sub_err.insert(sub_err.end(),order_err[global_counter]);
	    sub_true_err.insert(sub_true_err.end(),order_true_err[global_counter]);

	    global_counter++;
	  }
    

    
	  if(debug) cout<<"Subset Size "<<sub_diff.size()<<endl;


	  // Make sure there at least min_entries
	  if(sub_diff.size()<min_entries) {
            if(debug)cout<<"Not enough entries in this bin skipping "<<endl;
            if (fixed_bins)
            {
              mean_vec[bin] = 0.;
              sys_error[bin] = 0.;
              tot_error[bin] = 0.;
              mean_error[bin] = 0.;
              frac_passed[bin] = 0.;
              nbin_tot[bin] = 0;
              nbin_sys[bin] = 0;
            }
            else
              continue;
          }
          else {
            vector<double> dum(sub_diff.size());
            int good,bad;
            double mean,rms;
            sigma_clip(3,2.5,sub_diff,dum,mean,
                       good,bad,rms,true,false,1e-6);
            double hist_max=mean+10*rms;
            double hist_min=mean-10*rms;
            if(hist_max>hist_fit_max) hist_max=hist_fit_max;
            if(hist_min<-1*hist_fit_max) hist_min=-hist_fit_max;
            // fit to gaussian and to do  sigma clipping
            TH1D *h1=new TH1D(Form("fit%d",bin+1),"",35,
                              hist_min,hist_max);
            for(int i=0;i<sub_diff.size();i++) {
              h1->Fill(sub_diff[i]);
            }
            if(debug) cout<<"Clipped mean="<<mean<<" rms="<<rms<<endl;
            if(debug) cout<<"Hist range: "<<hist_min<<" "<<hist_max<<endl;
            if(h1->Integral()<min_entries) {
              if(debug) cout<<"Found less than the number of entries in "
                <<"the histogram skipping entries in bin"<<endl;
              h1->SetName("");
              continue;
            }

            double hist_mean=h1->GetMean();
            double hist_rms=h1->GetRMS();
            if(debug) cout<<"Hist Mean "<<hist_mean<<" RMS "<<hist_rms<<endl;

            TF1 *f1=new TF1("f1","gaus",hist_mean-func_fit*hist_rms,
                            hist_mean+func_fit*hist_rms);

            f1->SetNpx(10000);
            f1->SetParameter(1,0);
            h1->Fit(f1,"QR");

            double gaus_const=f1->GetParameter(0);
            double gaus_mean=f1->GetParameter(1);
            double gaus_sigma=f1->GetParameter(2);
            if(debug) cout<<"Fit Results mean="<<gaus_mean<<" sigma="
              <<gaus_sigma<<endl;
            // draw lines to indicate cuts
            if(debug) {

              TLine *hi=new TLine(gaus_mean+sigma*gaus_sigma,0,
                                  gaus_mean+sigma*gaus_sigma,h1->GetMaximum());
              TLine *low=new TLine(gaus_mean-sigma*gaus_sigma,0,
                                   gaus_mean-sigma*gaus_sigma,h1->GetMaximum());

              low->SetLineWidth(3);
              hi->SetLineWidth(3);
              hi->SetLineColor(2);
              low->SetLineColor(2);
              h1->Draw();
              hi->Draw();
              low->Draw();
            }

            // erase name to not have warning messages
            h1->SetName("");

            // cut out all values greater than sigma*gaus_sigma(from fit)
            vector<double> vec_obs_mag,vec_true_mag,vec_diff,vec_err,vec_true_err;

            int clipped_counter=0;
            for(int entry=0; entry<sub_diff.size() ; entry++) {

              if(fabs(sub_diff[entry]-gaus_mean)<sigma*gaus_sigma) {
                vec_obs_mag.insert(vec_obs_mag.end(),sub_obs_mag[entry]);
                vec_true_mag.insert(vec_true_mag.end(),sub_true_mag[entry]);
                vec_diff.insert(vec_diff.end(),sub_diff[entry]);
                vec_err.insert(vec_err.end(),sub_err[entry]);
                vec_true_err.insert(vec_true_err.end(),sub_true_err[entry]);
                scatter_clipped.SetPoint(clipped_counter,sub_true_mag[entry],
                                         sub_diff[entry]);
                clipped_counter++;
                sigma_scatter->SetPoint(sigma_counter,sub_true_mag[entry],
                                        sub_diff[entry]);
                sigma_counter++;
              } else {

                sigma_gr_scatter->SetPoint(sigma_gr_counter,sub_true_mag[entry],
                                           sub_diff[entry]);
                sigma_gr_counter++;
              }


            }


            //h1->Write();


            // calculate how many events were cut



            if(debug) cout<<"Bin "<<bin+1<<" with "<<vec_diff.size()
              <<"clipped entries"<<endl;      

            if(vec_diff.size()<min_entries) {
              if(debug) cout<<"Found fewer good events than minimum"
                <<" skipping entries in bin: "<<vec_diff.size()
                  <<endl;
              continue;
            }

            double frac=1.*vec_diff.size()/sub_diff.size();
            if (fixed_bins)
              frac_passed[bin] = frac;
            else
              frac_passed.push_back(frac);

            if(debug) {
              cout<<"Mag Range: "<<vec_true_mag[0]<<" to "    
                <<vec_true_mag[vec_true_mag.size()-1]<<endl;
            }

            scatter.SetName(Form("scatter%d",bin+1));
            //scatter.Write();

            scatter_clipped.SetName(Form("scatter_clipped%d",bin+1));
            //scatter_clipped.Write();


            SysErrFit fit;
            double err;
            double min=fit.Fit(&vec_diff,&vec_err,err,&vec_true_err);
            if(debug)cout<<"Minimization Minimum Found at = "<<abs(min)<<endl;


            // Look at what the fitting is doing
            if(debug) {
              c1->Update();
              c1->WaitPrimitive();
            }



            // if minimization fails use 0.0 as default
            if(fabs(min)>func_max) {
              cout<<"Error: root finder failed"<<endl;
              min=0.0;
            };

            // set variables
            // Bob points out that with fixed bins we now have a varying
            // events in each bin and should normalize by sqrt(N). But
            // if we want to see the scatter, then we shouldn't do that.
            if (fixed_bins)
            {
              // systematic error of the clipped data
              sys_error[bin] = min;
              //if (vec_diff.size())
              //  sys_error[bin] = min/sqrt(vec_diff.size());
              // width of Gaussian fit to the unclipped data
              tot_error[bin] = gaus_sigma;
              //if (h1->Integral())
              //  tot_error[bin] = gaus_sigma/sqrt(h1->Integral());
              // error on the mean of the unclipped data.
              if (h1->Integral())
                mean_error[bin] = hist_rms/sqrt(h1->Integral());
              // unclipped data mean found by Gaussian fit.
              mean_vec[bin] = gaus_mean;
              nbin_tot[bin] = (long)sub_diff.size();
              nbin_sys[bin] = (long)vec_diff.size();
            }
            else
            {
              sys_error.push_back(min);
              tot_error.push_back(gaus_sigma);
              mean_error.push_back(hist_rms/sqrt(h1->Integral()));
              mean_vec.push_back(gaus_mean);
              nbin_tot.push_back((long)sub_diff.size());
              nbin_sys.push_back((long)vec_diff.size());
            }
          }

          //if (fixed_bins)
          //{
          //  if (bin_le >= histmax)
          //    break;
          //}
          //else
          if (!fixed_bins)
          {
            // get the last of the observed magnitude for binning
            if(bin==0) bins.insert(bins.end(),sub_true_mag.front());
            bins.insert(bins.end(),sub_true_mag.back());
          }
	  bin++;
	}

        // all equal size fixed bins
        //if (fixed_bins)
        //{
        //  bins[0] = histmin;
        //  for (int i = 1; i < bins.size(); i++)
        //    bins[i] = bins[i - 1] + bin_width;
        //}
        // if using mixed bins sizes
        if (fixed_bins)
          for (int i = 0; i < bins.size(); i++)
            bins[i] = tbin[i];

	if(bins.size()==0) {delete elist; continue;}

        //for (int i = 0; i < bins.size(); i++)
        //  cout << "bins[" << i << "]: " << bins[i] << endl;

        //cout << "bins.size(): " << bins.size() << ", mean_vec.size(): " <<
        //  mean_vec.size() << ", sys_error.size(): " << sys_error.size() <<
        //  ", tot_error.size(): " << tot_error.size() <<
        //  ", mean_error.size(): " << mean_error.size() <<
        //  ", frac_passed.size(): " << frac_passed.size() << endl;

	if(debug)cout<<"Finished Fitting"<<endl;
       
	TH1D *hist_sys_err=new TH1D("hist_sys_err",
				    ";mag_{true};Systematic Error / 1000 events",
				    bins.size()-1,&bins[0]);
	TH1D *hist_tot_err=new TH1D("hist_tot_err",
				    ";mag_{true};Systematic Error / 1000 events",
				    bins.size()-1,&bins[0]);
	TH1D *hist_mean_err=new TH1D("hist_mean_err",
				     ";mag_{true};Systematic Error / 1000 events",
				     bins.size()-1,&bins[0]);
	TH1D *hist_frac=new TH1D("hist_frac",
				 Form(";%s_{true};Fraction within 3#sigma;",
				      band.c_str()),
				 bins.size()-1,&bins[0]);
        TGraph graph_tot_err;
        double hist_min=1e6,hist_max=-1e6;
        
	for(int i=0;i<sys_error.size();i++) {
	  hist_sys_err->SetBinContent(i+1,mean_vec[i]);
	  hist_sys_err->SetBinError(i+1,sys_error[i]);

	  hist_tot_err->SetBinContent(i+1,mean_vec[i]);
	  hist_tot_err->SetBinError(i+1,tot_error[i]);

	  hist_mean_err->SetBinContent(i+1,mean_vec[i]);
	  hist_mean_err->SetBinError(i+1,mean_error[i]);

	  hist_frac->SetBinContent(i+1,frac_passed[i]);

          graph_tot_err.SetPoint(i,
                                 hist_tot_err->GetXaxis()->GetBinCenter(i+1),
                                 tot_error[i]);

          if(mean_vec[i]+TMath::Max(tot_error[i],sys_error[i])>hist_max){
            hist_max=mean_vec[i]+TMath::Max(tot_error[i],sys_error[i]);
          }
          if(mean_vec[i]-TMath::Max(tot_error[i],sys_error[i])<hist_min){
            hist_min=mean_vec[i]-TMath::Max(tot_error[i],sys_error[i]);
          }
	} 

        if(hist_max>0.6) hist_max=0.6;
        if(hist_min<-0.6) hist_min=-0.6;
        
        if(!bcs) {
        FitGraph gfit(graph_tot_err);
        double val=gfit.Fit(0.100);
        cout<<"\n** "<<type_label<<" band="<<band<<" **"<<endl;
        cout<<"** True Magnitude where Scatter=0.1 mag= "<<val<<" **"<<endl;
        if(debug) cout<<"Actual "<<graph_tot_err.Eval(val)<<endl;
        if(debug) {
          c1->Clear();
          graph_tot_err.Draw("al*");
          c1->Update();
          c1->WaitPrimitive();
        }
        }

        //cout<<"** Actual value "<<graph_tot_err.Eval(val)<<endl;

	TLatex *label=new TLatex;
	label->SetNDC();
	label->SetTextSize(0.045);
	label->SetTextFont(42);
	label->SetTextAlign(22);
	
// 	c1->SetLeftMargin(0.17);
// 	hist_frac->GetYaxis()->CenterTitle();
// 	hist_frac->GetYaxis()->SetRangeUser(0,1);
// 	hist_frac->SetTitleOffset(1.6,"y");
// 	hist_frac->GetXaxis()->CenterTitle();
// 	hist_frac->Draw();
// 	label->DrawLatex(0.49,0.948,cur_title.c_str());
// 	c1->Update();
// 	if(debug) c1->WaitPrimitive();

 	string id=band+"_"+type_label+"_frac";
 	string print=dir+"/"+id+".png";
// 	if(debug)c1->WaitPrimitive();
// 	c1->Print(print.c_str());
// 	//hist_sys_err->Write();
// 	//hist_frac->Write();
        label->SetTextSize(0.06);
  
	TCanvas *c2;
        if (fixed_bins)
          c2 = new TCanvas("c2","",0,0,922,960);
        else
          c2 = new TCanvas("c2","",0,0,772,802);
	TPad *pad1 = new TPad("pad1"," ",0.0,0.37,1.0,1.0,0);
	TPad *pad2 = new TPad("pad2"," ",0.0,0.0,1.0,0.37,0.);

	pad1->SetBottomMargin(0.015);
	pad1->SetLeftMargin(0.15);
	pad2->SetLeftMargin(0.15);
	pad2->SetTopMargin(0.015);
	pad2->SetBottomMargin(0.3);
	pad1->SetTicks(1,0);
  
	pad1->Draw();
	pad2->Draw();
	pad1->cd();
	hist_tot_err->SetLabelOffset(1,"x");
	hist_tot_err->SetLabelSize(0.06,"y");
	
	if(!bcs)hist_tot_err->GetYaxis()->SetRangeUser(hist_min-0.1,hist_max+0.1);
	else hist_tot_err->GetYaxis()->SetRangeUser(-0.61,0.611);
	hist_tot_err->SetYTitle("");

	hist_mean_err->SetLineColor(2);
	hist_sys_err->SetLineColor(4);

	hist_mean_err->SetFillColor(1);
	hist_sys_err->SetFillColor(2);
	hist_tot_err->SetFillColor(4);

	hist_mean_err->SetLineWidth(3);
	hist_sys_err->SetLineWidth(3);
	hist_tot_err->SetLineWidth(3);


	hist_tot_err->Draw("e1");
	hist_sys_err->Draw("e1,same");  
	hist_mean_err->Draw("e1,same");
	TF1 *f3=new TF1("f3","0",0,100);
	f3->SetLineColor(15);
	f3->SetLineStyle(2);
	f3->Draw("same");
	
	label->DrawLatex(0.49,0.948,cur_title.c_str());

	TLatex *ty1=new TLatex;
	ty1->SetNDC();
	ty1->SetTextSize(0.07);
	ty1->SetTextFont(42);
	ty1->SetTextAngle(90);
	ty1->SetTextAlign(22);
	ty1->DrawLatex(0.031,0.5,Form("< %s_{%s} - %s_{%s} >",band.c_str(),
			       which_obs.c_str(),band.c_str(),
			       which_truth.c_str()));
   
	TLegend *leg=new TLegend(0.18,0.04,0.46,0.25);
	leg->SetTextFont(42);
	leg->SetTextSize(0.06);
	leg->SetBorderSize(0);
	leg->AddEntry(hist_mean_err,"#sigma_{mean}","l");
	leg->AddEntry(hist_sys_err,"#sigma_{sys}","l");
	leg->AddEntry(hist_tot_err,"#sigma_{tot}","l");
	TLegendEntry *leg_entry=leg->AddEntry(hist_frac,
					      "Fraction < 3#sigma_{tot}","l");
	leg_entry->SetTextSize(0.045);
	leg->Draw();

        if(draw_text) {
        // Draw numbers
        TLatex *txt=new TLatex;
        txt->SetTextSize(0.025);

        string sTableName;
        sTableName = "table_data_" + type_label + "_" + band + ".html";

        ofstream fTable(sTableName.c_str());

        fTable << "<table border = 1>" << endl;
        fTable << "<caption>" << type_label << ",  band = " << band <<
          "</caption>" << endl;
        fTable << "<tr><th>Bin</th><th>N_{sys}</th><th>N_{tot}</th>"
          "<th>mean</th><th>\\sigma_{mean}</th><th>\\sigma_{sys}</th>"
          "<th>\\sigma_{tot}</th><th>frac &lt; 3*\\sigma_{tot}</th></tr>" <<
          endl;

        for(int i=0;i<hist_mean_err->GetNbinsX();i++) {
          fTable << "<tr>" << endl;
          fTable << fixed << setprecision(2) << "   <td align=right>" <<
            hist_mean_err->GetBinLowEdge(i + 1) << " - " <<
            hist_mean_err->GetBinLowEdge(i + 1) +
            hist_mean_err->GetBinWidth(i + 1) << "</td>" << endl;
          fTable << "   <td align=right>" << nbin_sys[i] <<
            "</td>" << endl;
          fTable << "   <td align=right>" << nbin_tot[i] <<
            "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_mean_err->GetBinContent(i + 1) << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_mean_err->GetBinError(i + 1)*1000. << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_sys_err->GetBinError(i + 1)*1000. << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_tot_err->GetBinError(i + 1)*1000. << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_frac->GetBinContent(i + 1) << "</td>" << endl;
          fTable << "</tr>" << endl;
          double x=hist_mean_err->GetXaxis()->GetBinCenter(i+1);
          double y=hist_tot_err->GetBinContent(i+1)+
            TMath::Max(hist_tot_err->GetBinError(i+1),
                       hist_sys_err->GetBinError(i+1));
         
          // fixed bins 0.5 mag wide are too close together to print out
          // all the values on the plots.
          // same size fixed bins
          //if (fixed_bins && (i + 1)%2 != 0)
          //  continue;
          // mixed size fixed bins
          if (fixed_bins && x > 23. && (i + 1)%2 != 0)
            continue;

          double sep=0.02*(1.1*(hist_max-hist_min)/0.42);
          if(y+2*sep<hist_max+0.1) {
            txt->SetTextColor(kRed);
            txt->DrawLatex(x,y+sep,
                           Form("%0.2f",hist_mean_err->GetBinContent(i+1)));
            txt->SetTextColor(kBlue);
            txt->DrawLatex(x,y+2*sep,
                           Form("%0.1f",hist_sys_err->GetBinError(i+1)*1000));
            txt->SetTextColor(kBlack);
            txt->DrawLatex(x,y+3*sep,
                           Form("%0.1f",hist_tot_err->GetBinError(i+1)*1000));
          }

        }
        fTable << "</table>" << endl;
        fTable.close();
        cout << "Wrote table data to " << sTableName << endl;
        }          

	// draw fraction of values outside 3sigma
	pad1->Update();
	
	Float_t rightmax = 1;
	Float_t scale = pad1->GetUymax()/rightmax;
	hist_frac->SetLineColor(kMagenta);
	hist_frac->SetLineStyle(1);
	hist_frac->SetLineWidth(2);
	hist_frac->Scale(scale);
	hist_frac->Draw("same][");

	//draw an axis on the right side
	TGaxis *axis = new TGaxis(pad1->GetUxmax(),pad1->GetUymin(),
				  pad1->GetUxmax(), pad1->GetUymax(),
				  0.0001,rightmax,508,"+L");
	axis->SetLineColor(kMagenta);
	axis->SetLabelSize(0.06);
	axis->SetLabelOffset(0.012);
	axis->SetLabelFont(42);
	axis->SetLabelColor(kMagenta);
	axis->Draw();
// 	global_file->cd();
// 	hist_tot_err->Write();
// 	hist_frac->Write();
// 	global_file->Close();

	pad2->cd();

        // if there are too many points only sample the
        // the scatter plot
	TGraph *full_scatter=new TGraph;
	TGraph *sample_all=new TGraph;
	TGraph *sample_sub=new TGraph;
	TGraph *sample_gr=new TGraph;
        


        double frac_accept=1;
        if(sigma_scatter->GetN()>max_scatter) {
          frac_accept=max_scatter*1.0/full_diff.size();
        }

        TRandom3 rand;
	for(int i=0;i<full_diff.size();i++) {
          full_scatter->SetPoint(i,full_true_mag[i],full_diff[i]);
          if(rand.Uniform()<frac_accept) {
            sample_all->SetPoint(i,full_true_mag[i],full_diff[i]);
          }
	}

        
	for(int i=0;i<sigma_scatter->GetN();i++) {
          double x,y;
          sigma_scatter->GetPoint(i,x,y);
          if(rand.Uniform()<frac_accept) {
            sample_sub->SetPoint(i,x,y);
          }
        }  

	for(int i=0;i<sigma_gr_scatter->GetN();i++) {
          double x,y;
          sigma_gr_scatter->GetPoint(i,x,y);
          if(rand.Uniform()<frac_accept) {
            sample_gr->SetPoint(i,x,y);
          }
        }



        vector<double> dum(full_diff.size());
        int good,bad;
        double mean,rms;
        sigma_clip(3,2.5,full_diff,dum,mean,
                   good,bad,rms,true,false,1e-6);
        TH2D *h3=new TH2D("h3","",100,bins[0],
                          bins[bins.size()-1],100,mean-10*rms,mean+10*rms);
          
        double max_scat=mean+7*rms;
        double min_scat=mean-7*rms;
        if(max_scat>0.8) max_scat=0.8;
        if(min_scat<-0.8) min_scat=-0.8;

        h3->GetYaxis()->SetRangeUser(min_scat,max_scat);
        
	h3->SetNdivisions(506,"y");
	h3->Draw();
	h3->SetLabelSize(0.11,"xy");
	TF1 *f2=new TF1("f2","0",0,100);
	f2->SetLineColor(1);
	f2->Draw("same");
	sample_gr->SetMarkerColor(2);
	sample_gr->SetMarkerStyle(8);
	sample_gr->SetMarkerSize(0.25);
	sample_gr->Draw("p");
	sample_sub->SetMarkerColor(4);
	sample_sub->SetMarkerStyle(8);
	sample_sub->SetMarkerSize(0.25);
	sample_sub->Draw("p");

  
	TLatex *tx=new TLatex;
	tx->SetNDC();
	tx->SetTextSize(0.13);
	tx->SetTextFont(42);
	tx->SetTextAlign(22);
	tx->DrawLatex(0.55,0.079,Form("%s_{%s}",band.c_str(),
				      which_truth.c_str()));

	TLatex *ty2=new TLatex;
	ty2->SetNDC();
	ty2->SetTextSize(0.10);
	ty2->SetTextFont(42);
	ty2->SetTextAngle(90);
	ty2->SetTextAlign(22);
        ty2->DrawLatex(0.029,0.65,Form("%s_{%s} - %s_{%s}",band.c_str(),
				which_obs.c_str(),band.c_str(),
				which_truth.c_str()));



	TObject *dummy=0;
	TLegend *leg2=new TLegend(0.18,0.70,0.37,0.95);
	leg2->SetTextFont(42);
	leg2->SetTextSize(0.07);
	leg2->SetBorderSize(0);

	TLegendEntry *e1=leg2->AddEntry(dummy,"Outliers","p");
	e1->SetMarkerSize(0.9);
	e1->SetMarkerStyle(20);
	e1->SetMarkerColor(2);

	TLegendEntry *e2=leg2->AddEntry(dummy,"Within 3#sigma","p");
	e2->SetMarkerSize(0.9);
	e2->SetMarkerStyle(20);
	e2->SetMarkerColor(4);
	leg2->Draw();


	//c2->Write("can");

	id=dir+"/"+band+"_"+type_label+"_phot";
	print=id+".png";
	if(debug)c2->WaitPrimitive();
	c2->Print(print.c_str());
	
	
	string convert="convert "+id+".eps "+id+".png";
	//gSystem->Exec(convert.c_str());

	convert="rm "+print;
	//gSystem->Exec(convert.c_str());

	pad2->cd();
	//TH2D *h2=new TH2D("h2","",100,bins[0],
	//bins[bins.size()-1],100,min-0.01,max+0.01);
	TH2D *h2=new TH2D("h2","",100,bins[0],
			  bins[bins.size()-1],100,-8,1);

   
   
	h2->SetNdivisions(506,"y");
	h2->Draw();
	h2->SetLabelSize(0.11,"xy");
	TF1 *f4=new TF1("f4","0",0,100);
	f4->SetLineColor(1);
	f4->Draw("same");
	sample_gr->SetMarkerColor(2);
	sample_gr->SetMarkerStyle(0);
	sample_gr->SetMarkerSize(0.18);
	sample_gr->Draw("p");
	sample_sub->SetMarkerColor(4);
	sample_sub->SetMarkerStyle(0);
	sample_sub->SetMarkerSize(0.18);
	sample_sub->Draw("p");
   
   
		tx->DrawLatex(0.55,0.079,Form("%s_{%s}",band.c_str(),
					      which_truth.c_str()));
	ty2->DrawLatex(0.029,0.65,Form("%s_{%s} - %s_{%s}",band.c_str(),
				       which_obs.c_str(),band.c_str(),
				       which_truth.c_str()));

     
	TLegend *leg3=new TLegend(0.18,0.36,0.375,0.62);
	leg3->SetTextFont(42);
	leg3->SetTextSize(0.07);
	leg3->SetBorderSize(0);


	e1=leg3->AddEntry(dummy,"Total Sample","p");
	e1->SetMarkerSize(0.9);
	e1->SetMarkerStyle(20);
	e1->SetMarkerColor(2);

	e2=leg3->AddEntry(dummy,"Within 3#sigma","p");
      e2->SetMarkerSize(0.9);
      e2->SetMarkerStyle(20);
      e2->SetMarkerColor(4);
      leg3->Draw();

      print=id+"_zoomout.png";
      //c2->Print(print.c_str());


      convert="convert "+id+"_zoomout.eps "+id+"_zoomout.png";
      //gSystem->Exec(convert.c_str());

      convert="rm "+print;
      //gSystem->Exec(convert.c_str());
  
      delete f2; delete f3; delete hist_sys_err; delete hist_mean_err; delete hist_tot_err; 
      delete h2; delete h3; delete elist; delete hist_frac; delete c2;delete f4;
      
      }
  }
}
//sample_sub->Write("sample_sub");
  //sample_all->Write("sample_all");
  delete c1;

}

void plot_color_true(string inputfile,string title, string dir, 
                     string which_obs,string which_truth,
                     bool coadd,bool bcs,bool debug,bool draw_text,
                     double err_cut,int phot_flag,
                     double sigma,int N_bin,int max_scatter,
                     bool fixed_bins)
{
  int min_entries=50; //minimum entries per bin
  int func_min=0;    // min function limit
  int func_max=1;   // max function limit
  double hist_fit_max; // max for histogram fitting the gaussian
  int func_fit=3; // this*rms is max for function fitting the gaussian
 
  if(coadd==0) return;
  
  string colors[5]={"g-r","r-i","i-z","z-Y"};
  string types[2]={"S","G"};
  string int_types[2]={"2","1"};
  string type_labels[2]={"stars","galaxies"};

  string color,type,type_label;
  string true_mag_band;

  TCanvas *c1=new TCanvas("c1","");

  
  
  // create tree to hold variables
  string branch=get_branches(coadd,"all",bcs);
  TTree *tree=new TTree("tree","");

  tree->ReadFile(inputfile.c_str(),branch.c_str());
  if(debug) cout<<"Total Entries in tree:"<<tree->GetEntries()<<endl;
  double RA,RA_true,Dec,Dec_true,x_pos,y_pos;
  int classflag;
  
  // loop over galaxies and stars
  for(int cur_type=0;cur_type<2;cur_type++) {
    type=types[cur_type];
    type_label=type_labels[cur_type];
    
    if(debug)cout<<"Type :"<<type_label<<endl;
    
    // loop over colors
    for(int cur_color=0;cur_color<4;cur_color++) {
      color=colors[cur_color];
      
      if(bcs &&  color=="z-Y") continue;
      
      string cur_title=color+" color "+title+" "+type_label;
      if(debug) cout<<"Color :"<<color<<endl;
      
      if(!bcs) {
        if(type=="G") hist_fit_max=0.9*1.5;
        else hist_fit_max=0.45*1.5;
      }
      else {
        if(type=="G") hist_fit_max=0.4*2.5;
        else hist_fit_max=0.2*2.5;
      }
      c1->Clear();
      c1->cd();
      //hist_fit_max=10;
      // do any pre-selection cuts by producing an event list
      string basic_cut;
      if(color=="g-r") {
        basic_cut=Form("g_obs_err<%f && r_obs_err<%f && "
                       "g_flags < %d && r_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
        true_mag_band = "r";
      }
      else if(color=="r-i") {
        basic_cut=Form("r_obs_err<%f && i_obs_err<%f && "
                       "r_flags < %d && i_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
        true_mag_band = "i";
      }
      else if(color=="i-z") {
        basic_cut=Form("i_obs_err<%f && z_obs_err<%f && "
                       "i_flags < %d && z_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
        true_mag_band = "z";
      }
      else if(color=="z-Y") {
        basic_cut=Form("z_obs_err<%f && Y_obs_err<%f && "
                       "z_flags < %d && Y_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
        true_mag_band = "Y";
      }
      
      
      
      
      if(!bcs) basic_cut+=" && class==\""+type+"\"";
      else basic_cut+=" && classflag=="+int_types[cur_type];
      
      
      TEventList *elist=0;
      tree->Draw(">>elist",basic_cut.c_str());
      elist = (TEventList*)gDirectory->Get("elist");
      if(!elist) continue;
      
      if(elist->GetN()==0) continue;
      
      // load the vectors with the data
      double obs_color,true_color,diff,err,true_err;
      double obs_mag1,obs_mag2,mag_err1,mag_err2;
      double true_mag1,true_mag2;
        
      
	// allow for different truth labels
	string true_label;
      if(which_truth=="true") true_label=color+"_true";
      else true_label=which_truth+"_"+color;
      
      tree->ResetBranchAddresses();
      if(color=="g-r") {
        tree->SetBranchAddress("g_mag_obs",&obs_mag1);
        tree->SetBranchAddress("r_mag_obs",&obs_mag2);
        tree->SetBranchAddress("g_true",&true_mag1);
        tree->SetBranchAddress("r_true",&true_mag2);
        tree->SetBranchAddress("g_obs_err",&mag_err1);
        tree->SetBranchAddress("r_obs_err",&mag_err2);
      }
      else if (color=="r-i") {
        tree->SetBranchAddress("r_mag_obs",&obs_mag1);
        tree->SetBranchAddress("i_mag_obs",&obs_mag2);
        tree->SetBranchAddress("r_true",&true_mag1);
        tree->SetBranchAddress("i_true",&true_mag2);
        tree->SetBranchAddress("r_obs_err",&mag_err1);
        tree->SetBranchAddress("i_obs_err",&mag_err2);
      }
      else if (color=="i-z") {
        tree->SetBranchAddress("i_mag_obs",&obs_mag1);
        tree->SetBranchAddress("z_mag_obs",&obs_mag2);
        tree->SetBranchAddress("i_true",&true_mag1);
        tree->SetBranchAddress("z_true",&true_mag2);
        tree->SetBranchAddress("i_obs_err",&mag_err1);
        tree->SetBranchAddress("z_obs_err",&mag_err2);
      }
      else if (color=="z-Y") {
        tree->SetBranchAddress("z_mag_obs",&obs_mag1);
        tree->SetBranchAddress("Y_mag_obs",&obs_mag2);
        tree->SetBranchAddress("z_true",&true_mag1);
        tree->SetBranchAddress("Y_true",&true_mag2);
        tree->SetBranchAddress("z_obs_err",&mag_err1);
        tree->SetBranchAddress("Y_obs_err",&mag_err2);
      }
      

// 	string true_errlabel=which_truth+"err_"+band;
// 	if(bcs) {
// 	  tree->SetBranchAddress(true_errlabel.c_str(),&true_err);
// 	}
// 	else true_err=0;
      true_err=0;
	
	vector<double> full_obs_color,full_true_color,
          full_diff,full_err,full_true_err;  
        vector<double> full_true_mag;
	true_err=0;
	int i=0;
	while(tree->GetEntry(elist->GetEntry(i))>0) {
          obs_color=obs_mag1-obs_mag2;
          err=obs_mag1-obs_mag2;
          true_color=true_mag1-true_mag2;
          err=sqrt(mag_err1*mag_err1+mag_err2*mag_err2);

	  full_obs_color.push_back(obs_color);
	  full_true_color.push_back(true_color);
	  full_diff.push_back(obs_color-true_color);
	  full_err.push_back(err);
	  full_true_err.push_back(true_err);
          full_true_mag.push_back(true_mag2);
	  i++;
	}
	int N_entries = full_diff.size();

	if(debug) {
	  cout<<"Total Objects: "<<N_entries<<endl;
	}
	
	if(N_entries==0) {delete elist;continue;}

	// Sort tree into index according to true mag 
	int *index = new int[N_entries];
	//TMath::Sort(N_entries,&full_true_color[0],index,false);
	TMath::Sort(N_entries,&full_true_mag[0],index,false);

	// Fill ordered vectors
	vector<double> order_obs_color,order_true_color,
	  order_diff,order_err,order_true_err;  
        vector<double> order_true_mag;
        
        if (fixed_bins)
          N_bin = 50000;  // not really sure here... make it huge.
        else
        { 
          int desired_bins=8;
          if(1.*N_entries/N_bin> desired_bins) {
            N_bin=(int)N_entries/desired_bins*1.;
          }
        }

	for(int i=0;i<N_entries;i++) {

	  order_obs_color.insert(order_obs_color.end(),
                                 full_obs_color[index[i]]);
	  order_true_color.insert(order_true_color.end(),
                                  full_true_color[index[i]]);
	  order_diff.insert(order_diff.end(),full_diff[index[i]]);
	  order_err.insert(order_err.end(),full_err[index[i]]);
	  order_true_err.insert(order_true_err.end(),full_true_err[index[i]]);
	  
          order_true_mag.insert(order_true_mag.end(), full_true_mag[index[i]]);

	}
      

	TGraph *sigma_scatter=new TGraph;  
	TGraph *sigma_gr_scatter=new TGraph;  
	vector<double> bins,sys_error,tot_error,mean_error,mean_vec,frac_passed;
	int global_counter=0,sigma_counter=0,sigma_gr_counter=0;
        // number of events in tot_error and sys_error.
        vector<long> nbin_tot, nbin_sys;

	int bin=0;


        const double histmin   = 13.75;
        const double histmax   = 26.75;
        // for use with all bins fixed the same width
        //const double bin_width = 0.5;
        //const int nfixed_bins = (int)((histmax - histmin)/bin_width);

        // fixed width bins, but with 2 different sizes.
        double tbin[18] = {13.75, 14.75, 15.75, 16.75,
          17.75, 18.75, 19.75, 20.75, 21.75, 22.75, 23.25, 23.75, 24.25,
          24.75, 25.25, 25.75, 26.25, 26.75};
        const int nfixed_bins = 17;

        if (fixed_bins)
        {
          if (debug)
          {
            cout << "Using FIXED width bins." << endl;
            cout << "hist range: " << histmin << " " << histmax << endl;
            cout << "nbins: " << nfixed_bins << endl;
          }

          // bins defines the low edge of the histogram bins. It must be length
          // of nbins + 1.
          bins.assign(nfixed_bins + 1, 0.);
          sys_error.assign(nfixed_bins, 0.);
          tot_error.assign(nfixed_bins, 0.);
          mean_error.assign(nfixed_bins, 0.);
          mean_vec.assign(nfixed_bins, 0.);
          frac_passed.assign(nfixed_bins, 0.);
          nbin_tot.assign(nfixed_bins, 0);
          nbin_sys.assign(nfixed_bins, 0);
        }
        else
          cout << "Using VARIABLE width bins." << endl;

        double bin_le;

	// Loop until the end of the vectors have been reached
	bool finished=false;
	while(!finished) {

          // all same size fixed bins
          //bin_le = histmin + bin*bin_width;
          // mixed bin sizes with fixed bins
          bin_le = tbin[bin];

	  if(debug)
            if (!fixed_bins)
              cout << "Bin " << bin << endl;
	  TGraph scatter,scatter_clipped;

	  // create sub list of vectors with either N_bin entries or
	  // the number in a color difference of 1
	  vector<double> sub_obs_color,sub_true_color,sub_diff,
            sub_err,sub_true_err;
          vector<double> sub_true_mag;

	  int current_min=global_counter;
	  for(int entry=0; entry<N_bin ; entry++) {
      
	    // check if at the end of the vector;
	    if(global_counter  >= N_entries) {
	      finished=true;
	      break;
	    }
	    if(debug)cout<<"  entry "<<entry<<" "<<global_counter<<" "
              << order_true_mag[global_counter] << " " 
			 <<order_true_color[global_counter]<<" "
			 <<order_obs_color[global_counter]<<endl;

            if (fixed_bins)
            {
              if (order_true_mag[global_counter] >= histmax ||
                  bin_le >= histmax || bin >= nfixed_bins)
              {
                finished=true;
                break;
              }

              // all same size fixed bins
              //double bin_he = bin_le + bin_width;
              // mixed bin sizes with fixed bins
              double bin_he = tbin[bin + 1];
              if (debug)
                if (fixed_bins)
                  cout << "Bin " << bin << ", [" << bin_le << ", " << bin_he <<
                    ")" << endl;

              if (order_true_mag[global_counter] < bin_le)
              {
                global_counter++;
                continue;
              }
              else if (order_true_mag[global_counter] >= bin_he)
                break;
            }

            
	    // check if the color difference is greater than 1 between the
	    // current and first of the bin
            if (!fixed_bins)
              if(entry>2) {
                if(order_true_mag[global_counter] -
                   order_true_mag[current_min] >1.) {
                  global_counter++;
                  break;
                }
              }
      
	    scatter.SetPoint(entry,order_true_mag[global_counter],
			     order_diff[global_counter]);

	    sub_obs_color.insert(sub_obs_color.end(),order_obs_color[global_counter]);
	    sub_true_color.insert(sub_true_color.end(),order_true_color[global_counter]);
	    sub_diff.insert(sub_diff.end(),order_diff[global_counter]);
	    sub_err.insert(sub_err.end(),order_err[global_counter]);
	    sub_true_err.insert(sub_true_err.end(),order_true_err[global_counter]);
	    sub_true_mag.insert(sub_true_mag.end(),order_true_mag[global_counter]);

	    global_counter++;
	  }
    

    
	  if(debug) cout<<"Subset Size "<<sub_diff.size()<<endl;

	  // Make sure there at least min_entries
	  if(sub_diff.size()<min_entries) {
            if(debug)cout<<"Not enough entries in this bin skipping "<<endl;
            if (fixed_bins)
            {
              mean_vec[bin] = 0.;
              sys_error[bin] = 0.;
              tot_error[bin] = 0.;
              mean_error[bin] = 0.;
              frac_passed[bin] = 0.;
              nbin_tot[bin] = 0;
              nbin_sys[bin] = 0;
            }
            else
              continue;
          }
          else {
            vector<double> dum(sub_diff.size());
            int good,bad;
            double mean,rms;
            sigma_clip(3,2.5,sub_diff,dum,mean,
                       good,bad,rms,true,false,1e-6);
            double hist_max=mean+10*rms;
            double hist_min=mean-10*rms;
            if(hist_max>hist_fit_max) hist_max=hist_fit_max;
            if(hist_min<-1*hist_fit_max) hist_min=-hist_fit_max;
            // fit to gaussian and to do  sigma clipping
            TH1D *h1=new TH1D(Form("fit%d",bin+1),"",35,
                              hist_min,hist_max);
            for(int i=0;i<sub_diff.size();i++) {
              h1->Fill(sub_diff[i]);
            }
            if(debug) cout<<"Clipped mean="<<mean<<" rms="<<rms<<endl;
            if(debug) cout<<"Hist range: "<<hist_min<<" "<<hist_max<<endl;
            if(h1->Integral()<min_entries) {
              if(debug) cout<<"Found less than the number of entries in "
              <<"the histogram skipping entries in bin"<<endl;
              h1->SetName("");
              continue;
            }
            
            double hist_mean=h1->GetMean();
            double hist_rms=h1->GetRMS();
            if(debug) cout<<"Hist Mean "<<hist_mean<<" RMS "<<hist_rms<<endl;
            
            TF1 *f1=new TF1("f1","gaus",hist_mean-func_fit*hist_rms,
                            hist_mean+func_fit*hist_rms);

            f1->SetNpx(10000);
            f1->SetParameter(1,0);
            h1->Fit(f1,"QR");

            double gaus_const=f1->GetParameter(0);
            double gaus_mean=f1->GetParameter(1);
            double gaus_sigma=f1->GetParameter(2);
            if(debug) cout<<"Fit Results mean="<<gaus_mean<<" sigma="
                          <<gaus_sigma<<endl;
            // draw lines to indicate cuts
            if(debug) {
        
              TLine *hi=new TLine(gaus_mean+sigma*gaus_sigma,0,
                                  gaus_mean+sigma*gaus_sigma,h1->GetMaximum());
              TLine *low=new TLine(gaus_mean-sigma*gaus_sigma,0,
                                   gaus_mean-sigma*gaus_sigma,h1->GetMaximum());
        
              low->SetLineWidth(3);
              hi->SetLineWidth(3);
              hi->SetLineColor(2);
              low->SetLineColor(2);
              h1->Draw();
              hi->Draw();
              low->Draw();
            }

            // erase name to not have warning messages
            h1->SetName("");
          
            // cut out all values greater than sigma*gaus_sigma(from fit)
            vector<double> vec_obs_color,vec_true_color,vec_diff,vec_err,vec_true_err;
            vector<double> vec_true_mag;
      
            int clipped_counter=0;
            for(int entry=0; entry<sub_diff.size() ; entry++) {
        
              if(fabs(sub_diff[entry]-gaus_mean)<sigma*gaus_sigma) {
                vec_obs_color.insert(vec_obs_color.end(),sub_obs_color[entry]);
                vec_true_color.insert(vec_true_color.end(),sub_true_color[entry]);
                vec_true_mag.insert(vec_true_mag.end(),sub_true_mag[entry]);
                vec_diff.insert(vec_diff.end(),sub_diff[entry]);
                vec_err.insert(vec_err.end(),sub_err[entry]);
                vec_true_err.insert(vec_true_err.end(),sub_true_err[entry]);
                scatter_clipped.SetPoint(clipped_counter,sub_true_color[entry],
                                         sub_diff[entry]);
                clipped_counter++;
                sigma_scatter->SetPoint(sigma_counter,sub_true_mag[entry],
                                        sub_diff[entry]);
                sigma_counter++;
              } else {
                
                sigma_gr_scatter->SetPoint(sigma_gr_counter,sub_true_mag[entry],
                                           sub_diff[entry]);
                sigma_gr_counter++;
              }
              

            }


            //h1->Write();


            // calculate how many events were cut


      
            if(debug) cout<<"Bin "<<bin+1<<" with "<<vec_diff.size()
                          <<"clipped entries"<<endl;      

            if(vec_diff.size()<min_entries) {
              if(debug) cout<<"Found fewer good events than minimum"
                            <<" skipping entries in bin: "<<vec_diff.size()
                            <<endl;
              continue;
            }

            double frac=1.*vec_diff.size()/sub_diff.size();
            if (fixed_bins)
              frac_passed[bin] = frac;
            else
              frac_passed.push_back(frac);
      
            if(debug) {
              cout<<"Color Range: "<<vec_true_color[0]<<" to "    
                  <<vec_true_color[vec_true_color.size()-1]<<endl;
              cout<<"Mag Range: "<<vec_true_mag[0]<<" to "
                <<vec_true_mag[vec_true_mag.size()-1]<<endl;
            }
      
            scatter.SetName(Form("scatter%d",bin+1));
            //scatter.Write();

            scatter_clipped.SetName(Form("scatter_clipped%d",bin+1));
            //scatter_clipped.Write();
      

          SysErrFit fit;
          double err;
          double min=fit.Fit(&vec_diff,&vec_err,err,&vec_true_err);
          if(debug)cout<<"Minimization Minimum Found at = "<<abs(min)<<endl;
          
          
            // Look at what the fitting is doing
            if(debug) {
              c1->Update();
              c1->WaitPrimitive();
            }



            // if minimization fails use 0.0 as default
            if(fabs(min)>func_max) {
              cout<<"Error: root finder failed"<<endl;
              min=0.0;
            };

            // set variables
            // Bob points out that with fixed bins we now have a varying
            // events in each bin and should normalize by sqrt(N). But
            // if we want to see the scatter, then we shouldn't do that.
            if (fixed_bins)
            {
              // systematic error of the clipped data
              sys_error[bin] = min;
              //if (vec_diff.size())
              //  sys_error[bin] = min/sqrt(vec_diff.size());
              // width of Gaussian fit to the unclipped data
              tot_error[bin] = gaus_sigma;
              //if (h1->Integral())
              //  tot_error[bin] = gaus_sigma/sqrt(h1->Integral());
              // error on the mean of the unclipped data.
              if (h1->Integral())
                mean_error[bin] = hist_rms/sqrt(h1->Integral());
              // unclipped data mean found by Gaussian fit.
              mean_vec[bin] = gaus_mean;
              nbin_tot[bin] = (long)sub_diff.size();
              nbin_sys[bin] = (long)vec_diff.size();
            }
            else
            {
              sys_error.push_back(min);
              tot_error.push_back(gaus_sigma);
              mean_error.push_back(hist_rms/sqrt(h1->Integral()));
              mean_vec.push_back(gaus_mean);
              nbin_tot.push_back((long)sub_diff.size());
              nbin_sys.push_back((long)vec_diff.size());
            }
          }
     
          //if (fixed_bins)
          //{
          //  if (bin_le >= histmax)
          //    break;
          //}
          //else
          if (!fixed_bins)
          {
            // get the last of the observed magnitude for binning
            if(bin==0) bins.insert(bins.end(),sub_true_mag.front());
            bins.insert(bins.end(),sub_true_mag.back());
          }
	  bin++;
	}

        // all equal size fixed bins
        //if (fixed_bins)
        //{
        //  bins[0] = histmin;
        //  for (int i = 1; i < bins.size(); i++)
        //    bins[i] = bins[i - 1] + bin_width;
        //}
        // if using mixed bins sizes
        if (fixed_bins)
          for (int i = 0; i < bins.size(); i++)
            bins[i] = tbin[i];
	
        if(bins.size()==0) {delete elist; continue;}

        //for (int i = 0; i < bins.size(); i++)
        //  cout << "bins[" << i << "]: " << bins[i] << endl;

        //cout << "bins.size(): " << bins.size() << ", mean_vec.size(): " <<
        //  mean_vec.size() << ", sys_error.size(): " << sys_error.size() <<
        //  ", tot_error.size(): " << tot_error.size() <<
        //  ", mean_error.size(): " << mean_error.size() <<
        //  ", frac_passed.size(): " << frac_passed.size() << endl;

	if(debug)cout<<"Finished Fitting"<<endl;
  
	TH1D *hist_sys_err=new TH1D("hist_sys_err",
				    ";color_{true};Systematic Error / 1000 events",
				    bins.size()-1,&bins[0]);
	TH1D *hist_tot_err=new TH1D("hist_tot_err",
				    ";color_{true};Systematic Error / 1000 events",
				    bins.size()-1,&bins[0]);
	TH1D *hist_mean_err=new TH1D("hist_mean_err",
				     ";color_{true};Systematic Error / 1000 events",
				     bins.size()-1,&bins[0]);
	TH1D *hist_frac=new TH1D("hist_frac",
				 Form(";%s_{true};Fraction within 3#sigma;",
				      color.c_str()),
				 bins.size()-1,&bins[0]);
        TGraph graph_tot_err;
        double hist_min=1e6,hist_max=-1e6;
        
	for(int i=0;i<sys_error.size();i++) {
	  hist_sys_err->SetBinContent(i+1,mean_vec[i]);
	  hist_sys_err->SetBinError(i+1,sys_error[i]);

	  hist_tot_err->SetBinContent(i+1,mean_vec[i]);
	  hist_tot_err->SetBinError(i+1,tot_error[i]);

	  hist_mean_err->SetBinContent(i+1,mean_vec[i]);
	  hist_mean_err->SetBinError(i+1,mean_error[i]);

	  hist_frac->SetBinContent(i+1,frac_passed[i]);

          graph_tot_err.SetPoint(i,
                                 hist_tot_err->GetXaxis()->GetBinCenter(i+1),
                                 tot_error[i]);

          if(mean_vec[i]+TMath::Max(tot_error[i],sys_error[i])>hist_max){
            hist_max=mean_vec[i]+TMath::Max(tot_error[i],sys_error[i]);
          }
          if(mean_vec[i]-TMath::Max(tot_error[i],sys_error[i])<hist_min){
            hist_min=mean_vec[i]-TMath::Max(tot_error[i],sys_error[i]);
          }
	} 

        if(hist_max>0.6) hist_max=0.6;
        if(hist_min<-0.6) hist_min=-0.6;
        

        //cout<<"** Actual value "<<graph_tot_err.Eval(val)<<endl;

	TLatex *label=new TLatex;
	label->SetNDC();
	label->SetTextSize(0.045);
	label->SetTextFont(42);
	label->SetTextAlign(22);
	
// 	c1->SetLeftMargin(0.17);
// 	hist_frac->GetYaxis()->CenterTitle();
// 	hist_frac->GetYaxis()->SetRangeUser(0,1);
// 	hist_frac->SetTitleOffset(1.6,"y");
// 	hist_frac->GetXaxis()->CenterTitle();
// 	hist_frac->Draw();
// 	label->DrawLatex(0.49,0.948,cur_title.c_str());
// 	c1->Update();
// 	if(debug) c1->WaitPrimitive();

 	string id=color+"_"+type_label+"_frac";
 	string print=dir+"/"+id+".png";
// 	if(debug)c1->WaitPrimitive();
// 	c1->Print(print.c_str());
// 	//hist_sys_err->Write();
// 	//hist_frac->Write();
        label->SetTextSize(0.06);
  
	TCanvas *c2 = new TCanvas("c2","",0,0,772,802);
	TPad *pad1 = new TPad("pad1"," ",0.0,0.37,1.0,1.0,0);
	TPad *pad2 = new TPad("pad2"," ",0.0,0.0,1,0.37,0);

	pad1->SetBottomMargin(0.015);
	pad1->SetLeftMargin(0.15);
	pad2->SetLeftMargin(0.15);
	pad2->SetTopMargin(0.015);
	pad2->SetBottomMargin(0.3);
	pad1->SetTicks(1,0);
  
	pad1->Draw();
	pad2->Draw();
	pad1->cd();
	hist_tot_err->SetLabelOffset(1,"x");
	hist_tot_err->SetLabelSize(0.06,"y");
	
	if(!bcs)hist_tot_err->GetYaxis()->SetRangeUser(hist_min-0.1,hist_max+0.1);
	else hist_tot_err->GetYaxis()->SetRangeUser(-0.61,0.611);
	hist_tot_err->SetYTitle("");

	hist_mean_err->SetLineColor(2);
	hist_sys_err->SetLineColor(4);

	hist_mean_err->SetFillColor(1);
	hist_sys_err->SetFillColor(2);
	hist_tot_err->SetFillColor(4);

	hist_mean_err->SetLineWidth(3);
	hist_sys_err->SetLineWidth(3);
	hist_tot_err->SetLineWidth(3);


	hist_tot_err->Draw("e1");
	hist_sys_err->Draw("e1,same");  
	hist_mean_err->Draw("e1,same");
	TF1 *f3=new TF1("f3","0",-100,100);
	f3->SetLineColor(15);
	f3->SetLineStyle(2);
	f3->Draw("same");
	
	label->DrawLatex(0.49,0.948,cur_title.c_str());

	TLatex *ty1=new TLatex;
	ty1->SetNDC();
	ty1->SetTextSize(0.07);
	ty1->SetTextFont(42);
	ty1->SetTextAngle(90);
	ty1->SetTextAlign(22);
	ty1->DrawLatex(0.031,0.5,Form("< %s_{%s} - %s_{%s} >",color.c_str(),
			       which_obs.c_str(),color.c_str(),
			       which_truth.c_str()));
   
	TLegend *leg=new TLegend(0.18,0.04,0.46,0.25);
	leg->SetTextFont(42);
	leg->SetTextSize(0.06);
	leg->SetBorderSize(0);
	leg->AddEntry(hist_mean_err,"#sigma_{mean}","l");
	leg->AddEntry(hist_sys_err,"#sigma_{sys}","l");
	leg->AddEntry(hist_tot_err,"#sigma_{tot}","l");
	TLegendEntry *leg_entry=leg->AddEntry(hist_frac,
					      "Fraction < 3#sigma_{tot}","l");
	leg_entry->SetTextSize(0.045);
	leg->Draw();

        if(draw_text) {
        // Draw numbers
        TLatex *txt=new TLatex;
        txt->SetTextSize(0.025);

        string sTableName;
        sTableName = "table_data_" + type_label + "_" + color + ".html";

        ofstream fTable(sTableName.c_str());

        fTable << "<table border = 1>" << endl;
        fTable << "<caption>" << type_label << ",  color diff = " << color <<
          "</caption>" << endl;
        fTable << "<tr><th>Bin</th><th>N_{sys}</th><th>N_{tot}</th>"
          "<th>mean</th><th>\\sigma_{mean}</th><th>\\sigma_{sys}</th>"
          "<th>\\sigma_{tot}</th><th>frac &lt; 3*\\sigma_{tot}</th></tr>" <<
          endl;

        for(int i=0;i<hist_mean_err->GetNbinsX();i++) {
          fTable << "<tr>" << endl;
          fTable << fixed << setprecision(2) << "   <td align=right>" <<
            hist_mean_err->GetBinLowEdge(i + 1) << " - " <<
            hist_mean_err->GetBinLowEdge(i + 1) +
            hist_mean_err->GetBinWidth(i + 1) << "</td>" << endl;
          fTable << "   <td align=right>" << nbin_sys[i] <<
            "</td>" << endl;
          fTable << "   <td align=right>" << nbin_tot[i] <<
            "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_mean_err->GetBinContent(i + 1) << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_mean_err->GetBinError(i + 1)*1000. << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_sys_err->GetBinError(i + 1)*1000. << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_tot_err->GetBinError(i + 1)*1000. << "</td>" << endl;
          fTable << fixed << setprecision(3) << "   <td align=right>" <<
            hist_frac->GetBinContent(i + 1) << "</td>" << endl;
          fTable << "</tr>" << endl;
          double x=hist_mean_err->GetXaxis()->GetBinCenter(i+1);
          double y=hist_tot_err->GetBinContent(i+1)+
            TMath::Max(hist_tot_err->GetBinError(i+1),
                       hist_sys_err->GetBinError(i+1));

          // fixed bins 0.5 mag wide are too close together to print out
          // all the values on the plots.
          // same size fixed bins
          //if (fixed_bins && (i + 1)%2 != 0)
          //  continue;
          // mixed size fixed bins
          if (fixed_bins && x > 23. && (i + 1)%2 != 0)
            continue;

          double sep=0.02*(1.1*(hist_max-hist_min)/0.42);
          if(y+2*sep<hist_max+0.1) {
            txt->SetTextColor(kRed);
            txt->DrawLatex(x,y+sep,
                           Form("%0.2f",hist_mean_err->GetBinContent(i+1)));
            txt->SetTextColor(kBlue);
            txt->DrawLatex(x,y+2*sep,
                           Form("%0.1f",hist_sys_err->GetBinError(i+1)*1000));
            txt->SetTextColor(kBlack);
            txt->DrawLatex(x,y+3*sep,
                           Form("%0.1f",hist_tot_err->GetBinError(i+1)*1000));
          }
          
        }
        fTable << "</table>" << endl;
        fTable.close();
        cout << "Wrote table data to " << sTableName << endl;
        }          

	// draw fraction of values outside 3sigma
	pad1->Update();
	
	Float_t rightmax = 1;
	Float_t scale = pad1->GetUymax()/rightmax;
	hist_frac->SetLineColor(kMagenta);
	hist_frac->SetLineStyle(1);
	hist_frac->SetLineWidth(2);
	hist_frac->Scale(scale);
	hist_frac->Draw("same][");

	//draw an axis on the right side
	TGaxis *axis = new TGaxis(pad1->GetUxmax(),pad1->GetUymin(),
				  pad1->GetUxmax(), pad1->GetUymax(),
				  0.0001,rightmax,508,"+L");
	axis->SetLineColor(kMagenta);
	axis->SetLabelSize(0.06);
	axis->SetLabelOffset(0.012);
	axis->SetLabelFont(42);
	axis->SetLabelColor(kMagenta);
	axis->Draw();
// 	global_file->cd();
// 	hist_tot_err->Write();
// 	hist_frac->Write();
// 	global_file->Close();

	pad2->cd();

        // if there are too many points only sample the
        // the scatter plot
	TGraph *full_scatter=new TGraph;
	TGraph *sample_all=new TGraph;
	TGraph *sample_sub=new TGraph;
	TGraph *sample_gr=new TGraph;
        


        double frac_accept=1;
        if(sigma_scatter->GetN()>max_scatter) {
          frac_accept=max_scatter*1.0/full_diff.size();
        }

        TRandom3 rand;
	for(int i=0;i<full_diff.size();i++) {
          full_scatter->SetPoint(i,full_true_mag[i],full_diff[i]);
          if(rand.Uniform()<frac_accept) {
            sample_all->SetPoint(i,full_true_mag[i],full_diff[i]);
          }
	}

        
	for(int i=0;i<sigma_scatter->GetN();i++) {
          double x,y;
          sigma_scatter->GetPoint(i,x,y);
          if(rand.Uniform()<frac_accept) {
            sample_sub->SetPoint(i,x,y);
          }
        }  

	for(int i=0;i<sigma_gr_scatter->GetN();i++) {
          double x,y;
          sigma_gr_scatter->GetPoint(i,x,y);
          if(rand.Uniform()<frac_accept) {
            sample_gr->SetPoint(i,x,y);
          }
        }



        vector<double> dum(full_diff.size());
        int good,bad;
        double mean,rms;
        sigma_clip(3,2.5,full_diff,dum,mean,
                   good,bad,rms,true,false,1e-6);
        TH2D *h3=new TH2D("h3","",100,bins[0],
                          bins[bins.size()-1],100,mean-10*rms,mean+10*rms);
          
        double max_scat=mean+7*rms;
        double min_scat=mean-7*rms;
        if(max_scat>0.8) max_scat=0.8;
        if(min_scat<-0.8) min_scat=-0.8;

        h3->GetYaxis()->SetRangeUser(min_scat,max_scat);
        
	h3->SetNdivisions(506,"y");
	h3->Draw();
	h3->SetLabelSize(0.11,"xy");
	TF1 *f2=new TF1("f2","0",-100,100);
	f2->SetLineColor(1);
	f2->Draw("same");
	sample_gr->SetMarkerColor(2);
	sample_gr->SetMarkerStyle(8);
	sample_gr->SetMarkerSize(0.25);
	sample_gr->Draw("p");
	sample_sub->SetMarkerColor(4);
	sample_sub->SetMarkerStyle(8);
	sample_sub->SetMarkerSize(0.25);
	sample_sub->Draw("p");

  
	TLatex *tx=new TLatex;
	tx->SetNDC();
	tx->SetTextSize(0.13);
	tx->SetTextFont(42);
	tx->SetTextAlign(22);
	tx->DrawLatex(0.55,0.079,Form("%s_{%s}",true_mag_band.c_str(),
				      which_truth.c_str()));

	TLatex *ty2=new TLatex;
	ty2->SetNDC();
	ty2->SetTextSize(0.10);
	ty2->SetTextFont(42);
	ty2->SetTextAngle(90);
	ty2->SetTextAlign(22);
        ty2->DrawLatex(0.029,0.65,Form("%s_{%s} - %s_{%s}",color.c_str(),
				which_obs.c_str(),color.c_str(),
				which_truth.c_str()));



	TObject *dummy=0;
	TLegend *leg2=new TLegend(0.18,0.70,0.37,0.95);
	leg2->SetTextFont(42);
	leg2->SetTextSize(0.07);
	leg2->SetBorderSize(0);

	TLegendEntry *e1=leg2->AddEntry(dummy,"Outliers","p");
	e1->SetMarkerSize(0.9);
	e1->SetMarkerStyle(20);
	e1->SetMarkerColor(2);

	TLegendEntry *e2=leg2->AddEntry(dummy,"Within 3#sigma","p");
	e2->SetMarkerSize(0.9);
	e2->SetMarkerStyle(20);
	e2->SetMarkerColor(4);
	leg2->Draw();


	//c2->Write("can");

	id=dir+"/"+color+"_"+type_label+"_phot";
	print=id+".png";
	if(debug)c2->WaitPrimitive();
	c2->Print(print.c_str());
	
	
	string convert="convert "+id+".eps "+id+".png";
	//gSystem->Exec(convert.c_str());

	convert="rm "+print;
	//gSystem->Exec(convert.c_str());

	pad2->cd();
	//TH2D *h2=new TH2D("h2","",100,bins[0],
	//bins[bins.size()-1],100,min-0.01,max+0.01);
	TH2D *h2=new TH2D("h2","",100,bins[0],
			  bins[bins.size()-1],100,-8,1);

   
   
	h2->SetNdivisions(506,"y");
	h2->Draw();
	h2->SetLabelSize(0.11,"xy");
	TF1 *f4=new TF1("f4","0",0,100);
	f4->SetLineColor(1);
	f4->Draw("same");
	sample_gr->SetMarkerColor(2);
	sample_gr->SetMarkerStyle(0);
	sample_gr->SetMarkerSize(0.18);
	sample_gr->Draw("p");
	sample_sub->SetMarkerColor(4);
	sample_sub->SetMarkerStyle(0);
	sample_sub->SetMarkerSize(0.18);
	sample_sub->Draw("p");
   
   
		tx->DrawLatex(0.55,0.079,Form("%s_{%s}",color.c_str(),
					      which_truth.c_str()));
	ty2->DrawLatex(0.029,0.65,Form("%s_{%s} - %s_{%s}",color.c_str(),
				       which_obs.c_str(),color.c_str(),
				       which_truth.c_str()));

     
	TLegend *leg3=new TLegend(0.18,0.36,0.375,0.62);
	leg3->SetTextFont(42);
	leg3->SetTextSize(0.07);
	leg3->SetBorderSize(0);


	e1=leg3->AddEntry(dummy,"Total Sample","p");
	e1->SetMarkerSize(0.9);
	e1->SetMarkerStyle(20);
	e1->SetMarkerColor(2);

	e2=leg3->AddEntry(dummy,"Within 3#sigma","p");
      e2->SetMarkerSize(0.9);
      e2->SetMarkerStyle(20);
      e2->SetMarkerColor(4);
      leg3->Draw();

      print=id+"_zoomout.png";
      //c2->Print(print.c_str());


      convert="convert "+id+"_zoomout.eps "+id+"_zoomout.png";
      //gSystem->Exec(convert.c_str());

      convert="rm "+print;
      //gSystem->Exec(convert.c_str());
  
      delete f2; delete f3; delete hist_sys_err; delete hist_mean_err; delete hist_tot_err; 
      delete h2; delete h3; delete elist; delete hist_frac; delete c2;delete f4;
      
    }
  }
  
  delete c1;
  
}




void plot_color(string inputfile,string title, string dir, 
                string which_obs,string which_truth,
                bool coadd,bool bcs,bool debug,bool draw_text,
                double err_cut,int phot_flag,
                double sigma,int N_bin,int max_scatter)
{
  int min_entries=50; //minimum entries per bin
  int func_min=0;    // min function limit
  int func_max=1;   // max function limit
  double hist_fit_max; // max for histogram fitting the gaussian
  int func_fit=3; // this*rms is max for function fitting the gaussian
 
  if(coadd==0) return;
  
  string colors[5]={"g-r","r-i","i-z","z-Y"};
  string types[2]={"S","G"};
  string int_types[2]={"2","1"};
  string type_labels[2]={"stars","galaxies"};

  string color,type,type_label;

  TCanvas *c1=new TCanvas("c1","");

  
  
  // create tree to hold variables
  string branch=get_branches(coadd,"all",bcs);
  TTree *tree=new TTree("tree","");

  tree->ReadFile(inputfile.c_str(),branch.c_str());
  if(debug) cout<<"Total Entries in tree:"<<tree->GetEntries()<<endl;
  double RA,RA_true,Dec,Dec_true,x_pos,y_pos;
  int classflag;
  
  // loop over galaxies and stars
  for(int cur_type=0;cur_type<2;cur_type++) {
    type=types[cur_type];
    type_label=type_labels[cur_type];
    
    if(debug)cout<<"Type :"<<type_label<<endl;
    
    // loop over colors
    for(int cur_color=0;cur_color<4;cur_color++) {
      color=colors[cur_color];
      
      if(bcs &&  color=="z-Y") continue;
      
      string cur_title=color+" color "+title+" "+type_label;
      if(debug) cout<<"Color :"<<color<<endl;
      
      if(!bcs) {
        if(type=="G") hist_fit_max=0.9*1.5;
        else hist_fit_max=0.45*1.5;
      }
      else {
        if(type=="G") hist_fit_max=0.4*2.5;
        else hist_fit_max=0.2*2.5;
      }
      c1->Clear();
      c1->cd();
      //hist_fit_max=10;
      // do any pre-selection cuts by producing an event list
      string basic_cut;
      if(color=="g-r") {
        basic_cut=Form("g_obs_err<%f && r_obs_err<%f && "
                       "g_flags < %d && r_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
      }
      else if(color=="r-i") {
        basic_cut=Form("r_obs_err<%f && i_obs_err<%f && "
                       "r_flags < %d && i_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
      }
      else if(color=="i-z") {
        basic_cut=Form("i_obs_err<%f && z_obs_err<%f && "
                       "i_flags < %d && z_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
      }
      else if(color=="z-Y") {
        basic_cut=Form("z_obs_err<%f && Y_obs_err<%f && "
                       "z_flags < %d && Y_flags < %d",
                       err_cut,err_cut,phot_flag,phot_flag);
      }
      
      
      
      
      if(!bcs) basic_cut+=" && class==\""+type+"\"";
      else basic_cut+=" && classflag=="+int_types[cur_type];
      
      
      TEventList *elist=0;
      tree->Draw(">>elist",basic_cut.c_str());
      elist = (TEventList*)gDirectory->Get("elist");
      if(!elist) continue;
      
      if(elist->GetN()==0) continue;
      
      // load the vectors with the data
      double obs_color,true_color,diff,err,true_err;
      double obs_mag1,obs_mag2,mag_err1,mag_err2;
      double true_mag1,true_mag2;
        
      
	// allow for different truth labels
	string true_label;
      if(which_truth=="true") true_label=color+"_true";
      else true_label=which_truth+"_"+color;
      
      tree->ResetBranchAddresses();
      if(color=="g-r") {
        tree->SetBranchAddress("g_mag_obs",&obs_mag1);
        tree->SetBranchAddress("r_mag_obs",&obs_mag2);
        tree->SetBranchAddress("g_true",&true_mag1);
        tree->SetBranchAddress("r_true",&true_mag2);
        tree->SetBranchAddress("g_obs_err",&mag_err1);
        tree->SetBranchAddress("r_obs_err",&mag_err2);
      }
      else if (color=="r-i") {
        tree->SetBranchAddress("r_mag_obs",&obs_mag1);
        tree->SetBranchAddress("i_mag_obs",&obs_mag2);
        tree->SetBranchAddress("r_true",&true_mag1);
        tree->SetBranchAddress("i_true",&true_mag2);
        tree->SetBranchAddress("r_obs_err",&mag_err1);
        tree->SetBranchAddress("i_obs_err",&mag_err2);
      }
      else if (color=="i-z") {
        tree->SetBranchAddress("i_mag_obs",&obs_mag1);
        tree->SetBranchAddress("z_mag_obs",&obs_mag2);
        tree->SetBranchAddress("i_true",&true_mag1);
        tree->SetBranchAddress("z_true",&true_mag2);
        tree->SetBranchAddress("i_obs_err",&mag_err1);
        tree->SetBranchAddress("z_obs_err",&mag_err2);
      }
      else if (color=="z-Y") {
        tree->SetBranchAddress("z_mag_obs",&obs_mag1);
        tree->SetBranchAddress("Y_mag_obs",&obs_mag2);
        tree->SetBranchAddress("z_true",&true_mag1);
        tree->SetBranchAddress("Y_true",&true_mag2);
        tree->SetBranchAddress("z_obs_err",&mag_err1);
        tree->SetBranchAddress("Y_obs_err",&mag_err2);
      }
      

// 	string true_errlabel=which_truth+"err_"+band;
// 	if(bcs) {
// 	  tree->SetBranchAddress(true_errlabel.c_str(),&true_err);
// 	}
// 	else true_err=0;
      true_err=0;
	
	vector<double> full_obs_color,full_true_color,
          full_diff,full_err,full_true_err;  
	true_err=0;
	int i=0;
	while(tree->GetEntry(elist->GetEntry(i))>0) {
          obs_color=obs_mag1-obs_mag2;
          err=obs_mag1-obs_mag2;
          true_color=true_mag1-true_mag2;
          err=sqrt(mag_err1*mag_err1+mag_err2*mag_err2);

	  full_obs_color.push_back(obs_color);
	  full_true_color.push_back(true_color);
	  full_diff.push_back(obs_color-true_color);
	  full_err.push_back(err);
	  full_true_err.push_back(true_err);
	  i++;
	}
	int N_entries = full_diff.size();

	if(debug) {
	  cout<<"Total Objects: "<<N_entries<<endl;
	}
	
	if(N_entries==0) {delete elist;continue;}

	// Sort tree into index according to true color
	int *index = new int[N_entries];
	TMath::Sort(N_entries,&full_true_color[0],index,false);

	// Fill ordered vectors
	vector<double> order_obs_color,order_true_color,
	  order_diff,order_err,order_true_err;  
        
        int desired_bins=8;
        if(1.*N_entries/N_bin> desired_bins) {
          N_bin=(int)N_entries/desired_bins*1.;
        }

	for(int i=0;i<N_entries;i++) {

	  order_obs_color.insert(order_obs_color.end(),
                                 full_obs_color[index[i]]);
	  order_true_color.insert(order_true_color.end(),
                                  full_true_color[index[i]]);
	  order_diff.insert(order_diff.end(),full_diff[index[i]]);
	  order_err.insert(order_err.end(),full_err[index[i]]);
	  order_true_err.insert(order_true_err.end(),full_true_err[index[i]]);

	}
      

	TGraph *sigma_scatter=new TGraph;  
	TGraph *sigma_gr_scatter=new TGraph;  
	vector<double> bins,sys_error,tot_error,mean_error,mean_vec,frac_passed;
	int global_counter=0,sigma_counter=0,sigma_gr_counter=0;

	int bin=0;


	// Loop until the end of the vectors have been reached
	bool finished=false;
	while(!finished) {

	  if(debug) cout<<"Bin "<<bin<<endl;
	  TGraph scatter,scatter_clipped;


	  // create sub list of vectors with either N_bin entries or
	  // the number in a color difference of 1
	  vector<double> sub_obs_color,sub_true_color,sub_diff,
            sub_err,sub_true_err;

	  int current_min=global_counter;
	  for(int entry=0; entry<N_bin ; entry++) {
      
	    // check if at the end of the vector;
	    if(global_counter  >= N_entries) {
	      finished=true;
	      break;
	    }
	    if(debug)cout<<"  entry "<<entry<<" "<<global_counter<<" "
			 <<order_true_color[global_counter]<<" "
			 <<order_obs_color[global_counter]<<endl;
            
	    // check if the color difference is greater than 1 between the
	    // current and first of the bin
	   //  if(entry>2) {
// 	      if(order_true_color[global_counter]-order_true_color[current_min] >1.) {
// 		global_counter++;
// 		break;
// 	      }
// 	    }
      
	    scatter.SetPoint(entry,order_true_color[global_counter],
			     order_diff[global_counter]);

	    sub_obs_color.insert(sub_obs_color.end(),order_obs_color[global_counter]);
	    sub_true_color.insert(sub_true_color.end(),order_true_color[global_counter]);
	    sub_diff.insert(sub_diff.end(),order_diff[global_counter]);
	    sub_err.insert(sub_err.end(),order_err[global_counter]);
	    sub_true_err.insert(sub_true_err.end(),order_true_err[global_counter]);

	    global_counter++;
	  }
    

    
	  if(debug) cout<<"Subset Size "<<sub_diff.size()<<endl;

	  // Make sure there at least min_entries
	  if(sub_diff.size()<min_entries) {
            if(debug)cout<<"Not enough entries in this bin skipping "<<endl;
            continue;
          }
          
	  vector<double> dum(sub_diff.size());
          int good,bad;
          double mean,rms;
          sigma_clip(3,2.5,sub_diff,dum,mean,
                     good,bad,rms,true,false,1e-6);
          double hist_max=mean+10*rms;
          double hist_min=mean-10*rms;
          if(hist_max>hist_fit_max) hist_max=hist_fit_max;
          if(hist_min<-1*hist_fit_max) hist_min=-hist_fit_max;
	  // fit to gaussian and to do  sigma clipping
	  TH1D *h1=new TH1D(Form("fit%d",bin+1),"",35,
                            hist_min,hist_max);
	  for(int i=0;i<sub_diff.size();i++) {
	    h1->Fill(sub_diff[i]);
	  }
          if(debug) cout<<"Clipped mean="<<mean<<" rms="<<rms<<endl;
          if(debug) cout<<"Hist range: "<<hist_min<<" "<<hist_max<<endl;
	  if(h1->Integral()<min_entries) {
            if(debug) cout<<"Found less than the number of entries in "
            <<"the histogram skipping entries in bin"<<endl;
            h1->SetName("");
            continue;
	  }
	  
	  double hist_mean=h1->GetMean();
	  double hist_rms=h1->GetRMS();
	  if(debug) cout<<"Hist Mean "<<hist_mean<<" RMS "<<hist_rms<<endl;
          
	  TF1 *f1=new TF1("f1","gaus",hist_mean-func_fit*hist_rms,
			  hist_mean+func_fit*hist_rms);

	  f1->SetNpx(10000);
	  f1->SetParameter(1,0);
	  h1->Fit(f1,"QR");

	  double gaus_const=f1->GetParameter(0);
	  double gaus_mean=f1->GetParameter(1);
	  double gaus_sigma=f1->GetParameter(2);
          if(debug) cout<<"Fit Results mean="<<gaus_mean<<" sigma="
                        <<gaus_sigma<<endl;
	  // draw lines to indicate cuts
	  if(debug) {
      
	    TLine *hi=new TLine(gaus_mean+sigma*gaus_sigma,0,
				gaus_mean+sigma*gaus_sigma,h1->GetMaximum());
	    TLine *low=new TLine(gaus_mean-sigma*gaus_sigma,0,
				 gaus_mean-sigma*gaus_sigma,h1->GetMaximum());
      
	    low->SetLineWidth(3);
	    hi->SetLineWidth(3);
	    hi->SetLineColor(2);
	    low->SetLineColor(2);
	    h1->Draw();
	    hi->Draw();
	    low->Draw();
	  }

	  // erase name to not have warning messages
	  h1->SetName("");
	
	  // cut out all values greater than sigma*gaus_sigma(from fit)
	  vector<double> vec_obs_color,vec_true_color,vec_diff,vec_err,vec_true_err;
    
	  int clipped_counter=0;
	  for(int entry=0; entry<sub_diff.size() ; entry++) {
      
	    if(fabs(sub_diff[entry]-gaus_mean)<sigma*gaus_sigma) {
	      vec_obs_color.insert(vec_obs_color.end(),sub_obs_color[entry]);
	      vec_true_color.insert(vec_true_color.end(),sub_true_color[entry]);
	      vec_diff.insert(vec_diff.end(),sub_diff[entry]);
	      vec_err.insert(vec_err.end(),sub_err[entry]);
	      vec_true_err.insert(vec_true_err.end(),sub_true_err[entry]);
	      scatter_clipped.SetPoint(clipped_counter,sub_true_color[entry],
				       sub_diff[entry]);
	      clipped_counter++;
	      sigma_scatter->SetPoint(sigma_counter,sub_true_color[entry],
				      sub_diff[entry]);
	      sigma_counter++;
	    } else {
              
              sigma_gr_scatter->SetPoint(sigma_gr_counter,sub_true_color[entry],
                                         sub_diff[entry]);
              sigma_gr_counter++;
            }
            

	  }


	  //h1->Write();


	  // calculate how many events were cut


    
	  if(debug) cout<<"Bin "<<bin+1<<" with "<<vec_diff.size()
                        <<"clipped entries"<<endl;      

	  if(vec_diff.size()<min_entries) {
            if(debug) cout<<"Found fewer good events than minimum"
                          <<" skipping entries in bin: "<<vec_diff.size()
                          <<endl;
            continue;
          }

	  double frac=1.*vec_diff.size()/sub_diff.size();
	  frac_passed.push_back(frac);
    
	  if(debug) {
	    cout<<"Color Range: "<<vec_true_color[0]<<" to "    
		<<vec_true_color[vec_true_color.size()-1]<<endl;
	  }
    
	  scatter.SetName(Form("scatter%d",bin+1));
	  //scatter.Write();

	  scatter_clipped.SetName(Form("scatter_clipped%d",bin+1));
	  //scatter_clipped.Write();
    

	SysErrFit fit;
	double err;
	double min=fit.Fit(&vec_diff,&vec_err,err,&vec_true_err);
	if(debug)cout<<"Minimization Minimum Found at = "<<abs(min)<<endl;
	
	
	  // Look at what the fitting is doing
	  if(debug) {
	    c1->Update();
	    c1->WaitPrimitive();
	  }



	  // if minimization fails use 0.0 as default
	  if(fabs(min)>func_max) {
	    cout<<"Error: root finder failed"<<endl;
	    min=0.0;
	  };

	  // set variables
	  sys_error.push_back(min);
	  tot_error.push_back(gaus_sigma);
	  mean_error.push_back(hist_rms/sqrt(h1->Integral()));
	  mean_vec.push_back(gaus_mean);
    
    
	  // get the last of the observed colornitude for binning
	  if(bin==0) bins.insert(bins.end(),sub_true_color.front());
	  bins.insert(bins.end(),sub_true_color.back());
	  bin++;
      
	}
	if(bins.size()==0) {delete elist; continue;}

	if(debug)cout<<"Finished Fitting"<<endl;
  
	TH1D *hist_sys_err=new TH1D("hist_sys_err",
				    ";color_{true};Systematic Error / 1000 events",
				    bins.size()-1,&bins[0]);
	TH1D *hist_tot_err=new TH1D("hist_tot_err",
				    ";color_{true};Systematic Error / 1000 events",
				    bins.size()-1,&bins[0]);
	TH1D *hist_mean_err=new TH1D("hist_mean_err",
				     ";color_{true};Systematic Error / 1000 events",
				     bins.size()-1,&bins[0]);
	TH1D *hist_frac=new TH1D("hist_frac",
				 Form(";%s_{true};Fraction within 3#sigma;",
				      color.c_str()),
				 bins.size()-1,&bins[0]);
        TGraph graph_tot_err;
        double hist_min=1e6,hist_max=-1e6;
        
	for(int i=0;i<sys_error.size();i++) {
	  hist_sys_err->SetBinContent(i+1,mean_vec[i]);
	  hist_sys_err->SetBinError(i+1,sys_error[i]);

	  hist_tot_err->SetBinContent(i+1,mean_vec[i]);
	  hist_tot_err->SetBinError(i+1,tot_error[i]);

	  hist_mean_err->SetBinContent(i+1,mean_vec[i]);
	  hist_mean_err->SetBinError(i+1,mean_error[i]);

	  hist_frac->SetBinContent(i+1,frac_passed[i]);

          graph_tot_err.SetPoint(i,
                                 hist_tot_err->GetXaxis()->GetBinCenter(i+1),
                                 tot_error[i]);

          if(mean_vec[i]+TMath::Max(tot_error[i],sys_error[i])>hist_max){
            hist_max=mean_vec[i]+TMath::Max(tot_error[i],sys_error[i]);
          }
          if(mean_vec[i]-TMath::Max(tot_error[i],sys_error[i])<hist_min){
            hist_min=mean_vec[i]-TMath::Max(tot_error[i],sys_error[i]);
          }
	} 

        if(hist_max>0.6) hist_max=0.6;
        if(hist_min<-0.6) hist_min=-0.6;
        

        //cout<<"** Actual value "<<graph_tot_err.Eval(val)<<endl;

	TLatex *label=new TLatex;
	label->SetNDC();
	label->SetTextSize(0.045);
	label->SetTextFont(42);
	label->SetTextAlign(22);
	
// 	c1->SetLeftMargin(0.17);
// 	hist_frac->GetYaxis()->CenterTitle();
// 	hist_frac->GetYaxis()->SetRangeUser(0,1);
// 	hist_frac->SetTitleOffset(1.6,"y");
// 	hist_frac->GetXaxis()->CenterTitle();
// 	hist_frac->Draw();
// 	label->DrawLatex(0.49,0.948,cur_title.c_str());
// 	c1->Update();
// 	if(debug) c1->WaitPrimitive();

 	string id=color+"_"+type_label+"_frac";
 	string print=dir+"/"+id+".png";
// 	if(debug)c1->WaitPrimitive();
// 	c1->Print(print.c_str());
// 	//hist_sys_err->Write();
// 	//hist_frac->Write();
        label->SetTextSize(0.06);
  
	TCanvas *c2 = new TCanvas("c2","",0,0,772,802);
	TPad *pad1 = new TPad("pad1"," ",0.0,0.37,1.0,1.0,0);
	TPad *pad2 = new TPad("pad2"," ",0.0,0.0,1,0.37,0);

	pad1->SetBottomMargin(0.015);
	pad1->SetLeftMargin(0.15);
	pad2->SetLeftMargin(0.15);
	pad2->SetTopMargin(0.015);
	pad2->SetBottomMargin(0.3);
	pad1->SetTicks(1,0);
  
	pad1->Draw();
	pad2->Draw();
	pad1->cd();
	hist_tot_err->SetLabelOffset(1,"x");
	hist_tot_err->SetLabelSize(0.06,"y");
	
	if(!bcs)hist_tot_err->GetYaxis()->SetRangeUser(hist_min-0.1,hist_max+0.1);
	else hist_tot_err->GetYaxis()->SetRangeUser(-0.61,0.611);
	hist_tot_err->SetYTitle("");

	hist_mean_err->SetLineColor(2);
	hist_sys_err->SetLineColor(4);

	hist_mean_err->SetFillColor(1);
	hist_sys_err->SetFillColor(2);
	hist_tot_err->SetFillColor(4);

	hist_mean_err->SetLineWidth(3);
	hist_sys_err->SetLineWidth(3);
	hist_tot_err->SetLineWidth(3);


	hist_tot_err->Draw("e1");
	hist_sys_err->Draw("e1,same");  
	hist_mean_err->Draw("e1,same");
	TF1 *f3=new TF1("f3","0",-100,100);
	f3->SetLineColor(15);
	f3->SetLineStyle(2);
	f3->Draw("same");
	
	label->DrawLatex(0.49,0.948,cur_title.c_str());

	TLatex *ty1=new TLatex;
	ty1->SetNDC();
	ty1->SetTextSize(0.07);
	ty1->SetTextFont(42);
	ty1->SetTextAngle(90);
	ty1->SetTextAlign(22);
	ty1->DrawLatex(0.031,0.5,Form("< %s_{%s} - %s_{%s} >",color.c_str(),
			       which_obs.c_str(),color.c_str(),
			       which_truth.c_str()));
   
	TLegend *leg=new TLegend(0.18,0.04,0.46,0.25);
	leg->SetTextFont(42);
	leg->SetTextSize(0.06);
	leg->SetBorderSize(0);
	leg->AddEntry(hist_mean_err,"#sigma_{mean}","l");
	leg->AddEntry(hist_sys_err,"#sigma_{sys}","l");
	leg->AddEntry(hist_tot_err,"#sigma_{tot}","l");
	TLegendEntry *leg_entry=leg->AddEntry(hist_frac,
					      "Fraction < 3#sigma_{tot}","l");
	leg_entry->SetTextSize(0.045);
	leg->Draw();

        if(draw_text) {
        // Draw numbers
        TLatex *txt=new TLatex;
        txt->SetTextSize(0.025);
        for(int i=0;i<hist_mean_err->GetNbinsX();i++) {
          double x=hist_mean_err->GetXaxis()->GetBinCenter(i+1);
          double y=hist_tot_err->GetBinContent(i+1)+
            TMath::Max(hist_tot_err->GetBinError(i+1),
                       hist_sys_err->GetBinError(i+1));

          double sep=0.02*(1.1*(hist_max-hist_min)/0.42);
          if(y+2*sep<hist_max+0.1) {
            txt->SetTextColor(kRed);
            txt->DrawLatex(x,y+sep,
                           Form("%0.2f",hist_mean_err->GetBinContent(i+1)));
            txt->SetTextColor(kBlue);
            txt->DrawLatex(x,y+2*sep,
                           Form("%0.1f",hist_sys_err->GetBinError(i+1)*1000));
            txt->SetTextColor(kBlack);
            txt->DrawLatex(x,y+3*sep,
                           Form("%0.1f",hist_tot_err->GetBinError(i+1)*1000));
          }
          
        }
        }          

	// draw fraction of values outside 3sigma
	pad1->Update();
	
	Float_t rightmax = 1;
	Float_t scale = pad1->GetUymax()/rightmax;
	hist_frac->SetLineColor(kMagenta);
	hist_frac->SetLineStyle(1);
	hist_frac->SetLineWidth(2);
	hist_frac->Scale(scale);
	hist_frac->Draw("same][");

	//draw an axis on the right side
	TGaxis *axis = new TGaxis(pad1->GetUxmax(),pad1->GetUymin(),
				  pad1->GetUxmax(), pad1->GetUymax(),
				  0.0001,rightmax,508,"+L");
	axis->SetLineColor(kMagenta);
	axis->SetLabelSize(0.06);
	axis->SetLabelOffset(0.012);
	axis->SetLabelFont(42);
	axis->SetLabelColor(kMagenta);
	axis->Draw();
// 	global_file->cd();
// 	hist_tot_err->Write();
// 	hist_frac->Write();
// 	global_file->Close();

	pad2->cd();

        // if there are too many points only sample the
        // the scatter plot
	TGraph *full_scatter=new TGraph;
	TGraph *sample_all=new TGraph;
	TGraph *sample_sub=new TGraph;
	TGraph *sample_gr=new TGraph;
        


        double frac_accept=1;
        if(sigma_scatter->GetN()>max_scatter) {
          frac_accept=max_scatter*1.0/full_diff.size();
        }

        TRandom3 rand;
	for(int i=0;i<full_diff.size();i++) {
          full_scatter->SetPoint(i,full_true_color[i],full_diff[i]);
          if(rand.Uniform()<frac_accept) {
            sample_all->SetPoint(i,full_true_color[i],full_diff[i]);
          }
	}

        
	for(int i=0;i<sigma_scatter->GetN();i++) {
          double x,y;
          sigma_scatter->GetPoint(i,x,y);
          if(rand.Uniform()<frac_accept) {
            sample_sub->SetPoint(i,x,y);
          }
        }  

	for(int i=0;i<sigma_gr_scatter->GetN();i++) {
          double x,y;
          sigma_gr_scatter->GetPoint(i,x,y);
          if(rand.Uniform()<frac_accept) {
            sample_gr->SetPoint(i,x,y);
          }
        }



        vector<double> dum(full_diff.size());
        int good,bad;
        double mean,rms;
        sigma_clip(3,2.5,full_diff,dum,mean,
                   good,bad,rms,true,false,1e-6);
        TH2D *h3=new TH2D("h3","",100,bins[0],
                          bins[bins.size()-1],100,mean-10*rms,mean+10*rms);
          
        double max_scat=mean+7*rms;
        double min_scat=mean-7*rms;
        if(max_scat>0.8) max_scat=0.8;
        if(min_scat<-0.8) min_scat=-0.8;

        h3->GetYaxis()->SetRangeUser(min_scat,max_scat);
        
	h3->SetNdivisions(506,"y");
	h3->Draw();
	h3->SetLabelSize(0.11,"xy");
	TF1 *f2=new TF1("f2","0",-100,100);
	f2->SetLineColor(1);
	f2->Draw("same");
	sample_gr->SetMarkerColor(2);
	sample_gr->SetMarkerStyle(8);
	sample_gr->SetMarkerSize(0.25);
	sample_gr->Draw("p");
	sample_sub->SetMarkerColor(4);
	sample_sub->SetMarkerStyle(8);
	sample_sub->SetMarkerSize(0.25);
	sample_sub->Draw("p");

  
	TLatex *tx=new TLatex;
	tx->SetNDC();
	tx->SetTextSize(0.13);
	tx->SetTextFont(42);
	tx->SetTextAlign(22);
	tx->DrawLatex(0.55,0.079,Form("%s_{%s}",color.c_str(),
				      which_truth.c_str()));

	TLatex *ty2=new TLatex;
	ty2->SetNDC();
	ty2->SetTextSize(0.10);
	ty2->SetTextFont(42);
	ty2->SetTextAngle(90);
	ty2->SetTextAlign(22);
        ty2->DrawLatex(0.029,0.65,Form("%s_{%s} - %s_{%s}",color.c_str(),
				which_obs.c_str(),color.c_str(),
				which_truth.c_str()));



	TObject *dummy=0;
	TLegend *leg2=new TLegend(0.18,0.70,0.37,0.95);
	leg2->SetTextFont(42);
	leg2->SetTextSize(0.07);
	leg2->SetBorderSize(0);

	TLegendEntry *e1=leg2->AddEntry(dummy,"Outliers","p");
	e1->SetMarkerSize(0.9);
	e1->SetMarkerStyle(20);
	e1->SetMarkerColor(2);

	TLegendEntry *e2=leg2->AddEntry(dummy,"Within 3#sigma","p");
	e2->SetMarkerSize(0.9);
	e2->SetMarkerStyle(20);
	e2->SetMarkerColor(4);
	leg2->Draw();


	//c2->Write("can");

	id=dir+"/"+color+"_"+type_label+"_phot";
	print=id+".png";
	if(debug)c2->WaitPrimitive();
	c2->Print(print.c_str());
	
	
	string convert="convert "+id+".eps "+id+".png";
	//gSystem->Exec(convert.c_str());

	convert="rm "+print;
	//gSystem->Exec(convert.c_str());

	pad2->cd();
	//TH2D *h2=new TH2D("h2","",100,bins[0],
	//bins[bins.size()-1],100,min-0.01,max+0.01);
	TH2D *h2=new TH2D("h2","",100,bins[0],
			  bins[bins.size()-1],100,-8,1);

   
   
	h2->SetNdivisions(506,"y");
	h2->Draw();
	h2->SetLabelSize(0.11,"xy");
	TF1 *f4=new TF1("f4","0",0,100);
	f4->SetLineColor(1);
	f4->Draw("same");
	sample_gr->SetMarkerColor(2);
	sample_gr->SetMarkerStyle(0);
	sample_gr->SetMarkerSize(0.18);
	sample_gr->Draw("p");
	sample_sub->SetMarkerColor(4);
	sample_sub->SetMarkerStyle(0);
	sample_sub->SetMarkerSize(0.18);
	sample_sub->Draw("p");
   
   
		tx->DrawLatex(0.55,0.079,Form("%s_{%s}",color.c_str(),
					      which_truth.c_str()));
	ty2->DrawLatex(0.029,0.65,Form("%s_{%s} - %s_{%s}",color.c_str(),
				       which_obs.c_str(),color.c_str(),
				       which_truth.c_str()));

     
	TLegend *leg3=new TLegend(0.18,0.36,0.375,0.62);
	leg3->SetTextFont(42);
	leg3->SetTextSize(0.07);
	leg3->SetBorderSize(0);


	e1=leg3->AddEntry(dummy,"Total Sample","p");
	e1->SetMarkerSize(0.9);
	e1->SetMarkerStyle(20);
	e1->SetMarkerColor(2);

	e2=leg3->AddEntry(dummy,"Within 3#sigma","p");
      e2->SetMarkerSize(0.9);
      e2->SetMarkerStyle(20);
      e2->SetMarkerColor(4);
      leg3->Draw();

      print=id+"_zoomout.png";
      //c2->Print(print.c_str());


      convert="convert "+id+"_zoomout.eps "+id+"_zoomout.png";
      //gSystem->Exec(convert.c_str());

      convert="rm "+print;
      //gSystem->Exec(convert.c_str());
  
      delete f2; delete f3; delete hist_sys_err; delete hist_mean_err; delete hist_tot_err; 
      delete h2; delete h3; delete elist; delete hist_frac; delete c2;delete f4;
      
    }
  }
  
  delete c1;
  
}






////////////////////////////////////////////////////////////////
//   Functions to plot astrometry
//
////////////////////////////////////////////////////////////////

double BifurGaus(double *x, double *par)
{
  double constant=par[0];
  double mean=par[1];
  double sigmaL=par[2];
  double sigmaR=par[3];
  

  
  double arg = x[0] - mean;
  double coef(0.0);

  if (arg < 0.0){
    if (TMath::Abs(sigmaL) > 1e-30) {
      coef = -0.5/(sigmaL*sigmaL);
    }
  } else {
    if (TMath::Abs(sigmaR) > 1e-30) {
      coef = -0.5/(sigmaR*sigmaR);
    }
  }

  return constant*exp(coef*arg*arg);
}



int plot_astro(string input_file,string title,string dir,string which_band,
	       bool coadd,bool bcs,bool debug)
{

  // Some formatting options
  gStyle->SetOptStat(1110);
  gStyle->SetOptFit(0010);
  gStyle->SetStatFormat("4.2g"); 
  gStyle->SetFitFormat("4.2g"); 
  gStyle->SetTitleOffset(1.6,"y");

  string types[2]={"S","G"};
  string int_types[2]={"2","1"};
  string type_labels[2]={"stars","galaxies"};

  string band,type,type_label;

  
  // loop over galaxies and stars
  string branch=get_branches(coadd,"g",bcs);
  
  TTree *tree=new TTree("tree","");
  tree->ReadFile(input_file.c_str(),branch.c_str());

  TCanvas *c1=new TCanvas("c1","");
  c1->SetLeftMargin(0.16);  

  double RA,RA_true,Dec,Dec_true,x_pos,y_pos,RA_err,Dec_err;      
  char class_type;
  
  for(int cur_type=0;cur_type<2;cur_type++) {
    
    c1->Clear();
    c1->cd();


    type=types[cur_type];
    type_label=type_labels[cur_type];
    string cur_title=title+" "+type_label;
      
    
    // Read Tree from file
    tree->SetBranchAddress("RA_true",&RA_true);
    tree->SetBranchAddress("RA",&RA);
    tree->SetBranchAddress("RA_err",&RA_err);
    tree->SetBranchAddress("Dec",&Dec);
    tree->SetBranchAddress("Dec_err",&Dec_err);
    tree->SetBranchAddress("Dec_true",&Dec_true);
    tree->SetBranchAddress("x_pos",&x_pos);
    tree->SetBranchAddress("y_pos",&y_pos);
    
    // basic cut 
    string basic_cut;
    if(!bcs) basic_cut="class==\""+type+"\"";
    else basic_cut="classflag=="+int_types[cur_type];


    TEventList *elist=0;
    tree->Draw(">>elist",basic_cut.c_str());
    elist=(TEventList*)gDirectory->Get("elist");
    if(!elist) {
      cout<<"Didn't find any objects with cut: "<<endl;
      cout<<"\t"<<basic_cut<<endl;
      continue;
    }

    if(elist->GetN()==0) continue;
    
    vector<double> ra,ra_true,dec,dec_true,diff_ra,diff_dec,vec_x,vec_y,
      ra_err,dec_err;  
    double conv=2.777e-4;
    
    int ii=0;
    while(tree->GetEntry(elist->GetEntry(ii))>0) {
      
      ra.push_back(RA/conv);
      ra_err.push_back(RA_err/conv);
      ra_true.push_back(RA_true/conv);
      dec.push_back(Dec/conv);
      dec_err.push_back(Dec_err/conv);
      dec_true.push_back(Dec_true/conv);
      
      diff_ra.push_back(cos(Dec*TMath::DegToRad())*(RA-RA_true)/conv);
      diff_dec.push_back((Dec-Dec_true)/conv);
      vec_x.push_back(x_pos);
      vec_y.push_back(y_pos);
      ii++;
    }
    
    ShearFit shear_x;
    vector<double> x_par,y_par;
    vector<double> x_err,y_err;
    
    x_par=shear_x.Fit(&diff_ra,&ra,&dec,x_err);
    
//     cout<<"a = "<<x_par[0]<<" +-"<<x_err[0]<<endl;
//     cout<<"b = "<<x_par[1]<<" +-"<<x_err[1]<<endl;
//     cout<<"c = "<<x_par[2]<<" +-"<<x_err[2]<<endl;


    ShearFit shear_y;
    y_par=shear_y.Fit(&diff_dec,&ra,&dec,y_err);
    
    
//     cout<<"d = "<<y_par[1]<<" +-"<<y_err[1]<<endl;
//     cout<<"e = "<<y_par[0]<<" +-"<<y_err[0]<<endl;
//     cout<<"f = "<<y_par[2]<<" +-"<<y_err[2]<<endl;    
    
    double a=x_par[0];
    double b=x_par[1];
    double c=x_par[2];
    double d=y_par[1];
    double e=y_par[0];
    double f=y_par[2];
    
    
    double magnification=a+e;
    double rotation=b-d;
    double shear_1=a-e;
    double shear_2=b+d;
    
    double shear=sqrt(shear_1*shear_1+shear_2*shear_2);



    
    ///////////////////////////////////////////////////////////////
    // Do RA plot
    //////////////////////////////////////////////////////////////
    
    
    
    // Fill RA histogram
    // double max_limit,min_limit;
//     if(!bcs){
//       max_limit=0.5;min_limit=-0.5;
//     }
//     else {
//       max_limit=1.5;min_limit=-1.5;
//     }

    double ra_mean;//=TMath::Mean(diff_ra.size(),&diff_ra[0]);
    double ra_rms;//=TMath::RMS(diff_ra.size(),&diff_ra[0]);
    
    vector<double> dum(diff_ra.size());
    int good,bad;
    sigma_clip(3,2.5,diff_ra,dum,ra_mean,
               good,bad,ra_rms,true,false,1e-6);
    // Fill histogram
    double max_limit=ra_mean+15*ra_rms,min_limit=ra_mean-15*ra_rms;


    TH1D *RA_diff=new TH1D("RA_diff",";RA - RA_{true} (arcseconds);Entries",
			   50,min_limit,max_limit);

    RA_diff->GetXaxis()->CenterTitle();
    RA_diff->GetYaxis()->CenterTitle();
    int total=tree->Draw("cos(Dec*TMath::DegToRad())*(RA-RA_true)/2.777e-4>>RA_diff",basic_cut.c_str());

    
    // fit to a bifurcated gaussian
    TF1 *func = new TF1("bifurgaus",BifurGaus,min_limit,max_limit,4);
    func->SetNpx(100000);
    func->SetParameters(100,0.0,0.1,0.1);
    func->SetParNames("Constant","Mean Fit","#sigma_{Left}","#sigma_{Right}");
    func->SetParLimits(2,0,10);
    func->SetParLimits(3,0,10);
    RA_diff->Fit(func,"RQ");
    RA_diff->Draw();
    c1->Update();
    
    double gaus_const=func->GetParameter(0);
    double gaus_mean=func->GetParameter(1);
    double gaus_sigmaL=func->GetParameter(2);
    double gaus_sigmaR=func->GetParameter(3);
    
    
    double min_cut=3*gaus_sigmaL;
    double max_cut=3*gaus_sigmaR;
    double min=gaus_mean-min_cut;
    double max=gaus_mean+max_cut;
    
    
      // 3sigma cut
    string sig_cut=basic_cut+Form(" && cos(Dec*TMath::DegToRad())*(RA-RA_true)/2.777e-4>%f  && cos(Dec*TMath::DegToRad())*(RA-RA_true)/2.777e-4<%f",min,max);
    
    // get number of entries that with 3simga
    int inside=tree->Draw("RA",sig_cut.c_str(),"goff");
    
    // calculate the outlier fraciton
    double outside=1-(1.*inside/total);





    
    // Find systematic error
    vector<double> fit_diff_ra,fit_ra_err;
    
    for(int i=0;i<diff_ra.size();i++) {
      if(diff_ra[i]>min && diff_ra[i]<max) {
	fit_diff_ra.push_back(diff_ra[i]);
	fit_ra_err.push_back(ra_err[i]);
      }
    }

    SysErrFit RA_fit;
    double RA_min_err;
    double RA_min=RA_fit.Fit(&fit_diff_ra,&fit_ra_err,RA_min_err);
    if(debug)cout<<"RA Minimization Minimum Found at = "<<RA_min<<endl;




    
    RA_diff->Draw();

    
    // Add text to statbox
    TPaveStats *ps_RA = (TPaveStats*)c1->GetPrimitive("stats");
    ps_RA->SetName("mystats_RA");
    ps_RA->SetX1NDC(0.54) ; 
    ps_RA->SetX2NDC(0.84) ; 
    ps_RA->SetY1NDC(0.61) ; 
    ps_RA->SetY2NDC(0.89);
    TList *list_RA = ps_RA->GetListOfLines();
    TText *tconst_RA = ps_RA->GetLineWith("Constant"); 

    TLatex *myt_RAsys = new 
      TLatex(0,0,Form("#sigma_{sys}= %2.2g",RA_min));
    myt_RAsys->SetTextFont(tconst_RA->GetTextFont());
    myt_RAsys->SetTextSize(tconst_RA->GetTextSize());
    myt_RAsys->SetTextColor(2);
    list_RA->Add(myt_RAsys);

    TLatex *myt_RA = new 
      TLatex(0,0,Form("Fraction > 3#sigma = %0.2f",outside));
    myt_RA->SetTextFont(tconst_RA->GetTextFont());
    myt_RA->SetTextSize(tconst_RA->GetTextSize());
    myt_RA->SetTextColor(2);
    list_RA->Add(myt_RA);
    TLatex *myt_Shear = new 
      TLatex(0,0,Form("Shear = %2.2g",shear));
    myt_Shear->SetTextFont(myt_RA->GetTextFont());
    myt_Shear->SetTextSize(myt_RA->GetTextSize());
    myt_Shear->SetTextColor(2);
    list_RA->Add(myt_Shear);


    list_RA->Remove(tconst_RA); 
    ps_RA->SetTextSize(0.03);
    ps_RA->SetBorderSize(0);
    ps_RA->SetTextColor(2);
    RA_diff->SetStats(0);
    ps_RA->Draw();
    
    

      // Draw top label
      TLatex *label=new TLatex;
      label->SetNDC();
      label->SetTextSize(0.045);
      label->SetTextFont(42);
      label->SetTextAlign(22);
      string RA_label;
      if(!coadd) RA_label=which_band+" band RA "+cur_title;
      else RA_label="RA "+cur_title;
      label->DrawLatex(0.49,0.948,RA_label.c_str());
      
      if(debug) {
	c1->Update();
	c1->WaitPrimitive();
      }
      // print out plot
      string id="RA_"+type_label;
      string print=dir+"/"+id+".png";
      c1->Print(print.c_str());

      string convert="convert "+id+".eps "+id+".png";
      //gSystem->Exec(convert.c_str());
      
      convert="rm "+print;
      //gSystem->Exec(convert.c_str());

      
      
      ///////////////////////////////////////////////////////////////
      // Do Dec plot
      //////////////////////////////////////////////////////////////

      // fill
      TH1D *Dec_diff=new TH1D("Dec_diff",
			      ";Dec - Dec_{true} (arcseconds);Entries",
			      50,min_limit,max_limit);

      Dec_diff->GetXaxis()->CenterTitle();
      Dec_diff->GetYaxis()->CenterTitle();
      total=tree->Draw("(Dec-Dec_true)/2.777e-4>>Dec_diff",basic_cut.c_str());


      Dec_diff->Fit(func,"QR");
      Dec_diff->Draw();
      c1->Update();
      
      
       gaus_const=func->GetParameter(0);
       gaus_mean=func->GetParameter(1);
       gaus_sigmaL=func->GetParameter(2);
       gaus_sigmaR=func->GetParameter(3);

      
       min_cut=3*gaus_sigmaL;
       max_cut=3*gaus_sigmaR;
       min=gaus_mean-min_cut;
       max=gaus_mean+max_cut;

      sig_cut=basic_cut+Form(" && (Dec-Dec_true)/2.777e-4>%f  && (Dec-Dec_true)/2.777e-4<%f",min,max);
      inside=tree->Draw("Dec",sig_cut.c_str(),"goff");
      outside=1-(1.*inside/total);
      
      

      
    
    // Find systematic error
    vector<double> fit_diff_dec,fit_dec_err;
    
    for(int i=0;i<diff_dec.size();i++) {
      if(diff_dec[i]>min && diff_dec[i]<max) {
	fit_diff_dec.push_back(diff_dec[i]);
	fit_dec_err.push_back(dec_err[i]);
      }
    }

    SysErrFit Dec_fit;
    double Dec_min_err;
    double Dec_min=Dec_fit.Fit(&fit_diff_dec,&fit_dec_err,Dec_min_err);
    if(debug)cout<<"Dec Minimization Minimum Found at = "<<Dec_min<<endl;


      // Add text to statbox
      TPaveStats *ps_Dec = (TPaveStats*)c1->GetPrimitive("stats");
      ps_Dec->SetName("mystats_Dec");
      ps_Dec->SetX1NDC(0.54) ; 
      ps_Dec->SetX2NDC(0.84) ; 
      ps_Dec->SetY1NDC(0.61) ; 
      ps_Dec->SetY2NDC(0.89);
      TList *list_Dec = ps_Dec->GetListOfLines();
      TText *tconst_Dec = ps_Dec->GetLineWith("Constant"); 

      TLatex *myt_Decsys = new 
	TLatex(0,0,Form("#sigma_{sys}= %2.2g",Dec_min));
      myt_Decsys->SetTextFont(tconst_Dec->GetTextFont());
      myt_Decsys->SetTextSize(tconst_Dec->GetTextSize());
      myt_Decsys->SetTextColor(2);
      list_Dec->Add(myt_Decsys);



      TLatex *myt_Dec = 
	new TLatex(0,0,Form("Fraction > 3#sigma = %0.2f",outside));
      myt_Dec->SetTextFont(tconst_Dec->GetTextFont());
      myt_Dec->SetTextSize(tconst_Dec->GetTextSize());
      myt_Dec->SetTextColor(2);
      list_Dec->Remove(tconst_Dec); 
      list_Dec->Add(myt_Dec);
      TLatex *myt_Shear_dec = new 
	TLatex(0,0,Form("Shear = %2.2g",shear));
      myt_Shear_dec->SetTextFont(myt_Dec->GetTextFont());
      myt_Shear_dec->SetTextSize(myt_Dec->GetTextSize());
      myt_Shear_dec->SetTextColor(2);
      list_Dec->Add(myt_Shear_dec);


      
      ps_Dec->SetTextSize(0.03);
      ps_Dec->SetBorderSize(0);
      ps_Dec->SetTextColor(2);
      Dec_diff->SetStats(0);
      ps_Dec->Draw();
      

      string dec_label;

      if(!coadd) dec_label=which_band+" band Dec "+cur_title;
      else dec_label="Dec "+cur_title;
      label->DrawLatex(0.49,0.948,dec_label.c_str());
      c1->Update();

      if(debug) {
	c1->WaitPrimitive();
      }
      

      
      id="Dec_"+type_label;
      print=dir+"/"+id+".png";
      c1->Print(print.c_str());

      convert="convert "+id+".eps "+id+".png";
      //gSystem->Exec(convert.c_str());

      convert="rm "+print;
      //gSystem->Exec(convert.c_str());

      delete RA_diff;
      delete Dec_diff;
      delete elist;

  }
      delete c1;


  TFile astrofile("astrofile.root","recreate");
  tree->Write();
  astrofile.Close();
}
  

int plot_diffxy(string inputfile,string title,string dir,
		bool coadd,string band,string label,
		string which_truth,int bins,int neighbors,
		bool bcs,bool debug)
{
  LoadStyle();
  gStyle->SetNdivisions(506,"xy");
  gStyle->SetTitleOffset(1.8,"y");
  gStyle->SetTitleOffset(1.6,"z");
  string types[2]={"S","G"};
  string int_types[2]={"2","1"};
  string type_labels[2]={"stars","galaxies"};
  string type,type_label;
  bool print_points=false;

  string branch=get_branches(coadd,band,bcs);
 
  TTree *tree=new TTree("tree","");
  tree->ReadFile(inputfile.c_str(),branch.c_str());


  double RA,RA_true,Dec,Dec_true,x_pos,y_pos;
  double g_mag,g_mag_true,mag_obs,mag_true,
    r_mag,r_mag_true,
    i_mag,i_mag_true,
    z_mag,z_mag_true,
    Y_mag,Y_mag_true;
  int classflag;
  
  
  for(int cur_type=0;cur_type<2;cur_type++) {
    type=types[cur_type];
    type_label=type_labels[cur_type];

    string cur_title=title+" "+type_label;    
  
    tree->ResetBranchAddresses();
    tree->SetBranchAddress("RA",&RA);
    tree->SetBranchAddress("RA_true",&RA_true);
    tree->SetBranchAddress("Dec",&Dec);
    tree->SetBranchAddress("Dec_true",&Dec_true);
    tree->SetBranchAddress("x_pos",&x_pos);
    tree->SetBranchAddress("y_pos",&y_pos);
    if(bcs) tree->SetBranchAddress("classflag",&classflag);
    
    if(coadd) {  

      tree->SetBranchAddress("g_mag_obs",&g_mag);    
      tree->SetBranchAddress("r_mag_obs",&r_mag);    
      tree->SetBranchAddress("i_mag_obs",&i_mag);
      tree->SetBranchAddress("z_mag_obs",&z_mag);
      if(!bcs)tree->SetBranchAddress("Y_mag_obs",&Y_mag);
      
      if(which_truth=="true") {
	tree->SetBranchAddress("g_true",&g_mag_true);
	tree->SetBranchAddress("r_true",&r_mag_true);
	tree->SetBranchAddress("i_true",&i_mag_true);
	tree->SetBranchAddress("z_true",&z_mag_true);
	if(!bcs)tree->SetBranchAddress("Y_true",&Y_mag_true);
	
      }
      else {
	tree->SetBranchAddress((which_truth+"_g").c_str(),&g_mag_true);
	tree->SetBranchAddress((which_truth+"_r").c_str(),&r_mag_true);
	tree->SetBranchAddress((which_truth+"_i").c_str(),&i_mag_true);
	tree->SetBranchAddress((which_truth+"_z").c_str(),&z_mag_true);
	if(!bcs)tree->SetBranchAddress((which_truth+"_Y").c_str(),&Y_mag_true);
      }
    }
    else {
      tree->SetBranchAddress("mag_true",&mag_true);
      tree->SetBranchAddress("mag_obs",&mag_obs);
    }
    // do any pre-selection cuts by producing an event list
    string basic_cut;
    if(!bcs) basic_cut="class==\""+type+"\"";
    else basic_cut="classflag=="+int_types[cur_type];


    TEventList *elist=0;

    tree->Draw(">>elist",basic_cut.c_str());
    elist=(TEventList*)gDirectory->Get("elist");

    if(!elist) {
      cout<<"Didn't find any objects with cut: "<<endl;
      cout<<"\t"<<basic_cut<<endl;
      continue;
    }

    vector<double> ra,ra_true,dec,dec_true,diff_ra,diff_dec,vec_x,vec_y;  
    int ii=0;

    double conv=2.777e-4;
    TGraph points_xy;
    
    int N=elist->GetN();
    vector<double> x_pos_vec(N),y_pos_vec(N),RA_vec(N),RA_true_vec(N),
      Dec_vec(N),Dec_true_vec(N),
      g_obs_vec(N),g_true_vec(N),    r_obs_vec(N),r_true_vec(N),
      i_obs_vec(N),i_true_vec(N),    z_obs_vec(N),z_true_vec(N),
      Y_obs_vec(N),Y_true_vec(N),    obs_vec(N),true_vec(N);

    if(N==0) continue;
    
    while(tree->GetEntry(elist->GetEntry(ii))>0) {

      x_pos_vec[ii]=x_pos;
      y_pos_vec[ii]=y_pos;
      RA_vec[ii]=RA/conv;
      RA_true_vec[ii]=RA_true/conv;
      Dec_vec[ii]=Dec/conv;
      Dec_true_vec[ii]=Dec_true/conv;

      
      if(coadd) {
	g_obs_vec[ii]=g_mag;      
	r_obs_vec[ii]=r_mag;
	i_obs_vec[ii]=i_mag;      
	z_obs_vec[ii]=z_mag;
	Y_obs_vec[ii]=Y_mag;

	g_true_vec[ii]=g_mag_true;      
	r_true_vec[ii]=r_mag_true;
	i_true_vec[ii]=i_mag_true;      
	z_true_vec[ii]=z_mag_true;
	Y_true_vec[ii]=Y_mag_true;

      }
      else {
	obs_vec[ii]=mag_obs;      
	true_vec[ii]=mag_true;
      }
      points_xy.SetPoint(ii,x_pos,y_pos);
      ii++;
    }
  

    double x_min,y_min,x_max,y_max;
    if(coadd) {
      if(!bcs) {
	x_min=0,y_min=0,x_max=10000,y_max=10000;
      }
      else {
	x_min=0,y_min=0,x_max=8100,y_max=8100;
      }
    }
    else {
      x_min=0,y_min=0,x_max=2030,y_max=4080;
    }
  

    TH2D *hist_RA=new TH2D(Form("RA_%s",type_label.c_str()),
			   Form(";X (pixel);Y (pixel);< RA_{obs}-RA_{%s} > (arcseconds)",
				which_truth.c_str()),
			   bins,x_min,x_max,bins,y_min,y_max);
    TH2D *hist_Dec=new TH2D(Form("Dec_%s",type_label.c_str()),
			    Form(";X (pixel);Y (pixel);< Dec_{obs}-Dec_{%s} > (arcseconds)",
				 which_truth.c_str()),
			    bins,x_min,x_max,bins,y_min,y_max);
    TH2D *hist_g_mag=new TH2D(Form("g_mag_%s",type_label.c_str()),
			      Form(";X (pixel);Y (pixel);< g_{%s}-g_{%s} >",
				   label.c_str(),which_truth.c_str()),
			      bins,x_min,x_max,bins,y_min,y_max);
    TH2D *hist_r_mag=new TH2D(Form("r_mag_%s",type_label.c_str()),
			      Form(";X (pixel);Y (pixel);< r_{%s}-r_{%s} >",
				   label.c_str(),which_truth.c_str()),
			      bins,x_min,x_max,bins,y_min,y_max);
    TH2D *hist_i_mag=new TH2D(Form("i_mag_%s",type_label.c_str()),
			      Form(";X (pixel);Y (pixel);< i_{%s}-i_{%s} >",
				    label.c_str(),which_truth.c_str()),
			      bins,x_min,x_max,bins,y_min,y_max);
    TH2D *hist_z_mag=new TH2D(Form("z_mag_%s",type_label.c_str()),
			      Form(";X (pixel);Y (pixel);< z_{%s}-z_{%s} >",
				   label.c_str(),which_truth.c_str()),
			      bins,x_min,x_max,bins,y_min,y_max);
    TH2D *hist_Y_mag=new TH2D(Form("Y_mag_%s",type_label.c_str()),
			      Form(";X (pixel);Y (pixel);< Y_{%s}-Y_{%s} >",
				   label.c_str(),which_truth.c_str()),
			      bins,x_min,x_max,bins,y_min,y_max);

    TH2D *hist_mag=new TH2D(Form("mag_%s",type_label.c_str()),
			    Form(";X (pixel);Y (pixel);< %s_{%s}-%s_{%s} >",
				 band.c_str(),label.c_str(),band.c_str(),which_truth.c_str()),
			    bins,x_min,x_max,bins,y_min,y_max);

    vector<TH2D*> hists;


    // Loop over all the bins
    for(int i=0;i<bins;i++) 
      for(int j=0;j<bins;j++) {

	// find the center point
	double hist_x=hist_RA->GetXaxis()->GetBinCenter(i+1);
	double hist_y=hist_RA->GetYaxis()->GetBinCenter(j+1);

	vector<double> distance_vec(N);
	for(int ii=0;ii<N;ii++) {
	  distance_vec[ii]=TMath::Sqrt((x_pos_vec[ii]-hist_x)*(x_pos_vec[ii]-hist_x)+
				       (y_pos_vec[ii]-hist_y)*(y_pos_vec[ii]-hist_y));
	}      

	// Sort by distance
	int *index = new int[N];
	TMath::Sort(N,&distance_vec[0],index,false);

	vector<double> closest_RA(neighbors),closest_Dec(neighbors),
	  closest_g_mag(neighbors),closest_r_mag(neighbors),	
	  closest_i_mag(neighbors),closest_z_mag(neighbors),	
	  closest_Y_mag(neighbors), closest_mag(neighbors);

	for(int ii=0;ii<neighbors; ++ii) {
	  closest_RA[ii]=RA_vec[index[ii]]-RA_true_vec[index[ii]];
	  closest_Dec[ii]=Dec_vec[index[ii]]-Dec_true_vec[index[ii]];
	  if(coadd) {
	    closest_g_mag[ii]=(g_obs_vec[index[ii]]-g_true_vec[index[ii]]);
	    closest_r_mag[ii]=(r_obs_vec[index[ii]]-r_true_vec[index[ii]]);
	    closest_i_mag[ii]=(i_obs_vec[index[ii]]-i_true_vec[index[ii]]);
	    closest_z_mag[ii]=(z_obs_vec[index[ii]]-z_true_vec[index[ii]]);
	    closest_Y_mag[ii]=(Y_obs_vec[index[ii]]-Y_true_vec[index[ii]]);
	  }
	  else {
	    closest_mag[ii]=(obs_vec[index[ii]]-true_vec[index[ii]]);
	  }
	}
        
	double median_RA=TMath::Median(neighbors,&closest_RA[0]);
	double median_Dec=TMath::Median(neighbors,&closest_Dec[0]);

        
	hist_RA->SetBinContent(i+1,j+1,median_RA);
	hist_Dec->SetBinContent(i+1,j+1,median_Dec);

	if(coadd) {
	  double median_mag=TMath::Median(neighbors,&closest_g_mag[0]);
	  hist_g_mag->SetBinContent(i+1,j+1,median_mag);
	
	  median_mag=TMath::Median(neighbors,&closest_r_mag[0]);
	  hist_r_mag->SetBinContent(i+1,j+1,median_mag);
	
	  median_mag=TMath::Median(neighbors,&closest_i_mag[0]);
	  hist_i_mag->SetBinContent(i+1,j+1,median_mag);
	
	  median_mag=TMath::Median(neighbors,&closest_z_mag[0]);
	  hist_z_mag->SetBinContent(i+1,j+1,median_mag);
	
	  median_mag=TMath::Median(neighbors,&closest_Y_mag[0]);
	  hist_Y_mag->SetBinContent(i+1,j+1,median_mag);
	}
	else {
	  double median_mag=TMath::Median(neighbors,&closest_mag[0]);
	  hist_mag->SetBinContent(i+1,j+1,median_mag);
	}

	delete index;
      }
  
    hists.push_back(hist_RA);
    hists.push_back(hist_Dec);
    if(coadd) {
      hists.push_back(hist_g_mag);
      hists.push_back(hist_r_mag);
      hists.push_back(hist_i_mag);
      hists.push_back(hist_z_mag);
      hists.push_back(hist_Y_mag);
    }
    else hists.push_back(hist_mag);
 
//     TFile t("diff_xy.root","Recreate");
//     hist_RA->Write();
//     hist_Dec->Write();
//     if(coadd) {
//       hist_g_mag->Write();
//       hist_r_mag->Write();
//       hist_i_mag->Write();
//       hist_z_mag->Write();
//       hist_Y_mag->Write();
//     }
//     else hist_mag->Write();
//     points_xy.Write("points");
//     t.Close();
  
  
    TLatex *label=new TLatex;
    label->SetNDC();
    label->SetTextSize(0.045);
    label->SetTextFont(42);
    label->SetTextAlign(22);
  
   
    string bands[5]={"g","r","i","z","Y"};
    double min;
    for(int i=0;i<hists.size();i++) {
      if(bcs && i==6) continue;
      string print_title;
      string cur_band="";
      if(i>1 && coadd) cur_band=bands[i-2];
      else if(i>1 && !coadd) cur_band=band;
      

      if(i==0) {
	if(coadd) print_title="RA "+cur_title;
	else  print_title=band+" band RA "+cur_title;
      }
      else if(i==1) {
	if(coadd) print_title="Dec "+cur_title;
	else   print_title=band+" band Dec "+cur_title;
      }
      else {
	print_title=cur_band+" band "+cur_title;
      }
    
      hists[i]->GetXaxis()->CenterTitle();
      hists[i]->GetYaxis()->CenterTitle();
      hists[i]->GetZaxis()->CenterTitle();
      TCanvas c1;
      c1.SetLeftMargin(0.18);
      c1.SetRightMargin(0.22);
      hists[i]->Draw("colz");

      label->DrawLatex(0.49,0.948,print_title.c_str());
      
      string file_label;
      if(i==0)file_label="RA";
      else if(i==1)file_label="Dec";
      else file_label=cur_band;
      if(debug) {
	c1.Update();
	c1.WaitPrimitive();
      }

      c1.Print(Form("%s/%s_%s_diff_full.png",dir.c_str(),
		    file_label.c_str(),type_label.c_str()));
      if(print_points) {
	points_xy.Draw("p");
	c1.Print(Form("%s/%s_%s_diff_full_points.png",dir.c_str(),
		      file_label.c_str(),type_label.c_str()));
      }
      
    
      min=-0.2;
      hists[i]->GetZaxis()->SetRangeUser(-0.2,0.2);
      
      
      for(int ii=1;ii<=hists[i]->GetNbinsX();++ii) {
	for(int j=1;j<=hists[i]->GetNbinsY();++j) {
	  if(hists[i]->GetBinContent(ii,j)<min) {
	    hists[i]->SetBinContent(ii,j,min+0.00001);
	  }
	}
      }
    
      hists[i]->Draw("colz");
      label->DrawLatex(0.49,0.948,print_title.c_str());

      c1.Print(Form("%s/%s_%s_diff_zoom.png",dir.c_str(),
		    file_label.c_str(),type_label.c_str()));
      if(print_points) {
	points_xy.Draw("p");
	c1.Print(Form("%s/%s_%s_diff_zoom_points.png",dir.c_str(),
		      file_label.c_str(),type_label.c_str()));
      }
          
      //ac1.WaitPrimitive();
    }
    delete elist;

  }
 
}


void plot_comp(string inputfile,string truthfile,double tol,double mag_max,
               string title,string dir,
	       string which_band,
	       string which_mag,string which_truth,
               bool coadd,bool bcs,
               bool debug,double err_cut)
{


  

  string bands[5]={"g","r","i","z","Y"};
  string types[2]={"S","G"};
  string int_types[2]={"2","1"};
  string type_labels[2]={"stars","galaxies"};

  string band,type,type_label;

  TCanvas c1;
  
  // Set style

  // create tree to hold variables
  string branch=get_branches(coadd,which_band,bcs);
  TTree *tree=new TTree("tree","");
  tree->ReadFile(inputfile.c_str(),branch.c_str());

  TTree *truth_tree=new TTree("truth_tree","");
  string true_branches=get_truthbranches(coadd,which_band,bcs);
  truth_tree->ReadFile(truthfile.c_str(),true_branches.c_str());
  vector<TH1D*> hist_comp,hist_pur;
  for(int cur_band=0;cur_band<5;cur_band++) {
    band=bands[cur_band];
    if(debug) cout<<"\nband: "<<band<<endl;
    if(which_band=="all" || which_band==band) {
      double obs_mag,true_mag,mag_err,ra,dec,star_class;
      string stargal;
      char tband[10];
      string stband;

      tree->ResetBranchAddresses();
      if(!coadd) {
        
        tree->SetBranchAddress("mag_obs",&obs_mag);
        tree->SetBranchAddress("mag_true",&true_mag);
        tree->SetBranchAddress("mag_obs_err",&mag_err);
        tree->SetBranchAddress("band",&tband);
        //tree->SetBranchAddress("class",&star_class);
        tree->SetBranchAddress("RA",&ra);
        tree->SetBranchAddress("Dec",&dec);
      }
      else {
        
        tree->SetBranchAddress(Form("%s_mag_obs",band.c_str()),&obs_mag);
        tree->SetBranchAddress(Form("%s_true",band.c_str()),&true_mag);
        tree->SetBranchAddress(Form("%s_obs_err",band.c_str()),&mag_err);
        //tree->SetBranchAddress("class",&star_class);
        sprintf(tband,"%s",band.c_str());
        tree->SetBranchAddress("RA",&ra);
        tree->SetBranchAddress("Dec",&dec);
      }
      
      truth_tree->ResetBranchAddresses();
      truth_tree->SetBranchAddress((band+"_mag").c_str(),&true_mag);
      truth_tree->SetBranchAddress("RA_true",&ra);
      truth_tree->SetBranchAddress("Dec_true",&dec);
      
      
      vector<Object> matched,true_vec;
      KD_Tree true_all,matched_all;
      int i=0;

      while(tree->GetEntry(i)) {
        i++;
        stband=tband;

        if(stband!=band) continue;

        Object ob(ra,dec,obs_mag,mag_err,band,0,0);
        ob.class_star=star_class;
        ob.mjd=true_mag;
        
        double cd=cos(ob.Dec*TMath::DegToRad());
        ob.xyz[0]=cos(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[1]=sin(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[2]=sin(ob.Dec*TMath::DegToRad());
        if(mag_err<err_cut) {
          matched.push_back(ob);
          matched_all.insert(ob);
        }
      }
      
      if(debug)cout<<"Found "<<matched.size()<<" objects"<<endl;
        
      double max_mag=-1e6,min_mag=1e6;
      i=0;
      while(truth_tree->GetEntry(i)) {
        i++;
        if(true_mag>max_mag) max_mag=true_mag;
        if(true_mag<min_mag) min_mag=true_mag;
        Object ob(ra,dec,true_mag,0,band,0,0);
        ob.class_star=star_class;
        double cd=cos(ob.Dec*TMath::DegToRad());
        ob.xyz[0]=cos(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[1]=sin(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[2]=sin(ob.Dec*TMath::DegToRad());
        true_all.insert(ob);
        true_vec.push_back(ob);
      }

      if(debug) cout<<"Found "<<true_vec.size()<<" truth objects"<<endl;

       
       TH1D *hist_all=new TH1D(Form("hist_all_%s",band.c_str())
                               ,"",20,min_mag,max_mag);
       TH1D *hist_allmatch=new TH1D(Form("hist_allmatch_%s",band.c_str())
                                    ,"",20,min_mag,max_mag);
       TH1D *hist_match=new TH1D(Form("hist_match_%s",band.c_str())
                                 ,"",20,min_mag,max_mag);
       TH1D *hist_nomatch=new TH1D(Form("hist_nomatch_%s",band.c_str())
                                   ,"",20,min_mag,max_mag);
       
       
       // loop over truth objects and try to match a matched object
       // use photometric flag to tell if it is matched
       vector<Object>::iterator obj_iter=true_vec.begin();
       
       for(; obj_iter!=true_vec.end();obj_iter++) {
         
         // find closest match within tolerance
         KD_Match match=matched_all.find_nearest(*obj_iter,tol);
         
         if(match.first!=matched_all.end()) {
           
           // check that difference between truth and matched
           // magnitude is within tolerance otherwise 
           // do not count it as a match
           if(abs(match.first->mag-match.first->mjd)<mag_max) {
             obj_iter->photometric=1;
           }
           else obj_iter->photometric=0;
         }
         else obj_iter->photometric=0;
       }
       
       // now we need to loop over matched objects to find which
       // ones have no corresponding truth match
       for(obj_iter=matched.begin();obj_iter!=matched.end();obj_iter++) {
         
         KD_Match match=true_all.find_nearest(*obj_iter,tol);

         if(match.first!=true_all.end()) {
           if(abs(obj_iter->mag-obj_iter->mjd)<mag_max) {
             obj_iter->photometric=1;
           }
           else   obj_iter->photometric=0;
         }
         else obj_iter->photometric=0;
       }
       
       for(obj_iter=true_vec.begin();obj_iter!=true_vec.end();obj_iter++) {
         
         if(obj_iter->photometric) hist_match->Fill(obj_iter->mag); 
         //else hist_nomatch->Fill(obj_iter->mag);
         hist_all->Fill(obj_iter->mag);
         
       }
       
       for(obj_iter=matched.begin();obj_iter!=matched.end();obj_iter++) {
         
         if(!obj_iter->photometric) hist_nomatch->Fill(obj_iter->mag); 
         hist_allmatch->Fill(obj_iter->mag);
       }
       
       hist_nomatch->Divide(hist_nomatch,hist_allmatch,1,1,"B");
       hist_match->Divide(hist_all);
       
       if(debug) {
         hist_match->GetYaxis()->SetRangeUser(0,1.05);
         hist_match->Draw("");
         hist_nomatch->Draw("same");
         hist_nomatch->SetLineColor(kRed);
         // c1.Update();
//          c1.WaitPrimitive();
       }
       hist_comp.push_back(hist_match);
       hist_pur.push_back(hist_nomatch);
     }
  }


  TCanvas c3;
  
  //TLegend *leg=new TLegend(0.62,0.66,0.90,0.88);
  TLegend *leg=new TLegend(0.17,0.77,0.45,0.87);
  leg->SetTextFont(42);
  leg->SetTextSize(0.04);
  leg->SetBorderSize(0);
  leg->SetNColumns(3);
  leg->SetColumnSeparation(1.08);

  for(int i=0;i<hist_comp.size();i++) {
    leg->AddEntry(hist_comp[i],(bands[i]+" band").c_str(),"l");
    hist_comp[i]->SetLineColor(i+1);
    hist_pur[i]->SetLineColor(i+1);
    if(i==0) {
      if(coadd || which_band=="all")hist_comp[i]->GetYaxis()->SetRangeUser(0,1.2);
      else hist_comp[i]->GetYaxis()->SetRangeUser(0,1.05);
      hist_comp[i]->Draw();
    }
    else {
      hist_comp[i]->Draw("same");
    }
  }

  if(coadd || which_band=="all")leg->Draw();
  
  hist_comp[0]->GetXaxis()->CenterTitle();
      

  if(!coadd && which_band!="all") {
    hist_comp[0]->SetXTitle(Form("%s_{%s}",which_band.c_str(),
                                 which_truth.c_str()));
  }
  else {
    hist_comp[0]->SetXTitle(Form("band_{%s}",which_truth.c_str()));

  }

  TLatex *label=new TLatex;
  label->SetNDC();
  label->SetTextSize(0.045);
  label->SetTextFont(42);
  label->SetTextAlign(22);
  
  label->DrawLatex(0.32,0.94,"Completeness");

  if(debug) c3.WaitPrimitive();
  c3.Print("completeness.png");


  TLegend *leg2=new TLegend(0.17,0.77,0.45,0.87);
  leg2->SetTextFont(42);
  leg2->SetTextSize(0.04);
  leg2->SetBorderSize(0);
  leg2->SetNColumns(3);
  leg2->SetColumnSeparation(1.08);
  c3.Clear();

  for(int i=0;i<hist_pur.size();i++) {
    leg2->AddEntry(hist_pur[i],(bands[i]+" band").c_str(),"l");
    hist_pur[i]->SetLineColor(i+1);
    if(i==0) {
      
      if(coadd || which_band=="all")hist_pur[i]->GetYaxis()->SetRangeUser(0,1.2);
      else hist_pur[i]->GetYaxis()->SetRangeUser(0,1.05);
      hist_pur[i]->Draw();
    }
    else {
      hist_pur[i]->Draw("same");
    }
  }

  if(coadd || which_band=="all")leg2->Draw();
  
  hist_pur[0]->GetXaxis()->CenterTitle();
      

  if(!coadd && which_band!="all") {
    hist_pur[0]->SetXTitle(Form("%s_{%s}",which_band.c_str(),
                                which_truth.c_str()));
  }
  else {
    hist_pur[0]->SetXTitle(Form("band_{%s}",which_truth.c_str()));

  }

  label->DrawLatex(0.32,0.94,"Contamination");
  c3.Update();
  if(debug)c3.WaitPrimitive();
  c3.Print("contamination.png");
  
}








void LoadStyle()
{


gROOT->SetStyle("Plain");
gStyle->SetPalette(1);
 TStyle* miStyle = new  TStyle("miStyle", "MI Style");

 // Colors


//  set the background color to white
  miStyle->SetFillColor(10);


  miStyle->SetFrameFillColor(10);
  miStyle->SetCanvasColor(10);
  miStyle->SetCanvasDefH(680);
  miStyle->SetCanvasDefW(700);
  miStyle->SetPadColor(10);
  miStyle->SetTitleFillColor(0);
  miStyle->SetStatColor(10);

//  //dont put a colored frame around the plots
 miStyle->SetFrameBorderMode(0);
 miStyle->SetCanvasBorderMode(0);
 miStyle->SetPadBorderMode(0);

 //use the primary color palette
 miStyle->SetPalette(1);

 //set the default line color for a histogram to be black
  miStyle->SetHistLineColor(kBlack);

 //set the default line color for a fit function to be red
 miStyle->SetFuncColor(kBlue);

 //make the axis labels black
 miStyle->SetLabelColor(kBlack,"xyz");

 //set the default title color to be black
 miStyle->SetTitleColor(kBlack);

 // Sizes
 miStyle->SetHistLineWidth(3);
 miStyle->SetFrameLineWidth(3);
 miStyle->SetFuncWidth(2);


 //set the margins
 miStyle->SetPadBottomMargin(0.12);
 miStyle->SetPadTopMargin(0.1);
 miStyle->SetPadLeftMargin(0.14);
 miStyle->SetPadRightMargin(0.14);

 //set axis label and title text sizes
 miStyle->SetLabelSize(0.04,"x");
 miStyle->SetLabelSize(0.04,"y");
 miStyle->SetTitleSize(0.05,"xyz");
 miStyle->SetTitleOffset(1.1,"x");
 miStyle->SetTitleOffset(1.3,"yz");
 miStyle->SetLabelOffset(0.012,"y");
 miStyle->SetStatFontSize(0.025);
 miStyle->SetTextSize(0.02);
 miStyle->SetTitleBorderSize(0);

 //set line widths
 miStyle->SetHistLineWidth(3);
 miStyle->SetFrameLineWidth(3);
 miStyle->SetFuncWidth(2);

 // Misc

 //align the titles to be centered
 miStyle->SetTextAlign(22);

 //set the number of divisions to show
 // miStyle->SetNdivisions(506, "xy");

 //turn off xy grids
 miStyle->SetPadGridX(0);
 miStyle->SetPadGridY(0);

 //set the tick mark style
 miStyle->SetPadTickX(1);
 miStyle->SetPadTickY(1);

 //show the fit parameters in a box
 miStyle->SetOptFit(0111);
miStyle->SetOptTitle(0);

 //turn off all other stats
 miStyle->SetOptStat(0);
 miStyle->SetStatW(0.20);
 miStyle->SetStatH(0.15);
 miStyle->SetStatX(0.94);
 miStyle->SetStatY(0.92);


 miStyle->SetFillStyle(0);

 //  // Fonts
 miStyle->SetStatFont(42);
 miStyle->SetLabelFont(42,"xyz");
 miStyle->SetTitleFont(42,"xyz");
// miStyle->SetTextFont(40);

 //done

 miStyle->cd();


// gROOT->ForceStyle(1);
 

}



void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol)
{
  int N=array.size();
  mean=TMath::Mean(N,&array[0]);
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }

  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;
  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();

    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;
    
  }

  

  
  return;

}

