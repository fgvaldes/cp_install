#ifndef COLORPOINT_H
#define COLORPOINT_H

#include <kdtree++/kdtree.hpp>
using namespace std;


class ColorPoint{
public:

  // needed for kdtree
  typedef double value_type;

  ColorPoint(double _c1,double _c2,double _iz):
    c1(_c1),c2(_c2),c3(_iz)
  { d[0]=c1;d[1]=c2;d[2]=c3; }

  ColorPoint(double _c1,double _c2,double _iz,
             double _c1_err,double _c2_err,double _iz_err):
    c1(_c1),c2(_c2),c3(_iz),
    c1_err(_c1_err),c2_err(_c2_err),c3_err(_iz_err) 
  { d[0]=c1;d[1]=c2;d[2]=c3; }

  ColorPoint(): c1(-999),c2(-999),c3(-999),
                c1_err(0.),c2_err(0.),c3_err(0.) {}

  ColorPoint(const ColorPoint &p)
  {
    c1=p.c1;
    c2=p.c2;
    c3=p.c3;
    c1_err=p.c1_err;
    c2_err=p.c2_err;
    c3_err=p.c3_err;
    d[0]=p.c1;
    d[1]=p.c2;
    d[2]=p.c3;
  }   
  
  double c1;
  double c2;
  double c3;
  double c1_err;
  double c2_err;
  double c3_err;
  
  double d[3];

  inline double operator[](int const N) const { return d[N]; }
  
  friend ostream& operator<< (ostream &out, ColorPoint const &p);
  


};



typedef KDTree::
KDTree<3, ColorPoint> ColorPoint_Tree; 

typedef std::pair<ColorPoint_Tree::const_iterator,double> ColorPoint_Match;


class ColorPoint2D{
public:

  // needed for kdtree
  typedef double value_type;

  ColorPoint2D(double _c1,double _c2):
    c1(_c1),c2(_c2)
  { d[0]=c1;d[1]=c2;}

  ColorPoint2D(double _c1,double _c2,
               double _c1_err,double _c2_err):
    c1(_c1),c2(_c2),
    c1_err(_c1_err),c2_err(_c2_err)
  { d[0]=c1;d[1]=c2;}

  ColorPoint2D(): c1(-999), c2(-999),
                  c1_err(0.),c2_err(0.) {}

  ColorPoint2D(const ColorPoint2D &p)
  {
    c1=p.c1;
    c2=p.c2;
    c1_err=p.c1_err;
    c2_err=p.c2_err;

    d[0]=p.c1;
    d[1]=p.c2;
  }   
  
  double c1;
  double c2;
  double c1_err;
  double c2_err;

  
  double d[2];

  inline double operator[](int const N) const { return d[N]; }
  
  friend ostream& operator<< (ostream &out, ColorPoint2D const &p);
  


};



typedef KDTree::
KDTree<3, ColorPoint2D> ColorPoint2D_Tree; 

typedef std::pair<ColorPoint2D_Tree::const_iterator,double> ColorPoint2D_Match;




class ColorPoint4D{
public:

  // needed for kdtree
  typedef double value_type;

  ColorPoint4D(double _c1,double _c2,
               double _c3,double _c4):
    c1(_c1),c2(_c2),c3(_c3),c4(_c4)
  { d[0]=c1;d[1]=c2;d[2]=c3;d[3]=c4;}

  ColorPoint4D(double _c1,double _c2,
               double _c3,double _c4,
               double _c1_err,double _c2_err,
               double _c3_err,double _c4_err):
    c1(_c1),c2(_c2), c3(_c3),c4(_c4),
    c1_err(_c1_err),c2_err(_c2_err),
    c3_err(_c3_err),c4_err(_c4_err)
  { d[0]=c1;d[1]=c2;d[2]=c3;d[3]=c4;}

  ColorPoint4D(): c1(-999), c2(-999),
                  c3(-999), c4(-999),
                  c1_err(0.),c2_err(0.),
                  c3_err(0.),c4_err(0.) {}

  ColorPoint4D(const ColorPoint4D &p)
  {
    c1=p.c1;
    c2=p.c2;
    c3=p.c3;
    c4=p.c4;
    c1_err=p.c1_err;
    c2_err=p.c2_err;
    c3_err=p.c3_err;
    c4_err=p.c4_err;

    d[0]=p.d[0];
    d[1]=p.d[1];
    d[2]=p.d[2];
    d[3]=p.d[3];
  }   
  
  double c1;
  double c2;
  double c3;
  double c4;
  double c1_err;
  double c2_err;
  double c3_err;
  double c4_err;

  
  double d[4];

  inline double operator[](int const N) const { return d[N]; }
  
  friend ostream& operator<< (ostream &out, ColorPoint4D const &p);
  


};



typedef KDTree::
KDTree<4, ColorPoint4D> ColorPoint4D_Tree; 

typedef std::pair<ColorPoint4D_Tree::const_iterator,double> ColorPoint4D_Match;



#endif
