#ifndef OBJECT_H
#define OBJECT_H
#include <string>
#include <vector>
#include "TObject.h"
#include <cstring>

using namespace std;

class Object:public TObject
{
 public:

  typedef double value_type;
  Object(double _RA,double _Dec,double _mag, double _mag_err,string _band,
         long _id, int _image_id): RA(_RA),Dec(_Dec),mag(_mag),
                                  mag_err(_mag_err),id(_id),
                                  band(_band),image_id(_image_id) {}
  Object() {}
  
  
  Object(const Object &ob)
  { 
    RA=ob.RA;
    RA_err=ob.RA_err;
    Dec=ob.Dec;
    Dec_err=ob.Dec_err;
    mag=ob.mag;
    mag_err=ob.mag_err;
    id=ob.id;
    band=ob.band;
    strcpy(cband,ob.cband);
    image_id=ob.image_id;
    photometric=ob.photometric;
    xyz[0]=ob.xyz[0];
    xyz[1]=ob.xyz[1];
    xyz[2]=ob.xyz[2];
    class_star=ob.class_star;
    flag=ob.flag;
    mjd=ob.mjd;
    mura=ob.mura;
    mudec=ob.mudec;
    x_image=ob.x_image;
    y_image=ob.y_image;
    strcpy(cid,ob.cid);

  }
  bool operator==(const Object& other) const {
    if(RA==other.RA &&
       Dec==other.Dec &&
       mag==other.mag 
       ) return true;
    else return false;
  }
  
  ~Object() {}
  
  double RA;
  double RA_err;
  double Dec;
  double Dec_err;
  double mag;
  double mag_err;
  double class_star;
  int    flag;
  long id;
  string band;
  char cband[5];
  int image_id;
  bool photometric;
  double xyz[3];
  double mjd;
  double mura;
  double mudec;
  double x_image;
  double y_image;
  char cid[30];

  inline double operator[](int const N) const {return xyz[N];}
  ClassDef(Object,2);
};
  

class CoaddObject:public TObject
{

public:
  
  
  typedef double value_type;

  CoaddObject(int _coadd_id,double _RA,double _Dec,
              double _g_mag, double _g_mag_err,
              double _r_mag, double _r_mag_err,
              double _i_mag, double _i_mag_err,
              double _z_mag, double _z_mag_err, 
              double _Y_mag, double _Y_mag_err):
    RA(_RA),Dec(_Dec),//median(true),
    g_mag(_g_mag),g_mag_err(_g_mag_err),
    r_mag(_r_mag),r_mag_err(_r_mag_err),
    i_mag(_i_mag),i_mag_err(_i_mag_err),
    z_mag(_z_mag),z_mag_err(_z_mag_err),
    Y_mag(_Y_mag),Y_mag_err(_Y_mag_err),coadd_id(_coadd_id) {}

  CoaddObject() {}

  CoaddObject(const CoaddObject &ob)
  { 
     RA=ob.RA;
     Dec=ob.Dec;
     g_mag=ob.g_mag;
     g_mag_err=ob.g_mag_err;
     r_mag=ob.r_mag;
     r_mag_err=ob.r_mag_err;
     i_mag=ob.i_mag;
     i_mag_err=ob.i_mag_err;
     z_mag=ob.z_mag;
     z_mag_err=ob.z_mag_err;
     
     g_class_star=ob.g_class_star;
     r_class_star=ob.r_class_star;
     i_class_star=ob.i_class_star;
     z_class_star=ob.z_class_star;
     
     g_flag=ob.g_flag;
     r_flag=ob.r_flag;
     i_flag=ob.i_flag;
     z_flag=ob.z_flag;
     
     
     xyz[0]=ob.xyz[0];
     xyz[1]=ob.xyz[1];
     xyz[2]=ob.xyz[2];
     
     
     // copy(ob.g_band.begin(),ob.g_band.end(),
     //          back_inserter(g_band));
     //     copy(ob.r_band.begin(),ob.r_band.end(),
     //          back_inserter(r_band));
     //     copy(ob.i_band.begin(),ob.i_band.end(),
     //          back_inserter(i_band));
     //     copy(ob.z_band.begin(),ob.z_band.end(),
     //          back_inserter(z_band));
     
    //  copy(ob.se_objs.begin(),ob.se_objs.end(),
//           back_inserter(se_objs));
     se_objs.clear();
     se_objs=ob.se_objs;
    
    
  }
  
  ~CoaddObject() {}
  

  double RA;
  double Dec;
  
  double g_mag;
  double r_mag;
  double i_mag;
  double z_mag;
  double Y_mag;

  double g_mag_err;
  double r_mag_err;
  double i_mag_err;
  double z_mag_err;
  double Y_mag_err;

  double g_class_star;
  double r_class_star;
  double i_class_star;
  double z_class_star;
  double Y_class_star;


  double g_flag;
  double r_flag;
  double i_flag;
  double z_flag;
  double Y_flag;

  int coadd_id;

  vector<Object> se_objs;
 //  vector<Object> r_band;
//   vector<Object> i_band;
//   vector<Object> z_band;
//   vector<Object> Y_band;

  // double AverageG();
//   double AverageR();
//   double AverageI(); 
//   double AverageZ();
//   double AverageY();

//   double AveragePhotoG();
//   double AveragePhotoR();
//   double AveragePhotoI();
//   double AveragePhotoZ();
//   double AveragePhotoY();

  inline double operator[](int const N) const {return xyz[N];}  
  double xyz[3];
  //void UseMedian(bool val) { median=val;}

  private:
  
 //  double AverageMag(const vector<Object> &vec);
//   double AveragePhotoMag(const vector<Object> &vec);
  //bool median;
  //bool photo_median;
  ClassDef(CoaddObject,1);  

};

  
class ImageInfo
{
 public:
  ImageInfo() {}
  ~ImageInfo() {}
  ImageInfo(int _id,string _exp_name,double _exp_time,
            int _ccd,int _nite,string _run,double _fwhm, 
            double _skybrite):
    id(_id),exp_name(_exp_name),
    exp_time(_exp_time),ccd(_ccd), nite(_nite),
    run(_run), fwhm(_fwhm), skybrite(_skybrite),n_objects(0) {}

  int id;
  int parentid;
  string exp_name;
  double exp_time;
  int ccd;
  int nite;

  string run;
  double fwhm;
  double skybrite;
  //double zp_db;
  //double gaina;
  //double gainb;
  double true_zp;
 
  bool photometric;
  string band;
  double zp;
  double zp_err;
  double coadd_zp;
  double coadd_zp_err;
  double slr_zp;
  double slr_zp_err;
  int n_objects;
  vector<int> match_ids;
  double max_RA;
  double min_RA;
  double max_Dec;
  double min_Dec;
  vector<Object> objs;
  
  friend ostream& operator<< (ostream &out, ImageInfo const &p);  

};
#endif
