/*
**
** coadd_printimage.c
**
** DESCRIPTION:
**     Input the ID from varios coadd_* subroutines and output the imagename/full image path from DB query
**     
**
** Last commit:
**     $Rev$
**     $LastChangedBy$
**     $LastChangedDate$
**
*/

#include "imageproc.h"

main(argc,argv)
     int argc;
     char *argv[];
{
  char inputfile[1000],modname[100],output[1000],tilename[100];
  char event[1000],line[1000],command[1000],temp[100];
  char dbcall[1000],dblogin[1000],sqlcall[1000],sqlHEAD[1000];
  char **archivesite,**archivepath,**project;

  int i,j,N,*ID,*archiveid;
  int flag_verbose=2,flag_input,flag_archive=0,flag_tilename=0;

  float *ZP,*ZPerr;

  db_tiles      *tileinfo;
  
  /* external functions call */
  void select_dblogin(),reportevt(),get_input();

  FILE *pip,*fin,*fsqlout;

  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <optional inputs>\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -input [coadd_subroutine] <ID file>    [coadd_subroutine] is the name after \"coadd_\", eg. -selectimage if using the output file from coadd_selectimage (available subroutine name: selectimage,photozp,solvezp) \n");
    printf("    Optional Inputs:\n");
    printf("       -archive                               Printout the full path of the images for given archive\n");
    printf("       -tilename <tilename>                   Include the tilename in output images (i.e., remap image)\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }
  
  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    /* required input */
    if (!strcmp(argv[i],"-input")) {
      flag_input=1;
      sprintf(modname,"%s",argv[i+1]);
      
      sprintf(output,"%s",modname);
      if (!strncmp(&output[0],"-",1)) {
        sprintf(event,"Wrong input for -input option ");
        reportevt(flag_verbose,STATUS,5,event);
        exit(0);
      }
      
      sprintf(inputfile,"%s",argv[i+2]);
      sprintf(output,"%s",inputfile);
      if (!strncmp(&output[0],"-",1)) {
        sprintf(event,"Wrong input for -input option ");
        reportevt(flag_verbose,STATUS,5,event);
        exit(0);
      }
      
    }


    /* optional inputs */
    if (!strcmp(argv[i],"-archive")) 
      flag_archive=1;
    
    if (!strcmp(argv[i],"-tilename")) {
      flag_tilename=1;
      get_input(argv[i],argv[i+1],"-tilename",tilename,flag_verbose);
    }
  }

  /* quit if -input is not set */
  if(!flag_input) {
    sprintf(event,"Required inputs are not set ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }

  /* ** !! ALSO: starmatch, skybrite ** */
  /* check if the subroutine is correct */
  if(strcmp(modname,"selectimage") && strcmp(modname,"photozp") && strcmp(modname,"solvezp")) {
    sprintf(event,"Input [coadd_subroutine] is not one of the following: selectimage,photozp,solvezp ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }


  /* *** Setup DB login and make generic sqlcall *** */
  select_dblogin(dblogin,DB_READWRITE);
  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < temp.sql ",dblogin);
  sprintf(sqlHEAD,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;");
  
  
  /* *** Find out the number of lines from input file *** */
  sprintf(command,"wc -l %s",inputfile);
  pip=popen(command,"r");
  fscanf(pip,"%d",&N);
  pclose(pip);

  sprintf(event,"Number of images in %s = %d",inputfile,N);
  reportevt(flag_verbose,STATUS,2,event);

  /* memory allocation */
  tileinfo=(db_tiles *)calloc(N+1,sizeof(db_tiles));
  ID=(int *)calloc(N+1,sizeof(int));
  ZP=(float *)calloc(N+1,sizeof(float));
  ZPerr=(float *)calloc(N+1,sizeof(float));

  if(flag_archive) {
    archivesite=(char **)calloc(N+1,sizeof(char *));
    archivepath=(char **)calloc(N+1,sizeof(char *));
    project=(char **)calloc(N+1,sizeof(char *));
    for(i=1;i<=N;i++) {
      archivesite[i]=(char *)calloc(100,sizeof(char));
      archivepath[i]=(char *)calloc(100,sizeof(char));
      project[i]=(char *)calloc(10,sizeof(char));
    }
    archiveid=(int *)calloc(N+1,sizeof(int));
  }


  /* *** Input the ID (and ZP ZPerr) from input file *** */
  fin=fopen(inputfile,"r");
  for(i=1;i<=N;i++) {
    
    /* input the ID and ZP info based on the modname (subroutine) */
    if(!strcmp(modname,"selectimage")) 
      fscanf(fin,"%d",&ID[i]);
    if(!strcmp(modname,"photozp") || !strcmp(modname,"solvezp")) 
      fscanf(fin,"%d %f %f",&ID[i],&ZP[i],&ZPerr[i]);
  }
  fclose(fin);

  /* *** Find out the number of tiles *** */
  fsqlout=fopen("temp.sql", "w");
  fprintf(fsqlout,"%s\n",sqlHEAD);
  for(i=1;i<=N;i++) {
    fprintf(fsqlout,"select RUN,CCD,IMAGENAME,BAND ");
    fprintf(fsqlout,"from image where ");
    fprintf(fsqlout,"ID=%d ",ID[i]);
    fprintf(fsqlout,";\n");
  }
  fprintf(fsqlout,"exit;\n");
  fclose(fsqlout);

  /* input the result from query */
  pip=popen(sqlcall,"r");
  for(i=1;i<=N;i++) {
    fgets(line,1000,pip);
    sscanf(line,"%s %d %s %s ",tileinfo[i].runiddesc,&(tileinfo[i].ccdnum),tileinfo[i].imagename,output);
    sscanf(output,"%s %s",tileinfo[i].band,temp);
  }
  pclose(pip);

  /* *** Get the archive dir *** */
  if(flag_archive) {

    /* query the archive string */
    fsqlout=fopen("temp.sql", "w");
    fprintf(fsqlout,"%s\n",sqlHEAD);
    for(i=1;i<=N;i++) {
      fprintf(fsqlout,"select ARCHIVESITES,PROJECT ");
      fprintf(fsqlout,"from location where ");
      fprintf(fsqlout,"ID=%d ",ID[i]);
      fprintf(fsqlout,";\n");
    }
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);
    
    pip=popen(sqlcall,"r");
    for(i=1;i<=N;i++) {
      fgets(line,1000,pip);
      sscanf(line,"%s %s",archivesite[i],project[i]);
    }
    pclose(pip);

    /* find out the archive id */
    for(i=1;i<=N;i++) {
      sprintf(temp,"%s",archivesite[i]);
      for(j=0;j<strlen(temp);j++) {
	if(!strncmp(&(temp[j]),"Y",1))
	  archiveid[i]=j+1;
      }
    }
      
    /* find out the archive sites */
    fsqlout=fopen("temp.sql", "w");
    fprintf(fsqlout,"%s\n",sqlHEAD);
    for(i=1;i<=N;i++) {
      fprintf(fsqlout,"select ARCHIVE_ROOT ");
      fprintf(fsqlout,"from archive_sites where ");
      fprintf(fsqlout,"LOCATION_ID=%d ",archiveid[i]);
      fprintf(fsqlout,";\n");
    }
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);
    
    pip=popen(sqlcall,"r");
    for(i=1;i<=N;i++) {
      fgets(line,1000,pip);
      sscanf(line,"%s",archivepath[i]);
    }
    pclose(pip);

  }

  /* *** Output the results *** */
  for(i=1;i<=N;i++) {
    if(flag_archive) {
	 
      if(!flag_tilename)
	printf("%d\t%s/%s/red/%s/red/%s/%s_%02d.fits",ID[i],archivepath[i],project[i],tileinfo[i].runiddesc,tileinfo[i].imagename,tileinfo[i].imagename,tileinfo[i].ccdnum);
      else /* add the ID for remap (not the reduced images in ID[i]) later, the i is placeholder */
	printf("%d\t%s/%s/red/%s/remap/%s/%s_%02d_%s.fits",i,archivepath[i],project[i],tileinfo[i].runiddesc,tileinfo[i].imagename,tileinfo[i].imagename,tileinfo[i].ccdnum,tilename);
    }
    else 
      printf("%d\t%s\t%s\t%d\t%s\t",ID[i],tileinfo[i].runiddesc,tileinfo[i].band,tileinfo[i].ccdnum,tileinfo[i].imagename); 
    
    if(!strcmp(modname,"photozp") || !strcmp(modname,"solvezp")) 
      printf("%2.4f %2.4f",ZP[i],ZPerr[i]);
    printf("\n");
  }

  /* *** Clean up *** */
  system ("rm tem*");

  /* free memory */
  free(tileinfo); free(ID); free(ZP); free(ZPerr);
  if(flag_archive) {
    free(archivepath); free(archiveid); free(project);
  }
}
