#if 0
#include "fitsio.h"
#endif 
#include <string.h>
#include <stdio.h>
#include "imutils.h"

#define MAXFILE 500

static const char *svn_id = "$Id: fitscombine.c 9062 2012-10-04 22:02:58Z rgruendl $";

int main(argc,argv)
	int argc;
	char *argv[];
{
  	char 	infile[MAXFILE][500],outfile[500],command[1000],listname[500],
		event[1000];
  	int  	status=0, hdunum, hdutype,morekey=1,nfiles,i,j,hdustart,
		flag_verbose=2,flag_cleanup=0,flag_list=0,flag_no0exthead=0;
	FILE	*inp;
  	fitsfile *infptr,*outfptr;
	void	reportevt();

  	if (argc<4) {
      	  printf("fitscombine <file1.fits> ... <fileN.fits> <output.fits>\n");
	  printf("  -list <name>\n");
	  printf("  -no0exthead\n");
	  printf("  -cleanup\n");
	  printf("  -verbose <0-3>\n");
      	  exit(0);
    	}
	 
	if (argc>=MAXFILE) {
	  printf(" ** fitscombine:  MAXFILE exceeded\n");
	  exit(0);
	}

	/* process command line */
	nfiles=0;
	for (i=1;i<argc;i++) {
          if (!strcmp(argv[i],"-verbose")) {
            sscanf(argv[++i],"%d",&flag_verbose);
            if (flag_verbose<0 || flag_verbose>3) {
              sprintf(event,"Verbose level out of range %d. Reset to 2",
                flag_verbose);
              flag_verbose=2;
              reportevt(flag_verbose,STATUS,3,event);
            }
          }
	  if (!strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5)) {
	    /* create room for and store name */
	    sprintf(infile[nfiles],"%s",argv[i]);
	    nfiles++;
	  }
	  if (!strcmp(argv[i],"-list")) {
		flag_list=1;
	        i++;
		sscanf(argv[i],"%s",listname);
	  }
	  if (!strcmp(argv[i],"-no0exthead")) flag_no0exthead=1;
	  if (!strcmp(argv[i],"-cleanup")) flag_cleanup=1;
	}


	sprintf(outfile,"!%s",infile[nfiles-1]);

	/* if a list then read in filenames */
	if (flag_list) {
	  inp=fopen(listname,"r");
	  if (inp==NULL) {
	    sprintf(event,"File open failed: %s",listname);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	  nfiles=0;
	  while (fscanf(inp,"%s",infile+nfiles)!=EOF) {
	    nfiles++;
	    if (nfiles>=MAXFILE) {
	      sprintf(event,"MAXFILE (%d)  exceeded",MAXFILE);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	  }
	  nfiles++;
	  fclose(inp);
	}

	sprintf(event,"Combining %d FITS files into %s",nfiles-1,outfile+1);
	reportevt(flag_verbose,STATUS,1,event);

	/* open output file */
  	if (fits_create_file(&outfptr,outfile,&status)) {
	  sprintf(event,"Fits create failed: %s",outfile+1);
	  reportevt(flag_verbose,STATUS,5,event);
	  printerror(status);
	} 
	/* cycle through files */
	for (i=0;i<nfiles-1;i++) {
	  /* open fits files */
   	  if (fits_open_file(&infptr,infile[i],READONLY,&status)) {
	      sprintf(event,"Fits open failed: %s",infile[i]);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
  	    /* Get the HDU number */
  	    if (fits_get_num_hdus(infptr,&hdunum,&status)) {
	      sprintf(event,"Fits gethudnum failed: %s",infile[i]);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
  	    sprintf(event,"%s has %d HDU",infile[i],hdunum);
	    reportevt(flag_verbose,STATUS,1,event);
	    if (flag_no0exthead) hdustart=2;
	    else hdustart=1;
	    for (j=hdustart;j<=hdunum;j++) {
	      if (fits_movabs_hdu(infptr,j,&hdutype,&status)) {
	        sprintf(event,"Fits movabs failed: %s",infile[i]);
	        reportevt(flag_verbose,STATUS,5,event);
	        printerror(status);
	      }
  	      /* copy the HDU */
  	      if (fits_copy_hdu(infptr,outfptr,morekey,&status)) {
	        sprintf(event,"Fits hducopy failed: %s",infile[i]);
	        reportevt(flag_verbose,STATUS,5,event);
	        printerror(status);
	      }
	    }
	  /* close input file ptr */
	  if (fits_close_file(infptr,&status)) {
	    sprintf(event,"Fits close failed: %s",infile[i]);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}
  	if (fits_get_num_hdus(outfptr,&hdunum,&status)) {
	  sprintf(event,"Fits gethdunum failed: %s",outfile+1);
	  reportevt(flag_verbose,STATUS,5,event);
	  printerror(status);
	}
  	sprintf(event,"Output file %s has %d HDU",outfile+1,hdunum);
	reportevt(flag_verbose,STATUS,1,event);
  	/* close output file */
  	if (fits_close_file(outfptr,&status)) {
	  sprintf(event,"Fits close failed: %s",outfile+1);
	  reportevt(flag_verbose,STATUS,5,event);
	  printerror(status);
	}


	/* cleanup if needed */
	if (flag_cleanup) 
	  for (i=0;i<nfiles-1;i++) {
	    sprintf(command,"rm %s",infile[i]);
	    sprintf(event,"Removing %s",infile[i]);
	    reportevt(flag_verbose,STATUS,1,event);
	    system(command);
	    if (inp = fopen(infile[i],"r")!=NULL) {
	      sprintf(event,"Remove failed: %s",infile[i]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	  }

	/* send expected return flag */
	return(0);
}
  
#undef MAXFILE
