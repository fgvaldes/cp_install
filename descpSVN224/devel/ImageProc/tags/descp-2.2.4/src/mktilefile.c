/*
** mktilefile.c
**
** Program to produce input files for mangle
**
** usage:  mktilefile --tile <TILENAME> --outfile <OUTFILE>
**
** If outfile is not supplied, the a default file named destile
** is written in the current directory
**
** OUTPUTS:
**   des.tile
**
** The des.tile file contains a list of all DES tile in the following
** format:
**   TILENAME_ID|CENTERRA|CENTERDEC
**
** AUTHOR:  Tony Darnell (tdarnell@illinois.edu)
** DATE:  28 SEPT 2009
**
** $Rev::                                             $ Revision of last commit
** $Author::                                          $ Author of last commit
** $Date::                                            $ Date of last commit
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>

#include "ora_utils.h"
#include "wcs.h"

struct globalArgs_t {
        int  year;
        char *tile;
        char *range;
        int  verbosity;
        char *outfile;
        int *footprint;
} globalArgs;

static const char *optString = "y:t:r:fvh?";

static const struct option longOpts[] = {
        { "year", required_argument, NULL, 'y' },
        { "tile", required_argument, NULL, 't' },
        { "range", required_argument, NULL, 'r' },
        { "verbose", no_argument, NULL, 'v' },
        { "help", no_argument, NULL, 'h' },
        { "outfile", required_argument, NULL, 'o' },
        { "footprint", no_argument, NULL, 'f' },
        { NULL, no_argument, NULL, 0 }
};

//
// Prototypes
//
void usage();

int main(int argc, char* argv[]) 
{
  sword status;
  int opt,i,j,k, longIndex = 0;
  int coaddId, imageId, reducedId, expId, rawId, numCoadds = 0, 
      numTiles = 0, numImgs = 0;
  char errbuf[100],tileoutname[100],ccdoutname[100];
  char *currentType, *tag;
  int errcode;
  double ra, dec, zeroPoint = 0.0;
  double wcspos[2],rawpos[2];

  FILE *foutp;

  tileStruct    *desTileArr;
  double *corners;

  desTileArr = (tileStruct *) malloc(sizeof(tileStruct) * numTiles);
  corners = (double *) malloc(sizeof(position) * 4);

  //
  // Bind handles
  //
  static OCIBind *bnd1p = (OCIBind *) 0;
  
  //
  // Output variable definitions
  //
  static OCIDefine *dfnhp[2];

  //
  // initialize command line args
  //
  globalArgs.year = 0;
  globalArgs.tile = NULL;
  globalArgs.outfile = NULL;
  globalArgs.range = NULL;
  globalArgs.verbosity = 0;
  globalArgs.footprint = 0;

  //
  // Process command line args
  //
  opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
  while( opt != -1 ) {
     switch( opt ) {
        case 'y':
                globalArgs.year = atoi(optarg);
                break;

        case 't':

                globalArgs.tile = (char *) malloc(sizeof(optarg));
                strcpy(globalArgs.tile,optarg);
                char *mystr = strtok(optarg,",");
                while (mystr != NULL){
                  mystr = strtok(NULL,",");
                  numTiles++;
                }
                break;

        case 'f':
                globalArgs.footprint++;
                break;

        case 'r':
                globalArgs.range = optarg;
                break;

        case 'o':
                globalArgs.outfile = optarg;
                break;

        case 'v':
                globalArgs.verbosity++;
                break;

        case 'h':

        case '?':
                usage();
                break;
        case 0:

        default:
               break;
     }
     opt = getopt_long( argc, argv, optString, longOpts, &longIndex );
  }

  //
  // End of command line arg processing
  //

  //
  // Establish a DB connection
  //

  status = dbconnect();
  if (status){
    printf("DB ERROR:  Database connection failed.\n");
    exit(4);
  }

  //
  // Allocate statement handle
  //
  status = OCIHandleAlloc(envhp, (dvoid **) &sthp, OCI_HTYPE_STMT, 
                          (size_t) 0, (dvoid **) 0);
  checkDBerr(errhp,status);

  //
  // Get DES Tiles
  //
  numTiles = getNumDESTiles();
  desTileArr = (tileStruct *) malloc(sizeof(tileStruct) * numTiles);
  getDESTiles(desTileArr, numTiles);
  status = OCIAttrGet(sthp, OCI_HTYPE_STMT, &numTiles, (ub4 *) 0,
      OCI_ATTR_ROW_COUNT,errhp);

  /*
  ** Write the tile file
  */
  if (globalArgs.outfile == NULL){
    sprintf(tileoutname,"%s.tile","DES");
  }else{
    sprintf(tileoutname,"%s.tile",globalArgs.outfile);
  }
  foutp = fopen(tileoutname,"w");

/* 
** look at whether we are printing output for use in NVO footprint
** service
*/
  if (globalArgs.footprint){

    fprintf(foutp,"REGION\n");
     for (i=0;i < numTiles;i++){

      getTileCorners(desTileArr[i].ra,desTileArr[i].dec,10000,10000,0.27,corners);
      fprintf(foutp,"POLY J2000 %f %f %f %f %f %f %f %f\n",
          corners[0],corners[1], /* 1,1         */
          corners[2],corners[3], /* 10000,1     */
          corners[4],corners[5], /* 10000,10000 */
          corners[6],corners[7]  /* 1,10000     */
          );

     }

  } else {
    fprintf(foutp,"TILEID|CENTERRA|CENTERDEC|RA1,1|DEC1,1|RA10000,10000|DEC10000,10000|");
    fprintf(foutp,"RA1,10000|DEC1,10000\n");
    for (i=0;i < numTiles;i++){

      getTileCorners(desTileArr[i].ra,desTileArr[i].dec,10000,10000,0.27,corners);

      fprintf(foutp,"%d|%f|%f|%f|%f|%f|%f|%f|%f|%f|%f\n",
          desTileArr[i].tileId, 
          desTileArr[i].ra,
          desTileArr[i].dec,
          corners[0],corners[1], /* 1,1         */
          corners[2],corners[3], /* 10000,1     */
          corners[4],corners[5], /* 10000,10000 */
          corners[6],corners[7]  /* 1,10000     */
          );

    }
  }

  fclose(foutp);

  //
  // Free DB Handles
  //
  status = OCIHandleFree((dvoid *) sthp, OCI_HTYPE_STMT);
  status = OCIHandleFree((dvoid *) svchp, OCI_HTYPE_SVCCTX);
  status = OCIHandleFree((dvoid *) errhp, OCI_HTYPE_ERROR);

  //
  // Free memory
  //
  free(desTileArr);

  return(0);

}

void usage( void )
{
        puts( "mktilefile:  Makes an input des tile file for mangle" );
        /* ... */
        exit( EXIT_FAILURE );
}

