/*
**
** mkCoaddClassStarList.c
**
** Program to produce a list of parameters for
** model fitting photometry on Coadds
**
** usage: mkCoaddSpreadModelList --l <outfile>
**
** Author: Gurvan Bazin (gbazin@usm.uni-muenchen.de)
** Date: 26 January 2010
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>

#include "ora_utils.h"



void usage(){
	fprintf(stderr, "mkCoaddSpreadModelList -l <outfile>\n");
}


int main(int argc, char** argv) {

	char* filename = NULL;
	int index;
	int c;
	int i;

	sword status;
	int numCoaddObj;
        spread_modelStruct* spread_model;

	FILE* fileout;




	if(argc<2){
		usage();
		exit(2);
	}

	opterr = 0;
	while ((c = getopt (argc, argv, "l:")) != -1)
         switch (c)
           {
           case 'l':
             filename = optarg;
             break;
           case '?':
             if (optopt == 'l')
               fprintf (stderr, "Option -%c requires an argument.\n", optopt);
             else if (isprint (optopt))
               fprintf (stderr, "Unknown option `-%c'.\n", optopt);
             else
               fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
             return 1;
           default:
             abort ();
           }
     
       for (index = optind; index < argc; index++)
         printf ("Non-option argument %s\n", argv[index]);



	//
	// Establish a DB connection
	//
	status = dbconnect();
	if (status){
	  fprintf(stderr, "DB ERROR:  Database connection failed.");
	  exit(4);
	}

	//
	// Allocate statement handle
	//
	status = OCIHandleAlloc(envhp, (dvoid **) &sthp, OCI_HTYPE_STMT,
        	                (size_t) 0, (dvoid **) 0);
	checkDBerr(errhp,status);


	// Get DES Coadd Magnitudes
 	numCoaddObj = getNumCoaddObj();
	printf("%d objects in the coadd_objects table\n", numCoaddObj);
        spread_model = (spread_modelStruct *) malloc(sizeof(spread_modelStruct) * numCoaddObj);
	getSpreadModel(spread_model, numCoaddObj);
	status = OCIAttrGet(sthp, OCI_HTYPE_STMT, &numCoaddObj, (ub4 *) 0,
      				OCI_ATTR_ROW_COUNT,errhp);

	dbdisconnect();

	if((fileout = fopen(filename, "w")) == NULL) {
   	  fprintf(stderr, "Cannot open file.\n");
  	  exit(2);
  	}

	fprintf(fileout, "#COADD_OBJECTS_ID spread_model_g spread_model_r spread_model_i spread_model_z spread_model_y\n");
	for(i = 0; i < numCoaddObj; i++){
		fprintf(fileout, "%d %22.16f %22.16f %22.16f %22.16f %22.16lf\n", spread_model[i].objId, spread_model[i].g, spread_model[i].r, spread_model[i].i, spread_model[i].z, spread_model[i].y);
	}

	if( fclose( fileout )) fprintf(stderr, "File close error.\n");

	//
	// Free DB Handles
	//
	status = OCIHandleFree((dvoid *) sthp, OCI_HTYPE_STMT);
	status = OCIHandleFree((dvoid *) svchp, OCI_HTYPE_SVCCTX);
	status = OCIHandleFree((dvoid *) errhp, OCI_HTYPE_ERROR);

	//
	// Free memory
	//
	free(spread_model);

	return 0;	

}

