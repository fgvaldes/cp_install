/*
**
** tdeswcs.c
**
** DESCRIPTION:
**     Set of subroutines that interface DES data structure to the 
**     wcslib routines.
**
** AUTHOR:  Tony Darnell (tdarnell@uiuc.edu)
** DATE:    20 July 2009
**
** $Rev:: 4419                                        $ Revision of last commit
** $Author:: tdarnell                                 $ Author of last commit
** $Date:: 2009-09-28 17:00:18 -0500 (Mon, 28 Sep 200#$ Date of last commit
**
*/

#define PIXELSCALE 0.27

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ora_utils.h"
#include "fitswcs.h"
#include "define.h"
#include "fitscat_defs.h"

static int wcsproj0 = 0;

wcsstruct * 
deswcsinit (imageInfo *imageHeader){

  int epoch = 0;
  char **ctype;
  int *naxisn, naxis;
  double *cd1, *cd2, *crval, *crpix, *cdelt;

  wcsstruct *wcs;

  QMALLOC(ctype,char *,16);
  QMALLOC(crpix,double,2);
  QMALLOC(crval,double,2);
  QMALLOC(cd1,double,2);
  QMALLOC(cd2,double,2);
  QMALLOC(cdelt,double,2);
  QMALLOC(naxisn,int,2);

  /* Hard-code these for now */
  naxis = 2;
  ctype[0] = "RA---TAN";
  ctype[1] = "DEC--TAN";

  naxisn[0] = imageHeader->naxis1;
  naxisn[1] = imageHeader->naxis2;

  crpix[0] = imageHeader->crpix1;
  crpix[1] = imageHeader->crpix2;

  crval[0] = imageHeader->crval1;
  crval[1] = imageHeader->crval2;


  /* Coordinate reference frame and equinox */

  cd1[0] = imageHeader->cd1_1;
  cd1[1] = imageHeader->cd1_2;
  cd2[0] = imageHeader->cd2_1;
  cd2[1] = imageHeader->cd2_2;

  cdelt[0] = sqrt((cd1[0]*cd1[0]) + (cd1[1]*cd1[1]));
  cdelt[1] = sqrt((cd2[0]*cd2[0]) + (cd2[1]*cd2[1]));

  wcs = create_wcs(ctype,crval,crpix,cdelt,naxisn,naxis);

  wcs->equinox = imageHeader->equinox;

  /* Load PV distortion parameters */
  wcs->projp[0] = imageHeader->pv1_0;
  wcs->projp[1] = imageHeader->pv1_1;
  wcs->projp[2] = imageHeader->pv1_2;
  wcs->projp[3] = imageHeader->pv1_3;
  wcs->projp[4] = imageHeader->pv1_4;
  wcs->projp[5] = imageHeader->pv1_5;
  wcs->projp[6] = imageHeader->pv1_6;
  wcs->projp[7] = imageHeader->pv1_7;
  wcs->projp[8] = imageHeader->pv1_8;
  wcs->projp[9] = imageHeader->pv1_9;
  wcs->projp[10] = imageHeader->pv1_10;

  wcs->projp[100] = imageHeader->pv2_0;
  wcs->projp[101] = imageHeader->pv2_1;
  wcs->projp[102] = imageHeader->pv2_2;
  wcs->projp[103] = imageHeader->pv2_3;
  wcs->projp[104] = imageHeader->pv2_4;
  wcs->projp[105] = imageHeader->pv2_5;
  wcs->projp[106] = imageHeader->pv2_6;
  wcs->projp[107] = imageHeader->pv2_7;
  wcs->projp[108] = imageHeader->pv2_8;
  wcs->projp[109] = imageHeader->pv2_9;
  wcs->projp[110] = imageHeader->pv2_10;

  wcs->cd[0] = cd1[0];
  wcs->cd[1] = cd1[1];
  wcs->cd[2] = cd2[0];
  wcs->cd[3] = cd2[1];
  wcs->radecsys = RDSYS_FK5;

  init_wcs(wcs);

  return (wcs);

}
