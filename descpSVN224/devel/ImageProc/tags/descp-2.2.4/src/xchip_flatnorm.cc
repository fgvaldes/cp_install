///
/// \file xchip_flatnorm.cc
/// \brief Inter-chip flat normalization
/// \author Mike Campbell (mtcampbe@illinois.edu)
///
/// Takes a list of input flats, output flats,
/// and flat normalization weights and scales
/// normalizes them to the same level.
/// The input list should have one line per chip 
/// and expected to be in the following format:\n
/// input_flat  output_flat ampA_weight ampB_weight\n\n
/// The list itself can be specified from file or
/// read off stdin.
///
/// \todo Update to enable the reading of "T" from 
///       the processing history
/// 
#include <list>
#include "ComLine.hh"
#include "FitsTools.hh"

///
/// \brief ComLineObject for xchipfltnrm app
///
/// ComLine is a convenience object to help
/// deal with command-line arguments and
/// options.
class XChipFlatNormComLine : public ComLineObject
{
public:
  XChipFlatNormComLine()
    : ComLineObject()
  { Initialize(); };
  XChipFlatNormComLine(const char *args[])
    : ComLineObject(args)
  { Initialize(); };
  void Initialize(){
    AddOption('h',"help");
    AddOption('t',"normfactor",2,"value");
    AddOption('v',"verb",2,"level");
    AddOption('i',"input",2,"input_list");
    AddOption('o',"output",2,"output_list");
    //    AddOption('l',"list",2,"list_file");
    AddOption('c',"chip",2,"number");
    AddOption('m',"median");
    AddOption('f',"replace");
    AddArgument("infile");
    AddHelp("median","Use median of input weights.");
    AddHelp("normfactor","Use specified <value> for normalization factor.");
    AddHelp("chip","Use weight from chip <number> of input chips.");
    AddHelp("input","Text file with input file list. (no default)");
    AddHelp("output","Text file with output file list.(default = input list)");
    AddHelp("help","Prints this long version of help.");
    AddHelp("verb","Verbosity level [0-3], default = 1.");
    AddHelp("replace","If output file exists, replace it.");
    std::ostringstream Ostr;
    Ostr << "Optional input file with one line per chip in the following format:\n"
	 << "\t\tfits_infile fits_outfile ampA_weight ampB_weight.\n"
	 << "\t\t(mutually exclusive with \"--input\" option)\n";
    AddArgHelp("infile",Ostr.str());
    Ostr.str("");
    Ostr << "Takes a list of input and output fits files\n" 
	 << "for chip flats and their amp normalization\n"
	 << "factors and applies an inter-chip normalization.\n";
    _description.assign(Ostr.str());
  };
};



int XFlatNorm(char *argv[])
{

  // ------- Setup and Command Line Parsing --------
  
  // Initialization of command line object
  XChipFlatNormComLine comline;
  comline.ProcessCommandLine((const char **)argv);
  std::string program_name(comline.ProgramName());
  
  // Just print help on stdout and quit if user
  // did -h or --help. 
  if(!comline.GetOption("help").empty()){
    std::cout << comline.LongUsage() << std::endl;
    return(1);
  }

  // Find out what command line options were set
  bool do_median       = !comline.GetOption("median").empty();
  bool do_replace      = !comline.GetOption("replace").empty();
  std::string schip    =  comline.GetOption("chip");
  std::string sfac     =  comline.GetOption("normfactor");
  std::string sverb    =  comline.GetOption("verb");
  std::string inlistf  =  comline.GetOption("input");
  std::string outlistf =  comline.GetOption("output");
  
  // Parse verbosity level
  int flag_verbose = 1;
  if(!sverb.empty()){
    std::istringstream Istr(sverb);
    Istr >> flag_verbose;
    if(flag_verbose < 0 || flag_verbose > 3)
      flag_verbose = 1;
  }
  
  // - Set up IO objects -
  //
  std::ostringstream Out;
  std::istream *InStreamPtr = NULL;
  // User specified an input list
  std::vector<std::string> infile_names;
  std::vector<std::string> outfile_names;
  //  std::list<std::string> inlist;
  //  std::list<std::string> outlist;
  if(!inlistf.empty()){
    std::ifstream Inf;
    Inf.open(inlistf.c_str());
    if(!Inf){
      Out << program_name << ":Error: Cannot open specified input list, "
	  << inlistf << std::endl;
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    std::string line;
    while(std::getline(Inf,line)){
      infile_names.push_back(line);
      if(outlistf.empty())
	outfile_names.push_back(line);    
    }
    Inf.close();
  }
  if(!outlistf.empty()){
    std::ifstream Inf;
    Inf.open(outlistf.c_str());
    if(!Inf){
      Out << program_name << ":Error: Cannot open specified output list, "
	  << outlistf << std::endl;
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    std::string line;
    while(std::getline(Inf,line))
      outfile_names.push_back(line);
    Inf.close();
  }

  
  // Parse normalization factor
  int nfac = 0;
  double T = 0;
  if(!sfac.empty()){
    std::istringstream Istr(sfac);
    Istr >> T;
    if(T <= 0){
      Out << program_name << ": Invalid weighting factor, " << T << std::endl;
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    nfac++;
  }

  // Parse chip specification
  int chipno = 0;
  if(!schip.empty()){
    std::istringstream Istr(schip);
    Istr >> chipno;
    if(chipno <= 0){
      Out << program_name << ": Invalid chip number, " << chipno << ", must be 1-nchips." << std::endl;
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    nfac++;
  }

  // Make sure user didn't specify more than one strategy for determining T
  if(nfac > 1){
    Out << program_name << ":Error: Cannot specify more than one weighting factor." << std::endl;
    LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
    return(1);
  }

  // Default to finding the median of input weights
  if(nfac == 0)
    do_median = true;

  // If user specified an input file, then switch to using
  // the specified input file.
  std::vector<double> ampA_weights;
  std::vector<double> ampB_weights;
  std::vector<double> allweights;
  int nchips = infile_names.size();
  std::vector<std::string> input_files(comline.GetArgs());
  if(!input_files.empty()){
    if(nchips != 0){
      Out << program_name << ":Error: Cannot use more than one input type. The"
	  << " input list was already specified, " 
	  << inlistf << std::endl;
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    std::ifstream Inf;
    Inf.open(input_files[0].c_str());
    if(!Inf){
      Out << program_name << ":Error: Cannot open specified input file, "
	  << input_files[0] << std::endl;
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    std::string line;
    // Read the input one line at a time
    while(std::getline(Inf,line)){
      nchips++;
      std::string infile_name;
      std::string outfile_name;
      double ampA_weight = 0;
      double ampB_weight = 0;
      // Extract the data from the line
      std::istringstream Istr(line);
      Istr >> infile_name >> outfile_name >> ampA_weight >> ampB_weight;
      // Populate vectors containing the filenames and weights
      infile_names.push_back(infile_name);
      outfile_names.push_back(outfile_name);
      ampA_weights.push_back(ampA_weight);
      ampB_weights.push_back(ampB_weight);
      allweights.push_back(ampA_weight);
      allweights.push_back(ampB_weight);
      // If the "chip" option was used, then set T to the 
      // ampA weight from that chip. (currently, amp weights
      // for a given chip are identical).
      if(nchips == chipno){
	T = ampA_weight;
	if(flag_verbose){
	  Out.str("");
	  Out << program_name << ": Setting T to chip " << chipno 
	      << " weight, " << T;
	  LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
	}
      }
    }
    Inf.close();
  }

  // Set up a list of keywords we want
  // to exclude from our headers
  // - must be constructed in a deprecated way due to 
  //   unfortunate restriction in legacy utilities.
  char **exclusion_list;
  exclusion_list = new char * [43];
  exclusion_list[0] = static_cast<char *>("SIMPLE");
  exclusion_list[1] = static_cast<char *>("BITPIX");
  exclusion_list[2] = static_cast<char *>("NAXIS");
  exclusion_list[3] = static_cast<char *>("NAXIS1");
  exclusion_list[4] = static_cast<char *>("NAXIS2");
  exclusion_list[5] = static_cast<char *>("EXTEND");
  exclusion_list[6] = static_cast<char *>("NEXTEND");
  exclusion_list[7] = static_cast<char *>("CHECKSUM");
  exclusion_list[8] = static_cast<char *>("DATASUM");
  exclusion_list[9] = static_cast<char *>("CHECKVER");
  exclusion_list[10] =static_cast<char *>("GCOUNT");
  exclusion_list[11] =static_cast<char *>("PCOUNT");
  exclusion_list[12] =static_cast<char *>("BZERO");
  exclusion_list[13] =static_cast<char *>("BSCALE");
  exclusion_list[14] =static_cast<char *>("INHERIT");
  exclusion_list[15] =static_cast<char *>("XTENSION");
  exclusion_list[16] =static_cast<char *>("TFIELDS");
  exclusion_list[17] =static_cast<char *>("TFORM1");
  exclusion_list[18] =static_cast<char *>("ZIMAGE");
  exclusion_list[19] =static_cast<char *>("ZTILE1");
  exclusion_list[20] =static_cast<char *>("ZTILE2");
  exclusion_list[21] =static_cast<char *>("ZCMPTYPE");
  exclusion_list[22] =static_cast<char *>("ZNAME1");
  exclusion_list[23] =static_cast<char *>("ZVAL1");
  exclusion_list[24] =static_cast<char *>("ZNAME2");
  exclusion_list[25] =static_cast<char *>("ZVAL2");
  exclusion_list[26] =static_cast<char *>("EXTNAME");
  exclusion_list[27] =static_cast<char *>("ZTENSION");
  exclusion_list[28] =static_cast<char *>("ZBITPIX");
  exclusion_list[29] =static_cast<char *>("ZNAXIS");
  exclusion_list[30] =static_cast<char *>("ZNAXIS1");
  exclusion_list[31] =static_cast<char *>("ZNAXIS2");
  exclusion_list[32] =static_cast<char *>("ZPCOUNT");
  exclusion_list[33] =static_cast<char *>("ZGCOUNT");
  exclusion_list[34] =static_cast<char *>("DCREATED");
  exclusion_list[35] =static_cast<char *>("TTYPE1");
  exclusion_list[36] =static_cast<char *>("ZHECKSUM");
  exclusion_list[37] =static_cast<char *>("TTYPE2");
  exclusion_list[38] =static_cast<char *>("TTYPE3");
  exclusion_list[39] =static_cast<char *>("ZSIMPLE");
  exclusion_list[40] =static_cast<char *>("ZEXTEND");
  exclusion_list[41] =static_cast<char *>("TFORM2");
  exclusion_list[42] =static_cast<char *>("TFORM3");
  
  // Extract the chip weights from the header if necessary
  if(allweights.empty()){
    int chipcount = 0;
    std::vector<std::string>::iterator inamei = infile_names.begin();
    while(inamei != infile_names.end()){

      chipcount++;

      // Declare and initialize FitsImage object which
      // will handle most of the FITS-specific interactions
      // under-the-covers.
      FitsTools::FitsImage Inimage;
      Inimage.SetOutStream(Out);
      Inimage.SetExclusions(exclusion_list,43);

      // Set input and output FITS filenames
      std::string filename(*inamei++);
      if(flag_verbose){
	Out.str("");
	Out << "Searching for " << filename << std::endl;
	LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      }

      int status = 0;
      // If found, this will read the FITS file including
      // all data units, headers and images.
      if(Inimage.Read(filename,flag_verbose)){
	LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
	Out.str("");
	Out << "Failed to read " << filename << ".";
	LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
	return(1);
      }
      else {
	LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
	Out.str("");
      }
      // Close the input FITS file, we're done with it.
      Inimage.Close();
      //      std::cout << Inimage.ImageHeader() << std::endl;
      if(!FitsTools::HeaderKeyExists(Inimage.ImageHeader(),"SCALMEAN")){
	Out.str("");
	Out << "Failed to find SCALMEAN keyword in header of " << filename << ".";
	LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
	return(1);
      }
      double scalefactor = FitsTools::GetHeaderValue<double>(Inimage.ImageHeader(),"SCALMEAN");
      ampA_weights.push_back(scalefactor);
      ampB_weights.push_back(scalefactor);
      allweights.push_back(scalefactor);
      allweights.push_back(scalefactor);
      // If the "chip" option was used, then set T to the 
      // ampA weight from that chip. (currently, amp weights
      // for a given chip are identical).
      if(chipcount == chipno){
	T = scalefactor;
	if(flag_verbose){
	  Out.str("");
	  Out << program_name << ": Setting T to chip " << chipno 
	      << " (file=" << filename << ") weight = " << T;
	  LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
	}
      }
    }
  }

  // Report the method used to determine T.
  if(flag_verbose){
    Out.str("");
    Out << program_name << " weighting flats with " 
	      << (T>0 ? " T = " : (chipno > 0 ? " chip = " :
				   " median of weights."));
    if(T > 0 || chipno > 0){
      Out << (T > 0 ? T : chipno) << ".";
    }
    LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
  }


  // // ------- Data Reading and DataStructure Pop --------

  // // Read the input file (or stdin if no file),
  // // and parse the chip lines containing the
  // // input FITS file names, output FITS file 
  // // names, and weights for each amp.
  // std::string line; 
  // std::vector<std::string> infile_names;
  // std::vector<std::string> outfile_names;
  // std::vector<float> ampA_weights;
  // std::vector<float> ampB_weights;
  // std::vector<float> allweights;
  // int nchips = 0;
  // // Read the input one line at a time
  // while(std::getline(*InStreamPtr,line)){
  //   nchips++;
  //   std::string infile_name;
  //   std::string outfile_name;
  //   float ampA_weight = 0;
  //   float ampB_weight = 0;
  //   // Extract the data from the line
  //   std::istringstream Istr(line);
  //   Istr >> infile_name >> outfile_name >> ampA_weight >> ampB_weight;
  //   // Populate vectors containing the filenames and weights
  //   infile_names.push_back(infile_name);
  //   outfile_names.push_back(outfile_name);
  //   ampA_weights.push_back(ampA_weight);
  //   ampB_weights.push_back(ampB_weight);
  //   allweights.push_back(ampA_weight);
  //   allweights.push_back(ampB_weight);
  //   // If the "chip" option was used, then set T to the 
  //   // ampA weight from that chip. (currently, amp weights
  //   // for a given chip are identical).
  //   if(nchips == chipno){
  //     T = ampA_weight;
  //     if(flag_verbose){
  // 	Out.str("");
  // 	Out << program_name << ": Setting T to chip " << chipno 
  // 		  << " weight, " << T;
  // 	LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
  //     }
  //   }
  // }

  // If doing the median method, then Calculate T from median 
  // of the input weights.
  if(do_median){ 
    // Sort the weights
    std::sort(allweights.begin(),allweights.end());
    // Remove identical entries... we know there are two identical entries per chip
    // at this point.
    std::vector<double>::iterator uvi = std::unique(allweights.begin(),allweights.end());
    // Extract the median from the vector of sorted weights
    unsigned int nvals = uvi-allweights.begin();
    if(nvals%2){
      unsigned int index = nvals/2;
      T = allweights[index];
    }
    else{
      unsigned int index = nvals/2;
      T = (allweights[index] + allweights[index-1])/2.0;
    }
    // Report the median to stdout if verbose
    if(flag_verbose){
      Out.str("");
      Out << program_name << " found median = " << T;
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
    }
  }

  // Invert T in advance 
  double Tm1 = 1.0/T;



  // ------- Flats Processing from FITS files begins here --------
  //
  // At this point, the filenames, and all the amp-specific weights
  // are read in, and the value for "T" is known.  Now just need to
  // read in the flats from FITS, scale them, and output into the 
  // specified file.

  // These iterators are how we iterate through the vectors of
  // filenames and weights that we populated above.
  std::vector<std::string>::iterator inamei = infile_names.begin();
  std::vector<std::string>::iterator onamei = outfile_names.begin();
  std::vector<double>::iterator awi = ampA_weights.begin();
  std::vector<double>::iterator bwi = ampB_weights.begin();

  // Step through the file lists and read in the FITS images
  // Scale the flat and associated weights by the normalization
  // factor = <weight>/T.  Then output the result to a new FITS
  // file.
  while(inamei != infile_names.end()){

    // Declare and initialize FitsImage object which
    // will handle most of the FITS-specific interactions
    // under-the-covers.
    FitsTools::FitsImage Inimage;
    Inimage.SetOutStream(Out);
    Inimage.SetExclusions(exclusion_list,43);

    // Set input and output FITS filenames
    std::string filename(*inamei++);
    std::string ofilename(*onamei++);
    if(flag_verbose){
      Out.str("");
      Out << "Searching for " << filename;
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      Out.str("");
    }

    int status = 0;
    // If found, this will read the FITS file including
    // all data units, headers and images.
    if(Inimage.Read(filename,flag_verbose)){
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      Out.str("");
      Out << "Failed to read " << filename << ".";
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    else if (flag_verbose){
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      Out.str("");
    }
    // Close the input FITS file, we're done with it.
    Inimage.Close();
    // Let stdout know what file was actually read.
    // (it can be different due to the various valid extensions)
    if(flag_verbose){
      Out.str("");
      Out << "Found " << Inimage.DES()->name << ".";
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
    }

    // How many HDU's were read?  Report it to stdout if verbose
    int number_of_hdus = Inimage.Headers().size();
    std::vector<std::string>::iterator hi = Inimage.Headers().begin();
    if(flag_verbose){
      Out.str("");
      Out << "Read " << number_of_hdus << " data units from " 
		<< filename;
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
    }
    // If *really* verbose, dump the headers to stdout
    if(flag_verbose==3){
      while(hi != Inimage.Headers().end())
	{
	  Out.str("");
	  Out << "Size for image: " << Inimage.DES()->axes[0] << "X" 
		    << Inimage.DES()->axes[1] << ", number of pixels = " 
		    << Inimage.DES()->npixels << std::endl
		    << "Header " << hi-Inimage.Headers().begin()+1 << std::endl
		    << *hi << std::endl;
	  LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
	  hi++;
	}
    }
    
    // Apply the scaling factor to the image and weight (variance image)
    // Right now, AmpA weights are identical to AmpB weights
    if(Inimage.DES()->image){
      if(flag_verbose==3){
	Out.str("");
	Out << "Scaling flat image from " << filename;
	LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      }
      FitsTools::ImageData<float>(Inimage.DES()->image,Inimage.DES()->npixels).ScaleImage(*awi*Tm1);
    }
    if(Inimage.DES()->varim){
      if(flag_verbose==3){
	Out.str("");
	Out << "Scaling weight image from " << filename;
	LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      }
      FitsTools::ImageData<float>(Inimage.DES()->varim,Inimage.DES()->npixels).ScaleImage(*awi*Tm1);
    }
    awi++;
    bwi++;
    
    if(flag_verbose==3){
      Out.str("");
      Out << "Scaling done, preparing output for " << ofilename;
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      Out.str("");
    }

    // - - Output into FITS - - 

    // Leave processing history in output header
    //

    // Get date and time
    time_t tm = time(NULL);
    std::string dateandtime(asctime(localtime(&tm)));

    // For some oddball reason, the dateandtime string appears to 
    // have a newline in it.  Remove it with the following
    std::istringstream DStr(dateandtime);
    std::string day,month,date,stime,year;
    DStr >> day >> month >> date >> stime >> year;

    // Form the processing messages and append them to the header
    std::ostringstream Hstr;
    Hstr << "DESFLTNM= \'" << day << " " << month << " " << date 
	 << " " << stime << " " << year << "\' / interchip flat normalization";
    Inimage.AppendImageHeader(Hstr.str());
    LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
    Hstr.str("");
    Hstr << "DESXCNRM= " << std::setw(20) << T << " / interchip normalization factor";
    Inimage.AppendImageHeader(Hstr.str());


    // Now actually write the output FITS file
    if(flag_verbose==3){
      Out.str("");
      Out << "Writing rescaled flat into " << ofilename << ".";
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      Out.str("");
    }
    if(Inimage.Write(ofilename,do_replace,flag_verbose)){
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      Out << "Failed write output flat into  " << ofilename << ".";
      Inimage.Close();
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    else if (flag_verbose){
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      Out.str("");
    }
    // Close the output image.
    Inimage.Close();

    // Now actually write the output FITS file
    if(flag_verbose==3){
      Out.str("");
      Out << "Finished writing " << ofilename;
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
      Out.str("");
    }
  } // loop over input images

  // Clean up and exit
  delete [] exclusion_list;
  return 0;
};


int main(int argc,char *argv[])
{
  return(XFlatNorm(argv));
};
