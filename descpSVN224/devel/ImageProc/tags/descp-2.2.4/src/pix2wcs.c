/*
** DESCRIPTION:
**     Reads data from a csv textfile generated from a database call
**     and computes the wcs coordinates.  The textfile must have the following
**     values in the order presented here:
**     
**     imageid,xpos,ypos,naxis,naxis1,naxis2,crval1,crval2,crpix1,crpix2,
**     cd1_1,cd1_2,cd2_1,cd2_2,obsdate,equinox,pv1_0-10 distorion params,
**
**     xpos and ypos are the pixels of interest.  Obsdate must be of the form:
**     obsdate = 2000.0 - (MJD2000 - date)/365.25;
**        where date is the value found in the MJD_OBS fits header
**              MJD2000 is a constant:  51544.50000
**     
**     This program will interpret each line in the textfile as a separate file.
**
** INPUT:
**     csv textfile
** OUTPUT:
**     imageid,RA,DEC to stdout for each file read in.
**
** AUTHOR:  Tony Darnell (tdarnell@uiuc.edu)
**
** Last commit:
**     $Rev: 8189 $
**     $LastChangedBy: mtcampbe $
**     $LastChangedDate: 2012-07-19 12:19:21 -0700 (Thu, 19 Jul 2012) $
*/

#include "imageproc.h"
#include "define.h"
#include "fitswcs.h"
#include "fitscat_defs.h"

#define  MJD2000 51544.50000

static int verbose_flag;

main(argc,argv)
	int argc;
	char **argv;
{

  char *filename,fileline[1000],filelinetemp[1000],*tag,tempf[100],*metatable;
  char **ctype;
  double *cd1, *cd2, *crval, *crpix, *cdelt;
  int *naxisn, naxis;
  double wcspos[2],rawpos[2];
  int i,j,k,x,y,ncomma,len,imageId;
  FILE *infile, *outfile;

  keystruct *key;
  catstruct *cat;
  tabstruct *tab, *imatab;
  wcsstruct *wcsin;

  filename = argv[1];
  infile = fopen(filename,"r");

  QMALLOC(ctype,char *,16);
  QMALLOC(crpix,double,2);
  QMALLOC(crval,double,2);
  QMALLOC(cd1,double,2);
  QMALLOC(cd2,double,2);
  QMALLOC(cdelt,double,2);
  QMALLOC(naxisn,int,2);

  /* Hard-code these for now */
  ctype[0] = "RA---TAN";
  ctype[1] = "DEC--TAN";

  while(fscanf(infile,"%s",fileline)!=EOF) {

    ncomma = 1;
    sprintf(filelinetemp,"%s",fileline);
    len = strlen(fileline);
    tag = strtok(filelinetemp,",");

    /* Get the stuff needed to create the wcsstruct properly */
    while(tag != NULL){

      /* image id */
      if (ncomma == 1)
        imageId = atoi(tag);

      /* x,y pixel */
       if (ncomma == 2)
         rawpos[0] = atof(tag);
       if (ncomma == 3)
         rawpos[1] = atof(tag);

       /* naxis, naxis1, naxis1 */
       if (ncomma == 4)
         naxis = atoi(tag);
       if (ncomma == 5)
         naxisn[0] = atof(tag);
       if (ncomma == 6)
         naxisn[1] = atof(tag);

       /* crvals, crpix,cd1,cd2 */
       if (ncomma == 7)
         crval[0] = atof(tag);
       if (ncomma == 8)
         crval[1] = atof(tag);
       if (ncomma == 9)
         crpix[0] = atof(tag);
       if (ncomma == 10)
         crpix[1] = atof(tag);
       if (ncomma == 11)
         cd1[0] = atof(tag);
       if (ncomma == 12)
         cd1[1] = atof(tag);
       if (ncomma == 13)
         cd2[0] = atof(tag);
       if (ncomma == 14)
         cd2[1] = atof(tag);

       tag = strtok(NULL,",");
       ncomma++;
    }

    /*
  printf("cd1_1 --> %e\n",cd1[0]);
  printf("cd2_1 --> %e\n",cd2[0]);
  printf("cd1_2 --> %e\n",cd1[1]);
  printf("cd2_2 --> %e\n\n",cd2[1]);
  */

    cdelt[0] = sqrt((cd1[0]*cd1[0]) + (cd1[1]*cd1[1]));
    cdelt[1] = sqrt((cd2[0]*cd2[0]) + (cd2[1]*cd2[1]));

    /* Create a wcs structure to put the data in */
    wcsin = create_wcs(ctype,crval,crpix,cdelt,naxisn,naxis);

    /* printf("%d\n",len); */
    ncomma=1;
    sprintf(filelinetemp,"%s",fileline);
    tag = strtok(filelinetemp,",");
    while(tag != NULL){

       if (ncomma == 15)
         wcsin->obsdate = atof(tag);
       if (ncomma == 16)
         wcsin->equinox = atoi(tag);

       /* PV Distortion params */
       if (ncomma == 17)
         wcsin->projp[0] = atof(tag);
       if (ncomma == 18)
         wcsin->projp[1] = atof(tag);
       if (ncomma == 19)
         wcsin->projp[2] = atof(tag);
       if (ncomma == 20)
         wcsin->projp[3] = atof(tag);
       if (ncomma == 21)
         wcsin->projp[4] = atof(tag);
       if (ncomma == 22)
         wcsin->projp[5] = atof(tag);
       if (ncomma == 23)
         wcsin->projp[6] = atof(tag);
       if (ncomma == 24)
         wcsin->projp[7] = atof(tag);
       if (ncomma == 25)
         wcsin->projp[8] = atof(tag);
       if (ncomma == 26)
         wcsin->projp[9] = atof(tag);
       if (ncomma == 27)
         wcsin->projp[10] = atof(tag);
       if (ncomma == 28)
         wcsin->projp[100] = atof(tag);
       if (ncomma == 29)
         wcsin->projp[101] = atof(tag);
       if (ncomma == 30)
         wcsin->projp[102] = atof(tag);
       if (ncomma == 31)
         wcsin->projp[103] = atof(tag);
       if (ncomma == 32)
         wcsin->projp[104] = atof(tag);
       if (ncomma == 33)
         wcsin->projp[105] = atof(tag);
       if (ncomma == 34)
         wcsin->projp[106] = atof(tag);
       if (ncomma == 35)
         wcsin->projp[107] = atof(tag);
       if (ncomma == 36)
         wcsin->projp[108] = atof(tag);
       if (ncomma == 37)
         wcsin->projp[109] = atof(tag);
       if (ncomma == 38)
         wcsin->projp[110] = atof(tag);
       if (ncomma == 39)
         wcsin->epoch = atof(tag);
       if (ncomma == 40)
         metatable = tag;

       tag = strtok(NULL,",");
       ncomma++;
     }

     /*
     wcsin->nprojp = 11;
     wcsin->equinox =  2000.0;
     wcsin->chirality = -1;
     wcsin->pixscale =  0.000074;
     invert_wcs(wcsin);
     range_wcs(wcsin);
     */

     wcsin->cd[0] = cd1[0];
     wcsin->cd[1] = cd1[1];
     wcsin->cd[2] = cd2[0];
     wcsin->cd[3] = cd2[1];
     wcsin->radecsys = RDSYS_FK5;

     init_wcs(wcsin);

     sprintf(wcsin->cunit[0],"%s","deg");
     sprintf(wcsin->cunit[1],"%s","deg");

     /*
  printf("imageid--> %d\n\n",imageId);

  printf("naxis--> %d\n",wcsin->naxis);
  printf("naxis1--> %d\n",wcsin->naxisn[0]);
  printf("naxis2--> %d\n",wcsin->naxisn[1]);
  printf("ctype1--> %s\n",wcsin->ctype[0]);
  printf("ctype2--> %s\n",wcsin->ctype[1]);
  printf("cunit1--> %s\n",wcsin->cunit[0]);
  printf("cunit2--> %s\n",wcsin->cunit[1]);
  printf("crval1--> %e\n",wcsin->crval[0]);
  printf("crval2--> %e\n",wcsin->crval[1]);
  printf("cdelt1--> %e\n",wcsin->cdelt[0]);
  printf("cdelt2--> %e\n",wcsin->cdelt[1]);
  printf("crpix1--> %e\n",wcsin->crpix[0]);
  printf("crpix2--> %e\n",wcsin->crpix[1]);
  printf("crder1--> %e\n",wcsin->crder[0]);
  printf("crder2--> %e\n",wcsin->crder[1]);
  printf("csyer1--> %e\n",wcsin->csyer[0]);
  printf("csyer2--> %e\n",wcsin->csyer[1]);
  printf("cd1_1 --> %e\n",wcsin->cd[0]);
  printf("cd2_1 --> %e\n",wcsin->cd[1]);
  printf("cd1_2 --> %e\n",wcsin->cd[2]);
  printf("cd2_2 --> %e\n\n",wcsin->cd[3]);

  printf("nprojp--> %d\n",wcsin->nprojp);
  printf("projp1--> %0.10e\n",wcsin->projp[0]);
  printf("projp2--> %e\n",wcsin->projp[1]);
  printf("projp3--> %e\n",wcsin->projp[2]);
  printf("projp4--> %e\n",wcsin->projp[3]);
  printf("projp5--> %e\n",wcsin->projp[4]);
  printf("projp6--> %e\n",wcsin->projp[5]);
  printf("projp7--> %e\n",wcsin->projp[6]);
  printf("projp8--> %e\n",wcsin->projp[7]);
  printf("projp9--> %e\n",wcsin->projp[8]);
  printf("projp10--> %e\n",wcsin->projp[9]);
  printf("projp11--> %e\n\n",wcsin->projp[10]);

  printf("wcsmin1--> %f\n",wcsin->wcsmin[0]);
  printf("wcsmin2--> %f\n",wcsin->wcsmin[1]);
  printf("wcsmax1--> %f\n",wcsin->wcsmax[0]);
  printf("wcsmax1--> %f\n",wcsin->wcsmax[1]);
  printf("wcsscale1--> %f\n",wcsin->wcsscale[0]);
  printf("wcsscale2--> %f\n",wcsin->wcsscale[1]);
  printf("wcsmaxradius--> %f\n",wcsin->wcsmaxradius);
  printf("outmin--> %f\n",wcsin->outmin);
  printf("outmax--> %f\n",wcsin->outmax);
  printf("lat--> %d\n",wcsin->lat);
  printf("long--> %d\n",wcsin->lng);
  printf("r0--> %f\n",wcsin->r0);
  printf("lindet--> %f\n",wcsin->lindet);
  printf("chirality--> %d\n",wcsin->chirality);
  printf("ap2000--> %f\n",wcsin->ap2000);
  printf("dp2000--> %f\n",wcsin->dp2000);
  printf("ap1950--> %f\n",wcsin->ap1950);
  printf("dp1950--> %f\n",wcsin->dp1950);
  printf("equinox--> %f\n",wcsin->equinox);
  printf("epoch--> %f\n",wcsin->epoch);
  printf("radecsys--> %f\n",wcsin->radecsys);
  printf("celsys--> %f\n",wcsin->celsys);
  printf("celsysmat1--> %f\n",wcsin->celsysmat[0]);
  printf("celsysmat2--> %f\n",wcsin->celsysmat[1]);
  printf("celsysmat3--> %f\n",wcsin->celsysmat[2]);
  printf("celsysmat4--> %f\n",wcsin->celsysmat[3]);
  printf("celsysconvflag--> %d\n",wcsin->celsysconvflag);
  printf("obsdate--> %f\n",wcsin->obsdate);
  */


     /*
     wcsin->pixscale = cd1[0];
     printf("%f\n",wcsin->wcsmin[0]);
     printf("%f\n",rawpos[0]);
     printf("%e\n",wcsin->projp[0]);
     printf("%f\n",rawpos[1]);
     printf("%f\n",wcsin->pixscale);
     printf("%d\n",wcsin->naxisn[0]);
     printf("%d\n",wcsin->naxisn[1]);
     */
    
     /* Do the conversion */
     raw_to_wcs(wcsin,rawpos,wcspos);
     printf("%d,%2.8f,%2.8f,%s\n",imageId,wcspos[0],wcspos[1],metatable);

  }
  
  close(infile);

  return(0);

}
