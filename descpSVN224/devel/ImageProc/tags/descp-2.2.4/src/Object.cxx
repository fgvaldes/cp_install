#include "Object.h"
#include "TMath.h"
#include <iostream>



// double CoaddObject::AverageMag(const vector<Object> &vec)
// {

//   int size=vec.size();
//   if(size==0) return 0.0;
  
//   vector<double> mags;
//   vector<Object>::const_iterator iter=vec.begin();

//   for( ; iter!=vec.end(); ++iter) mags.push_back(iter->mag);

//   if(mags.size()==1) return mags[0];
//   if(median) return TMath::Median(size,&mags[0]);
//   else return TMath::Mean(size,&mags[0]);

// }


// double CoaddObject::AveragePhotoMag(const vector<Object> &vec)
// {

//    int size=vec.size();
//    if(size==0) return 0;
  
//    vector<double> mags;
//    vector<Object>::const_iterator iter=vec.begin();
  
//    for( ; iter!=vec.end(); ++iter) {
//      if(iter->photometric) {
//         mags.push_back(iter->mag);
//      }
//    }

//    if(mags.size()==1) return mags[0];
//    if(photo_median) return TMath::Median(size,&mags[0]);
//    else return TMath::Mean(size,&mags[0]);

// }



// double CoaddObject::AverageG()
// {
//   return AverageMag(g_band);

// }


// double CoaddObject::AverageR()
// {
//   return AverageMag(r_band);

// }


// double CoaddObject::AverageI()
// {
//   return AverageMag(i_band);

// }


// double CoaddObject::AverageZ()
// {
//   return AverageMag(z_band);

// }


// double CoaddObject::AverageY()
// {
//   return AverageMag(Y_band);

// }



// double CoaddObject::AveragePhotoG()
// {
//   return AveragePhotoMag(g_band);

// }


// double CoaddObject::AveragePhotoR()
// {
//   return AveragePhotoMag(r_band);

// }


// double CoaddObject::AveragePhotoI()
// {
//   return AveragePhotoMag(i_band);

// }


// double CoaddObject::AveragePhotoZ()
// {
//   return AveragePhotoMag(z_band);

// }


// double CoaddObject::AveragePhotoY()
// {
//   return AveragePhotoMag(Y_band);

// }


ostream& operator<<(ostream &out, ImageInfo const &T)
{
  return out << "(" << T.exp_name << ", " << T.ccd<<", "<<T.nite<<")\n";
}

