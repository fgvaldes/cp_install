#include "ExtinctionMap.h"
#include "TMath.h"
#include <iostream>

using namespace std;

ExtinctionMap::ExtinctionMap(string dir)
{
  
  
  FITS *fitsfile= new FITS(dir+"/SFD_dust_4096_ngp.fits");

  PHDU& table=fitsfile->pHDU();

  n_axis1=table.axis(0);
  n_axis2=table.axis(1);

  // assume ctype1 is GLON-ZEA and ctype2 is GLAT-ZEA

  table.readKey("CRVAL1",n_crval1);
  table.readKey("CRVAL2",n_crval2);
  table.readKey("CRPIX1",n_crpix1);
  table.readKey("CRPIX2",n_crpix2);


  // check for cdelt1, cdelt2
  try {
    double cdelt1,cdelt2;
    table.readKey("CDELT1",cdelt1);
    table.readKey("CDELT2",cdelt2);

    n_cd1_1=cdelt1;
    n_cd1_2=0.0;
    n_cd2_1=0.0;
    n_cd2_2=cdelt2;

  } 
  catch ( HDU::NoSuchKeyword ) {

    table.readKey("CD1_1",n_cd1_1);
    table.readKey("CD1_2",n_cd1_2);
    table.readKey("CD2_1",n_cd2_1);
    table.readKey("CD2_2",n_cd2_2);
  
  }

  try {table.readKey("LONPOLE",n_lonpole);}
  catch( HDU::NoSuchKeyword ) {n_lonpole=180.0;}

  table.read(north_ext);




  fitsfile= new FITS(dir+"/SFD_dust_4096_sgp.fits");

  table=fitsfile->pHDU();

  s_axis1=table.axis(0);
  s_axis2=table.axis(1);

  // assume ctype1 is GLON-ZEA and ctype2 is GLAT-ZEA

  table.readKey("CRVAL1",s_crval1);
  table.readKey("CRVAL2",s_crval2);
  table.readKey("CRPIX1",s_crpix1);
  table.readKey("CRPIX2",s_crpix2);


  // check for cdelt1, cdelt2
  try {
    double cdelt1,cdelt2;
    table.readKey("CDELT1",cdelt1);
    table.readKey("CDELT2",cdelt2);

    s_cd1_1=cdelt1;
    s_cd1_2=0.0;
    s_cd2_1=0.0;
    s_cd2_2=cdelt2;

  } 
  catch ( HDU::NoSuchKeyword ) {

    table.readKey("CD1_1",s_cd1_1);
    table.readKey("CD1_2",s_cd1_2);
    table.readKey("CD2_1",s_cd2_1);
    table.readKey("CD2_2",s_cd2_2);
  
  }

  try {table.readKey("LONPOLE",s_lonpole);}
  catch( HDU::NoSuchKeyword ) {s_lonpole=180.0;}

  table.read(south_ext);

  delete fitsfile;
  
}



void ExtinctionMap::ConvertToGal(double ra,double dec,
                                 double &gall,double &galb)
{
  gall=ra; galb=dec;
  wcscon(WCS_J2000,WCS_GALACTIC,0,0,&gall,&galb,2000.0);

  return;
}



double ExtinctionMap::GetExtinction(double ra,double dec)
{

  double gall,galb;

  ConvertToGal(ra,dec,gall,galb);

  int x,y;
  GetPixel(gall,galb,x,y);

  if(galb>=0.) return north_ext[n_axis1*y+x];
  else return south_ext[s_axis1*y+x];
}



void ExtinctionMap::GetPixel(double gall,double galb,int &x,int &y)
{

  double x_float,y_float;

  if(galb>=0.) {

    double theta,phi;

    // Rotation
    // Eqn (4) - degenerate case

    if(n_crval2 > 89.9999) {
      theta=galb;
      phi=gall+180.0+n_lonpole -n_crval1;
    }
    else if(n_crval2 < -89.9999) {
      theta=-galb;
      phi= n_lonpole+n_crval1-gall;
    }
    else {
      cout<<"Error: Unsupported projection (assuming NGP)"<<endl;
      theta=galb;
      phi=gall+180.0+n_lonpole -n_crval1;
    }

    // Put phi in range [0,360] degrees
    phi-phi-360.0*floor(phi/360.0);

    // Forward Map projection
    // Eqn (26)
    double Rtheta=2.*TMath::RadToDeg()*sin((0.5/TMath::RadToDeg())*
                                           (90.0-theta));
    
    // Eqns (10), (11)
    double xr=Rtheta*sin(phi/TMath::RadToDeg());
    double yr=-Rtheta*cos(phi/TMath::RadToDeg());
    
    double denom=n_cd1_1*n_cd2_2-n_cd1_2*n_cd2_1;
    x_float=(n_cd2_2 * xr - n_cd1_2 * yr) / denom + (n_crpix1 - 1.0);
    y_float=(n_cd1_1 * yr - n_cd2_1 * xr) / denom + (n_crpix2 - 1.0);
    
    x=(int)floor(xr+0.5);
    y=(int)floor(yr+0.5);

    if(y<0) y=0;
    if(x<0) x=0;

    if(x>=n_axis1) x=n_axis1-1;
    if(y>=n_axis2) y=n_axis2-1;
    
  }
  else {

    
    double theta,phi;

    // Rotation
    // Eqn (4) - degenerate case

    if(s_crval2 > 89.9999) {
      theta=galb;
      phi= gall + 180.0 + s_lonpole - s_crval1;
    }
    else if(s_crval2 < -89.9999) {
      theta = -galb;
      phi = s_lonpole + s_crval1 - gall;
    }
    else {
      cout<<"Error: Unsupported projection (assuming NGP)"<<endl;
      theta=galb;
      phi=gall+180.0 + s_lonpole -s_crval1;
    }
    
    // Put phi in range [0,360] degrees
    phi=phi - 360.0*floor(phi/360.0);

    // Forward Map projection
    // Eqn (26)
    double Rtheta=2.0*TMath::RadToDeg()*sin((0.5/TMath::RadToDeg())*
                                           (90.0-theta));
    
    // Eqns (10), (11)
    double xr= Rtheta*sin(phi/TMath::RadToDeg());
    double yr=-Rtheta*cos(phi/TMath::RadToDeg());

    double denom=s_cd1_1*s_cd2_2-s_cd1_2*s_cd2_1;
    x_float=(s_cd2_2 * xr - s_cd1_2 * yr) / denom + (s_crpix1 - 1.0);
    y_float=(s_cd1_1 * yr - s_cd2_1 * xr) / denom + (s_crpix2 - 1.0);
    
    x=(int)floor(x_float+0.5);
    y=(int)floor(y_float+0.5);

    

    if(y<0) y=0;
    if(x<0) x=0;

    if(x>=s_axis1) x=s_axis1-1;
    if(y>=s_axis2) y=s_axis2-1;
    
  }
  return; 
}
  



  
