#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "kdtree.h"

#define MAXLINE 5000


typedef struct Star{
	int id;
	double ra;
	double dec;
	double class_star;
	char band;
}Star;

typedef struct match{
	Star coadd;
	Star *objs;
}match;

void initFiles(void);
void cleanUp(void);
char *randstr(int);

double *eqToXYZ(double, double);

Star *readTile(char *);
void coadd2class_star_sqlpipe(Star *, char *);
Star *readObjects(void);
match *find_matches(Star *, Star *);
double **averageClassStars(match *);
void printCsToFile(char *, double **);

unsigned long numOfCoadds;
char *spoolfile, *queryfile, *pipefile;


int main(int nargs, char *args[]){
	Star *coadds;
	Star *objs;
	match *matches;
	int i;
	short minSet=-1;
	double lim;
	double goodratio;
	char *login=calloc(15, sizeof(char));	
	if (nargs<2){
		printf("\n  purpose: average class_star from the single-epoch objects used to create the coadds in the .objects grabmatches file. \n");
		printf("\n  usage: coadd2class_star <.objects file> [-desorch] \n"); 
		printf("\n  output: a .objects.class_star file with 4 extra columns: average g,r,i,z class_star from SE objects.");
		printf("\n  a value of 0.0000 for the class_star indicates that no SE matches were found. \n");
		exit(1);
	}
	for (i=2; i<nargs; i++){
		if (!strcmp(args[i], "-desorch")){
			strcpy(login, "desorch/stdes");
		}
		else{
			printf("\n  unrecognized option. \n"); exit(1);
		}
	}
	if (strcmp(login, "desorch/stdes")) strcpy(login, "desdb/prdes");
	initFiles();
	coadds = readTile(args[1]);
	coadd2class_star_sqlpipe(coadds, login);
	objs = readObjects();
	matches = find_matches(coadds, objs);
	printCsToFile(args[1], averageClassStars(matches));
	cleanUp();
}

void initFiles(void){
	char *suffix;
	srand(time(NULL));
	suffix = randstr(10);
	queryfile = calloc(MAXLINE, sizeof(char));
	spoolfile = calloc(MAXLINE, sizeof(char));
	pipefile = calloc(MAXLINE , sizeof(char));
	sprintf(queryfile, "/tmp/__find_matches_query_%s.sql", suffix);
	sprintf(spoolfile, "/tmp/__find_matches_spool_%s.lst", suffix);
	sprintf(pipefile, "/tmp/__find_matches_pipe_%s", suffix);
	free(suffix);
}

void cleanUp(void){
	char *cm = calloc(MAXLINE, sizeof(char));
	sprintf(cm, "rm %s", pipefile);   system(cm);
	sprintf(cm, "rm %s", queryfile);  system(cm);
	sprintf(cm, "rm %s", spoolfile);  system(cm);
	free(cm);
	free(queryfile);
	free(spoolfile);
	free(pipefile);
	printf("\n");fflush(stdout);
}

char *randstr(int len) {
	const char *chars = "abcdefghijklmnopqrstuvwxyz"
 						"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
 						"0123456789_";
 	int max = strlen(chars);
 	int i;
 	char *str = calloc(len+1, sizeof(char));
 
 	for (i=0 ; i < len; ++i ) {
 		str[i] = chars[rand()%max];
 	}
 	str[++i] = '\0';
 	return str;
}

Star *readTile(char *filename){
	FILE *fp;
	Star *data;
	unsigned long i=1;
	char *line = calloc(MAXLINE, sizeof(char));
	
	printf("\nreading tile...");fflush(stdout);
	
	if ((fp = fopen(filename, "r")) == NULL) {
		printf("\ninvalid file name.");
		exit(1);
	}
	while(fgets(line,MAXLINE,fp)!=NULL){
		i++;
	}
	numOfCoadds=i;
	data = calloc(i+1, sizeof(Star));
	printf("number of lines read in: %ld",numOfCoadds);fflush(stdout);
	fclose(fp);
	fp = fopen(filename, "r");
	for(i=0;fgets(line,MAXLINE,fp)!=NULL; i++){
		data[i].ra = strtod(strtok(line, " "), NULL);
		strtok(NULL, " ");
		data[i].dec = strtod(strtok(NULL, " "), NULL);
		data[i].id=0;
	}
	free(line);
	data[i].id=-1;
	return data;
}

void coadd2class_star_sqlpipe(Star *s, char *login){
	FILE *fp;
	int i;
	char *cm = calloc(MAXLINE, sizeof(char));
	double minra, maxra, mindec, maxdec;
    
    minra=s[0].ra;
	maxra=s[0].ra;
	mindec=s[0].dec;
	maxdec=s[0].dec;
	
	for (i=0; s[i].id!=-1; i++){
		if (s[i].ra<minra) minra=s[i].ra;
		if (s[i].ra>maxra) maxra=s[i].ra;
		if (s[i].dec<mindec) mindec=s[i].dec;
		if (s[i].dec>maxdec) maxdec=s[i].dec;
	}
	minra-=1.9392547243523283e-05;
	maxra+=1.9392547243523283e-05;
	mindec-=1.9392547243523283e-05;
	maxdec+=1.9392547243523283e-05;
	printf("\nquerying db for single epoch objects in ra dec rectangle: %lf %lf %lf %lf...", minra, maxra, mindec, maxdec); 
	fflush(stdout);
	
	if((fp = fopen(queryfile,"w"))==NULL){printf("\nfailed to open %s for writing, exiting.\n");exit(1);}
	fprintf(fp, "select ra, dec, class_star, band from objects_current where ra between %lf and %lf and dec between %lf and %lf;", 
	                minra, maxra, mindec, maxdec);
	fclose(fp);
	
	if((fp = fopen(pipefile, "w"))==NULL){printf("could not open %s for writing, exiting.", pipefile);exit(1);}
	fprintf(fp, "#!/bin/bash\n\n");
	fprintf(fp, "sqlplus -s pipeline/dc01user@%s <<! >/dev/null\n", login);
	fprintf(fp, "set linesize 200 trimspool on heading off echo off feedback off termout off verify off pagesize 0\n");
	fprintf(fp, "spool %s\n", spoolfile);
	fprintf(fp, "@%s", queryfile);
	fprintf(fp, "\nspool off\nexit\n!\n");
	fclose(fp);

	sprintf(cm, "chmod +x %s", pipefile); 	system(cm);
	sprintf(cm, pipefile); 					system(cm);
	return;
}

Star *readObjects(void){
	Star *objects;
	long i;
	FILE *fp;
	char *line = calloc(MAXLINE, sizeof(char));
	if ((fp=fopen(spoolfile, "r"))==NULL){printf("\n\nfailed to open %s for reading, exiting.\n",spoolfile);exit(1);}
	for(i=1; fgets(line,MAXLINE,fp)!=NULL; i++)
		;
	fclose(fp);
	objects = calloc(i+1, sizeof(Star));
	printf("read in %ld objects.",i);fflush(stdout);
	i=0;
	fp = fopen(spoolfile, "r");
	for(i=0; fgets(line, MAXLINE, fp)!=NULL; i++){
		objects[i].id = 0;
		objects[i].ra = strtod(strtok(line, " "), NULL);
		objects[i].dec = strtod(strtok(NULL, " "), NULL);
		objects[i].class_star=strtod(strtok(NULL, " "), NULL);
		objects[i].band=strtok(NULL, " ")[0]; 
	}
	objects[i].id=-1;
	fclose(fp);
	free(line);
	return objects;
}
    
match *find_matches(Star *coadds, Star *objects){
	match *m = calloc(numOfCoadds+1, sizeof(match));
	unsigned long i=1;
	FILE *fp;
	struct kdtree *kd = kd_create(3);
	struct kdres *neighbors;
	double *buf;
	char *line = calloc(MAXLINE, sizeof(char));
	int j;
	double distance_tol= 1.9392547e-05;/*2 arcsec in XYZ euclidean*/
	
	printf("\nmatching coadds with epoch objects...");fflush(stdout);
	for(i=0; objects[i].id!=-1; i++){
		buf = eqToXYZ(objects[i].ra, objects[i].dec);
		kd_insert(kd, buf, &(objects[i]));
		free(buf);
	}

	for (i=0; coadds[i].id!=-1; i++) {
		buf=eqToXYZ(coadds[i].ra, coadds[i].dec);
		neighbors=kd_nearest_range(kd, buf, distance_tol);
		free(buf);
		memcpy(&m[i].coadd,&coadds[i],sizeof(Star));
		m[i].objs = (Star*)calloc((int)neighbors->size + 1, sizeof(Star));
		for (j=0; !kd_res_end(neighbors); j++){
			memcpy(&(m[i].objs[j]),kd_res_item_data(neighbors),sizeof(Star));
			kd_res_next(neighbors);
		}
		m[i].objs[j].id=-1;
		kd_res_free(neighbors);
	}

	m[i].coadd.id=-1;
	return m;
}

double **averageClassStars(match *m){
	long a,j=0;
	double **cs = calloc(numOfCoadds+1, sizeof(double *));
	double g,r,i,z;
	int gc,rc,ic,zc;
	
	for (a=0; m[a].coadd.id!=-1; a++){
		cs[a] = calloc(4, sizeof(double));
		gc=0;rc=0;ic=0;zc=0;
		g=0;r=0;i=0;z=0;
		for (j=0; m[a].objs[j].id!=-1; j++){
			if (m[a].objs[j].band=='g'){gc++; g+=m[a].objs[j].class_star;}
			if (m[a].objs[j].band=='r'){rc++; r+=m[a].objs[j].class_star;}
			if (m[a].objs[j].band=='i'){ic++; i+=m[a].objs[j].class_star;}
			if (m[a].objs[j].band=='z'){zc++; z+=m[a].objs[j].class_star;}
		}
		cs[a][0]=(gc ? (double)g/(double)gc : 0.0);
		cs[a][1]=(rc ? (double)r/(double)rc : 0.0);
		cs[a][2]=(ic ? (double)i/(double)ic : 0.0);
		cs[a][3]=(zc ? (double)z/(double)zc : 0.0);
	}
	cs[a]=calloc(1,sizeof(double));
	cs[a][0]=-1;
	
	return cs;
}	

double *eqToXYZ(double ra, double dec){
	double *xyz=calloc(3, sizeof(double));
    double pr=3.141592653589793238/180.0;
    double cd=cos(dec*pr);
    xyz[0]=cos(ra*pr)*cd;
    xyz[1]=sin(ra*pr)*cd;
    xyz[2]=sin(dec*pr);
    return xyz;
}

void printCsToFile(char *tilename, double **cs){
	FILE *fp1;
	FILE *fp2;
	char *newname;
	int n=0;
	char *line = calloc(MAXLINE, sizeof(char *));
	printf("\nprinting to file %s_class_star",tilename);
	fp2 = fopen(tilename, "r");
	newname = calloc(strlen(tilename)+12, sizeof(char));
	memcpy(newname, tilename, strlen(tilename));
	strcat(newname, "_class_star");
	fp1 = fopen(newname, "w");
	while(cs[n][0]!=-1 && fgets(line, MAXLINE, fp2)!=NULL){
		if(line[strlen(line)-1]=='\n') line[strlen(line)-1]='\0';
		fprintf(fp1, strcat(line, " %2.4lf %2.4lf %2.4lf %2.4lf\n"), cs[n][0], cs[n][1], cs[n][2], cs[n][3]);
		n++;
	}
	free(line);
	free(newname);
	fclose(fp1);
	fclose(fp2);
}
