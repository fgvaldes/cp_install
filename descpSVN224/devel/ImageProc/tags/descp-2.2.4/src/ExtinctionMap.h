
#include <CCfits/CCfits>
#include <valarray>
#include "wcs.h"
using namespace CCfits;


class ExtinctionMap
{

public:

  ExtinctionMap(string _dir="./");
  
  double GetExtinction(double ra,double dec);

private:

  void GetPixel(double gall,double galb,int &x,int &y);
  void ConvertToGal(double ra,double dec,
                      double &gall,double &galb);

  valarray<double> north_ext,south_ext;

  // north variables
  double n_crval1,n_crval2,n_crpix1,n_crpix2,n_lonpole;
  double n_cd1_1,n_cd1_2,n_cd2_1,n_cd2_2;
  int n_axis1,n_axis2;

  // south variables
  double s_crval1,s_crval2,s_crpix1,s_crpix2,s_lonpole;
  double s_cd1_1,s_cd1_2,s_cd2_1,s_cd2_2;
  int s_axis1,s_axis2;
  
};
