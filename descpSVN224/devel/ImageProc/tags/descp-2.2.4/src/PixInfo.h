/*
 * PixInfo.h
 *
 * Simple class describing pixel information.
 * Used in PixTools.
 *
 *  Created on: Apr 3, 2012
 *      Author: Nikolay Kuropatkin           kuropat@fnal.gov
 */

#ifndef PIXINFO_H_
#define PIXINFO_H_
#include <vector>
#include "Vector3D.h"
namespace std {

class PixInfo {
public:
	Vector3D pixVect;
	double pixVertex[3][4];
	PixInfo();
	PixInfo(Vector3D a , double b[3][4]);
	void setPars(Vector3D a , double b[3][4]);
	virtual ~PixInfo();
};

inline PixInfo::PixInfo(void) {
	pixVect= Vector3D(0.,0.,0.);

	int i=0;
	int j = 0;
	for(i=0; i<3; i++) {
		for(j=0; j<4; j++) {
			PixInfo::pixVertex[i][j] = 0.;
		}
	}
}
inline PixInfo::PixInfo(Vector3D a, double b[3][4]) {
	pixVect = a;

	int i=0;
	int j = 0;
	for(i=0; i<3; i++) {
		for(j=0; j<4; j++) {
			PixInfo::pixVertex[i][j] = b[i][j];
		}
	}
}
inline void PixInfo::setPars(Vector3D a, double b[3][4] )
{
	pixVect = a;
	int i=0;
	int j = 0;
	for(i=0; i<3; i++) {
		for(j=0; j<4; j++) {
			PixInfo::pixVertex[i][j] = b[i][j];
		}
	}
}

inline PixInfo::~PixInfo() {
}
} /* namespace std */
#endif /* PIXINFO_H_ */
