/*--main

Basic syntax: roverscan <infile.fits> <outfile> <options>
  Input data:
    <infile.fits> = raw FITS image (i.e. one CCD image w/ overscan/bias present)
    <outfile> = output file name 

  Options:
    -overscansample <-1 for MEDIAN, 0 for MEAN, 1 for MEAN w/MINMAX>,
    -overscanfunction < -50 < N < -1 for cubic SPLINE, 0 for LINE_BY_LINE, 
                          1 < N < 50 for legendre polynomial>
    -overscanorder <1-6>, default=1 <order for legendre polynomial>
    -overscantrim <ncols>, default=0 
    -verbose <0-3>

Summary:
  roverscan characterizes and removes overscan (instrumental bias)

Detailed Description:

  The overscan sections (specified by BIASECA and BIASECB) are used to 
  evaluate and remove the instrumental bias.  A number of options are 
  possible which determine how the overscan is evaluated to accomplish 
  this.  Once the overscan has been subtracted for each amplifier, 
  the images are trimmed based on the TRIMSEC keyword prior to output. 
  The keyword DESOSCN is set to prevent subsequent processing from 
  repeating this action.

  The additional arguments necessary to invoke overscan (besides the -overscan
  option itself) are:

  -overscansample <N>
    Specifies how the overscan is initially characterized.  Here the choice 
    of N detemines how the pixel values in each overscan line are combined 
    (prior to further processing).  The options are:
       N=-1 --> will detemine the median value in each overscan line,
       N=0  --> will calculate the average value, and
       N=1  --> will calculate the average after rejecting the highest and 
                lowest value in each line.

  -overscantrim <ncols>, default=0
    Used to specify the number of start and end columns in the BIASEC regions 
    to ignore. For example -overscantrim 3 would ignore the first and last 
    three pixels in each line when evaluating the overscan.

  -overscanfunction <M>
    Specifies the method to evaluate the overscan (in the columnar direction).
    Choices are determined by the value of N (abs(N) is also used as a 
    binning/sampling factor). The behavior of the routine is as follows.  
    For:
      M<0 --> a cubic spline interpolation (after binning by abs(M) lines),
      M=0 --> causes the overscan to be evaluated on a line-by-line basis 
              (i.e. no fitting),
      M>0 --> will fit the overscan with a set of basis functions (Legendre 
              polynomials), after binning by abs(N) lines).

  -overscanorder <order>
    Is used to specify the order of the fit, order, that will be used for the 
    option -overscanfunction with M>0.  This controls the maximum order of 
    the basis functions (Legendre polynomials) that will be used in the fit.  
    Currently this is restricted to order<7 as higher order would potentially 
    be better characterized by the cubic spline option.

 -verbose (0-3,5)
    Verbosity functions as normal with the exception that if a value of 5 is
    used then a diagnostic flag is passed to the overscan subroutines which 
    causes diagnostic output showing the overscan value measured as well as
    the correction derived.

Known "Features":
  The current version does not update the WCS (based on the trimsec used).

*/

/// 
/// \file
/// \brief Command line interface for Ricardo's new overscan function.
///
///  OverScan combines bias frames into a composite bias correction that 
///  is mean to correct for residual structure perpendicular to the 
///  overscan column.  This command line utility takes a list of images
///  for input and applies the overscan correction by calling 
///  OverScan() from imarithsubs.c. 
///
#include "imageproc.h"

// Prototypes for external functions 
void OverScan(desimage *input_image,desimage *output_image,
	      overscan_config osconfig,int flag_verbose);


int CallOverScan(int argc,char *argv[])
{
  int i,j,hdutype,flag_verbose=1,
    mkpath(),keysexist,overscansample,overscanfunction,overscanorder,overscantrim;
  static int status=0;
  int	ccdnum=0,nkeys;
  void	printerror(),readimsections(),myoverscan();
  char	comment[1000],imagename[1000], keycard[100],
    longcomment[10000],obstype[200],oldvalue[1000],oldcomment[1000],
    event[10000],*striparchiveroot(),
    imtypename[6][10]={"","IMAGE","VARIANCE","MASK","SIGMA","WEIGHT"};
  float new_ltv1,new_ltv2,old_ltv1,old_ltv2;
  double new_crpix1,new_crpix2,old_crpix1,old_crpix2;
  void	rd_desimage(),shell(),decodesection(),headercheck(),
    reportevt(),image_compare();
  time_t	tm;
  desimage datain,output;
  fitsfile *fptr=NULL;
  int check_image_name();
  overscan_config osconfig;
  char	delkeys[100][10]={"PRESECA","PRESECB","POSTSECA",
			  "POSTSECB","TRIMSECA","TRIMSECB","TRIMSEC",
			  "BIASSECA","BIASSECB",""};
  
  if (argc<3) {
    printf(" %s  <input image> <output image> <options>\n",argv[0]);
    printf(" -overscansample <-1 for MEDIAN, 0 for MEAN, 1 for MEAN w/MINMAX>, \n");
    printf(" -overscanfunction < -50 < N < -1 for cubic SPLINE, 0 for LINE_BY_LINE, 1 < N < 50 for legendre polynomial>\n");
    printf(" -overscanorder <1-6>, default=1 <order for legendre polynomial>\n");
    printf(" -overscantrim <ncols>, default=0 <trim ncols at both edges of overscan, default = 0>\n");
    /* printf(" -bmp <image>\n");*/
    printf(" -verbose <0-3>\n");
    exit(1);
  }
	
  /* ****************************************************************** */
  /* ******************* Process Command Line ************************* */
  /* ****************************************************************** */
  osconfig.function = 0;
  osconfig.sample   = 0;
  osconfig.order    = 1;
  osconfig.trim     = 0;
  osconfig.debug    = 0;

  for (i=3;i<argc;i++) {
      
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if(flag_verbose == 5){
	flag_verbose = 3;
	osconfig.debug = 1;
      }
      if (flag_verbose<0 || flag_verbose>3) {
	sprintf(event, "Verbose level out of range %d. Reset to 2", flag_verbose);
	flag_verbose=2;
	reportevt(2,STATUS,3,event);
      }
    }
      
    if (!strcmp(argv[i],"-overscansample")) {
      sscanf(argv[++i],"%d",&overscansample);
      if (overscansample<-1 || overscansample>1) {
	sprintf(event,"Overscan sample must be -1, 0 or 1. Reset to 0");
	overscansample=0;
	reportevt(2,STATUS,3,event);
      }
      osconfig.sample   = overscansample; 
    }
               
    if (!strcmp(argv[i],"-overscanfunction")) {
      sscanf(argv[++i],"%d",&overscanfunction);
      if (overscanfunction<-51 || overscanfunction>51) {
	sprintf(event,"Overscan function must be <-50 to -1>, 0, or <1 to 50>. Reset to 0");
	overscanfunction=0; /*line by line*/
	reportevt(2,STATUS,3,event);
      }
      osconfig.function = overscanfunction;
    }
      
    if (!strcmp(argv[i],"-overscanorder")) {
      sscanf(argv[++i],"%d",&overscanorder);
      if (overscanorder<1 || overscanorder>6) {
	sprintf(event,"Overscan order must be <1-6>. Reset to 1");
	overscanorder=1; /* order for legendre fitting (line) */
	reportevt(2,STATUS,3,event);
      }
      osconfig.order    = overscanorder;
    } 
      
    if (!strcmp(argv[i],"-overscantrim")) {
      sscanf(argv[++i],"%d",&overscantrim);
      if (overscantrim > 20) {
	sprintf(event,"Overscan trim must be < 20 cols. Reset to 0");
	overscantrim=0;
	reportevt(2,STATUS,3,event);
      }
      osconfig.trim     = overscantrim;
    }
  }
  /* ****************************************************************** */
  /* ******************** Test input image **************************** */
  /* ****************************************************************** */
   
  /* check if image is fits, gz or fz. Copy input image name if fits file*/
  sprintf(imagename, "%s", argv[1]);
  if (!check_image_name(argv[1], REQUIRE_FITS, flag_verbose)) {
    reportevt(flag_verbose, STATUS, 5, "fits,fits.gz, or fits.fz image suffix required");
    exit(1);
  }
   
  /* open file and check header */
  if (fits_open_file(&fptr, imagename, READONLY, &status))  {
    sprintf(event, "Input image didn't open: %s", imagename);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
  /* confirm OBSTYPE */
  if (fits_read_key_str(fptr, "OBSTYPE", obstype, comment, &status) == KEY_NO_EXIST) {
    sprintf(event, "OBSTYPE keyword not found in %s", imagename);
    reportevt(flag_verbose, STATUS, 5, event);
    exit(1);
  }
  //
  // Disabled check on object type for overscan. Overscan anything.
  //
  //  if (strncmp(obstype, "raw_obj", 5) && strncmp(obstype, "RAW_OBJ", 5) && strncmp(obstype, "raw_bias", 5) && strncmp(obstype, "RAW_BIAS", 5)) {
  //    sprintf(event, "Input images doesn't have required  OBSTYPE='raw_bias' or OBSTYPE='raw_obj' in %s", imagename);
  //    reportevt(flag_verbose,STATUS,5,event);
  //    exit(1);
  //  }
  if (fits_close_file(fptr,&status)) {
    sprintf(event,"Input image didn't close: %s",imagename);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
  }
   
  /* ****************************************************************** */
  /* ********************* Test output image ************************** */
  /* ****************************************************************** */
   
  /* prepare output file - set to overwrite existing image if need be */
  sprintf(output.name,"!%s",argv[2]);
  /* check that it is a fits file */
  if (!check_image_name(argv[2], REQUIRE_FITS, flag_verbose)) {
    reportevt(flag_verbose, STATUS, 5, "fits image suffix required");
    exit(1);
  }
   
  /* ****************************************************************** */
  /* ********************** Begin processing ************************** */
  /* ****************************************************************** */

	

  /* ************************************************************** */
  /* ******* read input file and overscan subtract and trim ******* */
  /* ************************************************************** */

  /* copy input image name if FITS file*/
  sprintf(datain.name, "%s", argv[1]);

  /* read input image */
  rd_desimage(&datain, READONLY, flag_verbose);
   
  /* check image for ccdnumber */
  headercheck(&datain, "NOCHECK", &ccdnum, "NOCHECK", flag_verbose);
	
   
  /* retrieve basic image information from header */
  /* first make sure we are at the first extension */
  if (fits_movabs_hdu(datain.fptr, 1, &hdutype, &status)) {
    sprintf("Move to hdu=1 failed: %s", datain.name);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
   
  /* get the BIASSEC information */
  if (fits_read_key_str((datain.fptr), "BIASSECA", (datain.biasseca), comment, &status)
      ==KEY_NO_EXIST) {
    sprintf(event,"Keyword BIASSECA not defined in %s", datain.name);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
  decodesection((datain.biasseca), (datain.biassecan),flag_verbose);
   
  /* get the AMPSEC information */
  if (fits_read_key_str(datain.fptr, "AMPSECA", datain.ampseca, comment, &status)
      ==KEY_NO_EXIST) {
    sprintf(event,"Keyword AMPSECA not defined in %s",datain.name);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
  decodesection(datain.ampseca, datain.ampsecan, flag_verbose);
   
  /* get the BIASSEC information */
  if (fits_read_key_str(datain.fptr, "BIASSECB", datain.biassecb, comment, &status)
      ==KEY_NO_EXIST) {
    sprintf(event, "Keyword BIASSECB not defined in %s", datain.name);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
  decodesection(datain.biassecb, datain.biassecbn, flag_verbose);
   
  /* get the AMPSEC information */
  if (fits_read_key_str(datain.fptr, "AMPSECB", datain.ampsecb, comment, &status)
      ==KEY_NO_EXIST) {
    sprintf(event, "Keyword AMPSECB not defined in %s", datain.name);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
  decodesection(datain.ampsecb, datain.ampsecbn, flag_verbose);
   
  /* get the TRIMSEC information */
  if (fits_read_key_str(datain.fptr, "TRIMSEC", datain.trimsec, comment, &status)
      ==KEY_NO_EXIST) {
    sprintf(event, "Keyword TRIMSEC not defined in %s", datain.name);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
  decodesection(datain.trimsec, datain.trimsecn, flag_verbose);
   
  /* get the DATASEC information */
  if (fits_read_key_str(datain.fptr, "DATASEC", datain.datasec, comment, &status)
      ==KEY_NO_EXIST) {
    sprintf(event, "Keyword DATASEC not defined in %s", datain.name);
    reportevt(flag_verbose, STATUS, 5, event);
    printerror(status);
  }
  decodesection(datain.datasec, datain.datasecn, flag_verbose);
   
   
  /* report header parameters read */
  if (flag_verbose==3) {
    sprintf(event,"BIASSECA=%s AMPSECA=%s BIASSECB=%s AMPSECB=%s TRIMSEC=%s DATASEC=%s",
	    datain.biasseca, datain.ampseca, datain.biassecb, datain.ampsecb, datain.trimsec,
	    datain.datasec);        
    reportevt(flag_verbose, STATUS, 1, event);
  }
   
  /* ****************** OVERSCAN Section ********************* */
	
  /* check to see if DESOSCN keyword is set */
  if (fits_read_keyword(datain.fptr, "DESOSCN", comment, comment, &status) == KEY_NO_EXIST) {
    status=0;
    if (flag_verbose==3) {
      sprintf(event, "Overscan correcting: %s", datain.name);
      reportevt(flag_verbose,STATUS,1,event);
    }

    OverScan(&datain, &output, osconfig,flag_verbose);
      
    
    /* update dataset within the output image */
    sprintf(output.datasec,"[%d:%ld,%d:%ld]",1,output.axes[0],
	    1,output.axes[1]);
      
    output.datasecan[0] = (datain.ampsecan[0] < datain.ampsecan[1] ?
				 datain.ampsecan[0] : datain.ampsecan[1]);
    output.datasecan[1] = (datain.ampsecan[0] < datain.ampsecan[1] ?
				 datain.ampsecan[1] : datain.ampsecan[0]);
    output.datasecan[2] = (datain.ampsecan[2] < datain.ampsecan[3] ?
				 datain.ampsecan[2] : datain.ampsecan[3]);
    output.datasecan[3] = (datain.ampsecan[2] < datain.ampsecan[3] ?
				 datain.ampsecan[3] : datain.ampsecan[2]);
    output.datasecbn[0] = (datain.ampsecbn[0] < datain.ampsecbn[1] ?
				 datain.ampsecbn[0] : datain.ampsecbn[1]);
    output.datasecbn[1] = (datain.ampsecbn[0] < datain.ampsecbn[1] ?
				 datain.ampsecbn[1] : datain.ampsecbn[0]);
    output.datasecbn[2] = (datain.ampsecbn[2] < datain.ampsecbn[3] ?
				 datain.ampsecbn[2] : datain.ampsecbn[3]);
    output.datasecbn[3] = (datain.ampsecbn[2] < datain.ampsecbn[3] ?
				 datain.ampsecbn[3] : datain.ampsecbn[2]);
      
    sprintf(output.dataseca,"[%d:%d,%d:%d]",output.datasecan[0],
	    output.datasecan[1],output.datasecan[2],
	    output.datasecan[3]);
    sprintf(output.datasecb,"[%d:%d,%d:%d]",output.datasecbn[0],
	    output.datasecbn[1],output.datasecbn[2],
	    output.datasecbn[3]);

    if (output.image == NULL) {
      sprintf(event, "Calloc failed for output.image");
      reportevt(flag_verbose, STATUS, 5, event);
      exit(1);
    }
      
    if (flag_verbose) {
      sprintf(event, "Writing results to %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 1, event);
      fflush(stdout);
    }
      
    /* make sure path exists for new image */
    if (mkpath(output.name, flag_verbose)) {
      sprintf(event, "Failed to create path to file: %s", output.name+1);
      reportevt(flag_verbose, STATUS, 5, event);
      exit(1);
    }
    else {
      sprintf(event, "Created path to file: %s", output.name+1);
      reportevt(flag_verbose, STATUS, 1, event);
    }
      
    /* create the (image) file */
    if (fits_create_file(&output.fptr, output.name, &status)) {
      sprintf(event, "Creating file failed: %s", output.name);
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }

    /* create image extension */
    if (fits_create_img(output.fptr, FLOAT_IMG, 2, output.axes, &status)) {
      sprintf(event, "Creating image failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
      
      
    /* first read the number of keywords */
    if (fits_get_hdrspace(datain.fptr,&keysexist,&j,&status)) {
      sprintf(event,"Retrieving header failed: %s",datain.name);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
    }
    sprintf(event,"Copying %d keywords into output image",
	    keysexist);
    reportevt(flag_verbose,STATUS,1,event);
    /* reset to beginning of output header space */
    if (fits_read_record(output.fptr,0,keycard,&status)) {
      reportevt(flag_verbose,STATUS,5,
		"Reset to start of header failed");
      printerror(status);
    }
    if (fits_read_record(datain.fptr,0,keycard,&status)) {
      reportevt(flag_verbose,STATUS,5,"Reading header record failed");
      printerror(status);
    }
    for (j=1;j<=keysexist;j++) {
      if (fits_read_record(datain.fptr,j,keycard,&status)) {
	sprintf(event,"Reading header record %d failed",j);
	reportevt(flag_verbose,STATUS,5,event);
	printerror(status);
      }
         
      if (strncmp(keycard,"BITPIX  ",8) &&
	  strncmp(keycard,"NAXIS",5)    &&
	  strncmp(keycard,"PCOUNT  ",8) &&
	  strncmp(keycard,"EXTEND  ",8) &&
	  strncmp(keycard,"GCOUNT  ",8) &&
	  strncmp(keycard,"COMMENT   FITS (Flexible Image",30) &&
	  strncmp(keycard,"COMMENT   and Astrophysics', v",30) &&
	  strncmp(keycard,"EXTNAME ",8) &&
	  strncmp(keycard,"BSCALE  ",8) &&
	  strncmp(keycard,"BZERO   ",8)
	  ) {
	if (fits_write_record(output.fptr,keycard,&status)) {
	  sprintf(event,"Writing record failed: %s",keycard);
	  reportevt(flag_verbose,STATUS,5,event);
	  printerror(status);
	}
      }
    }
    /* remove unneeded information from the header */
    nkeys=0;
    while (strlen(delkeys[nkeys])) {
      if (fits_read_keyword(output.fptr,delkeys[nkeys],
			    comment,comment,&status)==KEY_NO_EXIST) status=0;
      else {
	if (fits_delete_key(output.fptr,delkeys[nkeys],&status)) {
	  if (flag_verbose) {
	    sprintf(event,"Keyword %s not deleted from image %s",
		    delkeys[nkeys],output.name+1);
	    reportevt(flag_verbose,STATUS,3,event);
	  }
	  status=0;
	}
      }
      nkeys++;
    }

    /* update selected keywords with new information */
    if (fits_update_key_str(output.fptr,"DATASEC",output.datasec,
			    "Data section within image",&status)) {
      sprintf(event,"Updating DATASEC failed: %s",output.name+1);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
    }
    if (fits_update_key_str(output.fptr,"DATASECA",output.dataseca,
			    NULL,&status)) {
      sprintf(event,"Updating DATASECA failed: %s",output.name+1);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
    }
    if (fits_update_key_str(output.fptr,"DATASECB",output.datasecb,
			    NULL,&status)) {
      sprintf(event,"Updating DATASECB failed: %s",output.name+1);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
    }

/*    printf(" RAG trim_sec: %d:%d,%d:%d \n",datain.trimsecn[0],datain.trimsecn[1],datain.trimsecn[2],datain.trimsecn[3]); */

/*  Currently the best solution for LTV1 and LTV2 is to set them to zero */

    if (fits_update_key_flt(output.fptr,"LTV1",0.0,2,NULL,&status)) {
      sprintf(event,"Update LTV1 failed: %s",output.name+1);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
    }
    if (fits_update_key_flt(output.fptr,"LTV2",0.0,2,NULL,&status)) {
      sprintf(event,"Update LTV2 failed: %s",output.name+1);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
    }  

/*  This is a new section to update LTV1, LTV2 based on how an image is trimmed. */
#if 0
    if (fits_read_key_flt(output.fptr,"LTV1",&old_ltv1,oldcomment,&status)==KEY_NO_EXIST){
      status=0;
      new_ltv1=0.0;
      if (fits_write_key(output.fptr,TFLOAT,"LTV1",&new_ltv1,NULL,&status)){
        sprintf(event,"Adding LTV1 failed: %s",output.name+1);
        reportevt(flag_verbose,STATUS,5,event);
        printerror(status);
      } 
    }else{
      new_ltv1=old_ltv1-(datain.trimsecn[0]-1.0);
      if (fits_update_key(output.fptr,TFLOAT,"LTV1",&new_ltv1,NULL,&status)){
        sprintf(event,"Update LTV1 failed: %s",output.name+1);
        reportevt(flag_verbose,STATUS,5,event);
        printerror(status);
      }
/*      printf(" RAG new LTV1: %f --> %f \n",old_ltv1,new_ltv1);  */
    }  

    if (fits_read_key_flt(output.fptr,"LTV2",&old_ltv2,oldcomment,&status)==KEY_NO_EXIST){
      status=0;
      new_ltv2=0.0;
      if (fits_write_key(output.fptr,TFLOAT,"LTV2",&new_ltv2,NULL,&status)){
        sprintf(event,"Adding LTV2 failed: %s",output.name+1);
        reportevt(flag_verbose,STATUS,5,event);
        printerror(status);
      } 
    }else{
      new_ltv2=old_ltv2-(datain.trimsecn[2]-1.0);
      if (fits_update_key(output.fptr,TFLOAT,"LTV2",&new_ltv2,NULL,&status)){
        sprintf(event,"Update LTV2 failed: %s",output.name+1);
        reportevt(flag_verbose,STATUS,5,event);
        printerror(status);
      }
/*      printf(" RAG new LTV2: %f --> %f \n",old_ltv2,new_ltv2);  */
    }
#endif

/*  This is a new section to update CRPIX1, CRPIX2 when an image is trimmed. */
#if 0

    if (fits_read_key_dbl(output.fptr,"CRPIX1",&old_crpix1,oldcomment,&status)==KEY_NO_EXIST){
      status=0;
      sprintf(event,"No CRPIX1 keyword detected in image: %s",output.name+1);
      reportevt(flag_verbose,STATUS,3,event);
    }else{ 
      new_crpix1=old_crpix1-(datain.trimsecn[0]-1.0);
      if (fits_update_key(output.fptr,TDOUBLE,"CRPIX1",&new_crpix1,NULL,&status)){
        sprintf(event,"Update CRPIX1 failed: %s",output.name+1);
        reportevt(flag_verbose,STATUS,5,event);
        printerror(status);
      }
/*      printf(" RAG new CRPIX1: %lf --> %lf \n",old_crpix1,new_crpix1);  */
    } 

    if (fits_read_key_dbl(output.fptr,"CRPIX2",&old_crpix2,oldcomment,&status)==KEY_NO_EXIST){
      status=0;
      sprintf(event,"No CRPIX2 keyword detected in image: %s",output.name+1);
      reportevt(flag_verbose,STATUS,3,event);
    }else{ 
      new_crpix2=old_crpix2-(datain.trimsecn[2]-1.0);
      if (fits_update_key(output.fptr,TDOUBLE,"CRPIX2",&new_crpix2,NULL,&status)){
        sprintf(event,"Update CRPIX2 failed: %s",output.name+1);
        reportevt(flag_verbose,STATUS,5,event);
        printerror(status);
      }
/*      printf(" RAG new CRPIX2: %lf --> %lf \n",old_crpix2,new_crpix2);  */
    } 
#endif

    /* write the corrected image*/
    if (fits_write_img(output.fptr, TFLOAT, 1, output.npixels, output.image, &status)) {
      sprintf(event, "Writing image failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
      
    /* write basic information into the header */
    /* RAG removed because this is not a valid DES image tupe and the value should have been populated elsewhere */     
/*    if (fits_write_key_str(output.fptr, "OBSTYPE", "overscancor", "Observation type", &status)) {
      sprintf(event, "Setting OBSTYPE=overscancor failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
*/
    /* RAG altered because this keyword should already exist */
/*
    if (fits_write_key_lng(output.fptr, "CCDNUM", ccdnum, "CCD Number", &status)) {
      sprintf(event, "Setting CCDNUM failed: %d", ccdnum);
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
*/
      
    /* Write processing history into the header */
    /* get system time */
    tm = time(NULL);
    sprintf(comment, "%s", asctime(localtime(&tm)));
    comment[strlen(comment) - 1] = 0;
    if (fits_write_key_str(output.fptr, "DESOSCOR", comment, "image from overscancor", &status)) {
      sprintf(event, "Writing DESOSCOR failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
    sprintf(longcomment, "DESDM:");
    for (i = 0; i < argc; i++) sprintf(longcomment, "%s %s", longcomment, argv[i]);
    if (flag_verbose) {
      sprintf(event, "%s", longcomment);
      reportevt(flag_verbose, STATUS, 1, event);
    }
    if (fits_write_comment(output.fptr, longcomment, &status)) {
      sprintf(event, "Writing roverscan call failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
    sprintf(longcomment, "DESDM: (%d) ", 1);
    sprintf(longcomment, "%s %s ", longcomment, striparchiveroot(datain.name));
    if (flag_verbose) {
      sprintf(event, "%s", longcomment);
      reportevt(flag_verbose, STATUS, 1, event);
    }
    if (fits_write_comment(output.fptr, longcomment, &status)) {
      sprintf(event, "Writing overscan corrected output image failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
    /* RAG added check to see if DES_EXT keyword already exists, if not then add */

    if (fits_read_key_str(output.fptr,"DES_EXT",oldvalue,oldcomment,&status)==KEY_NO_EXIST){
       status=0;
       if (fits_write_key_str(output.fptr,"DES_EXT",imtypename[DES_IMAGE],"Image extension",&status)){
          sprintf(event, "Writing DES_EXT=%s failed: %s", imtypename[DES_IMAGE], &(output.name[1]));
          reportevt(flag_verbose, STATUS, 5, event);
          printerror(status);
       }
    }
/*    if (fits_write_key_str(output.fptr, "DES_EXT", imtypename[DES_IMAGE], "Image extension", &status)) {
      sprintf(event, "Writing DES_EXT=%s failed: %s", imtypename[DES_IMAGE], &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }  */
    if (fits_write_key_str(output.fptr,"DESOSCN",comment, "overscan corrected",&status)) {
      sprintf(event, "Setting DESOSCN=overscan corrected failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
    /* close the corrected image */
    if (fits_close_file(output.fptr, &status)) {
      sprintf(event, "Closing overscancor image failed: %s", &(output.name[1]));
      reportevt(flag_verbose, STATUS, 5, event);
      printerror(status);
    }
    if (flag_verbose) {
      sprintf(event,"Closed %s: 2D ( %ld X %ld )", &(output.name[1]), output.axes[0], output.axes[1]);
      reportevt(flag_verbose, STATUS, 1, event);
    }
  } /*close of overscan correction and trimming*/
  else { /* image has already been OVERSCAN corrected and trimmed */
    /* don't do anything to it */
    if (flag_verbose) { 
      sprintf(event, "Already overscan corrected: %s", datain.name);
      reportevt(flag_verbose, STATUS, 1, event);
    }
  }
   
   
  /* ********************************************************** */
  /* ************ free internal arrays ************************ */
  /* ********************************************************** */
  free(output.image);
  free(datain.image);

  return(0);
}

int main(int argc,char *argv[])
{
  return(CallOverScan(argc,(char **)argv));
}
