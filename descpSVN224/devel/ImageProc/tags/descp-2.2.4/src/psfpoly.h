 /*
 				poly.h

*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*
*	Part of:	A program using Polynomials
*
*	Author:		E.BERTIN (IAP) 
*
*	Contents:	Include for poly.c
*
*	Last modify:	13/05/2009
*
*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/

#ifndef _POLY_H_
#define _POLY_H_

/*--------------------------------- constants -------------------------------*/

#define	POLY_MAXDIM		4	/* Max dimensionality of polynom */
#define POLY_MAXDEGREE		10	/* Max degree of the polynom */
#define	POLY_TINY		1e-30	/* A tiny number */

/*---------------------------------- macros ---------------------------------*/

/*--------------------------- structure definitions -------------------------*/

typedef struct poly
  {
  double	*basis;		/* Current values of the basis functions */
  double	*orthobasis;	/* Curr orthonormalized basis function values */
  double	*coeff;		/* Polynom coefficients */
  int		ncoeff;		/* Number of coefficients */
  int		*group;		/* Groups */
  int		ndim;		/* dimensionality of the polynom */
  int		*degree;	/* Degree in each group */
  int		ngroup;		/* Number of different groups */
  double	*orthomat;	/* Orthonormalization matrix */
  double	*deorthomat;	/* "Deorthonormalization" matrix */
  }	polystruct;

/*---------------------------------- protos --------------------------------*/

extern polystruct	*poly_copy(polystruct *poly),
			*poly_init(int *group,int ndim,int *degree,int ngroup);

extern double		poly_func(polystruct *poly, double *pos);

extern int		*poly_powers(polystruct *poly);

extern void		poly_end(polystruct *poly);

#endif

