#include <CCfits>
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"
#include "TPRegexp.h"
#include "TStopwatch.h"
#include "TString.h"
#include "TSQLServer.h"
#include "TSQLStatement.h"
#include <cmath>
#include <iostream>
#include <valarray>
#include <vector>
#include <fstream>
#include "kdtree++/kdtree.hpp"


// #include "PsfCatalog.h"
// #include "FittedPsf.h"

// #include "ConfigFile.h"
// #include "fp.h" // Generated with xxd -i fitspar
// #include "dbg.h"

// #include "Bounds.h"
// //std::ostream* dbgout = 0;
// //bool XDEBUG = true;

// std::ostream* dbgout = 0;
// bool XDEBUG = true;

class Object{
public:
  typedef double value_type;
  Object(double _ra,double _dec,double _mag):ra(_ra),dec(_dec),mag(_mag)
  {
    double cd=cos(dec*TMath::DegToRad());
    xyz[0]=cos(ra*TMath::DegToRad())*cd;
    xyz[1]=sin(ra*TMath::DegToRad())*cd;
    xyz[2]=sin(dec*TMath::DegToRad());
  }

  Object(const Object &ob)
  { 
    ra=ob.ra;
    dec=ob.dec;
    mag=ob.mag;
    g_mag=ob.g_mag;
    r_mag=ob.r_mag;
    i_mag=ob.i_mag;
    z_mag=ob.z_mag;
    Y_mag=ob.Y_mag;
    xyz[0]=ob.xyz[0];
    xyz[1]=ob.xyz[1];
    xyz[2]=ob.xyz[2];
    class_star=ob.class_star;
  } 
  double xyz[3];
  
  double ra;
  double dec;
  double mag;
  double g_mag;
  double r_mag;
  double i_mag;
  double z_mag;
  double Y_mag;
  double class_star;

  inline double operator[](int const N) const {return xyz[N];}
};


using namespace std;
using namespace CCfits;
typedef KDTree::KDTree<3,Object> KD_Tree;
typedef std::pair<KD_Tree::iterator,double> KD_Match;

int main(int argc,char *argv[])
{

  TStopwatch timer;
  TString tshearfile;
  string run="";
  string listfile="";
  vector<TString> shear_files;
  string config_file;
  bool match_truth=false;
   
  for(int ii=1;ii<argc;ii++) {
    string arg=argv[ii];
    if(arg=="-file")            {tshearfile=argv[ii+1];ii++;}
    else if(arg=="-list")       {listfile=argv[ii+1];ii++;}
    else if(arg=="-se_run")     {run=argv[ii+1];ii++;}
    else if(arg=="-config_file")     {config_file=argv[ii+1];ii++;}
    else if(arg=="-match_truth")     {match_truth=true;ii++;}
  }

  double zp=-1;
  
  
  //  TPRegexp reg("(.*)(decam.*-(\\w)-.*)_(\\d{2})_shear.fits");
  TPRegexp reg("(.*)(obj.*)_(\\d{2})_shear.fits");

  //  TPRegexp reg2("(.*)/\\d{14}_\\d{8}/shapelet/decam.*_shear.fits");
  TPRegexp reg2("(.*)/obj.*_shear.fits");
  bool proper_motion=false;
  
  if(listfile.empty()) {
    shear_files.push_back(tshearfile);
  }
  else {
    
    ifstream infile(listfile.c_str());
    
    while(infile>>tshearfile) {
      shear_files.push_back(tshearfile);
    }
  }

  string star_rootfile="~/dc5_truth/dc5_stars.root";
  string gal_rootfile="~/dc5_truth/dc5_galaxies.root";
  
  TFile *starfile=new TFile(star_rootfile.c_str());
  TTree *stars=(TTree*) starfile->Get("stars");
  
  TFile *galfile=new TFile(gal_rootfile.c_str());
  TTree *gals=(TTree*) galfile->Get("galaxies");
  
  KD_Tree truth_tree;
  
//   // Do appropriate command line checks
//   //ExtinctionMap ExtMap(extinction_dir);
   double tol=2;
   tol*=1./3600*TMath::DegToRad();
   double truth_ra,truth_dec,truth_mag;
   double mu_ra,mu_dec;
   double truth_g,truth_r,truth_i,truth_z,truth_Y;
   int truth_class; // 0-Star, 1-Galaxy

  
  stars->SetBranchAddress("g",&truth_g);
  stars->SetBranchAddress("r",&truth_r);
  stars->SetBranchAddress("i",&truth_i);
  stars->SetBranchAddress("z",&truth_z);
  stars->SetBranchAddress("Y",&truth_Y);
    
  stars->SetBranchAddress("ra",&truth_ra);
  stars->SetBranchAddress("mura",&mu_ra);
  stars->SetBranchAddress("dec",&truth_dec);
  stars->SetBranchAddress("mudec",&mu_dec);
  int i=0;
  if(match_truth) { 
  while(stars->GetEntry(i)) {
    i++;
      
    Object obj(truth_ra,truth_dec,0);
    obj.class_star=0;
    obj.g_mag=truth_g;
    obj.r_mag=truth_r;
    obj.i_mag=truth_i;
    obj.z_mag=truth_z;
    obj.Y_mag=truth_Y;
    
    truth_tree.insert(obj);
  }
    
  gals->SetBranchAddress("g",&truth_g);
  gals->SetBranchAddress("r",&truth_r);
  gals->SetBranchAddress("i",&truth_i);
  gals->SetBranchAddress("z",&truth_z);
  gals->SetBranchAddress("Y",&truth_Y);
  gals->SetBranchAddress("ra",&truth_ra);
  gals->SetBranchAddress("dec",&truth_dec);
  i=0;
  while(gals->GetEntry(i)) {
    i++;
    Object obj(truth_ra,truth_dec,0);
    obj.class_star=1;
    obj.g_mag=truth_g;
    obj.r_mag=truth_r;
    obj.i_mag=truth_i;
    obj.z_mag=truth_z;
    obj.Y_mag=truth_Y;
    
    truth_tree.insert(obj);
    
  }
  }
  
  for(int i=0;i<shear_files.size();i++) {
    
    tshearfile=shear_files[i];
    
    //    string ardir=(((TObjString*)(reg2.MatchS(tshearfile))->
    //             At(1))->String()).Data();
    string ardir="/data/Archive/SPT/red/";


    string dir=(((TObjString*)(reg.MatchS(tshearfile))->
                 At(1))->String()).Data();
    string exp=(((TObjString*)(reg.MatchS(tshearfile))->
                 At(2))->String()).Data();
    //    string band=(((TObjString*)(reg.MatchS(tshearfile))->
    //             At(3))->String()).Data();
    //string ccd=(((TObjString*)(reg.MatchS(tshearfile))->
    //            At(4))->String()).Data();
    string ccd=(((TObjString*)(reg.MatchS(tshearfile))->
                At(3))->String()).Data();
    string band,object;
    cout<<dir<<" "<<exp<<" "<<band<<" "<<ccd<<endl;
    double cr1,cr2;
    if(!run.empty()) {
       
      TSQLServer *db = TSQLServer::Connect("oracle://desdb/stdes",
                                           "pipeline", "dc01user");
      
       string db_cmd=Form("select b.mag_zero,a.crval1,a.crval2,"
                          "a.band,c.object from image a,"
                          "zeropoint b,exposure c "
                          "where a.imagename='%s_%s.fits' and run='%s' and "
                          "a.imagetype='red' and b.imageid=a.id "
                          "and a.exposureid=c.id",
                          exp.c_str(),ccd.c_str(),run.c_str());

       TSQLStatement *stmt=db->Statement(db_cmd.c_str());
       cout<<db_cmd<<endl;
       stmt->Process();
       stmt->StoreResult();
       stmt->NextResultRow();
       zp=stmt->GetDouble(0);
       cr1=stmt->GetDouble(1);
       cr2=stmt->GetDouble(2);
       band=stmt->GetString(3);
       object=stmt->GetString(4);
       db->Close();
       cout<<"Got zp="<<zp<<endl;
       cout<<"Got cr1="<<cr1<<"  cr2="<<cr2<<endl;
       cout<<"Got band:"<<band<<" "<<object<<endl;
     }


     string allfile=dir+exp+"_"+ccd+"_shpltall.fits";
     string psffile=dir+exp+"_"+ccd+"_shpltpsf.fits";
     string modelfile=dir+exp+"_"+ccd+"_psfmodel.fits";
     string shearfile=dir+exp+"_"+ccd+"_shear.fits";

     string redfile=ardir+"/"+run+"/red/"+exp+"/"+exp+"_"+ccd+"_cat.fits";
     cout<<redfile<<endl;
    

     std::auto_ptr<FITS> allFile(new FITS(allfile.c_str()));
     std::auto_ptr<FITS> psfFile(new FITS(psffile.c_str()));
     std::auto_ptr<FITS> shearFile(new FITS(shearfile.c_str()));
     std::auto_ptr<FITS> modelFile(new FITS(modelfile.c_str()));
     std::auto_ptr<FITS> redFile(new FITS(redfile.c_str()));

     ExtHDU& all_table = allFile->extension(1);
     ExtHDU& psf_table = psfFile->extension(1);
     ExtHDU& shear_table = shearFile->extension(1);
     ExtHDU& model_table = modelFile->extension(1);
     ExtHDU& red_table = redFile->extension(2);
     ExtHDU& red_main = redFile->extension(1);

     int N_all=all_table.rows();
     int N_psf=psf_table.rows();
     int N_shear=shear_table.rows();
     int N_model=model_table.rows();
     int N_red=red_table.rows();

     

     

     
//      red_table.readKey("CRPIX1",cr1);
//      red_table.readKey("CRPIX2",cr2);

     //red_main.readKey("OBJECT",object);



     char  texp[20],tccd[20],tband[20];
     sprintf(texp,"%s_%s",object.c_str(),band.c_str());
     sprintf(tccd,"%s",ccd.c_str());
     sprintf(tband,"%s",band.c_str());
     // main shapeletall tree
     vector<int> id_vec,size_flags_vec,star_flag_vec;
     vector<double> x_vec,y_vec,sky_vec,noise_vec,mag_vec,sigma0_vec;
   
     // psf tree
     vector<int> psf_flags_vec,psf_order_vec,psf_id_vec;
     vector<double>  psf_nu_vec,sigma_p_vec;
     vector<valarray<double> > shapelets_vec(66);

     // shear tree
     vector<int> shear_flags_vec,gal_order_vec;
     vector<double> ra_vec,dec_vec,shear1_vec,shear2_vec,shear_nu_vec,
       shear_cov00_vec,shear_cov01_vec,shear_cov11_vec,
       shapelet_sigma_vec;
     vector<valarray<double> > shapelets_prepsf_vec(28);

     // from red cats
     vector<double> mag_model_vec,mag_psf_vec,mag_auto_vec,
       magerr_model_vec,magerr_psf_vec,magerr_auto_vec,class_star_vec,
       spread_model_vec,spread_model_err_vec,ellip1_image_vec,
       ellip1_world_vec,ellip2_image_vec,ellip2_world_vec;


     int id,size_flags,star_flag;
     double x,y,x_f,y_f,sky,noise,mag,sigma0;
   
     // psf tree
     int psf_flags,psf_order,psf_id;
     double  psf_nu,sigma_p,shapelets[66],interp_shapelets[66];

     // shear tree
     int shear_flags,gal_order;
     double ra,dec,shear1,shear2,shear_nu,shear_cov00,
       shear_cov01,shear_cov11,shapelet_sigma,shapelets_prepsf[28];

     
     double mag_model,mag_psf,mag_auto,
       magerr_model,magerr_psf,magerr_auto,class_star,
       spread_model,spread_model_err,ellip1_image,
       ellip1_world,ellip2_image,ellip2_world; 

     string outfile=object+"-"+band+"_"+ccd+".root";
     TFile tfile(outfile.c_str(),"recreate");

     TTree *tree=new TTree("tree","");
     tree->Branch("exposure",&texp,"exposure/C");
     tree->Branch("ccd",&tccd,"ccd/C");
     tree->Branch("band",&tband,"band/C");
     tree->Branch("id",&id,"id/I");
     tree->Branch("size_flags",&size_flags,"size_flags/I");
     tree->Branch("star_flag",&star_flag,"star_flag/I");
     tree->Branch("x",&x,"x/D");
     tree->Branch("y",&y,"x/D");
     tree->Branch("x_f",&x_f,"x_f/D");
     tree->Branch("y_f",&y_f,"y_f/D");
     tree->Branch("sky",&sky,"sky/D");
     tree->Branch("noise",&noise,"noise/D");
     tree->Branch("mag",&mag,"mag/D");
     tree->Branch("sigma0",&sigma0,"sigma0/D");
     tree->Branch("psf_flags",&psf_flags,"psf_flags/D");
     tree->Branch("psf_order",&psf_order,"psf_order/D");
     tree->Branch("psf_nu",&psf_nu,"psf_nu/D");
     tree->Branch("sigma_p",&sigma_p,"sigma_p/D");
     tree->Branch("shapelets",&shapelets,"shapelets[66]/D");
     //tree->Branch("interp_shapelets",&interp_shapelets,"interp_shapelets[66]/D");
     tree->Branch("shear_flags",&shear_flags,"shear_flags/I");
     tree->Branch("gal_order",&gal_order,"gal_order/I");
     tree->Branch("ra",&ra,"ra/D");
     tree->Branch("dec",&dec,"dec/D");
     tree->Branch("shear1",&shear1,"shear1/D");
     tree->Branch("shear2",&shear2,"shear2/D");
     tree->Branch("shear_nu",&shear_nu,"shear_nu/D");
     tree->Branch("shear_cov00",&shear_cov00,"shear_cov00/D");
     tree->Branch("shear_cov01",&shear_cov01,"shear_cov01/D");
     tree->Branch("shear_cov11",&shear_cov11,"shear_cov11/D");
     tree->Branch("shapelet_sigma",&shapelet_sigma,"shapelet_sigma/D");
     tree->Branch("shapelets_prepsf",&shapelets_prepsf,"shapelets_prepsf[28]/D");
     tree->Branch("truth_ra",&truth_ra,"truth_ra/D");
     tree->Branch("truth_dec",&truth_dec,"truth_dec/D");
     tree->Branch("truth_mag",&truth_mag,"truth_mag/D");
     tree->Branch("truth_class",&truth_class,"truth_class/I");
     
     tree->Branch("mag_model",&mag_model,"mag_model/D");
     tree->Branch("magerr_model",&magerr_model,"magerr_model/D");
     tree->Branch("mag_psf",&mag_psf,"mag_psf/D");
     tree->Branch("magerr_psf",&magerr_psf,"magerr_psf/D");
     tree->Branch("mag_auto",&mag_auto,"mag_auto/D");
     tree->Branch("magerr_auto",&magerr_auto,"magerr_auto/D");
     //tree->Branch("class_star",&class_star,"class_star/D");
     tree->Branch("spread_model",&spread_model,"spread_model/D");
     tree->Branch("spread_model_err",&spread_model_err,"spread_model_err/D");
     tree->Branch("ellip1_image",&ellip1_image,"ellip1_image/D");
     tree->Branch("ellip2_image",&ellip2_image,"ellip2_image/D");
     tree->Branch("ellip1_world",&ellip1_world,"ellip1_world/D");
     tree->Branch("ellip2_world",&ellip2_world,"ellip2_world/D");
     

     // psfmodel 
     int psfmodel_order,fit_order,npca;
     double sigma,xmin,xmax,ymin,ymax,ave_psf[66];
     double rot_matrix[66][63],interp_matrix[63][6];


     vector<int> psfmodel_order_vec,fit_order_vec,npca_vec;
     vector<double> sigma_vec,xmin_vec,xmax_vec,ymin_vec,ymax_vec;
     vector<valarray<double> > ave_psf_vec;
     vector<valarray<double> > rot_matrix_vec,interp_matrix_vec;

   
     TTree *model_tree=new TTree("model_tree","");
     model_tree->Branch("psf_order",&psfmodel_order,"psf_order/I");
     model_tree->Branch("fit_order",&fit_order,"fit_order/I");
     model_tree->Branch("npca",&npca,"npca/I");
     model_tree->Branch("sigma",&sigma,"sigma/D");
     model_tree->Branch("xmin",&xmin,"xmin/D");
     model_tree->Branch("xmax",&xmax,"xmax/D");
     model_tree->Branch("ymin",&ymin,"ymin/D");
     model_tree->Branch("ymax",&ymax,"ymax/D");
     model_tree->Branch("ave_psf",&ave_psf,"ave_psf[66]/D");
     model_tree->Branch("rot_matrix",&rot_matrix,"rot_matrix[66][63]/D");
     model_tree->Branch("interp_matrix",&interp_matrix,"interp_matrix[63][6]/D");

     // all
     all_table.column("id").read(id_vec,0,N_all);
     all_table.column("size_flags").read(size_flags_vec,0,N_all);
     all_table.column("star_flag").read(star_flag_vec,0,N_all);
     all_table.column("x").read(x_vec,0,N_all);
     all_table.column("y").read(y_vec,0,N_all);
     all_table.column("sky").read(sky_vec,0,N_all);
     all_table.column("noise").read(noise_vec,0,N_all);
     all_table.column("mag").read(mag_vec,0,N_all);
     all_table.column("sigma0").read(sigma0_vec,0,N_all);

   
     // shear
     shear_table.column("shear_flags").read(shear_flags_vec,0,N_shear);
     shear_table.column("gal_order").read(gal_order_vec,0,N_shear);
     //shear_table.column("ra").read(ra_vec,0,N_shear);
     //shear_table.column("dec").read(dec_vec,0,N_shear);
     shear_table.column("shear1").read(shear1_vec,0,N_shear);
     shear_table.column("shear2").read(shear2_vec,0,N_shear);
     shear_table.column("nu").read(shear_nu_vec,0,N_shear);
     shear_table.column("shear_cov00").read(shear_cov00_vec,0,N_shear);
     shear_table.column("shear_cov01").read(shear_cov01_vec,0,N_shear);
     shear_table.column("shear_cov11").read(shear_cov11_vec,0,N_shear);
     shear_table.column("shapelet_sigma").read(shapelet_sigma_vec,0,N_shear);
     shear_table.column("shapelets_prepsf").readArrays(shapelets_prepsf_vec,0,N_shear);

     // psf
     psf_table.column("id").read(psf_id_vec,0,N_psf);
     psf_table.column("psf_flags").read(psf_flags_vec,0,N_psf);
     psf_table.column("psf_order").read(psf_order_vec,0,N_psf);
     psf_table.column("nu").read(psf_nu_vec,0,N_psf);
     psf_table.column("sigma_p").read(sigma_p_vec,0,N_psf);
     psf_table.column("shapelets").readArrays(shapelets_vec,0,N_psf);


     // red
     red_table.column("ALPHAMODEL_J2000").read(ra_vec,0,N_red);
     red_table.column("DELTAMODEL_J2000").read(dec_vec,0,N_red);
    
     red_table.column("MAG_MODEL").read(mag_model_vec,0,N_red);
     red_table.column("MAGERR_MODEL").read(magerr_model_vec,0,N_red);
     red_table.column("MAG_PSF").read(mag_psf_vec,0,N_red);
     red_table.column("MAGERR_PSF").read(magerr_psf_vec,0,N_red);
     red_table.column("MAG_AUTO").read(mag_auto_vec,0,N_red);
     red_table.column("MAGERR_AUTO").read(magerr_auto_vec,0,N_red);
     //red_table.column("CLASS_STAR").read(class_star_vec,0,N_red);
     red_table.column("SPREAD_MODEL").read(spread_model_vec,0,N_red);

     red_table.column("SPREADERR_MODEL").read(spread_model_err_vec,0,N_red);

     red_table.column("ELLIP1MODEL_IMAGE").read(ellip1_image_vec,0,N_red);
     red_table.column("ELLIP2MODEL_IMAGE").read(ellip2_image_vec,0,N_red);
     red_table.column("ELLIP1MODEL_WORLD").read(ellip1_world_vec,0,N_red);
     red_table.column("ELLIP2MODEL_WORLD").read(ellip2_world_vec,0,N_red);




     double mjd=0;
     // if(proper_motion) {
//        TString get_mjd=gSystem->GetFromPipe(Form("grep -P -o -a 'MJD-OBS.*\\MJD of observation' %s",redfile.c_str()));
       
//        TObjArray *obj = TPRegexp("MJD-OBS =\\s+(\\d.*)\\s+/").MatchS(get_mjd);
       
//        const TString mjd_string = ((TObjString *)obj->At(1))->GetString();
//        mjd=mjd_string.Atof();
     //}
     

//      ConfigFile params(config_file);
//      std::string fp((const char*)fitsparams_config,fitsparams_config_len);
//      std::istringstream is(fp);
//      params.read(is);
     
     
//      std::cerr<<"Loading a PSFCatalog\n";
//      PsfCatalog psfCat(params);
//      psfCat.read(psffile);

     
//      std::cerr<<"Loading a FittedPSF\n";
//      FittedPsf fitPsf(params);
//      fitPsf.read(modelfile);

//      BVec iPsf(fitPsf.getPsfOrder(), fitPsf.getSigma());
     
     int psf_i=0;
     for(int i=0;i<N_all;i++) {
       cout<<i<<endl;
       id=id_vec[i]; 
       size_flags=size_flags_vec[i]; 
       star_flag=star_flag_vec[i];
       x=x_vec[i]; y=y_vec[i]; 
       x_f=(ra-cr1)*TMath::Cos(dec*TMath::DegToRad()); y_f=dec-cr2;
       sky=sky_vec[i]; 
       noise=noise_vec[i];
       mag=mag_vec[i];

       sigma0=sigma0_vec[i];
       shear_flags=shear_flags_vec[i]; 
       gal_order=gal_order_vec[i];
       ra=ra_vec[i]; 
       dec=dec_vec[i]; 
       shear1=shear1_vec[i]; 
       shear2=shear2_vec[i];
       shear_nu=shear_nu_vec[i];  
       shear_cov00=shear_cov00_vec[i];
       shear_cov00=shear_cov01_vec[i]; 
       shear_cov00=shear_cov11_vec[i];
       shapelet_sigma=shapelet_sigma_vec[i];

       mag_model=mag_model_vec[i];
       magerr_model=magerr_model_vec[i];
       mag_psf=mag_psf_vec[i];
       magerr_psf=magerr_psf_vec[i];
       mag_auto=mag_auto_vec[i];
       magerr_auto=magerr_auto_vec[i];
       //class_star=class_star_vec[i];
       spread_model=spread_model_vec[i];
       spread_model_err=spread_model_err_vec[i];
       ellip1_image=ellip1_image_vec[i];
       ellip2_image=ellip2_image_vec[i];
       ellip1_world=ellip1_world_vec[i];
       ellip2_world=ellip2_world_vec[i];

       if(zp>0) {
         mag+=(zp-25);
         mag_model+=(zp-25);
         mag_psf+=(zp-25);
         mag_auto+=(zp-25);
       }
         


       for(int j=0;j<28;j++) shapelets_prepsf[j]=shapelets_prepsf_vec[i][j];
      
       if(psf_i<N_psf) {
       if((i+1)==psf_id_vec[psf_i]) {
         cout<<i+1<<" "<<psf_i<<" "<<psf_id_vec[psf_i]<<endl;
         psf_flags=psf_flags_vec[psf_i];
         psf_order=psf_order_vec[psf_i];
         psf_nu=psf_nu_vec[psf_i];
         sigma_p=sigma_p_vec[psf_i];

         //fitPsf.interpolate(psfCat.getPos(psf_i), iPsf);
         for(int j=0;j<66;j++) {
           shapelets[j]=shapelets_vec[psf_i][j];
           //interp_shapelets[j]=iPsf(j);
         }
         psf_i++;
       } else {
         psf_flags=-999;
         psf_order=-999;
         psf_nu=-999;
         sigma_p=-999;
         for(int j=0;j<66;j++) shapelets[j]=-999;
       }
       }

	 // match to truth object
	 Object obj(ra,dec,mag);
	 
	 
	 // find closest match within tolerance
	 KD_Match match=truth_tree.find_nearest(obj,tol);
	 if(match.first!=truth_tree.end()) {
	   truth_ra=match.first->ra;
	   truth_dec=match.first->dec;
	   if(band=="g") {
	     truth_mag=match.first->g_mag;
	   }
	   else if(band=="r") {
	     truth_mag=match.first->r_mag;
	   }
	   else if(band=="i") {
	     truth_mag=match.first->i_mag;
	   }
	   else if(band=="z") {
	     truth_mag=match.first->z_mag;
	   }
	   else if(band=="Y") {
	     truth_mag=match.first->Y_mag;
	   }
	   
	   truth_class=(int)match.first->class_star;
	 }
       else {
         truth_ra=-999;
         truth_dec=-999;
         truth_mag=-999;
         truth_class=-999;
       }
       
       tree->Fill();
     }


     model_table.column("psf_order").read(psfmodel_order_vec,0,1);
     model_table.column("fit_order").read(fit_order_vec,0,1);
     model_table.column("npca").read(npca_vec,0,1);
     model_table.column("sigma").read(sigma_vec,0,1);
     model_table.column("xmax").read(xmax_vec,0,1);
     model_table.column("xmin").read(xmin_vec,0,1);
     model_table.column("ymax").read(ymax_vec,0,1);
     model_table.column("ymin").read(ymin_vec,0,1);
     model_table.column("ave_psf").readArrays(ave_psf_vec,0,1);
     model_table.column("rot_matrix").readArrays(rot_matrix_vec,0,1);
     model_table.column("interp_matrix").readArrays(interp_matrix_vec,0,1);

     model_tree->GetEntry(0);
     psfmodel_order=psfmodel_order_vec[0];
     fit_order=fit_order_vec[0];
     npca=npca_vec[0];
     sigma=sigma_vec[0];
     xmax=xmax_vec[0];
     xmin=xmin_vec[0];
     ymax=ymax_vec[0];
     ymin=ymin_vec[0];
     for(int i=0;i<66;i++) ave_psf[i]=ave_psf_vec[0][i];
     for(int i=0;i<4168;i++) {
       rot_matrix[int(i/63)][i%63]=rot_matrix_vec[0][i];
     }
     for(int i=0;i<378;i++) {
       interp_matrix[int(i/6)][i%6]=interp_matrix_vec[0][i];
     }


     model_tree->Fill();

     tfile.cd();
     tree->Write();
     model_tree->Write();
     tfile.Close();
   }
   timer.Print();
}


   
   
