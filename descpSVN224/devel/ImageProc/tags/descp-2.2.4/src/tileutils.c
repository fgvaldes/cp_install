/*
**
** tileutils.c
**
** DESCRIPTION:
**
** AUTHOR:  Tony Darnell (tdarnell@uiuc.edu)
** DATE:    28 SEPT 2009
**
** $Rev::                                             $ Revision of last commit
** $Author::                                          $ Author of last commit
** $Date::                                            $ Date of last commit
**
*/

#define PIXELSCALE 0.27

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "define.h"
#include "wcs.h"

/*
** Get the ra/dec of the four corners of a DES tile given the ra/dec of the
** center of the tile
*/
void getTileCorners (ra,dec,npixra,npixdec,pixelscale,corners)
  double ra;
  double dec;
  int npixra;
  int npixdec;
  float pixelscale;
  double *corners;
{
  struct WorldCoor *wcs;
  double dist;
  float cenra, cendec;

  cenra  = npixra / 2.0;
  cendec = npixdec / 2.0;

  wcs = wcsxinit(ra,dec,pixelscale,cenra,cendec,npixra,npixdec,
      0.0,2000.0,0,"TAN");


  pix2wcs(wcs,1,1,&corners[0],&corners[1]);
  pix2wcs(wcs,npixra,1,&corners[2],&corners[3]);
  pix2wcs(wcs,npixra,npixdec,&corners[4],&corners[5]);
  pix2wcs(wcs,1,npixdec,&corners[6],&corners[7]);


  //dist = wcsdist(ra,dec,corners[6],corners[7]);
  //printf("%f\n",dist * 60);

}
