#include "ColorPoint.h"


ostream& operator<<(ostream &out, ColorPoint const &T)
{
  return out << '(' << T.d[0] << ',' << T.d[1] << ',' << T.d[2] << ')';
}

ostream& operator<<(ostream &out, ColorPoint2D const &T)
{
  return out << '(' << T.d[0] << ',' << T.d[1] << ',' << ')';
}

