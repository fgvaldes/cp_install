/* createTemplate.c 

   Version History:
     1.0   --- initial version

   NOTE: This is a very preliminary version

** Last commit:
**     $Rev:  $
**     $LastChangedBy:  $
**     $LastChangedDate:  $
*/

#include "imageproc.h"

#define VERSION "1.0"
#define PIXSCALE 0.27

void getkeyword(fitsfile *fptr, FILE *fout, char *keyname);

main(int argc, char *argv[]) 
{
  char reduceimage[1000],templateimg[1000],templatename[1000],temp[1000];
  char headname[1000],swarpcall[5000],event[1000];
  char tempdistortname[1000],tempdistortweight[1000];
  char terapixpath[1000],etcpath[1000];

  int i,status;
  int flag_template=0,flag_verbose=2,flag_terapixpath=0,flag_etcpath=0;

  FILE *fhead;

  fitsfile *fptr;

  void reportevt(),printerror();

  if(argc<2) {
    printf("Usage %s: <reduced image> -template <templage image> [OPTIONS]\n",argv[0]);
    printf("         OPTIONS\n");
    printf("            -terapixpath <full terapixpath>\n");
    printf("            -etcpath <full etcpath>\n");
    printf("            -verbose <0-3>\n",argv[0]);
    exit(0);
  }

  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }
  
  /* now go through again */
  for(i=1;i<argc;i++) {
    /* input template */
    if(!strcmp("-template",argv[i])) {
      flag_template=1;
      sprintf(templateimg,"%s",argv[i+1]);
    }

    /* options */
    if (!strcmp(argv[i],"-terapixpath")) {
      flag_terapixpath=1;
      get_input(argv[i],argv[i+1],"-terapixpath",terapixpath,flag_verbose);
    }
    
    if (!strcmp(argv[i],"-etcpath")) {
      flag_etcpath=1;
      get_input(argv[i],argv[i+1],"-etcpath",etcpath,flag_verbose);
    }
  }

  /* ---------------------------------------------------- */
  /* CHECK INPUT TEMPLATE IMAGE AND GET TEMPLATE NAME     */
  /* ---------------------------------------------------- */
  if(!flag_template) {
    sprintf(event," %s Error: -template option is not set, abort\n",argv[0]);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  
  if(strncmp(&(templateimg[strlen(templateimg)-5]),".fits",5)  
      && strncmp(&(templateimg[strlen(templateimg)-8]),".fits.gz",8)) {
    sprintf(event," %s Error: template image %s not end with .fits or .fits.gz (not a fits file?) ",argv[0],templateimg);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else {
    sprintf(temp,"%s",templateimg);
    for(i=strlen(temp);i>0;i--) {
      if(!strncmp(&(temp[i]),"template.fits",13)) {
	temp[i]=0;
	break;
      }
      if(!strncmp(&(temp[i]),"template.fits.gz",16)) {
	temp[i]=0;
	break;
      }   
      if(!strncmp(&(temp[i]),"template.fits.fz",16)) {
	temp[i]=0;
	break;
      }   
    }
    sscanf(temp,"%s",templatename);
  }


  /* ------------------------------- */
  /* CHECK INPUT REDUCED IMAGE       */
  /* ------------------------------- */
  
  /* input the reduced image and check if it is a fits file from the imagename */
  if(strncmp(&(argv[1][strlen(argv[1])-5]),".fits",5)  
      && strncmp(&(argv[1][strlen(argv[1])-8]),".fits.gz",8)) {
    sprintf(event," %s Error: input %s not end with .fits or .fits.gz (not a fits file?) ",argv[0],argv[1]);
    exit(0);
  }
  else
    sprintf(reduceimage,"%s",argv[1]);

  /* ---------------------------------------------------- */
  /* READ IN THE KEYWORDS AND CREATE THE .HEAD FILE       */
  /* ---------------------------------------------------- */

  /* set the .head filename */
  sprintf(headname,"%stempdistort.head",templatename);

  /* create the .head file */
  fhead=fopen(headname,"w");
  
  /* write the first line to .head file */
  fprintf(fhead,"COMMENT\n");

  /* open the reduced fits image to read in necessary information */
  status=0;
  if(fits_open_file(&fptr,reduceimage,READONLY,&status)) printerror(status);

  /* get the necessary keywords */
  getkeyword(fptr,fhead,"NAXIS");
  getkeyword(fptr,fhead,"NAXIS1");
  getkeyword(fptr,fhead,"NAXIS2");

  getkeyword(fptr,fhead,"CTYPE1");
  getkeyword(fptr,fhead,"CTYPE2");

  getkeyword(fptr,fhead,"CRVAL1");
  getkeyword(fptr,fhead,"CRVAL2");

  getkeyword(fptr,fhead,"CRPIX1");
  getkeyword(fptr,fhead,"CRPIX2");

  getkeyword(fptr,fhead,"CD1_1");
  getkeyword(fptr,fhead,"CD2_1");
  getkeyword(fptr,fhead,"CD1_2");
  getkeyword(fptr,fhead,"CD2_2");
  
  getkeyword(fptr,fhead,"PV1_0");
  getkeyword(fptr,fhead,"PV1_1");
  getkeyword(fptr,fhead,"PV1_2");
  getkeyword(fptr,fhead,"PV1_3");
  getkeyword(fptr,fhead,"PV1_4");
  getkeyword(fptr,fhead,"PV1_5");
  getkeyword(fptr,fhead,"PV1_6");
  getkeyword(fptr,fhead,"PV1_7");
  getkeyword(fptr,fhead,"PV1_8");
  getkeyword(fptr,fhead,"PV1_9");
  getkeyword(fptr,fhead,"PV1_10");

  getkeyword(fptr,fhead,"PV2_0");
  getkeyword(fptr,fhead,"PV2_1");
  getkeyword(fptr,fhead,"PV2_2");
  getkeyword(fptr,fhead,"PV2_3");
  getkeyword(fptr,fhead,"PV2_4");
  getkeyword(fptr,fhead,"PV2_5");
  getkeyword(fptr,fhead,"PV2_6");
  getkeyword(fptr,fhead,"PV2_7");
  getkeyword(fptr,fhead,"PV2_8");
  getkeyword(fptr,fhead,"PV2_9");
  getkeyword(fptr,fhead,"PV2_10");

  /* close the fits image and .head file */
  status=0;
  if(fits_close_file(fptr,&status)) printerror(status);

  fprintf(fhead,"END     \n");
  fclose(fhead);

  /* ------------------------------------- */
  /* DISTORT/REMAP THE TEMPLATE IMAGE      */
  /* ------------------------------------- */

  /* construct swarp call here */
  if(flag_terapixpath) sprintf(swarpcall,"%s/swarp ",terapixpath);
  else sprintf(swarpcall,"swarp ");

  sprintf(swarpcall,"%s %s",swarpcall,templateimg); 

  if(flag_etcpath) sprintf(swarpcall,"%s -c %s/default.swarp ",swarpcall,etcpath);
  else sprintf(swarpcall,"%s -c default.swarp ",swarpcall);

  sprintf(swarpcall,"%s -IMAGEOUT_NAME %stempdistort.fits ",swarpcall,templatename);
  sprintf(swarpcall,"%s -COMBINE Y ",swarpcall);
  sprintf(swarpcall,"%s -RESAMPLE Y ",swarpcall);
  sprintf(swarpcall,"%s -SUBTRACT_BACK N",swarpcall);
  sprintf(swarpcall,"%s -WRITE_XML N ",swarpcall);
  
  printf(" SWARPcall: %s\n",swarpcall);

  system(swarpcall);
  
  /* ---------------------- */
  /* CLEAN-UP (no need)     */
  /* ---------------------- */
 
  //system("rm *.head *.weight.fits");
}

void getkeyword(fitsfile *fptr, FILE *fout, char *keyname)
{
  char keyvalue[200];
  int status;
  void printerror();

  status=0;
  if(!fits_read_card(fptr,keyname,keyvalue,&status)) 
    fprintf(fout,"%s\n",keyvalue);
}

#undef VERSION 
#undef PIXSCALE 
