/* imreject
 *
 * A pre-processor for mksupersky; rejects images for the illumination correction
 * Algorithm:
 *  - apply an iterative sigma clip for each image to remove outlier pixels
 *  - create heavily binned median image, refered to as "sky images"
 *  - iterativelly reject sky images using z-test and sigma-test
 * Input: list of images taken by the same CCD
 * Output: list of (good) images that were not rejected
 *
 * 06/27/2012: created by V. Kindratenko (kindr@ncsa.illinois.edu)
 *
 */


#include "imageproc.h"

#define my_max(a, b) (((a) > (b)) ? (a) : (b))


/* command line arguments struct and subroutines */
typedef struct
{
    char *exename;     /* name of the executable */
    char *imglist;     /* file name containing list of images */
    char *outlist;     /* file name of the output list of images */
    int flag_verbose;  /* verbose level */
    int maximgs;       /* max number of images from imglist to process */
    char *tmpoutpath;  /* path to write output images */
    float nsigma;      /* nsigma times the standard deviation away from the mean */
    int maxiter;       /* max number of iterations in sigma-clipping */
    int scale;         /* scaling factor for binned sky images */
    float nsigmaK;     /* nsigmaK times away from the stanradr deviation */
    float ztest;       /* threshold value value to z-test */
    char flag_scale;   /* rescale images if YES */
} argslist;

int read_args(int argc, char **argv, argslist *args);
void print_args(argslist *args);

/* image list load/free subroutines */
desimage *load_allimages(char *imglist, int *nofimgs, int flag_verbose);
int save_allimages(desimage *data, int nofimgs, int scale,char *path, int flag_verbose);
void free_allimages(desimage *data, int nofimgs);
void save_newlist(char *outlist, desimage *data, int nofimgs, char *rejected, int flag_verbose);

/* image processing subroutines */
void rescaleimages(desimage *data, int nofimgs, int flag_rejectobjects, int flag_verbose);
void sigma_clipping(desimage *data, int nofimgs, float nsigma, int maxiter, char *rejected, int flag_verbose);
void get_skyimage(desimage *data, int nofimgs, float** skyimg, int scale, int threshold, char *rejected, int flag_verbose);
long get_stats(float *image, short *mask, long npixels, float *mean, float *median, float *sigma);
int reject_outliers(desimage *data, float** skyimg, int nofimgs, int scale, float ztest, float nsigma, char *rejected, int flag_verbose);


/* used accross multiple functions to produce status messages */
char event[10000];

#define interppix 4
#define maskpix 8
#define growpix 16
#define rejectpix 32
#define growpix2 64


int main(int argc, char **argv)
{
    argslist args;

    desimage *inptimg = NULL;
    int nofimgs = 0, im, imgs;

    /* process command line arguments */
    if (!read_args(argc, argv, &args))
        return EXIT_FAILURE;
    else if (args.flag_verbose > 2)
        print_args(&args);

    /* load and validate images from the image list */
    nofimgs = args.maximgs;
    //    printf("Loading images...");
    //    fflush(stdout);
    inptimg = load_allimages(args.imglist, &nofimgs, args.flag_verbose);
    //    printf("\n");
    if (inptimg == NULL)
    {
        sprintf(event,"Failed to load images from input list %s.", args.imglist);
        reportevt(args.flag_verbose, STATUS, 5, event);
        return EXIT_FAILURE;
    }
    else
    {
        sprintf(event,"Loaded %d images listed in %s", nofimgs, args.imglist);
        reportevt(args.flag_verbose, STATUS, 5, event);
    }

    /* rescale images */
    if (args.flag_scale == YES)
    {
        sprintf(event, "Rescaling %d images.", nofimgs);
        reportevt(args.flag_verbose, STATUS, 5, event);
        rescaleimages(inptimg, nofimgs, YES, args.flag_verbose);
    }

    /* all images presumably have the same size */
    int xdim = inptimg[0].axes[0];
    int ydim = inptimg[0].axes[1];

    /* allocate memory for all intermediates */
    float **skyimg = (float **)calloc(nofimgs, sizeof(float*));
    char *rejected = (char *)calloc(nofimgs, sizeof(char));
    if (skyimg == NULL || rejected == NULL)
    {
        sprintf(event,"Failed to allocate memory for intermediate values.");
        reportevt(args.flag_verbose, STATUS, 5, event);
        free_allimages(inptimg, nofimgs);
        return EXIT_FAILURE;
    }

    int skyimgsize = xdim/args.scale * ydim/args.scale;
    bzero(rejected, nofimgs); /* nothing is rejected at first */

    for (im = 0; im < nofimgs; im++)
    {
        skyimg[im] = (float *)calloc(skyimgsize, sizeof(float));
        if (skyimg[im] == NULL)
        {
            sprintf(event,"Failed to allocate memory for intermediate values.");
            reportevt(args.flag_verbose, STATUS, 5, event);
            free_allimages(inptimg, nofimgs);
            return EXIT_FAILURE;
        }
    }

    /* apply iterative sigma-clipping on all images */
    sprintf(event, "Applying %.2f-sigma-clipping to %d images.", args.nsigma, nofimgs);
    reportevt(args.flag_verbose, STATUS, 5, event);
    sigma_clipping(inptimg, nofimgs, args.nsigma, args.maxiter, rejected, args.flag_verbose);

    /* compute 'sky images' */
    sprintf(event, "Computing sky images of size %d x %d.", xdim/args.scale, ydim/args.scale);
    reportevt(args.flag_verbose, STATUS, 5, event);
    get_skyimage(inptimg, nofimgs, skyimg, args.scale, 10, rejected, args.flag_verbose);

    /* reject outliers */
    sprintf(event, "Rejecting outlier sky images");
    reportevt(args.flag_verbose, STATUS, 5, event);
    reject_outliers(inptimg, skyimg, nofimgs, args.scale, args.ztest, args.nsigmaK, rejected, args.flag_verbose);

    /* save reduced list */
    save_newlist(args.outlist, inptimg, nofimgs, rejected, args.flag_verbose);

    /* save modified images in a new location
       mostly useful for debuggin */
    if (args.tmpoutpath != NULL)
    {
      imgs = save_allimages(inptimg, nofimgs, args.scale,args.tmpoutpath, args.flag_verbose);
      sprintf(event,"Saved %d images.", imgs);
      reportevt(args.flag_verbose, STATUS, 5, event);
    }
    
    /* free memory allocated for images */
    free_allimages(inptimg, nofimgs);
    for (im = 0; im < nofimgs; im++) 
        free(skyimg[im]);
    free(skyimg);
    free(rejected);

    return EXIT_SUCCESS;
}



int read_args(int argc, char **argv, argslist *args)
{
    int i;

    /* check if valid argslist struct is supplied */
    if (args == NULL || argv == NULL)
    {
        sprintf(event, "NULL pointer encountered when reading command line arguments.");
        reportevt(2, STATUS, 3, event);
        return 0;
    }

    /* set up default options */
    args->exename = argv[0];
    args->imglist = NULL;
    args->outlist = NULL;
    args->flag_verbose = 1;
    args->maximgs = 64;
    args->tmpoutpath = NULL;
    args->nsigma = 2.0f;
    args->maxiter = 8;
    args->scale = 128;
    args->ztest = 3.0f;
    args->nsigmaK = 2.0f;
    args->flag_scale = YES;

    if (argc < 3)
    {
        printf("%s <input list> <output list> [options]\n", args->exename);
        printf("  Input Options\n");
        printf("    -maximgs <#>\n");
        printf("    -noscale\n");
        printf("  Sigma-clipping Options\n");
        printf("    -nsigma-sc <#>\n");
        printf("    -maxiter <#>\n");
        printf("  Image Binning Options\n");
        printf("    -scale <#>\n");
        printf("  Image Rejection Options\n");
        printf("    -ztest <#>\n");
        printf("    -nsigma-ir <#>\n");
        printf("  Testing Options\n");
        printf("    -tmpoutpath <path to store processed images>\n");
        printf("    -verbose <0-3>\n");
        return 0;
    }

    /* list of input images */
    args->imglist = argv[1];
    args->outlist = argv[2];

    /* process command line options */
    for (i = 3; i < argc; i++)
    {
        if (!strcmp(argv[i], "-verbose"))
        {
            if (i < argc-1)
                sscanf(argv[++i], "%d", &args->flag_verbose);
            else
            {
                sprintf(event, "Verbose level is not supplied. Using default level 2.");
                reportevt(2, STATUS, 3, event);
            }

            if (args->flag_verbose < 0 || args->flag_verbose > 3)
            {
                sprintf(event, "Verbose level %d is out of range. Reset to default.", args->flag_verbose);
                args->flag_verbose = 2;
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i], "-maximgs"))
        {
            if (i < argc-1)
                sscanf(argv[++i], "%d", &args->maximgs);
            else
            {
                sprintf(event, "Maximum number of images to process is not supplied. Using default.");
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i], "-tmpoutpath"))
        {
            if (i < argc-1)
                args->tmpoutpath = argv[++i];
            else
            {
                sprintf(event, "Path to store processed images is not supplied.");
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i], "-maxiter"))
        {
            if (i < argc-1)
                sscanf(argv[++i], "%d", &args->maxiter);
            else
            {
                sprintf(event, "Maximum number of iterations for sigma-clipping is not supplied. Using default.");
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i], "-nsigma-sc"))
        {
            if (i < argc-1)
                sscanf(argv[++i], "%f", &args->nsigma);
            else
            {
                sprintf(event, "nsigma for sigma-clipping is not supplied. Using default.");
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i], "-nsigma-ir"))
        {
            if (i < argc-1)
                sscanf(argv[++i], "%f", &args->nsigmaK);
            else
            {
                sprintf(event, "nsigma for image rejection is not supplied. Using default.");
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i], "-ztest"))
        {
            if (i < argc-1)
                sscanf(argv[++i], "%f", &args->ztest);
            else
            {
                sprintf(event, "ztest for image rejection is not supplied. Using default.");
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i], "-scale"))
        {
            if (i < argc-1)
                sscanf(argv[++i], "%d", &args->scale);
            else
            {
                sprintf(event, "Scaling factor for sky images is not supplied. Using default.");
                reportevt(2, STATUS, 3, event);
            }
        }

        if (!strcmp(argv[i],"-noscale")) args->flag_scale = NO;

    }



    return 1;
}

void print_args(argslist *args)
{
    /* check if valid argslist struct is supplied */
    if (args == NULL)
    {
        sprintf(event, "NULL pointer encountered when printing command line arguments.");
        reportevt(2, STATUS, 3, event);
        return;
    }

    sprintf(event, "Executing %s with\n imglist=%s\n outlist=%s\n rescale=%i\n maximgs=%d\n tmpoutpath=%s\n nsigma-sc=%f\n maxiter=%d\n scale=%d\n nsigma-ir=%f\n ztest=%f\n verbose=%d\n", 
            args->exename, args->imglist, args->outlist, args->flag_scale, args->maximgs, args->tmpoutpath, args->nsigma, args->maxiter, args->scale, args->nsigmaK, args->ztest, args->flag_verbose);

    reportevt(2, STATUS, 3, event);

    return;
}


/* load all images listed in the image list file
   nofimgs is expected to contain max number of images to load
   nofimgs on exit will contain actual number of loaded images */
desimage *load_allimages(char *imglist, int *nofimgs, int flag_verbose)
{
    desimage *data = NULL;
    FILE *imgfile;
    int im;
    //   printf("\n");
    /* allocate memory for nofimgs images */
    data = (desimage *)calloc(*nofimgs, sizeof(desimage));
    if (data == NULL)
    {
        sprintf(event,"Calloc for %d images failed.", *nofimgs);
        reportevt(flag_verbose, STATUS, 5, event);
        return NULL;
    }

    /* init image memory with NULL data */
    //    printf("initializing images\n");
    //    fflush(stdout);
    for (im = 0; im < *nofimgs; im++)
        init_desimage(data+im);

    //    printf("opening image list\n");
    //    fflush(stdout);
    /* open image list file */
    if ((imgfile = fopen(imglist, "r")) == NULL)
    {
        sprintf(event,"Unable to open image list file %s", imglist);
        reportevt(flag_verbose, STATUS, 5, event);
        free(data);
        *nofimgs = 0;
        return NULL;
    }

    /* cycle through input images and read them */
    //    printf("reading images\n");
    //    fflush(stdout);
    for (im = 0; im < *nofimgs; im++)
    {
      //      printf("Image %d\n",im);
      //    fflush(stdout);
        /* potential stack overflow bug, should use fgets instead of fscanf */
        if (fscanf(imgfile, "%s", data[im].name) == EOF)
            break;

        /* read input image */
        rd_desimage(data+im, READONLY, flag_verbose);

        /* if we are here, rd_desimage succeeded */
    }

    fclose(imgfile);

    /* fix for actual number of images read */
    if ((*nofimgs = im) == 0)
    {
        free(data);
        data = NULL;
    }
    //    printf ("Done reading images\n");
    //    fflush(stdout);
    return data;
}


void rescaleimages(desimage *data, int nofimgs, int flag_rejectobjects, int flag_verbose)
{
    int xdim = data[0].axes[0];
    int ydim = data[0].axes[1];
    int scaleregionn[4]={500,1500,1500,2500};
    float mode, fwhm, scalefactor, threshold;
    int im;
    long i, nummasked;

    if (data == NULL)
        return;

    int scalenum = (scaleregionn[1]-scaleregionn[0]+1) * (scaleregionn[3]-scaleregionn[2]+1);

    float *scalesort = (float *)calloc(scalenum, sizeof(float));   
    if (scalesort == NULL)
    {
        sprintf(event,"Calloc for scalesort failed");
        reportevt(flag_verbose,STATUS,5,event);
        exit(0);
    }

    /* cycle through all images and scale them */
    for (im = 0; im < nofimgs; im++)
    {
        retrievescale(data+im, scaleregionn, scalesort, flag_verbose, &scalefactor, &mode, &fwhm);

        if (fabs(scalefactor)<1.0e-4) 
        {
            sprintf(event,"Masking entire image: %s", data[im].name);
            reportevt(flag_verbose,STATUS,3,event);
            for (i = 0; i < data[im].npixels; i++) 
                data[im].mask[i] |= maskpix;
            continue;
        }

        if (flag_rejectobjects)
        {
            nummasked=0;

            /* set threshold at ~12 sigma above the mode in the sky */
            threshold = mode + 5.0f * fwhm;

            for (i = 0; i < data[im].npixels; i++)
            {
                if (data[im].image[i] > threshold)
                {
                    data[im].mask[i] |= rejectpix;
                    nummasked++;
                }
            }

            sprintf(event, "Image= %s & MaskedPixelsAbove %.1f = %.2f percent",
                    data[im].name, threshold, (float)(100.0f*nummasked/(data[im].npixels)));
            reportevt(flag_verbose, QA, 1, event);
        }

        for (i = 0; i < data[im].npixels; i++)
            data[im].image[i] /= scalefactor;
    }

    free(scalesort);

    return;
}


int save_allimages(desimage *data, int nofimgs, int scale,char *path, int flag_verbose)
{
    int im, i;
    char *fname = NULL;
    char fullfname[1000];
    fitsfile *fptr = NULL;
    int status = 0, hdutype;
    char comment[100], type[100];

    /* cycle through all images and save them */
    for (im = 0; im < nofimgs; im++)
    {
        /* prepare the image name */
        fname = data[im].name;
        for (i = strlen(data[im].name)-1; i > 0; i--)
        {
            if (data[im].name[i] == '/')
            {
                fname = &(data[im].name[i+1]);
                break;
            }
        }

        strcpy(fullfname, path);
	strncat(fullfname,"/sky_",5);
        strcpy(&(fullfname[strlen(fullfname)]), fname);

        sprintf(event,"  writing %s", fullfname);
        reportevt(flag_verbose, STATUS, 3, event);

        /* remove file, in case it already exists */
        sprintf(event,"rm %s", fullfname);
        system(event);

        if (fits_create_file(&fptr, fullfname, &status))
            printerror(status);

        /* write components of the image */
        for (i = 0; i < data[im].hdunum; i++)
        {
            if (fits_movabs_hdu(data[im].fptr, i+1, &hdutype, &status))
                printerror(status);

            /* determine what kind of image extension */
            if (fits_read_key_str(data[im].fptr, "DES_EXT", type, comment, &status) == KEY_NO_EXIST)
            {
                status=0; /* reset status flag */
                sprintf(event,"  No DES_EXT keyword found in %s", data[im].name);
                reportevt(flag_verbose, STATUS, 3, event);
                break; /* skip this image */
            }

            if (!strncmp(type, "IMAGE", 5)) /* write the image */
            {
	      if (fits_copy_header(data[im].fptr, fptr, &status))
		printerror(status);
	      fits_update_key_lng(fptr,"NAXIS1",data[im].axes[0]/scale,NULL,&status);
	      fits_update_key_lng(fptr,"NAXIS2",data[im].axes[1]/scale,NULL,&status);
	      if (fits_write_img(fptr, TFLOAT, 1, data[im].npixels/(scale*scale), data[im].varim, &status))
                    printerror(status);
            }
            if (!strncmp(type, "MASK", 4) || !strncmp(type, "BPM", 3)) /* write the mask */
            {
                if (fits_copy_header(data[im].fptr, fptr, &status))
                    printerror(status);
                if (fits_write_img(fptr, TUSHORT, 1, data[im].npixels, data[im].mask, &status))
                    printerror(status);
            }
	    /* write the variance */
	    /*            if (!strncmp(type, "VARIANCE", 5) || !strncmp(type, "WEIGHT", 6)) 
			  {
			  if (fits_copy_header(data[im].fptr, fptr, &status))
			  printerror(status);
			  if (fits_write_img(fptr, TFLOAT, 1, data[im].npixels, data[im].varim, &status))
			  printerror(status);
			  }
	    */
        }
	
        if (fits_close_file(fptr, &status))
	  printerror(status);
    }
    
    return im;
}


void free_allimages(desimage *data, int nofimgs)
{
    int im;
    int status = 0;

    if (data != NULL)
    {
        for (im = 0; im < nofimgs; im++)
        {
            fits_close_file(data[im].fptr, &status);
            if (data[im].image != NULL) free(data[im].image);
            if (data[im].varim != NULL) free(data[im].varim);
            if (data[im].mask != NULL) free(data[im].mask);
        }

        free(data);
    }

    return;
}


void save_newlist(char *outlist, desimage *data, int nofimgs, char *rejected, int flag_verbose)
{
    int i, im;
    FILE *listfile;

    sprintf(event, "Saving new file list to %s", outlist);
    reportevt(flag_verbose, STATUS, 5, event);

    if ((listfile = fopen(outlist, "w")) == NULL)
    {
        sprintf(event,"Unable to open image list file %s", outlist);
        reportevt(flag_verbose, STATUS, 5, event);
        return;
    }

    for (im = 0; im < nofimgs; im++)
    {
        if (!rejected[im])
            fprintf(listfile, "%s\n", data[im].name);
    }

    fclose(listfile);

    return;
}


/*
Sigma-clipping algorithm

An iterative, sigma-clipping algorithm can be used to reduce the effect of 
outliers on the computed statistics. The algorithm calculates the mean and
standard deviation of the data, then removes all points which are more than
nsigma times the standard deviation away from the mean. The procedure is
repeated until either no more points are rejected or the maximum number of
iterations (maxiter) is reached.
*/
void sigma_clipping(desimage *data, int nofimgs, float nsigma, int maxiter, char *rejected, int flag_verbose)
{
    int im, itr, nrejectedimgs=0;
    double sum, sum2;
    long i, n, count=0;
    float mean, sigma;

    if (data == NULL || rejected == NULL)
        return;

    for (im = 0; im < nofimgs; im++)
    {
        if (rejected[im]) continue;

        for (itr = 0; itr < maxiter; itr++)
        {
           /* get the mean and sigma for the data */
           mean = sigma = 0.0f;
           count = n = 0;

           n = get_stats(data[im].image, data[im].mask, data[im].npixels, &mean, NULL, &sigma);

           if (n != 0)
           {
               /* apply threshold */
               for (i = 0; i < data[im].npixels; i++)
               {
                   if (!data[im].mask[i] && fabsf(data[im].image[i]-mean) > nsigma*sigma)
                   {
                       data[im].mask[i] |= rejectpix;
                       count++;
                   }
               }
           }
           else /* all pixels must have been masked out earlier */
               rejected[im] = 1;

           sprintf(event, "S-C: Image %s: iter=%d: mean=%f, sigma=%f, count=%li", 
                   data[im].name, itr+1, mean, sigma, count);
           reportevt(flag_verbose, QA, 1, event);

           if (count == 0) break;
        }

        /* what if we did maxiter and there are pixels still being rejected? */
        if (count != 0)
        {
           mean = sigma = 0.0f;
           n = get_stats(data[im].image, data[im].mask, data[im].npixels, &mean, NULL, &sigma);

           if (n == 0) /* all pixels have been rejected ! */
               rejected[im] = 1;
           else
           {
               /* check if there are any pixels to be rejected */
               for (i = 0; i < data[im].npixels; i++)
               {
                   if (!data[im].mask[i] && fabsf(data[im].image[i]-mean) > nsigma*sigma)
                   {
                       rejected[im] = 1;
                       break;
                   }
               }
           }
        }
    }

    /* how many images have been rejected so far? */
    for (im = 0; im < nofimgs; im++)
        if (rejected[im]) nrejectedimgs++;

    sprintf(event, "Rejected %i out of %i images due to sigma-clipping:", nrejectedimgs, nofimgs);
    reportevt(flag_verbose, STATUS, 3, event);

    for (im = 0; im < nofimgs; im++)
    {
        if (rejected[im])
        {
            sprintf(event, "  Rejected:  %s", data[im].name);
            reportevt(flag_verbose, STATUS, 3, event);
        }
    }

    return;
}


/* computes mean and stdev for image
   applies mask, if supplied
   computes median, if requested
   returns number of pixels used in the computation */
long get_stats(float *image, short *mask, long npixels, float *mean, float *median, float *sigma)
{
    double sum, sum2;
    long i, n = 0;
    float *array = NULL;

    *mean = 0.0f;
    *sigma = 0.0f;
    sum = 0.0;
    sum2 = 0.0;

    if (image == NULL)
        return 0;

    if (median != NULL)
    {
        array = (float *)calloc(npixels+1, sizeof(float));
        if (array == NULL)
        {
            sprintf(event, "Unable to allocate memory to temporary use.");
            reportevt(2, STATUS, 3, event);
            exit(0);
        }
    }

    /* compute mean */
    for (i = 0; i < npixels; i++)
    {
        if (mask != NULL)
            if (mask[i]) continue;

        sum += image[i];
        if (median != NULL)
            array[n+1] = image[i];   /* since shell() uses 1-based indexing */
        n++;
    }

    if (n > 0)
    {
        *mean = sum / n;

        /* compute sigma */
        for (i = 0; i < npixels; i++)
        {
            if (mask != NULL)
                if (mask[i]) continue;

            sum2 += (image[i] - *mean) * (image[i] - *mean);
        }

        *sigma = sqrtf(sum2 / n);

        if (median != NULL)
        {
            /* compute median */
            shell(n, array);
            *median = (n % 2) ? array[n/2+1] : (array[n/2] + array[n/2+1])/2.0f;
        }
    }

    if (median != NULL)
        free(array);

    return n;
}


/*  - create heavily binned median image (NxN where N is of order 128-256)
      From here I refer to these as "sky images" 
  - threshold is % of binned bixels */
void get_skyimage(desimage *data, int nofimgs, float** skyimg, int scale, int threshold, char *rejected, int flag_verbose)
{
    int im;
    int xdim = data[0].axes[0];
    int ydim = data[0].axes[1];
    int bxdim = xdim/scale;
    int bydim = ydim/scale;
    int i, j, n, m, bx, by, x, y, loc, bloc;
    float sum;

    float *array = (float *)calloc(my_max(scale*scale, bxdim*bydim)+1, sizeof(float));
    if (array == NULL)
    {
        sprintf(event, "Unable to allocate memory to temporary use.");
        reportevt(2, STATUS, 3, event);
        exit(0);
    }

    /* turn threshold from % to # */
    threshold *= scale*scale / 100.0f;

    for (im = 0; im < nofimgs; im++)
    {
        if (rejected[im]) continue;

        for (by = 0; by < bydim; by++)
        for (bx = 0; bx < bxdim; bx++)
        {
            n = 0;
            for (y = 0; y < scale; y++)
            for (x = 0; x < scale; x++)
            {
                loc = (bx*scale+x) + (by*scale+y) * xdim;
                if (!data[im].mask[loc])
                {
                    array[n+1] = data[im].image[loc];  /* since shell() uses 1-based indexing */
                    n++;
                }
            }

            bloc = bx + by * bxdim;

            if (n > threshold)  /* compute median */
            {
                shell(n, array);
                skyimg[im][bloc] = (n % 2) ? array[n/2+1] : (array[n/2] + array[n/2+1])/2.0f;
            }
            else /* mark as such */
                skyimg[im][bloc] = -1.0f;
        }

        /*>     (Note if #_pix < threshold are available to obtained a binned value,
        >       ie. are masked either as an object or a bad pixel, then interpolate
        >       the value of the binned pixel from adjacent binned pixels) */
        for (by = 0; by < bydim; by++)
        for (bx = 0; bx < bxdim; bx++)
        {
            bloc = bx + by * bxdim;
            if (skyimg[im][bloc] < 0.0f)
            {
                array[bloc] = 0.0f;
                n = m = 0;

                /* TODO: need a true interpolation rather than averaging */

                while (n < 7)
                {
                    if (++m > my_max(bydim, bxdim)) break;

                    for (i = -m; i <= m; i++)
                    for (j = -m; j <= m; j++)
                    {
                        if ((by+i >= 0) && (by+i < bydim) && (bx+j >= 0) && (bx+j < bxdim))
                        {
                            if (skyimg[im][(bx+j) + (by+i) * bxdim] < 0.0f) continue;
                            array[bloc] += skyimg[im][(bx+j) + (by+i) * bxdim];
                            n++;
                        }
                    }
                }

                if (n > 0)
                    array[bloc] /= n;
            }
        }

	for(y = 0;y < ydim;y++)
	  for(x = 0;x < xdim;x++)
	    data[im].varim[y*xdim + x] = 0.0;
	
        for (by = 0; by < bydim; by++)
        for (bx = 0; bx < bxdim; bx++)
        {
            bloc = bx + by * bxdim;
            if (skyimg[im][bloc] < 0.0f)
                skyimg[im][bloc] = array[bloc];

	    /* Populate only small part of weight map with binned sky image */
	    data[im].varim[bloc] = skyimg[im][bloc];

            /* for test only, don't forget to remove when done */
	    /*
	      for (y = 0; y < scale; y++)
	      for (x = 0; x < scale; x++)
	      {
	      loc = (bx*scale+x) + (by*scale+y) * xdim;
	      data[im].varim[loc] = skyimg[im][bloc];
	      }
	    */
        }
    }
    
    free(array);
    
    return;
}


/* - consider set of M sky images as a stack (in z-direction)
1) calculate a pixelated supersky (a median of the so far non-rejected frames)
2) calculate the mean and stddev of the pixels in the supersky image
3) calculate the mean and stddev of the pixels in each plane
4) reject planes with outlier means using z-test and mean and stddev of the supersky image
5) repeat steps 1-4 until there is nothing to reject
*/
int reject_outliers(desimage *data, float** skyimg, int nofimgs, int scale, float ztest, float nsigma, char *rejected, int flag_verbose)
{
    int im, bx, by;
    int xdim = data[0].axes[0];
    int ydim = data[0].axes[1];
    int bxdim = xdim/scale;
    int bydim = ydim/scale;
    int n, bloc, ncrs=1, nrejectedimgs = 0, nrejectedimgs_tmp;

    float mean[nofimgs], sigma[nofimgs];
    float supersky[bxdim * bydim];
    float array[nofimgs];
    float ss_mean, ss_sigma, av_sigma, av_mean;

    /* how many images have been rejected so far? */
    for (im = 0; im < nofimgs; im++)
        if (rejected[im]) nrejectedimgs++;
    nrejectedimgs_tmp = nrejectedimgs;

    /* calculate the mean and stddev of the pixels in each plane */
    for (im = 0; im < nofimgs; im++)
    {
        if (rejected[im]) continue;
        get_stats(skyimg[im], NULL, bxdim*bydim, &(mean[im]), NULL, &(sigma[im]));
	sprintf(event, "%s (mean,sigma):  (%f,%f)\n",data[im].name,mean[im],sigma[im]);
	reportevt(flag_verbose, STATUS, 3, event);
    }

    while (nrejectedimgs != nofimgs && ncrs != 0)
    {
        ncrs = 0;  /* count number of images rejected in this iteration */

        /* calculate average of sigma[im] for non-rejected images */
        av_sigma = 0.0f;
        n = 0;
	av_mean  = 0.0f;
        for (im = 0; im < nofimgs; im++)
        {
            if (rejected[im]) continue;
            av_sigma += sigma[im];
	    av_mean  += mean[im];
            n++;
        }

        av_sigma /= n;
	av_mean /= n;
	sprintf(event, "Average mean/sigma:  %f/%f\n",av_mean,av_sigma);
	reportevt(flag_verbose, STATUS, 3, event);


        /* calculate a pixelated supersky (a median of the so far non-rejected frames) */
        for (by = 0; by < bydim; by++)
        for (bx = 0; bx < bxdim; bx++)
        {
            bloc = bx + by * bxdim;
            n = 0;
            for (im = 0; im < nofimgs; im++)
            {
                if (rejected[im]) continue;
                array[n+1] = skyimg[im][bloc];  /* since shell() uses 1-based indexing */
                n++;
            }

            shell(n, array);
            supersky[bloc] = (n % 2) ? array[n/2+1] : (array[n/2] + array[n/2+1])/2.0f;
        }

        /* calculate the mean and stddev of the pixels in the supersky image */
        get_stats(supersky, NULL, bxdim*bydim, &ss_mean, NULL, &ss_sigma);
	sprintf(event, "Supersky (mean,sigma):  (%f,%f)\n",ss_mean,ss_sigma);
	reportevt(flag_verbose, STATUS, 3, event);

        for (im = 0; im < nofimgs; im++)
        {
            if (rejected[im]) continue;

            /* reject planes with outlier means using z-test */
            if (fabsf(mean[im] - ss_mean) / ss_sigma > ztest) /* found an outlier */
            {
                 rejected[im] = 2; /* rejected */
                 ncrs++;
            }

            /* also reject planes with outlier sigma using average sigma */ 
            if (fabsf(sigma[im] - av_sigma) / av_sigma > nsigma)
            {
                 rejected[im] = 3; /* rejected */
                 ncrs++;
            }
        }

        nrejectedimgs += ncrs;
    }

    sprintf(event, "Rejected %i out of %i supersky images:", nrejectedimgs-nrejectedimgs_tmp, nofimgs-nrejectedimgs_tmp);
    reportevt(flag_verbose, STATUS, 3, event);

    for (im = 0; im < nofimgs; im++)
    {
        if (rejected[im]>1)
        {
            sprintf(event, "  Rejected:  %s", data[im].name);
            reportevt(flag_verbose, STATUS, 3, event);
        }
    }


    return nrejectedimgs;
}




