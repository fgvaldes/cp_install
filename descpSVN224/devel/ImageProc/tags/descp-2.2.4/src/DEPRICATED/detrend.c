/* Carries out basic detrending of CCD data */

#include "imageproc.h"

#define BADPIX_BPM 1   	     /* set in bad pixel mask (hot/dead pixel/column) */
#define BADPIX_SATURATE 2    /* saturated pixel */
#define BADPIX_INTERP 4
#define BADPIX_THRESHOLD 0.25/* pixels less than this fraction of sky     */
			     /* are filtered -- helps remove failed reads */
#define BADPIX_MASK   8      /* too little signal- i.e. poor read */


/* List of control flags */
#define FLG_OUTPUT              ctrl_flags[0]
#define FLG_OVERSCAN            ctrl_flags[1]
#define FLG_BIAS                ctrl_flags[2]
#define FLG_FLATTEN             ctrl_flags[3]
#define FLG_BPM                 ctrl_flags[4]
#define FLG_PUPIL               ctrl_flags[5]
#define FLG_ILLUMINATION        ctrl_flags[6]
#define FLG_FRINGE              ctrl_flags[7]
#define FLG_VARIANCE            ctrl_flags[8]
#define FLG_INTERPOLATE         ctrl_flags[9]
#define FLG_UPDATESKYBRITE      ctrl_flags[10]
#define FLG_UPDATESKYSIGMA      ctrl_flags[11]
#define FLG_MEF                 ctrl_flags[12]


main(argc,argv)
	int argc;
	char *argv[];
{

	int	c,i,scaleregionn[4]={500,1500,1500,2500},ccdnum=0,
		flag_verbose=1,flag_list=NO,imnum,imoutnum,im,hdutype,
		ctrl_flags[20];
	static int status=0;
	char	comment[1000],longcomment[10000],filter[100]="",imagename[1000],
		outputlist[1000],event[10000],outputfilename[1000],
		scaleregion[100];
	float	scale_interpolate;
	desimage bias,flat,data,output,bpm,illumination,fringe,pupil;
	void	rd_desimage(),headercheck(),printerror(),decodesection(),
		overscan(),retrievescale(),reportevt(),detrend();
	FILE	*inp,*out;

	if (argc<3) {
	  printf("%s <image or file list> <options>\n",argv[0]);
	  printf("  Input Data\n");
	  printf("    -bpm <image>\n");
	  printf("  Image Corrections\n");
	  printf("    -nooverscan\n");
	  printf("    -bias <image>\n");
	  printf("    -pupil <image>\n");
	  printf("    -flatten <image>\n");
	  printf("    -interpolate_col <variance scaling>\n");
	  printf("    -illumination <image>\n");
	  printf("    -fringe <image>\n");
	  printf("    -scaleregion <Xmin:Xmax,Ymin,Ymax>\n");
	  printf("  Output Options\n");
	  printf("    -output <image or file list>\n");
	  printf("    -variance\n");
	  printf("    -MEF (single file output)\n");
	  printf("    -verbose <0-3>\n");
	  exit(0);
	}


	FLG_OVERSCAN=FLG_UPDATESKYBRITE=FLG_UPDATESKYSIGMA=YES;
	FLG_BIAS=FLG_FLATTEN=FLG_OUTPUT=FLG_BPM=FLG_VARIANCE=NO;
	FLG_INTERPOLATE=FLG_ILLUMINATION=FLG_FRINGE=FLG_PUPIL=NO;
	FLG_MEF=NO;

	/* ************************************************************ */
	/* ********* Extract verbose flag from Command Line *********** */
	/* ************************************************************ */

	for (i=2;i<argc;i++) 
	  if (!strcmp(argv[i],"-verbose")) {
	    sscanf(argv[++i],"%d",&flag_verbose);
            if (flag_verbose<0 || flag_verbose>3) {
              sprintf(event,"Verbose level out of range %d. Reset to 2",
	 	flag_verbose);
	      flag_verbose=2;
              reportevt(2,STATUS,3,event);
            }
	  }
	

	/* ********************************************** */
	/* ********* Handle Input Image/File  *********** */
	/* ********************************************** */

	/* copy input image name if FITS file*/
	if (!strncmp(&(argv[1][strlen(argv[1])-5]),".fits",5) ||
	  !strncmp(&(argv[1][strlen(argv[1])-8]),".fits.gz",8))  {
	  sprintf(data.name,"%s",argv[1]);
	  imnum=1;
	}
	else { /* expect file containing list of images */
	  imnum=0;flag_list=YES;
	  inp=fopen(argv[1],"r");
	  if (inp==NULL) {
	    sprintf(event,"File not found: %s",argv[1]);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	  while (fscanf(inp,"%s",imagename)!=EOF) {
	    imnum++;
	    if (strncmp(&(imagename[strlen(imagename)-5]),".fits",5)
	      && strncmp(&(imagename[strlen(imagename)-8]),".fits.gz",8)) {
	      reportevt(flag_verbose,STATUS,5,
		"Input image list must contain FITS or compressed FITS images");
	      exit(0);
	    }
	  }
	  if (fclose(inp)) {
	    reportevt(flag_verbose,STATUS,5,"Closing input image list failed");
	  };
	}


	/* ****************************************** */
	/* ********* Process Command Line *********** */
	/* ****************************************** */

	if (flag_verbose==3) reportevt(flag_verbose,STATUS,1,
	  "Processing Command Line");

	for (i=2;i<argc;i++) {
	  if (!strcmp(argv[i],"-nooverscan")) FLG_OVERSCAN=NO;
	  if (!strcmp(argv[i],"-variance")) FLG_VARIANCE=YES;
	  if (!strcmp(argv[i],"-MEF")) FLG_MEF=YES;
          if (!strcmp(argv[i],"-scaleregion")) {
            sprintf(scaleregion,"%s",argv[i+1]);
            decodesection(scaleregion,scaleregionn,flag_verbose);
          }
	  if (!strcmp(argv[i],"-interpolate_col")) {
	    FLG_INTERPOLATE=YES;
	    i++;
	    sscanf(argv[i],"%f",&scale_interpolate);
	    if (scale_interpolate<1.0 || scale_interpolate>10.0) {
		sprintf(event,"Variance scale factor for interpolated pixels must be between 1 and 10 (%f)",
		  scale_interpolate);
		reportevt(flag_verbose,STATUS,5,event);
		exit(0);
	    }
	  }
	  if (!strcmp(argv[i],"-bias")) {
	    FLG_BIAS=YES;
	    i++;
	    if (strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5) &&
	      strncmp(&(argv[i][strlen(argv[i])-8]),".fits.gz",8)) {
	      reportevt(flag_verbose,STATUS,5,
		"Bias correction image must follow -bias");
	      exit(0);
	    }	    
	    else sprintf(bias.name,"%s",argv[i]);
	  }
	  if (!strcmp(argv[i],"-pupil")) {
	    FLG_PUPIL=YES;
	    i++;
	    if (strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5) &&
	      strncmp(&(argv[i][strlen(argv[i])-8]),".fits.gz",8)) {
	      reportevt(flag_verbose,STATUS,5,
		"Pupil image must follow -pupil");
	      exit(0);
	    }	    
	    else sprintf(pupil.name,"%s",argv[i]);
	  }
	  if (!strcmp(argv[i],"-bpm")) {
	    FLG_BPM=YES;
	    i++;
	    if (strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5) && 
	      strncmp(&(argv[i][strlen(argv[i])-8]),".fits.gz",8)) {
	      reportevt(flag_verbose,STATUS,5,
		"Bad Pixel Mask image must follow -bpm");
	      exit(0);
	    }	    
	    else sprintf(bpm.name,"%s",argv[i]);
	  }
	  if (!strcmp(argv[i],"-flatten")) {
	    FLG_FLATTEN=YES;
	    i++;
	    if (strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5)
	      && strncmp(&(argv[i][strlen(argv[i])-8]),".fits.gz",8)) {
	      reportevt(flag_verbose,STATUS,5,
		"Flat correction image must follow -flatten");
	      exit(0);
	    }
	    else sprintf(flat.name,"%s",argv[i]);
	  }
	  if (!strcmp(argv[i],"-illumination")) {
	    FLG_ILLUMINATION=YES;
	    i++;
	    if (strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5) &&
	      strncmp(&(argv[i][strlen(argv[i])-8]),".fits.gz",8)) {
	      reportevt(flag_verbose,STATUS,5,
		"Illimunation correction image must follow -illumination");
	      exit(0);
	    }
	    else sprintf(illumination.name,"%s",argv[i]);
	  }
	  if (!strcmp(argv[i],"-fringe")) {
	    FLG_FRINGE=YES;
	    i++;
	    if (strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5) &&
	      strncmp(&(argv[i][strlen(argv[i])-8]),".fits.gz",8)) {
	      reportevt(flag_verbose,STATUS,5,
		"Fringe correction image must follow -fringe");
	      exit(0);
	    }
	    else sprintf(fringe.name,"%s",argv[i]);
	  }

	  if (!strcmp(argv[i],"-output")) {
	    FLG_OUTPUT=YES;
	    i++;
	    if (!strncmp(&(argv[i][strlen(argv[i])-5]),".fits",5)
	      || !strncmp(&(argv[i][strlen(argv[i])-8]),".fits.gz",8)) {
	      if (!flag_list) sprintf(output.name,"!%s",argv[i]);
	      else { /* error because input list cannot map to single image */
		reportevt(flag_verbose,STATUS,5,
		"File list must follow -output option in multi image mode");
		exit(0);
	      }
	    }
	    else {
	      if (!flag_list) { /* input image needs single output image */
	        reportevt(flag_verbose,STATUS,5,
		"FITS image output name must follow -output option in single image mode");
		exit(0);
	      }
	      sprintf(outputlist,"%s",argv[i]);
	      imoutnum=0;
	      out=fopen(outputlist,"r");
	      if (out==NULL) {
	        sprintf(event,"Output file list not found: %s",outputlist);
		reportevt(flag_verbose,STATUS,5,event);
	        exit(0);
	      }
	      while (fscanf(out,"%s",imagename)!=EOF) {
		imoutnum++;
		if (strncmp(&(imagename[strlen(imagename)-5]),".fits",5) &&
		  strncmp(&(imagename[strlen(imagename)-8]),".fits.gz",8)) {
		  reportevt(flag_verbose,STATUS,5,"Output list must contain FITS or compressed FITS images");
		  exit(0);
		}
	      }
	      if (fclose(out)) {
	        sprintf(event,"Closing file failed: %s",outputlist);
	        reportevt(flag_verbose,STATUS,5,event);
	        exit(0);
	      };
	      if (imoutnum==imnum) {
		if (flag_verbose==3) 
		  reportevt(flag_verbose,STATUS,1,"Output and input lists of same length");
	      }
	      else {
		sprintf(event,"Input and output lists must be of same length in multi image mode");
		reportevt(flag_verbose,STATUS,5,event);
		exit(0);
	      }
	    }
	  }
	}
	

	sprintf(event,"Input list %s contains %d FITS images",argv[1],imnum);
	reportevt(flag_verbose,STATUS,1,event);

	/* ****************************************** */
	/* ********** READ CALIBRATION DATA ********* */
	/* ****************************************** */

	if (flag_verbose==3) reportevt(flag_verbose,STATUS,1,
	  "Reading Calibration Data");
			
	/* read bias image */
	if (FLG_BIAS) {
	  rd_desimage(&bias,READONLY,flag_verbose);
	  /* confirm that this is actual bias image */
	  /* make sure we are in the 1st extension */
	  if (fits_movabs_hdu(bias.fptr,1,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=1 failed: %s",bias.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* confirm that this is bias image */
	  headercheck(&bias,"NOCHECK",&ccdnum,"DESMKBCR",flag_verbose);
	  if (fits_close_file(bias.fptr,&status)) {
	    sprintf(event,"File close failed: %s",bias.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}
	/* read flat image */	
	if (FLG_FLATTEN) {
	  rd_desimage(&flat,READONLY,flag_verbose);
	  /* make sure we are in the 1st extension */
	  if (fits_movabs_hdu(flat.fptr,1,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=1 failed: %s",flat.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* confirm that this is flat image */
	  headercheck(&flat,filter,&ccdnum,"DESMKFCR",flag_verbose);
	  if (fits_close_file(flat.fptr,&status)) {
	    sprintf(event,"File close failed: %s",flat.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}
	/* read pupil image */
	if (FLG_PUPIL) {
	  rd_desimage(&pupil,READONLY,flag_verbose);
	  /* make sure we are in the 1st extension */
	  if (fits_movabs_hdu(pupil.fptr,1,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=1 failed: %s",pupil.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* confirm that this is pupil ghost image */
	  headercheck(&pupil,"NOCHECK",&ccdnum,"DESMKPUP",flag_verbose);
	  if (fits_close_file(pupil.fptr,&status)) {
	    sprintf(event,"File close failed: %s",pupil.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}
	/* read bad pixel mask image */
	if (FLG_BPM) {
	  rd_desimage(&bpm,READONLY,flag_verbose);
	  /* make sure we are in the 1st extension */
	  if (fits_movabs_hdu(bpm.fptr,1,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=1 failed: %s",bpm.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* confirm that this is bpm image */
	  headercheck(&bpm,"NOCHECK",&ccdnum,"DESMKBPM",flag_verbose);
	  if (fits_close_file(bpm.fptr,&status)) {
	    sprintf(event,"File close failed: %s",bpm.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}
	/* create a mock bpm regardless */
	else bpm=bias;	
		
	/* read illumination image */	
	if (FLG_ILLUMINATION) {
	  rd_desimage(&illumination,READONLY,flag_verbose);
	  /* make sure we are in the 1st extension */
	  if (fits_movabs_hdu(illumination.fptr,1,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=1 failed: %s",illumination.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* confirm that this is illumination image */
	  headercheck(&illumination,filter,&ccdnum,"DESMKICR",flag_verbose);
	  if (fits_close_file(illumination.fptr,&status)) {
	    sprintf(event,"File close failed: %s",illumination.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}
		
	/* read fringe image */	
	if (FLG_FRINGE) {
	  rd_desimage(&fringe,READONLY,flag_verbose);
	  /* make sure we are in the 1st extension */
	  if (fits_movabs_hdu(fringe.fptr,1,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=1 failed: %s",fringe.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* confirm that this is fringe image */
	  headercheck(&fringe,filter,&ccdnum,"DESMKFRG",flag_verbose);
	  if (fits_close_file(fringe.fptr,&status)) {
	    sprintf(event,"File close failed: %s",fringe.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}

	/* prepare header longcomment for processed images */
	sprintf(longcomment,"DESDM:");
        for (i=0;i<argc;i++) sprintf(longcomment,"%s %s",longcomment,argv[i]);
		
		
	/* ******************************************** */
	/* ********** Prepare for Processing  ********* */
	/* ******************************************** */

	if (flag_verbose==3) reportevt(flag_verbose,STATUS,1,
	  "Beginning to Process Images");
			
	/* reopen input/output lists if in multiimage mode */
	if (flag_list) {
	  /* reopen file for processing */
	  inp=fopen(argv[1],"r");
	  if (inp==NULL) {
	    sprintf(event,"File not found: %s",argv[1]);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	  /* reopen output list for processing */
	  out=fopen(outputlist,"r");
	  if (out==NULL) {
	    sprintf(event,"File not found: %s",outputlist);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	}


	/********************************************/
	/****** PROCESSING SECTION BEGINS HERE ******/
	/********************************************/

	/* now cycle through input images to process them */
	for (im=0;im<imnum;im++) {

	  /* get next image name */
	  if (flag_list) {
	    fscanf(inp,"%s",data.name);
	    if (flag_verbose=3) {
	      sprintf(event,"Now processing %s",data.name);
	      reportevt(flag_verbose,STATUS,1,event);
	    }
	  }

          if (FLG_OUTPUT) { /* writing output to new image */
            fscanf(out,"%s",outputfilename);
	    if (flag_verbose=3) {
	      sprintf(event,"Output to %s",outputfilename);
	      reportevt(flag_verbose,STATUS,1,event);
	    }
          }
	  else sprintf(outputfilename,"%s",data.name);

	  /* process the image */
	  detrend(&data,outputfilename,&bias,&flat,&bpm,&illumination,&fringe,
	    &pupil,ctrl_flags,longcomment,scaleregionn,scale_interpolate,
	    filter,&ccdnum,flag_verbose);
	}


	/********************************************/
	/****** CLOSE INPUT/OUTPUT FILE LISTS  ******/
	/********************************************/

	if (flag_list) {
	    if (fclose(inp)) {
	    sprintf(event,"Failed to close input file list: %s",argv[1]);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	  if (FLG_OUTPUT) 
	    if (fclose(out)) {
	    sprintf(event,"Failed to close output file list: %s",outputlist);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	}

	return (0);
}


#undef BADPIX_BPM
#undef BADPIX_THRESHOLD
#undef BADPIX_SATURATE 
#undef BADPIX_INTERP 
#undef BADPIX_MASK

#undef FLG_OUTPUT
#undef FLG_OVERSCAN
#undef FLG_BIAS
#undef FLG_FLATTEN
#undef FLG_BPM
#undef FLG_PUPIL
#undef FLG_ILLUMINATION
#undef FLG_FRINGE
#undef FLG_VARIANCE
#undef FLG_INTERPOLATE
#undef FLG_UPDATESKYBRITE
#undef FLG_UPDATESKYSIGMA
#undef FLG_MEF
