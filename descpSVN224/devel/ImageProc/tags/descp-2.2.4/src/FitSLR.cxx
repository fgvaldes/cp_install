///
/// \file    FitSLR.cxx
/// \brief   Vertex and n-prong fitting
/// \version $Id: FitSLR.cxx,v 1.3 2009/06/29 19:09:45 messier Exp $
/// \author  messier@indiana.edu
///
#include "FitSLR.h"
#include <iostream>
#include <cmath>
#include "Minuit2/Minuit2Minimizer.h"
#include "Math/GSLMinimizer.h"
#include <vector>
#include "TFile.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TGraph.h"
#include "TTree.h"
#include "TMath.h"
#include <algorithm>

using namespace std;
static void sigma_clip(int max_iter,double thresh,
                       const vector<double> &array,
                       const vector<double> &err,double &mean,
                       int &good, int &bad,double &rms,bool clip,
                       bool weighted,const double &tol,
                       vector<int> &curindex_array);
static void Tokenize(const string& str,
                     vector<string>& tokens,
                     const string& delimiters);

FitSLR::FitSLR(int _ncolors,string _colors,bool _debug):
  ncolors(_ncolors),
  debug(_debug),
  MaxOutlier(0.5),
  RemoveOutlier(true),
  SigmaCut(3),
  CutPoints(3),CutRange(0.04),MinPoints(20)
{

  // parse colors string to see which colors we
  // are fitting
  Tokenize(_colors,colors,":");
  if(colors.size()!=_ncolors) {
    cout<<"Number of colors does not match the color input string"<<endl;
    exit(0);
  }

  for(int i=0;i<4;i++) stored_color[i]=0;


}

void FitSLR::GetDiff(vector<double> &diff1,vector<double> &diff2,
                     bool use_fit_par)
{
  
  diff1.clear();
  diff2.clear();
  cout<<"2D"<<endl;
  vector<ColorPoint>::iterator iter=DataPoints_3D.begin();
  while(iter!=DataPoints_3D.end()) {

    ColorPoint p(*iter);
     
    // must shift colors to best fit
    if(use_fit_par) {
      p.d[0]+=stored_color[0];
      p.d[1]+=stored_color[1];
      p.d[2]+=stored_color[2];
    }

    ColorPoint_Match found = CoveyTree_3D.find_nearest(p);
    

    double dist1=sqrt(
                      (p.d[0]-found.first->d[0])*
                      (p.d[0]-found.first->d[0])+
                      (p.d[1]-found.first->d[1])*
                      (p.d[1]-found.first->d[1])
                      );
    // define points left median as negative
    if(p.d[0]<found.first->d[0]) dist1=-dist1;

    
    double dist2=sqrt(
                      (p.d[1]-found.first->d[1])*
                      (p.d[1]-found.first->d[1])+
                      (p.d[2]-found.first->d[2])*
                      (p.d[2]-found.first->d[2])
                      );
    // define points left of median as negative
    if(p.d[1]<found.first->d[1]) dist2=-dist2;
    


    diff1.push_back(dist1);
    diff2.push_back(dist2);
    ++iter;
  }
  
  return;
  
}

void FitSLR::GetDiff(vector<double> &diff1,
                     bool use_fit_par)
{
  
  diff1.clear();

  vector<ColorPoint2D>::iterator iter=DataPoints_2D.begin();
  while(iter!=DataPoints_2D.end()) {

    ColorPoint2D p(*iter);
     
    // must shift colors to best fit
    if(use_fit_par) {
      p.d[0]+=stored_color[0];
      p.d[1]+=stored_color[1];
    }

    ColorPoint2D_Match found = CoveyTree_2D.find_nearest(p);
    

    double dist1=sqrt(
                      (p.d[0]-found.first->d[0])*
                      (p.d[0]-found.first->d[0])+
                      (p.d[1]-found.first->d[1])*
                      (p.d[1]-found.first->d[1])
                      );
    // define points left median as negative
    if(p.d[0]<found.first->d[0]) dist1=-dist1;

    
    diff1.push_back(dist1);
    ++iter;
  }
  
  return;
  
}
void FitSLR::GetDiff(vector<double> &diff1,vector<double> &diff2,
                     vector<double> &diff3,
                     bool use_fit_par)
{
  
  diff1.clear();
  diff2.clear();
  diff3.clear();

  vector<ColorPoint4D>::iterator iter=DataPoints_4D.begin();
  while(iter!=DataPoints_4D.end()) {

    ColorPoint4D p(*iter);
     
    // must shift colors to best fit
    if(use_fit_par) {
      p.d[0]+=stored_color[0];
      p.d[1]+=stored_color[1];
      p.d[2]+=stored_color[2];
      p.d[3]+=stored_color[3];
    }
    
    ColorPoint4D_Match found = CoveyTree_4D.find_nearest(p);
    

    double dist1=sqrt(
                      (p.d[0]-found.first->d[0])*
                      (p.d[0]-found.first->d[0])+
                      (p.d[1]-found.first->d[1])*
                      (p.d[1]-found.first->d[1])
                      );
    // define points left median as negative
    if(p.d[0]<found.first->d[0]) dist1=-dist1;
    
    
    double dist2=sqrt(
                      (p.d[1]-found.first->d[1])*
                      (p.d[1]-found.first->d[1])+
                      (p.d[2]-found.first->d[2])*
                      (p.d[2]-found.first->d[2])
                      );
    // define points left of median as negative
    if(p.d[1]<found.first->d[1]) dist2=-dist2;

    double dist3=sqrt(
                      (p.d[2]-found.first->d[2])*
                      (p.d[2]-found.first->d[2])+
                      (p.d[3]-found.first->d[3])*
                      (p.d[3]-found.first->d[3])
                      );
    // define points left of median as negative
    if(p.d[1]<found.first->d[1]) dist3=-dist3;



    diff1.push_back(dist1);
    diff2.push_back(dist2);
    diff3.push_back(dist3);
    ++iter;
  }
  

 


  return;
  
}


void FitSLR::RemoveOutliers(vector<ColorPoint4D> &CutData)
{  
  
   vector<double> diff1,diff2,diff3;
  
  
   vector<ColorPoint4D>::iterator iter=DataPoints_4D.begin();
   while(iter!=DataPoints_4D.end()) {
    
     ColorPoint4D p(*iter);
     
     // must shift colors to best fit
     //    if(use_fit_par) {
       p.d[0]+=stored_color[0];
       p.d[1]+=stored_color[1];
       p.d[2]+=stored_color[2];
       p.d[3]+=stored_color[3];
       //}
    
     ColorPoint4D_Match found = CoveyTree_4D.find_nearest(p);
    

     double dist1=sqrt(
                       (p.d[0]-found.first->d[0])*
                       (p.d[0]-found.first->d[0])+
                       (p.d[1]-found.first->d[1])*
                       (p.d[1]-found.first->d[1])
                       );
     // define points left median as negative
     if(p.d[0]<found.first->d[0]) dist1=-dist1;
    
     double dist2=sqrt(
                       (p.d[1]-found.first->d[1])*
                       (p.d[1]-found.first->d[1])+
                       (p.d[2]-found.first->d[2])*
                       (p.d[2]-found.first->d[2])
                       );
     // define points left of median as negative
     if(p.d[1]<found.first->d[1]) dist2=-dist2;

     double dist3=sqrt(
                       (p.d[2]-found.first->d[2])*
                       (p.d[2]-found.first->d[2])+
                       (p.d[3]-found.first->d[3])*
                       (p.d[3]-found.first->d[3])
                       );
     // define points left of median as negative
     if(p.d[1]<found.first->d[1]) dist3=-dist3;



     diff1.push_back(dist1);
     diff2.push_back(dist2);
     diff3.push_back(dist3);
     ++iter;
   }
  
   // find the mean and sigma for the three color combos
    double mean1,rms1;
    double mean2,rms2;
    double mean3,rms3;
    int good1,bad1;
    int good2,bad2;
    int good3,bad3;
    vector<double> dum(diff1.size());
    vector<int> passed1_array,passed2_array,passed3_array;
    vector<int> good1_array(DataPoints_4D.size());
    vector<int> good2_array(DataPoints_4D.size());
    vector<int> good3_array(DataPoints_4D.size());
    sigma_clip(3,SigmaCut,diff2,dum,mean2,
               good2,bad2,rms2,true,false,1e-6,passed2_array);
    sigma_clip(3,SigmaCut,diff1,dum,mean1,
               good1,bad1,rms1,true,false,1e-6,passed1_array);
    sigma_clip(3,SigmaCut,diff3,dum,mean3,
               good3,bad3,rms3,true,false,1e-6,passed3_array);

    for(int i=0;i<DataPoints_4D.size();i++) {
      good1_array[i]=0;good2_array[i]=0;good3_array[i]=0;
    }


    for(int i=0;i<passed1_array.size();i++) {
      good1_array[passed1_array[i]]=1;
    }
    for(int i=0;i<passed2_array.size();i++) {
      good2_array[passed2_array[i]]=1;
    }
    for(int i=0;i<passed3_array.size();i++) {
      good3_array[passed3_array[i]]=1;
    }


    CutData.clear();                 
    for(int i=0;i<DataPoints_4D.size();i++) {
      if(good1_array[i] && good2_array[i] && good3_array[i]) {
        CutData.push_back(DataPoints_4D[i]);
      }
    }

    
        

   if(debug) {
     // cout<<"For combination "<<color[0]<<"-"<<color[1]<<endl;
     //     cout<<" Mean "<<mean<<" Clipped RMS "<<rms<<endl;
     //     cout<<" Removed "<<bad<<" points"<<endl;
   }

  
 }



// void FitSLR::RemoveOutliers(vector<ColorPoint> &_points,
//                     const double &dist)
// {  


//   vector<double> gr_ri,ri_iz;
//   vector<ColorPoint>::iterator iter=_points.begin();
//   while(iter!=_points.end()) {
    
//     ColorPoint p(*iter);
        
//     ColorPoint_Match found = CoveyTree.find_nearest(p);
    
//     double gr_ri_dist=sqrt(
//                            (p.d[0]-found.first->d[0])*
//                            (p.d[0]-found.first->d[0])+
//                            (p.d[1]-found.first->d[1])*
//                            (p.d[1]-found.first->d[1])
//                            );
    
//     double ri_iz_dist=sqrt(
//                            (p.d[1]-found.first->d[1])*
//                            (p.d[1]-found.first->d[1])+
//                            (p.d[2]-found.first->d[2])*
//                            (p.d[2]-found.first->d[2])
//                            );
//     if(gr_ri_dist<dist && ri_iz_dist<dist) ++iter;
//     else _points.erase(iter);
    
//   }
  

//   return ;
// }


// void FitSLR::RemoveOutliers(vector<ColorPoint2D> &_points,
//                     const double &dist)
// {  


//   vector<double> gr_ri,ri_iz;
//   vector<ColorPoint2D>::iterator iter=_points.begin();
//   while(iter!=_points.end()) {
    
//     ColorPoint2D p(*iter);
        
//     ColorPoint2D_Match found;
//     if(useGRI) found = CoveyTree_gr_ri.find_nearest(p);
//     else found = CoveyTree_ri_iz.find_nearest(p);
    
//     double color_dist=sqrt(
//                      (p.d[0]-found.first->d[0])*
//                      (p.d[0]-found.first->d[0])+
//                      (p.d[1]-found.first->d[1])*
//                      (p.d[1]-found.first->d[1])
//                      );
    
    
//     if(color_dist<dist) ++iter;
//     else _points.erase(iter);
    
//   }
  

//   return ;
// }



// void FitSLR::RemoveOutliers(vector<ColorPoint> &_points,
//                             int max_iter,double thresh,double tol)
// {

// //   vector<ColorPoint>::iterator iter=DataPoints.begin();
  
// //   while(iter!=DataPoints.end()) {

// //     if(!density) {
// //     ColorPoint_Match found = CoveyTree.find_nearest(*iter);
  
// //     if(found.second>MaxOutlier) {
// //       DataPoints.erase(iter);
// //     }
// //     else ++iter;
// //     }
// //     else {
// //       vector<ColorPoint> v;
// //       DataTree.find_within_range(*iter,
// //                                  CutRange,back_inserter(v));
       
// //       if(v.size()<CutPoints) {
// //         DataPoints.erase(iter);
// //       }
// //       else ++iter;
// //     }
    
// //   }


//   vector<double> gr_ri,ri_iz;
//   vector<ColorPoint>::iterator iter=_points.begin();
//   while(iter!=_points.end()) {

//     ColorPoint p(*iter);
     
//      // must shift colors to best fit
//     p.d[0]+=stored_gr;
//     p.d[1]+=stored_ri;
//     p.d[2]+=stored_iz;
    
//     ColorPoint_Match found = CoveyTree.find_nearest(p);
    

//     double gr_ri_dist=sqrt(
//                            (p.d[0]-found.first->d[0])*
//                            (p.d[0]-found.first->d[0])+
//                            (p.d[1]-found.first->d[1])*
//                            (p.d[1]-found.first->d[1])
//                            );
//     // define points with g-r above median as positive
//     if(p.d[0]<found.first->d[0]) gr_ri_dist=-gr_ri_dist;


//     double ri_iz_dist=sqrt(
//                            (p.d[1]-found.first->d[1])*
//                            (p.d[1]-found.first->d[1])+
//                            (p.d[2]-found.first->d[2])*
//                            (p.d[2]-found.first->d[2])
//                            );

//     // define points with r-i above median as positive
//     if(p.d[2]<found.first->d[2]) ri_iz_dist=-ri_iz_dist;


//     gr_ri.push_back(gr_ri_dist);
//     ri_iz.push_back(ri_iz_dist);
//     ++iter;
//   }
  
  
//   vector<double> dum(gr_ri.size());
//     vector<int> gr_ri_index,ri_iz_index;
//   double mean,rms;
//   int good,bad;
//   sigma_clip(max_iter,thresh,gr_ri,dum,mean,
//              good,bad,rms,true,false,tol,gr_ri_index);

  
//   sigma_clip(max_iter,thresh,ri_iz,dum,mean,
//              good,bad,rms,true,false,tol,ri_iz_index);

//   // look for index in both gr_ri and ri_iz
//   // if either one is bad remove point

//   iter=_points.begin();
//   int counter=0;

//   while(iter!=_points.end()) {

//     if(find(gr_ri_index.begin(),gr_ri_index.end(),counter)==gr_ri_index.end() ||
//        find(ri_iz_index.begin(),ri_iz_index.end(),counter)==ri_iz_index.end()) {
      
//       _points.erase(iter);
//     }
//     else {
//       iter++;
//     }
//     counter++;
//   }
  

//   return ;

// }



double FitSLR::DoEval(const double* par) const
{
  double sum=0;
  
  if(ncolors==3) {
    vector<ColorPoint>::const_iterator iter=DataPoints_3D.begin();
    
    for( ; iter!=DataPoints_3D.end(); ++iter) {
      
      ColorPoint p(*iter);
      
      // must shift the d array instead of the actual
      // colors to searh with the kdtree
      p.d[0]+=par[0]; 
      p.d[1]+=par[1]; 
      p.d[2]+=par[2]; 
      ColorPoint_Match found = CoveyTree_3D.find_nearest(p);
      sum+=found.second; 
    }
  }
  else if(ncolors==2){
    vector<ColorPoint2D>::const_iterator iter=DataPoints_2D.begin();
    
    for( ; iter!=DataPoints_2D.end(); ++iter) {
      
      ColorPoint2D p(*iter);
      
      // must shift the d array instead of the actual
      // colors to searh with the kdtree
      p.d[0]+=par[0]; // 1st color shift
      p.d[1]+=par[1]; // 2nd color shift

      
      ColorPoint2D_Match found;
      found = CoveyTree_2D.find_nearest(p);
      sum+=found.second; 
    }
  }
  else if(ncolors==4){
    vector<ColorPoint4D>::const_iterator iter=DataPoints_4D.begin();
    
    for( ; iter!=DataPoints_4D.end(); ++iter) {
      
      ColorPoint4D p(*iter);
      
      // must shift the d array instead of the actual
      // colors to searh with the kdtree
      p.d[0]+=par[0]; // 1st color shift
      p.d[1]+=par[1]; // 2nd color shift
      p.d[2]+=par[2]; // 2nd color shift
      p.d[3]+=par[3]; // 2nd color shift
      
      
      ColorPoint4D_Match found;
      found = CoveyTree_4D.find_nearest(p);
      sum+=found.second; 
    }
 }
 return sum;
  
}

void FitSLR::LoadData(string filename)
{

  CoveyData_2D.clear();
  CoveyData_3D.clear();
  CoveyData_4D.clear();
  graph.Clear();

  TFile *file=new TFile(filename.c_str());
  TTree *tree=(TTree*)file->Get("tree");


  double c1,c2,c3,c4;
  

  tree->SetBranchAddress(colors[0].c_str(),&c1);
  tree->SetBranchAddress(colors[1].c_str(),&c2);
  if(ncolors>2) {
    tree->SetBranchAddress(colors[2].c_str(),&c3);
  }
  if(ncolors==4) {
    tree->SetBranchAddress(colors[3].c_str(),&c4);
  }
  
  int i=0;

  

  while(tree->GetEntry(i)) {
    i++;
    if(ncolors==2) {
      ColorPoint2D point(c1,c2);
      CoveyData_2D.push_back(point);
      CoveyTree_2D.insert(point);
    }
    else if(ncolors==3) {
      ColorPoint2D point2(c1,c2);
      CoveyData_2D.push_back(point2);
      CoveyTree_2D.insert(point2);
      
      ColorPoint point(c1,c2,c3);
      CoveyData_3D.push_back(point);
      CoveyTree_3D.insert(point);
    }
    else if(ncolors==4) {
      ColorPoint point3(c1,c2,c3);
      CoveyData_3D.push_back(point3);
      CoveyTree_3D.insert(point3);
      
      ColorPoint4D point(c1,c2,c3,c4);
      CoveyData_4D.push_back(point);
      CoveyTree_4D.insert(point);
      
    }
  }

  CoveyEntries=i;   
  delete tree;
  delete file;

} 

vector<double> FitSLR::Fit(vector<ColorPoint> &_points,
                           vector<double> &err)
			     
{
  
  DataPoints_3D=_points;
  DataEntries=DataPoints_3D.size();
   

  DataTree_3D.clear();
  for(int i=0;i<DataEntries;i++) DataTree_3D.insert(DataPoints_3D[i]);

  if(DataPoints_3D.size()<MinPoints) {

    throw "Not enough points to do SLR fit";
  }

  DataEntries=DataPoints_3D.size();
  vector<double> par;
  err.clear();
  DoFit(par,err);

  _points=DataPoints_3D;
  return par;
}


vector<double> FitSLR::Fit(vector<ColorPoint2D> &_points,
                           vector<double> &err)
			     
{
  
  DataPoints_2D=_points;
  DataEntries=DataPoints_2D.size();
   

  DataTree_2D.clear();
  for(int i=0;i<DataEntries;i++) DataTree_2D.insert(DataPoints_2D[i]);

  if(DataPoints_2D.size()<MinPoints) {

    throw "Not enough points to do SLR fit";
  }

  DataEntries=DataPoints_2D.size();
  vector<double> par;
  err.clear();
  DoFit(par,err);

  _points=DataPoints_2D;
  return par;
}


vector<double> FitSLR::Fit(vector<ColorPoint4D> &_points,
                           vector<double> &err)
			     
{
  
  DataPoints_4D=_points;
  DataEntries=DataPoints_4D.size();
   

  DataTree_4D.clear();
  for(int i=0;i<DataEntries;i++) DataTree_4D.insert(DataPoints_4D[i]);

  if(DataPoints_4D.size()<MinPoints) {

    throw "Not enough points to do SLR fit";
  }

  DataEntries=DataPoints_4D.size();
  vector<double> par;
  err.clear();
  DoFit(par,err);

  _points=DataPoints_4D;

  if(RemoveOutlier) {
    vector<ColorPoint4D> CutData;
    RemoveOutliers(CutData);
    
    DataPoints_4D.clear();
    _points.clear();
    copy(CutData.begin(),CutData.end(),
         back_inserter(DataPoints_4D));
    copy(CutData.begin(),CutData.end(),
         back_inserter(_points));

    DataEntries=DataPoints_4D.size();
    err.clear();
    par.clear();
    DoFit(par,err);
  }


  return par;
}


void FitSLR::DoFit(vector<double> &fit_par,vector<double> &fit_err)
{
  
  ROOT::Minuit2::Minuit2Minimizer mini(ROOT::Minuit2::kCombined);
  
  if(debug) mini.SetPrintLevel(2);
  else mini.SetPrintLevel(0);
  mini.SetTolerance(0.001);
  mini.SetStrategy(2);
  mini.SetMaxFunctionCalls(10000);

  for(int i=0;i<ncolors;i++) {
    mini.SetLimitedVariable(i,colors[i].c_str(),0,0.001,-10,10);
  }

  
  mini.SetFunction(*this);
  mini.Minimize();

  const double *errors=mini.Errors();
  
  double chi2=mini.MinValue();
  double ndf=DataEntries-ncolors;

  for(int i=0;i<ncolors;i++) {
    stored_color[i]=mini.X()[i];
    fit_par.push_back(mini.X()[i]);
    double err=errors[i]*sqrt(chi2/(ndf-1));
    fit_err.push_back(err);
  }
    
}







void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol,
                vector<int> &curindex_array)
{
  int N=array.size();
  mean=TMath::Mean(N,&array[0]);
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  // initial flag of bad objs
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }

  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;

  vector<int> newindex_array;

  curindex_array.clear();
  for(int i=0;i<array.size();i++) curindex_array.push_back(i);

  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();
    newerr_array.clear();
    newindex_array.clear();
    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
        newindex_array.push_back(curindex_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    curindex_array.clear();
    cur_array.clear();
    curerr_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));
    copy(newindex_array.begin(),newindex_array.end(),
         back_inserter(curindex_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;
    
  }

  
  
  
  return;

}


void Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters)
{
  // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}


