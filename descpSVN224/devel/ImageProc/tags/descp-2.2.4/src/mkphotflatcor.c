/* mkphotflatcor
/*
/* 	takes an input description of the pixel sizes over the CCDs
/*	within the camera and produces a photflatcor that will correct
/*	for zeropoint variations with position that arise from flattening
/*	using the dome flats or superskies.
/*
/*	Currently it uses Numerical Recipies polin2 routine.
/*
*/

#include "imageproc.h"
#include "fitswcs.h"
#include "define.h"
#include "prefs.h"

main(argc,argv)
	int argc;
	char *argv[];
{
	char	filter[100]="",comment[10000],longcomment[10000],event[10000];
	int	i,flag_photflatcor=YES,flag_verbose=2,x,y,loc,mkpath(),
		ccdnum=0,ncompare=0,flag_normalize=YES,
		flag_photflatcor_compare=NO,scaleregionn[4],scalenum;
	static int status=0;
	float	offset,rms,maxdev,fwhm,delta,imagemin,imagemax,
		scaleval,*scalesort,mode,fhwm;
	double	ccdpos[2],skypos[2],decscale,yscale,xscale,
		skyymin[2],skyymax[2],skyxmin[2],skyxmax[2];
	
	desimage input,photflatcor,photflatcor_template;
	catstruct *cat;
	tabstruct *tab;
	wcsstruct *wcs;
	void	rd_desimage(),shell(),reportevt(),image_compare(),
		retrievescale();
	time_t	tm;

	if (argc<3) {
	  printf("%s <input image> <options>\n",argv[0]);
	  printf("    -nonormalize\n");
	  printf("  Output Options\n");
	  printf("    -output <name>\n");
	  printf("  Testing Options\n");
	  printf("    -photflatcor_compare <template>\n");
	  printf("    -verbose <0-3>\n");
	  exit(0);
	}

	/* ******************************************* */
	/* ********  PROCESS COMMAND LINE ************ */
	/* ******************************************* */

	for (i=2;i<argc;i++) 
	  if (!strcmp(argv[i],"-verbose")) {
           sscanf(argv[++i],"%d",&flag_verbose);
            if (flag_verbose<0 || flag_verbose>3) {
              sprintf(event,"Verbose level out of range %d . Reset to 2",
                flag_verbose);
              flag_verbose=2;
              reportevt(2,STATUS,3,event);
            }
	  }

	for (i=2;i<argc;i++) {
	  if (!strcmp(argv[i],"-output")) {
	    sprintf(photflatcor.name,"!%s",argv[++i]);
	  }
 	  if (!strcmp(argv[i],"-nonormalize"))  
            flag_normalize=NO;
 	  if (!strcmp(argv[i],"-photflatcor_compare"))  {
            flag_photflatcor_compare=YES;
            sprintf(photflatcor_template.name,"%s",argv[++i]);
            if (!strncmp(&(photflatcor_template.name[
		strlen(photflatcor_template.name)-5]),".fits",5)
             && !strncmp(&(photflatcor_template.name[
		strlen(photflatcor_template.name)-8]),".fits.gz",8))  {
              sprintf(event,"Template image must be FITS: %s",
		photflatcor_template.image);
              reportevt(flag_verbose,STATUS,5,event);
              exit(0);
            }
	  }
	}


	/* ******************************************** */
	/* ******************************************** */
	/* ***********  READ INPUT Image  ************* */
	/* ******************************************** */
	/* ******************************************** */

	sprintf(input.name,"%s",argv[1]);
	rd_desimage(&input,READONLY,flag_verbose);
        /* check image for ccdnumber */
        headercheck(&input,filter,&ccdnum,"DES_EXT",
	    flag_verbose);

	/* ******************************************************* */
	/* ******************************************************* */
	/* **************  CREATE PHOTFLATCOR IMAGE ************** */
	/* ******************************************************* */
	/* ******************************************************* */

	sprintf(event,"Creating photflatcor image %s",photflatcor.name+1);
	reportevt(flag_verbose,STATUS,1,event);
	photflatcor.npixels=input.npixels;
	photflatcor.nfound=input.nfound;
	for (i=0;i<photflatcor.nfound;i++) photflatcor.axes[i]=input.axes[i];
	photflatcor.bitpix=FLOAT_IMG;
	photflatcor.image=(float *)calloc(photflatcor.npixels,sizeof(float));
	if (photflatcor.image==NULL) {
	  reportevt(flag_verbose,STATUS,5,"Calloc of photflatcor.image failed");
	  exit(0);
	}
	photflatcor.mask=(short *)calloc(photflatcor.npixels,sizeof(short));
	if (photflatcor.mask==NULL) {
	  reportevt(flag_verbose,STATUS,5,"Calloc of photflatcor.mask failed");
	  exit(0);
	}
	/* initialize image mask */
	for (i=0;i<photflatcor.npixels;i++) photflatcor.mask[i]=0;

	/* read in WCS information from input image */
	if (!(cat=read_cat(input.name))) {
	  sprintf(event,"Image open failed: %s",input.name);	
	  reportevt(flag_verbose,STATUS,5,event);
	  exit(0);
	}

	tab=cat->tab;
	wcs=read_wcs(tab);

	/* extract central RA/Dec of image */
	ccdpos[0]=photflatcor.axes[0]/2.0;ccdpos[1]=photflatcor.axes[1]/2.0;
	raw_to_wcs(wcs,ccdpos,skypos);
	sprintf(event,"Central sky position is %.8f , %.8f",skypos[0],
	  skypos[1]);
	reportevt(flag_verbose,STATUS,1,event);
	

	/* cycle through calculating the platescale for each pixel */
	delta=10.0;
	for (y=0;y<photflatcor.axes[1];y++) {
	  for (x=0;x<photflatcor.axes[0];x++) {
	    loc=y*photflatcor.axes[0]+x;
	    ccdpos[0]=x;ccdpos[1]=y;
	    raw_to_wcs(wcs,ccdpos,skypos);
	    decscale=cos(skypos[1]*PI/180.0);
	    ccdpos[1]=y-delta;ccdpos[0]=x;
	    raw_to_wcs(wcs,ccdpos,skyymin);
	    ccdpos[1]=y+delta;ccdpos[0]=x;
	    raw_to_wcs(wcs,ccdpos,skyymax);
	    ccdpos[1]=y;ccdpos[0]=x+delta;
	    raw_to_wcs(wcs,ccdpos,skyxmax);
	    ccdpos[1]=y;ccdpos[0]=x-delta;
	    raw_to_wcs(wcs,ccdpos,skyxmin);
	    xscale=sqrt(Squ(skyxmax[0]-skyxmin[0])+Squ(skyxmax[1]-skyxmin[1]))/
	      (2.0*delta)*3600.0;
	    yscale=sqrt(Squ(skyymax[0]-skyymin[0])+Squ(skyymax[1]-skyymin[1]))/
	      (2.0*delta)*3600.0;
	    if (flag_verbose==3 && y%256==0 && x%256==0) 
	      printf("  x= %4d y= %4d  ==>  %.4f %.4f  %.6f\n",
	      x,y,xscale,yscale,xscale*yscale*decscale);
	    photflatcor.image[loc]=yscale*xscale*decscale;
	  }
	}

        /* ************************************************************ */
        /* ***************** Extract basic statistics ***************** */
        /* ************************************************************ */
	scaleregionn[0]=1;scaleregionn[1]=photflatcor.axes[0];
	scaleregionn[2]=1;scaleregionn[3]=photflatcor.axes[1];
        scalesort=(float *)calloc(photflatcor.npixels,sizeof(float));
        if (scalesort==NULL) {
          sprintf(event,"Calloc for scalesort failed");
          reportevt(flag_verbose,STATUS,5,event);
          exit(0);
        }
	retrievescale(&photflatcor,scaleregionn,scalesort,flag_verbose,
	  &scaleval,&mode,&fwhm);
	imagemin=imagemax=photflatcor.image[0];
	for (i=0;i<photflatcor.npixels;i++)  {
	  if (photflatcor.image[i]>imagemax) imagemax=photflatcor.image[i];
	  if (photflatcor.image[i]<imagemin) imagemin=photflatcor.image[i];
	}
	sprintf(event,"Image = %s & median = %.8f & min = %.8f & max = %.8f & ratio = %.8f",
	  input.name,scaleval,imagemin,imagemax,imagemax/imagemin);
	reportevt(flag_verbose,QA,1,event);
	  

        /* ************************************************************ */
        /* ************** Normalize by median plate scale ************* */
        /* ************************************************************ */
	if (flag_normalize) { /* extract median plate scale and normalize */
	  reportevt(flag_verbose,STATUS,1,"Normalizing plate scale image");
	  for (i=0;i<photflatcor.npixels;i++) 
	    photflatcor.image[i]/=scaleval;
	}

        /* ************************************************************ */
        /* ************************************************************ */
        /* ************** Test image against template ***************** */
        /* ************************************************************ */
        /* ************************************************************ */
        if (flag_photflatcor_compare) {
          /*  Read template image */
          rd_desimage(&photflatcor_template,READONLY,flag_verbose);
          /* check image for ccdnumber */
          headercheck(&photflatcor_template,filter,&ccdnum,"DESMKPFC",
	    flag_verbose);
          /* first compare images */
          rms=maxdev=offset=0.0;
          ncompare=0;
          image_compare(&photflatcor,&photflatcor_template,
	    &offset,&rms,&maxdev,&ncompare,flag_verbose);

        }



	/* ******************************************** */
	/* *********** WRITE OUTPUT IMAGE ************* */
	/* ******************************************** */
	if (flag_photflatcor) {
	  sprintf(event,"Writing results to %s",photflatcor.name+1);
	  reportevt(flag_verbose,STATUS,1,event);

          /* make sure path exists for new image */
          if (mkpath(photflatcor.name,flag_verbose)) {
            sprintf(event,"Failed to create path to file: %s",
	      photflatcor.name+1);
            reportevt(flag_verbose,STATUS,5,event);
            exit(0);
          }
          else {
            sprintf(event,"Created path to file: %s",photflatcor.name+1);
            reportevt(flag_verbose,STATUS,1,event);
          }

	  /* create the file */
          if (fits_create_file(&photflatcor.fptr,photflatcor.name,&status)) {
	    sprintf(event,"Creating file failed: %s",photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

	  /* create image extension */
	  if (fits_create_img(photflatcor.fptr,FLOAT_IMG,2,
	    photflatcor.axes,&status)) {
	    sprintf(event,"Creating image failed: %s",photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  
	  /* write the corrected image*/
	  if (fits_write_img(photflatcor.fptr,TFLOAT,1,photflatcor.npixels,
	    photflatcor.image,&status)) {
	    sprintf(event,"Writing image failed: %s",photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  
	  /* write basic information into the header */
	  if (fits_update_key_str(photflatcor.fptr,"OBSTYPE","photflatcor",
	    "Observation type",&status)) {
	    sprintf(event,"Writing keyword OBSTYPE=photflatcor failed: %s",
	      photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);  
	  }
	  if (fits_update_key_lng(photflatcor.fptr,"CCDNUM",ccdnum,
	    "CCD number",&status)) {
	    sprintf(event,"Writing keyword CCDNUM=%d failed: %s",
	      ccdnum,photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* Write information into the header describing the processing */
          /* get system time */
          tm=time(NULL);
          sprintf(comment,"%s",asctime(localtime(&tm)));
          comment[strlen(comment)-1]=0;
	  if (fits_write_key_str(photflatcor.fptr,"DESMKPFC",comment,
	    "Created photoflat correction",&status)) {
	    sprintf(event,"Writing processing history failed: %s",
	      photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  sprintf(longcomment,"DESDM:");
	  for (i=0;i<argc;i++) sprintf(longcomment,"%s %s",longcomment,argv[i]);
	  reportevt(flag_verbose,STATUS,1,longcomment);
	  if (fits_write_comment(photflatcor.fptr,longcomment,&status)) {
	    sprintf(event,"Writing longcomment failed: %s",photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (fits_write_key_str(photflatcor.fptr,"DES_EXT","IMAGE",
	    "Image extension", &status)) {
	    sprintf(event,"Writing DES_EXT=IMAGE failed: %s",
	      photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
          /* close the corrected image */
          if (fits_close_file(photflatcor.fptr,&status)) {
	    sprintf(event,"File close failed: %s",photflatcor.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	}

	printf("\n");

	return(0);
}

