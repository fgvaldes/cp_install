#include <fstream>
#include "TF1.h"
#include "Object.h"
#include "TPaveStats.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TCut.h"
#include "FitSLR.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TMath.h"
#include <string>
#include <iostream>
#include "TDirectory.h"
#include "TEventList.h"
#include "TFile.h"
#include "TH2D.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TGraph.h"
#include "TLatex.h"
#include "TDirectory.h"
#include "TSQLServer.h"
#include "TSQLStatement.h"
#include "ExtinctionMap.h"
using namespace std;
typedef KDTree::KDTree<3,CoaddObject> Coadd_Tree;
typedef std::pair<Coadd_Tree::const_iterator,double> Coadd_Match;
void LoadStyle();
void PrintSLRHelp();
void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol);

int main(int argc, char*argv[])
{
    LoadStyle();
  

    char**argv_dummy;
    char char_dummy='p';
    char *char_dummy2=&char_dummy;
    argv_dummy=&char_dummy2;
    int int_dummy=1;
    //TApplication app("app",&int_dummy,argv_dummy);
    

    bool debug=false;

  
    
    string tile="";
    string color_datafile("/home/rearmstr/slr_data/covey.root");
    
    string project="";
    string mag="auto";
    string run="";
    double star_cut_min=0.0;
    double star_cut_max=0.05;
    double err_cut=0.2;
    int cut_points=0;
    double cut_range=-1;
    bool extinction=false;
    bool out_extinction=false;
    bool applycolor=false;
    bool print=false;
    bool remove_outliers=false;
    double sigma_cut=3;
    string outcat;
    string outroot;
    bool truth_match=false;
    string outfile="";
    string image_name="";
    string image_dir="./";
    string nomad_mag="H";
    bool mfp=false;
    double tol=2;
    bool plot_all=false;
    bool insert_db=false;
    string class_star="class_star";
    string ra_string="alpha_j2000";
    string dec_string="delta_j2000";
    bool insert_slrdb=false;
     string which_db="fm";
     bool grz=false;
    string thband="i";
    if(argc==1) {
      PrintSLRHelp();
      exit(1);
    }
    
    for(int ii=1;ii<argc;ii++) {
      string arg=argv[ii];
      if(arg=="-tile")            {tile=argv[ii+1];ii++;}
      else if(arg=="-project")    {project=argv[ii+1];ii++;}
      else if(arg=="-run")        {run=argv[ii+1];ii++;}
      else if(arg=="-outcat")        {outcat=argv[ii+1];ii++;}
      else if(arg=="-outroot")        {outroot=argv[ii+1];ii++;}
      else if(arg=="-class")        {class_star=argv[ii+1];ii++;}
      else if(arg=="-extinction") {extinction=true;}
      else if(arg=="-out_extinction") {out_extinction=true;}
      else if(arg=="-db")    {which_db=argv[ii+1];ii++;}
      else if(arg=="-debug")      {debug=true;}
      else if(arg=="-mfp")      {mfp=true;}
      else if(arg=="-insert_db")      {insert_db=true;}
      else if(arg=="-insert_slrdb")      {insert_slrdb=true;}
      else if(arg=="-plot_all")      {plot_all=true;}
      else if(arg=="-print")      {print=true;}
      else if(arg=="-tol")        {tol=atof(argv[ii+1]);ii++;}
      else if(arg=="-color")      {applycolor=true;}
      else if(arg=="-color_file")  {color_datafile=argv[ii+1];ii++;}
      else if(arg=="-mag")        {mag=argv[ii+1];ii++;}
      else if(arg=="-star_cut")   {
        star_cut_min=atof(argv[ii+1]);
        star_cut_max=atof(argv[ii+2]);
        ii+=2;
      }
      else if(arg=="-image_name") {image_name=argv[ii+1];ii++;}
      else if(arg=="-2mass_mag") {nomad_mag=argv[ii+1];ii++;}
      else if(arg=="-image_dir")  {image_dir=argv[ii+1];ii++;}
      else if(arg=="-err_cut")    {err_cut=atof(argv[ii+1]);ii++;}
      else if(arg=="-sigma_cut")    {sigma_cut=atof(argv[ii+1]);ii++;}
      else if(arg=="-outfile")    {outfile=argv[ii+1];ii++;}
      else if(arg=="-cut_range")  {cut_range=atof(argv[ii+1]);ii++;}
      else if(arg=="-cut_points")    {cut_points=atoi(argv[ii+1]);ii++;}
      else if(arg=="-remove_outliers"){remove_outliers=true;}
      else if(arg=="-truth_match"){truth_match=true;}
      else if(arg=="-grz"){grz=true;}
      else {
        cerr<<"Not a valid command line entry: "<<arg<<endl;
        exit(1);
      }
    }
 
    if(grz) thband="z";
    
    if(class_star=="class") class_star="class_star";
    else if(class_star=="spread") class_star="spread_model";
    else {
      cout<<"Not valid classifier "<<class_star<<endl;
      exit(3);
    }

    tol*=1./3600*TMath::DegToRad();


    TString ts_tile=tile;
    if(ts_tile.BeginsWith("DES")) {
      color_datafile="/home/rearmstr/slr/dc5_colors.root";
    }

    cout<<"Using Color file: "<<color_datafile<<endl;
    if(tile.empty() && project.empty()) {
      cout<<"Must give a valid tilename"<<endl;
    }
    
    
    ExtinctionMap ExtMap("/home/zenteno/REDSHIFT_SURVEYS/EXTINCTION/MAPS_FITS/");
    

    TSQLServer *db; //= TSQLServer::Connect("oracle://desdb/stdes",
    //                      "pipeline", "dc01user");
    string db_cmd;
    TSQLStatement *stmt;
    
    TString db_string;  
    if(which_db=="fm") {
      db_string="oracle://decam-devdb.fnal.gov/fmdes";
    }
    else if(which_db=="pr") {
      db_string="oracle://desdb/prdes";
    }
    else if(which_db=="st") {
      db_string="oracle://desdb/stdes";
    }
    vector<string> run_string;
    vector<string> tile_string;

    TCanvas *c1 = new TCanvas("c1","",0,0,772,802);
    if( project!="DES" && project!="BCS" ) {    
      
      if(run.empty()) {
        
        db = TSQLServer::Connect(db_string,
                                 "pipeline", "dc01user");
        // get the most recent run
        db_cmd=Form("select distinct(run) from catalog where "
                    "tilename='%s' order by run desc",tile.c_str());
        
        if(debug) cout<<db_cmd<<endl;
        stmt=db->Statement(db_cmd.c_str());
        
        stmt->Process();
        stmt->StoreResult();
        stmt->NextResultRow();
        run=stmt->GetString(0);
        delete stmt;
        db->Close();
      }
      

      run_string.push_back(run);
      tile_string.push_back(tile);
    }
    else {

      db = TSQLServer::Connect(db_string,
                               "pipeline", "dc01user");
      db_cmd=Form("select distinct(tilename) from coadd where "
                  "project=\'%s\' and tilename like \'%s%%\' "
                  "order by tilename",project.c_str(),tile.c_str());
      if(debug) cout<<db_cmd<<endl;
      stmt=db->Statement(db_cmd.c_str());

      stmt->Process();
      stmt->StoreResult();
            

      while(stmt->NextResultRow()) {
        string tilename=stmt->GetString(0);
        
        TSQLServer *run_db = TSQLServer::Connect(db_string,
                                                 "pipeline", "dc01user");
        string run_cmd=Form("select distinct(run) from coadd where "
                            "tilename='%s'",
                            tilename .c_str());
        if(debug) cout<<run_cmd<<endl;
        TSQLStatement *run_stmt=run_db->Statement(run_cmd.c_str());
          
        run_stmt->Process();
        run_stmt->StoreResult();
       //  int rows=run_stmt->GetNumAffectedRows();
//         if(rows==0) {
//           if(debug) cout<<"Not using tile: "<<tilename
//                         <<" because there is no valid run"<<endl;
//           continue;
//         }
         
        run="-1";
        while (run_stmt->NextResultRow()) {
          
          run=run_stmt->GetString(0);
          tile_string.push_back(tilename);
          run_string.push_back(run);
        }
        
        if(run=="-1") {
          if(debug) cout<<"Not using tile: "<<tilename
                        <<" because there is no valid run"<<endl;
          continue;
        }


        delete run_stmt;
        run_db->Close();

      }
      delete stmt;
      db->Close();
    }


    
    if(tile_string.size()==0) {
      cout<<"Couldn't find any tiles to run on"<<endl;
      exit(1);
    }
    
    TTree *out_tree=new TTree("tree","");
    double c_gr,c_ri,c_iH;
    double grri_mean,grri_rms;
    double riiz_mean,riiz_rms;
    double izzH_mean,izzH_rms;
    double riiH_mean,riiH_rms;
    double grri_mean_true,grri_rms_true;
    double riiz_mean_true,riiz_rms_true;
    double izzH_mean_true,izzH_rms_true;
    
    char tilename[100],runname[100];

//     out_tree->Branch("tile",&tilename,"tile/C");
//     out_tree->Branch("run",&runname,"run/C");
//     out_tree->Branch("gr",&c_gr,"gr/D");
//     out_tree->Branch("ri",&c_ri,"ri/D");
//     out_tree->Branch("iz",&c_iz,"iz/D");
//     out_tree->Branch("zH",&c_zH,"zH/D");
//     out_tree->Branch("grri_mean",&grri_mean,"grri_mean/D");
//     out_tree->Branch("grri_rms",&grri_rms,"grri_rms/D");
//     out_tree->Branch("riiz_mean",&riiz_mean,"riiz_mean/D");
//     out_tree->Branch("riiz_rms",&riiz_rms,"riiz_rms/D");
//     out_tree->Branch("izzH_mean",&izzH_mean,"izzH_mean/D");
//     out_tree->Branch("izzH_rms",&izzH_rms,"izzH_rms/D");
//     out_tree->Branch("grri_mean_true",&grri_mean_true,"grri_mean_true/D");
//     out_tree->Branch("grri_rms_true",&grri_rms_true,"grri_rms_true/D");
//     out_tree->Branch("riiz_mean_true",&riiz_mean_true,"riiz_mean_true/D");
//     out_tree->Branch("riiz_rms_true",&riiz_rms_true,"riiz_rms_true/D");
//     out_tree->Branch("izzH_mean_true",&izzH_mean_true,"izzH_mean_true/D");
//     out_tree->Branch("izzH_rms_true",&izzH_rms_true,"izzH_rms_true/D");
    
    for(int ii=0;ii<run_string.size();ii++) {
      //    for(int ii=0;ii<1;ii++) {
      //if(ii>10) break;
      cout<<"Processing "<<tile_string[ii]<<" "<<run_string[ii]<<endl;
      int catalog_gid;
      db = TSQLServer::Connect(db_string,
                               "pipeline", "dc01user");

      double alpha;
      // check if there is model fitting or not
      db_cmd=Form("select a.alpha_j2000 from "
                  "coadd_objects a, catalog b where b.tilename="
                  "\'%s\' and b.id=a.catalogid_g and run like "
                  "\'%s%%\' and rownum<2", 
                  tile_string[ii].c_str(),run_string[ii].c_str());

      
      if(debug) cout<<db_cmd<<endl;
      
      stmt=db->Statement(db_cmd.c_str());
      
      stmt->Process();
      stmt->StoreResult();
      
       while(stmt->NextResultRow()) {
         alpha=stmt->GetDouble(0);
         break;
       }
      
       if(alpha==0) {
         ra_string="alphapeak_j2000";
         dec_string="deltapeak_j2000";
       }


       db_cmd=Form("select a.coadd_objects_id, a.%s,a.%s, "
                   "a.mag_%s_g, a.mag_%s_r, a.mag_%s_i, a.mag_%s_z,"
                   "a.magerr_%s_g, a.magerr_%s_r, a.magerr_%s_i, "
                   "a.magerr_%s_z,"
                   "a.%s_g, a.%s_r,a.%s_i,"
                   "a.%s_z, a.catalogid_g, "
                   "a.zeropoint_g,a.zeropoint_r,a.zeropoint_i,a.zeropoint_z, "
                   "a.flags_g, a.flags_r, a.flags_i, a.flags_z "
                   "from coadd_objects a, catalog b where b.tilename="
                   "\'%s\' and b.id=a.catalogid_g and run like \'%s%%\'",
                   ra_string.c_str(),dec_string.c_str(),
                   mag.c_str(),mag.c_str(),mag.c_str(),mag.c_str(),
                   mag.c_str(),mag.c_str(),mag.c_str(),mag.c_str(),
                   class_star.c_str(),class_star.c_str(),class_star.c_str(),
                   class_star.c_str(),
                   tile_string[ii].c_str(),run_string[ii].c_str());
       
       
       stmt=db->Statement(db_cmd.c_str());
       if(debug)cout<<db_cmd<<endl;
       
       if(!stmt->Process()) {
         cout<<stmt->GetErrorMsg()<<endl;
         exit(1);
       }
       stmt->StoreResult();
       
      double ra,dec;
      double g,r,i,z;
      double g_err,r_err,i_err,z_err;
      double g_class,r_class,i_class,z_class;
      int g_flag,r_flag,i_flag,z_flag;
      vector<CoaddObject> objs;
      vector<CoaddObject> all_objs;
      vector<ColorPoint> points,true_points;
      vector<ColorPoint2D> full_points;
      Coadd_Tree true_tree; //store first list in kd tree
      vector<CoaddObject> true_objs;
      double max_ra=-1e6,min_ra=1e6,max_dec=-1e6,min_dec=1e6;
      double zp_g,zp_r,zp_i,zp_z;
      while(stmt->NextResultRow()) {

        ra=stmt->GetDouble(1);
        dec=stmt->GetDouble(2);

        if(ra==0 || dec ==0) continue;

        if(ra>max_ra) max_ra=ra;
        if(dec>max_dec) max_dec=dec;

        if(ra<min_ra) min_ra=ra;
        if(dec<min_dec) min_dec=dec;
        
        g=stmt->GetDouble(3);
        r=stmt->GetDouble(4);
        i=stmt->GetDouble(5);
        z=stmt->GetDouble(6);
        
        
        g_err=stmt->GetDouble(7);
        r_err=stmt->GetDouble(8);
        i_err=stmt->GetDouble(9);
        z_err=stmt->GetDouble(10);
      
        g_class=stmt->GetDouble(11);
        r_class=stmt->GetDouble(12);
        i_class=stmt->GetDouble(13);
        z_class=stmt->GetDouble(14);
        catalog_gid=stmt->GetInt(15);
        zp_g=stmt->GetDouble(16);
        zp_r=stmt->GetDouble(17);
        zp_i=stmt->GetDouble(18);
        zp_z=stmt->GetDouble(19);
	g_flag=stmt->GetInt(20);
	r_flag=stmt->GetInt(21);
	i_flag=stmt->GetInt(22);
	z_flag=stmt->GetInt(23);



        //cout<<catalog_gid<<endl;
        //if(debug)cout<<g_class<<" "<<r_class<<" "<<i_class<<" "<<z_class<<endl;
        //cout<<ra<<" "<<dec<<endl;


       
       //  if( (g_class<star_cut_min || g_class>star_cut_max) &&
//             (r_class<star_cut_min || r_class>star_cut_max) &&
//             (i_class<star_cut_min || i_class>star_cut_max) &&
//             (z_class<star_cut_min || z_class>star_cut_max)) continue;
        //cout<<g_class<<" "<<r_class<<" "<<i_class<<" "<<z_class<<endl;
        if(extinction) {
            
          
          double R_g=3.793;
          double R_r=2.751;
          double R_i=2.086;
          double R_z=1.479;
          
          double EBV=ExtMap.GetExtinction(ra,dec);
          
          /* apply extinction correction in each band */
          g -= R_g * EBV;
          r -= R_r * EBV;
          i -= R_i * EBV;
          z -= R_z * EBV;
        }
        
        double gr=g-r;
        double ri=r-i;
        double iz=i-z;
        if(applycolor) {
          double gr_std=0.53;
          double iz_std=0.09;
          double Bg=-0.122107;
          double Br=-0.012270;
          //double Bi=-0.190718;
          //double Bz=0.022551;

          double ri_std=0.21;
          
          double Br_gr=-0.012270;
          double Br_ri=-0.015;
          double Bi=-0.12;
          
          double gr_cor=( (g-r)+ (Bg-Br)*gr_std)/(1+(Bg-Br))-gr_std;
          
          double ri_cor=( (r-i)+ (Br_ri-Bi)*ri_std)/(1+(Br_ri-Bi));
          
          if(g<99 && r<99)g-=gr_cor*Bg;
          if(r<99 && g<99)r-=gr_cor*Br;
          if(i<99 && r<99){
            i -=ri_cor*Bi;
          }
        }

        // if using grz switch the i and z bands
        // this proba
        if(grz) {
          double tmp,tmp_err,tmp_class;
          tmp=i;tmp_err=i_err;tmp_class=i_class;
          i=z;i_err=z_err;i_class=z_class;
          z=tmp;z_err=tmp_err;z_class=tmp_class;
        }

	CoaddObject obj(0,ra,dec,g,g_err,r,r_err,i,i_err,z,z_err,0,0);
	obj.g_class_star=g_class;
	obj.r_class_star=r_class;
	obj.i_class_star=i_class;
	obj.z_class_star=z_class;
	obj.g_flag=g_flag;
	obj.r_flag=r_flag;
	obj.i_flag=i_flag;
	obj.z_flag=z_flag;
	all_objs.push_back(obj);
	


         if( g>98 || r>98 || i>98 ||
             (g_err>err_cut || r_err>err_cut || i_err>err_cut )
             //|| z_err >err_cut)
            ) continue;
         //cout<<g_class<<" "<<r_class<<" "<<i_class<<endl;
        if( g_class<star_cut_min || g_class>star_cut_max ||
            r_class<star_cut_min || r_class>star_cut_max ||
            i_class<star_cut_min || i_class>star_cut_max //||
            //z_class<star_cut_min || z_class>star_cut_max
            ) continue;

        double cd=cos(obj.Dec*TMath::DegToRad());
        obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
        obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
        obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());
          
        objs.push_back(obj);
          
          
        
      }
      
      cout<<"Found "<<objs.size()<<" objects that pass cuts"<<endl;
      if(debug) cout<<"RA "<<min_ra<<" "<<max_ra<<"   Dec "<<min_dec
                    <<max_dec<<endl;

      Coadd_Tree nomad_tree; //store first list in kd tree
      
      // if we need to go get JHK values from the database
            db = TSQLServer::Connect(db_string,
                               "pipeline", "dc01user");
      
      db_cmd=Form("select ra,dec,J,H,K from nomad where "
                  " (ra between %f and %f) and (dec between %f and %f) "
                  " and J<30",
                  min_ra+tol,max_ra+tol,min_dec+tol,max_dec+tol);

        if(max_ra>350 && min_ra<10) {
        
        db_cmd=Form("select ra,dec,J,H,K from nomad where "
                    " ((ra between 0 and 0.5) or (ra between 359.5 and 360)) "
                    "and (dec between %f and %f) "
                    " and J<30",
                    min_dec+tol,max_dec+tol);
      }

       if(max_dec>350 && min_dec<10) {
        
        db_cmd=Form("select ra,dec,J,H,K from nomad where "
                    " ((dec between 0 and 0.5) or (dec between 359.5 and 360)) "
                    "and (ra between %f and %f) "
                    " and J<30",
                    min_ra+tol,max_ra+tol);
      }
      
      stmt=db->Statement(db_cmd.c_str());
      if(debug) {
        cout<<"Getting nomad data with : "<<endl;
        cout<<db_cmd<<endl;
      }
    
      if(!stmt->Process()) {
        cout<<stmt->GetErrorMsg()<<endl;
        exit(1);
      }
      stmt->StoreResult();

      double nomad_ra,nomad_dec;
      double nomad_j,nomad_h,nomad_k;
      
      while(stmt->NextResultRow()) {
        nomad_ra=stmt->GetDouble(0);
        nomad_dec=stmt->GetDouble(1);
        nomad_j=stmt->GetDouble(2);
        nomad_h=stmt->GetDouble(3);
        nomad_k=stmt->GetDouble(4);
        
        // g,r,i to store j,h,k in coadd structure
        CoaddObject obj(0,nomad_ra,nomad_dec,nomad_j,
                        0,nomad_h,0,nomad_k,0,0,0,0,0);
        
        double cd=cos(obj.Dec*TMath::DegToRad());
        obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
        obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
        obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());
          
        nomad_tree.insert(obj);
      }
      delete stmt;
      db->Close();
      cout<<"Found "<<nomad_tree.size()<<" from NOMAD table"<<endl;

      TFile *tfile=0;
      TTree *tr=0;
      double gg,rr,iii,gs,rs,is,nn;
      double gg_err,rr_err,iii_err;
      int nomad;
      if(!outfile.empty()) {  
        tfile=new TFile(outfile.c_str(),"recreate");
        
        tr=new TTree("tree","");

        tr->Branch("g",&gg,"g/D");
        tr->Branch("g_err",&gg_err,"g_err/D");
        tr->Branch("g_class",&gs,"g_calss/D");
        tr->Branch("r_class",&rs,"r_class/D");
        tr->Branch("i_class",&is,"i_class/D");
        tr->Branch("r",&rr,"r/D");
        tr->Branch("r_err",&rr_err,"r_err/D");
        tr->Branch("i",&iii,"i/D");
        tr->Branch("i_err",&iii_err,"i_err/D");
        tr->Branch("n",&nn,"n/D");
        tr->Branch("nomad",&nomad,"nomad/I");
      }




      for(int i=0;i<objs.size();i++) {
        ColorPoint2D ob2(objs[i].g_mag-objs[i].r_mag,
                         objs[i].r_mag-objs[i].i_mag,
                         sqrt(objs[i].g_mag_err+
                              objs[i].r_mag_err),
                         sqrt(objs[i].i_mag_err+
                              objs[i].r_mag_err));
        full_points.push_back(ob2);
        Coadd_Match match=nomad_tree.find_nearest(objs[i],tol);


        gg=objs[i].g_mag;
        rr=objs[i].r_mag;
        iii=objs[i].i_mag;
        gg_err=objs[i].g_mag_err;
        rr_err=objs[i].r_mag_err;
        iii_err=objs[i].i_mag_err;
        gs=objs[i].g_class_star;
        rs=objs[i].r_class_star;
        is=objs[i].i_class_star;

        if(match.first!=nomad_tree.end()) {
          
          nomad=1;
          double mag;
          nn=mag;
          if(nomad_mag=="H") mag=(*match.first).r_mag;
          else if(nomad_mag=="J") mag=(*match.first).g_mag;
          else if(nomad_mag=="K") mag=(*match.first).i_mag;
          ColorPoint ob3(objs[i].g_mag-objs[i].r_mag,
                         objs[i].r_mag-objs[i].i_mag,
                         objs[i].i_mag-mag,
                         sqrt(objs[i].g_mag_err+
                              objs[i].r_mag_err),
                         sqrt(objs[i].i_mag_err+
                              objs[i].r_mag_err),
                         0);
          points.push_back(ob3);
          
        } else {
          nomad=0;
        }
        if(!outfile.empty()) tr->Fill();
      }
    

      if(!outfile.empty()) {
        tr->Write();
        tfile->Close();
      }

      cout<<"Found "<<points.size()<<" from NOMAD matches"<<endl;
      
      if(points.size()<20) {
        cout<<"Only found "<<points.size()<<" points."
            <<"Not enough to do the fit."<<endl;
        continue;
      }

      string nomad_color;
      if(!grz) {
        if(nomad_mag=="H") nomad_color="i_h";
        else if(nomad_mag=="J") nomad_color="i_J";
        else if(nomad_mag=="K") nomad_color="i_k";
      } else {
        if(nomad_mag=="H") nomad_color="z_h";
        else if(nomad_mag=="J") nomad_color="z_J";
        else if(nomad_mag=="K") nomad_color="z_k";
      }

      FitSLR fit(3,Form("g_r:r_%s:%s",thband.c_str(),
                        nomad_color.c_str()),debug);
      //if(cut_points!=0) fit.SetCutPoints(cut_points);
      //if(cut_range>0) fit.SetCutRange(cut_range);
      if(remove_outliers) {
        fit.SetRemoveOutlier(true);
        fit.SetSigmaCut(sigma_cut);
      }
      //else  fit.SetRemoveOutlier(false);

      TStopwatch timer;
      
      fit.LoadData(color_datafile.c_str());
    
      int marker_style;
      double marker_size;
      double full_marker_size=0.3;
      int full_marker_style=4;
      if(points.size()<500) {
        marker_size=0.5;
        marker_style=4;
      }
      else {
        marker_size=0.4;
        marker_style=20;
      }
    
      vector<double> errs;
      vector<double> pars;
      try {
        pars=fit.Fit(points,errs);
        if(debug)cout<<pars[0]<<" "<<pars[1]<<" "<<pars[2]<<" "<<pars[3]<<endl;
      }
      catch(...) {
        
        cout<<"Not enough points to fit"<<endl;
        continue;
      }
      // fit.RemoveOutliers(points,3,2.5,1e-6);
//       try {
//         pars=fit.Fit(points,errs);
//       }
//       catch(...) {
        
//         cout<<"Not enough points to fit"<<endl;
//         continue;
//       }
//       cout<<pars[0]<<" "<<pars[1]<<" "<<pars[2]<<endl;

      string nomad_title;
      if(nomad_mag=="H") nomad_title="z-H";
      else if(nomad_mag=="J") nomad_title="z-J";
      else if(nomad_mag=="K") nomad_title="z-K";
      vector<double> gr_diff,iH_diff;
      fit.GetDiff(gr_diff,iH_diff);
      vector<double> dum(gr_diff.size());
      double mean,rms;
      int good,bad;
      sigma_clip(3,2.5,gr_diff,dum,mean,
                 good,bad,rms,true,false,1e-6);
      TH1D *hdiff_gr=new TH1D("hdiff_gr",
                              Form(";Difference from Median (g-r,r-%s);"
                                   "Entries",thband.c_str()),
                              50,mean-5*rms,mean+5*rms);

      sigma_clip(3,2.5,gr_diff,dum,mean,
                 good,bad,rms,true,false,1e-6);
      TH1D *hdiff_iH=new TH1D("hdiff_iH",
                              Form(";Difference from Median (r-%s,%s-H)"
                                   ";Entries",thband.c_str(),thband.c_str()),
                              50,mean-5*rms,mean+5*rms);

      
//       sigma_clip(3,2.5,gr_diff,dum,mean,
//                  good,bad,rms,true,false,1e-6);
//       TH1D *hdiff_zH=new TH1D("hdiff_zH",
//                               Form(";Difference from Median (i-z,%s);Entries",
//                                    nomad_title.c_str()),
//                               50,mean-5*rms,mean+5*rms);
      TH1D *hdiff_gr_true;
      TH1D *hdiff_iH_true;
      //TH1D *hdiff_zH_true;
      for(int i=0;i<gr_diff.size();++i)hdiff_gr->Fill(gr_diff[i]);
      for(int i=0;i<iH_diff.size();++i)hdiff_iH->Fill(iH_diff[i]);
      //for(int i=0;i<zH_diff.size();++i)hdiff_zH->Fill(zH_diff[i]);


      
      if(truth_match) {
        
//         db = TSQLServer::Connect(db_string,
//                                  "pipeline", "dc01user");
        
//         db_cmd=Form("select ra_dbl,dec_dbl,g_mag,r_mag,i_mag,z_mag,y_mag,"
//                     "class,mura,mudec from DC5_Truth"
//                     " where (RA between %f and %f) and "
//                     " (DEC between %f and %f)",
//                     min_ra,max_ra,min_dec,max_dec);
//         if(debug) cout<<db_cmd<<endl;
//         stmt=db->Statement(db_cmd.c_str());
//         stmt=db->Statement(db_cmd.c_str());

//         stmt->Process();
//         stmt->StoreResult();
        
//         double g_mag,r_mag,i_mag,z_mag,Y_mag,mura,mudec;
      
//         while(stmt->NextResultRow()) {
//           ra=stmt->GetDouble(0);       
//           dec=stmt->GetDouble(1);       
//           g_mag=stmt->GetDouble(2);       
//           r_mag=stmt->GetDouble(3);       
//           i_mag=stmt->GetDouble(4);       
//           z_mag=stmt->GetDouble(5);       
//           Y_mag=stmt->GetDouble(6);       
//           CoaddObject coadd(0,ra,dec,
//                             g_mag,0,r_mag,0,
//                             i_mag,0,z_mag,0,
//                             0,0);
        
//           double cd=cos(coadd.Dec*TMath::DegToRad());
//           coadd.xyz[0]=cos(coadd.RA*TMath::DegToRad())*cd;
//           coadd.xyz[1]=sin(coadd.RA*TMath::DegToRad())*cd;
//           coadd.xyz[2]=sin(coadd.Dec*TMath::DegToRad());
//           true_tree.insert(coadd);
//         }
      
      
//         // Try to match each selected star to truth catalog
//         vector<CoaddObject>::iterator coadd_iter=objs.begin();
//         for(; coadd_iter!=objs.end(); coadd_iter++) {
        
//           Coadd_Match match=true_tree.find_nearest(*coadd_iter,tol);
        
//           if(match.first!=true_tree.end()) {
//             true_objs.push_back(*match.first);
//           }
      }
      
      cout<<"Total objects: "<<objs.size()<<" Matched: "<<true_objs.size()<<endl;
        // create true color points
        // coadd_iter=true_objs.begin();
        //       for(; coadd_iter!=true_objs.end(); coadd_iter++) {
        //         true_points.push_back(ColorPoint(coadd_iter->g_mag-coadd_iter->r_mag,
        //                                          coadd_iter->r_mag-coadd_iter->i_mag,
        //                                          coadd_iter->i_mag-coadd_iter->z_mag,
        //                                          0,0,0));
        //       }
      
        //       fit.SetPoints(true_points);
      
        vector<double> gr_diff_true,iH_diff_true;
        fit.GetDiff(gr_diff_true,iH_diff_true,false);
        dum.resize(gr_diff_true.size());
        //double mean,rms;
        //int good,bad;
        sigma_clip(3,2.5,gr_diff_true,dum,mean,
                   good,bad,rms,true,false,1e-6);
        hdiff_gr_true =new TH1D("hdiff_gr_true",
                                Form(";Difference from Median (g-r,r-%s);Entries",thband.c_str()),
                                50,mean-5*rms,mean+5*rms);
        hdiff_iH_true=new TH1D("hdiff_iH_true",
                               Form(";Difference from Median (r-%s,%s-H);Entries",thband.c_str(),thband.c_str()),
                               50,mean-5*rms,mean+5*rms);
        for(int i=0;i<gr_diff_true.size();++i)hdiff_gr_true->Fill(gr_diff_true[i]);
        for(int i=0;i<iH_diff_true.size();++i)hdiff_iH_true->Fill(iH_diff_true[i]);
        

        
      fit.SetPoints(full_points);
      vector<double> gr_diff_all;
      fit.GetDiff(gr_diff_all,true);
      dum.clear();
      dum.resize(gr_diff_all.size());

      sigma_clip(3,2.5,gr_diff_all,dum,mean,
                 good,bad,rms,true,false,1e-6);
      TH1D *hdiff_all_gr=new TH1D("hdiff_all_gr",
                                  Form(";Difference from Median (g-r,r-%s);Entries",thband.c_str()),
                              50,mean-20*rms,mean+20*rms);

      for(int i=0;i<gr_diff_all.size();++i)hdiff_all_gr->Fill(gr_diff_all[i]);

        
      
      gStyle->SetStatFormat("4.2g"); 
      gStyle->SetOptStat("e");
      gStyle->SetOptFit(0010);
      gStyle->SetFitFormat("4.2g"); 
      
      hdiff_gr->Fit("gaus","Q");
      TF1 *gaus_fit=hdiff_gr->GetFunction("gaus");
      if (gaus_fit) {
        grri_mean=gaus_fit->GetParameter(1);
        grri_rms=gaus_fit->GetParameter(2);
        
      }
      else {
        grri_mean=-999;
        grri_rms=-999;\
      }
      
      if(truth_match) {

       //  hdiff_gr_true->Fit("gaus");
//         TF1 *gaus_fit_true=hdiff_gr_true->GetFunction("gaus");
//         if (gaus_fit) {
//           grri_mean_true=gaus_fit_true->GetParameter(1);
//           grri_rms_true=gaus_fit_true->GetParameter(2);
          
//         }
//         else {
//           grri_mean_true=-999;
//           grri_rms_true=-999;                        \
//         }
      }
      
        
      
      if(print) {        
        hdiff_gr->SetMaximum(1.2*hdiff_gr->GetMaximum());
        hdiff_gr->Draw();
        if(truth_match) {
          hdiff_gr_true->SetLineColor(kRed);
          hdiff_gr_true->Draw("same");
        }
        
        c1->Update();
        
        TPaveStats *ps_grri = (TPaveStats*)c1->GetPrimitive("stats");
        if(ps_grri) {
        ps_grri->SetName("mystats_grri");
        ps_grri->SetX1NDC(0.60) ; 
        ps_grri->SetX2NDC(0.85) ; 
        ps_grri->SetY1NDC(0.75) ; 
        ps_grri->SetY2NDC(0.87);

        TList *list_grri = ps_grri->GetListOfLines();
        TText *tconst_grri = ps_grri->GetLineWith("Constant"); 
        
        list_grri->Remove(tconst_grri); 
        ps_grri->SetTextSize(0.03);
        ps_grri->SetBorderSize(0);
        ps_grri->SetTextColor(2);
        hdiff_gr->SetStats(0);
        ps_grri->Draw();
        }
        if(image_name.empty()) {
          c1->Print(Form("%s/%s_grr%s_diff_%s.png",image_dir.c_str(),
                        tile_string[ii].c_str(), thband.c_str(),mag.c_str()));
        }
        else {
          c1->Print(Form("%s/%s_grr%s_diff_%s.png",image_dir.c_str(),
                         image_name.c_str(), thband.c_str(),mag.c_str()));
        }


        if(truth_match) {

//           hdiff_gr_true->SetMaximum(1.2*hdiff_gr_true->GetMaximum());
//           hdiff_gr_true->Draw();
          
//           c1->Update();
          
//           TPaveStats *ps_grri = (TPaveStats*)c1->GetPrimitive("stats");
//           if(ps_grri) {
//           ps_grri->SetName("mystats_grri");
//           ps_grri->SetX1NDC(0.60) ; 
//           ps_grri->SetX2NDC(0.85) ; 
//           ps_grri->SetY1NDC(0.75) ; 
//           ps_grri->SetY2NDC(0.87);
//           TList *list_grri = ps_grri->GetListOfLines();
//           TText *tconst_grri = ps_grri->GetLineWith("Constant"); 
          
//           list_grri->Remove(tconst_grri); 
//           ps_grri->SetTextSize(0.03);
//           ps_grri->SetBorderSize(0);
//           ps_grri->SetTextColor(2);
//           hdiff_gr_true->SetStats(0);
//           ps_grri->Draw();
//           }
//           if(image_name.empty()) {
//             c1->Print(Form("%s/%s_grri_diff_true.png",image_dir.c_str(),
//                          tile_string[ii].c_str()));
//           }
//           else {
//             c1->Print(Form("%s/%s_grri_diff_true.png",image_dir.c_str(),
//                            image_name.c_str()));
//           }
//         }
        }
      
      if(debug) {  
        c1->Update();
        c1->WaitPrimitive();
      }
      delete gaus_fit;
      }
      
      hdiff_iH->Fit("gaus","Q");
      gaus_fit=hdiff_iH->GetFunction("gaus");
       if (gaus_fit) {
         riiH_mean=gaus_fit->GetParameter(1);
         riiH_rms=gaus_fit->GetParameter(2);
       }
       else {
         riiH_mean=-999;
         riiH_rms=-999;
       }
      


      if(truth_match) {

//         hdiff_iz_true->Fit("gaus","Q");
//         TF1 *gaus_fit_true=hdiff_iz_true->GetFunction("gaus");
//         if (gaus_fit) {
//           riiz_mean_true=gaus_fit_true->GetParameter(1);
//           riiz_rms_true=gaus_fit_true->GetParameter(2);
          
//         }
//         else {
//           riiz_mean_true=-999;
//           riiz_rms_true=-999;                        \
//         }
      }
      


        
      if(print) {
        hdiff_iH->SetMaximum(1.2*hdiff_iH->GetMaximum());
        hdiff_iH->Draw();
        if(truth_match) {
          hdiff_iH_true->SetLineColor(kRed);
          hdiff_iH_true->Draw("same");
        }
        
        c1->Update();
        
        TPaveStats *ps_riiH = (TPaveStats*)c1->GetPrimitive("stats");
        if(ps_riiH) {
        ps_riiH->SetName("mystats_riiH");
        ps_riiH->SetX1NDC(0.60) ; 
        ps_riiH->SetX2NDC(0.85) ; 
        ps_riiH->SetY1NDC(0.75) ; 
        ps_riiH->SetY2NDC(0.87);
        TList *list_riiH = ps_riiH->GetListOfLines();
        TText *tconst_riiH = ps_riiH->GetLineWith("Constant"); 
        
        list_riiH->Remove(tconst_riiH); 
        ps_riiH->SetTextSize(0.03);
        ps_riiH->SetBorderSize(0);
        ps_riiH->SetTextColor(2);
        hdiff_iH->SetStats(0);
        ps_riiH->Draw();
        }
        
          if(image_name.empty()) {
            c1->Print(Form("%s/%s_r%s%sH_diff_%s.png",image_dir.c_str(),
                           tile_string[ii].c_str(),thband.c_str(),
                           thband.c_str(),mag.c_str()));
          }
          else {
            c1->Print(Form("%s/%s_r%s%sH_diff_%s.png",image_dir.c_str(),
                           image_name.c_str(),thband.c_str(),
                           thband.c_str(),mag.c_str()));
          }
          
          if(truth_match) {
            // hdiff_iH_true->SetMaximum(1.2*hdiff_iH_true->GetMaximum());
//             hdiff_iH_true->Draw();
        
//             c1->Update();
            
//             TPaveStats *ps_riiz = (TPaveStats*)c1->GetPrimitive("stats");
//             if(ps_riiz) {
//             ps_riiz->SetName("mystats_riiz");
//             ps_riiz->SetX1NDC(0.60) ; 
//             ps_riiz->SetX2NDC(0.85) ; 
//             ps_riiz->SetY1NDC(0.75) ; 
//             ps_riiz->SetY2NDC(0.87);
//             TList *list_riiz = ps_riiz->GetListOfLines();
//             TText *tconst_riiz = ps_riiz->GetLineWith("Constant"); 
            
//             list_riiz->Remove(tconst_riiz); 
//             ps_riiz->SetTextSize(0.03);
//             ps_riiz->SetBorderSize(0);
//             ps_riiz->SetTextColor(2);
//             hdiff_iH_true->SetStats(0);
//             ps_riiz->Draw();
//             }
            
//             if(image_name.empty()) {
//               c1->Print(Form("%s/%s_riiH_diff_true.png",image_dir.c_str(),
//                              tile_string[ii].c_str()));
//             }
//             else {
//               c1->Print(Form("%s/%s_riiH_diff_true.png",image_dir.c_str(),
//                              image_name.c_str()));
//             }
//           }
      }


      if(debug) {  
        c1->Update();
        c1->WaitPrimitive();
      }
      
       
      
      delete gaus_fit;
      }


      
      hdiff_all_gr->Fit("gaus","Q");
      gaus_fit=hdiff_all_gr->GetFunction("gaus");
      
      
      if(print) {        
        hdiff_all_gr->SetMaximum(1.2*hdiff_all_gr->GetMaximum());
        hdiff_all_gr->Draw();
                
        c1->Update();
        
        TPaveStats *ps_grri = (TPaveStats*)c1->GetPrimitive("stats");
        if(ps_grri) {
          ps_grri->SetName("mystats_grri_all");
          ps_grri->SetX1NDC(0.60) ; 
          ps_grri->SetX2NDC(0.85) ; 
          ps_grri->SetY1NDC(0.75) ; 
          ps_grri->SetY2NDC(0.87);
          
          TList *list_grri = ps_grri->GetListOfLines();
          TText *tconst_grri = ps_grri->GetLineWith("Constant"); 
          
          list_grri->Remove(tconst_grri); 
          ps_grri->SetTextSize(0.03);
          ps_grri->SetBorderSize(0);
          ps_grri->SetTextColor(2);
          hdiff_all_gr->SetStats(0);
          ps_grri->Draw();
        }
        if(image_name.empty()) {
          c1->Print(Form("%s/%s_grr%s_diff_all_%s.png",image_dir.c_str(),
                         tile_string[ii].c_str(),thband.c_str(),mag.c_str()));
        }
        else {
          c1->Print(Form("%s/%s_grr%s_diff_all_%s.png",image_dir.c_str(),
                         image_name.c_str(),thband.c_str(),mag.c_str()));
        }
        

      }
      
      if(debug) {  
        c1->Update();
        c1->WaitPrimitive();
      }
      delete gaus_fit;
      



      
      c_gr=pars[0];
      c_ri=pars[1];
      c_iH=pars[2];

      sprintf(tilename,"%s",tile_string[ii].c_str());
      sprintf(runname,"%s",run_string[ii].c_str());
      //out_tree->Fill();

      gStyle->SetOptStat(0);
      gStyle->SetLabelSize(0.065,"xy");
      gStyle->SetTitleSize(0.09,"xy");
      gStyle->SetTitleOffset(.8,"yx");
      if(print) {
        TGraph gr_cut_gr_ri,gr_cut_shift_gr_ri,gr_gr_ri_true;
        TGraph gr_cut_iH_ri,gr_cut_shift_iH_ri,gr_iH_ri_true;
        gr_cut_gr_ri.SetMarkerColor(2);
        gr_cut_shift_gr_ri.SetMarkerColor(2);
        gr_gr_ri_true.SetMarkerColor(3);
        gr_cut_gr_ri.SetMarkerStyle(marker_style);
        gr_cut_shift_gr_ri.SetMarkerStyle(marker_style);
        gr_gr_ri_true.SetMarkerStyle(full_marker_style);
        gr_cut_gr_ri.SetMarkerSize(marker_size);
        gr_cut_shift_gr_ri.SetMarkerSize(marker_size);
        gr_gr_ri_true.SetMarkerSize(full_marker_size);


        gr_cut_iH_ri.SetMarkerColor(2);
        gr_cut_shift_iH_ri.SetMarkerColor(2);
        gr_iH_ri_true.SetMarkerColor(3);
        gr_cut_iH_ri.SetMarkerStyle(marker_style);
        gr_cut_shift_iH_ri.SetMarkerStyle(marker_style);
        gr_iH_ri_true.SetMarkerStyle(full_marker_style);
        gr_cut_iH_ri.SetMarkerSize(marker_size);
        gr_cut_shift_iH_ri.SetMarkerSize(marker_size);
        gr_iH_ri_true.SetMarkerSize(full_marker_size);

        for(int i=0;i<points.size();i++) {
          gr_cut_shift_gr_ri.SetPoint(i,pars[1]+points[i].c2,
                                      pars[0]+points[i].c1);
          gr_cut_gr_ri.SetPoint(i,points[i].c2,points[i].c1);
          
          gr_cut_shift_iH_ri.SetPoint(i,pars[1]+points[i].c2,
                                      pars[2]+points[i].c3);
          gr_cut_iH_ri.SetPoint(i,points[i].c2,points[i].c3);
        }

        for(int i=0;i<full_points.size();i++) {
          gr_gr_ri_true.SetPoint(i,pars[1]+full_points[i].c2,
                                   pars[0]+full_points[i].c1);
         //  gr_iH_ri_true.SetPoint(i,pars[1]+full_points[i].c2,
//                                    pars[2]+full_points[i].c3);
        }

        TFile file(color_datafile.c_str());
        TTree *tr=(TTree*)file.Get("tree");
        TH2D *h2=new TH2D("h2",Form(";r_{%s}-%s_{%s};g_{%s}-r_{%s}",
                                    mag.c_str(),thband.c_str(),mag.c_str(),
                                    mag.c_str(),mag.c_str()),
                          //100,-0.5,2.4,100,-0.5,2);
                          100,-4.,5,100,-4.5,4.5);
        TH2D *h3=new TH2D("h3",Form(";r_{%s}-%s_{%s};%s_{%s}-H_{%s}",
                                    mag.c_str(),thband.c_str(),mag.c_str(),
                                    thband.c_str(),mag.c_str(),mag.c_str()),
                          //100,-0.5,2.4,100,0.25,3);
                          100,-4.,5,100,-4.5,4.5);
        h2->GetXaxis()->CenterTitle();
        h2->GetYaxis()->CenterTitle();
        h3->GetXaxis()->CenterTitle();
        h3->GetYaxis()->CenterTitle();

        tr->Draw(Form("g_r:r_%s",thband.c_str()),"","goff");
        TGraph *gr_gr_ri=new TGraph(tr->GetSelectedRows(),tr->GetV2(),
                                    tr->GetV1());
        gr_gr_ri->SetMarkerColor(kBlue);
        gr_gr_ri->SetLineColor(kBlue);

        tr->Draw(Form("%s_h:r_%s",thband.c_str(),thband.c_str()),"","goff");
        TGraph *gr_iH_ri=new TGraph(tr->GetSelectedRows(),tr->GetV2(),
                                    tr->GetV1());

        gr_iH_ri->SetMarkerColor(kBlue);
        gr_iH_ri->SetLineColor(kBlue);
    
        
        c1->Clear();
        TPad *pad1 = new TPad("pad1"," ",0.0,0.5,1.0,1.0,0);
        TPad *pad2 = new TPad("pad2"," ",0.0,0.0,1.0,0.5,0);
        pad1->Draw();
        pad2->Draw();
        pad1->SetBottomMargin(0.0015);
        pad1->SetLeftMargin(0.15);
        pad2->SetLeftMargin(0.15);
        pad2->SetTopMargin(0.005);
        pad2->SetBottomMargin(0.2);

        pad1->cd();
        h2->Draw();

        gr_gr_ri->SetLineWidth(4);
        gr_cut_gr_ri.SetMarkerColor(1);
        gr_gr_ri_true.SetMarkerColor(3);
        gr_cut_gr_ri.Draw("p");
        if(plot_all)gr_gr_ri_true.Draw("p");
        gr_cut_shift_gr_ri.Draw("p");


        TLegend *leg=new TLegend(0.567,0.056,0.817,0.37);
        leg->SetHeader(tile_string[ii].c_str());
        leg->AddEntry(gr_gr_ri,"SDSS Median","l");
        TObject obj;
        TLegendEntry *entry=leg->AddEntry(&obj,
                                          "Fit Uncorrected","ep");
        entry->SetMarkerSize(1.);
        entry->SetMarkerStyle(marker_style);
        entry->SetMarkerColor(1);
    
        entry=leg->AddEntry(&obj,
                            "Fit Corrected","p");
        entry->SetMarkerSize(1.);
        entry->SetMarkerStyle(marker_style);
        entry->SetMarkerColor(2);
        entry=leg->AddEntry(&obj,
                            "All Corrected","p");
        entry->SetMarkerSize(1.);
        entry->SetMarkerStyle(marker_style);
        entry->SetMarkerColor(3);
        leg->AddEntry("",Form("g-r Shift: %0.3f",c_gr),"");
        leg->AddEntry("",Form("r-%s Shift: %0.3f",thband.c_str(),c_ri),"");
        leg->SetBorderSize(0);
        leg->SetTextSize(0.045);
        leg->Draw();
        gr_gr_ri->Draw("l");
    

 
        pad2->cd();
        h3->Draw();
        gr_iH_ri->SetLineWidth(4);
        gr_cut_iH_ri.SetMarkerColor(1);
        gr_iH_ri_true.SetMarkerColor(3);
        gr_cut_iH_ri.Draw("p");
        if(plot_all)gr_iH_ri_true.Draw("p");
        gr_cut_shift_iH_ri.Draw("p");



        TLegend *leg2=new TLegend(0.13,0.83,0.39,0.94);
        leg2->AddEntry("",Form("%s-H Shift: %0.3f",thband.c_str(),c_iH),"");
        leg2->AddEntry("",Form("r-%s Shift: %0.3f",thband.c_str(),c_ri),"");
        leg2->SetBorderSize(0);
        leg2->SetTextSize(0.045);
        leg2->Draw();
        gr_iH_ri->Draw("l");
                

     
        c1->Update();
        if(debug) c1->WaitPrimitive();
        if(image_name.empty()) {
          c1->Print(Form("%s/%s_g%s_%s.png",image_dir.c_str(),
                         tile_string[ii].c_str(),nomad_mag.c_str(),mag.c_str()));
          h2->GetXaxis()->SetRangeUser(-0.5,2.4);
          h2->GetYaxis()->SetRangeUser(-0.5,2);
          h3->GetXaxis()->SetRangeUser(-0.5,2.4);
          h3->GetYaxis()->SetRangeUser(0.5,3.5);
          c1->Print(Form("%s/%s_g%s_%s_zoom.png",image_dir.c_str(),
                         tile_string[ii].c_str(),nomad_mag.c_str(),mag.c_str()));
        }
        else { 
          c1->Print(Form("%s/%s_g%s_%s.png",image_dir.c_str(),
                         image_name.c_str(),nomad_mag.c_str(),mag.c_str()));
          h2->GetXaxis()->SetRangeUser(-0.5,2.4);
          h2->GetYaxis()->SetRangeUser(-0.5,2);
          h3->GetXaxis()->SetRangeUser(-0.5,2.4);
          h3->GetYaxis()->SetRangeUser(0.5,3.5);
          c1->Print(Form("%s/%s_g%_%s_zoom.png",image_dir.c_str(),
                         image_name.c_str(),nomad_mag.c_str(),mag.c_str()));
        }
        
      }
      double z_corr=pars[3];
      double i_corr=pars[2]+z_corr;
      double r_corr=pars[1]+i_corr;
      double g_corr=pars[0]+r_corr;

      cout<<"Calibrations"<<endl;
      cout<<"g-band: "<<g_corr<<endl;
      cout<<"r-band: "<<r_corr<<endl;
      if(!grz) {
        cout<<"i-band: "<<i_corr<<endl;
      }
      else {
        cout<<"z-band: "<<i_corr<<endl;
      }
      
      if(insert_db) {
      
//         db = TSQLServer::Connect(db_string,
//                                  "des_admin", "desmgr");
        
//         string db_cmd=Form("update coadd_objects  set " 
//                            "zeropoint_g=(zeropoint_g+%f),"
//                            "zeropoint_r=(zeropoint_r+%f),"
//                            "zeropoint_i=(zeropoint_i+%f),"
//                            "zeropoint_z=(zeropoint_z+%f),"
//                            "mag_model_g=(mag_model_g+%f),"
//                            "mag_model_r=(mag_model_r+%f),"
//                            "mag_model_i=(mag_model_i+%f),"
//                            "mag_model_z=(mag_model_z+%f),"
//                            "mag_auto_g=(mag_auto_g+%f),"
//                            "mag_auto_r=(mag_auto_r+%f),"
//                            "mag_auto_i=(mag_auto_i+%f),"
//                            "mag_auto_z=(mag_auto_z+%f),"
//                            "mag_aper1_g=(mag_aper1_g+%f),"
//                            "mag_aper2_g=(mag_aper2_g+%f),"
//                            "mag_aper3_g=(mag_aper3_g+%f),"
//                            "mag_aper4_g=(mag_aper4_g+%f),"
//                            "mag_aper5_g=(mag_aper5_g+%f),"
//                            "mag_aper6_g=(mag_aper6_g+%f),"
//                            "mag_aper_7_g=(mag_aper_7_g+%f),"
//                            "mag_aper_8_g=(mag_aper_8_g+%f),"
//                            "mag_aper_9_g=(mag_aper_9_g+%f),"
//                            "mag_aper_10_g=(mag_aper_10_g+%f),"
//                            "mag_aper_11_g=(mag_aper_11_g+%f),"
//                            "mag_aper_12_g=(mag_aper_12_g+%f),"
//                            "mag_aper_13_g=(mag_aper_13_g+%f),"
//                            "mag_aper_14_g=(mag_aper_14_g+%f),"
//                            "mag_aper_15_g=(mag_aper_15_g+%f),"
//                            "mag_aper_16_g=(mag_aper_16_g+%f),"
//                            "mag_aper_17_g=(mag_aper_17_g+%f),"
//                            "mag_aper1_r=(mag_aper1_r+%f),"
//                            "mag_aper2_r=(mag_aper2_r+%f),"
//                            "mag_aper3_r=(mag_aper3_r+%f),"
//                            "mag_aper4_r=(mag_aper4_r+%f),"
//                            "mag_aper5_r=(mag_aper5_r+%f),"
//                            "mag_aper6_r=(mag_aper6_r+%f),"
//                            "mag_aper_7_r=(mag_aper_7_r+%f),"
//                            "mag_aper_8_r=(mag_aper_8_r+%f),"
//                            "mag_aper_9_r=(mag_aper_9_r+%f),"
//                            "mag_aper_10_r=(mag_aper_10_r+%f),"
//                            "mag_aper_11_r=(mag_aper_11_r+%f),"
//                            "mag_aper_12_r=(mag_aper_12_r+%f),"
//                            "mag_aper_13_r=(mag_aper_13_r+%f),"
//                            "mag_aper_14_r=(mag_aper_14_r+%f),"
//                            "mag_aper_15_r=(mag_aper_15_r+%f),"
//                            "mag_aper_16_r=(mag_aper_16_r+%f),"
//                            "mag_aper_17_r=(mag_aper_17_r+%f),",
//                            g_corr,r_corr,i_corr,z_corr,
//                            g_corr,r_corr,i_corr,z_corr,
//                            g_corr,r_corr,i_corr,z_corr,
//                            g_corr,g_corr,g_corr,g_corr,g_corr,g_corr,g_corr,
//                            g_corr,g_corr,g_corr,g_corr,g_corr,g_corr,g_corr,
//                            g_corr,g_corr,g_corr,
//                            r_corr,r_corr,r_corr,r_corr,r_corr,r_corr,r_corr,
//                            r_corr,r_corr,r_corr,r_corr,r_corr,r_corr,r_corr,
//                            r_corr,r_corr,r_corr);
                           
//         string db_cmd2=Form("mag_aper1_i=(mag_aper1_i+%f),"
//                             "mag_aper2_i=(mag_aper2_i+%f),"
//                             "mag_aper3_i=(mag_aper3_i+%f),"
//                             "mag_aper4_i=(mag_aper4_i+%f),"
//                             "mag_aper5_i=(mag_aper5_i+%f),"
//                             "mag_aper6_i=(mag_aper6_i+%f),"
//                             "mag_aper_7_i=(mag_aper_7_i+%f),"
//                             "mag_aper_8_i=(mag_aper_8_i+%f),"
//                             "mag_aper_9_i=(mag_aper_9_i+%f),"
//                             "mag_aper_10_i=(mag_aper_10_i+%f),"
//                             "mag_aper_11_i=(mag_aper_11_i+%f),"
//                             "mag_aper_12_i=(mag_aper_12_i+%f),"
//                             "mag_aper_13_i=(mag_aper_13_i+%f),"
//                             "mag_aper_14_i=(mag_aper_14_i+%f),"
//                             "mag_aper_15_i=(mag_aper_15_i+%f),"
//                             "mag_aper_16_i=(mag_aper_16_i+%f),"
//                             "mag_aper_17_i=(mag_aper_17_i+%f),"
//                             "mag_aper1_z=(mag_aper1_z+%f),"
//                             "mag_aper2_z=(mag_aper2_z+%f),"
//                             "mag_aper3_z=(mag_aper3_z+%f),"
//                             "mag_aper4_z=(mag_aper4_z+%f),"
//                             "mag_aper5_z=(mag_aper5_z+%f),"
//                             "mag_aper6_z=(mag_aper6_z+%f),"
//                             "mag_aper_7_z=(mag_aper_7_z+%f),"
//                             "mag_aper_8_z=(mag_aper_8_z+%f),"
//                             "mag_aper_9_z=(mag_aper_9_z+%f),"
//                             "mag_aper_10_z=(mag_aper_10_z+%f),"
//                             "mag_aper_11_z=(mag_aper_11_z+%f),"
//                             "mag_aper_12_z=(mag_aper_12_z+%f),"
//                             "mag_aper_13_z=(mag_aper_13_z+%f),"
//                             "mag_aper_14_z=(mag_aper_14_z+%f),"
//                             "mag_aper_15_z=(mag_aper_15_z+%f),"
//                             "mag_aper_16_z=(mag_aper_16_z+%f),"
//                             "mag_aper_17_z=(mag_aper_17_z+%f),",
//                             i_corr,i_corr,i_corr,i_corr,i_corr,i_corr,i_corr,
//                             i_corr,i_corr,i_corr,i_corr,i_corr,i_corr,i_corr,
//                             i_corr,i_corr,i_corr,
//                             z_corr,z_corr,z_corr,z_corr,z_corr,z_corr,z_corr,
//                             z_corr,z_corr,z_corr,z_corr,z_corr,z_corr,z_corr,
//                             z_corr,z_corr,z_corr);

//         string db_cmd3=Form("mag_iso_g=(mag_iso_g+%f),"
//                             "mag_iso_r=(mag_iso_r+%f),"
//                             "mag_iso_i=(mag_iso_i+%f),"
//                             "mag_iso_z=(mag_iso_z+%f),"
//                             "mag_petro_g=(mag_petro_g+%f),"
//                             "mag_petro_r=(mag_petro_r+%f),"
//                             "mag_petro_i=(mag_petro_i+%f),"
//                             "mag_petro_z=(mag_petro_z+%f),"
//                             "mag_psf_g=(mag_psf_g+%f),"
//                             "mag_psf_r=(mag_psf_r+%f),"
//                             "mag_psf_i=(mag_psf_i+%f),"
//                             "mag_psf_z=(mag_psf_z+%f),"
//                             "mag_spheroid_g=(mag_spheroid_g+%f),"
//                             "mag_spheroid_r=(mag_spheroid_r+%f),"
//                             "mag_spheroid_i=(mag_spheroid_i+%f),"
//                             "mag_spheroid_z=(mag_spheroid_z+%f) where "
//                             "catalogid_g='%d'",
//                             g_corr,r_corr,i_corr,z_corr,
//                             g_corr,r_corr,i_corr,z_corr,
//                             g_corr,r_corr,i_corr,z_corr,
//                             g_corr,r_corr,i_corr,z_corr,catalog_gid);

//         db_cmd+=db_cmd2+db_cmd3;
//         cout<<db_cmd<<endl;
//         stmt=db->Statement(db_cmd.c_str());
//         stmt->Process();
//         //db->Commit();
       
//         db->Close();  
      }

      if(insert_slrdb) {

        if(grz) {
          cout<<"Not ready to insert into slrdb.  Skipping"<<endl;
        }
        else {
        db = TSQLServer::Connect(db_string,
                                 "pipeline", "dc01user");

        // check if objects from this tile have 
        // already has been inserted
        // Currently we have to update if
        double g_corr=-99;
        string db_cmd=Form("select g_corr from slr_cal where run='%s'",
                           run.c_str());
        stmt=db->Statement(db_cmd.c_str());
        stmt->Process();
        stmt->StoreResult();
        bool ok=stmt->NextResultRow();
                
        if(ok) {
          cout<<"Run already ingested slr_cal, deleting old entries "
              <<"in database"<<endl;

          db_cmd=Form("delete from slr_cal where run='%s'",
                      run.c_str());
          cout<<db_cmd<<endl;
          stmt=db->Statement(db_cmd.c_str());
          stmt->Process();
        }
        
        db_cmd=Form("insert into slr_cal (run,tile,"
                    "g_corr,r_corr,i_corr,z_corr,"
                    "gr_ri_mean,ri_iH_mean,"
                    "gr_ri_sigma,ri_iH_sigma) " 
                    "values ('%s','%s',%f,%f,%f,%f,%f,%f,%f,%f)",
                    run.c_str(),tile.c_str(),g_corr,r_corr,i_corr,-999,
                    grri_mean,riiH_mean,
                    grri_rms,riiH_rms);
        cout<<db_cmd<<endl;
        stmt=db->Statement(db_cmd.c_str());
        stmt->Process();
        db->Commit();
        db->Close();  
        }
      }
        

      if(!outcat.empty()) {
	ofstream out(outcat.c_str());
        out.precision(10);    

	vector<CoaddObject>::iterator coadd_iter=all_objs.begin();
        for(; coadd_iter!=all_objs.end(); coadd_iter++) {

	  if(coadd_iter->g_mag<99)coadd_iter->g_mag+=g_corr;
	  if(coadd_iter->r_mag<99)coadd_iter->r_mag+=r_corr;
	  if(coadd_iter->i_mag<99)coadd_iter->i_mag+=i_corr;
         //  cout<<coadd_iter->g_mag<<" "<<coadd_iter->r_mag<<" "
//               <<coadd_iter->i_mag<<" "<<coadd_iter->g_mag_err<<" "
//               <<coadd_iter->r_mag_err<<" "
//               <<coadd_iter->i_mag_err<<" "<<endl;


            
          if( (coadd_iter->g_mag>98 && coadd_iter->r_mag>98 && 
               coadd_iter->i_mag>98) ||
              (coadd_iter->g_mag_err==0 && coadd_iter->r_mag_err==0 &&
               coadd_iter->i_mag_err==0 )) continue;
               

          if(coadd_iter->g_mag_err==0) {coadd_iter->g_mag=99;coadd_iter->g_mag_err=99;}
          if(coadd_iter->r_mag_err==0) {coadd_iter->r_mag=99;coadd_iter->r_mag_err=99;}
          if(coadd_iter->i_mag_err==0) {coadd_iter->i_mag=99;coadd_iter->i_mag_err=99;}
          
          if(out_extinction) {
            
            // apply extinction
            double R_g=3.793;
            double R_r=2.751;
            double R_i=2.086;
            double R_z=1.479;
            
            double EBV=ExtMap.GetExtinction(coadd_iter->RA,coadd_iter->Dec);
          
            /* apply extinction correction in each band */
            if(coadd_iter->g_mag<99)coadd_iter->g_mag -= R_g * EBV;
            if(coadd_iter->r_mag<99)coadd_iter->r_mag -= R_r * EBV;
            if(!grz) {
              if(coadd_iter->i_mag<99)coadd_iter->i_mag -= R_i * EBV;
            }
            else {
              if(coadd_iter->i_mag<99)coadd_iter->i_mag -= R_z * EBV;
            }
          }
          if(!grz) {
            out<<coadd_iter->RA<<" "<<coadd_iter->Dec<<" "
               <<coadd_iter->g_mag<<" "<<coadd_iter->g_mag_err<<" "
               <<coadd_iter->g_flag<<" "<<coadd_iter->g_class_star<<" "
               <<coadd_iter->r_mag<<" "<<coadd_iter->r_mag_err<<" "
               <<coadd_iter->r_flag<<" "<<coadd_iter->r_class_star<<" "
               <<coadd_iter->i_mag<<" "<<coadd_iter->i_mag_err<<" "
               <<coadd_iter->i_flag<<" "<<coadd_iter->i_class_star<<" "
               <<" 99 99 99 99"<<endl;
          } else {
            out<<coadd_iter->RA<<" "<<coadd_iter->Dec<<" "
               <<coadd_iter->g_mag<<" "<<coadd_iter->g_mag_err<<" "
               <<coadd_iter->g_flag<<" "<<coadd_iter->g_class_star<<" "
               <<coadd_iter->r_mag<<" "<<coadd_iter->r_mag_err<<" "
               <<coadd_iter->r_flag<<" "<<coadd_iter->r_class_star
               <<" 99 99 99 99 "
               <<coadd_iter->i_mag<<" "<<coadd_iter->i_mag_err<<" "
               <<coadd_iter->i_flag<<" "<<coadd_iter->i_class_star<<endl;
           
          }

            
        }
      }
    }
    
    delete c1;    
   
 





}
    

    
  
// Set style

void LoadStyle()
{


  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  TStyle* miStyle = new  TStyle("miStyle", "MI Style");

  // Colors


  //  set the background color to white
  miStyle->SetFillColor(10);


  miStyle->SetFrameFillColor(10);
  miStyle->SetCanvasColor(10);
  miStyle->SetCanvasDefH(680);
  miStyle->SetCanvasDefW(700);
  miStyle->SetPadColor(10);
  miStyle->SetTitleFillColor(0);
  miStyle->SetStatColor(10);

  //  //dont put a colored frame around the plots
  miStyle->SetFrameBorderMode(0);
  miStyle->SetCanvasBorderMode(0);
  miStyle->SetPadBorderMode(0);

  //use the primary color palette
  miStyle->SetPalette(1);

  //set the default line color for a histogram to be black
  miStyle->SetHistLineColor(kBlack);

  //set the default line color for a fit function to be red
  miStyle->SetFuncColor(kBlue);

  //make the axis labels black
  miStyle->SetLabelColor(kBlack,"xyz");

  //set the default title color to be black
  miStyle->SetTitleColor(kBlack);

  // Sizes
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);


  //set the margins
  miStyle->SetPadBottomMargin(0.12);
  miStyle->SetPadTopMargin(0.1);
  miStyle->SetPadLeftMargin(0.14);
  miStyle->SetPadRightMargin(0.14);

  //set axis label and title text sizes
  miStyle->SetLabelSize(0.04,"x");
  miStyle->SetLabelSize(0.04,"y");
  miStyle->SetTitleSize(0.05,"xyz");
  miStyle->SetTitleOffset(1.1,"x");
  miStyle->SetTitleOffset(1.3,"yz");
  miStyle->SetLabelOffset(0.012,"y");
  miStyle->SetStatFontSize(0.025);
  miStyle->SetTextSize(0.02);
  miStyle->SetTitleBorderSize(0);

  //set line widths
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);

  // Misc

  //align the titles to be centered
  miStyle->SetTextAlign(22);

  //set the number of divisions to show
  // miStyle->SetNdivisions(506, "xy");

  //turn off xy grids
  miStyle->SetPadGridX(0);
  miStyle->SetPadGridY(0);

  //set the tick mark style
  miStyle->SetPadTickX(1);
  miStyle->SetPadTickY(1);

  //show the fit parameters in a box
  miStyle->SetOptFit(0111);
  miStyle->SetOptTitle(0);

  //turn off all other stats
  miStyle->SetOptStat(0);
  miStyle->SetStatW(0.20);
  miStyle->SetStatH(0.15);
  miStyle->SetStatX(0.94);
  miStyle->SetStatY(0.92);


  miStyle->SetFillStyle(0);

  //  // Fonts
  miStyle->SetStatFont(42);
  miStyle->SetLabelFont(42,"xyz");
  miStyle->SetTitleFont(42,"xyz");
  // miStyle->SetTextFont(40);

  //done

  miStyle->cd();


  // gROOT->ForceStyle(1);
 

}

void PrintSLRHelp()
{
  cout<<"Usage: slr_check -tile (tile) (options)\n";
  cout<<"Options (default):\n";
  cout<<"  -project    (none)       For looping over all tiles in BCS/DES.\n";
  cout<<"  -run        (latest)     Use a specific run.   \n";
  cout<<"  -color      (no)         Apply BCS color corrections\n";
  cout<<"  -mag        (auto)       Which magnitude to use\n";
  cout<<"  -star_cut   (0.7)        Value of class_star to determine stars\n";
  cout<<"  -err_cut    (0.2)        Maximum flux error\n";
  cout<<"  -outfile    (out.root)   Output file (only if using -project)\n";
  cout<<"  -extinction (no)         Apply extinction correction\n";
  cout<<"  -image_name ((tile).png) Name of output\n";
  cout<<"  -image_dir  (./)         Name of directory for output\n";
  cout<<"  -print      (no)         produce plots\n";
  cout<<"  -tol        (2)          matching radius\n";
  cout<<"  -mfp        (no)         use model fitting parameters\n";
}


void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol)
{
  int N=array.size();
  mean=TMath::Mean(N,&array[0]);
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  // initial flag of bad objs
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }

  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;


  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();
    newerr_array.clear();
    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;
    
  }

  

  
  return;

}
