


#include "imageproc.h"
#include "fitswcs.h"
#include "define.h"
#include "math.h"
#include "fitscat_defs.h"
 
 

int main(int argc, char* argv[])
/* int get_pix(char *ctype1,char *ctype2, int axes0,int axes1, */
/*             double crval1,double crval1,double crpix1,double crpix2, */
/*             double epoch */
            
{
  double ra1,ra2,ra3,ra4,dec1,dec2,dec3,dec4,cdec,cra;
  char **ctype;
  double *cd1, *cd2, *crval, *crpix, *cdelt;
  int *naxisn;
  double wcspos[2],rawpos[2];
  char ctype1[32],ctype2[32];
  wcsstruct *wcsin;

  double pv[2][11];
  int naxis=2; /* hardwired at the moment */
  int i;
  double equinox_in;
  double epoch=2000.0;
  
  QMALLOC(ctype,char *,2);
  for(i=0;i<16;i++) {
    ctype[i]=(char *)calloc(20,sizeof(char));
  }
  QMALLOC(crpix,double,2);
  QMALLOC(crval,double,2);
  QMALLOC(cd1,double,2);
  QMALLOC(cd2,double,2);
  QMALLOC(cdelt,double,2);
  QMALLOC(naxisn,int,2);


   for (i=1;i<argc;i++) {
     
     if (!strcmp(argv[i],"-ctype"))  {
       
       if(argv[i+1]==NULL || argv[i+2]==NULL) {
         printf(" ** %s ERROR: input for -ctype option is not set."
                "  Abort!\n",argv[0]);
         exit(1);
       }
       sprintf(ctype[0],"%s",argv[i+1]);
       sprintf(ctype[1],"%s",argv[i+2]);
       i+=2;
     }
     else if (!strcmp(argv[i],"-axis"))  {
       
      if(argv[i+1]==NULL || argv[i+2]==NULL) {
        printf(" ** %s ERROR: input for -axis option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
      naxisn[0]=atoi(argv[i+1]);
      naxisn[1]=atoi(argv[i+2]);
      i+=2;
    }
    else if (!strcmp(argv[i],"-crval"))  {

      if(argv[i+1]==NULL || argv[i+2]==NULL ) {
        printf(" ** %s ERROR: input for -crval option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
      crval[0]=atof(argv[i+1]);
      crval[1]=atof(argv[i+2]);
      i+=2;
    }
    else if (!strcmp(argv[i],"-crpix"))  {

      if(argv[i+1]==NULL || argv[i+2]==NULL) {
        printf(" ** %s ERROR: input for -crpix option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
      crpix[0]=atof(argv[i+1]);
      crpix[1]=atof(argv[i+2]);
      i+=2;
    }
    else if (!strcmp(argv[i],"-epoch"))  {

      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -epoch option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
      epoch=atof(argv[i+1]);
      ++i;
    }
    else if (!strcmp(argv[i],"-cd1"))  {

      if(argv[i+1]==NULL || argv[i+2]==NULL) {
        printf(" ** %s ERROR: input for -cd1 option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
      cd1[0]=atof(argv[i+1]);
      cd1[1]=atof(argv[i+2]);
      i+=2;
    }
    else if (!strcmp(argv[i],"-cd2"))  {

      if(argv[i+1]==NULL || argv[i+2]==NULL) {
        printf(" ** %s ERROR: input for -cd2 option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
      cd2[0]=atof(argv[i+1]);
      cd2[1]=atof(argv[i+2]);
      i+=2;
    }
   else if (!strcmp(argv[i],"-equinox"))  {

      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -equinox option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
      equinox_in=atof(argv[i+1]);
      printf("%f\n",equinox_in);
      ++i;
   }
   else if (!strcmp(argv[i],"-pv1"))  {

      if(argv[i+1]==NULL || argv[i+2]==NULL || argv[i+3]==NULL ||
         argv[i+4]==NULL || argv[i+5]==NULL || argv[i+6]==NULL ||
         argv[i+7]==NULL || argv[i+8]==NULL || argv[i+9]==NULL ||
         argv[i+10]==NULL  ||  argv[i+11]==NULL) {
        printf(" ** %s ERROR: input for -pv1 option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
    
      pv[0][0]=atof(argv[i+1]);
      pv[0][1]=atof(argv[i+2]);
      pv[0][2]=atof(argv[i+3]);
      pv[0][3]=atof(argv[i+4]);
      pv[0][4]=atof(argv[i+5]);
      pv[0][5]=atof(argv[i+6]);
      pv[0][6]=atof(argv[i+7]);
      pv[0][7]=atof(argv[i+8]);
      pv[0][8]=atof(argv[i+9]);
      pv[0][9]=atof(argv[i+10]);
      pv[0][10]=atof(argv[i+11]);
      i+=11;
   }
   else if (!strcmp(argv[i],"-pv2"))  {

      if(argv[i+1]==NULL || argv[i+2]==NULL || argv[i+3]==NULL ||
         argv[i+4]==NULL || argv[i+5]==NULL || argv[i+6]==NULL ||
         argv[i+7]==NULL || argv[i+8]==NULL || argv[i+9]==NULL ||
         argv[i+10]==NULL ||  argv[i+11]==NULL) {
        printf(" ** %s ERROR: input for -pv2 option is not set."
               "  Abort!\n",argv[0]);
        exit(1);
      }
   
      pv[1][0]=atof(argv[i+1]);
      pv[1][1]=atof(argv[i+2]);
      pv[1][2]=atof(argv[i+3]);
      pv[1][3]=atof(argv[i+4]);
      pv[1][4]=atof(argv[i+5]);
      pv[1][5]=atof(argv[i+6]);
      pv[1][6]=atof(argv[i+7]);
      pv[1][7]=atof(argv[i+8]);
      pv[1][8]=atof(argv[i+9]);
      pv[1][9]=atof(argv[i+10]);
      pv[1][10]=atof(argv[i+11]);
      i+=11;
   }
   else {
     printf(" ** %s ERROR: no option %s"
            "  Abort!\n",argv[0],argv[i]);
     exit(1);
   }
   }
   
   
   cdelt[0] = sqrt((cd1[0]*cd1[0]) + (cd1[1]*cd1[1]));
   cdelt[1] = sqrt((cd2[0]*cd2[0]) + (cd2[1]*cd2[1]));
   
   /* create a wcs structure to put the data in */
   wcsin = create_wcs(ctype,crval,crpix,cdelt,naxisn,naxis);
   wcsin->obsdate = 0.0;
   wcsin->equinox = equinox_in;
   
   wcsin->projp[0] = pv[0][0];
   wcsin->projp[1] = pv[0][1];
   wcsin->projp[2] = pv[0][2];
   wcsin->projp[3] = pv[0][3];
   wcsin->projp[4] = pv[0][4];
   wcsin->projp[5] = pv[0][5];
   wcsin->projp[6] = pv[0][6];
   wcsin->projp[7] = pv[0][7];
   wcsin->projp[8] = pv[0][8];
   wcsin->projp[9] = pv[0][9];
   wcsin->projp[10]= pv[0][10];
   
   wcsin->projp[100] = pv[1][0];
   wcsin->projp[101] = pv[1][1];
   wcsin->projp[102] = pv[1][2];
   wcsin->projp[103] = pv[1][3];
   wcsin->projp[104] = pv[1][4];
   wcsin->projp[105] = pv[1][5];
   wcsin->projp[106] = pv[1][6];
   wcsin->projp[107] = pv[1][7];
   wcsin->projp[108] = pv[1][8];
   wcsin->projp[109] = pv[1][9];
   wcsin->projp[110] = pv[1][10];
    
  
  wcsin->epoch = epoch; /* hardwired at the moment */
  
  wcsin->cd[0] = cd1[0];
  wcsin->cd[1] = cd1[1];
  wcsin->cd[2] = cd2[0];
  wcsin->cd[3] = cd2[1];
  wcsin->radecsys = RDSYS_FK5;
  
  init_wcs(wcsin);
  
  sprintf(wcsin->cunit[0],"%s","deg");
  sprintf(wcsin->cunit[1],"%s","deg");
  
  /* getting the four courners here */
  rawpos[0]=0.0;
  rawpos[1]=0.0;
  raw_to_wcs(wcsin,rawpos,wcspos);
  ra1=wcspos[0];
  dec1=wcspos[1];
  
  rawpos[0]=(double)naxisn[0];
  rawpos[1]=0.0;
  raw_to_wcs(wcsin,rawpos,wcspos);
  ra2=wcspos[0];
  dec2=wcspos[1];
  
  rawpos[0]=0.0;
  rawpos[1]=(double)naxisn[1];
  raw_to_wcs(wcsin,rawpos,wcspos);
  ra3=wcspos[0];
  dec3=wcspos[1];
  
  rawpos[0]=(double)naxisn[0];
  rawpos[1]=(double)naxisn[1];
  raw_to_wcs(wcsin,rawpos,wcspos);
  ra4=wcspos[0];
  dec4=wcspos[1];

  /* get the center */
  rawpos[0]=(double)naxisn[0]/2;
  rawpos[1]=(double)naxisn[1]/2;
  raw_to_wcs(wcsin,rawpos,wcspos);
  cra=wcspos[0];
  cdec=wcspos[1];

  
  

  double maxra,minra,maxdec,mindec;
  maxra=ra1;
  if(ra2>maxra) maxra=ra2;
  if(ra3>maxra) maxra=ra3;
  if(ra4>maxra) maxra=ra4;
  
  minra=ra1;
  if(ra2<minra) minra=ra2;
  if(ra3<minra) minra=ra3;
  if(ra4<minra) minra=ra4;
  
  maxdec=dec1;
  if(dec2>maxdec) maxdec=dec2;
  if(dec3>maxdec) maxdec=dec3;
  if(dec4>maxdec) maxdec=dec4;
  
  mindec=dec1;
  if(dec2<mindec) mindec=dec2;
  if(dec3<mindec) mindec=dec3;
  if(dec4<mindec) mindec=dec4;

  printf("RA:%3.10f %3.10f %3.10f Dec:%3.10f %3.10f %3.10f\n",cra,minra,maxra,cdec,mindec,maxdec);
  
  
}
