/*
**
** mkCoaddModelList.c
**
** Program to produce a list of parameters for
** model fitting photometry on Coadds
**
** usage: mkCoaddModelList --l <outfile>
**
** Author: Gurvan Bazin (gbazin@usm.uni-muenchen.de)
** Date: 14 January 2010
**
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>

#include "ora_utils.h"



void usage(){
	fprintf(stderr, "mkCoaddModelList -l <outfile>\n");
}


int main(int argc, char** argv) {

	char* filename = NULL;
	int index;
	int c;
	int i;

	sword status;
	int numCoaddObj;
        fit_modelStruct* fit_model;

	FILE* fileout;




	if(argc<2){
		usage();
		exit(2);
	}

	opterr = 0;
	while ((c = getopt (argc, argv, "l:")) != -1)
         switch (c)
           {
           case 'l':
             filename = optarg;
             break;
           case '?':
             if (optopt == 'l')
               fprintf (stderr, "Option -%c requires an argument.\n", optopt);
             else if (isprint (optopt))
               fprintf (stderr, "Unknown option `-%c'.\n", optopt);
             else
               fprintf (stderr,
                        "Unknown option character `\\x%x'.\n",
                        optopt);
             return 1;
           default:
             abort ();
           }
     
       for (index = optind; index < argc; index++)
         printf ("Non-option argument %s\n", argv[index]);



	//
	// Establish a DB connection
	//
	status = dbconnect();
	if (status){
	  fprintf(stderr, "DB ERROR:  Database connection failed.");
	  exit(4);
	}

	//
	// Allocate statement handle
	//
	status = OCIHandleAlloc(envhp, (dvoid **) &sthp, OCI_HTYPE_STMT,
        	                (size_t) 0, (dvoid **) 0);
	checkDBerr(errhp,status);


	// Get DES Coadd Magnitudes
 	numCoaddObj = getNumCoaddObj();
	printf("%d objects in the coadd_objects table\n", numCoaddObj);
        fit_model = (fit_modelStruct *) malloc(sizeof(fit_modelStruct) * numCoaddObj);
	getFitModel(fit_model, numCoaddObj);
	status = OCIAttrGet(sthp, OCI_HTYPE_STMT, &numCoaddObj, (ub4 *) 0,
      				OCI_ATTR_ROW_COUNT,errhp);

	dbdisconnect();

	if((fileout = fopen(filename, "w")) == NULL) {
   	  fprintf(stderr, "Cannot open file.\n");
  	  exit(2);
  	}

	fprintf(fileout, "#COADD_OBJECTS_ID zeropoint_g zeropoint_r zeropoint_i zeropoint_z zeropoint_y ALPHAMODEL_J2000_G DELTAMODEL_J2000_G spheroid_reff_world_g disk_scale_world_g spheroid_aspect_world_g disk_aspect_world_g spheroid_theta_j2000_g disk_theta_j2000_g flux_spheroid_g flux_model_g ALPHAMODEL_J2000_R DELTAMODEL_J2000_R spheroid_reff_world_r disk_scale_world_r spheroid_aspect_world_r disk_aspect_world_r spheroid_theta_j2000_r disk_theta_j2000_r flux_spheroid_r flux_model_r ALPHAMODEL_J2000_I DELTAMODEL_J2000_I spheroid_reff_world_i disk_scale_world_i spheroid_aspect_world_i disk_aspect_world_i spheroid_theta_j2000_i disk_theta_j2000_i flux_spheroid_i flux_model_i ALPHAMODEL_J2000_Z DELTAMODEL_J2000_Z spheroid_reff_world_z disk_scale_world_z spheroid_aspect_world_z disk_aspect_world_z spheroid_theta_j2000_z disk_theta_j2000_z flux_spheroid_z flux_model_z ALPHAMODEL_J2000_Y DELTAMODEL_J2000_Y spheroid_reff_world_y disk_scale_world_y spheroid_aspect_world_y disk_aspect_world_y spheroid_theta_j2000_y disk_theta_j2000_y flux_spheroid_y flux_model_y\n");
	for(i = 0; i < numCoaddObj; i++){
		fprintf(fileout, "%d %22.16f %22.16f %22.16f %22.16f %22.16f %22.16lf %22.16lf %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16lf %22.16lf %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16lf %22.16lf %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16lf %22.16lf %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16lf %22.16lf %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f %22.16f\n",
fit_model[i].objId,
fit_model[i].zeropoint_g,
fit_model[i].zeropoint_r,
fit_model[i].zeropoint_i,
fit_model[i].zeropoint_z,
fit_model[i].zeropoint_y,
fit_model[i].ra_g,
fit_model[i].dec_g,
fit_model[i].spheroid_reff_world_g,
fit_model[i].disk_scale_world_g,
fit_model[i].spheroid_aspect_world_g,
fit_model[i].disk_aspect_world_g,
fit_model[i].spheroid_theta_j2000_g,
fit_model[i].disk_theta_j2000_g,
fit_model[i].flux_spheroid_g,
fit_model[i].flux_model_g,
fit_model[i].ra_r,
fit_model[i].dec_r,
fit_model[i].spheroid_reff_world_r,
fit_model[i].disk_scale_world_r,
fit_model[i].spheroid_aspect_world_r,
fit_model[i].disk_aspect_world_r,
fit_model[i].spheroid_theta_j2000_r,
fit_model[i].disk_theta_j2000_r,
fit_model[i].flux_spheroid_r,
fit_model[i].flux_model_r,
fit_model[i].ra_i,
fit_model[i].dec_i,
fit_model[i].spheroid_reff_world_i,
fit_model[i].disk_scale_world_i,
fit_model[i].spheroid_aspect_world_i,
fit_model[i].disk_aspect_world_i,
fit_model[i].spheroid_theta_j2000_i,
fit_model[i].disk_theta_j2000_i,
fit_model[i].flux_spheroid_i,
fit_model[i].flux_model_i,
fit_model[i].ra_z,
fit_model[i].dec_z,
fit_model[i].spheroid_reff_world_z,
fit_model[i].disk_scale_world_z,
fit_model[i].spheroid_aspect_world_z,
fit_model[i].disk_aspect_world_z,
fit_model[i].spheroid_theta_j2000_z,
fit_model[i].disk_theta_j2000_z,
fit_model[i].flux_spheroid_z,
fit_model[i].flux_model_z,
fit_model[i].ra_y,
fit_model[i].dec_y,
fit_model[i].spheroid_reff_world_y,
fit_model[i].disk_scale_world_y,
fit_model[i].spheroid_aspect_world_y,
fit_model[i].disk_aspect_world_y,
fit_model[i].spheroid_theta_j2000_y,
fit_model[i].disk_theta_j2000_y,
fit_model[i].flux_spheroid_y,
fit_model[i].flux_model_y
);
	}

	if( fclose( fileout )) fprintf(stderr, "File close error.\n");

	//
	// Free DB Handles
	//
	status = OCIHandleFree((dvoid *) sthp, OCI_HTYPE_STMT);
	status = OCIHandleFree((dvoid *) svchp, OCI_HTYPE_SVCCTX);
	status = OCIHandleFree((dvoid *) errhp, OCI_HTYPE_ERROR);

	//
	// Free memory
	//
	free(fit_model);

	return 0;	

}

