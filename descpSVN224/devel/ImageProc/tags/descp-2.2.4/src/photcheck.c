/*$Id: photcheck.c 5080 2010-02-13 01:00:17Z desai $
**
** photcheck.c
**
** DESCRIPTION:
**     
**
** NOTE:
**   1. need to add truth table selection?
**   2. need to add the option to work for BCS
**   3. add PLPLOT    
**   4. make all bands and all magtype?
**   5. make input ID: imageid or coadd_imageid
**  
** Last commit:
**     $Rev: 5080 $
**     $LastChangedBy: desai $
**     $LastChangedDate: 2010-02-12 18:00:17 -0700 (Fri, 12 Feb 2010) $
**
*/

#include "imageproc.h"

#define TOLERANCE_DEFAULT (1.0/3600.0) /* arcsec */
#define MAGERRCUT_DEFAULT 0.1
#define MAGSTART_DEFAULT 16
#define MAGSTOP_DEFAULT 19
#define FLAGS_DEFAULT 10
#define ACCURACY 1e-6 /* accuracy in the sigma clipping algorithm */

double calcdistance(double ra1,double dec1,double ra2,double dec2);
float getmedian(float data[],int N);
double getmean(float data[],float err[],int N,int flag);
double getrms(float data[],float err[],double mean,int N,int flag);
void iter_mean(float data[], float err[], int N, double *mean, double *rms, int *N_good, int *N_bad, int *N_iter, int flag, int flag_iter, int flag_Niterate, int Nmax_iterate, float THRESHOLD);

main(argc,argv)
     int argc;
     char *argv[];
{
  char command[1000],event[1000],line[5000],name[100],dblogin[1000],
	sqlcall[1000],sqlHEAD[1000],rootname[1000],nite[20],band[5],run[300],
	expname[200],project[10],tilename[100],SG[5],outfile[1000]="",
    filename[1000],queryfile[1000],magin[20],magerrin[20],magtrue[10],
    newexpname[200],redtile[200];
  char *fitstr=".fits",*position ;
  int i,j,N_obj,N_truth,Nrefcat,ccd,obj_flag,imageid,coaddid,itemp,nomatches,
        matches,locmatch,loclow,lochi,count,flag_verbose=2,
        flag_magtype=0,flag_ccd=0,flag_coadd=0,flag_SG=0,
    flag_imageid=0,flag_coaddid=0,flag_magrange=0,flag_magtruerange=0,flag_flags=0,flag_redtile=0,
	flag_Niterate=0,FLAGS=FLAGS_DEFAULT,N_good,N_bad,N_iter,
    Nmax_iterate=10,flag_cleanup=1,indexval; 
  float temp,temp_mag,mag_compare,true_mag,obj_mag,obj_magerr,obj_class,
	*sra,*tmag,*dmag,*dmagerr,magstart=MAGSTART_DEFAULT,
	magstop=MAGSTOP_DEFAULT,magerrcut=MAGERRCUT_DEFAULT,sigma=2.5;
  double ramin,ramax,decmin,decmax,ra,dec,tra,tdec,scale,mindist,distance,
	tolerance=TOLERANCE_DEFAULT,temp_ra,temp_dec,*ra_ref,*dec_ref,
	mean,mean_rms;
  unsigned long *index_ref;
  FILE *pip,*out,*inp;
  void select_dblogin(),reportevt(),indexx();
    
  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <Optional Inputs>\n",argv[0]);
    printf("       -band <band> (for truth table)\n");
    printf("       -project <project> \n");
    printf("       -run <run> \n");

    printf("     For SINGLE Exposure Images:\n");
    printf("       -nite <nite> \n");
    printf("       -redtile <tilename> \n"); 
    printf("       -expname <exposurename> \n");
    printf("          OR\n");
    printf("       -imageid <#> \n");

    printf("     For COADD Images:\n");
    printf("       -tilename <tilename> \n");
    printf("          OR\n");
    printf("       -coaddid <#> \n");

    printf("    Optional Inputs:\n");
    printf("       -ccd <#> \n");
    printf("       -magtype <0-6> (magauto=0)\n");
    printf("       -class_truth <class> (either S or G)\n");
    printf("       -matchtolerance <#> (%.1f arcsec)  \n",tolerance*3600.0);
    printf("       -magerrcut <#> (default: %.2f)  \n",magerrcut);
    printf("       -magrange <lower> <upper> (default: %.0f - %.0f)\n",
    	magstart,magstop);
    printf("       -sigmaclip <sigma> (default: 2.5)\n");
    printf("       -Nitermax <Niter_max> (default: 10)\n");	
    printf("       -output <filename> \n");
    printf("       -nocleanup\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }
  
  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {

    /* required inputs */
    if (!strcmp(argv[i],"-run")) sprintf(run,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-nite")) sprintf(nite,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-project")) sprintf(project,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-expname")) sprintf(expname,"%s",argv[i+1]);
    /*check of input name matches ".fits" and remove it if so */
    position = strstr(expname,fitstr);
    if(position!=NULL) {
      indexval = position - expname;
      strncpy(newexpname,expname,indexval);
      newexpname[indexval+1] ='\0' ;
      strcpy(expname,newexpname);
    }

    if (!strcmp(argv[i],"-output")) sprintf(outfile,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-tilename")) {
      sprintf(tilename,"%s",argv[i+1]);
      flag_coadd=1;
    }
    if (!strcmp(argv[i],"-imageid")) {
      imageid=atoi(argv[i+1]);
      flag_imageid=1;
    }
    if (!strcmp(argv[i],"-coaddid")) {
      imageid=atoi(argv[i+1]);
      flag_coaddid=1;
    }

    /* optional inputs */
    if (!strcmp(argv[i],"-magtype")) flag_magtype=atoi(argv[i+1]);
    if (!strcmp(argv[i],"-ccd")) {flag_ccd=1; ccd=atoi(argv[i+1]);}
    if (!strcmp(argv[i],"-band")) sprintf(band,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-class_truth")) {flag_SG=1; sprintf(SG,"%s",argv[i+1]);}
    if (!strcmp(argv[i],"-nocleanup")) flag_cleanup=0;
    if (!strcmp(argv[i],"-redtile")) {flag_redtile=1;sprintf(redtile,"%s",argv[i+1]);}
    if (!strcmp(argv[i],"-matchtolerance")) {
	sscanf(argv[i+1],"%lg",&tolerance); 
	tolerance/=3600.0;
    }
    if (!strcmp(argv[i],"-magerrcut")) {sscanf(argv[i+1],"%f",&magerrcut);}
    if (!strcmp(argv[i],"-flag")) {flag_flags=1; sscanf(argv[i+1],"%d",&FLAGS);}
    if (!strcmp(argv[i],"-magrange")) {
      flag_magrange=1;
      sscanf(argv[i+1],"%f",&magstart);
      sscanf(argv[i+2],"%f",&magstop);
    }
    if (!strcmp(argv[i],"-magtruerange")) {
      flag_magtruerange=1;
      sscanf(argv[i+1],"%f",&magstart);
      sscanf(argv[i+2],"%f",&magstop);
    }
    if (!strcmp(argv[i],"-sigmaclip"))  sscanf(argv[i+1],"%f",&sigma);  
    if (!strcmp(argv[i],"-Nitermax")) {
      flag_Niterate=1;
    Nmax_iterate=atoi(argv[i+1]);
  }
}
  if (flag_coadd) sprintf(rootname,"%s",tilename);
  else sprintf(rootname,"%s",nite);
  if (strlen(outfile)<1) sprintf(outfile,"%s.out",rootname);
  /* set input magtype */
  switch(flag_magtype) {
  case -1:  sprintf(magin,"mag_model"); sprintf(magerrin,"%s","magerr_model");
    break;
  case 0: sprintf(magin,"mag_auto"); sprintf(magerrin,"%s","magerr_auto"); 
    break; 
  case 1: sprintf(magin,"mag_aper_1"); sprintf(magerrin,"%s","magerr_aper_1"); 
    break; 
  case 2: sprintf(magin,"mag_aper_2"); sprintf(magerrin,"%s","magerr_aper_2"); 
    break; 
  case 3: sprintf(magin,"mag_aper_3"); sprintf(magerrin,"%s","magerr_aper_3"); 
    break; 
  case 4: sprintf(magin,"mag_aper_4"); sprintf(magerrin,"%s","magerr_aper_4"); 
    break; 
  case 5: sprintf(magin,"mag_aper_5"); sprintf(magerrin,"%s","magerr_aper_5"); 
    break; 
  case 6: sprintf(magin,"mag_aper_6"); sprintf(magerrin,"%s","magerr_aper_6"); 
    break; 
  default: printf(" ** %s error: -magtype out of range\n", argv[0]); exit(0);
  }

  if (flag_coadd) { /* for the coadd case */
	if (!strncmp("_",magin+strlen(magin)-2,1)) {
	  /* remove the underscore */
	  magin[strlen(magin)-2]=magin[strlen(magin)-1];
	  magin[strlen(magin)-1]=0;
	  magerrin[strlen(magerrin)-2]=magerrin[strlen(magerrin)-1];
	  magerrin[strlen(magerrin)-1]=0;
	}
	sprintf(magin,"%s_%s",magin,band);
	sprintf(magerrin,"%s_%s",magerrin,band);
  }


  /* for the true mag */
  if(!strcmp(band,"g")) sprintf(magtrue,"g_mag");
  if(!strcmp(band,"r")) sprintf(magtrue,"r_mag");
  if(!strcmp(band,"i")) sprintf(magtrue,"i_mag");
  if(!strcmp(band,"z")) sprintf(magtrue,"z_mag");
  if(!strcmp(band,"Y")) sprintf(magtrue,"Y_mag");

  /* *** Setup DB login and make generic sqlcall *** */
  select_dblogin(dblogin,DB_READONLY);
  sprintf(sqlHEAD,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;");


  /* *************************** */
  /* *** Query for data here *** */
  /* *************************** */
  
  /* *** Query for the objects/coadd_objects table here *** */
  sprintf(queryfile,"%s_objects.sql",rootname);
  out=fopen(queryfile, "w");
  fprintf(out,"%s\n",sqlHEAD);
  fprintf(out,"SPOOL %s_obj.dat;\n",rootname);

  if(!flag_coadd) {
    if (flag_redtile)
      {fprintf(out,"SELECT a.RA,a.DEC,to_char(a.%s,'S9.9999EEEE'),to_char(a.%s,'S9.9999EEEE'),a.spread_model,a.flags,a.imageid FROM objects_current a,image b \n",magin,magerrin);
	fprintf(out,"WHERE a.imageid=b.parentid and b.nite='%s' and b.project='%s' and b.tilename='%s' and  b.imagename like '%s%%' and b.run like '%s%%' ",nite,project,redtile,expname,run);
      }
    
    else if(!flag_imageid) {
      fprintf(out,"SELECT a.RA,a.DEC,to_char(a.%s,'S9.9999EEEE'),to_char(a.%s,'S9.9999EEEE'),a.spread_model,a.flags,a.imageid FROM objects_current a,image b \n",magin,magerrin);
      fprintf(out,"WHERE a.imageid=b.id and b.nite='%s' and b.project='%s' and b.imagetype='red' and b.band='%s' and b.imagename like '%s%%' and b.run like '%s%%' ",nite,project,band,expname,run);
      if(flag_ccd) 
	fprintf(out,"and b.ccd=%d ",ccd);  
    }
    else {
      fprintf(out,"SELECT a.RA,a.DEC,to_char(a.%s,'S9.9999EEEE'),to_char(a.%s,'S9.9999EEEE'),a.spread_model,a.flags,a.imageid FROM objects_current a\n",magin,magerrin);
      fprintf(out,"WHERE a.imageid=%d ",imageid);
    }
  }
  else { /* for the coadd case */

    if(!flag_coaddid) {
      fprintf(out,"SELECT a.RA,a.DEC,to_char(a.%s,'S9.9999EEEE'),to_char(a.%s,'S9.9999EEEE'),a.class_star_%s,a.flags_%s,a.imageid_%s from coadd_objects a, coadd b \n",
	      magin,magerrin,band,band,band);
      fprintf(out,"WHERE a.imageid_%s=b.id  and b.tilename='%s' and b.run like '%s%%' and b.project='%s' ",band,tilename,run,project);
    }
    else {
      fprintf(out,"SELECT a.RA,a.DEC,to_char(a.%s,'S9.9999EEEE'),to_char(a.%s,'S9.9999EEEE'),a.class_star_%s,a.flags_%s,a.imageid_%s from coadd_objects a\n",
	      magin,magerrin,band,band,band);
      fprintf(out,"WHERE a.imageid_%s=%d ",band,imageid);
    }
  }
  fprintf(out,"\nORDER BY a.RA;\n");
  fprintf(out,"SPOOL OFF;\n");
  fprintf(out,"exit;\n");
  fclose(out);
   
  /* run the query */
  sprintf(sqlcall, "sqlplus -S %s < %s > %s.out",dblogin,
	queryfile,queryfile);
  system(sqlcall);

  /* find out how many objects returned */  
  sprintf(command, " wc -l %s_obj.dat",rootname);
  pip=popen(command,"r");
  fscanf(pip,"%d",&N_obj);
  pclose(pip);
  if(!N_obj) {
    sprintf(event,"No object found from database");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }

  if(flag_coadd)
    sprintf(event,"Found %d objects from coadd_object Table",N_obj);
  else
    sprintf(event,"Found %d objects from objects Table",N_obj);
  reportevt(flag_verbose,STATUS,1,event);

  /* find out the RA/DEC range */
  sprintf(filename,"%s_obj.dat",rootname);
  inp=fopen(filename,"r");
  fgets(line,1000,inp);
  sscanf(line,"%lg %lg",&ra,&dec);
  ramax=ramin=ra;decmin=decmax=dec;
  while (fgets(line,1000,inp)!=NULL ) {
  sscanf(line,"%lg %lg",&ra,&dec);
    if(ra>ramax) ramax=ra; if(ra<ramin) ramin=ra;
    if(dec>decmax) decmax=dec; if(dec<decmin) decmin=dec;
  }
  fclose(inp);

  sprintf(event,"Querying DC5_Truth between %.5f:%.5f, %.5f:%.5f",
    ramin,ramax,decmin,decmax);
  reportevt(flag_verbose,STATUS,1,event);
  /* *** Query for DC4_Truth table *** */
  sprintf(queryfile,"%s_truth.sql",rootname);
  out=fopen(queryfile, "w");
  fprintf(out,"%s\n",sqlHEAD);
  fprintf(out,"SPOOL %s_truth.dat;\n",rootname);
  fprintf(out,"SELECT ra,dec,to_char(%s,'S9.9999EEEE') FROM DC5_Truth ",magtrue);
  fprintf(out," where (ra between %2.8f and %2.8f) ",ramin,ramax);
  fprintf(out," and (dec between %2.8f and %2.8f) ",decmin,decmax);
  if(flag_SG) 
    fprintf(out," and class='%s' ",SG);
  fprintf(out," order by ra");
  fprintf(out,";\nSPOOL OFF;\n");
  fprintf(out,"exit;\n");
  fclose(out);

  /* run the query */
  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s > %s.out",dblogin,
	queryfile,queryfile);
  system(sqlcall);

  /* find out how many objects returned */  
  sprintf(command, " wc -l %s_truth.dat",rootname);
  pip=popen(command,"r");
  fscanf(pip,"%d",&N_truth);
  pclose(pip);

  sprintf(event," Found %d objects from DC5_Truth Table ",N_truth);
  reportevt(flag_verbose,STATUS,1,event);

  if(!N_truth) {
    sprintf(event," No object found from DC5_Truth ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }


  /* *************************** */
  /* *** Begin Matching Here *** */
  /* *************************** */

  Nrefcat=N_truth;

  /* memory allocation */
  ra_ref=(double *)calloc(Nrefcat+1,sizeof(double ));
  dec_ref=(double *)calloc(Nrefcat+1,sizeof(double ));
  sra=(float *)calloc(Nrefcat+1,sizeof(float ));
  tmag=(float *)calloc(Nrefcat+1,sizeof(float ));
  index_ref=(unsigned long *)calloc(Nrefcat+1,sizeof(unsigned long));
  
  /* read in truth data */
  sprintf(filename,"%s_truth.dat",rootname);
  inp=fopen(filename,"r");
  
  for(i=1;i<=Nrefcat;i++) {

    fgets(line,5000,inp);
    sscanf(line,"%lg %lg %f",&temp_ra,&temp_dec,&temp_mag);
    ra_ref[i]=temp_ra;
    dec_ref[i]=temp_dec;
    sra[i]=(float)temp_ra;
    index_ref[i]=i;
    tmag[i]=temp_mag;
  }
  fclose(inp);
 
  /* indexing the RA array */
  indexx((unsigned long)Nrefcat,sra,index_ref);


  /* matching here */

  matches=nomatches=0;
  loclow=1;

  sprintf(filename,"%s_obj.dat",rootname);
  inp=fopen(filename,"r");
  out=fopen(outfile,"w");
  
  for(j=1;j<=N_obj;j++) {

    fgets(line,5000,inp);
    sscanf(line,"%lg %lg %f %f %f %d %d",&ra,&dec,&obj_mag,&obj_magerr,&obj_class,&obj_flag,&imageid);

    mindist=tolerance;
    scale=cos(dec*M_PI/180.0);

    ramin=ra-tolerance/scale;
    i=loclow;lochi=-1;
    
    if (loclow==0) i=1;
    while (lochi==-1) {
      if (ra_ref[index_ref[i]]>ramin && ra_ref[index_ref[i-1]]<=ramin) {
	loclow=lochi=i-1;
	if (loclow<0) loclow=lochi=0;
      }
      else { /* adjust the location */
	if (ra_ref[index_ref[i]]<=ramin) i++;
	else i--;
      }
      if (i==0) loclow=lochi=0;
      if (i>=Nrefcat-1) loclow=lochi=Nrefcat-1;
    }
    
    ramax=ra+tolerance/scale;
    for (i=loclow;i<=Nrefcat;i++) if (ra_ref[index_ref[i]]>ramax) break;
    lochi=i; if (lochi>=Nrefcat) lochi=Nrefcat-1;
    
    locmatch=-1;
    for (i=loclow;i<=lochi;i++) {
      /* if dec offset is acceptable then calculate distance */
      if (fabs(dec_ref[index_ref[i]]-dec)<tolerance) {
	distance=calcdistance(ra_ref[index_ref[i]],dec_ref[index_ref[i]],ra,dec);
	if (distance<mindist) {
	  mindist=distance;
	  locmatch=i;
	}
      }
    }
    
    if (locmatch==-1) {
      nomatches++;
    }
    else {
      matches++;
      
      fprintf(out,"%2.8f\t%2.8f\t%2.8f\t%2.8f %2.8f %2.4f\t%2.8f %2.8f %2.4f %2.4f %2.3f %d   %d\n",
	      mindist*3600.0,scale*(ra-ra_ref[index_ref[locmatch]])*3600.0,(dec-dec_ref[index_ref[locmatch]])*3600.0,
	      ra_ref[index_ref[locmatch]],dec_ref[index_ref[locmatch]],tmag[index_ref[locmatch]],
	      ra,dec,obj_mag,obj_magerr,obj_class,obj_flag,imageid);
    }
  }
  fclose(inp); fclose(out);

  sprintf(event," Search radius = %2.1f (arcsec)\tMatch = %d\tNoMatch = %d ",tolerance*3600.0,matches,nomatches);
  reportevt(flag_verbose,STATUS,1,event);


  /* ************************************** */
  /* *** Calculate the mean offset here *** */
  /* ************************************** */

  /* memory allocation and initialize */
  dmag=(float *)calloc(matches,sizeof(float));
  dmagerr=(float *)calloc(matches,sizeof(float));

  for(i=0;i<matches;i++) 
    dmag[i]=dmagerr[i]=0.0;

  /* input and screeing data */
  inp=fopen(outfile,"r");
  count=0;
  for(i=0;i<matches;i++) {
    fscanf(inp,"%f %f %f %f %f %f %f %f %f %f %f %d %d",
	   &temp,&temp,&temp,&temp,&temp,&true_mag,
	   &temp,&temp,&obj_mag,&obj_magerr,&obj_class,&obj_flag,&imageid);

   
    if(obj_magerr < magerrcut) { /* magerr cut */
  
      if(flag_magrange || flag_magtruerange) { /* mag range cut */

	if(flag_magrange) mag_compare=obj_mag;
	else mag_compare=true_mag;

	if(mag_compare>magstart && mag_compare<magstop) {

	  dmag[count]=true_mag-obj_mag;
	  if(obj_magerr<0.005) /* temporary solution */
	    obj_magerr=0.005;
	  dmagerr[count]=obj_magerr;
	  count++;
	}
      }
      else { /* no mag range cut */
	dmag[count]=true_mag-obj_mag;
	if(obj_magerr<0.005) /* temporary solution */
	  obj_magerr=0.005;
	dmagerr[count]=obj_magerr;
	count++;
      }
    }
    
  }
  fclose(inp);

  sprintf(event," %d data points after filtering (%2.1f < mag < %2.1f; magerr < %2.2f) ",count,magstart,magstop,magerrcut);
  reportevt(flag_verbose,STATUS,1,event);

  /* get the iterative mean */
  iter_mean(dmag,dmagerr,count,&mean,&mean_rms,&N_good,&N_bad,&N_iter,1,1,flag_Niterate,Nmax_iterate,sigma);

  /* output results */
  if(!flag_coadd) sprintf(name,"%s_%s_%s_%d_%d",nite,expname,band,ccd,imageid);
  else sprintf(name,"%s_%s_%d",tilename,band,imageid);
  sprintf(event,"Name = %s & Median_Offset = %2.4f & Mean_Offset = %2.4f & RMS = %2.4f & N_good=%d & N_bad=%d & N_iter=%d & magtype %d ",name,getmedian(dmag,count),mean,mean_rms,N_good,N_bad,N_iter,flag_magtype);
  if(flag_SG)
    sprintf(event,"%s & Class_truth %s ",event,SG);
  else
    sprintf(event,"%s & Class_truth S+G ",event);
  reportevt(flag_verbose,QA,1,event);

  /* clean up */
  if (flag_cleanup) {
    sprintf(command,"rm %s*dat %s*sql %s*sql.out",rootname,rootname,rootname);
    system (command);
  }


  /* free memory */
  free(ra_ref); free(dec_ref); free(sra); free(index_ref); free(tmag);
  free(dmag); free(dmagerr);
}


double calcdistance(double ra1,double dec1,double ra2, double dec2)
{
  double  distance,scale;
  //float	outdist;

  scale=cos(0.5*(dec1+dec2)*M_PI/180.0);
  distance=Squ(scale*(ra1-ra2))+Squ((dec1-dec2));
  //if (distance>1.0e-20) outdist=sqrt(distance);
  //else outdist=distance;
  return(sqrt(distance));

}

float getmedian(float data[],int N)
{
  float *vecsort,median;
  int i;
  unsigned long n;
  void shell();

  n=(unsigned long)N;
  vecsort=(float *)calloc(N+1,sizeof(float));
  
  for(i=1;i<=N;i++)
    vecsort[i]=data[i-1];
  
  /* sort with N.R. subroutine */
  shell(n,vecsort);

  /* get median value */
  if(n%2)  median = vecsort[(n+1)/2];
  else median = 0.5 * (vecsort[n/2] + vecsort[(n/2)+1]);

  free(vecsort);
  return (median);
}

double getmean(float data[],float err[],int N,int flag)
{
  int i;
  double sum,mean,sigsq;


  mean=0.0;
  sum=0.0;
  sigsq=0.0;
  for(i=0;i<N;i++) {
    sum+=data[i];
    sigsq+=1.0/(err[i]*err[i]);
  }

  if(flag==0)  /* unweighted average */  
    mean=sum/N;
  else {  /* weighted average */  
    sum = 0.0;
    for(i=0;i<N;i++)
      sum += data[i]/(err[i]*err[i]);
    mean = sum/sigsq;
  }

  return (mean);
}

double getrms(float data[],float err[],double mean,int N,int flag)
{
  int i;
  double var,s,ep,rms;

  //mean=getmean(data,err,N,flag);

  /* using two-pass formula as given in Numerical Recipes 14.1.8 */
  if(N>1) {
    var=ep=0.0;
    for(i=0;i<N;i++) { 
      s=data[i]-mean;
      ep+=s;
      var+=s*s;
    }
    rms=sqrt((var-ep*ep/N)/(float)(N-1));
  }
  else
    rms=0.0;

  return (rms);
}

void iter_mean(float data[], float err[], int N, double *mean, double *rms, int *N_good, int *N_bad, int *N_iter, int flag, int flag_iter, int flag_Niterate, int Nmax_iterate, float THRESHOLD)
{
  int ii,Nt,Noutlier=0;
  double t_mean,t_rms,new_mean,old_mean;
  float *olddata, *olderr;
  float *newdata, *newerr;
  
  *N_good=N;
  *N_bad=0;
  *N_iter=0;
  
  /* first find the sample average and  rms */
  t_mean=getmean(data,err,N,flag);
  t_rms=getrms(data,err,t_mean,N,flag);

  /* memory allocation for the newdata and newerr */
  olddata=(float *)calloc(N+1,sizeof(float));
  olderr =(float *)calloc(N+1,sizeof(float));
  newdata=(float *)calloc(N+1,sizeof(float));
  newerr =(float *)calloc(N+1,sizeof(float));

  /* first find if any outliers when doing sigma-clipping */
  for(ii=0;ii<N;ii++) {
    if((fabs(data[ii]-t_mean) > THRESHOLD*t_rms)) 
      Noutlier++;
  }

  if(Noutlier==0 || flag_iter==0) { /* no outliers or not using sigma-clipping, simply return the results */
    *mean=t_mean;
    *rms=t_rms;
  }
  else { /* remove outliers and begin iterative process */

    /* initiallize the olddata array and parameters */
    for(ii=0;ii<N;ii++) {
      olddata[ii]=data[ii];
      olderr[ii]=err[ii];
    }
    old_mean=t_mean;
    new_mean=0.0;
    Nt = N;
  
    /* iterative procedure until the mean converge */
    while(fabs(old_mean-new_mean) >= ACCURACY) {

      /* get the mean and rms for the old data */
      t_mean=getmean(olddata,olderr,Nt,flag);
      t_rms =getrms(olddata,olderr,t_mean,Nt,flag);
      old_mean=t_mean;

      /* find the number of outliers and put data in new array without the outlier */
      *N_good=0;
      for(ii=0;ii<Nt;ii++) {
	if((fabs(olddata[ii]-t_mean) > THRESHOLD*t_rms)) 
	  *N_bad+=1;
	else {
	  newdata[*N_good]=olddata[ii];
	  newerr[*N_good]=olderr[ii];
	  *N_good+=1;
	}
      }

      /* update information */
      if(Nt != (*N_good))
	*N_iter+=1;

      Nt = (*N_good);

      t_mean=getmean(newdata,newerr,Nt,flag);
      t_rms =getrms(newdata,newerr,t_mean,Nt,flag);      
      new_mean = t_mean;

      /* put the newdata array back to olddata array */
      for(ii=0;ii<Nt;ii++) {
	olddata[ii]=newdata[ii];
	olderr[ii]=newerr[ii];
      }
     
      /* quick the loop if set the Nmax_iterate */
      if(flag_Niterate && *N_iter==Nmax_iterate) break;
    }

    if((*N_good)==0 || (*N_bad)==N) {
      t_mean=0.0;
      t_rms=0.0;
    }

    *mean=t_mean;
    *rms=t_rms;
  }

  /* free memory */
  free(olddata);free(olderr);
  free(newdata);free(newerr);
}


#undef TOLERANCE_DEFAULT
#undef MAGERRCUT_DEFAULT
#undef MAGSTART_DEFAULT 
#undef MAGSTOP_DEFAULT
#undef FLAGS_DEFAULT
#undef ACCURACY 
