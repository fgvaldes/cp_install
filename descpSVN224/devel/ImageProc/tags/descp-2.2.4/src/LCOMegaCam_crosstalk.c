/* LCOMegaCam_crosstalk <> <> ....
/*
/* This program will transform MegaCam images from the Magellan 6.5m into 36 FITS 
/* files, each containing the imaging data from a single CCD. 
/*
/* The program assumes there is no crosstalk correction, alters the OBSTYPE header
/* keyword to conform to DESDM standards, and leaves a history in the header.
*/
#include "imageproc.h"

#define DOUBLE 1
#define STRING 2
#define FLOAT  3
#define INTEG  4

#define INCLUDECOMMENTS 0
#define DONTINCLUDECOMMENTS 1

#define NUM_KEYS 200

main(argc,argv)
	int argc;
	char *argv[];
{
	/*  ********************************************************  */
	/*  ************  List the Headers Keywords NOT to be ******  */
	/*  ************      copied from 0th Header          ******  */
	/*  ********************************************************  */
		
	char	*zeroheader,**exclkey,
		exclist[40][10]={
		"SIMPLE","BITPIX","NAXIS","NAXIS1",
		"NAXIS2","EXTEND","NEXTEND","CHECKSUM",
		"DATASUM","CHECKVER","GCOUNT","PCOUNT",
		"BZERO","BSCALE","INHERIT","XTENSION",
		"TFIELDS","TFORM1","ZIMAGE","ZTILE1",
		"ZTILE2","ZCMPTYPE","ZNAME1","ZVAL1",
		"ZNAME2","ZVAL2","EXTNAME","ZTENSION",
		"ZBITPIX","ZNAXIS","ZNAXIS1","ZNAXIS2",
		"ZPCOUNT","ZGCOUNT","DCREATED","TTYPE1",
		"ZHECKSUM"},
		*nthheader;
	int	numzerokeys,nexc=37,numnthkeys,numwrittenkeys=0,
		keyflag,keynum,totkeynum;
	
	/*  ******************************************************** */
	/*  ******** array for storing crosstalk coefficients ****** */
	/*  ******************************************************** */
	float	xtalk[72][72];
	
	/*  ******************************************************** */
	/*  ******* other variables needed for the processing ****** */
	/*  ******************************************************** */
	char	filename[500],trash[200],nfilename[500],tag1[100],tag2[100],
		newname[500],filetype[50],obstype[80],comment[100],
		longcomment[10000],event[10000],newimagename[500],
		filter[1000];
	int	anynull,nfound,i,hdunum,hdutype,j,k,x,y,chdu, ccdnum,locout,
		flag_crosstalk=0,flag_verbose=1,flag_phot=0,bitpix,naxis,
		ext1,ext2,ampextension,ampA,ampB,locA,locB,ampoffset1,
		ampoffset2,
		mkpath();
	static int status=0;
	float	value,uncertainty,significance,nullval,*outdata,**indata;
	long	axes[2],naxes[2],taxes[2],pixels,npixels,fpixel,oldpixel;
	double	xtalkcorrection=0.0;
	void	printerror(),reportevt(), fixheader_flt(), fixheader_int(), fixheader_str();
	fitsfile *fptr,*nfptr;
	FILE	*inp;
	time_t	tm;
	long long lccdnum;

	if (argc<3) {
	  printf("%s <infile.fits> <root/outfile> <options>\n",argv[0]);
          printf("  -crosstalk <crosstalk matrix- file>\n");
	  printf("  -photflag <0 or 1>\n");
          printf("  -verbose <0-3>\n");
          exit(0);
	}
	sprintf(filename,"%s",argv[1]);
	sprintf(nfilename,"%s",argv[2]);


	/* ****************************************************** */
	/* ****************  Process Command Line *************** */
	/* ****************************************************** */

        for (i=3;i<argc;i++) 
          if (!strcmp(argv[i],"-verbose")) {
	    sscanf(argv[++i],"%d",&flag_verbose);
	    if (flag_verbose<0 || flag_verbose>3) {
	      sprintf(event,"Verbose level out of range %d. Reset to 2",
		flag_verbose);
	      flag_verbose=2;
	      reportevt(flag_verbose,STATUS,3,event);
	    }
	  }
        for (i=3;i<argc;i++) {
          if (!strcmp(argv[i],"-photflag"))
	    sscanf(argv[++i],"%d",&flag_phot);
	  if (!strcmp(argv[i],"-crosstalk")) {
	    flag_crosstalk=1;
	    for (j=0;j<124;j++) for (k=0;k<124;k++) xtalk[j][k]=0.0;
	    i++;
	    /* read through crosstalk matrix file */
	    if (flag_verbose==3) {
	      sprintf(event,"Reading file %s\n",argv[i]);
	      reportevt(flag_verbose,STATUS,1,event);
	    }
	    inp=fopen(argv[i],"r");
	    if (inp==NULL) {
	      sprintf(event,"Crosstalk file %s not found",argv[i]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    while (fgets(trash,200,inp)!=NULL) {
	      if (!strncmp(trash,"ccd",3) || !strncmp(trash,"CCD",3)) {
	        /* first replace parentheses and commas with spaces */
		for (j=0;j<strlen(trash);j++) 
		  if (!strncmp(&(trash[j]),"(",1) || 
		  !strncmp(&(trash[j]),")",1)) trash[j]=32; 
	        sscanf(trash,"%s %s %f %f %f",tag1,tag2,
		  &value,&uncertainty,&significance);
		if (!strncmp(tag1+strlen(tag1)-1,"A",1)) ampoffset1=0;
		else if (!strncmp(tag1+strlen(tag1)-1,"B",1)) ampoffset1=1;
		else {
	          sprintf(event,"Amp not properly noted as A/B in %s",tag1);
		  reportevt(flag_verbose,STATUS,5,event);
		  exit(0);
		}
		if (!strncmp(tag2+strlen(tag2)-1,"A",1)) ampoffset2=0;
		else if (!strncmp(tag2+strlen(tag2)-1,"B",1)) ampoffset2=1;
		else {
	          sprintf(event,"Amp not properly noted as A/B in %s",tag2);
		  reportevt(flag_verbose,STATUS,5,event);
		  exit(0);
		}
		/* strip off the A/B suffix */	
		tag1[strlen(tag1)-1]=0;tag2[strlen(tag2)-1]=0; 
		/* read in the ccd number */       
		sscanf(&(tag1[3]),"%d",&ext1);
	        sscanf(&(tag2[3]),"%d",&ext2);
		ext1=(ext1-1)*2+ampoffset1;
		if (ext1<0 || ext1>=36) {
	          sprintf(event,"CCD number out of range: %s %d",tag1,ext1);
		  reportevt(flag_verbose,STATUS,5,event);
		  exit(0);
		}
		ext2=(ext2-1)*2+ampoffset2;
		if (ext2<0 || ext2>=36) {
	          sprintf(event,"CCD number out of range: %s %d",tag2,ext2);
		  reportevt(flag_verbose,STATUS,5,event);
		  exit(0);
		}
	        xtalk[ext1][ext2]=value;
	      }
	    }
	    if (fclose(inp)) {
	      sprintf(event,"File close failed: %s",argv[i]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    if (flag_verbose==3) /* print the crosstalk coefficients */
	      for (j=0;j<124;j++) {
	        if (j%2==0) printf("  ccd%02dA",j/2+1);
		else printf("  ccd%02dB",j/2+1);
	        for (k=0;k<124;k++) {
		  printf("  %7.5f",xtalk[j][k]);
		}
		printf("\n");
	      }
	  }
	}



	/**********************************************************/
	/***********************  Open Image **********************/
	/**********************************************************/
	
        /* open the file, checked for all supported flavors */
	/* Flavor=Y (uncompressed) */
        if (fits_open_file(&fptr,filename,READONLY,&status)) { 
	  status=0;
	  sprintf(newname,"%s.fz",filename); 
	  sprintf(event,"File open failed: %s",filename);
	  reportevt(flag_verbose,STATUS,3,event);
	  /* Flavor=F (fpacked) */
          if (fits_open_file(&fptr,newname,READONLY,&status)) {
	    sprintf(event,"File open failed: %s",newname);
	    reportevt(flag_verbose,STATUS,3,event);
	    status=0;
	    sprintf(newname,"%s.gz",filename); 
	    /* Flavor=G (gzipped) */
            if (fits_open_file(&fptr,newname,READONLY,&status)) {
	      sprintf(event,"File open failed: %s",newname);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  }
	}
	if (flag_verbose) {
	  sprintf(event,"Opened exposure: %s",filename);	
	  reportevt(flag_verbose,STATUS,1,event);
	}
		
	/* get the number of HDUs in the image */
	if (fits_get_num_hdus(fptr,&hdunum,&status)) {
	  sprintf(event,"Reading HDUNUM failed: %s",filename);
	  reportevt(flag_verbose,STATUS,5,event);
	  printerror(status);
	}
	if (flag_verbose) {
	  sprintf(event,"%s has %d HDUs",filename,hdunum);
	  reportevt(flag_verbose,STATUS,1,event);
	}
	if (hdunum!=37) {
	  sprintf(event,"%d HDUs not standard MegaCam format",hdunum);
	  reportevt(flag_verbose,STATUS,5,event);
	  exit(0);
	}

	indata=(float **)calloc(hdunum,sizeof(float *));
    	if (!indata) {
	  sprintf(event,"Could not calloc indata");
	  reportevt(flag_verbose,STATUS,5,event);
          exit(0);
    	}


	/**********************************************************/
	/*************  Read Header from 0th Extension ************/
	/**********************************************************/
	
        exclkey=(char **)calloc(nexc,sizeof(char *));
        if (!exclkey) {
	    sprintf(event,"Could not calloc exclkey");
	    reportevt(flag_verbose,STATUS,5,event);
            exit(0);
        }
        for (j=0;j<nexc;j++) {
          exclkey[j]=(char *)calloc(10,sizeof(char));
          if (!exclkey[j]) {
	    sprintf(event,"Could not calloc exclkey[%d]",j);
	    reportevt(flag_verbose,STATUS,5,event);
            exit(0);
          }
          sprintf(exclkey[j],"%s",exclist[j]);
        }
        if (fits_hdr2str(fptr,INCLUDECOMMENTS,exclkey,nexc,
	  &zeroheader,&numzerokeys,
	  &status)) {
	  sprintf(event,"Could not read kewords");
	  reportevt(flag_verbose,STATUS,5,event);
          printerror(status);
	}
	/* cut off last bogus "END" line */
	zeroheader[strlen(zeroheader)-80]=0;
        if (flag_verbose) {
	  printf("  Read %d header keywords from 0th header\n",numzerokeys);
	  if (flag_verbose==3) {
	    printf("  *************************************************\n");
	    printf("  %s",zeroheader);
	    printf("*************************************************\n");
	  }
	}

	/* read the OBSTYPE and use it to determine <filetype> */
        if (fits_read_key_str(fptr,"OBSTYPE",obstype,comment,&status)) {
	  sprintf(event,"Header Keyword OBSTYPE not found");
	  reportevt(flag_verbose,STATUS,5,event);
          printerror(status);
	}
	else { /* map OBSTYPE into <filetype> */
	  if (!strcmp(obstype,"object") || !strcmp(obstype,"OBJECT"))
	    sprintf(filetype,"raw_obj");
	  else if (!strcmp(obstype,"bias") || !strcmp(obstype,"BIAS")
	    || !strcmp(obstype,"zero") || !strcmp(obstype,"ZERO"))
	    sprintf(filetype,"raw_bias");
	  else if (!strcmp(obstype,"dark") || !strcmp(obstype,"DARK"))
	    sprintf(filetype,"raw_dark");
	  else if (!strcmp(obstype,"dome flat") || !strcmp(obstype,"DOME FLAT")
	    || !strcmp(obstype,"flat") || !strcmp(obstype,"FLAT"))
	    sprintf(filetype,"raw_dflat");
	  else {
	    sprintf(event,"OBSTYPE=%s not recognized",obstype);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
 	  }
	}
	

	/**********************************************************/
	/**********  Read All Image Data into Memory **************/
	/**********    Enables Crosstalk Correction  **************/
	/**********************************************************/

	if (flag_verbose) printf("  Reading image data from %s\n  ",
	    filename);	  
	for (i=2;i<=hdunum;i++) {

	  /* Move to the correct HDU */
	  if (fits_movabs_hdu(fptr,i,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=%d failed: %s",i,filename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);	
	  }

	  if (fits_get_img_param(fptr,2,&bitpix,&naxis,axes,&status)) {
	    sprintf(event,"Image params not found in %s",filename);
	    reportevt(flag_verbose,STATUS,5,event);
            printerror(status);
	  }

          /* double check image size */
          pixels  = axes[0]*axes[1];
          fpixel   = 1; nullval  = 0;
	  if (i>2) {
	    if (pixels!=oldpixel) {
	      sprintf(event,"Image extensions have different sizes:  %d  vs %d",
	        pixels,oldpixel);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	  }
	  else oldpixel=pixels;
	 
	  indata[i-1]=(float *)calloc(pixels,sizeof(float));
          if (!indata[i-1]) {
            sprintf(event,"Error: Could not calloc indata[%d] %d",i-1,pixels);
	    reportevt(flag_verbose,STATUS,5,event);
            exit(0);
          }
	  
          /* read the CHDU image  */
          if (fits_read_img(fptr,TFLOAT,fpixel,pixels,&nullval,
            indata[i-1],&anynull,&status)) {
            sprintf(event,"Reading image extension %d failed: %s",i-1,
	      filename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	    
	  if (flag_verbose) {printf(".");fflush(stdout);}
	
	}
	if (flag_verbose) {printf("\n");fflush(stdout);}
	

	/**********************************************************/
	/**********     Cycle through extensions        ***********/
	/**********    preparing and writing data       ***********/
	/**********************************************************/

	naxes[0]=axes[0];naxes[1]=axes[1];
	npixels=naxes[0]*naxes[1];

	/* jump to 2nd HDU  */
	if (fits_movabs_hdu(fptr,2,&hdutype,&status)) {
	  sprintf(event,"Move to HDU=2 failed");
	  reportevt(flag_verbose,STATUS,5,event);
	  printerror(status);	
	}

	/* now cycle through the HDU's parsing properly and writing */
	/* useful FITS file */
	for (i=2;i<=hdunum;i++) {

	  ccdnum=i-1;
	  outdata=indata[i-1];
	  
	  fits_get_hdu_num(fptr,&chdu);
	  if (flag_verbose) printf("  Currently located at HDU %d of %d\n",
	    chdu,hdunum);
	  if (chdu!=i) {
	    sprintf(event,"Not located at correct HDU (%d instead of %d)",
	      chdu,i);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }


	  /**********************************************************/
	  /*************  Read Header from Nth Extension ************/
	  /**********************************************************/

          if (fits_hdr2str(fptr,INCLUDECOMMENTS,exclkey,nexc,&nthheader,
	    &numnthkeys,&status)) {
	    sprintf(event,"Header %d not read",i-1);
	    reportevt(flag_verbose,STATUS,5,event);
            printerror(status);
	  }
	  /* cut off last bogus "END" line */
	  nthheader[strlen(nthheader)-80]=0;
          if (flag_verbose) {
	    printf("  Read %d header keywords\n",numnthkeys);
	    if (flag_verbose==3) {
	      printf("  *************************************************\n");
	      printf("  %s",nthheader);
	      printf("*************************************************\n");
	    }
	  }


	  /****************************************************************** */
	  /* ************** apply the crosstalk correction ****************** */
	  /* **************************************************************** */

	  if (flag_crosstalk) {
	    for (y=0;y<naxes[1];y++) for (x=0;x<naxes[0];x++) {
	      locout=y*naxes[0]+x;
	      xtalkcorrection=0.0;
	        if (x<naxes[0]/2) { /* in amp A */
		   ampextension=(i-2)*2;
		  for (j=1;j<hdunum;j++) {
		    ampA=(j-1)*2;
		    locA=locout;
		    if (xtalk[ampextension][ampA]>1.0e-6) 
		      xtalkcorrection+=xtalk[ampextension][ampA]*indata[j][locA];
		    ampB=ampA+1; /* Amp B flipped because readout on far side */
		    locB=y*naxes[0]+(naxes[0]-x-1);
		    if (xtalk[ampextension][ampB]>1.0e-6) 
		      xtalkcorrection+=xtalk[ampextension][ampB]*indata[j][locB];   
		  }
		  outdata[locout]-=xtalkcorrection;
		}
		else { /* in amp B */
		   ampextension=(i-2)*2+1;
		  for (j=1;j<hdunum;j++) {
		    ampA=(j-1)*2;
		    locA=y*naxes[0]+(naxes[0]-x-1);
		    if (xtalk[ampextension][ampA]>1.0e-6)
		      xtalkcorrection+=xtalk[ampextension][ampA]*indata[j][locA];	   
		    ampB=ampA+1; /* Amp B flipped because readout on far side */
		    locB=locout;
		    if (xtalk[ampextension][ampB]>1.0e-6) 
		      xtalkcorrection+=xtalk[ampextension][ampB]*indata[j][locB];	    
		  }
		  outdata[locout]-=xtalkcorrection;
		}
	    }
	  }

	  /* **************************************************************** */
	  /* **************** Write New Single CCD Image ******************** */
	  /* ************ Store Processing History in Header **************** */
	  /* **************************************************************** */

	  /* make sure path exists first time through */
	  if (i==2) {
	    sprintf(nfilename,"%s_%02d.fits",argv[2],i-1);
	    if (mkpath(nfilename,flag_verbose)) {
	      sprintf(event,"Failed to create path to file: %s",nfilename);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    else {
	      sprintf(event,"Created path to file: %s",nfilename);
	      reportevt(flag_verbose,STATUS,1,event);
	    }
	  }
	  sprintf(nfilename,"!%s_%02d.fits",argv[2],i-1);
	  if (fits_create_file(&nfptr,nfilename,&status)) {
	    sprintf(event,"File creation of %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (flag_verbose) {
	    sprintf(event,"Opened image for CCD %d : %s ",i-1,nfilename+1);
	    reportevt(flag_verbose,STATUS,1,event);
	  }
	  
	  /* create image extension  */
	  if (fits_create_img(nfptr,FLOAT_IMG,2,naxes,&status)) {
	    sprintf(event,"Creating image failed: %s",nfilename);
            reportevt(flag_verbose,STATUS,5,event);
            printerror(status);
          }

	  /* first reserve room within the header for the known # of keywords */
	  if (fits_set_hdrsize(nfptr,numzerokeys+numnthkeys+15,&status)) {
	    sprintf(event,"Reserving header space for %d keywords failed in %s",numzerokeys+numnthkeys+10,nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }


	  /* fix header issues for Megacam images */
	  fixheader_flt(nthheader, "MAXLINA", "SATURATA", 0., flag_verbose);	
	  fixheader_flt(nthheader, "MAXLINB", "SATURATB", 0., flag_verbose);
  	  status=0;
	  if(fits_read_key_lnglng(fptr, "EXTVER", &lccdnum, comment, &status)){
		sprintf(event,"Header Keyword EXTVER not found");
		reportevt(flag_verbose,STATUS,5,event);
		printerror(status);
	  }
	  fixheader_int(nthheader, "EXTVER", "CCDNUM", (int)(lccdnum+1), flag_verbose);	
	  fixheader_flt(nthheader, "BSECA", "BIASSECA", 0., flag_verbose);	
	  fixheader_flt(nthheader, "BSECB", "BIASSECB", 0., flag_verbose);	  
	  fixheader_flt(nthheader, "RA      ", "TELRA", 0., flag_verbose);    
          fixheader_flt(nthheader, "DEC     ", "TELDEC", 0., flag_verbose);
	  fixheader_str(nthheader, "ASECA", "AMPSECA", "[1:1024,1:4612]", flag_verbose);	
	  fixheader_str(nthheader, "ASECB", "AMPSECB", "[1025:2048,1:4612]", flag_verbose);	
	  fixheader_flt(nthheader, "DATASEC", "DATASEC", "\'[33:2080,1:4612]\'", flag_verbose);
	  fixheader_str(nthheader, "TSECA", "TRIMSEC", "\'[33:2080,1:4612]\'", flag_verbose);

	  /* read the FILTER and convert to accepted type */
	  status=0;
          if (fits_read_key_str(fptr,"FILTER",filter,comment,&status)) {
          	sprintf(event,"Header Keyword FILTER not found");
          	reportevt(flag_verbose,STATUS,5,event);
          	printerror(status);
          }
	  /*   ***********************************************  */
	  /*   ***** Currently only set up for  4 filters ****  */
	  /*   ***********************************************  */
	  if      (!strncmp(filter,"g.MP9401",1)) sprintf(filter,"\'g\'");
	  else if (!strncmp(filter,"r.MP9601",1)) sprintf(filter,"\'r\'");
	  else if (!strncmp(filter,"i.MP9701",1)) sprintf(filter,"\'i\'");
	  else if (!strncmp(filter,"z.MP9801",1)) sprintf(filter,"\'z\'");
	  else {
	  	sprintf(event,"Unrecognized FILTER type %s",filter);
	  	reportevt(flag_verbose,STATUS,5,event);
	  	exit(0);
	  }
	  /* apply fix to nthheader */
	  fixheader_str(nthheader,"FILTER","FILTER",filter,flag_verbose);


	  /* copy zero header information into the new header */
	  /* note that last keyword is null keyword and truncates the header! */
	  /* only copy the header params that live within zerokeyword */
	  if (fits_get_hdrpos(nfptr,&totkeynum,&keynum,&status)) {
	    sprintf(event,"Reading header position in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  numwrittenkeys=0;
	  for (j=0;j<numzerokeys-1;j++) {
	      /* don't write keyword if it is duplicate */
	      keyflag=0;
	      for (k=0;k<numnthkeys;k++) {
		if (!strncmp(zeroheader+j*80,nthheader+k*80,8)) {
	  	  keyflag=1;
		  break;
	        }
	      }
	      /* copy the record into the header */
	      if (!keyflag) {
		if (fits_insert_record(nfptr,totkeynum+numwrittenkeys,
		  zeroheader+j*80,&status)) {
	    	    sprintf(event,"Zero Header insert in %s failed",nfilename);
	    	    reportevt(flag_verbose,STATUS,5,event);
		    printerror(status);
	  	  }
	  	numwrittenkeys++;
	      }
	  }
	  if (flag_verbose) printf("%d keywords from 0th header, ",
	    numwrittenkeys);

	  /* copy the header information from the current HDU of MEF */
	  if (fits_get_hdrpos(nfptr,&totkeynum,&keynum,&status)) {
	    sprintf(event,"Reading header position in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (flag_verbose==3) {
	    sprintf(event,"Currently at header position %d with %d keywords",
	      keynum,totkeynum);
	    reportevt(flag_verbose,STATUS,1,event);
	  }
	  numwrittenkeys=0;
	  for (j=0;j<numnthkeys-1;j++) {
	    /* copy the record into the header */
	    if (fits_insert_record(nfptr,totkeynum+numwrittenkeys,
	      nthheader+j*80,&status)) {
	        sprintf(event,"Insert of %dth header keyword in %s failed:\n%s",
	          numwrittenkeys,nfilename,nthheader+j*80);
	  	  reportevt(flag_verbose,STATUS,5,event);
		  printerror(status);
	     }
	     numwrittenkeys++;
	  }
	  if (flag_verbose) printf("  Copied %d keywords from current header, ",
	    numwrittenkeys-3);

  
	    
	  /* write image */
	  if (fits_write_img(nfptr,TFLOAT,1,npixels,outdata,&status)) {
	    sprintf(event,"Writing image %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (flag_verbose) printf("  Wrote image data, ");

	  /* want only the image name-- search for first occurence of "/" */
          for (j=strlen(nfilename);j>=0;j--) 
	    if (!strncmp(&(nfilename[j]),"/",1)) break;
	  sprintf(newimagename,"%s",(nfilename+j+1));
	  for (j=strlen(newimagename);j>=0;j--) 
	    if (!strncmp(&(newimagename[j]),".fits",5)) break;
	  newimagename[j]=0;
	  if (!strncmp(newimagename,"!",1)) 
	    sprintf(newimagename,"%s",newimagename+1);
	  if (fits_modify_key_str(nfptr,"FILENAME",newimagename,"",&status)) {
	    sprintf(event,"Keyword FILENAME modify in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (flag_verbose) printf("FILENAME = %s, ",nfilename+1);

	  if (fits_insert_record(nfptr,totkeynum+(++numwrittenkeys),
	    "COMMENT ------------------------",
	    &status)) {
	    sprintf(event,"Comment insert in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

	  /* add photflag */
	  if (fits_update_key_lng(nfptr,"PHOTFLAG",flag_phot,
	    "Night Photometric (1) or not (0)",&status)) {
	    sprintf(event,"Keyword PHOTFLAG insert in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (flag_verbose) printf("PHOTFLAG = %d\n",flag_phot);

	  /* update OBSTYPE keyword */
	  if (fits_update_key_str(nfptr,"OBSTYPE",filetype,
	    "Type of observation",&status)) {
	    sprintf(event,"Keyword OBSTYPE update in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (flag_verbose) printf("  OBSTYPE = %s\n",filetype);

	  /* leave processing history in the header */
          tm=time(NULL);
          sprintf(comment,"%s",asctime(localtime(&tm)));
          comment[strlen(comment)-1]=0;
	  if (fits_write_key_str(nfptr,"DESDCxtk",comment,
	    "Megacam image conversion and crosstalk correction",
	    &status)) {
	    sprintf(event,"Keyword DESDCxtk insert in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

	  sprintf(longcomment,"DESDM:");
	  for (j=0;j<argc;j++) sprintf(longcomment,"%s %s",longcomment,argv[j]);
	  if (fits_write_comment(nfptr,longcomment,&status)) {
	    sprintf(event,"DESDM: longcomment insert in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (flag_verbose==3) printf("  => %s\n",longcomment);
	  /* mark this as an image extension */
	  if (fits_write_key_str(nfptr,"DES_EXT","IMAGE","Image extension",
	    &status)) {
	    sprintf(event,"Keyword DES_EXT insert in %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

          /* close the new image */
          if (fits_close_file(nfptr,&status)) {
	    sprintf(event,"Closing %s failed",nfilename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
          if (flag_verbose) {
            sprintf(event,"Closed image %d X %d  OBSTYPE = %s PHOTFLAG = %d : %s",
              naxes[0],naxes[1],filetype,flag_phot,nfilename+1);
            reportevt(flag_verbose,STATUS,1,event);
          }


	  if (flag_verbose) printf("  Closed image %s\n",nfilename+1);
	  /* Move to the next HDU */
	  if (i<hdunum) 
	    if (fits_movrel_hdu(fptr,1,&hdutype,&status)) {
	      sprintf(event,"Move to next HDU failed");
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }

        }


	/* ********************************************************* */
	/* ************** Close Input Image and Exit *************** */
	/* ********************************************************* */

        if (fits_close_file(fptr,&status)) {
	  sprintf(event,"Closing Exposure failed");
	  reportevt(flag_verbose,STATUS,5,event);
	  printerror(status);
	}
	if (flag_verbose) {
	  sprintf(event,"Closed image %s",filename);
	  reportevt(flag_verbose,STATUS,1,event);
	}
	return(0);
}

int grabkeywordval(header,numkeys,keyword,keytyp,keyval)
  char header[],keyword[];
  int numkeys,keytyp;
  void *keyval;
{
	int	i,loc,j=0,found=0;
	char 	*val_str;
	float	*val_flt;
	double	*val_dbl;
	int	*val_int;
	
	for (i=0;i<numkeys;i++) {
	  loc=i*80;
	  /* find the keyword */
	  if (!strncmp(keyword,header+loc,strlen(keyword))) {
	    loc+=9;
	    /* strip of any leading spaces or ' */
	    while (!strncmp(header+loc," ",1) || 
	      !strncmp(header+loc,"'",1)) loc++;
	    found++;
	    if (keytyp==STRING) {
	      val_str=(char *)keyval;
	      /* copy over until "/" */
	      while (strncmp(header+loc,"/",1)) val_str[j++]=header[loc++];
	      j--;
	      /* strip off any trailing spaces of ' */
	      while (!strncmp(val_str+j," ",1) || 
	        !strncmp(val_str+j,"'",1)) j--;
	      val_str[j+1]=0;
	      /*printf("  KW: %8s %s\n",keyword,val_str);*/
	      break;
	    }
	    if (keytyp==FLOAT)  {
	      val_flt=(float *)keyval;
	      sscanf(header+loc,"%f",val_flt);
	      /*printf("  KW: %8s %f\n",keyword,*val_flt);*/
	      break;
	    }
	    if (keytyp==DOUBLE) {
	      val_dbl=(double *)keyval;
	      sscanf(header+loc,"%lf",val_dbl);
	      /*printf("  KW: %8s %f\n",keyword,*val_dbl);*/
	      break;
	    }
	    if (keytyp==INTEG) {
	      val_int=(int *)keyval;
	      sscanf(header+loc,"%d",val_int);
	      /*printf("  KW: %8s %d\n",keyword,*val_int);*/
	      break;
	    }
	  }
	}
	return(found);
}




void fixheader_str(hdr,oldkey,newkey,value,flag_verbose)
	char hdr[],oldkey[],newkey[],value[];
	int flag_verbose;
{
	
	int	len,lines,flag=0,keylen,i,nlinelen,j,cflag=0,nkeylen;
	char	event[500],nline[100],comment[80];
	void	reportevt();

	len=strlen(hdr);
	keylen=strlen(oldkey);
	nkeylen=strlen(newkey);if (nkeylen>8) nkeylen=8;
	lines=len/80;

	for (i=0;i<lines;i++) {
	  if (!strncmp(oldkey,hdr+i*80,keylen)) {
	    flag=1;
	    if (value=="") strncpy(hdr+i*80,newkey,nkeylen);
	    else {
	      sprintf(nline,"%-8s= %-20s",newkey,value);
	      nlinelen=strlen(nline);  if (nlinelen>79) nlinelen=79;
	      strncpy(hdr+i*80,nline,nlinelen);
	    }
	    break;
	  }
	}
	if (!flag && flag_verbose) {
	  sprintf(event,"Keyword %s not found and updated to %s",oldkey,newkey);
	  reportevt(flag_verbose,STATUS,5,event);
	   exit(0); 
	}
}

void fixheader_flt(hdr,oldkey,newkey,value,flag_verbose)
     char hdr[],oldkey[],newkey[];
	float value;
	int flag_verbose;
{
	int	len,lines,flag=0,keylen,i,nlinelen,j,cflag=0;
	char	event[500],nline[100],comment[80];
	void	reportevt();
	len=strlen(hdr);
	keylen=strlen(oldkey);
	lines=len/80;
	for (i=0;i<lines;i++) {
	  if (!strncmp(oldkey,hdr+i*80,keylen)) {
	    flag=1;
	    /* just change the keyword */
	    if (fabs(value)<1.0e-6)strncpy(hdr+i*80,newkey,strlen(newkey));
	    else { /* change keyword and value while preserving comment */
	      if (!cflag) sprintf(comment,"");
	      sprintf(nline,"%-8s= '%18.2f'",newkey,value);
	      nlinelen=strlen(nline)-1;  if (nlinelen>79) nlinelen=79;
	      strncpy(hdr+i*80,nline,strlen(nline)-1);
	    }
	    break;
	  }
	}

		if (!flag && flag_verbose) {
		 	  sprintf(event,"Keyword %s not found and updated to %s",oldkey,newkey);
	   reportevt(flag_verbose,STATUS,5,event); 
	   /*printf("******\n%s\n*******\n",hdr); */
	    exit(0);  
		}

	else if (flag_verbose==3) {
	  sprintf(event,"Keyword %s found and updated to %s",oldkey,newkey);
	  reportevt(flag_verbose,STATUS,1,event);
	}

}




void fixheader_int(hdr,oldkey,newkey,value,flag_verbose)
     char hdr[],oldkey[],newkey[];
        int value;
        int flag_verbose;
{
        int     len,lines,flag=0,keylen,i,nlinelen,j,cflag=0;
        char    event[500],nline[100],comment[80];
        void    reportevt();
        len=strlen(hdr);
        keylen=strlen(oldkey);
        lines=len/80;
        for (i=0;i<lines;i++) {
          if (!strncmp(oldkey,hdr+i*80,keylen)) {
            flag=1;
            /* just change the keyword */
            if (value==0)strncpy(hdr+i*80,newkey,strlen(newkey));
            else { /* change keyword and value while preserving comment */
              if (!cflag) sprintf(comment,"");
              sprintf(nline,"%-8s= %-22d",newkey,value);
              nlinelen=strlen(nline)-1;  if (nlinelen>79) nlinelen=79;
              strncpy(hdr+i*80,nline,strlen(nline)-1);
            }
            break;
          }
        }

        if (!flag && flag_verbose) {
           sprintf(event,"Keyword %s not found and updated to %s",oldkey,newkey);
           reportevt(flag_verbose,STATUS,5,event);
           /*printf("******\n%s\n*******\n",hdr); */
            exit(0);
                }

        else if (flag_verbose==3) {
          sprintf(event,"Keyword %s found and updated to %s",oldkey,newkey);
          reportevt(flag_verbose,STATUS,1,event);
        }

}












#undef STRING
#undef FLOAT
#undef DOUBLE
#undef INTEG
