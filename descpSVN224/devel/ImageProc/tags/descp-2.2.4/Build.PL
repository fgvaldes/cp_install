#!/usr/bin/perl

use Module::Build;
use Data::Dumper;

# Code to install everything in libexec to ${install_base}/libexec:
my $libexec_dir = 'libexec';
my $libexec_files = {};
my $libexec_file_list = Module::Build->rscan_dir($libexec_dir, sub {-f $_ and not /svn/});
foreach my $libexec_file (@$libexec_file_list) {
    $libexec_files->{$libexec_file} = $libexec_file;
}

# Code to install everything in etc to ${install_base}/etc:
my $etc_dir = 'etc';
my $etc_files = {};
my $etc_file_list = Module::Build->rscan_dir($etc_dir, sub {-f $_ and not /svn/});
foreach my $etc_file (@$etc_file_list) {
    $etc_files->{$etc_file} = $etc_file;
}

my $build = Module::Build->new (
   dist_name => 'DES_ImageProc',
   dist_version => 'noversion',
   requires => {
                 'DBI'            => 1.602,
                 'DBD::Oracle'    => 1.20,
                 'List::Util' => 1.19,
                 'Exception::Class' => 1.23,
                 'Config::General' => 2.38,
               },
   libexec_files => $libexec_files,
   etc_files => $etc_files,
);

# Additional calls for added directories
$build->add_build_element('libexec');
$build->add_build_element('etc');

$build->install_path("$libexec_dir" => $build->install_base().'/libexec');
$build->install_path("$etc_dir" => $build->install_base().'/etc');


$build->create_build_script;
