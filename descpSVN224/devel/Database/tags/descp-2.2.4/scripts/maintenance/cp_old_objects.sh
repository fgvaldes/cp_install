#!/bin/bash
#
# Script for copying the missing SCS objects 
# from old_objects to objects.
# The imageid range for Location.project='SCS' is 15236 to 10003281
#
# Dated : Jan. 09, 2009

DIR='/oracle/scripts/des'
NEWTBLNAME='objects'
OLDTBLNAME='objects'
INCREMENT=10000
MINID=10000
MAXID=20000
STOPID=10050000
SQLFILE='/oracle/scripts/des/cp_old_objects.sql'
source $DIR/des.bash

while [ $MINID -lt $STOPID ] ; do 
  cp /dev/null $SQLFILE
  echo "insert into objects" >> $SQLFILE
  echo "select b.*, b.alpha_j2000, b.delta_j2000" >> $SQLFILE
  echo "from old_objects b, location c" >> $SQLFILE
  echo "where b.imageid = c.id" >> $SQLFILE
  echo "and c.project = 'SCS'" >> $SQLFILE
  echo "and c.id >= $MINID " >> $SQLFILE
  echo "and c.id < $MAXID " >> $SQLFILE
  echo "and not exists (select d.object_id from objects d where d.object_id = b.object_id);" >> $SQLFILE
  echo "commit; " >> $SQLFILE
  echo "exit; " >> $SQLFILE
  cat $SQLFILE
  sqlplus des_admin/passwd @$SQLFILE
  MINID=$MAXID
  MAXID=`expr $MINID + $INCREMENT`
done

exit
