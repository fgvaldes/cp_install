create smallfile tablespace desdr
datafile '/oracle/data/desdr/desdr_01.dbf'
size 1000m autoextend on next 500m maxsize unlimited
logging extent management local segment space management auto;

create table objects_dr001 
  partition by range(ra)
    (partition ra10 values less than(10) tablespace desdr, 
     partition ra20 values less than(20) tablespace desdr, 
     partition ra30 values less than(30) tablespace desdr, 
     partition ra40 values less than(40) tablespace desdr, 
     partition ra50 values less than(50) tablespace desdr, 
     partition ra60 values less than(60) tablespace desdr, 
     partition ra70 values less than(70) tablespace desdr, 
     partition ra80 values less than(80) tablespace desdr, 
     partition ra90 values less than(90) tablespace desdr, 
     partition ra100 values less than(100) tablespace desdr, 
     partition ra110 values less than(110) tablespace desdr, 
     partition ra120 values less than(120) tablespace desdr, 
     partition ra130 values less than(130) tablespace desdr, 
     partition ra140 values less than(140) tablespace desdr, 
     partition ra150 values less than(150) tablespace desdr, 
     partition ra160 values less than(160) tablespace desdr, 
     partition ra170 values less than(170) tablespace desdr, 
     partition ra180 values less than(180) tablespace desdr, 
     partition ra190 values less than(190) tablespace desdr, 
     partition ra200 values less than(200) tablespace desdr, 
     partition ra210 values less than(210) tablespace desdr, 
     partition ra220 values less than(220) tablespace desdr, 
     partition ra230 values less than(230) tablespace desdr, 
     partition ra240 values less than(240) tablespace desdr, 
     partition ra250 values less than(250) tablespace desdr, 
     partition ra260 values less than(260) tablespace desdr, 
     partition ra270 values less than(270) tablespace desdr, 
     partition ra280 values less than(280) tablespace desdr, 
     partition ra290 values less than(290) tablespace desdr, 
     partition ra300 values less than(300) tablespace desdr, 
     partition ra310 values less than(310) tablespace desdr, 
     partition ra320 values less than(320) tablespace desdr, 
     partition ra330 values less than(330) tablespace desdr, 
     partition ra340 values less than(340) tablespace desdr, 
     partition ra350 values less than(350) tablespace desdr, 
     partition ra360 values less than(360) tablespace desdr, 
     partition rahigh values less than(MAXVALUE) tablespace desdr) 
as
select wla.project, wla.nite, wla.run, wla.wltype filetype, 
obj.object_id, obj.equinox, obj.band, obj.htmid,
obj.cx, obj.cy, obj.cz, obj.parentid, obj.softid, obj.imageid,
obj.zeropoint, obj.errzeropoint, obj.zeropointid,
obj.object_number, obj.mag_auto, obj.magerr_auto,
obj.mag_aper_1, obj.magerr_aper_1, obj.mag_aper_2, obj.magerr_aper_2, 
obj.mag_aper_3, obj.magerr_aper_3, obj.mag_aper_4, obj.magerr_aper_4, 
obj.mag_aper_5, obj.magerr_aper_5, obj.mag_aper_6, obj.magerr_aper_6,
obj.mag_aper_7, obj.magerr_aper_7, obj.mag_aper_8, obj.magerr_aper_8, 
obj.mag_aper_9, obj.magerr_aper_9, obj.mag_aper_10, obj.magerr_aper_10, 
obj.mag_aper_11, obj.magerr_aper_11, obj.mag_aper_12, obj.magerr_aper_12, 
obj.mag_aper_13, obj.magerr_aper_13, obj.mag_aper_14, obj.magerr_aper_14, 
obj.mag_aper_15, obj.magerr_aper_15, obj.mag_aper_16, obj.magerr_aper_16, 
obj.mag_aper_17, obj.magerr_aper_17,
obj.alpha_j2000, obj.delta_j2000, obj.alphapeak_j2000, obj.deltapeak_j2000, 
obj.x2_world, obj.errx2_world, obj.y2_world, obj.erry2_world, 
obj.xy_world, obj.errxy_world, obj.threshold, 
obj.x_image, obj.y_image, obj.xmin_image, obj.ymin_image, 
obj.xmax_image, obj.ymax_image, obj.x2_image, obj.errx2_image, 
obj.y2_image, obj.erry2_image, obj.xy_image, obj.errxy_image, 
obj.a_image, obj.erra_image, obj.b_image, obj.errb_image,
obj.theta_image, obj.errtheta_image,
obj.ellipticity, obj.class_star, obj.flags,
obj.flux_radius, obj.fwhm_world, obj.isoarea_world,
obj.theta_j2000, obj.background, obj.catalogid object_catalogid,
obj.chi2_model, obj.flags_model, obj.niter_model, 
obj.flux_model, obj.fluxerr_model, 
obj.mag_model, obj.magerr_model, 
obj.xmodel_image, obj.ymodel_image, obj.xmodel_world, obj.ymodel_world, 
obj.alphamodel_sky, obj.deltamodel_sky, 
obj.alphamodel_j2000, obj.deltamodel_j2000,
obj.errx2model_image, obj.erry2model_image, obj.errxymodel_image,
obj.errx2model_world, obj.erry2model_world, obj.errxymodel_world,
obj.errcxxmodel_image, obj.errcyymodel_image, obj.errcxymodel_image,
obj.errcxxmodel_world, obj.errcyymodel_world, obj.errcxymodel_world,
obj.erramodel_image, obj.errbmodel_image, obj.errthetamodel_image,
obj.erramodel_world, obj.errbmodel_world, obj.errthetamodel_world,
obj.errthetamodel_sky, obj.errthetamodel_j2000, 
obj.x2model_image, obj.y2model_image, obj.xymodel_image, 
obj.e1model_image, obj.e2model_image, 
obj.eps1model_image, obj.eps2model_image,
obj.flux_spheroid, obj.fluxerr_spheroid, 
obj.mag_spheroid, obj.magerr_spheroid, 
obj.spheroid_reff_image, obj.spheroid_refferr_image,
obj.spheroid_reff_world, obj.spheroid_refferr_world, 
obj.spheroid_aspect_image, obj.spheroid_aspecterr_image, 
obj.spheroid_aspect_world, obj.spheroid_aspecterr_world,
obj.spheroid_theta_image, obj.spheroid_thetaerr_image, 
obj.spheroid_theta_world, obj.spheroid_thetaerr_world, 
obj.spheroid_theta_sky, obj.spheroid_theta_j2000,
obj.flux_disk, obj.fluxerr_disk, obj.mag_disk, obj.magerr_disk, 
obj.disk_scale_image, obj.disk_scaleerr_image, 
obj.disk_scale_world, obj.disk_scaleerr_world,
obj.disk_aspect_image, obj.disk_aspecterr_image, 
obj.disk_aspect_world, obj.disk_aspecterr_world, 
obj.disk_inclination, obj.disk_inclinationerr,
obj.disk_theta_image, obj.disk_thetaerr_image, 
obj.disk_theta_world, obj.disk_thetaerr_world, 
obj.disk_theta_sky, obj.disk_theta_j2000,
obj.flux_psf, obj.fluxerr_psf, obj.mag_psf, obj.magerr_psf, 
fnd.id findstar_id, fnd.catalogid findstar_catalogid, 
fnd.sigma0, fnd.size_flags, fnd.star_flag, 
shr.id shear_id, shr.catalogid shear_catalogid, shr.shear_flags,
shr.shear1, shr.shear2, 
shr.shear_cov00, shr.shear_cov01, shr.shear_cov11, shr.gal_order,
shr.coeffs_1, shr.coeffs_2, shr.coeffs_3, shr.coeffs_4, shr.coeffs_5, 
shr.coeffs_6, shr.coeffs_7, shr.coeffs_8, shr.coeffs_9, shr.coeffs_10, 
shr.coeffs_11, shr.coeffs_12, shr.coeffs_13, shr.coeffs_14, shr.coeffs_15, 
shr.coeffs_16, shr.coeffs_17, shr.coeffs_18, shr.coeffs_19, shr.coeffs_20, 
shr.coeffs_21, shr.coeffs_22, shr.coeffs_23, shr.coeffs_24, shr.coeffs_25, 
shr.coeffs_26, shr.coeffs_27, shr.coeffs_28,
obj.ra, obj.dec
from objects_2008 obj, wl wla, wl wlb, wl_findstars fnd, wl_shear shr
where wla.run = '20081229182045_DES2212-3520'
and   wla.wltype = 'shapelet_shpltall'
and   fnd.catalogid = wla.id 
and   shr.catalogid = wlb.id 
and   wla.run = wlb.run 
and   obj.object_id = fnd.object_id 
and   fnd.object_id = shr.object_id;

insert into objects_dr001
select wla.project, wla.nite, wla.run, wla.wltype filetype, 
obj.object_id, obj.equinox, obj.band, obj.htmid,
obj.cx, obj.cy, obj.cz, obj.parentid, obj.softid, obj.imageid,
obj.zeropoint, obj.errzeropoint, obj.zeropointid,
obj.object_number, obj.mag_auto, obj.magerr_auto,
obj.mag_aper_1, obj.magerr_aper_1, obj.mag_aper_2, obj.magerr_aper_2, 
obj.mag_aper_3, obj.magerr_aper_3, obj.mag_aper_4, obj.magerr_aper_4, 
obj.mag_aper_5, obj.magerr_aper_5, obj.mag_aper_6, obj.magerr_aper_6,
obj.mag_aper_7, obj.magerr_aper_7, obj.mag_aper_8, obj.magerr_aper_8, 
obj.mag_aper_9, obj.magerr_aper_9, obj.mag_aper_10, obj.magerr_aper_10, 
obj.mag_aper_11, obj.magerr_aper_11, obj.mag_aper_12, obj.magerr_aper_12, 
obj.mag_aper_13, obj.magerr_aper_13, obj.mag_aper_14, obj.magerr_aper_14, 
obj.mag_aper_15, obj.magerr_aper_15, obj.mag_aper_16, obj.magerr_aper_16, 
obj.mag_aper_17, obj.magerr_aper_17,
obj.alpha_j2000, obj.delta_j2000, obj.alphapeak_j2000, obj.deltapeak_j2000, 
obj.x2_world, obj.errx2_world, obj.y2_world, obj.erry2_world, 
obj.xy_world, obj.errxy_world, obj.threshold, 
obj.x_image, obj.y_image, obj.xmin_image, obj.ymin_image, 
obj.xmax_image, obj.ymax_image, obj.x2_image, obj.errx2_image, 
obj.y2_image, obj.erry2_image, obj.xy_image, obj.errxy_image, 
obj.a_image, obj.erra_image, obj.b_image, obj.errb_image,
obj.theta_image, obj.errtheta_image,
obj.ellipticity, obj.class_star, obj.flags,
obj.flux_radius, obj.fwhm_world, obj.isoarea_world,
obj.theta_j2000, obj.background, obj.catalogid object_catalogid,
obj.chi2_model, obj.flags_model, obj.niter_model, 
obj.flux_model, obj.fluxerr_model, 
obj.mag_model, obj.magerr_model, 
obj.xmodel_image, obj.ymodel_image, obj.xmodel_world, obj.ymodel_world, 
obj.alphamodel_sky, obj.deltamodel_sky, 
obj.alphamodel_j2000, obj.deltamodel_j2000,
obj.errx2model_image, obj.erry2model_image, obj.errxymodel_image,
obj.errx2model_world, obj.erry2model_world, obj.errxymodel_world,
obj.errcxxmodel_image, obj.errcyymodel_image, obj.errcxymodel_image,
obj.errcxxmodel_world, obj.errcyymodel_world, obj.errcxymodel_world,
obj.erramodel_image, obj.errbmodel_image, obj.errthetamodel_image,
obj.erramodel_world, obj.errbmodel_world, obj.errthetamodel_world,
obj.errthetamodel_sky, obj.errthetamodel_j2000, 
obj.x2model_image, obj.y2model_image, obj.xymodel_image, 
obj.e1model_image, obj.e2model_image, 
obj.eps1model_image, obj.eps2model_image,
obj.flux_spheroid, obj.fluxerr_spheroid, 
obj.mag_spheroid, obj.magerr_spheroid, 
obj.spheroid_reff_image, obj.spheroid_refferr_image,
obj.spheroid_reff_world, obj.spheroid_refferr_world, 
obj.spheroid_aspect_image, obj.spheroid_aspecterr_image, 
obj.spheroid_aspect_world, obj.spheroid_aspecterr_world,
obj.spheroid_theta_image, obj.spheroid_thetaerr_image, 
obj.spheroid_theta_world, obj.spheroid_thetaerr_world, 
obj.spheroid_theta_sky, obj.spheroid_theta_j2000,
obj.flux_disk, obj.fluxerr_disk, obj.mag_disk, obj.magerr_disk, 
obj.disk_scale_image, obj.disk_scaleerr_image, 
obj.disk_scale_world, obj.disk_scaleerr_world,
obj.disk_aspect_image, obj.disk_aspecterr_image, 
obj.disk_aspect_world, obj.disk_aspecterr_world, 
obj.disk_inclination, obj.disk_inclinationerr,
obj.disk_theta_image, obj.disk_thetaerr_image, 
obj.disk_theta_world, obj.disk_thetaerr_world, 
obj.disk_theta_sky, obj.disk_theta_j2000,
obj.flux_psf, obj.fluxerr_psf, obj.mag_psf, obj.magerr_psf, 
fnd.id findstar_id, fnd.catalogid findstar_catalogid, 
fnd.sigma0, fnd.size_flags, fnd.star_flag, 
shr.id shear_id, shr.catalogid shear_catalogid, shr.shear_flags,
shr.shear1, shr.shear2, 
shr.shear_cov00, shr.shear_cov01, shr.shear_cov11, shr.gal_order,
shr.coeffs_1, shr.coeffs_2, shr.coeffs_3, shr.coeffs_4, shr.coeffs_5, 
shr.coeffs_6, shr.coeffs_7, shr.coeffs_8, shr.coeffs_9, shr.coeffs_10, 
shr.coeffs_11, shr.coeffs_12, shr.coeffs_13, shr.coeffs_14, shr.coeffs_15, 
shr.coeffs_16, shr.coeffs_17, shr.coeffs_18, shr.coeffs_19, shr.coeffs_20, 
shr.coeffs_21, shr.coeffs_22, shr.coeffs_23, shr.coeffs_24, shr.coeffs_25, 
shr.coeffs_26, shr.coeffs_27, shr.coeffs_28,
obj.ra, obj.dec
from objects_2008 obj, wl wla, wl wlb, wl_findstars fnd, wl_shear shr
where wla.run = '20081229182045_DES2212-3520'
and   wla.wltype = 'shapelet_shpltall'
and   fnd.catalogid = wla.id 
and   shr.catalogid = wlb.id 
and   wla.run = wlb.run 
and   obj.object_id = fnd.object_id 
and   fnd.object_id = shr.object_id;
-- 299067 rows

insert into objects_dr001
select wla.project, wla.nite, wla.run, wla.wltype filetype, 
obj.object_id, obj.equinox, obj.band, obj.htmid,
obj.cx, obj.cy, obj.cz, obj.parentid, obj.softid, obj.imageid,
obj.zeropoint, obj.errzeropoint, obj.zeropointid,
obj.object_number, obj.mag_auto, obj.magerr_auto,
obj.mag_aper_1, obj.magerr_aper_1, obj.mag_aper_2, obj.magerr_aper_2, 
obj.mag_aper_3, obj.magerr_aper_3, obj.mag_aper_4, obj.magerr_aper_4, 
obj.mag_aper_5, obj.magerr_aper_5, obj.mag_aper_6, obj.magerr_aper_6,
obj.mag_aper_7, obj.magerr_aper_7, obj.mag_aper_8, obj.magerr_aper_8, 
obj.mag_aper_9, obj.magerr_aper_9, obj.mag_aper_10, obj.magerr_aper_10, 
obj.mag_aper_11, obj.magerr_aper_11, obj.mag_aper_12, obj.magerr_aper_12, 
obj.mag_aper_13, obj.magerr_aper_13, obj.mag_aper_14, obj.magerr_aper_14, 
obj.mag_aper_15, obj.magerr_aper_15, obj.mag_aper_16, obj.magerr_aper_16, 
obj.mag_aper_17, obj.magerr_aper_17,
obj.alpha_j2000, obj.delta_j2000, obj.alphapeak_j2000, obj.deltapeak_j2000, 
obj.x2_world, obj.errx2_world, obj.y2_world, obj.erry2_world, 
obj.xy_world, obj.errxy_world, obj.threshold, 
obj.x_image, obj.y_image, obj.xmin_image, obj.ymin_image, 
obj.xmax_image, obj.ymax_image, obj.x2_image, obj.errx2_image, 
obj.y2_image, obj.erry2_image, obj.xy_image, obj.errxy_image, 
obj.a_image, obj.erra_image, obj.b_image, obj.errb_image,
obj.theta_image, obj.errtheta_image,
obj.ellipticity, obj.class_star, obj.flags,
obj.flux_radius, obj.fwhm_world, obj.isoarea_world,
obj.theta_j2000, obj.background, obj.catalogid object_catalogid,
obj.chi2_model, obj.flags_model, obj.niter_model, 
obj.flux_model, obj.fluxerr_model, 
obj.mag_model, obj.magerr_model, 
obj.xmodel_image, obj.ymodel_image, obj.xmodel_world, obj.ymodel_world, 
obj.alphamodel_sky, obj.deltamodel_sky, 
obj.alphamodel_j2000, obj.deltamodel_j2000,
obj.errx2model_image, obj.erry2model_image, obj.errxymodel_image,
obj.errx2model_world, obj.erry2model_world, obj.errxymodel_world,
obj.errcxxmodel_image, obj.errcyymodel_image, obj.errcxymodel_image,
obj.errcxxmodel_world, obj.errcyymodel_world, obj.errcxymodel_world,
obj.erramodel_image, obj.errbmodel_image, obj.errthetamodel_image,
obj.erramodel_world, obj.errbmodel_world, obj.errthetamodel_world,
obj.errthetamodel_sky, obj.errthetamodel_j2000, 
obj.x2model_image, obj.y2model_image, obj.xymodel_image, 
obj.e1model_image, obj.e2model_image, 
obj.eps1model_image, obj.eps2model_image,
obj.flux_spheroid, obj.fluxerr_spheroid, 
obj.mag_spheroid, obj.magerr_spheroid, 
obj.spheroid_reff_image, obj.spheroid_refferr_image,
obj.spheroid_reff_world, obj.spheroid_refferr_world, 
obj.spheroid_aspect_image, obj.spheroid_aspecterr_image, 
obj.spheroid_aspect_world, obj.spheroid_aspecterr_world,
obj.spheroid_theta_image, obj.spheroid_thetaerr_image, 
obj.spheroid_theta_world, obj.spheroid_thetaerr_world, 
obj.spheroid_theta_sky, obj.spheroid_theta_j2000,
obj.flux_disk, obj.fluxerr_disk, obj.mag_disk, obj.magerr_disk, 
obj.disk_scale_image, obj.disk_scaleerr_image, 
obj.disk_scale_world, obj.disk_scaleerr_world,
obj.disk_aspect_image, obj.disk_aspecterr_image, 
obj.disk_aspect_world, obj.disk_aspecterr_world, 
obj.disk_inclination, obj.disk_inclinationerr,
obj.disk_theta_image, obj.disk_thetaerr_image, 
obj.disk_theta_world, obj.disk_thetaerr_world, 
obj.disk_theta_sky, obj.disk_theta_j2000,
obj.flux_psf, obj.fluxerr_psf, obj.mag_psf, obj.magerr_psf, 
fnd.id findstar_id, fnd.catalogid findstar_catalogid, 
fnd.sigma0, fnd.size_flags, fnd.star_flag, 
shr.id shear_id, shr.catalogid shear_catalogid, shr.shear_flags,
shr.shear1, shr.shear2, 
shr.shear_cov00, shr.shear_cov01, shr.shear_cov11, shr.gal_order,
shr.coeffs_1, shr.coeffs_2, shr.coeffs_3, shr.coeffs_4, shr.coeffs_5, 
shr.coeffs_6, shr.coeffs_7, shr.coeffs_8, shr.coeffs_9, shr.coeffs_10, 
shr.coeffs_11, shr.coeffs_12, shr.coeffs_13, shr.coeffs_14, shr.coeffs_15, 
shr.coeffs_16, shr.coeffs_17, shr.coeffs_18, shr.coeffs_19, shr.coeffs_20, 
shr.coeffs_21, shr.coeffs_22, shr.coeffs_23, shr.coeffs_24, shr.coeffs_25, 
shr.coeffs_26, shr.coeffs_27, shr.coeffs_28,
obj.ra, obj.dec
from objects_2008 obj, wl wla, wl wlb, wl_findstars fnd, wl_shear shr
where wla.run = '20081231054208_DES2218-3103'
and   wla.wltype = 'shapelet_shpltall'
and   fnd.catalogid = wla.id 
and   shr.catalogid = wlb.id 
and   wla.run = wlb.run 
and   obj.object_id = fnd.object_id 
and   fnd.object_id = shr.object_id;
-- 452568 rows

insert into objects_dr001
select wla.project, wla.nite, wla.run, wla.wltype filetype, 
obj.object_id, obj.equinox, obj.band, obj.htmid,
obj.cx, obj.cy, obj.cz, obj.parentid, obj.softid, obj.imageid,
obj.zeropoint, obj.errzeropoint, obj.zeropointid,
obj.object_number, obj.mag_auto, obj.magerr_auto,
obj.mag_aper_1, obj.magerr_aper_1, obj.mag_aper_2, obj.magerr_aper_2, 
obj.mag_aper_3, obj.magerr_aper_3, obj.mag_aper_4, obj.magerr_aper_4, 
obj.mag_aper_5, obj.magerr_aper_5, obj.mag_aper_6, obj.magerr_aper_6,
obj.mag_aper_7, obj.magerr_aper_7, obj.mag_aper_8, obj.magerr_aper_8, 
obj.mag_aper_9, obj.magerr_aper_9, obj.mag_aper_10, obj.magerr_aper_10, 
obj.mag_aper_11, obj.magerr_aper_11, obj.mag_aper_12, obj.magerr_aper_12, 
obj.mag_aper_13, obj.magerr_aper_13, obj.mag_aper_14, obj.magerr_aper_14, 
obj.mag_aper_15, obj.magerr_aper_15, obj.mag_aper_16, obj.magerr_aper_16, 
obj.mag_aper_17, obj.magerr_aper_17,
obj.alpha_j2000, obj.delta_j2000, obj.alphapeak_j2000, obj.deltapeak_j2000, 
obj.x2_world, obj.errx2_world, obj.y2_world, obj.erry2_world, 
obj.xy_world, obj.errxy_world, obj.threshold, 
obj.x_image, obj.y_image, obj.xmin_image, obj.ymin_image, 
obj.xmax_image, obj.ymax_image, obj.x2_image, obj.errx2_image, 
obj.y2_image, obj.erry2_image, obj.xy_image, obj.errxy_image, 
obj.a_image, obj.erra_image, obj.b_image, obj.errb_image,
obj.theta_image, obj.errtheta_image,
obj.ellipticity, obj.class_star, obj.flags,
obj.flux_radius, obj.fwhm_world, obj.isoarea_world,
obj.theta_j2000, obj.background, obj.catalogid object_catalogid,
obj.chi2_model, obj.flags_model, obj.niter_model, 
obj.flux_model, obj.fluxerr_model, 
obj.mag_model, obj.magerr_model, 
obj.xmodel_image, obj.ymodel_image, obj.xmodel_world, obj.ymodel_world, 
obj.alphamodel_sky, obj.deltamodel_sky, 
obj.alphamodel_j2000, obj.deltamodel_j2000,
obj.errx2model_image, obj.erry2model_image, obj.errxymodel_image,
obj.errx2model_world, obj.erry2model_world, obj.errxymodel_world,
obj.errcxxmodel_image, obj.errcyymodel_image, obj.errcxymodel_image,
obj.errcxxmodel_world, obj.errcyymodel_world, obj.errcxymodel_world,
obj.erramodel_image, obj.errbmodel_image, obj.errthetamodel_image,
obj.erramodel_world, obj.errbmodel_world, obj.errthetamodel_world,
obj.errthetamodel_sky, obj.errthetamodel_j2000, 
obj.x2model_image, obj.y2model_image, obj.xymodel_image, 
obj.e1model_image, obj.e2model_image, 
obj.eps1model_image, obj.eps2model_image,
obj.flux_spheroid, obj.fluxerr_spheroid, 
obj.mag_spheroid, obj.magerr_spheroid, 
obj.spheroid_reff_image, obj.spheroid_refferr_image,
obj.spheroid_reff_world, obj.spheroid_refferr_world, 
obj.spheroid_aspect_image, obj.spheroid_aspecterr_image, 
obj.spheroid_aspect_world, obj.spheroid_aspecterr_world,
obj.spheroid_theta_image, obj.spheroid_thetaerr_image, 
obj.spheroid_theta_world, obj.spheroid_thetaerr_world, 
obj.spheroid_theta_sky, obj.spheroid_theta_j2000,
obj.flux_disk, obj.fluxerr_disk, obj.mag_disk, obj.magerr_disk, 
obj.disk_scale_image, obj.disk_scaleerr_image, 
obj.disk_scale_world, obj.disk_scaleerr_world,
obj.disk_aspect_image, obj.disk_aspecterr_image, 
obj.disk_aspect_world, obj.disk_aspecterr_world, 
obj.disk_inclination, obj.disk_inclinationerr,
obj.disk_theta_image, obj.disk_thetaerr_image, 
obj.disk_theta_world, obj.disk_thetaerr_world, 
obj.disk_theta_sky, obj.disk_theta_j2000,
obj.flux_psf, obj.fluxerr_psf, obj.mag_psf, obj.magerr_psf, 
fnd.id findstar_id, fnd.catalogid findstar_catalogid, 
fnd.sigma0, fnd.size_flags, fnd.star_flag, 
shr.id shear_id, shr.catalogid shear_catalogid, shr.shear_flags,
shr.shear1, shr.shear2, 
shr.shear_cov00, shr.shear_cov01, shr.shear_cov11, shr.gal_order,
shr.coeffs_1, shr.coeffs_2, shr.coeffs_3, shr.coeffs_4, shr.coeffs_5, 
shr.coeffs_6, shr.coeffs_7, shr.coeffs_8, shr.coeffs_9, shr.coeffs_10, 
shr.coeffs_11, shr.coeffs_12, shr.coeffs_13, shr.coeffs_14, shr.coeffs_15, 
shr.coeffs_16, shr.coeffs_17, shr.coeffs_18, shr.coeffs_19, shr.coeffs_20, 
shr.coeffs_21, shr.coeffs_22, shr.coeffs_23, shr.coeffs_24, shr.coeffs_25, 
shr.coeffs_26, shr.coeffs_27, shr.coeffs_28,
obj.ra, obj.dec
from objects_2008 obj, wl wla, wl wlb, wl_findstars fnd, wl_shear shr
where wla.run = '20081230170313_DES2231-3229'
and   wla.wltype = 'shapelet_shpltall'
and   fnd.catalogid = wla.id 
and   shr.catalogid = wlb.id 
and   wla.run = wlb.run 
and   obj.object_id = fnd.object_id 
and   fnd.object_id = shr.object_id;
-- 222298 rows

create index dr001_radec_idx on objects_dr001(ra,dec) local
tablespace desdr
parallel 4;

create index dr001_nite_idx on objects_dr001(nite) global
tablespace desdr
parallel 4;

create index dr001_run_idx on objects_dr001(run) global
tablespace desdr
parallel 4;

create index dr001_objectid_idx on objects_dr001(object_id) global
tablespace desdr
parallel 4;

create index dr001_findstarid_idx on objects_dr001(findstar_id) global
tablespace desdr
parallel 4;

create index dr001_shearid_idx on objects_dr001(shear_id) global
tablespace desdr
parallel 4;


