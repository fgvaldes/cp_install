connect "SYS"/"&&sysPassword" as SYSDBA
set echo on
spool /oracle/scripts/des//cloneDBCreation.log
Create controlfile reuse set database "des"
MAXINSTANCES 8
MAXLOGHISTORY 1
MAXLOGFILES 16
MAXLOGMEMBERS 3
MAXDATAFILES 500
Datafile 
'/oracle/data/des/system01.dbf',
'/oracle/data/des/undotbs01.dbf',
'/oracle/data/des/sysaux01.dbf',
'/oracle/data/des/users01.dbf'
LOGFILE GROUP 1 ('/oracle/redo/des/redo01.log') SIZE 400M,
GROUP 2 ('/oracle/redo/des/redo02.log') SIZE 400M,
GROUP 3 ('/oracle/redo/des/redo03.log') SIZE 400M,
GROUP 4 ('/oracle/redo/des/redo04.log') SIZE 400M,
GROUP 5 ('/oracle/redo/des/redo05.log') SIZE 400M,
GROUP 6 ('/oracle/redo/des/redo06.log') SIZE 400M,
GROUP 7 ('/oracle/redo/des/redo07.log') SIZE 400M,
GROUP 8 ('/oracle/redo/des/redo08.log') SIZE 400M,
GROUP 9 ('/oracle/redo/des/redo09.log') SIZE 400M,
GROUP 10 ('/oracle/redo/des/redo10.log') SIZE 400M,
GROUP 11 ('/oracle/redo/des/redo11.log') SIZE 400M,
GROUP 12 ('/oracle/redo/des/redo12.log') SIZE 400M RESETLOGS;
exec dbms_backup_restore.zerodbid(0);
shutdown immediate;
startup nomount pfile="/oracle/scripts/des//initdesTemp.ora";
Create controlfile reuse set database "des"
MAXINSTANCES 8
MAXLOGHISTORY 1
MAXLOGFILES 16
MAXLOGMEMBERS 3
MAXDATAFILES 500
Datafile 
'/oracle/data/des/system01.dbf',
'/oracle/data/des/undotbs01.dbf',
'/oracle/data/des/sysaux01.dbf',
'/oracle/data/des/users01.dbf'
LOGFILE GROUP 1 ('/oracle/redo/des/redo01.log') SIZE 400M,
GROUP 2 ('/oracle/redo/des/redo02.log') SIZE 400M,
GROUP 3 ('/oracle/redo/des/redo03.log') SIZE 400M,
GROUP 4 ('/oracle/redo/des/redo04.log') SIZE 400M,
GROUP 5 ('/oracle/redo/des/redo05.log') SIZE 400M,
GROUP 6 ('/oracle/redo/des/redo06.log') SIZE 400M,
GROUP 7 ('/oracle/redo/des/redo07.log') SIZE 400M,
GROUP 8 ('/oracle/redo/des/redo08.log') SIZE 400M,
GROUP 9 ('/oracle/redo/des/redo09.log') SIZE 400M,
GROUP 10 ('/oracle/redo/des/redo10.log') SIZE 400M,
GROUP 11 ('/oracle/redo/des/redo11.log') SIZE 400M,
GROUP 12 ('/oracle/redo/des/redo12.log') SIZE 400M RESETLOGS;
alter system enable restricted session;
alter database "des" open resetlogs;
alter database rename global_name to "des";
ALTER TABLESPACE TEMP ADD TEMPFILE '/oracle/data/des/temp01.dbf' SIZE 20480K REUSE AUTOEXTEND ON NEXT 640K MAXSIZE UNLIMITED;
select tablespace_name from dba_tablespaces where tablespace_name='USERS';
select sid, program, serial#, username from v$session;
alter database character set INTERNAL_CONVERT WE8ISO8859P1;
alter database national character set INTERNAL_CONVERT AL16UTF16;
alter user sys identified by "&&sysPassword";
alter user system identified by "&&systemPassword";
alter system disable restricted session;
