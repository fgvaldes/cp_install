#!/bin/bash
#
# Script for updating the values in photoz column
# using the values in photo_z table
# in dc5_coadd_objects_tag, the photoz_id range is 234857652 to 319334718
# script1 from 234800000 to 260000000
# script2 from 260000000 to 280000000
# script3 from 280000000 to 300000000
# script4 from 300000000 to 319400000
# Dated : Apr. 08, 2010

DIR='/oracle/scripts/des'
INCREMENT=100000
MINID=234800000
MAXID=234900000
STOPID=260000000
SQLFILE='/oracle/scripts/des/upd_photoz_1.sql'
source $DIR/des.bash

while [ $MINID -lt $STOPID ] ; do 
  echo "update dc5_coadd_objects_tag a " > $SQLFILE
  echo "set a.photoz = (select b.photoz " >> $SQLFILE
  echo "from photo_z b " >> $SQLFILE
  echo "where b.id = a.photoz_id) " >> $SQLFILE
  echo "where photoz_id >= $MINID " >> $SQLFILE
  echo "and   photoz_id < $MAXID; " >> $SQLFILE
  echo "commit; " >> $SQLFILE
  echo "exit; " >> $SQLFILE
  cat $SQLFILE
  sqlplus des_admin/passwd @$SQLFILE
  MINID=$MAXID
  MAXID=`expr $MINID + $INCREMENT`
done

exit
