     select a.sql_id, max(substr(to_char(a.sql_text),1,500)),
            max(c.executions_total), max(c.loads_total),
            max(c.cpu_time_total),
            max(c.elapsed_time_total), max(c.iowait_total)
      rom dba_hist_sqltext a,
          dba_hist_active_sess_history b,
          dba_hist_sqlstat c,
          dba_users d
      here a.sql_id = b.sql_id
     and   a.sql_id = c.sql_id
     and   b.user_id = d.user_id
     and   d.default_tablespace = 'DES'
     group by a.sql_id
