#!/bin/bash
export ORACLE_BASE=/oracle
export ORACLE_HOME=/oracle/11.1.0
export ORACLE_SID=rcat
export ORACLE_ASK=NO
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/lib:$PATH
echo "shutdown immediate;" > /tmp/qry.$$
echo "exit;" >> /tmp/qry.$$
sqlplus / as sysdba @/tmp/qry.$$
rm /tmp/qry.$$
lsnrctl stop
exit
