-- log in as sys to do this
create view des_active_sessions
as select Logon_time, Username database_user, sid db_system_id, osuser os_user, process os_processid, machine, Program, Module
from v$session
where schemaname IN ('DES_ADMIN','PIPELINE');

create public synonym des_active_sessions for sys.des_active_sessions;

grant all on des_active_sessions to public;
