\e to edit the last sql statement.
\q to quit.
\g to run.
\dt to show tables in the current database
\d table_name to show column definition and indexes for table_name
\i script_file to execute a scripts.
or
psql -f script_file db_name

-- find the postgreSQl version
select version();

-- do a backup
pg_dumpall -U postgres > backupMMDDYYYY.sql

-- do a backup and gzip the output file
pg_dumpall -U postgres | gzip > backupMMDDYYYY.sql

-- do a backup and split to 5G files
pg_dumpall -U postgres | split -b 20000000000 - backupMMDDYYYY

-- do a restore
psql -f backupMMDDYYYY.sql template1

-- do a restore from gzipped file
gunzip -c backupMMDDYYYY.sql | psql postgres

-- do a restore from split files
cat backupMMDDYYYY* | psql postgres


-- startup the database
/usr/local/pgsql/bin/postmaster -D /usr/local/pgsql/data > logfile 2>&1 &

-- shutdown the database
ps -ef | grep pgsql
kill -15 nnnn
kill -INT nnnn


select table_name from information_schema.tables where table_schema = 'public';

select * from information_schema.tables where table_name = 'xxxxxx';

select datname from pg_database;

select column_name from information_schema.columns where column_name = 'xxxxxx';

select table_name, column_name
from information_schema.columns
where table_name in ('archive_sites', 'catalog')
order by table_name, ordinal_position;

-- create a role for des_writer
create role des_writer nocreatedb nocreaterole nocreateuser inherit nologin;

-- create a role for des_reader
create role des_reader nocreatedb nocreaterole nocreateuser inherit nologin;

-- grant privilege to a role
grant all privileges on zeropoint to des_writer;

-- create a user belong to the role of des_writer
create user pipeline inherit login in role des_writer;

-- create a user belong to the role of des_reader
create user pipeline inherit login in role des_reader;

-- drop primary key
alter table dc4_truth drop constraint dc4_truth_pkey;

-- drop an index
drop index dc4truth_ra_dec_idx;

-- run ora2pq program (first modify ora2pg.conf)
perl ora2pg.pl ora2pg.conf > /dev/null 2>&1 &

-- carry out the command and show the acture runtime
des=# EXPLAIN ANALYZE full_query_statement;

-- display the execution plan
des=# EXPLAIN full_query_statement;

-- to get estimate row count from database catalog
des=# select reltuples from pg_class where relname='table_name';

-- to rename a table
des=# alter table old_table rename to new_table;

-- to add a new column to a table
des=# alter table table_name add column column_name date_type;

-- to drop a column from a table
des=# alter table table_name drop column column_name;

-- data type equivalence
oracle: varchar2 	postgres: varchar
oracle: date		postgres: timestamp
