# /usr/bin/perl

#ankit

#/home/ankitc/test/Ankit/devel/Database/trunk/scripts/maintenance/createMV_files_dr002.sql


use strict;
use warnings;
use FileHandle;
use DBI;
use DBD::Oracle;
use Switch;

my $table_name = $ARGV[0];
my $fp_insert;
my $fp_select;
my $fp_where;
my @lines;
my $sqlInsert_red;# = join("",@lines);
my $sqlInsert_red_cat;# = join("",@lines);
my $sqlInsert_remap;# = join("",@lines);
my $sqlInsert_red_bkg;# = join("",@lines);
my $sqlInsert_src;# = join("",@lines);
my $sqlInsert_other;# = join("",@lines);
my $sqlInsert_coadd; # = join("",@lines);
my $sqlInsert_coadd_cat; # = join("",@lines);
my $sqlInsert_coadd_det;# = join("",@lines);

my $sqlSelect_red;# = join("",@lines);
my $sqlSelect_red_cat;# = join("",@lines);
my $sqlSelect_remap;# = join("",@lines);
my $sqlSelect_red_bkg;# = join("",@lines);
my $sqlSelect_src;# = join("",@lines);
my $sqlSelect_other;# = join("",@lines);
my $sqlSelect_coadd; # = join("",@lines);
my $sqlSelect_coadd_cat; # = join("",@lines);
my $sqlSelect_coadd_det;# = join("",@lines);


my $sqlWhere_red;# = join("",@lines);
my $sqlWhere_red_cat;# = join("",@lines);
my $sqlWhere_remap;# = join("",@lines);
my $sqlWhere_red_bkg;# = join("",@lines);
my $sqlWhere_src;# = join("",@lines);
my $sqlWhere_other;# = join("",@lines);
my $sqlWhere_coadd; # = join("",@lines);
my $sqlWhere_coadd_cat; # = join("",@lines);
my $sqlWhere_coadd_det;# = join("",@lines);


 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_redMV.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_red = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_redMV.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_red = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_redMV.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_red = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}

my $fileSQL_red = 'insert into '.$table_name.' '.$sqlInsert_red." ".$sqlSelect_red;

 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_red_catMV.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_red_cat = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_red_catMV.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_red_cat = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_red_catMV.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_red_cat = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}


my $fileSQL_red_cat = 'insert into '.$table_name.' '.$sqlInsert_red_cat." ".$sqlSelect_red_cat;



 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_remapMV.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_remap = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_remapMV.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_remap = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_remapMV.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_remap = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}


my $fileSQL_remap = 'insert into '.$table_name.' '.$sqlInsert_remap." ".$sqlSelect_remap;

 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_red_bkg.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_red_bkg = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_red_bkg.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_red_bkg = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_red_bkg.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_red_bkg = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}

my $fileSQL_red_bkg = 'insert into '.$table_name.' '.$sqlInsert_red_bkg." ".$sqlSelect_red_bkg;




 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_src.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_src = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_src.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_src = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_src.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_src = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}


my $fileSQL_src = 'insert into '.$table_name.' '.$sqlInsert_src." ".$sqlSelect_src;

 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_other.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_other = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_other.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_other = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_other.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_other = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}

my $fileSQL_Other = 'insert into '.$table_name.' '.$sqlInsert_other." ".$sqlSelect_other;


 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_coadd.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_coadd = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_coadd.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_coadd = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_coadd.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_coadd = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}

my $fileSQL_coadd = 'insert into '.$table_name.' '.$sqlInsert_coadd." ".$sqlSelect_coadd;





 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_coadd_cat.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_coadd_cat = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_coadd_cat.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_coadd_cat = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_coadd_cat.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_coadd_cat = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}

my $fileSQL_coadd_cat = 'insert into '.$table_name.' '.$sqlInsert_coadd_cat." ".$sqlSelect_coadd_cat;



 $fp_insert = FileHandle->new('maintenance/sqlInsert_files_coadd_det.sql','r');
if(defined $fp_insert){
 @lines = <$fp_insert>;
 $sqlInsert_coadd_det = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

 $fp_select = FileHandle->new('maintenance/sqlSelect_files_coadd_det.sql','r');
if(defined $fp_select){
 @lines = <$fp_select>;
 $sqlSelect_coadd_det = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

 $fp_where = FileHandle->new('maintenance/sqlWhere_files_coadd_det.sql','r');
if(defined $fp_where){
 @lines = <$fp_where>;
 $sqlWhere_coadd_det = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}

my $fileSQL_coadd_det = 'insert into '.$table_name.' '.$sqlInsert_coadd_det." ".$sqlSelect_coadd_det;
############################################# INITIALIZATION OF DATABASES AND OTHER VARIABLES ########################################

my @lines;
my %queryParams;# = ('1','a','2','b');
my @newarr;
my $sourcefile;

# CONNECT TO THE DATABASE AND FIRE THE QUERY
my @driver_names = DBI->available_drivers;
my  %drivers      = DBI->installed_drivers;
my  @data_sources = DBI->data_sources('Oracle');

my $dsn = 'desdb.cosmology.illinois.edu';
my $user = 'des_admin';
my $password = 'deSadM1005';
my $pw = $password;

 $dsn = "DBI:Oracle:host=desdb.cosmology.illinois.edu;sid=des";

# PERL DBI CONNECT
 my $dbh = DBI->connect($dsn, $user, $pw,  { AutoCommit => 0 }) or print " \ncould not connect to the DB..";
############################################ INITIALIZATION OF DATABASES AND OTHER VARIABLES DONE########################################

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}



# create a materialized view: DONE FOR NOW: do not need to do this for this time

############################################### read data from the input file into an array ####################################################

	# the first param passed with the script is used as a file name
$sourcefile = $ARGV[1];

if($sourcefile eq '')
{
	die "File parameter not found! Please enter one...";
	print "\n should not see this!";
}
else
{
	print "\n\n file found, moving on... "; print $sourcefile;
}

print "\n continuing further... ";

# open the file and read it into a variable;

my $fp = FileHandle->new($sourcefile,'r');
if(defined $fp){
@lines = <$fp>;
undef $fp;
}
else
{
	print " could not open the file name: the error is: $!";
	die " exiting...";
}



############################################### read data from file complete ####################################################################


############################################ CREATE WHERE CLAUSE AND APPEND TO PARENT QUERY ########################################
my $i = 0;
my $WhereClause = '';
my $fileSQL_final = '';
my $val1;
my $val2;
foreach my $filelist ( @lines)
{
	print "\n new item: $filelist";
	@newarr = split (/ +/,$filelist);
	print "\n newarray $i for 0: $newarr[0]<--";
	print "\n newarray $i for 1: $newarr[1]<--";
	$i++;
	$val1 = $newarr[0];	
	$val2 = $newarr[1];	


######################################################################################

# WITH ROWNUM

#switch (trim($newarr[1])) {

#		case 'red'		{ print "\n red case \n"; $WhereClause = ' where loc.run = \''.trim($newarr[0]).'\' and   loc.filetype = \'red\' and   loc.id = img.id and   img.exposureid = exp.id and rownum <= 10' ; $fileSQL_final = $fileSQL_red.$WhereClause;}
#		case 'red_cat'	{ print "\n red_cat case \n"; $WhereClause = ' where loc.run = \''.trim($newarr[0]).'\' and   loc.filetype = \'red_cat\' and   loc.id = clg.id and   img.id = clg.parentid and   clg.exposureid = exp.id and rownum <= 10'; $fileSQL_final = $fileSQL_red_cat.$WhereClause; }
#		case 'remap'	{ print "\n remap case \n"; $WhereClause = ' where loc.run = \''.trim($newarr[0]).'\' and   loc.filetype = \'remap\' and   loc.id = img.id and   img.exposureid = exp.id and rownum <= 10'; $fileSQL_final = $fileSQL_remap.$WhereClause; }
#		case 'red_bkg'	{ print "\n red_bkg case \n"; $WhereClause = ' where loc.run = \''.trim($newarr[0]).'\' and   loc.filetype = \'red_bkg\' and   loc.id = img.id and   img.exposureid = exp.id and rownum <= 10'; $fileSQL_final = $fileSQL_red_bkg.$WhereClause; }
#		case 'src'	{ print "\n src case \n"; $WhereClause = ' where loc.nite = \''.trim($newarr[0]).'\' and   loc.filetype = \'src\' and   loc.id = exp.id and rownum <= 10'; $fileSQL_final = $fileSQL_src.$WhereClause;}
#		case /shapelet.*/	{ print "\n shapelet shear case \n"; $WhereClause = ' where loc.run = \''.trim($newarr[0]).'\' and   wl.wltype in (\'shapelet_shear\', \'shapelet_shpltall\', \'shapelet_psfmodel\')  and   loc.filetype in (\'shapelet_shear\', \'shapelet_shpltall\', \'shapelet_psfmodel\')  and   loc.filetype = wl.wltype and   loc.run = wl.run and   loc.id = wl.id and rownum <= 10';  $fileSQL_final = $fileSQL_Other.$WhereClause; }
#		case 'coadd'	{ print "\n coadd case \n"; $WhereClause = ' where loc.run =  \''.trim($newarr[0]).'\'  and   loc.filetype = \'coadd\' and   loc.id = cdd.id and rownum <= 10'; $fileSQL_final = $fileSQL_coadd.$WhereClause; }
#		case 'coadd_cat'	{ print "\n coadd_cat case \n"; $WhereClause = ' where loc.run =  \''.trim($newarr[0]).'\' and   loc.filetype = \'coadd_cat\' and   loc.id = clg.id and   clg.parentid = cdd.id and rownum <= 10';  $fileSQL_final = $fileSQL_coadd_cat.$WhereClause; }
#		case 'coadd_det'	{ print "\n coadd_det case \n"; $WhereClause = ' where loc.run =  \''.trim($newarr[0]).'\' and   loc.filetype = \'coadd_det\' and   loc.id = cdd.id and rownum <= 10'; $fileSQL_final = $fileSQL_coadd_det.$WhereClause; }
#		else		{ print "\n previous case not true for "; print $newarr[1]; print "\n"; }
#	}  

######################################################################################

	
	switch (trim($newarr[1])) {

		case 'red'		{ print "\n red case \n";
		 $WhereClause = $sqlWhere_red;
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_red.$WhereClause; }

		case 'red_cat'	{ print "\n red_cat case \n";
		 $WhereClause = $sqlWhere_red_cat;
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_red_cat.$WhereClause; }

		case 'remap'	{ print "\n remap case \n";
		 $WhereClause = $sqlWhere_remap;
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_remap.$WhereClause; }

		case 'red_bkg'	{ print "\n red_bkg case \n";
 		 $WhereClause = $sqlWhere_red_bkg;
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_red_bkg.$WhereClause; }

		case 'src'	{ print "\n src case \n"; 
		 $WhereClause = $sqlWhere_src; 
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_src.$WhereClause; }

		case /shapelet[_shear|_psfmodel|_shpltall]/	{ 
		 print "\n shapelet shear case \n"; 
		 $WhereClause = $sqlWhere_other; 
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_Other.$WhereClause; }

		case 'coadd'	{ print "\n coadd case \n";
 		 $WhereClause = $sqlWhere_coadd; 
		 $WhereClause =~ s/PARAM1/$val1/g;
		print $WhereClause;
		 $fileSQL_final = $fileSQL_coadd.$WhereClause; }

		case 'coadd_cat'	{ print "\n coadd_cat case \n";
 		 $WhereClause = $sqlWhere_coadd_cat; 
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_coadd_cat.$WhereClause; }

		case 'coadd_det'	{ print "\n coadd_det case \n";
 		 $WhereClause = $sqlWhere_coadd_det; 
		 $WhereClause =~ s/PARAM1/$val1/g;
		 $fileSQL_final = $fileSQL_coadd_det.$WhereClause; }
		else		{ print "\n previous case not true for "; print $newarr[1]; print "\n"; }
	}  
	
	print $fileSQL_final;
	my $stmt = $dbh->prepare($fileSQL_final)   or &displayError("Could not prepare queries aborting");
    $stmt->execute() or print "Found error ".$stmt->errstr; #&displayError(" Couldnt execute statement:  " .$stmt->errstr);
    my $rows_affected = $stmt->rows;
    print "\n\n rows affected: $rows_affected";
    if($rows_affected > 0)
    {
    	$dbh->commit;
    }
	#die '\n looping\n';
}

print "\n did u see this?? ";
die "\n hmm..";
exit;
print "\n must not see this";

########################################### CREATE WHERE CLAUSE DONE ########################################
=for
# iterate through the arguments
foreach  my $argnum (0 .. $#ARGV) {

 #  print "$ARGV[$argnum]\n";

}
=cut

# Create the sql query to parse

########################################### EXECUTE QUERY ######################################## 
print "\n\n\n\n\n\n going to execute now!! \n\n\n\n";
sleep 5;
die "Done for now";
exit;
die" bass.....";

#my $stmt = $dbh->prepare($fileSQL_final)   or &displayError("Could not prepare queries aborting");
 #$stmt->execute() or print "Found error ".$stmt->errstr; #&displayError(" Couldnt execute statement:  " .$stmt->errstr);
#while (my @row = $stmt->fetchrow_array() ) {
 #   print " new row: @row\n";
#}
#$dbh->commit;

=for
$dbh = DBI->connect($sid, $user, $pwd, "Oracle")
     or &displayError("Couldn't connect to database: " .DBI->errstr);

  # Prepare the SQL statement
  $stmt = $dbh->prepare(' INSERT INTO guestbook_entry(name, email, comments, commentdate) VALUES ('
                          . $nameValue .','. $emailValue. ','.$commentsValue. ',sysdate )')
    or &displayError("Could not prepare queries aborting");

 #Execute the above SQL statement
 $stmt->execute()
    or &displayError(" Couldnt execute statement:  " .$stmt->errstr);

 # Submit the changes
 $dbh->commit;

 # Close up the cursor
 $stmt->finish;

 # Disconnect from the database
 $dbh->disconnect;

=cut

#$sth = $dbh->prepare("SELECT foo, bar FROM table WHERE baz=?");

#$sth->execute( my $baz );
=for
  while (my @row = my$sth->fetchrow_array ) {
    print "@row\n";
  }
=cut
print "\n done! \n";


