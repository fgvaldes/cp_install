#!/bin/bash
#
# Parallel des offline backup script 2 (after manually shutdown database)

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/des'
DATADIR='/oracle/data/des'
DATADIR2='/oracle/data/des_temp'
DATADIR3='/oracle/data/partdes'
REDODIR='/oracle/redo/des'
DBSDIR='/oracle/database/11.1.0/dbs'
NETDIR='/oracle/database/11.1.0/network/admin'
BUDIR='/oracle/backup/des.'$TODAY
source $DIR/des.bash

echo `date`: DES offline backup for second set of data files is started >> $DIR/des_backup.log
cp -rp $DATADIR2 $BUDIR/.
cp -rp $DATADIR3 $BUDIR/.
cp -rp $REDODIR $BUDIR/.
cp -rp $DESDIR $BUDIR/.
cp -rp $NETDIR $BUDIR/.

echo `date`: DES database backup second set complete. >> $DIR/des_backup.log
exit
