select count(1)
from objects_2008 partition (par_20080918134848_20071002);

alter table des_admin.objects_2008 add partition PTMP_20100415105724_20061216 values ('TMP_20100415105724_20061216');

alter table des_admin.objects_2008 
drop partition par_20080918134848_20071002;

alter table des_admin.objects_2008 
exchange partition par_20080918134848_20071002
with table tmp_20080918134848_20071002
including indexes without validation update global indexes;

alter table des_admin.objects_2008 
truncate partition par_20080918134848_20071002
update global indexes;

alter table des_admin.objects_2008 
drop partition par_20080918134848_20071002
update global indexes;

-- num_rows only shows up after table is analysed
select partition_name, num_rows
from all_tab_partitions
where table_name = 'OBJECTS_2008';

-- drop the primary key of the temp table
alter table tmp_des20071031_des20071011b
drop constraint pk_des20071031_des20071011b;

-- drop the primary key of the main table
alter table objects_2008
drop constraint pk_objects_2008;

-- exchange a partition with an empty temp table
alter table part_objects exchange partition des20071031_des20071011b 
with table tmp_des20071031_des20071011b
excluding indexes update global indexes parallel 8;

-- drop the partition
alter table part_objects drop partition des20071031_des20071011b update global indexes parallel 4;

-- select rows from a partition
select imageid, object_id
from objects_2008 partition (par_20081029110020_20081005)
where rownum < 20;

