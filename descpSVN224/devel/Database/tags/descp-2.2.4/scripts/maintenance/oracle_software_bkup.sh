#!/bin/bash
#
# Oracle software backup before upgrade (after manually shutdown database)

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/des'
SWDIR='/oracle/11.1.0'
INVDIR='/oracle/oraInventory'
LOCALBINDIR='/usr/local/bin'
ETCDIR='/etc'
BUDIR='/oracle/backup/des.'$TODAY
#mkdir $BUDIR
#cp /dev/null $DIR/des_backup.log
source $DIR/des.bash

echo `date`: Oracle software backup is started >> $DIR/des_backup.log
cp -rp $SWDIR $BUDIR/.
cp -rp $INVDIR $BUDIR/.
mkdir $BUDIR/etc
cp -p $LOCALBINDIR/*ora* $BUDIR/etc/.
cp -p $ETCDIR/*ora* $BUDIR/etc/.

echo `date`: Oracle software backup complete. >> $DIR/des_backup.log
exit
