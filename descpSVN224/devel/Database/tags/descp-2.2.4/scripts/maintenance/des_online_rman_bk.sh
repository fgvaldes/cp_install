#!/bin/bash
#
# RMAN online Backup of DES database
# Date: Nov 20, 2006

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/des'
BUDIR='/data1/backup/des.'$TODAY
LOG=$DIR/des_online_rman_bk.log
cp /dev/null $LOG
mkdir $BUDIR
source $DIR/des.bash

cp /dev/null $DIR/des_online_rman_bk.cmd
echo 'run {' >> $DIR/des_online_rman_bk.cmd
echo 'allocate channel t1 type disk MAXPIECESIZE = 5G; ' >> $DIR/des_online_rman_bk.cmd
echo "backup database format '$BUDIR/DES_%U'; " >> $DIR/des_online_rman_bk.cmd
echo "backup current controlfile format '$BUDIR/CTRFl_%U'; " >> $DIR/des_online_rman_bk.cmd
echo 'release channel t1;} ' >> $DIR/des_online_rman_bk.cmd


rman target / nocatalog cmdfile=$DIR/des_online_rman_bk.cmd > $LOG

line_no=0
line_no=`grep 'ORA-' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"DES backup on desdb1 failed" des_oracle@ncsa.uiuc.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

line_no=0
line_no=`grep 'fail' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"DES backup on desdb1 failed" des_oracle@ncsa.uiuc.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

mail -s"DES backup on desdb1 is completed successfully" des_oracle@ncsa.uiuc.edu < $LOG
echo suceeded > $BUDIR/.status
exit

