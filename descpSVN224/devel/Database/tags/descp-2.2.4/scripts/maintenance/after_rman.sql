alter database recover managed standby database cancel;
alter database open read only;
alter database recover managed standby database disconnect using current logfile;
exit;
