select s.username, s.sid, s.serial#, s.program, n.name cpu, t.value
from v$session s, v$statname n, v$sesstat t
where s.username = 'PIPELINE'
and n.statistic# = t.statistic#
and n.name like '%cpu%'
and t.sid = s.sid
/
