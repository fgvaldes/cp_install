set echo on;
column component format A30

select component, current_size, min_size, max_size
from v$memory_dynamic_components
where current_size != 0;

-- this script is copied from the book Oracle Solid State Disk Tuning
column c1 heading 'Cache Size (m)' format 999,999,999,999
column c2 heading 'Buffers' format 999,999,999
column c3 heading 'Estd Phys|Read Factor' format 999.90
column c4 heading 'Estd Phys| Reads' format 999,999,999,999

select size_for_estimate c1,
       buffers_for_estimate c2,
       estd_physical_read_factor c3,
       estd_physical_reads c4
from v$db_cache_advice
where name = 'DEFAULT'
and   block_size = (select value from v$parameter
                    where name = 'db_block_size')
and   advice_status = 'ON';

set linesize 160
select * from v$memory_target_advice order by memory_size;

