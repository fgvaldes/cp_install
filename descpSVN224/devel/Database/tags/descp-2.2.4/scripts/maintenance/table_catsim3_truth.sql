-- run this script as des_admin
drop table catsim3_truth purge;
create table catsim3_truth(
catsim3_truth_id	number(11),
star_galaxy_id		number(10),
class			char(1),
ra			number(10,7),
dec			number(10,7),
u_mag			number(7,4),
g_mag			number(7,4),
r_mag			number(7,4),
i_mag			number(7,4),
z_mag			number(7,4),
htmid			number(16),
cx			number(10,6),
cy			number(10,6),
cz			number(10,6),
datafile_name		varchar2(40),
constraint pk_catsim3_truth primary key (catsim3_truth_id))
storage (initial 1M);

create public synonym catsim3_truth for des_admin.catsim3_truth;
grant all on catsim3_truth to des_writer;
grant select on catsim3_truth to des_reader;

create sequence catsim3_truth_seq cache 1000 start with 70000000 increment by 1;

create public synonym catsim3_truth_seq for des_admin.catsim3_truth_seq;
grant all on catsim3_truth_seq to des_writer;
grant select on catsim3_truth_seq to des_reader;

