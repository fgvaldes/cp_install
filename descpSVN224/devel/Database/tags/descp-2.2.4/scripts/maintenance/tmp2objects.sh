#!/bin/bash
#
# Creates dynamic sqlldr statements for all imsimGalaxy datafiles


DIR='/oracle/scripts/des'
outfile=$DIR/tmp2objects.log
infile=$DIR/tmp2objects.sql
cp /dev/null $outfile

source $DIR/des.bash

start_id=981000000;
while [ ${start_id} -le 1078000000 ]
do
 end_id=`expr $start_id + 1000000`
 cp /dev/null $infile
 echo "insert /*+ APPEND NOLOGGING */ " >> $infile
 echo "into objects " >> $infile
 echo "select * from tmp_objects " >> $infile
 echo "where object_id >= $start_id " >> $infile
 echo "and object_id < $end_id; " >> $infile
 echo "commit; " >> $infile
 echo "exit; " >> $infile
 cat $infile >> $outfile
 sqlplus des_admin/passwd @$infile
 echo `date` "finish loading $start_id and $end_id" >> $outfile
 echo " " >> $outfile
 start_id=$end_id
done

exit

