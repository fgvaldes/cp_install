-- This document summarizes the procedure we have done to restore and 
-- recover the Fermi production standby database.

-- Description of the problem: fmdes' file system for /oracle/data and 
-- /oracle/redo was corrupted and rebuilt. The database was offline for 3
-- months. The database needs full restore and recovery.

-- Step 1:
-- perform a full backup which includes all non-readonly tablespaces from
-- Jan. 14 - Jan. 18. All readonly tablespaces have been backed up in Dec.

-- Step 2:
-- copy all backup files (for readonly tablespaces and non-readonly tablespaces)
-- from desorch to Fermi

-- Step 3:
-- create all required directories for /oracle/data and /oracle/redo

-- Step 4:
-- copy all redo files and standby-redo files from desorch to Fermi

-- Step 5:
-- start listener
$ lsnrctl start

-- Step 6:
-- restore control file
setenv ORACLE_SID des 
$ sqlplus / as sysdba
SQL> startup nomount;
SQL> exit;
$ rman target / catalog rman/passwd@desar.cosmology.illinois.edu/rcat 
RMAN> change backup for db_unique_name stdes reset db_unique_name;
RMAN> restore controlfile;
RMAN> exit; 

-- Step 7
-- change database to the mount mode
SQL> alter database mount;
SQL> exit;

-- Step 8
-- Restore regular database backup files and readonly tablespace backup files: 
RMAN> restore tablespace 'DESDR' force;
RMAN> restore tablespace 'DESDR007’ force;
RMAN> restore tablespace 'DESDR008’ force;
RMAN> restore tablespace 'DES_TEMP’ force;
RMAN> restore tablespace 'SYSTEM’;
RMAN> restore tablespace 'SYSAUX’;
RMAN> restore tablespace 'DES’;
RMAN> restore tablespace 'USERS’;
RMAN> restore tablespace 'UNDOTBS1’;
-- since desdr009_01.dbf was created before backup, and desdr009_02.dbf
-- was created after backup, we cannot use "restore tablespace 'DESDR009' 
-- force". We have to restore by datafile instead.
RMAN> restore datafile '/oracle/data/desdr009/desdr009_01’;

-- Step 9
-- recover database
RMAN> recover database check readonly;
-- This step first applies the archived redo logs generated from Jan. 14 
-- to Jan. 17, and then register and apply all logs generated since Jan. 17.
-- The restore process was ended with an error due to unavailable of the
-- log which has not produced yet.  
RMAN> exit; 

-- Step 10
-- Shutdown database and Start database in mount mode and redo apply
$ sqlplus / as sysdba
SQL> shutdown immediate;
SQL> startup mount;
-- startup will flush the memory to avoid the wrong info in dictionary cache,
-- and will create temp files automatically
-- Temp files are not covered by RMAN. Temp files must be added to the temp 
-- tablespace either automatically by shutdown/restart or by SQL "alter 
-- tablespace temp add tempfile ....".
SQL> alter database recover managed standby database using current logfile disconnect from session;
 
-- Step 10
-- if everything is OK, switch the database to the read-only mode.
  SQL> alter database recover managed standby database cancel;
  SQL> alter database open read only;
  SQL> recover managed standby database disconnect using current logfile;










-- Step 1:
-- transfer the latest backup files from UIUC's stanby server to Fermi.
-- use symbolic links to make the location consistent for the backup files.
$ cd /decam-devdb/dbs_nas_backup01
$ mkdir backup
$ ln -s /decam-devdb/dbs_nas_backup01/backup /oracle/backup 
$ mkdir backup1
$ ln -s /decam-devdb/dbs_nas_backup01/backup1 /oracle/backup1

-- Step 2:
-- remove the previous backup
$ rm -rf /oracle/backup/dvlp.*
$ rm -rf /oracle/backup1/dvlp.*

-- Step 3:
$ scp -rp oracle@deslogin.cosmology.illinois.edu:/oracle/backup/dvlp.092610 /oracle/backup
$ scp -rp oracle@deslogin.cosmology.illinois.edu:/oracle/backup1/dvlp.092610 /oracle/backup1
$ scp -rp oracle@deslogin.cosmology.illinois.edu:/oracle/backup/readonly.061710 /oracle/backup

-- Step 4:
-- to stop Redo Apply, and shutdown the database
$ sqlplus / as sysdba
  SQL> alter database recover managed standby database cancel;
  SQL> shutdown immediate;

-- Step 5:
-- to start database in the nomount mode
  SQL> startup nomount;

-- Step 6
-- use RMAN to restore the control files
$ rman target / catalog rman/passwd@rcatd  
-- change the association of the backup
RMAN> change backup for db_unique_name stdvlp reset db_unique_name;
RMAN> restore controlfile;

-- Step 7:
-- change database into the mount mode
  SQL> alter database mount;

-- Step 8:
-- use RMAN to restore database files and readonly tablespace files
RMAN> restore database;
RMAN> restore tablespace 'DESDR';
RMAN> recover tablespace 'DESDR';
RMAN> recover database;
RMAN> exit;

-- Step 9:
-- start redo apply
$ sqlplus / as sysdba
  SQL> alter database recover managed standby database using current logfile disconnect from session;
  -- if everything goes OK, switch the database to the read-only mode.
  SQL> alter database recover managed standby database cancel;
  SQL> alter database open read only;
  SQL> recover managed standby database disconnect using current logfile;
  SQL> exit;

