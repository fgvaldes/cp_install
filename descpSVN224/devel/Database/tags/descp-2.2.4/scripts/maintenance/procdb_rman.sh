#!/bin/bash
#
# RMAN Backup without catalog for procdb
# Date: Feb. 18, 2010

TODAY=`date '+%m%d%y'`
DIR='/data/oracle/scripts/procdb'
BUDIR='/data/oracle/backup/procdb.'$TODAY
LOG=$DIR/procdb_rman.log
cp /dev/null $LOG
mkdir $BUDIR
source $DIR/procdb.bash

cp /dev/null $DIR/procdb_rman.cmd
echo 'run {' >> $DIR/procdb_rman.cmd
echo 'crosscheck backup device type disk; ' >> $DIR/procdb_rman.cmd
echo 'delete noprompt expired backup; '  >> $DIR/procdb_rman.cmd
echo 'crosscheck archivelog all; ' >> $DIR/procdb_rman.cmd
echo "delete noprompt expired archivelog all;} " >> $DIR/procdb_rman.cmd
echo 'run {' >> $DIR/procdb_rman.cmd
echo 'allocate channel t1 type disk MAXPIECESIZE = 5G; ' >> $DIR/procdb_rman.cmd
echo "backup database format '$BUDIR/PROCDB_%U'; " >> $DIR/procdb_rman.cmd
echo "backup current controlfile format '$BUDIR/CTRFl_%U'; " >> $DIR/procdb_rman.cmd
echo "backup archivelog all format '$BUDIR/ARC_%s_%p'; " >> $DIR/procdb_rman.cmd
echo 'release channel t1; } ' >> $DIR/procdb_rman.cmd

rman target / nocatalog cmdfile=$DIR/procdb_rman.cmd >> $LOG

line_no=0
line_no=`grep 'ORA-' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"PROCDB backup on desproc failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

line_no=0
line_no=`grep 'fail' $LOG | grep -v 'validation failed' | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"PROCDB backup on desproc failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

mail -s"PROCDB backup on desproc is completed successfully" desdm-db@cosmology.illinois.edu < $LOG
echo suceeded > $BUDIR/.status
exit

