run {
allocate channel t1 type disk MAXPIECESIZE = 5G; 
backup database format '/data1/backup/des.081908/PRDES_%U'; 
backup current controlfile for standby format '/data1/backup/des.081908/CTRFl_%U'; 
sql "alter system archive log current";
backup archivelog all format '/data1/backup/des.081908/ARC_%s_%p'; 
release channel t1;} 
