#!/bin/bash
#
# Parallel des offline backup script 1 (after manually shutdown database)

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/des'
DATADIR='/oracle/data/des'
DATADIR2='/oracle/data/des_temp'
DATADIR3='/oracle/data/partdes'
REDODIR='/oracle/redo/des'
DBSDIR='/oracle/database/11.1.0/dbs'
NETDIR='/oracle/database/11.1.0/network/admin'
BUDIR='/oracle/backup/des.'$TODAY
mkdir $BUDIR
cp /dev/null $DIR/des_backup.log
source $DIR/des.bash

echo `date`: DES offline backup for first set of data files is started >> $DIR/des_backup.log
cp -rp $DATADIR $BUDIR/.

echo `date`: DES database backup first set complete. >> $DIR/des_backup.log
mail -s"Offline backup successful" ycai@ncsa.uiuc.edu <$DIR/des_backup.log
exit
