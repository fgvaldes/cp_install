#!/bin/bash
#
# Script for updating the values in n_mag_model_$band columns
# using the values in mag_model_$band
#
# script1 from 234800000 to 260000000
# script2 from 260000000 to 280000000
# script2 from 280000000 to 300000000
# script2 from 300000000 to 319400000
# Dated : Apr. 06, 2010

DIR='/oracle/scripts/des'
INCREMENT=100000
#MINID=234800000
MINID=235000000
#MAXID=234900000
MAXID=235100000
#STOPID=235000000
STOPID=260000000
BASEFILE='/oracle/scripts/des/ins_dc5tag_base.sql'
SQLFILE='/oracle/scripts/des/ins_dc5tag_run.sql'
source $DIR/des.bash

while [ $MINID -lt $STOPID ] ; do 
  cp $BASEFILE $SQLFILE
  echo "where coadd_objects_id >= $MINID " >> $SQLFILE
  echo "and   coadd_objects_id < $MAXID; " >> $SQLFILE
  echo "commit; " >> $SQLFILE
  echo "exit; " >> $SQLFILE
  cat $SQLFILE
  sqlplus des_admin/passwd @$SQLFILE
  MINID=$MAXID
  MAXID=`expr $MINID + $INCREMENT`
done

exit
