* to append the output of rman to a file
$ rman target / log full_path_of_file append

* to show all configuration setting
$ rman target / catalog rman/passwd@rcat 
RMAN> show all;

* to use parallalism 4 for backup process
$ rman target / catalog rman/passwd@rcat 
RAMN> configure device type disk parallelism 4;

* to clear a configuration setting
$ rman target / catalog rman/passwd@rcat 
RMAN> configure archivelog deletion policy clear;

* to back up database and archivelog
$ rman target / catalog rman/passwd@rcat cmdfile=full_path_cmdfile log full_path_of_file append
  The cmdfile will look like:
  run {
    allocate channel t1 type disk MAXPIECESIZE = 5G;
    backup database format '$BUDIR/DES_%U'; 
    backup current controlfile format '$BUDIR/CTRFl_%U';
    backup archivelog all format '$BUDIR/ARC_%s_%p'; 
    release channel t1;
  } 

* to list backup information
$ rman target / catalog rman/passwd@rcat
RMAN> list backup;
RMAN> list backup summary;
RMAN> list archivelog all;
RMAN> list db_unique_name all;
RMAN> list restore point all;
RMAN> list backup of database;
RMAN> list datafilecopy '/oracle/oradata/des/des_01.dbf';

* to report backup information
$ rman target / catalog rman/passwd@rcat
RMAN> report need backup;
RMAN> report unrecoverable;
RMAN> crosscheck backup device type disk;
RMAN> delete expired backup; -- against disk_removed backup files
RMAN> report obsolete;
RMAN> delete obsolete; -- against obsoleted files up to the last backup
RMAN> delete noprompt obsolete recovery window of 21 days;
RMAN> list expired archivelog all;
RMAN> crosscheck archivelog all;
RMAN> delete expired archivelog all;
RMAN> delete archivelog all;
RMAN> delete archivelog all completed before 'sysdate - 21';

* to resync catalog 
$ rman target / catalog rman/passwd@rcat
RMAN> resync catalog;

* Recovery scenarios
* Recovery from loss of datafiles on the primary database
-- on primary:
-- if database is open, query v$recover_file to find which datafiles 
-- need to be recovered.
sql> alter database datafile 'full_path_filename1' offline;
sql> alter database datafile 'full_path_filename2' offline;
-- copy the latest backup done on standby to primary on the same path.
$ rman target / catalog rman/passwd@rcat
-- change the association of the backups
RMAN> change backup for db_unique_name stdes reset db_unique_name;
RMAN> restore datafile 'full_path_filename1', 'full_path_filename2';
RMAN> recover datafile 'full_path_filename1', 'full_path_filename2';
-- this step will read through all archived logs since last backup.
sql> alter database datafile 'full_path_filename1' online;
sql> alter database datafile 'full_path_filename2' online;

* Recovery a tablespace on the primary database
$ rman target / catalog rman/passwd@rcat
RMAN> sql 'alter tablespace tablespace_name1 offline';
RMAN> sql 'alter tablespace tablespace_name2 offline';
RMAN> restore tablespace tablespace_name1, tablespace_name2;
RMAN> recover tablespace tablespace_name1, tablespace_name2;
RMAN> sql 'alter tablespace tablespace_name1 online';
RMAN> sql 'alter tablespace tablespace_name2 online';

* Recovery from loss of datafiles on the standby database
on standby:
$ sqlplus -- to stop Redo Apply
$ sqlplus -- if database is in read-only mode, shutdown the database and restart
  in the mount mode.
$ rman target / catalog rman/passwd@rcat
RMAN> restore datafile full_path_filename1, full_path_filename2;
RMAN> recover database;
$ sqlplus -- start active Redo Apply
          -- switch logfile from primary database, wait the redo apply finish
          -- stop redo apply
          -- alter database open read only
          -- start active redo apply

* Recovery a tablespace on the standby database
$ sqlplus -- to stop active Redo Apply
$ sqlplus -- if database is in read-only mode, shutdown the database and restart
  in the mount mode.
$ rman target / catalog rman/passwd@rcat
RMAN> restore tablespace tablespace_name1, tablespace_name2;
RMAN> recover database;
$ sqlplus -- start active Redo Apply
          -- switch logfile from primary database, wait the redo apply finish
          -- stop redo apply
          -- alter database open read only
          -- start active redo apply

* Recovery from loss of a standby control file (database has been offlined)
-- if still have one copy of control file is good
$ sqlplus -- shutdown database abort if database is still running
$ cp good_ctrl_file bad_ctrl_file -- replace the bad control file
$ sqlplus -- startup mount
          -- start active Redo Apply
          -- switch logfile from primary database, wait the redo apply finish
          -- stop redo apply
          -- alter database open read only
          -- start active redo apply
-- if no good copy of control file on standby
$ rman target / catalog rman/passwd@rcat
RMAN> restore controlfile;
RMAN> recover database;

* Recovery from loss of a primary control file (database has been offlined)
$ rman target / catalog rman/passwd@rcat
RMAN> restore controlfile;
RMAN> recover database;

* Recovery from Loss of an Online Redo Log File on primary
-- if the loss is permanent, a new member can be added to a group
sql> alter database add logfile member 'full_path_filename' resue to group N;
-- if all members of an inactive group that has been archived are lost, 
   the group can be dropped and re-created.
-- if lost of all online log members for the current active group, or an 
   inactive group which has not yet been archived, you must fail over to the 
   standby database, or do incomplete recovery.

* Incomplete recovery of the primary database
-- on the primary server, identify the time or scn at which all the data 
   in the database is known to be good.
-- using the time or SCN, run RMAN and then open the database with the 
   RESETLOGS option (after connecting to catalog database and primary instance
   that is in MOUNT state):
RMAN>   RUN
   {
     set until time ('12-04-2008 15:30','mm-dd-yyyy hh24:mi');
     restore database;
     recover database;
   }
sql> alter database open resetlogs;
-- After this process, all standby database instances must be reestablished
-- in the Data Guard configuration.   

* Whenever the database structure is changed, ex. add a new datafile, the
  backup using catalog will get the error like this:
  RMAN-00571: ===========================================================
  RMAN-00569: =============== ERROR MESSAGE STACK FOLLOWS ===============
  RMAN-00571: ===========================================================
  RMAN-03002: failure of allocate command at 01/04/2009 01:36:07
  RMAN-03014: implicit resync of recovery catalog failed
  RMAN-03009: failure of partial resync command on default channel at 01/04/2009 01:36:07
  RMAN-20079: full resync from primary database is not done

  To fix the problem (from the standby database):
  rman target / catalog rman/rmanpass@rcat
  RMAN> resync catalog;
  
* Whenever the database structure is changed, ex. add or drop a datafile, add
  or drop a tablespace the backup using catalog on standby will get the error 
  like this:
  RMAN-00571: ===========================================================
  RMAN-00569: =============== ERROR MESSAGE STACK FOLLOWS ===============
  RMAN-00571: ===========================================================
  RMAN-03009: failure of resync command on default channel at 03/08/2009 01:46:09
  RMAN-20079: full resync from primary database is not done

  To fix the problem (from the primary database):

  rman target / catalog rman/rmanpass@rcat
  RMAN> resync catalog;

  Then from the standby database: 

  rman target / catalog rman/rmanpass@rcat
  RMAN> resync catalog;

* Check which file need to recover
  select * from v$recover_file;

* To see which datafiles and tablespaces are in need of recovery
  select r.file# as df#, d.name as df_name, t.name as tbsp_name
         d.status, r.error, r.change#, r.time
  from v$recover_file r, v$datafile d, v$tablespace t
  where t.ts# = d.ts#
  and   d.file# = r.file#;

* to exclude some tablespaces from the full database backup. This command
  must be executed while connect rman to catalog from the primary database.
  RMAN>CONFIGURE EXLUDE FOR TABLESPACE 'DESDR';
  RMAN>CONFIGURE EXLUDE FOR TABLESPACE 'DESDR007';

* to clear the exclution on tablespaces from the full database backup. 
  This command must be executed while connect rman to catalog from 
  the primary database.
  RMAN>CONFIGURE EXLUDE FOR TABLESPACE 'DESDR' clear;
  RMAN>CONFIGURE EXLUDE FOR TABLESPACE 'DESDR007' clear;

  
