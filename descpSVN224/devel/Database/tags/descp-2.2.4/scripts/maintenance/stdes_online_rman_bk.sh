#!/bin/bash
#
# RMAN online Backup run from the standby database
# Date: Aug. 27, 2008

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/des'
BUDIR='/oracle/backup/des.'$TODAY
LOG=$DIR/stdes_online_rman_bk.log
cp /dev/null $LOG
mkdir $BUDIR
source $DIR/des.bash

sqlplus / as sysdba @$DIR/before_rman.sql
echo 'database is shutdown and remounted.' > $LOG
cp /dev/null $DIR/stdes_online_rman_bk.cmd
echo 'run {' >> $DIR/stdes_online_rman_bk.cmd
echo 'allocate channel t1 type disk MAXPIECESIZE = 5G; ' >> $DIR/stdes_online_rman_bk.cmd
echo "backup database format '$BUDIR/DES_%U'; " >> $DIR/stdes_online_rman_bk.cmd
echo "backup current controlfile format '$BUDIR/CTRFl_%U'; " >> $DIR/stdes_online_rman_bk.cmd
echo 'release channel t1;} ' >> $DIR/stdes_online_rman_bk.cmd


rman target / nocatalog cmdfile=$DIR/stdes_online_rman_bk.cmd >> $LOG

sqlplus / as sysdba @$DIR/after_rman.sql
echo 'database is reopened.' >> $LOG

line_no=0
line_no=`grep 'ORA-' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"DES backup on des1 failed" des_oracle@ncsa.uiuc.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

line_no=0
line_no=`grep 'fail' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"DES backup on des1 failed" des_oracle@ncsa.uiuc.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

mail -s"DES backup on des1 is completed successfully" des_oracle@ncsa.uiuc.edu < $LOG
echo suceeded > $BUDIR/.status
exit

