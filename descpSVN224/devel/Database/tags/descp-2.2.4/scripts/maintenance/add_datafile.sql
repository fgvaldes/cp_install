connect / as sysdba
alter tablespace des
add datafile '/oracle/data/des/des_26.dbf'
size 1000M autoextend on next 500M maxsize unlimited;
exit
