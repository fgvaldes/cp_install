#!/bin/bash
#
# Script for populating the column redshift in dc4_truth
# using the values in table dc4_truth_redshift
#
# Dated : Feb. 19, 2009

DIR='/oracle/scripts/des/load_dc4_redshift'
TBLNAME='dc4_truth'
INCREMENT=10000
MINID=1
MAXID=10000
STOPID=30000000
SQLFILE='/oracle/scripts/des/load_dc4_redshift/upd_dc4_truth.sql'
source $DIR/des.bash

while [ $MINID -lt $STOPID ] ; do 
  cp /dev/null $SQLFILE
  echo "update dc4_truth a " >> $SQLFILE
  echo "set a.redshift = (select b.redshift " >> $SQLFILE
  echo "from dc4_truth_redshift b " >> $SQLFILE
  echo "where b.star_galaxy_id = a.star_galaxy_id " >> $SQLFILE
  echo "and   b.datafile_name = a.datafile_name) " >> $SQLFILE
  echo "where a.dc4_truth_id >= $MINID " >> $SQLFILE
  echo "and   a.dc4_truth_id < $MAXID; " >> $SQLFILE
  echo "commit; " >> $SQLFILE
  echo "exit; " >> $SQLFILE
  cat $SQLFILE
  sqlplus des_admin/passwd @$SQLFILE
  MINID=$MAXID
  MAXID=`expr $MINID + $INCREMENT`
done

exit
