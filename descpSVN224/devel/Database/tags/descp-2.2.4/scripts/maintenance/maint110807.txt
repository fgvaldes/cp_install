-- create a new data file for pipeline's temporary table
mkdir /data1/oracle/oradata/des
sql>alter tablespace des_temp
    add datafile '/data1/oracle/oradata/des/des_test_02.dbf'
    size 1000m autoextend on next 500m maxsize unlimited;
sql>alter user pipeline
    default tablespace des_temp;
sql>alter user pipeline identified by dc01user
    default tablespace des_temp temporary tablespace temp
    quota unlimited on des_temp
    quota unlimited on des account unlock;
sql> shutdown immediate;
cp /oracle/oradata/des/des_test_01.dbf /data1/oracle/oradata/des/.
sql> startup mount;
sql> alter database rename file '/oracle/oradata/des/des_test_01.dbf' to
     '/data1/oracle/oradata/des/des_test_01.dbf';
sql> alter database open;

-- move redo logs to new location.
mkdir /data1/oracle/redo/des
sql> shutdown immediate;
cp /oracle/redo/des/redo*.log /data1/oracle/redo/des/.
sql> startup mount;
sql> alter database rename file '/oracle/redo/des/redo01.log' to
     '/data1/oracle/redo/des/redo01.log';
sql> alter database rename file '/oracle/redo/des/redo02.log' to
     '/data1/oracle/redo/des/redo02.log';
sql> alter database rename file '/oracle/redo/des/redo03.log' to
     '/data1/oracle/redo/des/redo03.log';
sql> alter database rename file '/oracle/redo/des/redo04.log' to
     '/data1/oracle/redo/des/redo04.log';
sql> alter database rename file '/oracle/redo/des/redo05.log' to
     '/data1/oracle/redo/des/redo05.log';
sql> alter database rename file '/oracle/redo/des/redo06.log' to
     '/data1/oracle/redo/des/redo06.log';
sql> alter database open;

-- Adjust database parameter: db_writer_processes in SPFile
at the page: em/Administration/All Initialization Parameters/
change db_writer_processes from 1 to 4, and save to SPFile.

-- Enable database archive
mkdir /data1/oracle/archive/des
at the page: em/Administration/All Initialization Parameters/
change log_archive_dest_1 to location=/data1/oracle/archive/des
change log_archive_start to TRUE
sql> shutdown immediate;
sql> startup mount;
sql> alter database archivelog;
sql> alter database open;
sql> alter system switch logfile; // this command is to test archive process

-- Clean up if every thing is OK
rm /oracle/oradata/des/redo*
rm /oracle/redo/des/redo*
rm /oracle/oradata/des_test_01.dbf



