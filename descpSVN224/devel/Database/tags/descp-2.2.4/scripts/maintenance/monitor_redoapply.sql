set pagesize 1000
select sequence#, status, archived, applied, first_time, next_time
from v$archived_log
where applied = 'NO'
order by sequence#;
