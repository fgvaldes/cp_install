--DROP TABLE objects CASCADE CONSTRAINTS PURGE;
--DROP TABLE test_des_stripe82_stds CASCADE CONSTRAINTS PURGE;
--DROP TABLE des_testobs CASCADE CONSTRAINTS PURGE;
--DROP TABLE dco_truth CASCADE CONSTRAINTS PURGE;
--DROP TABLE usno_b CASCADE CONSTRAINTS PURGE;


-- Table: Objects

CREATE TABLE Objects(
  OBJECT_ID	Number(11,0) NOT NULL,
  EQUINOX	Number(6,2),
  BAND	        Varchar2(10),
  HTMID	        NUMBER(16,0),
  CX		Number(10,6),
  CY		Number(10,6),
  CZ		Number(10,6),
  ParentID	Number(11,0),
  SoftID	Number(4,0),
  ImageID	Number(9,0),
  Zeropoint	Number(7,4),
  ERRZeropoint	Number(7,4),
  ZeropointID	Number(10,0),
  OBJECT_NUMBER	Number(6,0),
  MAG_AUTO	Number(7,4),
  MAGERR_AUTO	Number(7,4),
  MAG_APER_1	Number(7,4),
  MAGERR_APER_1	Number(7,4),
  MAG_APER_2	Number(7,4),
  MAGERR_APER_2	Number(7,4),
  MAG_APER_3	Number(7,4),
  MAGERR_APER_3	Number(7,4),
  MAG_APER_4	Number(7,4),
  MAGERR_APER_4	Number(7,4),
  MAG_APER_5	Number(7,4),
  MAGERR_APER_5	Number(7,4),
  MAG_APER_6	Number(7,4),
  MAGERR_APER_6	Number(7,4),
  ALPHA_J2000	Number(8,5),
  DELTA_J2000	Number(8,5),
  ALPHAPEAK_J2000	Number(8,5),
  DELTAPEAK_J2000	Number(8,5),
  X2_WORLD	Number(24,20),
  ERRX2_WORLD	Number(24,20),
  Y2_WORLD	Number(24,20),
  ERRY2_WORLD	Number(24,20),
  XY_WORLD	Number(24,20),
  ERRXY_WORLD	Number(24,20),
  THRESHOLD	Number(6,2),
  X_IMAGE	Number(6,2),
  Y_IMAGE	Number(6,2),
  XMIN_IMAGE	Number(4,0),
  YMIN_IMAGE	Number(4,0),
  XMAX_IMAGE	Number(4,0),
  YMAX_IMAGE	Number(4,0),
  X2_IMAGE	Number(24,20),
  ERRX2_IMAGE	Number(24,20),
  Y2_IMAGE	Number(24,20),
  ERRY2_IMAGE	Number(24,20),
  XY_IMAGE	Number(24,20),
  ERRXY_IMAGE	Number(24,20),
  A_IMAGE	Number(6,2),
  ERRA_IMAGE	Number(6,2),
  B_IMAGE	Number(6,2),
  ERRB_IMAGE	Number(6,2),
  THETA_IMAGE	Number(4,2),
  ERRTHETA_IMAGE	Number(4,2),
  ELLIPTICITY	Number(5,4),
  CLASS_STAR	Number(3,2),
  FLAGS		Number(3,0),
  CONSTRAINT pk_objects PRIMARY KEY (object_id))
  storage (initial 100M) ;

drop sequence objects_seq;
create sequence objects_seq nocache start with 1 increment by 1;

create index idx_htmid on objects(htmid)
nologging
tablespace des;

create index idx_htmid_truth on dc0_truth(htmid)
parallel 4
nologging
tablespace des;


/* TABLE: test_des_stripe82_stds */

CREATE TABLE test_des_stripe82_stds(
  name  varchar2(40) NOT NULL,
  radeg  number(10,6),
  decdeg        number(10,6),
  stdmag_u      number(10,6),
  stdmag_g      number(10,6),
  stdmag_r      number(10,6),
  stdmag_i      number(10,6),
  stdmag_z      number(10,6),
  stdmagerr_u   number(10,6),
  stdmagerr_g   number(10,6),
  stdmagerr_r   number(10,6),
  stdmagerr_i   number(10,6),
  stdmagerr_z   number(10,6),
  nobs_u                number(10,6),
  nobs_g                number(10,6),
  nobs_r                number(10,6),
  nobs_i                number(10,6),
  nobs_z                number(10,6),
  CONSTRAINT pk_test_des_stripe82_stds PRIMARY KEY (name))
    storage (initial 10M);


/* TABLE: DES_STRIPE82_STDS_V1
 Created Sept 28th, Requested by Douglas */

CREATE TABLE des_stripe82_stds_v1(
  stripe82_id number(10),
  name  varchar2(40) NOT NULL,
  radeg  number(10,6),
  decdeg        number(10,6),
  stdmag_u      number(10,6),
  stdmag_g      number(10,6),
  stdmag_r      number(10,6),
  stdmag_i      number(10,6),
  stdmag_z      number(10,6),
  stdmagerr_u   number(10,6),
  stdmagerr_g   number(10,6),
  stdmagerr_r   number(10,6),
  stdmagerr_i   number(10,6),
  stdmagerr_z   number(10,6),
  nobs_u                number(10,6),
  nobs_g                number(10,6),
  nobs_r                number(10,6),
  nobs_i                number(10,6),
  nobs_z                number(10,6),
  CONSTRAINT pk_des_stripe82_stds_v1 PRIMARY KEY (stripe82_id))
    storage (initial 10M);

drop sequence des_stripe82_stds_v1_seq;
create sequence des_stripe82_stds_v1_seq nocache start with 1 increment by 1;


/* Table: DES_TESTOBS */

CREATE TABLE DES_testobs(
  object_id     Number(12),
  ra	        Number(9,6),
  dec	        Number(9,6),
  mjd	        Number(15,10),
  ccd	        Number(2),
  exptime	Number(3),
  airmass	Number(3,2),
  filter	varchar2(8),
  counts1	Number(10,4),
  counts2	Number(10,4),
  counts3	Number(10,4),
  saturated     Number(2),
  a             Number(3,1),
  k             Number(4,2),
  rms           Number(4,2),
  stdmag        Number(8,4),
  instmag       Number(8,4),
  CONSTRAINT pk_des_testobs PRIMARY KEY (object_id))
  storage (initial 1M) ;
  
drop sequence des_testobs_seq;
create sequence des_testobs_seq nocache start with 1 increment by 1;


-- Table: Files (created Sept29,05)
-- Table is updated below with new column definitions

CREATE TABLE FILES(
  File_id	Number(9,0),
  RA	Number(10,7),
  DEC	Number(10,7),
  EQUINOX	Number(10,5),
  File_Date	timestamp,
  HTMID	Number(16,0),
  Cx	Number(10,6),
  Cy	Number(10,6),
  Cz	Number(10,6),
  CCD_Number	Number(2,0),
  Device_ID	Number(6,0),
  Gain	Number(5,4),
  RdNoise	Number(5,2),
  NSA_Identifier	Varchar(80),
  Band	Varchar(8),
  Nite	Varchar(12),
  ImageType	Varchar(15),
  RunIDDesc	Varchar(14),
  FrameID	Varchar(80),
  Airmass	Number(4,3),
  Quality_Flag_1	Number(5,0),
  Quality_Flag_2	Number(5,0),
  Quality_Flag_3	Number(5,0),
  Quality_Flag_4	Number(5,0),
  Quality_Flag_5	Number(5,0),
  Quality_Flag_6	Number(5,0),
  Quality_Flag_7	Number(5,0),
  Quality_Flag_8	Number(5,0),
  CONSTRAINT pk_files PRIMARY KEY (file_id))
  storage (initial 1M) ;

drop sequence files_seq;
create sequence files_seq nocache start with 1 increment by 1;

/* TABLE: FILES TABLE UPDATED 

/*
create or replace trigger tgr_objects_seq_ins
before insert on objects
for each row
begin
select objects_seq.nextval
into :new.object_id
from dual;
end;
*/



/*
-- Table: DCO_TRUTH */
CREATE TABLE DC0_TRUTH(
TRUTH_ID  NUMBER(7) not null,
HTMID   Number(16),
CX      Number(10,6),
CY      Number(10,6),
CZ      Number(10,6),
ID1     Number(6),
ID2     Number(6),
ID3     Number(6),
ID4     Number(6),
ID5     Number(6),
X_IMAGE Number(6,2),
Y_IMAGE Number(6,2),
ALPHA_J2000     Number(8,5),
DELTA_J2000     Number(8,5),
EQUINOX Number(6,2),
u_MAG_AUTO      Number(7,4),
u_MAGERR_AUTO   Number(7,4),
g_MAG_AUTO      Number(7,4),
g_MAGERR_AUTO   Number(7,4),
r_MAG_AUTO      Number(7,4),
r_MAGERR_AUTO   Number(7,4),
i_MAG_AUTO      Number(7,4),
i_MAGERR_AUTO   Number(7,4),
z_MAG_AUTO      Number(7,4),
z_MAGERR_AUTO   Number(7,4),
u_MAG_APER5     Number(7,4),
g_MAG_APER5     Number(7,4),
r_MAG_APER5     Number(7,4),
i_MAG_APER5     Number(7,4),
z_MAG_APER5     Number(7,4),
u_MAG_APER10    Number(7,4),
g_MAG_APER10    Number(7,4),
r_MAG_APER10    Number(7,4),
i_MAG_APER10    Number(7,4),
z_MAG_APER10    Number(7,4),
u_Reddening     Number(7,4),
g_Reddening     Number(7,4),
r_Reddening     Number(7,4),
i_Reddening     Number(7,4),
z_Reddening     Number(7,4),
u_FWHM  Number(6,2),
g_FWHM  Number(6,2),
r_FWHM  Number(6,2),
i_FWHM  Number(6,2),
z_FWHM  Number(6,2),
u_CLASS_STAR    Number(3,2),
g_CLASS_STAR    Number(3,2),
r_CLASS_STAR    Number(3,2),
i_CLASS_STAR    Number(3,2),
z_CLASS_STAR    Number(3,2),
A_IMAGE Number(6,2),
B_IMAGE Number(6,2),
THETA   Number(5,2),
CXX     Number(8,4),
CYY     Number(8,4),
CXY     Number(8,4),
u_FLAGS Number(3),
g_FLAGS Number(3),
r_FLAGS Number(3),
i_FLAGS Number(3),
z_FLAGS Number(3),
CONSTRAINT PK_DC0_TRUTH PRIMARY KEY (truth_id))
 storage (initial 500M);

--drop sequence dc0_truth_seq;
create sequence dc0_truth_seq nocache start with 1 increment by 1;


-- Table: USNO_B
CREATE TABLE USNO_B(
  USNOB_ID	Varchar2(12) NOT NULL,
  RA	Number(10,7),
  DEC	Number(10,7),
  sra	Number(3,0),
  sde	Number(3,0),
  Epoch	Number(10,5),
  HTMID	Number(16,0),
  Cx	Number(10,6),
  Cy	Number(10,6),
  Cz	Number(10,6),
  MuRA	Number(7,0),
  MuDEC	Number(7,0),
  MuProb	Number(6,0),
  sMuRA	Number(7,0),
  sMuDE	Number(7,0),
  sFitRA	Number(6,0),
  sFitDE	Number(6,0),
  NFitPt	Number(6,0),
  Flags	Varchar2(8),
  B1	Number(5,3),
  B1MagFlg	Number(6,0),
  B1FldID	Number(5,0),
  B1S/G	Number(3,0),
  B1Xresid	Number(8,2),
  B1Yresid	Number(8,2),
  B1PltIdx	Number(10,0),
  R1	Number(5,3),
  R1MagFlg	Number(6,0),
  R1FldID	Number(5,0),
  R1S/G	Number(3,0),
  R1Xresid	Number(8,2),
  R1Yresid	Number(8,2),
  R1PltIdx	Number(10,0),
  B2	Number(5,3),
  B2MagFlg	Number(6,0),
  B2FldID	Number(5,0),
  B2S/G	Number(3,0),
  B2Xresid	Number(8,2),
  B2Yresid	Number(8,2),
  B2PltIdx	Number(10,0),
  R2	Number(5,3),
  R2MagFlg	Number(6,0),
  R2FldID	Number(5,0),
  R2S/G	Number(3,0),
  R2Xresid	Number(8,2),
  R2Yresid	Number(8,2),
  R2PltIdx	Number(10,0),
  I2	Number(5,3),
  I2MagFlg	Number(6,0),
  I2FldID	Number(5,0),
  I2S/G	Number(3,0),
  I2Xresid	Number(8,2),
  I2Yresid	Number(8,2),
  I2PltIdx	Number(10,0),
  CONSTRAINT pk_USNO_B PRIMARY KEY (USNOB_ID))
    storage (initial 500M); 
*/

/* CoAddTile table: created for co-addition module*/
CREATE TABLE COADDTILE
(COADDTILE_ID		  NUMBER(6)  NOT NULL 
,project			  VARCHAR(25)
,tilename			  VARCHAR(50)
,ra					  NUMBER(10,7)
,DEC				  NUMBER(10,7)
,epoch  			  NUMBER(8,4)
,pixelsize	 		NUMBER(8,6)
,npix_ra 			NUMBER(7)
,npix_dec 			NUMBER(7)
,CONSTRAINT pk_COADDTILE PRIMARY KEY (COADDTILE_ID ))
STORAGE (INITIAL 5M); 

CREATE SEQUENCE coaddtile_seq START WITH 1 INCREMENT BY 1;

CREATE PUBLIC SYNONYM coaddtile_seq FOR des_admin.coaddtile_seq;

/* FILES Table
Created Dec 2005
*/

CREATE TABLE FILES
(
  IMAGEID         NUMBER(9),
  RA              NUMBER(10,7),
  DEC             NUMBER(10,7),
  EQUINOX         NUMBER(10,5),
  FILE_DATE       VARCHAR2(25 BYTE),
  HTMID           NUMBER(16),
  CX              NUMBER(10,6),
  CY              NUMBER(10,6),
  CZ              NUMBER(10,6),
  CCD_NUMBER      NUMBER(2),
  DEVICE_ID       NUMBER(6),
  NSA_IDENTIFIER  VARCHAR2(80 BYTE),
  BAND            VARCHAR2(8 BYTE),
  NITE            VARCHAR2(80 BYTE),
  IMAGETYPE       VARCHAR2(15 BYTE),
  RUNIDDESC       VARCHAR2(80 BYTE),
  IMAGENAME       VARCHAR2(80 BYTE),
  AIRMASS         NUMBER(4,3),
  QUALITY_FLAG_1  NUMBER(5),
  QUALITY_FLAG_2  NUMBER(5),
  QUALITY_FLAG_3  NUMBER(5),
  QUALITY_FLAG_4  NUMBER(5),
  QUALITY_FLAG_5  NUMBER(5),
  QUALITY_FLAG_6  NUMBER(5),
  QUALITY_FLAG_7  NUMBER(5),
  QUALITY_FLAG_8  NUMBER(5),
  EXPTIME         NUMBER(6,2),
  DARKTIME        NUMBER(6,2),
  GAIN_A          NUMBER(4,2),
  RDNOISE_A       NUMBER(4,2),
  GAIN_B          NUMBER(4,2),
  RDNOISE_B       NUMBER(4,2),
  OBJECT          VARCHAR2(80 BYTE),
  OBSERVATORY     VARCHAR2(80 BYTE),
  TELESCOPE       VARCHAR2(20 BYTE),
  DETECTOR        VARCHAR2(20 BYTE),
  OBSERVER        VARCHAR2(80 BYTE),
  PROPID          VARCHAR2(20 BYTE),
  HOURANGLE       VARCHAR2(20 BYTE),
  ZENITHD         VARCHAR2(20 BYTE),
  WEATHERDATE     VARCHAR2(80 BYTE),
  WINDSPD         VARCHAR2(20 BYTE),
  WINDDIR         VARCHAR2(20 BYTE),
  AMBTEMP         VARCHAR2(20 BYTE),
  HUMIDITY        VARCHAR2(20 BYTE),
  PRESSURE        VARCHAR2(20 BYTE),
  DIMMSEEING      VARCHAR2(20 BYTE),
  WCSDIM          NUMBER(2),
  CTYPE1          VARCHAR2(10 BYTE),
  CTYPE2          VARCHAR2(10 BYTE),
  CRVAL1          NUMBER(10,7),
  CRVAL2          NUMBER(10,7),
  CRPIX1          NUMBER(10,4),
  CRPIX2          NUMBER(10,4),
  CD1_1           NUMBER(10,7),
  CD2_1           NUMBER(10,7),
  CD1_2           NUMBER(10,7),
  CD2_2           NUMBER(10,7),
  RADECEQ         NUMBER(10,5),
  TILENAME        VARCHAR2(50 BYTE),
  NPIX1           NUMBER(6),
  NPIX2           NUMBER(6),
  NEXTEND         NUMBER(3),
CONSTRAINT PK_FILES_IMAGEID PRIMARY KEY (IMAGEID))
STORAGE (INITIAL 5M);

CREATE INDEX DES_ADMIN.FILES_NITE_CCD_IDX ON DES_ADMIN.FILES (NITE, CCD_NUMBER);

CREATE INDEX DES_ADMIN.FILES_RA_DEC_IDX ON DES_ADMIN.FILES (RA, DEC)

alter table files
add (archivesites varchar2(12));

alter table files
modify (archivesites default 'NNNNNNNNNNNNNNNNNNNNNNNNN');

/*modified files table on oct 3, 2006 as per joe's request*/
alter table files add (
PV1_0 NUMBER(11,10),
PV1_1 NUMBER(11,10),
PV1_2 NUMBER(11,10),
PV1_3 NUMBER(11,10),
PV1_4 NUMBER(11,10),
PV1_5 NUMBER(11,10),
PV1_6 NUMBER(11,10),
PV1_7 NUMBER(11,10),
PV1_8 NUMBER(11,10),
PV1_9 NUMBER(11,10),
PV1_10 NUMBER(11,10),
PV2_0  NUMBER(11,10),
PV2_1 NUMBER(11,10),
PV2_2 NUMBER(11,10),
PV2_3 NUMBER(11,10),
PV2_4 NUMBER(11,10),
PV2_5 NUMBER(11,10),
PV2_6 NUMBER(11,10),
PV2_7 NUMBER(11,10),
PV2_8 NUMBER(11,10),
PV2_9 NUMBER(11,10),
PV2_10 NUMBER(11,10));

-- added on Oct 24th, requested by joe
alter table files add(imageclass varchar2(20));



/* TABLE: archive_sites table; created for distributed archive */
CREATE TABLE archive_sites (
  location_id varchar2(8) not null,
  location_name varchar2(100),
  archive_host varchar2(60),
  archive_root varchar2(100),
  staging_host varchar2(60),
  staging_root varchar2(100),
  ip_subnet varchar2(100),
  download_dir varchar2(100),
  transfer_host varchar2(60),
  transfer_args_small varchar2(200),
  transfer_args_large varchar2(200),
  rating number(3,0),
  country varchar2(20),
  CONSTRAINT pk_archive_sites PRIMARY KEY (location_id))
  STORAGE (INITIAL 10k); 


/*STANDARD_STARS TABLE
Created for Douglas, Oct 2 2006
will replace des_stripe82_v1 table
*/
CREATE TABLE standard_stars
(STANDARD_STAR_ID              NUMBER(10) not null,
NAME                           VARCHAR2(40) not null,
RADEG                                   NUMBER(10,6),
DECDEG                                  NUMBER(10,6),
STDMAG_U                                NUMBER(10,6),
STDMAG_G                                NUMBER(10,6),
STDMAG_R                                NUMBER(10,6),
STDMAG_I                                NUMBER(10,6),
STDMAG_Z                                NUMBER(10,6),
STDMAGERR_U                             NUMBER(10,6),
STDMAGERR_G                             NUMBER(10,6),
STDMAGERR_R                             NUMBER(10,6),
STDMAGERR_I                             NUMBER(10,6),
STDMAGERR_Z                             NUMBER(10,6),
NOBS_U                                  NUMBER(10,6),
NOBS_G                                  NUMBER(10,6),
NOBS_R                                  NUMBER(10,6),
NOBS_I                                  NUMBER(10,6),
NOBS_Z                                  NUMBER(10,6),
FIELDNAME                               VARCHAR2(40),
VERSION_ID                              NUMBER(10,6),
CONSTRAINT PK_STANDARD_STARS PRIMARY KEY (STANDARD_STAR_ID))
STORAGE (INITIAL 2M);

CREATE SEQUENCE STANDARD_STARS_SEQ CACHE 1000 START WITH 1 INCREMENT BY 1;

/*
2ND U.S NAVAL OBSERVATORY CCD ASTROGRAPH CATALOG
TABLE: UCAC2 
Created for matching objects info provided by Wayne, Oct4, 2006
*/

Create table UCAC2 (
  UCAC2_ID number(9,0) not null,
  RA  number(10, 7),
  DEC number(10, 7),
  U2Rmag  number(5, 0),
  e_RAm number(3, 0),
  e_DEm number(3, 0),
  nobs  number(3, 0),
  e_pos number(3, 0),
  ncat  number(3, 0),
  cflg  number(3, 0),
  EpRAm number(6, 0),
  EpDEm number(6, 0),
  pmRA  number(10, 0),
  pmDEC  number(10, 0),
  e_pmRA  number(3, 0),
  e_pmDEC  number(3, 0),
  q_pmRA  number(3, 0),
  q_pmDEC  number(3, 0),
  twomass_id number(10, 0),
  twomass_J  number(6, 0),
  twomass_H  number(6, 0),
  twomass_Ks number(6, 0),
  twomass_ph varchar2(4),
  twomass_cc varchar2(4),
  htmid NUMBER(16,0),
  cx number(10,6),
  cy number(10,6),
  cz number(10,6),
CONSTRAINT pk_ucac2 PRIMARY KEY (UCAC2_ID))
STORAGE (INITIAL 1M);

--drop table ucac2;

create sequence ucac2_seq start with 1 increment by 1;

/* table for mapping NVO login name with DES user name */ 
Create table DES_ACCOUNTS (
  NVO_user varchar2(30) not null,
  DES_group varchar2(30) not null,
constraint pk_des_accounts primary key (NVO_user, DES_group))
storage (initial 1k);

/* table for controlling which night's data can be accessed */ 
Create table NITE_ACCESS (
  DES_group varchar2(30) not null,
  nite varchar2(80) not null,
constraint pk_nite_access primary key (DES_group, nite))
storage (initial 1k);

/* table for controlling which runid's data can be accessed */ 
Create table RUNIDDESC_ACCESS (
  DES_group varchar2(30) not null,
  runiddesc varchar2(80) not null,
constraint pk_runiddesc_access primary key (DES_group, runiddesc))
storage (initial 1k);

/* table for controlling which imagetype can be accessed */ 
Create table IMAGETYPE_ACCESS (
  DES_group varchar2(30) not null,
  imagetype varchar2(80) not null,
constraint pk_imagetype_access primary key (DES_group, imagetype))
storage (initial 1k);

/* table for registration infomation of NVO users */ 
Create table user_registration (
  NVO_user varchar2(30) not null,
  first_name varchar2(30),
  last_name varchar2(30),
  affiliation varchar2(100),
  telephone varchar2(30),
  email varchar2(30),
  admin_role char(1),
  webmaster_role char(1),
  prefer_archive_site varchar2(8),
  country varchar2(20),
constraint pk_user_registration primary key (NVO_user))
storage (initial 1k);


CREATE TABLE COADD_OBJECTS(
COADD_OBJECTS_ID	Number(11,0),
EQUINOX	Number(6,2),
HTMID	NUMBER(16,0),
CX	Number(10,6),
CY	Number(10,6),
CZ	Number(10,6),
SoftID	Number(4,0),
ImageID_g	Number(9,0),
ImageID_r	Number(9,0),
ImageID_i	Number(9,0),
ImageID_z	Number(9,0),
Zeropoint_g	Number(7,4),
Zeropoint_r	Number(7,4),
Zeropoint_i	Number(7,4),
Zeropoint_z	Number(7,4),
ERRZeropoint_g	Number(7,4),
ERRZeropoint_r	Number(7,4),
ERRZeropoint_i	Number(7,4),
ERRZeropoint_z	Number(7,4),
ZeropointID_g	Number(10,0),
ZeropointID_r	Number(10,0),
ZeropointID_i	Number(10,0),
ZeropointID_z	Number(10,0),
R_OBJECT_NUMBER	Number(6,0),
MAG_AUTO_g	Number(7,4),
MAGERR_AUTO_g	Number(7,4),
MAG_APER1_g	Number(7,4),
MAGERR_APER1_g	Number(7,4),
MAG_APER2_g	Number(7,4),
MAGERR_APER2_g	Number(7,4),
MAG_APER3_g	Number(7,4),
MAGERR_APER3_g	Number(7,4),
MAG_APER4_g	Number(7,4),
MAGERR_APER4_g	Number(7,4),
MAG_APER5_g	Number(7,4),
MAGERR_APER5_g	Number(7,4),
MAG_APER6_g	Number(7,4),
MAGERR_APER6_g	Number(7,4),
MAG_AUTO_r	Number(7,4),
MAGERR_AUTO_r	Number(7,4),
MAG_APER1_r	Number(7,4),
MAGERR_APER1_r	Number(7,4),
MAG_APER2_r	Number(7,4),
MAGERR_APER2_r	Number(7,4),
MAG_APER3_r	Number(7,4),
MAGERR_APER3_r	Number(7,4),
MAG_APER4_r	Number(7,4),
MAGERR_APER4_r	Number(7,4),
MAG_APER5_r	Number(7,4),
MAGERR_APER5_r	Number(7,4),
MAG_APER6_r	Number(7,4),
MAGERR_APER6_r	Number(7,4),
MAG_AUTO_i	Number(7,4),
MAGERR_AUTO_i	Number(7,4),
MAG_APER1_i	Number(7,4),
MAGERR_APER1_i	Number(7,4),
MAG_APER2_i	Number(7,4),
MAGERR_APER2_i	Number(7,4),
MAG_APER3_i	Number(7,4),
MAGERR_APER3_i	Number(7,4),
MAG_APER4_i	Number(7,4),
MAGERR_APER4_i	Number(7,4),
MAG_APER5_i	Number(7,4),
MAGERR_APER5_i	Number(7,4),
MAG_APER6_i	Number(7,4),
MAGERR_APER6_i	Number(7,4),
MAG_AUTO_z	Number(7,4),
MAGERR_AUTO_z	Number(7,4),
MAG_APER1_z	Number(7,4),
MAGERR_APER1_z	Number(7,4),
MAG_APER2_z	Number(7,4),
MAGERR_APER2_z	Number(7,4),
MAG_APER3_z	Number(7,4),
MAGERR_APER3_z	Number(7,4),
MAG_APER4_z	Number(7,4),
MAGERR_APER4_z	Number(7,4),
MAG_APER5_z	Number(7,4),
MAGERR_APER5_z	Number(7,4),
MAG_APER6_z	Number(7,4),
MAGERR_APER6_z	Number(7,4),
ALPHA_J2000	Number(8,5),
DELTA_J2000	Number(8,5),
X2_WORLD	Number(11,3),
Y2_WORLD	Number(11,3),
XY_WORLD	Number(11,3),
THRESHOLD	Number(6,2),
X_IMAGE	Number(6,2),
Y_IMAGE	Number(6,2),
THETA_IMAGE_g	Number(4,2),
ERRTHETA_IMAGE_g	Number(4,2),
ELLIPTICITY_g	Number(5,4),
CLASS_STAR_g	Number(3,2),
FLAGS_g	Number(3,0),
THETA_IMAGE_r	Number(4,2),
ERRTHETA_IMAGE_r	Number(4,2),
ELLIPTICITY_r	Number(5,4),
CLASS_STAR_r	Number(3,2),
FLAGS_r	Number(3,0),
THETA_IMAGE_i	Number(4,2),
ERRTHETA_IMAGE_i	Number(4,2),
ELLIPTICITY_i	Number(5,4),
CLASS_STAR_i	Number(3,2),
FLAGS_i	Number(3,0),
THETA_IMAGE_z	Number(4,2),
ERRTHETA_IMAGE_z	Number(4,2),
ELLIPTICITY_z	Number(5,4),
CLASS_STAR_z	Number(3,2),
FLAGS_z	Number(3,0),
CONSTRAINT PK_COADD_OBJECTS_ID PRIMARY KEY (COADD_OBJECTS_ID))
storage (initial 2M);

-- add by Dora 01/10/08
Create table coadd_src (
  coadd_imageid number(9),
  src_imageid number(9),
  magzp binary_float,
  magzp_err binary_float,
constraint pk_coadd_src primary key (coadd_imageid, src_imageid))
storage (initial 100k);

create index src_imageid_idx on coadd_src(src_imageid)
tablespace des_index;

create public synonym coadd_src for des_admin.coadd_src;

grant all on coadd_src to des_writer;
grant select on coadd_src to des_reader;

-- done by Dora 02/28/08
alter table files
rename column quality_flag_7 to skysigma;

alter table files
modify (skysigma number(9,3));

alter table files
rename column mnseeing to fwhm;

-- Add table per Choong's request
Create table BCS_Reduction_Status (
  runid varchar2(80),
  Stage_detrend varchar2(10),
  Stage_scamp varchar2(10),
  Stage_psfex varchar2(10),
  Stage_postscamp varchar2(10),
  Stage_cat_ingest varchar2(10),
  Stage_psm varchar2(10),
constraint pk_bcs_reduction_status primary key (runid))
storage (initial 10k);

-- Create new tables per Joe's request
Create table location (
ID	Number(10,0),
FileClass	VarChar2(20),
FileType	VarChar2(20),
FileName	VarChar2(200),
FileDate	Date,
FileSize	Number(12,0),
Run	VarChar2(100),
Nite	VarChar2(20),
Band	VarChar2(20),
TileName	VarChar2(20),
ExposureName	VarChar2(100),
CCD	Number(3,0),
Project	VarChar2(20),
ArchiveSites	VarChar2(30),
CONSTRAINT pk_location PRIMARY KEY (id))
storage (initial 100K) ;

drop sequence location_seq;
create sequence location_seq cache 100 start with 1 increment by 1;

create table Image (
ID	Number(10,0),
Run	VarChar2(100),
Nite	VarChar2(20),
Band	VarChar2(20),
TileName	VarChar2(20),
ImageName	VarChar2(100),
CCD	Number(3,0),
ImageType	VarChar2(20),
Project	VarChar2(20),
ExposureID	Number(9,0),
ParentID	Number(9,0),
Airmass	Binary_Float,
EXPTIME	Binary_Float,
Device_ID	NUMBER(6),
GAIN_A	Binary_Float,
RDNOISE_A	Binary_Float,
GAIN_B	Binary_Float,
RDNOISE_B	Binary_Float,
RA	Binary_Double,
DEC	Binary_Double,
EQUINOX	Binary_Float,
HTMID	Number(16),
Cx	Binary_Double,
Cy	Binary_Double,
Cz	Binary_Double,
WCSDIM	NUMBER(2),
CTYPE1	VarChar2(10),
CUNIT1	VarChar2(20),
CRVAL1	Binary_Double,
CRPIX1	Binary_Double,
CD1_1	Binary_Double,
CD1_2	Binary_Double,
PV1_1	Binary_Double,
PV1_2	Binary_Double,
PV1_3	Binary_Double,
PV1_4	Binary_Double,
PV1_5	Binary_Double,
PV1_6	Binary_Double,
PV1_7	Binary_Double,
PV1_8	Binary_Double,
PV1_9	Binary_Double,
PV1_10	Binary_Double,
CTYPE2	VarChar2(10),
CUNIT2	VarChar2(20),
CRVAL2	Binary_Double,
CRPIX2	Binary_Double,
CD2_1	Binary_Double,
CD2_2	Binary_Double,
PV2_1	Binary_Double,
PV2_2	Binary_Double,
PV2_3	Binary_Double,
PV2_4	Binary_Double,
PV2_5	Binary_Double,
PV2_6	Binary_Double,
PV2_7	Binary_Double,
PV2_8	Binary_Double,
PV2_9	Binary_Double,
PV2_10	Binary_Double,
SKYBRITE	Binary_Float,
SKYSIGMA	Binary_Float,
ELLIPTICITY	Binary_Float,
FWHM	Binary_Float,
SCAMPNUM	NUMBER(5),
SCAMPCHI	Binary_Float,
SCAMPFLG	NUMBER(2),
CONSTRAINT pk_image PRIMARY KEY (id))
storage (initial 1M) ;

create table Catalog (
ID	Number(10,0),
Run	VarChar2(100),
Nite	VarChar2(20),
Band	VarChar2(20),
TileName	VarChar2(20),
CatalogName	VarChar2(100),
CCD	Number(3,0),
CatalogType	VarChar2(20),
ParentID	Number(9,0),
ExposureID	Number(9,0),
Objects	Number(8,0),
CONSTRAINT pk_catalog PRIMARY KEY (id))
storage (initial 1M) ;

create table Exposure (
ID	NUMBER(10,0),
Nite	VarChar2(20),
Band	VarChar2(20),
ExposureName	VarChar2(100),
ExposureType	VarChar2(20),
TELRADEC	VarChar2(10),
TELRA	Binary_Double,
TELDEC	Binary_Double,
TELEQUIN	Binary_Float,
HTMID	Number(16),
Cx	Binary_Double,
Cy	Binary_Double,
Cz	Binary_Double,
HA	VarChar2(20),
ZD	Binary_Float,
AIRMASS	Binary_Float,
TELFOCUS	Binary_Float,
OBJECT	VarChar2(80),
OBSERVER	VarChar2(80),
PROPID	VarChar2(20),
DETECTOR	VarChar2(20),
DETSIZE	VarChar2(40),
TELESCOPE	VarChar2(20),
OBSERVATORY	VarChar2(80),
LATITUDE	VarChar2(80),
LONGITUDE	VarChar2(80),
ALTITUDE	VarChar2(80),
TIMESYS	VarChar2(80),
DATE_OBS	VarChar2(80),
TIME_OBS	VarChar2(80),
MJD_OBS	Binary_Double,
EXPTIME	Binary_Float,
EXPREQUEST	Binary_Float,
DARKTIME	Binary_Float,
EPOCH	Binary_Float,
WINDSPD	VarChar2(20),
WINDDIR	VarChar2(20),
AMBTEMP	VarChar2(20),
HUMIDITY	VarChar2(20),
PRESSURE	VarChar2(20),
SKYVAR	Binary_Float,
FLUXVAR	Binary_Float,
DIMMSEEING	Binary_Float,
PHOTFLAG	Number(2),
IMAGEHWV	VarChar2(80),
IMAGESWV	VarChar2(80),
CONSTRAINT pk_exposure PRIMARY KEY (id))
storage (initial 100K) ;

create table Coadd (
ID	Number(10,0),
Run	VarChar2(100),
Band	VarChar2(20),
TileName	VarChar2(20),
ImageName	VarChar2(100),
RA	Binary_Double,
DEC	Binary_Double,
RADECEQUIN	Binary_Float,
HTMID	Number(16),
Cx	Binary_Double,
Cy	Binary_Double,
Cz	Binary_Double,
WCSTYPE	VarChar2(80),
WCSDIM	NUMBER(2),
EQUINOX	Binary_Float,
CTYPE1	VarChar2(20),
CTYPE2	VarChar2(20),
CUNIT1	Varchar2(20),
CUNIT2	Varchar2(20),
CRVAL1	Binary_Double,
CRVAL2	Binary_Double,
CRPIX1	NUMBER(10,4),
CRPIX2	NUMBER(10,4),
CD1_1	Binary_Double,
CD2_1	Binary_Double,
CD1_2	Binary_Double,
CD2_2	Binary_Double,
NAXIS1	NUMBER(6),
NAXIS2	NUMBER(6),
NEXTEND	NUMBER(3),
FWHM	Binary_Float,
ELLIPTICITY	Binary_Float,
CONSTRAINT pk_coadd PRIMARY KEY (id))
storage (initial 100K) ;


