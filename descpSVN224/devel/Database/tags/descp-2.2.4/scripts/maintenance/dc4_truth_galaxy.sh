#!/bin/bash
#
# Creates dynamic sqlldr statements for loading dc4_truth galaxy file

DIR='/oracle/scripts/des/load_dc4_truth'
DATADIR='/home/bcs/DC4/Truth'
outfile=$DIR/galaxy.log
infile=$DIR/galaxy_in.lst
cp /dev/null $outfile

source $DIR/des.bash

cat $infile | \
while read line
do
 datafile=`echo $line | cut -c 21-80`
 echo "datafile is " $datafile
 ctrfile=$DIR/$datafile'.ctl'
 badfile=$DIR/$datafile'.bad'
 logfile=$DIR/$datafile'.log'
 parfile=$DIR/'common.par'

 echo `date` "Start loading $datafile" >> $outfile
 cp /dev/null $ctrfile


 echo "Load data" >>  $ctrfile
 echo "infile '$line'" >>  $ctrfile
 echo "append" >>  $ctrfile
 echo "into table dc4_truth" >>  $ctrfile
 echo "fields terminated by ','" >>  $ctrfile
 echo "trailing nullcols(" >>  $ctrfile
 echo "star_galaxy_id  integer external," >> $ctrfile
 echo "ra         decimal external," >> $ctrfile
 echo "dec        decimal external," >> $ctrfile
 echo "u_mag      decimal external," >> $ctrfile
 echo "g_mag      decimal external," >> $ctrfile
 echo "r_mag      decimal external," >> $ctrfile
 echo "i_mag      decimal external," >> $ctrfile
 echo "z_mag      decimal external," >> $ctrfile
 echo "y_mag      decimal external," >> $ctrfile
 echo "class      CONSTANT 'G'," >> $ctrfile
 echo "datafile_name   CONSTANT '$datafile'," >> $ctrfile
 echo "dc4_truth_id  \"dc4_truth_seq.nextval\")" >> $ctrfile
 
 sqlldr parfile=$parfile control=$ctrfile log=$logfile bad=$badfile
 echo `date` "Finish loading $datafile" >> $outfile

 done

exit

