#!/bin/bash
#
# Creates dynamic sqlldr statements for loading dc5_truth galaxy file

DIR='/oracle/scripts/des/load_dc5_truth'
DATADIR='/home/gdaues/GSN/imSim5_aug_gsn/csv'
outfile=$DIR/galaxy.log
infile=$DIR/galaxy_in.lst
cp /dev/null $outfile

source $DIR/des.bash

cat $infile | \
while read line
do
 datafile=`echo $line | cut -c 37-80`
 echo "datafile is " $datafile
 ctrfile=$DIR/$datafile'.ctl'
 badfile=$DIR/$datafile'.bad'
 logfile=$DIR/$datafile'.log'
 parfile=$DIR/'common.par'

 echo `date` "Start loading $datafile" >> $outfile
 cp /dev/null $ctrfile


 echo "Load data" >>  $ctrfile
 echo "infile '$line'" >>  $ctrfile
 echo "append" >>  $ctrfile
 echo "into table dc5_truth" >>  $ctrfile
 echo "fields terminated by ','" >>  $ctrfile
 echo "trailing nullcols(" >>  $ctrfile
 echo "star_galaxy_id  integer external," >> $ctrfile
 echo "ra_dbl         decimal external," >> $ctrfile
 echo "dec_dbl       decimal external," >> $ctrfile
 echo "u_mag      decimal external," >> $ctrfile
 echo "g_mag      decimal external," >> $ctrfile
 echo "r_mag      decimal external," >> $ctrfile
 echo "i_mag      decimal external," >> $ctrfile
 echo "z_mag      decimal external," >> $ctrfile
 echo "y_mag      decimal external," >> $ctrfile
 echo "redshift   decimal external," >> $ctrfile
 echo "class      CONSTANT 'G'," >> $ctrfile
 echo "datafile_name   CONSTANT '$datafile'," >> $ctrfile
 echo "ra         decimal external \":ra_dbl\"," >> $ctrfile
 echo "dec       decimal external \":dec_dbl\"," >> $ctrfile
 echo "dc5_truth_id  \"dc5_truth_seq.nextval\")" >> $ctrfile

 sqlldr parfile=$parfile control=$ctrfile log=$logfile bad=$badfile
 echo `date` "Finish loading $datafile" >> $outfile

 done

exit

