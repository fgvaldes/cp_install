#!/bin/bash
export ORACLE_BASE=/oracle
export ORACLE_HOME=/oracle/11.1.0
export ORACLE_SID=des
export ORACLE_ASK=NO
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/lib:$PATH
echo "alter database recover managed standby database cancel;" > /tmp/qry.$$
echo "shutdown immediate;" >> /tmp/qry.$$
echo "exit;" >> /tmp/qry.$$
sqlplus / as sysdba @/tmp/qry.$$
rm /tmp/qry.$$
lsnrctl stop
exit
