#!/bin/bash
#
# Apr. 1, 2009: RMAN Backup with catalog run from the dvlp standby database
# 05/25/2010: distribute the backup to two directories

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/dvlp'
BUDIR='/oracle/backup/dvlp.'$TODAY
BUDIR1='/oracle/backup1/dvlp.'$TODAY
LOG=$DIR/stdvlp_rman_cat.log
cp /dev/null $LOG
mkdir $BUDIR
mkdir $BUDIR1
source $DIR/dvlp.bash

cp /dev/null $DIR/stdvlp_rman_cat.cmd
#echo 'run {' >> $DIR/stdvlp_rman_cat.cmd
#echo 'crosscheck backup device type disk; ' >> $DIR/stdvlp_rman_cat.cmd
#echo 'delete noprompt expired backup; ' >> $DIR/stdvlp_rman_cat.cmd
#echo 'crosscheck archivelog all; ' >> $DIR/stdvlp_rman_cat.cmd
#echo "delete noprompt expired archivelog all;} " >> $DIR/stdvlp_rman_cat.cmd
echo 'run {' >> $DIR/stdvlp_rman_cat.cmd
echo 'resync catalog;' >> $DIR/stdvlp_rman_cat.cmd
echo "allocate channel t1 type disk format '$BUDIR/DVLP_%U' MAXPIECESIZE = 5G; " >> $DIR/stdvlp_rman_cat.cmd
echo "allocate channel t2 type disk format '$BUDIR1/DVLP_%U' MAXPIECESIZE = 5G; " >> $DIR/stdvlp_rman_cat.cmd
echo "backup as compressed backupset database; " >> $DIR/stdvlp_rman_cat.cmd
echo "backup current controlfile format '$BUDIR/CTRFl_%U'; " >> $DIR/stdvlp_rman_cat.cmd
echo "backup archivelog all format '$BUDIR1/ARC_%s_%p'; " >> $DIR/stdvlp_rman_cat.cmd
echo 'release channel t1; ' >> $DIR/stdvlp_rman_cat.cmd
echo 'release channel t2;}' >> $DIR/stdvlp_rman_cat.cmd

rman target / catalog rman/rmanmgr@rcatd cmdfile=$DIR/stdvlp_rman_cat.cmd >> $LOG

line_no=0
line_no=`grep 'ORA-' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"DVLP backup on deslogin failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

line_no=0
line_no=`grep 'fail' $LOG | grep -v 'validation failed' | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"DVLP backup on deslogin failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

mail -s"DVLP backup on deslogin is completed successfully" desdm-db@cosmology.illinois.edu < $LOG
echo suceeded > $BUDIR/.status
exit

