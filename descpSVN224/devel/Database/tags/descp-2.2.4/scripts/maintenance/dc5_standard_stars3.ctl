load data
infile 'stdfield+34.5.dat.joined.reformatted'
append
into table standard_stars_dc5
fields terminated by ','
trailing nullcols(
 field1          filler,
 STANDARD_STAR_ID integer external,
 NAME            char,
 RADEG           decimal external,
 DECDEG          decimal external,
 STDMAG_U        decimal external,
 STDMAG_G        decimal external,
 STDMAG_R        decimal external,
 STDMAG_I        decimal external,
 STDMAG_Z        decimal external,
 STDMAG_Y        decimal external,
 STDMAGERR_U     decimal external,
 STDMAGERR_G     decimal external,
 STDMAGERR_R     decimal external,
 STDMAGERR_I     decimal external,
 STDMAGERR_Z     decimal external,
 STDMAGERR_Y     decimal external,
 NOBS_U          decimal external,
 NOBS_G          decimal external,
 NOBS_R          decimal external,
 NOBS_I          decimal external,
 NOBS_Z          decimal external,
 NOBS_Y          decimal external,
 FIELDNAME       char,
 VERSION_ID      decimal external)

