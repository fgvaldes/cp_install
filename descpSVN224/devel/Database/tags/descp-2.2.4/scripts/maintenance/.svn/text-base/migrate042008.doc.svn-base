* Create export directory
cd /oracle
mkdir export
cd export
mkdir des 
SQL> create or replace directory des_exp_dir as '/oracle/export/des/';

* Edit parfile expdp_des.par
full=Y
content=all
filesize=10G
dumpfile=des%U.dmp
logfile=des_exp.log
parallel=4
directory=des_exp_dir

* Place tablespace des in read only mode
SQL> alter tablespace des read only;

* Call expdp 
expdp system/passwd parfile=expdp_des.par
(run time: about 10 hours)

* Place tablespace des in read write mode
SQL> alter tablespace des read write;

-- all steps below are done on desdb1
* scp the dmp files to desdb1

* install Oracle 10.2.0.1 without creating the starter database

* create new database des with proper redo logs (size and location) and db parameters   (dbw = 4, db_writer_process=4, open_cursors=300, processes=500, sga_max_size=4784M,
   sga, target=4784M)
  redo in /oracle/redo

* create tablespace des with 25 datafiles in /oracle/data/des,
  create tablespace partdes with 1 datafiles in /oracle/data/des_temp, 
  create tablespace partdes with 10 datafiles in /oracle/data/partdes, 
  create tablespace partdes2 with 10 datafiles in /oracle/data/partdes2, 
  create tablespace des_index with 5 datafiles in /oracle/data/des_index

* Call impdp
  impdp system/passwd parfile=impdp_des.par

-- file impdp_des.par
full=Y
content=all
dumpfile=des%U.dmp
logfile=des_imp.log
parallel=4
directory=des_exp_dir
table_exists_action=truncate
reuse_datafiles=Y
transform=segment_attributes:n
transform=storage:n
--

* shutdown database and do an offline backup

* startup database

* drop the tablespace partdes2, des_index
alter tablespace des_index offline;
drop tablespace des_index including contents;

alter tablespace partdes2 offline;
drop tablespace partdes2 including contents;

* shutdown database

* install Oracle 11.1

* upgrade database

* Turn archivelog on
  
* rebuilt EM repository:
  emca -deconfig dbcontrol db -repos drop
  emca -config dbcontrol db -repos create

* Test queries and programs

--Lesson learnt:
* Since the parfile for import uses transform=segment_attributes:n and 
  transform=storage:n, the storage clause and tablespace clause are removed
  from import process, all data goes to the tablespace the owner belongs to.
* Before upgrade to 11.1, patch 4547817 must be applied to upgrade the database from 10.2.0.1 to 10.2.0.2, and patch 5632264 must be applied to include the new Time_Zone library (Upgrade 11.1 will use it).
* Since a new ORACLE_HOME is created, the Enterprise Manager's repository must be rebuilt, because the repository key in the old ORACLE_HOME is lost.
