#!/bin/sh

mkdir -p /oracle/10.2.0/admin/des/adump
mkdir -p /oracle/10.2.0/admin/des/bdump
mkdir -p /oracle/10.2.0/admin/des/cdump
mkdir -p /oracle/10.2.0/admin/des/dpdump
mkdir -p /oracle/10.2.0/admin/des/pfile
mkdir -p /oracle/10.2.0/admin/des/udump
mkdir -p /oracle/10.2.0/cfgtoollogs/dbca/des
mkdir -p /oracle/10.2.0/dbs
mkdir -p /oracle/10.2.0/flash_recovery_area
mkdir -p /oracle/data/des
mkdir -p /oracle/redo/des
ORACLE_SID=des; export ORACLE_SID
echo You should Add this entry in the /etc/oratab: des:/oracle/10.2.0:Y
/oracle/10.2.0/bin/sqlplus /nolog @/oracle/scripts/des//des.sql
