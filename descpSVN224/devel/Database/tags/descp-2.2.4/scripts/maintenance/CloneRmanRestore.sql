connect "SYS"/"&&sysPassword" as SYSDBA
set echo on
spool /oracle/scripts/des//CloneRmanRestore.log
startup nomount pfile="/oracle/scripts/des/init.ora";
@/oracle/scripts/des//rmanRestoreDatafiles.sql;
