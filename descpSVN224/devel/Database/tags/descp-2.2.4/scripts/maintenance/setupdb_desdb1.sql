-- Run script as system
CREATE SMALLFILE TABLESPACE "DES"
DATAFILE '/oracle/data/des/des_01.dbf'
SIZE 1000M AUTOEXTEND ON NEXT 500M MAXSIZE UNLIMITED
LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE USER "DES_ADMIN" PROFILE "DEFAULT" IDENTIFIED BY "desmgr"
DEFAULT TABLESPACE "DES" TEMPORARY TABLESPACE "TEMP"
QUOTA UNLIMITED ON "DES" ACCOUNT UNLOCK;
GRANT DBA TO DES_ADMIN;
ALTER USER DES_ADMIN DEFAULT ROLE DBA;

-- Create a table space for testing
CREATE SMALLFILE TABLESPACE "DES_TEMP"
DATAFILE '/oracle/data/des_temp/des_test_01.dbf'
SIZE 100M AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED
LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

create smallfile tablespace desdr
datafile '/oracle/data/desdr/desdr_01.dbf'
size 1000m autoextend on next 500m maxsize unlimited
logging extent management local segment space management auto;

create smallfile tablespace desdr007
datafile '/oracle/data/desdr007/desdr007_01.dbf'
size 1000m autoextend on next 500m maxsize unlimited
logging extent management local segment space management auto;

create smallfile tablespace desdr008
datafile '/oracle/data/desdr008/desdr008_01.dbf'
size 1000m autoextend on next 500m maxsize unlimited
logging extent management local segment space management auto;

