--
--ER/Studio 6.5 SQL Code Generation
-- Company :      NCSA
-- Project :      desdb1080502.dm1
-- Author :       Dora Cai
--
-- Date Created : Friday, May 02, 2008 13:54:10
-- Target DBMS : Oracle 10g
--

-- 
-- TABLE: ARCHIVE_SITES 
--

CREATE TABLE ARCHIVE_SITES(
    LOCATION_ID            VARCHAR2(8)      NOT NULL,
    LOCATION_NAME          VARCHAR2(100),
    ARCHIVE_HOST           VARCHAR2(60),
    ARCHIVE_ROOT           VARCHAR2(100),
    STAGING_HOST           VARCHAR2(60),
    STAGING_ROOT           VARCHAR2(100),
    IP_SUBNET              VARCHAR2(100),
    DOWNLOAD_DIR           VARCHAR2(100),
    TRANSFER_HOST          VARCHAR2(60),
    TRANSFER_ARGS_SMALL    VARCHAR2(200),
    TRANSFER_ARGS_LARGE    VARCHAR2(200),
    RATING                 NUMBER(3, 0),
    COUNTRY                VARCHAR2(20),
    SITE_NAME              VARCHAR2(32),
    CONSTRAINT PK_ARCHIVE_SITES PRIMARY KEY (LOCATION_ID)
)
;



-- 
-- TABLE: BCS_REDUCTION_STATUS 
--

CREATE TABLE BCS_REDUCTION_STATUS(
    RUNID               VARCHAR2(80)    NOT NULL,
    STAGE_DETREND       VARCHAR2(10),
    STAGE_SCAMP         VARCHAR2(10),
    STAGE_PSFEX         VARCHAR2(10),
    STAGE_POSTSCAMP     VARCHAR2(10),
    STAGE_CAT_INGEST    VARCHAR2(10),
    STAGE_PSM           VARCHAR2(10),
    CONSTRAINT PK_BCS_REDUCTION_STATUS PRIMARY KEY (RUNID)
)
;



-- 
-- TABLE: CATALOG 
--

CREATE TABLE CATALOG(
    ID             NUMBER(10, 0)    NOT NULL,
    RUN            VARCHAR2(100),
    NITE           VARCHAR2(20),
    BAND           VARCHAR2(20),
    TILENAME       VARCHAR2(20),
    CATALOGNAME    VARCHAR2(100),
    CCD            NUMBER(3, 0),
    CATALOGTYPE    VARCHAR2(20),
    PARENTID       NUMBER(9, 0),
    EXPOSUREID     NUMBER(9, 0),
    OBJECTS        NUMBER(8, 0),
    CONSTRAINT PK_CATALOG PRIMARY KEY (ID)
)
;



-- 
-- TABLE: CATSIM1_TRUTH 
--

CREATE TABLE CATSIM1_TRUTH(
    CATSIM1_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID      NUMBER(10, 0),
    CLASS               CHAR(1),
    RA                  NUMBER(10, 7),
    DEC                 NUMBER(10, 7),
    U_MAG               NUMBER(7, 4),
    G_MAG               NUMBER(7, 4),
    R_MAG               NUMBER(7, 4),
    I_MAG               NUMBER(7, 4),
    Z_MAG               NUMBER(7, 4),
    HTMID               NUMBER(16, 0),
    CX                  NUMBER(10, 6),
    CY                  NUMBER(10, 6),
    CZ                  NUMBER(10, 6),
    CONSTRAINT PK_CATSIM1_TRUTH PRIMARY KEY (CATSIM1_TRUTH_ID)
)
;



-- 
-- TABLE: CATSIM3_TRUTH 
--

CREATE TABLE CATSIM3_TRUTH(
    CATSIM3_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID      NUMBER(10, 0),
    CLASS               CHAR(1),
    RA                  NUMBER(10, 7),
    DEC                 NUMBER(10, 7),
    U_MAG               NUMBER(7, 4),
    G_MAG               NUMBER(7, 4),
    R_MAG               NUMBER(7, 4),
    I_MAG               NUMBER(7, 4),
    Z_MAG               NUMBER(7, 4),
    HTMID               NUMBER(16, 0),
    CX                  NUMBER(10, 6),
    CY                  NUMBER(10, 6),
    CZ                  NUMBER(10, 6),
    DATAFILE_NAME       VARCHAR2(40),
    CONSTRAINT PK_CATSIM3_TRUTH PRIMARY KEY (CATSIM3_TRUTH_ID)
)
;



-- 
-- TABLE: COADD 
--

CREATE TABLE COADD(
    ID             NUMBER(10, 0)    NOT NULL,
    RUN            VARCHAR2(100),
    BAND           VARCHAR2(20),
    TILENAME       VARCHAR2(20),
    IMAGENAME      VARCHAR2(100),
    RA             BINARY_DOUBLE,
    DEC            BINARY_DOUBLE,
    RADECEQUIN     BINARY_FLOAT,
    HTMID          NUMBER(16, 0),
    CX             BINARY_DOUBLE,
    CY             BINARY_DOUBLE,
    CZ             BINARY_DOUBLE,
    WCSTYPE        VARCHAR2(80),
    WCSDIM         NUMBER(2, 0),
    EQUINOX        BINARY_FLOAT,
    CTYPE1         VARCHAR2(20),
    CTYPE2         VARCHAR2(20),
    CUNIT1         VARCHAR2(20),
    CUNIT2         VARCHAR2(20),
    CRVAL1         BINARY_DOUBLE,
    CRVAL2         BINARY_DOUBLE,
    CRPIX1         NUMBER(10, 4),
    CRPIX2         NUMBER(10, 4),
    CD1_1          BINARY_DOUBLE,
    CD2_1          BINARY_DOUBLE,
    CD1_2          BINARY_DOUBLE,
    CD2_2          BINARY_DOUBLE,
    NAXIS1         NUMBER(6, 0),
    NAXIS2         NUMBER(6, 0),
    NEXTEND        NUMBER(3, 0),
    FWHM           BINARY_FLOAT,
    ELLIPTICITY    BINARY_FLOAT,
    CONSTRAINT PK_COADD PRIMARY KEY (ID)
)
;



-- 
-- TABLE: COADD_SRC 
--

CREATE TABLE COADD_SRC(
    COADD_IMAGEID    NUMBER(9, 0)    NOT NULL,
    SRC_IMAGEID      NUMBER(9, 0)    NOT NULL,
    MAGZP            BINARY_FLOAT,
    MAGZP_ERR        BINARY_FLOAT,
    CONSTRAINT PK_COADD_SRC PRIMARY KEY (COADD_IMAGEID, SRC_IMAGEID)
)
;



-- 
-- TABLE: COADDTILE 
--

CREATE TABLE COADDTILE(
    COADDTILE_ID    NUMBER(6, 0)     NOT NULL,
    PROJECT         VARCHAR2(25),
    TILENAME        VARCHAR2(50),
    RA              NUMBER(10, 7),
    DEC             NUMBER(10, 7),
    EQUINOX         NUMBER(10, 5),
    PIXELSIZE       NUMBER(8, 6),
    NPIX_RA         NUMBER(7, 0),
    NPIX_DEC        NUMBER(7, 0),
    CONSTRAINT PK_COADDTILE PRIMARY KEY (COADDTILE_ID)
)
;



-- 
-- TABLE: DC0_TRUTH 
--

CREATE TABLE DC0_TRUTH(
    TRUTH_ID         NUMBER(12, 0)    NOT NULL,
    HTMID            NUMBER(16, 0),
    CX               NUMBER(10, 6),
    CY               NUMBER(10, 6),
    CZ               NUMBER(10, 6),
    ID1              NUMBER(6, 0),
    ID2              NUMBER(6, 0),
    ID3              NUMBER(6, 0),
    ID4              NUMBER(6, 0),
    ID5              NUMBER(6, 0),
    X_IMAGE          NUMBER(6, 2),
    Y_IMAGE          NUMBER(6, 2),
    ALPHA_J2000      NUMBER(8, 5),
    DELTA_J2000      NUMBER(8, 5),
    EQUINOX          NUMBER(6, 2),
    U_MAG_AUTO       NUMBER(7, 4),
    U_MAGERR_AUTO    NUMBER(7, 4),
    G_MAG_AUTO       NUMBER(7, 4),
    G_MAGERR_AUTO    NUMBER(7, 4),
    R_MAG_AUTO       NUMBER(7, 4),
    R_MAGERR_AUTO    NUMBER(7, 4),
    I_MAG_AUTO       NUMBER(7, 4),
    I_MAGERR_AUTO    NUMBER(7, 4),
    Z_MAG_AUTO       NUMBER(7, 4),
    Z_MAGERR_AUTO    NUMBER(7, 4),
    U_MAG_APER5      NUMBER(7, 4),
    G_MAG_APER5      NUMBER(7, 4),
    R_MAG_APER5      NUMBER(7, 4),
    I_MAG_APER5      NUMBER(7, 4),
    Z_MAG_APER5      NUMBER(7, 4),
    U_MAG_APER10     NUMBER(7, 4),
    G_MAG_APER10     NUMBER(7, 4),
    R_MAG_APER10     NUMBER(7, 4),
    I_MAG_APER10     NUMBER(7, 4),
    Z_MAG_APER10     NUMBER(7, 4),
    U_REDDENING      NUMBER(7, 4),
    G_REDDENING      NUMBER(7, 4),
    R_REDDENING      NUMBER(7, 4),
    I_REDDENING      NUMBER(7, 4),
    Z_REDDENING      NUMBER(7, 4),
    U_FWHM           NUMBER(6, 2),
    G_FWHM           NUMBER(6, 2),
    R_FWHM           NUMBER(6, 2),
    I_FWHM           NUMBER(6, 2),
    Z_FWHM           NUMBER(6, 2),
    U_CLASS_STAR     NUMBER(3, 2),
    G_CLASS_STAR     NUMBER(3, 2),
    R_CLASS_STAR     NUMBER(3, 2),
    I_CLASS_STAR     NUMBER(3, 2),
    Z_CLASS_STAR     NUMBER(3, 2),
    A_IMAGE          NUMBER(6, 2),
    B_IMAGE          NUMBER(6, 2),
    THETA            NUMBER(5, 2),
    CXX              NUMBER(8, 4),
    CYY              NUMBER(8, 4),
    CXY              NUMBER(8, 4),
    U_FLAGS          NUMBER(3, 0),
    G_FLAGS          NUMBER(3, 0),
    R_FLAGS          NUMBER(3, 0),
    I_FLAGS          NUMBER(3, 0),
    Z_FLAGS          NUMBER(3, 0),
    CONSTRAINT PK_DC0_TRUTH PRIMARY KEY (TRUTH_ID)
)
;



-- 
-- TABLE: DES_ACCOUNTS 
--

CREATE TABLE DES_ACCOUNTS(
    NVO_USER     VARCHAR2(30)    NOT NULL,
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    CONSTRAINT PK_DES_ACCOUNTS PRIMARY KEY (NVO_USER, DES_GROUP)
)
;



-- 
-- TABLE: DES_EXPOSURES 
--

CREATE TABLE DES_EXPOSURES(
    IMAGE_N        NUMBER(10, 0)    NOT NULL,
    RA_CENTER      NUMBER(10, 6),
    DECL_CENTER    NUMBER(10, 6),
    EQUINOX        NUMBER(4, 0),
    LAYER_N        NUMBER(10, 0)    NOT NULL,
    TILE_N         NUMBER(10, 0)    NOT NULL,
    OBS_DATE       TIMESTAMP(6),
    NSA_ID         NUMBER(10, 0),
    QUALITY        NUMBER(10, 2),
    AIRMASS        NUMBER(10, 2),
    GAIN           NUMBER(10, 6),
    RD_NOISE       NUMBER(10, 6),
    CONSTRAINT PK_DES_EXPOSURES PRIMARY KEY (IMAGE_N)
)
;



-- 
-- TABLE: DES_STRIPE82_STDS_V1 
--

CREATE TABLE DES_STRIPE82_STDS_V1(
    STRIPE82_ID    NUMBER(10, 0)    NOT NULL,
    NAME           VARCHAR2(40)     NOT NULL,
    RADEG          NUMBER(10, 6),
    DECDEG         NUMBER(10, 6),
    STDMAG_U       NUMBER(10, 6),
    STDMAG_G       NUMBER(10, 6),
    STDMAG_R       NUMBER(10, 6),
    STDMAG_I       NUMBER(10, 6),
    STDMAG_Z       NUMBER(10, 6),
    STDMAGERR_U    NUMBER(10, 6),
    STDMAGERR_G    NUMBER(10, 6),
    STDMAGERR_R    NUMBER(10, 6),
    STDMAGERR_I    NUMBER(10, 6),
    STDMAGERR_Z    NUMBER(10, 6),
    NOBS_U         NUMBER(10, 6),
    NOBS_G         NUMBER(10, 6),
    NOBS_R         NUMBER(10, 6),
    NOBS_I         NUMBER(10, 6),
    NOBS_Z         NUMBER(10, 6),
    CONSTRAINT PK_DES_STRIPE82_STDS_V1 PRIMARY KEY (STRIPE82_ID)
)
;



-- 
-- TABLE: DEVICE_LIST 
--

CREATE TABLE DEVICE_LIST(
    DEVICE_N        NUMBER(10, 0)     NOT NULL,
    ORIGIN_ID       NUMBER(10, 0),
    INSTALL_DATE    TIMESTAMP(6),
    AMP             NUMBER(2, 0),
    NOTE            VARCHAR2(1000),
    GAIN            NUMBER(10, 0),
    RD_NOISE        NUMBER(10, 0),
    CONSTRAINT PK_DEVICE_LIST PRIMARY KEY (DEVICE_N)
)
;



-- 
-- TABLE: EXPOSURE 
--

CREATE TABLE EXPOSURE(
    ID              NUMBER(10, 0)    NOT NULL,
    NITE            VARCHAR2(20),
    BAND            VARCHAR2(20),
    EXPOSURENAME    VARCHAR2(100),
    EXPOSURETYPE    VARCHAR2(20),
    TELRADEC        VARCHAR2(10),
    TELRA           BINARY_DOUBLE,
    TELDEC          BINARY_DOUBLE,
    TELEQUIN        BINARY_FLOAT,
    HTMID           NUMBER(16, 0),
    CX              BINARY_DOUBLE,
    CY              BINARY_DOUBLE,
    CZ              BINARY_DOUBLE,
    HA              VARCHAR2(20),
    ZD              BINARY_FLOAT,
    AIRMASS         BINARY_FLOAT,
    TELFOCUS        BINARY_FLOAT,
    OBJECT          VARCHAR2(80),
    OBSERVER        VARCHAR2(80),
    PROPID          VARCHAR2(20),
    DETECTOR        VARCHAR2(20),
    DETSIZE         VARCHAR2(40),
    TELESCOPE       VARCHAR2(20),
    OBSERVATORY     VARCHAR2(80),
    LATITUDE        VARCHAR2(80),
    LONGITUDE       VARCHAR2(80),
    ALTITUDE        VARCHAR2(80),
    TIMESYS         VARCHAR2(80),
    DATE_OBS        VARCHAR2(80),
    TIME_OBS        VARCHAR2(80),
    MJD_OBS         BINARY_DOUBLE,
    EXPTIME         BINARY_FLOAT,
    EXPREQUEST      BINARY_FLOAT,
    DARKTIME        BINARY_FLOAT,
    EPOCH           BINARY_FLOAT,
    WINDSPD         VARCHAR2(20),
    WINDDIR         VARCHAR2(20),
    AMBTEMP         VARCHAR2(20),
    HUMIDITY        VARCHAR2(20),
    PRESSURE        VARCHAR2(20),
    SKYVAR          BINARY_FLOAT,
    FLUXVAR         BINARY_FLOAT,
    DIMMSEEING      BINARY_FLOAT,
    PHOTFLAG        NUMBER(2, 0),
    IMAGEHWV        VARCHAR2(80),
    IMAGESWV        VARCHAR2(80),
    CONSTRAINT PK_EXPOSURE PRIMARY KEY (ID)
)
;



-- 
-- TABLE: FILE_LOCATIONS 
--

CREATE TABLE FILE_LOCATIONS(
    IMAGE_NAME     VARCHAR2(80),
    LOCATION_ID    VARCHAR2(8)
)
;



-- 
-- TABLE: FILES 
--

CREATE TABLE FILES(
    IMAGEID           NUMBER(9, 0)      NOT NULL,
    RA                NUMBER(10, 7),
    DEC               NUMBER(10, 7),
    EQUINOX           NUMBER(10, 5),
    FILE_DATE         VARCHAR2(25),
    HTMID             NUMBER(16, 0),
    CX                NUMBER(10, 6),
    CY                NUMBER(10, 6),
    CZ                NUMBER(10, 6),
    CCD_NUMBER        NUMBER(2, 0),
    DEVICE_ID         NUMBER(6, 0),
    NSA_IDENTIFIER    VARCHAR2(80),
    BAND              VARCHAR2(8),
    NITE              VARCHAR2(80),
    IMAGETYPE         VARCHAR2(15),
    RUNIDDESC         VARCHAR2(80),
    IMAGENAME         VARCHAR2(80),
    AIRMASS           NUMBER(4, 3),
    SCAMPNUM          NUMBER(5, 0),
    SCAMPCHI          NUMBER(8, 2),
    SCAMPFLG          NUMBER(2, 0),
    SKYBRITE          NUMBER(9, 3),
    PHOTFLAG          NUMBER(2, 0),
    FWHM              NUMBER(5, 3),
    SKYSIGMA          NUMBER(9, 3),
    QUALITY_FLAG_8    NUMBER(5, 0),
    EXPTIME           NUMBER(6, 2),
    DARKTIME          NUMBER(6, 2),
    GAIN_A            NUMBER(4, 2),
    RDNOISE_A         NUMBER(4, 2),
    GAIN_B            NUMBER(4, 2),
    RDNOISE_B         NUMBER(4, 2),
    OBJECT            VARCHAR2(80),
    OBSERVATORY       VARCHAR2(80),
    TELESCOPE         VARCHAR2(20),
    DETECTOR          VARCHAR2(20),
    OBSERVER          VARCHAR2(80),
    PROPID            VARCHAR2(20),
    HOURANGLE         VARCHAR2(20),
    ZENITHD           VARCHAR2(20),
    WEATHERDATE       VARCHAR2(80),
    WINDSPD           VARCHAR2(20),
    WINDDIR           VARCHAR2(20),
    AMBTEMP           VARCHAR2(20),
    HUMIDITY          VARCHAR2(20),
    PRESSURE          VARCHAR2(20),
    DIMMSEEING        VARCHAR2(20),
    WCSDIM            NUMBER(2, 0),
    CTYPE1            VARCHAR2(10),
    CTYPE2            VARCHAR2(10),
    CRVAL1            NUMBER(10, 7),
    CRVAL2            NUMBER(10, 7),
    CRPIX1            NUMBER(10, 4),
    CRPIX2            NUMBER(10, 4),
    CD1_1             NUMBER(10, 7),
    CD2_1             NUMBER(10, 7),
    CD1_2             NUMBER(10, 7),
    CD2_2             NUMBER(10, 7),
    RADECEQ           NUMBER(10, 5),
    TILENAME          VARCHAR2(50),
    NPIX1             NUMBER(6, 0),
    NPIX2             NUMBER(6, 0),
    NEXTEND           NUMBER(3, 0),
    ARCHIVESITES      VARCHAR2(25)       DEFAULT 'NNNNNNNNNNNN',
    PV1_0             NUMBER(11, 10),
    PV1_1             NUMBER(11, 10),
    PV1_2             NUMBER(11, 10),
    PV1_3             NUMBER(11, 10),
    PV1_4             NUMBER(11, 10),
    PV1_5             NUMBER(11, 10),
    PV1_6             NUMBER(11, 10),
    PV1_7             NUMBER(11, 10),
    PV1_8             NUMBER(11, 10),
    PV1_9             NUMBER(11, 10),
    PV1_10            NUMBER(11, 10),
    PV2_1             NUMBER(11, 10),
    PV2_2             NUMBER(11, 10),
    PV2_3             NUMBER(11, 10),
    PV2_4             NUMBER(11, 10),
    PV2_5             NUMBER(11, 10),
    PV2_6             NUMBER(11, 10),
    PV2_7             NUMBER(11, 10),
    PV2_8             NUMBER(11, 10),
    PV2_9             NUMBER(11, 10),
    PV2_10            NUMBER(11, 10),
    PV2_0             NUMBER(11, 10),
    IMAGECLASS        VARCHAR2(20),
    MD_ELLIPTICITY    NUMBER(6, 4),
    CONSTRAINT PK_FILES_IMAGEID PRIMARY KEY (IMAGEID)
)
;



-- 
-- TABLE: FP_RESPONSE 
--

CREATE TABLE FP_RESPONSE(
    RESPONSE_ID     NUMBER(10, 0)    NOT NULL,
    RUNIDDESC       VARCHAR2(80),
    NITE            VARCHAR2(80),
    TELESCOPE       VARCHAR2(20),
    DETECTOR        VARCHAR2(20),
    CCD_NUMBER      NUMBER(3, 0),
    VAL_RESPONSE    NUMBER(8, 4),
    SIG_RESPONSE    NUMBER(8, 4),
    SOURCE          VARCHAR2(20),
    COMMENTS        VARCHAR2(200),
    CONSTRAINT PK_FP_RESPONSE PRIMARY KEY (RESPONSE_ID)
)
;



-- 
-- TABLE: IMAGE 
--

CREATE TABLE IMAGE(
    ID             NUMBER(10, 0)    NOT NULL,
    RUN            VARCHAR2(100),
    NITE           VARCHAR2(20),
    BAND           VARCHAR2(20),
    TILENAME       VARCHAR2(20),
    IMAGENAME      VARCHAR2(100),
    CCD            NUMBER(3, 0),
    IMAGETYPE      VARCHAR2(20),
    PROJECT        VARCHAR2(20),
    EXPOSUREID     NUMBER(9, 0),
    PARENTID       NUMBER(9, 0),
    AIRMASS        BINARY_FLOAT,
    EXPTIME        BINARY_FLOAT,
    DEVICE_ID      NUMBER(6, 0),
    GAIN_A         BINARY_FLOAT,
    RDNOISE_A      BINARY_FLOAT,
    GAIN_B         BINARY_FLOAT,
    RDNOISE_B      BINARY_FLOAT,
    RA             BINARY_DOUBLE,
    DEC            BINARY_DOUBLE,
    EQUINOX        BINARY_FLOAT,
    HTMID          NUMBER(16, 0),
    CX             BINARY_DOUBLE,
    CY             BINARY_DOUBLE,
    CZ             BINARY_DOUBLE,
    WCSDIM         NUMBER(2, 0),
    CTYPE1         VARCHAR2(10),
    CUNIT1         VARCHAR2(20),
    CRVAL1         BINARY_DOUBLE,
    CRPIX1         BINARY_DOUBLE,
    CD1_1          BINARY_DOUBLE,
    CD1_2          BINARY_DOUBLE,
    PV1_1          BINARY_DOUBLE,
    PV1_2          BINARY_DOUBLE,
    PV1_3          BINARY_DOUBLE,
    PV1_4          BINARY_DOUBLE,
    PV1_5          BINARY_DOUBLE,
    PV1_6          BINARY_DOUBLE,
    PV1_7          BINARY_DOUBLE,
    PV1_8          BINARY_DOUBLE,
    PV1_9          BINARY_DOUBLE,
    PV1_10         BINARY_DOUBLE,
    CTYPE2         VARCHAR2(10),
    CUNIT2         VARCHAR2(20),
    CRVAL2         BINARY_DOUBLE,
    CRPIX2         BINARY_DOUBLE,
    CD2_1          BINARY_DOUBLE,
    CD2_2          BINARY_DOUBLE,
    PV2_1          BINARY_DOUBLE,
    PV2_2          BINARY_DOUBLE,
    PV2_3          BINARY_DOUBLE,
    PV2_4          BINARY_DOUBLE,
    PV2_5          BINARY_DOUBLE,
    PV2_6          BINARY_DOUBLE,
    PV2_7          BINARY_DOUBLE,
    PV2_8          BINARY_DOUBLE,
    PV2_9          BINARY_DOUBLE,
    PV2_10         BINARY_DOUBLE,
    SKYBRITE       BINARY_FLOAT,
    SKYSIGMA       BINARY_FLOAT,
    ELLIPTICITY    BINARY_FLOAT,
    FWHM           BINARY_FLOAT,
    SCAMPNUM       NUMBER(5, 0),
    SCAMPCHI       BINARY_FLOAT,
    SCAMPFLG       NUMBER(2, 0),
    CONSTRAINT PK_IMAGE PRIMARY KEY (ID)
)
;



-- 
-- TABLE: IMAGETYPE_ACCESS 
--

CREATE TABLE IMAGETYPE_ACCESS(
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    IMAGETYPE    VARCHAR2(80)    NOT NULL,
    CONSTRAINT PK_IMAGETYPE_ACCESS PRIMARY KEY (DES_GROUP, IMAGETYPE)
)
;



-- 
-- TABLE: IMSIM2_TRUTH 
--

CREATE TABLE IMSIM2_TRUTH(
    IMSIM2_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID     NUMBER(10, 0),
    CLASS              CHAR(1),
    RA                 NUMBER(10, 7),
    DEC                NUMBER(10, 7),
    U_MAG              NUMBER(7, 4),
    G_MAG              NUMBER(7, 4),
    R_MAG              NUMBER(7, 4),
    I_MAG              NUMBER(7, 4),
    Z_MAG              NUMBER(7, 4),
    HTMID              NUMBER(16, 0),
    CX                 NUMBER(10, 6),
    CY                 NUMBER(10, 6),
    CZ                 NUMBER(10, 6),
    DATAFILE_NAME      VARCHAR2(40),
    CONSTRAINT PK_IMSIM2_TRUTH PRIMARY KEY (IMSIM2_TRUTH_ID)
)
;



-- 
-- TABLE: LOCATION 
--

CREATE TABLE LOCATION(
    ID              NUMBER(10, 0)    NOT NULL,
    FILECLASS       VARCHAR2(20),
    FILETYPE        VARCHAR2(20),
    FILENAME        VARCHAR2(200),
    FILEDATE        DATE,
    FILESIZE        NUMBER(12, 0),
    RUN             VARCHAR2(100),
    NITE            VARCHAR2(20),
    BAND            VARCHAR2(20),
    TILENAME        VARCHAR2(20),
    EXPOSURENAME    VARCHAR2(100),
    CCD             NUMBER(3, 0),
    PROJECT         VARCHAR2(20),
    ARCHIVESITES    VARCHAR2(30),
    CONSTRAINT PK_LOCATION PRIMARY KEY (ID)
)
;





-- 
-- TABLE: NOMAD 
--

CREATE TABLE NOMAD(
    NOMAD_ID         NUMBER(11, 0)    NOT NULL,
    RA               NUMBER(10, 7),
    DEC              NUMBER(10, 7),
    SRA              NUMBER(6, 3),
    SDE              NUMBER(6, 3),
    MURA             NUMBER(6, 3),
    MUDEC            NUMBER(6, 3),
    SMURA            NUMBER(6, 3),
    SMUDEC           NUMBER(6, 3),
    EPOCHRA          NUMBER(10, 5),
    EPOCHDEC         NUMBER(10, 5),
    B                NUMBER(8, 4),
    V                NUMBER(8, 4),
    R                NUMBER(8, 4),
    J                NUMBER(8, 4),
    H                NUMBER(8, 4),
    K                NUMBER(8, 4),
    USNOBID          NUMBER,
    TWO_MASSID       NUMBER,
    YB86ID           NUMBER,
    UCAC2ID          NUMBER,
    TYCHO2ID         NUMBER,
    FLAGS            NUMBER,
    HTMID            NUMBER(16, 0),
    CX               NUMBER(10, 6),
    CY               NUMBER(10, 6),
    CZ               NUMBER(10, 6),
    DATAFILE_NAME    VARCHAR2(50),
    CONSTRAINT PK_NOMAD_STAGING PRIMARY KEY (NOMAD_ID)
)
;



-- 
-- TABLE: OBJECTS 
--
-- Modified by Dora on 07/04/08, to add 7 more columns.

CREATE TABLE OBJECTS(
    OBJECT_ID          NUMBER(11, 0)    NOT NULL,
    EQUINOX            NUMBER(6, 2),
    BAND               VARCHAR2(10),
    HTMID              NUMBER(16, 0),
    CX                 NUMBER(10, 6),
    CY                 NUMBER(10, 6),
    CZ                 NUMBER(10, 6),
    PARENTID           NUMBER(11, 0),
    SOFTID             NUMBER(4, 0),
    IMAGEID            NUMBER(9, 0),
    ZEROPOINT          NUMBER(7, 4),
    ERRZEROPOINT       NUMBER(7, 4),
    ZEROPOINTID        NUMBER(10, 0),
    OBJECT_NUMBER      NUMBER(6, 0),
    MAG_AUTO           NUMBER(7, 4),
    MAGERR_AUTO        NUMBER(7, 4),
    MAG_APER_1         NUMBER(7, 4),
    MAGERR_APER_1      NUMBER(7, 4),
    MAG_APER_2         NUMBER(7, 4),
    MAGERR_APER_2      NUMBER(7, 4),
    MAG_APER_3         NUMBER(7, 4),
    MAGERR_APER_3      NUMBER(7, 4),
    MAG_APER_4         NUMBER(7, 4),
    MAGERR_APER_4      NUMBER(7, 4),
    MAG_APER_5         NUMBER(7, 4),
    MAGERR_APER_5      NUMBER(7, 4),
    MAG_APER_6         NUMBER(7, 4),
    MAGERR_APER_6      NUMBER(7, 4),
    ALPHA_J2000        NUMBER(8, 5),
    DELTA_J2000        NUMBER(8, 5),
    ALPHAPEAK_J2000    NUMBER(8, 5),
    DELTAPEAK_J2000    NUMBER(8, 5),
    X2_WORLD           NUMBER(10, 2),
    ERRX2_WORLD        NUMBER(10, 2),
    Y2_WORLD           NUMBER(10, 2),
    ERRY2_WORLD        NUMBER(10, 2),
    XY_WORLD           NUMBER(10, 2),
    ERRXY_WORLD        NUMBER(10, 2),
    THRESHOLD          NUMBER(6, 2),
    X_IMAGE            NUMBER(6, 2),
    Y_IMAGE            NUMBER(6, 2),
    XMIN_IMAGE         NUMBER(4, 0),
    YMIN_IMAGE         NUMBER(4, 0),
    XMAX_IMAGE         NUMBER(4, 0),
    YMAX_IMAGE         NUMBER(4, 0),
    X2_IMAGE           NUMBER(10, 2),
    ERRX2_IMAGE        NUMBER(10, 2),
    Y2_IMAGE           NUMBER(10, 2),
    ERRY2_IMAGE        NUMBER(10, 2),
    XY_IMAGE           NUMBER(10, 2),
    ERRXY_IMAGE        NUMBER(10, 2),
    A_IMAGE            NUMBER(6, 2),
    ERRA_IMAGE         NUMBER(6, 2),
    B_IMAGE            NUMBER(6, 2),
    ERRB_IMAGE         NUMBER(6, 2),
    THETA_IMAGE        NUMBER(4, 2),
    ERRTHETA_IMAGE     NUMBER(4, 2),
    ELLIPTICITY        NUMBER(5, 4),
    CLASS_STAR         NUMBER(3, 2),
    FLAGS              NUMBER(3, 0),
    flux_radius binary_float,
    fwhm_world binary_float,
    isoarea_world binary_float,
    theta_j2000 binary_float,
    background binary_float,
    run varchar2(30),
    catalogid number(9),
    CONSTRAINT PK_OBJECTS PRIMARY KEY (OBJECT_ID)
)
;



-- 
-- TABLE: PSMFIT 
--

CREATE TABLE PSMFIT(
    PSMFIT_ID          NUMBER(5, 0)      NOT NULL,
    NITE               VARCHAR2(12),
    MJDLO              NUMBER(15, 10),
    MJDHI              NUMBER(15, 10),
    CCDID              NUMBER(4, 0),
    FILTER             VARCHAR2(8),
    A                  NUMBER(10, 6),
    AERR               NUMBER(10, 6),
    B                  NUMBER(10, 6),
    BERR               NUMBER(10, 6),
    K                  NUMBER(10, 6),
    KERR               NUMBER(10, 6),
    RMS                NUMBER(10, 6),
    CHI2               NUMBER(10, 6),
    DOF                NUMBER(12, 0),
    PHOTOMETRICFLAG    NUMBER(2, 0),
    PSMVERSION         VARCHAR2(20),
    FIT_TIMESTAMP      TIMESTAMP(6),
    CFILTER            VARCHAR2(8),
    STDCOLOR0          NUMBER(10, 6),
    ASOLVE             NUMBER(2, 0),
    BSOLVE             NUMBER(2, 0),
    KSOLVE             NUMBER(2, 0),
    RUN                VARCHAR2(100),
    CONSTRAINT PK_PSMFIT PRIMARY KEY (PSMFIT_ID)
)
;



-- 
-- TABLE: RUNIDDESC_ACCESS 
--

CREATE TABLE RUNIDDESC_ACCESS(
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    RUNIDDESC    VARCHAR2(80)    NOT NULL,
    CONSTRAINT PK_RUNIDDESC_ACCESS PRIMARY KEY (DES_GROUP, RUNIDDESC)
)
;



-- 
-- TABLE: SHAPELET_OBJECTS 
--

CREATE TABLE SHAPELET_OBJECTS(
    OBJECT_ID    NUMBER(11, 0)    NOT NULL,
    MAG_AUTO     BINARY_FLOAT,
    FLAGS        NUMBER(10, 0),
    SIGMA        BINARY_FLOAT,
    COEFF1       BINARY_FLOAT,
    COEFF2       BINARY_FLOAT,
    COEFF3       BINARY_FLOAT,
    COEFF4       BINARY_FLOAT,
    COEFF5       BINARY_FLOAT,
    COEFF6       BINARY_FLOAT
)
;



-- 
-- TABLE: SHAPELET_PSF 
--

CREATE TABLE SHAPELET_PSF(
    OBJECT_ID    NUMBER(11, 0)    NOT NULL,
    MAG_AUTO     BINARY_FLOAT,
    FLAGS        NUMBER(10, 0),
    SIGMA        BINARY_FLOAT,
    COEFF1       BINARY_FLOAT,
    COEFF2       BINARY_FLOAT,
    COEFF3       BINARY_FLOAT,
    COEFF4       BINARY_FLOAT,
    COEFF5       BINARY_FLOAT,
    COEFF6       BINARY_FLOAT,
    COEFF7       BINARY_FLOAT,
    COEFF8       BINARY_FLOAT,
    COEFF9       BINARY_FLOAT,
    COEFF10      BINARY_FLOAT,
    COEFF11      BINARY_FLOAT,
    COEFF12      BINARY_FLOAT,
    COEFF13      BINARY_FLOAT,
    COEFF14      BINARY_FLOAT,
    COEFF15      BINARY_FLOAT
)
;



-- 
-- TABLE: SITES 
--

CREATE TABLE SITES(
    SITE_NAME            VARCHAR2(32)     NOT NULL,
    SITE_ID              NUMBER(3, 0)     NOT NULL,
    LOGIN_HOST           VARCHAR2(255),
    LOGIN_GSISSH_PORT    VARCHAR2(8),
    GRID_HOST            VARCHAR2(255),
    GRID_PORT            VARCHAR2(8),
    GRID_TYPE            VARCHAR2(8),
    GRIDFTP_HOST         VARCHAR2(255),
    GRIDFTP_PORT         VARCHAR2(8),
    COMPUTE_PPN          NUMBER(3, 0),
    BATCH_TYPE           VARCHAR2(16),
    CONSTRAINT SYS_C005678 PRIMARY KEY (SITE_NAME)
)
;



-- 
-- TABLE: SOFTWARE 
--

CREATE TABLE SOFTWARE(
    SW_VERSION_N    NUMBER(10, 0)    NOT NULL,
    RELEASE_DATE    TIMESTAMP(6),
    CVS_VERSION     VARCHAR2(20),
    NOTES           VARCHAR2(500),
    CONSTRAINT PK_SOFTWARE PRIMARY KEY (SW_VERSION_N)
)
;



-- 
-- TABLE: STANDARD_STARS 
--

CREATE TABLE STANDARD_STARS(
    STANDARD_STAR_ID    NUMBER(10, 0)    NOT NULL,
    NAME                VARCHAR2(40)     NOT NULL,
    RADEG               NUMBER(10, 6),
    DECDEG              NUMBER(10, 6),
    STDMAG_U            NUMBER(10, 6),
    STDMAG_G            NUMBER(10, 6),
    STDMAG_R            NUMBER(10, 6),
    STDMAG_I            NUMBER(10, 6),
    STDMAG_Z            NUMBER(10, 6),
    STDMAGERR_U         NUMBER(10, 6),
    STDMAGERR_G         NUMBER(10, 6),
    STDMAGERR_R         NUMBER(10, 6),
    STDMAGERR_I         NUMBER(10, 6),
    STDMAGERR_Z         NUMBER(10, 6),
    NOBS_U              NUMBER(10, 6),
    NOBS_G              NUMBER(10, 6),
    NOBS_R              NUMBER(10, 6),
    NOBS_I              NUMBER(10, 6),
    NOBS_Z              NUMBER(10, 6),
    FIELDNAME           VARCHAR2(40),
    VERSION_ID          NUMBER(10, 6),
    CONSTRAINT PK_STANDARD_STARS PRIMARY KEY (STANDARD_STAR_ID)
)
;



-- 
-- TABLE: SURVEY_TILES 
--

CREATE TABLE SURVEY_TILES(
    LAYER_N        NUMBER(10, 0)    NOT NULL,
    TILE_N         NUMBER(10, 0)    NOT NULL,
    RA_CENTER      NUMBER(10, 6),
    DECL_CENTER    NUMBER(10, 6),
    EQUINOX        NUMBER(4, 0),
    CONSTRAINT PK_SURVEY_TILES PRIMARY KEY (LAYER_N, TILE_N)
)
;



-- 
-- TABLE: UCAC2 
--

CREATE TABLE UCAC2(
    UCAC2_ID      NUMBER(9, 0)     NOT NULL,
    RA            NUMBER(10, 7),
    DEC           NUMBER(10, 7),
    U2RMAG        NUMBER(5, 0),
    E_RAM         NUMBER(3, 0),
    E_DEM         NUMBER(3, 0),
    NOBS          NUMBER(3, 0),
    E_POS         NUMBER(3, 0),
    NCAT          NUMBER(3, 0),
    CFLG          NUMBER(3, 0),
    EPRAM         NUMBER(6, 0),
    EPDEM         NUMBER(6, 0),
    PMRA          NUMBER(10, 0),
    PMDEC         NUMBER(10, 0),
    E_PMRA        NUMBER(3, 0),
    E_PMDEC       NUMBER(3, 0),
    Q_PMRA        NUMBER(3, 0),
    Q_PMDEC       NUMBER(3, 0),
    TWOMASS_ID    NUMBER(10, 0),
    TWOMASS_J     NUMBER(6, 0),
    TWOMASS_H     NUMBER(6, 0),
    TWOMASS_KS    NUMBER(6, 0),
    TWOMASS_PH    VARCHAR2(4),
    TWOMASS_CC    VARCHAR2(4),
    HTMID         NUMBER(16, 0),
    CX            NUMBER(10, 6),
    CY            NUMBER(10, 6),
    CZ            NUMBER(10, 6),
    CONSTRAINT PK_UCAC2 PRIMARY KEY (UCAC2_ID)
)
;



-- 
-- TABLE: USER_REGISTRATION 
--

CREATE TABLE USER_REGISTRATION(
    NVO_USER               VARCHAR2(30)     NOT NULL,
    FIRST_NAME             VARCHAR2(30),
    LAST_NAME              VARCHAR2(30),
    AFFILIATION            VARCHAR2(100),
    TELEPHONE              VARCHAR2(30),
    EMAIL                  VARCHAR2(30),
    ADMIN_ROLE             CHAR(1),
    WEBMASTER_ROLE         CHAR(1),
    PREFER_ARCHIVE_SITE    VARCHAR2(8),
    COUNTRY                VARCHAR2(20),
    CONSTRAINT PK_USER_REGISTRATION PRIMARY KEY (NVO_USER)
)
;



-- 
-- TABLE: USNOB_CAT1 
--

CREATE TABLE USNOB_CAT1(
    USNOB_CAT1_ID       NUMBER(11, 0)    NOT NULL,
    STAR_ID             VARCHAR2(13),
    RA                  NUMBER(10, 7),
    DEC                 NUMBER(10, 7),
    SRA                 NUMBER(5, 2),
    SDE                 NUMBER(5, 2),
    EPOCH               NUMBER(9, 4),
    MURA                NUMBER(6, 2),
    MUDEC               NUMBER(6, 2),
    MUPROB              NUMBER(5, 2),
    SMURA               NUMBER(5, 2),
    SMUDE               NUMBER(5, 2),
    SFITRA              NUMBER(6, 2),
    SFITDE              NUMBER(6, 2),
    NFITPT              NUMBER(1, 0),
    FLAGS               NUMBER(1, 0),
    B1                  NUMBER(6, 2),
    B1_MAGFLG           NUMBER(2, 0),
    B1_FLDID            NUMBER(4, 0),
    B1_SG               NUMBER(2, 0),
    B1_XRESID           NUMBER(5, 2),
    B1_YRESID           NUMBER(5, 2),
    R1                  NUMBER(6, 2),
    R1_MAGFLG           NUMBER(2, 0),
    R1_FLDID            NUMBER(4, 0),
    R1_SG               NUMBER(2, 0),
    R1_XRESID           NUMBER(5, 2),
    R1_YRESID           NUMBER(5, 2),
    B2                  NUMBER(6, 2),
    B2_MAGFLG           NUMBER(2, 0),
    B2_FLDID            NUMBER(4, 0),
    B2_SG               NUMBER(2, 0),
    B2_XRESID           NUMBER(5, 2),
    B2_YRESID           NUMBER(5, 2),
    R2                  NUMBER(6, 2),
    R2_MAGFLG           NUMBER(2, 0),
    R2_FLDID            NUMBER(4, 0),
    R2_SG               NUMBER(2, 0),
    R2_XRESID           NUMBER(5, 2),
    R2_YRESID           NUMBER(5, 2),
    I2                  NUMBER(6, 2),
    I2_MAGFLG           NUMBER(2, 0),
    I2_FLDID            NUMBER(4, 0),
    I2_SG               NUMBER(2, 0),
    I2_XRESID           NUMBER(5, 2),
    I2_YRESID           NUMBER(5, 2),
    XI                  NUMBER(7, 4),
    ETA                 NUMBER(7, 4),
    DISTCTR             NUMBER(7, 4),
    CX                  NUMBER(10, 6),
    CY                  NUMBER(10, 6),
    CZ                  NUMBER(10, 6),
    HTMID               NUMBER(16, 0),
    LOADING_FILENAME    VARCHAR2(30),
    OLD_RA              NUMBER(10, 7),
    CONSTRAINT PK_USNOB_CAT1 PRIMARY KEY (USNOB_CAT1_ID)
)
;



-- 
-- TABLE: ZEROPOINT 
--

CREATE TABLE ZEROPOINT(
    ZP_N         NUMBER(10, 0)    NOT NULL,
    ORIGIN_ID    NUMBER(10, 0),
    ADD_DATE     TIMESTAMP(6),
    IMAGE_N      NUMBER(10, 0)    NOT NULL,
    MAG_ZERO     NUMBER(10, 6),
    SIGMA_ZP     NUMBER(10, 6),
    SOURCE_ZP    NUMBER(10, 6),
    B            NUMBER(10, 6),
    BERR         NUMBER(10, 6),
    CONSTRAINT PK_ZEROPOINT PRIMARY KEY (ZP_N)
)
;



-- 
-- TABLE: DES_EXPOSURES 
--

ALTER TABLE DES_EXPOSURES ADD CONSTRAINT FK_DES_EXPOSURES1 
    FOREIGN KEY (LAYER_N, TILE_N)
    REFERENCES SURVEY_TILES(LAYER_N, TILE_N)
;


-- 
-- TABLE: ZEROPOINT 
--

ALTER TABLE ZEROPOINT ADD CONSTRAINT FK_ZEROPOINT1 
    FOREIGN KEY (IMAGE_N)
    REFERENCES DES_EXPOSURES(IMAGE_N)
;

--
-- TABLE: MATCHES
-- Dora Cai added on 05/22/08
-- 

CREATE TABLE MATCHES(
    COADD_OBJECTS_ID  NUMBER(11, 0)    NOT NULL,
    OBJECT_ID    NUMBER(11,0) NOT NULL,
    OFFSET	 BINARY_FLOAT,
    SIGMA_OFFSET BINARY_FLOAT,
    CONSTRAINT PK_MATCHES PRIMARY KEY (COADD_OBJECTS_ID, OBJECT_ID)
);

--
-- TABLE: OBJECTS_2008
-- Dora Cai added on 05/27/08
-- Modified by Dora on 07/04/08 to add 1 column, catalogid.
-- 

CREATE TABLE OBJECTS_2008(
OBJECT_ID               Number(11) NOT NULL,
EQUINOX                 BINARY_FLOAT,
BAND                    varchar2(10),
HTMID                   Number(16),
CX                      BINARY_DOUBLE,
CY                      BINARY_DOUBLE,
CZ                      BINARY_DOUBLE,
PARENTID                Number(11),
SOFTID                  Number(4),
IMAGEID                 Number(9),
ZEROPOINT               BINARY_FLOAT,
ERRZEROPOINT            BINARY_FLOAT,
ZEROPOINTID             Number(10),
OBJECT_NUMBER           Number(6),
MAG_AUTO                BINARY_FLOAT,
MAGERR_AUTO             BINARY_FLOAT,
MAG_APER_1              BINARY_FLOAT,
MAGERR_APER_1           BINARY_FLOAT,
MAG_APER_2              BINARY_FLOAT,
MAGERR_APER_2           BINARY_FLOAT,
MAG_APER_3              BINARY_FLOAT,
MAGERR_APER_3           BINARY_FLOAT,
MAG_APER_4              BINARY_FLOAT,
MAGERR_APER_4           BINARY_FLOAT,
MAG_APER_5              BINARY_FLOAT,
MAGERR_APER_5           BINARY_FLOAT,
MAG_APER_6              BINARY_FLOAT,
MAGERR_APER_6           BINARY_FLOAT,
ALPHA_J2000             BINARY_DOUBLE,
DELTA_J2000             BINARY_DOUBLE,
ALPHAPEAK_J2000         BINARY_DOUBLE,
DELTAPEAK_J2000         BINARY_DOUBLE,
X2_WORLD                Binary_FLOAT,
ERRX2_WORLD             BINARY_DOUBLE,
Y2_WORLD                Binary_FLOAT,
ERRY2_WORLD             BINARY_DOUBLE,
XY_WORLD                Binary_FLOAT,
ERRXY_WORLD             BINARY_DOUBLE,
THRESHOLD               Binary_FLOAT,
X_IMAGE                 Binary_FLOAT,
Y_IMAGE                 Binary_FLOAT,
XMIN_IMAGE              Binary_FLOAT,
YMIN_IMAGE              Binary_FLOAT,
XMAX_IMAGE              Binary_FLOAT,
YMAX_IMAGE              Binary_FLOAT,
X2_IMAGE                Binary_FLOAT,
ERRX2_IMAGE             Binary_FLOAT,
Y2_IMAGE                Binary_FLOAT,
ERRY2_IMAGE             Binary_FLOAT,
XY_IMAGE                Binary_FLOAT,
ERRXY_IMAGE             Binary_FLOAT,
A_IMAGE                 Binary_FLOAT,
ERRA_IMAGE              Binary_FLOAT,
B_IMAGE                 Binary_FLOAT,
ERRB_IMAGE              Binary_FLOAT,
THETA_IMAGE             Binary_FLOAT,
ERRTHETA_IMAGE          Binary_FLOAT,
ELLIPTICITY             Binary_FLOAT,
CLASS_STAR              Binary_FLOAT,
FLAGS                   Number(3),
FLUX_RADIUS             Binary_FLOAT,
FWHM_WORLD              Binary_FLOAT,
ISOAREA_WORLD           Binary_FLOAT,
THETA_J2000             Binary_FLOAT,
BACKGROUND              Binary_FLOAT, 
RUN			Varchar2(30),
catalogid               Number(9),
Constraint PK_OBJECTS_2008 PRIMARY KEY (OBJECT_ID))
partition by list(run)
(partition init_partition values('before2008'));

-- 
-- TABLE: NITE_ACCESS 
-- Dora Cai added a new column, project, on 05/29/08
--

CREATE TABLE NITE_ACCESS(
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    NITE         VARCHAR2(80)    NOT NULL,
    PROJECT 	 VARCHAR2(20)	 NOT NULL,
    CONSTRAINT PK_NITE_ACCESS PRIMARY KEY (DES_GROUP, NITE, PROJECT)
)
;

-- 
-- TABLE: Spec_Standards 
-- Dora Cai created the new table on 06/25/08
--

CREATE TABLE Spec_Standards (
    spec_id      number(11) NOT NULL,
    source_id    number(20),
    ra           binary_double,
    dec          binary_double,
    zspec        binary_float,
    zspec_err    binary_float,
    zspec_min    binary_float,
    zspec_max    binary_float,
    source_type  varchar2(10),
    confidence	 number(2),
    u            binary_float,
    u_err        binary_float,
    g            binary_float,
    g_err        binary_float,
    r            binary_float,
    r_err        binary_float,
    i            binary_float,
    i_err        binary_float,
    z            binary_float,
    z_err        binary_float,
    u_jkc        binary_float,
    u_jkc_err    binary_float,
    g_jkc        binary_float,
    g_jkc_err    binary_float,
    r_jkc        binary_float,
    r_jkc_err    binary_float,
    i_jkc        binary_float,
    i_jkc_err    binary_float,
    k_jkc        binary_float,
    k_jkc_err    binary_float,
    source 	varchar2(40),
    constraint pk_spec_standards primary key (spec_id))
;
-- 
-- TABLE: SNScan 
-- Dora Cai created the new table on 07/02/08
--
CREATE TABLE SNScan (
SNScanId number(9),
SNId number(9),
SNObsId number(9),
ScanDate date,
CategoryType varchar2(20),
Scanner varchar2(20),
constraint pk_snscan primary key (SNScanId));

create sequence snscan_seq cache 100 start with 1 increment by 1;


-- 
-- TABLE: coadd_objects 
-- Dora Cai modified the data types and add two more columns: errx2_world and erry2_world, on 07/02/08
-- Dora Cai added 4 columns on 07/04/08: catalogid_g, catalogid_r, catalogid_i, catalogid_z)

create table coadd_objects (
 COADD_OBJECTS_ID                         NUMBER(11),
 EQUINOX                                  BINARY_FLOAT,
 HTMID                                    NUMBER(16),
 CX                                       BINARY_DOUBLE,
 CY                                       BINARY_DOUBLE,
 CZ                                       BINARY_DOUBLE,
 SOFTID                                   NUMBER(4),
 IMAGEID_G                                NUMBER(9),
 IMAGEID_R                                NUMBER(9),
 IMAGEID_I                                NUMBER(9),
 IMAGEID_Z                                NUMBER(9),
 ZEROPOINT_G                              BINARY_FLOAT,
 ZEROPOINT_R                              BINARY_FLOAT,
 ZEROPOINT_I                              BINARY_FLOAT,
 ZEROPOINT_Z                              BINARY_FLOAT,
 ERRZEROPOINT_G                           BINARY_FLOAT,
 ERRZEROPOINT_R                           BINARY_FLOAT,
 ERRZEROPOINT_I                           BINARY_FLOAT,
 ERRZEROPOINT_Z                           BINARY_FLOAT,
 ZEROPOINTID_G                            NUMBER(10),
 ZEROPOINTID_R                            NUMBER(10),
 ZEROPOINTID_I                            NUMBER(10),
 ZEROPOINTID_Z                            NUMBER(10),
 R_OBJECT_NUMBER                          NUMBER(6),
 MAG_AUTO_G                               BINARY_FLOAT,
 MAGERR_AUTO_G                            BINARY_FLOAT,
 MAG_APER1_G                              BINARY_FLOAT,
 MAGERR_APER1_G                           BINARY_FLOAT,
 MAG_APER2_G                              BINARY_FLOAT,
 MAGERR_APER2_G                           BINARY_FLOAT,
 MAG_APER3_G                              BINARY_FLOAT,
 MAGERR_APER3_G                 	  BINARY_FLOAT,
 MAG_APER4_G                              BINARY_FLOAT,
 MAGERR_APER4_G                           BINARY_FLOAT,
 MAG_APER5_G                              BINARY_FLOAT,
 MAGERR_APER5_G                           BINARY_FLOAT,
 MAG_APER6_G          	 	 	  BINARY_FLOAT,
 MAGERR_APER6_G                           BINARY_FLOAT,
 MAG_AUTO_R                               BINARY_FLOAT,
 MAGERR_AUTO_R                            BINARY_FLOAT,
 MAG_APER1_R                              BINARY_FLOAT,
 MAGERR_APER1_R                           BINARY_FLOAT,
 MAG_APER2_R                              BINARY_FLOAT,
 MAGERR_APER2_R                           BINARY_FLOAT,
 MAG_APER3_R                              BINARY_FLOAT,
 MAGERR_APER3_R                           BINARY_FLOAT,
 MAG_APER4_R                              BINARY_FLOAT,
 MAGERR_APER4_R                           BINARY_FLOAT,
 MAG_APER5_R                              BINARY_FLOAT,
 MAGERR_APER5_R                           BINARY_FLOAT,
 MAG_APER6_R                              BINARY_FLOAT,
 MAGERR_APER6_R                           BINARY_FLOAT,
 MAG_AUTO_I                               BINARY_FLOAT,
 MAGERR_AUTO_I                            BINARY_FLOAT,
 MAG_APER1_I                              BINARY_FLOAT,
 MAGERR_APER1_I                           BINARY_FLOAT,
 MAG_APER2_I                              BINARY_FLOAT,
 MAGERR_APER2_I                           BINARY_FLOAT,
 MAG_APER3_I                              BINARY_FLOAT,
 MAGERR_APER3_I                           BINARY_FLOAT,
 MAG_APER4_I                              BINARY_FLOAT,
 MAGERR_APER4_I                           BINARY_FLOAT,
 MAG_APER5_I                              BINARY_FLOAT,
 MAGERR_APER5_I                           BINARY_FLOAT,
 MAG_APER6_I                              BINARY_FLOAT,
 MAGERR_APER6_I                           BINARY_FLOAT,
 MAG_AUTO_Z                               BINARY_FLOAT,
 MAGERR_AUTO_Z                            BINARY_FLOAT,
 MAG_APER1_Z                              BINARY_FLOAT,
 MAGERR_APER1_Z                           BINARY_FLOAT,
 MAG_APER2_Z                              BINARY_FLOAT,
 MAGERR_APER2_Z                           BINARY_FLOAT,
 MAG_APER3_Z                              BINARY_FLOAT,
 MAGERR_APER3_Z                           BINARY_FLOAT,
 MAG_APER4_Z                              BINARY_FLOAT,
 MAGERR_APER4_Z                           BINARY_FLOAT,
 MAG_APER5_Z                              BINARY_FLOAT,
 MAGERR_APER5_Z                           BINARY_FLOAT,
 MAG_APER6_Z                              BINARY_FLOAT,
 MAGERR_APER6_Z                           BINARY_FLOAT,
 ALPHA_J2000                              BINARY_DOUBLE,
 DELTA_J2000                              BINARY_DOUBLE,
 X2_WORLD                                 BINARY_FLOAT,
 Y2_WORLD                                 BINARY_FLOAT,
 XY_WORLD                                 BINARY_FLOAT,
 THRESHOLD                                BINARY_FLOAT,
 X_IMAGE                                  BINARY_FLOAT,
 Y_IMAGE                                  BINARY_FLOAT,
 THETA_IMAGE_G                            BINARY_FLOAT,
 ERRTHETA_IMAGE_G                         BINARY_FLOAT,
 ELLIPTICITY_G                            BINARY_FLOAT,
 CLASS_STAR_G                             BINARY_FLOAT,
 FLAGS_G                                  BINARY_FLOAT,
 THETA_IMAGE_R                            BINARY_FLOAT,
 ERRTHETA_IMAGE_R                         BINARY_FLOAT,
 ELLIPTICITY_R                            BINARY_FLOAT,
 CLASS_STAR_R                             BINARY_FLOAT,
 FLAGS_R                                  NUMBER(3),
 THETA_IMAGE_I                            BINARY_FLOAT,
 ERRTHETA_IMAGE_I                         BINARY_FLOAT,
 ELLIPTICITY_I                            BINARY_FLOAT,
 CLASS_STAR_I                             BINARY_FLOAT,
 FLAGS_I                                  NUMBER(3),
 THETA_IMAGE_Z                            BINARY_FLOAT,
 ERRTHETA_IMAGE_Z                         BINARY_FLOAT,
 ELLIPTICITY_Z                            BINARY_FLOAT,
 CLASS_STAR_Z                             BINARY_FLOAT,
 FLAGS_Z                                  NUMBER(3),
 ERRX2_WORLD                              BINARY_DOUBLE,
 ERRY2_WORLD                              BINARY_DOUBLE,
 catalogid_g                              number(9),
 catalogid_r                              number(9),
 catalogid_i                              number(9),
 catalogid_z                              number(9),
 CONSTRAINT PK_OBJECTS PRIMARY KEY (COADD_OBJECTS_ID))
storage (initial 2M);

-- 
-- TABLE: SNSpect 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNSpect (
SNSpectId number(9),
SNId number(9),
Req_Date date,
Telescope varchar2(20),
Obs_Date date,
Spec_Eval varchar2(20),
CategoryType varchar2(20),
z_sn binary_float,
z_sn_err binary_float,
z_gal binary_float,
z_gal_err binary_float,
constraint pk_snspect primary key (SNSpectId));

create sequence snspect_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNFit 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNFit (
SNFitId number(9),
SNId number(9),
Version number(9),
Peak_Mjd binary_float,
Peak_MagB binary_float,
Delta binary_float,
Av binary_float,
z binary_float,
chisq binary_float,
constraint pk_snfit primary key (SNFitId));

create sequence snfit_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNGals 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNGals (
SNGalId number(9),
SNId number(9),
g binary_float,
g_err binary_float,
r binary_float,
r_err binary_float,
i binary_float,
i_err binary_float,
z binary_float,
z_err binary_float,
Y binary_float,
Y_err binary_float,
photoz binary_float,
photoz_err binary_float,
specz binary_float,
specz_err binary_float,
constraint pk_sngals primary key (SNGalId));

create sequence sngals_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNObs 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNObs (
SNObjId number(12),
ExposureID number(9),
CoaddID number(9),
Object_type number(6),
Version number(9),
RA binary_double,
Dec binary_double,
flux binary_float,
flux_err binary_float,
chisq binary_float,
constraint pk_snobs primary key (SNObjId));

create sequence snobs_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNCand 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNCand (
SNId number(9),
RA binary_double,
Dec binary_double,
Entry_date date,
Orig_RA binary_double,
Orig_Dec binary_double,
Cand_Type varchar2(20),
constraint pk_sncand primary key (SNId));

create sequence sncand_seq cache 100 start with 1 increment by 1;

