-- Some of bbaker's favorite queries

-- list of tables and views
select table_name, tablespace_name from dba_tables 
    where tablespace_name like 'PORTALS%'
    order by tablespace_name, table_name;
select view_name, owner from dba_views where owner like 'PORTAL%'
    order by owner, view_name;

-- mydb non-zero quotas
select mydb_account.id mydb, des_user.id "user", quota,
       substr(value, 0, 20) nvo_id 
  from mydb_account, des_user, user_detail 
 where mydb_account.des_user_id=des_user.id 
       and des_user.id=user_detail.des_user_id
       and user_detail.name='identity' 
       and quota > 0;

-- db_credential
select id, substr(symbolic_name, 0, 30) symbolic, substr(role, 0, 6) role,
       substr(description, 0, 20) description, substr(host, 0, 35) host,
       substr(sid, 0, 10) sid, substr(db_name, 0, 10) db,
       substr(login, 0, 15) login, substr(pwd, 0, 20)
  from portals_admin.db_credential;

-- user details
select id, des_user_id, substr(name, 0, 15) name,
       substr(subname, 0, 15) subname, substr(value, 0, 100) value
  from user_detail where des_user_id=189;

-- dictionary
select id, tablename, colname, substr(colvalue, 0, 30) colvalue,
       substr(description, 0, 50) description, prettyname
  from dictionary order by id;
