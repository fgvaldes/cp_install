------------------------------------------------------------------
-- copy desproc.procdb.portals_admin tablespace to destest.dvlp --
------------------------------------------------------------------
CREATE SMALLFILE TABLESPACE portals_admin
DATAFILE '/oracle/oradata/dvlp/portals_001.dbf'
SIZE 1000M AUTOEXTEND ON NEXT 500M MAXSIZE UNLIMITED
LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE USER portals_admin PROFILE "DEFAULT" IDENTIFIED BY portalsmgr
DEFAULT TABLESPACE portals_admin
QUOTA UNLIMITED ON portals_admin ACCOUNT UNLOCK;
GRANT DBA TO portals_admin;
ALTER USER portals_admin DEFAULT ROLE DBA;

------------------------------------------------------
-- create portals_admin_devel tablespace on destest --
------------------------------------------------------
CREATE SMALLFILE TABLESPACE portals_admin_devel
DATAFILE '/oracle/oradata/dvlp/portals_devel_001.dbf'
SIZE 1000M AUTOEXTEND ON NEXT 500M MAXSIZE UNLIMITED
LOGGING EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;

CREATE USER portals_admin_devel PROFILE "DEFAULT" IDENTIFIED BY portalsmgr
DEFAULT TABLESPACE portals_admin_devel
QUOTA UNLIMITED ON portals_admin_devel ACCOUNT UNLOCK;
GRANT DBA TO portals_admin_devel;
ALTER USER portals_admin_devel DEFAULT ROLE DBA;

-------------------------------------
-- export portals_admin on desproc --
-------------------------------------

-- 1. create virtual directory
-- [oracle@desproc]$ mkdir /data/oracle/backup/portals_admin
-- as oracle superuser:
create directory portals_admin_dump_dir as '/data/oracle/backup/portals_admin';
grant read,write on directory portals_admin_dump_dir to portals_admin;

-- 2. dump tables to file
-- [oracle@desproc]$ expdp portals_admin/portalsmgr directory=portals_admin_dump_dir dumpfile=portals_admin.20100408.dmp logfile=portals_admin.20100408.log job_name=portals_admin_dump tables=portals_admin.dictionary,portals_admin.sample_queries,portals_admin.user_access,portals_admin.user_profile,portals_admin.des_accounts version=11.1.0.7

-- 3. copy to destest and clean up
-- [oracle@desproc]$ scp /data/oracle/backup/portals_admin/portals_admin.20100408.dmp bbaker@destest:~
-- as oracle superuser:
drop directory portals_admin_dump_dir;

-------------------------------------
-- import portals_admin on destest --
-------------------------------------

-- 1. create virtual directory and put dump file in it
-- [oracle@destest]$ mkdir /oracle/backup/portals_admin
-- [bbaker@destest]$ sudo mv portals_admin.20100408.dmp /oracle/backup/portals_admin
-- [bbaker@destest]$ sudo chown -R oracle.oinstall /oracle/backup/portals_admin
-- as oracle superuser:
create directory portals_admin_dump_dir as '/oracle/backup/portals_admin';
grant read on directory portals_admin_dump_dir to portals_admin;

-- 2. import tables into it
-- [oracle@destest]$ impdp portals_admin/portalsmgr dumpfile=portals_admin.20100408.dmp directory=portals_admin_dump_dir tables=portals_admin.dictionary,portals_admin.sample_queries,portals_admin.user_access,portals_admin.user_profile,portals_admin.des_accounts remap_schema=procdb:dvlp remap_tablespace=portals:portals_admin
-- there will be some errors; it will end with something like this:
-- Job "PORTALS_ADMIN"."SYS_IMPORT_TABLE_01" completed with 8 error(s) at 17:22:32

-- 3. clean up
-- as oracle superuser:
drop directory portals_admin_dump_dir
