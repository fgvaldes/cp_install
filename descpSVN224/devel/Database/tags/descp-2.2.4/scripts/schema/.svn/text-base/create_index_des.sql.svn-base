
-- OBJECTS TABLE
create index objects_imageid_idx on objects(imageid)
parallel 2
;       

create index objects_htmid_idx on objects(htmid)
parallel 4
;

create index objects_ra_dec_idx on objects(ra,dec)
parallel 4
;

create index objects_catalogid_idx on objects(catalogid)
parallel 4
;       

-- FILES TABLE
create index files_nite_ccd_idx on files(nite,ccd_number)
parallel 2
 ;

create index files_ra_dec_idx on files(ra,dec)
parallel 2
 ;

--USNOB TABLE under pipeline user
create index usnob-ra_dec_idx on usnob(ra,dec)
parallel 2
;

--CATSIM1_TRUTH TABLE
create index catsim1truth_ra_dec_idx on catsim1_truth(ra,dec)
parallel 2
;

create index catsim1truth_htmid_idx on catsim1_truth(htmid)
parallel 2
;

--USNOB_CAT1 TABLE
create index usnobcat1_ra_dec_idx on usnob_cat1(ra,dec)
parallel 4 
; 

create index usnobcat1_htmid_idx on usnob_cat1(htmid)
parallel 4
;

--NOMAD TABLE
create index nomad_ra_dec_idx on nomad(ra,dec)
parallel 4
;

create index nomad_htmid_idx on nomad(htmid)
parallel 4
;

--COADD_OBJECTS
create index coaddobj_ra_dec_idx on coadd_objects(ra,dec)
parallel 4 ;

create index coaddobj_imageid_g_idx on coadd_objects(imageid_g)
parallel 4 ;

create index coaddobj_imageid_r_idx on coadd_objects(imageid_r)
parallel 4 ;

create index coaddobj_imageid_i_idx on coadd_objects(imageid_i)
parallel 4 ;

create index coaddobj_imageid_z_idx on coadd_objects(imageid_z)
parallel 4 ;

create index coaddobj_imageid_y_idx on coadd_objects(imageid_y)
parallel 4 ;

-- Dora 09/05/07 index on files.runiddesc
create index files_runiddesc_idx on files(runiddesc)
parallel 4 ;

-- Dora 01/02/08 index on catsim3_truth
create index catsim3_ra_dec_idx on catsim3_truth(ra,dec)
parallel 4
;

create index location_run_idx on location(run)
;

create index location_nite_idx on location(nite)
;

create index location_tilename_idx on location(tilename)
parallel 4
;

create index image_run_idx on image(run)
;

create index image_radec_idx on image(ra, dec)
parallel 4
;

create index image_parentid_idx on image(parentid)
parallel 4; 

create index image_nite_idx on image(nite)
;

create index catalog_run_idx on catalog(run)
;

create index catalog_nite_idx on catalog(nite)
;

create index catalog_parentid_idx on catalog(parentid)
parallel 4;

create index exposure_nite_idx on exposure(nite)
;

create index exposure_radec_idx on exposure(telra, teldec)
;

create index coadd_run_idx on coadd(run)
;

create index coadd_radec_idx on coadd(ra, dec)
;

-- OBJECTS_2008 TABLE
create index objects2008_imageid_idx on objects_2008(imageid)
parallel 4
;       

create index objects2008_htmid_idx on objects_2008(htmid)
parallel 4
;

create index objects2008_ra_dec_idx on objects_2008(ra,dec)
parallel 4
;

create index objects2008_catalogid_idx on objects_2008(catalogid)
parallel 4
;       

--DC4_TRUTH TABLE
create index dc4truth_ra_dec_idx on dc4_truth(ra,dec)
parallel 4
;

--DC4_TRUTH TABLE
create index dc4truth_starid_idx on dc4_truth(star_galaxy_id)
parallel 4
;

--zero_point (imageid)
create index zeropoint_imageid_idx on zeropoint(imageid)
parallel 4
;

--zero_point (originid)
create index zeropoint_originid_idx on zeropoint(originid)
parallel 4
;

--run (id)
create unique index run_id_idx on run(id)
;

--wl_findstars (catalogid)
create index wlfindstars_catalogid_idx on wl_findstars(catalogid);

--wl_findstars (object_id)
create index wlfindstars_object_id_idx on wl_findstars(object_id);

--wl_psf (catalogid)
create index wlpsf_catalogid_idx on wl_psf(catalogid);

--wl_psf (object_id)
create index wlpsf_object_id_idx on wl_psf(object_id);

--wl_shear (catalogid)
create index wlshear_catalogid_idx on wl_shear(catalogid);

--wl_shear (object_id)
create index wlshear_object_id_idx on wl_shear(object_id);

-- Dora 05/06/09 add index on OBJECTS_2009 TABLE
-- Dora 06/06/09 change the table name from objects_2009 to objects_current
create index objects2009_imageid_idx on objects_current(imageid)
parallel 4;

create index objects2009_htmid_idx on objects_current(htmid)
parallel 4;

create index objects2009_ra_dec_idx on objects_current(ra,dec)
parallel 4;

create index objects2009_catalogid_idx on objects_current(catalogid)
parallel 4;

-- 09/09/28 Dora changed the index name after the table is renamed
--gsn_aug09_truth TABLE
create index gsnaug09truth_radec_idx on gsn_aug09_truth(ra,dec)
parallel 4;

--gsn_aug09_truth TABLE
create index gsnaug09truth_starid_idx on gsn_aug09_truth(star_galaxy_id)
parallel 4;

-- 08/13/2009 Dora created for speeding up MV building
-- wl(run)
create index wl_run_idx on wl(run)
parallel 4;

-- 08/14/2009 Dora created indexes for objects_dr001
create index dr001_radec_idx on objects_dr001(ra,dec) local
tablespace desdr
parallel 4;

create index dr001_nite_idx on objects_dr001(nite) global
tablespace desdr
parallel 4;

create index dr001_run_idx on objects_dr001(run) global
tablespace desdr
parallel 4;

create index dr001_objectid_idx on objects_dr001(object_id) global
tablespace desdr
parallel 4;

create index dr001_findstarid_idx on objects_dr001(findstar_id) global
tablespace desdr
parallel 4;

create index dr001_shearid_idx on objects_dr001(shear_id) global
tablespace desdr
parallel 4;

-- 08/25/2009 Dora created indexes for coadd_objects
create index coaddobj002_radec_idx on coadd_objects_dr002(ra,dec) local
tablespace desdr
parallel 4;

create index coaddobj002_run_idx on coadd_objects_dr002(run) global
tablespace desdr
parallel 4;

create index coaddobj002_objectid_idx on coadd_objects_dr002(coadd_objects_id) global
tablespace desdr
parallel 4;

create index coaddobj003_radec_idx on coadd_objects_dr003(ra,dec) local
tablespace desdr
parallel 4;

create index coaddobj003_run_idx on coadd_objects_dr003(run) global
tablespace desdr
parallel 4;

create index coaddobj003_objectid_idx on coadd_objects_dr003(coadd_objects_id) global
tablespace desdr
parallel 4;

create index files002_imgradec_idx on files_dr002(image_ra,image_dec) 
tablespace desdr
parallel 4;

create index files002_cddradec_idx on files_dr002(coadd_ra,coadd_dec) 
tablespace desdr
parallel 4;

create index files002_expradec_idx on files_dr002(telra,teldec) 
tablespace desdr
parallel 4;

create index files002_run_idx on files_dr002(run)
tablespace desdr
parallel 4;

create index files002_id_idx on files_dr002(id)
tablespace desdr
parallel 4;

create index files003_imgradec_idx on files_dr003(image_ra,image_dec) 
tablespace desdr
parallel 4;

create index files003_cddradec_idx on files_dr003(coadd_ra,coadd_dec) 
tablespace desdr
parallel 4;

create index files003_expradec_idx on files_dr003(telra,teldec) 
tablespace desdr
parallel 4;

create index files003_run_idx on files_dr003(run)
tablespace desdr
parallel 4;

create index files003_id_idx on files_dr003(id)
tablespace desdr
parallel 4;

create index objdr002_radec_idx on objects_dr002(ra,dec) local
tablespace desdr
parallel 4;

create index objdr002_nite_idx on objects_dr002(nite) global
tablespace desdr
parallel 4;

create index objdr002_run_idx on objects_dr002(run) global
tablespace desdr
parallel 4;

create index objdr002_objectid_idx on objects_dr002(object_id) global
tablespace desdr
parallel 4;

create index objdr003_radec_idx on objects_dr003(ra,dec) local
tablespace desdr
parallel 4;

create index objdr003_nite_idx on objects_dr003(nite) global
tablespace desdr
parallel 4;

create index objdr003_run_idx on objects_dr003(run) global
tablespace desdr
parallel 4;

create index objdr003_objectid_idx on objects_dr003(object_id) global
tablespace desdr
parallel 4;

-- 09/09/28 Dora changed the index name after the table is renamed
--GSNSIM_AUG09_USNOB TABLE
create index gsnsimaug09_usnob_radec_idx on gsnsim_aug09_usnob(ra,dec)
parallel 4;

--DC5SIM_USNOB TABLE
create index gsnsimaug09_usnob_starid_idx on gsnsim_aug09_usnob(star_galaxy_id)
parallel 4;

--DC5_TRUTH TABLE
create index dc5truth_radec_idx on dc5_truth(ra,dec)
parallel 4;

--DC5_TRUTH TABLE
create index dc5truth_starid_idx on dc5_truth(star_galaxy_id)
parallel 4;

--DC5SIM_USNOB TABLE
create index dc5simusnob_radec_idx on dc5sim_usnob(ra,dec)
parallel 4;

--DC5SIM_USNOB TABLE
create index dc5simusnob_starid_idx on dc5sim_usnob(star_galaxy_id)
parallel 4;

--DC5SIM_USNOB_oct11 TABLE
create index dc5simoct11_radec_idx on dc5sim_usnob_oct11(ra,dec)
parallel 4;

--DC5SIM_USNOB_oct11 TABLE
create index dc5simoct11_starid_idx on dc5sim_usnob_oct11(star_galaxy_id)
parallel 4;

--DC5SIM_USNOB_oct12 TABLE
create index dc5simoct12_radec_idx on dc5sim_usnob_oct12(ra,dec)
parallel 4;

--DC5SIM_USNOB_oct12 TABLE
create index dc5simoct12_starid_idx on dc5sim_usnob_oct12(star_galaxy_id)
parallel 4;

--DC5SIM_USNOB_oct13 TABLE
create index dc5simoct13_radec_idx on dc5sim_usnob_oct13(ra,dec)
parallel 4;

--DC5SIM_USNOB_oct13 TABLE
create index dc5simoct13_starid_idx on dc5sim_usnob_oct13(star_galaxy_id)
parallel 4;

--DC5SIM_USNOB_oct14 TABLE
create index dc5simoct14_radec_idx on dc5sim_usnob_oct14(ra,dec)
parallel 4;

--DC5SIM_USNOB_oct14 TABLE
create index dc5simoct14_starid_idx on dc5sim_usnob_oct14(star_galaxy_id)
parallel 4;

--DC5SIM_USNOB_oct15 TABLE
create index dc5simoct15_radec_idx on dc5sim_usnob_oct15(ra,dec)
parallel 4;

--DC5SIM_USNOB_oct15 TABLE
create index dc5simoct15_starid_idx on dc5sim_usnob_oct15(star_galaxy_id)
parallel 4;

--Dora 11/11/2009 add 5 more index to coadd_objects (JIRA DES-1431)
create index coaddobj_catalogid_g_idx on coadd_objects(catalogid_g)
parallel 4;

create index coaddobj_catalogid_r_idx on coadd_objects(catalogid_r)
parallel 4;

create index coaddobj_catalogid_i_idx on coadd_objects(catalogid_i)
parallel 4;

create index coaddobj_catalogid_z_idx on coadd_objects(catalogid_z)
parallel 4;

create index coaddobj_catalogid_y_idx on coadd_objects(catalogid_y)
parallel 4;

create index files004_imgradec_idx on dr004_files(image_ra,image_dec) 
tablespace desdr
parallel 4;

create index files004_cddradec_idx on dr004_files(coadd_ra,coadd_dec) 
tablespace desdr
parallel 4;

create index files004_expradec_idx on dr004_files(telra,teldec) 
tablespace desdr
parallel 4;

create index files004_run_idx on dr004_files(run)
tablespace desdr
parallel 4;

create index files004_id_idx on dr004_files(id)
tablespace desdr
parallel 4;

create index objdr004_radec_idx on dr004_objects(ra,dec) local
tablespace desdr
parallel 4;

create index objdr004_nite_idx on dr004_objects(nite) global
tablespace desdr
parallel 4;

create index objdr004_run_idx on dr004_objects(run) global
tablespace desdr
parallel 4;

create index objdr004_imageid_idx on dr004_objects(imageid) global
tablespace desdr
parallel 4;

create index objdr004_objectid_idx on dr004_objects(object_id) global
tablespace desdr
parallel 4;

-- Dora 11/28/2009 create index for dr004_coadd_objects
create index coaddobj004_radec_idx on dr004_coadd_objects(ra,dec) local
tablespace desdr
parallel 4;

create index coaddobj004_run_idx on dr004_coadd_objects(run) global
tablespace desdr
parallel 4;

create index coaddobj004_objectid_idx on dr004_coadd_objects(coadd_objects_id) global
tablespace desdr
parallel 4;

-- Dora 04/05/2010 create more index for dr004_coadd_objects
-- Dora 04/07/2010 drop this index
create index coaddobj004_magmodelr_idx on dr004_coadd_objects(mag_model_r) local
tablespace desdr
parallel 4;

-- MichelleG 12/17/09 create indices on PFW tables on columns used in most searches and for joining tables
create index run_run_idx on run(run);
create index block_runid_idx on block(run_id);
create index block_blockname_idx on block(block_name);
create index pfwtask_blockid_idx on pfwtask(block_id);
create index pfwtask_pfwtaskname_idx on pfwtask(pfwtask_name);
create index pfwmsg_pfwtaskid_idx on pfwmsg(pfwtask_id);

--Dora 12/23/2009 add 3 more index to SN_* tables
create index sncand_radec_idx on sncand(ra,dec)
parallel 4;

create index snobs_radec_idx on snobs(ra,dec)
parallel 4;

create index snveto_radec_idx on snveto(ra,dec)
parallel 4;

-- Dora 01/10/2010 create an index on standard_stars_dc5
create index standardstarsdc5_radec_idx on standard_stars_dc5(radeg,decdeg)
parallel 4;

-- Dora 03/10/2010 create indexes for dr005_*
create index files005_imgradec_idx on dr005_files(image_ra,image_dec) 
tablespace desdr
parallel 4;

create index files005_cddradec_idx on dr005_files(coadd_ra,coadd_dec) 
tablespace desdr
parallel 4;

create index files005_expradec_idx on dr005_files(telra,teldec) 
tablespace desdr
parallel 4;

create index files005_run_idx on dr005_files(run)
tablespace desdr
parallel 4;

create index files005_id_idx on dr005_files(id)
tablespace desdr
parallel 4;

create index objdr005_radec_idx on dr005_objects(ra,dec) local
tablespace desdr
parallel 4;

create index objdr005_nite_idx on dr005_objects(nite) global
tablespace desdr
parallel 4;

create index objdr005_run_idx on dr005_objects(run) global
tablespace desdr
parallel 4;

create index objdr005_objectid_idx on dr005_objects(object_id) global
tablespace desdr
parallel 4;

create index coaddobj005_radec_idx on dr005_coadd_objects(ra,dec) local
tablespace desdr
parallel 4;

create index coaddobj005_run_idx on dr005_coadd_objects(run) global
tablespace desdr
parallel 4;

create index coaddobj005_objectid_idx on dr005_coadd_objects(coadd_objects_id) global
tablespace desdr
parallel 4;

-- Dora 03/11/2010 create indexes for wl_me_shear
create index wlmeshear_coaddobjid_idx on wl_me_shear(coadd_object_id)
parallel 4;

create index wlmeshear_radec_idx on wl_me_shear(ra,dec)
parallel 4;

-- Dora 03/11/2010 create indexes for photo_z
create index photoz_catalogid_idx on photo_z(catalogid)
parallel 4;

-- Dora 04/07/2010 create indexes for dr004_coadd_objects_tag
create index dc5tag_objectid_idx on dr004_coadd_objects_tag(coadd_objects_id) global
tablespace desdr
parallel 4;

create index dc5tag_radec_idx on dr004_coadd_objects_tag(ra,dec) local
tablespace desdr
parallel 4;

create index dc5tag_magmodelg_idx on dr004_coadd_objects_tag(mag_model_g) local
tablespace desdr
parallel 4;

create index dc5tag_magmodelr_idx on dr004_coadd_objects_tag(mag_model_r) local
tablespace desdr
parallel 4;

create index dc5tag_magmodeli_idx on dr004_coadd_objects_tag(mag_model_i) local
tablespace desdr
parallel 4;

create index dc5tag_magmodelz_idx on dr004_coadd_objects_tag(mag_model_z) local
tablespace desdr
parallel 4;

create index dc5tag_magmodely_idx on dr004_coadd_objects_tag(mag_model_y) local
tablespace desdr
parallel 4;

create index dc5tag_starr_idx on dr004_coadd_objects_tag(class_star_r) local
tablespace desdr
parallel 4;

create index dc5tag_stari_idx on dr004_coadd_objects_tag(class_star_i) local
tablespace desdr
parallel 4;

create index dc5tag_spreadr_idx on dr004_coadd_objects_tag(spread_model_r) local
tablespace desdr
parallel 4;

create index dc5tag_spreadi_idx on dr004_coadd_objects_tag(spread_model_i) local
tablespace desdr
parallel 4;

-- Dora 04/14/10 add index on OBJECTS_CURRENT TABLE
create index objects2010_objectid_idx on objects_current(object_id)
parallel 4;

create index objects2010_imageid_idx on objects_current(imageid)
parallel 4;

create index objects2010_ra_dec_idx on objects_current(ra,dec)
parallel 4;

create index objects2010_catalogid_idx on objects_current(catalogid)
parallel 4;

-- Dora 05/14/2010 create indexes for dr006_*
create index files006_imgradec_idx on dr006_files(image_ra,image_dec) 
tablespace desdr
parallel 4;

create index files006_cddradec_idx on dr006_files(coadd_ra,coadd_dec) 
tablespace desdr
parallel 4;

create index files006_expradec_idx on dr006_files(telra,teldec) 
tablespace desdr
parallel 4;

create index files006_run_idx on dr006_files(run)
tablespace desdr
parallel 4;

create index files006_id_idx on dr006_files(id)
tablespace desdr
parallel 4;

create index objdr006_radec_idx on dr006_objects(ra,dec) local
tablespace desdr
parallel 4;

create index objdr006_nite_idx on dr006_objects(nite) global
tablespace desdr
parallel 4;

create index objdr006_run_idx on dr006_objects(run) global
tablespace desdr
parallel 4;

create index objdr006_objectid_idx on dr006_objects(object_id) global
tablespace desdr
parallel 4;

create index coaddobj006_radec_idx on dr006_coadd_objects(ra,dec) local
tablespace desdr
parallel 4;

create index coaddobj006_run_idx on dr006_coadd_objects(run) global
tablespace desdr
parallel 4;

create index coaddobj006_objectid_idx on dr006_coadd_objects(coadd_objects_id) global
tablespace desdr
parallel 4;

-- Dora 05/19/2010 create indexes for dr007_coadd_objects
create index coaddobj007_radec_idx on dr007_coadd_objects(ra,dec) local
tablespace desdr
parallel 4;

create index coaddobj007_magmodelg_idx on dr007_coadd_objects(mag_model_g) local
tablespace desdr
parallel 4;

create index coaddobj007_magmodelr_idx on dr007_coadd_objects(mag_model_r) local
tablespace desdr
parallel 4;

create index coaddobj007_magmodeli_idx on dr007_coadd_objects(mag_model_i) local
tablespace desdr
parallel 4;

create index coaddobj007_magmodelz_idx on dr007_coadd_objects(mag_model_z) local
tablespace desdr
parallel 4;

create index coaddobj007_magmodely_idx on dr007_coadd_objects(mag_model_y) local
tablespace desdr
parallel 4;

create index coaddobj007_starg_idx on dr007_coadd_objects(class_star_g) local
tablespace desdr
parallel 4;

create index coaddobj007_starr_idx on dr007_coadd_objects(class_star_r) local
tablespace desdr
parallel 4;

create index coaddobj007_stari_idx on dr007_coadd_objects(class_star_i) local
tablespace desdr
parallel 4;

create index coaddobj007_starz_idx on dr007_coadd_objects(class_star_z) local
tablespace desdr
parallel 4;

create index coaddobj007_stary_idx on dr007_coadd_objects(class_star_y) local
tablespace desdr
parallel 4;

create index coaddobj007_spreadg_idx on dr007_coadd_objects(spread_model_g) local
tablespace desdr
parallel 4;

create index coaddobj007_spreadr_idx on dr007_coadd_objects(spread_model_r) local
tablespace desdr
parallel 4;

create index coaddobj007_spreadi_idx on dr007_coadd_objects(spread_model_i) local
tablespace desdr
parallel 4;

create index coaddobj007_spreadz_idx on dr007_coadd_objects(spread_model_z) local
tablespace desdr
parallel 4;

create index coaddobj007_spready_idx on dr007_coadd_objects(spread_model_y) local
tablespace desdr
parallel 4;

-- Dora 05/20/2010 create indexes for dr007_coadd_objects_tag
create index coaddobj007_tag_radec_idx on dr007_coadd_objects_tag(ra,dec) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_magmodelg_idx on dr007_coadd_objects_tag(mag_model_g) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_magmodelr_idx on dr007_coadd_objects_tag(mag_model_r) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_magmodeli_idx on dr007_coadd_objects_tag(mag_model_i) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_magmodelz_idx on dr007_coadd_objects_tag(mag_model_z) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_magmodely_idx on dr007_coadd_objects_tag(mag_model_y) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_starr_idx on dr007_coadd_objects_tag(class_star_r) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_stari_idx on dr007_coadd_objects_tag(class_star_i) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_spreadr_idx on dr007_coadd_objects_tag(spread_model_r) local
tablespace desdr
parallel 4;

create index coaddobj007_tag_spreadi_idx on dr007_coadd_objects_tag(spread_model_i) local
tablespace desdr
parallel 4;

-- Dora 05/26/2010 create indexes on run for dr007_coadd_objects to speed 
-- up incremental copy from dr007_coadd_objects to dr007_coadd_objects_tag 
create index coaddobj007_run_idx on dr007_coadd_objects(run) global
tablespace desdr
parallel 4;

-- Dora 06/26/2010 create indexes on parentid for WL
create index wl_parentid_idx on wl(parentid)
parallel 4;

-- Dora 06/27/2010 create indexes on exposureid for IMAGE
create index image_exposureid_idx on image(exposureid)
parallel 4;


