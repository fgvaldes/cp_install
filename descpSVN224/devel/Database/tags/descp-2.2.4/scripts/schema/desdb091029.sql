-- 
-- TABLE: ARCHIVE_SITES 
--

CREATE TABLE ARCHIVE_SITES(
    LOCATION_ID            VARCHAR2(8)      NOT NULL,
    LOCATION_NAME          VARCHAR2(100),
    ARCHIVE_HOST           VARCHAR2(60),
    ARCHIVE_ROOT           VARCHAR2(100),
    SITE_NAME              VARCHAR2(32),
    SITE_ID                NUMBER(3) 	    NOT NULL,
    CONSTRAINT PK_ARCHIVE_SITES PRIMARY KEY (LOCATION_ID)
)
;



-- 
-- TABLE: BCS_REDUCTION_STATUS 
--

CREATE TABLE BCS_REDUCTION_STATUS(
    RUNID               VARCHAR2(80)    NOT NULL,
    STAGE_DETREND       VARCHAR2(10),
    STAGE_SCAMP         VARCHAR2(10),
    STAGE_PSFEX         VARCHAR2(10),
    STAGE_POSTSCAMP     VARCHAR2(10),
    STAGE_CAT_INGEST    VARCHAR2(10),
    STAGE_PSM           VARCHAR2(10),
    CONSTRAINT PK_BCS_REDUCTION_STATUS PRIMARY KEY (RUNID)
)
;



-- 
-- TABLE: CATALOG 
-- /08/15/08 Add new column PROJECT
--

CREATE TABLE CATALOG(
    ID             NUMBER(10, 0)    NOT NULL,
    RUN            VARCHAR2(100),
    NITE           VARCHAR2(20),
    BAND           VARCHAR2(20),
    TILENAME       VARCHAR2(20),
    CATALOGNAME    VARCHAR2(100),
    CCD            NUMBER(3, 0),
    CATALOGTYPE    VARCHAR2(20),
    PARENTID       NUMBER(9, 0),
    EXPOSUREID     NUMBER(9, 0),
    OBJECTS        NUMBER(8, 0),
    PROJECT        VARCHAR2(20),
    CONSTRAINT PK_CATALOG PRIMARY KEY (ID)
)
;



-- 
-- TABLE: CATSIM1_TRUTH 
--

CREATE TABLE CATSIM1_TRUTH(
    CATSIM1_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID      NUMBER(10, 0),
    CLASS               CHAR(1),
    RA                  NUMBER(10, 7),
    DEC                 NUMBER(10, 7),
    U_MAG               NUMBER(7, 4),
    G_MAG               NUMBER(7, 4),
    R_MAG               NUMBER(7, 4),
    I_MAG               NUMBER(7, 4),
    Z_MAG               NUMBER(7, 4),
    HTMID               NUMBER(16, 0),
    CX                  NUMBER(10, 6),
    CY                  NUMBER(10, 6),
    CZ                  NUMBER(10, 6),
    CONSTRAINT PK_CATSIM1_TRUTH PRIMARY KEY (CATSIM1_TRUTH_ID)
)
;



-- 
-- TABLE: CATSIM3_TRUTH 
--

CREATE TABLE CATSIM3_TRUTH(
    CATSIM3_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID      NUMBER(10, 0),
    CLASS               CHAR(1),
    RA                  NUMBER(10, 7),
    DEC                 NUMBER(10, 7),
    U_MAG               NUMBER(7, 4),
    G_MAG               NUMBER(7, 4),
    R_MAG               NUMBER(7, 4),
    I_MAG               NUMBER(7, 4),
    Z_MAG               NUMBER(7, 4),
    HTMID               NUMBER(16, 0),
    CX                  NUMBER(10, 6),
    CY                  NUMBER(10, 6),
    CZ                  NUMBER(10, 6),
    DATAFILE_NAME       VARCHAR2(40),
    CONSTRAINT PK_CATSIM3_TRUTH PRIMARY KEY (CATSIM3_TRUTH_ID)
)
;



-- 
-- TABLE: COADD 
-- /08/15/08 Add new column PROJECT
-- Modified by Dora on 11/03/08, add two new columns: ra_num and dec_num
-- Added a new column, coadd_type, per request of Tony (JIRA DES-1068)
--

CREATE TABLE COADD(
    ID             NUMBER(10, 0)    NOT NULL,
    RUN            VARCHAR2(100),
    BAND           VARCHAR2(20),
    TILENAME       VARCHAR2(20),
    IMAGENAME      VARCHAR2(100),
    RADECEQUIN     BINARY_FLOAT,
    HTMID          NUMBER(16, 0),
    CX             BINARY_DOUBLE,
    CY             BINARY_DOUBLE,
    CZ             BINARY_DOUBLE,
    WCSTYPE        VARCHAR2(80),
    WCSDIM         NUMBER(2, 0),
    EQUINOX        BINARY_FLOAT,
    CTYPE1         VARCHAR2(20),
    CTYPE2         VARCHAR2(20),
    CUNIT1         VARCHAR2(20),
    CUNIT2         VARCHAR2(20),
    CRVAL1         BINARY_DOUBLE,
    CRVAL2         BINARY_DOUBLE,
    CRPIX1         NUMBER(10, 4),
    CRPIX2         NUMBER(10, 4),
    CD1_1          BINARY_DOUBLE,
    CD2_1          BINARY_DOUBLE,
    CD1_2          BINARY_DOUBLE,
    CD2_2          BINARY_DOUBLE,
    NAXIS1         NUMBER(6, 0),
    NAXIS2         NUMBER(6, 0),
    NEXTEND        NUMBER(3, 0),
    FWHM           BINARY_FLOAT,
    ELLIPTICITY    BINARY_FLOAT,
    PROJECT        VARCHAR2(20),
    RA                  Number(8,5),
    DEC                 Number(8,5),
    COADD_TYPE     VARCAHR2(20),
    CONSTRAINT PK_COADD PRIMARY KEY (ID)
)
;



-- 
-- TABLE: COADD_SRC 
--

CREATE TABLE COADD_SRC(
    COADD_IMAGEID    NUMBER(9, 0)    NOT NULL,
    SRC_IMAGEID      NUMBER(9, 0)    NOT NULL,
    MAGZP            BINARY_FLOAT,
    MAGZP_ERR        BINARY_FLOAT,
    CONSTRAINT PK_COADD_SRC PRIMARY KEY (COADD_IMAGEID, SRC_IMAGEID)
)
;



-- 
-- TABLE: COADDTILE 
--

CREATE TABLE COADDTILE(
    COADDTILE_ID    NUMBER(6, 0)     NOT NULL,
    PROJECT         VARCHAR2(25),
    TILENAME        VARCHAR2(50),
    RA              NUMBER(10, 7),
    DEC             NUMBER(10, 7),
    EQUINOX         NUMBER(10, 5),
    PIXELSIZE       NUMBER(8, 6),
    NPIX_RA         NUMBER(7, 0),
    NPIX_DEC        NUMBER(7, 0),
    CONSTRAINT PK_COADDTILE PRIMARY KEY (COADDTILE_ID)
)
;



-- 
-- TABLE: DC0_TRUTH 
--

CREATE TABLE DC0_TRUTH(
    TRUTH_ID         NUMBER(12, 0)    NOT NULL,
    HTMID            NUMBER(16, 0),
    CX               NUMBER(10, 6),
    CY               NUMBER(10, 6),
    CZ               NUMBER(10, 6),
    ID1              NUMBER(6, 0),
    ID2              NUMBER(6, 0),
    ID3              NUMBER(6, 0),
    ID4              NUMBER(6, 0),
    ID5              NUMBER(6, 0),
    X_IMAGE          NUMBER(6, 2),
    Y_IMAGE          NUMBER(6, 2),
    ALPHA_J2000      NUMBER(8, 5),
    DELTA_J2000      NUMBER(8, 5),
    EQUINOX          NUMBER(6, 2),
    U_MAG_AUTO       NUMBER(7, 4),
    U_MAGERR_AUTO    NUMBER(7, 4),
    G_MAG_AUTO       NUMBER(7, 4),
    G_MAGERR_AUTO    NUMBER(7, 4),
    R_MAG_AUTO       NUMBER(7, 4),
    R_MAGERR_AUTO    NUMBER(7, 4),
    I_MAG_AUTO       NUMBER(7, 4),
    I_MAGERR_AUTO    NUMBER(7, 4),
    Z_MAG_AUTO       NUMBER(7, 4),
    Z_MAGERR_AUTO    NUMBER(7, 4),
    U_MAG_APER5      NUMBER(7, 4),
    G_MAG_APER5      NUMBER(7, 4),
    R_MAG_APER5      NUMBER(7, 4),
    I_MAG_APER5      NUMBER(7, 4),
    Z_MAG_APER5      NUMBER(7, 4),
    U_MAG_APER10     NUMBER(7, 4),
    G_MAG_APER10     NUMBER(7, 4),
    R_MAG_APER10     NUMBER(7, 4),
    I_MAG_APER10     NUMBER(7, 4),
    Z_MAG_APER10     NUMBER(7, 4),
    U_REDDENING      NUMBER(7, 4),
    G_REDDENING      NUMBER(7, 4),
    R_REDDENING      NUMBER(7, 4),
    I_REDDENING      NUMBER(7, 4),
    Z_REDDENING      NUMBER(7, 4),
    U_FWHM           NUMBER(6, 2),
    G_FWHM           NUMBER(6, 2),
    R_FWHM           NUMBER(6, 2),
    I_FWHM           NUMBER(6, 2),
    Z_FWHM           NUMBER(6, 2),
    U_CLASS_STAR     NUMBER(3, 2),
    G_CLASS_STAR     NUMBER(3, 2),
    R_CLASS_STAR     NUMBER(3, 2),
    I_CLASS_STAR     NUMBER(3, 2),
    Z_CLASS_STAR     NUMBER(3, 2),
    A_IMAGE          NUMBER(6, 2),
    B_IMAGE          NUMBER(6, 2),
    THETA            NUMBER(5, 2),
    CXX              NUMBER(8, 4),
    CYY              NUMBER(8, 4),
    CXY              NUMBER(8, 4),
    U_FLAGS          NUMBER(3, 0),
    G_FLAGS          NUMBER(3, 0),
    R_FLAGS          NUMBER(3, 0),
    I_FLAGS          NUMBER(3, 0),
    Z_FLAGS          NUMBER(3, 0),
    CONSTRAINT PK_DC0_TRUTH PRIMARY KEY (TRUTH_ID)
)
;



-- 
-- TABLE: DES_ACCOUNTS 
-- 10/28/2009: Dora add two new columns (JIRA DES-1400)

CREATE TABLE DES_ACCOUNTS(
    NVO_USER     VARCHAR2(30)    NOT NULL,
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    HAS_MYDB_ACCOUNT CHAR(1) DEFAULT 'N',
    MYDB_HOST VARCHAR2(100), 
    CONSTRAINT PK_DES_ACCOUNTS PRIMARY KEY (NVO_USER, DES_GROUP)
)
;



-- 
-- TABLE: DES_EXPOSURES 
-- /08/15/08 Add new column PROJECT
-- Dora Cai drop the table on 07/15/2009 
--

--CREATE TABLE DES_EXPOSURES(
    --IMAGE_N        NUMBER(10, 0)    NOT NULL,
    --RA_CENTER      NUMBER(10, 6),
    --DECL_CENTER    NUMBER(10, 6),
    --EQUINOX        NUMBER(4, 0),
    --LAYER_N        NUMBER(10, 0)    NOT NULL,
    --TILE_N         NUMBER(10, 0)    NOT NULL,
    --OBS_DATE       TIMESTAMP(6),
    --NSA_ID         NUMBER(10, 0),
    --QUALITY        NUMBER(10, 2),
    --AIRMASS        NUMBER(10, 2),
    --GAIN           NUMBER(10, 6),
    --RD_NOISE       NUMBER(10, 6),
    --PROJECT        VARCHAR2(20),
    --CONSTRAINT PK_DES_EXPOSURES PRIMARY KEY (IMAGE_N)
--)
--;



-- 
-- TABLE: DES_STRIPE82_STDS_V1 
--

CREATE TABLE DES_STRIPE82_STDS_V1(
    STRIPE82_ID    NUMBER(10, 0)    NOT NULL,
    NAME           VARCHAR2(40)     NOT NULL,
    RADEG          NUMBER(10, 6),
    DECDEG         NUMBER(10, 6),
    STDMAG_U       NUMBER(10, 6),
    STDMAG_G       NUMBER(10, 6),
    STDMAG_R       NUMBER(10, 6),
    STDMAG_I       NUMBER(10, 6),
    STDMAG_Z       NUMBER(10, 6),
    STDMAGERR_U    NUMBER(10, 6),
    STDMAGERR_G    NUMBER(10, 6),
    STDMAGERR_R    NUMBER(10, 6),
    STDMAGERR_I    NUMBER(10, 6),
    STDMAGERR_Z    NUMBER(10, 6),
    NOBS_U         NUMBER(10, 6),
    NOBS_G         NUMBER(10, 6),
    NOBS_R         NUMBER(10, 6),
    NOBS_I         NUMBER(10, 6),
    NOBS_Z         NUMBER(10, 6),
    CONSTRAINT PK_DES_STRIPE82_STDS_V1 PRIMARY KEY (STRIPE82_ID)
)
;



-- 
-- TABLE: DEVICE_LIST 
-- Dora Cai drop the table on 07/15/2009 
--

--CREATE TABLE DEVICE_LIST(
    --DEVICE_N        NUMBER(10, 0)     NOT NULL,
    --ORIGIN_ID       NUMBER(10, 0),
    --INSTALL_DATE    TIMESTAMP(6),
    --AMP             NUMBER(2, 0),
    --NOTE            VARCHAR2(1000),
    --GAIN            NUMBER(10, 0),
    --RD_NOISE        NUMBER(10, 0),
    --CONSTRAINT PK_DEVICE_LIST PRIMARY KEY (DEVICE_N)
--)
--;



-- 
-- TABLE: EXPOSURE 
-- Add three columns: NAXIS1, NAXIS2 and NEXTEDND on 08/04/08 by Dora
-- Modified by Dora on 11/03/08, add two new columns: ra_num and dec_num
-- Modified by Dora on 01/02/09 for Joe's request, expand the column BAND 
-- to varchar2(68)
-- Modified by Dora on 02/25/09 for Joe's request, add a new column obstype 
-- varchar2(80)
-- Modified by Dora on 02/25/09 for Joe's request, rename the column 
-- exprequest to expreq
-- Modified by Dora on 02/27/09 for Joe's request, rename the column 
-- dimmseeing to dimmsee
-- Modified by Dora on 09/25/2009 for Tony's request, add 16 new columns. (JIRA DES-1258) 

CREATE TABLE EXPOSURE(
    ID              NUMBER(10, 0)    NOT NULL,
    NITE            VARCHAR2(20),
    BAND            VARCHAR2(68),
    EXPOSURENAME    VARCHAR2(100),
    EXPOSURETYPE    VARCHAR2(20),
    TELRADEC        VARCHAR2(10),
    TELEQUIN        BINARY_FLOAT,
    HTMID           NUMBER(16, 0),
    CX              BINARY_DOUBLE,
    CY              BINARY_DOUBLE,
    CZ              BINARY_DOUBLE,
    HA              VARCHAR2(20),
    ZD              BINARY_FLOAT,
    AIRMASS         BINARY_FLOAT,
    TELFOCUS        BINARY_FLOAT,
    OBJECT          VARCHAR2(80),
    OBSERVER        VARCHAR2(80),
    PROPID          VARCHAR2(20),
    DETECTOR        VARCHAR2(20),
    DETSIZE         VARCHAR2(40),
    TELESCOPE       VARCHAR2(20),
    OBSERVATORY     VARCHAR2(80),
    LATITUDE        VARCHAR2(80),
    LONGITUDE       VARCHAR2(80),
    ALTITUDE        VARCHAR2(80),
    TIMESYS         VARCHAR2(80),
    DATE_OBS        VARCHAR2(80),
    TIME_OBS        VARCHAR2(80),
    MJD_OBS         BINARY_DOUBLE,
    EXPTIME         BINARY_FLOAT,
    EXPREQ          BINARY_FLOAT,
    DARKTIME        BINARY_FLOAT,
    EPOCH           BINARY_FLOAT,
    WINDSPD         VARCHAR2(20),
    WINDDIR         VARCHAR2(20),
    AMBTEMP         VARCHAR2(20),
    HUMIDITY        VARCHAR2(20),
    PRESSURE        VARCHAR2(20),
    SKYVAR          BINARY_FLOAT,
    FLUXVAR         BINARY_FLOAT,
    DIMMSEE         BINARY_FLOAT,
    PHOTFLAG        NUMBER(2, 0),
    IMAGEHWV        VARCHAR2(80),
    IMAGESWV        VARCHAR2(80),
    NAXIS1          NUMBER(6),
    NAXIS2          NUMBER(6),
    NEXTEND         NUMBER(3),
    TELRA          Number(8,5),
    TELDEC         Number(8,5),
    OBSTYPE        VARCHAR2(80),
    EXPNUM NUMBER(10),
    PROCTYPE VARCHAR(10),
    PRODTYPE VARCHAR(10),
    PROPOSER VARCHAR(20),
    INSTRUMENT VARCHAR(10),
    PIXSCAL1 BINARY_FLOAT,
    PIXSCAL2 BINARY_FLOAT,
    CAMSHUT VARCHAR(10),
    RADESYS VARCHAR(10),
    FILTID VARCHAR(10),
    FILPOS NUMBER(3),
    TELSTAT VARCHAR(10),
    DOMESTAT VARCHAR(10),
    DOMESHUT VARCHAR(10),
    DOMELAMP VARCHAR(10),
    MOONANGLE BINARY_FLOAT,
    CONSTRAINT PK_EXPOSURE PRIMARY KEY (ID)
)
;



-- 
-- TABLE: FILE_LOCATIONS 
-- Dora Cai drop the table on 07/15/2009 
--

--CREATE TABLE FILE_LOCATIONS(
    --IMAGE_NAME     VARCHAR2(80),
    --LOCATION_ID    VARCHAR2(8)
--)
--;



-- 
-- TABLE: FILES 
--

CREATE TABLE FILES(
    IMAGEID           NUMBER(9, 0)      NOT NULL,
    RA                NUMBER(10, 7),
    DEC               NUMBER(10, 7),
    EQUINOX           NUMBER(10, 5),
    FILE_DATE         VARCHAR2(25),
    HTMID             NUMBER(16, 0),
    CX                NUMBER(10, 6),
    CY                NUMBER(10, 6),
    CZ                NUMBER(10, 6),
    CCD_NUMBER        NUMBER(2, 0),
    DEVICE_ID         NUMBER(6, 0),
    NSA_IDENTIFIER    VARCHAR2(80),
    BAND              VARCHAR2(8),
    NITE              VARCHAR2(80),
    IMAGETYPE         VARCHAR2(15),
    RUNIDDESC         VARCHAR2(80),
    IMAGENAME         VARCHAR2(80),
    AIRMASS           NUMBER(4, 3),
    SCAMPNUM          NUMBER(5, 0),
    SCAMPCHI          NUMBER(8, 2),
    SCAMPFLG          NUMBER(2, 0),
    SKYBRITE          NUMBER(9, 3),
    PHOTFLAG          NUMBER(2, 0),
    FWHM              NUMBER(5, 3),
    SKYSIGMA          NUMBER(9, 3),
    QUALITY_FLAG_8    NUMBER(5, 0),
    EXPTIME           NUMBER(6, 2),
    DARKTIME          NUMBER(6, 2),
    GAIN_A            NUMBER(4, 2),
    RDNOISE_A         NUMBER(4, 2),
    GAIN_B            NUMBER(4, 2),
    RDNOISE_B         NUMBER(4, 2),
    OBJECT            VARCHAR2(80),
    OBSERVATORY       VARCHAR2(80),
    TELESCOPE         VARCHAR2(20),
    DETECTOR          VARCHAR2(20),
    OBSERVER          VARCHAR2(80),
    PROPID            VARCHAR2(20),
    HOURANGLE         VARCHAR2(20),
    ZENITHD           VARCHAR2(20),
    WEATHERDATE       VARCHAR2(80),
    WINDSPD           VARCHAR2(20),
    WINDDIR           VARCHAR2(20),
    AMBTEMP           VARCHAR2(20),
    HUMIDITY          VARCHAR2(20),
    PRESSURE          VARCHAR2(20),
    DIMMSEEING        VARCHAR2(20),
    WCSDIM            NUMBER(2, 0),
    CTYPE1            VARCHAR2(10),
    CTYPE2            VARCHAR2(10),
    CRVAL1            NUMBER(10, 7),
    CRVAL2            NUMBER(10, 7),
    CRPIX1            NUMBER(10, 4),
    CRPIX2            NUMBER(10, 4),
    CD1_1             NUMBER(10, 7),
    CD2_1             NUMBER(10, 7),
    CD1_2             NUMBER(10, 7),
    CD2_2             NUMBER(10, 7),
    RADECEQ           NUMBER(10, 5),
    TILENAME          VARCHAR2(50),
    NPIX1             NUMBER(6, 0),
    NPIX2             NUMBER(6, 0),
    NEXTEND           NUMBER(3, 0),
    ARCHIVESITES      VARCHAR2(25)       DEFAULT 'NNNNNNNNNNNNNNNNNNNNNNNNN',
    PV1_0             NUMBER(11, 10),
    PV1_1             NUMBER(11, 10),
    PV1_2             NUMBER(11, 10),
    PV1_3             NUMBER(11, 10),
    PV1_4             NUMBER(11, 10),
    PV1_5             NUMBER(11, 10),
    PV1_6             NUMBER(11, 10),
    PV1_7             NUMBER(11, 10),
    PV1_8             NUMBER(11, 10),
    PV1_9             NUMBER(11, 10),
    PV1_10            NUMBER(11, 10),
    PV2_1             NUMBER(11, 10),
    PV2_2             NUMBER(11, 10),
    PV2_3             NUMBER(11, 10),
    PV2_4             NUMBER(11, 10),
    PV2_5             NUMBER(11, 10),
    PV2_6             NUMBER(11, 10),
    PV2_7             NUMBER(11, 10),
    PV2_8             NUMBER(11, 10),
    PV2_9             NUMBER(11, 10),
    PV2_10            NUMBER(11, 10),
    PV2_0             NUMBER(11, 10),
    IMAGECLASS        VARCHAR2(20),
    MD_ELLIPTICITY    NUMBER(6, 4),
    CONSTRAINT PK_FILES_IMAGEID PRIMARY KEY (IMAGEID)
)
;



-- 
-- TABLE: FP_RESPONSE 
-- Dora Cai drop the table on 07/15/2009 
--

--CREATE TABLE FP_RESPONSE(
    --RESPONSE_ID     NUMBER(10, 0)    NOT NULL,
    --RUNIDDESC       VARCHAR2(80),
    --NITE            VARCHAR2(80),
    --TELESCOPE       VARCHAR2(20),
    --DETECTOR        VARCHAR2(20),
    --CCD_NUMBER      NUMBER(3, 0),
    --VAL_RESPONSE    NUMBER(8, 4),
    --SIG_RESPONSE    NUMBER(8, 4),
    --SOURCE          VARCHAR2(20),
    --COMMENTS        VARCHAR2(200),
    --CONSTRAINT PK_FP_RESPONSE PRIMARY KEY (RESPONSE_ID)
--)
--;



-- 
-- TABLE: IMAGE 
-- Add two columns: PV1_0 and PV2_0, on 08/01/08 by Dora
-- Add three columns: NAXIS1, NAXIS2 and NEXTEDND on 08/04/08 by Dora
-- Modified by Dora on 11/03/08, add two new columns: ra_num and dec_num
-- Modified by Dora on 02/25/09, change the column names: gain_a -> gaina,
-- gain_b -> gainb, rdnoise_a -> rdnoisea, rdnoise_b -> rdnoiseb
-- Modified by Dora on 02/27/09, change the column names: ellipticity -> elliptic,
-- Modified by Dora on 09/25/2009, add 22 new columns (JIRA DES-1258)
-- Modified by Dora on 09/28/2009, change (JIRA DES-1281)
-- CCDNAME from VARCHAR2(10) to VARCHAR2(40), 
-- PCOUNT from NUMBER(2) to NUMBER(10),
-- DETSEC from varchar2(20) to varchar2(30),
-- DATASEC from varchar2(20) to varchar2(30),
-- TRIMSEC from varchar2(20) to varchar2(30),
-- AMPSECA from varchar2(20) to varchar2(30),
-- BIASSECA from varchar2(20) to varchar2(30),
-- AMPSECB from binary_float to VARCHAR2(30),
-- BIASSECB from binary_float to VARCHAR2(30),

CREATE TABLE IMAGE(
    ID             NUMBER(10, 0)    NOT NULL,
    RUN            VARCHAR2(100),
    NITE           VARCHAR2(20),
    BAND           VARCHAR2(20),
    TILENAME       VARCHAR2(20),
    IMAGENAME      VARCHAR2(100),
    CCD            NUMBER(3, 0),
    IMAGETYPE      VARCHAR2(20),
    PROJECT        VARCHAR2(20),
    EXPOSUREID     NUMBER(9, 0),
    PARENTID       NUMBER(9, 0),
    AIRMASS        BINARY_FLOAT,
    EXPTIME        BINARY_FLOAT,
    DEVICE_ID      NUMBER(6, 0),
    GAINA         BINARY_FLOAT,
    RDNOISEA      BINARY_FLOAT,
    GAINB         BINARY_FLOAT,
    RDNOISEB      BINARY_FLOAT,
    EQUINOX        BINARY_FLOAT,
    HTMID          NUMBER(16, 0),
    CX             BINARY_DOUBLE,
    CY             BINARY_DOUBLE,
    CZ             BINARY_DOUBLE,
    WCSDIM         NUMBER(2, 0),
    CTYPE1         VARCHAR2(10),
    CUNIT1         VARCHAR2(20),
    CRVAL1         BINARY_DOUBLE,
    CRPIX1         BINARY_DOUBLE,
    CD1_1          BINARY_DOUBLE,
    CD1_2          BINARY_DOUBLE,
    PV1_1          BINARY_DOUBLE,
    PV1_2          BINARY_DOUBLE,
    PV1_3          BINARY_DOUBLE,
    PV1_4          BINARY_DOUBLE,
    PV1_5          BINARY_DOUBLE,
    PV1_6          BINARY_DOUBLE,
    PV1_7          BINARY_DOUBLE,
    PV1_8          BINARY_DOUBLE,
    PV1_9          BINARY_DOUBLE,
    PV1_10         BINARY_DOUBLE,
    CTYPE2         VARCHAR2(10),
    CUNIT2         VARCHAR2(20),
    CRVAL2         BINARY_DOUBLE,
    CRPIX2         BINARY_DOUBLE,
    CD2_1          BINARY_DOUBLE,
    CD2_2          BINARY_DOUBLE,
    PV2_1          BINARY_DOUBLE,
    PV2_2          BINARY_DOUBLE,
    PV2_3          BINARY_DOUBLE,
    PV2_4          BINARY_DOUBLE,
    PV2_5          BINARY_DOUBLE,
    PV2_6          BINARY_DOUBLE,
    PV2_7          BINARY_DOUBLE,
    PV2_8          BINARY_DOUBLE,
    PV2_9          BINARY_DOUBLE,
    PV2_10         BINARY_DOUBLE,
    SKYBRITE       BINARY_FLOAT,
    SKYSIGMA       BINARY_FLOAT,
    ELLIPTIC       BINARY_FLOAT,
    FWHM           BINARY_FLOAT,
    SCAMPNUM       NUMBER(5, 0),
    SCAMPCHI       BINARY_FLOAT,
    SCAMPFLG       NUMBER(2, 0),
    PV1_0          BINARY_DOUBLE,
    PV2_0          BINARY_DOUBLE,
    NAXIS1         NUMBER(6),
    NAXIS2         NUMBER(6),
    NEXTEND        NUMBER(3),
    RA                  Number(8,5),
    DEC                 Number(8,5),
    PCOUNT NUMBER(10),
    GCOUNT NUMBER(2),
    BZERO BINARY_FLOAT,
    BSCALE BINARY_FLOAT,
    BUNIT VARCHAR(10),
    CCDNAME VARCHAR(40),
    DETSEC VARCHAR(30),
    CCDSUM VARCHAR(10),
    DATASEC VARCHAR(30),
    TRIMSEC VARCHAR(30),
    AMPSECA VARCHAR(30),
    BIASSECA VARCHAR(30),
    SATURATA BINARY_FLOAT,
    SATURATB BINARY_FLOAT,
    LTM1_1 BINARY_FLOAT,
    LTM2_2 BINARY_FLOAT,
    LTV1 BINARY_FLOAT,
    LTV2 BINARY_FLOAT,
    RADESYS VARCHAR(20),
    WCSAXES NUMBER(1),
    AMPSECB VARCHAR2(30),
    BIASSECB VARCHAR2(30),
    CONSTRAINT PK_IMAGE PRIMARY KEY (ID)
)
;



-- 
-- TABLE: IMAGETYPE_ACCESS 
--

CREATE TABLE IMAGETYPE_ACCESS(
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    IMAGETYPE    VARCHAR2(80)    NOT NULL,
    CONSTRAINT PK_IMAGETYPE_ACCESS PRIMARY KEY (DES_GROUP, IMAGETYPE)
)
;



-- 
-- TABLE: IMSIM2_TRUTH 
--

CREATE TABLE IMSIM2_TRUTH(
    IMSIM2_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID     NUMBER(10, 0),
    CLASS              CHAR(1),
    RA                 NUMBER(10, 7),
    DEC                NUMBER(10, 7),
    U_MAG              NUMBER(7, 4),
    G_MAG              NUMBER(7, 4),
    R_MAG              NUMBER(7, 4),
    I_MAG              NUMBER(7, 4),
    Z_MAG              NUMBER(7, 4),
    HTMID              NUMBER(16, 0),
    CX                 NUMBER(10, 6),
    CY                 NUMBER(10, 6),
    CZ                 NUMBER(10, 6),
    DATAFILE_NAME      VARCHAR2(40),
    CONSTRAINT PK_IMSIM2_TRUTH PRIMARY KEY (IMSIM2_TRUTH_ID)
)
;



-- 
-- TABLE: LOCATION 
-- Dora Cai added 1 new column, release_code, on 08/05/2009 per Tony's request (JIRA DES-1061) 
-- Dora Cai rename column release_code to dr (means: data release) on 08/07/2009 per Joe's request (JIRA DES-1061) 
--

CREATE TABLE LOCATION(
    ID              NUMBER(10, 0)    NOT NULL,
    FILECLASS       VARCHAR2(20),
    FILETYPE        VARCHAR2(20),
    FILENAME        VARCHAR2(200),
    FILEDATE        DATE,
    FILESIZE        NUMBER(12, 0),
    RUN             VARCHAR2(100),
    NITE            VARCHAR2(20),
    BAND            VARCHAR2(20),
    TILENAME        VARCHAR2(20),
    EXPOSURENAME    VARCHAR2(100),
    CCD             NUMBER(3, 0),
    PROJECT         VARCHAR2(20),
    ARCHIVESITES    VARCHAR2(30),
    FILESIZE_GZ     NUMBER(12, 0),
    FILESIZE_FZ     NUMBER(12, 0),
    DR		    NUMBER(10), 
    CONSTRAINT PK_LOCATION PRIMARY KEY (ID)
)
;





-- 
-- TABLE: NOMAD 
--

CREATE TABLE NOMAD(
    NOMAD_ID         NUMBER(11, 0)    NOT NULL,
    RA               NUMBER(10, 7),
    DEC              NUMBER(10, 7),
    SRA              NUMBER(6, 3),
    SDE              NUMBER(6, 3),
    MURA             NUMBER(6, 3),
    MUDEC            NUMBER(6, 3),
    SMURA            NUMBER(6, 3),
    SMUDEC           NUMBER(6, 3),
    EPOCHRA          NUMBER(10, 5),
    EPOCHDEC         NUMBER(10, 5),
    B                NUMBER(8, 4),
    V                NUMBER(8, 4),
    R                NUMBER(8, 4),
    J                NUMBER(8, 4),
    H                NUMBER(8, 4),
    K                NUMBER(8, 4),
    USNOBID          NUMBER,
    TWO_MASSID       NUMBER,
    YB86ID           NUMBER,
    UCAC2ID          NUMBER,
    TYCHO2ID         NUMBER,
    FLAGS            NUMBER,
    HTMID            NUMBER(16, 0),
    CX               NUMBER(10, 6),
    CY               NUMBER(10, 6),
    CZ               NUMBER(10, 6),
    DATAFILE_NAME    VARCHAR2(50),
    CONSTRAINT PK_NOMAD_STAGING PRIMARY KEY (NOMAD_ID)
)
;



-- 
-- TABLE: OBJECTS_2007 
--
-- Modified by Dora on 07/04/08, to add 7 more columns.
-- Modified by Dora on 10/29/08, convert data type from number to binary_double
-- and binary_float.
-- Modified by Dora on 10/30/08, rename the column "run" to "partkey"
-- Modified by Dora on 11/03/08, add two new columns: ra_num and dec_num
-- Modified by Dora on 11/14/08, rename ra_num to ra and dec_num to dec
-- Modified by Dora on 07/16/09, rename the table from objects to objects_2007
-- Dora Cai added 80 columns on 07/30/2009 per Shantanu's request (JIRA DES-1030)
-- Dora Cai added 1 new column, release_code, on 08/05/2009 per Tony's request (JIRA DES-1061) 
-- Dora Cai rename column release_code to dr (means: data release) on 08/07/2009 per Joe's request (JIRA DES-1061) 
-- Dora Cai added 26 new columns per Joe's request (JIRA DES-1079) 
-- Dora Cai added 3 new columns, on 08/31/2009 per Shantanu's request (JIRA DES-1170) 
-- Dora Cai added 89 new columns, on 09/24/2009 per Shantanu's request (JIRA DES-1260) 
-- Dora Cai on 10/01/2009 added 3 columns on flags_weight, nlowweight_iso and nlowdweight_iso (JIRA DES-1178)
-- Dora Cai on 10/05/2009 added 2 columns: spread_model and spreaderr_model (JIRA DES-1178)

CREATE TABLE OBJECTS_2007(
OBJECT_ID               Number(11) NOT NULL,
EQUINOX                 BINARY_FLOAT,
BAND                    varchar2(10),
HTMID                   Number(16),
CX                      BINARY_DOUBLE,
CY                      BINARY_DOUBLE,
CZ                      BINARY_DOUBLE,
PARENTID                Number(11),
SOFTID                  Number(4),
IMAGEID                 Number(9),
ZEROPOINT               BINARY_FLOAT,
ERRZEROPOINT            BINARY_FLOAT,
ZEROPOINTID             Number(10),
OBJECT_NUMBER           Number(6),
MAG_AUTO                BINARY_FLOAT,
MAGERR_AUTO             BINARY_FLOAT,
MAG_APER_1              BINARY_FLOAT,
MAGERR_APER_1           BINARY_FLOAT,
MAG_APER_2              BINARY_FLOAT,
MAGERR_APER_2           BINARY_FLOAT,
MAG_APER_3              BINARY_FLOAT,
MAGERR_APER_3           BINARY_FLOAT,
MAG_APER_4              BINARY_FLOAT,
MAGERR_APER_4           BINARY_FLOAT,
MAG_APER_5              BINARY_FLOAT,
MAGERR_APER_5           BINARY_FLOAT,
MAG_APER_6              BINARY_FLOAT,
MAGERR_APER_6           BINARY_FLOAT,
ALPHA_J2000             BINARY_DOUBLE,
DELTA_J2000             BINARY_DOUBLE,
ALPHAPEAK_J2000         BINARY_DOUBLE,
DELTAPEAK_J2000         BINARY_DOUBLE,
X2_WORLD                Binary_FLOAT,
ERRX2_WORLD             BINARY_DOUBLE,
Y2_WORLD                Binary_FLOAT,
ERRY2_WORLD             BINARY_DOUBLE,
XY_WORLD                Binary_FLOAT,
ERRXY_WORLD             BINARY_DOUBLE,
THRESHOLD               Binary_FLOAT,
X_IMAGE                 Binary_FLOAT,
Y_IMAGE                 Binary_FLOAT,
XMIN_IMAGE              Binary_FLOAT,
YMIN_IMAGE              Binary_FLOAT,
XMAX_IMAGE              Binary_FLOAT,
YMAX_IMAGE              Binary_FLOAT,
X2_IMAGE                Binary_FLOAT,
ERRX2_IMAGE             Binary_FLOAT,
Y2_IMAGE                Binary_FLOAT,
ERRY2_IMAGE             Binary_FLOAT,
XY_IMAGE                Binary_FLOAT,
ERRXY_IMAGE             Binary_FLOAT,
A_IMAGE                 Binary_FLOAT,
ERRA_IMAGE              Binary_FLOAT,
B_IMAGE                 Binary_FLOAT,
ERRB_IMAGE              Binary_FLOAT,
THETA_IMAGE             Binary_FLOAT,
ERRTHETA_IMAGE          Binary_FLOAT,
ELLIPTICITY             Binary_FLOAT,
CLASS_STAR              Binary_FLOAT,
FLAGS                   Number(3),
FLUX_RADIUS             Binary_FLOAT,
FWHM_WORLD              Binary_FLOAT,
ISOAREA_WORLD           Binary_FLOAT,
THETA_J2000             Binary_FLOAT,
BACKGROUND              Binary_FLOAT, 
PARTKEY			Varchar2(30),
catalogid               Number(9),
RA                      Number(8,5),
DEC                     Number(8,5),
CHI2_MODEL 		BINARY_FLOAT,
FLAGS_MODEL 		NUMBER(1),
NITER_MODEL 		NUMBER(5),
FLUX_MODEL 		BINARY_FLOAT,
FLUXERR_MODEL 		BINARY_FLOAT,
MAG_MODEL 		BINARY_FLOAT,
MAGERR_MODEL 		BINARY_FLOAT,
XMODEL_IMAGE 		BINARY_FLOAT,
YMODEL_IMAGE 		BINARY_FLOAT,
XMODEL_WORLD 		BINARY_DOUBLE,
YMODEL_WORLD 		BINARY_DOUBLE,
ALPHAMODEL_SKY 		BINARY_DOUBLE,
DELTAMODEL_SKY 		BINARY_DOUBLE,
ALPHAMODEL_J2000 	BINARY_DOUBLE,
DELTAMODEL_J2000 	BINARY_DOUBLE,
ERRX2MODEL_IMAGE 	BINARY_DOUBLE,
ERRY2MODEL_IMAGE 	BINARY_DOUBLE,
ERRXYMODEL_IMAGE 	BINARY_DOUBLE,
ERRX2MODEL_WORLD 	BINARY_DOUBLE,
ERRY2MODEL_WORLD 	BINARY_DOUBLE,
ERRXYMODEL_WORLD 	BINARY_DOUBLE,
ERRCXXMODEL_IMAGE 	BINARY_FLOAT,
ERRCYYMODEL_IMAGE 	BINARY_FLOAT,
ERRCXYMODEL_IMAGE 	BINARY_FLOAT,
ERRCXXMODEL_WORLD 	BINARY_FLOAT,
ERRCYYMODEL_WORLD 	BINARY_FLOAT,
ERRCXYMODEL_WORLD 	BINARY_FLOAT,
ERRAMODEL_IMAGE 	BINARY_FLOAT,
ERRBMODEL_IMAGE 	BINARY_FLOAT,
ERRTHETAMODEL_IMAGE 	BINARY_FLOAT,
ERRAMODEL_WORLD 	BINARY_FLOAT,
ERRBMODEL_WORLD 	BINARY_FLOAT,
ERRTHETAMODEL_WORLD 	BINARY_FLOAT,
ERRTHETAMODEL_SKY 	BINARY_FLOAT,
ERRTHETAMODEL_J2000 	BINARY_FLOAT,
X2MODEL_IMAGE 		BINARY_DOUBLE,
Y2MODEL_IMAGE 		BINARY_DOUBLE,
XYMODEL_IMAGE 		BINARY_DOUBLE,
E1MODEL_IMAGE 		BINARY_FLOAT,
E2MODEL_IMAGE 		BINARY_FLOAT,
EPS1MODEL_IMAGE 	BINARY_FLOAT,
EPS2MODEL_IMAGE 	BINARY_FLOAT,
FLUX_SPHEROID 		BINARY_FLOAT,
FLUXERR_SPHEROID 	BINARY_FLOAT,
MAG_SPHEROID 		BINARY_FLOAT,
MAGERR_SPHEROID 	BINARY_FLOAT,
SPHEROID_REFF_IMAGE 	BINARY_FLOAT,
SPHEROID_REFFERR_IMAGE 	BINARY_FLOAT,
SPHEROID_REFF_WORLD 	BINARY_FLOAT,
SPHEROID_REFFERR_WORLD 	BINARY_FLOAT,
SPHEROID_ASPECT_IMAGE 	BINARY_FLOAT,
SPHEROID_ASPECTERR_IMAGE 	BINARY_FLOAT,
SPHEROID_ASPECT_WORLD 	BINARY_FLOAT,
SPHEROID_ASPECTERR_WORLD 	BINARY_FLOAT,
SPHEROID_THETA_IMAGE 	BINARY_FLOAT,
SPHEROID_THETAERR_IMAGE BINARY_FLOAT,
SPHEROID_THETA_WORLD 	BINARY_FLOAT,
SPHEROID_THETAERR_WORLD BINARY_FLOAT,
SPHEROID_THETA_SKY 	BINARY_FLOAT,
SPHEROID_THETA_J2000 	BINARY_FLOAT,
FLUX_DISK 		BINARY_FLOAT,
FLUXERR_DISK 		BINARY_FLOAT,
MAG_DISK 		BINARY_FLOAT,
MAGERR_DISK 		BINARY_FLOAT,
DISK_SCALE_IMAGE 	BINARY_FLOAT,
DISK_SCALEERR_IMAGE 	BINARY_FLOAT,
DISK_SCALE_WORLD 	BINARY_FLOAT,
DISK_SCALEERR_WORLD 	BINARY_FLOAT,
DISK_ASPECT_IMAGE 	BINARY_FLOAT,
DISK_ASPECTERR_IMAGE 	BINARY_FLOAT,
DISK_ASPECT_WORLD 	BINARY_FLOAT,
DISK_ASPECTERR_WORLD 	BINARY_FLOAT,
DISK_INCLINATION 	BINARY_FLOAT,
DISK_INCLINATIONERR 	BINARY_FLOAT,
DISK_THETA_IMAGE 	BINARY_FLOAT,
DISK_THETAERR_IMAGE 	BINARY_FLOAT,
DISK_THETA_WORLD 	BINARY_FLOAT,
DISK_THETAERR_WORLD 	BINARY_FLOAT,
DISK_THETA_SKY 		BINARY_FLOAT,
DISK_THETA_J2000 	BINARY_FLOAT,
DR	                NUMBER(10), 
FLUX_PSF 		BINARY_FLOAT,
FLUXERR_PSF 		BINARY_FLOAT,
MAG_PSF 		BINARY_FLOAT,
MAGERR_PSF 		BINARY_FLOAT,
MAG_APER_7 		BINARY_FLOAT,
MAGERR_APER_7 		BINARY_FLOAT,
MAG_APER_8 		BINARY_FLOAT,
MAGERR_APER_8 		BINARY_FLOAT,
MAG_APER_9 		BINARY_FLOAT,
MAGERR_APER_9 		BINARY_FLOAT,
MAG_APER_10 		BINARY_FLOAT,
MAGERR_APER_10 		BINARY_FLOAT,
MAG_APER_11 		BINARY_FLOAT,
MAGERR_APER_11 		BINARY_FLOAT,
MAG_APER_12 		BINARY_FLOAT,
MAGERR_APER_12 		BINARY_FLOAT,
MAG_APER_13 		BINARY_FLOAT,
MAGERR_APER_13 		BINARY_FLOAT,
MAG_APER_14 		BINARY_FLOAT,
MAGERR_APER_14 		BINARY_FLOAT,
MAG_APER_15 		BINARY_FLOAT,
MAGERR_APER_15 		BINARY_FLOAT,
MAG_APER_16 		BINARY_FLOAT,
MAGERR_APER_16 		BINARY_FLOAT,
MAG_APER_17 		BINARY_FLOAT,
MAGERR_APER_17 		BINARY_FLOAT,
ERRAWIN_WORLD		BINARY_FLOAT,
ERRBWIN_WORLD		BINARY_FLOAT,
ERRTHETAWIN_J2000	BINARY_FLOAT,
ALPHAPSF_J2000 binary_double,
ALPHAWIN_J2000 binary_double,
AMODEL_IMAGE binary_float,
AMODEL_WORLD binary_float,
A_WORLD binary_float,
BMODEL_IMAGE binary_float,
BMODEL_WORLD binary_float,
B_WORLD binary_float,
CHI2_PSF binary_float,
CONCENTRATION_MODEL binary_float,
DELTAPSF_J2000 binary_double,
DELTAWIN_J2000 binary_double,
ELLIP1MODEL_IMAGE binary_float,
ELLIP1MODEL_WORLD binary_float,
ELLIP2MODEL_IMAGE binary_float,
ELLIP2MODEL_WORLD binary_float,
ELONGATION binary_float,
ERRAPSF_IMAGE binary_float,
ERRAPSF_WORLD binary_float,
ERRAWIN_IMAGE binary_float,
ERRBPSF_IMAGE binary_float,
ERRBPSF_WORLD binary_float,
ERRBWIN_IMAGE binary_float,
ERRTHETAPSF_IMAGE binary_float,
ERRTHETAPSF_J2000 binary_float,
ERRTHETAWIN_IMAGE binary_float,
FLUX_APER_1 binary_float,
FLUX_APER_2 binary_float,
FLUX_APER_3 binary_float,
FLUX_APER_4 binary_float,
FLUX_APER_5 binary_float,
FLUX_APER_6 binary_float,
FLUX_APER_7 binary_float,
FLUX_APER_8 binary_float,
FLUX_APER_9 binary_float,
FLUX_APER_10 binary_float,
FLUX_APER_11 binary_float,
FLUX_APER_12 binary_float,
FLUX_APER_13 binary_float,
FLUX_APER_14 binary_float,
FLUX_APER_15 binary_float,
FLUX_APER_16 binary_float,
FLUX_APER_17 binary_float,
FLUX_AUTO binary_float,
FLUX_EFF_MODEL binary_float,
FLUXERR_APER_1 binary_float,
FLUXERR_APER_2 binary_float,
FLUXERR_APER_3 binary_float,
FLUXERR_APER_4 binary_float,
FLUXERR_APER_5 binary_float,
FLUXERR_APER_6 binary_float,
FLUXERR_APER_7 binary_float,
FLUXERR_APER_8 binary_float,
FLUXERR_APER_9 binary_float,
FLUXERR_APER_10 binary_float,
FLUXERR_APER_11 binary_float,
FLUXERR_APER_12 binary_float,
FLUXERR_APER_13 binary_float,
FLUXERR_APER_14 binary_float,
FLUXERR_APER_15 binary_float,
FLUXERR_APER_16 binary_float,
FLUXERR_APER_17 binary_float,
FLUXERR_AUTO binary_float,
FLUXERR_ISO binary_float,
FLUXERR_PETRO binary_float,
FLUX_ISO binary_float,
FLUX_MAX binary_float,
FLUX_MAX_MODEL binary_float,
FLUX_MEAN_MODEL binary_float,
FLUX_PETRO binary_float,
ISOAREAF_IMAGE binary_float,
ISOAREAF_WORLD binary_float,
ISOAREA_IMAGE binary_float,
KRON_RADIUS binary_float,
MAGERR_ISO binary_float,
MAGERR_PETRO binary_float,
MAG_ISO binary_float,
MAG_PETRO binary_float,
MU_EFF_MODEL binary_float,
MU_MAX binary_float,
MU_MAX_MODEL binary_float,
MU_MEAN_MODEL binary_float,
MU_THRESHOLD binary_float,
NITER_PSF number(10),
PETRO_RADIUS binary_float,
THETAMODEL_IMAGE binary_float,
THETAMODEL_J2000 binary_float,
XPEAK_IMAGE number(10),
XPSF_IMAGE binary_double,
XWIN_IMAGE binary_double,
YPEAK_IMAGE number(10),
YPSF_IMAGE binary_double,
YWIN_IMAGE binary_double,
DURATION_ANALYSIS binary_float,
FLAGS_WEIGHT VARCHAR2(1),
NLOWWEIGHT_ISO NUMBER,
NLOWDWEIGHT_ISO NUMBER,
SPREAD_MODEL BINARY_FLOAT,
SPREADERR_MODEL BINARY_FLOAT,
Constraint PKOBJECTS PRIMARY KEY (OBJECT_ID)); 

-- 
-- TABLE: PSMFIT 
-- Dora 09/04/2009 add new column mag_type per Douglas request (JIRA DES-1100)
--

CREATE TABLE PSMFIT(
    PSMFIT_ID          NUMBER(5, 0)      NOT NULL,
    NITE               VARCHAR2(12),
    MJDLO              NUMBER(15, 10),
    MJDHI              NUMBER(15, 10),
    CCDID              NUMBER(4, 0),
    FILTER             VARCHAR2(8),
    A                  NUMBER(10, 6),
    AERR               NUMBER(10, 6),
    B                  NUMBER(10, 6),
    BERR               NUMBER(10, 6),
    K                  NUMBER(10, 6),
    KERR               NUMBER(10, 6),
    RMS                NUMBER(10, 6),
    CHI2               NUMBER(10, 6),
    DOF                NUMBER(12, 0),
    PHOTOMETRICFLAG    NUMBER(2, 0),
    PSMVERSION         VARCHAR2(20),
    FIT_TIMESTAMP      TIMESTAMP(6),
    CFILTER            VARCHAR2(8),
    STDCOLOR0          NUMBER(10, 6),
    ASOLVE             NUMBER(2, 0),
    BSOLVE             NUMBER(2, 0),
    KSOLVE             NUMBER(2, 0),
    RUN                VARCHAR2(100),
    PROJECT            VARCHAR2(20),
    MAG_TYPE	       VARCHAR2(15),
    CONSTRAINT PK_PSMFIT PRIMARY KEY (PSMFIT_ID)
)
;



-- 
-- TABLE: RUNIDDESC_ACCESS 
--

CREATE TABLE RUNIDDESC_ACCESS(
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    RUNIDDESC    VARCHAR2(80)    NOT NULL,
    CONSTRAINT PK_RUNIDDESC_ACCESS PRIMARY KEY (DES_GROUP, RUNIDDESC)
)
;



-- 
-- TABLE: SHAPELET_OBJECTS 
--

CREATE TABLE SHAPELET_OBJECTS(
    OBJECT_ID    NUMBER(11, 0)    NOT NULL,
    MAG_AUTO     BINARY_FLOAT,
    FLAGS        NUMBER(10, 0),
    SIGMA        BINARY_FLOAT,
    COEFF1       BINARY_FLOAT,
    COEFF2       BINARY_FLOAT,
    COEFF3       BINARY_FLOAT,
    COEFF4       BINARY_FLOAT,
    COEFF5       BINARY_FLOAT,
    COEFF6       BINARY_FLOAT
)
;



-- 
-- TABLE: SHAPELET_PSF 
--

CREATE TABLE SHAPELET_PSF(
    OBJECT_ID    NUMBER(11, 0)    NOT NULL,
    MAG_AUTO     BINARY_FLOAT,
    FLAGS        NUMBER(10, 0),
    SIGMA        BINARY_FLOAT,
    COEFF1       BINARY_FLOAT,
    COEFF2       BINARY_FLOAT,
    COEFF3       BINARY_FLOAT,
    COEFF4       BINARY_FLOAT,
    COEFF5       BINARY_FLOAT,
    COEFF6       BINARY_FLOAT,
    COEFF7       BINARY_FLOAT,
    COEFF8       BINARY_FLOAT,
    COEFF9       BINARY_FLOAT,
    COEFF10      BINARY_FLOAT,
    COEFF11      BINARY_FLOAT,
    COEFF12      BINARY_FLOAT,
    COEFF13      BINARY_FLOAT,
    COEFF14      BINARY_FLOAT,
    COEFF15      BINARY_FLOAT
)
;



-- 
-- TABLE: SITES 
--

CREATE TABLE SITES(
    SITE_NAME            VARCHAR2(32)     NOT NULL,
    SITE_ID              NUMBER(3, 0)     NOT NULL,
    LOGIN_HOST           VARCHAR2(255),
    LOGIN_GSISSH_PORT    VARCHAR2(8),
    GRID_HOST            VARCHAR2(255),
    GRID_PORT            VARCHAR2(8),
    GRID_TYPE            VARCHAR2(8),
    GRIDFTP_HOST         VARCHAR2(255),
    GRIDFTP_PORT         VARCHAR2(8),
    COMPUTE_PPN          NUMBER(3, 0),
    BATCH_TYPE           VARCHAR2(16),
    CONSTRAINT SYS_C005678 PRIMARY KEY (SITE_NAME)
)
;



-- 
-- TABLE: SOFTWARE 
--

CREATE TABLE SOFTWARE(
    SW_VERSION_N    NUMBER(10, 0)    NOT NULL,
    RELEASE_DATE    TIMESTAMP(6),
    CVS_VERSION     VARCHAR2(20),
    NOTES           VARCHAR2(500),
    CONSTRAINT PK_SOFTWARE PRIMARY KEY (SW_VERSION_N)
)
;



-- 
-- TABLE: STANDARD_STARS 
-- 09/05/08 Dora added 3 columns

CREATE TABLE STANDARD_STARS(
    STANDARD_STAR_ID    NUMBER(10, 0)    NOT NULL,
    NAME                VARCHAR2(40)     NOT NULL,
    RADEG               NUMBER(10, 6),
    DECDEG              NUMBER(10, 6),
    STDMAG_U            NUMBER(10, 6),
    STDMAG_G            NUMBER(10, 6),
    STDMAG_R            NUMBER(10, 6),
    STDMAG_I            NUMBER(10, 6),
    STDMAG_Z            NUMBER(10, 6),
    STDMAGERR_U         NUMBER(10, 6),
    STDMAGERR_G         NUMBER(10, 6),
    STDMAGERR_R         NUMBER(10, 6),
    STDMAGERR_I         NUMBER(10, 6),
    STDMAGERR_Z         NUMBER(10, 6),
    NOBS_U              NUMBER(10, 6),
    NOBS_G              NUMBER(10, 6),
    NOBS_R              NUMBER(10, 6),
    NOBS_I              NUMBER(10, 6),
    NOBS_Z              NUMBER(10, 6),
    FIELDNAME           VARCHAR2(40),
    VERSION_ID          NUMBER(10, 6),
    STDMAG_Y            NUMBER(10, 6),
    STDMAGERR_Y         NUMBER(10, 6),
    NOBS_Y              NUMBER(10, 6),
    CONSTRAINT PK_STANDARD_STARS PRIMARY KEY (STANDARD_STAR_ID)
)
;



-- 
-- TABLE: SURVEY_TILES 
-- Dora Cai drop the table on 07/15/2009 
--

--CREATE TABLE SURVEY_TILES(
    --LAYER_N        NUMBER(10, 0)    NOT NULL,
    --TILE_N         NUMBER(10, 0)    NOT NULL,
    --RA_CENTER      NUMBER(10, 6),
    --DECL_CENTER    NUMBER(10, 6),
    --EQUINOX        NUMBER(4, 0),
    --CONSTRAINT PK_SURVEY_TILES PRIMARY KEY (LAYER_N, TILE_N)
--)
--;



-- 
-- TABLE: UCAC2 
--

CREATE TABLE UCAC2(
    UCAC2_ID      NUMBER(9, 0)     NOT NULL,
    RA            NUMBER(10, 7),
    DEC           NUMBER(10, 7),
    U2RMAG        NUMBER(5, 0),
    E_RAM         NUMBER(3, 0),
    E_DEM         NUMBER(3, 0),
    NOBS          NUMBER(3, 0),
    E_POS         NUMBER(3, 0),
    NCAT          NUMBER(3, 0),
    CFLG          NUMBER(3, 0),
    EPRAM         NUMBER(6, 0),
    EPDEM         NUMBER(6, 0),
    PMRA          NUMBER(10, 0),
    PMDEC         NUMBER(10, 0),
    E_PMRA        NUMBER(3, 0),
    E_PMDEC       NUMBER(3, 0),
    Q_PMRA        NUMBER(3, 0),
    Q_PMDEC       NUMBER(3, 0),
    TWOMASS_ID    NUMBER(10, 0),
    TWOMASS_J     NUMBER(6, 0),
    TWOMASS_H     NUMBER(6, 0),
    TWOMASS_KS    NUMBER(6, 0),
    TWOMASS_PH    VARCHAR2(4),
    TWOMASS_CC    VARCHAR2(4),
    HTMID         NUMBER(16, 0),
    CX            NUMBER(10, 6),
    CY            NUMBER(10, 6),
    CZ            NUMBER(10, 6),
    CONSTRAINT PK_UCAC2 PRIMARY KEY (UCAC2_ID)
)
;



-- 
-- TABLE: USER_REGISTRATION 
-- 01/22/09 drop column prefered_archive_site, add archivesites

CREATE TABLE USER_REGISTRATION(
    NVO_USER               VARCHAR2(30)     NOT NULL,
    FIRST_NAME             VARCHAR2(30),
    LAST_NAME              VARCHAR2(30),
    AFFILIATION            VARCHAR2(100),
    TELEPHONE              VARCHAR2(30),
    EMAIL                  VARCHAR2(30),
    ADMIN_ROLE             CHAR(1),
    WEBMASTER_ROLE         CHAR(1),
    COUNTRY                VARCHAR2(20),
    CREATE_TIME            DATE,
    ARCHIVESITES           VARCHAR2(30)     DEFAULT 'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN',
    CONSTRAINT PK_USER_REGISTRATION PRIMARY KEY (NVO_USER)
)
;



-- 
-- TABLE: USNOB_CAT1 
--

CREATE TABLE USNOB_CAT1(
    USNOB_CAT1_ID       NUMBER(11, 0)    NOT NULL,
    STAR_ID             VARCHAR2(13),
    RA                  NUMBER(10, 7),
    DEC                 NUMBER(10, 7),
    SRA                 NUMBER(5, 2),
    SDE                 NUMBER(5, 2),
    EPOCH               NUMBER(9, 4),
    MURA                NUMBER(6, 2),
    MUDEC               NUMBER(6, 2),
    MUPROB              NUMBER(5, 2),
    SMURA               NUMBER(5, 2),
    SMUDE               NUMBER(5, 2),
    SFITRA              NUMBER(6, 2),
    SFITDE              NUMBER(6, 2),
    NFITPT              NUMBER(1, 0),
    FLAGS               NUMBER(1, 0),
    B1                  NUMBER(6, 2),
    B1_MAGFLG           NUMBER(2, 0),
    B1_FLDID            NUMBER(4, 0),
    B1_SG               NUMBER(2, 0),
    B1_XRESID           NUMBER(5, 2),
    B1_YRESID           NUMBER(5, 2),
    R1                  NUMBER(6, 2),
    R1_MAGFLG           NUMBER(2, 0),
    R1_FLDID            NUMBER(4, 0),
    R1_SG               NUMBER(2, 0),
    R1_XRESID           NUMBER(5, 2),
    R1_YRESID           NUMBER(5, 2),
    B2                  NUMBER(6, 2),
    B2_MAGFLG           NUMBER(2, 0),
    B2_FLDID            NUMBER(4, 0),
    B2_SG               NUMBER(2, 0),
    B2_XRESID           NUMBER(5, 2),
    B2_YRESID           NUMBER(5, 2),
    R2                  NUMBER(6, 2),
    R2_MAGFLG           NUMBER(2, 0),
    R2_FLDID            NUMBER(4, 0),
    R2_SG               NUMBER(2, 0),
    R2_XRESID           NUMBER(5, 2),
    R2_YRESID           NUMBER(5, 2),
    I2                  NUMBER(6, 2),
    I2_MAGFLG           NUMBER(2, 0),
    I2_FLDID            NUMBER(4, 0),
    I2_SG               NUMBER(2, 0),
    I2_XRESID           NUMBER(5, 2),
    I2_YRESID           NUMBER(5, 2),
    XI                  NUMBER(7, 4),
    ETA                 NUMBER(7, 4),
    DISTCTR             NUMBER(7, 4),
    CX                  NUMBER(10, 6),
    CY                  NUMBER(10, 6),
    CZ                  NUMBER(10, 6),
    HTMID               NUMBER(16, 0),
    LOADING_FILENAME    VARCHAR2(30),
    OLD_RA              NUMBER(10, 7),
    CONSTRAINT PK_USNOB_CAT1 PRIMARY KEY (USNOB_CAT1_ID)
)
;



-- 
-- TABLE: ZEROPOINT 
--

CREATE TABLE ZEROPOINT(
    ID         NUMBER(10, 0),
    IMAGEID    NUMBER(10, 0),
    INSERT_DATE  DATE,
    MAG_ZERO     BINARY_FLOAT,
    SIGMA_MAG_ZERO BINARY_FLOAT,
    ORIGINID    NUMBER(10, 0),
    SOURCE      VARCHAR2(10),
    CONSTRAINT PK_ZP PRIMARY KEY (ID)
)
;

--
-- TABLE: MATCHES
-- Dora Cai added on 05/22/08
-- 

CREATE TABLE MATCHES(
    COADD_OBJECTS_ID  NUMBER(11, 0)    NOT NULL,
    OBJECT_ID    NUMBER(11,0) NOT NULL,
    OFFSET	 BINARY_FLOAT,
    SIGMA_OFFSET BINARY_FLOAT,
    CONSTRAINT PK_MATCHES PRIMARY KEY (COADD_OBJECTS_ID, OBJECT_ID)
);

--
-- TABLE: OBJECTS_2008
-- Dora Cai added on 05/27/08
-- Modified by Dora on 07/04/08 to add 1 column, catalogid.
-- Modified by Dora on 10/30/08 to rename the column "run" to "partkey"
-- Modified by Dora on 11/03/08, add two new columns: ra_num and dec_num
-- Modified by Dora on 11/14/08, rename columns ra_num to ra and dec_num to dec
-- Dora Cai added 80 columns on 07/30/2009 per Shantanu's request (JIRA DES-1030)
-- Dora Cai added 1 new column, release_code, on 08/05/2009 per Tony's request (JIRA DES-1061) 
-- Dora Cai rename column release_code to dr (means: data release) on 08/07/2009 per Joe's request (JIRA DES-1061) 
-- Dora Cai added 26 new columns per Joe's request (JIRA DES-1079) 
-- Dora Cai added 3 new columns, on 08/31/2009 per Shantanu's request (JIRA DES-1170) 
-- Dora Cai added 89 new columns, on 09/24/2009 per Shantanu's request (JIRA DES-1260) 
-- Dora Cai on 10/01/2009 added 3 columns on flags_weight, nlowweight_iso and nlowdweight_iso (JIRA DES-1178)
-- Dora Cai on 10/05/2009 added 2 columns: spread_model and spreaderr_model (JIRA DES-1178)
-- 

CREATE TABLE OBJECTS_2008(
OBJECT_ID               Number(11) NOT NULL,
EQUINOX                 BINARY_FLOAT,
BAND                    varchar2(10),
HTMID                   Number(16),
CX                      BINARY_DOUBLE,
CY                      BINARY_DOUBLE,
CZ                      BINARY_DOUBLE,
PARENTID                Number(11),
SOFTID                  Number(4),
IMAGEID                 Number(9),
ZEROPOINT               BINARY_FLOAT,
ERRZEROPOINT            BINARY_FLOAT,
ZEROPOINTID             Number(10),
OBJECT_NUMBER           Number(6),
MAG_AUTO                BINARY_FLOAT,
MAGERR_AUTO             BINARY_FLOAT,
MAG_APER_1              BINARY_FLOAT,
MAGERR_APER_1           BINARY_FLOAT,
MAG_APER_2              BINARY_FLOAT,
MAGERR_APER_2           BINARY_FLOAT,
MAG_APER_3              BINARY_FLOAT,
MAGERR_APER_3           BINARY_FLOAT,
MAG_APER_4              BINARY_FLOAT,
MAGERR_APER_4           BINARY_FLOAT,
MAG_APER_5              BINARY_FLOAT,
MAGERR_APER_5           BINARY_FLOAT,
MAG_APER_6              BINARY_FLOAT,
MAGERR_APER_6           BINARY_FLOAT,
ALPHA_J2000             BINARY_DOUBLE,
DELTA_J2000             BINARY_DOUBLE,
ALPHAPEAK_J2000         BINARY_DOUBLE,
DELTAPEAK_J2000         BINARY_DOUBLE,
X2_WORLD                Binary_FLOAT,
ERRX2_WORLD             BINARY_DOUBLE,
Y2_WORLD                Binary_FLOAT,
ERRY2_WORLD             BINARY_DOUBLE,
XY_WORLD                Binary_FLOAT,
ERRXY_WORLD             BINARY_DOUBLE,
THRESHOLD               Binary_FLOAT,
X_IMAGE                 Binary_FLOAT,
Y_IMAGE                 Binary_FLOAT,
XMIN_IMAGE              Binary_FLOAT,
YMIN_IMAGE              Binary_FLOAT,
XMAX_IMAGE              Binary_FLOAT,
YMAX_IMAGE              Binary_FLOAT,
X2_IMAGE                Binary_FLOAT,
ERRX2_IMAGE             Binary_FLOAT,
Y2_IMAGE                Binary_FLOAT,
ERRY2_IMAGE             Binary_FLOAT,
XY_IMAGE                Binary_FLOAT,
ERRXY_IMAGE             Binary_FLOAT,
A_IMAGE                 Binary_FLOAT,
ERRA_IMAGE              Binary_FLOAT,
B_IMAGE                 Binary_FLOAT,
ERRB_IMAGE              Binary_FLOAT,
THETA_IMAGE             Binary_FLOAT,
ERRTHETA_IMAGE          Binary_FLOAT,
ELLIPTICITY             Binary_FLOAT,
CLASS_STAR              Binary_FLOAT,
FLAGS                   Number(3),
FLUX_RADIUS             Binary_FLOAT,
FWHM_WORLD              Binary_FLOAT,
ISOAREA_WORLD           Binary_FLOAT,
THETA_J2000             Binary_FLOAT,
BACKGROUND              Binary_FLOAT, 
PARTKEY			Varchar2(30),
CATALOGID               Number(9),
RA                      Number(8,5),
DEC                     Number(8,5),
CHI2_MODEL 		BINARY_FLOAT,
FLAGS_MODEL 		NUMBER(1),
NITER_MODEL 		NUMBER(5),
FLUX_MODEL 		BINARY_FLOAT,
FLUXERR_MODEL 		BINARY_FLOAT,
MAG_MODEL 		BINARY_FLOAT,
MAGERR_MODEL 		BINARY_FLOAT,
XMODEL_IMAGE 		BINARY_FLOAT,
YMODEL_IMAGE 		BINARY_FLOAT,
XMODEL_WORLD 		BINARY_DOUBLE,
YMODEL_WORLD 		BINARY_DOUBLE,
ALPHAMODEL_SKY 		BINARY_DOUBLE,
DELTAMODEL_SKY 		BINARY_DOUBLE,
ALPHAMODEL_J2000 	BINARY_DOUBLE,
DELTAMODEL_J2000 	BINARY_DOUBLE,
ERRX2MODEL_IMAGE 	BINARY_DOUBLE,
ERRY2MODEL_IMAGE 	BINARY_DOUBLE,
ERRXYMODEL_IMAGE 	BINARY_DOUBLE,
ERRX2MODEL_WORLD 	BINARY_DOUBLE,
ERRY2MODEL_WORLD 	BINARY_DOUBLE,
ERRXYMODEL_WORLD 	BINARY_DOUBLE,
ERRCXXMODEL_IMAGE 	BINARY_FLOAT,
ERRCYYMODEL_IMAGE 	BINARY_FLOAT,
ERRCXYMODEL_IMAGE 	BINARY_FLOAT,
ERRCXXMODEL_WORLD 	BINARY_FLOAT,
ERRCYYMODEL_WORLD 	BINARY_FLOAT,
ERRCXYMODEL_WORLD 	BINARY_FLOAT,
ERRAMODEL_IMAGE 	BINARY_FLOAT,
ERRBMODEL_IMAGE 	BINARY_FLOAT,
ERRTHETAMODEL_IMAGE 	BINARY_FLOAT,
ERRAMODEL_WORLD 	BINARY_FLOAT,
ERRBMODEL_WORLD 	BINARY_FLOAT,
ERRTHETAMODEL_WORLD 	BINARY_FLOAT,
ERRTHETAMODEL_SKY 	BINARY_FLOAT,
ERRTHETAMODEL_J2000 	BINARY_FLOAT,
X2MODEL_IMAGE 		BINARY_DOUBLE,
Y2MODEL_IMAGE 		BINARY_DOUBLE,
XYMODEL_IMAGE 		BINARY_DOUBLE,
E1MODEL_IMAGE 		BINARY_FLOAT,
E2MODEL_IMAGE 		BINARY_FLOAT,
EPS1MODEL_IMAGE 	BINARY_FLOAT,
EPS2MODEL_IMAGE 	BINARY_FLOAT,
FLUX_SPHEROID 		BINARY_FLOAT,
FLUXERR_SPHEROID 	BINARY_FLOAT,
MAG_SPHEROID 		BINARY_FLOAT,
MAGERR_SPHEROID 	BINARY_FLOAT,
SPHEROID_REFF_IMAGE 	BINARY_FLOAT,
SPHEROID_REFFERR_IMAGE 	BINARY_FLOAT,
SPHEROID_REFF_WORLD 	BINARY_FLOAT,
SPHEROID_REFFERR_WORLD 	BINARY_FLOAT,
SPHEROID_ASPECT_IMAGE 	BINARY_FLOAT,
SPHEROID_ASPECTERR_IMAGE 	BINARY_FLOAT,
SPHEROID_ASPECT_WORLD 	BINARY_FLOAT,
SPHEROID_ASPECTERR_WORLD 	BINARY_FLOAT,
SPHEROID_THETA_IMAGE 	BINARY_FLOAT,
SPHEROID_THETAERR_IMAGE BINARY_FLOAT,
SPHEROID_THETA_WORLD 	BINARY_FLOAT,
SPHEROID_THETAERR_WORLD BINARY_FLOAT,
SPHEROID_THETA_SKY 	BINARY_FLOAT,
SPHEROID_THETA_J2000 	BINARY_FLOAT,
FLUX_DISK 		BINARY_FLOAT,
FLUXERR_DISK 		BINARY_FLOAT,
MAG_DISK 		BINARY_FLOAT,
MAGERR_DISK 		BINARY_FLOAT,
DISK_SCALE_IMAGE 	BINARY_FLOAT,
DISK_SCALEERR_IMAGE 	BINARY_FLOAT,
DISK_SCALE_WORLD 	BINARY_FLOAT,
DISK_SCALEERR_WORLD 	BINARY_FLOAT,
DISK_ASPECT_IMAGE 	BINARY_FLOAT,
DISK_ASPECTERR_IMAGE 	BINARY_FLOAT,
DISK_ASPECT_WORLD 	BINARY_FLOAT,
DISK_ASPECTERR_WORLD 	BINARY_FLOAT,
DISK_INCLINATION 	BINARY_FLOAT,
DISK_INCLINATIONERR 	BINARY_FLOAT,
DISK_THETA_IMAGE 	BINARY_FLOAT,
DISK_THETAERR_IMAGE 	BINARY_FLOAT,
DISK_THETA_WORLD 	BINARY_FLOAT,
DISK_THETAERR_WORLD 	BINARY_FLOAT,
DISK_THETA_SKY 		BINARY_FLOAT,
DISK_THETA_J2000 	BINARY_FLOAT,
DR	            NUMBER(10), 
FLUX_PSF 		BINARY_FLOAT,
FLUXERR_PSF 		BINARY_FLOAT,
MAG_PSF 		BINARY_FLOAT,
MAGERR_PSF 		BINARY_FLOAT,
MAG_APER_7 		BINARY_FLOAT,
MAGERR_APER_7 		BINARY_FLOAT,
MAG_APER_8 		BINARY_FLOAT,
MAGERR_APER_8 		BINARY_FLOAT,
MAG_APER_9 		BINARY_FLOAT,
MAGERR_APER_9 		BINARY_FLOAT,
MAG_APER_10 		BINARY_FLOAT,
MAGERR_APER_10 		BINARY_FLOAT,
MAG_APER_11 		BINARY_FLOAT,
MAGERR_APER_11 		BINARY_FLOAT,
MAG_APER_12 		BINARY_FLOAT,
MAGERR_APER_12 		BINARY_FLOAT,
MAG_APER_13 		BINARY_FLOAT,
MAGERR_APER_13 		BINARY_FLOAT,
MAG_APER_14 		BINARY_FLOAT,
MAGERR_APER_14 		BINARY_FLOAT,
MAG_APER_15 		BINARY_FLOAT,
MAGERR_APER_15 		BINARY_FLOAT,
MAG_APER_16 		BINARY_FLOAT,
MAGERR_APER_16 		BINARY_FLOAT,
MAG_APER_17 		BINARY_FLOAT,
MAGERR_APER_17 		BINARY_FLOAT,
ERRAWIN_WORLD		BINARY_FLOAT,
ERRBWIN_WORLD		BINARY_FLOAT,
ERRTHETAWIN_J2000	BINARY_FLOAT,
ALPHAPSF_J2000 binary_double,
ALPHAWIN_J2000 binary_double,
AMODEL_IMAGE binary_float,
AMODEL_WORLD binary_float,
A_WORLD binary_float,
BMODEL_IMAGE binary_float,
BMODEL_WORLD binary_float,
B_WORLD binary_float,
CHI2_PSF binary_float,
CONCENTRATION_MODEL binary_float,
DELTAPSF_J2000 binary_double,
DELTAWIN_J2000 binary_double,
ELLIP1MODEL_IMAGE binary_float,
ELLIP1MODEL_WORLD binary_float,
ELLIP2MODEL_IMAGE binary_float,
ELLIP2MODEL_WORLD binary_float,
ELONGATION binary_float,
ERRAPSF_IMAGE binary_float,
ERRAPSF_WORLD binary_float,
ERRAWIN_IMAGE binary_float,
ERRBPSF_IMAGE binary_float,
ERRBPSF_WORLD binary_float,
ERRBWIN_IMAGE binary_float,
ERRTHETAPSF_IMAGE binary_float,
ERRTHETAPSF_J2000 binary_float,
ERRTHETAWIN_IMAGE binary_float,
FLUX_APER_1 binary_float,
FLUX_APER_2 binary_float,
FLUX_APER_3 binary_float,
FLUX_APER_4 binary_float,
FLUX_APER_5 binary_float,
FLUX_APER_6 binary_float,
FLUX_APER_7 binary_float,
FLUX_APER_8 binary_float,
FLUX_APER_9 binary_float,
FLUX_APER_10 binary_float,
FLUX_APER_11 binary_float,
FLUX_APER_12 binary_float,
FLUX_APER_13 binary_float,
FLUX_APER_14 binary_float,
FLUX_APER_15 binary_float,
FLUX_APER_16 binary_float,
FLUX_APER_17 binary_float,
FLUX_AUTO binary_float,
FLUX_EFF_MODEL binary_float,
FLUXERR_APER_1 binary_float,
FLUXERR_APER_2 binary_float,
FLUXERR_APER_3 binary_float,
FLUXERR_APER_4 binary_float,
FLUXERR_APER_5 binary_float,
FLUXERR_APER_6 binary_float,
FLUXERR_APER_7 binary_float,
FLUXERR_APER_8 binary_float,
FLUXERR_APER_9 binary_float,
FLUXERR_APER_10 binary_float,
FLUXERR_APER_11 binary_float,
FLUXERR_APER_12 binary_float,
FLUXERR_APER_13 binary_float,
FLUXERR_APER_14 binary_float,
FLUXERR_APER_15 binary_float,
FLUXERR_APER_16 binary_float,
FLUXERR_APER_17 binary_float,
FLUXERR_AUTO binary_float,
FLUXERR_ISO binary_float,
FLUXERR_PETRO binary_float,
FLUX_ISO binary_float,
FLUX_MAX binary_float,
FLUX_MAX_MODEL binary_float,
FLUX_MEAN_MODEL binary_float,
FLUX_PETRO binary_float,
ISOAREAF_IMAGE binary_float,
ISOAREAF_WORLD binary_float,
ISOAREA_IMAGE binary_float,
KRON_RADIUS binary_float,
MAGERR_ISO binary_float,
MAGERR_PETRO binary_float,
MAG_ISO binary_float,
MAG_PETRO binary_float,
MU_EFF_MODEL binary_float,
MU_MAX binary_float,
MU_MAX_MODEL binary_float,
MU_MEAN_MODEL binary_float,
MU_THRESHOLD binary_float,
NITER_PSF number(10),
PETRO_RADIUS binary_float,
THETAMODEL_IMAGE binary_float,
THETAMODEL_J2000 binary_float,
XPEAK_IMAGE number(10),
XPSF_IMAGE binary_double,
XWIN_IMAGE binary_double,
YPEAK_IMAGE number(10),
YPSF_IMAGE binary_double,
YWIN_IMAGE binary_double,
DURATION_ANALYSIS binary_float,
FLAGS_WEIGHT VARCHAR2(1),
NLOWWEIGHT_ISO NUMBER,
NLOWDWEIGHT_ISO NUMBER,
SPREAD_MODEL BINARY_FLOAT,
SPREADERR_MODEL BINARY_FLOAT,
Constraint PK_OBJECTS_2008 PRIMARY KEY (OBJECT_ID))
partition by list(partkey)
(partition init_partition values('before2008'));

-- 
-- TABLE: NITE_ACCESS 
-- Dora Cai added a new column, project, on 05/29/08
--

CREATE TABLE NITE_ACCESS(
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    NITE         VARCHAR2(80)    NOT NULL,
    PROJECT 	 VARCHAR2(20)	 NOT NULL,
    CONSTRAINT PK_NITE_ACCESS PRIMARY KEY (DES_GROUP, NITE, PROJECT)
)
;

-- 
-- TABLE: Spec_Standards 
-- Dora Cai created the new table on 06/25/08
--

CREATE TABLE Spec_Standards (
    spec_id      number(11) NOT NULL,
    source_id    number(20),
    ra           binary_double,
    dec          binary_double,
    zspec        binary_float,
    zspec_err    binary_float,
    zspec_min    binary_float,
    zspec_max    binary_float,
    source_type  varchar2(10),
    confidence	 number(2),
    u            binary_float,
    u_err        binary_float,
    g            binary_float,
    g_err        binary_float,
    r            binary_float,
    r_err        binary_float,
    i            binary_float,
    i_err        binary_float,
    z            binary_float,
    z_err        binary_float,
    u_jkc        binary_float,
    u_jkc_err    binary_float,
    g_jkc        binary_float,
    g_jkc_err    binary_float,
    r_jkc        binary_float,
    r_jkc_err    binary_float,
    i_jkc        binary_float,
    i_jkc_err    binary_float,
    k_jkc        binary_float,
    k_jkc_err    binary_float,
    source 	varchar2(40),
    constraint pk_spec_standards primary key (spec_id))
;
-- 
-- TABLE: SNScan 
-- Dora Cai created the new table on 07/02/08
--
CREATE TABLE SNScan (
SNScanId number(9),
SNId number(9),
SNObsId number(9),
ScanDate date,
CategoryType varchar2(20),
Scanner varchar2(20),
constraint pk_snscan primary key (SNScanId));

create sequence snscan_seq cache 100 start with 1 increment by 1;


-- 
-- TABLE: coadd_objects 
-- Dora Cai modified the data types and add two more columns: errx2_world and erry2_world, on 07/02/08
-- Dora Cai added 4 columns on 07/04/08: catalogid_g, catalogid_r, catalogid_i, catalogid_z)
-- Dora Cai added 4 columns on 08/03/08 with data type NUMBER(4): xmin_image, ymin_image, xmax_image, ymax_image)
-- Dora Cai added columns: ra, dec, and 24 columns for the Y-band
-- Dora Cai added columns: fwhm_world_g, fwhm_world_r, fwhm_world_i, 
-- fwhm_world_z, fwhm_world_y.
-- Dora Cai added 1 new column, release_code, on 08/05/2009 per Tony's request (JIRA DES-1061) 
-- Dora Cai rename column release_code to dr (means: data release) on 08/07/2009 per Joe's request (JIRA DES-1061) 
-- Dora Cai on 09/11/2009 added 110 columns for mag_aper_7..17 and 
-- magerr_aper_7..17 for each band to the table per Shantanu's request (JIRA DES-1178)
-- Dora Cai on 09/25/2009 added 634 columns (JIRA DES-1178)
-- Dora Cai on 09/30/2009 added 5 columns on duration_analysis (JIRA DES-1178)
-- Dora Cai on 09/30/2009 added 11 columns on flags_weight, nlowweight_iso and nlowdweight_iso (JIRA DES-1178)
-- Dora Cai on 10/05/2009 added 10 columns on spread_model and spreaderr_model (JIRA DES-1178)
create table coadd_objects (
 COADD_OBJECTS_ID                         NUMBER(11),
 EQUINOX                                  BINARY_FLOAT,
 HTMID                                    NUMBER(16),
 CX                                       BINARY_DOUBLE,
 CY                                       BINARY_DOUBLE,
 CZ                                       BINARY_DOUBLE,
 SOFTID                                   NUMBER(4),
 IMAGEID_G                                NUMBER(9),
 IMAGEID_R                                NUMBER(9),
 IMAGEID_I                                NUMBER(9),
 IMAGEID_Z                                NUMBER(9),
 ZEROPOINT_G                              BINARY_FLOAT,
 ZEROPOINT_R                              BINARY_FLOAT,
 ZEROPOINT_I                              BINARY_FLOAT,
 ZEROPOINT_Z                              BINARY_FLOAT,
 ERRZEROPOINT_G                           BINARY_FLOAT,
 ERRZEROPOINT_R                           BINARY_FLOAT,
 ERRZEROPOINT_I                           BINARY_FLOAT,
 ERRZEROPOINT_Z                           BINARY_FLOAT,
 ZEROPOINTID_G                            NUMBER(10),
 ZEROPOINTID_R                            NUMBER(10),
 ZEROPOINTID_I                            NUMBER(10),
 ZEROPOINTID_Z                            NUMBER(10),
 R_OBJECT_NUMBER                          NUMBER(6),
 MAG_AUTO_G                               BINARY_FLOAT,
 MAGERR_AUTO_G                            BINARY_FLOAT,
 MAG_APER1_G                              BINARY_FLOAT,
 MAGERR_APER1_G                           BINARY_FLOAT,
 MAG_APER2_G                              BINARY_FLOAT,
 MAGERR_APER2_G                           BINARY_FLOAT,
 MAG_APER3_G                              BINARY_FLOAT,
 MAGERR_APER3_G                 	  BINARY_FLOAT,
 MAG_APER4_G                              BINARY_FLOAT,
 MAGERR_APER4_G                           BINARY_FLOAT,
 MAG_APER5_G                              BINARY_FLOAT,
 MAGERR_APER5_G                           BINARY_FLOAT,
 MAG_APER6_G          	 	 	  BINARY_FLOAT,
 MAGERR_APER6_G                           BINARY_FLOAT,
 MAG_AUTO_R                               BINARY_FLOAT,
 MAGERR_AUTO_R                            BINARY_FLOAT,
 MAG_APER1_R                              BINARY_FLOAT,
 MAGERR_APER1_R                           BINARY_FLOAT,
 MAG_APER2_R                              BINARY_FLOAT,
 MAGERR_APER2_R                           BINARY_FLOAT,
 MAG_APER3_R                              BINARY_FLOAT,
 MAGERR_APER3_R                           BINARY_FLOAT,
 MAG_APER4_R                              BINARY_FLOAT,
 MAGERR_APER4_R                           BINARY_FLOAT,
 MAG_APER5_R                              BINARY_FLOAT,
 MAGERR_APER5_R                           BINARY_FLOAT,
 MAG_APER6_R                              BINARY_FLOAT,
 MAGERR_APER6_R                           BINARY_FLOAT,
 MAG_AUTO_I                               BINARY_FLOAT,
 MAGERR_AUTO_I                            BINARY_FLOAT,
 MAG_APER1_I                              BINARY_FLOAT,
 MAGERR_APER1_I                           BINARY_FLOAT,
 MAG_APER2_I                              BINARY_FLOAT,
 MAGERR_APER2_I                           BINARY_FLOAT,
 MAG_APER3_I                              BINARY_FLOAT,
 MAGERR_APER3_I                           BINARY_FLOAT,
 MAG_APER4_I                              BINARY_FLOAT,
 MAGERR_APER4_I                           BINARY_FLOAT,
 MAG_APER5_I                              BINARY_FLOAT,
 MAGERR_APER5_I                           BINARY_FLOAT,
 MAG_APER6_I                              BINARY_FLOAT,
 MAGERR_APER6_I                           BINARY_FLOAT,
 MAG_AUTO_Z                               BINARY_FLOAT,
 MAGERR_AUTO_Z                            BINARY_FLOAT,
 MAG_APER1_Z                              BINARY_FLOAT,
 MAGERR_APER1_Z                           BINARY_FLOAT,
 MAG_APER2_Z                              BINARY_FLOAT,
 MAGERR_APER2_Z                           BINARY_FLOAT,
 MAG_APER3_Z                              BINARY_FLOAT,
 MAGERR_APER3_Z                           BINARY_FLOAT,
 MAG_APER4_Z                              BINARY_FLOAT,
 MAGERR_APER4_Z                           BINARY_FLOAT,
 MAG_APER5_Z                              BINARY_FLOAT,
 MAGERR_APER5_Z                           BINARY_FLOAT,
 MAG_APER6_Z                              BINARY_FLOAT,
 MAGERR_APER6_Z                           BINARY_FLOAT,
 ALPHA_J2000                              BINARY_DOUBLE,
 DELTA_J2000                              BINARY_DOUBLE,
 X2_WORLD                                 BINARY_FLOAT,
 Y2_WORLD                                 BINARY_FLOAT,
 XY_WORLD                                 BINARY_FLOAT,
 THRESHOLD                                BINARY_FLOAT,
 X_IMAGE                                  BINARY_FLOAT,
 Y_IMAGE                                  BINARY_FLOAT,
 THETA_IMAGE_G                            BINARY_FLOAT,
 ERRTHETA_IMAGE_G                         BINARY_FLOAT,
 ELLIPTICITY_G                            BINARY_FLOAT,
 CLASS_STAR_G                             BINARY_FLOAT,
 FLAGS_G                                  BINARY_FLOAT,
 THETA_IMAGE_R                            BINARY_FLOAT,
 ERRTHETA_IMAGE_R                         BINARY_FLOAT,
 ELLIPTICITY_R                            BINARY_FLOAT,
 CLASS_STAR_R                             BINARY_FLOAT,
 FLAGS_R                                  NUMBER(3),
 THETA_IMAGE_I                            BINARY_FLOAT,
 ERRTHETA_IMAGE_I                         BINARY_FLOAT,
 ELLIPTICITY_I                            BINARY_FLOAT,
 CLASS_STAR_I                             BINARY_FLOAT,
 FLAGS_I                                  NUMBER(3),
 THETA_IMAGE_Z                            BINARY_FLOAT,
 ERRTHETA_IMAGE_Z                         BINARY_FLOAT,
 ELLIPTICITY_Z                            BINARY_FLOAT,
 CLASS_STAR_Z                             BINARY_FLOAT,
 FLAGS_Z                                  NUMBER(3),
 ERRX2_WORLD                              BINARY_DOUBLE,
 ERRY2_WORLD                              BINARY_DOUBLE,
 catalogid_g                              number(9),
 catalogid_r                              number(9),
 catalogid_i                              number(9),
 catalogid_z                              number(9),
 xmin_image				  number(4),
 ymin_image				  number(4),
 xmax_image				  number(4),
 ymax_image				  number(4),
 ra					  number(8,5),
 dec                                      number(8,5),
 imageid_Y 				  number(9),
 zeropoint_Y 				  binary_float,
 errzeropoint_Y 			  binary_float,
 zeropointid_Y 				  number(10),
 mag_auto_Y 				  binary_float,
 magerr_auto_Y 				  binary_float,
 mag_aper1_Y 				  binary_float,
 magerr_aper1_Y 			  binary_float,
 mag_aper2_Y 				  binary_float,
 magerr_aper2_Y 			  binary_float,
 mag_aper3_Y 				  binary_float,
 magerr_aper3_Y 			  binary_float,
 mag_aper4_Y 				  binary_float,
 magerr_aper4_Y 			  binary_float,
 mag_aper5_Y 				  binary_float,
 magerr_aper5_Y 			  binary_float,
 mag_aper6_Y 				  binary_float,
 magerr_aper6_Y 			  binary_float,
 theta_image_Y 				  binary_float,
 errtheta_image_Y 			  binary_float,
 ellipticity_Y 				  binary_float,
 class_star_Y 				  binary_float,
 flags_Y 				  binary_float,
 catalogid_Y 				  number(9),
 fwhm_world_g 				  binary_float,
 fwhm_world_r 				  binary_float,
 fwhm_world_i 				  binary_float,
 fwhm_world_z 				  binary_float,
 fwhm_world_y 				  binary_float,
 flux_radius_g 				  binary_float,
 flux_radius_r 				  binary_float,
 flux_radius_i 				  binary_float,
 flux_radius_z 				  binary_float,
 flux_radius_y 				  binary_float,
 DR	                             NUMBER(10), 
 MAG_APER_7_G 		BINARY_FLOAT,
 MAGERR_APER_7_G 		BINARY_FLOAT,
 MAG_APER_8_G 		BINARY_FLOAT,
 MAGERR_APER_8_G 		BINARY_FLOAT,
 MAG_APER_9_G 		BINARY_FLOAT,
 MAGERR_APER_9_G 		BINARY_FLOAT,
 MAG_APER_10_G 		BINARY_FLOAT,
 MAGERR_APER_10_G 		BINARY_FLOAT,
 MAG_APER_11_G 		BINARY_FLOAT,
 MAGERR_APER_11_G 		BINARY_FLOAT,
 MAG_APER_12_G 		BINARY_FLOAT,
 MAGERR_APER_12_G 		BINARY_FLOAT,
 MAG_APER_13_G 		BINARY_FLOAT,
 MAGERR_APER_13_G 		BINARY_FLOAT,
 MAG_APER_14_G 		BINARY_FLOAT,
 MAGERR_APER_14_G 		BINARY_FLOAT,
 MAG_APER_15_G 		BINARY_FLOAT,
 MAGERR_APER_15_G 		BINARY_FLOAT,
 MAG_APER_16_G 		BINARY_FLOAT,
 MAGERR_APER_16_G 		BINARY_FLOAT,
 MAG_APER_17_G 		BINARY_FLOAT,
 MAGERR_APER_17_G 		BINARY_FLOAT,
 MAG_APER_7_R 		BINARY_FLOAT,
 MAGERR_APER_7_R 		BINARY_FLOAT,
 MAG_APER_8_R 		BINARY_FLOAT,
 MAGERR_APER_8_R 		BINARY_FLOAT,
 MAG_APER_9_R 		BINARY_FLOAT,
 MAGERR_APER_9_R 		BINARY_FLOAT,
 MAG_APER_10_R 		BINARY_FLOAT,
 MAGERR_APER_10_R 		BINARY_FLOAT,
 MAG_APER_11_R 		BINARY_FLOAT,
 MAGERR_APER_11_R 		BINARY_FLOAT,
 MAG_APER_12_R 		BINARY_FLOAT,
 MAGERR_APER_12_R 		BINARY_FLOAT,
 MAG_APER_13_R 		BINARY_FLOAT,
 MAGERR_APER_13_R 		BINARY_FLOAT,
 MAG_APER_14_R 		BINARY_FLOAT,
 MAGERR_APER_14_R 		BINARY_FLOAT,
 MAG_APER_15_R 		BINARY_FLOAT,
 MAGERR_APER_15_R 		BINARY_FLOAT,
 MAG_APER_16_R 		BINARY_FLOAT,
 MAGERR_APER_16_R 		BINARY_FLOAT,
 MAG_APER_17_R 		BINARY_FLOAT,
 MAGERR_APER_17_R 		BINARY_FLOAT,
 MAG_APER_7_I 		BINARY_FLOAT,
 MAGERR_APER_7_I 		BINARY_FLOAT,
 MAG_APER_8_I 		BINARY_FLOAT,
 MAGERR_APER_8_I 		BINARY_FLOAT,
 MAG_APER_9_I 		BINARY_FLOAT,
 MAGERR_APER_9_I 		BINARY_FLOAT,
 MAG_APER_10_I 		BINARY_FLOAT,
 MAGERR_APER_10_I 		BINARY_FLOAT,
 MAG_APER_11_I 		BINARY_FLOAT,
 MAGERR_APER_11_I 		BINARY_FLOAT,
 MAG_APER_12_I 		BINARY_FLOAT,
 MAGERR_APER_12_I 		BINARY_FLOAT,
 MAG_APER_13_I 		BINARY_FLOAT,
 MAGERR_APER_13_I 		BINARY_FLOAT,
 MAG_APER_14_I 		BINARY_FLOAT,
 MAGERR_APER_14_I 		BINARY_FLOAT,
 MAG_APER_15_I 		BINARY_FLOAT,
 MAGERR_APER_15_I 		BINARY_FLOAT,
 MAG_APER_16_I 		BINARY_FLOAT,
 MAGERR_APER_16_I 		BINARY_FLOAT,
 MAG_APER_17_I 		BINARY_FLOAT,
 MAGERR_APER_17_I 		BINARY_FLOAT,
 MAG_APER_7_Z 		BINARY_FLOAT,
 MAGERR_APER_7_Z 		BINARY_FLOAT,
 MAG_APER_8_Z 		BINARY_FLOAT,
 MAGERR_APER_8_Z 		BINARY_FLOAT,
 MAG_APER_9_Z 		BINARY_FLOAT,
 MAGERR_APER_9_Z 		BINARY_FLOAT,
 MAG_APER_10_Z 		BINARY_FLOAT,
 MAGERR_APER_10_Z 		BINARY_FLOAT,
 MAG_APER_11_Z 		BINARY_FLOAT,
 MAGERR_APER_11_Z 		BINARY_FLOAT,
 MAG_APER_12_Z 		BINARY_FLOAT,
 MAGERR_APER_12_Z 		BINARY_FLOAT,
 MAG_APER_13_Z 		BINARY_FLOAT,
 MAGERR_APER_13_Z 		BINARY_FLOAT,
 MAG_APER_14_Z 		BINARY_FLOAT,
 MAGERR_APER_14_Z 		BINARY_FLOAT,
 MAG_APER_15_Z 		BINARY_FLOAT,
 MAGERR_APER_15_Z 		BINARY_FLOAT,
 MAG_APER_16_Z 		BINARY_FLOAT,
 MAGERR_APER_16_Z 		BINARY_FLOAT,
 MAG_APER_17_Z 		BINARY_FLOAT,
 MAGERR_APER_17_Z 		BINARY_FLOAT,
 MAG_APER_7_Y 		BINARY_FLOAT,
 MAGERR_APER_7_Y 		BINARY_FLOAT,
 MAG_APER_8_Y 		BINARY_FLOAT,
 MAGERR_APER_8_Y 		BINARY_FLOAT,
 MAG_APER_9_Y 		BINARY_FLOAT,
 MAGERR_APER_9_Y 		BINARY_FLOAT,
 MAG_APER_10_Y 		BINARY_FLOAT,
 MAGERR_APER_10_Y 		BINARY_FLOAT,
 MAG_APER_11_Y 		BINARY_FLOAT,
 MAGERR_APER_11_Y 		BINARY_FLOAT,
 MAG_APER_12_Y 		BINARY_FLOAT,
 MAGERR_APER_12_Y 		BINARY_FLOAT,
 MAG_APER_13_Y 		BINARY_FLOAT,
 MAGERR_APER_13_Y 		BINARY_FLOAT,
 MAG_APER_14_Y 		BINARY_FLOAT,
 MAGERR_APER_14_Y 		BINARY_FLOAT,
 MAG_APER_15_Y 		BINARY_FLOAT,
 MAGERR_APER_15_Y 		BINARY_FLOAT,
 MAG_APER_16_Y 		BINARY_FLOAT,
 MAGERR_APER_16_Y 		BINARY_FLOAT,
 MAG_APER_17_Y 		BINARY_FLOAT,
 MAGERR_APER_17_Y 		BINARY_FLOAT,
 XPEAK_IMAGE BINARY_FLOAT,
 YPEAK_IMAGE BINARY_FLOAT,
 ALPHAPEAK_J2000	BINARY_DOUBLE,
 DELTAPEAK_J2000	BINARY_DOUBLE,
 THETA_IMAGE BINARY_FLOAT,
 THETA_J2000 BINARY_FLOAT,
 A_IMAGE BINARY_FLOAT,
 B_IMAGE BINARY_FLOAT,
 A_WORLD BINARY_FLOAT,
 B_WORLD BINARY_FLOAT,
 ELONGATION BINARY_FLOAT,
 KRON_RADIUS BINARY_FLOAT,
 PETRO_RADIUS BINARY_FLOAT,
 MU_THRESHOLD BINARY_FLOAT,
 ALPHAPSF_J2000_G BINARY_DOUBLE,
 ALPHAWIN_J2000_G BINARY_DOUBLE,
 AMODEL_IMAGE_G BINARY_FLOAT,
 AMODEL_WORLD_G BINARY_FLOAT,
 BMODEL_IMAGE_G BINARY_FLOAT,
 BMODEL_WORLD_G BINARY_FLOAT,
 CHI2_PSF_G BINARY_FLOAT,
 DELTAPSF_J2000_G BINARY_DOUBLE,
 DELTAWIN_J2000_G BINARY_DOUBLE,
 ELLIP1MODEL_IMAGE_G BINARY_FLOAT,
 ELLIP1MODEL_WORLD_G BINARY_FLOAT,
 ELLIP2MODEL_IMAGE_G BINARY_FLOAT,
 ELLIP2MODEL_WORLD_G BINARY_FLOAT,
 ERRAPSF_IMAGE_G BINARY_FLOAT,
 ERRAPSF_WORLD_G BINARY_FLOAT,
 ERRAWIN_IMAGE_G BINARY_FLOAT,
 ERRAWIN_WORLD_G	BINARY_FLOAT,
 ERRBPSF_IMAGE_G BINARY_FLOAT,
 ERRBPSF_WORLD_G BINARY_FLOAT,
 ERRBWIN_IMAGE_G BINARY_FLOAT,
 ERRBWIN_WORLD_G	BINARY_FLOAT,
 ERRTHETAPSF_IMAGE_G BINARY_FLOAT,
 ERRTHETAPSF_J2000_G BINARY_FLOAT,
 ERRTHETAWIN_IMAGE_G BINARY_FLOAT,
 ERRTHETAWIN_J2000_G BINARY_FLOAT,
 FLUX_APER_1_G BINARY_FLOAT,
 FLUX_APER_2_G BINARY_FLOAT,
 FLUX_APER_3_G BINARY_FLOAT,
 FLUX_APER_4_G BINARY_FLOAT,
 FLUX_APER_5_G BINARY_FLOAT,
 FLUX_APER_6_G BINARY_FLOAT,
 FLUX_APER_7_G BINARY_FLOAT,
 FLUX_APER_8_G BINARY_FLOAT,
 FLUX_APER_9_G BINARY_FLOAT,
 FLUX_APER_10_G BINARY_FLOAT,
 FLUX_APER_11_G BINARY_FLOAT,
 FLUX_APER_12_G BINARY_FLOAT,
 FLUX_APER_13_G BINARY_FLOAT,
 FLUX_APER_14_G BINARY_FLOAT,
 FLUX_APER_15_G BINARY_FLOAT,
 FLUX_APER_16_G BINARY_FLOAT,
 FLUX_APER_17_G BINARY_FLOAT,
 FLUX_AUTO_G BINARY_FLOAT,
 FLUX_EFF_MODEL_G BINARY_FLOAT,
 FLUXERR_APER_1_G BINARY_FLOAT,
 FLUXERR_APER_2_G BINARY_FLOAT,
 FLUXERR_APER_3_G BINARY_FLOAT,
 FLUXERR_APER_4_G BINARY_FLOAT,
 FLUXERR_APER_5_G BINARY_FLOAT,
 FLUXERR_APER_6_G BINARY_FLOAT,
 FLUXERR_APER_7_G BINARY_FLOAT,
 FLUXERR_APER_8_G BINARY_FLOAT,
 FLUXERR_APER_9_G BINARY_FLOAT,
 FLUXERR_APER_10_G BINARY_FLOAT,
 FLUXERR_APER_11_G BINARY_FLOAT,
 FLUXERR_APER_12_G BINARY_FLOAT,
 FLUXERR_APER_13_G BINARY_FLOAT,
 FLUXERR_APER_14_G BINARY_FLOAT,
 FLUXERR_APER_15_G BINARY_FLOAT,
 FLUXERR_APER_16_G BINARY_FLOAT,
 FLUXERR_APER_17_G BINARY_FLOAT,
 FLUXERR_AUTO_G BINARY_FLOAT,
 FLUXERR_ISO_G BINARY_FLOAT,
 FLUXERR_PETRO_G BINARY_FLOAT,
 FLUX_ISO_G BINARY_FLOAT,
 FLUX_MAX_G BINARY_FLOAT,
 FLUX_MAX_MODEL_G BINARY_FLOAT,
 FLUX_MEAN_MODEL_G BINARY_FLOAT,
 FLUX_PETRO_G BINARY_FLOAT,
 ISOAREAF_IMAGE_G BINARY_FLOAT,
 ISOAREAF_WORLD_G BINARY_FLOAT,
 ISOAREA_IMAGE_G BINARY_FLOAT,
 ISOAREA_WORLD_G	BINARY_FLOAT,
 MAGERR_ISO_G BINARY_FLOAT,
 MAGERR_PETRO_G BINARY_FLOAT,
 MAG_ISO_G BINARY_FLOAT,
 MAG_PETRO_G BINARY_FLOAT,
 MU_EFF_MODEL_G BINARY_FLOAT,
 MU_MAX_G BINARY_FLOAT,
 MU_MAX_MODEL_G BINARY_FLOAT,
 MU_MEAN_MODEL_G BINARY_FLOAT,
 NITER_PSF_G NUMBER(10),
 THETAMODEL_IMAGE_G BINARY_FLOAT,
 THETAMODEL_J2000_G BINARY_FLOAT,
 XPSF_IMAGE_G BINARY_DOUBLE,
 XWIN_IMAGE_G BINARY_DOUBLE,
 YPSF_IMAGE_G BINARY_DOUBLE,
 YWIN_IMAGE_G BINARY_DOUBLE,
 BACKGROUND_G BINARY_FLOAT,
 CONCENTRATION_MODEL_G BINARY_FLOAT,
 FLUX_MODEL_G BINARY_FLOAT,
 FLUXERR_MODEL_G	BINARY_FLOAT,
 MAG_MODEL_G BINARY_FLOAT,
 MAGERR_MODEL_G BINARY_FLOAT,
 FLUX_SPHEROID_G	BINARY_FLOAT,
 FLUXERR_SPHEROID_G BINARY_FLOAT,
 MAG_SPHEROID_G BINARY_FLOAT,
 MAGERR_SPHEROID_G BINARY_FLOAT,
 SPHEROID_REFF_IMAGE_G BINARY_FLOAT,
 SPHEROID_REFFERR_IMAGE_G BINARY_FLOAT,
 SPHEROID_REFF_WORLD_G BINARY_FLOAT,
 SPHEROID_REFFERR_WORLD_G BINARY_FLOAT,
 SPHEROID_ASPECT_IMAGE_G BINARY_FLOAT,
 SPHEROID_ASPECTERR_IMAGE_G BINARY_FLOAT,
 SPHEROID_ASPECT_WORLD_G BINARY_FLOAT,
 SPHEROID_ASPECTERR_WORLD_G BINARY_FLOAT,
 SPHEROID_THETA_IMAGE_G BINARY_FLOAT,
 SPHEROID_THETAERR_IMAGE_G BINARY_FLOAT,
 SPHEROID_THETA_J2000_G BINARY_FLOAT,
 SPHEROID_THETAERR_WORLD_G BINARY_FLOAT,
 DISK_SCALE_IMAGE_G BINARY_FLOAT,
 DISK_SCALEERR_IMAGE_G BINARY_FLOAT,
 DISK_SCALE_WORLD_G BINARY_FLOAT,
 DISK_SCALEERR_WORLD_G BINARY_FLOAT,
 DISK_ASPECT_IMAGE_G BINARY_FLOAT,
 DISK_ASPECTERR_IMAGE_G BINARY_FLOAT,
 DISK_ASPECT_WORLD_G BINARY_FLOAT,
 DISK_ASPECTERR_WORLD_G BINARY_FLOAT,
 DISK_THETA_IMAGE_G BINARY_FLOAT,
 DISK_THETAERR_IMAGE_G BINARY_FLOAT,
 DISK_THETA_J2000_G BINARY_FLOAT,
 DISK_THETAERR_WORLD_G BINARY_FLOAT,
 CHI2_MODEL_G BINARY_FLOAT,
 NITER_MODEL_G NUMBER(5),
 ALPHAPSF_J2000_R BINARY_DOUBLE,
 ALPHAWIN_J2000_R BINARY_DOUBLE,
 AMODEL_IMAGE_R BINARY_FLOAT,
 AMODEL_WORLD_R BINARY_FLOAT,
 BMODEL_IMAGE_R BINARY_FLOAT,
 BMODEL_WORLD_R BINARY_FLOAT,
 CHI2_PSF_R BINARY_FLOAT,
 DELTAPSF_J2000_R BINARY_DOUBLE,
 DELTAWIN_J2000_R BINARY_DOUBLE,
 ELLIP1MODEL_IMAGE_R BINARY_FLOAT,
 ELLIP1MODEL_WORLD_R BINARY_FLOAT,
 ELLIP2MODEL_IMAGE_R BINARY_FLOAT,
 ELLIP2MODEL_WORLD_R BINARY_FLOAT,
 ERRAPSF_IMAGE_R BINARY_FLOAT,
 ERRAPSF_WORLD_R BINARY_FLOAT,
 ERRAWIN_IMAGE_R BINARY_FLOAT,
 ERRAWIN_WORLD_R	BINARY_FLOAT,
 ERRBPSF_IMAGE_R BINARY_FLOAT,
 ERRBPSF_WORLD_R BINARY_FLOAT,
 ERRBWIN_IMAGE_R BINARY_FLOAT,
 ERRBWIN_WORLD_R	BINARY_FLOAT,
 ERRTHETAPSF_IMAGE_R BINARY_FLOAT,
 ERRTHETAPSF_J2000_R BINARY_FLOAT,
 ERRTHETAWIN_IMAGE_R BINARY_FLOAT,
 ERRTHETAWIN_J2000_R BINARY_FLOAT,
 FLUX_APER_1_R BINARY_FLOAT,
 FLUX_APER_2_R BINARY_FLOAT,
 FLUX_APER_3_R BINARY_FLOAT,
 FLUX_APER_4_R BINARY_FLOAT,
 FLUX_APER_5_R BINARY_FLOAT,
 FLUX_APER_6_R BINARY_FLOAT,
 FLUX_APER_7_R BINARY_FLOAT,
 FLUX_APER_8_R BINARY_FLOAT,
 FLUX_APER_9_R BINARY_FLOAT,
 FLUX_APER_10_R BINARY_FLOAT,
 FLUX_APER_11_R BINARY_FLOAT,
 FLUX_APER_12_R BINARY_FLOAT,
 FLUX_APER_13_R BINARY_FLOAT,
 FLUX_APER_14_R BINARY_FLOAT,
 FLUX_APER_15_R BINARY_FLOAT,
 FLUX_APER_16_R BINARY_FLOAT,
 FLUX_APER_17_R BINARY_FLOAT,
 FLUX_AUTO_R BINARY_FLOAT,
 FLUX_EFF_MODEL_R BINARY_FLOAT,
 FLUXERR_APER_1_R BINARY_FLOAT,
 FLUXERR_APER_2_R BINARY_FLOAT,
 FLUXERR_APER_3_R BINARY_FLOAT,
 FLUXERR_APER_4_R BINARY_FLOAT,
 FLUXERR_APER_5_R BINARY_FLOAT,
 FLUXERR_APER_6_R BINARY_FLOAT,
 FLUXERR_APER_7_R BINARY_FLOAT,
 FLUXERR_APER_8_R BINARY_FLOAT,
 FLUXERR_APER_9_R BINARY_FLOAT,
 FLUXERR_APER_10_R BINARY_FLOAT,
 FLUXERR_APER_11_R BINARY_FLOAT,
 FLUXERR_APER_12_R BINARY_FLOAT,
 FLUXERR_APER_13_R BINARY_FLOAT,
 FLUXERR_APER_14_R BINARY_FLOAT,
 FLUXERR_APER_15_R BINARY_FLOAT,
 FLUXERR_APER_16_R BINARY_FLOAT,
 FLUXERR_APER_17_R BINARY_FLOAT,
 FLUXERR_AUTO_R BINARY_FLOAT,
 FLUXERR_ISO_R BINARY_FLOAT,
 FLUXERR_PETRO_R BINARY_FLOAT,
 FLUX_ISO_R BINARY_FLOAT,
 FLUX_MAX_R BINARY_FLOAT,
 FLUX_MAX_MODEL_R BINARY_FLOAT,
 FLUX_MEAN_MODEL_R BINARY_FLOAT,
 FLUX_PETRO_R BINARY_FLOAT,
 ISOAREAF_IMAGE_R BINARY_FLOAT,
 ISOAREAF_WORLD_R BINARY_FLOAT,
 ISOAREA_IMAGE_R BINARY_FLOAT,
 ISOAREA_WORLD_R	BINARY_FLOAT,
 MAGERR_ISO_R BINARY_FLOAT,
 MAGERR_PETRO_R BINARY_FLOAT,
 MAG_ISO_R BINARY_FLOAT,
 MAG_PETRO_R BINARY_FLOAT,
 MU_EFF_MODEL_R BINARY_FLOAT,
 MU_MAX_R BINARY_FLOAT,
 MU_MAX_MODEL_R BINARY_FLOAT,
 MU_MEAN_MODEL_R BINARY_FLOAT,
 NITER_PSF_R NUMBER(10),
 THETAMODEL_IMAGE_R BINARY_FLOAT,
 THETAMODEL_J2000_R BINARY_FLOAT,
 XPSF_IMAGE_R BINARY_DOUBLE,
 XWIN_IMAGE_R BINARY_DOUBLE,
 YPSF_IMAGE_R BINARY_DOUBLE,
 YWIN_IMAGE_R BINARY_DOUBLE,
 BACKGROUND_R BINARY_FLOAT,
 CONCENTRATION_MODEL_R BINARY_FLOAT,
 FLUX_MODEL_R BINARY_FLOAT,
 FLUXERR_MODEL_R	BINARY_FLOAT,
 MAG_MODEL_R BINARY_FLOAT,
 MAGERR_MODEL_R BINARY_FLOAT,
 FLUX_SPHEROID_R	BINARY_FLOAT,
 FLUXERR_SPHEROID_R BINARY_FLOAT,
 MAG_SPHEROID_R BINARY_FLOAT,
 MAGERR_SPHEROID_R BINARY_FLOAT,
 SPHEROID_REFF_IMAGE_R BINARY_FLOAT,
 SPHEROID_REFFERR_IMAGE_R BINARY_FLOAT,
 SPHEROID_REFF_WORLD_R BINARY_FLOAT,
 SPHEROID_REFFERR_WORLD_R BINARY_FLOAT,
 SPHEROID_ASPECT_IMAGE_R BINARY_FLOAT,
 SPHEROID_ASPECTERR_IMAGE_R BINARY_FLOAT,
 SPHEROID_ASPECT_WORLD_R BINARY_FLOAT,
 SPHEROID_ASPECTERR_WORLD_R BINARY_FLOAT,
 SPHEROID_THETA_IMAGE_R BINARY_FLOAT,
 SPHEROID_THETAERR_IMAGE_R BINARY_FLOAT,
 SPHEROID_THETA_J2000_R BINARY_FLOAT,
 SPHEROID_THETAERR_WORLD_R BINARY_FLOAT,
 DISK_SCALE_IMAGE_R BINARY_FLOAT,
 DISK_SCALEERR_IMAGE_R BINARY_FLOAT,
 DISK_SCALE_WORLD_R BINARY_FLOAT,
 DISK_SCALEERR_WORLD_R BINARY_FLOAT,
 DISK_ASPECT_IMAGE_R BINARY_FLOAT,
 DISK_ASPECTERR_IMAGE_R BINARY_FLOAT,
 DISK_ASPECT_WORLD_R BINARY_FLOAT,
 DISK_ASPECTERR_WORLD_R BINARY_FLOAT,
 DISK_THETA_IMAGE_R BINARY_FLOAT,
 DISK_THETAERR_IMAGE_R BINARY_FLOAT,
 DISK_THETA_J2000_R BINARY_FLOAT,
 DISK_THETAERR_WORLD_R BINARY_FLOAT,
 CHI2_MODEL_R BINARY_FLOAT,
 NITER_MODEL_R NUMBER(5),
 ALPHAPSF_J2000_I BINARY_DOUBLE,
 ALPHAWIN_J2000_I BINARY_DOUBLE,
 AMODEL_IMAGE_I BINARY_FLOAT,
 AMODEL_WORLD_I BINARY_FLOAT,
 BMODEL_IMAGE_I BINARY_FLOAT,
 BMODEL_WORLD_I BINARY_FLOAT,
 CHI2_PSF_I BINARY_FLOAT,
 DELTAPSF_J2000_I BINARY_DOUBLE,
 DELTAWIN_J2000_I BINARY_DOUBLE,
 ELLIP1MODEL_IMAGE_I BINARY_FLOAT,
 ELLIP1MODEL_WORLD_I BINARY_FLOAT,
 ELLIP2MODEL_IMAGE_I BINARY_FLOAT,
 ELLIP2MODEL_WORLD_I BINARY_FLOAT,
 ERRAPSF_IMAGE_I BINARY_FLOAT,
 ERRAPSF_WORLD_I BINARY_FLOAT,
 ERRAWIN_IMAGE_I BINARY_FLOAT,
 ERRAWIN_WORLD_I	BINARY_FLOAT,
 ERRBPSF_IMAGE_I BINARY_FLOAT,
 ERRBPSF_WORLD_I BINARY_FLOAT,
 ERRBWIN_IMAGE_I BINARY_FLOAT,
 ERRBWIN_WORLD_I	BINARY_FLOAT,
 ERRTHETAPSF_IMAGE_I BINARY_FLOAT,
 ERRTHETAPSF_J2000_I BINARY_FLOAT,
 ERRTHETAWIN_IMAGE_I BINARY_FLOAT,
 ERRTHETAWIN_J2000_I BINARY_FLOAT,
 FLUX_APER_1_I BINARY_FLOAT,
 FLUX_APER_2_I BINARY_FLOAT,
 FLUX_APER_3_I BINARY_FLOAT,
 FLUX_APER_4_I BINARY_FLOAT,
 FLUX_APER_5_I BINARY_FLOAT,
 FLUX_APER_6_I BINARY_FLOAT,
 FLUX_APER_7_I BINARY_FLOAT,
 FLUX_APER_8_I BINARY_FLOAT,
 FLUX_APER_9_I BINARY_FLOAT,
 FLUX_APER_10_I BINARY_FLOAT,
 FLUX_APER_11_I BINARY_FLOAT,
 FLUX_APER_12_I BINARY_FLOAT,
 FLUX_APER_13_I BINARY_FLOAT,
 FLUX_APER_14_I BINARY_FLOAT,
 FLUX_APER_15_I BINARY_FLOAT,
 FLUX_APER_16_I BINARY_FLOAT,
 FLUX_APER_17_I BINARY_FLOAT,
 FLUX_AUTO_I BINARY_FLOAT,
 FLUX_EFF_MODEL_I BINARY_FLOAT,
 FLUXERR_APER_1_I BINARY_FLOAT,
 FLUXERR_APER_2_I BINARY_FLOAT,
 FLUXERR_APER_3_I BINARY_FLOAT,
 FLUXERR_APER_4_I BINARY_FLOAT,
 FLUXERR_APER_5_I BINARY_FLOAT,
 FLUXERR_APER_6_I BINARY_FLOAT,
 FLUXERR_APER_7_I BINARY_FLOAT,
 FLUXERR_APER_8_I BINARY_FLOAT,
 FLUXERR_APER_9_I BINARY_FLOAT,
 FLUXERR_APER_10_I BINARY_FLOAT,
 FLUXERR_APER_11_I BINARY_FLOAT,
 FLUXERR_APER_12_I BINARY_FLOAT,
 FLUXERR_APER_13_I BINARY_FLOAT,
 FLUXERR_APER_14_I BINARY_FLOAT,
 FLUXERR_APER_15_I BINARY_FLOAT,
 FLUXERR_APER_16_I BINARY_FLOAT,
 FLUXERR_APER_17_I BINARY_FLOAT,
 FLUXERR_AUTO_I BINARY_FLOAT,
 FLUXERR_ISO_I BINARY_FLOAT,
 FLUXERR_PETRO_I BINARY_FLOAT,
 FLUX_ISO_I BINARY_FLOAT,
 FLUX_MAX_I BINARY_FLOAT,
 FLUX_MAX_MODEL_I BINARY_FLOAT,
 FLUX_MEAN_MODEL_I BINARY_FLOAT,
 FLUX_PETRO_I BINARY_FLOAT,
 ISOAREAF_IMAGE_I BINARY_FLOAT,
 ISOAREAF_WORLD_I BINARY_FLOAT,
 ISOAREA_IMAGE_I BINARY_FLOAT,
 ISOAREA_WORLD_I	BINARY_FLOAT,
 MAGERR_ISO_I BINARY_FLOAT,
 MAGERR_PETRO_I BINARY_FLOAT,
 MAG_ISO_I BINARY_FLOAT,
 MAG_PETRO_I BINARY_FLOAT,
 MU_EFF_MODEL_I BINARY_FLOAT,
 MU_MAX_I BINARY_FLOAT,
 MU_MAX_MODEL_I BINARY_FLOAT,
 MU_MEAN_MODEL_I BINARY_FLOAT,
 NITER_PSF_I NUMBER(10),
 THETAMODEL_IMAGE_I BINARY_FLOAT,
 THETAMODEL_J2000_I BINARY_FLOAT,
 XPSF_IMAGE_I BINARY_DOUBLE,
 XWIN_IMAGE_I BINARY_DOUBLE,
 YPSF_IMAGE_I BINARY_DOUBLE,
 YWIN_IMAGE_I BINARY_DOUBLE,
 BACKGROUND_I BINARY_FLOAT,
 CONCENTRATION_MODEL_I BINARY_FLOAT,
 FLUX_MODEL_I BINARY_FLOAT,
 FLUXERR_MODEL_I BINARY_FLOAT,
 MAG_MODEL_I BINARY_FLOAT,
 MAGERR_MODEL_I BINARY_FLOAT,
 FLUX_SPHEROID_I BINARY_FLOAT,
 FLUXERR_SPHEROID_I BINARY_FLOAT,
 MAG_SPHEROID_I BINARY_FLOAT,
 MAGERR_SPHEROID_I BINARY_FLOAT,
 SPHEROID_REFF_IMAGE_I BINARY_FLOAT,
 SPHEROID_REFFERR_IMAGE_I BINARY_FLOAT,
 SPHEROID_REFF_WORLD_I BINARY_FLOAT,
 SPHEROID_REFFERR_WORLD_I BINARY_FLOAT,
 SPHEROID_ASPECT_IMAGE_I BINARY_FLOAT,
 SPHEROID_ASPECTERR_IMAGE_I BINARY_FLOAT,
 SPHEROID_ASPECT_WORLD_I BINARY_FLOAT,
 SPHEROID_ASPECTERR_WORLD_I BINARY_FLOAT,
 SPHEROID_THETA_IMAGE_I BINARY_FLOAT,
 SPHEROID_THETAERR_IMAGE_I BINARY_FLOAT,
 SPHEROID_THETA_J2000_I BINARY_FLOAT,
 SPHEROID_THETAERR_WORLD_I BINARY_FLOAT,
 DISK_SCALE_IMAGE_I BINARY_FLOAT,
 DISK_SCALEERR_IMAGE_I BINARY_FLOAT,
 DISK_SCALE_WORLD_I BINARY_FLOAT,
 DISK_SCALEERR_WORLD_I BINARY_FLOAT,
 DISK_ASPECT_IMAGE_I BINARY_FLOAT,
 DISK_ASPECTERR_IMAGE_I BINARY_FLOAT,
 DISK_ASPECT_WORLD_I BINARY_FLOAT,
 DISK_ASPECTERR_WORLD_I BINARY_FLOAT,
 DISK_THETA_IMAGE_I BINARY_FLOAT,
 DISK_THETAERR_IMAGE_I BINARY_FLOAT,
 DISK_THETA_J2000_I BINARY_FLOAT,
 DISK_THETAERR_WORLD_I BINARY_FLOAT,
 CHI2_MODEL_I BINARY_FLOAT,
 NITER_MODEL_I NUMBER(5),
 ALPHAPSF_J2000_Z BINARY_DOUBLE,
 ALPHAWIN_J2000_Z BINARY_DOUBLE,
 AMODEL_IMAGE_Z BINARY_FLOAT,
 AMODEL_WORLD_Z BINARY_FLOAT,
 BMODEL_IMAGE_Z BINARY_FLOAT,
 BMODEL_WORLD_Z BINARY_FLOAT,
 CHI2_PSF_Z BINARY_FLOAT,
 DELTAPSF_J2000_Z BINARY_DOUBLE,
 DELTAWIN_J2000_Z BINARY_DOUBLE,
 ELLIP1MODEL_IMAGE_Z BINARY_FLOAT,
 ELLIP1MODEL_WORLD_Z BINARY_FLOAT,
 ELLIP2MODEL_IMAGE_Z BINARY_FLOAT,
 ELLIP2MODEL_WORLD_Z BINARY_FLOAT,
 ERRAPSF_IMAGE_Z BINARY_FLOAT,
 ERRAPSF_WORLD_Z BINARY_FLOAT,
 ERRAWIN_IMAGE_Z BINARY_FLOAT,
 ERRAWIN_WORLD_Z	BINARY_FLOAT,
 ERRBPSF_IMAGE_Z BINARY_FLOAT,
 ERRBPSF_WORLD_Z BINARY_FLOAT,
 ERRBWIN_IMAGE_Z BINARY_FLOAT,
 ERRBWIN_WORLD_Z	BINARY_FLOAT,
 ERRTHETAPSF_IMAGE_Z BINARY_FLOAT,
 ERRTHETAPSF_J2000_Z BINARY_FLOAT,
 ERRTHETAWIN_IMAGE_Z BINARY_FLOAT,
 ERRTHETAWIN_J2000_Z BINARY_FLOAT,
 FLUX_APER_1_Z BINARY_FLOAT,
 FLUX_APER_2_Z BINARY_FLOAT,
 FLUX_APER_3_Z BINARY_FLOAT,
 FLUX_APER_4_Z BINARY_FLOAT,
 FLUX_APER_5_Z BINARY_FLOAT,
 FLUX_APER_6_Z BINARY_FLOAT,
 FLUX_APER_7_Z BINARY_FLOAT,
 FLUX_APER_8_Z BINARY_FLOAT,
 FLUX_APER_9_Z BINARY_FLOAT,
 FLUX_APER_10_Z BINARY_FLOAT,
 FLUX_APER_11_Z BINARY_FLOAT,
 FLUX_APER_12_Z BINARY_FLOAT,
 FLUX_APER_13_Z BINARY_FLOAT,
 FLUX_APER_14_Z BINARY_FLOAT,
 FLUX_APER_15_Z BINARY_FLOAT,
 FLUX_APER_16_Z BINARY_FLOAT,
 FLUX_APER_17_Z BINARY_FLOAT,
 FLUX_AUTO_Z BINARY_FLOAT,
 FLUX_EFF_MODEL_Z BINARY_FLOAT,
 FLUXERR_APER_1_Z BINARY_FLOAT,
 FLUXERR_APER_2_Z BINARY_FLOAT,
 FLUXERR_APER_3_Z BINARY_FLOAT,
 FLUXERR_APER_4_Z BINARY_FLOAT,
 FLUXERR_APER_5_Z BINARY_FLOAT,
 FLUXERR_APER_6_Z BINARY_FLOAT,
 FLUXERR_APER_7_Z BINARY_FLOAT,
 FLUXERR_APER_8_Z BINARY_FLOAT,
 FLUXERR_APER_9_Z BINARY_FLOAT,
 FLUXERR_APER_10_Z BINARY_FLOAT,
 FLUXERR_APER_11_Z BINARY_FLOAT,
 FLUXERR_APER_12_Z BINARY_FLOAT,
 FLUXERR_APER_13_Z BINARY_FLOAT,
 FLUXERR_APER_14_Z BINARY_FLOAT,
 FLUXERR_APER_15_Z BINARY_FLOAT,
 FLUXERR_APER_16_Z BINARY_FLOAT,
 FLUXERR_APER_17_Z BINARY_FLOAT,
 FLUXERR_AUTO_Z BINARY_FLOAT,
 FLUXERR_ISO_Z BINARY_FLOAT,
 FLUXERR_PETRO_Z BINARY_FLOAT,
 FLUX_ISO_Z BINARY_FLOAT,
 FLUX_MAX_Z BINARY_FLOAT,
 FLUX_MAX_MODEL_Z BINARY_FLOAT,
 FLUX_MEAN_MODEL_Z BINARY_FLOAT,
 FLUX_PETRO_Z BINARY_FLOAT,
 ISOAREAF_IMAGE_Z BINARY_FLOAT,
 ISOAREAF_WORLD_Z BINARY_FLOAT,
 ISOAREA_IMAGE_Z BINARY_FLOAT,
 ISOAREA_WORLD_Z	BINARY_FLOAT,
 MAGERR_ISO_Z BINARY_FLOAT,
 MAGERR_PETRO_Z BINARY_FLOAT,
 MAG_ISO_Z BINARY_FLOAT,
 MAG_PETRO_Z BINARY_FLOAT,
 MU_EFF_MODEL_Z BINARY_FLOAT,
 MU_MAX_Z BINARY_FLOAT,
 MU_MAX_MODEL_Z BINARY_FLOAT,
 MU_MEAN_MODEL_Z BINARY_FLOAT,
 NITER_PSF_Z NUMBER(10),
 THETAMODEL_IMAGE_Z BINARY_FLOAT,
 THETAMODEL_J2000_Z BINARY_FLOAT,
 XPSF_IMAGE_Z BINARY_DOUBLE,
 XWIN_IMAGE_Z BINARY_DOUBLE,
 YPSF_IMAGE_Z BINARY_DOUBLE,
 YWIN_IMAGE_Z BINARY_DOUBLE,
 BACKGROUND_Z BINARY_FLOAT,
 CONCENTRATION_MODEL_Z BINARY_FLOAT,
 FLUX_MODEL_Z BINARY_FLOAT,
 FLUXERR_MODEL_Z	BINARY_FLOAT,
 MAG_MODEL_Z BINARY_FLOAT,
 MAGERR_MODEL_Z BINARY_FLOAT,
 FLUX_SPHEROID_Z	BINARY_FLOAT,
 FLUXERR_SPHEROID_Z BINARY_FLOAT,
 MAG_SPHEROID_Z BINARY_FLOAT,
 MAGERR_SPHEROID_Z BINARY_FLOAT,
 SPHEROID_REFF_IMAGE_Z BINARY_FLOAT,
 SPHEROID_REFFERR_IMAGE_Z BINARY_FLOAT,
 SPHEROID_REFF_WORLD_Z BINARY_FLOAT,
 SPHEROID_REFFERR_WORLD_Z BINARY_FLOAT,
 SPHEROID_ASPECT_IMAGE_Z BINARY_FLOAT,
 SPHEROID_ASPECTERR_IMAGE_Z BINARY_FLOAT,
 SPHEROID_ASPECT_WORLD_Z BINARY_FLOAT,
 SPHEROID_ASPECTERR_WORLD_Z BINARY_FLOAT,
 SPHEROID_THETA_IMAGE_Z BINARY_FLOAT,
 SPHEROID_THETAERR_IMAGE_Z BINARY_FLOAT,
 SPHEROID_THETA_J2000_Z BINARY_FLOAT,
 SPHEROID_THETAERR_WORLD_Z BINARY_FLOAT,
 DISK_SCALE_IMAGE_Z BINARY_FLOAT,
 DISK_SCALEERR_IMAGE_Z BINARY_FLOAT,
 DISK_SCALE_WORLD_Z BINARY_FLOAT,
 DISK_SCALEERR_WORLD_Z BINARY_FLOAT,
 DISK_ASPECT_IMAGE_Z BINARY_FLOAT,
 DISK_ASPECTERR_IMAGE_Z BINARY_FLOAT,
 DISK_ASPECT_WORLD_Z BINARY_FLOAT,
 DISK_ASPECTERR_WORLD_Z BINARY_FLOAT,
 DISK_THETA_IMAGE_Z BINARY_FLOAT,
 DISK_THETAERR_IMAGE_Z BINARY_FLOAT,
 DISK_THETA_J2000_Z BINARY_FLOAT,
 DISK_THETAERR_WORLD_Z BINARY_FLOAT,
 CHI2_MODEL_Z BINARY_FLOAT,
 NITER_MODEL_Z NUMBER(5),
 ALPHAPSF_J2000_Y BINARY_DOUBLE,
 ALPHAWIN_J2000_Y BINARY_DOUBLE,
 AMODEL_IMAGE_Y BINARY_FLOAT,
 AMODEL_WORLD_Y BINARY_FLOAT,
 BMODEL_IMAGE_Y BINARY_FLOAT,
 BMODEL_WORLD_Y BINARY_FLOAT,
 CHI2_PSF_Y BINARY_FLOAT,
 DELTAPSF_J2000_Y BINARY_DOUBLE,
 DELTAWIN_J2000_Y BINARY_DOUBLE,
 ELLIP1MODEL_IMAGE_Y BINARY_FLOAT,
 ELLIP1MODEL_WORLD_Y BINARY_FLOAT,
 ELLIP2MODEL_IMAGE_Y BINARY_FLOAT,
 ELLIP2MODEL_WORLD_Y BINARY_FLOAT,
 ERRAPSF_IMAGE_Y BINARY_FLOAT,
 ERRAPSF_WORLD_Y BINARY_FLOAT,
 ERRAWIN_IMAGE_Y BINARY_FLOAT,
 ERRAWIN_WORLD_Y	BINARY_FLOAT,
 ERRBPSF_IMAGE_Y BINARY_FLOAT,
 ERRBPSF_WORLD_Y BINARY_FLOAT,
 ERRBWIN_IMAGE_Y BINARY_FLOAT,
 ERRBWIN_WORLD_Y	BINARY_FLOAT,
 ERRTHETAPSF_IMAGE_Y BINARY_FLOAT,
 ERRTHETAPSF_J2000_Y BINARY_FLOAT,
 ERRTHETAWIN_IMAGE_Y BINARY_FLOAT,
 ERRTHETAWIN_J2000_Y BINARY_FLOAT,
 FLUX_APER_1_Y BINARY_FLOAT,
 FLUX_APER_2_Y BINARY_FLOAT,
 FLUX_APER_3_Y BINARY_FLOAT,
 FLUX_APER_4_Y BINARY_FLOAT,
 FLUX_APER_5_Y BINARY_FLOAT,
 FLUX_APER_6_Y BINARY_FLOAT,
 FLUX_APER_7_Y BINARY_FLOAT,
 FLUX_APER_8_Y BINARY_FLOAT,
 FLUX_APER_9_Y BINARY_FLOAT,
 FLUX_APER_10_Y BINARY_FLOAT,
 FLUX_APER_11_Y BINARY_FLOAT,
 FLUX_APER_12_Y BINARY_FLOAT,
 FLUX_APER_13_Y BINARY_FLOAT,
 FLUX_APER_14_Y BINARY_FLOAT,
 FLUX_APER_15_Y BINARY_FLOAT,
 FLUX_APER_16_Y BINARY_FLOAT,
 FLUX_APER_17_Y BINARY_FLOAT,
 FLUX_AUTO_Y BINARY_FLOAT,
 FLUX_EFF_MODEL_Y BINARY_FLOAT,
 FLUXERR_APER_1_Y BINARY_FLOAT,
 FLUXERR_APER_2_Y BINARY_FLOAT,
 FLUXERR_APER_3_Y BINARY_FLOAT,
 FLUXERR_APER_4_Y BINARY_FLOAT,
 FLUXERR_APER_5_Y BINARY_FLOAT,
 FLUXERR_APER_6_Y BINARY_FLOAT,
 FLUXERR_APER_7_Y BINARY_FLOAT,
 FLUXERR_APER_8_Y BINARY_FLOAT,
 FLUXERR_APER_9_Y BINARY_FLOAT,
 FLUXERR_APER_10_Y BINARY_FLOAT,
 FLUXERR_APER_11_Y BINARY_FLOAT,
 FLUXERR_APER_12_Y BINARY_FLOAT,
 FLUXERR_APER_13_Y BINARY_FLOAT,
 FLUXERR_APER_14_Y BINARY_FLOAT,
 FLUXERR_APER_15_Y BINARY_FLOAT,
 FLUXERR_APER_16_Y BINARY_FLOAT,
 FLUXERR_APER_17_Y BINARY_FLOAT,
 FLUXERR_AUTO_Y BINARY_FLOAT,
 FLUXERR_ISO_Y BINARY_FLOAT,
 FLUXERR_PETRO_Y BINARY_FLOAT,
 FLUX_ISO_Y BINARY_FLOAT,
 FLUX_MAX_Y BINARY_FLOAT,
 FLUX_MAX_MODEL_Y BINARY_FLOAT,
 FLUX_MEAN_MODEL_Y BINARY_FLOAT,
 FLUX_PETRO_Y BINARY_FLOAT,
 ISOAREAF_IMAGE_Y BINARY_FLOAT,
 ISOAREAF_WORLD_Y BINARY_FLOAT,
 ISOAREA_IMAGE_Y BINARY_FLOAT,
 ISOAREA_WORLD_Y	BINARY_FLOAT,
 MAGERR_ISO_Y BINARY_FLOAT,
 MAGERR_PETRO_Y BINARY_FLOAT,
 MAG_ISO_Y BINARY_FLOAT,
 MAG_PETRO_Y BINARY_FLOAT,
 MU_EFF_MODEL_Y BINARY_FLOAT,
 MU_MAX_Y BINARY_FLOAT,
 MU_MAX_MODEL_Y BINARY_FLOAT,
 MU_MEAN_MODEL_Y BINARY_FLOAT,
 NITER_PSF_Y NUMBER(10),
 THETAMODEL_IMAGE_Y BINARY_FLOAT,
 THETAMODEL_J2000_Y BINARY_FLOAT,
 XPSF_IMAGE_Y BINARY_DOUBLE,
 XWIN_IMAGE_Y BINARY_DOUBLE,
 YPSF_IMAGE_Y BINARY_DOUBLE,
 YWIN_IMAGE_Y BINARY_DOUBLE,
 BACKGROUND_Y BINARY_FLOAT,
 CONCENTRATION_MODEL_Y BINARY_FLOAT,
 FLUX_MODEL_Y BINARY_FLOAT,
 FLUXERR_MODEL_Y	BINARY_FLOAT,
 MAG_MODEL_Y BINARY_FLOAT,
 MAGERR_MODEL_Y BINARY_FLOAT,
 FLUX_SPHEROID_Y	BINARY_FLOAT,
 FLUXERR_SPHEROID_Y BINARY_FLOAT,
 MAG_SPHEROID_Y BINARY_FLOAT,
 MAGERR_SPHEROID_Y BINARY_FLOAT,
 SPHEROID_REFF_IMAGE_Y BINARY_FLOAT,
 SPHEROID_REFFERR_IMAGE_Y BINARY_FLOAT,
 SPHEROID_REFF_WORLD_Y BINARY_FLOAT,
 SPHEROID_REFFERR_WORLD_Y BINARY_FLOAT,
 SPHEROID_ASPECT_IMAGE_Y BINARY_FLOAT,
 SPHEROID_ASPECTERR_IMAGE_Y BINARY_FLOAT,
 SPHEROID_ASPECT_WORLD_Y BINARY_FLOAT,
 SPHEROID_ASPECTERR_WORLD_Y BINARY_FLOAT,
 SPHEROID_THETA_IMAGE_Y BINARY_FLOAT,
 SPHEROID_THETAERR_IMAGE_Y BINARY_FLOAT,
 SPHEROID_THETA_J2000_Y BINARY_FLOAT,
 SPHEROID_THETAERR_WORLD_Y BINARY_FLOAT,
 DISK_SCALE_IMAGE_Y BINARY_FLOAT,
 DISK_SCALEERR_IMAGE_Y BINARY_FLOAT,
 DISK_SCALE_WORLD_Y BINARY_FLOAT,
 DISK_SCALEERR_WORLD_Y BINARY_FLOAT,
 DISK_ASPECT_IMAGE_Y BINARY_FLOAT,
 DISK_ASPECTERR_IMAGE_Y BINARY_FLOAT,
 DISK_ASPECT_WORLD_Y BINARY_FLOAT,
 DISK_ASPECTERR_WORLD_Y BINARY_FLOAT,
 DISK_THETA_IMAGE_Y BINARY_FLOAT,
 DISK_THETAERR_IMAGE_Y BINARY_FLOAT,
 DISK_THETA_J2000_Y BINARY_FLOAT,
 DISK_THETAERR_WORLD_Y BINARY_FLOAT,
 CHI2_MODEL_Y BINARY_FLOAT,
 NITER_MODEL_Y NUMBER(5),
 DURATION_ANALYSIS_G BINARY_FLOAT,
 DURATION_ANALYSIS_R BINARY_FLOAT,
 DURATION_ANALYSIS_I BINARY_FLOAT,
 DURATION_ANALYSIS_Z BINARY_FLOAT,
 DURATION_ANALYSIS_Y BINARY_FLOAT,
 FLAGS_WEIGHT_G VARCHAR2(1),
 FLAGS_WEIGHT_R VARCHAR2(1),
 FLAGS_WEIGHT_I VARCHAR2(1),
 FLAGS_WEIGHT_Z VARCHAR2(1),
 FLAGS_WEIGHT_Y VARCHAR2(1),
 NLOWWEIGHT_ISO_G NUMBER,
 NLOWWEIGHT_ISO_R NUMBER,
 NLOWWEIGHT_ISO_I NUMBER,
 NLOWWEIGHT_ISO_Z NUMBER,
 NLOWWEIGHT_ISO_Y NUMBER,
 NLOWDWEIGHT_ISO NUMBER,
 SPREAD_MODEL_G BINARY_FLOAT,
 SPREAD_MODEL_R BINARY_FLOAT,
 SPREAD_MODEL_I BINARY_FLOAT,
 SPREAD_MODEL_Z BINARY_FLOAT,
 SPREAD_MODEL_Y BINARY_FLOAT,
 SPREADERR_MODEL_G BINARY_FLOAT,
 SPREADERR_MODEL_R BINARY_FLOAT,
 SPREADERR_MODEL_I BINARY_FLOAT,
 SPREADERR_MODEL_Z BINARY_FLOAT,
 SPREADERR_MODEL_Y BINARY_FLOAT,
 CONSTRAINT PK_COADD_OBJECTS PRIMARY KEY (COADD_OBJECTS_ID))
storage (initial 2M);

-- 
-- TABLE: SNSpect 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNSpect (
SNSpectId number(9),
SNId number(9),
Req_Date date,
Telescope varchar2(20),
Obs_Date date,
Spec_Eval varchar2(20),
CategoryType varchar2(20),
z_sn binary_float,
z_sn_err binary_float,
z_gal binary_float,
z_gal_err binary_float,
constraint pk_snspect primary key (SNSpectId));

create sequence snspect_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNFit 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNFit (
SNFitId number(9),
SNId number(9),
Version number(9),
Peak_Mjd binary_float,
Peak_MagB binary_float,
Delta binary_float,
Av binary_float,
z binary_float,
chisq binary_float,
constraint pk_snfit primary key (SNFitId));

create sequence snfit_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNGals 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNGals (
SNGalId number(9),
SNId number(9),
g binary_float,
g_err binary_float,
r binary_float,
r_err binary_float,
i binary_float,
i_err binary_float,
z binary_float,
z_err binary_float,
Y binary_float,
Y_err binary_float,
photoz binary_float,
photoz_err binary_float,
specz binary_float,
specz_err binary_float,
constraint pk_sngals primary key (SNGalId));

create sequence sngals_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNObs 
-- Dora Cai created the new table on 07/03/08
-- Dora Cai added 4 new columns on 03/16/2009 as per request from John Marriner:
-- pizelx, pixely, status, mag
--
CREATE TABLE SNObs (
SNObjId number(12),
ExposureID number(9),
CoaddID number(9),
Object_type number(6),
Version number(9),
RA binary_double,
Dec binary_double,
flux binary_float,
flux_err binary_float,
chisq binary_float,
pixelx binary_float,
pixely binary_float,
status number(10),
mag binary_float,
constraint pk_snobs primary key (SNObjId));

create sequence snobs_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: SNCand 
-- Dora Cai created the new table on 07/03/08
--
CREATE TABLE SNCand (
SNId number(9),
RA binary_double,
Dec binary_double,
Entry_date date,
Orig_RA binary_double,
Orig_Dec binary_double,
Cand_Type varchar2(20),
constraint pk_sncand primary key (SNId));

create sequence sncand_seq cache 100 start with 1 increment by 1;

-- 
-- TABLE: galaxy_standards 
-- Dora Cai created the new table on 09/15/08
--
create table galaxy_standards(
id  Number(20) not null,
ra  binary_double,
dec binary_double,
u binary_float,
g binary_float,
r binary_float,
i binary_float,
z binary_float,
u_err binary_float,
g_err binary_float,
r_err binary_float,
i_err binary_float,
z_err binary_float,
modelMag_u  binary_float, 
modelMag_g binary_float,
modelMag_r binary_float,
modelMag_i binary_float,
modelMag_z binary_float,
modelMagErr_u binary_float,
modelMagErr_g binary_float,
modelMagErr_r binary_float,
modelMagErr_i binary_float,
modelMagErr_z binary_float,
petroMag_u binary_float,
petroMag_g binary_float,
petroMag_r binary_float,
petroMag_i binary_float,
petroMag_z binary_float,
petroMagErr_u binary_float,
petroMagErr_g binary_float,
petroMagErr_r binary_float,
petroMagErr_i binary_float,
petroMagErr_z binary_float,
petroR50_u binary_float,
petroR50_g binary_float,
petroR50_r binary_float,
petroR50_i binary_float,
petroR50_z binary_float,
petroR50Err_u binary_float,
petroR50Err_g binary_float,
petroR50Err_r binary_float,
petroR50Err_i binary_float,
petroR50Err_z binary_float,
petroR90_u binary_float,
petroR90_g binary_float,
petroR90_r binary_float,
petroR90_i binary_float,
petroR90_z binary_float,
petroR90Err_u binary_float,
petroR90Err_g binary_float,
petroR90Err_r binary_float,
petroR90Err_i binary_float,
petroR90Err_z binary_float,
probPSF_u binary_float,
probPSF_g binary_float,
probPSF_r binary_float,
probPSF_i binary_float,
probPSF_z binary_float,
extinction_u binary_float,
extinction_g binary_float,
extinction_r binary_float,
extinction_i binary_float,
extinction_z binary_float,
constraint pk_galaxy_standards primary key (id));

-- 
-- TABLE: dc4_truth 
-- Dora Cai created the new table on 10/15/08
-- Dora Cai 02/19/09 add new column redshift
--
CREATE TABLE DC4_TRUTH(
    DC4_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    DATAFILE_NAME       VARCHAR2(40),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    REDSHIFT		BINARY_FLOAT,
    CONSTRAINT PK_DC4_TRUTH PRIMARY KEY (DC4_TRUTH_ID)
)
;

create sequence dc4_truth_seq cache 100 start with 1 increment by 1;

-- 
-- VIEW: objects_all 
-- Dora Cai created new view on 10/21/08
-- Dora Cai modified on 10/21/08 to rename the column "run" to "partkey"
-- Dora Cai modified on 05/06/09 to add "union objects_2009"
-- Dora Cai modified on 06/06/09 to modify "union objects_current"
-- Dora Cai modified on 07/17/09 to modify "union objects_2007"
-- Dora Cai drop the view on 07/22/09 
--
CREATE view OBJECTS_ALL
as select OBJECT_ID, EQUINOX, BAND, HTMID,
CX, CY, CZ, PARENTID, SOFTID, IMAGEID,
ZEROPOINT, ERRZEROPOINT, ZEROPOINTID,
OBJECT_NUMBER, MAG_AUTO, MAGERR_AUTO,
MAG_APER_1, MAGERR_APER_1, MAG_APER_2,
MAGERR_APER_2, MAG_APER_3, MAGERR_APER_3,
MAG_APER_4, MAGERR_APER_4, MAG_APER_5,
MAGERR_APER_5, MAG_APER_6, MAGERR_APER_6, 
ALPHA_J2000, DELTA_J2000, ALPHAPEAK_J2000,
DELTAPEAK_J2000, X2_WORLD, ERRX2_WORLD,
Y2_WORLD, ERRY2_WORLD, XY_WORLD,
ERRXY_WORLD, THRESHOLD, X_IMAGE,
Y_IMAGE, XMIN_IMAGE, YMIN_IMAGE,
XMAX_IMAGE, YMAX_IMAGE, X2_IMAGE,
ERRX2_IMAGE, Y2_IMAGE, ERRY2_IMAGE,
XY_IMAGE, ERRXY_IMAGE, A_IMAGE,
ERRA_IMAGE, B_IMAGE, ERRB_IMAGE,
THETA_IMAGE, ERRTHETA_IMAGE,
ELLIPTICITY, CLASS_STAR, FLAGS,
FLUX_RADIUS, FWHM_WORLD, ISOAREA_WORLD,
THETA_J2000, BACKGROUND, PARTKEY, catalogid,
ra, dec
from objects
union
select OBJECT_ID, EQUINOX, BAND, HTMID,
CX, CY, CZ, PARENTID, SOFTID, IMAGEID,
ZEROPOINT, ERRZEROPOINT, ZEROPOINTID,
OBJECT_NUMBER, MAG_AUTO, MAGERR_AUTO,
MAG_APER_1, MAGERR_APER_1, MAG_APER_2,
MAGERR_APER_2, MAG_APER_3, MAGERR_APER_3,
MAG_APER_4, MAGERR_APER_4, MAG_APER_5,
MAGERR_APER_5, MAG_APER_6, MAGERR_APER_6, 
ALPHA_J2000, DELTA_J2000, ALPHAPEAK_J2000,
DELTAPEAK_J2000, X2_WORLD, ERRX2_WORLD,
Y2_WORLD, ERRY2_WORLD, XY_WORLD,
ERRXY_WORLD, THRESHOLD, X_IMAGE,
Y_IMAGE, XMIN_IMAGE, YMIN_IMAGE,
XMAX_IMAGE, YMAX_IMAGE, X2_IMAGE,
ERRX2_IMAGE, Y2_IMAGE, ERRY2_IMAGE,
XY_IMAGE, ERRXY_IMAGE, A_IMAGE,
ERRA_IMAGE, B_IMAGE, ERRB_IMAGE,
THETA_IMAGE, ERRTHETA_IMAGE,
ELLIPTICITY, CLASS_STAR, FLAGS,
FLUX_RADIUS, FWHM_WORLD, ISOAREA_WORLD,
THETA_J2000, BACKGROUND, PARTKEY, catalogid,
ra, dec
from objects_2008
union
select OBJECT_ID, EQUINOX, BAND, HTMID,
CX, CY, CZ, PARENTID, SOFTID, IMAGEID,
ZEROPOINT, ERRZEROPOINT, ZEROPOINTID,
OBJECT_NUMBER, MAG_AUTO, MAGERR_AUTO,
MAG_APER_1, MAGERR_APER_1, MAG_APER_2,
MAGERR_APER_2, MAG_APER_3, MAGERR_APER_3,
MAG_APER_4, MAGERR_APER_4, MAG_APER_5,
MAGERR_APER_5, MAG_APER_6, MAGERR_APER_6, 
ALPHA_J2000, DELTA_J2000, ALPHAPEAK_J2000,
DELTAPEAK_J2000, X2_WORLD, ERRX2_WORLD,
Y2_WORLD, ERRY2_WORLD, XY_WORLD,
ERRXY_WORLD, THRESHOLD, X_IMAGE,
Y_IMAGE, XMIN_IMAGE, YMIN_IMAGE,
XMAX_IMAGE, YMAX_IMAGE, X2_IMAGE,
ERRX2_IMAGE, Y2_IMAGE, ERRY2_IMAGE,
XY_IMAGE, ERRXY_IMAGE, A_IMAGE,
ERRA_IMAGE, B_IMAGE, ERRB_IMAGE,
THETA_IMAGE, ERRTHETA_IMAGE,
ELLIPTICITY, CLASS_STAR, FLAGS,
FLUX_RADIUS, FWHM_WORLD, ISOAREA_WORLD,
THETA_J2000, BACKGROUND, PARTKEY, catalogid,
ra, dec
from objects_current;

-- 
-- VIEW: objects 
-- Dora Cai created new view on 07/16/09
-- Dora Cai added 80 columns on 07/30/2009 per Shantanu's request (JIRA DES-1030)
-- Dora Cai added 1 new column, release_code, on 08/05/2009 per Tony's request (JIRA DES-1061) 
-- Dora Cai rename column release_code to dr (means: data release) on 08/07/2009 per Joe's request (JIRA DES-1061) 
-- Dora Cai added 26 new columns per Joe's request (JIRA DES-1079) 
-- Dora Cai added 3 new columns, on 08/31/2009 per Shantanu's request (JIRA DES-1170) 
-- Dora Cai added 89 new columns, on 09/24/2009 per Shantanu's request (JIRA DES-1260) 
-- Dora Cai on 10/01/2009 added 3 columns on flags_weight, nlowweight_iso and nlowdweight_iso (JIRA DES-1178)
-- Dora Cai on 10/05/2009 added 2 columns: spread_model and spreaderr_model (JIRA DES-1178)
--
CREATE view OBJECTS
as select OBJECT_ID, EQUINOX, BAND, HTMID,
CX, CY, CZ, PARENTID, SOFTID, IMAGEID,
ZEROPOINT, ERRZEROPOINT, ZEROPOINTID,
OBJECT_NUMBER, MAG_AUTO, MAGERR_AUTO,
MAG_APER_1, MAGERR_APER_1, MAG_APER_2,
MAGERR_APER_2, MAG_APER_3, MAGERR_APER_3,
MAG_APER_4, MAGERR_APER_4, MAG_APER_5,
MAGERR_APER_5, MAG_APER_6, MAGERR_APER_6, 
ALPHA_J2000, DELTA_J2000, ALPHAPEAK_J2000,
DELTAPEAK_J2000, X2_WORLD, ERRX2_WORLD,
Y2_WORLD, ERRY2_WORLD, XY_WORLD,
ERRXY_WORLD, THRESHOLD, X_IMAGE,
Y_IMAGE, XMIN_IMAGE, YMIN_IMAGE,
XMAX_IMAGE, YMAX_IMAGE, X2_IMAGE,
ERRX2_IMAGE, Y2_IMAGE, ERRY2_IMAGE,
XY_IMAGE, ERRXY_IMAGE, A_IMAGE,
ERRA_IMAGE, B_IMAGE, ERRB_IMAGE,
THETA_IMAGE, ERRTHETA_IMAGE,
ELLIPTICITY, CLASS_STAR, FLAGS,
FLUX_RADIUS, FWHM_WORLD, ISOAREA_WORLD,
THETA_J2000, BACKGROUND, PARTKEY, catalogid,
ra, dec, 
CHI2_MODEL, FLAGS_MODEL, NITER_MODEL, FLUX_MODEL, 
FLUXERR_MODEL, MAG_MODEL, MAGERR_MODEL, XMODEL_IMAGE,
YMODEL_IMAGE, XMODEL_WORLD, YMODEL_WORLD, ALPHAMODEL_SKY,
DELTAMODEL_SKY, ALPHAMODEL_J2000, DELTAMODEL_J2000,
ERRX2MODEL_IMAGE, ERRY2MODEL_IMAGE, ERRXYMODEL_IMAGE,
ERRX2MODEL_WORLD, ERRY2MODEL_WORLD, ERRXYMODEL_WORLD,
ERRCXXMODEL_IMAGE, ERRCYYMODEL_IMAGE, ERRCXYMODEL_IMAGE,
ERRCXXMODEL_WORLD, ERRCYYMODEL_WORLD, ERRCXYMODEL_WORLD,
ERRAMODEL_IMAGE, ERRBMODEL_IMAGE, ERRTHETAMODEL_IMAGE,
ERRAMODEL_WORLD, ERRBMODEL_WORLD, ERRTHETAMODEL_WORLD,
ERRTHETAMODEL_SKY, ERRTHETAMODEL_J2000, X2MODEL_IMAGE, 
Y2MODEL_IMAGE, XYMODEL_IMAGE, E1MODEL_IMAGE,
E2MODEL_IMAGE, EPS1MODEL_IMAGE, EPS2MODEL_IMAGE,
FLUX_SPHEROID, FLUXERR_SPHEROID, MAG_SPHEROID, 
MAGERR_SPHEROID, SPHEROID_REFF_IMAGE, SPHEROID_REFFERR_IMAGE,
SPHEROID_REFF_WORLD, SPHEROID_REFFERR_WORLD, SPHEROID_ASPECT_IMAGE,
SPHEROID_ASPECTERR_IMAGE, SPHEROID_ASPECT_WORLD, SPHEROID_ASPECTERR_WORLD,
SPHEROID_THETA_IMAGE, SPHEROID_THETAERR_IMAGE, SPHEROID_THETA_WORLD,
SPHEROID_THETAERR_WORLD, SPHEROID_THETA_SKY, SPHEROID_THETA_J2000,
FLUX_DISK, FLUXERR_DISK, MAG_DISK, MAGERR_DISK, DISK_SCALE_IMAGE,
DISK_SCALEERR_IMAGE, DISK_SCALE_WORLD, DISK_SCALEERR_WORLD,
DISK_ASPECT_IMAGE, DISK_ASPECTERR_IMAGE, DISK_ASPECT_WORLD,
DISK_ASPECTERR_WORLD, DISK_INCLINATION, DISK_INCLINATIONERR,
DISK_THETA_IMAGE, DISK_THETAERR_IMAGE, DISK_THETA_WORLD,
DISK_THETAERR_WORLD, DISK_THETA_SKY, DISK_THETA_J2000, DR,	
FLUX_PSF, FLUXERR_PSF, MAG_PSF, MAGERR_PSF, MAG_APER_7, 
MAGERR_APER_7, MAG_APER_8, MAGERR_APER_8, MAG_APER_9, 
MAGERR_APER_9, MAG_APER_10, MAGERR_APER_10, MAG_APER_11, 
MAGERR_APER_11, MAG_APER_12, MAGERR_APER_12, MAG_APER_13,
MAGERR_APER_13, MAG_APER_14, MAGERR_APER_14, MAG_APER_15,
MAGERR_APER_15, MAG_APER_16, MAGERR_APER_16, MAG_APER_17, MAGERR_APER_17,
ERRAWIN_WORLD, ERRBWIN_WORLD, ERRTHETAWIN_J2000,
ALPHAPSF_J2000, ALPHAWIN_J2000, AMODEL_IMAGE, AMODEL_WORLD,
A_WORLD, BMODEL_IMAGE, BMODEL_WORLD, B_WORLD, CHI2_PSF,
CONCENTRATION_MODEL, DELTAPSF_J2000, DELTAWIN_J2000, ELLIP1MODEL_IMAGE,
ELLIP1MODEL_WORLD, ELLIP2MODEL_IMAGE, ELLIP2MODEL_WORLD, ELONGATION,
ERRAPSF_IMAGE, ERRAPSF_WORLD, ERRAWIN_IMAGE, ERRBPSF_IMAGE,
ERRBPSF_WORLD, ERRBWIN_IMAGE, ERRTHETAPSF_IMAGE, ERRTHETAPSF_J2000,
ERRTHETAWIN_IMAGE, FLUX_APER_1, FLUX_APER_2, FLUX_APER_3,
FLUX_APER_4, FLUX_APER_5, FLUX_APER_6, FLUX_APER_7, FLUX_APER_8,
FLUX_APER_9, FLUX_APER_10, FLUX_APER_11, FLUX_APER_12, FLUX_APER_13,
FLUX_APER_14, FLUX_APER_15, FLUX_APER_16, FLUX_APER_17,
FLUX_AUTO, FLUX_EFF_MODEL, FLUXERR_APER_1, FLUXERR_APER_2,
FLUXERR_APER_3, FLUXERR_APER_4, FLUXERR_APER_5, FLUXERR_APER_6,
FLUXERR_APER_7, FLUXERR_APER_8, FLUXERR_APER_9, FLUXERR_APER_10,
FLUXERR_APER_11, FLUXERR_APER_12, FLUXERR_APER_13, FLUXERR_APER_14,
FLUXERR_APER_15, FLUXERR_APER_16, FLUXERR_APER_17, FLUXERR_AUTO,
FLUXERR_ISO, FLUXERR_PETRO, FLUX_ISO, FLUX_MAX, FLUX_MAX_MODEL,
FLUX_MEAN_MODEL, FLUX_PETRO, ISOAREAF_IMAGE, ISOAREAF_WORLD,
ISOAREA_IMAGE, KRON_RADIUS, MAGERR_ISO, MAGERR_PETRO, MAG_ISO,
MAG_PETRO, MU_EFF_MODEL, MU_MAX, MU_MAX_MODEL, MU_MEAN_MODEL,
MU_THRESHOLD, NITER_PSF, PETRO_RADIUS, THETAMODEL_IMAGE, THETAMODEL_J2000,
XPEAK_IMAGE, XPSF_IMAGE, XWIN_IMAGE, YPEAK_IMAGE, YPSF_IMAGE,
YWIN_IMAGE, DURATION_ANALYSIS, 
FLAGS_WEIGHT, NLOWWEIGHT_ISO, NLOWDWEIGHT_ISO,
SPREAD_MODEL, SPREADERR_MODEL
from objects_2007
union
select OBJECT_ID, EQUINOX, BAND, HTMID,
CX, CY, CZ, PARENTID, SOFTID, IMAGEID,
ZEROPOINT, ERRZEROPOINT, ZEROPOINTID,
OBJECT_NUMBER, MAG_AUTO, MAGERR_AUTO,
MAG_APER_1, MAGERR_APER_1, MAG_APER_2,
MAGERR_APER_2, MAG_APER_3, MAGERR_APER_3,
MAG_APER_4, MAGERR_APER_4, MAG_APER_5,
MAGERR_APER_5, MAG_APER_6, MAGERR_APER_6, 
ALPHA_J2000, DELTA_J2000, ALPHAPEAK_J2000,
DELTAPEAK_J2000, X2_WORLD, ERRX2_WORLD,
Y2_WORLD, ERRY2_WORLD, XY_WORLD,
ERRXY_WORLD, THRESHOLD, X_IMAGE,
Y_IMAGE, XMIN_IMAGE, YMIN_IMAGE,
XMAX_IMAGE, YMAX_IMAGE, X2_IMAGE,
ERRX2_IMAGE, Y2_IMAGE, ERRY2_IMAGE,
XY_IMAGE, ERRXY_IMAGE, A_IMAGE,
ERRA_IMAGE, B_IMAGE, ERRB_IMAGE,
THETA_IMAGE, ERRTHETA_IMAGE,
ELLIPTICITY, CLASS_STAR, FLAGS,
FLUX_RADIUS, FWHM_WORLD, ISOAREA_WORLD,
THETA_J2000, BACKGROUND, PARTKEY, catalogid,
ra, dec,
CHI2_MODEL, FLAGS_MODEL, NITER_MODEL, FLUX_MODEL, 
FLUXERR_MODEL, MAG_MODEL, MAGERR_MODEL, XMODEL_IMAGE,
YMODEL_IMAGE, XMODEL_WORLD, YMODEL_WORLD, ALPHAMODEL_SKY,
DELTAMODEL_SKY, ALPHAMODEL_J2000, DELTAMODEL_J2000,
ERRX2MODEL_IMAGE, ERRY2MODEL_IMAGE, ERRXYMODEL_IMAGE,
ERRX2MODEL_WORLD, ERRY2MODEL_WORLD, ERRXYMODEL_WORLD,
ERRCXXMODEL_IMAGE, ERRCYYMODEL_IMAGE, ERRCXYMODEL_IMAGE,
ERRCXXMODEL_WORLD, ERRCYYMODEL_WORLD, ERRCXYMODEL_WORLD,
ERRAMODEL_IMAGE, ERRBMODEL_IMAGE, ERRTHETAMODEL_IMAGE,
ERRAMODEL_WORLD, ERRBMODEL_WORLD, ERRTHETAMODEL_WORLD,
ERRTHETAMODEL_SKY, ERRTHETAMODEL_J2000, X2MODEL_IMAGE, 
Y2MODEL_IMAGE, XYMODEL_IMAGE, E1MODEL_IMAGE,
E2MODEL_IMAGE, EPS1MODEL_IMAGE, EPS2MODEL_IMAGE,
FLUX_SPHEROID, FLUXERR_SPHEROID, MAG_SPHEROID, 
MAGERR_SPHEROID, SPHEROID_REFF_IMAGE, SPHEROID_REFFERR_IMAGE,
SPHEROID_REFF_WORLD, SPHEROID_REFFERR_WORLD, SPHEROID_ASPECT_IMAGE,
SPHEROID_ASPECTERR_IMAGE, SPHEROID_ASPECT_WORLD, SPHEROID_ASPECTERR_WORLD,
SPHEROID_THETA_IMAGE, SPHEROID_THETAERR_IMAGE, SPHEROID_THETA_WORLD,
SPHEROID_THETAERR_WORLD, SPHEROID_THETA_SKY, SPHEROID_THETA_J2000,
FLUX_DISK, FLUXERR_DISK, MAG_DISK, MAGERR_DISK, DISK_SCALE_IMAGE,
DISK_SCALEERR_IMAGE, DISK_SCALE_WORLD, DISK_SCALEERR_WORLD,
DISK_ASPECT_IMAGE, DISK_ASPECTERR_IMAGE, DISK_ASPECT_WORLD,
DISK_ASPECTERR_WORLD, DISK_INCLINATION, DISK_INCLINATIONERR,
DISK_THETA_IMAGE, DISK_THETAERR_IMAGE, DISK_THETA_WORLD,
DISK_THETAERR_WORLD, DISK_THETA_SKY, DISK_THETA_J2000, DR,	
FLUX_PSF, FLUXERR_PSF, MAG_PSF, MAGERR_PSF, MAG_APER_7, 
MAGERR_APER_7, MAG_APER_8, MAGERR_APER_8, MAG_APER_9, 
MAGERR_APER_9, MAG_APER_10, MAGERR_APER_10, MAG_APER_11, 
MAGERR_APER_11, MAG_APER_12, MAGERR_APER_12, MAG_APER_13,
MAGERR_APER_13, MAG_APER_14, MAGERR_APER_14, MAG_APER_15,
MAGERR_APER_15, MAG_APER_16, MAGERR_APER_16, MAG_APER_17, MAGERR_APER_17,
ERRAWIN_WORLD, ERRBWIN_WORLD, ERRTHETAWIN_J2000,
ALPHAPSF_J2000, ALPHAWIN_J2000, AMODEL_IMAGE, AMODEL_WORLD,
A_WORLD, BMODEL_IMAGE, BMODEL_WORLD, B_WORLD, CHI2_PSF,
CONCENTRATION_MODEL, DELTAPSF_J2000, DELTAWIN_J2000, ELLIP1MODEL_IMAGE,
ELLIP1MODEL_WORLD, ELLIP2MODEL_IMAGE, ELLIP2MODEL_WORLD, ELONGATION,
ERRAPSF_IMAGE, ERRAPSF_WORLD, ERRAWIN_IMAGE, ERRBPSF_IMAGE,
ERRBPSF_WORLD, ERRBWIN_IMAGE, ERRTHETAPSF_IMAGE, ERRTHETAPSF_J2000,
ERRTHETAWIN_IMAGE, FLUX_APER_1, FLUX_APER_2, FLUX_APER_3,
FLUX_APER_4, FLUX_APER_5, FLUX_APER_6, FLUX_APER_7, FLUX_APER_8,
FLUX_APER_9, FLUX_APER_10, FLUX_APER_11, FLUX_APER_12, FLUX_APER_13,
FLUX_APER_14, FLUX_APER_15, FLUX_APER_16, FLUX_APER_17,
FLUX_AUTO, FLUX_EFF_MODEL, FLUXERR_APER_1, FLUXERR_APER_2,
FLUXERR_APER_3, FLUXERR_APER_4, FLUXERR_APER_5, FLUXERR_APER_6,
FLUXERR_APER_7, FLUXERR_APER_8, FLUXERR_APER_9, FLUXERR_APER_10,
FLUXERR_APER_11, FLUXERR_APER_12, FLUXERR_APER_13, FLUXERR_APER_14,
FLUXERR_APER_15, FLUXERR_APER_16, FLUXERR_APER_17, FLUXERR_AUTO,
FLUXERR_ISO, FLUXERR_PETRO, FLUX_ISO, FLUX_MAX, FLUX_MAX_MODEL,
FLUX_MEAN_MODEL, FLUX_PETRO, ISOAREAF_IMAGE, ISOAREAF_WORLD,
ISOAREA_IMAGE, KRON_RADIUS, MAGERR_ISO, MAGERR_PETRO, MAG_ISO,
MAG_PETRO, MU_EFF_MODEL, MU_MAX, MU_MAX_MODEL, MU_MEAN_MODEL,
MU_THRESHOLD, NITER_PSF, PETRO_RADIUS, THETAMODEL_IMAGE, THETAMODEL_J2000,
XPEAK_IMAGE, XPSF_IMAGE, XWIN_IMAGE, YPEAK_IMAGE, YPSF_IMAGE,
YWIN_IMAGE, DURATION_ANALYSIS,
FLAGS_WEIGHT, NLOWWEIGHT_ISO, NLOWDWEIGHT_ISO,
SPREAD_MODEL, SPREADERR_MODEL
from objects_2008
union
select OBJECT_ID, EQUINOX, BAND, HTMID,
CX, CY, CZ, PARENTID, SOFTID, IMAGEID,
ZEROPOINT, ERRZEROPOINT, ZEROPOINTID,
OBJECT_NUMBER, MAG_AUTO, MAGERR_AUTO,
MAG_APER_1, MAGERR_APER_1, MAG_APER_2,
MAGERR_APER_2, MAG_APER_3, MAGERR_APER_3,
MAG_APER_4, MAGERR_APER_4, MAG_APER_5,
MAGERR_APER_5, MAG_APER_6, MAGERR_APER_6, 
ALPHA_J2000, DELTA_J2000, ALPHAPEAK_J2000,
DELTAPEAK_J2000, X2_WORLD, ERRX2_WORLD,
Y2_WORLD, ERRY2_WORLD, XY_WORLD,
ERRXY_WORLD, THRESHOLD, X_IMAGE,
Y_IMAGE, XMIN_IMAGE, YMIN_IMAGE,
XMAX_IMAGE, YMAX_IMAGE, X2_IMAGE,
ERRX2_IMAGE, Y2_IMAGE, ERRY2_IMAGE,
XY_IMAGE, ERRXY_IMAGE, A_IMAGE,
ERRA_IMAGE, B_IMAGE, ERRB_IMAGE,
THETA_IMAGE, ERRTHETA_IMAGE,
ELLIPTICITY, CLASS_STAR, FLAGS,
FLUX_RADIUS, FWHM_WORLD, ISOAREA_WORLD,
THETA_J2000, BACKGROUND, PARTKEY, catalogid,
ra, dec,
CHI2_MODEL, FLAGS_MODEL, NITER_MODEL, FLUX_MODEL, 
FLUXERR_MODEL, MAG_MODEL, MAGERR_MODEL, XMODEL_IMAGE,
YMODEL_IMAGE, XMODEL_WORLD, YMODEL_WORLD, ALPHAMODEL_SKY,
DELTAMODEL_SKY, ALPHAMODEL_J2000, DELTAMODEL_J2000,
ERRX2MODEL_IMAGE, ERRY2MODEL_IMAGE, ERRXYMODEL_IMAGE,
ERRX2MODEL_WORLD, ERRY2MODEL_WORLD, ERRXYMODEL_WORLD,
ERRCXXMODEL_IMAGE, ERRCYYMODEL_IMAGE, ERRCXYMODEL_IMAGE,
ERRCXXMODEL_WORLD, ERRCYYMODEL_WORLD, ERRCXYMODEL_WORLD,
ERRAMODEL_IMAGE, ERRBMODEL_IMAGE, ERRTHETAMODEL_IMAGE,
ERRAMODEL_WORLD, ERRBMODEL_WORLD, ERRTHETAMODEL_WORLD,
ERRTHETAMODEL_SKY, ERRTHETAMODEL_J2000, X2MODEL_IMAGE, 
Y2MODEL_IMAGE, XYMODEL_IMAGE, E1MODEL_IMAGE,
E2MODEL_IMAGE, EPS1MODEL_IMAGE, EPS2MODEL_IMAGE,
FLUX_SPHEROID, FLUXERR_SPHEROID, MAG_SPHEROID, 
MAGERR_SPHEROID, SPHEROID_REFF_IMAGE, SPHEROID_REFFERR_IMAGE,
SPHEROID_REFF_WORLD, SPHEROID_REFFERR_WORLD, SPHEROID_ASPECT_IMAGE,
SPHEROID_ASPECTERR_IMAGE, SPHEROID_ASPECT_WORLD, SPHEROID_ASPECTERR_WORLD,
SPHEROID_THETA_IMAGE, SPHEROID_THETAERR_IMAGE, SPHEROID_THETA_WORLD,
SPHEROID_THETAERR_WORLD, SPHEROID_THETA_SKY, SPHEROID_THETA_J2000,
FLUX_DISK, FLUXERR_DISK, MAG_DISK, MAGERR_DISK, DISK_SCALE_IMAGE,
DISK_SCALEERR_IMAGE, DISK_SCALE_WORLD, DISK_SCALEERR_WORLD,
DISK_ASPECT_IMAGE, DISK_ASPECTERR_IMAGE, DISK_ASPECT_WORLD,
DISK_ASPECTERR_WORLD, DISK_INCLINATION, DISK_INCLINATIONERR,
DISK_THETA_IMAGE, DISK_THETAERR_IMAGE, DISK_THETA_WORLD,
DISK_THETAERR_WORLD, DISK_THETA_SKY, DISK_THETA_J2000, DR,	
FLUX_PSF, FLUXERR_PSF, MAG_PSF, MAGERR_PSF, MAG_APER_7, 
MAGERR_APER_7, MAG_APER_8, MAGERR_APER_8, MAG_APER_9, 
MAGERR_APER_9, MAG_APER_10, MAGERR_APER_10, MAG_APER_11, 
MAGERR_APER_11, MAG_APER_12, MAGERR_APER_12, MAG_APER_13,
MAGERR_APER_13, MAG_APER_14, MAGERR_APER_14, MAG_APER_15,
MAGERR_APER_15, MAG_APER_16, MAGERR_APER_16, MAG_APER_17, MAGERR_APER_17,
ERRAWIN_WORLD, ERRBWIN_WORLD, ERRTHETAWIN_J2000,
ALPHAPSF_J2000, ALPHAWIN_J2000, AMODEL_IMAGE, AMODEL_WORLD,
A_WORLD, BMODEL_IMAGE, BMODEL_WORLD, B_WORLD, CHI2_PSF,
CONCENTRATION_MODEL, DELTAPSF_J2000, DELTAWIN_J2000, ELLIP1MODEL_IMAGE,
ELLIP1MODEL_WORLD, ELLIP2MODEL_IMAGE, ELLIP2MODEL_WORLD, ELONGATION,
ERRAPSF_IMAGE, ERRAPSF_WORLD, ERRAWIN_IMAGE, ERRBPSF_IMAGE,
ERRBPSF_WORLD, ERRBWIN_IMAGE, ERRTHETAPSF_IMAGE, ERRTHETAPSF_J2000,
ERRTHETAWIN_IMAGE, FLUX_APER_1, FLUX_APER_2, FLUX_APER_3,
FLUX_APER_4, FLUX_APER_5, FLUX_APER_6, FLUX_APER_7, FLUX_APER_8,
FLUX_APER_9, FLUX_APER_10, FLUX_APER_11, FLUX_APER_12, FLUX_APER_13,
FLUX_APER_14, FLUX_APER_15, FLUX_APER_16, FLUX_APER_17,
FLUX_AUTO, FLUX_EFF_MODEL, FLUXERR_APER_1, FLUXERR_APER_2,
FLUXERR_APER_3, FLUXERR_APER_4, FLUXERR_APER_5, FLUXERR_APER_6,
FLUXERR_APER_7, FLUXERR_APER_8, FLUXERR_APER_9, FLUXERR_APER_10,
FLUXERR_APER_11, FLUXERR_APER_12, FLUXERR_APER_13, FLUXERR_APER_14,
FLUXERR_APER_15, FLUXERR_APER_16, FLUXERR_APER_17, FLUXERR_AUTO,
FLUXERR_ISO, FLUXERR_PETRO, FLUX_ISO, FLUX_MAX, FLUX_MAX_MODEL,
FLUX_MEAN_MODEL, FLUX_PETRO, ISOAREAF_IMAGE, ISOAREAF_WORLD,
ISOAREA_IMAGE, KRON_RADIUS, MAGERR_ISO, MAGERR_PETRO, MAG_ISO,
MAG_PETRO, MU_EFF_MODEL, MU_MAX, MU_MAX_MODEL, MU_MEAN_MODEL,
MU_THRESHOLD, NITER_PSF, PETRO_RADIUS, THETAMODEL_IMAGE, THETAMODEL_J2000,
XPEAK_IMAGE, XPSF_IMAGE, XWIN_IMAGE, YPEAK_IMAGE, YPSF_IMAGE,
YWIN_IMAGE, DURATION_ANALYSIS, 
FLAGS_WEIGHT, NLOWWEIGHT_ISO, NLOWDWEIGHT_ISO,
SPREAD_MODEL, SPREADERR_MODEL
from objects_current;

-- 
-- TABLE: wl_findstars 
-- Dora Cai created the new table on 12/16/08 for Tony's request
--
CREATE TABLE WL_FINDSTARS(
    ID    NUMBER(11, 0),
    CATALOGID NUMBER(9, 0),
    OBJECT_ID NUMBER(11, 0),
    OBJECT_NUMBER NUMBER(6, 0),
    SIGMA0 BINARY_DOUBLE,
    SIZE_FLAGS NUMBER(5),
    STAR_FLAG NUMBER(5),
    CONSTRAINT PK_WL_FINDSTARS PRIMARY KEY (ID)
)
;

create sequence wl_findstars_seq cache 100 start with 1 increment by 1;

create public synonym wl_findstars for des_admin.wl_findstars;
create public synonym wl_findstars_seq for des_admin.wl_findstars_seq;
grant all on wl_findstars to des_writer;
grant select on wl_findstars to des_reader;
grant all on wl_findstars_seq to des_writer;
grant select on wl_findstars_seq to des_reader;

-- 
-- TABLE: wl_psf 
-- Dora Cai created the new table on 12/16/08 for Tony's request
--
CREATE TABLE WL_PSF(
    ID    NUMBER(11, 0),
    CATALOGID NUMBER(9, 0),
    OBJECT_ID NUMBER(11, 0),
    OBJECT_NUMBER NUMBER(6, 0),
    PSF_FLAGS NUMBER(5, 0),
    NU BINARY_DOUBLE,
    PSF_ORDER NUMBER(2, 0),
    SIGMA_P BINARY_DOUBLE,
    COEFFS_1 BINARY_DOUBLE,
    COEFFS_2 BINARY_DOUBLE,
    COEFFS_3 BINARY_DOUBLE,
    COEFFS_4 BINARY_DOUBLE,
    COEFFS_5 BINARY_DOUBLE,
    COEFFS_6 BINARY_DOUBLE,
    COEFFS_7 BINARY_DOUBLE,
    COEFFS_8 BINARY_DOUBLE,
    COEFFS_9 BINARY_DOUBLE,
    COEFFS_10 BINARY_DOUBLE,
    COEFFS_11 BINARY_DOUBLE,
    COEFFS_12 BINARY_DOUBLE,
    COEFFS_13 BINARY_DOUBLE,
    COEFFS_14 BINARY_DOUBLE,
    COEFFS_15 BINARY_DOUBLE,
    COEFFS_16 BINARY_DOUBLE,
    COEFFS_17 BINARY_DOUBLE,
    COEFFS_18 BINARY_DOUBLE,
    COEFFS_19 BINARY_DOUBLE,
    COEFFS_20 BINARY_DOUBLE,
    COEFFS_21 BINARY_DOUBLE,
    COEFFS_22 BINARY_DOUBLE,
    COEFFS_23 BINARY_DOUBLE,
    COEFFS_24 BINARY_DOUBLE,
    COEFFS_25 BINARY_DOUBLE,
    COEFFS_26 BINARY_DOUBLE,
    COEFFS_27 BINARY_DOUBLE,
    COEFFS_28 BINARY_DOUBLE,
    COEFFS_29 BINARY_DOUBLE,
    COEFFS_30 BINARY_DOUBLE,
    COEFFS_31 BINARY_DOUBLE,
    COEFFS_32 BINARY_DOUBLE,
    COEFFS_33 BINARY_DOUBLE,
    COEFFS_34 BINARY_DOUBLE,
    COEFFS_35 BINARY_DOUBLE,
    COEFFS_36 BINARY_DOUBLE,
    COEFFS_37 BINARY_DOUBLE,
    COEFFS_38 BINARY_DOUBLE,
    COEFFS_39 BINARY_DOUBLE,
    COEFFS_40 BINARY_DOUBLE,
    COEFFS_41 BINARY_DOUBLE,
    COEFFS_42 BINARY_DOUBLE,
    COEFFS_43 BINARY_DOUBLE,
    COEFFS_44 BINARY_DOUBLE,
    COEFFS_45 BINARY_DOUBLE,
    COEFFS_46 BINARY_DOUBLE,
    COEFFS_47 BINARY_DOUBLE,
    COEFFS_48 BINARY_DOUBLE,
    COEFFS_49 BINARY_DOUBLE,
    COEFFS_50 BINARY_DOUBLE,
    COEFFS_51 BINARY_DOUBLE,
    COEFFS_52 BINARY_DOUBLE,
    COEFFS_53 BINARY_DOUBLE,
    COEFFS_54 BINARY_DOUBLE,
    COEFFS_55 BINARY_DOUBLE,
    COEFFS_56 BINARY_DOUBLE,
    COEFFS_57 BINARY_DOUBLE,
    COEFFS_58 BINARY_DOUBLE,
    COEFFS_59 BINARY_DOUBLE,
    COEFFS_60 BINARY_DOUBLE,
    COEFFS_61 BINARY_DOUBLE,
    COEFFS_62 BINARY_DOUBLE,
    COEFFS_63 BINARY_DOUBLE,
    COEFFS_64 BINARY_DOUBLE,
    COEFFS_65 BINARY_DOUBLE,
    COEFFS_66 BINARY_DOUBLE,
    CONSTRAINT PK_WL_PSF PRIMARY KEY (ID)
)
;

create sequence wl_psf_seq cache 100 start with 1 increment by 1;

create public synonym wl_psf for des_admin.wl_psf;
create public synonym wl_psf_seq for des_admin.wl_psf_seq;
grant all on wl_psf to des_writer;
grant select on wl_psf to des_reader;
grant all on wl_psf_seq to des_writer;
grant select on wl_psf_seq to des_reader;

-- 
-- TABLE: wl_shear 
-- Dora Cai created the new table on 12/16/08 for Tony's request
-- Dora Cai add 2 new columns on 09/14/2009: nu, shapelet_sigma (JIRA DES-1212)  
-- Dora Cai add 2 new columns on 09/14/2009: expand shear_flags from number(5,0) to number(10,0) (JIRA DES-1212)  
--
CREATE TABLE WL_SHEAR(
    ID    NUMBER(11, 0),
    CATALOGID NUMBER(9, 0),
    OBJECT_ID NUMBER(11, 0),
    OBJECT_NUMBER NUMBER(6, 0),
    SHEAR_FLAGS NUMBER(10, 0),
    SHEAR1 BINARY_DOUBLE,
    SHEAR2 BINARY_DOUBLE,
    SHEAR_COV00 BINARY_DOUBLE,
    SHEAR_COV01 BINARY_DOUBLE,
    SHEAR_COV11 BINARY_DOUBLE,
    GAL_ORDER NUMBER(1,0),
    COEFFS_1 BINARY_DOUBLE,
    COEFFS_2 BINARY_DOUBLE,
    COEFFS_3 BINARY_DOUBLE,
    COEFFS_4 BINARY_DOUBLE,
    COEFFS_5 BINARY_DOUBLE,
    COEFFS_6 BINARY_DOUBLE,
    COEFFS_7 BINARY_DOUBLE,
    COEFFS_8 BINARY_DOUBLE,
    COEFFS_9 BINARY_DOUBLE,
    COEFFS_10 BINARY_DOUBLE,
    COEFFS_11 BINARY_DOUBLE,
    COEFFS_12 BINARY_DOUBLE,
    COEFFS_13 BINARY_DOUBLE,
    COEFFS_14 BINARY_DOUBLE,
    COEFFS_15 BINARY_DOUBLE,
    COEFFS_16 BINARY_DOUBLE,
    COEFFS_17 BINARY_DOUBLE,
    COEFFS_18 BINARY_DOUBLE,
    COEFFS_19 BINARY_DOUBLE,
    COEFFS_20 BINARY_DOUBLE,
    COEFFS_21 BINARY_DOUBLE,
    COEFFS_22 BINARY_DOUBLE,
    COEFFS_23 BINARY_DOUBLE,
    COEFFS_24 BINARY_DOUBLE,
    COEFFS_25 BINARY_DOUBLE,
    COEFFS_26 BINARY_DOUBLE,
    COEFFS_27 BINARY_DOUBLE,
    COEFFS_28 BINARY_DOUBLE,
    NU BINARY_FLOAT,
    SHAPELET_SIGMA BINARY_FLOAT,
    CONSTRAINT PK_WL_SHEAR PRIMARY KEY (ID)
)
;

create sequence wl_shear_seq cache 100 start with 1 increment by 1;

create public synonym wl_shear for des_admin.wl_shear;
create public synonym wl_shear_seq for des_admin.wl_shear_seq;
grant all on wl_shear to des_writer;
grant select on wl_shear to des_reader;
grant all on wl_shear_seq to des_writer;
grant select on wl_shear_seq to des_reader;

-- 
-- TABLE: wl 
-- Dora Cai created the new table on 12/16/08 for Tony's request
-- Dora Cai modify the table on 12/18/08 for Joe's request (delete 4 columns
-- and add 28 columns)
--

CREATE TABLE WL(
    ID              NUMBER(10, 0),
    PROJECT         VARCHAR2(20),
    RUN             VARCHAR2(100),
    NITE            VARCHAR2(20),
    TILENAME        VARCHAR2(20),
    BAND            VARCHAR2(20),
    CCD             NUMBER(3, 0),
    WLNAME          VARCHAR2(100),
    WLTYPE          VARCHAR2(20),
    PARENTID        NUMBER(10, 0),
    OBJECTS         NUMBER(8, 0),
    IMAGEID         NUMBER(10, 0),
    CATALOGID       NUMBER(9, 0),
    FSEXIT  NUMBER(5),
    FSNOBJ NUMBER(6),
    FSNSTARS NUMBER(6),
    MPEXIT NUMBER(5),
    MPNSTARS NUMBER(6),
    NS_PSF  NUMBER(6),
    NF_RANGE NUMBER(6),
    NF_EDGE NUMBER(6),
    NF_NPIX NUMBER(6),
    NF_MPTMV NUMBER(6),
    NF_MPOTH NUMBER(6),
    NF_PSF NUMBER(6),
    MSEXIT NUMBER(5),
    MSNOBJ NUMBER(6),
    NS_GAMMA NUMBER(6),
    NS_NATIV NUMBER(6),
    NS_RNGE1 NUMBER(6),
    NS_RNGE2 NUMBER(6),
    NF_EDGE1 NUMBER(6),
    NF_EDGE2 NUMBER(6),
    NF_NPIX1 NUMBER(6),
    NF_NPIX2 NUMBER(6),
    NF_NATIV NUMBER(6),
    NF_SMALL NUMBER(6),
    NF_MSTMV NUMBER(6),
    NF_MSOTH NUMBER(6),
    NF_MU NUMBER(6),
    NF_GAMMA NUMBER(6),
    CONSTRAINT PK_WL PRIMARY KEY (ID)
)
;

create public synonym wl for des_admin.wl;
grant all on wl to des_writer;
grant select on wl to des_reader;

-- 
-- TABLE: desdm_columns 
-- Dora Cai created the new table on 01/15/09 for Tony's request
--
CREATE TABLE desdm_columns(
  column_name varchar2(30),
  table_name varchar2(30),
  column_id   number(3),
  data_type varchar2(20),
  on_index char(1) default 'N',
  units varchar2(25),
  tform varchar2(10),
  hdr_desc varchar2(73),
  description varchar2(2000),
  constraint pk_desdm_columns primary key (column_name, table_name)
)
;
  
-- 
-- TABLE: control_users 
-- Dora Cai created the new table on 01/22/09 for Tony's request
--
CREATE TABLE control_users(
    nvo_user     varchar2(30),
    project      varchar2(20),  
    constraint pk_control_users primary key (nvo_user, project)
)
;

-- 
-- TABLE: photo_z 
-- Dora Cai created the new table on 02/12/09 for Joe's request
--
CREATE TABLE photo_z(
    id           number(11),
    catalogid    number(9),
    photoz       binary_double,
    photozerr    binary_double,  
    flag         number(2),
    photoztype   varchar2(10),
    constraint pk_photo_z primary key (id)
)
;
create sequence photo_z_seq cache 100 start with 1 increment by 1;
create public synonym photo_z for des_admin.photo_z;
grant all on photo_z to des_writer;
grant select on photo_z to des_reader;
create public synonym photo_z_seq for des_admin.photo_z_seq;
grant all on photo_z_seq to des_writer;
grant select on photo_z_seq to des_reader;

-- 
-- TABLE: nitecmb_src 
-- Dora Cai created the new table on 04/02/09 for Choong's request
--
CREATE TABLE nitecmb_src(
    nitecmb_imageid number(9),
    src_imageid    number(9),
    constraint pk_nitecmd_src primary key (nitecmb_imageid, src_imageid)
)
;
create public synonym nitecmb_src for des_admin.nitecmb_src;
grant all on nitecmb_src to des_writer;
grant select on nitecmb_src to des_reader;


--
-- TABLE: OBJECTS_2009
-- Dora Cai added on 05/06/09
-- Dora Cai renamed the table from objects_2009 to objects_current
-- Dora Cai added 80 columns on 07/30/2009 per Shantanu's request (JIRA DES-1030)
-- Dora Cai added 1 new column, release_code, on 08/05/2009 per Tony's request (JIRA DES-1061) 
-- Dora Cai rename column release_code to dr (means: data release) on 08/07/2009 per Joe's request (JIRA DES-1061) 
-- Dora Cai added 26 new columns per Joe's request (JIRA DES-1079) 
-- Dora Cai added 3 new columns, on 08/31/2009 per Shantanu's request (JIRA DES-1170) 
-- Dora Cai added 89 new columns, on 09/24/2009 per Shantanu's request (JIRA DES-1260) 
-- Dora Cai on 10/01/2009 added 3 columns on flags_weight, nlowweight_iso and nlowdweight_iso (JIRA DES-1178)
-- Dora Cai on 10/05/2009 added 2 columns: spread_model and spreaderr_model (JIRA DES-1178)

CREATE TABLE OBJECTS_CURRENT(
OBJECT_ID               Number(11) NOT NULL,
EQUINOX                 BINARY_FLOAT,
BAND                    varchar2(10),
HTMID                   Number(16),
CX                      BINARY_DOUBLE,
CY                      BINARY_DOUBLE,
CZ                      BINARY_DOUBLE,
PARENTID                Number(11),
SOFTID                  Number(4),
IMAGEID                 Number(9),
ZEROPOINT               BINARY_FLOAT,
ERRZEROPOINT            BINARY_FLOAT,
ZEROPOINTID             Number(10),
OBJECT_NUMBER           Number(6),
MAG_AUTO                BINARY_FLOAT,
MAGERR_AUTO             BINARY_FLOAT,
MAG_APER_1              BINARY_FLOAT,
MAGERR_APER_1           BINARY_FLOAT,
MAG_APER_2              BINARY_FLOAT,
MAGERR_APER_2           BINARY_FLOAT,
MAG_APER_3              BINARY_FLOAT,
MAGERR_APER_3           BINARY_FLOAT,
MAG_APER_4              BINARY_FLOAT,
MAGERR_APER_4           BINARY_FLOAT,
MAG_APER_5              BINARY_FLOAT,
MAGERR_APER_5           BINARY_FLOAT,
MAG_APER_6              BINARY_FLOAT,
MAGERR_APER_6           BINARY_FLOAT,
ALPHA_J2000             BINARY_DOUBLE,
DELTA_J2000             BINARY_DOUBLE,
ALPHAPEAK_J2000         BINARY_DOUBLE,
DELTAPEAK_J2000         BINARY_DOUBLE,
X2_WORLD                Binary_FLOAT,
ERRX2_WORLD             BINARY_DOUBLE,
Y2_WORLD                Binary_FLOAT,
ERRY2_WORLD             BINARY_DOUBLE,
XY_WORLD                Binary_FLOAT,
ERRXY_WORLD             BINARY_DOUBLE,
THRESHOLD               Binary_FLOAT,
X_IMAGE                 Binary_FLOAT,
Y_IMAGE                 Binary_FLOAT,
XMIN_IMAGE              Binary_FLOAT,
YMIN_IMAGE              Binary_FLOAT,
XMAX_IMAGE              Binary_FLOAT,
YMAX_IMAGE              Binary_FLOAT,
X2_IMAGE                Binary_FLOAT,
ERRX2_IMAGE             Binary_FLOAT,
Y2_IMAGE                Binary_FLOAT,
ERRY2_IMAGE             Binary_FLOAT,
XY_IMAGE                Binary_FLOAT,
ERRXY_IMAGE             Binary_FLOAT,
A_IMAGE                 Binary_FLOAT,
ERRA_IMAGE              Binary_FLOAT,
B_IMAGE                 Binary_FLOAT,
ERRB_IMAGE              Binary_FLOAT,
THETA_IMAGE             Binary_FLOAT,
ERRTHETA_IMAGE          Binary_FLOAT,
ELLIPTICITY             Binary_FLOAT,
CLASS_STAR              Binary_FLOAT,
FLAGS                   Number(3),
FLUX_RADIUS             Binary_FLOAT,
FWHM_WORLD              Binary_FLOAT,
ISOAREA_WORLD           Binary_FLOAT,
THETA_J2000             Binary_FLOAT,
BACKGROUND              Binary_FLOAT, 
PARTKEY			Varchar2(30),
CATALOGID               Number(9),
RA                      Number(8,5),
DEC                     Number(8,5),
CHI2_MODEL 		BINARY_FLOAT,
FLAGS_MODEL 		NUMBER(1),
NITER_MODEL 		NUMBER(5),
FLUX_MODEL 		BINARY_FLOAT,
FLUXERR_MODEL 		BINARY_FLOAT,
MAG_MODEL 		BINARY_FLOAT,
MAGERR_MODEL 		BINARY_FLOAT,
XMODEL_IMAGE 		BINARY_FLOAT,
YMODEL_IMAGE 		BINARY_FLOAT,
XMODEL_WORLD 		BINARY_DOUBLE,
YMODEL_WORLD 		BINARY_DOUBLE,
ALPHAMODEL_SKY 		BINARY_DOUBLE,
DELTAMODEL_SKY 		BINARY_DOUBLE,
ALPHAMODEL_J2000 	BINARY_DOUBLE,
DELTAMODEL_J2000 	BINARY_DOUBLE,
ERRX2MODEL_IMAGE 	BINARY_DOUBLE,
ERRY2MODEL_IMAGE 	BINARY_DOUBLE,
ERRXYMODEL_IMAGE 	BINARY_DOUBLE,
ERRX2MODEL_WORLD 	BINARY_DOUBLE,
ERRY2MODEL_WORLD 	BINARY_DOUBLE,
ERRXYMODEL_WORLD 	BINARY_DOUBLE,
ERRCXXMODEL_IMAGE 	BINARY_FLOAT,
ERRCYYMODEL_IMAGE 	BINARY_FLOAT,
ERRCXYMODEL_IMAGE 	BINARY_FLOAT,
ERRCXXMODEL_WORLD 	BINARY_FLOAT,
ERRCYYMODEL_WORLD 	BINARY_FLOAT,
ERRCXYMODEL_WORLD 	BINARY_FLOAT,
ERRAMODEL_IMAGE 	BINARY_FLOAT,
ERRBMODEL_IMAGE 	BINARY_FLOAT,
ERRTHETAMODEL_IMAGE 	BINARY_FLOAT,
ERRAMODEL_WORLD 	BINARY_FLOAT,
ERRBMODEL_WORLD 	BINARY_FLOAT,
ERRTHETAMODEL_WORLD 	BINARY_FLOAT,
ERRTHETAMODEL_SKY 	BINARY_FLOAT,
ERRTHETAMODEL_J2000 	BINARY_FLOAT,
X2MODEL_IMAGE 		BINARY_DOUBLE,
Y2MODEL_IMAGE 		BINARY_DOUBLE,
XYMODEL_IMAGE 		BINARY_DOUBLE,
E1MODEL_IMAGE 		BINARY_FLOAT,
E2MODEL_IMAGE 		BINARY_FLOAT,
EPS1MODEL_IMAGE 	BINARY_FLOAT,
EPS2MODEL_IMAGE 	BINARY_FLOAT,
FLUX_SPHEROID 		BINARY_FLOAT,
FLUXERR_SPHEROID 	BINARY_FLOAT,
MAG_SPHEROID 		BINARY_FLOAT,
MAGERR_SPHEROID 	BINARY_FLOAT,
SPHEROID_REFF_IMAGE 	BINARY_FLOAT,
SPHEROID_REFFERR_IMAGE 	BINARY_FLOAT,
SPHEROID_REFF_WORLD 	BINARY_FLOAT,
SPHEROID_REFFERR_WORLD 	BINARY_FLOAT,
SPHEROID_ASPECT_IMAGE 	BINARY_FLOAT,
SPHEROID_ASPECTERR_IMAGE 	BINARY_FLOAT,
SPHEROID_ASPECT_WORLD 	BINARY_FLOAT,
SPHEROID_ASPECTERR_WORLD 	BINARY_FLOAT,
SPHEROID_THETA_IMAGE 	BINARY_FLOAT,
SPHEROID_THETAERR_IMAGE BINARY_FLOAT,
SPHEROID_THETA_WORLD 	BINARY_FLOAT,
SPHEROID_THETAERR_WORLD BINARY_FLOAT,
SPHEROID_THETA_SKY 	BINARY_FLOAT,
SPHEROID_THETA_J2000 	BINARY_FLOAT,
FLUX_DISK 		BINARY_FLOAT,
FLUXERR_DISK 		BINARY_FLOAT,
MAG_DISK 		BINARY_FLOAT,
MAGERR_DISK 		BINARY_FLOAT,
DISK_SCALE_IMAGE 	BINARY_FLOAT,
DISK_SCALEERR_IMAGE 	BINARY_FLOAT,
DISK_SCALE_WORLD 	BINARY_FLOAT,
DISK_SCALEERR_WORLD 	BINARY_FLOAT,
DISK_ASPECT_IMAGE 	BINARY_FLOAT,
DISK_ASPECTERR_IMAGE 	BINARY_FLOAT,
DISK_ASPECT_WORLD 	BINARY_FLOAT,
DISK_ASPECTERR_WORLD 	BINARY_FLOAT,
DISK_INCLINATION 	BINARY_FLOAT,
DISK_INCLINATIONERR 	BINARY_FLOAT,
DISK_THETA_IMAGE 	BINARY_FLOAT,
DISK_THETAERR_IMAGE 	BINARY_FLOAT,
DISK_THETA_WORLD 	BINARY_FLOAT,
DISK_THETAERR_WORLD 	BINARY_FLOAT,
DISK_THETA_SKY 		BINARY_FLOAT,
DISK_THETA_J2000 	BINARY_FLOAT,
DR	                NUMBER(10), 
FLUX_PSF 		BINARY_FLOAT,
FLUXERR_PSF 		BINARY_FLOAT,
MAG_PSF 		BINARY_FLOAT,
MAGERR_PSF 		BINARY_FLOAT,
MAG_APER_7 		BINARY_FLOAT,
MAGERR_APER_7 		BINARY_FLOAT,
MAG_APER_8 		BINARY_FLOAT,
MAGERR_APER_8 		BINARY_FLOAT,
MAG_APER_9 		BINARY_FLOAT,
MAGERR_APER_9 		BINARY_FLOAT,
MAG_APER_10 		BINARY_FLOAT,
MAGERR_APER_10 		BINARY_FLOAT,
MAG_APER_11 		BINARY_FLOAT,
MAGERR_APER_11 		BINARY_FLOAT,
MAG_APER_12 		BINARY_FLOAT,
MAGERR_APER_12 		BINARY_FLOAT,
MAG_APER_13 		BINARY_FLOAT,
MAGERR_APER_13 		BINARY_FLOAT,
MAG_APER_14 		BINARY_FLOAT,
MAGERR_APER_14 		BINARY_FLOAT,
MAG_APER_15 		BINARY_FLOAT,
MAGERR_APER_15 		BINARY_FLOAT,
MAG_APER_16 		BINARY_FLOAT,
MAGERR_APER_16 		BINARY_FLOAT,
MAG_APER_17 		BINARY_FLOAT,
MAGERR_APER_17 		BINARY_FLOAT,
ERRAWIN_WORLD		BINARY_FLOAT,
ERRBWIN_WORLD		BINARY_FLOAT,
ERRTHETAWIN_J2000	BINARY_FLOAT,
ALPHAPSF_J2000 binary_double,
ALPHAWIN_J2000 binary_double,
AMODEL_IMAGE binary_float,
AMODEL_WORLD binary_float,
A_WORLD binary_float,
BMODEL_IMAGE binary_float,
BMODEL_WORLD binary_float,
B_WORLD binary_float,
CHI2_PSF binary_float,
CONCENTRATION_MODEL binary_float,
DELTAPSF_J2000 binary_double,
DELTAWIN_J2000 binary_double,
ELLIP1MODEL_IMAGE binary_float,
ELLIP1MODEL_WORLD binary_float,
ELLIP2MODEL_IMAGE binary_float,
ELLIP2MODEL_WORLD binary_float,
ELONGATION binary_float,
ERRAPSF_IMAGE binary_float,
ERRAPSF_WORLD binary_float,
ERRAWIN_IMAGE binary_float,
ERRBPSF_IMAGE binary_float,
ERRBPSF_WORLD binary_float,
ERRBWIN_IMAGE binary_float,
ERRTHETAPSF_IMAGE binary_float,
ERRTHETAPSF_J2000 binary_float,
ERRTHETAWIN_IMAGE binary_float,
FLUX_APER_1 binary_float,
FLUX_APER_2 binary_float,
FLUX_APER_3 binary_float,
FLUX_APER_4 binary_float,
FLUX_APER_5 binary_float,
FLUX_APER_6 binary_float,
FLUX_APER_7 binary_float,
FLUX_APER_8 binary_float,
FLUX_APER_9 binary_float,
FLUX_APER_10 binary_float,
FLUX_APER_11 binary_float,
FLUX_APER_12 binary_float,
FLUX_APER_13 binary_float,
FLUX_APER_14 binary_float,
FLUX_APER_15 binary_float,
FLUX_APER_16 binary_float,
FLUX_APER_17 binary_float,
FLUX_AUTO binary_float,
FLUX_EFF_MODEL binary_float,
FLUXERR_APER_1 binary_float,
FLUXERR_APER_2 binary_float,
FLUXERR_APER_3 binary_float,
FLUXERR_APER_4 binary_float,
FLUXERR_APER_5 binary_float,
FLUXERR_APER_6 binary_float,
FLUXERR_APER_7 binary_float,
FLUXERR_APER_8 binary_float,
FLUXERR_APER_9 binary_float,
FLUXERR_APER_10 binary_float,
FLUXERR_APER_11 binary_float,
FLUXERR_APER_12 binary_float,
FLUXERR_APER_13 binary_float,
FLUXERR_APER_14 binary_float,
FLUXERR_APER_15 binary_float,
FLUXERR_APER_16 binary_float,
FLUXERR_APER_17 binary_float,
FLUXERR_AUTO binary_float,
FLUXERR_ISO binary_float,
FLUXERR_PETRO binary_float,
FLUX_ISO binary_float,
FLUX_MAX binary_float,
FLUX_MAX_MODEL binary_float,
FLUX_MEAN_MODEL binary_float,
FLUX_PETRO binary_float,
ISOAREAF_IMAGE binary_float,
ISOAREAF_WORLD binary_float,
ISOAREA_IMAGE binary_float,
KRON_RADIUS binary_float,
MAGERR_ISO binary_float,
MAGERR_PETRO binary_float,
MAG_ISO binary_float,
MAG_PETRO binary_float,
MU_EFF_MODEL binary_float,
MU_MAX binary_float,
MU_MAX_MODEL binary_float,
MU_MEAN_MODEL binary_float,
MU_THRESHOLD binary_float,
NITER_PSF number(10),
PETRO_RADIUS binary_float,
THETAMODEL_IMAGE binary_float,
THETAMODEL_J2000 binary_float,
XPEAK_IMAGE number(10),
XPSF_IMAGE binary_double,
XWIN_IMAGE binary_double,
YPEAK_IMAGE number(10),
YPSF_IMAGE binary_double,
YWIN_IMAGE binary_double,
DURATION_ANALYSIS binary_float,
FLAGS_WEIGHT VARCHAR2(1),
NLOWWEIGHT_ISO NUMBER,
NLOWDWEIGHT_ISO NUMBER,
SPREAD_MODEL BINARY_FLOAT,
SPREADERR_MODEL BINARY_FLOAT,
Constraint PK_OBJECTS_2009 PRIMARY KEY (OBJECT_ID))
partition by list(partkey)
(partition init_partition values('before2009'));

-- 
-- TABLE: RUN
-- Created by Darren Adams
-- Dora Cai Added 5 columns on 07/28/2009 per Michelle's request 
-- Dora Cai Added 1 columns on 08/26/2009 per Michelle's request (JIRA des-1134) 

CREATE TABLE RUN(
 RUN		 VARCHAR2(255) NOT NULL,
 PROJECT	 VARCHAR2(255) NOT NULL,
 OPERATOR	 VARCHAR2(255) NOT NULL,
 CLASS		 VARCHAR2(255) NOT NULL,
 SUBMIT_CONFIG_TEXT CLOB NOT NULL,
 ALL_CONFIG_TEXT CLOB NOT NULL,
 ARCHIVE_NODE	 VARCHAR2(255) NOT NULL,
 EVENT_TAG	VARCHAR2(255)
 SUBMIT_HOST	VARCHAR2(255) NOT NULL,
 SUBMIT_DIR	VARCHAR2(255) NOT NULL,
 BLOCK_LIST	VARCHAR2(1000) NOT NULL,
 NOTES		VARCHAR2(1000),
 TARGET_RELEASE VARCHAR2(32),
 TARGET_BASEVERSIONS CLOB,
 TARGET_ENV	 CLOB,
 TARGET_BUILD	 CLOB,
 TARGET_INSTALL  CLOB,
 SUBMIT_RELEASE  VARCHAR2(32),
 SUBMIT_BASEVERSIONS CLOB,
 SUBMIT_ENV	 CLOB,
 SUBMIT_BUILD	 CLOB,
 SUBMIT_INSTALL  CLOB,
 SUBMIT_SVN_REVISION VARCHAR2(16),
 TARGET_SVN_REVISION VARCHAR2(16),
 ID		 NUMBER(10),
 submit_node 	varchar2(255),
 run_submit 	date,
 run_start 	date,
 run_end	date,
 status 	number(3),
 run_comment    varchar2(255)
)
;

-- 
-- TABLE: block
-- Dora Cai create the new table on 07/28/2009 per Michelle's request 

CREATE TABLE BLOCK(
BLOCK_ID 	 NUMBER(10),
BLOCK_NAME 	VARCHAR2(255),	 
MODULE_LIST 	VARCHAR2(1000), 	 
TARGET_NODE 	VARCHAR2(255), 	 
TARGET_RELEASE 	VARCHAR2(32), 	 
TARGET_BASEVERSIONS 	CLOB, 	 
TARGET_ENV 	CLOB, 	 
TARGET_BUILD 	CLOB, 	 
TARGET_INSTALL 	CLOB, 	 
TARGET_SVN_REVISION 	VARCHAR2(16), 	 
RUN_ID 	NUMBER(10), 
START_JOBID 	NUMBER(5), 	 
END_JOBID 	NUMBER(5), 	 
BLOCK_SUBMIT 	DATE, 	 
BLOCK_START 	DATE,
BLOCK_END 	 DATE,
STATUS 	NUMBER(3),
CONSTRAINT PK_BLOCK PRIMARY KEY (BLOCK_ID)
)
;
create sequence block_seq cache 10 start with 1 increment by 1;

-- 
-- TABLE: pfwtask
-- Dora Cai create the new table on 07/28/2009 per Michelle's request 

CREATE TABLE PFWTASK(
PFWTASK_ID 	 NUMBER(10),
BLOCK_ID 	NUMBER(10),
PFWTASK_NAME 	VARCHAR2(255),
PFWTASK_TYPE 	NUMBER(2), 
ORCH_ID 	NUMBER(10),
ORCH_START 	DATE, 
ORCH_END 	DATE,
ORCH_EXIT 	NUMBER(3),
EXEC_HOST 	VARCHAR2(255),
JOB_ID 	NUMBER(10),
JOB_START 	DATE,
JOB_END 	DATE,
ELF_START 	DATE,
ELF_END 	DATE,
ELF_EXIT 	NUMBER(3),
WALL_TIME 	NUMBER(7),
QUEUE_WAIT_TIME 	NUMBER(7), 
CONSTRAINT PK_PFWTASK PRIMARY KEY (PFWTASK_ID)
)
;
create sequence pfwtask_seq cache 10 start with 1 increment by 1;

-- 
-- TABLE: data_release
-- Dora Cai create the new table on 08/05/2009 per Tony's request (JIRA DES-1061) 
-- Dora Cai change 3 column's name on 08/07/2009 per Joe's request (JIRA DES-1061) 
-- Dora Cai change on 08/19/2009: description varchar2(100) 
-- Dora Cai change on 08/20/2009: change primary key on code and name
-- Dora Cai change on 08/28/2009: add a new column viewname pre Joe's request 

CREATE TABLE DATA_RELEASE (
CODE NUMBER(10),
RELEASE_DATE DATE,
NAME VARCHAR2(20),
DESCRIPTION VARCHAR2(100),
VIEWNAME VARCHAR2(20),
CONSTRAINT PK_DATA_RELEASE PRIMARY KEY (CODE,NAME,VIEWNAME)
)
;

-- 
-- TABLE: GSN_AUG09_TRUTH 
-- Dora Cai created the new table on 08/11/09 (JIRA DES-1082)
-- Dora Cai 09/09/28 renamed the table to GSN_AUG09_TRUTH, rename the column DC5_TRUTH_ID to GSN_AUG09_TRUTH_ID, rename the primary key, and rename the sequence (JIRA DES-1276)

--
CREATE TABLE GSN_AUG09_TRUTH(
    GSN_AUG09_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    REDSHIFT		BINARY_FLOAT,
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40),
    CONSTRAINT PK_GSN_AUG09_TRUTH PRIMARY KEY (GSN_AUG09_TRUTH_ID)
)
;

create sequence gsn_aug09_truth_seq cache 1000 start with 1 increment by 1;

-- 
-- TABLE: DR001_OBJECTS  
-- 08/14/2009 Dora Cai created. This is the base table for a materialized view
-- 09/03/2009 Dora Cai rename from objects_dr001 to dr001_objects (JIRA DES-1180).
--
create table dr001_objects 
  partition by range(ra)
    (partition ra10 values less than(10) tablespace desdr, 
     partition ra20 values less than(20) tablespace desdr, 
     partition ra30 values less than(30) tablespace desdr, 
     partition ra40 values less than(40) tablespace desdr, 
     partition ra50 values less than(50) tablespace desdr, 
     partition ra60 values less than(60) tablespace desdr, 
     partition ra70 values less than(70) tablespace desdr, 
     partition ra80 values less than(80) tablespace desdr, 
     partition ra90 values less than(90) tablespace desdr, 
     partition ra100 values less than(100) tablespace desdr, 
     partition ra110 values less than(110) tablespace desdr, 
     partition ra120 values less than(120) tablespace desdr, 
     partition ra130 values less than(130) tablespace desdr, 
     partition ra140 values less than(140) tablespace desdr, 
     partition ra150 values less than(150) tablespace desdr, 
     partition ra160 values less than(160) tablespace desdr, 
     partition ra170 values less than(170) tablespace desdr, 
     partition ra180 values less than(180) tablespace desdr, 
     partition ra190 values less than(190) tablespace desdr, 
     partition ra200 values less than(200) tablespace desdr, 
     partition ra210 values less than(210) tablespace desdr, 
     partition ra220 values less than(220) tablespace desdr, 
     partition ra230 values less than(230) tablespace desdr, 
     partition ra240 values less than(240) tablespace desdr, 
     partition ra250 values less than(250) tablespace desdr, 
     partition ra260 values less than(260) tablespace desdr, 
     partition ra270 values less than(270) tablespace desdr, 
     partition ra280 values less than(280) tablespace desdr, 
     partition ra290 values less than(290) tablespace desdr, 
     partition ra300 values less than(300) tablespace desdr, 
     partition ra310 values less than(310) tablespace desdr, 
     partition ra320 values less than(320) tablespace desdr, 
     partition ra330 values less than(330) tablespace desdr, 
     partition ra340 values less than(340) tablespace desdr, 
     partition ra350 values less than(350) tablespace desdr, 
     partition ra360 values less than(360) tablespace desdr, 
     partition rahigh values less than(MAXVALUE) tablespace desdr) 

-- 
-- TABLE: DR_ACCESS 
-- 08/14/2009 Dora Cai created new table for access control on data release
--

CREATE TABLE DR_ACCESS(
    DES_GROUP    VARCHAR2(30)    NOT NULL,
    DRNAME       VARCHAR2(20)    NOT NULL,
    CONSTRAINT PK_DR_ACCESS PRIMARY KEY (DES_GROUP, DRNAME)
)
;

-- 
-- TABLE: dr002_coadd_objects  
-- 08/25/2009 Dora Cai created for materialized view
-- 09/03/2009 Dora Cai rename from coadd_objects_dr002 to dr002_coadd_objects (JIRA DES-1180).
--
create table dr002_coadd_objects 
  partition by range(ra)
    (partition ra10 values less than(10) tablespace desdr, 
     partition ra20 values less than(20) tablespace desdr, 
     partition ra30 values less than(30) tablespace desdr, 
     partition ra40 values less than(40) tablespace desdr, 
     partition ra50 values less than(50) tablespace desdr, 
     partition ra60 values less than(60) tablespace desdr, 
     partition ra70 values less than(70) tablespace desdr, 
     partition ra80 values less than(80) tablespace desdr, 
     partition ra90 values less than(90) tablespace desdr, 
     partition ra100 values less than(100) tablespace desdr, 
     partition ra110 values less than(110) tablespace desdr, 
     partition ra120 values less than(120) tablespace desdr, 
     partition ra130 values less than(130) tablespace desdr, 
     partition ra140 values less than(140) tablespace desdr, 
     partition ra150 values less than(150) tablespace desdr, 
     partition ra160 values less than(160) tablespace desdr, 
     partition ra170 values less than(170) tablespace desdr, 
     partition ra180 values less than(180) tablespace desdr, 
     partition ra190 values less than(190) tablespace desdr, 
     partition ra200 values less than(200) tablespace desdr, 
     partition ra210 values less than(210) tablespace desdr, 
     partition ra220 values less than(220) tablespace desdr, 
     partition ra230 values less than(230) tablespace desdr, 
     partition ra240 values less than(240) tablespace desdr, 
     partition ra250 values less than(250) tablespace desdr, 
     partition ra260 values less than(260) tablespace desdr, 
     partition ra270 values less than(270) tablespace desdr, 
     partition ra280 values less than(280) tablespace desdr, 
     partition ra290 values less than(290) tablespace desdr, 
     partition ra300 values less than(300) tablespace desdr, 
     partition ra310 values less than(310) tablespace desdr, 
     partition ra320 values less than(320) tablespace desdr, 
     partition ra330 values less than(330) tablespace desdr, 
     partition ra340 values less than(340) tablespace desdr, 
     partition ra350 values less than(350) tablespace desdr, 
     partition ra360 values less than(360) tablespace desdr, 
     partition rahigh values less than(MAXVALUE) tablespace desdr) 
as
select loc.project, loc.run, 
loc.band, loc.filetype, loc.tilename, 
cdd.id coadd_id, cdd.imagename, cdd.radecequin, 
cdd.wcstype, cdd.wcsdim, 
cdd.ctype1, cdd.ctype2,
cdd.cunit1, cdd.cunit2,
cdd.crval1, cdd.crval2,
cdd.crpix1, cdd.crpix2,
cdd.cd1_1, cdd.cd2_1,
cdd.cd1_2, cdd.cd2_2,
cdd.naxis1, cdd.naxis2,
cdd.nextend, cdd.fwhm,
cdd.ellipticity, cdd.coadd_type,
cob.coadd_objects_id, cob.equinox, cob.softid,
cob.imageid_g, cob.imageid_r, cob.imageid_i, 
cob.imageid_z, cob.imageid_y, 
cob.zeropoint_g, cob.zeropoint_r, cob.zeropoint_i, 
cob.zeropoint_z, cob.zeropoint_y, 
cob.errzeropoint_g, cob.errzeropoint_r, cob.errzeropoint_i, 
cob.errzeropoint_z, cob.errzeropoint_y, 
cob.zeropointid_g, cob.zeropointid_r, cob.zeropointid_i, 
cob.zeropointid_z, cob.zeropointid_y, 
cob.r_object_number,
cob.mag_auto_g, cob.mag_auto_r, cob.mag_auto_i, 
cob.mag_auto_z, cob.mag_auto_y, 
cob.magerr_auto_g, cob.magerr_auto_r, cob.magerr_auto_i, 
cob.magerr_auto_z, cob.magerr_auto_y, 
cob.mag_aper1_g, cob.mag_aper1_r, cob.mag_aper1_i, 
cob.mag_aper1_z, cob.mag_aper1_y, 
cob.magerr_aper1_g, cob.magerr_aper1_r, cob.magerr_aper1_i, 
cob.magerr_aper1_z, cob.magerr_aper1_y, 
cob.mag_aper2_g, cob.mag_aper2_r, cob.mag_aper2_i, 
cob.mag_aper2_z, cob.mag_aper2_y, 
cob.magerr_aper2_g, cob.magerr_aper2_r, cob.magerr_aper2_i, 
cob.magerr_aper2_z, cob.magerr_aper2_y, 
cob.mag_aper3_g, cob.mag_aper3_r, cob.mag_aper3_i, 
cob.mag_aper3_z, cob.mag_aper3_y, 
cob.magerr_aper3_g, cob.magerr_aper3_r, cob.magerr_aper3_i, 
cob.magerr_aper3_z, cob.magerr_aper3_y, 
cob.mag_aper4_g, cob.mag_aper4_r, cob.mag_aper4_i, 
cob.mag_aper4_z, cob.mag_aper4_y, 
cob.magerr_aper4_g, cob.magerr_aper4_r, cob.magerr_aper4_i, 
cob.magerr_aper4_z, cob.magerr_aper4_y, 
cob.mag_aper5_g, cob.mag_aper5_r, cob.mag_aper5_i, 
cob.mag_aper5_z, cob.mag_aper5_y, 
cob.magerr_aper5_g, cob.magerr_aper5_r, cob.magerr_aper5_i, 
cob.magerr_aper5_z, cob.magerr_aper5_y, 
cob.mag_aper6_g, cob.mag_aper6_r, cob.mag_aper6_i, 
cob.mag_aper6_z, cob.mag_aper6_y, 
cob.magerr_aper6_g, cob.magerr_aper6_r, cob.magerr_aper6_i, 
cob.magerr_aper6_z, cob.magerr_aper6_y, 
cob.alpha_j2000, cob.delta_j2000,
cob.x2_world, cob.errx2_world,
cob.y2_world, cob.erry2_world,
cob.xy_world, cob.threshold,
cob.x_image, cob.y_image,
cob.xmin_image, cob.ymin_image,
cob.xmax_image, cob.ymax_image,
cob.theta_image_g, cob.theta_image_r, cob.theta_image_i, 
cob.theta_image_z, cob.theta_image_y, 
cob.errtheta_image_g, cob.errtheta_image_r, cob.errtheta_image_i, 
cob.errtheta_image_z, cob.errtheta_image_y, 
cob.ellipticity_g, cob.ellipticity_r, cob.ellipticity_i, 
cob.ellipticity_z, cob.ellipticity_y, 
cob.class_star_g, cob.class_star_r, cob.class_star_i, 
cob.class_star_z, cob.class_star_y, 
cob.flags_g, cob.flags_r, cob.flags_i, 
cob.flags_z, cob.flags_y, 
cob.catalogid_g, cob.catalogid_r, cob.catalogid_i, 
cob.catalogid_z, cob.catalogid_y, 
cob.fwhm_world_g, cob.fwhm_world_r, cob.fwhm_world_i, 
cob.fwhm_world_z, cob.fwhm_world_y, 
cob.flux_radius_g, cob.flux_radius_r, cob.flux_radius_i, 
cob.flux_radius_z, cob.flux_radius_y, 
cob.ra, cob.dec
from location loc, coadd cdd, coadd_objects cob
where loc.run = '20090809212530_DES2219-4147'
and   loc.filetype = 'coadd'
and   loc.id = cdd.id
and   (cob.imageid_g = cdd.id
or     cob.imageid_r = cdd.id
or     cob.imageid_i = cdd.id   
or     cob.imageid_z = cdd.id   
or     cob.imageid_y = cdd.id);   

-- 
-- TABLE: dr002_files 
-- 08/25/2009 Dora Cai created for materialized view
-- 09/03/2009 Dora Cai rename from files_dr002 to dr002_files (JIRA DES-1180).
--
create table dr002_files 
tablespace desdr
as
select loc.id, loc.project, loc.run, 
loc.nite, loc.band, loc.filetype, loc.filename, 
loc.filedate, loc.filesize, loc.filesize_gz, loc.filesize_fz,
loc.tilename, loc.ccd, img.exposureid image_exposureid, 
loc.exposurename file_exposure_name, 
img.airmass image_airmass, img.exptime image_exptime, img.device_id, 
img.gaina, img.rdnoisea, img.gainb, img.rdnoiseb, img.equinox, img.wcsdim, 
img.ctype1 image_ctype1, img.cunit1 image_cunit1, 
img.crval1 image_crval1, img.crpix1 image_crpix1, 
img.cd1_1 image_cd1_1, img.cd1_2 image_cd1_2,
img.pv1_0, img.pv1_1, img.pv1_2, img.pv1_3, img.pv1_4, img.pv1_5,
img.pv1_6, img.pv1_7, img.pv1_8, img.pv1_9, img.pv1_10,
img.ctype2 image_ctype2, img.cunit2 image_cunit2, 
img.crval2 image_crval2, img.crpix2 image_crpix2, 
img.cd2_1 image_cd2_1, img.cd2_2 image_cd2_2,
img.pv2_0, img.pv2_1, img.pv2_2, img.pv2_3, img.pv2_4, img.pv2_5,
img.pv2_6, img.pv2_7, img.pv2_8, img.pv2_9, img.pv2_10,
img.skybrite, img.skysigma, img.elliptic, img.fwhm, 
img.scampnum, img.scampchi, img.scampflg, 
img.naxis1 image_naxis1, img.naxis2 image_naxis2, img.nextend image_nextend,
img.ra image_ra, img.dec image_dec, img.parentid image_parentid,
cdd.radecequin, cdd.wcstype, 
cdd.ctype1 coadd_ctype1, cdd.cunit1 coadd_cunit1, 
cdd.crval1 coadd_crval1, cdd.crpix1 coadd_crpix1, 
cdd.cd1_1 coadd_cd1_1, cdd.cd1_2 coadd_cd1_2,
cdd.ctype2 coadd_ctype2, cdd.cunit2 coadd_cunit2, 
cdd.crval2 coadd_crval2, cdd.crpix2 coadd_crpix2, 
cdd.cd2_1 coadd_cd2_1, cdd.cd2_2 coadd_cd2_2,
cdd.ellipticity coadd_ellipticity, cdd.fwhm coadd_fwhm, 
cdd.naxis1 coadd_naxis1, cdd.naxis2 coadd_naxis2, 
cdd.nextend coadd_nextend,
cdd.ra coadd_ra, cdd.dec coadd_dec,
clg.catalogname, clg.catalogtype, 
clg.exposureid catalog_exposureid, clg.objects catalog_objects,
clg.parentid catalog_parentid,
exp.exposurename, exp.exposuretype, exp.telradec,
exp.telequin, exp.ha, exp.zd, exp.airmass exposure_airmass, 
exp.telfocus, exp.object exposure_objects,
exp.observer, exp.propid, exp.detector, 
exp.detsize, exp.telescope, exp.observatory, 
exp.latitude, exp.longitude, exp.altitude,
exp.timesys, exp.date_obs, exp.time_obs, 
exp.mjd_obs, exp.exptime exposure_exptime, exp.expreq,
exp.darktime, exp.epoch, exp.windspd,
exp.winddir, exp.ambtemp, exp.humidity,
exp.pressure, exp.skyvar, exp.fluxvar, 
exp.dimmsee, exp.photflag, exp.imagehwv,
exp.imageswv, exp.naxis1 exposure_naxis1, exp.naxis2 exposure_naxis2,
exp.nextend exposure_nextend, exp.telra, exp.teldec, 
exp.obstype
from location loc, image img, coadd cdd, catalog clg, exposure exp
where loc.run = 'not applicable'
and   loc.id = cdd.id
and   loc.id = img.id
and   loc.id = clg.id
and   loc.id = exp.id;

-- to add wl columns to the materialized view
alter table dr002_files
add (wlname varchar2(100),
     wltype varchar2(20),
     wl_objects number(8),
     wl_imageid number(10),
     wl_catalogid number(9),
     fsexit number(5),
     fsnobj number(6),
     fsnstars number(6),
     mpexit number(5),
     mpnstars number(6),
     ns_psf number(6),
     nf_range number(6),
     nf_edge number(6),
     nf_npix number(6),
     nf_mptmv number(6),
     nf_mpoth number(6),
     nf_psf   number(6),
     msexit   number(5),
     msnobj   number(6),
     ns_gamma number(6),
     ns_nativ number(6),
     ns_rnge1 number(6),
     ns_rnge2 number(6),
     nf_edge1 number(6),
     nf_edge2 number(6),
     nf_npix1 number(6),
     nf_npix2 number(6),
     nf_nativ number(6),
     nf_small number(6),
     nf_mstmv number(6),
     nf_msoth number(6),
     nf_mu    number(6),
     nf_gamma number(6));
     
-- 
-- TABLE: DICTIONARY 
-- 09/14/2009 Dora Cai created new table for pretty-name translation (JIRA DES-1174)
--

CREATE TABLE DICTIONARY(
    COLNAME   VARCHAR2(20)    NOT NULL,
    COLVALUE  VARCHAR2(20)    NOT NULL,
    PRETTYNAME VARCHAR2(30),
    DESCRIPTION VARCHAR2(100),
    CONSTRAINT PK_DICTIONARY PRIMARY KEY (COLNAME, COLVALUE)
); 

-- 
-- TABLE: DC5SIM_AUG09_USNOB 
-- Dora Cai created the new table on 09/17/09 (JIRA DES-1189)
-- Dora Cai 09/09/28 renamed the table to GSNSIM_AUG09_USNOB, rename the column DC5SIM_USNOB_ID to GSNSIM_AUG09_USNOB_ID, rename the primary key, and rename the sequence (JIRA DES-1276)
-- Dora Cai 09/10/29 added two new columns ra_dbl and dec_dbl (JIRA DES-1219)
--
CREATE TABLE DC5SIM_AUG09_USNOB(
    GSNSIM_AUG09_USNOB_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA                  NUMBER(10,7),
    DEC                 NUMBER(10,7),
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    DATAFILE_NAME       VARCHAR2(40),
    RA_DBL		BINARY_DOUBLE,
    DEC_DBL	        BINARY_DOUBLE,
    CONSTRAINT PK_GSNSIM_AUG09_USNOB PRIMARY KEY (GSNSIM_AUG09_USNOB_ID)
)
;
create sequence gsnsim_aug09_usnob_seq cache 1000 start with 1 increment by 1;

-- 
-- TABLE: DC5_TRUTH 
-- Dora Cai created the new table on 09/28/09 (JIRA DES-1276)
--
CREATE TABLE DC5_TRUTH(
    DC5_TRUTH_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    REDSHIFT		BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    MURA                NUMBER(6,2),
    MUDEC               NUMBER(6,2),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40),
    CONSTRAINT PK_DC5_TRUTH PRIMARY KEY (DC5_TRUTH_ID)
)
;
create sequence dc5_truth_seq cache 1000 start with 1 increment by 1;

-- TABLE: DC5SIM_USNOB 
-- Dora Cai created the new table on 09/28/09 (JIRA DES-1276)
--
CREATE TABLE DC5SIM_USNOB(
    DC5SIM_USNOB_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    MURA                NUMBER(6,2),
    MUDEC               NUMBER(6,2),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40),
    CONSTRAINT PK_DC5SIM_USNOB PRIMARY KEY (DC5SIM_USNOB_ID)
)
;
create sequence dc5sim_usnob_seq cache 1000 start with 1 increment by 1;

-- 
-- TABLE: SNVeto 
-- Dora Cai created the new table on 09/29/2009, requested by John Marriner (JIRA DES-1286)
--
CREATE TABLE SNVeto (
SNVetoId number(12),
RA binary_double,
DEC binary_double,
Radius binary_double,
vType number(6),
rmag binary_double,
magVar binary_double,
constraint pk_snveto primary key (SNVetoId));

create sequence snveto_seq cache 100 start with 1 increment by 1;

-- TABLE: DC5SIM_USNOB_OCT11 
-- Dora Cai created the new table on 10/29/09 (JIRA DES-1399)
--
CREATE TABLE DC5SIM_USNOB_OCT11(
    DC5SIM_USNOB_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    MURA                NUMBER(6,2),
    MUDEC               NUMBER(6,2),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40)
)
;

-- TABLE: DC5SIM_USNOB_OCT12 
-- Dora Cai created the new table on 10/29/09 (JIRA DES-1399)
--
CREATE TABLE DC5SIM_USNOB_OCT12(
    DC5SIM_USNOB_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    MURA                NUMBER(6,2),
    MUDEC               NUMBER(6,2),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40)
)
;

-- TABLE: DC5SIM_USNOB_OCT13 
-- Dora Cai created the new table on 10/29/09 (JIRA DES-1399)
--
CREATE TABLE DC5SIM_USNOB_OCT13(
    DC5SIM_USNOB_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    MURA                NUMBER(6,2),
    MUDEC               NUMBER(6,2),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40)
)
;

-- TABLE: DC5SIM_USNOB_OCT14 
-- Dora Cai created the new table on 10/29/09 (JIRA DES-1399)
--
CREATE TABLE DC5SIM_USNOB_OCT14(
    DC5SIM_USNOB_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    MURA                NUMBER(6,2),
    MUDEC               NUMBER(6,2),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40)
)
;

-- TABLE: DC5SIM_USNOB_OCT15 
-- Dora Cai created the new table on 10/29/09 (JIRA DES-1399)
--
CREATE TABLE DC5SIM_USNOB_OCT15(
    DC5SIM_USNOB_ID    NUMBER(11, 0)    NOT NULL,
    STAR_GALAXY_ID  NUMBER(10, 0),
    CLASS               CHAR(1),
    RA_DBL              BINARY_DOUBLE,
    DEC_DBL             BINARY_DOUBLE,
    U_MAG               BINARY_FLOAT,
    G_MAG               BINARY_FLOAT,
    R_MAG               BINARY_FLOAT,
    I_MAG               BINARY_FLOAT,
    Z_MAG               BINARY_FLOAT,
    Y_MAG               BINARY_FLOAT,
    SRA                 NUMBER(6,2),
    SDE                 NUMBER(6,2),
    MURA                NUMBER(6,2),
    MUDEC               NUMBER(6,2),
    RA                  NUMBER(8,5),
    DEC                 NUMBER(8,5),
    DATAFILE_NAME       VARCHAR2(40)
)
;
