create or replace function to_int( p_data in varchar2 ) return number
is
l_number number default 0;
l_bytes number default length(p_data);
begin
 for i in 1 .. l_bytes loop
   l_number := l_number + (ascii(substr(p_data,i,1))-ascii('0')) * 
   power(2,(l_bytes-i));
 end loop;
 return l_number;
end to_int; 
/
