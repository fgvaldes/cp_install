-- This program first get a list of images from the Files table and
-- the range of RA and DEC for the objects in each image.
-- Then this program searches the standard_stars table to get a list
-- of standard objects which are located in the designated area.
-- Using the RA and DEC of each standard objects, this program then 
-- find the nearest (within 1 arc second) observed objects from the Objects 
-- table. 
-- The program has 8 input parameters: image_type, image_name, nite, band,
-- ccd_number, mag_lo, mag_hi, and runiddesc. 
-- The wildcard '%' is allowed for the parameters nite, and runiddesc. 
-- The program generates an output table which has 7 columns: 
-- Standard_star_id, object_id, airmass, exptime, 
-- mag_aper_3, magerr_aper_3, zeropoint.
-- to run the procedure: select * from table(fPSM('remap', '%', 
-- 'scs20060903', 'r', 0, 15.0, 18.0, 'SCS20070316%'));
-- If you want to process all CCDs, you should enter ccd_number=0.
create or replace function fPSM(
image_type_in varchar,
image_name_in varchar,
nite_in varchar,
band_in varchar,
ccd_number_in number,
mag_lo_in float,
mag_hi_in float,
runiddesc_in varchar)
return PMObjTab pipelined
as
pragma autonomous_transaction;

matchobj PMObj := PMObj(null,null,null,null,null,null,null);
vec vector := vector(null,null,null);
image_name_like varchar2(100);
runiddesc_like varchar2(100);
max_ra float;
min_ra float;
max_dec float;
min_dec float;
stds_id number(10);
stds_ra float;
stds_dec float;
imgid number(9);
ccd_num number(2);
band_val varchar2(8);
airmass_val float;
exptime_val float;
dist float;
flag_val number(3);

-- 0.017 arc minute = 1.0 arc second
close_r float := 0.017; 

-- cursor to get a list of images from files and wcsoffset
cursor filecsr is 
  select /*+ INDEX(files files_nite_ccd_idx) */ 
       a.ra, a.dec, a.imageid, a.ccd_number, a.band, a.airmass, a.exptime
  from files a
  where a.imagetype = image_type_in
  and   a.imagename like image_name_like
  and   a.nite = nite_in
  and   a.band = band_in
  and   (a.runiddesc is null or a.runiddesc like runiddesc_like);

-- cursor for retrieving standard objects from U band
cursor u_stdscsr is 
  select /*+ INDEX(standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg
  from standard_stars
  where stdmag_u between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from G band
cursor g_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg
  from standard_stars
  where stdmag_g between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from R band
cursor r_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg
  from standard_stars
  where stdmag_r between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from I band
cursor i_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg
  from standard_stars
  where stdmag_i between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from Z band
cursor z_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg
  from standard_stars
  where stdmag_z between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

begin
  image_name_like := '%' || image_name_in || '%';
  runiddesc_like := '%' || runiddesc_in || '%';
  for filecsr_rec in filecsr
  loop
   -- ccd_number_in = 0 means to process all CCDs
   if ((ccd_number_in = 0) or (filecsr_rec.ccd_number = ccd_number_in)) then
    imgid := filecsr_rec.imageid;
    --dbms_output.put_line('imgid is ' || imgid);
    ccd_num := filecsr_rec.ccd_number;
    band_val := filecsr_rec.band;
    airmass_val := filecsr_rec.airmass;
    exptime_val := filecsr_rec.exptime;
   
    -- for each image, get the max_ra, min_ra, max_dec and min_dec
    select min(alpha_j2000), max(alpha_j2000), min(delta_j2000), 
           max(delta_j2000) 
    into min_ra, max_ra, min_dec, max_dec
    from objects
    where imageid = imgid;
    -- dbms_output.put_line('range is ' || min_ra || ',' || max_ra || ',' || min_dec || ',' || max_dec);
    -- for each standard object, find the nearest matching observed 
    -- object from table Objects. Those objects must come from the designated
    -- images.
    if (band_val = 'u') then
      for stdscsr_rec in u_stdscsr
      loop
        begin
          stds_id := stdscsr_rec.standard_star_id;
          stds_ra := stdscsr_rec.radeg;
          stds_dec := stdscsr_rec.decdeg;
          select a.object_id, b.mag_aper_5, b.magerr_aper_5, b.zeropoint, b.flags, a.distance     
          into matchobj.object_id, matchobj.mag_aper_3, matchobj.magerr_aper_3,
               matchobj.zeropoint, flag_val, dist
          from table(fGetNearestEq_imageid(stds_ra, stds_dec, close_r, imgid)) a,
               objects b
          where a.object_id = b.object_id
          and   a.distance < close_r
          and   b.mag_aper_5 < 99
          and   b.magerr_aper_5 < 0.2 
          and   b.flags <= 2;
          matchobj.standard_star_id := stds_id;
          matchobj.airmass := airmass_val; 
          matchobj.exptime := exptime_val;   
          pipe row (matchobj);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
        end;
      end loop;     
    elsif (band_val = 'g') then
      for stdscsr_rec in g_stdscsr
      loop
        begin
          stds_id := stdscsr_rec.standard_star_id;
          --dbms_output.put_line('stds_id is ' || stds_id);
          stds_ra := stdscsr_rec.radeg;
          --dbms_output.put_line('stds_ra is ' || stds_ra);
          stds_dec := stdscsr_rec.decdeg;
          --dbms_output.put_line('stds_dec is ' || stds_dec);
          select a.object_id, b.mag_aper_5, b.magerr_aper_5, b.zeropoint, b.flags, a.distance     
          into matchobj.object_id, matchobj.mag_aper_3, matchobj.magerr_aper_3,
               matchobj.zeropoint, flag_val, dist
          from table(fGetNearestEq_imageid(stds_ra, stds_dec, close_r, imgid)) a,
               objects b
          where a.object_id = b.object_id
          and   a.distance < close_r
          and   b.mag_aper_5 < 99
          and   b.magerr_aper_5 < 0.2 
          and   b.flags <= 2;
          matchobj.standard_star_id := stds_id;
          matchobj.airmass := airmass_val; 
          matchobj.exptime := exptime_val;   
          pipe row (matchobj);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
        end;
      end loop;     
    elsif (band_val = 'r') then
      for stdscsr_rec in r_stdscsr
      loop
        begin
          stds_id := stdscsr_rec.standard_star_id;
          --dbms_output.put_line('stds_id is ' || stds_id);
          stds_ra := stdscsr_rec.radeg;
          --dbms_output.put_line('stds_ra is ' || stds_ra);
          stds_dec := stdscsr_rec.decdeg;
          --dbms_output.put_line('stds_dec is ' || stds_dec);
          select a.object_id, b.mag_aper_5, b.magerr_aper_5, b.zeropoint, b.flags, a.distance     
          into matchobj.object_id, matchobj.mag_aper_3, matchobj.magerr_aper_3,
               matchobj.zeropoint, flag_val, dist
          from table(fGetNearestEq_imageid(stds_ra, stds_dec, close_r, imgid)) a,
               objects b
          where a.object_id = b.object_id
          and   a.distance < close_r
          and   b.mag_aper_5 < 99
          and   b.magerr_aper_5 < 0.2 
          and   b.flags <= 2;
          matchobj.standard_star_id := stds_id;
          matchobj.airmass := airmass_val; 
          matchobj.exptime := exptime_val;   
          pipe row (matchobj);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
        end;
      end loop;     
    elsif (band_val = 'i') then
      for stdscsr_rec in i_stdscsr
      loop
        begin
          stds_id := stdscsr_rec.standard_star_id;
          stds_ra := stdscsr_rec.radeg;
          stds_dec := stdscsr_rec.decdeg;
          select a.object_id, b.mag_aper_5, b.magerr_aper_5, b.zeropoint, b.flags, a.distance     
          into matchobj.object_id, matchobj.mag_aper_3, matchobj.magerr_aper_3,
               matchobj.zeropoint, flag_val, dist
          from table(fGetNearestEq_imageid(stds_ra, stds_dec, close_r, imgid)) a,
               objects b
          where a.object_id = b.object_id
          and   a.distance < close_r
          and   b.mag_aper_5 < 99
          and   b.magerr_aper_5 < 0.2 
          and   b.flags <= 2;
          matchobj.standard_star_id := stds_id;
          matchobj.airmass := airmass_val; 
          matchobj.exptime := exptime_val;   
          pipe row (matchobj);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
        end;
      end loop;     
    elsif (band_val = 'z') then
      for stdscsr_rec in z_stdscsr
      loop
        begin
          stds_id := stdscsr_rec.standard_star_id;
          stds_ra := stdscsr_rec.radeg;
          stds_dec := stdscsr_rec.decdeg;
          select a.object_id, b.mag_aper_5, b.magerr_aper_5, b.zeropoint, b.flags, a.distance     
          into matchobj.object_id, matchobj.mag_aper_3, matchobj.magerr_aper_3,
               matchobj.zeropoint, flag_val, dist
          from table(fGetNearestEq_imageid(stds_ra, stds_dec, close_r, imgid)) a,
               objects b
          where a.object_id = b.object_id
          and   a.distance < close_r
          and   b.mag_aper_5 < 99
          and   b.magerr_aper_5 < 0.2 
          and   b.flags <= 2;
          matchobj.standard_star_id := stds_id;
          matchobj.airmass := airmass_val; 
          matchobj.exptime := exptime_val;   
          pipe row (matchobj);
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              NULL;
        end;
      end loop;     
    end if;
   end if;
  end loop;
  commit;
  return;
    
end fPSM;
/
