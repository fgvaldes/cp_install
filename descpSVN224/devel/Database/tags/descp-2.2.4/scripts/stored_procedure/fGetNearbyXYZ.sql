-- find the nearby objects to the point specified by coordinates X, Y and Z 
-- run the procedure: select * from table(fGetNearbyXYZ(.9821, .1816, -.0506, 5))
create or replace function fGetNearbyXYZ(
nx float, 
ny float, 
nz float, 
r float) 
return matchedObjTab pipelined 
is
pragma autonomous_transaction;

mobj matchedObj := matchedObj(null,null,null,null,null,null);
r_ float;

cursor matchrc is
  select object_id, htmid, cx, cy, cz, 
         distance
  from table(fGetNearbyXYZpre(nx,ny,nz,r_))
  where distance < r_
  order by distance;

begin
  r_ := r;
  if (r_ < 0.1) then
    r_ := 0.1;
  end if;

  for matchrc_rec in matchrc
  loop
     mobj.object_id := matchrc_rec.object_id;
     mobj.htmid := matchrc_rec.htmid;
     mobj.cx := matchrc_rec.cx;
     mobj.cy := matchrc_rec.cy;
     mobj.cz := matchrc_rec.cz;
     mobj.distance := matchrc_rec.distance;
     pipe row (mobj);
  end loop;
  commit;
  return;
end fGetNearbyXYZ;
/

