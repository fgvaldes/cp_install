-- this stored procedure matches the objects in two images, and returns a list
-- of pairs of object ids which are matched
-- the program has 3 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- the program returns a table which has 2 columns:
-- object_id_1, 
-- object_id_2

create or replace function fGetCoaddCol(
image1 number,
image2 number,
distance_threshold float)
return MatchCoaddIDObjTab pipelined
as
pragma autonomous_transaction;

matchobj MatchCoaddIDObj := MatchCoaddIDObj(null, null);

min_ra1            float;
max_ra1            float;
min_dec1           float;
max_dec1           float;
min_ra2            float;
max_ra2            float;
min_dec2           float;
max_dec2           float;
min_ra             float;
max_ra             float;
min_dec            float;
max_dec            float;
tt            TIMESTAMP;
tt_old            TIMESTAMP;

i number := 0;
j number := 0;
c number := 0;

coadd_objects_id_1 number(11,0);
cx_1               number(10,6);
cy_1               number(10,6);
cz_1               number(10,6);
alpha_j2000_1      number(8,5);
delta_j2000_1      number(8,5);

type list is RECORD (
COADD_OBJECTS_ID_1  NUMBER(11,0),
CX_1  NUMBER(10,6),
CY_1  NUMBER(10,6),
CZ_1  NUMBER(10,6),
ALPHA_J2000_1  NUMBER(8,5),
DELTA_J2000_1  NUMBER(8,5)
);

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;


cursor selectcsr1 is
  select /*+ INDEX (coadd_objects coaddobj_ra_dec_idx) */
        a.coadd_objects_id  coadd_objects_id_1,
        a.cx             cx_1,
        a.cy                cy_1,               a.cz             cz_1,
        a.alpha_j2000       alpha_j2000_1,      a.delta_j2000      delta_j2000_1

        from coadd_objects a
          where (image1 in (a.imageid_g, a.imageid_r, a.imageid_i, a.imageid_z))
          and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
order by a.alpha_j2000;

cursor selectcsr2 is
  select /*+ INDEX (coadd_objects coaddobj_ra_dec_idx) */
        a.coadd_objects_id  coadd_objects_id_1,
        a.cx             cx_1,
        a.cy                cy_1,               a.cz             cz_1,
        a.alpha_j2000       alpha_j2000_1,      a.delta_j2000      delta_j2000_1

        from coadd_objects a
          where (image2 in (a.imageid_g, a.imageid_r, a.imageid_i, a.imageid_z))
          and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
order by a.alpha_j2000;


-- end of setting up sql cursors

begin
  -- find the overlap part of these two images
  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra1, max_ra1, min_dec1, max_dec1
  from coadd_objects
  where image1 in (imageid_g, imageid_r, imageid_i, imageid_z);

  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra2, max_ra2, min_dec2, max_dec2
  from coadd_objects
  where image2 in (imageid_g, imageid_r, imageid_i, imageid_z);

  min_ra := greatest(min_ra1, min_ra2) - (distance_threshold * 0.1);
  max_ra := least(max_ra1, max_ra2) + (distance_threshold * 0.1);
  min_dec := greatest(min_dec1, min_dec2) - (distance_threshold * 0.1);
  max_dec := least(max_dec1, max_dec2) + (distance_threshold * 0.1);

  open selectcsr1;
  open selectcsr2;

  fetch selectcsr1 bulk collect into collect1;
  fetch selectcsr2 bulk collect into collect2;

  dbms_output.put_line('1 has ' || collect1.count || ' elements.');
  dbms_output.put_line('2 has ' || collect2.count || ' elements.');

  for i in 1 .. collect1.count
  loop
    for j in 1 .. collect2.count
    loop

      if( collect2(j).alpha_j2000_1 >= (collect1(i).alpha_j2000_1 - (distance_threshold*0.1))
          and collect2(j).alpha_j2000_1 <= (collect1(i).alpha_j2000_1 + (distance_threshold*0.1))
          and collect2(j).delta_j2000_1 >= (collect1(i).delta_j2000_1 - (distance_threshold*0.1))
          and collect2(j).delta_j2000_1 <= (collect1(i).delta_j2000_1 + (distance_threshold*0.1))
          and (fgetdistancexyz(collect1(i).cx_1,collect1(i).cy_1,collect1(i).cz_1,collect2(j).cx_1,collect2(j).cy_1,collect2(j).cz_1) < distance_threshold)
)     then
--        dbms_output.put_line('HERE(' || c  || '): ' || collect1(i).coadd_objects_id_1);
        c := c + 1;

        matchobj.coadd_objects_id_1 := collect1(i).coadd_objects_id_1;
        matchobj.coadd_objects_id_2 := collect2(j).coadd_objects_id_1;
        pipe row (matchobj);

        exit;
      end if;
    end loop;
  end loop;
  commit;
  return;

end fGetCoaddCol;
/
