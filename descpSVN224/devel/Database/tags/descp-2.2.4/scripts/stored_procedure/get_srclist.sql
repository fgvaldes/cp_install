-- get the list of source images for processing
-- to run the program:
-- select * from table(get_srclist('des20061014'));

create or replace function get_srclist(
nite varchar)
return ImageList pipelined
is
pragma autonomous_transaction;

imgObj img := img(null,null,null);
nite_like varchar2(20) := '%' || nite || '%';
sitename varchar2(80);
sitelist varchar2(12);
pos number(2) := 1;
found number(1) := 0;

cursor crsr is 
  select imageid, imagename, archivesites
  from files
  where nite like nite_like
  and   imagetype = 'src'
  order by imagename;

begin
  for crsr_rec in crsr
  loop
    found := 0;
    pos := 1;
    imgObj.imageid := crsr_rec.imageid;
    imgObj.imagename := crsr_rec.imagename;
    sitelist := crsr_rec.archivesites;
    while (pos <= length(sitelist) and found = 0)  
    loop
      if substr(sitelist,pos,1) = 'Y' then
        select location_name
        into sitename
        from archive_sites
        where location_id = to_char(pos);
        dbms_output.put_line('site name is ' || sitename);
        imgObj.archivesite := sitename;
        pipe row (imgObj);
        found := 1;
      else
        pos := pos+1;
      end if;
    end loop;
  end loop;
  commit;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      null;
end get_srclist;
/
