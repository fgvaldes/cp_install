-- This stored procedure find the matched pairs of coadd objects and 
-- regular objects. 
-- The program has 8 parameters:
-- 1. tile name (the tile from which the coadd objects are generated)
-- 2. project (the project both the coadd objects and regular objects belong to)
-- 3. distance_threshold (the distance tolerance in arcminute betweem two objects)
-- 4. since date (search objects created since that date). The date must be entered in this format 'dd-mon-yy', such as '01-jan-08' for Jan 01, 2008.
-- 5. low RA (low value for RA range)
-- 6. high RA (high value for RA range)
-- 7. low DEC (low value for DEC range)
-- 8. high DEC (high value for DEC range)
-- The program outputs the matched pairs with 5 columns: coadd_objects_id
-- object_id, offset, ra and dec
--  
-- To run the program: select * from table(find_matches_RADEC('BCS2336-5529', 'BCS', 0.033, '01-dec-09', 354.21, 354.22, -55.46, -55.4));

create or replace function find_matches_RADEC(
tileName varchar,
project varchar,
distance_threshold float,
since varchar,
lo_ra float,
hi_ra float,
lo_dec float,
hi_dec float)
return MatchCddSglObjTbl pipelined
as
pragma autonomous_transaction;

matchobj MatchCddSglObj := MatchCddSglObj(null,null,null,null,null);
min_ra float;
max_ra float;
min_dec float;
max_dec float;
dist float;
sigma float;
tile varchar2(50);

i number := 0;
j number := 0;
runid varchar2(50);
tilename_like varchar2(50);
since_date varchar2(20);

type list is RECORD (
OBJECT  NUMBER(11,0),
RA  NUMBER(8,5), 
DEC NUMBER(8,5));

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;

-- get the max(run) for the specified tilename
cursor runcsr is
  select tilename, max(run) run
  from location b
  where b.tilename like tilename_like
  and   b.project = upper(project)
  and   b.filetype = 'coadd'
  group by tilename;

-- get the list of coadd objects 
cursor selectcsr1 is
  select 
        a.coadd_objects_id object,
        a.ra,  
        a.dec
  from coadd_objects a, location b
  where b.tilename like tilename_like
  and   b.run = runid
  and   b.project = upper(project)
  and   b.filetype = 'coadd'
  and   a.ra >= lo_ra
  and   a.ra <= hi_ra
  and   a.dec >= lo_dec
  and   a.dec <= hi_dec
  and   (a.imageid_g = b.id or a.imageid_r = b.id 
  or    a.imageid_i = b.id or a.imageid_z = b.id
  or    a.imageid_y = b.id);

-- get the list of objects
cursor selectcsr2 is
  select 
        a.object_id object,
        a.ra,  
        a.dec
  from objects_current a, location b
  where a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.imageid = b.id
  and   b.project = upper(project)
  and   b.filedate >= to_date(since_date);

begin
  tilename_like := tileName || '%';
  --dbms_output.put_line('tilename_like is ' || tilename_like);
  if (length(since) < 9) then
    since_date := '01-jan-07'; 
  else
    since_date := since;
  end if;

 for runcsr_rec in runcsr
 loop
  runid := runcsr_rec.run;
  --dbms_output.put_line('runid is ' || runid);

  open selectcsr1;
  fetch selectcsr1 bulk collect into collect1;

  for i in 1 .. collect1.count
  loop
  begin
    -- define the RA/DEC search range for objects
    min_ra := collect1(i).ra - (distance_threshold/60);
    max_ra := collect1(i).ra + (distance_threshold/60);
    min_dec := collect1(i).dec - (distance_threshold/60);
    max_dec := collect1(i).dec + (distance_threshold/60);

    --dbms_output.put_line('rectangle is ' || min_ra || ' ' || max_ra || ' ' ||
                         --min_dec || ' ' || max_dec);
    open selectcsr2;

    fetch selectcsr2 bulk collect into collect2;

    for j in 1 .. collect2.count
    loop
      begin
        --dbms_output.put_line('try to match: ' || collect1(i).object || ' with ' || collect2(j).object);
        dist := fgetdistanceEQ(collect1(i).ra,collect1(i).dec,collect2(j).ra,collect2(j).dec);
        if (dist < distance_threshold) then
          matchobj.coadd_objects_id := collect1(i).object;
          matchobj.object_id := collect2(j).object;
          matchobj.offset := dist;
          matchobj.ra := collect1(i).ra;
          matchobj.dec := collect1(i).dec;
          pipe row (matchobj);
          exit;
        end if;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
          WHEN OTHERS THEN
            NULL;
      end;
    end loop;
    close selectcsr2;
    commit;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN OTHERS THEN
        NULL;
  end;
  end loop;
  close selectcsr1;
  commit;
 end loop;

end find_matches_RADEC;
/
