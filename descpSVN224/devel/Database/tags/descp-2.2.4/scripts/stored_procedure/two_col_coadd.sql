create type MatchCoaddIDObj as object
(COADD_OBJECTS_ID_1  NUMBER(11,0),
COADD_OBJECTS_ID_2  NUMBER(11,0));
/

-- a table of MatchCoaddIDObj
create type MatchCoaddIDObjTab as table of MatchCoaddIDObj;
/

