-- find the nearby objects to the point specified by ra and dec
-- this procedure output ra, dec, htmid, cx, cy, cz, distance.
-- this procedure is for des web page.
-- run the procedure: select * from table(fGetNearbyEq_ObjHead(10.48, -2.87, 5))
create or replace function fGetNearbyEq_ObjHead(
ra float,
dec float,
r float)
return objectHeadTab pipelined
is
pragma autonomous_transaction;

objhead objectHead := objectHead(null,null,null,null,null,null,null,null);
vec vector := vector(null,null,null);
r_ float;
d float;
nx float;
ny float;
nz float;

cursor mstfrc is 
  select object_id, htmid, ra, dec,
         cx, cy, cz, distance
  from table(fGetNearbyXYZpre_ObjHead(nx,ny,nz,r_))
  where distance < r_
  order by distance;

begin
    r_ := r;
    if (r_<0.1) then
      r_ := 0.1;
    end if;
    --dbms_output.put_line('r_ in nearbyEQ is ' || r_ );
    vec := fEqToXyz(ra, dec);
    nx := vec(1);
    ny := vec(2);
    nz := vec(3);

    --dbms_output.put_line('xyz in nearbyEQ is ' || nx ||',' || ny ||',' || nz);
    
    for mstfrc_rec in mstfrc
    loop
      objhead.object_id := mstfrc_rec.object_id;
      objhead.htmid := mstfrc_rec.htmid;
      objhead.ra := mstfrc_rec.ra;
      objhead.dec := mstfrc_rec.dec;
      objhead.cx := mstfrc_rec.cx;
      objhead.cy := mstfrc_rec.cy;
      objhead.cz := mstfrc_rec.cz;
      objhead.distance := round(mstfrc_rec.distance,6);
      pipe row (objhead);
    end loop;
    commit;
    return;
end fGetNearbyEq_ObjHead;
/
