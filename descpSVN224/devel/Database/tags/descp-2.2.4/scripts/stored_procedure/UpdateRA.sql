-- calculate ra values in degrees and update the columns in USNOB_CAT1 table

create or replace procedure UpdateRA(
start_obj number, end_obj number) as

obj_id number(11);
ra number(8,5);
--dec number(8,5);
--htmid_val number(16);
--vec vector;
--cx_val number(10,6);
--cy_val number(10,6);
--cz_val number(10,6);
row_cnt number(11);

--cursor obj_cr is 
--  select /*+ INDEX(objects PK_OBJECTS) */
--       object_id, alpha_j2000, delta_j2000
--  from objects
--  where object_id >= start_obj
--  and   object_id <= end_obj;

cursor obj_cr is
  select /*+ INDEX(usnob_cat1 PK_USNOB_CAT1) */
	usnob_cat1_id, ra
   from usnob_cat1
   where usnob_cat1_id >= start_obj
   and   usnob_cat1_id <= end_obj;


begin
 -- dbms_output.put_line ('start_obj is ' || start_obj);
 -- dbms_output.put_line ('end_obj is ' || end_obj);
  row_cnt := 1;
  open obj_cr;
  loop
    fetch obj_cr into obj_id, ra;
    exit when obj_cr%NOTFOUND;

--   select fEqToHtm(ra, dec)
--    into htmid_val
--    from dual;   

--    select fEqToXYZ(ra, dec)
--    into vec
--    from dual;

--    cx_val := vec(1);
--    cy_val := vec(2);
--    cz_val := vec(3);
     
--    update /*+ INDEX(objects PK_OBJECTS) */
--      objects
--    set htmid = htmid_val,
--        cx = cx_val,
--        cy = cy_val,
--        cz = cz_val
---    where object_id = obj_id;

    update /* INDEX(usnob_cat1 PK_USNOB_CAT1) */
    usnob_cat1
    set ra = ra * 15
    where usnob_cat1_id = obj_id;

    row_cnt := row_cnt + 1;
    if (mod(row_cnt,10000) = 0) then
      commit;
 	dbms_output.put_line (row_cnt);
    end if;
  end loop;
  
end UpdateRA;
/
