-- find objects in a circle specified by ra and dec
-- run the procedure: select * from table(fGetObjFromCirc(10.48, -2.87, 0.5))
create or replace function fGetObjFromCirc(
ra float,
dec float,
r float)
return CircleObjTab pipelined
is
pragma autonomous_transaction;

cobj CircleObj := CircleObj(null,null,null,null,null,null);
vec vector := vector(null,null,null);
r_ float;
d float;
nx float;
ny float;
nz float;
level number(2);
shift number(16);
cmd varchar2(200);

cursor matchrc is
  select /*+ ordered USE_NL(b) */
         b.object_id, b.alpha_j2000, b.delta_j2000, b.band, b.imageid, 
         fGetDistanceXYZ(nx,ny,nz,b.cx,b.cy,b.cz) distance
  from table(fHtmCover(cmd,r_)) a, objects b
  where b.htmid >= a.htm_id_start
  and   b.htmid <= a.htm_id_end;

begin
  r_ := r;
  if (r_<0.1) then
    r_ := 0.1;
  end if;
  --dbms_output.put_line('r_ in circleObj is ' || r_ );
  vec := fEqToXyz(ra, dec);
  nx := vec(1);
  ny := vec(2);
  nz := vec(3);

  --dbms_output.put_line('xyz in circleObj is ' || nx ||',' || ny ||',' || nz);

  level := 13 - (floor(log(10,r_)/log(10,2.0)));
  --dbms_output.put_line('level is ' || level);
  if (level < 5) then
    level := 5;
  elsif (level > 13) then
    level := 13;
  end if;
  --dbms_output.put_line('level2 is ' || level);
  shift := power(4, 20-level);
  --dbms_output.put_line('shift is ' || shift);
  cmd := to_char(level) || ' ' || to_char(nx) || ' ' || to_char(ny) || ' ' || 
         to_char(nz) || ' ' || to_char(r);
  --dbms_output.put_line('cmd is ' || cmd);
  for matchrc_rec in matchrc
  loop
    if (matchrc_rec.distance <= r) then
      cobj.object_id := matchrc_rec.object_id;
      cobj.alpha_j2000 := matchrc_rec.alpha_j2000;
      cobj.delta_j2000 := matchrc_rec.delta_j2000;
      cobj.band := matchrc_rec.band;
      cobj.imageid := matchrc_rec.imageid;
      cobj.distance := matchrc_rec.distance;
      pipe row (cobj);
    end if;
  end loop;
  commit;
  return;
end fGetObjFromCirc;
/

