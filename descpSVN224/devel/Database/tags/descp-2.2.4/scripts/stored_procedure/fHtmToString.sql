-- Translate an HTMID to an equivalent string
-- To run the function: select fHtmToString(12683) from dual;
--                      select fHtmToString(fEqToHtm(240.0, 38.0)) from dual;
create or replace function fHtmToString(
htm number) 
return varchar2
is

temp_htm number := htm; -- eat away when parse it
str_htm varchar2(100) := ''; -- string equivalent to htmid
triangle_id number; -- triangle id in 0,1,2,3

begin
  -- loop over the HTM pulling off a triangle until reaching the face id (N, S)
  while (temp_htm > 0)
  loop
    -- reach the face id
    if (temp_htm < 8) then
      if (temp_htm = 3) then -- North sphere
        str_htm := 'N' || str_htm;
      elsif (temp_htm = 2) then -- South sphere
        str_htm := 'S' || str_htm;
      else
        str_htm := to_char(temp_htm) || str_htm;
      end if;
      temp_htm := 0;
    else
      triangle_id := mod(temp_htm, 4);
      str_htm := to_char(triangle_id) || str_htm;
      temp_htm := floor(temp_htm / 4);
    end if;
  end loop;
  return str_htm;

end fHtmToString;
/

