-- compute the coordinate X, Y and Z for the given ra and dec
-- to run the function: select fEqToXyz(11.10, -2.30) from dual;
-- note: VARRAY subscript starts at 1, not 0
create or replace function fEqToXyz(
ra number,
dec number)
return vector as

x float;
y float;
z float;
cd float;
pr float := 3.1415926535897932385E0/180.0;
v vector := vector(null,null,null);

begin
  cd := cos(dec * pr);
  x := cos(ra * pr) * cd;
  y := sin(ra * pr) * cd;
  z := sin(dec * pr);
  v := vector(x,y,z);

  return v;  
  
end fEqToXyz;
/
