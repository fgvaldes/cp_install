-- Stored procedure to filter values during data ingestion

create or replace function object_check_maglength(V_Mag_field  number)
return float is
mag_value float;
begin 
	  if V_Mag_field > 999.999 then
	  	 mag_value := 99;
	  Else
	     mag_value := V_mag_field;
	  End if;
	  return mag_value;
end object_check_maglength;
