-- This program first get a list of images from the Files table and
-- the range of RA and DEC for the objects in each image.
-- Then this program searches the standard_stars table to get a list
-- of standard objects which are located in the designated area.
-- Using the RA and DEC of each standard objects, this program then 
-- find the nearest (within 1 arc second) observed objects from the Objects 
-- table. 
-- The program has 11 input parameters: image_type, image_name, nite, band,
-- ccd_number, mag_lo, mag_hi, runiddesc, project, table-to-seach and standard-table-to-use. 
-- The wildcard '%' is allowed for the parameters image_name,  
-- runiddesc, and project. 
-- The program generates an output table which has 7 columns: 
-- Standard_star_id, object_id, airmass, exptime, 
-- mag_aper_5, magerr_aper_5, zeropoint.
-- to run the procedure: 
-- select * from table(fPhotoStdsMatch('red', '%', '20091005', 'g', 11, 15.0, 18.0, '20091101164416_20091005', 'DES', 'CURRENT', 'STANDARD_STARS_DC5'));
-- If you want to process all CCDs, you should enter ccd_number=0.
-- 10/16/2009 Dora changed alpha_j2000 to alphamodel_j2000, and delta_j2000 to
-- deltamodel_j2000 (JIRA DES-1379)
-- 10/26/2009 Dora changed the select clause in objscsr to: 
-- select b.object_id, nvl(b.alphamodel_j2000, b.alpha_j2000), nvl(b.deltamodel_j2000, b.delta_j2000). Also in the calculation of MIN, MAX. (JIRA DES-1395)
-- 10/27/2009 Dora added a new input parameter for table-to-search. If the value
-- for this parameter is 'CURRENT', the OBJECTS_CURRENT will be searched, 
-- otherwise the view OBJECTS will be searched (JIRA DES-1395).
-- 01/10/2010 Dora added a new input parameter for standard-table-to-use. If 
-- the value for this parameter (not case-sensitive) is 'standard_stars_dc5', 
-- the STANDARD_STARS_DC5 will be used; otherwise, the table STANDARD_STARS 
-- will be used (JIRA DES-1412).
-- 01/14/2010 Dora removed the where-clause on mag_aper_5 and magerr_aper_r. (JIRA DES-1412)
create or replace function fPhotoStdsMatch(
image_type_in varchar,
image_name_in varchar,
nite_in varchar,
band_in varchar,
ccd_number_in number,
mag_lo_in float,
mag_hi_in float,
runiddesc_in varchar,
project_in varchar,
table_in varchar,
standard_in varchar)
return PMObjTab pipelined
as
pragma autonomous_transaction;

matchobj PMObj := PMObj(null,null,null,null,null,null,null);
image_name_like varchar2(100);
runiddesc_like varchar2(100);
project_like varchar2(20);
max_ra float;
min_ra float;
max_dec float;
min_dec float;
imgid number(9);
ccd_num number(2);
band_val varchar2(8);
airmass_val float;
exptime_val float;

type list is RECORD (
  id number(11),
  radeg float,
  decdeg float,
  mag float,
  magerr float,
  zeropoint number(10));

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;

-- 0.017 arc minute = 1.0 arc second
close_r float := 0.017; 

-- cursor to get a list of images from files and wcsoffset
cursor filecsr is 
  select 
       a.ra, a.dec, a.id, a.ccd, a.band, a.airmass, a.exptime
  from image a
  where a.imagetype = image_type_in
  and   a.imagename like image_name_like
  and   a.nite = nite_in
  and   a.band = band_in
  and   (a.run is null or a.run like runiddesc_like)
  and   (a.project is null or a.project like project_like);

-- cursor for retrieving standard objects from U band
cursor u_stdscsr is 
  select /*+ INDEX(standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars
  where stdmag_u between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from G band
cursor g_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars
  where stdmag_g between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from R band
cursor r_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars
  where stdmag_r between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from I band
cursor i_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars
  where stdmag_i between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from Z band
cursor z_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars
  where stdmag_z between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from Y band
cursor y_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars
  where stdmag_y between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving DC5 standard objects from U band
cursor u_stdscsr_dc5 is 
  select 
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars_dc5
  where stdmag_u between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving DC5 standard objects from G band
cursor g_stdscsr_dc5 is 
  select 
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars_dc5
  where stdmag_g between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving DC5 standard objects from R band
cursor r_stdscsr_dc5 is 
  select 
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars_dc5
  where stdmag_r between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving DC5 standard objects from I band
cursor i_stdscsr_dc5 is 
  select 
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars_dc5
  where stdmag_i between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving DC5 standard objects from Z band
cursor z_stdscsr_dc5 is 
  select 
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars_dc5
  where stdmag_z between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving DC5 standard objects from Y band
cursor y_stdscsr_dc5 is 
  select 
       standard_star_id, radeg, decdeg, null, null, null
  from standard_stars_dc5
  where stdmag_y between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving objects of a selected image from the objects view
cursor objscsr is 
  select b.object_id, 
         nvl(b.alphamodel_j2000, b.alpha_j2000), 
         nvl(b.deltamodel_j2000, b.delta_j2000),
         b.mag_aper_5, b.magerr_aper_5, b.zeropoint
  from objects b
  where b.imageid = imgid
  and   b.flags <= 2;

-- cursor for retrieving objects of a selected image from the objects_current only
cursor objscsr_current is 
  select b.object_id, 
         nvl(b.alphamodel_j2000, b.alpha_j2000), 
         nvl(b.deltamodel_j2000, b.delta_j2000),
         b.mag_aper_5, b.magerr_aper_5, b.zeropoint
  from objects_current b
  where b.imageid = imgid
  and   b.flags <= 2;

begin
  image_name_like := '%' || image_name_in || '%';
  runiddesc_like := '%' || runiddesc_in || '%';
  project_like := '%' || project_in || '%';
  for filecsr_rec in filecsr
  loop
   -- ccd_number_in = 0 means to process all CCDs
   if ((ccd_number_in = 0) or (filecsr_rec.ccd = ccd_number_in)) then
    imgid := filecsr_rec.id;
    --dbms_output.put_line('imgid is ' || imgid);
    ccd_num := filecsr_rec.ccd;
    band_val := filecsr_rec.band;
    airmass_val := filecsr_rec.airmass;
    exptime_val := filecsr_rec.exptime;
   
    -- for each image, get the max_ra, min_ra, max_dec and min_dec
    if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
      -- search objects_current only
      select min(nvl(alphamodel_j2000,alpha_j2000)), 
             max(nvl(alphamodel_j2000,alpha_j2000)), 
             min(nvl(deltamodel_j2000,delta_j2000)), 
             max(nvl(deltamodel_j2000,delta_j2000)) 
      into min_ra, max_ra, min_dec, max_dec
      from objects_current
      where imageid = imgid;
    else 
      -- search objects
      select min(nvl(alphamodel_j2000,alpha_j2000)), 
             max(nvl(alphamodel_j2000,alpha_j2000)), 
             min(nvl(deltamodel_j2000,delta_j2000)), 
             max(nvl(deltamodel_j2000,delta_j2000)) 
      into min_ra, max_ra, min_dec, max_dec
      from objects
      where imageid = imgid;
    end if;
    --dbms_output.put_line('range1 is ' || min_ra || ',' || max_ra || ',' || min_dec || ',' || max_dec);
    -- requested by Choong, add 2 arcseconds (2/3600) to RA and DEC boundary.
    min_ra := min_ra - (2/3600);
    max_ra := max_ra + (2/3600);
    min_dec := min_dec - (2/3600);
    max_dec := max_dec + (2/3600);
    --dbms_output.put_line('range2 is ' || min_ra || ',' || max_ra || ',' || min_dec || ',' || max_dec);
    -- for each standard object, find the nearest matching observed 
    -- object from table Objects. Those objects must come from the designated
    -- images.
    if (band_val = 'u') then
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        -- use standard_stars_dc5 as the standard table
        open u_stdscsr_dc5;
      else
        -- use standard_stars as the standard table
        open u_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        -- search objects_current only
        open objscsr_current;
      else 
        -- search objects view
        open objscsr;
      end if;

      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        fetch u_stdscsr_dc5 bulk collect into collect1;
      else
        fetch u_stdscsr bulk collect into collect1;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        fetch objscsr_current bulk collect into collect2;
      else
        fetch objscsr bulk collect into collect2;
      end if;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_5 := collect2(j).mag;
            matchobj.magerr_aper_5 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        close u_stdscsr_dc5;
      else
        close u_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        close objscsr_current;
      else
        close objscsr;
      end if;

    elsif (band_val = 'g') then
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        -- use standard_stars_dc5 as the standard table
        open g_stdscsr_dc5;
      else
        -- use standard_stars as the standard table
        open g_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        open objscsr_current;
      else 
        open objscsr;
      end if;

      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        fetch g_stdscsr_dc5 bulk collect into collect1;
      else
        fetch g_stdscsr bulk collect into collect1;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        fetch objscsr_current bulk collect into collect2;
      else
        fetch objscsr bulk collect into collect2;
      end if;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_5 := collect2(j).mag;
            matchobj.magerr_aper_5 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        close g_stdscsr_dc5;
      else
        close g_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        close objscsr_current;
      else
        close objscsr;
      end if;

    elsif (band_val = 'r') then
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        -- use standard_stars_dc5 as the standard table
        open r_stdscsr_dc5;
      else
        -- use standard_stars as the standard table
        open r_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        open objscsr_current;
      else 
        open objscsr;
      end if;

      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        fetch r_stdscsr_dc5 bulk collect into collect1;
      else
        fetch r_stdscsr bulk collect into collect1;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        fetch objscsr_current bulk collect into collect2;
      else
        fetch objscsr bulk collect into collect2;
      end if;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_5 := collect2(j).mag;
            matchobj.magerr_aper_5 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        close r_stdscsr_dc5;
      else
        close r_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        close objscsr_current;
      else
        close objscsr;
      end if;

    elsif (band_val = 'i') then
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        -- use standard_stars_dc5 as the standard table
        open i_stdscsr_dc5;
      else
        -- use standard_stars as the standard table
        open i_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        open objscsr_current;
      else 
        open objscsr;
      end if;

      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        fetch i_stdscsr_dc5 bulk collect into collect1;
      else
        fetch i_stdscsr bulk collect into collect1;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        fetch objscsr_current bulk collect into collect2;
      else
        fetch objscsr bulk collect into collect2;
      end if;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_5 := collect2(j).mag;
            matchobj.magerr_aper_5 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        close i_stdscsr_dc5;
      else
        close i_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        close objscsr_current;
      else
        close objscsr;
      end if;

    elsif (band_val = 'z') then
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        -- use standard_stars_dc5 as the standard table
        open z_stdscsr_dc5;
      else
        -- use standard_stars as the standard table
        open z_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        open objscsr_current;
      else 
        open objscsr;
      end if;

      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        fetch z_stdscsr_dc5 bulk collect into collect1;
      else
        fetch z_stdscsr bulk collect into collect1;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        fetch objscsr_current bulk collect into collect2;
      else
        fetch objscsr bulk collect into collect2;
      end if;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          --dbms_output.put_line('in the loop, ' || i || ' and ' || j);
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_5 := collect2(j).mag;
            matchobj.magerr_aper_5 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        close z_stdscsr_dc5;
      else
        close z_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        close objscsr_current;
      else
        close objscsr;
      end if;

    elsif (band_val = 'Y') then
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        -- use standard_stars_dc5 as the standard table
        open y_stdscsr_dc5;
      else
        -- use standard_stars as the standard table
        open y_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        open objscsr_current;
      else 
        open objscsr;
      end if;

      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        fetch y_stdscsr_dc5 bulk collect into collect1;
      else
        fetch y_stdscsr bulk collect into collect1;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        fetch objscsr_current bulk collect into collect2;
      else
        fetch objscsr bulk collect into collect2;
      end if;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          --dbms_output.put_line('in the loop, ' || i || ' and ' || j);
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_5 := collect2(j).mag;
            matchobj.magerr_aper_5 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      if (instr(upper(standard_in), 'DC5', 1, 1) >= 1) then
        close y_stdscsr_dc5;
      else
        close y_stdscsr;
      end if;
      if (instr(upper(table_in), 'CURRENT', 1, 1) >= 1) then
        close objscsr_current;
      else
        close objscsr;
      end if;

    end if; -- end of band_val
   end if; -- end of if ccd_number is valid
  end loop; -- end of filecsr 

  commit;
  return;
    
end fPhotoStdsMatch;
/
