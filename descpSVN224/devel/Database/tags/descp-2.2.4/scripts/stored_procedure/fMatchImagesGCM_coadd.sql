-- This program compares the objects in two coadd images and find the 
-- matching pairs based on the location. 
-- The program has 9 input parameters: imageid_1, imageid_2, band,
-- flags, class_star, mag_lo, mag_hi, magerr_hi.
-- The program generates an output table which has 14 columns: 
-- object1, object2, mag1, mag2, magerr1, magerr2, flag1, flag2, 
-- class_star1, class_star2, ra1, ra2, dec1, dec2.
-- In the program, aperture 5 is used.
-- To run the program: select * from table(fmatchimagesgcm_coadd(9572983, 9622150, 'g', 0, 0.55, 15.2, 18.0, 0.05, 0.032));
create or replace function fMatchImagesGCM_coadd(
imageid1 number,
imageid2 number,
band varchar,
flags number,
class_star number,
mag_lo float,
mag_hi float,
magerr float,
distance_threshold float)
return MatchCoaddObjTab pipelined
as
pragma autonomous_transaction;

matchobj MatchCoaddObj := MatchCoaddObj(null,null,null,null,null,null,null,
                                        null,null,null,null,null,null,null);

min_ra1            float;
max_ra1            float;
min_dec1           float;
max_dec1           float;
min_ra2            float;
max_ra2            float;
min_dec2           float;
max_dec2           float;
min_ra             float;
max_ra             float;
min_dec            float;
max_dec            float;

type list is RECORD (
  object number(11),
  mag float,
  magerr float,
  flag number(2),
  class_star float,
  ra float,
  dec float);

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;

-- cursor for imageid1's G band
cursor coadd1_g is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_g,
        a.magerr_aper5_g,
        a.flags_g,
        a.class_star_g,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_g = imageid1
  and   a.flags_g = flags
  and   a.class_star_g > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_g between mag_lo and mag_hi
  and   a.magerr_aper5_g between 0 and magerr;

-- cursor for imageid1's R band
cursor coadd1_r is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_r,
        a.magerr_aper5_r,
        a.flags_r,
        a.class_star_r,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_r = imageid1
  and   a.flags_r = flags
  and   a.class_star_r > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_r between mag_lo and mag_hi
  and   a.magerr_aper5_r between 0 and magerr;

-- cursor for imageid1's I band
cursor coadd1_i is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_i,
        a.magerr_aper5_i,
        a.flags_i,
        a.class_star_i,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_i = imageid1
  and   a.flags_i = flags
  and   a.class_star_i > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_i between mag_lo and mag_hi
  and   a.magerr_aper5_i between 0 and magerr;

-- cursor for imageid1's Z band
cursor coadd1_z is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_z,
        a.magerr_aper5_z,
        a.flags_z,
        a.class_star_z,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_z = imageid1
  and   a.flags_z = flags
  and   a.class_star_z > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_z between mag_lo and mag_hi
  and   a.magerr_aper5_z between 0 and magerr;

-- cursor for imageid1's Y band
cursor coadd1_y is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_y,
        a.magerr_aper5_y,
        a.flags_y,
        a.class_star_y,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_y = imageid1
  and   a.flags_y = flags
  and   a.class_star_y > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_y between mag_lo and mag_hi
  and   a.magerr_aper5_y between 0 and magerr;

-- cursor for imageid2's G band
cursor coadd2_g is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_g,
        a.magerr_aper5_g,
        a.flags_g,
        a.class_star_g,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_g = imageid2
  and   a.flags_g = flags
  and   a.class_star_g > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_g between mag_lo and mag_hi
  and   a.magerr_aper5_g between 0 and magerr;

-- cursor for imageid2's R band
cursor coadd2_r is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_r,
        a.magerr_aper5_r,
        a.flags_r,
        a.class_star_r,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_r = imageid2
  and   a.flags_r = flags
  and   a.class_star_r > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_r between mag_lo and mag_hi
  and   a.magerr_aper5_r between 0 and magerr;

-- cursor for imageid2's I band
cursor coadd2_i is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_i,
        a.magerr_aper5_i,
        a.flags_i,
        a.class_star_i,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_i = imageid2
  and   a.flags_i = flags
  and   a.class_star_i > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_i between mag_lo and mag_hi
  and   a.magerr_aper5_i between 0 and magerr;

-- cursor for imageid2's Z band
cursor coadd2_z is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_z,
        a.magerr_aper5_z,
        a.flags_z,
        a.class_star_z,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_z = imageid2
  and   a.flags_z = flags
  and   a.class_star_z > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_z between mag_lo and mag_hi
  and   a.magerr_aper5_z between 0 and magerr;

-- cursor for imageid2's Y band
cursor coadd2_y is
  select 
        a.coadd_objects_id object,
        a.mag_aper5_y,
        a.magerr_aper5_y,
        a.flags_y,
        a.class_star_y,
        a.ra,  
        a.dec
  from coadd_objects a
  where a.imageid_y = imageid2
  and   a.flags_y = flags
  and   a.class_star_y > class_star
  and   a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.mag_aper5_y between mag_lo and mag_hi
  and   a.magerr_aper5_y between 0 and magerr;

begin
  -- find the overlap part of these two images
  if (lower(band) = 'g') then
    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra1, max_ra1, min_dec1, max_dec1
    from coadd_objects
    where imageid_g = imageid1;

    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra2, max_ra2, min_dec2, max_dec2
    from coadd_objects
    where imageid_g = imageid2;

  elsif (lower(band) = 'r') then
    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra1, max_ra1, min_dec1, max_dec1
    from coadd_objects
    where imageid_r = imageid1;

    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra2, max_ra2, min_dec2, max_dec2
    from coadd_objects
    where imageid_r = imageid2;

  elsif (lower(band) = 'i') then
    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra1, max_ra1, min_dec1, max_dec1
    from coadd_objects
    where imageid_i = imageid1;

    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra2, max_ra2, min_dec2, max_dec2
    from coadd_objects
    where imageid_i = imageid2;

  elsif (lower(band) = 'z') then
    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra1, max_ra1, min_dec1, max_dec1
    from coadd_objects
    where imageid_z = imageid1;

    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra2, max_ra2, min_dec2, max_dec2
    from coadd_objects
    where imageid_z = imageid2;

  elsif (lower(band) = 'y') then
    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra1, max_ra1, min_dec1, max_dec1
    from coadd_objects
    where imageid_y = imageid1;

    select min(ra), max(ra),
         min(dec), max(dec)
    into min_ra2, max_ra2, min_dec2, max_dec2
    from coadd_objects
    where imageid_y = imageid2;
  end if;

  min_ra := greatest(min_ra1, min_ra2) - (distance_threshold / 60);
  max_ra := least(max_ra1, max_ra2) + (distance_threshold / 60);
  min_dec := greatest(min_dec1, min_dec2) - (distance_threshold / 60);
  max_dec := least(max_dec1, max_dec2) + (distance_threshold / 60);

  dbms_output.put_line('ra range: ' || min_ra || ', ' || max_ra);
  dbms_output.put_line('dec range: ' || min_dec || ', ' || max_dec);

  -- open different cursor based on the selected band
  if (lower(band) = 'g') then
    open coadd1_g;
    open coadd2_g;
    fetch coadd1_g bulk collect into collect1; 
    fetch coadd2_g bulk collect into collect2; 
  elsif (lower(band) = 'r') then
    open coadd1_r;
    open coadd2_r;
    fetch coadd1_r bulk collect into collect1; 
    fetch coadd2_r bulk collect into collect2; 
  elsif (lower(band) = 'i') then
    open coadd1_i;
    open coadd2_i;
    fetch coadd1_i bulk collect into collect1; 
    fetch coadd2_i bulk collect into collect2; 
  elsif (lower(band) = 'z') then
    open coadd1_z;
    open coadd2_z;
    fetch coadd1_z bulk collect into collect1; 
    fetch coadd2_z bulk collect into collect2; 
  elsif (lower(band) = 'y') then
    open coadd1_y;
    open coadd2_y;
    fetch coadd1_y bulk collect into collect1; 
    fetch coadd2_y bulk collect into collect2; 
  end if;

  dbms_output.put_line('1 has ' || collect1.count || ' elements.');
  dbms_output.put_line('2 has ' || collect2.count || ' elements.');

  for i in 1 .. collect1.count
  loop
    for j in 1 .. collect2.count
    loop

      if( collect2(j).ra >= (collect1(i).ra - (distance_threshold/60))
          and collect2(j).ra <= (collect1(i).ra + (distance_threshold/60))
          and collect2(j).ra >= (collect1(i).ra - (distance_threshold/60))
          and collect2(j).ra <= (collect1(i).ra + (distance_threshold/60))
          and (fgetdistanceeq(collect1(i).ra,collect1(i).dec,collect2(j).ra,collect2(j).dec) < distance_threshold)
)     then
        dbms_output.put_line('match: ' || collect1(i).object || ', ' || collect2(j).object);

        matchobj.object1 := collect1(i).object;
        matchobj.object2 := collect2(j).object;
        matchobj.mag1 := collect1(i).mag;
        matchobj.mag2 := collect2(j).mag;
        matchobj.magerr1 := collect1(i).magerr;
        matchobj.magerr2 := collect2(j).magerr;
        matchobj.flag1 := collect1(i).flag;
        matchobj.flag2 := collect2(j).flag;
        matchobj.class_star1 := collect1(i).class_star;
        matchobj.class_star2 := collect2(j).class_star;
        matchobj.ra1 := collect1(i).ra;
        matchobj.ra2 := collect2(j).ra;
        matchobj.dec1 := collect1(i).dec;
        matchobj.dec2 := collect2(j).dec;

        pipe row (matchobj);

        exit;
      end if;
    end loop;
  end loop;
  commit;
  return;

end fMatchImagesGCM_coadd;
/
