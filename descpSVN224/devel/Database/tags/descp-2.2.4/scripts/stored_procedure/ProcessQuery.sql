-- This program accepts queries submitted either from the Archive Portal
-- or from the Web Service, then formulate and optimizes the query, and finally 
-- execute the query and return the result by a reference cursor.
-- The program has 4 input parameters: username, select_clause, table_list, 
-- where_clause. 
-- The output of the program is a reference cursor pointing to data set 
-- with N columns. N depends on the input parameter: select_clause.
-- to run the procedure in sqlplus: 
-- var ret refcursor
-- exec :ret := processquery('pipeline', 'id, filetype, nite', 'location, image', 'id=4946847');
-- print ret;
-- or
-- var ret refcursor
-- var username varchar2(32)
-- var select_clause varchar2(500)
-- var table_list varchar2(100)
-- var where_clause varchar2(500)
-- begin
  -- :username := 'pipeline';
  -- :select_clause := 'id, filetype, nite, archivesites, imagename, ra, dec';
  -- :table_list := 'location, image';
  -- :where_clause := 'id=246603';
  -- :ret := processquery(:username, :select_clause, :table_list, :where_clause);
-- end;
-- /
-- print ret;
-- or
-- exec :ret := processquery('pipeline', 'coadd_objects_id, alpha_j2000, delta_j2000, mag_auto_g', 'coadd_objects', 'coadd_objects_id=285');
-- or
-- exec :ret := processquery('pipeline', 'object_id, alpha_j2000, delta_j2000, mag_auto', 'objects', 'object_id=12345');

create or replace function ProcessQuery(
username_in IN varchar,
select_clause_in IN varchar,
table_list_in IN varchar,
where_clause_in IN varchar)
return sys_refcursor
as

result sys_refcursor;

where_clause_full varchar(1000) := '';
grp_list varchar(100) := '''' || 'guest' || '''';
cursor_str varchar(1000) := '';
select_clause varchar(500) := select_clause_in;
from_clause varchar(100) := table_list_in;
where_clause varchar(500) := where_clause_in;
table_cnt number(2) := 0;

cursor grp_csr is
  select des_group
  from des_accounts
  where nvo_user = username_in;

begin
  --dbms_output.put_line('before processing, where_clause is ' || where_clause_in);
  -- need image table to get RA, DEC if query wl table
  if (instr(lower(from_clause), 'wl', 1, 1) >= 1) then
    if (instr(lower(from_clause), 'image', 1, 1) < 1) then
        from_clause := from_clause || ',image';
    end if;
  end if;
  -- location table and meta tables have same column of "id","run",etc, need to add table prefix
  if (instr(lower(from_clause), 'location', 1, 1) >= 1) then
     select_clause := replace(lower(select_clause), 'id', 'location.id');
     select_clause := replace(lower(select_clause), 'run', 'location.run');
     select_clause := replace(lower(select_clause), 'nite', 'location.nite');
     select_clause := replace(lower(select_clause), 'ccd', 'location.ccd');
     select_clause := replace(lower(select_clause), 'band', 'location.band');
     select_clause := replace(lower(select_clause), 'project', 'location.project');
     where_clause := replace(where_clause, 'id', 'location.id');
     where_clause := replace(where_clause, 'ID', 'location.id');
     where_clause := replace(where_clause, 'Id', 'location.id');
     where_clause := replace(where_clause, 'run', 'location.run');
     where_clause := replace(where_clause, 'RUN', 'location.run');
     where_clause := replace(where_clause, 'Run', 'location.run');
     where_clause := replace(where_clause, 'nite', 'location.nite');
     where_clause := replace(where_clause, 'NITE', 'location.nite');
     where_clause := replace(where_clause, 'Nite', 'location.nite');
     where_clause := replace(where_clause, 'ccd', 'location.ccd');
     where_clause := replace(where_clause, 'CCD', 'location.ccd');
     where_clause := replace(where_clause, 'Ccd', 'location.ccd');
     where_clause := replace(where_clause, 'band', 'location.band');
     where_clause := replace(where_clause, 'BAND', 'location.band');
     where_clause := replace(where_clause, 'Band', 'location.band');
     where_clause := replace(where_clause, 'project', 'location.project');
     where_clause := replace(where_clause, 'PROJECT', 'location.project');
     where_clause := replace(where_clause, 'Project', 'location.project');
  end if;
  -- exposure table uses telra and teldec, not ra and dec
  if (instr(lower(from_clause), 'exposure', 1, 1) >= 1) then
     select_clause := replace(lower(select_clause), 'ra', 'exposure.telra');
     select_clause := replace(lower(select_clause), 'dec', 'exposure.teldec');
     where_clause := replace(where_clause, 'ra', 'telra');
     where_clause := replace(where_clause, 'dec', 'teldec');
     where_clause := replace(where_clause, 'Ra', 'telra');
     where_clause := replace(where_clause, 'Dec', 'teldec');
     where_clause := replace(where_clause, 'RA', 'telra');
     where_clause := replace(where_clause, 'DEC', 'teldec');
  end if;

  -- form the list for the from-clause
  for grp in grp_csr
  loop
    -- if the user belongs a group, then remove the default value "guest"
    if (instr(grp_list, 'guest', 1, 1) >= 1) then
      grp_list := '';
    end if;
    --dbms_output.put_line('grp is ' || grp.des_group);
    grp_list := grp_list || '''' || grp.des_group || ''',';
  end loop;

  -- dbms_output.put_line('grp_list is ' || grp_list);
  -- remove the last ',', if grp_list is not "guest"
  if (instr(grp_list, 'guest', 1, 1) < 1) then
    grp_list := substr(grp_list, 1, length(grp_list)-1);
  end if;

  -- if not only location table is in the from_clause (e.g. location, image)
  table_cnt := fCountRepeatChar(',', from_clause) + 1; 
  dbms_output.put_line('table_cnt is ' || table_cnt);
  -- location and one of the meta table, but not objects tables, are selected
  if (table_cnt = 1) then
    dbms_output.put_line('where_clause_full 1 is ' || where_clause_full);
    -- coadd images use run-id for access control
    if (instr(lower(from_clause), 'coadd', 1, 1) >= 1) then    
      where_clause_full := where_clause_full || 
        'run in (select x.runiddesc from runiddesc_access x where x.des_group in (' || grp_list || ')) ';
    -- even coadd table is not in the query, the image may still be coadd images
    -- which have to use runiddesc for data access control
    else -- coadd table is not in the query
      where_clause_full := where_clause_full || 
        '((run in (select x.runiddesc from runiddesc_access x where x.des_group in (' || grp_list || '))) or ';
      where_clause_full := where_clause_full || 
        '(nite in (select x.nite from nite_access x where x.des_group in (' || 
        grp_list || ') and project = x.project) ' || 
        ' and filetype in (select y.imagetype from imagetype_access y where y.des_group in (' || 
        grp_list || ')))) ';
    end if;
    where_clause_full := where_clause_full || ' and ' || where_clause;
    --dbms_output.put_line('where_clause_full is ' || where_clause_full);
    cursor_str := 'select ' || select_clause || ' from ' || from_clause 
      || ' where '  || where_clause_full;
    dbms_output.put_line('cursor_str is ' || cursor_str);
    open result for cursor_str;

  -- table_cnt > 1
  elsif (instr(lower(from_clause), 'location', 1, 1) >= 1 and
      instr(lower(from_clause), 'objects', 1, 1) < 1 and 
      table_cnt > 1) then    
    where_clause_full := where_clause_full || '(';
    if (instr(lower(from_clause), 'wl', 1, 1) >= 1) then    
      where_clause_full := where_clause_full || 
      'location.id = wl.id and wl.imageid = image.id';
    elsif (instr(lower(from_clause), 'image', 1, 1) >= 1) then    
      where_clause_full := where_clause_full || 
      'location.id = image.id ';
    elsif (instr(lower(from_clause), 'exposure', 1, 1) >= 1) then    
      where_clause_full := where_clause_full || 
      'location.id = exposure.id ';
    elsif (instr(lower(from_clause), 'catalog', 1, 1) >= 1) then    
      where_clause_full := where_clause_full || 
      'location.id = catalog.id ';
    elsif (instr(lower(from_clause), 'coadd', 1, 1) >= 1) then    
      where_clause_full := where_clause_full || 
      'location.id = coadd.id ';
    end if;
    where_clause_full := where_clause_full || ') and ';

    -- coadd images use run-id for access control
    if (instr(lower(from_clause), 'coadd', 1, 1) >= 1) then    
      where_clause_full := where_clause_full || 
        'location.run in (select x.runiddesc from runiddesc_access x where x.des_group in (' || grp_list || ')) ';
    -- even coadd table is not in the query, the image may still be coadd images
    -- which have to use runiddesc for data access control
    else -- coadd table is not in the query
      where_clause_full := where_clause_full || 
        '((location.run in (select x.runiddesc from runiddesc_access x where x.des_group in (' || grp_list || '))) or ';
      where_clause_full := where_clause_full || 
        '(location.nite in (select x.nite from nite_access x where x.des_group in (' || 
        grp_list || ') and location.project = x.project) ' || 
        ' and location.filetype in (select y.imagetype from imagetype_access y where y.des_group in (' || 
        grp_list || ')))) ';
    end if;
    where_clause_full := where_clause_full || ' and ' || where_clause;
    --dbms_output.put_line('where_clause_full is ' || where_clause_full);
    cursor_str := 'select ' || select_clause || ' from ' || from_clause 
      || ' where '  || where_clause_full;
    dbms_output.put_line('cursor_str is ' || cursor_str);
    open result for cursor_str;

  elsif (instr(lower(from_clause), 'coadd_objects', 1, 1) >= 1) then    
    where_clause_full := where_clause_full || 
      'exists (select runiddesc_access.runiddesc from runiddesc_access, location ' ||
      'where runiddesc_access.des_group in (' || grp_list || ') and ' ||
      '(location.id = coadd_objects.imageid_g or ' ||
      'location.id = coadd_objects.imageid_r or ' ||
      'location.id = coadd_objects.imageid_i or ' ||
      'location.id = coadd_objects.imageid_z) and ' ||
      'location.run = runiddesc_access.runiddesc)';
    where_clause_full := where_clause_full || ' and ' || where_clause;

    --dbms_output.put_line('where_clause_full for coadd_objects is ' || where_clause_full);
    cursor_str := 'select ' || select_clause || ' from ' || from_clause 
      || ' where '  || where_clause_full;
    dbms_output.put_line('cursor_str is ' || cursor_str);
    open result for cursor_str;

  elsif (instr(lower(from_clause), 'objects', 1, 1) >= 1) then    
    where_clause_full := where_clause_full || 
      'exists (select nite_access.nite from nite_access, location ' ||
      'where nite_access.des_group in (' || grp_list || ') and ' ||
      'location.id = objects.imageid and ' ||
      'location.nite = nite_access.nite and ' ||
      'location.project = nite_access.project) and ' ||
      'exists (select imagetype_access.imagetype from imagetype_access, location ' ||
      'where imagetype_access.des_group in (' || grp_list || ') and ' ||
      'location.id = objects.imageid and ' ||
      'location.filetype = imagetype_access.imagetype)';
    where_clause_full := where_clause_full || ' and ' || where_clause;
    --dbms_output.put_line('where_clause_full for objects is ' || where_clause_full);
    cursor_str := 'select ' || select_clause || ' from ' || from_clause 
      || ' where '  || where_clause_full;
    dbms_output.put_line('cursor_str is ' || cursor_str);
    open result for cursor_str;
  end if;

  return result;  

end ProcessQuery;
/
