-- find the nearest object to the point specified by ra and dec
-- run the procedure: select * from table(fGetNearestEq(10.47, -2.9, 5, 127918))
create or replace function fGetNearestEq_imageid(
ra float,
dec float,
r float,
imgid number)
return MatchedObjTab pipelined
is
pragma autonomous_transaction;

matchobj matchedObj := matchedObj(null,null,null,null,null,null);
vec vector := vector(null,null,null);
r_ float;
d float;
nx float;
ny float;
nz float;

cursor mstfrc is 
  select a.object_id, a.htmid,
         a.cx, a.cy, a.cz, a.imageid, 
         fGetDistanceXYZ(nx,ny,nz,cx,cy,cz) distance
  from objects a
  where a.imageid = imgid
  and   a.alpha_j2000 >= ra - r
  and   a.alpha_j2000 <= ra + r
  and   a.delta_j2000 >= dec - r
  and   a.delta_j2000 <= dec + r 
  order by distance;

begin
    vec := fEqToXyz(ra, dec);
    nx := vec(1);
    ny := vec(2);
    nz := vec(3);

    --dbms_output.put_line('xyz is ' || nx ||',' || ny ||',' || nz);
    
    for mstfrc_rec in mstfrc
    loop
      if (mstfrc_rec.distance < r) then
      -- only output the nearest object, this loop only runs once
        matchobj.object_id := mstfrc_rec.object_id;
        matchobj.htmid := mstfrc_rec.htmid;
        matchobj.cx := mstfrc_rec.cx;
        matchobj.cy := mstfrc_rec.cy;
        matchobj.cz := mstfrc_rec.cz;
        matchobj.distance := mstfrc_rec.distance;
        pipe row (matchobj);
        return;
      end if;
    end loop;
end fGetNearestEq_imageid;
/
