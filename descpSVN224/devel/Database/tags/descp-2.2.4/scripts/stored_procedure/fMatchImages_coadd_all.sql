create or replace function fMatchImages_coadd_all(
image1 number,
image2 number,
distance_threshold float)
return MatchCoaddImgObjTab pipelined
as
pragma autonomous_transaction;

matchobj MatchCoaddImgObj := MatchCoaddImgObj(null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null);

cursor get_matches is
 select
   a.coadd_objects_id  coadd_objects_id_1, a.equinox        equinox_1,
   a.htmid             htmid_1,            a.cx             cx_1,
   a.cy                cy_1,               a.cz             cz_1,
   a.softid            softid_1,
   a.imageid_g         imageid_g_1,        a.imageid_r      imageid_r_1,
   a.imageid_i         imageid_i_1,        a.imageid_z      imageid_z_1,
   a.zeropoint_g       zeropoint_g_1,      a.zeropoint_r    zeropoint_r_1,
   a.zeropoint_i       zeropoint_i_1,      a.zeropoint_z    zeropoint_z_1,
   a.errzeropoint_g    errzeropoint_g_1,   a.errzeropoint_r errzeropoint_r_1,
   a.errzeropoint_i    errzeropoint_i_1,   a.errzeropoint_z errzeropoint_z_1,
   a.zeropointid_g     zeropointid_g_1,    a.zeropointid_r  zeropointid_r_1,
   a.zeropointid_i     zeropointid_i_1,    a.zeropointid_z  zeropointid_z_1,
   a.r_object_number   r_object_number_1,
   a.mag_auto_g        mag_auto_g_1,       a.magerr_auto_g    magerr_auto_g_1,
   a.mag_aper1_g       mag_aper1_g_1,      a.magerr_aper1_g   magerr_aper1_g_1,
   a.mag_aper2_g       mag_aper2_g_1,      a.magerr_aper2_g   magerr_aper2_g_1,
   a.mag_aper3_g       mag_aper3_g_1,      a.magerr_aper3_g   magerr_aper3_g_1,
   a.mag_aper4_g       mag_aper4_g_1,      a.magerr_aper4_g   magerr_aper4_g_1,
   a.mag_aper5_g       mag_aper5_g_1,      a.magerr_aper5_g   magerr_aper5_g_1,
   a.mag_aper6_g       mag_aper6_g_1,      a.magerr_aper6_g   magerr_aper6_g_1,
   a.mag_auto_r        mag_auto_r_1,       a.magerr_auto_r    magerr_auto_r_1,
   a.mag_aper1_r       mag_aper1_r_1,      a.magerr_aper1_r   magerr_aper1_r_1,
   a.mag_aper2_r       mag_aper2_r_1,      a.magerr_aper2_r   magerr_aper2_r_1,
   a.mag_aper3_r       mag_aper3_r_1,      a.magerr_aper3_r   magerr_aper3_r_1,
   a.mag_aper4_r       mag_aper4_r_1,      a.magerr_aper4_r   magerr_aper4_r_1,
   a.mag_aper5_r       mag_aper5_r_1,      a.magerr_aper5_r   magerr_aper5_r_1,
   a.mag_aper6_r       mag_aper6_r_1,      a.magerr_aper6_r   magerr_aper6_r_1,
   a.mag_auto_i        mag_auto_i_1,       a.magerr_auto_i    magerr_auto_i_1,
   a.mag_aper1_i       mag_aper1_i_1,      a.magerr_aper1_i   magerr_aper1_i_1,
   a.mag_aper2_i       mag_aper2_i_1,      a.magerr_aper2_i   magerr_aper2_i_1,
   a.mag_aper3_i       mag_aper3_i_1,      a.magerr_aper3_i   magerr_aper3_i_1,
   a.mag_aper4_i       mag_aper4_i_1,      a.magerr_aper4_i   magerr_aper4_i_1,
   a.mag_aper5_i       mag_aper5_i_1,      a.magerr_aper5_i   magerr_aper5_i_1,
   a.mag_aper6_i       mag_aper6_i_1,      a.magerr_aper6_i   magerr_aper6_i_1,
   a.mag_auto_z        mag_auto_z_1,       a.magerr_auto_z    magerr_auto_z_1,
   a.mag_aper1_z       mag_aper1_z_1,      a.magerr_aper1_z   magerr_aper1_z_1,
   a.mag_aper2_z       mag_aper2_z_1,      a.magerr_aper2_z   magerr_aper2_z_1,
   a.mag_aper3_z       mag_aper3_z_1,      a.magerr_aper3_z   magerr_aper3_z_1,
   a.mag_aper4_z       mag_aper4_z_1,      a.magerr_aper4_z   magerr_aper4_z_1,
   a.mag_aper5_z       mag_aper5_z_1,      a.magerr_aper5_z   magerr_aper5_z_1,
   a.mag_aper6_z       mag_aper6_z_1,      a.magerr_aper6_z   magerr_aper6_z_1,
   a.alpha_j2000       alpha_j2000_1,      a.delta_j2000      delta_j2000_1,
   a.x2_world          x2_world_1,         a.y2_world         y2_world_1,
   a.xy_world          xy_world_1,
   a.threshold         threshold_1,        a.x_image          x_image_1,
   a.y_image           y_image_1,
   a.theta_image_g     theta_image_g_1,    a.errtheta_image_g errtheta_image_g_1,
   a.ellipticity_g     ellipticity_g_1,    a.class_star_g     class_star_g_1,
   a.flags_g           flags_g_1,          a.theta_image_r    theta_image_r_1,
   a.class_star_r      class_star_r_1,     a.flags_r          flags_r_1,
   a.theta_image_i     theta_image_i_1,    a.errtheta_image_i errtheta_image_i_1,
   a.ellipticity_i     ellipticity_i_1,    a.class_star_i     class_star_i_1,
   a.flags_i           flags_i_1,          a.theta_image_z    theta_image_z_1,
   a.class_star_z      class_star_z_1,     a.flags_z          flags_z_1,
   a.errtheta_image_z  errtheta_image_z_1, a.ellipticity_z    ellipticity_z_1,
   a.errtheta_image_r  errtheta_image_r_1, a.ellipticity_r    ellipticity_r_1,
 
   b.coadd_objects_id  coadd_objects_id_2, b.equinox        equinox_2,
   b.htmid             htmid_2,            b.cx             cx_2,
   b.cy                cy_2,               b.cz             cz_2,
   b.softid            softid_2,
   b.imageid_g         imageid_g_2,        b.imageid_r      imageid_r_2,
   b.imageid_i         imageid_i_2,        b.imageid_z      imageid_z_2,
   b.zeropoint_g       zeropoint_g_2,      b.zeropoint_r    zeropoint_r_2,
   b.zeropoint_i       zeropoint_i_2,      b.zeropoint_z    zeropoint_z_2,
   b.errzeropoint_g    errzeropoint_g_2,   b.errzeropoint_r errzeropoint_r_2,
   b.errzeropoint_i    errzeropoint_i_2,   b.errzeropoint_z errzeropoint_z_2,
   b.zeropointid_g     zeropointid_g_2,    b.zeropointid_r  zeropointid_r_2,
   b.zeropointid_i     zeropointid_i_2,    b.zeropointid_z  zeropointid_z_2,
   b.r_object_number   r_object_number_2,
   b.mag_auto_g        mag_auto_g_2,       b.magerr_auto_g    magerr_auto_g_2,
   b.mag_aper1_g       mag_aper1_g_2,      b.magerr_aper1_g   magerr_aper1_g_2,
   b.mag_aper2_g       mag_aper2_g_2,      b.magerr_aper2_g   magerr_aper2_g_2,
   b.mag_aper3_g       mag_aper3_g_2,      b.magerr_aper3_g   magerr_aper3_g_2,
   b.mag_aper4_g       mag_aper4_g_2,      b.magerr_aper4_g   magerr_aper4_g_2,
   b.mag_aper5_g       mag_aper5_g_2,      b.magerr_aper5_g   magerr_aper5_g_2,
   b.mag_aper6_g       mag_aper6_g_2,      b.magerr_aper6_g   magerr_aper6_g_2,
   b.mag_auto_r        mag_auto_r_2,       b.magerr_auto_r    magerr_auto_r_2,
   b.mag_aper1_r       mag_aper1_r_2,      b.magerr_aper1_r   magerr_aper1_r_2,
   b.mag_aper2_r       mag_aper2_r_2,      b.magerr_aper2_r   magerr_aper2_r_2,
   b.mag_aper3_r       mag_aper3_r_2,      b.magerr_aper3_r   magerr_aper3_r_2,
   b.mag_aper4_r       mag_aper4_r_2,      b.magerr_aper4_r   magerr_aper4_r_2,
   b.mag_aper5_r       mag_aper5_r_2,      b.magerr_aper5_r   magerr_aper5_r_2,
   b.mag_aper6_r       mag_aper6_r_2,      b.magerr_aper6_r   magerr_aper6_r_2,
   b.mag_auto_i        mag_auto_i_2,       b.magerr_auto_i    magerr_auto_i_2,
   b.mag_aper1_i       mag_aper1_i_2,      b.magerr_aper1_i   magerr_aper1_i_2,
   b.mag_aper2_i       mag_aper2_i_2,      b.magerr_aper2_i   magerr_aper2_i_2,
   b.mag_aper3_i       mag_aper3_i_2,      b.magerr_aper3_i   magerr_aper3_i_2,
   b.mag_aper4_i       mag_aper4_i_2,      b.magerr_aper4_i   magerr_aper4_i_2,
   b.mag_aper5_i       mag_aper5_i_2,      b.magerr_aper5_i   magerr_aper5_i_2,
   b.mag_aper6_i       mag_aper6_i_2,      b.magerr_aper6_i   magerr_aper6_i_2,
   b.mag_auto_z        mag_auto_z_2,       b.magerr_auto_z    magerr_auto_z_2,
   b.mag_aper1_z       mag_aper1_z_2,      b.magerr_aper1_z   magerr_aper1_z_2,
   b.mag_aper2_z       mag_aper2_z_2,      b.magerr_aper2_z   magerr_aper2_z_2,
   b.mag_aper3_z       mag_aper3_z_2,      b.magerr_aper3_z   magerr_aper3_z_2,
   b.mag_aper4_z       mag_aper4_z_2,      b.magerr_aper4_z   magerr_aper4_z_2,
   b.mag_aper5_z       mag_aper5_z_2,      b.magerr_aper5_z   magerr_aper5_z_2,
   b.mag_aper6_z       mag_aper6_z_2,      b.magerr_aper6_z   magerr_aper6_z_2,
   b.alpha_j2000       alpha_j2000_2,      b.delta_j2000      delta_j2000_2,
   b.x2_world          x2_world_2,         b.y2_world         y2_world_2,
   b.xy_world          xy_world_2,
   b.threshold         threshold_2,        b.x_image          x_image_2,
   b.y_image           y_image_2,
   b.theta_image_g     theta_image_g_2,    b.errtheta_image_g errtheta_image_g_2,
   b.ellipticity_g     ellipticity_g_2,    b.class_star_g     class_star_g_2,
   b.flags_g           flags_g_2,          b.theta_image_r    theta_image_r_2,
   b.class_star_r      class_star_r_2,     b.flags_r          flags_r_2,
   b.theta_image_i     theta_image_i_2,    b.errtheta_image_i errtheta_image_i_2,
   b.ellipticity_i     ellipticity_i_2,    b.class_star_i     class_star_i_2,
   b.flags_i           flags_i_2,          b.theta_image_z    theta_image_z_2,
   b.class_star_z      class_star_z_2,     b.flags_z          flags_z_2,
   b.errtheta_image_z  errtheta_image_z_2, b.ellipticity_z    ellipticity_z_2,
   b.errtheta_image_r  errtheta_image_r_2, b.ellipticity_r    ellipticity_r_2

    from coadd_objects a,
         coadd_objects b,
         table(testcol(1645779, 1645783, 0.032)) c
   where a.coadd_objects_id = c.coadd_objects_id_1 and
         b.coadd_objects_id = c.coadd_objects_id_2;

begin
  for matchcsr_rec in get_matches
  loop
    matchobj.coadd_objects_id_1 := matchcsr_rec.coadd_objects_id_1;
    matchobj.equinox_1          := matchcsr_rec.equinox_1;
    matchobj.htmid_1            := matchcsr_rec.htmid_1;
    matchobj.cx_1               := matchcsr_rec.cx_1;
    matchobj.cy_1               := matchcsr_rec.cy_1;
    matchobj.cz_1               := matchcsr_rec.cz_1;
    matchobj.softid_1           := matchcsr_rec.softid_1;
    matchobj.imageid_g_1        := matchcsr_rec.imageid_g_1;
    matchobj.imageid_r_1        := matchcsr_rec.imageid_r_1;
    matchobj.imageid_i_1        := matchcsr_rec.imageid_i_1;
    matchobj.imageid_z_1        := matchcsr_rec.imageid_z_1;
    matchobj.zeropoint_g_1      := matchcsr_rec.zeropoint_g_1;
    matchobj.zeropoint_r_1      := matchcsr_rec.zeropoint_r_1;
    matchobj.zeropoint_i_1      := matchcsr_rec.zeropoint_i_1;
    matchobj.zeropoint_z_1      := matchcsr_rec.zeropoint_z_1;
    matchobj.errzeropoint_g_1   := matchcsr_rec.errzeropoint_g_1;
    matchobj.errzeropoint_r_1   := matchcsr_rec.errzeropoint_r_1;
    matchobj.errzeropoint_i_1   := matchcsr_rec.errzeropoint_i_1;
    matchobj.errzeropoint_z_1   := matchcsr_rec.errzeropoint_z_1;
    matchobj.zeropointid_g_1    := matchcsr_rec.zeropointid_g_1;
    matchobj.zeropointid_r_1    := matchcsr_rec.zeropointid_r_1;
    matchobj.zeropointid_i_1    := matchcsr_rec.zeropointid_i_1;
    matchobj.zeropointid_z_1    := matchcsr_rec.zeropointid_z_1;
    matchobj.r_object_number_1  := matchcsr_rec.r_object_number_1;
    matchobj.mag_auto_g_1       := matchcsr_rec.mag_auto_g_1;
    matchobj.magerr_auto_g_1    := matchcsr_rec.magerr_auto_g_1;
    matchobj.mag_aper1_g_1      := matchcsr_rec.mag_aper1_g_1;
    matchobj.magerr_aper1_g_1   := matchcsr_rec.magerr_aper1_g_1;
    matchobj.mag_aper2_g_1      := matchcsr_rec.mag_aper2_g_1;
    matchobj.magerr_aper2_g_1   := matchcsr_rec.magerr_aper2_g_1;
    matchobj.mag_aper3_g_1      := matchcsr_rec.mag_aper3_g_1;
    matchobj.magerr_aper3_g_1   := matchcsr_rec.magerr_aper3_g_1;
    matchobj.mag_aper4_g_1      := matchcsr_rec.mag_aper4_g_1;
    matchobj.magerr_aper4_g_1   := matchcsr_rec.magerr_aper4_g_1;
    matchobj.mag_aper5_g_1      := matchcsr_rec.mag_aper5_g_1;
    matchobj.magerr_aper5_g_1   := matchcsr_rec.magerr_aper5_g_1;
    matchobj.mag_aper6_g_1      := matchcsr_rec.mag_aper6_g_1;
    matchobj.magerr_aper6_g_1   := matchcsr_rec.magerr_aper6_g_1;
    matchobj.mag_auto_r_1       := matchcsr_rec.mag_auto_r_1;
    matchobj.magerr_auto_r_1    := matchcsr_rec.magerr_auto_r_1;
    matchobj.mag_aper1_r_1      := matchcsr_rec.mag_aper1_r_1;
    matchobj.magerr_aper1_r_1   := matchcsr_rec.magerr_aper1_r_1;
    matchobj.mag_aper2_r_1      := matchcsr_rec.mag_aper2_r_1;
    matchobj.magerr_aper2_r_1   := matchcsr_rec.magerr_aper2_r_1;
    matchobj.mag_aper3_r_1      := matchcsr_rec.mag_aper3_r_1;
    matchobj.magerr_aper3_r_1   := matchcsr_rec.magerr_aper3_r_1;
    matchobj.mag_aper4_r_1      := matchcsr_rec.mag_aper4_r_1;
    matchobj.magerr_aper4_r_1   := matchcsr_rec.magerr_aper4_r_1;
    matchobj.mag_aper5_r_1      := matchcsr_rec.mag_aper5_r_1;
    matchobj.magerr_aper5_r_1   := matchcsr_rec.magerr_aper5_r_1;
    matchobj.mag_aper6_r_1      := matchcsr_rec.mag_aper6_r_1;
    matchobj.magerr_aper6_r_1   := matchcsr_rec.magerr_aper6_r_1;
    matchobj.mag_auto_i_1       := matchcsr_rec.mag_auto_i_1;
    matchobj.magerr_auto_i_1    := matchcsr_rec.magerr_auto_i_1;
    matchobj.mag_aper1_i_1      := matchcsr_rec.mag_aper1_i_1;
    matchobj.magerr_aper1_i_1   := matchcsr_rec.magerr_aper1_i_1;
    matchobj.mag_aper2_i_1      := matchcsr_rec.mag_aper2_i_1;
    matchobj.magerr_aper2_i_1   := matchcsr_rec.magerr_aper2_i_1;
    matchobj.mag_aper3_i_1      := matchcsr_rec.mag_aper3_i_1;
    matchobj.magerr_aper3_i_1   := matchcsr_rec.magerr_aper3_i_1;
    matchobj.mag_aper4_i_1      := matchcsr_rec.mag_aper4_i_1;
    matchobj.magerr_aper4_i_1   := matchcsr_rec.magerr_aper4_i_1;
    matchobj.mag_aper5_i_1      := matchcsr_rec.mag_aper5_i_1;
    matchobj.magerr_aper5_i_1   := matchcsr_rec.magerr_aper5_i_1;
    matchobj.mag_aper6_i_1      := matchcsr_rec.mag_aper6_i_1;
    matchobj.magerr_aper6_i_1   := matchcsr_rec.magerr_aper6_i_1;
    matchobj.mag_auto_z_1       := matchcsr_rec.mag_auto_z_1;
    matchobj.magerr_auto_z_1    := matchcsr_rec.magerr_auto_z_1;
    matchobj.mag_aper1_z_1      := matchcsr_rec.mag_aper1_z_1;
    matchobj.magerr_aper1_z_1   := matchcsr_rec.magerr_aper1_z_1;
    matchobj.mag_aper2_z_1      := matchcsr_rec.mag_aper2_z_1;
    matchobj.magerr_aper2_z_1   := matchcsr_rec.magerr_aper2_z_1;
    matchobj.mag_aper3_z_1      := matchcsr_rec.mag_aper3_z_1;
    matchobj.magerr_aper3_z_1   := matchcsr_rec.magerr_aper3_z_1;
    matchobj.mag_aper4_z_1      := matchcsr_rec.mag_aper4_z_1;
    matchobj.magerr_aper4_z_1   := matchcsr_rec.magerr_aper4_z_1;
    matchobj.mag_aper5_z_1      := matchcsr_rec.mag_aper5_z_1;
    matchobj.magerr_aper5_z_1   := matchcsr_rec.magerr_aper5_z_1;
    matchobj.mag_aper6_z_1      := matchcsr_rec.mag_aper6_z_1;
    matchobj.magerr_aper6_z_1   := matchcsr_rec.magerr_aper6_z_1;
    matchobj.alpha_j2000_1      := matchcsr_rec.alpha_j2000_1;
    matchobj.delta_j2000_1      := matchcsr_rec.delta_j2000_1;
    matchobj.x2_world_1         := matchcsr_rec.x2_world_1;
    matchobj.y2_world_1         := matchcsr_rec.y2_world_1;
    matchobj.xy_world_1         := matchcsr_rec.xy_world_1;
    matchobj.threshold_1        := matchcsr_rec.threshold_1;
    matchobj.x_image_1          := matchcsr_rec.x_image_1;
    matchobj.y_image_1          := matchcsr_rec.y_image_1;
    matchobj.theta_image_g_1    := matchcsr_rec.theta_image_g_1;
    matchobj.errtheta_image_g_1 := matchcsr_rec.errtheta_image_g_1;
    matchobj.ellipticity_g_1    := matchcsr_rec.ellipticity_g_1;
    matchobj.class_star_g_1     := matchcsr_rec.class_star_g_1;
    matchobj.flags_g_1          := matchcsr_rec.flags_g_1;
    matchobj.theta_image_r_1    := matchcsr_rec.theta_image_r_1;
    matchobj.errtheta_image_r_1 := matchcsr_rec.errtheta_image_r_1;
    matchobj.ellipticity_r_1    := matchcsr_rec.ellipticity_r_1;
    matchobj.class_star_r_1     := matchcsr_rec.class_star_r_1;
    matchobj.flags_r_1          := matchcsr_rec.flags_r_1;
    matchobj.theta_image_i_1    := matchcsr_rec.theta_image_i_1;
    matchobj.errtheta_image_i_1 := matchcsr_rec.errtheta_image_i_1;
    matchobj.ellipticity_i_1    := matchcsr_rec.ellipticity_i_1;
    matchobj.class_star_i_1     := matchcsr_rec.class_star_i_1;
    matchobj.flags_i_1          := matchcsr_rec.flags_i_1;
    matchobj.theta_image_z_1    := matchcsr_rec.theta_image_z_1;
    matchobj.errtheta_image_z_1 := matchcsr_rec.errtheta_image_z_1;
    matchobj.ellipticity_z_1    := matchcsr_rec.ellipticity_z_1;
    matchobj.class_star_z_1     := matchcsr_rec.class_star_z_1;
    matchobj.flags_z_1          := matchcsr_rec.flags_z_1;

    matchobj.coadd_objects_id_2 := matchcsr_rec.coadd_objects_id_2;
    matchobj.equinox_2          := matchcsr_rec.equinox_2;
    matchobj.htmid_2            := matchcsr_rec.htmid_2;
    matchobj.cx_2               := matchcsr_rec.cx_2;
    matchobj.cy_2               := matchcsr_rec.cy_2;
    matchobj.cz_2               := matchcsr_rec.cz_2;
    matchobj.softid_2           := matchcsr_rec.softid_2;
    matchobj.imageid_g_2        := matchcsr_rec.imageid_g_2;
    matchobj.imageid_r_2        := matchcsr_rec.imageid_r_2;
    matchobj.imageid_i_2        := matchcsr_rec.imageid_i_2;
    matchobj.imageid_z_2        := matchcsr_rec.imageid_z_2;
    matchobj.zeropoint_g_2      := matchcsr_rec.zeropoint_g_2;
    matchobj.zeropoint_r_2      := matchcsr_rec.zeropoint_r_2;
    matchobj.zeropoint_i_2      := matchcsr_rec.zeropoint_i_2;
    matchobj.zeropoint_z_2      := matchcsr_rec.zeropoint_z_2;
    matchobj.errzeropoint_g_2   := matchcsr_rec.errzeropoint_g_2;
    matchobj.errzeropoint_r_2   := matchcsr_rec.errzeropoint_r_2;
    matchobj.errzeropoint_i_2   := matchcsr_rec.errzeropoint_i_2;
    matchobj.errzeropoint_z_2   := matchcsr_rec.errzeropoint_z_2;
    matchobj.zeropointid_g_2    := matchcsr_rec.zeropointid_g_2;
    matchobj.zeropointid_r_2    := matchcsr_rec.zeropointid_r_2;
    matchobj.zeropointid_i_2    := matchcsr_rec.zeropointid_i_2;
    matchobj.zeropointid_z_2    := matchcsr_rec.zeropointid_z_2;
    matchobj.r_object_number_2  := matchcsr_rec.r_object_number_2;
    matchobj.mag_auto_g_2       := matchcsr_rec.mag_auto_g_2;
    matchobj.magerr_auto_g_2    := matchcsr_rec.magerr_auto_g_2;
    matchobj.mag_aper1_g_2      := matchcsr_rec.mag_aper1_g_2;
    matchobj.magerr_aper1_g_2   := matchcsr_rec.magerr_aper1_g_2;
    matchobj.mag_aper2_g_2      := matchcsr_rec.mag_aper2_g_2;
    matchobj.magerr_aper2_g_2   := matchcsr_rec.magerr_aper2_g_2;
    matchobj.mag_aper3_g_2      := matchcsr_rec.mag_aper3_g_2;
    matchobj.magerr_aper3_g_2   := matchcsr_rec.magerr_aper3_g_2;
    matchobj.mag_aper4_g_2      := matchcsr_rec.mag_aper4_g_2;
    matchobj.magerr_aper4_g_2   := matchcsr_rec.magerr_aper4_g_2;
    matchobj.mag_aper5_g_2      := matchcsr_rec.mag_aper5_g_2;
    matchobj.magerr_aper5_g_2   := matchcsr_rec.magerr_aper5_g_2;
    matchobj.mag_aper6_g_2      := matchcsr_rec.mag_aper6_g_2;
    matchobj.magerr_aper6_g_2   := matchcsr_rec.magerr_aper6_g_2;
    matchobj.mag_auto_r_2       := matchcsr_rec.mag_auto_r_2;
    matchobj.magerr_auto_r_2    := matchcsr_rec.magerr_auto_r_2;
    matchobj.mag_aper1_r_2      := matchcsr_rec.mag_aper1_r_2;
    matchobj.magerr_aper1_r_2   := matchcsr_rec.magerr_aper1_r_2;
    matchobj.mag_aper2_r_2      := matchcsr_rec.mag_aper2_r_2;
    matchobj.magerr_aper2_r_2   := matchcsr_rec.magerr_aper2_r_2;
    matchobj.mag_aper3_r_2      := matchcsr_rec.mag_aper3_r_2;
    matchobj.magerr_aper3_r_2   := matchcsr_rec.magerr_aper3_r_2;
    matchobj.mag_aper4_r_2      := matchcsr_rec.mag_aper4_r_2;
    matchobj.magerr_aper4_r_2   := matchcsr_rec.magerr_aper4_r_2;
    matchobj.mag_aper5_r_2      := matchcsr_rec.mag_aper5_r_2;
    matchobj.magerr_aper5_r_2   := matchcsr_rec.magerr_aper5_r_2;
    matchobj.mag_aper6_r_2      := matchcsr_rec.mag_aper6_r_2;
    matchobj.magerr_aper6_r_2   := matchcsr_rec.magerr_aper6_r_2;
    matchobj.mag_auto_i_2       := matchcsr_rec.mag_auto_i_2;
    matchobj.magerr_auto_i_2    := matchcsr_rec.magerr_auto_i_2;
    matchobj.mag_aper1_i_2      := matchcsr_rec.mag_aper1_i_2;
    matchobj.magerr_aper1_i_2   := matchcsr_rec.magerr_aper1_i_2;
    matchobj.mag_aper2_i_2      := matchcsr_rec.mag_aper2_i_2;
    matchobj.magerr_aper2_i_2   := matchcsr_rec.magerr_aper2_i_2;
    matchobj.mag_aper3_i_2      := matchcsr_rec.mag_aper3_i_2;
    matchobj.magerr_aper3_i_2   := matchcsr_rec.magerr_aper3_i_2;
    matchobj.mag_aper4_i_2      := matchcsr_rec.mag_aper4_i_2;
    matchobj.magerr_aper4_i_2   := matchcsr_rec.magerr_aper4_i_2;
    matchobj.mag_aper5_i_2      := matchcsr_rec.mag_aper5_i_2;
    matchobj.magerr_aper5_i_2   := matchcsr_rec.magerr_aper5_i_2;
    matchobj.mag_aper6_i_2      := matchcsr_rec.mag_aper6_i_2;
    matchobj.magerr_aper6_i_2   := matchcsr_rec.magerr_aper6_i_2;
    matchobj.mag_auto_z_2       := matchcsr_rec.mag_auto_z_2;
    matchobj.magerr_auto_z_2    := matchcsr_rec.magerr_auto_z_2;
    matchobj.mag_aper1_z_2      := matchcsr_rec.mag_aper1_z_2;
    matchobj.magerr_aper1_z_2   := matchcsr_rec.magerr_aper1_z_2;
    matchobj.mag_aper2_z_2      := matchcsr_rec.mag_aper2_z_2;
    matchobj.magerr_aper2_z_2   := matchcsr_rec.magerr_aper2_z_2;
    matchobj.mag_aper3_z_2      := matchcsr_rec.mag_aper3_z_2;
    matchobj.magerr_aper3_z_2   := matchcsr_rec.magerr_aper3_z_2;
    matchobj.mag_aper4_z_2      := matchcsr_rec.mag_aper4_z_2;
    matchobj.magerr_aper4_z_2   := matchcsr_rec.magerr_aper4_z_2;
    matchobj.mag_aper5_z_2      := matchcsr_rec.mag_aper5_z_2;
    matchobj.magerr_aper5_z_2   := matchcsr_rec.magerr_aper5_z_2;
    matchobj.mag_aper6_z_2      := matchcsr_rec.mag_aper6_z_2;
    matchobj.magerr_aper6_z_2   := matchcsr_rec.magerr_aper6_z_2;
    matchobj.alpha_j2000_2      := matchcsr_rec.alpha_j2000_2;
    matchobj.delta_j2000_2      := matchcsr_rec.delta_j2000_2;
    matchobj.x2_world_2         := matchcsr_rec.x2_world_2;
    matchobj.y2_world_2         := matchcsr_rec.y2_world_2;
    matchobj.xy_world_2         := matchcsr_rec.xy_world_2;
    matchobj.threshold_2        := matchcsr_rec.threshold_2;
    matchobj.x_image_2          := matchcsr_rec.x_image_2;
    matchobj.y_image_2          := matchcsr_rec.y_image_2;
    matchobj.theta_image_g_2    := matchcsr_rec.theta_image_g_2;
    matchobj.errtheta_image_g_2 := matchcsr_rec.errtheta_image_g_2;
    matchobj.ellipticity_g_2    := matchcsr_rec.ellipticity_g_2;
    matchobj.class_star_g_2     := matchcsr_rec.class_star_g_2;
    matchobj.flags_g_2          := matchcsr_rec.flags_g_2;
    matchobj.theta_image_r_2    := matchcsr_rec.theta_image_r_2;
    matchobj.errtheta_image_r_2 := matchcsr_rec.errtheta_image_r_2;
    matchobj.ellipticity_r_2    := matchcsr_rec.ellipticity_r_2;
    matchobj.class_star_r_2     := matchcsr_rec.class_star_r_2;
    matchobj.flags_r_2          := matchcsr_rec.flags_r_2;
    matchobj.theta_image_i_2    := matchcsr_rec.theta_image_i_2;
    matchobj.errtheta_image_i_2 := matchcsr_rec.errtheta_image_i_2;
    matchobj.ellipticity_i_2    := matchcsr_rec.ellipticity_i_2;
    matchobj.class_star_i_2     := matchcsr_rec.class_star_i_2;
    matchobj.flags_i_2          := matchcsr_rec.flags_i_2;
    matchobj.theta_image_z_2    := matchcsr_rec.theta_image_z_2;
    matchobj.errtheta_image_z_2 := matchcsr_rec.errtheta_image_z_2;
    matchobj.ellipticity_z_2    := matchcsr_rec.ellipticity_z_2;
    matchobj.class_star_z_2     := matchcsr_rec.class_star_z_2;
    matchobj.flags_z_2          := matchcsr_rec.flags_z_2;

    pipe row (matchobj);
  end loop;

commit;
return;
end fMatchImages_coadd_all;
/

