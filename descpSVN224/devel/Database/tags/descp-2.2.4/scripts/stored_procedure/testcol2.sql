-- this stored procedure matches the objects in two images
-- the program has 7 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. stella_lo (minimum value for class_star)
-- 4. stella_hi (maximum value for class_star)
-- 5. flag_upperlim (upperlim for flags)
-- 6. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- 7. magerr_filter (magnitude error filter used: magerr_auto, magerr_aper_1, etc)
-- 8. magerr_limit (magnitude error limit, only the objects with error < magerr_limit will be matched)
-- the program returns a table which has 36 columns:
-- object_id_1, band_1, class_star_1, flags_1, mag_auto_1, magerr_auto_1,
-- mag_aper_1_1, magerr_aper_1_1, mag_aper_2_1, magerr_aper_2_1,
-- mag_aper_3_1, magerr_aper_3_1, mag_aper_4_1, magerr_aper_4_1,
-- mag_aper_5_1, magerr_aper_5_1, mag_aper_6_1, magerr_aper_6_1,
-- object_id_2, band_2, class_star_2, flags_2, mag_auto_2, magerr_auto_2,
-- mag_aper_1_2, magerr_aper_1_2, mag_aper_2_2, magerr_aper_2_2,
-- mag_aper_3_2, magerr_aper_3_2, mag_aper_4_2, magerr_aper_4_2,
-- mag_aper_5_2, magerr_aper_5_2, mag_aper_6_2, magerr_aper_6_2
-- to match image 1627161 and image 1553539 with class_star from 0.75 to 1.0,
-- flag upperlimit 0, matching tolerance 2 arc seconds, using 'magerr_aper_5' as
-- error filter and magnitude error < 0.10:
-- select * from table(fmatchimages(1627161, 1553539, 0.75, 1.0, 0, 0.032, 'magerr_aper_5', 0.10));

create or replace function testcol2(
image1 number,
image2 number,
distance_threshold float)
return MatchCoaddImgObjTab pipelined
as
pragma autonomous_transaction;


matchobj MatchCoaddImgObj := MatchCoaddImgObj(null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null);

min_ra             float;
max_ra             float;
min_dec            float;
max_dec            float;
min_ra1            float;
max_ra1            float;
min_dec1           float;
max_dec1           float;
min_ra2            float;
max_ra2            float;
min_dec2           float;
max_dec2           float;

i                  number;
j                  number;

type obj_collection is table of coadd_objects%ROWTYPE;

collect1 obj_collection;
collect2 obj_collection;


cursor selectcsr1 is
  select /*+ INDEX (coadd_objects coaddobj_ra_dec_idx) */
        a.*
  from  coadd_objects a
  where (image1 in (a.imageid_g, a.imageid_r, a.imageid_i, a.imageid_z)) and
        a.alpha_j2000 between min_ra and max_ra and
        a.delta_j2000 between min_dec and max_dec
  order by a.alpha_j2000;

cursor selectcsr2 is
  select /*+ INDEX (coadd_objects coaddobj_ra_dec_idx) */
        a.*
  from coadd_objects a
  where (image2 in (a.imageid_g, a.imageid_r, a.imageid_i, a.imageid_z)) and
        a.alpha_j2000 between min_ra and max_ra and
        a.delta_j2000 between min_dec and max_dec
  order by a.alpha_j2000;

-- end of setting up sql cursors

begin
  -- find the overlap part of these two images
  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra1, max_ra1, min_dec1, max_dec1
  from coadd_objects
  where image1 in (imageid_g, imageid_r, imageid_i, imageid_z);

  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra2, max_ra2, min_dec2, max_dec2
  from coadd_objects
  where image2 in (imageid_g, imageid_r, imageid_i, imageid_z);

  min_ra := greatest(min_ra1, min_ra2) - (distance_threshold * 0.1);
  max_ra := least(max_ra1, max_ra2) + (distance_threshold * 0.1);
  min_dec := greatest(min_dec1, min_dec2) - (distance_threshold * 0.1);
  max_dec := least(max_dec1, max_dec2) + (distance_threshold * 0.1);

  open selectcsr1;
  fetch selectcsr1 bulk collect into collect1;
  close selectcsr1;

  open selectcsr2;
  fetch selectcsr2 bulk collect into collect2;
  close selectcsr2;

  for i in 1 .. collect1.count
  loop
    for j in 1 .. collect2.count
    loop

      if( collect2(j).alpha_j2000 >= 
         (collect1(i).alpha_j2000 - (distance_threshold*0.1))
          and collect2(j).alpha_j2000 <= 
             (collect1(i).alpha_j2000 + (distance_threshold*0.1))
          and collect2(j).delta_j2000 >= 
             (collect1(i).delta_j2000 - (distance_threshold*0.1))
          and collect2(j).delta_j2000 <= 
             (collect1(i).delta_j2000 + (distance_threshold*0.1))
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,collect1(i).cz,
                               collect2(j).cx,collect2(j).cy,collect2(j).cz) 
                         < distance_threshold) ) 
      then

        matchobj.coadd_objects_id_1 := collect1(i).coadd_objects_id;
        matchobj.equinox_1          := collect1(i).equinox;
        matchobj.htmid_1            := collect1(i).htmid;
        matchobj.cx_1               := collect1(i).cx;
        matchobj.cy_1               := collect1(i).cy;
        matchobj.cz_1               := collect1(i).cz;
        matchobj.softid_1           := collect1(i).softid;
        matchobj.imageid_g_1        := collect1(i).imageid_g;
        matchobj.imageid_r_1        := collect1(i).imageid_r;
        matchobj.imageid_i_1        := collect1(i).imageid_i;
        matchobj.imageid_z_1        := collect1(i).imageid_z;
        matchobj.zeropoint_g_1      := collect1(i).zeropoint_g;
        matchobj.zeropoint_r_1      := collect1(i).zeropoint_r;
        matchobj.zeropoint_i_1      := collect1(i).zeropoint_i;
        matchobj.zeropoint_z_1      := collect1(i).zeropoint_z;
        matchobj.errzeropoint_g_1   := collect1(i).errzeropoint_g;
        matchobj.errzeropoint_r_1   := collect1(i).errzeropoint_r;
        matchobj.errzeropoint_i_1   := collect1(i).errzeropoint_i;
        matchobj.errzeropoint_z_1   := collect1(i).errzeropoint_z;
        matchobj.zeropointid_g_1    := collect1(i).zeropointid_g;
        matchobj.zeropointid_r_1    := collect1(i).zeropointid_r;
        matchobj.zeropointid_i_1    := collect1(i).zeropointid_i;
        matchobj.zeropointid_z_1    := collect1(i).zeropointid_z;
        matchobj.r_object_number_1  := collect1(i).r_object_number;
        matchobj.mag_auto_g_1       := collect1(i).mag_auto_g;
        matchobj.magerr_auto_g_1    := collect1(i).magerr_auto_g;
        matchobj.mag_aper1_g_1      := collect1(i).mag_aper1_g;
        matchobj.magerr_aper1_g_1   := collect1(i).magerr_aper1_g;
        matchobj.mag_aper2_g_1      := collect1(i).mag_aper2_g;
        matchobj.magerr_aper2_g_1   := collect1(i).magerr_aper2_g;
        matchobj.mag_aper3_g_1      := collect1(i).mag_aper3_g;
        matchobj.magerr_aper3_g_1   := collect1(i).magerr_aper3_g;
        matchobj.mag_aper4_g_1      := collect1(i).mag_aper4_g;
        matchobj.magerr_aper4_g_1   := collect1(i).magerr_aper4_g;
        matchobj.mag_aper5_g_1      := collect1(i).mag_aper5_g;
        matchobj.magerr_aper5_g_1   := collect1(i).magerr_aper5_g;
        matchobj.mag_aper6_g_1      := collect1(i).mag_aper6_g;
        matchobj.magerr_aper6_g_1   := collect1(i).magerr_aper6_g;
        matchobj.mag_auto_r_1       := collect1(i).mag_auto_r;
        matchobj.magerr_auto_r_1    := collect1(i).magerr_auto_r;
        matchobj.mag_aper1_r_1      := collect1(i).mag_aper1_r;
        matchobj.magerr_aper1_r_1   := collect1(i).magerr_aper1_r;
        matchobj.mag_aper2_r_1      := collect1(i).mag_aper2_r;
        matchobj.magerr_aper2_r_1   := collect1(i).magerr_aper2_r;
        matchobj.mag_aper3_r_1      := collect1(i).mag_aper3_r;
        matchobj.magerr_aper3_r_1   := collect1(i).magerr_aper3_r;
        matchobj.mag_aper4_r_1      := collect1(i).mag_aper4_r;
        matchobj.magerr_aper4_r_1   := collect1(i).magerr_aper4_r;
        matchobj.mag_aper5_r_1      := collect1(i).mag_aper5_r;
        matchobj.magerr_aper5_r_1   := collect1(i).magerr_aper5_r;
        matchobj.mag_aper6_r_1      := collect1(i).mag_aper6_r;
        matchobj.magerr_aper6_r_1   := collect1(i).magerr_aper6_r;
        matchobj.mag_auto_i_1       := collect1(i).mag_auto_i;
        matchobj.magerr_auto_i_1    := collect1(i).magerr_auto_i;
        matchobj.mag_aper1_i_1      := collect1(i).mag_aper1_i;
        matchobj.magerr_aper1_i_1   := collect1(i).magerr_aper1_i;
        matchobj.mag_aper2_i_1      := collect1(i).mag_aper2_i;
        matchobj.magerr_aper2_i_1   := collect1(i).magerr_aper2_i;
        matchobj.mag_aper3_i_1      := collect1(i).mag_aper3_i;
        matchobj.magerr_aper3_i_1   := collect1(i).magerr_aper3_i;
        matchobj.mag_aper4_i_1      := collect1(i).mag_aper4_i;
        matchobj.magerr_aper4_i_1   := collect1(i).magerr_aper4_i;
        matchobj.mag_aper5_i_1      := collect1(i).mag_aper5_i;
        matchobj.magerr_aper5_i_1   := collect1(i).magerr_aper5_i;
        matchobj.mag_aper6_i_1      := collect1(i).mag_aper6_i;
        matchobj.magerr_aper6_i_1   := collect1(i).magerr_aper6_i;
        matchobj.mag_auto_z_1       := collect1(i).mag_auto_z;
        matchobj.magerr_auto_z_1    := collect1(i).magerr_auto_z;
        matchobj.mag_aper1_z_1      := collect1(i).mag_aper1_z;
        matchobj.magerr_aper1_z_1   := collect1(i).magerr_aper1_z;
        matchobj.mag_aper2_z_1      := collect1(i).mag_aper2_z;
        matchobj.magerr_aper2_z_1   := collect1(i).magerr_aper2_z;
        matchobj.mag_aper3_z_1      := collect1(i).mag_aper3_z;
        matchobj.magerr_aper3_z_1   := collect1(i).magerr_aper3_z;
        matchobj.mag_aper4_z_1      := collect1(i).mag_aper4_z;
        matchobj.magerr_aper4_z_1   := collect1(i).magerr_aper4_z;
        matchobj.mag_aper5_z_1      := collect1(i).mag_aper5_z;
        matchobj.magerr_aper5_z_1   := collect1(i).magerr_aper5_z;
        matchobj.mag_aper6_z_1      := collect1(i).mag_aper6_z;
        matchobj.magerr_aper6_z_1   := collect1(i).magerr_aper6_z;
        matchobj.alpha_j2000_1      := collect1(i).alpha_j2000;
        matchobj.delta_j2000_1      := collect1(i).delta_j2000;
        matchobj.x2_world_1         := collect1(i).x2_world;
        matchobj.y2_world_1         := collect1(i).y2_world;
        matchobj.xy_world_1         := collect1(i).xy_world;
        matchobj.threshold_1        := collect1(i).threshold;
        matchobj.x_image_1          := collect1(i).x_image;
        matchobj.y_image_1          := collect1(i).y_image;
        matchobj.theta_image_g_1    := collect1(i).theta_image_g;
        matchobj.errtheta_image_g_1 := collect1(i).errtheta_image_g;
        matchobj.ellipticity_g_1    := collect1(i).ellipticity_g;
        matchobj.class_star_g_1     := collect1(i).class_star_g;
        matchobj.flags_g_1          := collect1(i).flags_g;
        matchobj.theta_image_r_1    := collect1(i).theta_image_r;
        matchobj.errtheta_image_r_1 := collect1(i).errtheta_image_r;
        matchobj.ellipticity_r_1    := collect1(i).ellipticity_r;
        matchobj.class_star_r_1     := collect1(i).class_star_r;
        matchobj.flags_r_1          := collect1(i).flags_r;
        matchobj.theta_image_i_1    := collect1(i).theta_image_i;
        matchobj.errtheta_image_i_1 := collect1(i).errtheta_image_i;
        matchobj.ellipticity_i_1    := collect1(i).ellipticity_i;
        matchobj.class_star_i_1     := collect1(i).class_star_i;
        matchobj.flags_i_1          := collect1(i).flags_i;
        matchobj.theta_image_z_1    := collect1(i).theta_image_z;
        matchobj.errtheta_image_z_1 := collect1(i).errtheta_image_z;
        matchobj.ellipticity_z_1    := collect1(i).ellipticity_z;
        matchobj.class_star_z_1     := collect1(i).class_star_z;
        matchobj.flags_z_1          := collect1(i).flags_z;

        matchobj.coadd_objects_id_2 := collect2(j).coadd_objects_id;
        matchobj.equinox_2          := collect2(j).equinox;
        matchobj.htmid_2            := collect2(j).htmid;
        matchobj.cx_2               := collect2(j).cx;
        matchobj.cy_2               := collect2(j).cy;
        matchobj.cz_2               := collect2(j).cz;
        matchobj.softid_2           := collect2(j).softid;
        matchobj.imageid_g_2        := collect2(j).imageid_g;
        matchobj.imageid_r_2        := collect2(j).imageid_r;
        matchobj.imageid_i_2        := collect2(j).imageid_i;
        matchobj.imageid_z_2        := collect2(j).imageid_z;
        matchobj.zeropoint_g_2      := collect2(j).zeropoint_g;
        matchobj.zeropoint_r_2      := collect2(j).zeropoint_r;
        matchobj.zeropoint_i_2      := collect2(j).zeropoint_i;
        matchobj.zeropoint_z_2      := collect2(j).zeropoint_z;
        matchobj.errzeropoint_g_2   := collect2(j).errzeropoint_g;
        matchobj.errzeropoint_r_2   := collect2(j).errzeropoint_r;
        matchobj.errzeropoint_i_2   := collect2(j).errzeropoint_i;
        matchobj.errzeropoint_z_2   := collect2(j).errzeropoint_z;
        matchobj.zeropointid_g_2    := collect2(j).zeropointid_g;
        matchobj.zeropointid_r_2    := collect2(j).zeropointid_r;
        matchobj.zeropointid_i_2    := collect2(j).zeropointid_i;
        matchobj.zeropointid_z_2    := collect2(j).zeropointid_z;
        matchobj.r_object_number_2  := collect2(j).r_object_number;
        matchobj.mag_auto_g_2       := collect2(j).mag_auto_g;
        matchobj.magerr_auto_g_2    := collect2(j).magerr_auto_g;
        matchobj.mag_aper1_g_2      := collect2(j).mag_aper1_g;
        matchobj.magerr_aper1_g_2   := collect2(j).magerr_aper1_g;
        matchobj.mag_aper2_g_2      := collect2(j).mag_aper2_g;
        matchobj.magerr_aper2_g_2   := collect2(j).magerr_aper2_g;
        matchobj.mag_aper3_g_2      := collect2(j).mag_aper3_g;
        matchobj.magerr_aper3_g_2   := collect2(j).magerr_aper3_g;
        matchobj.mag_aper4_g_2      := collect2(j).mag_aper4_g;
        matchobj.magerr_aper4_g_2   := collect2(j).magerr_aper4_g;
        matchobj.mag_aper5_g_2      := collect2(j).mag_aper5_g;
        matchobj.magerr_aper5_g_2   := collect2(j).magerr_aper5_g;
        matchobj.mag_aper6_g_2      := collect2(j).mag_aper6_g;
        matchobj.magerr_aper6_g_2   := collect2(j).magerr_aper6_g;
        matchobj.mag_auto_r_2       := collect2(j).mag_auto_r;
        matchobj.magerr_auto_r_2    := collect2(j).magerr_auto_r;
        matchobj.mag_aper1_r_2      := collect2(j).mag_aper1_r;
        matchobj.magerr_aper1_r_2   := collect2(j).magerr_aper1_r;
        matchobj.mag_aper2_r_2      := collect2(j).mag_aper2_r;
        matchobj.magerr_aper2_r_2   := collect2(j).magerr_aper2_r;
        matchobj.mag_aper3_r_2      := collect2(j).mag_aper3_r;
        matchobj.magerr_aper3_r_2   := collect2(j).magerr_aper3_r;
        matchobj.mag_aper4_r_2      := collect2(j).mag_aper4_r;
        matchobj.magerr_aper4_r_2   := collect2(j).magerr_aper4_r;
        matchobj.mag_aper5_r_2      := collect2(j).mag_aper5_r;
        matchobj.magerr_aper5_r_2   := collect2(j).magerr_aper5_r;
        matchobj.mag_aper6_r_2      := collect2(j).mag_aper6_r;
        matchobj.magerr_aper6_r_2   := collect2(j).magerr_aper6_r;
        matchobj.mag_auto_i_2       := collect2(j).mag_auto_i;
        matchobj.magerr_auto_i_2    := collect2(j).magerr_auto_i;
        matchobj.mag_aper1_i_2      := collect2(j).mag_aper1_i;
        matchobj.magerr_aper1_i_2   := collect2(j).magerr_aper1_i;
        matchobj.mag_aper2_i_2      := collect2(j).mag_aper2_i;
        matchobj.magerr_aper2_i_2   := collect2(j).magerr_aper2_i;
        matchobj.mag_aper3_i_2      := collect2(j).mag_aper3_i;
        matchobj.magerr_aper3_i_2   := collect2(j).magerr_aper3_i;
        matchobj.mag_aper4_i_2      := collect2(j).mag_aper4_i;
        matchobj.magerr_aper4_i_2   := collect2(j).magerr_aper4_i;
        matchobj.mag_aper5_i_2      := collect2(j).mag_aper5_i;
        matchobj.magerr_aper5_i_2   := collect2(j).magerr_aper5_i;
        matchobj.mag_aper6_i_2      := collect2(j).mag_aper6_i;
        matchobj.magerr_aper6_i_2   := collect2(j).magerr_aper6_i;
        matchobj.mag_auto_z_2       := collect2(j).mag_auto_z;
        matchobj.magerr_auto_z_2    := collect2(j).magerr_auto_z;
        matchobj.mag_aper1_z_2      := collect2(j).mag_aper1_z;
        matchobj.magerr_aper1_z_2   := collect2(j).magerr_aper1_z;
        matchobj.mag_aper2_z_2      := collect2(j).mag_aper2_z;
        matchobj.magerr_aper2_z_2   := collect2(j).magerr_aper2_z;
        matchobj.mag_aper3_z_2      := collect2(j).mag_aper3_z;
        matchobj.magerr_aper3_z_2   := collect2(j).magerr_aper3_z;
        matchobj.mag_aper4_z_2      := collect2(j).mag_aper4_z;
        matchobj.magerr_aper4_z_2   := collect2(j).magerr_aper4_z;
        matchobj.mag_aper5_z_2      := collect2(j).mag_aper5_z;
        matchobj.magerr_aper5_z_2   := collect2(j).magerr_aper5_z;
        matchobj.mag_aper6_z_2      := collect2(j).mag_aper6_z;
        matchobj.magerr_aper6_z_2   := collect2(j).magerr_aper6_z;
        matchobj.alpha_j2000_2      := collect2(j).alpha_j2000;
        matchobj.delta_j2000_2      := collect2(j).delta_j2000;
        matchobj.x2_world_2         := collect2(j).x2_world;
        matchobj.y2_world_2         := collect2(j).y2_world;
        matchobj.xy_world_2         := collect2(j).xy_world;
        matchobj.threshold_2        := collect2(j).threshold;
        matchobj.x_image_2          := collect2(j).x_image;
        matchobj.y_image_2          := collect2(j).y_image;
        matchobj.theta_image_g_2    := collect2(j).theta_image_g;
        matchobj.errtheta_image_g_2 := collect2(j).errtheta_image_g;
        matchobj.ellipticity_g_2    := collect2(j).ellipticity_g;
        matchobj.class_star_g_2     := collect2(j).class_star_g;
        matchobj.flags_g_2          := collect2(j).flags_g;
        matchobj.theta_image_r_2    := collect2(j).theta_image_r;
        matchobj.errtheta_image_r_2 := collect2(j).errtheta_image_r;
        matchobj.ellipticity_r_2    := collect2(j).ellipticity_r;
        matchobj.class_star_r_2     := collect2(j).class_star_r;
        matchobj.flags_r_2          := collect2(j).flags_r;
        matchobj.theta_image_i_2    := collect2(j).theta_image_i;
        matchobj.errtheta_image_i_2 := collect2(j).errtheta_image_i;
        matchobj.ellipticity_i_2    := collect2(j).ellipticity_i;
        matchobj.class_star_i_2     := collect2(j).class_star_i;
        matchobj.flags_i_2          := collect2(j).flags_i;
        matchobj.theta_image_z_2    := collect2(j).theta_image_z;
        matchobj.errtheta_image_z_2 := collect2(j).errtheta_image_z;
        matchobj.ellipticity_z_2    := collect2(j).ellipticity_z;
        matchobj.class_star_z_2     := collect2(j).class_star_z;
        matchobj.flags_z_2          := collect2(j).flags_z;

        pipe row (matchobj);

        exit;

      end if;
    end loop;
  end loop;

  commit;
  return;
end testcol2;
/
