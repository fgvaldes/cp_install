drop public synonym GetRange;
create public synonym GetRange for des_admin.GetRange;
grant all on GetRange to des_writer;
grant all on GetRange to des_reader;

drop public synonym fDecToDms;
create public synonym fDecToDms for des_admin.fDecToDms;
grant all on fDecToDms to des_writer;
grant all on fDecToDms to des_reader;

drop public synonym fEqToHtm;
create public synonym fEqToHtm for des_admin.fEqToHtm;
grant all on fEqToHtm to des_writer;
grant all on fEqToHtm to des_reader;

drop public synonym fEqToXyz;
create public synonym fEqToXyz for des_admin.fEqToXyz;
grant all on fEqToXyz to des_writer;
grant all on fEqToXyz to des_reader;

drop public synonym fGetDistanceEq;
create public synonym fGetDistanceEq for des_admin.fGetDistanceEq;
grant all on fGetDistanceEq to des_writer;
grant all on fGetDistanceEq to des_reader;

drop public synonym fGetDistanceXYZ;
create public synonym fGetDistanceXYZ for des_admin.fGetDistanceXYZ;
grant all on fGetDistanceXYZ to des_writer;
grant all on fGetDistanceXYZ to des_reader;

drop public synonym fGetNearbyEq;
create public synonym fGetNearbyEq for des_admin.fGetNearbyEq;
grant all on fGetNearbyEq to des_writer;
grant all on fGetNearbyEq to des_reader;

drop public synonym fGetObjFromRect;
create public synonym fGetObjFromRect for des_admin.fGetObjFromRect;
grant all on fGetObjFromRect to des_writer;
grant all on fGetObjFromRect to des_reader;

drop public synonym fGetNearbyXYZpre_nohint;
create public synonym fGetNearbyXYZpre_nohint for des_admin.fGetNearbyXYZpre_nohint;
grant all on fGetNearbyXYZpre_nohint to des_writer;
grant all on fGetNearbyXYZpre_nohint to des_reader;

drop public synonym fGetNearbyXYZpre;
create public synonym fGetNearbyXYZpre for des_admin.fGetNearbyXYZpre;
grant all on fGetNearbyXYZpre to des_writer;
grant all on fGetNearbyXYZpre to des_reader;

drop public synonym fGetNearbyXYZ;
create public synonym fGetNearbyXYZ for des_admin.fGetNearbyXYZ;
grant all on fGetNearbyXYZ to des_writer;
grant all on fGetNearbyXYZ to des_reader;

drop public synonym fGetNearestEq;
create public synonym fGetNearestEq for des_admin.fGetNearestEq;
grant all on fGetNearestEq to des_writer;
grant all on fGetNearestEq to des_reader;

drop public synonym fGetNearestXYZ;
create public synonym fGetNearestXYZ for des_admin.fGetNearestXYZ;
grant all on fGetNearestXYZ to des_writer;
grant all on fGetNearestXYZ to des_reader;

drop public synonym fHtmCover;
create public synonym fHtmCover for des_admin.fHtmCover;
grant all on fHtmCover to des_writer;
grant all on fHtmCover to des_reader;

drop public synonym fHtmToString;
create public synonym fHtmToString for des_admin.fHtmToString;
grant all on fHtmToString to des_writer;
grant all on fHtmToString to des_reader;

drop public synonym fHtmToXyz;
create public synonym fHtmToXyz for des_admin.fHtmToXyz;
grant all on fHtmToXyz to des_writer;
grant all on fHtmToXyz to des_reader;

drop public synonym fRaToHms;
create public synonym fRaToHms for des_admin.fRaToHms;
grant all on fRaToHms to des_writer;
grant all on fRaToHms to des_reader;

drop public synonym fXyzToHtm;
create public synonym fXyzToHtm for des_admin.fXyzToHtm;
grant all on fXyzToHtm to des_writer;
grant all on fXyzToHtm to des_reader;

drop public synonym fGetNearbyXYZpre_Truth;
create public synonym fGetNearbyXYZpre_Truth for des_admin.fGetNearbyXYZpre_Truth;
grant all on fGetNearbyXYZpre_Truth to des_writer;
grant all on fGetNearbyXYZpre_Truth to des_reader;

drop public synonym fGetNearestEq_Truth;
create public synonym fGetNearestEq_Truth for des_admin.fGetNearestEq_Truth;
grant all on fGetNearestEq_Truth to des_writer;
grant all on fGetNearestEq_Truth to des_reader;

drop public synonym fMatchObjects;
create public synonym fMatchObjects for des_admin.fMatchObjects;
grant all on fMatchObjects to des_writer;
grant all on fMatchObjects to des_reader;

drop public synonym fGetNearbyXYZpre_ObjHead;
create public synonym fGetNearbyXYZpre_ObjHead for des_admin.fGetNearbyXYZpre_ObjHead;
grant all on fGetNearbyXYZpre_ObjHead to des_writer;
grant all on fGetNearbyXYZpre_ObjHead to des_reader;

drop public synonym fGetNearbyXYZ_ObjHead;
create public synonym fGetNearbyXYZ_ObjHead for des_admin.fGetNearbyXYZ_ObjHead;
grant all on fGetNearbyXYZ_ObjHead to des_writer;
grant all on fGetNearbyXYZ_ObjHead to des_reader;

drop public synonym fGetNearbyEq_ObjHead;
create public synonym fGetNearbyEq_ObjHead for des_admin.fGetNearbyEq_ObjHead;
grant all on fGetNearbyEq_ObjHead to des_writer;
grant all on fGetNearbyEq_ObjHead to des_reader;

drop public synonym fEqToX;
create public synonym fEqToX for des_admin.fEqToX;
grant all on fEqToX to des_writer;
grant all on fEqToX to des_reader;

drop public synonym fEqToY;
create public synonym fEqToY for des_admin.fEqToY;
grant all on fEqToY to des_writer;
grant all on fEqToY to des_reader;

drop public synonym fEqToZ;
create public synonym fEqToZ for des_admin.fEqToZ;
grant all on fEqToZ to des_writer;
grant all on fEqToZ to des_reader;

drop public synonym fPhotoMatch;
create public synonym fPhotoMatch for des_admin.fPhotoMatch;
grant all on fPhotoMatch to des_writer;
grant all on fPhotoMatch to des_reader;

drop public synonym fTwoTablesMatch;
create public synonym fTwoTablesMatch for des_admin.fTwoTablesMatch;
grant all on fTwoTablesMatch to des_writer;
grant all on fTwoTablesMatch to des_reader;

drop public synonym fMatchCatSim1Obj;
create public synonym fMatchCatSim1Obj for des_admin.fMatchCatSim1Obj;
grant all on fMatchCatSim1Obj to des_writer;
grant all on fMatchCatSim1Obj to des_reader;

drop public synonym fMatchImages;
create public synonym fMatchImages for des_admin.fMatchImages;
grant all on fMatchImages to des_writer;
grant all on fMatchImages to des_reader;

drop public synonym upd_archivesites;
create public synonym upd_archivesites for des_admin.upd_archivesites;
grant all on upd_archivesites to des_writer;
grant all on upd_archivesites to des_reader;

drop public synonym get_srclist;
create public synonym get_srclist for des_admin.get_srclist;
grant all on get_srclist to des_writer;
grant all on get_srclist to des_reader;

drop public synonym fGetObjFromCirc;
create public synonym fGetObjFromCirc for des_admin.fGetObjFromCirc;
grant all on fGetObjFromCirc to des_writer;
grant all on fGetObjFromCirc to des_reader;

drop public synonym fPSM;
create public synonym fPSM for des_admin.fPSM;
grant all on fPSM to des_writer;
grant all on fPSM to des_reader;

drop public synonym old_fPhotoMatch;
create public synonym old_fPhotoMatch for des_admin.old_fPhotoMatch;
grant all on old_fPhotoMatch to des_writer;
grant all on old_fPhotoMatch to des_reader;

drop public synonym fGetRaOffset;
create public synonym fGetRaOffset for des_admin.fGetRaOffset;
grant all on fGetRaOffset to des_writer;
grant all on fGetRaOffset to des_reader;

drop public synonym fMatchImages_coadd;
create public synonym fMatchImages_coadd for des_admin.fMatchImages_coadd;
grant all on fMatchImages_coadd to des_writer;
grant all on fMatchImages_coadd to des_reader;

drop public synonym DelCoadd;
create public synonym DelCoadd for des_admin.DelCoadd;
grant all on DelCoadd to des_writer;
grant all on DelCoadd to des_reader;

drop public synonym ProcessQuery;
create public synonym ProcessQuery for des_admin.ProcessQuery;
grant all on ProcessQuery to des_writer;
grant all on ProcessQuery to des_reader;

drop public synonym fPhotoStdsMatch;
create public synonym fPhotoStdsMatch for des_admin.fPhotoStdsMatch;
grant all on fPhotoStdsMatch to des_writer;
grant all on fPhotoStdsMatch to des_reader;

drop public synonym find_matches;
create public synonym find_matches for des_admin.find_matches;
grant all on find_matches to des_writer;
grant all on find_matches to des_reader;

drop public synonym fMatchImagesGCM_coadd;
create public synonym fMatchImagesGCM_coadd for des_admin.fMatchImagesGCM_coadd;
grant all on fMatchImagesGCM_coadd to des_writer;
grant all on fMatchImagesGCM_coadd to des_reader;

drop public synonym find_matches_RADEC;
create public synonym find_matches_RADEC for des_admin.find_matches_RADEC;
grant all on find_matches_RADEC to des_writer;
grant all on find_matches_RADEC to des_reader;

