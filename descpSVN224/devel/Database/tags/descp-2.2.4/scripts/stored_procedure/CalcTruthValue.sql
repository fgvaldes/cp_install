-- This procedure calculates htmid, cx, cy, cz and update 
-- the corresponding columns in dc0_truth table.
-- The procedure takes two parameters: the starting id of the truth object,
-- and the ending id of the truth object.
-- To run the program: exec CalcTruthValue(1, 1000000);
create or replace procedure CalcTruthValue(
start_tr number, end_tr number) as

tr_id number(11);
ra number(8,5);
dec number(8,5);
htmid_val number(16);
vec vector;
cx_val number(10,6);
cy_val number(10,6);
cz_val number(10,6);
row_cnt number(11);

cursor tr_cr is 
  select /*+ INDEX(dc0_truth PK_DC0_TRUTH) */
       truth_id, alpha_j2000, delta_j2000
  from dc0_truth
  where truth_id >= start_tr
  and   truth_id <= end_tr;

begin
  --dbms_output.put_line ('start_tr is ' || start_tr);
  --dbms_output.put_line ('end_tr is ' || end_tr);
  row_cnt := 1;
  open tr_cr;
  loop
    fetch tr_cr into tr_id, ra, dec;
    exit when tr_cr%NOTFOUND;

    select fEqToHtm(ra, dec)
    into htmid_val
    from dual;   

    select fEqToXYZ(ra, dec)
    into vec
    from dual;

    cx_val := vec(1);
    cy_val := vec(2);
    cz_val := vec(3);
     
    update /*+ INDEX(dc0_truth PK_DC0_TRUTH) */
      dc0_truth
    set htmid = htmid_val,
        cx = cx_val,
        cy = cy_val,
        cz = cz_val
    where truth_id = tr_id;

    row_cnt := row_cnt + 1;
    if (mod(row_cnt,1000) = 0) then
      commit;
    end if;
  end loop;
  
end CalcTruthValue;
/
