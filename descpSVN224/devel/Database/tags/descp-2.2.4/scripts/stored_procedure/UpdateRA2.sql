-- Batch job to convert RA values from hours to degrees and update the RA column in USNOB_CAT1 table

create or replace procedure UpdateRA(
begin_id number, end_id number) as

start_id number(11) := begin_id ;
increm number(11) :=5000;

begin

while (start_id <= begin_id)
loop
 
    update /* INDEX(usnob_cat1 PK_USNOB_CAT1) */
    usnob_cat1
    set old_ra = ra,
	ra = ra * 15,
	htmid = fEqToHtm(ra*15, DEC), 
	cx = fEqToX(ra*15, DEC), 
	cy = fEqToY(ra*15, DEC), 
	cz = fEqToZ(ra*15, DEC)
    where usnob_cat1_id >= start_id
     and usnob_cat1_id < (start_id + increm)
     and old_ra is null;

commit;

start_id  := start_id + increm;

end loop;

dbms_output.put_line ('begin_id is :' || begin_id);
dbms_output.put_line ('end_id is :' || end_id);

end UpdateRA;
/
