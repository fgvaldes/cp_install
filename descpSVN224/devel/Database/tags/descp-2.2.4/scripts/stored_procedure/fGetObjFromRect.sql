-- find the objects that are inside of the rectangle specified by ra and dec
-- run the procedure: select * from table(fGetObjFromRect(11.10, -2.30, 11.15, -2.20))
create or replace function fGetObjFromRect(
ra1 float, -- smaller RA
dec1 float, -- smaller DEC
ra2 float, -- bigger RA
dec2 float) -- bigger DEC
return MatchedObjTab pipelined
is
pragma autonomous_transaction;

matchobj matchedObj := matchedObj(null,null,null,null,null,null);
vec vector := vector(null,null,null);
vec1 vector := vector(null,null,null);
vec2 vector := vector(null,null,null);
ra float;
dec float;
d1 float;
d2 float;
dot float;
nx float;
ny float;
nz float;
nx1 float;
ny1 float;
nz1 float;
nx2 float;
ny2 float;
nz2 float;
radius float;

cursor rectrc is 
  select object_id, htmid,
         cx, cy, cz, distance 
  from table(fGetNearbyEq(ra,dec,radius))
  where (cz>nz1) 
  and   (cz<nz2)
  --and   (-cx*nx1 + cy*ny1) > 0
  --and   (cx*nx2 - cy*ny2) > 0
  and   (-cx*ny1 + cy*nx1) > 0
  and   (cx*ny2 - cy*nx2) > 0
  order by distance;

begin
    vec1 := fEqToXyz(ra1, dec1);
    nx1 := vec1(1);
    ny1 := vec1(2);
    nz1 := vec1(3); 
    --dbms_output.put_line('point1 is ' || nx1 || ', ' || ny1 || ', ' || nz1);

    vec2 := fEqToXyz(ra2, dec2);
    nx2 := vec2(1);
    ny2 := vec2(2);
    nz2 := vec2(3); 
    --dbms_output.put_line('point2 is ' || nx2 || ', ' || ny2 || ', ' || nz2);

    --ra := (ra1 + ra2)/2;
    -- dealing the wrap around problem, such as ra1=355, ra2=12
    if (ra1 < ra2) then
      ra := (ra1 + ra2)/2;
    else
      ra := (360 - ra1 + ra2)/2;
      -- case (ra1=355, ra2=12) 
      if (ra < ra2) then
        ra := ra2 - ra; -- 8.5 < 12, ra=12-8.5
      -- case (ra1=300, ra2=6)
      else 
        ra := ra1 + ra; -- 33 > 6, ra=300+33
      end if;
    end if; 
    --dbms_output.put_line('ra is ' || ra);
    dec := (dec1 + dec2)/2;
    --dbms_output.put_line('dec is ' || dec);
    vec := fEqToXyz(ra, dec);
    nx := vec(1);
    ny := vec(2);
    nz := vec(3); 

    d1 := nx1*nx + ny1*ny + nz1*nz;
    d2 := nx2*nx + ny2*ny + nz2*nz;

    if (d1<d2) then
      dot := d1;
    else
      dot := d2;
    end if;

    radius := acos(dot)/(3.1415926535897932385E0/180)*60; 
    --dbms_output.put_line('radius is ' || radius);

    for rectrc_rec in rectrc
    loop
      matchobj.object_id := rectrc_rec.object_id;
      matchobj.htmid := rectrc_rec.htmid;
      matchobj.cx := rectrc_rec.cx;
      matchobj.cy := rectrc_rec.cy;
      matchobj.cz := rectrc_rec.cz;
      matchobj.distance := rectrc_rec.distance;
      pipe row (matchobj);
    end loop;
    commit;
    return;
end fGetObjFromRect;
/
