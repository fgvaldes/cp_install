-- find the nearest object to the point specified by ra and dec
-- run the procedure: select * from table(fGetNearestEq(10.47, -2.9, 5))
create or replace function fGetNearestEq(
ra float,
dec float,
r float)
return MatchedObjTab pipelined
is
pragma autonomous_transaction;

matchobj matchedObj := matchedObj(null,null,null,null,null,null);
vec vector := vector(null,null,null);
r_ float;
d float;
nx float;
ny float;
nz float;

cursor mstfrc is 
  select object_id, htmid,
         cx, cy, cz, distance
  from table(fGetNearbyXYZpre(nx,ny,nz,r_))
  where distance < r_
  order by distance;

begin
    r_ := r;
    if (r_<0.1) then
      r_ := 0.1;
    end if;
    vec := fEqToXyz(ra, dec);
    nx := vec(1);
    ny := vec(2);
    nz := vec(3);

    --dbms_output.put_line('xyz is ' || nx ||',' || ny ||',' || nz);
    
    for mstfrc_rec in mstfrc
    loop
      -- only output the nearest object, this loop only runs once
        matchobj.object_id := mstfrc_rec.object_id;
        matchobj.htmid := mstfrc_rec.htmid;
        matchobj.cx := mstfrc_rec.cx;
        matchobj.cy := mstfrc_rec.cy;
        matchobj.cz := mstfrc_rec.cz;
        matchobj.distance := mstfrc_rec.distance;
        pipe row (matchobj);
        return;
    end loop;
end fGetNearestEq;
/
