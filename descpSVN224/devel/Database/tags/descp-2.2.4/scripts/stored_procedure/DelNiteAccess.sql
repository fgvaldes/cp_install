-- Check newly removed nite identifiers
create or replace procedure DelNiteAccess
as

nite_id varchar(20);
project_id varchar(20);

cursor nite_del_cr is 
  select distinct a.nite, a.project
  from nite_access a
  where a.des_group = 'all'
  and   not exists (select distinct b.nite, b.project
                    from location b
                    where a.nite = b.nite
                    and   a.project = b.project
                    and   regexp_like(archivesites, '[^N]')
                    and   b.project in ('BCS', 'DES', 'SCS', 'CPT', 'SPT', 'CFH'));

begin
  open nite_del_cr;
  loop
    fetch nite_del_cr into nite_id, project_id;
    exit when nite_del_cr%NOTFOUND;
    dbms_output.put_line ('    ' || nite_id || ',' || project_id);
  end loop;
end DelNiteAccess;
/
