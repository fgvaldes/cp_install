-- this stored procedure matches the objects in two images
-- the program has 7 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. stella_lo (minimum value for class_star)
-- 4. stella_hi (maximum value for class_star)
-- 5. flag_upperlim (upperlim for flags)
-- 6. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- 7. magerr_filter (magnitude error filter used: magerr_auto, magerr_aper_1, etc)
-- 8. magerr_limit (magnitude error limit, only the objects with error < magerr_limit will be matched)
-- the program returns a table which has 36 columns:
-- object_id_1, band_1, class_star_1, flags_1, mag_auto_1, magerr_auto_1,
-- mag_aper_1_1, magerr_aper_1_1, mag_aper_2_1, magerr_aper_2_1,
-- mag_aper_3_1, magerr_aper_3_1, mag_aper_4_1, magerr_aper_4_1,
-- mag_aper_5_1, magerr_aper_5_1, mag_aper_6_1, magerr_aper_6_1,
-- object_id_2, band_2, class_star_2, flags_2, mag_auto_2, magerr_auto_2,
-- mag_aper_1_2, magerr_aper_1_2, mag_aper_2_2, magerr_aper_2_2,
-- mag_aper_3_2, magerr_aper_3_2, mag_aper_4_2, magerr_aper_4_2,
-- mag_aper_5_2, magerr_aper_5_2, mag_aper_6_2, magerr_aper_6_2
-- to match image 1627161 and image 1553539 with class_star from 0.75 to 1.0,
-- flag upperlimit 0, matching tolerance 2 arc seconds, using 'magerr_aper_5' as
-- error filter and magnitude error < 0.10:
-- select * from table(fmatchimages(1627161, 1553539, 0.75, 1.0, 0, 0.032, 'magerr_aper_5', 0.10));

create or replace function fMatchImages_coadd(
image1 number,
image2 number,
distance_threshold float)
return MatchCoaddImgObjTab pipelined
as
pragma autonomous_transaction;

matchobj MatchCoaddImgObj := MatchCoaddImgObj(null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null, null, null, null, null,
                                              null);

min_ra1            float;
max_ra1            float;
min_dec1           float;
max_dec1           float;
min_ra2            float;
max_ra2            float;
min_dec2           float;
max_dec2           float;
min_ra             float;
max_ra             float;
min_dec            float;
max_dec            float;
select_ra          float;
select_dec         float;
distance           float;


coadd_objects_id_1 number(11,0);
equinox_1          number(6,2);
htmid_1            number(16,0);
cx_1               number(10,6);
cy_1               number(10,6);
cz_1               number(10,6);
softid_1           number(4,0);
imageid_g_1        number(9,0);
imageid_r_1        number(9,0);
imageid_i_1        number(9,0);
imageid_z_1        number(9,0);
zeropoint_g_1      number(7,4);
zeropoint_r_1      number(7,4);
zeropoint_i_1      number(7,4);
zeropoint_z_1      number(7,4);
errzeropoint_g_1   number(7,4);
errzeropoint_r_1   number(7,4);
errzeropoint_i_1   number(7,4);
errzeropoint_z_1   number(7,4);
zeropointid_g_1    number(10,0);
zeropointid_r_1    number(10,0);
zeropointid_i_1    number(10,0);
zeropointid_z_1    number(10,0);
r_object_number_1  number(6,0);
mag_auto_g_1       number(7,4);
magerr_auto_g_1    number(7,4);
mag_aper1_g_1      number(7,4);
magerr_aper1_g_1   number(7,4);
mag_aper2_g_1      number(7,4);
magerr_aper2_g_1   number(7,4);
mag_aper3_g_1      number(7,4);
magerr_aper3_g_1   number(7,4);
mag_aper4_g_1      number(7,4);
magerr_aper4_g_1   number(7,4);
mag_aper5_g_1      number(7,4);
magerr_aper5_g_1   number(7,4);
mag_aper6_g_1      number(7,4);
magerr_aper6_g_1   number(7,4);
mag_auto_r_1       number(7,4);
magerr_auto_r_1    number(7,4);
mag_aper1_r_1      number(7,4);
magerr_aper1_r_1   number(7,4);
mag_aper2_r_1      number(7,4);
magerr_aper2_r_1   number(7,4);
mag_aper3_r_1      number(7,4);
magerr_aper3_r_1   number(7,4);
mag_aper4_r_1      number(7,4);
magerr_aper4_r_1   number(7,4);
mag_aper5_r_1      number(7,4);
magerr_aper5_r_1   number(7,4);
mag_aper6_r_1      number(7,4);
magerr_aper6_r_1   number(7,4);
mag_auto_i_1       number(7,4);
magerr_auto_i_1    number(7,4);
mag_aper1_i_1      number(7,4);
magerr_aper1_i_1   number(7,4);
mag_aper2_i_1      number(7,4);
magerr_aper2_i_1   number(7,4);
mag_aper3_i_1      number(7,4);
magerr_aper3_i_1   number(7,4);
mag_aper4_i_1      number(7,4);
magerr_aper4_i_1   number(7,4);
mag_aper5_i_1      number(7,4);
magerr_aper5_i_1   number(7,4);
mag_aper6_i_1      number(7,4);
magerr_aper6_i_1   number(7,4);
mag_auto_z_1       number(7,4);
magerr_auto_z_1    number(7,4);
mag_aper1_z_1      number(7,4);
magerr_aper1_z_1   number(7,4);
mag_aper2_z_1      number(7,4);
magerr_aper2_z_1   number(7,4);
mag_aper3_z_1      number(7,4);
magerr_aper3_z_1   number(7,4);
mag_aper4_z_1      number(7,4);
magerr_aper4_z_1   number(7,4);
mag_aper5_z_1      number(7,4);
magerr_aper5_z_1   number(7,4);
mag_aper6_z_1      number(7,4);
magerr_aper6_z_1   number(7,4);
alpha_j2000_1      number(8,5);
delta_j2000_1      number(8,5);
x2_world_1         number(11,3);
y2_world_1         number(11,3);
xy_world_1         number(11,3);
threshold_1        number(6,2);
x_image_1          number(6,2);
y_image_1          number(6,2);
theta_image_g_1    number(4,2);
errtheta_image_g_1 number(4,2);
ellipticity_g_1    number(5,4);
class_star_g_1     number(3,2);
flags_g_1          number(3,0);
theta_image_r_1    number(4,2);
errtheta_image_r_1 number(4,2);
ellipticity_r_1    number(5,4);
class_star_r_1     number(3,2);
flags_r_1          number(3,0);
theta_image_i_1    number(4,2);
errtheta_image_i_1 number(4,2);
ellipticity_i_1    number(5,4);
class_star_i_1     number(3,2);
flags_i_1          number(3,0);
theta_image_z_1    number(4,2);
errtheta_image_z_1 number(4,2);
ellipticity_z_1    number(5,4);
class_star_z_1     number(3,2);
flags_z_1          number(3,0);

coadd_objects_id_2 number(11,0);
equinox_2          number(6,2);
htmid_2            number(16,0);
cx_2               number(10,6);
cy_2               number(10,6);
cz_2               number(10,6);
softid_2           number(4,0);
imageid_g_2        number(9,0);
imageid_r_2        number(9,0);
imageid_i_2        number(9,0);
imageid_z_2        number(9,0);
zeropoint_g_2      number(7,4);
zeropoint_r_2      number(7,4);
zeropoint_i_2      number(7,4);
zeropoint_z_2      number(7,4);
errzeropoint_g_2   number(7,4);
errzeropoint_r_2   number(7,4);
errzeropoint_i_2   number(7,4);
errzeropoint_z_2   number(7,4);
zeropointid_g_2    number(10,0);
zeropointid_r_2    number(10,0);
zeropointid_i_2    number(10,0);
zeropointid_z_2    number(10,0);
r_object_number_2  number(6,0);
mag_auto_g_2       number(7,4);
magerr_auto_g_2    number(7,4);
mag_aper1_g_2      number(7,4);
magerr_aper1_g_2   number(7,4);
mag_aper2_g_2      number(7,4);
magerr_aper2_g_2   number(7,4);
mag_aper3_g_2      number(7,4);
magerr_aper3_g_2   number(7,4);
mag_aper4_g_2      number(7,4);
magerr_aper4_g_2   number(7,4);
mag_aper5_g_2      number(7,4);
magerr_aper5_g_2   number(7,4);
mag_aper6_g_2      number(7,4);
magerr_aper6_g_2   number(7,4);
mag_auto_r_2       number(7,4);
magerr_auto_r_2    number(7,4);
mag_aper1_r_2      number(7,4);
magerr_aper1_r_2   number(7,4);
mag_aper2_r_2      number(7,4);
magerr_aper2_r_2   number(7,4);
mag_aper3_r_2      number(7,4);
magerr_aper3_r_2   number(7,4);
mag_aper4_r_2      number(7,4);
magerr_aper4_r_2   number(7,4);
mag_aper5_r_2      number(7,4);
magerr_aper5_r_2   number(7,4);
mag_aper6_r_2      number(7,4);
magerr_aper6_r_2   number(7,4);
mag_auto_i_2       number(7,4);
magerr_auto_i_2    number(7,4);
mag_aper1_i_2      number(7,4);
magerr_aper1_i_2   number(7,4);
mag_aper2_i_2      number(7,4);
magerr_aper2_i_2   number(7,4);
mag_aper3_i_2      number(7,4);
magerr_aper3_i_2   number(7,4);
mag_aper4_i_2      number(7,4);
magerr_aper4_i_2   number(7,4);
mag_aper5_i_2      number(7,4);
magerr_aper5_i_2   number(7,4);
mag_aper6_i_2      number(7,4);
magerr_aper6_i_2   number(7,4);
mag_auto_z_2       number(7,4);
magerr_auto_z_2    number(7,4);
mag_aper1_z_2      number(7,4);
magerr_aper1_z_2   number(7,4);
mag_aper2_z_2      number(7,4);
magerr_aper2_z_2   number(7,4);
mag_aper3_z_2      number(7,4);
magerr_aper3_z_2   number(7,4);
mag_aper4_z_2      number(7,4);
magerr_aper4_z_2   number(7,4);
mag_aper5_z_2      number(7,4);
magerr_aper5_z_2   number(7,4);
mag_aper6_z_2      number(7,4);
magerr_aper6_z_2   number(7,4);
alpha_j2000_2      number(8,5);
delta_j2000_2      number(8,5);
x2_world_2         number(11,3);
y2_world_2         number(11,3);
xy_world_2         number(11,3);
threshold_2        number(6,2);
x_image_2          number(6,2);
y_image_2          number(6,2);
theta_image_g_2    number(4,2);
errtheta_image_g_2 number(4,2);
ellipticity_g_2    number(5,4);
class_star_g_2     number(3,2);
flags_g_2          number(3,0);
theta_image_r_2    number(4,2);
errtheta_image_r_2 number(4,2);
ellipticity_r_2    number(5,4);
class_star_r_2     number(3,2);
flags_r_2          number(3,0);
theta_image_i_2    number(4,2);
errtheta_image_i_2 number(4,2);
ellipticity_i_2    number(5,4);
class_star_i_2     number(3,2);
flags_i_2          number(3,0);
theta_image_z_2    number(4,2);
errtheta_image_z_2 number(4,2);
ellipticity_z_2    number(5,4);
class_star_z_2     number(3,2);
flags_z_2          number(3,0);


cursor selectcsr is
  select
        coadd_objects_id  coadd_objects_id_1, equinox        equinox_1,
        htmid             htmid_1,            cx             cx_1,
        cy                cy_1,               cz             cz_1,
        softid            softid_1,
        imageid_g         imageid_g_1,        imageid_r      imageid_r_1,
        imageid_i         imageid_i_1,        imageid_z      imageid_z_1,
        zeropoint_g       zeropoint_g_1,      zeropoint_r    zeropoint_r_1,
        zeropoint_i       zeropoint_i_1,      zeropoint_z    zeropoint_z_1,
        errzeropoint_g    errzeropoint_g_1,   errzeropoint_r errzeropoint_r_1,
        errzeropoint_i    errzeropoint_i_1,   errzeropoint_z errzeropoint_z_1,
        zeropointid_g     zeropointid_g_1,    zeropointid_r  zeropointid_r_1,
        zeropointid_i     zeropointid_i_1,    zeropointid_z  zeropointid_z_1,
        r_object_number   r_object_number_1,
        mag_auto_g        mag_auto_g_1,       magerr_auto_g    magerr_auto_g_1,
        mag_aper1_g       mag_aper1_g_1,      magerr_aper1_g   magerr_aper1_g_1,
        mag_aper2_g       mag_aper2_g_1,      magerr_aper2_g   magerr_aper2_g_1,
        mag_aper3_g       mag_aper3_g_1,      magerr_aper3_g   magerr_aper3_g_1,
        mag_aper4_g       mag_aper4_g_1,      magerr_aper4_g   magerr_aper4_g_1,
        mag_aper5_g       mag_aper5_g_1,      magerr_aper5_g   magerr_aper5_g_1,
        mag_aper6_g       mag_aper6_g_1,      magerr_aper6_g   magerr_aper6_g_1,
        mag_auto_r        mag_auto_r_1,       magerr_auto_r    magerr_auto_r_1,
        mag_aper1_r       mag_aper1_r_1,      magerr_aper1_r   magerr_aper1_r_1,
        mag_aper2_r       mag_aper2_r_1,      magerr_aper2_r   magerr_aper2_r_1,
        mag_aper3_r       mag_aper3_r_1,      magerr_aper3_r   magerr_aper3_r_1,
        mag_aper4_r       mag_aper4_r_1,      magerr_aper4_r   magerr_aper4_r_1,
        mag_aper5_r       mag_aper5_r_1,      magerr_aper5_r   magerr_aper5_r_1,
        mag_aper6_r       mag_aper6_r_1,      magerr_aper6_r   magerr_aper6_r_1,
        mag_auto_i        mag_auto_i_1,       magerr_auto_i    magerr_auto_i_1,
        mag_aper1_i       mag_aper1_i_1,      magerr_aper1_i   magerr_aper1_i_1,
        mag_aper2_i       mag_aper2_i_1,      magerr_aper2_i   magerr_aper2_i_1,
        mag_aper3_i       mag_aper3_i_1,      magerr_aper3_i   magerr_aper3_i_1,
        mag_aper4_i       mag_aper4_i_1,      magerr_aper4_i   magerr_aper4_i_1,
        mag_aper5_i       mag_aper5_i_1,      magerr_aper5_i   magerr_aper5_i_1,
        mag_aper6_i       mag_aper6_i_1,      magerr_aper6_i   magerr_aper6_i_1,
        mag_auto_z        mag_auto_z_1,       magerr_auto_z    magerr_auto_z_1,
        mag_aper1_z       mag_aper1_z_1,      magerr_aper1_z   magerr_aper1_z_1,
        mag_aper2_z       mag_aper2_z_1,      magerr_aper2_z   magerr_aper2_z_1,
        mag_aper3_z       mag_aper3_z_1,      magerr_aper3_z   magerr_aper3_z_1,
        mag_aper4_z       mag_aper4_z_1,      magerr_aper4_z   magerr_aper4_z_1,
        mag_aper5_z       mag_aper5_z_1,      magerr_aper5_z   magerr_aper5_z_1,
        mag_aper6_z       mag_aper6_z_1,      magerr_aper6_z   magerr_aper6_z_1,
        alpha_j2000       alpha_j2000_1,      delta_j2000      delta_j2000_1,
        x2_world          x2_world_1,         y2_world         y2_world_1,
        xy_world          xy_world_1,
        threshold         threshold_1,        x_image          x_image_1,
        y_image           y_image_1,
        theta_image_g     theta_image_g_1,    errtheta_image_g errtheta_image_g_1,
        ellipticity_g     ellipticity_g_1,    class_star_g     class_star_g_1,
        flags_g           flags_g_1,          theta_image_r    theta_image_r_1,
        class_star_r      class_star_r_1,     flags_r          flags_r_1,
        theta_image_i     theta_image_i_1,    errtheta_image_i errtheta_image_i_1,
        ellipticity_i     ellipticity_i_1,    class_star_i     class_star_i_1,
        flags_i           flags_i_1,          theta_image_z    theta_image_z_1,
        class_star_z      class_star_z_1,     flags_z          flags_z_1,
        errtheta_image_z  errtheta_image_z_1, ellipticity_z    ellipticity_z_1,
        errtheta_image_r  errtheta_image_r_1, ellipticity_r    ellipticity_r_1
  from coadd_objects a
  where (imageid_g = image1
  or     imageid_r = image1
  or     imageid_i = image1
  or     imageid_z = image1)
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec;



cursor  matchcsr is
  select
        coadd_objects_id  coadd_objects_id_2, equinox          equinox_2,
        htmid             htmid_2,            cx               cx_2,
        cy                cy_2,               cz               cz_2,
        softid  softid_2,
        imageid_g         imageid_g_2,        imageid_r        imageid_r_2,
        imageid_i         imageid_i_2,        imageid_z        imageid_z_2,
        zeropoint_g       zeropoint_g_2,      zeropoint_r      zeropoint_r_2,
        zeropoint_i       zeropoint_i_2,      zeropoint_z      zeropoint_z_2,
        errzeropoint_g    errzeropoint_g_2,   errzeropoint_r   errzeropoint_r_2,
        errzeropoint_i    errzeropoint_i_2,   errzeropoint_z   errzeropoint_z_2,
        zeropointid_g     zeropointid_g_2,    zeropointid_r    zeropointid_r_2,
        zeropointid_i     zeropointid_i_2,    zeropointid_z    zeropointid_z_2,
        r_object_number   r_object_number_2,
        mag_auto_g        mag_auto_g_2,       magerr_auto_g    magerr_auto_g_2,
        mag_aper1_g       mag_aper1_g_2,      magerr_aper1_g   magerr_aper1_g_2,
        mag_aper2_g       mag_aper2_g_2,      magerr_aper2_g   magerr_aper2_g_2,
        mag_aper3_g       mag_aper3_g_2,      magerr_aper3_g   magerr_aper3_g_2,
        mag_aper4_g       mag_aper4_g_2,      magerr_aper4_g   magerr_aper4_g_2,
        mag_aper5_g       mag_aper5_g_2,      magerr_aper5_g   magerr_aper5_g_2,
        mag_aper6_g       mag_aper6_g_2,      magerr_aper6_g   magerr_aper6_g_2,
        mag_auto_r        mag_auto_r_2,       magerr_auto_r    magerr_auto_r_2,
        mag_aper1_r       mag_aper1_r_2,      magerr_aper1_r   magerr_aper1_r_2,
        mag_aper2_r       mag_aper2_r_2,      magerr_aper2_r   magerr_aper2_r_2,
        mag_aper3_r       mag_aper3_r_2,      magerr_aper3_r   magerr_aper3_r_2,
        mag_aper4_r       mag_aper4_r_2,      magerr_aper4_r   magerr_aper4_r_2,
        mag_aper5_r       mag_aper5_r_2,      magerr_aper5_r   magerr_aper5_r_2,
        mag_aper6_r       mag_aper6_r_2,      magerr_aper6_r   magerr_aper6_r_2,
        mag_auto_i        mag_auto_i_2,       magerr_auto_i    magerr_auto_i_2,
        mag_aper1_i       mag_aper1_i_2,      magerr_aper1_i   magerr_aper1_i_2,
        mag_aper2_i       mag_aper2_i_2,      magerr_aper2_i   magerr_aper2_i_2,
        mag_aper3_i       mag_aper3_i_2,      magerr_aper3_i   magerr_aper3_i_2,
        mag_aper4_i       mag_aper4_i_2,      magerr_aper4_i   magerr_aper4_i_2,
        mag_aper5_i       mag_aper5_i_2,      magerr_aper5_i   magerr_aper5_i_2,
        mag_aper6_i       mag_aper6_i_2,      magerr_aper6_i   magerr_aper6_i_2,
        mag_auto_z        mag_auto_z_2,       magerr_auto_z    magerr_auto_z_2,
        mag_aper1_z       mag_aper1_z_2,      magerr_aper1_z   magerr_aper1_z_2,
        mag_aper2_z       mag_aper2_z_2,      magerr_aper2_z   magerr_aper2_z_2,
        mag_aper3_z       mag_aper3_z_2,      magerr_aper3_z   magerr_aper3_z_2,
        mag_aper4_z       mag_aper4_z_2,      magerr_aper4_z   magerr_aper4_z_2,
        mag_aper5_z       mag_aper5_z_2,      magerr_aper5_z   magerr_aper5_z_2,
        mag_aper6_z       mag_aper6_z_2,      magerr_aper6_z   magerr_aper6_z_2,
        alpha_j2000       alpha_j2000_2,      delta_j2000      delta_j2000_2,
        x2_world          x2_world_2,         y2_world         y2_world_2,
        xy_world          xy_world_2,
        threshold         threshold_2,        x_image          x_image_2,
        y_image           y_image_2,
        theta_image_g     theta_image_g_2,    errtheta_image_g errtheta_image_g_2,
        ellipticity_g     ellipticity_g_2,    class_star_g     class_star_g_2,
        flags_g           flags_g_2,          theta_image_r    theta_image_r_2,
        class_star_r      class_star_r_2,     flags_r          flags_r_2,
        theta_image_i     theta_image_i_2,    errtheta_image_i errtheta_image_i_2,
        ellipticity_i     ellipticity_i_2,    class_star_i     class_star_i_2,
        flags_i           flags_i_2,          theta_image_z    theta_image_z_2,
        class_star_z      class_star_z_2,     flags_z          flags_z_2,
        errtheta_image_z  errtheta_image_z_2, ellipticity_z    ellipticity_z_2,
        errtheta_image_r  errtheta_image_r_2, ellipticity_r    ellipticity_r_2
  from coadd_objects a
  where (imageid_g = image2
  or     imageid_r = image2
  or     imageid_i = image2
  or     imageid_z = image2)
  and   (coadd_objects_id > coadd_objects_id_1
  or    coadd_objects_id < coadd_objects_id_1) 
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  -- using ra range and dec range to reduced the search range.
  and   alpha_j2000 >= alpha_j2000_1 - distance_threshold
  and   alpha_j2000 <= alpha_j2000_1 + distance_threshold
  and   delta_j2000 >= delta_j2000_1 - distance_threshold
  and   delta_j2000 <= delta_j2000_1 + distance_threshold;




-- end of setting up sql cursors

begin
  -- find the overlap part of these two images
  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra1, max_ra1, min_dec1, max_dec1
  from coadd_objects
  where imageid_g = image1
  or    imageid_r = image1
  or    imageid_i = image1
  or    imageid_z = image1;

  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra2, max_ra2, min_dec2, max_dec2
  from coadd_objects
  where imageid_g = image2
  or    imageid_r = image2
  or    imageid_i = image2
  or    imageid_z = image2;


  min_ra := greatest(min_ra1, min_ra2) - distance_threshold;
  max_ra := least(max_ra1, max_ra2) + distance_threshold;
  min_dec := greatest(min_dec1, min_dec2) - distance_threshold;
  max_dec := least(max_dec1, max_dec2) + distance_threshold;


  for selectcsr_rec in selectcsr
  loop
    begin
        coadd_objects_id_1    := selectcsr_rec.coadd_objects_id_1;
        equinox_1             := selectcsr_rec.equinox_1;
        htmid_1               := selectcsr_rec.htmid_1;
        cx_1                  := selectcsr_rec.cx_1;
        cy_1                  := selectcsr_rec.cy_1;
        cz_1                  := selectcsr_rec.cz_1;
        softid_1              := selectcsr_rec.softid_1;
        imageid_g_1           := selectcsr_rec.imageid_g_1;
        imageid_r_1           := selectcsr_rec.imageid_r_1;
        imageid_i_1           := selectcsr_rec.imageid_i_1;
        imageid_z_1           := selectcsr_rec.imageid_z_1;
        zeropoint_g_1         := selectcsr_rec.zeropoint_g_1;
        zeropoint_r_1         := selectcsr_rec.zeropoint_r_1;
        zeropoint_i_1         := selectcsr_rec.zeropoint_i_1;
        zeropoint_z_1         := selectcsr_rec.zeropoint_z_1;
        errzeropoint_g_1      := selectcsr_rec.errzeropoint_g_1;
        errzeropoint_r_1      := selectcsr_rec.errzeropoint_r_1;
        errzeropoint_i_1      := selectcsr_rec.errzeropoint_i_1;
        errzeropoint_z_1      := selectcsr_rec.errzeropoint_z_1;
        zeropointid_g_1       := selectcsr_rec.zeropointid_g_1;
        zeropointid_r_1       := selectcsr_rec.zeropointid_r_1;
        zeropointid_i_1       := selectcsr_rec.zeropointid_i_1;
        zeropointid_z_1       := selectcsr_rec.zeropointid_z_1;
        r_object_number_1     := selectcsr_rec.r_object_number_1;
        mag_auto_g_1          := selectcsr_rec.mag_auto_g_1;
        magerr_auto_g_1       := selectcsr_rec.magerr_auto_g_1;
        mag_aper1_g_1         := selectcsr_rec.mag_aper1_g_1;
        magerr_aper1_g_1      := selectcsr_rec.magerr_aper1_g_1;
        mag_aper2_g_1         := selectcsr_rec.mag_aper2_g_1;
        magerr_aper2_g_1      := selectcsr_rec.magerr_aper2_g_1;
        mag_aper3_g_1         := selectcsr_rec.mag_aper3_g_1;
        magerr_aper3_g_1      := selectcsr_rec.magerr_aper3_g_1;
        mag_aper4_g_1         := selectcsr_rec.mag_aper4_g_1;
        magerr_aper4_g_1      := selectcsr_rec.magerr_aper4_g_1;
        mag_aper5_g_1         := selectcsr_rec.mag_aper5_g_1;
        magerr_aper5_g_1      := selectcsr_rec.magerr_aper5_g_1;
        mag_aper6_g_1         := selectcsr_rec.mag_aper6_g_1;
        magerr_aper6_g_1      := selectcsr_rec.magerr_aper6_g_1;
        mag_auto_r_1          := selectcsr_rec.mag_auto_r_1;
        magerr_auto_r_1       := selectcsr_rec.magerr_auto_r_1;
        mag_aper1_r_1         := selectcsr_rec.mag_aper1_r_1;
        magerr_aper1_r_1      := selectcsr_rec.magerr_aper1_r_1;
        mag_aper2_r_1         := selectcsr_rec.mag_aper2_r_1;
        magerr_aper2_r_1      := selectcsr_rec.magerr_aper2_r_1;
        mag_aper3_r_1         := selectcsr_rec.mag_aper3_r_1;
        magerr_aper3_r_1      := selectcsr_rec.magerr_aper3_r_1;
        mag_aper4_r_1         := selectcsr_rec.mag_aper4_r_1;
        magerr_aper4_r_1      := selectcsr_rec.magerr_aper4_r_1;
        mag_aper5_r_1         := selectcsr_rec.mag_aper5_r_1;
        magerr_aper5_r_1      := selectcsr_rec.magerr_aper5_r_1;
        mag_aper6_r_1         := selectcsr_rec.mag_aper6_r_1;
        magerr_aper6_r_1      := selectcsr_rec.magerr_aper6_r_1;
        mag_auto_i_1          := selectcsr_rec.mag_auto_i_1;
        magerr_auto_i_1       := selectcsr_rec.magerr_auto_i_1;
        mag_aper1_i_1         := selectcsr_rec.mag_aper1_i_1;
        magerr_aper1_i_1      := selectcsr_rec.magerr_aper1_i_1;
        mag_aper2_i_1         := selectcsr_rec.mag_aper2_i_1;
        magerr_aper2_i_1      := selectcsr_rec.magerr_aper2_i_1;
        mag_aper3_i_1         := selectcsr_rec.mag_aper3_i_1;
        magerr_aper3_i_1      := selectcsr_rec.magerr_aper3_i_1;
        mag_aper4_i_1         := selectcsr_rec.mag_aper4_i_1;
        magerr_aper4_i_1      := selectcsr_rec.magerr_aper4_i_1;
        mag_aper5_i_1         := selectcsr_rec.mag_aper5_i_1;
        magerr_aper5_i_1      := selectcsr_rec.magerr_aper5_i_1;
        mag_aper6_i_1         := selectcsr_rec.mag_aper6_i_1;
        magerr_aper6_i_1      := selectcsr_rec.magerr_aper6_i_1;
        mag_auto_z_1          := selectcsr_rec.mag_auto_z_1;
        magerr_auto_z_1       := selectcsr_rec.magerr_auto_z_1;
        mag_aper1_z_1         := selectcsr_rec.mag_aper1_z_1;
        magerr_aper1_z_1      := selectcsr_rec.magerr_aper1_z_1;
        mag_aper2_z_1         := selectcsr_rec.mag_aper2_z_1;
        magerr_aper2_z_1      := selectcsr_rec.magerr_aper2_z_1;
        mag_aper3_z_1         := selectcsr_rec.mag_aper3_z_1;
        magerr_aper3_z_1      := selectcsr_rec.magerr_aper3_z_1;
        mag_aper4_z_1         := selectcsr_rec.mag_aper4_z_1;
        magerr_aper4_z_1      := selectcsr_rec.magerr_aper4_z_1;
        mag_aper5_z_1         := selectcsr_rec.mag_aper5_z_1;
        magerr_aper5_z_1      := selectcsr_rec.magerr_aper5_z_1;
        mag_aper6_z_1         := selectcsr_rec.mag_aper6_z_1;
        magerr_aper6_z_1      := selectcsr_rec.magerr_aper6_z_1;
        alpha_j2000_1         := selectcsr_rec.alpha_j2000_1;
        delta_j2000_1         := selectcsr_rec.delta_j2000_1;
        x2_world_1            := selectcsr_rec.x2_world_1;
        y2_world_1            := selectcsr_rec.y2_world_1;
        xy_world_1            := selectcsr_rec.xy_world_1;
        threshold_1           := selectcsr_rec.threshold_1;
        x_image_1             := selectcsr_rec.x_image_1;
        y_image_1             := selectcsr_rec.y_image_1;
        theta_image_g_1       := selectcsr_rec.theta_image_g_1;
        errtheta_image_g_1    := selectcsr_rec.errtheta_image_g_1;
        ellipticity_g_1       := selectcsr_rec.ellipticity_g_1;
        class_star_g_1        := selectcsr_rec.class_star_g_1;
        flags_g_1             := selectcsr_rec.flags_g_1;
        theta_image_r_1       := selectcsr_rec.theta_image_r_1;
        errtheta_image_r_1    := selectcsr_rec.errtheta_image_r_1;
        ellipticity_r_1       := selectcsr_rec.ellipticity_r_1;
        class_star_r_1        := selectcsr_rec.class_star_r_1;
        flags_r_1             := selectcsr_rec.flags_r_1;
        theta_image_i_1       := selectcsr_rec.theta_image_i_1;
        errtheta_image_i_1    := selectcsr_rec.errtheta_image_i_1;
        ellipticity_i_1       := selectcsr_rec.ellipticity_i_1;
        class_star_i_1        := selectcsr_rec.class_star_i_1;
        flags_i_1             := selectcsr_rec.flags_i_1;
        theta_image_z_1       := selectcsr_rec.theta_image_z_1;
        errtheta_image_z_1    := selectcsr_rec.errtheta_image_z_1;
        ellipticity_z_1       := selectcsr_rec.ellipticity_z_1;
        class_star_z_1        := selectcsr_rec.class_star_z_1;
        flags_z_1             := selectcsr_rec.flags_z_1;

        for matchcsr_rec in matchcsr
        loop
          begin
            distance := fgetdistancexyz(cx_1,cy_1,cz_1,
                      matchcsr_rec.cx_2,matchcsr_rec.cy_2,
                      matchcsr_rec.cz_2);


            if (distance < distance_threshold) then
                matchobj.coadd_objects_id_1 := coadd_objects_id_1;
                matchobj.equinox_1          := equinox_1;
                matchobj.htmid_1            := htmid_1;
                matchobj.cx_1               := cx_1;
                matchobj.cy_1               := cy_1;
                matchobj.cz_1               := cz_1;
                matchobj.softid_1           := softid_1;
                matchobj.imageid_g_1        := imageid_g_1;
                matchobj.imageid_r_1        := imageid_r_1;
                matchobj.imageid_i_1        := imageid_i_1;
                matchobj.imageid_z_1        := imageid_z_1;
                matchobj.zeropoint_g_1      := zeropoint_g_1;
                matchobj.zeropoint_r_1      := zeropoint_r_1;
                matchobj.zeropoint_i_1      := zeropoint_i_1;
                matchobj.zeropoint_z_1      := zeropoint_z_1;
                matchobj.errzeropoint_g_1   := errzeropoint_g_1;
                matchobj.errzeropoint_r_1   := errzeropoint_r_1;
                matchobj.errzeropoint_i_1   := errzeropoint_i_1;
                matchobj.errzeropoint_z_1   := errzeropoint_z_1;
                matchobj.zeropointid_g_1    := zeropointid_g_1;
                matchobj.zeropointid_r_1    := zeropointid_r_1;
                matchobj.zeropointid_i_1    := zeropointid_i_1;
                matchobj.zeropointid_z_1    := zeropointid_z_1;
                matchobj.r_object_number_1  := r_object_number_1;
                matchobj.mag_auto_g_1       := mag_auto_g_1;
                matchobj.magerr_auto_g_1    := magerr_auto_g_1;
                matchobj.mag_aper1_g_1      := mag_aper1_g_1;
                matchobj.magerr_aper1_g_1   := magerr_aper1_g_1;
                matchobj.mag_aper2_g_1      := mag_aper2_g_1;
                matchobj.magerr_aper2_g_1   := magerr_aper2_g_1;
                matchobj.mag_aper3_g_1      := mag_aper3_g_1;
                matchobj.magerr_aper3_g_1   := magerr_aper3_g_1;
                matchobj.mag_aper4_g_1      := mag_aper4_g_1;
                matchobj.magerr_aper4_g_1   := magerr_aper4_g_1;
                matchobj.mag_aper5_g_1      := mag_aper5_g_1;
                matchobj.magerr_aper5_g_1   := magerr_aper5_g_1;
                matchobj.mag_aper6_g_1      := mag_aper6_g_1;
                matchobj.magerr_aper6_g_1   := magerr_aper6_g_1;
                matchobj.mag_auto_r_1       := mag_auto_r_1;
                matchobj.magerr_auto_r_1    := magerr_auto_r_1;
                matchobj.mag_aper1_r_1      := mag_aper1_r_1;
                matchobj.magerr_aper1_r_1   := magerr_aper1_r_1;
                matchobj.mag_aper2_r_1      := mag_aper2_r_1;
                matchobj.magerr_aper2_r_1   := magerr_aper2_r_1;
                matchobj.mag_aper3_r_1      := mag_aper3_r_1;
                matchobj.magerr_aper3_r_1   := magerr_aper3_r_1;
                matchobj.mag_aper4_r_1      := mag_aper4_r_1;
                matchobj.magerr_aper4_r_1   := magerr_aper4_r_1;
                matchobj.mag_aper5_r_1      := mag_aper5_r_1;
                matchobj.magerr_aper5_r_1   := magerr_aper5_r_1;
                matchobj.mag_aper6_r_1      := mag_aper6_r_1;
                matchobj.magerr_aper6_r_1   := magerr_aper6_r_1;
                matchobj.mag_auto_i_1       := mag_auto_i_1;
                matchobj.magerr_auto_i_1    := magerr_auto_i_1;
                matchobj.mag_aper1_i_1      := mag_aper1_i_1;
                matchobj.magerr_aper1_i_1   := magerr_aper1_i_1;
                matchobj.mag_aper2_i_1      := mag_aper2_i_1;
                matchobj.magerr_aper2_i_1   := magerr_aper2_i_1;
                matchobj.mag_aper3_i_1      := mag_aper3_i_1;
                matchobj.magerr_aper3_i_1   := magerr_aper3_i_1;
                matchobj.mag_aper4_i_1      := mag_aper4_i_1;
                matchobj.magerr_aper4_i_1   := magerr_aper4_i_1;
                matchobj.mag_aper5_i_1      := mag_aper5_i_1;
                matchobj.magerr_aper5_i_1   := magerr_aper5_i_1;
                matchobj.mag_aper6_i_1      := mag_aper6_i_1;
                matchobj.magerr_aper6_i_1   := magerr_aper6_i_1;
                matchobj.mag_auto_z_1       := mag_auto_z_1;
                matchobj.magerr_auto_z_1    := magerr_auto_z_1;
                matchobj.mag_aper1_z_1      := mag_aper1_z_1;
                matchobj.magerr_aper1_z_1   := magerr_aper1_z_1;
                matchobj.mag_aper2_z_1      := mag_aper2_z_1;
                matchobj.magerr_aper2_z_1   := magerr_aper2_z_1;
                matchobj.mag_aper3_z_1      := mag_aper3_z_1;
                matchobj.magerr_aper3_z_1   := magerr_aper3_z_1;
                matchobj.mag_aper4_z_1      := mag_aper4_z_1;
                matchobj.magerr_aper4_z_1   := magerr_aper4_z_1;
                matchobj.mag_aper5_z_1      := mag_aper5_z_1;
                matchobj.magerr_aper5_z_1   := magerr_aper5_z_1;
                matchobj.mag_aper6_z_1      := mag_aper6_z_1;
                matchobj.magerr_aper6_z_1   := magerr_aper6_z_1;
                matchobj.alpha_j2000_1      := alpha_j2000_1;
                matchobj.delta_j2000_1      := delta_j2000_1;
                matchobj.x2_world_1         := x2_world_1;
                matchobj.y2_world_1         := y2_world_1;
                matchobj.xy_world_1         := xy_world_1;
                matchobj.threshold_1        := threshold_1;
                matchobj.x_image_1          := x_image_1;
                matchobj.y_image_1          := y_image_1;
                matchobj.theta_image_g_1    := theta_image_g_1;
                matchobj.errtheta_image_g_1 := errtheta_image_g_1;
                matchobj.ellipticity_g_1    := ellipticity_g_1;
                matchobj.class_star_g_1     := class_star_g_1;
                matchobj.flags_g_1          := flags_g_1;
                matchobj.theta_image_r_1    := theta_image_r_1;
                matchobj.errtheta_image_r_1 := errtheta_image_r_1;
                matchobj.ellipticity_r_1    := ellipticity_r_1;
                matchobj.class_star_r_1     := class_star_r_1;
                matchobj.flags_r_1          := flags_r_1;
                matchobj.theta_image_i_1    := theta_image_i_1;
                matchobj.errtheta_image_i_1 := errtheta_image_i_1;
                matchobj.ellipticity_i_1    := ellipticity_i_1;
                matchobj.class_star_i_1     := class_star_i_1;
                matchobj.flags_i_1          := flags_i_1;
                matchobj.theta_image_z_1    := theta_image_z_1;
                matchobj.errtheta_image_z_1 := errtheta_image_z_1;
                matchobj.ellipticity_z_1    := ellipticity_z_1;
                matchobj.class_star_z_1     := class_star_z_1;
                matchobj.flags_z_1          := flags_z_1;

                matchobj.coadd_objects_id_2 := matchcsr_rec.coadd_objects_id_2;
                matchobj.equinox_2          := matchcsr_rec.equinox_2;
                matchobj.htmid_2            := matchcsr_rec.htmid_2;
                matchobj.cx_2               := matchcsr_rec.cx_2;
                matchobj.cy_2               := matchcsr_rec.cy_2;
                matchobj.cz_2               := matchcsr_rec.cz_2;
                matchobj.softid_2           := matchcsr_rec.softid_2;
                matchobj.imageid_g_2        := matchcsr_rec.imageid_g_2;
                matchobj.imageid_r_2        := matchcsr_rec.imageid_r_2;
                matchobj.imageid_i_2        := matchcsr_rec.imageid_i_2;
                matchobj.imageid_z_2        := matchcsr_rec.imageid_z_2;
                matchobj.zeropoint_g_2      := matchcsr_rec.zeropoint_g_2;
                matchobj.zeropoint_r_2      := matchcsr_rec.zeropoint_r_2;
                matchobj.zeropoint_i_2      := matchcsr_rec.zeropoint_i_2;
                matchobj.zeropoint_z_2      := matchcsr_rec.zeropoint_z_2;
                matchobj.errzeropoint_g_2   := matchcsr_rec.errzeropoint_g_2;
                matchobj.errzeropoint_r_2   := matchcsr_rec.errzeropoint_r_2;
                matchobj.errzeropoint_i_2   := matchcsr_rec.errzeropoint_i_2;
                matchobj.errzeropoint_z_2   := matchcsr_rec.errzeropoint_z_2;
                matchobj.zeropointid_g_2    := matchcsr_rec.zeropointid_g_2;
                matchobj.zeropointid_r_2    := matchcsr_rec.zeropointid_r_2;
                matchobj.zeropointid_i_2    := matchcsr_rec.zeropointid_i_2;
                matchobj.zeropointid_z_2    := matchcsr_rec.zeropointid_z_2;
                matchobj.r_object_number_2  := matchcsr_rec.r_object_number_2;
                matchobj.mag_auto_g_2       := matchcsr_rec.mag_auto_g_2;
                matchobj.magerr_auto_g_2    := matchcsr_rec.magerr_auto_g_2;
                matchobj.mag_aper1_g_2      := matchcsr_rec.mag_aper1_g_2;
                matchobj.magerr_aper1_g_2   := matchcsr_rec.magerr_aper1_g_2;
                matchobj.mag_aper2_g_2      := matchcsr_rec.mag_aper2_g_2;
                matchobj.magerr_aper2_g_2   := matchcsr_rec.magerr_aper2_g_2;
                matchobj.mag_aper3_g_2      := matchcsr_rec.mag_aper3_g_2;
                matchobj.magerr_aper3_g_2   := matchcsr_rec.magerr_aper3_g_2;
                matchobj.mag_aper4_g_2      := matchcsr_rec.mag_aper4_g_2;
                matchobj.magerr_aper4_g_2   := matchcsr_rec.magerr_aper4_g_2;
                matchobj.mag_aper5_g_2      := matchcsr_rec.mag_aper5_g_2;
                matchobj.magerr_aper5_g_2   := matchcsr_rec.magerr_aper5_g_2;
                matchobj.mag_aper6_g_2      := matchcsr_rec.mag_aper6_g_2;
                matchobj.magerr_aper6_g_2   := matchcsr_rec.magerr_aper6_g_2;
                matchobj.mag_auto_r_2       := matchcsr_rec.mag_auto_r_2;
                matchobj.magerr_auto_r_2    := matchcsr_rec.magerr_auto_r_2;
                matchobj.mag_aper1_r_2      := matchcsr_rec.mag_aper1_r_2;
                matchobj.magerr_aper1_r_2   := matchcsr_rec.magerr_aper1_r_2;
                matchobj.mag_aper2_r_2      := matchcsr_rec.mag_aper2_r_2;
                matchobj.magerr_aper2_r_2   := matchcsr_rec.magerr_aper2_r_2;
                matchobj.mag_aper3_r_2      := matchcsr_rec.mag_aper3_r_2;
                matchobj.magerr_aper3_r_2   := matchcsr_rec.magerr_aper3_r_2;
                matchobj.mag_aper4_r_2      := matchcsr_rec.mag_aper4_r_2;
                matchobj.magerr_aper4_r_2   := matchcsr_rec.magerr_aper4_r_2;
                matchobj.mag_aper5_r_2      := matchcsr_rec.mag_aper5_r_2;
                matchobj.magerr_aper5_r_2   := matchcsr_rec.magerr_aper5_r_2;
                matchobj.mag_aper6_r_2      := matchcsr_rec.mag_aper6_r_2;
                matchobj.magerr_aper6_r_2   := matchcsr_rec.magerr_aper6_r_2;
                matchobj.mag_auto_i_2       := matchcsr_rec.mag_auto_i_2;
                matchobj.magerr_auto_i_2    := matchcsr_rec.magerr_auto_i_2;
                matchobj.mag_aper1_i_2      := matchcsr_rec.mag_aper1_i_2;
                matchobj.magerr_aper1_i_2   := matchcsr_rec.magerr_aper1_i_2;
                matchobj.mag_aper2_i_2      := matchcsr_rec.mag_aper2_i_2;
                matchobj.magerr_aper2_i_2   := matchcsr_rec.magerr_aper2_i_2;
                matchobj.mag_aper3_i_2      := matchcsr_rec.mag_aper3_i_2;
                matchobj.magerr_aper3_i_2   := matchcsr_rec.magerr_aper3_i_2;
                matchobj.mag_aper4_i_2      := matchcsr_rec.mag_aper4_i_2;
                matchobj.magerr_aper4_i_2   := matchcsr_rec.magerr_aper4_i_2;
                matchobj.mag_aper5_i_2      := matchcsr_rec.mag_aper5_i_2;
                matchobj.magerr_aper5_i_2   := matchcsr_rec.magerr_aper5_i_2;
                matchobj.mag_aper6_i_2      := matchcsr_rec.mag_aper6_i_2;
                matchobj.magerr_aper6_i_2   := matchcsr_rec.magerr_aper6_i_2;
                matchobj.mag_auto_z_2       := matchcsr_rec.mag_auto_z_2;
                matchobj.magerr_auto_z_2    := matchcsr_rec.magerr_auto_z_2;
                matchobj.mag_aper1_z_2      := matchcsr_rec.mag_aper1_z_2;
                matchobj.magerr_aper1_z_2   := matchcsr_rec.magerr_aper1_z_2;
                matchobj.mag_aper2_z_2      := matchcsr_rec.mag_aper2_z_2;
                matchobj.magerr_aper2_z_2   := matchcsr_rec.magerr_aper2_z_2;
                matchobj.mag_aper3_z_2      := matchcsr_rec.mag_aper3_z_2;
                matchobj.magerr_aper3_z_2   := matchcsr_rec.magerr_aper3_z_2;
                matchobj.mag_aper4_z_2      := matchcsr_rec.mag_aper4_z_2;
                matchobj.magerr_aper4_z_2   := matchcsr_rec.magerr_aper4_z_2;
                matchobj.mag_aper5_z_2      := matchcsr_rec.mag_aper5_z_2;
                matchobj.magerr_aper5_z_2   := matchcsr_rec.magerr_aper5_z_2;
                matchobj.mag_aper6_z_2      := matchcsr_rec.mag_aper6_z_2;
                matchobj.magerr_aper6_z_2   := matchcsr_rec.magerr_aper6_z_2;
                matchobj.alpha_j2000_2      := matchcsr_rec.alpha_j2000_2;
                matchobj.delta_j2000_2      := matchcsr_rec.delta_j2000_2;
                matchobj.x2_world_2         := matchcsr_rec.x2_world_2;
                matchobj.y2_world_2         := matchcsr_rec.y2_world_2;
                matchobj.xy_world_2         := matchcsr_rec.xy_world_2;
                matchobj.threshold_2        := matchcsr_rec.threshold_2;
                matchobj.x_image_2          := matchcsr_rec.x_image_2;
                matchobj.y_image_2          := matchcsr_rec.y_image_2;
                matchobj.theta_image_g_2    := matchcsr_rec.theta_image_g_2;
                matchobj.errtheta_image_g_2 := matchcsr_rec.errtheta_image_g_2;
                matchobj.ellipticity_g_2    := matchcsr_rec.ellipticity_g_2;
                matchobj.class_star_g_2     := matchcsr_rec.class_star_g_2;
                matchobj.flags_g_2          := matchcsr_rec.flags_g_2;
                matchobj.theta_image_r_2    := matchcsr_rec.theta_image_r_2;
                matchobj.errtheta_image_r_2 := matchcsr_rec.errtheta_image_r_2;
                matchobj.ellipticity_r_2    := matchcsr_rec.ellipticity_r_2;
                matchobj.class_star_r_2     := matchcsr_rec.class_star_r_2;
                matchobj.flags_r_2          := matchcsr_rec.flags_r_2;
                matchobj.theta_image_i_2    := matchcsr_rec.theta_image_i_2;
                matchobj.errtheta_image_i_2 := matchcsr_rec.errtheta_image_i_2;
                matchobj.ellipticity_i_2    := matchcsr_rec.ellipticity_i_2;
                matchobj.class_star_i_2     := matchcsr_rec.class_star_i_2;
                matchobj.flags_i_2          := matchcsr_rec.flags_i_2;
                matchobj.theta_image_z_2    := matchcsr_rec.theta_image_z_2;
                matchobj.errtheta_image_z_2 := matchcsr_rec.errtheta_image_z_2;
                matchobj.ellipticity_z_2    := matchcsr_rec.ellipticity_z_2;
                matchobj.class_star_z_2     := matchcsr_rec.class_star_z_2;
                matchobj.flags_z_2          := matchcsr_rec.flags_z_2;

              pipe row (matchobj);
            end if;
            exception
              when no_data_found then
                null;
          end;
        end loop;

        exception
          when no_data_found then
            null;
        end;
        end loop;

  commit;
  return;

end fMatchImages_coadd;
/

