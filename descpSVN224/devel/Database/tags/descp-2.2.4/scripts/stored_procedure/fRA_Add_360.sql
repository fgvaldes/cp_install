-- Check RA values that are negative and add 360 degrees to them to make the number positive
-- Done to remove negative RA values in that were in the datafile for ImSim2_truth table

create or replace function FRA_ADD_360(V_RA  number)
return number is
new_ra number;
begin 
	  if V_RA < 0 then
	  	 new_ra := V_RA + 360;
	  Else
	     new_ra := V_RA;
	  End if;
	  return new_ra;
end FRA_ADD_360;
/
