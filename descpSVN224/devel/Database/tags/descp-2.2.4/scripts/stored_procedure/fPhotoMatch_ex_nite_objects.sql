-- This program first get a list of images from the Files table and
-- the range of RA and DEC for the objects in each image.
-- Then this program searches the standard_stars table to get a list
-- of standard objects which are located in the designated area.
-- Using the RA and DEC of each standard objects, this program then 
-- find the nearest (within 1 arc second) observed objects from the Objects 
-- table. 
-- The program has 10 input parameters: image_type, image_name, nite, band,
-- ccd_number, mag_lo, mag_hi, detector, telescope and runiddesc. 
-- The wildcard '%' is allowed for the parameters nite, detector, telescope
-- and runiddesc. The program generates an output table which has 7 columns: 
-- Standard_star_id, object_id, airmass, exptime, 
-- mag_aper_3, magerr_aper_3, zeropoint.
-- to run the procedure: select * from table(fPhotoMatch_ex_nite_objects('remap', '%', 
-- 'scs20060903', 'r', 0, 15.0, 18.0, 'Mosaic2', 'Blanco 4m', 'SCS20070316%'));
-- If you want to process all CCDs, you should enter ccd_number=0.
create or replace function fPhotoMatch_ex_nite_objects(
image_type_in varchar,
image_name_in varchar,
nite_in varchar,
band_in varchar,
ccd_number_in number,
mag_lo_in float,
mag_hi_in float,
detector_in varchar,
telescope_in varchar,
runiddesc_in varchar)
return PMObjTab pipelined
as
pragma autonomous_transaction;

matchobj PMObj := PMObj(null,null,null,null,null,null,null);
image_name_like varchar2(100);
runiddesc_like varchar2(100);
max_ra float;
min_ra float;
max_dec float;
min_dec float;
imgid number(9);
ccd_num number(2);
band_val varchar2(8);
airmass_val float;
exptime_val float;

type list is RECORD (
  id number(11),
  radeg number(8,5),
  decdeg number(8,5),
  mag number(7,4),
  magerr number(7,4),
  zeropoint number(10));

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;

-- 0.017 arc minute = 1.0 arc second
close_r float := 0.017; 

-- cursor to get a list of images from files and wcsoffset
cursor filecsr is 
  select /*+ INDEX(des_admin.files files_nite_ccd_idx) */ 
       a.ra, a.dec, a.imageid, a.ccd_number, a.band, a.airmass, a.exptime
  from des_admin.files a
  where a.imagetype = image_type_in
  and   a.imagename like image_name_like
  and   a.nite = nite_in
  and   a.band = band_in
  and   (a.runiddesc is null or a.runiddesc like runiddesc_like);

-- cursor for retrieving standard objects from U band
cursor u_stdscsr is 
  select /*+ INDEX(standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from des_admin.standard_stars
  where stdmag_u between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from G band
cursor g_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from des_admin.standard_stars
  where stdmag_g between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from R band
cursor r_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from des_admin.standard_stars
  where stdmag_r between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from I band
cursor i_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from des_admin.standard_stars
  where stdmag_i between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving standard objects from Z band
cursor z_stdscsr is 
  select /*+ INDEX( standard_stars standard_stars_ra_dec_idx) */
       standard_star_id, radeg, decdeg, null, null, null
  from des_admin.standard_stars
  where stdmag_z between mag_lo_in and mag_hi_in
  and   (radeg between min_ra and max_ra)
  and   (decdeg between min_dec and max_dec);

-- cursor for retrieving objects from a selected image
cursor objscsr is 
  select b.object_id, b.alpha_j2000, b.delta_j2000,
         b.mag_aper_5, b.magerr_aper_5, b.zeropoint
  from ex_nite_objects b
  where b.mag_aper_5 < 99
  and   b.imageid = imgid
  and   b.magerr_aper_5 < 0.2 
  and   b.flags <= 2;

begin
  image_name_like := '%' || image_name_in || '%';
  runiddesc_like := '%' || runiddesc_in || '%';
  for filecsr_rec in filecsr
  loop
   -- ccd_number_in = 0 means to process all CCDs
   if ((ccd_number_in = 0) or (filecsr_rec.ccd_number = ccd_number_in)) then
    imgid := filecsr_rec.imageid;
    --dbms_output.put_line('imgid is ' || imgid);
    ccd_num := filecsr_rec.ccd_number;
    band_val := filecsr_rec.band;
    airmass_val := filecsr_rec.airmass;
    exptime_val := filecsr_rec.exptime;
   
    -- for each image, get the max_ra, min_ra, max_dec and min_dec
    select min(alpha_j2000), max(alpha_j2000), min(delta_j2000), 
           max(delta_j2000) 
    into min_ra, max_ra, min_dec, max_dec
    from ex_nite_objects
    where imageid = imgid;
    --dbms_output.put_line('range1 is ' || min_ra || ',' || max_ra || ',' || min_dec || ',' || max_dec);
    -- requested by Choong, add 2 arcseconds (2/3600) to RA and DEC boundary.
    min_ra := min_ra - (2/3600);
    max_ra := max_ra + (2/3600);
    min_dec := min_dec - (2/3600);
    max_dec := max_dec + (2/3600);
    --dbms_output.put_line('range2 is ' || min_ra || ',' || max_ra || ',' || min_dec || ',' || max_dec);
    -- for each standard object, find the nearest matching observed 
    -- object from table Objects. Those objects must come from the designated
    -- images.
    if (band_val = 'u') then
      open u_stdscsr;
      open objscsr;

      fetch u_stdscsr bulk collect into collect1;
      fetch objscsr bulk collect into collect2;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_3 := collect2(j).mag;
            matchobj.magerr_aper_3 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      close u_stdscsr;
      close objscsr;

    elsif (band_val = 'g') then
      open g_stdscsr;
      open objscsr;

      fetch g_stdscsr bulk collect into collect1;
      fetch objscsr bulk collect into collect2;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_3 := collect2(j).mag;
            matchobj.magerr_aper_3 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      close g_stdscsr;
      close objscsr;

    elsif (band_val = 'r') then
      open r_stdscsr;
      open objscsr;

      fetch r_stdscsr bulk collect into collect1;
      fetch objscsr bulk collect into collect2;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_3 := collect2(j).mag;
            matchobj.magerr_aper_3 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      close r_stdscsr;
      close objscsr;

    elsif (band_val = 'i') then
      open i_stdscsr;
      open objscsr;

      fetch i_stdscsr bulk collect into collect1;
      fetch objscsr bulk collect into collect2;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_3 := collect2(j).mag;
            matchobj.magerr_aper_3 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      close i_stdscsr;
      close objscsr;

    elsif (band_val = 'z') then
      open z_stdscsr;
      open objscsr;

      fetch z_stdscsr bulk collect into collect1;
      fetch objscsr bulk collect into collect2;

      for i in 1 .. collect1.count
      loop
        for j in 1 .. collect2.count
        loop
          --dbms_output.put_line('in the loop, ' || i || ' and ' || j);
          if( collect2(j).radeg >= (collect1(i).radeg - close_r)
            and collect2(j).radeg <= (collect1(i).radeg + close_r)
            and collect2(j).decdeg >= (collect1(i).decdeg - close_r)
            and collect2(j).decdeg <= (collect1(i).decdeg + close_r)
            and (fgetdistanceEQ(collect1(i).radeg,collect1(i).decdeg,
                 collect2(j).radeg,collect2(j).decdeg) < close_r))       
          then
            matchobj.object_id := collect2(j).id;
            matchobj.mag_aper_3 := collect2(j).mag;
            matchobj.magerr_aper_3 := collect2(j).magerr;
            matchobj.zeropoint := collect2(j).zeropoint;
            matchobj.standard_star_id := collect1(i).id;
            matchobj.airmass := airmass_val; 
            matchobj.exptime := exptime_val;   
          
            pipe row (matchobj);
            exit;
          end if;
        end loop;
      end loop;     
      close z_stdscsr;
      close objscsr;
    end if; -- end of band_val
   end if; -- end of if ccd_number is valid
  end loop; -- end of filecsr 

  commit;
  return;
    
end fPhotoMatch_ex_nite_objects;
/
