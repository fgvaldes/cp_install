create or replace procedure UpdateRA_HTMID(
start_obj number, end_obj number) as

start_id number(11) := start_obj;
increm number(11) := 5000;

begin

  while (start_id <= end_obj)
  loop
    update usnob_cat1
    set old_ra = ra,
        ra = (ra * 15),
        htmid = fEqToHtm((ra*15), dec) ,
        cx = fEqToX(ra*15, dec),
        cy = fEqToY(ra*15, dec),
        cz = fEqToZ(ra*15, dec) 
    where usnob_cat1_id >= start_id
     and usnob_cat1_id < (start_id + increm)
     and old_ra is null;

    commit;

    start_id  := start_id + increm;

  end loop;

dbms_output.put_line ('start_obj is :' || start_obj);
dbms_output.put_line ('end_obj is :' || end_obj);

end UpdateRA_HTMID;
/
