-- compute the coordinate Y for the given ra and dec
-- to run the function: select fEqToY(11.10, -2.30) from dual;
-- note: VARRAY subscript starts at 1, not 0
create or replace function fEqToY(
ra number,
dec number)
return float as

y float;
cd float;
pr float := 3.1415926535897932385E0/180.0;

begin
  cd := cos(dec * pr);
  y := sin(ra * pr) * cd;

  return y;  
  
end fEqToY;
/
