create type vector as array (3) of float;
/

create type HtmBase as object
(name varchar2(2),
 ID   number(2),
 v1   number(1),
 v2   number(1),
 v3   number(1));
/

create type HtmBaseTab as array (8) of HTMBase;
/

create type vectorTab as array(6) of vector;
/

create type range as object
(htm_id_start number(16),
 htm_id_end number(16));
/

create type rangeTab as table of range;
/

create type matchedObj as object
(object_id number(11),
 htmid number(16),
 cx float,
 cy float,
 cz float,
 distance float);
/

create type matchedObjTab as table of matchedObj;
/

create type matchTruthObj as object
(t_obj_id number(12),
 o_obj_id number(12),
 t_alpha_j2000 number(8,5),
 t_delta_j2000 number(8,5),
 distance_ra float,
 distance_dec float,
 distance float,
 o_mag_auto float,
 o_magerr_auto float,
 mag_diff float);
/

create type matchTruthObjTab as table of matchTruthObj;
/

create type truthObj as object
(t_obj_id number(12),
 t_alpha_j2000 number(8,5),
 t_delta_j2000 number(8,5),
 t_mag_auto number(8,5));
/

create type objectHead as object
(object_id number(11),
 htmid number(16),
 ra float,
 dec float,
 cx float,
 cy float,
 cz float,
 distance float);
/

create type objectHeadTab as table of objectHead;
/

-- objects for photometric match 
create type PMObj as object
(standard_star_id number(10),
 object_id number(11),
 airmass number(4,3),
 exptime number(6,2),
 mag_aper_5 number(7,4),
 magerr_aper_5 number(7,4),
 zeropoint number(7,4));
/

-- a table of objects for photometric match
create type PMObjTab as table of PMObj;
/

-- the mapping of selected table object with matched table object
create type objectMap as object
(select_object_id number(11),
 match_object_id number(11));
/

-- a table of objectMap
create type objectMapTab as table of objectMap;
/

-- Dora changed on 08/22/2009 8AM: change all number(7,4) to number(10,7)
-- Dora changed on 08/22/2009 4PM: change all number(10,7) to binary_float
-- Dora changed on 09/23/2009: change class_star to binary_float
-- Dora changed on 02/12/2010: add 4 columns - mag_model and magerr_model for two objects.
-- matched objects in two images
create type MatchImgObj as object
(imageid_1 number(9),
 object_id_1 number(11),
 band_1 varchar2(10),
 class_star_1 binary_float,
 flags_1 number(3),
 mag_auto_1 binary_float,
 magerr_auto_1 binary_float,
 mag_aper_1_1 binary_float,
 magerr_aper_1_1 binary_float,
 mag_aper_2_1 binary_float,
 magerr_aper_2_1 binary_float,
 mag_aper_3_1 binary_float,
 magerr_aper_3_1 binary_float,
 mag_aper_4_1 binary_float,
 magerr_aper_4_1 binary_float,
 mag_aper_5_1 binary_float,
 magerr_aper_5_1 binary_float,
 mag_aper_6_1 binary_float,
 magerr_aper_6_1 binary_float,
 mag_aper_7_1 binary_float,
 magerr_aper_7_1 binary_float,
 mag_aper_8_1 binary_float,
 magerr_aper_8_1 binary_float,
 mag_aper_9_1 binary_float,
 magerr_aper_9_1 binary_float,
 mag_aper_10_1 binary_float,
 magerr_aper_10_1 binary_float,
 mag_aper_11_1 binary_float,
 magerr_aper_11_1 binary_float,
 mag_aper_12_1 binary_float,
 magerr_aper_12_1 binary_float,
 mag_aper_13_1 binary_float,
 magerr_aper_13_1 binary_float,
 mag_aper_14_1 binary_float,
 magerr_aper_14_1 binary_float,
 mag_aper_15_1 binary_float,
 magerr_aper_15_1 binary_float,
 mag_aper_16_1 binary_float,
 magerr_aper_16_1 binary_float,
 mag_aper_17_1 binary_float,
 magerr_aper_17_1 binary_float,
 imageid_2 number(9),
 object_id_2 number(11),
 band_2 varchar2(10),
 class_star_2 binary_float,
 flags_2 number(3),
 mag_auto_2 binary_float,
 magerr_auto_2 binary_float,
 mag_aper_1_2 binary_float,
 magerr_aper_1_2 binary_float,
 mag_aper_2_2 binary_float,
 magerr_aper_2_2 binary_float,
 mag_aper_3_2 binary_float,
 magerr_aper_3_2 binary_float,
 mag_aper_4_2 binary_float,
 magerr_aper_4_2 binary_float,
 mag_aper_5_2 binary_float,
 magerr_aper_5_2 binary_float,
 mag_aper_6_2 binary_float,
 magerr_aper_6_2 binary_float,
 mag_aper_7_2 binary_float,
 magerr_aper_7_2 binary_float,
 mag_aper_8_2 binary_float,
 magerr_aper_8_2 binary_float,
 mag_aper_9_2 binary_float,
 magerr_aper_9_2 binary_float,
 mag_aper_10_2 binary_float,
 magerr_aper_10_2 binary_float,
 mag_aper_11_2 binary_float,
 magerr_aper_11_2 binary_float,
 mag_aper_12_2 binary_float,
 magerr_aper_12_2 binary_float,
 mag_aper_13_2 binary_float,
 magerr_aper_13_2 binary_float,
 mag_aper_14_2 binary_float,
 magerr_aper_14_2 binary_float,
 mag_aper_15_2 binary_float,
 magerr_aper_15_2 binary_float,
 mag_aper_16_2 binary_float,
 magerr_aper_16_2 binary_float,
 mag_aper_17_2 binary_float,
 magerr_aper_17_2 binary_float);
/

-- a table of MatchImgObj
create type MatchImgObjTab as table of MatchImgObj;
/

-- data type for fMatchImages
create type Match2ImgObj as object
(imageid_1 number(9),
 object_id_1 number(11),
 band_1 varchar2(10),
 spread_model_1 binary_float,
 flags_1 number(3),
 mag_auto_1 binary_float,
 magerr_auto_1 binary_float,
 mag_aper_1_1 binary_float,
 magerr_aper_1_1 binary_float,
 mag_aper_2_1 binary_float,
 magerr_aper_2_1 binary_float,
 mag_aper_3_1 binary_float,
 magerr_aper_3_1 binary_float,
 mag_aper_4_1 binary_float,
 magerr_aper_4_1 binary_float,
 mag_aper_5_1 binary_float,
 magerr_aper_5_1 binary_float,
 mag_aper_6_1 binary_float,
 magerr_aper_6_1 binary_float,
 mag_aper_7_1 binary_float,
 magerr_aper_7_1 binary_float,
 mag_aper_8_1 binary_float,
 magerr_aper_8_1 binary_float,
 mag_aper_9_1 binary_float,
 magerr_aper_9_1 binary_float,
 mag_aper_10_1 binary_float,
 magerr_aper_10_1 binary_float,
 mag_aper_11_1 binary_float,
 magerr_aper_11_1 binary_float,
 mag_aper_12_1 binary_float,
 magerr_aper_12_1 binary_float,
 mag_aper_13_1 binary_float,
 magerr_aper_13_1 binary_float,
 mag_aper_14_1 binary_float,
 magerr_aper_14_1 binary_float,
 mag_aper_15_1 binary_float,
 magerr_aper_15_1 binary_float,
 mag_aper_16_1 binary_float,
 magerr_aper_16_1 binary_float,
 mag_aper_17_1 binary_float,
 magerr_aper_17_1 binary_float,
 mag_model_1 binary_float,
 magerr_model_1 binary_float,
 imageid_2 number(9),
 object_id_2 number(11),
 band_2 varchar2(10),
 spread_model_2 binary_float,
 flags_2 number(3),
 mag_auto_2 binary_float,
 magerr_auto_2 binary_float,
 mag_aper_1_2 binary_float,
 magerr_aper_1_2 binary_float,
 mag_aper_2_2 binary_float,
 magerr_aper_2_2 binary_float,
 mag_aper_3_2 binary_float,
 magerr_aper_3_2 binary_float,
 mag_aper_4_2 binary_float,
 magerr_aper_4_2 binary_float,
 mag_aper_5_2 binary_float,
 magerr_aper_5_2 binary_float,
 mag_aper_6_2 binary_float,
 magerr_aper_6_2 binary_float,
 mag_aper_7_2 binary_float,
 magerr_aper_7_2 binary_float,
 mag_aper_8_2 binary_float,
 magerr_aper_8_2 binary_float,
 mag_aper_9_2 binary_float,
 magerr_aper_9_2 binary_float,
 mag_aper_10_2 binary_float,
 magerr_aper_10_2 binary_float,
 mag_aper_11_2 binary_float,
 magerr_aper_11_2 binary_float,
 mag_aper_12_2 binary_float,
 magerr_aper_12_2 binary_float,
 mag_aper_13_2 binary_float,
 magerr_aper_13_2 binary_float,
 mag_aper_14_2 binary_float,
 magerr_aper_14_2 binary_float,
 mag_aper_15_2 binary_float,
 magerr_aper_15_2 binary_float,
 mag_aper_16_2 binary_float,
 magerr_aper_16_2 binary_float,
 mag_aper_17_2 binary_float,
 magerr_aper_17_2 binary_float,
 mag_model_2 binary_float,
 magerr_model_2 binary_float);
/

-- a table of Match2ImgObj
create type Match2ImgObjTab as table of Match2ImgObj;
/

-- data type for fMatchImages_ClassStar
create type Match2ImgObj_ClassStar as object
(imageid_1 number(9),
 object_id_1 number(11),
 band_1 varchar2(10),
 class_star_1 binary_float,
 flags_1 number(3),
 mag_auto_1 binary_float,
 magerr_auto_1 binary_float,
 mag_aper_1_1 binary_float,
 magerr_aper_1_1 binary_float,
 mag_aper_2_1 binary_float,
 magerr_aper_2_1 binary_float,
 mag_aper_3_1 binary_float,
 magerr_aper_3_1 binary_float,
 mag_aper_4_1 binary_float,
 magerr_aper_4_1 binary_float,
 mag_aper_5_1 binary_float,
 magerr_aper_5_1 binary_float,
 mag_aper_6_1 binary_float,
 magerr_aper_6_1 binary_float,
 mag_aper_7_1 binary_float,
 magerr_aper_7_1 binary_float,
 mag_aper_8_1 binary_float,
 magerr_aper_8_1 binary_float,
 mag_aper_9_1 binary_float,
 magerr_aper_9_1 binary_float,
 mag_aper_10_1 binary_float,
 magerr_aper_10_1 binary_float,
 mag_aper_11_1 binary_float,
 magerr_aper_11_1 binary_float,
 mag_aper_12_1 binary_float,
 magerr_aper_12_1 binary_float,
 mag_aper_13_1 binary_float,
 magerr_aper_13_1 binary_float,
 mag_aper_14_1 binary_float,
 magerr_aper_14_1 binary_float,
 mag_aper_15_1 binary_float,
 magerr_aper_15_1 binary_float,
 mag_aper_16_1 binary_float,
 magerr_aper_16_1 binary_float,
 mag_aper_17_1 binary_float,
 magerr_aper_17_1 binary_float,
 mag_model_1 binary_float,
 magerr_model_1 binary_float,
 imageid_2 number(9),
 object_id_2 number(11),
 band_2 varchar2(10),
 class_star_2 binary_float,
 flags_2 number(3),
 mag_auto_2 binary_float,
 magerr_auto_2 binary_float,
 mag_aper_1_2 binary_float,
 magerr_aper_1_2 binary_float,
 mag_aper_2_2 binary_float,
 magerr_aper_2_2 binary_float,
 mag_aper_3_2 binary_float,
 magerr_aper_3_2 binary_float,
 mag_aper_4_2 binary_float,
 magerr_aper_4_2 binary_float,
 mag_aper_5_2 binary_float,
 magerr_aper_5_2 binary_float,
 mag_aper_6_2 binary_float,
 magerr_aper_6_2 binary_float,
 mag_aper_7_2 binary_float,
 magerr_aper_7_2 binary_float,
 mag_aper_8_2 binary_float,
 magerr_aper_8_2 binary_float,
 mag_aper_9_2 binary_float,
 magerr_aper_9_2 binary_float,
 mag_aper_10_2 binary_float,
 magerr_aper_10_2 binary_float,
 mag_aper_11_2 binary_float,
 magerr_aper_11_2 binary_float,
 mag_aper_12_2 binary_float,
 magerr_aper_12_2 binary_float,
 mag_aper_13_2 binary_float,
 magerr_aper_13_2 binary_float,
 mag_aper_14_2 binary_float,
 magerr_aper_14_2 binary_float,
 mag_aper_15_2 binary_float,
 magerr_aper_15_2 binary_float,
 mag_aper_16_2 binary_float,
 magerr_aper_16_2 binary_float,
 mag_aper_17_2 binary_float,
 magerr_aper_17_2 binary_float,
 mag_model_2 binary_float,
 magerr_model_2 binary_float);
/

-- a table of Match2ImgObj_ClassStar
create type Match2ImgObj_ClassStarTab as table of Match2ImgObj_ClassStar;
/

-- an image record with id, name, archivesites
create type img as object
(imageid number(9),
 imagename varchar2(80),
 archivesite varchar2(80));
/

-- a table of img
create type ImageList as table of img;
/

create type CircleObj as object
(object_id number(11),
 alpha_j2000 float,
 delta_j2000 float,
 band varchar2(10),
 imageid number(9),
 distance float);
/

create type CircleObjTab as table of CircleObj;
/

-- matched objects in two coadd images
create type MatchCoaddObj as object(
  object1 number(11),
  object2 number(11),
  mag1 float,
  mag2 float,
  magerr1 float,
  magerr2 float,
  flag1 number(2),
  flag2 number(2),
  class_star1 float,
  class_star2 float,
  ra1 float,
  ra2 float,
  dec1 float,
  dec2 float);
/

-- a table of MatchCoaddObj
create type MatchCoaddObjTab as table of MatchCoaddObj;
/

-- matched coadd objects with single objects 
create type MatchCddSglObj as object(
  coadd_objects_id number(11),
  object_id number(11),
  offset float,
  ra float,
  dec float);
/

-- a table of MatchCddSglObj
create type MatchCddSglObjTbl as table of MatchCddSglObj;
/

