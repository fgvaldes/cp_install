-- Check newly added and removed nite identifiers and run identifiers 
create or replace procedure CheckAccessTbls
as

nite_id varchar(20);
run_id varchar(100);
project_id varchar(20);

cursor nite_add_cr is 
  select distinct a.nite, a.project
  from location a
  where a.filedate > (sysdate - 10)
  and   a.project in ('BCS', 'DES', 'SCS')
  and   not exists (select distinct b.nite, b.project
                    from nite_access b
                    where a.nite = b.nite
                    and   a.project = b.project
                    and   b.des_group = 'all');

cursor run_add_cr is 
  select distinct a.run
  from location a
  where a.filedate > (sysdate - 10)
  and   a.run is not null
  and   a.project in ('BCS', 'DES', 'SCS')
  and   not exists (select distinct b.runiddesc
                    from runiddesc_access b
                    where a.run = b.runiddesc
                    and   b.des_group = 'all');

cursor nite_del_cr is 
  select distinct a.nite, a.project
  from nite_access a
  where a.des_group = 'all'
  and   not exists (select distinct b.nite, b.project
                    from location b
                    where a.nite = b.nite
                    and   a.project = b.project
                    and   b.project in ('BCS', 'DES', 'SCS'));

cursor run_del_cr is 
  select distinct a.runiddesc
  from runiddesc_access a
  where a.des_group = 'all'
  and   not exists (select distinct b.run
                    from location b
                    where a.runiddesc = b.run
                    and   b.project in ('BCS', 'DES', 'SCS'));

begin
  dbms_output.put_line ('Nite need to add: ');
  open nite_add_cr;
  loop
    fetch nite_add_cr into nite_id, project_id;
    exit when nite_add_cr%NOTFOUND;
    dbms_output.put_line ('    ' || nite_id);
  end loop;
  
  dbms_output.put_line ('Run need to add: ');
  open run_add_cr;
  loop
    fetch run_add_cr into run_id;
    exit when run_add_cr%NOTFOUND;
    dbms_output.put_line ('    ' || run_id);
  end loop;
  
  dbms_output.put_line ('Nite need to delete: ');
  open nite_del_cr;
  loop
    fetch nite_del_cr into nite_id, project_id;
    exit when nite_del_cr%NOTFOUND;
    dbms_output.put_line ('    ' || nite_id);
  end loop;
  
  dbms_output.put_line ('Run need to delete: ');
  open run_del_cr;
  loop
    fetch run_del_cr into run_id;
    exit when run_del_cr%NOTFOUND;
    dbms_output.put_line ('    ' || run_id);
  end loop;
  
end CheckAccessTbls;
/
