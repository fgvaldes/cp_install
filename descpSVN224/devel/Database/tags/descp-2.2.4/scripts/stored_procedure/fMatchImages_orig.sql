-- This stored procedure matches the objects in two images
-- The program has 7 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. stella_lo (minimum value for class_star)
-- 4. stella_hi (maximum value for class_star)
-- 5. flag_upperlim (upperlim for flags)
-- 6. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- 7. magerr_filter (magnitude error filter used: magerr_auto, magerr_aper_1, etc)
-- 8. magerr_limit (magnitude error limit, only the objects with error < magerr_limit will be matched)
-- The program returns a table which has 36 columns:
-- object_id_1, band_1, class_star_1, flags_1, mag_auto_1, magerr_auto_1, 
-- mag_aper_1_1, magerr_aper_1_1, mag_aper_2_1, magerr_aper_2_1,
-- mag_aper_3_1, magerr_aper_3_1, mag_aper_4_1, magerr_aper_4_1,
-- mag_aper_5_1, magerr_aper_5_1, mag_aper_6_1, magerr_aper_6_1,
-- object_id_2, band_2, class_star_2, flags_2, mag_auto_2, magerr_auto_2, 
-- mag_aper_1_2, magerr_aper_1_2, mag_aper_2_2, magerr_aper_2_2,
-- mag_aper_3_2, magerr_aper_3_2, mag_aper_4_2, magerr_aper_4_2,
-- mag_aper_5_2, magerr_aper_5_2, mag_aper_6_2, magerr_aper_6_2
-- to match image 1627161 and image 1553539 with class_star from 0.75 to 1.0,
-- flag upperlimit 0, matching tolerance 2 arc seconds, using 'magerr_aper_5' as
-- error filter and magnitude error < 0.10:
-- select * from table(fMatchImages(1627161, 1553539, 0.75, 1.0, 0, 0.032, 'magerr_aper_5', 0.10));

create or replace function fMatchImages(
image1 number,
image2 number, 
stella_lo float,
stella_hi float,
flag_upperlim number,
distance_threshold float,
magerr_filter varchar,
magerr_limit float)
return MatchImgObjTab pipelined
as
pragma autonomous_transaction;

matchObj MatchImgObj := MatchImgObj(null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null);
min_ra1 float;
max_ra1 float;
min_dec1 float;
max_dec1 float;
min_ra2 float;
max_ra2 float;
min_dec2 float;
max_dec2 float;
min_ra float;
max_ra float;
min_dec float;
max_dec float;
imageid_1 number(9);
object_id_1 number(11);
mag_auto_1 float;
magerr_auto_1 float;
mag_aper_1_1 float;
magerr_aper_1_1 float;
mag_aper_2_1 float;
magerr_aper_2_1 float;
mag_aper_3_1 float;
magerr_aper_3_1 float;
mag_aper_4_1 float;
magerr_aper_4_1 float;
mag_aper_5_1 float;
magerr_aper_5_1 float;
mag_aper_6_1 float;
magerr_aper_6_1 float;
select_x float;
select_y float;
select_z float;
band_1 varchar2(10);
class_star_1 number(3,2);
flags_1 number(3);
select_ra float;
select_dec float;
distance float;

cursor selectCsr_auto is 
  select imageid imageid_1, object_id object_id_1, 
         mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1, 
         alpha_j2000, delta_j2000  
  from objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_auto < magerr_limit; 

cursor selectCsr_1 is 
  select imageid imageid_1, object_id object_id_1, 
         mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1, 
         alpha_j2000, delta_j2000  
  from objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_1 < magerr_limit; 

cursor selectCsr_2 is 
  select imageid imageid_1, object_id object_id_1, 
         mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1, 
         alpha_j2000, delta_j2000  
  from objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_2 < magerr_limit; 

cursor selectCsr_3 is 
  select imageid imageid_1, object_id object_id_1, 
         mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1, 
         alpha_j2000, delta_j2000  
  from objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_3 < magerr_limit; 

cursor selectCsr_4 is 
  select imageid imageid_1, object_id object_id_1, 
         mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1, 
         alpha_j2000, delta_j2000  
  from objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_4 < magerr_limit; 

cursor selectCsr_5 is 
  select imageid imageid_1, object_id object_id_1, 
         mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1, 
         alpha_j2000, delta_j2000  
  from objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_5 < magerr_limit; 

cursor selectCsr_6 is 
  select imageid imageid_1, object_id object_id_1, 
         mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1, 
         alpha_j2000, delta_j2000  
  from objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_6 < magerr_limit; 

cursor matchCsr_auto is 
  select imageid imageid_2, object_id object_id_2, 
         mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_auto < magerr_limit
  -- using RA range and DEC range to reduced the search range. 
  and   alpha_j2000 >= select_ra - distance_threshold
  and   alpha_j2000 <= select_ra + distance_threshold
  and   delta_j2000 >= select_dec - distance_threshold
  and   delta_j2000 <= select_dec + distance_threshold
; 

cursor matchCsr_1 is 
  select imageid imageid_2, object_id object_id_2, 
         mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_1 < magerr_limit
  -- using RA range and DEC range to reduced the search range. 
  and   alpha_j2000 >= select_ra - distance_threshold
  and   alpha_j2000 <= select_ra + distance_threshold
  and   delta_j2000 >= select_dec - distance_threshold
  and   delta_j2000 <= select_dec + distance_threshold
; 

cursor matchCsr_2 is 
  select imageid imageid_2, object_id object_id_2, 
         mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_2 < magerr_limit
  -- using RA range and DEC range to reduced the search range. 
  and   alpha_j2000 >= select_ra - distance_threshold
  and   alpha_j2000 <= select_ra + distance_threshold
  and   delta_j2000 >= select_dec - distance_threshold
  and   delta_j2000 <= select_dec + distance_threshold
; 

cursor matchCsr_3 is 
  select imageid imageid_2, object_id object_id_2, 
         mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_3 < magerr_limit
  -- using RA range and DEC range to reduced the search range. 
  and   alpha_j2000 >= select_ra - distance_threshold
  and   alpha_j2000 <= select_ra + distance_threshold
  and   delta_j2000 >= select_dec - distance_threshold
  and   delta_j2000 <= select_dec + distance_threshold
; 

cursor matchCsr_4 is 
  select imageid imageid_2, object_id object_id_2, 
         mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_4 < magerr_limit
  -- using RA range and DEC range to reduced the search range. 
  and   alpha_j2000 >= select_ra - distance_threshold
  and   alpha_j2000 <= select_ra + distance_threshold
  and   delta_j2000 >= select_dec - distance_threshold
  and   delta_j2000 <= select_dec + distance_threshold
; 

cursor matchCsr_5 is 
  select imageid imageid_2, object_id object_id_2, 
         mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_5 < magerr_limit
  -- using RA range and DEC range to reduced the search range. 
  and   alpha_j2000 >= select_ra - distance_threshold
  and   alpha_j2000 <= select_ra + distance_threshold
  and   delta_j2000 >= select_dec - distance_threshold
  and   delta_j2000 <= select_dec + distance_threshold; 

cursor matchCsr_6 is 
  select imageid imageid_2, object_id object_id_2, 
         mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_6 < magerr_limit
  -- using RA range and DEC range to reduced the search range. 
  and   alpha_j2000 >= select_ra - distance_threshold
  and   alpha_j2000 <= select_ra + distance_threshold
  and   delta_j2000 >= select_dec - distance_threshold
  and   delta_j2000 <= select_dec + distance_threshold; 

begin
  -- find the overlap part of these two images
  select min(alpha_j2000), max(alpha_j2000), 
         min(delta_j2000), max(delta_j2000)
  into min_ra1, max_ra1, min_dec1, max_dec1
  from objects
  where imageid = image1;
  select min(alpha_j2000), max(alpha_j2000), 
         min(delta_j2000), max(delta_j2000)
  into min_ra2, max_ra2, min_dec2, max_dec2
  from objects
  where imageid = image2;
  min_ra := greatest(min_ra1, min_ra2) - distance_threshold;
  max_ra := least(max_ra1, max_ra2) + distance_threshold;
  min_dec := greatest(min_dec1, min_dec2) - distance_threshold;
  max_dec := least(max_dec1, max_dec2) + distance_threshold;

  -- magerr_auto is used for filtering
  if (lower(magerr_filter) = 'magerr_auto') then
    for selectCsr_rec in selectCsr_auto
    loop
      begin
        imageid_1 := selectCsr_rec.imageid_1;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        select_ra := selectCsr_rec.alpha_j2000;
        select_dec := selectCsr_rec.delta_j2000;
                                                                                
        for matchCsr_rec in matchCsr_auto
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.imageid_1 := imageid_1;
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.imageid_2 := matchCsr_rec.imageid_2;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;

  -- magerr_aper_1 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_1') then
    for selectCsr_rec in selectCsr_1
    loop
      begin
        imageid_1 := selectCsr_rec.imageid_1;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        select_ra := selectCsr_rec.alpha_j2000;
        select_dec := selectCsr_rec.delta_j2000;
                                                                                
        for matchCsr_rec in matchCsr_1
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.imageid_1 := imageid_1;
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.imageid_2 := matchCsr_rec.imageid_2;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;

  -- magerr_aper_2 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_2') then
    for selectCsr_rec in selectCsr_2
    loop
      begin
        imageid_1 := selectCsr_rec.imageid_1;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        select_ra := selectCsr_rec.alpha_j2000;
        select_dec := selectCsr_rec.delta_j2000;
                                                                                
        for matchCsr_rec in matchCsr_2
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.imageid_1 := imageid_1;
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.imageid_2 := matchCsr_rec.imageid_2;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;

  -- magerr_aper_3 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_3') then
    for selectCsr_rec in selectCsr_3
    loop
      begin
        imageid_1 := selectCsr_rec.imageid_1;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        select_ra := selectCsr_rec.alpha_j2000;
        select_dec := selectCsr_rec.delta_j2000;
                                                                                
        for matchCsr_rec in matchCsr_3
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.imageid_1 := imageid_1;
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.imageid_2 := matchCsr_rec.imageid_2;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;

  -- magerr_aper_4 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_4') then
    for selectCsr_rec in selectCsr_4
    loop
      begin
        imageid_1 := selectCsr_rec.imageid_1;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        select_ra := selectCsr_rec.alpha_j2000;
        select_dec := selectCsr_rec.delta_j2000;
                                                                                
        for matchCsr_rec in matchCsr_4
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.imageid_1 := imageid_1;
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.imageid_2 := matchCsr_rec.imageid_2;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;

  -- magerr_aper_5 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_5') then
    for selectCsr_rec in selectCsr_5
    loop
      begin
        imageid_1 := selectCsr_rec.imageid_1;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        select_ra := selectCsr_rec.alpha_j2000;
        select_dec := selectCsr_rec.delta_j2000;
                                                                                
        for matchCsr_rec in matchCsr_5
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.imageid_1 := imageid_1;
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.imageid_2 := matchCsr_rec.imageid_2;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;

  -- magerr_aper_6 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_6') then
    for selectCsr_rec in selectCsr_6
    loop
      begin
        imageid_1 := selectCsr_rec.imageid_1;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        select_ra := selectCsr_rec.alpha_j2000;
        select_dec := selectCsr_rec.delta_j2000;
                                                                                
        for matchCsr_rec in matchCsr_6
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.imageid_1 := imageid_1;
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.imageid_2 := matchCsr_rec.imageid_2;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;

  end if;
  commit;
  return;
    
end fMatchImages;
/
