-- Check newly removed run identifiers 
create or replace procedure DelRunAccess
as

run_id varchar(100);
project_id varchar(20);

cursor run_del_cr is 
  select distinct a.runiddesc
  from runiddesc_access a
  where a.des_group = 'all'
  and   not exists (select distinct b.run
                    from location b
                    where a.runiddesc = b.run
                    and   regexp_like(archivesites, '[^N]')
                    and   b.project in ('BCS', 'DES', 'SCS', 'CPT', 'SPT', 'CFH'));

begin
  open run_del_cr;
  loop
    fetch run_del_cr into run_id;
    exit when run_del_cr%NOTFOUND;
    dbms_output.put_line ('    ' || run_id || ',' || 'NA');
  end loop;
  
end DelRunAccess;
/
