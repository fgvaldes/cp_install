-- find the nearest object to the point specified by coordinates X, Y and Z 
-- run the procedure: select * from table(fGetNearestXYZ(.9821, .1816, -.0506, 5))
create or replace function fGetNearestXYZ(
nx float, 
ny float, 
nz float, 
r float) 
return MatchedObjTab pipelined 
is
pragma autonomous_transaction;

mobj matchedObj := matchedObj(null,null,null,null,null,null);
r_ float;

cursor mstfrc is 
  select object_id, htmid,
         cx, cy, cz, distance
  from table(fGetNearbyXYZpre(nx,ny,nz,r_))
  where distance < r_
  order by distance;

begin
    r_ := r;
    if (r_<0.1) then
      r_ := 0.1;
    end if;

    for mstfrc_rec in mstfrc
    loop
      -- only output the nearest object, this loop only runs once
        mobj.object_id := mstfrc_rec.object_id;
        mobj.htmid := mstfrc_rec.htmid;
        mobj.cx := mstfrc_rec.cx;
        mobj.cy := mstfrc_rec.cy;
        mobj.cz := mstfrc_rec.cz;
        mobj.distance := mstfrc_rec.distance;
        pipe row (mobj);
        return;
    end loop;
end fGetNearestXYZ;
/

