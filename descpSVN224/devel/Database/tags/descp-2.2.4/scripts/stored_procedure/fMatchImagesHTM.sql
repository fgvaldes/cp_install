-- This version uses HTM search and not efficient.
-- should not use HTM for searching small range of objects.
-- This stored procedure matches the objects in two images
-- The program has 5 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. stella_lo (minimum value for class_star)
-- 4. stella_hi (maximum value for class_star)
-- 5. flag_upperlim (upperlim for flags)
-- 6. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- The program returns a table which has 36 columns:
-- object_id_1, band_1, class_star_1, flags_1, mag_auto_1, magerr_auto_1, 
-- mag_aper_1_1, magerr_aper_1_1, mag_aper_2_1, magerr_aper_2_1,
-- mag_aper_3_1, magerr_aper_3_1, mag_aper_4_1, magerr_aper_4_1,
-- mag_aper_5_1, magerr_aper_5_1, mag_aper_6_1, magerr_aper_6_1,
-- object_id_2, band_2, class_star_2, flags_2, mag_auto_2, magerr_auto_2, 
-- mag_aper_1_2, magerr_aper_1_2, mag_aper_2_2, magerr_aper_2_2,
-- mag_aper_3_2, magerr_aper_3_2, mag_aper_4_2, magerr_aper_4_2,
-- mag_aper_5_2, magerr_aper_5_2, mag_aper_6_2, magerr_aper_6_2
-- to match image 119308 and image 126188 with class_star from 0.8 to 1.0,
-- flag upperlimit 50 and matching tolerance 2 arc seconds:
-- select * from table(fMatchImages(119308, 126188, 0.8, 1.0, 50, 0.034));

create or replace function fMatchImages(
image1 number,
image2 number, 
stella_lo float,
stella_hi float,
flag_upperlim number,
distance_threshold float)
return MatchImgObjTab pipelined
as
pragma autonomous_transaction;

matchObj MatchImgObj := MatchImgObj(null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null);
r1 float;
clevel number(2);
cmd1 varchar2(200);
object_id_1 number(11);
mag_auto_1 float;
magerr_auto_1 float;
mag_aper_1_1 float;
magerr_aper_1_1 float;
mag_aper_2_1 float;
magerr_aper_2_1 float;
mag_aper_3_1 float;
magerr_aper_3_1 float;
mag_aper_4_1 float;
magerr_aper_4_1 float;
mag_aper_5_1 float;
magerr_aper_5_1 float;
mag_aper_6_1 float;
magerr_aper_6_1 float;
select_x float;
select_y float;
select_z float;
band_1 varchar2(10);
class_star_1 number(3,2);
flags_1 number(3);
distance float;

cursor selectCsr is 
  select object_id object_id_1, mag_auto mag_auto_1, magerr_auto magerr_auto_1, 
         mag_aper_1 mag_aper_1_1, magerr_aper_1 magerr_aper_1_1,
         mag_aper_2 mag_aper_2_1, magerr_aper_2 magerr_aper_2_1,
         mag_aper_3 mag_aper_3_1, magerr_aper_3 magerr_aper_3_1,
         mag_aper_4 mag_aper_4_1, magerr_aper_4 magerr_aper_4_1,
         mag_aper_5 mag_aper_5_1, magerr_aper_5 magerr_aper_5_1,
         mag_aper_6 mag_aper_6_1, magerr_aper_6 magerr_aper_6_1,
         cx select_x, cy select_y, cz select_z, band band_1, 
         class_star class_star_1, flags flags_1 
  from objects a
  where imageid = image1
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi; 

cursor matchCsr is 
  select /*+ ordered USE_NL(b) INDEX(b objects_htmid_idx pk_objects) */
  --select /*+ ordered USE_NL(b) INDEX(b objects_htmid_idx) */
         object_id object_id_2, mag_auto mag_auto_2, magerr_auto magerr_auto_2, 
         mag_aper_1 mag_aper_1_2, magerr_aper_1 magerr_aper_1_2,
         mag_aper_2 mag_aper_2_2, magerr_aper_2 magerr_aper_2_2,
         mag_aper_3 mag_aper_3_2, magerr_aper_3 magerr_aper_3_2,
         mag_aper_4 mag_aper_4_2, magerr_aper_4 magerr_aper_4_2,
         mag_aper_5 mag_aper_5_2, magerr_aper_5 magerr_aper_5_2,
         mag_aper_6 mag_aper_6_2, magerr_aper_6 magerr_aper_6_2,
         cx match_x, cy match_y, cz match_z, band band_2, 
         class_star class_star_2, flags flags_2 
  from table(fHtmCover(cmd1,r1)) a, objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   b.htmid >= a.htm_id_start
  and   b.htmid <= a.htm_id_end;

begin
    if (distance_threshold < 0.1) then
      r1 := 0.1;
    else
      r1 := distance_threshold;
    end if;

    for selectCsr_rec in selectCsr
    loop
      begin
    	-- determine the level of HTMID for circle search on the match objects
        clevel := 13 - (floor(log(10,r1)/log(10,2.0)));
        if (clevel < 5) then
          clevel := 5;
        elsif (clevel > 13) then
          clevel := 13;
        end if;
        object_id_1 := selectCsr_rec.object_id_1;
        mag_auto_1 := selectCsr_rec.mag_auto_1;
        magerr_auto_1 := selectCsr_rec.magerr_auto_1;
        mag_aper_1_1 := selectCsr_rec.mag_aper_1_1;
        magerr_aper_1_1 := selectCsr_rec.magerr_aper_1_1;
        mag_aper_2_1 := selectCsr_rec.mag_aper_2_1;
        magerr_aper_2_1 := selectCsr_rec.magerr_aper_2_1;
        mag_aper_3_1 := selectCsr_rec.mag_aper_3_1;
        magerr_aper_3_1 := selectCsr_rec.magerr_aper_3_1;
        mag_aper_4_1 := selectCsr_rec.mag_aper_4_1;
        magerr_aper_4_1 := selectCsr_rec.magerr_aper_4_1;
        mag_aper_5_1 := selectCsr_rec.mag_aper_5_1;
        magerr_aper_5_1 := selectCsr_rec.magerr_aper_5_1;
        mag_aper_6_1 := selectCsr_rec.mag_aper_6_1;
        magerr_aper_6_1 := selectCsr_rec.magerr_aper_6_1;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;
        band_1 := selectCsr_rec.band_1;
        class_star_1 := selectCsr_rec.class_star_1;
        flags_1 := selectCsr_rec.flags_1;
        cmd1 := to_char(clevel) || ' ' || to_char(select_x) || 
                ' ' || to_char(select_y) || 
                ' ' || to_char(select_z) || 
                ' ' || to_char(r1);
                                                                                
        for matchCsr_rec in matchCsr
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < distance_threshold) then
              matchObj.object_id_1 := object_id_1;
              matchObj.band_1 := band_1; 
              matchObj.class_star_1 := class_star_1; 
              matchObj.flags_1 := flags_1; 
              matchObj.mag_auto_1 := mag_auto_1;
              matchObj.magerr_auto_1 := magerr_auto_1;
              matchObj.mag_aper_1_1 := mag_aper_1_1;
              matchObj.magerr_aper_1_1 := magerr_aper_1_1;
              matchObj.mag_aper_2_1 := mag_aper_2_1;
              matchObj.magerr_aper_2_1 := magerr_aper_2_1;
              matchObj.mag_aper_3_1 := mag_aper_3_1;
              matchObj.magerr_aper_3_1 := magerr_aper_3_1;
              matchObj.mag_aper_4_1 := mag_aper_4_1;
              matchObj.magerr_aper_4_1 := magerr_aper_4_1;
              matchObj.mag_aper_5_1 := mag_aper_5_1;
              matchObj.magerr_aper_5_1 := magerr_aper_5_1;
              matchObj.mag_aper_6_1 := mag_aper_6_1;
              matchObj.magerr_aper_6_1 := magerr_aper_6_1;
              matchObj.object_id_2 := matchCsr_rec.object_id_2;
              matchObj.band_2 := matchCsr_rec.band_2; 
              matchObj.class_star_2 := matchCsr_rec.class_star_2; 
              matchObj.flags_2 := matchCsr_rec.flags_2; 
              matchObj.mag_auto_2 := matchCsr_rec.mag_auto_2;
              matchObj.magerr_auto_2 := matchCsr_rec.magerr_auto_2;
              matchObj.mag_aper_1_2 := matchCsr_rec.mag_aper_1_2;
              matchObj.magerr_aper_1_2 := matchCsr_rec.magerr_aper_1_2;
              matchObj.mag_aper_2_2 := matchCsr_rec.mag_aper_2_2;
              matchObj.magerr_aper_2_2 := matchCsr_rec.magerr_aper_2_2;
              matchObj.mag_aper_3_2 := matchCsr_rec.mag_aper_3_2;
              matchObj.magerr_aper_3_2 := matchCsr_rec.magerr_aper_3_2;
              matchObj.mag_aper_4_2 := matchCsr_rec.mag_aper_4_2;
              matchObj.magerr_aper_4_2 := matchCsr_rec.magerr_aper_4_2;
              matchObj.mag_aper_5_2 := matchCsr_rec.mag_aper_5_2;
              matchObj.magerr_aper_5_2 := matchCsr_rec.magerr_aper_5_2;
              matchObj.mag_aper_6_2 := matchCsr_rec.mag_aper_6_2;
              matchObj.magerr_aper_6_2 := matchCsr_rec.magerr_aper_6_2;
              pipe row (matchObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;
    commit;
    return;
    
end fMatchImages;
/
