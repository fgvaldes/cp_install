#!/bin/bash
#
# Creates dynamic sqlldr statements for recalculating CX, CY, CZ
# (JIRA DES-1588)

DIR='/oracle/scripts/des'
ProcDIR='/oracle/procedures/des'
sqlfile=$ProcDIR/UpdateCXYZ_run.sql
logfile=$ProcDIR/UpdateCXYZ.log
infile=$ProcDIR/UpdateCXYZ_run.list
cp /dev/null $logfile

source $DIR/des.bash

echo `date` "Start updating cx, cy and cz" >> $logfile

cat $infile | \
while read line
do
 cp /dev/null $sqlfile
 echo "set timing on;" >> $sqlfile
 echo "set echo on;" >> $sqlfile
 echo "exec UpdateCXYZ('$line');" >> $sqlfile
 echo "exit;" >> $sqlfile
 sqlplus des_admin/desmgr @$sqlfile
 echo `date` "Finish updating the run $line" >> $logfile
done

exit

