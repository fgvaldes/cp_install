-- recalculate and update HTM for each object based on RA and DEC
-- this procedure will take care of rollback segments by committing after 100 rows
create or replace procedure RecalcHtm(
obs varchar, col number) as

obs_like varchar(15) := obs || '%';
obs_id varchar(15);
col_n number(4);
obj_id number(10);
upd_cnt number(10) := 0;
commit_threshold number(10) := 1000;

cursor obj_cr is 
  select observation_id, column_n, object_id
  from objects
  where observation_id like obs_like
  and   column_n = col;

begin
  --dbms_output.put_line ('obs_like is ' || obs_like);
  open obj_cr;
  loop
    fetch obj_cr into obs_id, col_n, obj_id;
    exit when obj_cr%NOTFOUND;

    --dbms_output.put_line ('obs_id is ' || obs_id);
    --dbms_output.put_line ('col_n is ' || col_n);

    update /*+ INDEX(objects PK_OBJECTS) */
    objects
    set HTMID = fEqToHtm(ra, dec)
    where observation_id = obs_id
    and   column_n = col_n
    and   object_id = obj_id;
       
    upd_cnt := upd_cnt + 1; 
    
    if (upd_cnt > commit_threshold) then
       commit;
       upd_cnt := 0;
    end if;
  end loop;
  
end RecalcHtm;
/
