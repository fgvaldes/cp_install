-- This stored procedure find the matched pair of coadd objects and 
-- regular objects. 
-- The program has 5 parameters:
-- 1. tile name (the tile from which the coadd objects are generated)
-- 2. run (the run from which the coadd objects are generated)
-- 3. project (the project both the coadd objects and regular objects belong to)
-- 4. distance_threshold (the distance tolerance in arcminute betweem two objects)
-- 5. since date (search objects created since that date). The date must be entered in this format 'dd-mon-yy', such as '01-jan-08' for Jan 01, 2008.
-- The program inserted each matched pair into the table MATCHES. If the 
-- matched pair exists in the table, then the matched pair will be ignored.
--  
-- To run the program: exec find_matches('BCS0535-5441','BCS20080917%','BCS', 0.033, '01-jan-08');

create or replace procedure find_matches(
tileName varchar,
run varchar,
project varchar,
distance_threshold float,
since varchar)
as

min_ra float;
max_ra float;
min_dec float;
max_dec float;
dist float;
sigma float;

i number := 0;
j number := 0;
c number := 0;
object1 number := 0;
object2 number := 0;
run_like varchar2(50);
tilename_like varchar2(50);
since_date varchar2(20);

type list is RECORD (
OBJECT  NUMBER(11,0),
RA  NUMBER(8,5), 
DEC NUMBER(8,5),
ERRX2_WORLD  BINARY_DOUBLE,
ERRY2_WORLD  BINARY_DOUBLE);

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;

-- get the list of coadd objects 
cursor selectcsr1 is
  select 
        a.coadd_objects_id object,
        a.ra,  
        a.dec,
        a.errx2_world,
        a.erry2_world
  from coadd_objects a, location b
  where b.tilename like tilename_like
  and   b.run like run_like
  and   b.project = upper(project)
  and   b.filetype = 'coadd'
  and   (a.imageid_g = b.id or a.imageid_r = b.id 
  or    a.imageid_i = b.id or a.imageid_z = b.id
  or    a.imageid_y = b.id);

-- get the list of objects
cursor selectcsr2 is
  select 
        a.object_id object,
        a.ra,  
        a.dec,
        a.errx2_world,
        a.erry2_world
  from objects a, location b
  where a.ra between min_ra and max_ra
  and   a.dec between min_dec and max_dec
  and   a.imageid = b.id
  and   b.project = upper(project)
  and   b.filedate >= to_date(since_date);

begin
  run_like := run || '%';
  tilename_like := tileName || '%';
  --dbms_output.put_line('tilename_like is ' || tilename_like);
  if (length(since) < 9) then
    since_date := '01-jan-07'; 
  else
    since_date := since;
  end if;

  open selectcsr1;
  fetch selectcsr1 bulk collect into collect1;

  for i in 1 .. collect1.count
  loop
  begin
    -- define the RA/DEC search range for objects
    min_ra := collect1(i).ra - (distance_threshold/60);
    max_ra := collect1(i).ra + (distance_threshold/60);
    min_dec := collect1(i).dec - (distance_threshold/60);
    max_dec := collect1(i).dec + (distance_threshold/60);

    --dbms_output.put_line('rectangle is ' || min_ra || ' ' || max_ra || ' ' ||
                         --min_dec || ' ' || max_dec);
    open selectcsr2;

    fetch selectcsr2 bulk collect into collect2;

    --dbms_output.put_line('1 has ' || collect1.count || ' elements.');
    --dbms_output.put_line('collect2 has ' || collect2.count || ' elements.');

    for j in 1 .. collect2.count
    loop
      begin
        --dbms_output.put_line('try to match: ' || collect1(i).object || ' with ' || collect2(j).object);
        dist := fgetdistanceEQ(collect1(i).ra,collect1(i).dec,collect2(j).ra,collect2(j).dec);
        if (dist < distance_threshold) then
          sigma := fgetsigmaoffset(collect1(i).ra,
                   collect1(i).dec,
                   collect1(i).errx2_world,
                   collect1(i).erry2_world,
                   collect2(j).ra,
                   collect2(j).dec,
                   collect2(j).errx2_world,
                   collect2(j).erry2_world);
          object1 := collect1(i).object;
          object2 := collect2(j).object;
          insert into matches(coadd_objects_id,object_id,offset,sigma_offset)
          values (object1, object2, dist, sigma);
          --dbms_output.put_line('match: ' || object1 || ' with ' || object2);
          c := c+1;
        end if;
        -- commit every 1000 rows
        if (mod(c,1000) = 0) then
          commit;
        end if; 
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
          WHEN OTHERS THEN
            NULL;
      end;
    end loop;
    close selectcsr2;
    commit;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
      WHEN OTHERS THEN
        NULL;
  end;
  end loop;
  close selectcsr1;
  commit;

end find_matches;
/
