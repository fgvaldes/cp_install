#!/usr/bin/perl

use strict;
use warnings;
use Config::General;
use Cwd;
use FindBin;
use lib "$FindBin::Bin/../lib/perl";
#use DAF::Base;

my @bands = ('g','i','r','z');
my %valid_project = ('BCS'=>1,'DES'=>0,'SCS'=>0);
my %valid_fileclass = ('red'=>0, 'raw'=>1, 'coadd'=>0, 'cal'=>0);

  my $do_redlog = 1;
  my $do_redcal = 1;
  my $do_redgirz = 1;
  my $do_redxml = 1;
  my $do_redetc = 1;
  my $do_redbin =1;

  my $do_rawsrc = 1;
  my $do_rawlog = 0;
  my $do_rawetc = 0;
  my $do_rawbin = 0;
  my $do_rawraw = 1;

  my $ingest = 0;
  my $short = 0;
  my $very_short = 0;
  my $project_override = 'T1';

my ($fileclass, %filelist);

my $old_archive_dir = $ARGV[0];
die "I need an old archive location to parse...\n" if (! -d $old_archive_dir);

my $file_ingest = '/home/dadams/devel/desdm/devel/ImageProc/trunk/bin/file_ingest.pl';

# Make log dir:
# Check for existing job scripts in this directory.
opendir(FH,".") or die "Unable to open directory:$!\n";
my @names = readdir(FH) or die "Unable to read directory:$!\n";
closedir(FH);

# Set the current job id.
my @job_nums;
foreach (@names) {
  if (/MIGRATE_(\d\d)$/) {
    push (@job_nums, $1);
  }
}
my $job_num = 1;
if ($#job_nums > -1) {
  $job_num = $#job_nums +2;
}
my $pad_len = 2;
my $padded_job_num = sprintf("%0*d", $pad_len, $job_num);
my $outdir = "MIGRATE_$padded_job_num";
mkdir $outdir;

#############################################################################
# Fileclass red:
#############################################################################
my (%redlog, %redcal, %redgirz, %redxml, %redetc, %redbin);
if ($valid_fileclass{'red'}) {
  $fileclass = 'red';
  # get runid listing:
  my $basedir = join('/',$old_archive_dir,$fileclass);
  my $dirs = grab_file_list($basedir);

  my %have_etc;
  my %have_bin;
  #for(my $i=0;$i<=15;$i++) {
  for(my $i=0;$i<=$#$dirs;$i++) {
    my $dir = $dirs->[$i];
    print "Processing: $basedir/$dir\n";
    # Get project run and nite from old runid directory name:
    my $copy = $dir;
    $dir =~ s/_\d\d$//;
    $copy =~ /([^_]+)_(\S+)_(\d\d)$/;
    my $run = $1;
    my $nite = $2;
    my $ccd = $3;
    $run =~ /^(.{3})/;
    my $project = $1;
    my $newrun = $run;
    $newrun =~ s/^\S\S\S//;
    my $newnite = $nite;
    $newnite =~ s/^\S\S\S//;
    if (length $newnite < 8) {
      $newnite = '20'.$newnite;
    }
    $newrun = $newrun.'000000_'.$newnite;
    $project = $project_override if ($project_override);
    #########################################################################
    # Process xml
    #########################################################################
    if ($do_redxml) {
      my $xml_dir = join('/',$basedir,$copy,'xml');
      #print "Processing $xml_dir\n";
      my $xml_files = grab_file_list($xml_dir);
      foreach my $file (@$xml_files) {
        my $newfile;
        if (! -f "$xml_dir/$file") {
          print "Skipping $file ... not a file.\n";
          next;
        }
        if ($file =~/tmp/) {
          $newfile = $file.'_'.$ccd;
        }
        push(@{$redxml{'file'}},
            {
              'project' => $project,
              'fileclass' => $fileclass,
              'nite' => $newnite,
              'run' => $newrun,
              'ccd' => $ccd,
              'localfilename' => $file,
              'localpath' => $xml_dir,
              'fileid' => 0,
              'filetype' => 'xml'
            }
          );
        $redxml{'file'}->[$#{$redxml{'file'}}]->{'filename'} = $newfile if $newfile;
      }
    }
    #########################################################################
    # Process etc
    #########################################################################
    if ($do_redetc) {
      if (! $have_etc{$run}) {
        my $etc_dir = join('/',$basedir,$copy,'etc');
        #print "Processing $etc_dir\n";
        my $etc_files = grab_file_list($etc_dir);
        foreach my $file (@$etc_files) {
          if (! -f "$etc_dir/$file") {
          print "Skipping $file ... not a file.\n";
          next;
        }
        push(@{$redetc{'file'}},
            {
              'project' => $project,
              'fileclass' => $fileclass,
              'nite' => $newnite,
              'run' => $newrun,
              'localfilename' => $file,
              'localpath' => $etc_dir,
              'fileid' => 0,
              'filetype' => 'etc'
            }
          );
        }
        $have_etc{$run} = 1;
      } 
      else {
        print "Already have etc file for $run.\n"
      } 
    }
    #########################################################################
    # Process bin
    #########################################################################
    if ($do_redbin) {
      if (! $have_bin{$run}) {
        my $bin_dir = join('/',$basedir,$copy,'bin');
        #print "Processing $bin_dir\n";
        my $bin_files = grab_file_list($bin_dir);
        foreach my $file (@$bin_files) {
          if (! -f "$bin_dir/$file") {
          print "Skipping $file ... not a file.\n";
          next;
        }
        push(@{$redbin{'file'}},
            {
              'project' => $project,
              'fileclass' => $fileclass,
              'nite' => $newnite,
              'run' => $newrun,
              'localfilename' => $file,
              'localpath' => $bin_dir,
              'fileid' => 0,
              'filetype' => 'bin'
            }
          );
        }
        $have_bin{$run} = 1;
      }
      else {
        print "Already have bin file for $run.\n"
      }
    }

    #########################################################################
    # Process the "data" directory:
    #########################################################################
    my $data_dir = join('/',$basedir,$copy,'data',$nite);
    my $log_dir = join('/',$data_dir,'log');
    my $cal_dir = join('/',$data_dir,'cal');

    #########################################################################
    # Process data/log:
    #########################################################################
    if ($do_redlog) {
      #print "Processing $log_dir\n";
      my $logfiles = grab_file_list($log_dir);
      foreach my $file (@$logfiles) {
        if (! -f "$log_dir/$file") {
          print "Skipping $file ... not a file.\n";
          next;
        }
        my $newfile;
        if ($file =~ /createlist.log/) {
          $newfile = "createlist_$ccd.log";
#          print "Renamed: $file to $newfile\n";
        }
        elsif ($file =~ /runCatalog_ingest/) {
          $newfile = $file;
          $newfile =~ s/runCatalog_ingest/runCatalog_ingest_$ccd/;
#          print "Renamed: $file to $newfile\n";
        }
        my $filetype = 'log';
        if ($file =~ /\.xml$|\.properties$/) {
          $filetype = 'xml';
        }
        
        push(@{$redlog{'file'}},
          {
            'project' => $project,
            'fileclass' => $fileclass,
            'nite' => $newnite,
            'run' => $newrun,
            'ccd' => $ccd,
            'localfilename' => $file,
            'localpath' => $log_dir,
            'fileid' => 0,
            'filetype' => $filetype
          }
        );
        $redlog{'file'}->[$#{$redlog{'file'}}]->{'filename'} = $newfile if $newfile;
      }
    }  
   
    #########################################################################
    # Process data/cal:
    #########################################################################
    if ($do_redcal) {
      my $caldirs = grab_file_list($cal_dir);
      foreach my $dir (@$caldirs) {
        #print "Processing $dir\n";
        my $calfiles = grab_file_list("$cal_dir/$dir");
        foreach my $file (@$calfiles) {
          next if (! -f "$cal_dir/$dir/$file");
          $file =~ /^([^_]+)_/;
          my $ft = $1;
          my $filetype;
          my $band;
          if ($ft =~ /pupil|bpm/) {
            $filetype = $ft;
          } 
          elsif ($ft =~ /bias|dark/) {
            $filetype = $ft.'cor';
          }
          else {
            $filetype = $ft.'cor';
            $file =~ /^([^_]+)_([^_]+)/;
            $band = $2;
          }
          push(@{$redcal{'file'}},
            {
            'project' => $project,
            'fileclass' => $fileclass,
            'nite' => $newnite,
            'run' => $newrun,
            'ccd' => $ccd,
            'localfilename' => $file,
            'localpath' => "$cal_dir/$dir",
            'fileid' => 0,
            'filetype' => $filetype
            }
          );
          $redcal{'file'}->[$#{$redcal{'file'}}]->{'band'} = $band if $band;
        }
      }
    }
    
    

    #########################################################################
    # Process data/g,i,r,z directories:
    #########################################################################
    if ($do_redgirz) {
      foreach my $band (@bands) {
        my $idirs = grab_file_list("$data_dir/$band");
        foreach my $idir (@$idirs) {
          my $exposurename = $idir;
          #print "Processing $data_dir/$band/$idir\n";
          my $files = grab_file_list("$data_dir/$band/$idir");
          foreach my $file (@$files) {
            if (! -f "$data_dir/$band/$idir/$file") {
              warn "Why do I have a subdir in $data_dir/$band/$idir?\n";
              next;
            }
            my $newfile;
            my $filetype;
            my $tilename;
            my $basered = $idir."_$ccd";
            if ($file eq "$basered.fits") {
              $filetype = 'red';
            }
            elsif ($file eq $basered.'_cat.fits') {
              $filetype = 'red_cat';
            }
            elsif($file =~ /im-allout.csv$/) {
              $filetype = 'red_shpltall';
              $newfile = $file;
              $newfile =~ s/im-allout.csv/shpltall.csv/;
            }
            elsif($file =~ /im-starout.csv$/) {
              $filetype = 'red_shpltpsf';
              $newfile = $file;
              $newfile =~ s/im-starout.csv/shpltpsf.csv/;
            }
            elsif($file eq "$basered.psf") {
              $filetype = 'red_psf';
              $newfile = $file;
              $newfile =~ s/.psf/_psf.fits/;
            }
            elsif($file eq $basered.'_scamp.fits') {
              $filetype = 'red_scamp';
            }           
            elsif($file eq $basered.'_vig.fits') {
              $filetype = 'red_psfcat';
            }
            elsif ($file =~ /\Q$basered\E\.([^\.]+)\.fits/) {
              $tilename = $1;
              if ($tilename =~ /_cat$/) {
                $tilename =~ s/_cat//;
                $filetype = 'remap_cat';
              }
              else {
                $filetype = 'remap';
              }
	    }
	    elsif ($file =~ /.*\.log$/) {
              $filetype = 'log';
            }           
            if ($filetype) {
              push(@{$redgirz{'file'}}, 
                {
                  'project' => $project,
                  'fileclass' => $fileclass,
                  'filetype' => $filetype,
                  'exposurename' => $exposurename,
                  'nite' => $newnite,
                  'run' => $newrun,
                  'ccd' => $ccd,
                  'band' => $band,
                  'localfilename' => "$file",
                  'localpath' => "$data_dir/$band/$idir",
                  'fileid' => 0
                }
              );
              $redgirz{'file'}->[$#{$redgirz{'file'}}]->{'filename'} = $newfile if $newfile;
              $redgirz{'file'}->[$#{$redgirz{'file'}}]->{'tilename'} = $tilename if $tilename;
            }
            else {
              warn "No filetype match for $data_dir/$band/$idir/$file\n"
            }


        }
        last if $very_short;
      }
    }
  }
  last if $short;
}

  # Create input files
  if (%redlog) {
    my $redlog_conf = new Config::General(-ConfigHash => \%redlog, -SplitPolicy => 'equalsign');
    $redlog_conf->save_file("$outdir/redlog.list");
  }
  if (%redcal) {
    my $redcal_conf = new Config::General(-ConfigHash => \%redcal, -SplitPolicy => 'equalsign');
    $redcal_conf->save_file("$outdir/redcal.list");
  }
  if (%redgirz) {
    my $redgirz_conf = new Config::General(-ConfigHash => \%redgirz, -SplitPolicy => 'equalsign');
    $redgirz_conf->save_file("$outdir/redgirz.list");
  }
  if (%redxml) {
    my $redxml_conf = new Config::General(-ConfigHash => \%redxml, -SplitPolicy => 'equalsign');
    $redxml_conf->save_file("$outdir/redxml.list");
  }
  if (%redetc) {
    my $redetc_conf = new Config::General(-ConfigHash => \%redetc, -SplitPolicy => 'equalsign');
    $redetc_conf->save_file("$outdir/redetc.list");
  }
  if (%redbin) {
    my $redbin_conf = new Config::General(-ConfigHash => \%redbin, -SplitPolicy => 'equalsign');
    $redbin_conf->save_file("$outdir/redbin.list");
  }
}

#############################################################################
# Fileclass raw
#############################################################################
if ($valid_fileclass{'raw'}) {
my (%rawsrc,%rawlog,%rawetc,%rawbin,%rawraw);
print <<STR;
####################################################################
#
# Migrating data from ..raw/<nite>/.. directories.
#
####################################################################
STR
  $fileclass = 'raw';
  my $basedir = join('/',$old_archive_dir,$fileclass);
  my $dirs = grab_file_list($basedir);
  foreach my $dir (@$dirs) {
    next if ($dir !~ /\d{6}$/);
    print "Processing: $basedir/$dir\n";
    $dir =~ /^(\S\S\S)(.*)$/;
    my $project = uc($1);
    my $newnite = $2;
    if (length $newnite < 8) {
      $newnite = '20'.$newnite;
    }
    $project = $project_override if ($project_override);
    #########################################################################
    # Process src directory:
    #########################################################################
    if ($do_rawsrc) {
      my $newfileclass = 'src';
      my $srcdir = join('/',$basedir, $dir,'src');
      next if (! -d $srcdir);
      my $srcfiles = grab_file_list($srcdir);
      foreach my $file(@$srcfiles) {
        if (-d "$srcdir/$file") {
          print "Skipping dir: $srcdir/$file\n";
          next;
        }
        if ($file !~ /.*\.fits|.*\.fits.gz$/) {
          print "Skipping file: $srcdir/$file\n";
          next;
        }
        push(@{$rawsrc{'file'}},
          {
            'project' => $project,
            'fileclass' => $newfileclass,
            'nite' => $newnite,
            'localfilename' => $file,
            'localpath' => $srcdir,
            'fileid' => 0,
            'filetype' => 'src'
          }
        );
      
      }
    }
    #########################################################################
    # Process raw directory:
    #########################################################################
    if ($do_rawraw) {
      my $newfileclass = 'red';
      my $rawdir = join('/',$basedir, $dir,'raw');
      if (! -d $rawdir) {
        warn "No raw directory in $basedir/$dir\n";
        next;
      }
      my $idirs = grab_file_list($rawdir); 
      foreach my $idir (@$idirs) {
        if (! -d "$rawdir/$idir") {
          warn "$rawdir/$idir is not a directory.\n";
          next;
        }
        my $filetype;
        if ($idir =~ /dflat/) {
          $filetype = 'raw_dflat';
        }
        elsif ($idir =~ /zero/) {
          $filetype = 'raw_bias';
        }
        else {
          $filetype = 'raw_obj';
        } 
        my $files = grab_file_list("$rawdir/$idir");
        foreach my $file (@$files) {
          if (! -f "$rawdir/$idir/$file") {
            warn "$rawdir/$idir/$file is not a file.\n";
            next;
          }
          my $ccd;
          if ($file =~ /\Q$idir\E_(\d\d).fits/) {
            $ccd = $1;
          }
          else {
            warn "No ccd found for $rawdir/$idir/$file\n";
          }
          my $newrun = $newnite.'000000'."_$newnite";
          push(@{$rawraw{'file'}},
                {
                  'project' => $project,
                  'fileclass' => $newfileclass,
                  'filetype' => $filetype,
                  'exposurename' => $idir,
                  'nite' => $newnite,
                  'run' => $newrun,
                  'localfilename' => "$file",
                  'localpath' => "$rawdir/$idir",
                  'fileid' => 0
                }
              );
          $rawraw{'file'}->[$#{$rawraw{'file'}}]->{'ccd'} = $ccd if $ccd;

        }
      last if $very_short;
      }
    }
  last if $short;
  }
  if (%rawsrc) {
    my $rawsrc_conf = new Config::General(-ConfigHash => \%rawsrc, -SplitPolicy => 'equalsign');
    $rawsrc_conf->save_file("$outdir/rawsrc.list");
  }
  if (%rawraw) {
    my $rawraw_conf = new Config::General(-ConfigHash => \%rawraw, -SplitPolicy => 'equalsign');
    $rawraw_conf->save_file("$outdir/rawraw.list");
  }
}

if ($ingest) {
  my $cmd;
  if (-f "$outdir/redlog.list") {
    print "Ingesting from  $outdir/redlog.list...";
    $cmd = "$file_ingest -filelist $outdir/redlog.list > $outdir/redlog_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
  if (-f "$outdir/redcal.list") {
    print "Ingesting from  $outdir/redcal.list...";
    $cmd = "$file_ingest -filelist $outdir/redcal.list > $outdir/redcal_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
  if (-f "$outdir/redgirz.list") {
    print "Ingesting from  $outdir/redgirz.list...";
    $cmd = "$file_ingest -filelist $outdir/redgirz.list > $outdir/redgirz_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
  if (-f "$outdir/redxml.list") {
    print "Ingesting from  $outdir/redxml.list...";
    $cmd = "$file_ingest -filelist $outdir/redxml.list > $outdir/redxml_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
  if (-f "$outdir/redetc.list") {
    print "Ingesting from  $outdir/redetc.list...";
    $cmd = "$file_ingest -filelist $outdir/redetc.list > $outdir/redetc_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
  if (-f "$outdir/redbin.list") {
    print "Ingesting from  $outdir/redbin.list...";
    $cmd = "$file_ingest -filelist $outdir/redbin.list > $outdir/redbin_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
  if (-f "$outdir/rawsrc.list") {
    print "Ingesting from  $outdir/rawsrc.list...";
    $cmd = "$file_ingest -filelist $outdir/rawsrc.list > $outdir/rawsrc_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
  if (-f "$outdir/rawraw.list") {
    print "Ingesting from  $outdir/rawraw.list...";
    $cmd = "$file_ingest -filelist $outdir/rawraw.list > $outdir/rawraw_ingest.log 2>&1";
    system($cmd);
    print "Done!\n";
  }
}


sub grab_file_list {
  my $dir = shift;

  my @list = `ls -1 $dir`;
  foreach (@list) {
    chomp;
  }
  return \@list;
}

sub grab_file_list_recursive {
  my $dir = shift;

  my @list = `ls -1 -R $dir`;
  foreach (@list) {
    chomp;
  }
  return \@list;
}

sub map_filetype {
  my $file = shift;

  my $filetype;

  return $filetype;
}
