#!/usr/bin/perl -w

#
# Regression test suite for file_ingest.pl
#
# DESCRIPTION:
#
# AUTHOR:  
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev:: 4386                                          $ Revision of last commit
# $LastChangedBy:: tdarnell                            $ Author of last commit
# $LastChangedDate:: 2009-09-24 09:23:19 -0700 (Thu, 2#$ Date of last commit
#


use strict;
use FindBin;
$|=1;

use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");

use Test::More tests=>77;

use_ok("Config::General");
use_ok("Data::Dumper");
use_ok("DB::FileUtils");
use_ok("DB::DESUtil");;

#
# Make a DB connection
#
my $dbh = DB::DESUtil->new
  (
             DBIattr => {
              AutoCommit => 0,
              RaiseError => 1
             }
  );

my ($sql,$sth,$result,$numCCDs,$exposureName);
my $project = 'UnitTestTony';
#my $project = 'BCS';

if ($project eq 'BCS'){
  $numCCDs = 8;
} else {
  $numCCDs = 62;
  $exposureName = 'decam--24--35-i-2';
}

#
# Make sure there are right number of files present in the database
#
$sql = qq{
  select count(id) from exposure where project='$project'
};
$sth= $dbh->prepare($sql);
$sth->execute();
$result = $sth->fetchrow_arrayref();
is_deeply($result,[14],"number of exposure files");

#
# Check parentIDs for red images
#
$sql = qq{
  select distinct(imagetype) from image where
  project='$project' and id in (select parentid from image
  where imagetype='red')
};
$sth= $dbh->prepare($sql);
$sth->execute();
$result = $sth->fetchrow_arrayref();
is_deeply($result,['raw_obj'],"parentID for red filetype");

#
# Check parentIDs for red_cat
#
$sql = qq{
  select distinct(imagetype) from image where
  project='$project' and id in (select parentid from catalog
  where catalogtype='red_cat')
};
$sth= $dbh->prepare($sql);
$sth->execute();
$result = $sth->fetchrow_arrayref();
is_deeply($result,['red'],"parentID for red_cat filetype");

#
# Check parentID for remap
#
$sql = qq{
  select distinct(imagetype) from image where
  project='$project' and id in (select parentid from image
  where imagetype='remap')
};
$sth= $dbh->prepare($sql);
$sth->execute();
$result = $sth->fetchrow_arrayref();
is_deeply($result,['red'],"parentID for remap filetype");

#
# Check parentID for remap_cat
#
$sql = qq{
  select distinct(imagetype) from image where
  project='$project' and id in (select parentid from catalog
  where catalogtype='remap_cat')
};
$sth= $dbh->prepare($sql);
$sth->execute();
$result = $sth->fetchrow_arrayref();
is_deeply($result,['remap'],"parentID for remap_cat filetype");

#
# Check whether all parents of type red and ccd=10 are in fact ccd=10
#
foreach my $ccd (1..$numCCDs){
  $sql = qq{
    select distinct(ccd) from image where
      project='$project' and id in (select parentid from image
          where imagetype='red' and ccd=$ccd)
  };
  $sth= $dbh->prepare($sql);
  $sth->execute();
  $result = $sth->fetchrow_arrayref();
  is_deeply($result,[$ccd],"ccd $ccd check");
} 

#
# Check whether all parents of type red and band = $band have the 
# appropriate band.
#
foreach my $band ('g','r','i','z','Y'){
  $sql = qq{
    select distinct(band) from image where
      project='$project' and id in (select parentid from image
      where imagetype='red' and band = '$band')
  };
  $sth= $dbh->prepare($sql);
  $sth->execute();
  $result = $sth->fetchrow_arrayref();
  is_deeply($result,[$band],"band $band check");
}

#
# Verify that all parents of type red actually come from the same 
# exposure

$sql = qq{
  select distinct(exposure.exposurename) from image,exposure
  where image.project='$project' and
  image.exposureid=exposure.id and image.id in (select
  parentid from image where imagetype='red' and imagename like
  '$exposureName%')
};
$sth= $dbh->prepare($sql);
$sth->execute();
$result = $sth->fetchrow_arrayref();
is_deeply($result,[$exposureName . '.fits'],"exposure name check");

# 
#
# Close the DB connection
#
$sth->finish();
$dbh->disconnect();

1;
