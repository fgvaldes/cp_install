#!/usr/bin/perl -w

#
# Regression test suite for IngestUtils.pm
# DESCRIPTION:
#
# AUTHOR:  
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev:: 4386                                          $ Revision of last commit
# $LastChangedBy:: tdarnell                            $ Author of last commit
# $LastChangedDate:: 2009-09-24 09:23:19 -0700 (Thu, 2#$ Date of last commit
#


use strict;
use Data::Dumper;
use FindBin;
use Test::More tests=>18;
$|=1;

use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");

use_ok("DB::IngestUtils");
use_ok("DB::DESUtil");

#
# Create a temporary table
#

my $tmpTableName = qq{TMP_REGRESSIONTEST};

my $dbh = DB::DESUtil->new
  (
             DBIattr => {
              AutoCommit => 0,
              RaiseError => 1
             }
  );

ok(defined $dbh, "Connect to database");

my $tableExists = checkIfTableExists($dbh,$tmpTableName);
is($tableExists,0, 'Temporary table does not exist');

my $isCreated = createTmpObjectsTable($dbh, $tmpTableName);
is($isCreated,1, 'Temporary table created');

my $isDropped = dropTmpObjectsTable($dbh, $tmpTableName);
is($isDropped,1, 'Temporary table dropped');

#
# Create some test data in the database for testing out the ingestion 
# methods.
#
my $fileInfoHashRef;

#
# Start with a src image
#
$fileInfoHashRef->{'ID'} = getNextFileID($dbh);
$fileInfoHashRef->{'FILENAME'} = 'ExposureName.fits';
$fileInfoHashRef->{'FILECLASS'} = 'src';
$fileInfoHashRef->{'FILETYPE'} = 'src';
$fileInfoHashRef->{'NITE'} = 'TODAY';
$fileInfoHashRef->{'PROJECT'} = 'TEST';
$fileInfoHashRef->{'RUN'} = 'RegressionTest';
$fileInfoHashRef->{'BAND'} = '';
$fileInfoHashRef->{'EXPOSURENAME'} = 'ExposureName';
$fileInfoHashRef->{'CCD'} = 0;
$fileInfoHashRef->{'FILEDATE'} = localtime;
$fileInfoHashRef->{'FILESIZE'} = 123456;
$fileInfoHashRef->{'ARCHIVESITES'} = 'NNNNNNNNNNNNNNNNNYNN';
$fileInfoHashRef->{'TILENAME'} = 'MyTile';

my $numRows = insertHashRefInDB($dbh,'LOCATION',$fileInfoHashRef);
is($numRows,1,'Inserted a src image OK');

#
# Now lets ingest a raw file
#
my $rawID = getNextFileID($dbh);
$fileInfoHashRef->{'ID'} = $rawID;
$fileInfoHashRef->{'FILEDATE'} = localtime;
$fileInfoHashRef->{'FILENAME'} = 'ExposureName_08.fits';
$fileInfoHashRef->{'CCD'} = 8;
$fileInfoHashRef->{'BAND'} = 'z';
$fileInfoHashRef->{'FILETYPE'} = 'raw_obj';

$numRows = insertHashRefInDB($dbh,'LOCATION',$fileInfoHashRef);
is($numRows,1,'Inserted a raw image OK');

#
# Ingest a reduced file
#
my $redID = getNextFileID($dbh);
$fileInfoHashRef->{'ID'} = $redID;
$fileInfoHashRef->{'FILEDATE'} = localtime;
$fileInfoHashRef->{'FILETYPE'} = 'red';
$numRows = insertHashRefInDB($dbh,'LOCATION',$fileInfoHashRef);
is($numRows,1,'Inserted a reduced image OK');

#
# Ingest a cat file
#
my $catID = getNextFileID($dbh);
$fileInfoHashRef->{'ID'} = $catID;
$fileInfoHashRef->{'FILEDATE'} = localtime;
$fileInfoHashRef->{'FILENAME'} = 'ExposureName_08_cat.fits';
$fileInfoHashRef->{'FILETYPE'} = 'red_cat';
$numRows = insertHashRefInDB($dbh,'LOCATION',$fileInfoHashRef);
is($numRows,1,'Inserted a cat image OK');

#
# Ingest a supersky file
#
my $superID = getNextFileID($dbh);
$fileInfoHashRef->{'ID'} = $superID;
$fileInfoHashRef->{'FILENAME'} = 'supersky_z_08.fits';
$fileInfoHashRef->{'FILETYPE'} = 'supersky';
$fileInfoHashRef->{'FILEDATE'} = localtime;
$numRows = insertHashRefInDB($dbh,'LOCATION',$fileInfoHashRef);
is($numRows,1,'Inserted a supersky image OK');

#
# ingest a illumcor file
#
my $illumID = getNextFileID($dbh);
$fileInfoHashRef->{'ID'} = $illumID;
$fileInfoHashRef->{'FILENAME'} = 'illumcor_z_08.fits';
$fileInfoHashRef->{'FILETYPE'} = 'illumcor';
$fileInfoHashRef->{'FILEDATE'} = localtime;
$numRows = insertHashRefInDB($dbh,'LOCATION',$fileInfoHashRef);
is($numRows,1,'Inserted an illumcor image OK');

#
# ingest a fringecor file
#
my $fringID = getNextFileID($dbh);
$fileInfoHashRef->{'ID'} = $fringID;
$fileInfoHashRef->{'FILENAME'} = 'fringecor_z_08.fits';
$fileInfoHashRef->{'FILETYPE'} = 'fringecor';
$fileInfoHashRef->{'FILEDATE'} = localtime;
$numRows = insertHashRefInDB($dbh,'LOCATION',$fileInfoHashRef);
is($numRows,1,'Inserted an fringecor image OK');

#
# getParentID tests
#
my $queryHashRef;
$queryHashRef->{'FILETYPE'} = 'red_cat';
$queryHashRef->{'FILENAME'} = 'ExposureName_08_cat.fits';
#$queryHashRef->{'TILENAME'} = '';
$queryHashRef->{'RUN'} = 'RegressionTest';

my ($parentId,$ccd,$band) = getParentID($dbh,$queryHashRef);
is($parentId,$redID,'ParentId for red_cat filetype checks out');

$queryHashRef->{'FILETYPE'} = 'red';
$queryHashRef->{'FILENAME'} = 'ExposureName_08.fits';
($parentId,$ccd,$band) = getParentID($dbh,$queryHashRef);
is($parentId,$rawID,'ParentId for red filetype checks out');

$queryHashRef->{'FILETYPE'} = 'illumcor';
$queryHashRef->{'FILENAME'} = 'illumcor_z_08.fits';
($parentId,$ccd,$band) = getParentID($dbh,$queryHashRef);
is($parentId,$superID,'ParentId for illumcor filetype checks out');

$queryHashRef->{'FILETYPE'} = 'fringecor';
$queryHashRef->{'FILENAME'} = 'fringecor_z_08.fits';
($parentId,$ccd,$band) = getParentID($dbh,$queryHashRef);
is($parentId,$superID,'ParentId for fringecor filetype checks out');

#
# getFilesForRunID tests
#
my %runIDS;
$runIDS{'20080319000000_20061031'} = 1;
$runIDS{'20080319000000_20061214'} = 1;
my $nites;
$nites->{'20061214'} = 1;
my $ingestedFiles = getFilesForRunID($dbh,\%runIDS,$nites);
my $tmpCount = 0;
foreach my $key (keys %$ingestedFiles){
  my $tmpNum = scalar keys %{$ingestedFiles->{$key}};
  $tmpCount += $tmpNum;
}
  
is($tmpCount,11187, 'getFilesForRunID returned correct number of files');


$dbh->rollback();
$dbh->disconnect();

1;
