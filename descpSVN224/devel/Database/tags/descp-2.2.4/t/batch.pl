#!/usr/bin/perl -w
use strict;
use warnings;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use DB::DESUtil;

my $file = $ARGV[0];
my (@files,$t1,$t2, $Rows);
$|=1;

my $db = DB::DESUtil->new
  (
      'DBIattr'=>{'ora_array_chunk_size'=>50000}
  );

print "\nReading filelist...";
readFileList($file, \@files);
print "Done!\n";

my $nfiles = $#files;
my $nf = $nfiles+1;
print $nf," entries in this list\n";

my $dbq = 'SELECT FILENAME FROM LOCATION WHERE ID=?';

my @Rows;

#Test 1:
#my $sth = $db->prepare($dbq);
print "\nRunning db query:\n";
$t1 = time;
for(my $i=0;$i<=$nfiles;$i++) {
  my $sth = $db->prepare($dbq);
  my $id;
  my $stat = $sth->execute($files[$i]->{'fileid'});
  $sth->bind_col(1,\$id);
  push(@Rows, $id);
}
$t2=time;
print "Rows: ",scalar @Rows,"\n";
print "Done in ",$t2-$t1," seconds.\n";
print "A value: ", $Rows[0],"\n";


#print "\nRunning db query:\n";
#$t1 = time;
#for(my $i=0;$i<=$nfiles;$i++) {
#  $sth->bind_param_array(1,$files[$i]->{'fileid'});
#}
#$sth->execute();
#$Rows = $sth->fetchall_arrayref({});
#$t2=time;
#print "Done in ",$t2-$t1," seconds.\n";
#print "$#$Rows\n";
#print "A value: ",$Rows->[0]->{'filename'},"\n";


sub readFileList {
  my $file = shift;
  my $files = shift;
  my ($filehref);

  open (FH, "$file") or die "Cannot open $file";
  my @lines=<FH>;
  #while (my $line = <FH>) {
  foreach my $line (@lines) {
    chomp($line);
    $line =~ s/^\s*//;
    $line =~ s/\s*$//;
    if ($line eq "</file>") {
      push (@$files, $filehref);
    }
    elsif ($line eq "<file>") {
      $filehref = {};
    }
    else {
      (my $left, my $right) = split /\s*=\s*/, $line;
      $filehref->{"$left"} = $right;
    }
  }
  close FH;
}

