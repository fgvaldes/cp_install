#!/usr/bin/perl -w
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib/perl";
use DAF::DB::Util;
use NCSA::Transfer::Util qw(:all);
use NCSA::Transfer::Uberftp qw(:all);

my $desdb;

#$desdb = new DAF::DB::Util();
#$desdb = new DAF::DB::Util({'verbose' =>1, 'DBIattr' => {'AutoCommit' => 1}});
#$desdb = new DAF::DB::Util({'DBIattr' => {'AutoCommit' => 1}});
$desdb = new DAF::DB::Util({'verbose' =>1, 'db_server' => 'des1.cosmology.uiuc.edu', 'db_name'=>'des', 'db_user'=>'des_admin', 'db_pass'=>'desmgr'});
#$desdb = new DAF::DB::Util({'verbose' =>0});

my $data = $desdb->getArchiveSiteInfo('mercury');

#print "\n$desdb->{Driver}->{Name}\n";

my $scan_dir = '/home/dadams/tdata';
my $dest_dir = '/gpfs_scratch1/dadams/tdata';
my $tdir = $scan_dir;
$tdir  =~  s/\/[^\/]+$//;
my (@flist, @dlist,@sizes, @destlist);
scan_dir({
  'scan_dir' => $scan_dir,
  'start_dir' => $scan_dir,
  'filenames_ref' => \@flist,
  'sizes_ref' => \@sizes,
  'dlist_ref' => \@dlist,
  'recursive' => 1
});

foreach (@flist) {
  push(@destlist, join('/',$dest_dir,$_));
  $_ = join('/',$scan_dir, $_);
#  print "$_\n";
}

foreach (@dlist) {
  s/^$tdir\///; 
  $_ = join('/',$dest_dir, $_);
}

my @dirs = ('/home/ncsa/dadams', '/home/ncsa/dadams/d1', '/home/ncsa/dadams/d1', '/home/ncsa/dadams/d1/d11', '/home/ncsa/dadams/d1/d12');
#createRemoteDirs(\@dirs,{'target_server' => 'gridftp-hg.ncsa.teragrid.org', 'cmd_file' => 'ubertmpcmds'});

#$desdb->disconnect();

#createRemoteDirs(\@dlist,{'target_server' => 'gridftp-hg.ncsa.teragrid.org'});
#transferUber([{'target_server' => 'gridftp-hg.ncsa.teragrid.org', 'srcfiles' => \@flist, 'destfiles' => \@destlist}]);


my %vfiles = (
               '/home/dadams/vdirs/there' => 1, 
               '/home/dadams/vdirs/nothere' => 1, 
               '/home/dadams/vdirs/notmine/there' => 1,
               '/home/dadams/vdirs/notmine/notthere' => 1,
               '/home/dadams/vdirs/there2' => 1
             );

verifyRemoteFiles({'files'=>\%vfiles}, {'target_server' => 'tethys.ncsa.uiuc.edu'});
