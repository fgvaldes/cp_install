#!/usr/bin/env perl
########################################################################
#
#  $Id: parsedbconfig.pl 6059 2010-10-28 18:48:47Z mgower $
#
#  $Rev:: 6059                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2010-10-28 11:48:47 #$:  # Date of last commit.
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  DESCRIPTION:
#       test parse_db_config_file function from lib/DB/Connection.pm
#
# History:
#   06-Jul-2010 Added access to dbtype member variable
################################################################################

use lib ("../lib");
use Test::More;
use File::Temp qw/ tempfile /;
use DB::Connection;
use Data::Dumper;

my ($fh, $filename);
my (%expected, $got, @passtests);




# returns lower case keys, so make sure expected has lower case keys
%expected = (
    'db_user'    => 'testuser',
    'db_pass'    => 'dUmMyPaSsWd',
    'db_server'  => 'somemachine.cosmology.illinois.edu',
    'db_name'    => 'thedbname',
    'db_type'    => 'ORACLE'
);

@passtests = (
    { # simple file matches exactly expected 
        'testname'   => 'simple',
        'whichdb'    => undef,
        'keyorder'   => ['db_user','db_pass','db_server','db_name','db_type'],
        'db_user'    => 'testuser',
        'db_pass'    => 'dUmMyPaSsWd',
        'db_server'  => 'somemachine.cosmology.illinois.edu',
        'db_name'    => 'thedbname',
        'db_type'    => 'ORACLE'
    },
    { # file matches expected except for case of keys
        'testname'   => 'uppercase',
        'whichdb'    => undef,
        'keyorder'   => ['DB_USER','DB_PASS','DB_SERVER','DB_NAME','DB_TYPE'],
        'DB_USER'    => 'testuser',
        'DB_PASS'    => 'dUmMyPaSsWd',
        'DB_SERVER'  => 'somemachine.cosmology.illinois.edu',
        'DB_NAME'    => 'thedbname',
        'DB_TYPE'    => 'ORACLE'
    },
    { # DB_PASSWD instead of DB_PASS
        'testname'   => 'DB_PASSWD',
        'whichdb'    => undef,
        'keyorder'   => ['DB_USER','DB_PASSWD','DB_SERVER','DB_NAME','DB_TYPE'],
        'DB_USER'    => 'testuser',
        'DB_PASSWD'  => 'dUmMyPaSsWd',
        'DB_SERVER'  => 'somemachine.cosmology.illinois.edu',
        'DB_NAME'    => 'thedbname',
        'DB_TYPE'    => 'ORACLE'
    },
    { # DB_SID should be used instead of DB_NAME
        'testname'   => 'DB_SID',
        'whichdb'    => undef,
        'keyorder'   => ['DB_USER','DB_PASSWD','DB_SERVER','DB_NAME','DB_SID','DB_TYPE'],
        'DB_USER'    => 'testuser',
        'DB_PASSWD'  => 'dUmMyPaSsWd',
        'DB_SERVER'  => 'somemachine.cosmology.illinois.edu',
        'DB_NAME'    => 'baddbname',
        'DB_TYPE'    => 'ORACLE',
        'DB_SID'    => 'thedbname',
    },
    { # Order should not be important
        'testname'   => 'DB_SID ORDER',
        'whichdb'    => undef,
        'keyorder'   => ['DB_SID', 'DB_USER','DB_PASSWD','DB_SERVER','DB_NAME','DB_TYPE'],
        'DB_USER'    => 'testuser',
        'DB_PASSWD'  => 'dUmMyPaSsWd',
        'DB_SERVER'  => 'somemachine.cosmology.illinois.edu',
        'DB_NAME'    => 'baddbname',
        'DB_TYPE'    => 'ORACLE',
        'DB_SID'    => 'thedbname',
    },
    { # comments
        'testname'   => 'COMMENTS',
        'whichdb'    => undef,
        'keyorder'   => ['DB_SID', '#DB_SID', '#DB_USER','DB_USER','DB_PASSWD','DB_SERVER','DB_NAME','#DB_NAME','DB_TYPE'],
        '#DB_USER'   => 'commentuser',
        'DB_USER'    => 'testuser',
        'DB_PASSWD'  => 'dUmMyPaSsWd',
        'DB_SERVER'  => 'somemachine.cosmology.illinois.edu',
        'DB_NAME'    => 'baddbname',
        '#DB_NAME'    => 'commentname',
        'DB_TYPE'    => 'ORACLE',
        'DB_SID'    => 'thedbname',
        '#DB_SID'    => 'commentsid',
    },
    { 
        'testname'   => 'standby',
        'whichdb'    => 'standby',
        'keyorder'   => ['DB_TYPE', 'DB_USER','DB_PASSWD','DB_SERVER','DB_NAME','DB_SERVER_STANDBY', 'DB_NAME_STANDBY'],
        'DB_TYPE'    => 'ORACLE',
        'DB_USER'    => 'testuser',
        'DB_PASSWD'  => 'dUmMyPaSsWd',
        'DB_SERVER'  => 'primary.cosmology.illinois.edu',
        'DB_NAME'    => 'prdes',
        'DB_SERVER_STANDBY' => 'somemachine.cosmology.illinois.edu',
        'DB_NAME_STANDBY'   => 'thedbname',
    },
    {
        'testname'   => 'proc',
        'whichdb'    => 'proc',
        'keyorder'   => ['DB_TYPE', 'DB_USER','DB_PASSWD','DB_SERVER','DB_NAME','DB_SID', 'DB_SERVER_STANDBY', 'DB_NAME_STANDBY', 'DB_SERVER_PROC', 'DB_NAME_PROC', 'DB_SID_PROC'],
        'DB_TYPE'    => 'ORACLE',
        'DB_USER'    => 'testuser',
        'DB_PASSWD'  => 'dUmMyPaSsWd',
        'DB_SERVER'  => 'desdb.cosmology.illinois.edu',
        'DB_NAME'    => 'prdes',
        'DB_SID'     => 'wrongsid',
        'DB_SERVER_STANDBY'  => 'desorch.cosmology.illinois.edu',
        'DB_NAME_STANDBY'  => 'stdes',
        'DB_SERVER_PROC'  => 'somemachine.cosmology.illinois.edu',
        'DB_NAME_PROC'  => 'wrongprocname',
        'DB_SID_PROC'  => 'thedbname',
    },
);

my $cnt = 1;
foreach $t (@passtests) {
    my $testname = $t->{'testname'};
    my $keyorder = $t->{'keyorder'};
    my $whichdb = $t->{'whichdb'};

    # write test config file
    ($fh, $filename) = tempfile();
    foreach my $k (@$keyorder) {
        print $fh $k," = ", $t->{$k}, "\n";
    }
    close $fh;

    eval {
        $got = DB::Connection::parse_db_config_file(undef, $filename, $whichdb);
    };
    if ($@) {
        warn ("parse_db_config_file:  Error ", $@);
    }
    else {
#        print Dumper(\%expected),"\n";
#        print Dumper($got),"\n";
        is_deeply($got, \%expected, $testname);
    }
    unlink $filename;
    $cnt++;
}
