#!/usr/bin/perl

use strict;
use warnings;
use Test::More tests => 5;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use DAF::Archive::Tools qw(:all);

my $dir;

# Test failures:
my %f1 = ('project' => 'DES');
my %f2 = ('project' => 'DES','fileclass' => 'src');
my %f3 = ('project' => 'DES','fileclass' => 'red', 'run' => 'DES_20080306121303', 'filetype' => 'red');
my %f4 = ('project' => 'DES','fileclass' => 'red', 'run' => 'DES_20080306121303', 'filetype' => 'red_kjhfd');

eval{$dir = getArchiveLocation(\%f1);};
isnt($@, undef);
eval{$dir = getArchiveLocation(\%f2);};
isnt($@, undef);
eval{$dir = getArchiveLocation(\%f3);};
isnt($@, undef);
eval{$dir = getArchiveLocation(\%f4);};
isnt($@, undef);

# Test Sucesses:
my %s1 = ('project' => 'DES','fileclass' => 'src','filetype'=>'src', 'nite' => '20071011');
my %s2 = ('project' => 'DES','fileclass' => 'red','filetype'=>'red', 'nite' => '20071011');
$dir = getArchiveLocation(\%s1);
is($dir,'DES/src/20071011/src');

exit(0);
