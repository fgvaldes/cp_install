########################################################################
#
#  $Id: wrapDBI.pm 2559 2008-12-03 21:21:13Z dadams $
#
#  $Rev:: 2559                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-12-03 15:21:13 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
# History:
#   06-Jul-2010 Added access to dbtype member variable
################################################################################
################################################################################
package DB::Connection;
use strict;
use warnings;
use NEXT;
use Data::Dumper;
use Switch;
use Carp;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use base qw(DBI);

################################################################################
# Constructor for a new DB::Connection object which is really a DBI::db object
# blessed into the calling Class::db.
#
# THIS CONSTRUCTOR MUST BE CALLED
#  --It will not work unless leftmost in the @ISA since
#  DBI connect requires that it bless its' own objects.
#  
################################################################################
sub new {
  my ($this, %Args) = @_;
  my $self = $this->connect(%Args);
  $self->EVERY::LAST::_init(%Args);
  return $self;
}


################################################################################
#
################################################################################
sub connect {
  my ($self, %Args) = @_;

  my ($dbh, %ConnectData, $type, $server, $name, $sid, $user, $pass);
  my $DBIattr = {};
  my $ConfigVals = {};
  #my @ConnectDataKeys = ('db_type','db_server','db_name','db_user','db_pass');
  my @ConnectDataKeys = ('db_type','db_server','db_name','db_sid','db_user','db_passwd');


  # If a config file is said to exist, parse it.
  my $whichdb = undef;
  if (defined($Args{'which_db'})) {
     $whichdb = $Args{'which_db'};
  }
  if ($Args{'db_config_file'}) {
    $ConfigVals = $self->parse_db_config_file($Args{'db_config_file'},$whichdb);
  }
  else {
    croak("Must provide a database connection parameter file.\n");
  }

  # If a hash reference is provided as an argument, assume these are DBI attributes 
  # to be passed to the connection:
  foreach my $Arg (values %Args) {
    if (ref $Arg eq 'HASH') {
      $DBIattr = $Arg;
      last;
    }
  }

  # As of March 2011, no overriding allowed, so die if override attempted
  # Fill in the gaps or override with stuff from Args
  foreach my $key (@ConnectDataKeys) {
    if (defined $ConfigVals->{$key}) {
      $ConnectData{$key} = $ConfigVals->{$key};
      if ($Args{'debug'}) {
        print "$key defined.\n";
      }
    }
    else {
      if ((lc($ConfigVals->{'db_type'}) eq 'oracle') && ($key eq 'db_sid')) {
         croak("Insufficient data to make a database connection. Missing $key.\n");
      }
    }
    if (defined $Args{$key}) {
      #$ConnectData{$key} = $Args{$key};
      croak("Overriding connection properties file values not longer permitted: $key.\n");
    }
  }
  $type = $ConnectData{'db_type'};
  $server = $ConnectData{'db_server'};
  $name = $ConnectData{'db_name'};
  $sid = $ConnectData{'db_sid'};
  $user = $ConnectData{'db_user'};
  $pass = $ConnectData{'db_passwd'};

  # Verbose connection status output:
  if ($Args{'debug'}) {
    if ($DBIattr) {
      print "Connecting to the $name database on $server with the following DBI attributes:\n";
      while ((my $k, my $v) = each %{$DBIattr}) {
        print "$k = $v\n";
      }
    }
  }
  ##############################################################################
  # Oracle connection
  ##############################################################################
  if ($type =~ /ORACLE/i) {

    $dbh = $self->SUPER::connect("DBI:Oracle:host=$server;sid=$sid",$user,$pass, $DBIattr)
                or croak("Database connection error: $DBI::errstr\n");
  }
  ##############################################################################
  # Postgres connection                (24-Mar-2010 jp Added postgres connection
  ##############################################################################
  elsif ($type =~ /POSTGRES/i) {
    $dbh = $self->SUPER::connect("dbi:Pg:host=$server;dbname=$name;user=$user",$user,$pass, $DBIattr)
                or croak("Database connection error: $DBI::errstr\n");
  }
  ##############################################################################
  # Another connection...
  ##############################################################################
  else {
    croak("No connection method yet defined for db_type: $type.\n");
  }

  # set database type variable for use in other utility routines
  $dbh->dbtype( $type );

  return $dbh;
}


################################################################################
# Parse_db_config_file 
#
# Notes:
# - DB-specific config values are of the form  key_whichdb  (Ex: DB_SERVER_STANDBY)
# - Generic config values are of the form key (Ex: DB_USER)
# - If values not specified for specific DB, then default to generic value
#   Example:  If DB_USER_STANDBY not defined, look for DB_USER 
# - Return values just use the lower case key as the hash key (i.e. db_server instead of DB_SERVER_STANDBY)
# - Leaves checking for missing data to function that makes connection
################################################################################
sub parse_db_config_file {
    my $self = shift;
    my $file = shift;
    my $whichdb = shift;
    my $origwhich = $whichdb;

    # actual connection keywords (in hash for quick check)
    #my %keywords=('db_type'=>1, 'db_user'=>1,'db_pass'=>1,'db_server'=>1,'db_name'=>1);
    my %keywords=('db_type'=>1, 'db_user'=>1,'db_passwd'=>1,'db_server'=>1,'db_name'=>1,'db_sid'=>1 );

    # other keywords allowed in config file that override main keywords
    #my %overrides = ( 'db_passwd'=>'db_pass', 'db_sid'=>'db_name');
    #my %overrides = ( 'db_sid'=>'db_name');

    croak("Unable to read: $file\nto parse for database connection information.\n") if (! -r $file);

    # Knock permissions back to 0600 to keep passwords safe.
    # TODO - Check current permissions and warn user if not strong enough
    chmod 0600, $file or warn "Unable to chmod $file, maybe you don't own the file...\n";

    open(FH, "< ".$file) or croak("Unable to open: $file\n to parse for database connection information.\n");

    # Parse the config file, looking for the standard info:
    my ($line, $left, $right);
    my $linenum = 0;
    my %info;
    while($line = <FH>) {
        $linenum++;

        # remove beginning and trailing white space
        $line =~  s/^\s+//g;
        $line =~ s/\s+$//g;

        # skip empty lines or comment lines that start with #
        if (($line =~ /\S/) && ($line !~ /^#/)) {
            # no longer allow '=' in lines
            # Allow lines like "left = right" as well as "left right"
            #if ($line =~ /=/) {
            #    ($left, $right) = $line =~ m/^(\S+)\s*=\s*(\S.*)$/;
            #}
            #else {
                ($left, $right) = $line =~ m/^(\S+)\s*(.*)$/;
            #}

            $left = lc($left);

            # Check whether recognize key.  If not, print warning
            #if (exists($keywords{$left}) || !exists($overrides{$left})) {
            if (exists($keywords{$left})) {
                $info{$left} = $right;
            }
            else { # remove the whichdb part of the left and check again
                my ($leftpref) = $left =~ m/(.+)_.+/;
                #if (!exists($keywords{$leftpref}) && !exists($overrides{$leftpref})) {
                if (exists($keywords{$leftpref})) {
                    $info{$left} = $right;
                }
                else {
                    warn "\nWARNING: Unrecognized line:\n$line\nin db config file:'$file'\n";
                }
            }
        }
    }
    close FH;

    # if missing whichdb value, assign a dummy value to avoid undef messages
    if (!defined($whichdb)) {
        $origwhich = "default";
        $whichdb = "undef";
    }
    $whichdb = lc($whichdb);
# 'primary' no longer is means 'nosuffix'
#    if ($whichdb eq "primary") {
#        $origwhich = "primary";
#        $whichdb = "undef";
#    }

    my %Data;
    my $cntwhich = 0;

    # save data based upon keywords first
    foreach my $k (keys %keywords) {
        if (defined($info{"${k}_$whichdb"})) {
            $Data{$k} = $info{"${k}_$whichdb"};
            $cntwhich++;
        }
        elsif (defined($info{$k})) {
            $Data{$k} = $info{$k};
        }
    }

    # apply any override values
    #foreach my $overkey (keys %overrides) {
    #    my $k = $overrides{$overkey};
    #    if (defined($info{"${overkey}_$whichdb"})) {
    #        $Data{$k} = $info{"${overkey}_$whichdb"};
    #        $cntwhich++;
    #    }
    #    elsif (defined($info{$overkey})) {
    #        $Data{$k} = $info{$overkey};
    #    }
    #}

    if (($cntwhich == 0) && ($origwhich ne "default")) {
       #printf "Warning: Did not find any keys specific to $origwhich DB. All values are defaults.\n\n";
       croak( "Did not find any keys specific to $origwhich DB");
    }

    return \%Data;
} # end parse_db_config_file


package DB::Connection::db;
our @ISA = qw(DBI::db);

################################################################################
#  Initialize class data for all derived objects of this class.
################################################################################
sub _init {
  my ($self, %Args) = @_;
  $self->verbose(1);
  $self->debug(0);
  $self->verbose($Args{'verbose'}) if (defined $Args{'verbose'});
  $self->debug($Args{'debug'}) if (defined $Args{'debug'});
#  $self->dbtype($Args{'db_type'}) if (defined $Args{'db_type'});
  return $self;
}


{
  my %ClassData = ();

  sub verbose {
    my($self, $newvalue) = @_;
    if (@_ > 1) {
      $ClassData{'verbose'} = $newvalue;
    }
    return $ClassData{'verbose'};
  }

  sub debug {
    my($self, $newvalue) = @_;
    if (@_ > 1) {
      $ClassData{'debug'} = $newvalue;
    }
    return $ClassData{'debug'};
  }

  sub dbtype {
    my($self, $newvalue) = @_;
    if (@_ > 1) {
      $ClassData{'dbtype'} = $newvalue;
    }
    return $ClassData{'dbtype'};
  }

  sub isOracle {
     my($self) = @_;
     return (lc($ClassData{'dbtype'}) eq "oracle");
  }

  sub isPostgres {
     my($self) = @_;
     return (lc($ClassData{'dbtype'}) eq "postgres");
  }
}

package DB::Connection::st;
our @ISA = qw(DBI::st);

1;
