#  $Id: Checksum.pm 6914 2011-06-15 21:02:59Z mgower $
#  $Rev: 6914 $: Revision of last commit
#  $Author: mgower $: Author of last commit
#  $Date: 2011-06-15 14:02:59 -0700 (Wed, 15 Jun 2011) $: Date of last commit

package Checksum;

# ankit

#use 5.008008;
use strict;
use warnings;
use FileHandle;
use DBI;
use Switch;
use Cwd 'abs_path';
use Benchmark qw(:all);
use DBI;
use DBD::Oracle;
use Data::Dumper;

#check 1

use File::Find::Rule;
use File::Path 'rmtree';
use DB::DESUtil;

use Digest::MD5;
use List::MoreUtils qw( any );
use Astro::FITS::CFITSIO;
use Astro::FITS::CFITSIO qw( :longnames );
use Astro::FITS::CFITSIO qw( :shortnames );
use Astro::FITS::CFITSIO qw( :constants );
use FindBin qw($Bin);
use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use DB::EventUtils;
use DB::FileUtils;

use vars qw($db_dsn $db_username $db_password $Checksum_fromscript );
$db_dsn = 'DBI:Oracle:host=desdb.cosmology.illinois.edu;sid=des';




require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Checksum ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';


# Preloaded methods go here.

sub writeDb_checksum
{
	my ($db_handle, $file_dbid, $filetype, $md5sum) = @_;
	my $table_column = ''; 
	
	
	switch($filetype)
	{
		case 'fits' { $table_column = 'checksum';}
		case 'fpack' {  $table_column = 'checksum_fz';}
		case 'gzip' { $table_column = 'checksum_gz';}
		case 'other' { $table_column = 'checksum';}	
	}
	
	my $sql_update = "update location set $table_column = $md5sum";
	print "the sql statement is: $sql_update";	
}

#write md5sum into the database
sub writeMd5Sum
{
	
	# need file handle, filepath, data base handle and file id in the database to verify a file's checksum information in the DB
	my ($filehandle, $filepath, $db_handle, $file_dbid) = @_; 
	
	my $calc_md5sum = 0;
	my $db_checksum = 0;
	my $filetype = '';
	my $file = '';
	my $verified = 0;
	
	# if a DB handle is not present, connect to the db and get a new db handle
	if(! defined $db_handle)
	{
		$db_handle = connectDatabase();
	}
	
	if($filepath =~  m/^.*\.fits$/i)
	{
		$filetype = 'fits';
	}
	elsif($filepath =~  m/^.*\.fits\.fz$/i)
	{
		$filetype = 'fpack';
	}
	elsif($filepath =~  m/^.*\.gz$/i)
	{
		$filetype = 'gzip';
	}
	else
	{
		$filetype = 'other';
	}

	print "md5sum val";	
	$calc_md5sum = calculateMd5Sum($filehandle,$filepath);
	$db_checksum = writeDb_checksum($db_handle, $file_dbid, $filetype, $calc_md5sum);
	
	if ($db_checksum != $calc_md5sum)
	{
		$verified = 0;
	}
	else
	{
		$verified = 1;
	}
	
	return $verified ;
	
	#ankit jkj
}

sub connectDatabase
{
	my $dbh = DB::DESUtil->new(

    DBIattr => {
        AutoCommit => 1,
        RaiseError => 1,
        PrintError => 0
    }
);
	print "\n getting the data base handle: in the function ";
	#my $dbh = DBI->connect($db_dsn, $db_username, $db_password,  { AutoCommit => 1 }) or print " \ncould not connect to the DB..";
	return $dbh;
	
}

sub disconnectDatabase
{
	my $db_handle = @_;
	$db_handle->disconnect();
}

sub getDb_checksum
{
	my ( $db_handle, $file_dbid, $filetype,$checksum, $checksum_fz, $checksum_gz) = @_;
	
	# get the checksum depending on the file types: normal, zipped
	
	my $db_checksum;
	my $field_name  = '';
	my $row;
    
    if(! defined $db_handle)
    {
    	#print "\n getting the db handle";
    	$db_handle = connectDatabase();
    }
    
    	switch($filetype)
		{
			case 'fits'		{ 
				#print "\n fits case \n"; 
				$field_name = "CHECKSUM";
				$db_checksum = $checksum;
			}
			case 'fpack'	{ 
				#print "\n fpack case \n"; 
				$field_name = "CHECKSUM_FZ";
				$db_checksum = $checksum_fz;
			}
				
			case 'gzip'		{ 
				#print "\n gzip case \n"; 
				$field_name = "CHECKSUM_GZ";
				$db_checksum = $checksum_gz;			
			}
			case 'other'	{ 
				#print "\n normal other case \n"; 
				$field_name = "CHECKSUM";
				$db_checksum = $checksum;
			}
			else
			{
				print "\n \t file type mismatch!!! : $filetype";
				$field_name = "CHECKSUM";
				$db_checksum = $checksum;
			}
		}
	
    
    if(!defined $checksum && !defined $checksum_fz && !defined $checksum_gz)
    {
    	if(defined $file_dbid)
    	{  
			my $sql = "select $field_name from new_location where ID = $file_dbid";
			#print $sql;
			my $stmt = $db_handle->prepare($sql)   or print "Could not prepare queries aborting";
		    $stmt->execute() or print "Found error ".$stmt->errstr; #&displayError(" Couldnt execute statement:  " .$stmt->errstr);
		    while ( $row = $stmt->fetchrow_hashref ) {
			print "the value of checsum is "; print $row->{'CHECKSUM'};
			#print Dumper($row);
			    $db_checksum = $row->{$field_name};
		    }
		    print "\n\n db checksum = $db_checksum";
    	}
	    
	    #disconnectDatabase($db_handle);
    }
    
    #$db_handle->disconnect();
    print "\n \t checksum received $checksum and the db checksum $db_checksum";
    return $db_checksum; #'d186cbbad9bfa54178ca745c40c83d42';
	
}

#verify md5sum of a file from the database
sub verifyMd5Sum
{
	# need file handle, filepath, data base handle and file id in the database to verify a file's checksum information in the DB
	my ($filehandle, $filepath, $db_handle, $file_dbid, $checksum, $checksum_fz, $checksum_gz, $filetype) = @_; 
	
	my $calc_md5sum = 0;
	my $db_checksum = 0;
	my $status;
	
	# if a DB handle is not present, connect to the db and get a new db handle
	if(! defined $db_handle)
	{
		$db_handle = connectDatabase();
	}
	
	if($filepath =~  m/^.*\.fits$/i)
	{
		$filetype = 'fits';
	}
	elsif($filepath =~  m/^.*\.fits\.fz$/i)
	{
		$filetype = 'fpack';
	}
	elsif($filepath =~  m/^.*\.gz$/i)
	{
		$filetype = 'gzip';
	}
	else
	{
		$filetype = 'other';
	}
	
	$calc_md5sum = calculateMd5Sum($filehandle,$filepath);
	$db_checksum = getDb_checksum($db_handle, $file_dbid, $filetype, $checksum, $checksum_fz, $checksum_gz);
	
	
	#$db_checksum = hex 0;
	
	#print "calcuated md5sum is ->$calc_md5sum<- and received md5sum = ->$db_checksum<-";
	
	#$calc_md5sum =  sprintf("%X", $calc_md5sum);
	#$db_checksum =  sprintf("%X", $db_checksum);
	#print "\n new: calcuated md5sum is ->$calc_md5sum<- and received md5sum = ->$db_checksum<-";
	
	print "\n \t $filepath 1: $calc_md5sum 2: $db_checksum\n";
	
	if ( "$db_checksum" ne  "$calc_md5sum")
	{
		#print "\n the sum is verified as false  ";
		$status = 1;
	}
	else
	{
		#print "\n the sum is verified as true  ";
		$status = 0;
	}
	
	return $status ;
		
}

# reads md5sum from the database
sub calculateMd5Sum
{
	my $digest = '';
	my $file = '';
	my $starttime = 0;
	my $endtime = 0;
	my $filehandle;
	my $filepath;

	# ensure parameters have been passed.
	($filehandle, $filepath, $digest) = @_; 
		
	#print "\n in calc md5 sum the filepath: $filepath and the curr eror $!";
    #my $fp = FileHandle->new($filepath,'r') or print " error for $filepath: $!";
    
    if(! defined $filehandle)
    {
	#print "\n MD5SUM getting a enw file handle for path $filepath";
    	if(! defined $filepath)
    	{
		#print "no file path found!";
    		return 1;
    	}
    	
    	#$filehandle = FileHandle->new($filepath,'r') or print "error in reading file -> $!";
    	$filehandle = FileHandle->new($filepath,'r') or return 1; #print "error in reading file -> $!";
    	
    }
    
    my $ctx = Digest::MD5->new;
    $ctx->addfile($filehandle);
    $$digest = $ctx->hexdigest;
    close($filehandle);
	return 0;	
	#return $$digest;
		
}

#uses cfistio functionality to straightaway verify whether a checksum is valid in the file. needs file pointer or absolute file path as param 
sub verifyCfitsioChecksum
{
	my ($filepath, $filehandle, $allHdus) = @_;
	my $isDataOk;
	my $isHduOk;
	my $status = 0;
	my $ret;
	my $i;
	my $hdunum;
	
	print "the input file path is $filepath and file handle is $filehandle";
	if(! defined $filehandle)
	{
		print "\n check 1-1";
		if($filepath eq '')
		{
			print "\n check err 1";
			return 1; # no inputs provided. error.
		}
		
		# in absence of file handle, open the file and create a file handle
		$filehandle = Astro::FITS::CFITSIO::open_file($filepath, Astro::FITS::CFITSIO::READONLY(),$status);
		if($status != 0)
		{
			print "error in opening file $status";
			return $status;
		}
					
	}
	
	#default to allHdus = 1 when not specified.
	if (! defined $allHdus)
	{
		$allHdus = 1;
	}
	
	print "\n check 1";
	if($allHdus == 1)
	{
		
		Astro::FITS::CFITSIO::fits_get_num_hdus($filehandle, $hdunum, $status);
		print "\n the number of hdus found: $hdunum and the stat: $status";
		#$retstr = "\n total hdus: $hdunum ";
		
		for ($i = 1; $i<=$hdunum; $i++ )
		{
			print "\n going after the hdu number $i ";
			Astro::FITS::CFITSIO::fits_movabs_hdu ($filehandle, $i, my $hdutype, $status);
			print "\n moved to the next header... status: $status hdutype $hdutype";
			
			Astro::FITS::CFITSIO::fits_verify_chksum ($filehandle, $isDataOk, $isHduOk, $status);
			print "\n\t total hdus: $isHduOk datasum $isDataOk and stat $status ";
			
			$ret->{$i}{'status'} = $status;
			$ret->{$i}{'data'} = $isDataOk; 
			$ret->{$i}{'hdu'} = $isHduOk;
			
		}
	}
	else
	{
		print "\n check 2";
		# run the verify routine for the current hdu (CHDU)
		Astro::FITS::CFITSIO::fits_verify_chksum ($filehandle, $isDataOk, $isHduOk, $status);
		
		$ret->{1}{'status'} = $status;
		$ret->{1}{'data'} = $isDataOk;
		$ret->{1}{'hdu'} = $isHduOk;	
	}
	
	
	# return a Hash with data_status and hdu_status carrying the database checksum and header data unit checksum values
	return $ret; 
	
}


#calculate the fits file checksum from the file. needs file path or file handler; returns one checksum data set or a list of all HDU checksums
sub calculateCfitsioChecksum
{
	
	# set the initial params into function variables get the file path/ or the file handle for fits file.
	my ($filepath, $filehandle, $allHdus) = @_;
	
	my $i = 0;
	#print "in the function to check the cfitsio file";
		
	#print "\n opening file name $filename";
	my $status = 0;
	my $hdunum = 0;
	my %ret_allhdus;
	my $retstr = '';
	my $hdusum;
	my $datasum;
	
	
	#default to allHdus = 1 when not specified.
	if (! defined $allHdus)
	{
		$allHdus = 1;
	}

	
	if(!defined $filehandle)
	{
		if($filepath eq '')
		{
			return 1; # no inputs provided. error.
		}
		
		$filehandle = Astro::FITS::CFITSIO::open_file($filepath, Astro::FITS::CFITSIO::READONLY(),$status);
		if($status != 0)
		{
			return $status;
		}
		
		
		if($allHdus == 1)
		{
			Astro::FITS::CFITSIO::fits_get_num_hdus($filehandle, $hdunum, $status);
			print "\n the number of hdus found: $hdunum and the stat: $status";
			#$retstr = "\n total hdus: $hdunum ";
			
			for ($i = 1; $i<=$hdunum; $i++ )
			{
				#print "\n going after the hdu number $i ";
				
				Astro::FITS::CFITSIO::fits_movabs_hdu ($filehandle, $i, my $hdutype, $status);
				#print "\n moved to the next header... status: $stat hdutype $hdutype";
				
				Astro::FITS::CFITSIO::fits_get_chksum($filehandle, $datasum, $hdusum, $status);
				$ret_allhdus{$i}{'status'} = $status;
				$ret_allhdus{$i}{'data'} = $datasum;
				$ret_allhdus{$i}{'hdu'} = $hdusum;
				print "\n\t total hdus: $hdunum datasum $datasum hdusum $hdusum and stat $status ";
				
			}
		}
		else
		{
			Astro::FITS::CFITSIO::fits_get_chksum($filehandle, $datasum, $hdusum, $status);
			$ret_allhdus{1}{'status'} = $status;
			$ret_allhdus{1}{'data'} = $datasum;
			$ret_allhdus{1}{'hdu'} = $hdusum;
		}
		
	    #$filehandle->read_key_str('NAXIS1',$naxis1,undef,$status);	
	}
	
	return %ret_allhdus;
		    		
	#my $ret = Astro::FITS::CFITSIO::fits_get_hdrspace($filehandle,my $keysexist, my $morekeys, my $stat );
	#print " the return val is $ret, val of keysexist $keysexist ; val of morekeys $morekeys and the val of stat $stat";
	
	
}

#calculate and write fits checksum to the header of the file
sub writeCfitsioChecksum
{

	my ($filepath, $filehandle, $allHdus) = @_;
	
	my $isDataOk;
	my $isHduOk;
	my $status = 0;
	my $status_gethdunums = 0;
	my $status_move = 0;
	my $status_read = 0;
	my $status_read3 = 0;
	my $status_write = 0;
	my $status_writechecksum = 0;
	my $ret;
	my $i;
	my $hdunum = 0;
	
	if(! defined $filehandle)
	{
		if($filepath eq '')
		{
			return 1; # no inputs provided. error.
		}
		
		# in absence of file handle, open the file and create a file handle
		$filehandle = Astro::FITS::CFITSIO::open_file($filepath, Astro::FITS::CFITSIO::READWRITE(),$status);
	print "\n \t coming in here to create a new file handle at $filepath and  the status is $status 000";	
		if($status != 0)
		{
			#print "\n check error!! $status";
			return $status;
		}
	}
	
	#default to allHdus = 1 when not specified.
	if (! defined $allHdus)
	{
		$allHdus = 1;
	}
	
	
	if($allHdus == 1)
	{
		Astro::FITS::CFITSIO::fits_get_num_hdus($filehandle, $hdunum, $status_gethdunums);
		#print "\n the number of hdus found: $hdunum and the stat: $status";
		#$retstr = "\n total hdus: $hdunum ";
		
		for ($i = 1; $i<=$hdunum; $i++ )
		{
			#print "\n going after the hdu number $i ";
			Astro::FITS::CFITSIO::fits_movabs_hdu ($filehandle, $i, my $hdutype,  $status_move);
			#print "\n moved to the next header... status: $status_move hdutype $hdutype";
			Astro::FITS::CFITSIO::fits_read_keyword ($filehandle, "CHECKSUM", my $value, my $comment, $status_read);
			#print "\n the keyword value: $value  the comment $comment the status $status_read";
			
			if($status_read == 202 )
			{
				
				Astro::FITS::CFITSIO::fits_write_key_null($filehandle, "CHECKSUM", "", $status_write);
				#print "\n\t FOUND STATUS 202... the new null checksum  write status $status_write ";
			}
			
			Astro::FITS::CFITSIO::fits_write_chksum($filehandle,  $status_writechecksum);
			#print "\n\t write status $status_writechecksum ";
			
			#Astro::FITS::CFITSIO::fits_read_keyword ($filehandle, "CHECKSUM", $value, $comment,  $status_read3);
			#print "\n the keyword value: $value  the comment $comment the status $status_read3";

			$ret->{$i}{'status'} = $status_writechecksum;
			
			
		}
	}
	else
	{
		# run the verify routine for the current hdu (CHDU)
		
		Astro::FITS::CFITSIO::fits_write_chksum($filehandle,  $status_writechecksum);
		#Astro::FITS::CFITSIO::fits_verify_chksum ($filehandle, $isDataOk, $isHduOk, $status);
		
		$ret->{1}{'status'} = $status_writechecksum;
		
	}
	
	# return a Hash with data_status and hdu_status carrying the database checksum and header data unit checksum values
	#print "all hdus done";
	return $ret; 

}

#print "hello, here is the test: \n";

#getMd5sum('/home/ankitc/test/runsummary','cfitsio','1','1');

sub traverseDirold
{
	my $dirpath = "";
	($dirpath) = @_;
	
	if($dirpath eq '')
	{
		die "No dir path found!";
	}
	
	print "going to traverse the directory path $dirpath  \n\n";
	
	opendir (my $dh,$dirpath) or die "open failed : $!\n";
	
	
	foreach my $val (readdir $dh)
	{		 
			if (-d $val ) {
				if( $val ne '..' &&  $val ne '.')
				{
		    		print "$val is a directory!, going to traverse it... \n\n";
		    		if(defined $dh)
		    		{
		    			#closedir ($dh) or die "close failed : $!\n";
		    		}
		    		traverseDir($val);
				}
				else
				{
					print "$val is a directory, but ../. , WILL NOT traverse it. \n\n";
				}
		    
		}
		elsif (-f $val) {
		    print "$val is a leaf!\n\n";
		}
		else {
		    print "$val is something else!\n\n";
		}
		#process($_) if /^$regex$/ 
	}
	
}

# hello
sub traverseDir
{
	
	my $dirpath = "";
	my $verbosity;
	my $filecount = 0;
	my $fitsfilecount = 0;
	my $md5sumtime = 0;
	my $fitstime = 0;
	my $t0 = 0;
	my $t1 = 0;
	my $t2 = 0;
	my $notfits;
	my $fitsret = '';
	my $file ;
	my $md5ret;
	
	
	($dirpath, $verbosity) = @_;	
	
	for $file (

	File::Find::Rule->new
		->file()
		->in($dirpath),)
{
	$filecount++;
	my $file_abs_path = abs_path($file);
	#print " \n the file name: $file, finding the md5 for this. The abs path is $file_abs_path";
	
	print "\n $file";
	#$t1 = Benchmark->new; #time;
	$md5ret = getMd5sum($file_abs_path, 'normal', '1', '1');
	#$t2 = Benchmark->new; #time;
	
	if($md5ret > 0)
	{
		
	}
	
	#$md5sumtime = $md5sumtime + $t2 - $t1;
	
	if($file =~  m/^.*\.fits(\.\w*)*$/i)
	{
		$fitsfilecount++;
		#print "\n $file is fits file";
		$t1 = Benchmark->new; #time;
		
		$t0 = Benchmark->new;
		$fitsret = getCfitsioChecksum($file_abs_path);
		$t1 = Benchmark->new; #time;
		
		#$fitstime = $fitstime + $t2 - $t1;
		my $td = timediff($t1,$t0);
		my $timediff  = timestr($td, 'nop');
		$timediff =~ m/(\s{1,})(\d{1})(.*)/;
		
		$fitstime =  $fitstime + $2;
		print "\n fits time taken: $2 file count $fitsfilecount and total time taken $fitstime";
		
	}
	else
	{
		#print "\n $file is not a fits file!";
		$notfits++;
	}
	
	#print "\nfile count: $filecount";
}

my %retvals = ("md5time", $md5sumtime, "fitstime", $fitstime, "filecount", $filecount, 'fitsfilecount', $fitsfilecount, 'notfits', $notfits);

return %retvals;
}

sub readFileintoHash
{
	my ($fp) = @_;
	my $fileDetsHashRef;
	my @fileDetailParams;
	my $fileIterator;
	my @lines;
	if(defined $fp)
	{
		 @lines = <$fp>;
	}
	else
	{
		#print " could not open the file name: the error is: $!";
		die " could not open the file name: the error is: $! exiting...";
	}
	
	foreach my $filelist (@lines)
	{
		chomp($filelist);
		@fileDetailParams = split (/ +/,$filelist);
		if(defined $fileDetailParams[0] && $fileDetailParams[0] ne '|X|')
		{
			$fileDetsHashRef->{$fileDetailParams[0]}->{'file_path'} = $fileDetailParams[0];
		}
		if(defined $fileDetailParams[1] && $fileDetailParams[1] ne '|X|')
		{
			#print "\n got a param!!!!!!\n";
			$fileDetsHashRef->{$fileDetailParams[0]}->{'checksum'} = $fileDetailParams[1];
		}
		if(defined $fileDetailParams[2] && $fileDetailParams[2] ne '|X|')
		{
			$fileDetsHashRef->{$fileDetailParams[0]}->{'checksum_fz'} = $fileDetailParams[2];
		}
		if(defined $fileDetailParams[3] && $fileDetailParams[0] ne '|X|')
		{
			$fileDetsHashRef->{$fileDetailParams[0]}->{'checksum_gz'} = $fileDetailParams[3];
		}
		
		#push@({$fileDetsHashRef->{$fileDetailParams[0]}->{'checksum_gz'}},$fileDetailParams[3]);
	}
	
	return $fileDetsHashRef;
}



sub processArgs
{
	my @args = @_;
	my $fileNameDetsHashref;
	my $md5Status;
	my $filehandle;
	my $dbhandle;
	my $file_dbid;
	my $verbosity;
	my $filelistpath;
	my $dirpath;	
	my $filepath;

	#print "\n in the func for dump @args ::: "; print $args[3];

	if($args[2] eq '-verbose')
	{
		#print "\n the variavle verbose IS THERE !!!";
		$verbosity = $args[3];
	}
	else
	{
		$verbosity = 1;
	}
	
	
	switch($args[0])
	{
		case '-filelist' 
		{
			$filelistpath = $args[1];
			
			if (!-e $filelistpath)
			{
				reportEvent($verbosity,'STATUS',5, " the filepath: $filelistpath not found!");
				die(" the filepath: $filelistpath not found!");
				
			}
			
			$filehandle = FileHandle->new($filelistpath,'r');
			if(!defined $filehandle)
			{
				reportEvent($verbosity,'STATUS', 5, "IGNORE: checksum: TEST :Cannot open the file: $filelistpath for checksum processing");
			}
			else
			{
				#print "\n the file handle is found!\n";
				
			}
			
			my $fileHashref = readFileintoHash($filehandle);
			close $filehandle;
			#print "check point 1: the result from readfileintohash "; 
			#print Dumper($fileHashref);
			#my $key (keys %{$fileInfoHashRef}
			foreach my $key (keys %{$fileHashref})
			{
				my $filehandle_next = FileHandle->new($fileHashref->{$key}->{'file_path'});
				
				print "\n the file: ".$fileHashref->{$key}->{'file_path'}." and the checksum: ".$fileHashref->{$key}->{'checksum'};
				$fileNameDetsHashref = filenameResolve($fileHashref->{$key}->{'file_path'});
				$md5Status = verifyMd5Sum( $filehandle_next,
											$fileHashref->{$key}->{'file_path'},
											$dbhandle, $file_dbid,
											$fileHashref->{$key}->{'checksum'},
											$fileHashref->{$key}->{'checksum_fz'},
											$fileHashref->{$key}->{'checksum_gz'},
											$fileNameDetsHashref->{'FILETYPE'});
				
				#($filehandle, $filepath, $db_handle, $file_dbid,$checksum, $checksum_fz, $checksum_gz, $filetype) 							
				if($md5Status != 0)
				{
					reportEvent($verbosity,'STATUS',5, " md5 checksum failed at ".$fileNameDetsHashref->{$key}->{'file_path'});
				}
				
				close $filehandle_next;
				
			}
			
			
	#read file,extract values, 	determine file type calc checksum	compare with db/ or the value u got		
			
		}
		case '-directorypath' 
		{
			$dirpath = $args[1];
			if (-e $dirpath)
			{
				die("$dirpath not found!");
			}
			
			traverseDir($dirpath,$verbosity);
			
		}
		case '-filepath' 
		{
			$filepath = $args[1];
			if (-e $filepath)
			{
				die("$filepath not found!");
			}
		}
		
		else
		{
			die "unexpected parameter. run this file as Checksum.pm -(filepath | filelistpath | directorypath) ###Argument Value#### -verbose (1|2)";
		}
			

	}


		if($filelistpath.$dirpath.$filepath eq '')
		{
			print " Argument missing!! Please provide at least one of filelist path as:\n -filelist full-path-to-filelist\n -directory full-path-to-directory\n -filepath full-path-to-file";
			exit;
		}
}

#writeCfitsioChecksum('/home/ankitc/test/decam--25--42-r-6_62.fits.fz');
# /data/Archive/DES/red/20091111141553_20091001/red/decam--30--42-i-2/decam--30--42-i-2_08_cat.fits

#getDb_checksum(undef , 16777592, 'fits');

#print Dumper(@ARGV);

#my $result_md5verification = verifyMd5Sum($filehandle, "/home/ankitc/test/decam--30--42-i-2_08_cat.fits", $db_handle, 16777594);
#print "\n the return value from the function is $result";

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

Checksum - Perl extension for creating, verifying and storing checksum values for all files in the DES system.

=head1 SYNOPSIS

  use Checksum;
  blah blah blah

=head1 DESCRIPTION


The Checksum module has the following functionality:

Fits files

This module uses Cfitsio library/module for working on the fits files, and has the following functionality

1. Verify the checksum in the HDUs of the fits files. All HDUs are checked by default. But this can be over ridden by passing appropriate flag.
2. Calculate the checksum for the HDUs of the fits files. Checksums for All HDUs are calculated by default. But this can be over ridden by passing appropriate flag.
3. Write the checksums in the HDUs of the fits files. Checksums for All HDUs are calculated by default. But this can be over ridden by passing appropriate flag.


All Other files, including Fits files

This module uses Digest::MD5 module for working on the files, and returns a hexadecimal value for the checksum. It supports following functionality

1. Verify the md5sum of a file by calculating a fresh md5sum of the actual file, and then comparing it with the checksum stored for the file in the Database in the LOCATION table
2. Calculate the md5sum of the file as it exists now and return the value to the calling function in hexadecimal format
 

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

Ankit Chandra, E<lt>ankitc@ncsa.illinois.edu<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010 by Ankit Chandra

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
