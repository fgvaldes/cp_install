import os
import cx_Oracle
import psycopg
import string

def parse_db_config_file( config_file=None, dbkey=None, diag=False ):
	"""
	Parse the file named as an argument and return a connection for the user named,
	with the password named, to the database named. The file should consist of lines
	of the form
	  [optional whitespace]<key><whitespace><value>[optional whitespace]"
	key and value must not contain any whitespace. Blank lines and lines beginning
	with a hash (#), or whitespace followed by a hash, are ignored. If a line has 
	any other form, or if key is not in a list of allowed keys, a warning will be
	returned.
	The name of the configuration file used is
	   1) taken from the first argument if it exists, or else
	   2) taken from the environment variable DESDB_PARAMS if it exists, or else
	   3) set equal to $HOME/.desdm
	If dbkey is defined, it is appended with an intervening underscore to the keys
	in keylist.
	If diag is set (True), then diagnostic and debugging information is printed.
	"""

	def print_diag(arg):
		if diag:
			print "... diag: "+arg

	dbconfig = {}

	if not config_file:
		config_file = os.getenv('DESDB_PARAMS',
			os.path.join(os.environ["HOME"],".desdm"))
	print_diag("config_file = "+config_file)

	try:
		lines = open(config_file).read().split('\n')
	except IOError:
		print "Cannot read DB configuration file", config_file
		pass
	else:
		keylist = ["TYPE", "USER", "PASSWD", "SERVER", "NAME", 
				"SID", "SERVER_STANDBY", "NAME_STANDBY" ]
		for l in lines:
			key_value_pair = l.split()
			if len(key_value_pair) == 0 or key_value_pair[0][0] == '#':
				#blank or comment line
				continue
			elif len(key_value_pair) != 2:
				print "Ignoring line not of form key<whitespace>value: \""+l+"\""
				continue
			else:
				value = key_value_pair[1]
				key_parts = key_value_pair[0].split('_')
				if len(key_parts) < 2 or len(key_parts) > 3 or key_parts[0] != "DB":
					print "Ignoring line with key ("+key_value_pair[0]+") not of form DB_<param>[_<dbkey>]: \""+l+"\""
					continue
				elif not key_parts[1] in keylist:
					print "Ignoring line with improper parameter ("+key_parts[1]+") in key: \""+l+"\""
				else:
					print_diag("accepted line \""+l+"\"")
					param = key_parts[1]
					dbkey = key_parts[2] if len(key_parts) == 3 else None
					if dbkey not in dbconfig:
						dbconfig.update({dbkey:{param:value}})
					else:
						dbconfig[dbkey].update({param:value})

	# TYPE is required. The allowed values are ORACLE and POSTGRES, case
	# insensitive. The default value is ORACLE.

	for dbkey in dbconfig:
		if "TYPE" not in dbconfig[dbkey]:
			dbconfig[dbkey].update({"TYPE":"ORACLE"})
		dbconfig[dbkey]["TYPE"] = string.upper(dbconfig[dbkey]["TYPE"])

	

	if diag:
		for dbkey in dbconfig:
			print "dbkey =",dbkey
			for param in dbconfig[dbkey]:
				print "   param =",param,",   value =",dbconfig[dbkey][param]


	conn_str = dbconfig[dbkey]["USER"] + "/" + dbconfig[dbkey]["PASSWD"] + \
			"@" + dbconfig[dbkey]["SERVER"] + "/" + dbconfig[dbkey]["NAME"]
	print conn_str
	if dbconfig[dbkey]["TYPE"] == "ORACLE":
		con = cx_Oracle.connect(conn_str)
	else:
		con = psycopg.connection(conn_str)
	return con
