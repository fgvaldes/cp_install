#!/usr/bin/perl -w
########################################################################
#
#  $Id: DAF.pm 6323 2011-01-04 15:49:36Z mgower $
#
#  $Rev:: 6323                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-01-04 08:49:36 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
################################################################################
package DAF;
use strict;
use warnings;
use Data::Dumper;
use Carp;
use Getopt::Long;
use Pod::Usage;
use Config::General;
use Cwd qw(abs_path);
use File::Spec;
use Exception::Class('bail');
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use DAF::DB::Util;
use DAF::Transfer;
use NCSA::Misc::Util;

#######################################################################
# System environment defaults:
#######################################################################
my $VersionFile = "$FindBin::Bin/../VERSION";
my $SystemConfigFile = "$FindBin::Bin/../etc/DAF.config";
my $UserConfigFile;
if ($ENV{'HOME'}) {
  $UserConfigFile = $ENV{'HOME'}.'/.daf/DAF.config';
}

my @db_option_defs = (
'asite|site|archivesite|anode=s@',
    'fileclass|fc|class=s@',
    'filetype|ft|type=s@',
    'project|p=s@',
    'ccd|c=s@',
    'run|runid|r=s@',
    'filename=s@',
    'id=s@',
    'band|b=s@',
    'nite|n=s@',
    'tilename|t=s@',
    'exposurename|e=s@'
);

my %ClassData = (
  'verbose' => 1, 
  'common_option_defs' => 
  [
    #Database fields (location table)
    @db_option_defs,
    'where=s@',
    # Config variables
    'logical_file_table|logical-file-table|lft=s',
    'root_work_directory|root-work-directory|rwd=s',
    #
    'filelist|file-list|fl=s',
    # Runtime related
    'work_directory_prefix|work-directory-prefix|wdp=s',
    'verbose|v+',
    'quiet|q',
    'debug|d',
    'dumpargs',
    'help|h|?',
    'keep_logs|keep-logs|keep|k',
    'man',
    'version'
  ],
  'VERSION' => "svn: ".'$Rev: 6323 $',
  'config' => {
    'logical_file_table' => 'location',
    'root_work_directory' => '/tmp',
   },
  'RunStatus' => 0,

);
  
if ($ENV{'GLOBUS_LOCATION'}) {
  $ClassData{'config'}->{'globus_location'} = $ENV{'GLOBUS_LOCATION'};
}

#######################################################################
#
#######################################################################
sub new {
  my $this = shift;
  my $argsRef = shift;
  my $class = ref($this) || $this;
  my $self = {};
  bless $self, $class;
  $self->verbose($argsRef->{'verbose'}) if ($argsRef->{'verbose'});

  # Read command line:
  my $defs = $self->common_option_defs();
  if ($argsRef->{'additional_cmd_line_opt_defs'}) {
    foreach (@{$argsRef->{'additional_cmd_line_opt_defs'}}) {
      push (@$defs, $_);
    }
  }
  my %opts;
  Getopt::Long::GetOptions (\%opts,@$defs)        
    or Pod::Usage::pod2usage(-verbose => 0,-exitval => 2,-output => \*STDERR);

  if ($opts{'dumpargs'}) {
     print "$0 ";
     while (my ($k,$v) = each(%opts)) {
        print "--$k=";
        if (ref($v) eq 'ARRAY') {
           for (my $i=0; $i < scalar(@$v)-1; $i++) {
              print $$v[$i],",";
           }
           print $$v[scalar(@$v)-1];
        }
        else {
           print "$v";
        }
        print " ";
     }
     for (my $i=0; $i < scalar(@ARGV); $i++) {
        print $ARGV[$i]," ";
     }
     print "\n";
     exit 0;
  }

  # Grab comma separated stuff from relevant command line args and split it up:
  foreach my $def (@db_option_defs) {
    my ($key) = split (/\|/, $def);
    $key =~ s/=.*$//;
    if (exists $opts{$key}) {
      @{$opts{$key}} = split(/,/,join(',',@{$opts{$key}}));
    }
  }

  # Set requested vebose level:
  if (%opts) {
    $self->CmdLineOpts(\%opts);
    $self->setverboseFromArgs(\%opts);
    if ($opts{'debug'}) {
      $self->debug(1);
      print "\nCommand line dump:\n";
      print Dumper (\%opts),"\n\n";
    }
  }
  if ($opts{'help'}) {
    Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
  }
  elsif ($opts{'man'}) {
    Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
  }
  elsif($opts{'version'}) {
    if (-r $VersionFile) {
      $self->readVersionFile($VersionFile);
    }
    #print "\nDark Energy Survey -- Data Access Framework\nVersion: $ClassData{'VERSION'}\n\n";
    print "\nDark Energy Survey -- Data Access Framework\nVersion: ",$self->dafversion(),"\n\n";
    exit(0);
  }
  # Get a new runkey for this run:
  $self->RunKey(NCSA::Misc::Util::getRandomKey(9));

  # Read config file(s) if present:
  if (-r $SystemConfigFile) {
    my %CurrentConfig = %{$self->config()};
    $self->config(
      {
        Config::General::ParseConfig (
          -ConfigFile => $SystemConfigFile,
          -DefaultConfig => \%CurrentConfig,
          -LowerCaseNames => 1,
        )
      }
    );
  }

  if (-r $UserConfigFile) {
    my %CurrentConfig = %{$self->config()};
    $self->config(
      {
        Config::General::ParseConfig (
          -ConfigFile => $UserConfigFile,
          -DefaultConfig => \%CurrentConfig,
          -LowerCaseNames => 1,
        )
      }
    );
  }

  # Override with any command line specifed config options:
  foreach my $key (keys %{$self->config()}) {
    if ($opts{$key}) {
      $self->config()->{$key} = $opts{$key};
    }
  }

  # Find Globus and uberftp Software:
  my $globusbin;
  my $uberbin;
  my $uberftp = 'uberftp';
  my $guc = 'guc';
  my $grid_proxy_info = 'grid-proxy-info';
  if ($self->config()->{'globus_location'}) {
    $globusbin = $self->config()->{'globus_location'}.'/bin';
    $uberbin = $self->config()->{'globus_location'}.'/bin';
  }
  if ($self->config()->{'uberftp_location'}) {
    $uberbin = $self->config()->{'uberftp_location'}.'/bin';
  }
  $uberftp = $uberbin.'/uberftp' if ($uberbin);
  $guc = $globusbin.'/globus-url-copy' if ($globusbin);
  $grid_proxy_info = $globusbin.'/grid-proxy-info' if ($globusbin);
  bail->throw("\nERROR: Unable to locate a 'uberftp' executable. Exiting...\n\n") if (! -x $uberftp);
  bail->throw("\nERROR: Unable to locate a Globus 'globus-url-copy' executable. Exiting...\n\n") if (! -x $guc);
  bail->throw("\nERROR: Unable to locate a Globus 'grid-proxy-info' executable. Exiting...\n\n") if (! -x $grid_proxy_info);

  my $uberftp_version = `$uberftp -version`;
  bail->throw if ($uberftp_version !~ /1\.25/);

  if ($self->debug()) {
    print "\nConfig dump:\n";
    print Dumper($self->config());
    print "\n";
  }

  # Create a log directory for this run:
  if ($opts{'work_directory_prefix'}) {
    $self->LogDir( join ('_',$opts{'work_directory_prefix'},$self->RunKey()) );
  }
  elsif($argsRef->{'logfile_prefix'}) {
    $self->LogDir( join ('_',$argsRef->{'logfile_prefix'},$self->RunKey()) );
  }
  else {
    $self->LogDir( $self->RunKey() );
  }
  if ($self->config()->{'root_work_directory'}) {
    my $absdir = join('/',$self->config()->{'root_work_directory'},$self->LogDir());
    $self->LogDir($absdir);
  }
  if (-d $self->LogDir()) {
    croak ("\nLog directory: ".$self->LogDir()." already exists.\n");
  }
  else {
    mkdir $self->LogDir() or croak("\nUnable to create directory: ".$self->LogDir()."\n");
  }

  print "Using ", $self->LogDir()," for tmp data\n" if ($self->debug());

  # Read the file list and store the data, if provided:
  if ($opts{'filelist'}) {
    $self->filelist($opts{'filelist'});
  }

  # Initialize a database object:
  $self->DB( DAF::DB::Util->new('verbose'=>$self->verbose(), 'debug'=>$self->debug()) );

  # Create hash for key relevant when doing lookup to the master DB table
  # of file records (LOCATION).
  $self->FileQueryKeys(\%opts) if (%opts);
  $self->setFileQueryKeysFromArgs(\%opts) if (%opts);

  # Initialize transfer tools object:
  my %XferArgs;
  $XferArgs{'verbose'} = $self->verbose() if (defined $self->verbose());
  $XferArgs{'LogDir'} = $self->LogDir() if ($self->LogDir());
  $XferArgs{'debug'} = $self->debug() if ($self->debug());
  $self->Xfer( new DAF::Transfer(\%XferArgs) );
  $self->Xfer()->uberftp_client($uberftp);
  $self->Xfer()->guc_client($guc);
  $self->Xfer()->debug(1) if ($self->debug());

  return $self;
}

sub checkGlobusProxy {

  my $self = shift;

  # Find Globus Software:
  my $globusbin;
  my $grid_proxy_info = 'grid-proxy-info';
  if ($self->config()->{'globus_location'}) {
    $globusbin = $self->config()->{'globus_location'}.'/bin';
  }
  $grid_proxy_info = $globusbin.'/grid-proxy-info' if ($globusbin);

  my $notproxy = system("$grid_proxy_info -exists -valid 00:30");
  bail->throw("\nERROR: Cannot verify a valid Globus proxy certificate.\n\n") if ($notproxy);

}

#######################################################################
#
#######################################################################
sub DESTROY {
  my $self = shift;
  if (defined $self->CmdLineOpts() ) {
    if (! defined $self->CmdLineOpts()->{'keep_logs'}) {
      if (defined $self->LogDir()) { 
        if (-d $self->LogDir()) {
          if ($self->RunStatus() == 0) {
            if ($self->verbose() >= 1) {
              print "\nRemoving temporary files...\n";
            }
            system("rm -rf ".$self->LogDir());
          }
        }
      }
    }
  }
}

#######################################################################
#
#######################################################################


sub setFileQueryKeysFromArgs {
  my($self, $opts) = @_;
  my $newvalue;
  my %hash;
  if (@_ > 1) {
    my $cols = $self->DB()->getColumnNames('LOCATION');
    foreach (keys %$cols) {
      if (exists $opts->{"$_"}) {
        $hash{"$_"} = $opts->{"$_"};
      }
    }
    if ($opts->{'asite'}) {
      $hash{'asite'} = $opts->{'asite'};
    }
    if ($opts->{'where'}) {
      $hash{'where'} = $opts->{'where'};
    }
    $hash{'sql'} = $opts->{'sql'} if $opts->{'sql'};
    if ($self->filelist()) {
      $hash{'filelist'} = $self->filelist();
    }
    $newvalue = \%hash;
  }
  if (defined $newvalue) {
    $self->FileQueryKeys($newvalue);
  }
  return $self->FileQueryKeys($newvalue);
}

sub readFileList {
  my $self = shift;
  my $file = shift;
  my $files = shift;
  my ($filehref);

  open (FH, "$file") or die "Cannot open $file";
  my @lines=<FH>;
  #while (my $line = <FH>) {
  foreach my $line (@lines) {
    chomp($line);
    $line =~ s/^\s*//;
    $line =~ s/\s*$//;
    if ($line eq "</file>") {
      push (@$files, $filehref);
    }
    elsif ($line eq "<file>") {
      $filehref = {};
    }
    else {
      (my $left, my $right) = split /\s*=\s*/, $line;
      $filehref->{"$left"} = $right;
    }
  }
  close FH;
}

sub readVersionFile {
  my $self = shift;
  my $file = shift;

  open (FH, "$file") or die "Cannot open $file";
  my @lines = <FH>;
  close FH;

  $lines[0] =~ /:\s*(\S+).*$/;
  my $version = $1;
  $lines[1] =~ /:\D*(\d+).*$/;
  my $revision = $1;
  $lines[2] =~ /:\s*(\S+).*$/;
  my $date = $1;

  my $str = $version."\nSubverion Revision: ".$revision."\nRelease Date: ".$date;

  $self->dafversion($str);
}

sub dumpFileList {
  my $self = shift;
  my $file = shift;
  my $FileList = shift;
  my $AllFilesARef = shift;

  open (FH, ">$file") or bail->throw("Cannot open $file for writing");
  foreach my $i (keys %$FileList) {
    print FH "<file>\n";
    print FH 'fileid=',$AllFilesARef->[$i]->{'location_id'},"\n";
    print FH "</file>\n";
  }
  close FH;

}

sub filelist {
  my($self, $file) = @_;
  my $newvalue;
  my %config;
  my @files;
  if (@_ > 1) {
    if (-r $file) {
      print "\nReading filelist..." if ($self->verbose() >= 5);
      $self->readFileList($file, \@files);
#      %config = Config::General::ParseConfig (
#        -ConfigFile => $file,
#        -LowerCaseNames => 1,
#        -SplitPolicy => 'equalsign'
#      );
      print "Done!\n"  if ($self->verbose() >= 5);
#      my $allowed = $config{'file'};
#      if(ref($allowed) eq "ARRAY") {
#        @files = @{$allowed};
#      }
#      else {
#        @files = ($allowed);
#      }
      # For each file in the list set the localpath key
      # to be an absolute path.
      print "\nScanning filelist for location and filename info..."  if ($self->verbose() >= 5);
      foreach my $file (@files) {
        my ($abs,$rel);
        croak("A filelist containing entires with no fileid information was provided.\n") if (! exists $file->{'fileid'});
        croak("A filelist containing entires with fileid=0 was provided.\n") if ($file->{'fileid'} == 0);
        if ($file->{'localfilename'}) {
          if ($file->{'localpath'}) {
            #$rel = abs_path( join('/',$file->{'localpath'}, $file->{'localfilename'}) );
            $rel = join('/',$file->{'localpath'}, $file->{'localfilename'});
          }
          else {
            #$rel = abs_path( join('/',$file->{'localpath'}, $file->{'localfilename'}) );
            $rel = $file->{'localfilename'};
          }
          if ($rel) {
            $abs = File::Spec->rel2abs($rel);
            $file->{'srcfile'} = $abs;
          }
        }
      }
      print "Done!\n"  if ($self->verbose() >= 5);

     if (scalar @files <= 0) { 
       print "\nAn empty filelist was provided -- nothing to do.\n";
       print "Exiting...\n\n";
       exit 0;
     }
      $newvalue = \@files;
    }
    else {
      croak("\nUnable to read filelist file: $file\n.");
    }

    # set locally first 
    $ClassData{'filelist'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::filelist")) {
      $self->SUPER::filelist($newvalue);
    }
  }
  return $ClassData{'filelist'};

}

sub setverboseFromArgs {
  my ($self,$OptsRef) = @_;
  my $newvalue;

  if (@_ > 1) {
    if (defined $OptsRef->{'verbose'}) {
      $newvalue = $ClassData{'verbose'} + $OptsRef->{'verbose'};
    }
    else {
      return $ClassData{'verbose'};
    }
    if (defined $OptsRef->{'quiet'}) {
      $newvalue = 0;
    }
    #if (defined $OptsRef->{'debug'}) {
    #  $newvalue = 5;
    #}
  }
  if (defined $newvalue) {
    $self->verbose($newvalue);
  }

  return $self->verbose();
}

#######################################################################
# Run-of-the-mill data accessors
#######################################################################
sub FileQueryKeys {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'FileQueryKeys'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::FileQueryKeys")) {
      $self->SUPER::FileQueryKeys($newvalue);
    }
  }
  return $ClassData{'FileQueryKeys'};
}

sub config {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'config'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::config")) {
      $self->SUPER::config($newvalue);
    }
  }
  return $ClassData{'config'};
}
sub LogDir {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'LogDir'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::LogDir")) {
      $self->SUPER::LogDir($newvalue);
    }
  }
  return $ClassData{'LogDir'};
}


sub RunKey {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'RunKey'} = $newvalue;
    
    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::RunKey")) {
      $self->SUPER::RunKey($newvalue);
    }
  } 
  return $ClassData{'RunKey'};
}   


sub CmdLineOpts {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'CmdLineOpts'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::CmdLineOpts")) {
      $self->SUPER::CmdLineOpts($newvalue);
    }
  }
  return $ClassData{'CmdLineOpts'};
}


sub Xfer {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'Xfer'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::Xfer")) {
      $self->SUPER::Xfer($newvalue);
    }
  }
  return $ClassData{'Xfer'};
}
  

sub DB {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'DB'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::DB")) {
      $self->SUPER::DB($newvalue);
    }
  }
  return $ClassData{'DB'};
}

sub verbose {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'verbose'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::verbose")) {
      $self->SUPER::verbose($newvalue);
    }
  }
  return $ClassData{'verbose'};
}

sub dafversion {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'VERSION'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::dafversion")) {
      $self->SUPER::dafversion($newvalue);
    }
  }
  return $ClassData{'VERSION'};
}


sub debug {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'debug'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::debug")) {
      $self->SUPER::debug($newvalue);
    }
  }
  return $ClassData{'debug'};
}

sub RunStatus {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'RunStatus'} = $newvalue;
  
    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::RunStatus")) {
      $self->SUPER::RunStatus($newvalue);
    }
  } 
  return $ClassData{'RunStatus'};
}   
 



sub common_option_defs {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'common_option_defs'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::common_option_defs")) {
      $self->SUPER::verbose($newvalue);
    }
  }
  return $ClassData{'common_option_defs'};
}

1;
