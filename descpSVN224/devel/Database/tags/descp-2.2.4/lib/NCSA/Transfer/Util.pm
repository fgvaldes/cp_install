#######################################################################
#  $Id: Util.pm 3030 2009-02-03 20:29:36Z dadams $
#
#  $Rev:: 3030                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-02-03 13:29:36 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package NCSA::Transfer::Util;

use strict;
use warnings;
use Carp;
use List::Util;
use POSIX;

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&scan_dir &getDepthDirs &get_file_sums &getMkdirCommands &getRmdirCommands &getDNSHosts &fork_process &prepareTransfers);
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}

#######################################################################
#######################################################################
sub scan_dir {
  my $args = shift;
  my ($start_dir, @names, $path);

  ###########################################################################
  # Args:
  # scan_dir: Absolute path of directory to be scanned.
  # start_dir: Same as scan_dir, but will remain static upon recursive calling.
  # filenames_ref: Array ref to store file names.
  # sizes_ref: Array ref to store file sizes.
  # dlist_ref: Array ref containing at least scan_dir.
  # file_exclude: regex of file names to exclude (optional).
  # dir_excludes: Array ref of regex of directory names to exclude (optional).
  # recursive: Define this key to enable recursive parsing (optional).
  #
  ###########################################################################


  ###########################################################################
  # Get names in directory listing and loop through them, recursively calling 
  # this routine if the name is a directory:
  ###########################################################################

  $start_dir = getcwd();
  chdir($args->{'scan_dir'}) or die "Unable to enter $args->{'scan_dir'}:$!\n";
  opendir(FH,".") or die "Unable to open $args->{'scan_dir'}:$!\n";
  @names = readdir(FH) or die "Unable to read $args->{'scan_dir'}:$!\n";
  closedir(FH);
  $path = getcwd;

  NAME:foreach my $name (@names) {
    next if ($name eq '.');  # Skip current dir.
    next if ($name eq '..'); # Skip "up one" dir.
    if (-d $name) {
      if ($args->{'dir_excludes'}) {
        foreach my $exclude (@{$args->{'dir_excludes'}}) {
          next NAME if ($name =~ /$exclude/);       # Skip dirs that match the dir_excludes regexes.
        }
      }
      my $dir = join('/',$path,$name);               # Keep absolute path.
      push (@{$args->{'dlist_ref'}},$dir);           # Store directory in list.
      if ($args->{'recursive'}) {                    # Go deeper.
        $args->{'scan_dir'} = $dir;
        scan_dir($args);
        next NAME;
      }
    }
    elsif (-f $name){
      if ($args->{'file_excludes'}) {
        foreach my $exclude (@{$args->{'file_excludes'}}) {
          next NAME if ($name=~ /$exclude/);        # Skip files that match the file_excludes regexes.
        }
      }
      my $fullname=join('/',$path,$name);            # Get absolute path/filename.
      my $fsize= -s $fullname;                       # Get file size in bytes.
      $fullname=~s/^$args->{'start_dir'}//;          # Keep only relative path part.
      $fullname=~s/^\///;                            # Ditch leading '/'.
      push (@{$args->{'filenames_ref'}},$fullname);  # Store "rel path/filename".
      push (@{$args->{'sizes_ref'}},$fsize);         # Store size.
    }
  }

  chdir ($start_dir) or die "Unable to enter $start_dir:$!\n" if $start_dir;
}

#####################################################################################################################################################
# SUBROUTINE: getDepthDirs
#####################################################################################################################################################
sub getDepthDirs {
  my $dlist = shift;

  # Deconstruct each provided directory and create a hash with the "depth" as the
  # top-level key, and each depth value pointing to a hash of directories that
  # exist at that level.  The hash key uniqueness will keep this list to a minimal size,
  # so this routine serve as a good filter for turning big lists of directories into
  # a minimal set of mkdir commands.
  # e.g) $depth_dirs{3} => {'/home/dadams/stuff'=> '', '/home/dadams/more_stuff'=> ''}
  my %depth_dirs;
  foreach my $dir (@$dlist) {
    if ($dir !~/^\//) {
      croak("The \"getDepthDirs\" sorting routine only operates on absolute paths.\nYou provided:\n$dir\n");
    }
    (my $blank, my @parts) = split('/',$dir);
    my $dguts = '';
    for(my $i=0; $i<=$#parts; $i++) {
      my $part = $parts[$i];
      $dguts = join('/',$dguts,$part);
      ${$depth_dirs{"$i"}}->{$dguts} = $dguts;  # Maybe I can stop setting the value equal to the key, need to check...
    }
  }

# Debug print statements:
#  foreach my $key (sort keys %depth_dirs) {
#    print "\n$key:\n";
#    my $val = $depth_dirs{"$key"};
#    while ((my $k, my $v) = each %$$val) { 
#      print "$k = $v\n";
#    }
#  }

  return \%depth_dirs;
}

#####################################################################################################################################################
# get_file_sums
#####################################################################################################################################################
sub get_file_sums {
  my $file=shift;

  my %file_sums;
  # Get file size.
  my $size = -s $file;
  # Get cksum.
  my $cksum_out = `cksum $file`;
  (my $cksum,my $cksize,my $ckfile) = split (' ',$cksum_out);
  # Get md5sum.
  my $md5sum_out = `md5sum $file`;
  (my $md5sum, my $md5file) = split (' ',$md5sum_out);

  $file_sums{'size'} = clean_string($size);
  $file_sums{'cksum'} = clean_string($cksum);
  $file_sums{'md5sum'} = clean_string($md5sum);

  return \%file_sums;

}

#################################################################################
# getMkdirCommands 
#
#   Spit out a list (in order of execution) of the minimal set of mkdir
#   commands from a list of arbitray directories that may have duplicates.
#################################################################################

sub getMkdirCommands {
  my $dlist = shift;

  my $cmd;

  my $DepthDirs = getDepthDirs($dlist);

  my @keys = sort {$a <=> $b} keys %$DepthDirs;
  for(my $i=0; $i<=$#keys; $i++) {
    foreach my $dir (keys %${$DepthDirs->{$keys[$i]}} ) {
      #print "$keys[$i]: $dir\n";
      if ($cmd) {
        $cmd = $cmd."mkdir $dir\n";
      }
      else {
        $cmd = "mkdir $dir\n";
      }
    }
  }
  return $cmd;
}

sub getRmdirCommands {
  my $dlist = shift;

  my $cmd;

  my $DepthDirs = getDepthDirs($dlist);

  my @keys = sort {$b <=> $a} keys %$DepthDirs;
  for(my $i=0; $i<=$#keys; $i++) {
    foreach my $dir (keys %${$DepthDirs->{$keys[$i]}} ) {
      #print "$keys[$i]: $dir\n";
      if ($cmd) {
        $cmd = $cmd."quote RMD $dir\n";
      }
      else {
        $cmd = "quote RMD $dir\n";
      }
    }
  }
  return $cmd;
}

sub getDNSHosts {
  my $alias = shift;

  if (! $alias) {
    croak("getDNSHosts: Must supply a hostname.\n");
  }
  my $cmd = 'host';
  $cmd = $cmd. " $alias";
  my @out = `$cmd` or croak("Unable to execute $cmd.\n");

  my @hosts; 
  foreach (@out) {
   if (/^.*\s*has\s*address\s*([\S]+)$/) {
     push(@hosts, $1);
   }
  }
  return \@hosts;
}

#################################################################################
#
#################################################################################
sub prepareTransfers {
  my $ArgsARef = shift;
  my $base_cmd_file = shift;
  my $base_out_file = shift;
  my $verbose = shift;

  my $ctr = 0;

  foreach my $ArgsRef (@$ArgsARef) {
    my ($srcFilesRef, $destFilesRef);
    if (exists $ArgsRef->{'srcfiles'}) {
      $srcFilesRef = $ArgsRef->{'srcfiles'};
    }
    else {
      croak("I need srcfiles.\n");
    }
    croak("I need srcfiles.\n") if ($#$srcFilesRef == -1);
    $destFilesRef = $ArgsRef->{'destfiles'};

    ##########################################
    # Bust up file list into multiple sets to 
    # enable concurrency
    ##########################################
    print "\nShuffle file list and break for concurrency if needed..." if ($verbose >= 5);
    my $nclients = 1;
    my @FileIndexLists;
    my @all;
    for(my $i=0;$i<=$#$srcFilesRef;$i++) {
      push(@all,$i);
    }
    # Shuffle index list:
    @all = List::Util::shuffle(@all);
    for(my $c=0;$c<$nclients; $c++) {
      for(my $i=0;$i<=$#all;$i++) {
        push(@{$FileIndexLists[$c]}, $all[$i]);  # FIX THIS!!!!
      }
    }
    $ArgsRef->{'file_index_lists'} = \@FileIndexLists;
    print "Done!\n" if ($verbose >= 5);

    ##########################################
    # Create process related filenames.
    ##########################################
    for(my $ln=0;$ln<=$#FileIndexLists;$ln++) {
      my $list = $FileIndexLists[$ln];
      $ctr++; # why not $ln+1 ??
      my $jobnum = sprintf("%0*d", 2, $ctr);
      $ArgsRef->{'cmd_file'}  = join('_',$base_cmd_file,$jobnum);
      $ArgsRef->{'out_file'}  = $base_out_file.'_'.$jobnum.'.out';
      $ArgsRef->{'err_file'}  = $base_out_file.'_'.$jobnum.'.err';
      if ($ArgsRef->{'log_prefix'}) {
        $ArgsRef->{'cmd_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'cmd_file'});
        $ArgsRef->{'out_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'out_file'});
        $ArgsRef->{'err_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'err_file'});
      }
    }
  }

}

#################################################################################
#
#################################################################################
sub breakList {
  my $Args = shift;

  my (@List,$N,$shuffle, @BrokenLists);
  
  # Process Arguments:
  if ($Args->{'N'}) {
    $N = $Args->{'N'};
    croak("The number of list must be greater than 0.\n") if ($N <= 0) ;
  }
  else {
    croak("Must Provide number of lists to create.\n");
  }
  if ($Args->{'Shuffle'} || $Args->{'shuffle'}) {
    $shuffle = 1;
  }
  if ($Args->{'List'}) {
    croak("List must be and array reference.\n") if (ref $Args->{'List'} ne 'ARRAY');
    if ($shuffle) {
      @List = List::Util::shuffle(@{$Args->{'List'}});
    }
    else {
      @List = @{$Args->{'List'}};
    }
  }
  else {
    croak("Must provide a list to break up.\n");
  }

  # Adjust N if the list is too short;
  my $NList = $#List + 1;
  if ($NList <= $N) {
    $N = $NList;
  }
  
  my $ListLen = 0;
  if ($N > 0) {
    $ListLen = POSIX::floor( $NList/$N );
  }
  my $Left = 0;
  if ($N > 0) {
    $Left = ($NList)%($N);
  }

  # Create each list:
  for(my $i=0;$i<$N;$i++) {
    for (my $j=0;$j<$ListLen;$j++) {
      my $fileindex = $j + $i*$ListLen;
      push(@{$BrokenLists[$i]},$List[$fileindex]);
    }
  }
  # Spread remaining entries evenly:
  if ($Left > 0) {
    for(my $i=1; $i<=$Left; $i++) {
      my $fileindex = $ListLen*$N + $i - 1;
      my $l = $i % $N;
      push(@{$BrokenLists[$l]},$List[$fileindex]);
    }
  }

  return \@BrokenLists;
}

#################################################################################
#
#                                fork_process
#
#################################################################################
sub fork_process {
  my $cmd = shift;
  if (!defined(my $kidpid = fork())) {
    # fork returned undef, so failed
    die "cannot fork: $!";
  } 
  elsif ($kidpid == 0) {
    # fork returned 0, so this branch is the child
    exec("$cmd");
    # if the exec fails, fall through to the next statement
    die "can't exec date: $!";
  } 
  else {
    # fork returned neither 0 nor undef,
    # so this branch is the parent
#      print "Child process id: $kidpid\n";
    return $kidpid;
  }
}


1;
