#######################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package NCSA::String::Util;

use strict;
use warnings;

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&clean_string &no_space &make_human &glob2regex &commify);
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}

################################################################################
#
#                                clean_string
#
#################################################################################
sub clean_string {
  my $str=$_[0];

  if ($str) {
    chomp($str);               # Remove trailing newline.
    $str =~ s/^\s*//g;         # Remove leading whitespace.
    $str =~ s/\s*$//g;         # Remove trailing whitespace.
  }

  return $str;
}

################################################################################
# no_space
#   INPUT:
#     * String.
#   OUTPUT:
#     * String
#   PURPOSE:
#     Replace any whitespace within a string with underscores.
################################################################################
sub no_space {
  my $str = shift;
  if ($str) {
    $str =~ s/\s/_/g;
  }
  return $str;
}

#####################################################################################################################################################
#
# make_human
#  PURPOSE:
#    Provide unit appendage to binary bandwidth in bytes.
#
#####################################################################################################################################################
sub make_human {
  my $val = shift;
  my $sys = shift if ($#_ >= 1);
  my (@digits,$hr_val,$hr_suf);

  $val = clean_string($val);
  # Check for decimal.
  my $frac = '';
  my $int = '';
  if ($val =~/\./) {
#    print "$val has a decimal\n";
    ($int,$frac) = split(/\./,$val,2);
    $int = clean_string($int);
    $frac = clean_string($frac);
  }
#  print "$int\n";
 # print "$frac\n" if $frac eq '';

  if ($int eq '') {
    @digits = split (//,$val);
  }
  else {
    @digits = split (//,$int);
#    print "$int\n";
  }
#  foreach (@digits) {
#    $_ = clean_string($_);
#    print "$_\n";
#  }
#print "n_digits: $#digits\n";
  if ($#digits <= 5) {
   $hr_val = $val/1024;
   $hr_suf = 'KiB';
  }
  if ($#digits > 5 ) {
    if ($#digits <= 8) {
      $hr_val = $val/(1024*1024);
      $hr_suf = 'MiB';
    }
  }
  if ($#digits > 8 ) {
    if ($#digits <= 11) {
      $hr_val = $val/(1024*1024*1024);
      $hr_suf = 'GiB';
    }
  }
  if ($#digits > 11 ) {
#    if ($#digits <= 14) {
      $hr_val = $val/(1024*1024*1024*1024);
      $hr_suf = 'TiB';
#    }
  }

return ($hr_val,$hr_suf);

}

#################################################################################
#
#
#
#################################################################################
sub glob2regex {
    my $str = shift;
    my %patmap = (
        '*' => '.*',
        '?' => '.',
        '[' => '[',
        ']' => ']',
    );
    $str =~ s{(.)} { $patmap{$1} || "\Q$1" }ge;
    return '^' . $str . '$';
}


#################################################################################
#   
#   
#   
#################################################################################
sub commify {
    local($_) = shift;
    1 while s/^(-?\d+)(\d{3})/$1,$2/;
    return $_;
}


1;
