########################################################################
#  $Id: Tools.pm 1077 2008-05-27 20:57:01Z dadams $
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Authors: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package NCSA::Transfer;
use strict;
use warnings;
use Carp;
use Data::Dumper;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use NCSA::Transfer::Util qw(:all);
use Exception::Class('bail');
use NCSA::Transfer::Exceptions;
use base qw(NCSA::Transfer::Uberftp NCSA::Transfer::guc);

my %ClassData;

sub new {
  my $this = shift;
  my $Args = shift if (@_);
  $Args = {} if (! defined $Args);
  my $class = ref($this) || $this;
  my $self = {};
  bless $self, $class;
  $self->verbose($Args->{'verbose'}) if ($Args->{'verbose'});
  $self->LogDir($Args->{'LogDir'}) if ($Args->{'LogDir'});
#  $self->_init($Args);
  return $self;
}

#sub _init {
#  my $self = shift;
#  my $Args = shift if (@_);
#  $Args = {} if (! defined $Args);
#  my $PackageArgs;
#  my $PACKAGE = __PACKAGE__;
#  if ($Args->{$PACKAGE}) {
#    $PackageArgs = $Args->{$PACKAGE};
#  }
#  else {
#    $PackageArgs = $Args;
#  }
#  $self->verbose($PackageArgs->{'verbose'}) if ($PackageArgs->{'verbose'});
#  $self->LogDir($PackageArgs->{'LogDir'}) if ($PackageArgs->{'LogDir'});
#  return $self;
#}

sub LogDir {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'LogDir'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::LogDir")) {
      $self->SUPER::LogDir($newvalue);
    }
  }
  return $ClassData{'LogDir'};
}


#sub verbose {
#  my($self, $newvalue) = @_;
#  if (@_ > 1) {
#    # set locally first
#    $ClassData{'verbose'} = $newvalue;
#
#    # then pass the buck up to the first 
#    # overridden version, if there is one
#    if ($self->can("SUPER::verbose")) {
#      $self->SUPER::verbose($newvalue);
#    }
#  }
#  return $ClassData{'verbose'};
#}

sub debug {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'debug'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::debug")) {
      $self->SUPER::debug($newvalue);
    }
  }
  return $ClassData{'debug'};
}

sub try {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first 
    $ClassData{'try'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::try")) {
      $self->SUPER::try($newvalue);
    }
  }
  return $ClassData{'try'};
}

sub MaxClients {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'MaxClients'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::MaxClients")) {
      $self->SUPER::MaxClients($newvalue);
    }
  }
  return $ClassData{'MaxClients'};
}


#######################################################################
#######################################################################
sub verifyFileList {
  my $this = shift;
  my $Args = shift;

  my ($SiteData, $Method, $FileList, $e);

  # Grab main arguments:
  if ($Args->{'SiteData'}) {
    $SiteData = $Args->{'SiteData'};
  }
  else {
    bail->throw("Must provide site information.\n");
  }
  if ($Args->{'list_method'}) {
    $Method = $Args->{'list_method'};
  }
  else {
    $Method = 'MLST';
  }
  if ($Args->{'FileList'}) {
    $FileList = $Args->{'FileList'}
  }
  else {
    bail->throw("Must have a file list to operate on\n");
  }

  if ($Method eq 'MLST' || $Method eq 'LIST') {
    # Need to use uberftp and gridftp for these methods:
    bail->throw("No gridftp host information provided") if (! $SiteData->{'gridftp_host'});

    my %UberArgs;
    $UberArgs{'target_server'} = $SiteData->{'gridftp_host'};
    $UberArgs{'options'}->{'-P'} = $SiteData->{'gridftp_port'} if ($SiteData->{'gridftp_port'});
    #$UberArgs{'log_prefix'} = $Args->{'LogPrefix'} if ($Args->{'LogPrefix'});
    $UberArgs{'log_prefix'} = $this->LogDir() if ($this->LogDir());
    $UberArgs{'BaseFileName'} = $Args->{'BaseFileName'} if ($Args->{'BaseFileName'});

    # Call verification routine, rethrow all exceptions:
    eval {
      #NCSA::Transfer::Uberftp::verifyRemoteFiles(
      $this->verifyRemoteFiles(
        {
          'list_method' => $Method,
          'files' => $FileList
        },
        \%UberArgs
      );
    };
    if ($e = Exception::Class->caught() ) {
      if ($e = NCSA::Transfer::Exceptions::runUber->caught('runUber')) {
        warn "Caught a fatal globus connection error when attempting to run uberftp:\n";
        warn "-------------- snip -------------\n";
        foreach(@{$e->err_lines}) {
          warn $_;
        }
        warn "-------------- snip -------------\n";
        ref $e ? $e->rethrow : die $e;
      }
      else {
        ref $e ? $e->rethrow : die $e;
      }
    }
  }

  else {
    bail->throw("Unknown verification method: $Method\n");
  }
  
}


#######################################################################
#######################################################################
sub transferFileList {
  my $this = shift;
  my $Args = shift;

  my ($Transfers);

  if ($Args->{'Transfers'}) {
    $Transfers = $Args->{'Transfers'};
  }
  else {
    bail->throw("Must provide a transfer argument.\n");
  }

  foreach my $Xfer (@$Transfers) {
#    print "hi\n";
#    print Dumper ($Xfer);
    my ($Method, %ClientArgs, $SrcSiteData, $DestSiteData);
    my (@SrcHosts, @DestHosts, $nclients,$nsrchosts,$ndesthosts);

    if ($Xfer->{'TransferMethod'}) {
      $Method = $Xfer->{'TransferMethod'};
    }
    else {
      $Method = 'guc';
    }
    if ($Method ne 'guc' && $Method ne 'uberftp') {
      warn "Unsupported transfer method: $Method\nSkipping this transfer...\n";
      next;
    }
    if ($Xfer->{'SrcSiteData'}) {
      $SrcSiteData = $Xfer->{'SrcSiteData'};
    }
    else {
      warn "No source site data provided, skipping this transfer...\n";
      next;
    }
    if ($Xfer->{'DestSiteData'}) {
      $DestSiteData = $Xfer->{'DestSiteData'};
    }
    else {
      warn "No destination site data provided, skipping this transfer...\n";
      next;
    }

    ###################################################################
    ###################################################################



    # Transform transfer settings for requested client and
    # set other defualt settings particular to that client:
    my ($tcpbuf,$pstreams,$nodcau,$MSS,$MSS_Family, $MSS_Wait, $ENSTORE);
    $tcpbuf = $Xfer->{'TransferSettings'}->{'tcpbuf'} if $Xfer->{'TransferSettings'}->{'tcpbuf'};
    $pstreams = $Xfer->{'TransferSettings'}->{'parallel_streams'} if $Xfer->{'TransferSettings'}->{'parallel_streams'};
    $nodcau = $Xfer->{'TransferSettings'}->{'nodcau'};

    $MSS = 1 if (exists $Xfer->{'TransferSettings'}->{'MSS'});
    $MSS_Wait = 1 if (exists $Xfer->{'TransferSettings'}->{'MSS_Wait'});
    $MSS_Family = $Xfer->{'TransferSettings'}->{'MSS_Family'} if (exists $Xfer->{'TransferSettings'}->{'MSS_Family'});
    $ENSTORE = 1 if (exists $Xfer->{'TransferSettings'}->{'ENSTORE'});


    if ($Method eq 'guc') {
      # Default guc options:
      $ClientArgs{'options'}->{'-fast'} = '';
      $ClientArgs{'options'}->{'-rst'} = '';
      $ClientArgs{'options'}->{'-v'} = '';
      $ClientArgs{'options'}->{'-dbg'} = '';
      
#      $ClientArgs{'options'}->{'-g2'} = '';
#      $ClientArgs{'options'}->{'-pp'} = '';
      if ($tcpbuf) {
        $ClientArgs{'options'}->{'-tcp-bs'} = $tcpbuf;
      }
      if ($pstreams && ! $MSS) {
        $ClientArgs{'options'}->{'-p'} = $pstreams;
      }
      if ($MSS) {
        $ClientArgs{'options'}->{'-nodcau'} = '';
      }
      if ($ENSTORE) {
        $ClientArgs{'options'}->{'-nodcau'} = '';
        delete $ClientArgs{'options'}->{'-fast'} if exists($ClientArgs{'options'}->{'-fast'});
        delete $ClientArgs{'options'}->{'-p'} if exists($ClientArgs{'options'}->{'-p'});
        delete $ClientArgs{'options'}->{'-pp'} if exists($ClientArgs{'options'}->{'-pp'});
        delete $ClientArgs{'options'}->{'-g2'} if exists($ClientArgs{'options'}->{'-g2'});
      }
      if ($nodcau) {
        $ClientArgs{'options'}->{'-nodcau'} = '';
      }
    
    }
    elsif ($Method eq 'uberftp') {
      if ($tcpbuf) {
        $ClientArgs{'options'}->{'tcpbuf'} = $tcpbuf;
      }
      if ($pstreams && ! $MSS) {
        $ClientArgs{'options'}->{'parallel'} = $pstreams;
      }
      if ($MSS) {
        $ClientArgs{'options'}->{'active'} = '';
        $ClientArgs{'options'}->{'mode'} = 'stream';
        # This is bad.  The "quote" key can only be used once!
        if ($MSS_Family) {
          $ClientArgs{'options'}->{'quote'} = "setfam $MSS_Family";
        }
        if ($MSS_Wait) {
          $ClientArgs{'options'}->{'quote'} = 'wait';
        }
      }

    }

     
    # Dig for a list of hosts behind the alias (unless local):
    if (! $SrcSiteData->{'islocal'}) {
      #print "Digging hostnames from $src->{'gridftp_host'}\n";
      @SrcHosts = @{NCSA::Transfer::Util::getDNSHosts($SrcSiteData->{'gridftp_host'}) };
    }
    @DestHosts = @{NCSA::Transfer::Util::getDNSHosts($DestSiteData->{'gridftp_host'}) };

    # Decide how to break lists up among src/dest hosts
    if ($#SrcHosts >= $#DestHosts) {
      $nclients = $#SrcHosts;
    }
    else {
      $nclients = $#DestHosts;
    }
    if (@SrcHosts) {
      $nsrchosts = $#SrcHosts +1;
    }
    else {
      $nsrchosts = 1;
    }

    $ndesthosts = $#DestHosts +1;

    if ($this->MaxClients()) {
      if ($this->MaxClients() > 0) {
        $nclients = $this->MaxClients() -1
      }
      else {
        $nclients = 0;
      }

      while ($ndesthosts + $nsrchosts -1> $this->MaxClients()) {
        if ($nsrchosts > $ndesthosts) {
          $nsrchosts = $nsrchosts - 1;
        }
        else {
          $ndesthosts = $ndesthosts -1;
        }
      }
    }
    

#    # Turn off auto multi-client for now:
#    $nclients = 0;
#    $nsrchosts = 1;
#    $ndesthosts = 1;




    my @FileListIndexes;
    for(my $i=0;$i<=$#{$SrcSiteData->{'AbsFileList'}};$i++) {
      $FileListIndexes[$i] = $i;
    }

    my @ClientFileLists = @{NCSA::Transfer::Util::breakList(
        {
          'List' => \@FileListIndexes,
          'N' => $nclients + 1,
          'shuffle' => 1
        }
      )
    };

    $nclients = $#ClientFileLists;
    print "Clients: ",scalar @ClientFileLists,"\n";

    # Loop over clients:
    my @XferSet;
    for (my $i=0; $i<=$nclients;$i++) {
       my $si = $i % $nsrchosts;
       my $di = $i % $ndesthosts;

       my %ThisClientArgs = %ClientArgs;

       # Set hostnames for this client:
       if (! $DestSiteData->{'islocal'}) {
        if ($nclients <= 0) {
          $ThisClientArgs{'target_server'} = $DestSiteData->{'gridftp_host'};
        }
        else {
          $ThisClientArgs{'target_server'} = $DestHosts[$di];
        }
      }
      if (! $SrcSiteData->{'islocal'}) {
        if ($nclients <= 0) {
          $ThisClientArgs{'src_server'} = $SrcSiteData->{'gridftp_host'}
        }
        else {
          $ThisClientArgs{'src_server'} = $SrcHosts[$si];
        }
      }

       # Create file lists for this client:
       my $list = $ClientFileLists[$i];

      #$ThisClientArgs{'log_prefix'} = $Args->{'LogPrefix'} if $Args->{'LogPrefix'};
      #$ThisClientArgs{'LogPrefix'} = $Args->{'LogPrefix'} if $Args->{'LogPrefix'};
      $ThisClientArgs{'log_prefix'} = $this->LogDir() if ($this->LogDir());
      $ThisClientArgs{'LogPrefix'} = $this->LogDir() if ($this->LogDir());
      $ThisClientArgs{'MSS_Stage'} = '' if $Args->{'MSS_Stage'};

       foreach my $fi (@$list) {
         push(@{$ThisClientArgs{'destfiles'}},$DestSiteData->{'AbsFileList'}->[$fi]);
         push(@{$ThisClientArgs{'srcfiles'}},$SrcSiteData->{'AbsFileList'}->[$fi]);
       }
       
       
       # Put it all in the transfer argument array:
       push(@XferSet, \%ThisClientArgs)
    }

    # Make directories at destination:
    my @DirectoryList;
    foreach my $dir (@{$DestSiteData->{'AbsFileList'}}) {
      $dir =~ s/\/[^\/]+$//; # Remove filename
      push(@DirectoryList,$dir);
    }
    # Create remote directories:
    print "\nCreating remote directories...";
    #NCSA::Transfer::Uberftp::createRemoteDirs(\@DirectoryList,$XferSet[0]);
    $this->createRemoteDirs(\@DirectoryList,$XferSet[0]);
    print "Done!\n";
    if (exists $Xfer->{'TransferSettings'}->{'MSS_Stage'}) {
      print "\nIssuing stage requests on mss...\n";
      $this->stageUber({'files' => $SrcSiteData->{'AbsFileList'}},{'target_server' => $SrcSiteData->{'gridftp_host'}});
    }

    
    my %TransferArgs = (
      'xferArgs' => \@XferSet,
    );
    $TransferArgs{'log_prefix'} = $this->LogDir() if ($this->LogDir());
    $TransferArgs{'LogPrefix'} = $this->LogDir() if ($this->LogDir());
    $TransferArgs{'try'} = $this->try() if ($this->try());
    $TransferArgs{'debug'} = $this->debug() if ($this->debug());
    $TransferArgs{'verbose'} = $this->verbose() if ($this->verbose());
    print "\nStarting Transfer...\n";
    # Execute transfer calls:
    if ($Method eq 'uberftp') {
      #NCSA::Transfer::Uberftp::transferUber(\%TransferArgs);
      $this->transferUber(\%TransferArgs);

    }
    elsif ($Method eq 'guc') {
      #NCSA::Transfer::guc::transferguc(\%TransferArgs);
      $this->transferguc(\%TransferArgs);
    }


  }

  # Now reap transfers and parse output:
  
}



#######################################################################
#######################################################################
sub removeFileList {
  my $this = shift;
  my $Args = shift;

  my ($SiteData, $Method, $FileList,$e);

  # Grab main arguments:
  if ($Args->{'SiteData'}) {
    $SiteData = $Args->{'SiteData'};
  }
  else {
    bail->throw("Must provide site information.\n");
  }
  if ($Args->{'Method'}) {
    $Method = $Args->{'Method'};
  }
  else {
    $Method = 'uberftp';
  }
  if ($Args->{'FileList'}) {
    $FileList = $Args->{'FileList'}
  }
  else {
    bail->throw("Must have a file list to operate on\n");
  }

  if ($Method eq 'uberftp') {
    # Need to use uberftp and gridftp for this method:
    bail->throw("No gridftp host information provided") if (! $SiteData->{'gridftp_host'});
    my %UberArgs;
    $UberArgs{'target_server'} = $SiteData->{'gridftp_host'};
    $UberArgs{'options'}->{'-P'} = $SiteData->{'gridftp_port'} if ($SiteData->{'gridftp_port'});
    $UberArgs{'log_prefix'} = $this->LogDir() if ($this->LogDir());
   
    eval {
      $this->removeRemoteFiles(
        {
          'files' => $FileList
        },
        \%UberArgs
      ); 
    };
    if ($e = Exception::Class->caught() ) {
      if ($e = NCSA::Transfer::Exceptions::runUber->caught('runUber')) {
        warn "Caught a fatal globus connection error when attempting to run uberftp:\n";
        warn "-------------- snip -------------\n";
        foreach(@{$e->err_lines}) {
          warn $_;
        }
        warn "-------------- snip -------------\n";
        ref $e ? $e->rethrow : die $e;
      }
      else {
        ref $e ? $e->rethrow : die $e;
      }
    }

  # Re-initialize uber args--pretty brain dead...
  %UberArgs =();
  $UberArgs{'target_server'} = $SiteData->{'gridftp_host'};
  $UberArgs{'options'}->{'-P'} = $SiteData->{'gridftp_port'} if ($SiteData->{'gridftp_port'});
  $UberArgs{'log_prefix'} = $this->LogDir() if ($this->LogDir());

  # Make List od directories at destination:
  my @DirectoryList;
  foreach my $dir (keys %$FileList) {
    $dir =~ s/\/[^\/]+$//; # Remove filename
    push(@DirectoryList,$dir);
  }
  print "\nRemoving empty directories...\n";
   $this->removeRemoteDirs(
     \@DirectoryList,\%UberArgs
   ); 


  }
  else {
    bail->throw ("Unknown remove method: $Method\n");
  }


}

1;
