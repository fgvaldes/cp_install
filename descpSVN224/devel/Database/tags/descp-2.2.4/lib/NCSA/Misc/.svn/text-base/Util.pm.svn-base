#######################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package NCSA::Misc::Util;

use strict;
use warnings;
use Carp;

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&getRandomKey);
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}

#################################################################################
#
#                             getRandomInt
#
#################################################################################
sub getRandomInt {
   my($min, $max) = @_;

   # Make sure arguments are integers:
   if (defined $min && defined $max) {
     if ($min !~ m/^[+\-]?\d+\.?0*$/ || $max !~m/^[+\-]?\d+\.?0*$/) {
       croak('getRandomInt: Must provide integer arguments ($min, $max).\n');
     }
     return $min if $min == $max;
     ($min, $max) = ($max, $min)  if  $min > $max;
     return $min + int rand(1 + $max - $min);
  }
  else {
    croak('getRandomInt: Must provide integer arguments ($min, $max).\n');
  }
}

#################################################################################
#
#                               getRandomKey
#
#################################################################################
sub getRandomKey {
  my $key_len=shift;

  my $key='';
  if (! $key_len) {
    $key_len = 4;  # Default key size.
  }
  for (my $j=0;$j<$key_len;$j++) {   # Generate a unique key to stamp temp files with
    my $digit=getRandomInt(0,9);
    $key = join('',$key,$digit);
  }
  return $key;
}


1;
