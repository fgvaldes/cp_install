#!/usr/bin/perl

#
# post_ingest.pl
#
# DESCRIPTION:
#
# AUTHOR:  
# Michelle Gower (mgower@ncsa.uiuc.edu)
#
# $Rev: 2322 $
# $LastChangedBy: tdarnell $
# $LastChangedDate: 2008-11-06 12:27:42 -0600 (Thu, 06 Nov 2008) $
#
#
# Ingest the input list and output log of a previous ingest.
#
# <rundir>/aux/<block>_ingest.list
# <rundir>/log/<block>_ingest_ingest.log
#

use strict;
use warnings;

use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use File::Basename;
use Getopt::Long;

my ($block, $rundir, $archivenode) = undef;
my $verbose = 1;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'block=s'  => \$block,
    'rundir=s' => \$rundir,
    'archivenode=s' => \$archivenode,
    'verbose=s' => \$verbose,
);

if (!defined($block)) {
    print STDERR "Must define block\n"; 
    exit 1;
}
if (!defined($rundir)) {
    print STDERR "Must define rundir\n"; 
    exit 1;
}
if (!defined($archivenode)) {
    print STDERR "Must define archivenode\n"; 
    exit 1;
}

chdir "/tmp";

my ($out, $cmd, $stat); 
my $ingestfile = "/tmp/post_ingest_$$.list";
my $bindir = $FindBin::RealBin."/../bin";

# ingest runtime dir
my @dirs = <$rundir/runtime/${block}_ingest>;
if (scalar(@dirs) == 0) {
    print "No runtime directories for block ${block}\n";
}
else {
    foreach my $d (@dirs) {
        if (-d "$d") {
            print "========================================\n";
            $cmd = "$bindir/find_ingest.pl -ingestfile $ingestfile -verbose $verbose -archivenode $archivenode -cleanup $d";
            print "$cmd\n";
            $out = `$cmd 2>&1`;
            $stat = $?;
            print "$out\n\n";
            print "find_ingest.pl exit code: $stat\n\n\n";
            if (-r $ingestfile) {
                print "Ingest list with file ids:\n";
                open FH, "< $ingestfile";
                while (<FH>) {
                    print $_;
                }
                close FH;
                unlink $ingestfile;
            }
            else {
                print "Ingest list missing ($ingestfile)\n";
            }
        }
    }
    print "========================================\n\n";
}



# ingest main input list and output log
my $aux = $rundir .'/aux/'.$block."_ingest.list";
my $log = $rundir .'/log/'.$block."_ingest_ingest.log";
if (! -r "$aux") {
    print STDERR "Could not find ingest list for block $block\n"; 
    exit 1;
}
if (! -r "$log") {
    print STDERR "Could not find ingest log for block $block\n"; 
    exit 1;
}

open FH, "> $ingestfile" or die "Could not open $ingestfile for writing\n";

my ($name,$path,$suffix);

($name,$path,$suffix) = fileparse($aux);
$path =~ s/\/$//;
print FH "<file>\n";
print FH "\tfileid=0\n";
print FH "\tlocalpath=$path\n";
print FH "\tlocalfilename=$name\n";
print FH "</file>\n";

($name,$path,$suffix) = fileparse($log);
$path =~ s/\/$//;
print FH "<file>\n";
print FH "\tfileid=0\n";
print FH "\tlocalpath=$path\n";
print FH "\tlocalfilename=$name\n";
print FH "</file>\n";

close FH;

print "Input file for file_ingest\n";
open FH, "< $ingestfile";
while (<FH>) {
    print $_;
}
close FH;

$cmd = "$bindir/file_ingest.pl -filelist $ingestfile -verbose $verbose -archivenode $archivenode";
print "$cmd\n";
$out = `$cmd 2>&1`;
$stat = $?;
print "$out\n\n";
print "file_ingest.pl exit code: $stat\n\n\n";
print "Modified input file with file ids:\n";
open FH, "< $ingestfile";
while (<FH>) {
    print $_;
}
close FH;
unlink $ingestfile;
