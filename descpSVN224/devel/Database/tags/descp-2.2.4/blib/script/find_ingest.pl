#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell

use strict;
use warnings;

use File::Path;
use File::Basename;
use Getopt::Long;
use Cwd;

use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::EventUtils;

my ($newer, $ingestfile, $archivenode, $noingest, $path, $pastrun, $cleanup) = undef;
my @excludepat;
my $verbose = 1;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if ($verbose > 1) {
    print "find_ingest.pl @ARGV\n";
    print "find_ingest started at ", scalar localtime(time), "\n";
}
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'newer=s'  => \$newer,
    'exclude=s'  => \@excludepat,
    'ingestfile=s' => \$ingestfile,
    'pastrun=s' => \$pastrun,
    'archivenode=s' => \$archivenode,
    'noingest' => \$noingest,
    'verbose=s' => \$verbose,
    'cleanup' => \$cleanup,
);
@excludepat = split(/,/,join(',',@excludepat));

if (scalar(@ARGV) == 0) {
    reportEvent($verbose, "STATUS", 5, "Error: must specify starting path");
    exit 1;
}
elsif (scalar(@ARGV) > 1) {
    reportEvent($verbose, "STATUS", 5, "Error: unknown arguments");
    exit 1;
}
else {
    $path = $ARGV[0];
    if (! -d $path) {
        reportEvent($verbose, "STATUS", 5, "Error: invalid path ($path)");
        exit 1;
    }
}

my $newerstr = "";
if (defined($newer)) {
    if (! -r $newer) {
        reportEvent($verbose, "STATUS", 5, "Error: could not read file to be used with find's newer flag ($newer)");
        exit 1;
    }
    $newerstr = "-newer $newer ";
}


# Make a list of files for file_ingest removing empty files if requested
my ($findcmd, $findout, $findstat);
my $cnt = 0;

my $start = time;

#$findcmd = "find $path $newerstr -type f -exec ls --time-style=long-iso -l {} \\;";
$findcmd = "find $path $newerstr -type f -print | xargs ls --time-style=long-iso -l" ;
print "$findcmd\n";
$findout = `$findcmd 2>&1`;
$findstat = $?;
my $out2 = $findout;
$out2 =~ s/\n/\n\t\t/g;
print "\t\t$out2\n";
print "Find command took ", time - $start, " seconds\n";

$start = time;
if ($findstat == 0) {
    my $FH;
    if (defined($ingestfile)) {
        my ($filename, $path) = fileparse($ingestfile);
        if (($path =~ /\S/) && (! -d $path)) {
            eval { mkpath($path) };
            if ($@) {
                reportEvent($verbose, "STATUS", 5, "Error\nCouldn’t create $path: $@");
                exit 1;
            }
        }
        my $opensuccess = open $FH, "> $ingestfile";
        if (!$opensuccess) {
            reportEvent($verbose, "STATUS", 5, "Error: Could not open ingestfile $ingestfile");
            exit 1;
        }
    }
    else {
        $FH = *STDOUT;
    }

    my @files = split /\n/, $findout;
    foreach my $f (@files) {
        $f =~ s/^\s+//;
        $f =~ s/\s+$//;
        my @finfo = split /\s+/, $f;
        if (scalar(@finfo) < 8) {
            reportEvent($verbose, "STATUS", 5, "Error: missing information from listing output\n\t$f");
            exit 1;
        }
#        print "size = ", $finfo[4], " name = ", $finfo[8], "\n"; 
        my $fsize = $finfo[4];
        my $fname = $finfo[7];
        my $excl = 0;
        if (scalar(@excludepat) > 0) {
            my $i = 0;
            while (!$excl && ($i < scalar(@excludepat))) {
                if ($fname =~ /$excludepat[$i]/) {
                    $excl = 1;
                }
                $i++;
            }
        }
        if (!$excl) {
            if ($fsize == 0) {
                if (($fname !~ /runtime/) && ($fname !~ /ingest.list.wcs.txt/)) {
                    reportEvent($verbose, "STATUS", 4, "Warning: a non-runtime file ($fname) has 0 bytes.  Skipping ingestion.");
                }
                if (defined ($cleanup)) {
                    print "Deleting empty file $fname\n";
                    unlink $fname;
                }
            }
            elsif (($fname =~ /runtime/) && ($fname =~ /org.eclipse/)) {
                print "Deleting org.eclipse file $fname\n";
                unlink $fname;
            }
            else {
                my ($name,$path,$suffix) = fileparse($fname);
                $path =~ s/\/$//;
                print $FH "<file>\n";
                print $FH "\tfileid=0\n";
                print $FH "\tlocalpath=$path\n";
                print $FH "\tlocalfilename=$name\n";
                print $FH "</file>\n";
                $cnt++;
            }
        }
    }

    if (defined($ingestfile)) {
        close $FH;
    }

    if ($cnt == 0) {
        reportEvent($verbose, "STATUS", 5, "0 files to ingest");
        exit 1;
    }
    else {
        reportEvent($verbose, "STATUS", 1, "Created ingest list containing $cnt files");
    }

    if (defined($cleanup)) {
        # remove empty directories
        print "\n\nRemoving empty subdirectories of $path\n";
        $findcmd = "find $path -depth -type d -empty -exec ls -ld {} \\; -exec rmdir {} \\;";
        print "$findcmd\n";
        $findout = `$findcmd 2>&1`;
        $findstat = $?;
        print "$findout\n";
    	print "Creating ingest list and doing cleanup took  ", time - $start, " seconds\n";
    }
    else {
    	print "Creating ingest list took  ", time - $start, " seconds\n";
    }



    if (!defined($noingest)) {
        my $bindir = $FindBin::RealBin;
        my $archivenodestr = "";
        if (defined($archivenode)) {
            $archivenodestr = "-archivenode $archivenode ";
        }

        my $cmd = "$bindir/file_ingest.pl -filelist $ingestfile -verbose $verbose ";
        if (defined($pastrun)) {
            $cmd .= " -pastrun $pastrun ";
        }
        $cmd .= " $archivenodestr ";
        
        print "\n\n$cmd\n";
        exec("$cmd");
    }
}
else {
    reportEvent($verbose, "STATUS", 5, "\nfind exited with non-zero status ($findstat)\n$findout\n");
    exit 1;
}

exit 0;
