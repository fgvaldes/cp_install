#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# fstest.pl
#
# DESCRIPTION:
#  This script runs a file system check by opening and reading
#  the headers of many files
#
# AUTHOR:  
# Tony Darnell (tdarnell@illinois.edu)
#
# $Rev: 4975 $
# $LastChangedBy: tdarnell $
# $LastChangedDate: 2010-01-25 09:43:00 -0700 (Mon, 25 Jan 2010) $
#

use strict;
use Astro::FITS::CFITSIO qw( :constants );
use Astro::FITS::CFITSIO qw( :longnames );
use Benchmark;
use Cwd;
use Data::Dumper;
use FindBin qw($Bin);
use File::Basename;
use File::stat;
use Getopt::Long;
use Time::localtime;

$| = 1;

my $VERSION = '$Rev: 4975 $';

my ($fileList, $archiveNode, $pastRuns) = undef;

#
# Main Loop
#
my $tCount = 0;
my $updateHashRef;
my $insertHashRef;
my $zeropointHashRef;
my ($eventStr,$exitStatus,$verboseLevel);

my $now = `date`;
chomp($now);
print "Starting test at: $now\n";

print "Getting filelist....";
#my $dir = "/data1/Archive/DES/red/20091108205752_20091010/red";
#my @files = glob("$dir/*/*");
my $dir = "/data1/Archive/DES/red/20091124190000_20091008/shapelet";
my @files = glob("$dir/*");
print "\n";

print "Running test...";

my $t0 = new Benchmark;
my $count = 0;
foreach my $fileName ( @files ){

     my $status = 0;
     my $nHdus;

     last if ($count == 15000);

     my $isFits = 
       ($fileName =~ m/(fits|fit|fits\.gz|fts|fts\.gz|fits\.fz|fts\.fz)$/ )
       ? 1 : 0;

     next if !$isFits;

     print "Opening file# $count\n";

     my $fptr = Astro::FITS::CFITSIO::open_file(
         "$fileName",
         READONLY,
         $status
         );

     if ($status){
       $eventStr = "Problem opening $fileName:  $status";
       reportEvent($verboseLevel,'STATUS',4,$eventStr);

       $status=0;
       $exitStatus = 1;
     }

     my $naxes;
     my ($bitpix,$naxis);
     my $hduType = 0;
     $status = 0;

     my $hdrHashRef = $fptr->read_header();

     Astro::FITS::CFITSIO::fits_close_file($fptr,$status);
     $count++;

} # Big foreach loop by filetype

print "\n";

my $t1 = new Benchmark;

my $diff = timediff($t1,$t0);

print "Total number of files:  $count\n";
print "Total time to open $count files:  ",timestr($diff,'all'),"\n";



