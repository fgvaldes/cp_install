#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# photoz_catalog_ingest.pl
#
# DESCRIPTION:
#
# AUTHOR:  
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev:: 4386                                            $ rev of latest commit
# $LastChangedBy:: tdarnell                              $ Author of last rev
# $LastChangedDate:: 2009-09-24 09:23:19 -0700 (Thu, 24 #$ Date of last rev
#

use strict;
use Cwd;
use Data::Dumper;
use FindBin;
use File::Path;
use File::stat;
use Getopt::Long;
use Time::localtime;

use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use DB::DESUtil;
use DB::FileUtils;
use DB::EventUtils;
use DB::IngestUtils;

my ($fileList,$archiveNode,$imageId,$equinox,$isphotoz,$pastRun) = undef;
my $verboseLevel = 2;
my $eventStr = q{};
my $isIngested;

Getopt::Long::GetOptions(
   "filelist=s" => \$fileList,
   "archivenode=s" => \$archiveNode,
   "pastrun=s" => \$pastRun,
   "verboselevel=i" => \$verboseLevel,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply a filelist") unless defined $fileList;

#
# Read in the filelist
#
my @files;
readFileList($fileList,\@files);

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new
  (
             DBIattr => {
               AutoCommit => 0,
               printError => 0,
               RaiseError => 1
             }
  );


my ($locationId,$archiveHost,$archiveRoot,$archiveSiteStr) =
   getArchiveNodeInfo($desdbh, $archiveNode) if defined $archiveNode;

my $objTableInfoHashRef = getTableInfo($desdbh,'PHOTO_Z');

my ($resolvedFilesArrRef,$runIDSHashRef,$nitesHashRef,$project) = 
  parseFilelist(\@files,$archiveSiteStr,0,0,0);

#
# Main file loop
#
my $numFiles = 0;
my $numObjs  = 0;
my $localPath = q{};
my $catFile = q{};
my $nites;
my @resolvedFilenames = ();
my $ingestedIds;
my $insertHashRef;

foreach my $file (@files){

  my $catFile = $file->{'localfilename'};
  my $localPath = $file->{'localpath'};
  my $photozCatalogId = $file->{'fileid'};

  if (!$photozCatalogId){
    $eventStr = qq{fileid not present in filelist for $catFile};
    reportEvent($verboseLevel,'STATUS',5,$eventStr);
    exit(1);
  }

#
# if localpath is not absolute, set it to the current dir and append
# localpath to cwd.
#

  if ( defined $localPath ){
    if ( $localPath !~ m{^\/} ){
      $localPath = cwd() . qq{/$localPath};
    }
  } else {
    $localPath = cwd();
  }

  my $resolvePath = setUpResolvePath($file);

  my $fileInfo = filenameResolve(qq{$resolvePath/$catFile});

  my $fileType = $fileInfo->{'FILETYPE'};
  my $runId    = $fileInfo->{'RUN'};
  my $tileName = $fileInfo->{'TILENAME'};

#
# Check if this is a supported filetype
#
  if ( $fileType ne 'photoz_zcat' ){

      $eventStr =  qq{Not supported filetype:  $fileType
      Currently ingesting only catalogs of type 'photoz'
      };
      reportEvent($verboseLevel,'STATUS',5,$eventStr);
  }

#
# Read in this photoz tile
#

  $eventStr =  "Reading catalog: $catFile";
  reportEvent($verboseLevel,'STATUS',1,$eventStr);

  my $catHashRef = readPhotozCatalog( qq{$localPath/$catFile} );
  my $nRows = scalar @{$catHashRef->{'ID'}};

#
# Get photoz image and catalog information from location table
#
  my @catalogIds = ($photozCatalogId) x $nRows;
  my @photozTypes = ('HLIN') x $nRows;

  my $ingestedIds = checkIfPhotozCatalogIngested($desdbh,$photozCatalogId);
  $isIngested = $ingestedIds->[0][0];

  foreach my $key (keys %$catHashRef){
   $insertHashRef->{'PHOTO_Z'}->{$key} = $catHashRef->{$key};
  }

  $insertHashRef->{'PHOTO_Z'}->{'CATALOGID'} = \@catalogIds;
  $insertHashRef->{'PHOTO_Z'}->{'PHOTOZTYPE'} = \@photozTypes;

  #print Dumper (@{$insertHashRef->{'PHOTO_Z'}->{ID}});
  #printHashRef($insertHashRef);
  #my $numKeys = scalar(keys %$insertHashRef);
  #print "$numKeys\n";
  #exit(0);

}

if ($isIngested){
   $eventStr = qq{"Updating objects is not currently supported in photoz_catalog_ingest
   Please delete objects first, then reingest...};
   reportEvent(2,'STATUS',5,$eventStr);
   exit(0);
} else {
  $eventStr = qq{Starting batch ingest};
  reportEvent($verboseLevel,'STATUS',1,$eventStr);
  my $numObjIngested = 
    batchIngest($desdbh,$insertHashRef,0);
  $numObjIngested = 0 if not $numObjIngested;
  $eventStr = "Ingested $numObjIngested objects into PHOTOZ\n";
  reportEvent($verboseLevel,'STATUS',1,$eventStr);
}

$desdbh->commit();
$desdbh->disconnect;

exit(0);

#
# Subroutines
#
  
sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -filelist files -archivenode archive\n" .
      "       filelist contains the list of files along with the full path\n" .
      "       archivenode corresponds to one of the known archive nodes:\n" .
      "          bcs, des1, etc...\n"
   );

   die("\n")

}

