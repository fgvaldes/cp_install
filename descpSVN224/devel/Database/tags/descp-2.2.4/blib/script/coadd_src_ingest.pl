#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# coadd_src_ingest
#
# DESCRIPTION:
#
# AUTHOR:  
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev:: 6204                                         $ Revision of last commit
# $LastChangedBy:: mgower                             $ Author of last commit
# $LastChangedDate:: 2010-11-22 08:15:59 -0700 (Mon, #$ Date of last commit
#

use strict;
use Data::Dumper;
use FindBin;
use File::Basename;
use File::Path;
use File::stat;
use Getopt::Long;
use Time::localtime;

use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use DB::DESUtil;
use DB::EventUtils;
use DB::FileUtils;
use DB::IngestUtils;

use lib ("$ENV{DES_PREREQ}/lib/perl5/VOTable");
use Astro::VO::VOTable::Document;

my ($ingestList,$archiveNode,$imageName,$imageId,$band,$tileName,$run,$fileList,$xmlFile) = undef;

my $verboseLevel = 2;
Getopt::Long::GetOptions(
   "ingestlist=s" => \$ingestList,
   "filelist=s"   => \$fileList,
    "xmlfile=s"   => \$xmlFile,
   "verbose=i"   => \$verboseLevel,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply an ingestlist ") unless defined $ingestList;
usage("\n\nYou must supply a filelist ") unless defined $fileList;


#
# Read in the filelist
#
open (my $INPUTFILE, "< $ingestList");
my @all_lines = <$INPUTFILE>;
close ($INPUTFILE);

my @col0;
my @col1;
my @col2;
my @col3;

foreach my $line (@all_lines){
  chomp($line);
  my @splitArr = split(' ',$line);
  push @col1,$splitArr[0];
  push @col2,$splitArr[1];
  push @col3,$splitArr[2];
}

my $nRows = scalar (@all_lines);

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new
  (
             DBIattr => {
              AutoCommit => 0,
              RaiseError => 1
             }
  );

#
# Store the weight scale
#
my %weights;

if($xmlFile) {
    my @info=getInfo($xmlFile);

    for (my $i = 0; $i <= $#info; $i++) {
        my $weight=$info[$i]{'Weight_Scaling'};
        my $run=$info[$i]{'Run'};
        my $src_file=$info[$i]{'File'};
        
        # Get the image id from each src image
        my $sql = qq{ SELECT ID FROM IMAGE WHERE RUN = ? AND IMAGENAME = ?};
        my $sth = $desdbh->prepare($sql);
        $sth->execute($run,$src_file);
        my $results = $sth->fetchall_arrayref();
        my $src_id=$results->[0][0];
        $weights{$src_id}=$weight;
    }

    my $xml_Rows = scalar (@info);
    die "The number of files in the xml file does not equal the number of files in the zeropoint list\n" unless $xml_Rows==$nRows;
    
}

#
# make a list of weights with the same order as the zeropoints
#
my @col4;
for (my $i = 0; $i <= $#col1; $i++) {
    push @col4, $weights{$col1[$i]};
}

#
# Get image id of image catalog was made from
#
my @files;
readFileList($fileList,\@files);

#
# Take first row regardless of how many are in the list
#
my $eventStr = q{};
$imageId   = $files[0]->{'fileid'};
@col0 = ($imageId) x $nRows;

my $sql = qq{ SELECT COUNT(SRC_IMAGEID) FROM COADD_SRC WHERE COADD_IMAGEID = ?};
my $sth = $desdbh->prepare($sql);
$sth->execute($imageId);
my $results = $sth->fetchall_arrayref();
my $isIngested = $results->[0][0];

if ($isIngested){
  $eventStr = qq{$ingestList has already been ingested for coadd_imageid = $imageId and has $isIngested entries};
  reportEvent($verboseLevel,'STATUS',5,$eventStr);
  $desdbh->disconnect();
  exit(1);
}

#
# Check if this file has been ingested
#

my @tupleStatus, my $tuples;
if($xmlFile) {
    $sql = qq{
          INSERT INTO COADD_SRC 
          (COADD_IMAGEID,SRC_IMAGEID,MAGZP,MAGZP_ERR,WEIGHT_SCALING)
          VALUES (?,?,?,?,?)
    };

    $sth = $desdbh->prepare($sql);
    
    $tuples = $sth->execute_array(
        { ArrayTupleStatus => \@tupleStatus },
        (\@col0,\@col1,\@col2,\@col3,\@col4)
        );
    
    $sth->finish();
}
else {
    $sql = qq{
          INSERT INTO COADD_SRC 
          (COADD_IMAGEID,SRC_IMAGEID,MAGZP,MAGZP_ERR)
          VALUES (?,?,?,?)
    };

    $sth = $desdbh->prepare($sql);
    my @tupleStatus;
    my $tuples = $sth->execute_array(
        { ArrayTupleStatus => \@tupleStatus },
        (\@col0,\@col1,\@col2,\@col3)
        );
    
    $sth->finish();
}

if ($tuples){
   $eventStr = "Successfully inserted $tuples objects\n";
   reportEvent($verboseLevel,'STATUS',1,$eventStr);
} else {
}

$desdbh->commit();
$desdbh->disconnect;

exit(0);

#
# Subroutines
#
  
sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -ingestlist textfile\n" .
      " -filelist filelist\n" .
      "       ingestlist contains the data to be ingested in coadd_src\n".
      "       filelist contains the filename and fileid\n"
   );

   die("\n")

}


sub getInfo 
{

    my $file=shift; 

    # Read the VOTABLE document.
    my $doc = Astro::VO::VOTable::Document->new_from_file($file) or die "No xml file\n";
    
    # Get the (only) VOTABLE element.
    my $votable = $doc->get_VOTABLE or die "Did not find a VOTABLE\n";

    my $res=$votable->get_RESOURCE(0);

    my @info;
    foreach my $res2 ($res->get_RESOURCE) {
        
        # Table 0 contains the image info
        my $table=$res2->get_TABLE(0);
        
        # Get the FIELD elements.
        my(@field_names) = ();
        my($field);
        foreach $field ($table->get_FIELD) {
            push(@field_names, $field->get_name);
        }
        
        
        my $data = $table->get_DATA or die "Could not find a table\n";
        my $tabledata = $data->get_TABLEDATA or die "Could not find table data\n";
        
        # Get each TR element, then each TD element, and print its contents.
        my($tr, $td);
        my($i, $j);
        $i = 0;
        foreach $tr ($tabledata->get_TR) {
            #print "Data for target $i:\n";
            $j = 0;
            foreach $td ($tr->get_TD) {
                #print "  $field_names[$j] = ", $td->get, "\n";
                my $var=$field_names[$j];
                $info[$i]{$var}=$td->get;
                $j++;
            }
            $i++;
        }
    }

    # Add the filename and coadd run to the to the hash
    for (my $i = 0; $i <= $#info; $i++) {
        my ($run,$name)=$info[$i]{'Image_Name'}=~
            /.*\/(\d{14}_.*)\/.*\/(.*fits)/;
        my $filename.=".fits";
        $info[$i]{'File'}=$name;
        $info[$i]{'Run'}=$run;
    }
    
    return @info;
}

