#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# mes_catalog_ingest.pl
#
# DESCRIPTION:
#
# AUTHOR:
# Tony Darnell (tdarnell@tdarnell.edu)
#
# $Rev:: 6080                                              $ rev of latest commit
# $LastChangedBy:: mgower                                $ Author of last rev
# $LastChangedDate:: 2010-11-04 07:21:32 -0700 (Thu, 04 #$ Date of last rev
#

use strict;
use Cwd;
use Data::Dumper;
use FindBin;
use Getopt::Long;
use Time::localtime;

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use DB::DESUtil;
use DB::FileUtils;
use DB::EventUtils;
use DB::IngestUtils;

$| = 1;

my ( $fileList, $archiveNode, $verboseLevel ) = undef;
my $eventStr     = q{};

Getopt::Long::GetOptions(
    "filelist=s"     => \$fileList,
    "archivenode=s"  => \$archiveNode,
    "verboselevel=i" => \$verboseLevel,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply a filelist")     unless defined $fileList;
usage("\n\nYou must supply an archivenode") unless defined $archiveNode;

$verboseLevel = 2 if not $verboseLevel;

#
# Read in the filelist
#
my @files;
readFileList( $fileList, \@files );

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new(
    DBIattr => {
        AutoCommit => 0,
        printError => 0,
        RaiseError => 1
    }
);

my ( $locationId, $archiveHost, $archiveRoot, $archiveSiteStr ) =
  getArchiveNodeInfo( $desdbh, $archiveNode )
  if defined $archiveNode;

my ( $resolvedFilesArrRef, $runIDSHashRef, $nitesHashRef, $project ) =
  parseFilelist( \@files, $archiveSiteStr, 0, 0, 0 );

#
# Main file loop
#
my $numFiles  = 0;
my $numObjs   = 0;
my $localPath = q{};
my $catFile   = q{};
my $nites;
my @resolvedFilenames = ();
my $ingestedIds;
my $isIngested = 0;


my $tableName = 'WL_ME_SHEAR';
my $insertHashRef;

my $wlTableInfoHashRef;
my $band;
my $ucband;

foreach my $fileArrs (@$resolvedFilesArrRef) {

foreach my $file (@$fileArrs)
{
  #$file = $file->[0];

  my $catFile            = $file->{'FILENAME'};
  my $localPath          = $file->{'LOCALPATH'};
  my $wlCatalogId        = $file->{'FILEID'};
  my $resFileType        = $file->{'FILETYPE'};


#
# Check if this is a supported filetype
#
  if ( $resFileType ne 'shapelet_mes' ) {

    $eventStr =
      qq{mes_catalog_ingest: Not supported filetype, $resFileType, skipping... };
    reportEvent( $verboseLevel, 'STATUS', 4, $eventStr );
    next;

  }

   $wlTableInfoHashRef = getTableInfo( $desdbh, $tableName );

  if ( !$wlCatalogId ) {
    $eventStr = qq{fileid not present in filelist for $catFile};
    reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
    exit(1);
  }

#
# if localpath is not absolute, set it to the current dir and append
# localpath to cwd.
#

  if ( defined $localPath ) {
    if ( $localPath !~ m{^\/} ) {
      $localPath = cwd() . qq{/$localPath};
    }
  } else {
    $localPath = cwd();
  }

  my $resolvePath = setUpResolvePath($file);

  my $fileInfo = filenameResolve(qq{$resolvePath/$catFile});

  my $fileType = $fileInfo->{'FILETYPE'};
  my $runId    = $fileInfo->{'RUN'};
  $band     = $fileInfo->{'BAND'};
  my $tileName = $fileInfo->{'TILENAME'};

#
# Read in this MES catalog
#
  $eventStr = "Reading catalog: $catFile";
  reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );

#
# keys returned in hash ref will be in lower case
#
  my $catHashRef;
    $catHashRef = readCatalog( qq{$localPath/$catFile} );
#
# Convert all keys to upper case
#
  %$catHashRef = map {uc($_) => $catHashRef->{$_}} keys %$catHashRef;

#
# Rename the id key to object_number since id is already used
# from the sequencer.  This is usually a sequence from 1 .. # of rows
# in the catalog.
#
  $catHashRef->{'OBJECT_NUMBER'} = delete $catHashRef->{'ID'};

  my $nRows      = scalar( @{$catHashRef->{'OBJECT_NUMBER'}} );
  my @catalogIds = ($wlCatalogId) x $nRows;

#
# Check to see if this MES catalog is already in the WL_ME_SHEAR table
#
  my $ingestedIds =
    checkIfWLCatalogIngested( $desdbh, $wlCatalogId, $tableName, $band );
  $isIngested = $ingestedIds->[0][0];

  if ($isIngested) {
      $eventStr =
         qq{Contents of id $wlCatalogId already exist in the database\n};
         $eventStr .= qq{Moving to the next catalog...};
         reportEvent( 2, 'STATUS', 4, $eventStr );
         next;
  }

#
# Build the insert hash ref based on the table columns
#
 $ucband = uc $band;
my $bandKey;
  foreach my $key ( keys %$catHashRef ) {

	if($key ne 'ID' && $key ne 'RA' && $key ne 'DEC' && $key ne 'OBJECT_NUMBER' && $key ne 'COADD_OBJECTS_ID' )
	{
		$bandKey = $key."_".$ucband;
	}
	else
	{
		$bandKey = $key;
	}
    next if not $wlTableInfoHashRef->{$bandKey};
    $insertHashRef->{$bandKey} = $catHashRef->{$key};

  }

#
# Insert the CATALOGID.  This is the ID if the current
# MES catalog.  Taken from the filelist.
#
  $insertHashRef->{'CATALOGID'."_".$ucband} = \@catalogIds;

#
# Get the coadd_object_id corresponding to coadd objects in this catalog.  
# This is the ID in the COADD_OBJECTS table belonging to the coadd this was
# generated from
#

  my $objects = getMESObjects($desdbh,$wlCatalogId,$band);
  $insertHashRef->{'COADD_OBJECT_ID'} = $objects;

  my $numObjs = scalar (@$objects);

  if ($numObjs != $nRows){
    $eventStr = 
      "Number of coadd objects ($nRows) != number of objects ($numObjs) in mes catalog id: $wlCatalogId";
    reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
    next;
  }

#
# Do the batch ingestion
#


}
}

  my $numObjIngested =
    ingestWLHashRef( $desdbh, $insertHashRef, $wlTableInfoHashRef,
        $tableName, $band);
  $eventStr = "Ingested $numObjIngested objects into $tableName\n";
  reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );
  $desdbh->commit();
$desdbh->disconnect;

exit(0);

#
# Subroutines
#

sub usage {

    my $message = $_[0];
    if ( defined $message && length $message ) {
        $message .= "\n"
          unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

    print STDERR (
        $message,
        "\n"
          . "usage: $command "
          . " -filelist files -archivenode archive\n"
          . "       filelist contains the list of files along with the full path\n"
          . "       archivenode corresponds to one of the known archive nodes:\n"
          . "          bcs, des1, etc...\n"
    );

    die("\n")

}

