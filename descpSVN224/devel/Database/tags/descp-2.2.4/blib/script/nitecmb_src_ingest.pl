#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# nitecmb_src_ingest
#
# DESCRIPTION:
#
# AUTHOR:  
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev:: 4386                                         $: Revision of last commit
# $LastChangedBy:: tdarnell                           $: Author of last commit
# $LastChangedDate:: 2009-09-24 09:23:19 -0700 (Thu, #$: Date of last commit
#

use strict;
use Data::Dumper;
use FindBin;
use File::Basename;
use File::Path;
use File::stat;
use Getopt::Long;
use Time::localtime;

use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use DB::DESUtil;
use DB::EventUtils;
use DB::FileUtils;
use DB::IngestUtils;

my ($ingestList,$archiveNode,$imageName,$imageId,$band,$tileName,$run,$fileList) = undef;

my $verboseLevel = 2;
Getopt::Long::GetOptions(
   "ingestlist=s" => \$ingestList,
   "filelist=s"   => \$fileList,
   "verbose=i"   => \$verboseLevel,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply an ingestlist ") unless defined $ingestList;
usage("\n\nYou must supply a filelist ") unless defined $fileList;


#
# Read in the filelist
#
open (my $INPUTFILE, "< $ingestList");
my @all_lines = <$INPUTFILE>;
close ($INPUTFILE);

my @col1 = ();

foreach my $line (@all_lines){
  chomp($line);
  next if not $line;
  push @col1,$line;
}

my $nRows = scalar (@col1);

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new
  (
             DBIattr => {
              AutoCommit => 0,
              RaiseError => 1
             }
  );

#
# Get image id of image catalog was made from
#
my @files;
readFileList($fileList,\@files);

#
# Take first row regardless of how many are in the list
#
my $eventStr = q{};
$imageId = $files[0]->{'fileid'};
my @col0 = ($imageId) x $nRows;

my $sql = qq{ SELECT COUNT(SRC_IMAGEID) FROM NITECMB_SRC WHERE NITECMB_IMAGEID = ?};
my $sth = $desdbh->prepare($sql);
$sth->execute($imageId);
my $results = $sth->fetchall_arrayref();
my $isIngested = $results->[0][0];

if ($isIngested){
  $eventStr = qq{$ingestList has already been ingested for nitecmb_imageid = $imageId and has $isIngested entries};
  reportEvent($verboseLevel,'STATUS',5,$eventStr);
  $desdbh->disconnect();
  exit(1);
}

$sql = qq{
  INSERT INTO NITECMB_SRC
         (NITECMB_IMAGEID,SRC_IMAGEID)
         VALUES (?,?)
};

#$desdbh->trace(1);
$sth = $desdbh->prepare($sql);
my @tupleStatus;
my $tuples = $sth->execute_array(
   { ArrayTupleStatus => \@tupleStatus },
   (\@col0,\@col1)
   );

$sth->finish();

if ($tuples){
   $eventStr = "Successfully inserted $tuples objects\n";
   reportEvent($verboseLevel,'STATUS',1,$eventStr);
} else {
}

$desdbh->commit();
$desdbh->disconnect;

exit(0);

#
# Subroutines
#
  
sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -ingestlist textfile\n" .
      " -filelist filelist\n" .
      "       ingestlist contains the data to be ingested in coadd_src\n".
      "       filelist contains the filename and fileid\n"
   );

   die("\n")

}

