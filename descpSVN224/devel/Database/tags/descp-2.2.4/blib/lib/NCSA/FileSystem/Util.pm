#######################################################################
#  $Id: Util.pm 109 2008-02-04 23:28:19Z dadams $
#
#  $Rev:: 109                              $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-02-04 17:28:19 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package NCSA::FileSystem::Util;

use strict;
use warnings;
use Cwd;

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&scan_dir &get_file_sums);
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}

sub scan_dir {
  my $args = shift;
  my ($start_dir, @names, $path);

  ###########################################################################
  # Args:
  # scan_dir: Absolute path of directory to be scanned.
  # start_dir: Same as scan_dir, but will remain static upon recursive calling.
  # filenames_ref: Array ref to store file names.
  # sizes_ref: Array ref to store file sizes.
  # dlist_ref: Array ref containing at least scan_dir.
  # file_exclude: regex of file names to exclude (optional).
  # dir_excludes: Array ref of regex of directory names to exclude (optional).
  # recursive: Define this key to enable recursive parsing (optional).
  #
  ###########################################################################



  ###########################################################################
  # Get names in directory listing and loop through them, recursively calling 
  # this routine if the name is a directory:
  ###########################################################################

  $start_dir = getcwd();
  chdir($args->{'scan_dir'}) or die "Unable to enter $args->{'scan_dir'}:$!\n";
  opendir(FH,".") or die "Unable to open $args->{'scan_dir'}:$!\n";
  @names = readdir(FH) or die "Unable to read $args->{'scan_dir'}:$!\n";
  closedir(FH);
  $path = getcwd;

  NAME:foreach my $name (@names) {
    next if ($name eq '.');  # Skip current dir.
    next if ($name eq '..'); # Skip "up one" dir.
    if (-d $name) {
      if ($args->{'dir_excludes'}) {
        foreach my $exclude (@{$args->{'dir_excludes'}}) {
          next NAME if ($name =~ /$exclude/);       # Skip dirs that match the dir_excludes regexes.
        }
      }
      my $dir = join('/',$path,$name);               # Keep absolute path.
      push (@{$args->{'dlist_ref'}},$dir);           # Store directory in list.
      if ($args->{'recursive'}) {                    # Go deeper.
        $args->{'scan_dir'} = $dir;
        scan_dir($args);
        next NAME;
      }
    }
    elsif (-f $name){
      if ($args->{'file_excludes'}) {
        foreach my $exclude (@{$args->{'file_excludes'}}) {
          next NAME if ($name=~ /$exclude/);        # Skip files that match the file_excludes regexes.
        }
      }
      my $fullname=join('/',$path,$name);            # Get absolute path/filename.
      my $fsize= -s $fullname;                       # Get file size in bytes.
      $fullname=~s/^$args->{'start_dir'}//;          # Keep only relative path part.
      $fullname=~s/^\///;                            # Ditch leading '/'.
      push (@{$args->{'filenames_ref'}},$fullname);  # Store "rel path/filename".
      push (@{$args->{'sizes_ref'}},$fsize);         # Store size.
    }
  }

  chdir ($start_dir) or die "Unable to enter $start_dir:$!\n" if $start_dir;
}


#####################################################################################################################################################
# get_file_sums
#####################################################################################################################################################
sub get_file_sums {
  my $file=shift;

  my %file_sums;
  # Get file size.
  my $size = -s $file;
  # Get cksum.
  my $cksum_out = `cksum $file`;
  (my $cksum,my $cksize,my $ckfile) = split (' ',$cksum_out);
  # Get md5sum.
  my $md5sum_out = `md5sum $file`;
  (my $md5sum, my $md5file) = split (' ',$md5sum_out);

  $file_sums{'size'} = clean_string($size);
  $file_sums{'cksum'} = clean_string($cksum);
  $file_sums{'md5sum'} = clean_string($md5sum);

  return \%file_sums;

}


1;
