#######################################################################
#  $Id: Uberftp.pm 1077 2008-05-27 20:57:01Z dadams $
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
# 
#  DESCRIPTION:
# 
#######################################################################

package NCSA::Transfer::guc;
use strict;
use warnings;
use Carp;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
#use NCSA::Transfer;
use base qw(NCSA::Common);
use NCSA::Transfer::Util qw(:all);
use NCSA::Transfer::Exceptions;
use NCSA::String::Util;
use List::Util qw(sum shuffle);

#use vars qw(@ISA);
#our @ISA = qw(NCSA::Common);
  
my %ClassData = (
  'guc_client' => 'globus-url-copy',
);


sub transferguc {
  my $this = shift;
  my $Args = shift;

  my $ArgsARef = $Args->{'xferArgs'};
  my $verbose;
  if ($Args->{'verbose'}) {
    $verbose = $Args->{'verbose'};
  }
  else {
    $verbose = 1;
  }
  my $try = 1;
  if ($Args->{'try'}) {
    $try = $Args->{'try'};
  }

  # Set output file names:
  my $base_cmd_file = 'guc-transfer-input';
  my $base_out_file = 'guc-transfer';
  my $trynum = sprintf("%0*d", 2, $try);
  $base_cmd_file = join('_',$base_cmd_file,$trynum);
  $base_out_file = join('_',$base_out_file,$trynum);

  my $ctr = 0;
  my (@pids, @outfiles);

  prepareTransfers($ArgsARef, $base_cmd_file, $base_out_file, $verbose);

  foreach my $ArgsRef (@$ArgsARef) {
    my $srcFilesRef = $ArgsRef->{'srcfiles'};
    my $destFilesRef = $ArgsRef->{'destfiles'};
    my $FileIndexLists = $ArgsRef->{'file_index_lists'};
    for(my $ln=0;$ln<=$#$FileIndexLists;$ln++) {
      my $list = $FileIndexLists->[$ln];
      my $cmds;
      push(@outfiles, $ArgsRef->{'out_file'});
      # Generate put commands and put them in the cmd_file:
      print "\nGenerating globus-url-copy transfer commands" if ($verbose >= 5);
      my ($srcuri, $desturi);
      if ($ArgsRef->{'src_server'}) {
        $srcuri = 'gsiftp://'.$ArgsRef->{'src_server'};
        if ($ArgsRef->{'src_server_port'}) {
          $srcuri = $srcuri.":$ArgsRef->{'src_server_port'}";
        }
      }
      else {
        $srcuri = 'file://';
      }
      if ($ArgsRef->{'target_server'}) {
        $desturi = 'gsiftp://'.$ArgsRef->{'target_server'};
        if ($ArgsRef->{'target_server_port'}) {
          $desturi = $desturi.":$ArgsRef->{'target_server_port'}";
        }
      }
      else {
        $desturi = 'file://';
      }

      for(my $i=0; $i<=$#$list;$i++) {
        if ($cmds) {
          $cmds = $cmds.$srcuri.$srcFilesRef->[$list->[$i]].' '.$desturi.$destFilesRef->[$list->[$i]]."\n";
        }
        else {
          $cmds = $srcuri.$srcFilesRef->[$list->[$i]].' '.$desturi.$destFilesRef->[$list->[$i]]."\n";
        }
      }
      open (FH,">$ArgsRef->{'cmd_file'}");
      print FH $cmds;
      close FH;
      print "Done!\n" if ($verbose >= 5);
      $ArgsRef->{'verbose'} = $verbose if (defined $verbose);
      #my $pid = rungucTransferFile($ArgsRef);
      my $pid = $this->rungucTransferFile($ArgsRef);
      push(@pids, $pid);
    }
  }
  #Wait on transfer(s) to finish:
  foreach(@pids) {
    waitpid($_,0);
  }
  ##########################################
  # Verify transfer(s) output.
  ##########################################


}

#################################################################################
#
#################################################################################
sub rungucTransferFile {
  my $this = shift;
  my $Args = shift;
  
#  if (! $Args->{'guc'}) {
#    $Args->{'guc'} = 'globus-url-copy';
#  } 
  if (! $Args->{'cmd_file'}) {
    croak("Must provide \'cmd_file\'.\n");
  } 
  if (! $Args->{'out_file'}) {
    croak("Must provide \'out_file\'.\n");
  } 
  
#  my $cmd = $Args->{'guc'};
  my $cmd = $this->guc_client();
  
  # Add options:
  if ($Args->{'options'}) {
    while ((my $key, my $val) = each %{$Args->{'options'}}) {
      if ($key =~ /^-/) {
        $cmd = $cmd." $key $val";
      }
    }
  } 
  
  # Create and execute command:
  if ($Args->{'err_file'}) {
    $cmd = "$cmd -f $Args->{'cmd_file'} 1>$Args->{'out_file'} 2>$Args->{'err_file'}";
  } 
  else {
    $cmd = "$cmd -f $Args->{'cmd_file'} >$Args->{'out_file'} 2>&1";
  }
  my $pid = fork_process($cmd);
  
  if (exists $Args->{'verbose'}) {
    if ($this->debug()) {
      print "Executing command:\n$cmd\npid: $pid\n";
    }
  } 

  return $pid;
}

sub guc_client {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'guc_client'} = $newvalue;
  
    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::guc_client")) {
      $self->SUPER::guc_client($newvalue);
    }
  }
  return $ClassData{'guc_client'};
} 
  

1;
