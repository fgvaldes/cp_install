########################################################################
#
#  $Id: wrapDBI.pm 2613 2008-12-09 20:16:50Z dadams $
#
#  $Rev:: 2613                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-12-09 13:16:50 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
################################################################################
################################################################################
package NCSA::DB::wrapDBI;
use strict;
use warnings;
use Data::Dumper;
use Switch;
use Carp;
use Exception::Class::DBI;
use DBI;
use NCSA::Common;
#use vars qw(@ISA);
our @ISA = qw(DBI NCSA::Common);

################################################################################
#  SUBROUTINE: new
#    Get a DBI connection object and return it.
################################################################################
sub new {
  my $this = shift;
  my $Args = shift;

  my $self;

  # If a config file is said to exist, use it.
  if ($Args->{'db_config_file'}) {
    $self = $this->connectFromConfigFile($Args);
  }
  else {
    $self = $this->connectUsingVars($Args);
  }

  $this->_init($Args);
  return $self;  
}

sub _init {
  my $self = shift;
  my $Args = shift if (@_);
  $Args = {} if (! defined $Args);
  my $PackageArgs;
  my $PACKAGE = __PACKAGE__;
  if ($Args->{$PACKAGE}) {
    $PackageArgs = $Args->{$PACKAGE};
  }
  else {
    $PackageArgs = $Args;
  }
  $self->verbose($PackageArgs->{'verbose'}) if ($PackageArgs->{'verbose'});

  return $self;
}

################################################################################
# SUBROUTINE: connectFromConfigFile
#  Parse a 'db_config_file' and get a connection.
################################################################################
sub connectFromConfigFile {
  my $this = shift;
  my $argsRef = shift;

  
  my %args;
  $argsRef = \%args if (! defined $argsRef);
  my $dbcfgfile = $argsRef->{'db_config_file'} if ($argsRef->{'db_config_file'});
  croak ("Must provide 'db_config_file' argument\n") if (! $dbcfgfile);

  my ($line, $left, $right);

  if (! -r $dbcfgfile)
  {
     croak("Unable to read: '$dbcfgfile'\nto parse for database connection information.");
  }

  # Knock permissions back to 0600 to keep passwords safe.
  # TODO - Check current permissions and warn user if not strong enough
  chmod 0600, $dbcfgfile or warn "Unable to chmod $$dbcfgfile, maybe you don't own the file...\n";

  open(FH, "< $dbcfgfile") or croak("Unable to open: '$dbcfgfile'\n to parse for database connection information.\n");

  # Parse the config file, looking for the standard info:
  my $linenum = 0;
  my $dbname = undef;
  while($line = <FH>) {
    $linenum++;
    $line =~  s/^\s+//g;
    $line =~ s/\s+$//g;
    if (($line =~ /\S/) && ($line !~ /^#/))
    {
       if ($line =~ /=/)
       {
          ($left, $right) = $line =~ m/^(\S+)\s*=\s*(\S.*)$/;
       }
       else
       {
          ($left, $right) = $line =~ m/^(\S+)\s*(.*)$/;
       }
       $left =~ tr/A-Z/a-z/;
       switch ($left)
       {
          case /^db_type$/i { $argsRef->{'db_type'} = $right if (! defined $argsRef->{'db_type'}); }
          case /^db_server$/i { $argsRef->{'db_server'} = $right if (! defined $argsRef->{'db_server'}); }
          case /^db_name$/i { $dbname = $right if (! defined $dbname); }
          case /^db_sid$/i { $dbname = $right; }
          case /^db_user$/i { $argsRef->{'db_user'} = $right if (! defined $argsRef->{'db_user'}); }
          case /^db_pass$/i { $argsRef->{'db_pass'} = $right if (! defined $argsRef->{'db_pass'}); }
          case /^DB_PASSWD$/i { $argsRef->{'db_pass'} = $right if (! defined $argsRef->{'db_pass'}); }
          else  
          {
             warn "ERROR: Unrecognized line in db config ('$dbcfgfile')\n";
          }
       }
    }
  }
  close FH;
  $argsRef->{'db_name'} = $dbname if (! defined($argsRef->{'db_name'}));

  # Connect:
  if ($argsRef->{'verbose'} && $argsRef->{'verbose'} >= 3) {
    print "\nConnecting with \"connectFromConfigFile\" method.\n";
  }
  my $self = getConnection($this, $argsRef);
 
  return $self;
}

################################################################################
# SUBROUTINE: connectUsingVars
################################################################################
sub connectUsingVars {
  my $this = shift;
  my $argsRef = shift;

  my $self;
  if (! defined $argsRef) {
    croak ("Database arguments are needed to connect with this routine.\n");
  }
  # Connect:
  if ($argsRef->{'verbose'} && $argsRef->{'verbose'} >= 3) {
    print "\nConnecting with \"connectUsingVars\" method.\n";
  }
  $self = getConnection($this, $argsRef);
}

################################################################################
# SUBROUTINE: getConnection
################################################################################
sub getConnection {
  my $this = shift;
  my $argsRef = shift;

  my $dbh;

  # Test that we have what we need to do the connection:
  foreach my $key (("db_type","db_server", "db_name", "db_user", "db_pass")) {
    if (! defined  $argsRef->{"$key"} ) {
       croak("Insufficient data to make a dtabase connection.\nMissing value for $key\n");
    }
  }

  ##############################################################################
  # Oracle connection
  ##############################################################################
  if ($argsRef->{'db_type'} =~ /ORACLE/i) {
    my $connect_string;
    if ($argsRef->{'DBIattr'}) {
    $dbh = $this->SUPER::connect("DBI:Oracle:host=$argsRef->{'db_server'};sid=$argsRef->{'db_name'}",$argsRef->{'db_user'},$argsRef->{'db_pass'}, $argsRef->{'DBIattr'})
                or croak("Database connection error: $DBI::errstr\n");
    }
    else {
    $dbh = $this->SUPER::connect("DBI:Oracle:host=$argsRef->{'db_server'};sid=$argsRef->{'db_name'}",$argsRef->{'db_user'},$argsRef->{'db_pass'})
                or croak("Database connection error: $DBI::errstr\n");
    }
    # Verbose connection status output:
    if ($argsRef->{'verbose'} && $argsRef->{'verbose'} >= 3) {
      print "Successfully connected to the '$argsRef->{'db_name'}' database on $argsRef->{'db_server'}.\n";
      if ($argsRef->{'DBIattr'}) {
        print "Connected with the following DBI attributes:\n";
        while ((my $k, my $v) = each %{$argsRef->{'DBIattr'}}) {
          print "$k = $v\n";
        }
      }
      print "\n";
    }
  }
  ##############################################################################
  # Another connection...
  ##############################################################################

  else {
    croak("No connection method yet defined for db_type: $argsRef->{'db_type'}.\n");
  }

  return $dbh;
}

package NCSA::DB::wrapDBI::db;

use Carp;
use strict;
use warnings;
use Data::Dumper;
use Exception::Class::DBI;
use NCSA::Common;


#use vars qw(@ISA);
our @ISA = qw(DBI::db NCSA::Common);

#######################################################################
#  createTableAs
#  
#  INPUT:
#    Single hashref of arguments:
#      'table_name'  - Name of new table
#      'as'          - Name of existing table to mimic
#      'existok'     - 
#
#  DESCRIPTION:
#    Create a new database table that shares its' entire structure with
#    another existing table.  This function is written with exception 
#    handling that depends on Exception::Class::DBI.
#
#######################################################################
sub createTableAs {
  my $dbh = shift;
  my $Args = shift;  # pass a hashref of arguments:

  my $tableName = $Args->{'table_name'} if ($Args->{'table_name'});
  my $AsTableName = $Args->{'as'} if ($Args->{'table_name'});
  my $existok = $Args->{'existok'} if ($Args->{'existok'});

  croak("Must provide a table name.\n") if !($tableName);
  croak("Must provide a table name to created new table \"AS\".\n") if !($AsTableName);

  print "Creating $tableName using $AsTableName as a template.\n";

  my $sql= "CREATE TABLE $tableName AS SELECT * FROM $AsTableName WHERE 0=1";

  eval {
  my $sth = $dbh->do($sql);
  $sth->finish();
  };

  if (my $e =  Exception::Class::DBI->caught()) {
    if ($e->err == 955 && $existok) {
      warn "Table: $tableName exists continuing...\n";
    }
    else {
      print STDERR "\nDBI Exception:\n";
      print STDERR "  Exception Type: ", ref $e, "\n";
      print STDERR "  Err:            ", $e->err, "\n";
      #print STDERR "  Error:\n", $e->error, "\n\n";
      $e->rethrow;
    }

  }

}

################################################################################
# SUBROUTINE: queryDB
# Arguments:
#   A single hash(ref) with the following parameters:
#      'table' - Name of database table.
#      'key_vals' - Data to define the WHERE clause (optional).
#      'where' - Specified where argument (optional).
#      'select_fields' - Arrayref of field names to be selected (optional).
################################################################################
sub queryDB {
  my $this = shift;
  my $Args = shift;

  my ($where,$table,$dbq,$fields,$select,$Rows,@where_statements,$FKeys,@FKeys,@bind_keys);

  local * bail = sub {
    my $message = shift;
    croak (
      qq(\nIn: NCSA::DB::wrapDBI::queryDB:\n),
      qq($message\n),
      qq(Expected Arguments:\n),
      qq(  'table' => string\n),
      qq(  'key_vals' => hashref OPTIONAL\n),
      qq(  'where => string OPTIONAL\n),
      qq(  'select_fields' => arrayref OPTIONAL\n\n)
    );

  };

  #########################################################
  #
  # Process arguments
  #
  #########################################################
  
  # Die if no table is provided:
  if ($Args->{'table'}) {
    $table = $Args->{'table'};
  }
  else {
    bail('Must specify a table name');
  }

  # Funky where arguments can also be specified:
  if ($Args->{'where'}) {
    @where_statements = @{$Args->{'where'}};
  }

  # List of key_vals for where clause:
  if (exists $Args->{'key_vals'}) {
    $FKeys = $Args->{'key_vals'};
    @FKeys = keys %{$Args->{'key_vals'}};
  }

  # Get actual column names for the provided table:
  my $cols = $this->getColumnNames($table);

  #########################################################
  #
  # Build the SQL
  #
  #########################################################
  
  
  # Setup the SELECT part of the sql statement, grab all columns (*) 
  # unless some are provided:
  if ($Args->{'select_fields'}) {
    foreach my $field ( @{$Args->{'select_fields'}} ) {
      if (exists $cols->{lc($field)}) {
        if ($fields) {
          $fields .= ','.$field;
        }
        else {
          $fields = $field;
        }
      }
      else {
        bail("The $field column does not exist in the $table table.");
      }
    }
    $select = "SELECT $fields FROM $table";
  }
  else {
    $select = "SELECT * FROM $table";
  }

  

  # Turn key_vals information into a where clause.  If any value is an arrayref
  # it gets pushed into and array of keys that will be parameter bound and looped over
  # for each value in the array when the query is executed below.
  if ($FKeys) {
    foreach my $key (@FKeys) {
      if (! exists $cols->{lc($key)}) {
        bail("The $key column does not exist in the $table table.");
      }
      my $val = $FKeys->{"$key"};
      # Do parameter bind and multiple executes for large lists.  For
      # small lists simply add 'IN' clause to the where statement
      if (ref $val eq 'ARRAY') {
        if (scalar @$val > 50) {
          push(@bind_keys, $key);
          if ($where) {
            $where = $where." AND $key=?";
          }
          else {
            $where = "WHERE $key=?";
          }
        }
        else {
          if ($where) {
            $where = $where." AND $key IN (".join(',', map{$this->quote($_)} @$val ).")";
          }
          else {
            $where = "WHERE $key IN (".join(',', map{$this->quote($_)} @$val ).")";
          }
        }
      }
      else {
        # REGEXP_LIKE:
        if ($val =~ /[\*\^\$\[\]\&]/) {
           if ($where) {
            $where = $where." AND REGEXP_LIKE($key,'$val')";
          }
          else {
            $where = "WHERE REGEXP_LIKE($key,'$val')";
          }
        }
        # LIKE:
        elsif ($val =~ /\%/ && $val !~ /!/) {
          if ($where) {
            $where = $where." AND ".$key.' LIKE '.$this->quote($val);
          }
          else {
            $where = "WHERE ".$key.' LIKE '.$this->quote($val);
          }
          if ($val =~ /\\/) {
            $where .= " ESCAPE '\\'";
          }
        }
        # NOT LIKE:
        elsif ($val =~ /\%/ && $val =~ /!/){
          $val =~ s/!//;
          if ($where) {
            $where = $where." AND ".$key.' NOT LIKE '.$this->quote($val);
          }
          else {
            $where = "WHERE ".$key.' NOT LIKE '.$this->quote($val);
          }
          if ($val =~ /\\/) {
            $where .= " ESCAPE '\\'";
          }
        }
        # Not equal:
        elsif ($val =~ /!/ && $val !~ /\%/) {
          $val =~ s/!//;
          if ($where) {
            $where = $where." AND $key!=".$this->quote($val);
          }
          else {
            $where = "WHERE $key!=".$this->quote($val);
          }
        }
        # Equal:
        else {
          if ($where) {
            $where = $where." AND $key=".$this->quote($val);
          }
          else {
            $where = "WHERE $key=".$this->quote($val);
          }
        }
      }
    }
  }

  # Add completly specified where arguments:
  if (@where_statements) {
    foreach my $clause (@where_statements) {
      if ($where) {
        $where = $where." AND ".$clause;
      }
      else {
        $where = "WHERE ".$clause;
      }
    }

  }

  # The rest of the sql...
  if ($where) {
    $dbq = join(' ',$select,$where);
  }
  else {
    $dbq = $select;
  }

  #########################################################
  #
  # Execute the SQL and get result data set.
  #
  #########################################################

  my $sth = $this->prepare($dbq);

  if ($this->verbose() >= 2) {
    print "\nExecuting: $dbq\n";
  }



  if (@bind_keys) {
    my $N_bound_fields = $#bind_keys;
    bail("Only one multi-valued field is currently supported") if ( $N_bound_fields > 0);
    #foreach my $bound_field (@bind_keys) {
      my $bound_field = $bind_keys[0];
      my $values = $Args->{'key_vals'}->{"$bound_field"};
      my $N_values = $#$values;
 
#      # Expecting only one row in each call:
#      foreach (my $i=0; $i<=$N_values; $i++) {
#        $sth->execute($values->[$i]);
#        $Rows->[$i] = $sth->fetchrow_hashref;
#      }

      # Expecting multiple rows in one call:
      foreach (my $i=0; $i<=$N_values; $i++) {
        $sth->execute($values->[$i]);
        my $Set =  $sth->fetchall_arrayref({});
        push(@$Rows,@$Set);  # Seems like this might suck.
      }


    #}


  }


  else {
    $sth->execute();
    $Rows = $sth->fetchall_arrayref({});
  }
  $sth ->finish();
  $this->commit();

  my $nrows = $#$Rows + 1;
  if ($this->verbose() >= 2) {
    if ($nrows == 1) {
      print "Selected $nrows row.\n";
    }
    else {
      print "Selected $nrows rows\n";
    }
  }

  return $Rows;
}

################################################################################
# SUBROUTINE: get_row
# Returns a string that is the result of supplied query.  Use when expecting 
# only one field from the query.
################################################################################
sub get_row {
  my $dbh = shift;
  my $dbq = shift;

#  my $sth = $dbh->prepare($dbq);
#  $sth->execute();
#  my $row = $sth->fetchrow_array();
#  $sth->finish();
  my $row = $dbh->selectrow_array($dbq);
  chomp $row  if $row;               # Remove trailing newline.
  $row =~ s/^\s*//g if $row;         # Remove leading whitespace.
  $row =~ s/\s*$//g if $row;         # Remove trailing whitespace.
  return $row;
}

################################################################################
# SUBROUTINE: make_insert_sql
#
################################################################################
sub makeInsertSql {
  my $this = shift;
  my $table = shift;
  my $fields_ar = shift;
  my $values_ar = shift;

  my @fields_copy = @$fields_ar;

  $table = join('','`',$table,'`');
  # Make fields string
  my $fields;
  foreach my $field (@fields_copy) {
    $field = join('','`',$field,'`');
    if (! $fields) {
      $fields = $field;
    }
    else {
      $fields = join(', ', $fields, $field);
    }
  }
  # Make values string.
  my $values;
  foreach my $value (@$values_ar) {
#    $value = join('','\'',$value,'\'');
    $value = $this->{dbh}->quote($value) if ($value !~ /^\?$/);
    if (! $values) {
      $values = $value;
    }
    else {
      $values = join(', ',$values,$value);
    }
  }

  my $dbi = "INSERT INTO $table ($fields) VALUES ($values)";

  return $dbi;
}

################################################################################
# SUBROUTINE: get_single_result
#
################################################################################
sub getSingleResult {
  my $this = shift;
  my $dbq = shift;

  my $Result = $this->get_row($dbq);
  return $Result;
}

################################################################################
# SUBROUTINE: getColumnNames
#
################################################################################
sub getColumnNames {
  my $this = shift;
  my $table = shift;

  # Get all column names for our table:
  my $dbq = "SELECT * FROM $table WHERE 0=1";
  my $sth = $this->prepare($dbq);
  $sth->execute;
  my $cols = $sth->{NAME_lc_hash};
  $sth->finish;

  return $cols;

}


#
#
#
package NCSA::DB::wrapDBI::st;
use vars qw(@ISA);
our @ISA = qw(DBI::st);

=head1 NAME

NCSA::DB::wrapDBI;

=head1 SYNOPSIS

use NCSA::DB::wrapDBI;
my $db = new NCSA::DB::wrapDBI;

$db-> all DBI methods.
$db->connectFromConfigFile('file');

=head1 DESCRIPTION



1;
