########################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package Orchestration::email;

use strict;
use warnings;

use Cwd;
use File::Basename;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::desconfig;

use Exporter   ();
our (@ISA, @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw(&sendEmail &sendSubblockEmail &getJobInfo);



#################################################################
sub sendEmail {
    my $config = shift;
    my $block = shift;
    my $status = shift;
    my $subject = shift;
    my $msg1 = shift;
    my $msg2 = shift;
 
    my $run = $config->value('submit_run');
    my $project = $config->value('project');

    my $localmachine = `/bin/hostname -f`;
    chomp($localmachine);
 
    my $mailfile = "email_$block.txt";
    open MAIL, "> $mailfile";

    print MAIL "**************************************************************\n";
    print MAIL "*                                                            *\n";
    print MAIL "*  This is an automated message from DESDM.  Do not reply.   *\n";
    print MAIL "*                                                            *\n";
    print MAIL "**************************************************************\n";
    print MAIL "\n";

    print MAIL "$msg1\n\n\n";

    print MAIL "operator = ", $config->value('operator'), "\n";
    print MAIL "project = ", $project, "\n";
    print MAIL "run = ", $run, "\n";
    if ($config->exists('event_tag')) {
        print MAIL "run name = ", $config->value('event_tag'), "\n";
    }
    print MAIL "\n";

    print MAIL "Submit:\n";
    print MAIL "\tmachine = $localmachine\n";
    print MAIL "\tnode = ", $config->value("submit_node"), "\n";
    print MAIL "\tDES_HOME = ", $config->value("des_home"),"\n";
    print MAIL "\tconfig = ", $config->value("submit_dir"), "/", $config->value("config_filename"), "\n";
    print MAIL "\tdirectory = ", $config->value("work_dir"),"\n\n";

    print MAIL "Target:\n";
    print MAIL "\tmachine = ", $config->value('login_host'),"\n";
    print MAIL "\tnode = ", $config->value("target_node"), "\n";
    print MAIL "\tdirectory = ", $config->value("archive_root"),"/",$config->value("run_dir"), "\n";
    print MAIL "\n\n";
    print MAIL "------------------------------\n";

    if (defined($msg2)) {
        print MAIL "$msg2\n";
    }

    close MAIL;

    $subject = "DESDM: $project $run $block $subject";
#    if ($status == $NOTARGET) {
#        $subject .= " [NOTARGET]";
#    }
#    elsif ($status != $SUCCESS) {
#        $subject .= " [FAILED]";
#    }

    if ($config->exists('email')) {
        my $email = $config->value('email');
        debug("$block", "Sending email to $email");
        `/bin/mail -s "$subject" $email < $mailfile`;

#        unlink "$mailfile";
    }
    else {
        debug($block, "No email address.  Not sending email.\n");
    }

    if ($config->exists('nhpps_input') && $block ne 'endrun') {
        my $fname = $config->value('nhpps_input');
	`/bin/echo "$subject" > $fname`;
	`/bin/cat $mailfile >> $fname`;
    }
    if ($config->exists('nhpps_trig') && $block ne 'endrun') {
        my $fname = $config->value('nhpps_trig');
	`/bin/touch $fname`;
    }
}

sub sendSubblockEmail {
    my ($config, $block, $subblock, $retval) = @_;
    print "sendSubblockEmail BEG\n"; 
    print "sendSubblockEmail block=$block\n";
    print "sendSubblockEmail subblock=$subblock\n";
    print "sendSubblockEmail retval=$retval\n"; 
    my $Msg1 = "Failed subblock = $subblock";
    my $Msg2 = getSubblockOutput($subblock);
    sendEmail($config, $block, $retval, "[FAILED]", $Msg1, $Msg2);
    print "sendSubblockEmail END\n"; 
}


sub getJobInfo {
    my ($block) = @_;
    my $str = "";
    $str = sprintf "%6s\t%25s\t%7s\t%7s\t%s\n", 'JOBID', 'MODULE', 'STATUS4', 'STATUS5', 'MSG';
    my @jobinfofiles = <../${block}_*/*.jobinfo.out>;
    foreach my $f (sort @jobinfofiles) {
        open FH, $f;
        while (<FH>) {
            $str .= $_;
        }
        close FH;
    }
    return $str;
}
    
sub getSubblockOutput {
    my ($subblock) = @_;
    my $NUMLINES = 50;
    my $Output = "";

    my ($block, $path) = fileparse(cwd()); 
    my ($fileout, $fileerr);

    $fileout = "$path/$block/$subblock.out";
    $fileerr = "$path/$block/$subblock.err";

    $Output .= "Standard output = $fileout\n";
    $Output .= "Standard error = $fileerr\n";
    $Output .= "\n\n";

    $Output .= "===== Standard error  - Last $NUMLINES lines =====\n";
    if (-r $fileerr) {
        $Output .= `tail -$NUMLINES $fileerr`;
    }
    else {
        $Output .= "Could not read standard err file for $subblock\n";
    }
    $Output .= "\n\n";
    $Output .= "===== Standard output - Last $NUMLINES lines =====\n";
    if (-r $fileout) {
        $Output .= `tail -$NUMLINES $fileout`;
    }
    else {
        $Output .= "Could not read standard out file for $subblock\n";
    }
    return $Output;
}


1;
