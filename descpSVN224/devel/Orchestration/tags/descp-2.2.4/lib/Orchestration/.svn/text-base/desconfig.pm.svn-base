########################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit.
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at:
#  The National Center for Supercomputing Applications (NCSA).
#  with useful conversations with Jordi Cohen and Jim Phillips from the
#  Theoretical and Computation Biophysics Group at the University
#  of Illinois at Urbana-Champaign.
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package Orchestration::desconfig;

use warnings;
use strict;

use File::Basename;
use File::Temp;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use DAF::Archive::Tools qw(getArchiveLocation);
use Config::General;
use Orchestration::misc;
use Cwd;
use Data::Dumper;

use vars qw(@ISA);
use Exporter ();
our ( @ISA, @EXPORT );
#@ISA    = qw(Config::General Config::General::Extended Config::General::Interpolated Exporter);
@ISA    = qw(Config::General Config::General::Extended Exporter);


#@EXPORT = qw($SUCCESS $REPEAT $FAILURE);
#
#our $SUCCESS    = 0;
#our $REPEAT     = 100;
#our $DONTREPEAT = 1;
#our $FAILURE    = 1;

# order in which to search for values
use constant DEFORDER => qw(job module block archive software site);

##########################################################################
sub new {
    my $this = shift;
    my $args = shift;
    my $configfile = $args->{'configfile'};

    # read default values from database
    my %defaultconfig = ();

    if (exists($args->{'querydb'}) && $args->{'querydb'}) {
       $this->query_db(\%defaultconfig);
    }
#    $defaultconfig{"current"}{"block"}    = "convert";
#    $defaultconfig{"current"}{"site"}     = "ncsa_mercury";
#    $defaultconfig{"current"}{"archive"}  = "mercury_mgower";
#    $defaultconfig{"current"}{"software"} = "mercury_1.0";

    my $etcpath = "$FindBin::Bin/../etc";
    my %config_options = (
        -ConfigFile            => "$configfile",
        -DefaultConfig         => \%defaultconfig,
        -ConfigPath            => $etcpath,
        -IncludeGlob           => 1,
        -IncludeDirectories    => 1,
        -IncludeRelative       => 1,
        -LowerCaseNames        => 1,
        -AutoTrue              => 1,
        -MergeDuplicateBlocks  => 1,
        -MergeDuplicateOptions => 1,
        -ExtendedAccess        => 1,
        -SaveSorted            => 1,
    );

    my $class = ref($this) || $this;

    my $self;

    # To ensure case-insensitivity for named blocks, write out config and read it again.
    # Writing it out will convert named blocks to nested blocks which when read in are case-insensitive
    if (exists($args->{'namedblocks'}) && $args->{'namedblocks'}) {
        my $namedcfg = $class->SUPER::new(%config_options);
        my $filename = "/tmp/$configfile.$$.".time;
        $namedcfg->save_file($filename);
        $namedcfg=undef;
        $config_options{'-ConfigFile'} = $filename;
        $config_options{'-DefaultConfig'} = undef;
        delete($config_options{'-DefaultConfig'});
        $self = $class->SUPER::new(%config_options);
        unlink $filename;
    }
    else {
        $self = $class->SUPER::new(%config_options);
    }
    bless ($self, $class);

    if (exists($args->{'notarget'})) {
        $self->SUPER::value('notarget', $args->{'notarget'});
    }

    my $block_list = lc($self->value("block_list"));
    my @blockarray =  split /\s*,\s*/, $block_list;
    $self->SUPER::value("block_array", \@blockarray);
    $self->SUPER::value("num_blocks", scalar(@blockarray));
    # Set value to store the file name of the top-level config:
    if (! $self->SUPER::exists("config_filename")) {
      $self->SUPER::value("config_filename",$configfile);
    }

    if (! $self->SUPER::exists("current")) {
#        print " initializing current \n";
        my %curhash = ( job_id => 1, block_num => 0, block => "", 
            archive => "", software => "", site => "" );
#        print Dumper(\%curhash), "\n";
        $self->SUPER::value("current", \%curhash);
    }
    return $self;
}

##########################################################################
# query_db: read configuration values from database to use as defaults
sub query_db {
    my $self       = shift;
    my $dbinfo_ref = shift;

    # Create a new DES database connection object:
    my $desdb = new DB::DESUtil(verbose=>0);
    my (@rows, $pkey, $rowref, $col, $val);

    foreach my $d (DEFORDER) {
#        print "Grabbing $d config info from database...\n";
        eval { $pkey = $desdb->getDBTableInfo( $d, \@rows ); };
#        print "\tGot ", scalar(@rows), " rows\n";
        if ( defined($pkey)  && ($pkey =~ /\S/) ) {
#            print "pkey = $pkey\n";
            foreach $rowref (@rows) {
#                print "Need to store ", $$rowref{"$pkey"}, "\n";
                foreach $col ( keys %$rowref ) {
                    $val = $$rowref{"$col"};
                    $col = lc $col;
                    if ($val) {
#                        print "col = $col, value = ", $val, "\n";
                        $dbinfo_ref->{$d}{ $$rowref{$pkey} }{$col} = $val;
                    }
                }
            }
        }
    }
    $desdb->disconnect();
}

##########################################################################

sub exists {
    my ($self, $key, $opt_ref) = @_;
    my ($found, $value) = $self->__search($key, $opt_ref);

    return $found;
}

##########################################################################
sub value {
    my ($self, $key, $arg2) = @_;
    my ($found, $value) = undef;
    if (!defined($arg2) || ((ref($arg2) eq "HASH") && defined($arg2->{'currentvals'}))) {
            ($found, $value) = $self->__search($key, $arg2);
    }
    else {
        ## Only sets value at top level
        $value = $self->SUPER::value($key,$arg2);
    }

    return $value;
}


##########################################################################
# internal function that searches for value in configuration
sub __search {
    my ($self, $key, $opt_ref) = @_;

    my $found = 0;
    my $value = "";

    my %curvals;

#print "************************* Searching for $key **********************************\n";

    # use current values passed into function if given
    if ( defined($opt_ref) && defined($opt_ref->{"currentvals"})) {
        foreach my $k (keys %{$opt_ref->{"currentvals"}}) {
#print "using specified curval $k = ", $opt_ref->{"currentvals"}->{$k}, "\n";
            $curvals{$k} = $opt_ref->{"currentvals"}->{$k};
        }
    }

    # use stored current values for those not passed into function
    my $objcur = $self->obj("current");
    my @ckeys = $self->keys("current");
    foreach my $k (@ckeys) {
        if (!defined($curvals{$k})) {
            $curvals{$k} = $objcur->value($k);
#print "using stored curval $k = ", $objcur->value($k), "\n";
        }
    }

    if (defined($curvals{"$key"})) {
#print "found $key in curvals\n";
        $found = 1;
        $value = $curvals{"$key"};
    }
    elsif (defined($opt_ref->{"searchobj"}) && $opt_ref->{'searchobj'}->exists($key)) {
#print "found $key in searchobj\n";
            $found = 1;
            $value = $opt_ref->{'searchobj'}->value($key);
    }
    else {
        foreach my $d ( DEFORDER ) {
#print "Searching $d for $key\n";
            if ( defined( $curvals{"$d"} ) ) {
                my $defname = $curvals{"$d"};
#print "\tdefname = $defname\n";
                if ( $self->SUPER::exists("$d") ) {
                    my $deftype = $self->obj("$d");
                    if ( $deftype->exists("$defname") ) {
                        my $defobj = $deftype->obj("$defname");
                        if ( $defobj->exists("$key") ) {
                            $found = 1;
                            $value = $defobj->value($key);
                            last;
                        }
                    }
                }
            }
        }
    }
    if (!$found) {
#print "\t$key not found, checking global values\n";
        if ( $self->SUPER::exists($key) ) {
            $found = 1;
            $value = $self->SUPER::value($key);
        }
    }

    if ($found && (lc($key) eq 'ccd')) {
        $value = sprintf("%02d", $value);
    }

    # target_node replaces depricated archive_node
    if (!$found && (lc($key) eq 'target_node')) {
        ($found, $value) = $self->__search('archive_node', $opt_ref);
    }
    if (!$found && (lc($key) eq 'archive_node')) {
        ($found, $value) = $self->__search('target_node', $opt_ref);
    }
    
    return ($found, $value);
}


sub setSubmitInfo {
    my $self = shift;
    my $run="";

    # initialize try count which keeps track of how many times run has been (re)started
    $self->SUPER::value('try_count', 1);    

    # make sure project is all uppercase
    my $project = $self->value("project");
    $project = uc($project);
    $self->SUPER::value("project", $project);

    # to allow both replica_node as well as the correctly-spelled replica_nodes
    if ($self->exists('replica_node') && !$self->exists('replica_nodes')) {
        $self->SUPER::value('replica_nodes', $self->value('replica_node'));
    }

    # class is deprecated
    if ($self->exists('class') && !$self->exists('fileclass')) {
        print "Warning: class is deprecated.  Use fileclass instead\n";
        $self->SUPER::value('fileclass', $self->value('class'));
    }
    my $fileclass = lc($self->value('fileclass'));

    my $deshome = getHome() or die "Error: Can't determine DES_HOME";
    $self->SUPER::value("des_home", $deshome);

    my $submitdir = cwd();
    $self->SUPER::value("submit_dir", $submitdir);
    my $path = "$submitdir/DESJobs";

    my $submit_host = `hostname -f`;
    chomp $submit_host;
    $self->SUPER::value("submit_host", $submit_host);

    my ($submit_time,$submit_epoch);
    if ($self->exists('submit_time')) {   # operator providing submit_time
        $submit_time = $self->value('submit_time');
        my ($year,$mon,$mday,$hour,$min,$sec)= $submit_time =~ /(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/;
        $submit_epoch = timelocal($sec,$min,$hour,$mday,$mon-1,$year-1900); 
    }
    else {
        $submit_epoch = time;
        my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime($submit_epoch);
        $submit_time = sprintf("%d%02d%02d%02d%02d%02d", $year+1900,$mon+1,$mday,$hour,$min,$sec); 
        $self->SUPER::value("submit_time", $submit_time);
    }
    $self->SUPER::value("submit_epoch", $submit_epoch);

    # Create block definitions for simple single module blocks.
    # Also check all blocks in block_list have definitions as well as all modules in their module_lists
    # Also check software_node matches target_node
    if ($self->exists('block_list')) {
        my $block_list = $self->value('block_list');

        # case-insensitive
        $block_list = lc($block_list);
        $self->SUPER::value('block_list', $block_list);

        my @blocks = split /\s*,\s*/, $block_list;
        my $uber_block_obj = $self->obj('block');
        my $uber_module_obj = $self->obj('module');
        foreach my $block (@blocks) {
            if ($uber_block_obj->exists($block)) {
                my $block_obj = $uber_block_obj->obj($block);
                if ($block_obj->exists('module_list')) {
                    my $module_list = $block_obj->value('module_list');

                    # case-insensitive
                    $module_list = lc($module_list);
                    $block_obj->SUPER::value('module_list', $module_list);

                    my @modules = split /\s*,\s*/, $module_list;
                    foreach my $module (@modules) {
                        if (!$uber_module_obj->exists($module)) {
                            print "\tError: Missing definition for module $module from block $block\n";
                            exit $FAILURE;
                        }
                    }
                }
                elsif ($uber_module_obj->exists($block)) {
                    print "\tWarning: Missing module_list definition for block $block\n";
                    print "\t         Defaulting to module_list=$block\n";
                    $block_obj->value('module_list', $block);
                }
                else {
                    print "\tError: Missing module_list definition for block $block\n";
                    exit $FAILURE;
                }
            }
            else {
                if ($uber_module_obj->exists($block)) {
                    print "\tWarning: Missing block definition for $block\n";
                    print "\t         Creating new block definition with module_list=$block\n";
                    $uber_block_obj->value($block, { module_list => $block });
                }
                else {
                    print "\tError: Missing definition for block $block\n";
                    exit $FAILURE;
                }
            }

            # check software_node
            my $block_obj = $uber_block_obj->obj($block);
            my ($target_node, $software_node);
            if ($block_obj->exists('target_node')) {
                $target_node = $block_obj->value('target_node');
            }
            else {
                $target_node = $self->SUPER::value('target_node');
            }

            if ($block_obj->exists('software_node')) {
                $software_node = $block_obj->value('software_node');
            }
            else {
                $software_node = $self->SUPER::value('software_node');
            }

            my ($target_siteid, $software_siteid);
            if ($self->obj('archive')->exists($target_node)) {
                if ($self->obj('archive')->obj($target_node)->exists('site_id')) {
                    $target_siteid = $self->obj('archive')->obj($target_node)->value('site_id');
                }
                else {
                    print "\tError: missing site_id for target node $target_node in block $block\n";
                    print "\nABORTING\n";
                    exit $FAILURE;
                }
            }
            else {
                print "\tError: missing definition for target node $target_node in block $block\n";
                print "\nABORTING\n";
                exit $FAILURE;
            }
            if ($self->obj('software')->exists($software_node)) {
                if ($self->obj('software')->obj($software_node)->exists('site_id')) {
                    $software_siteid = $self->obj('software')->obj($software_node)->value('site_id');
                }
                else {
                    print "\tError: missing site_id for software node $software_node in block $block\n";
                    print "\nABORTING\n";
                    exit $FAILURE;
                }
            }
            else {
                print "\tError: missing definition for software node $software_node in block $block\n";
                print "\nABORTING\n";
                exit $FAILURE;
            }

            if ($software_siteid < 1000) {
                if ($target_siteid != $software_siteid) {
                    print "\tError: software node and target node do not match in block $block\n";
                    printf "\t       software node: %-30s      site_id: %3d\n",$software_node,$software_siteid;
                    printf "\t         target node: %-30s      site_id: %3d\n",$target_node,$target_siteid;
                    print "\nABORTING\n";
                    exit $FAILURE;
                }
            }
            else {   # software def for mult sites that share install 
                my $siteid = $software_siteid - 1000;
                my $site_id2name = $self->value('site_id2name');
                if (defined($site_id2name->{$siteid})) {
                    my $sitenameS = $site_id2name->{$siteid};
                    if (defined($site_id2name->{$target_siteid})) {
                        my $sitenameT = $site_id2name->{$target_siteid};
                    
                        if ($self->obj('site')->exists($sitenameS)) {
                            my $hostS = $self->obj('site')->obj($sitenameS)->value('grid_host');
                            if ($self->obj('site')->exists($sitenameT)) {
                                my $hostT = $self->obj('site')->obj($sitenameT)->value('grid_host');

                                my ($domainS) = $hostS =~ m/[^\.]+\.(.+)/;
                                my ($domainT) = $hostT =~ m/[^\.]+\.(.+)/;

                                if ($domainS ne $domainT) {
                                    print "\tError: software node and target node do not match in block $block\n";
                                    printf "\t       software node: %-30s      site_id: %4d    domain: %-s\n",$software_node,$software_siteid, $domainS;
                                    printf "\t         target node: %-30s      site_id: %4d    domain: %-s\n",$target_node,$target_siteid, $domainT;
                                    print "\nABORTING\n";
                                    exit $FAILURE;
                                }
                            }
                            else {
                                print "\tError: missing definition for site $sitenameT in block $block\n";
                                print "\nABORTING\n";
                                exit $FAILURE;
                            }
                        }
                        else {
                            print "\tError: missing definition for site $sitenameS ($siteid) in block $block\n";
                            print "\nABORTING\n";
                            exit $FAILURE;
                        }
                    }
                    else {
                        print "\tError: missing site for $target_siteid in block $block\n";
                        print "\nABORTING\n";
                        exit $FAILURE;
                    }
                }
                else {
                    print "\tError: missing site for $siteid in block $block\n";
                    print "\nABORTING\n";
                    exit $FAILURE;
                }
            }
        } # end foreach block
    }
    else {
        print "\tError: Must have block_list defined\n";
        exit $FAILURE;
    }

    my $curobj = $self->obj("current");
    $curobj->SUPER::value("job_id", 1);
    $curobj->SUPER::value("block_num", 0);
    $self->setBlockInfo();

    if ($fileclass eq "red") {
        if (!$self->exists('nite')) {
            print "ERROR: missing nite value for fileclass=red.  Cannot create run value.\n";
            print "ABORTING SUBMISSION\n";
            exit $FAILURE;
        }
        my $nite = $self->value('nite');
        $run = $submit_time.'_'.$nite;
    }
    elsif ($fileclass eq "coadd") {
        if (!$self->exists('tilename')) {
            print "ERROR: missing tilename value for fileclass=coadd.  Cannot create run value.\n";
            print "ABORTING SUBMISSION\n";
            exit $FAILURE;
        }
        my $tilename = $self->value("tilename");
        $run =$submit_time."_".$tilename;
    }
    elsif ($fileclass eq "wl") {
        if (!$self->exists('tilename')) {
            print "ERROR: missing tilename value for fileclass=wl.  Cannot create run value.\n";
            print "ABORTING SUBMISSION\n";
            exit $FAILURE;
        }
        my $tilename = $self->value("tilename");
        $run =$submit_time."_".$tilename;
    }
    elsif ($fileclass eq "diff") {
        if (!$self->exists('nite')) {
            print "ERROR: missing nite value for fileclass=diff.  Cannot create run value.\n";
            print "ABORTING SUBMISSION\n";
            exit $FAILURE;
        }
        my $nite = $self->value('nite');
        $run = $submit_time.'_'.$nite;
    }
    else {
        print "Error:  Cannot determine how to create run value.\n";
        print "        Unknown fileclass '$fileclass'\n";
        print "ABORTING SUBMISSION\n";
        exit $FAILURE;
    }

    $self->SUPER::value("run", $run);
    $self->SUPER::value("submit_run", $run);
    my %keys = ('project'=>$project, 'fileclass'=>$fileclass, 'run'=>$run);
    $keys{'filetype'} = 'runtime';
    my $run_dir = getArchiveLocation(\%keys);
    $run_dir = dirname($run_dir);
    $self->SUPER::value("run_dir", $run_dir);

    if (!$self->exists('submit_node')) {
        print "Error: Missing submit_node value\n";
        print "ABORTING SUBMISSION\n";
        exit $FAILURE;
    }
    my $submit_node = $self->value('submit_node');
    if ($self->exists('archive')) {
        if ($self->obj('archive')->exists($submit_node)) {
            my $submit_obj = $self->obj('archive')->obj($submit_node);
            if ($submit_obj->exists('archive_root')) {
                my $work_dir = $submit_obj->value('archive_root');
                $work_dir .= "/".$run_dir;
                $self->SUPER::value("work_dir", $work_dir); 
                my $uberdir = $work_dir."/runtime/uberctrl";
                $self->SUPER::value("uberctrl_dir", $uberdir); 
            }
            else {
                print "Error: Problem finding archive_root for submit_node $submit_node\n";
                print "ABORTING SUBMISSION\n";
                exit $FAILURE;
            }
        } 
        else {
            print "Error: Problem finding information for submit_node $submit_node\n";
            print "ABORTING SUBMISSION\n";
            exit $FAILURE;
        }
    }
    else {
        print "Error: Problem finding archive information\n";
        print "ABORTING SUBMISSION\n";
        exit $FAILURE;
    }
}

sub setBlockInfo {
    my $self = shift;

    my $curobj = $self->obj("current");
     
    my $blocknum = $curobj->value("block_num");
    my $blockname = $self->getBlockName($blocknum); 
    if ($blockname !~ /\S/) {
        print STDERR "Error: setBlockInfo cannot determine block name value for blocknum=$blocknum\n";
        exit $FAILURE;
    }
    $curobj->SUPER::value("block", $blockname);

    if (!$self->exists('target_node')) {
        print STDERR "Error: setBlockInfo cannot determine target_node value\n";
        exit $FAILURE;
    }
    my $targetnode = $self->value("target_node");

    if (!$self->obj('archive')->exists($targetnode)) {
        print STDERR "Error: invalid target_node value ($targetnode)\n";
        print STDERR "ABORTING\n";
        exit $FAILURE;
    }

    $curobj->SUPER::value("archive", $targetnode);

    if ($self->SUPER::exists('listtargets')) {
        my $listt = $self->SUPER::value('listtargets');
        if ($listt !~ /$targetnode/) {
            $self->SUPER::value('listtargets', $listt.','.$targetnode);
        }
    }
    else {
        $self->SUPER::value('listtargets', $targetnode);
    }
    $curobj->SUPER::value("software", $self->value("software_node"));

    if ($self->exists("site_id") && $self->exists('site_id2name')) {
        my $site_id2name = $self->value('site_id2name');
        my $run_site = $site_id2name->{$self->value("site_id")};
        $self->SUPER::value("run_site", $run_site);
        $curobj->SUPER::value("site", $run_site);
    }
    else {
        print STDERR "Error: setBlockInfo cannot determine run_site value\n";
        exit $FAILURE;
    }
}

sub incBlock {
    my $self = shift;
    my $curobj = $self->obj("current");
    my $blocknum = $curobj->value("block_num");
    $blocknum++;
    $curobj->value("block_num", $blocknum);
}

sub resetBlock {
    my $self = shift;
    my $curobj = $self->obj("current");
    my $blocknum = $curobj->value("block_num");
    $curobj->value("block_num", 0);
}

sub incJobID {
    my $self = shift;
    my $inc = shift;

    my $curobj = $self->obj("current");
    my $job_id = $curobj->value("job_id");
    $job_id += $inc;
    $curobj->value("job_id", $job_id);
}

sub interpolate {
    my ($self, $str, $opt_ref) = @_;

    my $maxtries = 1000;
    my $count = 0;
    my $done = 0;
    while ( !$done && ($count < $maxtries)) {
        my $optval;
        my $var;
        if ($str =~ /\$opt\{(\w+)\}/) {
            $var = $1;
            $optval = 1;
        }
        elsif ($str =~ /\$\{(\w+)\}/) {
            $var = $1;
            $optval = 0;
        }
        else {
            $done = 1;
        }

        if (!$done) {
            if ($self->exists($var,$opt_ref)) {
                my $val = $self->value($var,$opt_ref);
                if ($optval) {
                    $str =~ s/\$opt\{$var\}/$val/g;
                }
                else {
                    $str =~ s/\$\{$var\}/$val/g;
                }
            }
            elsif ($self->exists(lc($var),$opt_ref))  {
                my $val = $self->value(lc($var),$opt_ref);
                if ($optval) {
                    $str =~ s/\$opt\{$var\}/$val/g;
                }
                else {
                    $str =~ s/\$\{$var\}/$val/g;
                }
            }
            elsif ($optval) {
                # value for optional value doesn't exist, so replace will empty string
                $str =~ s/\$opt\{$var\}//ig;
            }
            else {
                print STDERR "Error: interpolate function could not find value for $var\n";
                print STDERR "\tCurrent string: '$str'\n";
                exit $FAILURE;
            }
        }
        $count++;
    }
    if ($count >= $maxtries) {
        print STDERR "Error: interpolate function aborting from infinite loop\n";
        print STDERR "\tCurrent string: '$str'\n";
        exit $FAILURE;
    }
    return $str;
}

sub incTryCount {
   my ($self) = @_;
   my $trycnt = $self->value('try_count');
   $trycnt++;
   $self->SUPER::value('try_count', $trycnt); 
}

sub getBlockName {
    my ($self, $blocknum) = @_;
    my $blockname = "";

    my @blockarray =();
    if ($self->is_array("block_array")) {
        @blockarray = $self->array("block_array");
    }
    else {
        @blockarray = ( $self->value("block_array") );
    }

    if ((0 <= $blocknum) && ($blocknum < scalar(@blockarray))) {
        $blockname = $blockarray[$blocknum];
    }
    return $blockname;
}


1;
