########################################################################
#  $Id: provenance.pm 4600 2009-10-20 21:50:23Z mgower $
#
#  $Rev:: 4600                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-10-20 16:50:23 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package Orchestration::provenance;

use strict;
use warnings;

use File::Copy;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;

use Exporter   ();
our (@ISA, @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw(&getProvStr &saveProvLogs &cleanProv);



#################################################################
sub saveProvLogs {
    my ($deshome) = @_;

    if (!defined($deshome)) {
        print "saveProvLogs - Error: undefined deshome\n";
        exit $FAILURE;
    }
    elsif (! -d $deshome) {
        print "saveProvLogs - Error: deshome ($deshome) is not a directory\n";
        exit $FAILURE;
    }
    foreach my $f (("BUILD.log", "ENV.log", "INSTALL.log")) { 
        if (-r "$deshome/$f") {
            copy "$deshome/$f", "DESDM_$f";
        }
        else {
            print "\tsaveProvLogs:  Could not find $deshome/$f\n";
        }
    }

    return 0;
}

sub getProvStr {
    my ($deshome) = @_;
    my ($provstr) = "";


    if (!defined($deshome)) {
        print "getProvStr: undefined deshome\n";
        exit $FAILURE;
    }
    elsif (! -d $deshome) {
        print "getProvStr: deshome ($deshome) is not a directory\n";
        exit $FAILURE;
    }

    if (! -r "$deshome/VERSION") {
        print "getProvStr: Could not find $deshome/VERSION\n";
        exit $FAILURE;
    }
    open FH, "$deshome/VERSION";
    while (my $line=<FH>) {
        $provstr .= $line;
    }
    close FH;
    
    # BASEVERSIONS is just appended to so grab last entry for each subproject
    my $str;
    open FH, "$deshome/BASEVERSIONS";
    while (my $line=<FH>) {
        $str .= $line; 
    }
    close FH;

    $provstr .= cleanProv($str);
    

    # make sure will fit within DB column
    if (length($provstr) > 4000) {  
        print "Warning: shortening provenance string to fit DB column\n";
        $provstr = substr($provstr,0,4000);
    }

    return $provstr;
}

sub cleanProv {
    my ($str) = @_;
    my $provstr = "";

    # BASEVERSIONS is just appended to so grab last entry for each subproject
    my %prov = ();
    foreach my $line (split /\n/,$str) {
        chomp($line);
        if ($line =~ m/([^\/]+)\/(\S+:.+)$/) {
            my ($subp, $vers) = ($1,$2);
            $prov{$subp} = $vers;
        }
        else {
            print "Unrecognized BASEVERSIONS line\n\t$line\n";
        }
    }

    foreach my $k (sort keys %prov) {
        my $v = $prov{$k};
        $provstr .= "$k$v\n";
    }

    return $provstr;
}




1;
