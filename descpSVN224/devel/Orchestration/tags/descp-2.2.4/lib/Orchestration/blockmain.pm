#!/usr/bin/env perl
########################################################################
#  $Id: blockmain.pl 2925 2009-01-16 01:02:10Z mgower $
#
#  $Rev:: 2925                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-01-15 19:02:10 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
package Orchestration::blockmain;

use strict;
use warnings;

use Exporter   ();
our (@ISA, @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw(&assignFullnames &assignWork &calcNumJobs &calcWall 
                &createElfFiles &createFullnames &createJobDirs 
                &createPipelinesMngrDAG &createStageArchiveList 
                &createSublists &getFileArray &getLineArray &getListsAndFiles 
                &getSearchObjPastRun &getValuesLine &outputFullList 
                &outputIndependentLists &outputJobInputLists &outputLine 
                &parseStageOutput &printValue &processDivideBy &processLoops 
                &readXML &saveJobInfo &savePastRun &calcNumTargetJobs);


use List::Util qw(min max);
use POSIX qw(floor ceil);
use Cwd;
use Data::Dumper;
use File::Basename;
use File::Path;
use Storable qw(dclone);
use XML::Simple;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::submitfunc;
use Orchestration::ogrescript;
use DAF::Archive::Tools qw(getArchiveLocation);

#####################################################################
### Read master lists and files from XML files created earlier 
sub readXML {
    my ($Config) = @_;

    print "readXML: BEG\n";
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my %modules_prev_in_list = ();
    foreach my $module (@module_array) {
        if (! $uber_module_obj->exists($module)) {
            print "Error: Could not find module description for module $module\n";
            exit $FAILURE;
        }
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);
        my $uber_list_obj = $module_obj->obj('list');
        my $num_jobs = 0;
        my @listorder;
        if ($module_obj->exists('list_order')) {
            @listorder = split /\s*,\s*/, $module_obj->value('list_order');
        }
        else {
            @listorder = $uber_list_obj->keys();
        }
        if (scalar(@listorder) == 0) {
            print "\t\tNo lists\n";
        }
        foreach my $listname (@listorder) {
            my $list_obj = $uber_list_obj->obj($listname);
            if (!$list_obj->exists('depends') || 
                !defined($modules_prev_in_list{ $list_obj->value('depends') }) ) {

                print "\t\t$listname: creating master list\n";
                my $outputxml = "${module}_${listname}.xml";
                # read xml file
                print "\t\t\tParsing XML file - start ", time, "\n";
                my $master = XMLin($outputxml, 'KeyAttr' => []);
                print "\t\t\tParsing XML file - end ", time, "\n";
                
                my $numlines = scalar(@{getLineArray($master)});
                print "\t\t\tNumber of lines in list $listname: $numlines\n";

                if ($numlines == 0) {
                    print "ERROR: 0 lines in list $listname in module $module\n";
                    exit $FAILURE;
                }

                $list_obj->value('master', $master);
                $list_obj->value('depends', 0);
            }
            else {
                $list_obj->value('depends', 1);
            }
        }

        # process each "file" in each module
        my $uber_file_obj = $module_obj->obj('file');
        if (scalar($uber_file_obj->keys()) == 0) {
            print "\t\tNo files\n";
        }
        foreach my $file_name ($uber_file_obj->keys()) {
            my $file_obj = $uber_file_obj->obj($file_name);
        
#print "module = $module\n";
#print "file_name = $file_name\n";
#print "depends = ", $file_obj->value('depends'), "\n" if $file_obj->exists('depends');
#print "modules_prev_in_list = ";
#foreach my $m (keys %modules_prev_in_list) {
#    print "$m   ";
#}
#print "\n";

            if (!$file_obj->exists('depends') || 
                !defined($modules_prev_in_list{ $file_obj->value('depends') }) ) {

                print "\t$module-$file_name: creating master list\n";
                my $outputxml = "${module}_${file_name}.xml";
                # read xml file
                print "\t\tParsing XML file - start ", time, "\n";
                my $master = XMLin($outputxml, 'KeyAttr' => []);
                print "\t\tParsing XML file - end ", time, "\n";
                
                my $numlines = scalar(@{getLineArray($master)});
                print "\t$module-$file_name: number of files found: $numlines\n";

                if ($numlines == 0) {
                    print "ERROR: 0 files for $file_name in module $module\n";
                    exit $FAILURE;
                }

                $file_obj->value('master', $master);
                $file_obj->value('depends', 0);
            }
            else {
                $file_obj->value('depends', 1);
            }
        }
        $modules_prev_in_list{$module} = 1;
    }
    print "readXML: END\n\n";
} ## end readXML

#####################################################################
### Get master lists and files calling external codes when needed
sub getListsAndFiles {
    my ($Config) = @_;

    print "getListsAndFiles: BEG\n";
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my %modules_prev_in_list = ();
    foreach my $module (@module_array) {
        if (! $uber_module_obj->exists($module)) {
            print "Error: Could not find module description for module $module\n";
            exit $FAILURE;
        }
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);
        my $uber_list_obj = $module_obj->obj('list');
        my $num_jobs = 0;
        my @listorder;
        if ($module_obj->exists('list_order')) {
            @listorder = split /\s*,\s*/, $module_obj->value('list_order');
        }
        else {
            @listorder = $uber_list_obj->keys();
        }
        foreach my $listname (@listorder) {
            my $list_obj = $uber_list_obj->obj($listname);
            if (!$list_obj->exists('depends') || 
                !defined($modules_prev_in_list{ $list_obj->value('depends') }) ) {

                print "\t$module-$listname: creating master list\n";
                my $outputxml = "${module}_${listname}.xml";
                # read xml file
                print "\t\tParsing XML file - start ", time, "\n";
                my $master = XMLin($outputxml, 'KeyAttr' => []);
                print "\t\tParsing XML file - end ", time, "\n";

                
                my $numlines = scalar(@{getLineArray($master)});
                print "\t$module-$listname: number of lines in list $listname: $numlines\n";

                if ($numlines == 0) {
                    print "ERROR: 0 lines in list $listname in module $module\n";
                    exit $FAILURE;
                }

                $list_obj->value('master', $master);
                $list_obj->value('depends', 0);
            }
            else {
                $list_obj->value('depends', 1);
            }
        }

        # process each "file" in each module
        my $uber_file_obj = $module_obj->obj('file');
        foreach my $file_name ($uber_file_obj->keys()) {
            my $file_obj = $uber_file_obj->obj($file_name);
        
#print "module = $module\n";
#print "file_name = $file_name\n";
#print "depends = ", $file_obj->value('depends'), "\n" if $file_obj->exists('depends');
#print "modules_prev_in_list = ";
#foreach my $m (keys %modules_prev_in_list) {
#    print "$m   ";
#}
#print "\n";

            if (!$file_obj->exists('depends') || 
                !defined($modules_prev_in_list{ $file_obj->value('depends') }) ) {

                print "\t$module-$file_name: creating master list\n";
                my $outputxml = "${module}_${file_name}.xml";
                # read xml file
                print "\t\tParsing XML file - start ", time, "\n";
                my $master = XMLin($outputxml, 'KeyAttr' => []);
                print "\t\tParsing XML file - end ", time, "\n";

                
                my $numlines = scalar(@{getLineArray($master)});
                print "\t$module-$file_name: number of files found: $numlines\n";

                if ($numlines == 0) {
                    print "ERROR: 0 files for $file_name in module $module\n";
                    exit $FAILURE;
                }

                $file_obj->value('master', $master);
                $file_obj->value('depends', 0);
            }
            else {
                $file_obj->value('depends', 1);
            }
        }
        $modules_prev_in_list{$module} = 1;
    }
    print "readXML: END\n\n";
} ## end readXML


#####################################################################
sub processLoops {
    my ($Config) = @_;

    print "processLoops: BEG\n";

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    foreach my $module (@module_array) {
        if (! $uber_module_obj->exists($module)) {
            print "Error: Could not find module description for module $module\n";
            exit $FAILURE;
        }
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);
        my $uber_list_obj = $module_obj->obj('list');
        my $num_jobs = 0;
        foreach my $listname ($uber_list_obj->keys()) {
            my $list_obj = $uber_list_obj->obj($listname);
            if ($module_obj->exists('loop')) {
                my $loop = lc($module_obj->value('loop'));
                $module_obj->value('loop', $loop);

                # clean up divide_by, remove loop if there
                if ($list_obj->exists('divide_by')) {
                    my $divide_by = lc($list_obj->value('divide_by'));
                    $divide_by =~ s/\Q$loop\E//;
                    $divide_by =~ s/\+\s*\+/+/;
                    $divide_by =~ s/\+\s*$//;
                    $divide_by =~ s/^\s*\+//;
                    $list_obj->value('divide_by', $divide_by);
                }

                # save loop values and divide master list by loop value
                my %sublists=();
                my $master = $list_obj->value('master');
                my %loopvals=();
                
                foreach my $line (@{getLineArray($master)}) {
                    my $lval = getValuesLine($line, $loop, undef, undef);
                    if (!defined($lval)) {
                        print "Couldn't find '$loop' loop value for line\n";
                        print Dumper($line),"\n";
                        exit $FAILURE;
                    }
                    elsif (UNIVERSAL::isa( $lval, "ARRAY")) {
                        if (scalar(@$lval) > 1) {
                            print "Multiple loop values ($loop) for same line\n";
                            print "line: ", Dumper($line),"\n";
                            print "values: ", Dumper($lval),"\n";
                            exit $FAILURE;
                        }
                        else {
                            $lval = $$lval[0];
                        }
                    }

                    # Make sure remove beginning and trailing spaces
                    $lval =~ s/^\s+//;
                    $lval =~ s/\s+$//;

                    $loopvals{$lval} = 1;
                    if (!defined($sublists{$lval})) {
                        $sublists{$lval} = ();
                    }
                    push(@{$sublists{$lval}->{'line'}},$line);
                }

                $list_obj->value('sublists', \%sublists);
                $list_obj->delete('master');
                $module_obj->value('loopvals', \%loopvals);
            }
        }
    }

    # create a copy module for each loop value
    my %fixmodules = ();
    my $modified_module_list = 0;
    foreach my $module (@module_array) {
        my $module_obj = $uber_module_obj->obj($module);

        if ($module_obj->exists('loop')) {
            my $module_str = "";
            my $loop = $module_obj->value('loop');
            my $loopvals = $module_obj->value('loopvals');
            $module_obj->value('loopvals', undef);
            $module_obj->delete('loopvals');


            # Deep copy doesn't clone LibXML objects
            my %sublists = ();
            my $uber_list_obj = $module_obj->obj('list');
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);
                if ( $list_obj->exists('sublists') ) {
                    $sublists{$listname} = $list_obj->value('sublists');
                    $list_obj->value('sublists', undef);
                    $list_obj->delete('sublists');
                }
            }

            my %files = ();
            my $uber_file_obj = $module_obj->obj('file');
            foreach my $filename ($uber_file_obj->keys()) {
                my $file_obj = $uber_file_obj->obj($filename);
                if ( $file_obj->exists('master') ) {
                    $files{$filename} = $file_obj->value('master');
                    $file_obj->value('master', undef);
                    $file_obj->delete('master');
                }
            }

            ## Copy the orig module assigning corresponding sublist
            foreach my $key (keys %$loopvals) {
                my $new_mod_name = $module."_";
  
                # since module names will be case-insensitive, rewrite key so it handles both upper and lowercase differently
                while ($key =~ m/(.)/g) { # . is never a newline here
                    my $char = $1;
                    if ($char =~ /[A-Z]/) {
                        $new_mod_name .= "cap". lc($char);
                    }
                    else {
                        $new_mod_name .= $char;
                    }
                }
                # guarantee uniqueness
                while ($uber_module_obj->exists($new_mod_name)) {
                    $new_mod_name .= '_';
                }
                print "\tcreating module $new_mod_name with $loop=$key\n";

                # copy the orig module
                my $new_module = dclone($module_obj);

                $new_module->value($loop, $key);
                $new_module->value('loopval', $key);
                my $new_uber_list_obj = $new_module->obj('list');
                foreach my $listname ($new_uber_list_obj->keys()) {
                    my $list_obj = $new_uber_list_obj->obj($listname);
                    if (!defined($sublists{$listname}{$key})){
                        print "Error: Missing sublist for key $key in list $listname\n";
                        exit $FAILURE;
                    }
                    $list_obj->value('master', $sublists{$listname}{$key});
                    my $filename = "";
                    if ($list_obj->exists('basename')) {
                        $filename = $list_obj->value('basename');
                    }
                    else {
                        $filename = $listname;
                    }
                    $filename .= "_".$key;
                    $list_obj->value('basename', $filename);
                }
                my $new_uber_file_obj = $new_module->obj('file');
                foreach my $filename ($new_uber_file_obj->keys()) {
                    my $file_obj = $new_uber_file_obj->obj($filename);
                    if (defined($files{$filename})){
                        $file_obj->value('master', $files{$filename});
                    }
                }

                my %obj_href = $new_module->getall();
                $uber_module_obj->value($new_mod_name,\%obj_href);
                $module_str .= $new_mod_name.",";
                $modified_module_list = 1;
            }
            $fixmodules{$module} = $module_str;
            $uber_module_obj->value($module, undef);
            $uber_module_obj->delete($module);
        }
    }

    # clean up new module list if exists and save in config
    if ($modified_module_list) {
        $module_list = $Config->value('module_list');
        while (my ($m,$nl) = each %fixmodules) {
            $nl =~ s/,$//;
            $module_list =~ s/$m/$nl/;
        }
        $module_list =~ s/,,/,/g;
        @module_array = split /\s*,\s*/, $module_list;
        my $block = $Config->value('block');

        $Config->obj('block')->obj($block)->value('module_list', $module_list);
    }

    print "processLoops: END\n\n";
} ##  END processLoops


sub createStageArchiveList {
    my ($Config) = @_;

    print "createStageArchiveList: BEG\n";
    my $stagefile = "stagearchive.list";

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my %stageids;
    foreach my $module (@module_array) {
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);

        if ($module_obj->exists('list')) {
            my $uber_list_obj = $module_obj->obj('list');
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);

                # if supposed to stage files
                if ($list_obj->exists('master')) {
                    my $master = $list_obj->value('master');
                    if ((!$Config->exists('notarget') || !$Config->value('notarget')) && (!$list_obj->exists("stage_files") || $list_obj->value("stage_files")))  {
                        foreach my $line (@{getLineArray($master)}) {
                            my $filearr = getFileArray($line);
                            foreach my $file (@$filearr) {
                                $stageids{$file->{'fileid'}} = 1;
                            }
                        }
                    }
                    else {
                        my $archive_root = $Config->value('archive_root');
                        foreach my $line (@{getLineArray($master)}) {
                            my $filearr = getFileArray($line);
                            foreach my $file (@$filearr) {
                                my $archive_path = "";
                                eval { $archive_path = getArchiveLocation($file); };
                                if ($@) {
                                    # There was an error in the eval {}
                                    print "File missing data needed by getArchiveLocation\n";
                                    print "\t$@\n";
                                    print Dumper($file),"\n";
                                    exit $FAILURE;
                                }
                                $file->{'localpath'} = $archive_root.'/'.$archive_path;
                                $file->{'localfilename'} = $file->{'filename'};
                                $file->{'fileid'} = $file->{'id'};
                                $file->{'fullname'} = $file->{'localpath'}.'/'.$file->{'filename'};
                            }
                        }
                    }
                }
            }
        }

        # process each "file" in each module
        if ($module_obj->exists('file')) {
            my $uber_file_obj = $module_obj->obj('file');
            foreach my $file_name ($uber_file_obj->keys()) {
                my $file_obj = $uber_file_obj->obj($file_name);
                # if supposed to stage files
                if ($file_obj->exists('master')) {
                    my $master = $file_obj->value('master');
                    if ((!$Config->exists('notarget') || !$Config->value('notarget')) && (!$file_obj->exists("stage_files") || $file_obj->value("stage_files")))  {
                        foreach my $line (@{getLineArray($master)}) {
                            my $filearr = getFileArray($line);
                            foreach my $file (@$filearr) {
                                $stageids{$file->{'fileid'}} = 1;
                            }
                        }
                    }
                    else {
                        my $archive_root = $Config->value('archive_root');
                        foreach my $line (@{getLineArray($master)}) {
                            my $filearr = getFileArray($line);
                            foreach my $file (@$filearr) {
                                my $archive_path = getArchiveLocation($file);
                                $file->{'localpath'} = $archive_root.'/'.$archive_path;
                                $file->{'localfilename'} = $file->{'filename'};
                                $file->{'fileid'} = $file->{'id'};
                                $file->{'fullname'} = $file->{'localpath'}.'/'.$file->{'filename'};
                            }
                        }
                    }
                }
            }
        }
    }

    # create empty file if no files to stage so arcp will not abort
    open SF, "> $stagefile";
    if (scalar(keys %stageids)) {
        foreach my $id (keys %stageids) {
            print SF "<file>\n";
            print SF "fileid = ", $id, "\n"; 
            print SF "</file>\n";
        }
    }
    else {
        print "\tNo files to stage\n";
    }
    close SF;

    print "createStageArchiveList: END\n\n";
}  ## end createStageArchiveList



## break into sublists based upon divide_by
sub processDivideBy {
    my ($Config) = @_;

    print "processDivideBy: BEG\n";
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    foreach my $module (@module_array) {
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);

        my $uber_list_obj = $module_obj->obj('list');
        my $num_jobs = 0;
        foreach my $listname ($uber_list_obj->keys()) {
            my $list_obj = $uber_list_obj->obj($listname);

            if ($list_obj->exists('master')) {
                my $master=$list_obj->value('master');
                if (!defined($master)) {
                    print STDERR "Error: master list doesn't exist in list $listname in module $module\n";
                    exit $FAILURE;
                }
                my $numlines = scalar(@{getLineArray($master)});
                print "\t$module-$listname: number of lines in master: ", $numlines, "\n";
                if ($numlines == 0) {
                    print STDERR "Error: 0 lines in master list.\n";
                    exit $FAILURE;
                }


                my %sublists=();
                if ($list_obj->exists('divide_by')) {
                    my $divide_by = lc($list_obj->value('divide_by'));
                    print "\t$module-$listname: dividing by $divide_by\n";
                    my @divide_by = split /\s*\+\s*/, $divide_by;
                    foreach my $line (@{getLineArray($master)}) {
                        my $filearr = getFileArray($line);
                        my $key = "";
                        foreach my $divb (@divide_by) {
                            my $val = getValuesLine($line, $divb, 1, undef);
                            $val =~ s/^\s*(\S*(?:\s+\S+)*)\s*$/$1/;
                            $key .= $val."_";
                        }
                        if (!defined($sublists{$key})) {
                            $sublists{$key}->{'line'} = ();
                        }
                        push(@{$sublists{$key}->{'line'}}, $line);
                    }
                }
                else {
                    $sublists{'onlyone'} = $master;
                    $list_obj->value('master', undef);
                    my $numlines2 = scalar(@{getLineArray($sublists{'onlyone'})});
                    print "\t$module-$listname: onlyone list: size ", $numlines2, "\n";
                }

                $list_obj->delete('master');
                $list_obj->value('sublists',\%sublists);
                print "\t$module-$listname: number of sublists = ", scalar(keys %sublists), "\n";
            }
            else {
                print "\t$module-$listname: no masterlist for $listname....skipping\n";
            }
        } # end foreach list
    } # end foreach module
    print "processDivideBy: END\n\n";
} ## end processDivideBy


#####################################################################
## figure out number of jobs
sub calcNumJobs {
    my ($Config) = @_;

    print "calcNumJobs: BEG\n";
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my %max_num_jobs = ();
    foreach my $module (@module_array) {
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);
        my $num_jobs = 0;

        if ($module_obj->exists('num_jobs')) {
            $max_num_jobs{$module} = $module_obj->value('num_jobs');
        } 
        else {
            my $uber_list_obj = $module_obj->obj('list');
            my $list_num_jobs = 99999;
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);
        
                if (!$list_obj->exists('jobdependent') || $list_obj->value('jobdependent')) {
                    if ($list_obj->exists('sublists')) {
                        my $sublists=$list_obj->value('sublists');
                
                        if ($list_obj->exists('min_num_per_job')) {
                            my $min_num_per_job = $list_obj->value('min_num_per_job');
                            print "\t$module-$listname: min_num_per_job = $min_num_per_job\n";
                            foreach my $listkey (keys %$sublists) {
                                my $numlines = scalar(@{getLineArray($sublists->{$listkey})});

                                my $floor = floor($numlines/$min_num_per_job);
                                if ($floor == 0) {
                                    $floor = 1;
                                }
                                print "\t$module-$listname: numlines = $numlines, jobincrease = $floor\n";
                      
                                $num_jobs += $floor;
                            }
                        }
                        else {
                            $num_jobs += scalar(keys %$sublists);
                        }
                        print "\t$module-$listname: num_jobs = $num_jobs\n";
                        if ($num_jobs < $list_num_jobs) {
                            $list_num_jobs = $num_jobs;
                        }
                    }
                    else {
                        print "\t$module-$listname: no sublists\n";
                    }
                }
            }
            if ($list_num_jobs == 99999) {
                $list_num_jobs = 0;
            }
            $max_num_jobs{$module} = $list_num_jobs;
        }
    }

    my $num_jobs = 99999;
    while ( my ($mod, $max) = each(%max_num_jobs) ) {
        if (($max != 0) && ($max < $num_jobs)) {
            $num_jobs = $max;
        }
    }

    if ($num_jobs == 99999) {
        $num_jobs = 1;
    }

    print "\tCalculated number of jobs = $num_jobs\n";
    if ($num_jobs == 0) {
        print "Error: num_jobs = 0\n";
        exit $FAILURE;
    }

    if ($Config->exists('num_jobs')) {
        my $block_num_jobs = $Config->value('num_jobs');
        print "\tNumber of jobs set in block = $block_num_jobs\n";
        if ($num_jobs <= $block_num_jobs) {
            $num_jobs = $block_num_jobs; 
            print "\tNew number of jobs = $num_jobs\n";
        }
    }

    print "calcNumJobs: END\n\n";
    return $num_jobs;
}
    

#####################################################################
## Divide work into smaller jobs if needed
sub createSublists {
    my ($Config, $numjobs) = @_;

    print "createSublists: BEG\n";
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    foreach my $module (@module_array) {
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);

        my $uber_list_obj = $module_obj->obj('list');
        foreach my $listname ($uber_list_obj->keys()) {
            my $list_obj = $uber_list_obj->obj($listname);

            if (!$list_obj->exists('jobdependent') || $list_obj->value('jobdependent')) {
        
                if ($list_obj->exists('sublists')) {
                    my $sublists = $list_obj->value('sublists');
                    my $num_sublists = scalar(keys %$sublists);
                    if ( $numjobs < $num_sublists ) {
                        print STDERR "Error: In blockmain's createSublists the number of sublists ",$num_sublists,
                                    " is greater than the number of jobs ($numjobs)\n";
                        exit $FAILURE;
                    }
                    my $divide_list_into = floor($numjobs/$num_sublists);
                    print "\t$module-$listname: divide_list_into = $divide_list_into\n";
                    my %new_sublists = ();
                    while (my ($sublist_key, $sublist) = each(%$sublists)) {
                        my @list = ();
                        my $cnt = 0;
                        foreach my $line (@{getLineArray($sublist)}) {
                            my $j = $cnt % $divide_list_into;
                            push(@{$list[$j]->{'line'}},$line);
                            $cnt++;
                        }
                        $new_sublists{$sublist_key} = \@list;
                    }
                    $list_obj->value('sublists', \%new_sublists);
                }
            }
        }
    }
    print "createSublists: END\n\n";
} ## end createSublists



#####################################################################
####  Now have appropriate sublists and files, assign work to jobs
sub assignWork {
    my ($Config, $numjobs) = @_;

    print "assignWork: BEG\n";

    my $block = $Config->value('block');
    my $block_obj = $Config->obj('block')->obj($block);

    my $archive_root = $Config->value('archive_root');
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my %jobinfo = ();
    my $jobnum = $Config->value('job_id');
    $block_obj->value('start_jobid', $jobnum);

    my %key2job = ();
    foreach my $module (@module_array) {
        print "\tModule $module\n";
        my $module_obj = $uber_module_obj->obj($module);

        my $uber_list_obj = $module_obj->obj('list');
        foreach my $listname ($uber_list_obj->keys()) {
            my $list_obj = $uber_list_obj->obj($listname);
            if (!$list_obj->exists('jobdependent') || $list_obj->value('jobdependent')) {
                my $list_basename = "";
                if ($list_obj->exists('basename')) {
                    $list_basename = $list_obj->value('basename');
                }
                else {
                    $list_basename = $listname;
                }
        
                if ($list_obj->exists('sublists')) {
                    my $sublists = $list_obj->value('sublists');
                    my $num_sublists = scalar(keys %$sublists);
                    print "$listname has $num_sublists sublists\n";
                    foreach my $sublist_key (sort keys %$sublists) {
                        print "\tsublist key $sublist_key\n";
                        my $sublist_aref = $sublists->{$sublist_key};
                        if (exists($key2job{$sublist_key})) {
                            my $i = scalar(@{$key2job{$sublist_key}}) - 1;
                            while (scalar(@$sublist_aref) > scalar(@{$key2job{$sublist_key}})) {
                                my $jobidstr = sprintf("%04d", $jobnum);
                                my $list_filename = $list_basename."_".$jobidstr.".list";
                                $jobinfo{$jobidstr}{'module'}{$module}{'list'}{$listname}{'filename'} = $list_filename;
                                $jobinfo{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'} = $sublist_aref->[$i];

                                # save values for divide_by keys to get written to properties file
                                if ($list_obj->exists('divide_by')) { 
                                    my @keys = split /\s*\+\s*/,$list_obj->value('divide_by');
                                    my $firstline = @{getLineArray($sublist_aref->[$i])}[0];
                                    foreach my $k (@keys) {
                                        $jobinfo{$jobidstr}{'module'}{$module}{$k} = getValuesLine($firstline, $k, 1, undef);
                                        $jobinfo{$jobidstr}{$k} = $jobinfo{$jobidstr}{'module'}{$module}{$k};
                                    }
                                }
                                push (@{$key2job{$sublist_key}}, $jobidstr);
                                $jobnum++;
                                $i++;
                            }
                            for (my $i=0; $i < scalar(@$sublist_aref); $i++) {
                                my $jobidstr = sprintf("%04d", $key2job{$sublist_key}->[$i]);
                                my $list_filename = $list_basename."_".$jobidstr.".list";
                                $jobinfo{$jobidstr}{'module'}{$module}{'list'}{$listname}{'filename'} = $list_filename;
                                $jobinfo{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'} = $sublist_aref->[$i];
                                if ($list_obj->exists('divide_by')) { 
                                    my @keys = split /\s*\+\s*/,$list_obj->value('divide_by');
                                    my $firstline = @{getLineArray($sublist_aref->[$i])}[0];
                                    foreach my $k (@keys) {
                                        $jobinfo{$jobidstr}{'module'}{$module}{$k} = getValuesLine($firstline, $k, 1, undef);
                                        $jobinfo{$jobidstr}{$k} = $jobinfo{$jobidstr}{'module'}{$module}{$k};
                                    }
                                }
                            }
                        }
                        else {
                            for (my $i=0; $i < scalar(@$sublist_aref); $i++) {
                                my $jobidstr = sprintf("%04d", $jobnum);
                                my $list_filename = $list_basename."_".$jobidstr.".list";
                                $jobinfo{$jobidstr}{'module'}{$module}{'list'}{$listname}{'filename'} = $list_filename;
                                $jobinfo{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'} = $sublist_aref->[$i];
                                if ($list_obj->exists('divide_by')) { 
                                    my @keys = split /\s*\+\s*/,$list_obj->value('divide_by');
                                    my $firstline = @{getLineArray($sublist_aref->[$i])}[0];
                                    foreach my $k (@keys) {
                                        $jobinfo{$jobidstr}{'module'}{$module}{$k} = getValuesLine($firstline, $k, 1, undef);
                                        $jobinfo{$jobidstr}{$k} = $jobinfo{$jobidstr}{'module'}{$module}{$k};
                                    }
                                }
                                push (@{$key2job{$sublist_key}}, $jobnum);
                                $jobnum++;
                            }
                        }
                    }
                }
                $list_obj->delete('sublists');
            }
        }
    }

    foreach my $jobid (keys %jobinfo) {
        my $jobidstr = sprintf("%04d", $jobid);

        foreach my $module (@module_array) {
            print "\tModule $module\n";
            my $module_obj = $uber_module_obj->obj($module);

            # process each "file" in each module
            my $uber_file_obj = $module_obj->obj('file');
            foreach my $file_name ($uber_file_obj->keys()) {
                my $file_obj = $uber_file_obj->obj($file_name);

                
                if (!$file_obj->exists('jobdependent') || $file_obj->value('jobdependent')) {
                    if ((!$file_obj->exists('depends') || !$file_obj->value('depends')) && $file_obj->exists('master')) {
                        my $master = $file_obj->value('master');
                        if ($file_obj->exists('match')) {

                            my @matchkeys = split /\s*\+\s*/,$file_obj->value('match');
                    
                            # make key string for each file in list (i.e. all bpm files)
                            my %key2file = ();
                            foreach my $line (@{getLineArray($master)}) {
                                my $key = "";
                                foreach my $k (@matchkeys) {
                                    my $val = getValuesLine($line, $k, 1, undef);
                                    if ($val !~ /\S/) {
                                        print "\t$module-$file_name: Warning: Could not find key value $k for matching file\n";
                                    }
                                    else {
                                        $val =~ s/^\s*(\S*(?:\s+\S+)*)\s*$/$1/;
                                        $key .= $val."_";    
                                    }
                                }
                                $key2file{$key} = $line;
                            }

                            # make key string for this job
                            my $key = "";
                            foreach my $k (@matchkeys) {
                                if (exists($jobinfo{$jobidstr}{'module'}{$module}{$k})) {
                                    $key .= $jobinfo{$jobidstr}{'module'}{$module}{$k}."_";    
                                }
                                elsif (exists($jobinfo{$jobidstr}{$k})) {
                                    $key .= $jobinfo{$jobidstr}{$k}."_";    
                                }
                                else {
                                    print "Error: key $k doesn't exist for job $jobidstr (module $module)\n";
                                    print "jobinfo for module $module: ", Dumper(\%{$jobinfo{$jobidstr}{'module'}{$module}}), "\n";
                                    exit $FAILURE;
                                }
                            }
#                           print "\t$jobidstr-$module-$file_name: key for this job = $key\n";

                            # match job's key string to a file's key string to select file
                            if (exists($key2file{$key})) {
                                my $line = $key2file{$key};
                                my $fullname = getValuesLine($line, 'fullname', 1, undef);
                                if ($fullname !~ /\S/) {
                                    print "Error: Missing fullname for file\n";
                                    print Dumper($line),"\n";
                                    exit $FAILURE;
                                }
                                $jobinfo{$jobidstr}{'module'}{$module}{'file'}{$file_name}=$fullname;
                            }
                            else {
                                print "ERROR: Could not find match on $key for job $jobidstr for filename $file_name\n";
                                exit $FAILURE;
                            }
                        }
                        else {
                            if (scalar(@{getLineArray($master)}) > 1) {
                                print "Error: blockmain found more than one valid DB entry for file $file_name\n";
                                exit $FAILURE;
                            }
                    
                            else {
                                my $firstline = @{getLineArray($master)}[0];
                                my $fullname = getValuesLine($firstline, 'fullname', 1, undef);
                                if ($fullname !~ /\S/) {
                                    print "Error: Missing fullname for file\n";
                                    print Dumper($firstline),"\n";
                                    exit $FAILURE;
                                }
                                $jobinfo{$jobidstr}{'module'}{$module}{'file'}{$file_name}=$fullname;
                            }
                        }
                    }
                    else {
                        print "\t$jobidstr-$module-$file_name: no master list, defaulting name\n";

                        # Create input filename and output to jobinfo file
                        my $fileclass = $file_obj->value("fileclass");
                        my $filetype = $file_obj->value("filetype");
                        my $filename = $file_obj->value("filename");
                        my $run = $Config->value("submit_run");
                        my $project = $Config->value("project");
                        my $archive_path = getArchiveLocation({'project'=>$project, 'run'=>$run, 
                                                               'fileclass'=>$fileclass, 'filetype'=>$filetype}); 
                        $jobinfo{$jobidstr}{'module'}{$module}{'file'}{$file_name}=$archive_root.'/'.$archive_path.'/'.$filename;
                    }
                }
            }
        }
    }
    if (scalar(keys %jobinfo) == 0) {
        print "\tassignWork: 0 jobs due to lists, so defaulting to $numjobs job(s)\n";
        for (my $i = 0; $i < $numjobs; $i++) {
            my $jobidstr = sprintf("%04d", $jobnum);
            $jobinfo{$jobidstr}{'nolists'} = 1;
            $jobnum++;
        }
    }

    $block_obj->value('end_jobid', $jobnum-1);
    print "assignWork: END\n\n";

    return \%jobinfo;
} ## end assignWork

#####################################################################
sub outputIndependentLists {
    my ($Config) = @_;

    print "outputIndependentLists: BEG\n";

    my $workdir = $Config->value('work_dir');
    my $project = $Config->value('project');
    my $run = $Config->value('submit_run');
    my $fileclass = $Config->value('fileclass');

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    # create directory in which to store lists
    if (! -d "$workdir/aux") {
        print "Making aux directory...";
        eval { mkpath("$workdir/aux") };
        if ($@) {
            print "Error\nCouldn’t create $workdir/aux: $@\n";
            exit $FAILURE;
        }
        else {
            print "done\n";
        }
    }

    foreach my $module (@module_array) {
        my $module_obj = $uber_module_obj->obj($module);

        my $uber_list_obj = $module_obj->obj('list');
        foreach my $listname ($uber_list_obj->keys()) {
            my $list_obj = $uber_list_obj->obj($listname);

            # output lists of files
            if ($list_obj->exists('jobdependent') && !$list_obj->value('jobdependent') && 
                $list_obj->exists('sublists')) {

                my $format = "textsp";
                if ($list_obj->exists("format")) {
                    $format = $list_obj->value("format");
                }
                my $output_fields;
                if ($format =~ m/config/i) {
                    $output_fields = "localpath,localfilename,fileid";
                }
                else {
                    $output_fields = "fullname";
                }
                if ($list_obj->exists("output_fields")) {
                    $output_fields = lc($list_obj->value("output_fields"));
                }
                my @keyarr = split /\s*,\s*/, $output_fields;

                my $FH;
                foreach my $list (values %{$list_obj->value('sublists')}) {
                    my $currentvals = {};
                    if ($list_obj->exists('divide_by')) { 
                        my @keys = split /\s*\+\s*/,$list_obj->value('divide_by');
                        my $firstline = @{getLineArray($list)}[0];
                        foreach my $k (@keys) {
                            $currentvals->{$k} = getValuesLine($firstline, $k, 1, undef);
                        }
                    }
                    my $list_filename = "";
                    if ($list_obj->exists('filename')) {
                        $list_filename = $list_obj->value('filename');
                    }
                    else {
                        $list_filename = "${module}_$listname.list";
                    }
                    $list_filename = $Config->interpolate($list_filename,{'currentvals' => $currentvals}); 
                    my $osuccess = open $FH, "> $workdir/aux/$list_filename";
                    if (!$osuccess) {
                        print "Error: Could not write to list: $workdir/aux/$list_filename\n";
                        exit $FAILURE;
                    }
                    foreach my $line (@{getLineArray($list)}) {
                        outputLine($FH, $line, $format, \@keyarr); 
                    }
                    close $FH;
                }
            }
        }
    }

    my @dirs = <$workdir/aux/*>;
    if (scalar(@dirs) == 0) {
        rmdir "$workdir/aux";
    }
    print "outputIndependentLists: END\n\n";
} ## end outputIndependentLists

#####################################################################
sub outputJobInputLists {
    my ($Config, $jobinfo) = @_;

    print "outputJobInputLists: BEG\n";

    my $workdir = $Config->value('work_dir');
    my $project = $Config->value('project');
    my $run = $Config->value('submit_run');
    my $fileclass = $Config->value('fileclass');

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    # create directory in which to store lists
    if (! -d "$workdir/aux") {
        print "Making aux directory...";
        eval { mkpath("$workdir/aux") };
        if ($@) {
            print "Error\nCouldn’t create $workdir/aux: $@\n";
            exit $FAILURE;
        }
        else {
            print "done\n";
        }
    }

print Dumper($jobinfo), "\n\n";
    foreach my $jobid (sort keys %$jobinfo) {
        my $jobidstr = sprintf("%04d", $jobid);

        foreach my $module (@module_array) {
            my $module_obj = $uber_module_obj->obj($module);

            my $uber_list_obj = $module_obj->obj('list');
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);
print "$jobid, $module, $listname,", $jobinfo->{$jobidstr}{'ccd'},"\n";
                if (!$list_obj->exists('jobdependent') || $list_obj->value('jobdependent')) {

                    if (exists($jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'})) {
                        my $list = $jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'};
                        my $list_filename = $jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'filename'};

                        my $format = "textsp";
                        if ($list_obj->exists("format")) {
                            $format = $list_obj->value("format");
                        }
                        my $output_fields;
                        if ($format =~ m/config/i) {
                            $output_fields = "localpath,localfilename,fileid";
                        }
                        else {
                            $output_fields = "fullname";
                        }
                        if ($list_obj->exists("output_fields")) {
                            $output_fields = lc($list_obj->value("output_fields"));
                        }
                        my @keyarr = split /\s*,\s*/, $output_fields;

                        my $FH;
                        open $FH, "> $workdir/aux/$list_filename";
                        foreach my $line (@{getLineArray($list)}) {
                            outputLine($FH, $line, $format, \@keyarr); 
                        }
                        close $FH;

                        # save list filename as it would be on target machine in archive structure
                        my $archive_path = getArchiveLocation( { project=>$project, run=>$run, 
                                                                 fileclass=>$fileclass,  filetype => 'aux', });
                        $jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'filename'}=$archive_path."/".$list_filename;

                    } 
                    else {
                        # Create input filename and output to jobinfo file
                        my $fileclass = $list_obj->value("fileclass");
                        my $filetype = 'aux';
                        if (!$list_obj->exists("filename")) {
                            print STDERR "Could not find filename for list object $listname in job $jobidstr\n";
                            exit $FAILURE;    
                        }
                        my $filename = $list_obj->value("filename");
                        my $archive_path = getArchiveLocation({'project'=>$project, 'run'=>$run, 
                                                              'fileclass'=>$fileclass, 'filetype'=>$filetype}); 
                        $jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'filename'}=$archive_path."/".$filename;
                    }
                }
            }
        }
    }
    my @dirs = <$workdir/aux/*>;
    if (scalar(@dirs) == 0) {
        rmdir "$workdir/aux";
    }
    print "outputJobInputLists: END\n\n";
} ## end outputJobInputLists


#####################################################################
sub calcWall {
    my ($Config, $jobinfo) = @_;

    print "calcWall: BEG\n";
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my $wall_startup = 0;
    if ($Config->exists('wall_startup')) {
        $wall_startup = $Config->value('wall_startup');
    }

    foreach my $jobid (keys %$jobinfo) {
        my $jobidstr = sprintf("%04d", $jobid);

        $jobinfo->{$jobidstr}{'wallmins'} = $wall_startup;

        foreach my $module (@module_array) {
            my $module_obj = $uber_module_obj->obj($module);
            if ($module_obj->exists('wall_mod')) {
                $jobinfo->{$jobidstr}{'wallmins'} += $module_obj->value('wall_mod'); 
#                print "\tAdding on wall_mod for module $module in job $jobidstr\n";
            }
            else {
                print "\tNo wall_mod for module $module in job $jobidstr\n";
            }

            my $uber_list_obj = $module_obj->obj('list');
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);
        
                if (!$list_obj->exists('jobdependent') || $list_obj->value('jobdependent')) {

                    if (exists($jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'})) {
                        my $list = $jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'};

                        if ($list_obj->exists('wall_each')) {
                            my @lines = @{getLineArray($list)};
                            my $numinlist = scalar(@lines);
                            my $wallinc = $list_obj->value('wall_each') * $numinlist;
                            $jobinfo->{$jobidstr}{'wallmins'} += $wallinc;
                            print "Adding in wall_each for module $module list $listname for job $jobidstr: inc $wallinc\n";
                        }
                    }
                }
            }
        }
    }
    print "calcWall: END\n\n";
} ## end calcWall


#####################################################################
sub saveJobInfo {
    my ($Config, $jobinfo, $DESFile) = @_;

    print "saveJobInfo: BEG\n";
    # First delete lists
    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    foreach my $jobid (keys %$jobinfo) {
        my $jobidstr = sprintf("%04d", $jobid);

        foreach my $module (@module_array) {
            my $module_obj = $uber_module_obj->obj($module);

            my $uber_list_obj = $module_obj->obj('list');
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);
        
                if (exists($jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'})) {
                    delete $jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}{'listlines'};
                }
                if (scalar(keys %{$jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname}}) == 0) {
                    delete $jobinfo->{$jobidstr}{'module'}{$module}{'list'}{$listname};
                }
                if ($list_obj->exists('master')) {
                    $list_obj->delete('master');
                }
                if ($list_obj->exists('sublists')) {
                    $list_obj->delete('sublists');
                }
            }

            my $uber_file_obj = $module_obj->obj('file');
            foreach my $file_name ($uber_file_obj->keys()) {
                my $file_obj = $uber_file_obj->obj($file_name);
                if ($file_obj->exists('master')) {
                    $file_obj->delete('master');
                }
            }
        }
    }

    ### increment job_id for future blocks 
    my $newConfig = new Orchestration::desconfig({configfile=>$DESFile});
    $newConfig->incJobID(scalar(keys %$jobinfo));
    $newConfig->save_file($DESFile);

    ### save jobinfo to file
    my %confighash = $Config->getall();
    $confighash{'job'} = $jobinfo;
    my $jobfile = 'jobinfo.des';
    $Config->save_file($jobfile, \%confighash);

    print "saveJobInfo: END\n\n";
    return $jobfile;
} ## end saveJobInfo


############################################################
# if make it here, assume global and block def for ingest_pastrun = true
# check ingest_pastrun for each module and each list/file obj 
sub savePastRun {
    my ($Config) = @_;

    print "savePastRun: BEG\n";

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my $curr_run = $Config->value('submit_run');
    my %pastrun = ();

    foreach my $module (@module_array) {
        my $module_obj = $uber_module_obj->obj($module);

        if (!$module_obj->exists('ingest_pastrun') || 
            $module_obj->value('ingest_pastrun')) {
            my $uber_list_obj = $module_obj->obj('list');
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);

                if ($list_obj->exists('master') && 
                    (!$list_obj->exists('ingest_pastrun') || 
                     $list_obj->value('ingest_pastrun') )) {
                    my $master = $list_obj->value('master');
                    my $newPastRun = getSearchObjPastRun($master, $curr_run);
                    %pastrun = (%pastrun, %$newPastRun);
                }
                else {
                    print "\t\tSkipping runs from list $listname in module $module\n";
                }
            }

            # process each "file" in each module
            my $uber_file_obj = $module_obj->obj('file');
            foreach my $file_name ($uber_file_obj->keys()) {
                my $file_obj = $uber_file_obj->obj($file_name);
                if ($file_obj->exists('master') && 
                    (!$file_obj->exists('ingest_pastrun') || 
                     $file_obj->value('ingest_pastrun') )) {
                    my $master = $file_obj->value('master');
                    my $newPastRun = getSearchObjPastRun($master, $curr_run);
                    %pastrun = (%pastrun, %$newPastRun);
                }
                else {
                    print "\t\tSkipping runs from file $file_name in module $module\n";
                }
            }
        }
        else {
            print "\t\tSkipping runs from module $module\n";
        }
    }

    my $paststr = "";
    foreach my $r (keys %pastrun) {
        $paststr .= $r.',';
    }
        
    # remove trailing comma if any
    $paststr =~ s/,$//;

    print "\t\tpastrun = $paststr\n";

    if ($paststr =~ /\S/) {
        my $block = $Config->value('block');
        $Config->obj('block')->obj($block)->value('pastrun', $paststr);
    }
    print "savePastRun: END\n\n";
} ## end savePastRun

############################################################
sub getLineArray {
    my ($list) = @_;
    my $linearr;
    if (UNIVERSAL::isa( $list->{'line'}, "HASH" )) {
        push(@$linearr, $list->{'line'});
    }
    else {
        $linearr = $list->{'line'};
    }
    return $linearr;
}

############################################################
sub getFileArray {
    my ($line) = @_;
    my $filearr;

    if (UNIVERSAL::isa( $line->{'file'}, "HASH" )) {
        push(@$filearr, $line->{'file'});
    }
    else {
        $filearr = $line->{'file'};
    }
    return $filearr;
}

############################################################
sub getSearchObjPastRun {
    my ($master, $curr_run) = @_;
    my $pastrun = {};
    foreach my $line (@{getLineArray($master)}) {
        my $filearr = getFileArray($line);
        foreach my $file (@$filearr) {
            my $fileclass = $file->{'fileclass'}; 
            my $id = $file->{'id'}; 
            my $run = $file->{'run'};
            if ($fileclass !~ /\S/) {
                print "WARNING: file $id missing fileclass \n";
                print "Cannot determine pastrun\n";
            }
            if (($fileclass ne 'src') && ($fileclass ne 'cal')) {
                if ($run !~ /\S/) {
                    print "WARNING: file $id missing run \n";
                    print "Cannot determine pastrun\n";
                }
                elsif ($run ne $curr_run) {
                    $pastrun->{$run} = 1;
                }
            }
        }
    }
    return $pastrun;
}

############################################################
sub getValuesLine {
    my ($line, $value, $numvals, $nickname) = @_;
    my %valhash = ();
    my @valarr = ();
    my $filearr = getFileArray($line);
    foreach my $file (@$filearr) {
        if (defined($file->{$value}) && (!defined($nickname) || $nickname eq $file->{'nickname'})) {
            $valhash{$file->{$value}} = 1; 
        }
    }
    if (defined($line->{$value})) {
        $valhash{$line->{$value}} = 1; 
    }

    @valarr = keys %valhash;

    if (defined($numvals) && (scalar(@valarr) != $numvals)) {
        print "Warning: in getValuesLine number found doesn't match requested ($numvals)\n";
        if (defined($nickname)) {
            print "\tnickname = $nickname\n";
        }
        print "\tvalue to find: $value\n";
        print "\tline: ",Dumper($line), "\n";
        print "\tvalarr:",Dumper(\@valarr), "\n";
        exit $FAILURE;
    }

    my $retval = undef;
    if (defined($numvals) && ($numvals == 1)) {
        $retval = $valarr[0];
    }
    elsif (scalar(@valarr) == 0) {
        $retval = undef;
    }
    elsif (scalar(@valarr) == 1) {
        $retval = $valarr[0];
    }
    else {
        $retval = \@valarr;
    }
    return $retval;
}

############################################################
sub outputFullList {
    my ($Config) = @_;

    print "outputFullList: BEG\n";

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    foreach my $module (@module_array) {
        my $module_obj = $uber_module_obj->obj($module);

        my $uber_list_obj = $module_obj->obj('list');
        foreach my $listname ($uber_list_obj->keys()) {
            print "\t$listname\n";
            my $list_obj = $uber_list_obj->obj($listname);

            if ($list_obj->exists('master')) {
                my $master = $list_obj->value('master');
                my $fulllist = "${module}_${listname}_full.list";
                open FH, "> $fulllist"; 
                foreach my $line (@{getLineArray($master)}) {
                    my $filearr = getFileArray($line);
                    foreach my $file (@$filearr) {
                        print FH $file->{'fullname'},"\n";
                    }
                }
                close FH;
            }
        }
    }
    print "outputFullList: END\n\n";
} ## end outputFullList

############################################################
### output line info to input list for science code
sub outputLine {
    my ($FH, $line, $format, $keys) = @_;    
    
    if ($format =~ m/config/i) {
        print $FH "<file>\n";
    }

    my $numkeys = scalar(@$keys);
    for (my $i = 0; $i < $numkeys; $i++) {
        my $key = $$keys[$i];
        my $value = undef;

        my @nodes;
        if ($key =~ /\./) {
            my ($nickname, $key2) = split /\./, $key;
            $value = getValuesLine($line, $key2, undef, $nickname);
            if (!defined($value)) {
                $value = getValuesLine($line, $key2, 1, undef);
                if (!defined($value)) {
                    print STDERR "Error: could not find value $key for line...\n";
                    print STDERR Dumper($line),"\n";
                    exit $FAILURE;
                }
                else { # assume nickname was really table name
                    $key = $key2;
                }
            }
        }
        else {
            $value = getValuesLine($line, $key, 1, undef);
        }

        # handle last field (separate to avoid trailing comma)
        if ($i == $numkeys - 1) {
            printValue($FH, $key, $value, $format, 1);
        }
        else {
            printValue($FH, $key, $value, $format, 0);
        }
    }
 
    if ($format =~ m/config/i) {
        print $FH "</file>\n";
    }
    else {
        print $FH "\n";
    }
} ## end outputLine


#####################################################################
sub printValue {
    my ($FH, $key, $value, $format, $last) = @_;

    if ($format =~ m/config/i) {
        print $FH "     ",$key,"=", $value,"\n";
    }
    else {
        print $FH $value;
        if (!$last) {
            if ($format =~ m/textcsv/i) {
                print $FH ",";
            }
            elsif ($format =~ m/texttab/i) {
                print $FH "\t";
            }
            else {
                print $FH " ";
            }
        }
    }
}

sub createJobDirs {
    my ($block, $jobinfo) = @_;
#    foreach my $jobid (keys %$jobinfo) {
#        my $jobidstr = sprintf("%04d", $jobid);
#        mkdir "../${block}_${jobidstr}";
#    }
    mkdir "../${block}_tjobs";
}


sub createPipelinesMngrDAG {
    my ($Config) = @_;
    print "calcNumTargetJobs: BEG\n";
    my $block = $Config->value("block");
    my $block_obj = $Config->obj('block')->obj($block);
    my $run = $Config->value("submit_run");
    my $runsite = $Config->value("run_site");
    my $deshome = $Config->value("des_home");
    my $uber_job_obj = $Config->obj('job');
    my @jobarray = sort($uber_job_obj->keys());
    my $numjobs = scalar(@jobarray);
    my $numtargetjobs = 0;
    my $numcpus = 1;

    my %tjobinfo = ();

    if ($Config->exists('usevirtualmachines') && $Config->value('usevirtualmachines')) {
        if ($Config->exists('num_cpus')) {
            $numcpus = $Config->value('num_cpus');
        }
        elsif ($Config->exists('num_cpu')) {
            $numcpus = $Config->value('num_cpu');
        }
        $numtargetjobs = ceil($numjobs/$numcpus);
    }
    else {
        $numtargetjobs = $numjobs;
    }

    my $targetdir = $Config->value('archive_root').'/'.$Config->value('run_dir');
    my $runtimeprefix = "$targetdir/runtime/$block";
    my $xmlprefix = "$targetdir/xml/$block";
    my $firstid = $Config->value('start_jobid');
    my $dbid_block = $Config->value('dbid_block');


    # reservation wall overrides all block or mod wall defs
    my $reservewall = 0;
    if ($Config->exists('reserve_wall')) {
        $reservewall = $Config->value('reserve_wall');
    }

print "numjobs = $numjobs\n";
print "numcpus = $numcpus\n";
print "numtargetjobs = $numtargetjobs\n";
    open DAG, "> pipelinesmngr.dag";
    print DAG "DOT pipelinesmngr.dot\n";

    my $j = 0;
    for (my $tj=1; $tj <= $numtargetjobs; $tj++) {
        # figure out args for run_elf
        my $numj = min($numcpus, $numjobs-$j);
        my $jobid = $jobarray[$j];
        my $jpad = sprintf("%04d", $j);
        my $tjpad = sprintf("TJ%04d", $tj);
        my $args = "$runtimeprefix $xmlprefix $tjpad $numj $jobid $firstid $dbid_block"; 

        $tjobinfo{$tjpad}{'start_jobid'} = $jobid;
        $tjobinfo{$tjpad}{'end_jobid'} = sprintf("%04d", $jobid+$numj-1);

        my $wall = 0;
        if ($reservewall > 0) {
           $wall = $reservewall;
        }
        else {
            # find max wall for DES jobs in target job
            $wall = 0; 
            for (my $k=0; $k<$numj; $k++) {
                my $jidx = $j + $k;
                $jobid = $jobarray[$jidx];
                my $jobobj = $uber_job_obj->obj($jobid);
                my $jobwall = 0;
                if ($jobobj->exists('wallmins')) {
                    $jobwall = $jobobj->value('wallmins'); 
                }
                $wall = max($wall, $jobwall);
            }

            # pad wall just cause
            $wall = ceil($wall*1.1);
        }

        if ($Config->exists('min_wall')) {
            my $min = $Config->value('min_wall');
            if ($wall < $min) {
                print "WARNING: wall too small ($wall).   Resetting to $min\n";
                $wall = $min;
            }
        }
        if ($Config->exists('max_wall')) {
            my $max = $Config->value('max_wall');
            if ($wall > $max) {
                print "WARNING: wall too large ($wall).   Resetting to $max\n";
                $wall = $max;
            }
        }

        print DAG <<EOF;
JOB $tjpad runjob.condor
VARS $tjpad deshome="$deshome" descfg="jobinfo.des" 
VARS $tjpad block="$block" run="$run" runsite="$runsite" jobid="$tjpad"
VARS $tjpad wall="$wall" args="$args"
SCRIPT pre $tjpad $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
SCRIPT post $tjpad $deshome/libexec/jobpost.pl jobinfo.des \$JOB \$RETURN 
EOF

        $j += $numj;
    }
    close DAG;

    $block_obj->value('tjobinfo', \%tjobinfo);

    my $stat = addInfoToDag($Config, "pipelinesmngr.dag", \*STDOUT);
    if ($stat != 0) {
        print "addInfoToDag returned non-zero status.  Aborting\n";
        exit $FAILURE;
    }
}

sub createElfFiles {
    my ($Config) = @_;
    
    print "Createfiles - Begin\n";
    # create directory in which to store lists
    my $workdir = $Config->value("work_dir");
    if (! -d "$workdir/xml") {
        print "Making xml directory...";
        eval { mkpath("$workdir/xml") };
        if ($@) {
            print "Error\nCouldn’t create $workdir/xml: $@\n";
            exit $FAILURE;
        }
        else {
            print "done\n";
        }
    }

    #print Dumper($Config->getall());

    my $module_list = $Config->value('module_list');
    print "module_list = $module_list\n";
    my @module_array = split /\s*,\s*/, $module_list;

    my $uber_job_obj = $Config->obj('job');
    my @jobarray = $uber_job_obj->keys();

    foreach my $jobid (sort @jobarray) {
        print "--- Creating files for job $jobid ---\n";
        createBootstrapFile($Config, $jobid);
        my $job_obj = $uber_job_obj->obj($jobid);
        my $uber_mod_obj = $job_obj->obj('module');
        foreach my $module (@module_array) {
            print "\tCreating properties files for job $jobid for module $module\n";
            my $mod_obj = $uber_mod_obj->obj($module);
            createPropertiesFiles($Config, $jobid, $module, $mod_obj);
        }
        createXMLFile($Config, $jobid);
    }

    # create findingest job
    if (!$Config->exists('ingest') || $Config->value('ingest')) {
        my $jobid = 'ingest';
        my $module = 'ingest';
        print "--- Creating files for job $jobid ---\n";
        createBootstrapFile($Config, $jobid);
        my $uber_mod_obj = $Config->obj('module');
        my $mod_obj = $uber_mod_obj->obj($module);
        print "\tCreating properties files for job $jobid for module $module\n";
        if ($Config->exists('pastrun') &&
            (!$Config->exists('ingest_pastrun') || $Config->value('ingest_pastrun'))) {
            my $new_module_obj = dclone($mod_obj);
            my $args = $new_module_obj->value('args');
            $args .= " -pastrun ".$Config->value('pastrun');
            $new_module_obj->value('args',$args);
            $mod_obj = $new_module_obj;
        }
        createPropertiesFiles($Config, $jobid, $module, $mod_obj);
        createXMLFile($Config, $jobid);
    }
}

sub parseStageOutput {
    my ($stageout) = @_;
    my $insidelist = 0;
    my %fullnames;
    open STAGE, "< $stageout" or die "Cannot open stage output '$stageout'"; 
    while (my $line = <STAGE>) {
       chomp($line);
       if ($line eq "BEGIN DESTINATION FILE LISTING") {
           $insidelist = 1;
       }
       elsif ($line eq "END DESTINATION FILE LISTING") {
           $insidelist = 0;
       }
       elsif ($insidelist) {
           my ($fileid, $filename) = split /\s+/, $line;
           $fullnames{$fileid} = $filename;
       }
    }
    close STAGE;
    return \%fullnames;
}

sub fileFullname {
    my ($Config, $fullnames, $file, $staged) = @_;
    my ($name,$path,$suffix);

    if ($staged) {
        my $id = $file->{'fileid'};
        if (!defined($fullnames->{$id}) || ($fullnames->{$id} !~ /\S/)) {
            print "Error:  bad name from arcp for id $id\n";
            exit $FAILURE;
        }
        ($name,$path,$suffix) = fileparse($fullnames->{$id}, ('.fz','.gz'));
    }
    else {
        my $archive_root = $Config->value('archive_root');
        $name = $file->{'filename'};
        eval { $path = getArchiveLocation($file); };
        if ($@) {
            # There was an error in the eval {}
            print "File missing data needed by getArchiveLocation\n";
            print "\t$@\n";
            print Dumper($file),"\n";
            exit $FAILURE;
        }
        $path = $archive_root.'/'.$path;
        $file->{'localfilename'} = $file->{'filename'};

    }
    $path =~ s/\/$//;
    my $newfullname = $path.'/'.$name;

    $file->{'fullname'} = $newfullname;
    $file->{'localpath'} = $path;
    $file->{'localfilename'} = $name;
    $file->{'fileid'} = $file->{'id'};
}


sub assignFullnames {
    my ($Config, $fullnames) = @_;

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    ## insert absolute paths into master list
    foreach my $module (@module_array) {
        my $module_obj = $uber_module_obj->obj($module);

        # process each "list" in each module
        my $uber_list_obj = $module_obj->obj('list');
        foreach my $listname ($uber_list_obj->keys()) {
            my $list_obj = $uber_list_obj->obj($listname);

            if ($list_obj->exists('master')) {
                my $master = $list_obj->value('master');
                my $staged = (!$Config->exists('notarget') || !$Config->value('notarget')) && 
                             (!$list_obj->exists("stage_files") || $list_obj->value("stage_files"));
                foreach my $line (@{getLineArray($master)}) {
                    my $filearr = getFileArray($line);
                    foreach my $file (@$filearr) {
                        fileFullname($Config, $fullnames, $file, $staged);
                    }
                }
            }
        }

        # process each "file" in each module
        my $uber_file_obj = $module_obj->obj('file');
        foreach my $file_name ($uber_file_obj->keys()) {
            my $file_obj = $uber_file_obj->obj($file_name);
            if ($file_obj->exists('master') && (!$file_obj->exists("stage_files") || $file_obj->value("stage_files"))) {
                my $master = $file_obj->value('master');
                my $staged = (!$Config->exists('notarget') || !$Config->value('notarget')) && 
                             (!$file_obj->exists("stage_files") || $file_obj->value("stage_files"));
                foreach my $line (@{getLineArray($master)}) {
                    my $filearr = getFileArray($line);
                    foreach my $file (@$filearr) {
                        fileFullname($Config, $fullnames, $file, $staged);
                    }
                }
            }
        }
    }
}


sub createFullnames {
    my ($Config) = @_;

    print "createFullnames: BEG\n";

    my $uber_module_obj = $Config->obj('module');
    my $module_list = $Config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;

    my %stageids;
    foreach my $module (@module_array) {
        my $module_obj = $uber_module_obj->obj($module);

        if ($module_obj->exists('list')) {
            my $uber_list_obj = $module_obj->obj('list');
            foreach my $listname ($uber_list_obj->keys()) {
                my $list_obj = $uber_list_obj->obj($listname);

                # if supposed to stage files
                if ($list_obj->exists('master')) {
                    my $master = $list_obj->value('master');
                    my $archive_root = $Config->value('archive_root');
                    foreach my $line (@{getLineArray($master)}) {
                        my $filearr = getFileArray($line);
                        foreach my $file (@$filearr) {
                            my $archive_path = "";
                            eval { $archive_path = getArchiveLocation($file); };
                            if ($@) {
                                # There was an error in the eval {}
                                print "File missing data needed by getArchiveLocation\n";
                                print "\t$@\n";
                                print Dumper($file),"\n";
                                exit $FAILURE;
                            }
                            $file->{'localpath'} = $archive_root.'/'.$archive_path;
                            $file->{'localfilename'} = $file->{'filename'};
                            $file->{'fileid'} = $file->{'id'};
                            $file->{'fullname'} = $file->{'localpath'}.'/'.$file->{'filename'};
                        }
                    }
                }
            }
        }

        # process each "file" in each module
        if ($module_obj->exists('file')) {
            my $uber_file_obj = $module_obj->obj('file');
            foreach my $file_name ($uber_file_obj->keys()) {
                my $file_obj = $uber_file_obj->obj($file_name);
                if ($file_obj->exists('master')) {
                    my $master = $file_obj->value('master');
                    my $archive_root = $Config->value('archive_root');
                    foreach my $line (@{getLineArray($master)}) {
                        my $filearr = getFileArray($line);
                        foreach my $file (@$filearr) {
                            my $archive_path = getArchiveLocation($file);
                            $file->{'localpath'} = $archive_root.'/'.$archive_path;
                            $file->{'localfilename'} = $file->{'filename'};
                            $file->{'fileid'} = $file->{'id'};
                            $file->{'fullname'} = $file->{'localpath'}.'/'.$file->{'filename'};
                        }
                    }
                }
            }
        }
    } # end foreach module

    print "createFullnames: END\n\n";
}  ## end createFullnames


###########################################################
sub calcNumTargetJobs {
    my ($Config) = @_;
    print "calcNumTargetJobs: BEG\n";

    my $uber_job_obj = $Config->obj('job');
    my @jobarray = $uber_job_obj->keys();
    my $numjobs = scalar(@jobarray);
    my $numtargetjobs = 0;

    if ($Config->exists('usevirtualmachines') && $Config->value('usevirtualmachines')) {
        my $numcpus = 1;
        if ($Config->exists('num_cpus')) {
            $numcpus = $Config->value('num_cpus');
        }
        elsif ($Config->exists('num_cpu')) {
            $numcpus = $Config->value('num_cpu');
        }

        $numtargetjobs = ceil($numjobs/$numcpus);
    }
    else {
        $numtargetjobs = $numjobs;
    }

    print "calcNumTargetJobs: END\n\n";
    return $numtargetjobs;
}

1;
