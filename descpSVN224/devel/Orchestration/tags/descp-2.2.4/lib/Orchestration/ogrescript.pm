########################################################################
##  $Id: ogrescript.pm 6629 2011-04-13 01:19:01Z mgower $
##
##  $Rev:: 6629                             $:  # Revision of last commit.
##  $LastChangedBy:: mgower                 $:  # Author of last commit.
##  $LastChangedDate:: 2011-04-12 18:19:01 #$:  # Date of last commit.
##
##  Authors:
##         Michelle Gower (mgower@ncsa.uiuc.edu)
##         Darren Adams (dadams@ncsa.uiuc.edu)
##
##  Developed at:
##  The National Center for Supercomputing Applications (NCSA).
##
##  Copyright (C) 2007 Board of Trustees of the University of Illinois.
##  All rights reserved.
##
##  DESCRIPTION:
##
########################################################################

package Orchestration::ogrescript;

use strict;
use warnings;

use Data::Dumper;

use File::Basename;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::desconfig;

use Exporter   ();
our (@ISA, @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw(&createPropertiesFiles &createXMLFile &createBootstrapFile);



#####################################################################
#
# Since elf/ogrescript can replace variables in strings, leave them
# Just make sure all variables are given values in properties file.
#
# By leaving the variables, it is easier to manually edit file if needed
# (i.e. change value in one place instead of many)
#
sub createPropertiesFiles {
    my $config = shift;
    my $jobid = shift;
    my $module = shift;
    my $job_module_obj = shift;

print "createPropertiesFiles BEG\n";

print "\tWorking on module $module\n";
    my $rundir = $config->value("run_dir");
    my $workdir = $config->value("work_dir");
    my $archivedir = $config->value("archive_root");
    my $softdir = $config->value("software_root");
    my $prereqdir = $config->value("prereq_root");
    my $operator = $config->value("operator");
    my $block = $config->value("block");
    my $filename = "$workdir/xml/${block}_${module}_$jobid.properties";

    my $module_obj = $config->obj("module")->obj($module);

    my ($timecmd, $stracecmd);
    if ($config->exists("timecmd")) {
        $timecmd = $config->value("timecmd");
    }
    else {
        print "Could not find timecmd value.\nMake sure it is set in the relevant software def.\nABORTING";
        exit $FAILURE;
    }

    if ($config->exists("stracecmd")) {
        $stracecmd = $config->value("stracecmd");
    }
    else {
        print "Could not find stracecmd value.\nMake sure it is set in the relevant software def.\nABORTING";
        exit $FAILURE;
    }

    my %neededvals = (  'module' => $module, 
                        'block' => $block,
                        'job_id' => $jobid,
                        'des_home' => $softdir,
                        'des_prereq' => $prereqdir,
                        'archive_root' => $archivedir,
                        'run_dir' => $rundir,
                        'timecmd' => $timecmd,
                        'stracecmd' => $stracecmd,
                        'log_dir' => "\${archive_root}/\${run_dir}/log",
                        'log' => "\${block}_\${module}_\${job_id}.log",
                        'logger' => "\${log_dir}/\${log}",
                        'runtime_dir' => "\${archive_root}/\${run_dir}/runtime/\${block}_\${job_id}",
                     );

    foreach my $k ($module_obj->keys()) {
        if ($k eq 'list') {
            my $uber_list_obj = $job_module_obj->obj('list');
            foreach my $l ($uber_list_obj->keys()) {
                my $val = $archivedir."/".$uber_list_obj->obj($l)->value('filename');
                $neededvals{$l} = $val;
            }
        }
        elsif ($k eq 'file') {
            my $uber_file_obj = $job_module_obj->obj('file');
            foreach my $f ($uber_file_obj->keys()) {
                my $val = $uber_file_obj->value($f);
                $neededvals{$f} = $val;
            }
        }
        else {  
            if (defined($job_module_obj) && $job_module_obj->exists($k)) {
                $neededvals{$k} = $job_module_obj->value($k);
            }
            else {
                if ($config->exists($k, {currentvals=>{job=>$jobid, module=>$module}})) {
                    my $val = $config->value($k, {currentvals=>{job=>$jobid, module=>$module}});
                    $neededvals{$k} = $val;
                }
            }
        }
    }

    if ($module_obj->exists('req_vals')) {
        my $reqvalslist = $module_obj->value("req_vals");
        $reqvalslist = lc($reqvalslist);
        my @reqvals = split /\s*,\s*/, $reqvalslist;
        foreach my $rv (@reqvals) {
            if ($config->exists($rv, {currentvals=>{job=>$jobid, module=>$module}})) {
                my $val = $config->value($rv, {currentvals=>{job=>$jobid, module=>$module}});
                $neededvals{$rv} = $val;
            }
            else {
                print "Error: could not find required value $rv\n";
                exit $FAILURE;
            }
        }
    }

    # make sure any variables used in values are lowercase to make it case-insensitive for elf.
    while (my ($nk,$nv) = each(%neededvals)) {
        my @vararray = ($nv =~ /\$\{(\w+)\}/g);
        foreach my $var (@vararray) {
            my $lc_var = lc($var);
            $nv =~ s/\$\{$var\}/\$\{$lc_var\}/g;
        }
        $neededvals{$nk} = $nv;
    }

    # make sure all variables will have values in properties file
    my $done = 0;
    while (!$done) {
        $done = 1;

        # make sure include any variables used in value strings
        while (my ($nk,$nv) = each(%neededvals)) {
            while ($nv =~ /\$\{(\w+)\}/g) {
                my $var = lc($1);
                if (!defined($neededvals{$var})) {
                    if ($config->exists($var, {currentvals=>{job=>$jobid, module=>$module}})) {
                        my $nval = $config->value($var, {currentvals=>{job=>$jobid, module=>$module}});
                        $neededvals{$var} = $nval;
                        $done = 0;
                    }
                    else {
                        print "Error: could not find value $var\n";
                        exit $FAILURE;
                    }
                }
            }
        }

        # replace optional values with either their value or empty string
        while (my ($nk,$nv) = each(%neededvals)) {
            while ($nv =~ /\$opt\{(\w+)\}/i) {
                my $var = lc($1);
                if (defined($neededvals{$var})) {
                    $nv =~ s/\$opt\{$var\}/\$\{$var\}/ig;
                }
                elsif ($config->exists($var, {currentvals=>{job=>$jobid, module=>$module}})) {
                    $nv =~ s/\$opt\{$var\}/\$\{$var\}/ig;
                }
                else {
                    $nv =~ s/\$opt\{$var\}//ig;
                }
                $neededvals{$nk} = $nv;
                $done = 0;
            }
        }
    }
   
    # ccd must be padded
    if (defined($neededvals{'ccd'})) {
        $neededvals{'ccd'} = sprintf("%02d", $neededvals{'ccd'});
    }

    open FILE, "> $filename";
    foreach my $k (sort keys %neededvals) {
        my $v = $neededvals{$k};
        print FILE "$k = $v\n";
    }
    close FILE;

    print "createPropertiesFiles END\n";
}


#####################################################################
sub createBootstrapFile {
    my $config = shift;
    my $jobid = shift;

    my $workdir = $config->value("work_dir");
    my $eventhost = $config->value("event_host");
    my $project = $config->value("project");
    my $run = $config->value("submit_run");
    my $block = $config->value("block");
    my $operator = $config->value("operator");

    my $save_events = 0;
    if ($config->exists("save_events")) {
        $save_events = $config->value("save_events");
    }

    my $filename = "$workdir/xml/${block}_$jobid.bootstrap.properties";
    open FILE, "> $filename";
    if ($save_events) {
#        print FILE <<EOF;
#eventSender.0=tcp://$eventhost:61616,ncsa.tools.jms.localevent.JMSLocalEventSender
#eventReceiver.0=tcp://$eventhost:61616,ncsa.tools.jms.localevent.JMSLocalEventReceiver
#EOF
        print FILE <<EOF;
eventSender.0=rmi://${eventhost}:1099/vizier,ncsa.services.events.VizierRMIBatchedLocalEventSender
EOF
    }

    my $sep = '-';
    my $groupid = $operator . $sep;
    if ($config->exists('event_tag')) {
        $groupid .= $config->value('event_tag');
    }
    $groupid .= $sep . $project . $sep . $run;

    print FILE <<EOF;
eventListener.tag=loggingListener
eventTopic.0=Status:1
eventTopic.1=Status:2
eventTopic.2=Status:3
eventTopic.3=Status:4
eventTopic.4=Status:5
eventTopic.5=QA:1
eventTopic.6=QA:2
eventTopic.7=QA:3
eventTopic.8=QA:4
eventTopic.9=QA:5
defaultEventTopic=Status:1
identityManager.tag=defaultIdentification
identityManager.defaultIdentification.baseId=http://daues:8080/app?level=INFO&groupId=$groupid&workflow=$block&node=$jobid&user=$operator
EOF
    close FILE;
}


#####################################################################
sub createXMLFile {
    my $config = shift;
    my $jobid = shift;

print "createXMLFile BEG\n";
    my $block = $config->value("block");
    my $archiveroot = $config->value("archive_root");
    my $rundir = $config->value("run_dir");
    my $softdir = $config->value("software_root");

    my $xmldir="$archiveroot/$rundir/xml";
    my $workdir = $config->value("work_dir");
    my $filename = "$workdir/xml/${block}_$jobid.xml";
    open FILE, "> $filename";
    print FILE <<EOF;
<elf>
   <workdir />
   <serial-scripts separate-script-dirs="false">

<ogrescript>
    <echo message="PIPELINE ELFBEGIN $jobid" stdout="true"/>
    <write-lines path="pipeline_$jobid.log" append="true">
        <line>PIPELINE ELFBEGIN $jobid</line>
    </write-lines>

    <declare name="errorFlag" string="Failed"/>
    <declare name="totStatus4" long="0"/>
    <declare name="totStatus5" long="0"/>
    <declare name="cntStatus4" long="0"/>
    <declare name="cntStatus5" long="0"/>

    <import file="$softdir/xml/execprog.xml"/>

EOF
    
    my $modulelist;
    if ($jobid =~ /ingest/i) {
        $modulelist = 'ingest';
    }
    else {
        $modulelist = $config->value("module_list");
    }

    my @modules = split /\s*,\s*/, $modulelist;
    foreach my $mod (@modules) {
        my $xmlfile = "generic.xml"; 
        if ($config->exists("xml", {currentvals=>{job=>$jobid, module=>$mod}})) {
            $xmlfile = $config->value("xml", {currentvals=>{job=>$jobid, module=>$mod}});
        }
        else {
            if ($config->exists('exec3', {currentvals=>{job=>$jobid, module=>$mod}})) {
                $xmlfile = "generic3.xml";
            }
            elsif ($config->exists('exec2', {currentvals=>{job=>$jobid, module=>$mod}})) {
                $xmlfile = "generic2.xml";
            }
        }
        print FILE <<EOF;
    <assign name="errorFlag" string="Failed"/>
    <assign name="cntStatus4" long="0"/>
    <assign name="cntStatus5" long="0"/>
    <properties file="$xmldir/${block}_${mod}_$jobid.properties"/>
    <import file="$softdir/xml/$xmlfile"/>
    <echo message="PIPELINE MODSTATUS $jobid $mod \${errorFlag}" stdout="true"/>
    <echo message="PIPELINE cntStatus4 $jobid $mod \${cntStatus4}" stdout="true"/>
    <echo message="PIPELINE cntStatus5 $jobid $mod \${cntStatus5}" stdout="true"/>
    <write-lines path="pipeline_$jobid.log" append="true">
        <line>PIPELINE MODSTATUS $jobid $mod \${errorFlag}</line>
        <line>PIPELINE cntStatus4 $jobid $mod \${cntStatus4}</line>
        <line>PIPELINE cntStatus5 $jobid $mod \${cntStatus5}</line>
    </write-lines>
EOF
    }

    print FILE <<EOF;
    <echo message="PIPELINE totStatus4 $jobid \${totStatus4}" stdout="true"/>
    <echo message="PIPELINE totStatus5 $jobid \${totStatus5}" stdout="true"/>
    <echo message="PIPELINE ELFEND $jobid"  stdout="true"/>
    <write-lines path="pipeline_$jobid.log" append="true">
        <line>PIPELINE totStatus4 $jobid \${totStatus4}</line>
        <line>PIPELINE totStatus5 $jobid \${totStatus5}</line>
        <line>PIPELINE ELFEND $jobid</line>
    </write-lines>
</ogrescript>

   </serial-scripts>
</elf>
EOF
    close FILE;
print "createXMLFile END\n";
}

1;
