########################################################################
#  $Id: misc.pm 6146 2010-11-16 20:02:36Z mgower $
#
#  $Rev:: 6146                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2010-11-16 13:02:36 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package Orchestration::misc;

use strict;
use warnings;

use POSIX qw(strftime);
use File::Basename;
use File::Copy;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

use Exporter   ();
our (@ISA, @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw($SUCCESS $REPEAT $FAILURE $NOTARGET $WARNINGS
                  &printVersion &getHome &getUserCfgDir &getTimeStamp 
                  &debug &runCmd &exitCodeInfo &flattenDir 
                  &createConfigFilename &epoch2str);

our $SUCCESS    = 0;
our $REPEAT     = 100;
our $FAILURE    = 10;
our $NOTARGET   = 2;
our $WARNINGS   = 3;

################################################################
sub printVersion {
  print "DES: version 0.1a\n";
}


################################################################
sub getHome {
   my $Home="";

   $Home = $FindBin::RealBin;
   $Home =~ s/\/bin$//;
   $Home =~ s/\/libexec$//;
   $ENV{DES_HOME} = $Home;

   # verify pointing to DES installation
   if (! -r "$Home/bin/desversion")
   {
     print STDERR "Error: invalid DES installation\n";
     print STDERR "\tDES_HOME = '$Home'\n";
     $Home = "";
   }
   return $Home;
}

################################################################
sub getUserCfgDir
{
   return $ENV{"HOME"}."/.des";
}

################################################################
sub getTimeStamp {
  my @DInfo = localtime();
  my $TS = sprintf("%02d/%02d/%4d %02d:%02d:%02d", 
                    $DInfo[4]+1, $DInfo[3], $DInfo[5]+1900, 
                    $DInfo[2], $DInfo[1], $DInfo[0]);

  return $TS;
}

################################################################
sub debug {
  my $Stage = shift;
  my $Str = shift;

  print getTimeStamp(), " - $Stage: $Str\n"; 
}

################################################################
sub runCmd {
  my $Cmd = shift;
  my $TimeStamp = getTimeStamp();
  my $Out = `$Cmd 2>&1`;
  print "$TimeStamp - $Cmd\n";
  print "\t$Out\n";
}


################################################################
sub exitCodeInfo {
    my ($code) = @_;

    my $str = "";
    my $exit = 0;

    if ($code & 128) {
        $str = "coredump";
        $exit = $code & 127;
    }
    elsif ($code & 127) {
        $exit = $code & 127;
        if ($exit == 11) {
            $str = "segfault";
        }
        else {
            $str = "signal";
        }
    }
    else {
        $exit = $code >> 8;
        $str = "exitcode";
    }
    return ($exit, $str);
}

################################################################
sub flattenDir {
    my ($name) = @_;

    my @dirs = <${name}_tmpdir_*>;
    if (scalar(@dirs) == 0) {
        print "FYI: No matching directory to flatten: ${name}_tmpdir\n";
    }
    elsif (scalar(@dirs) > 1) {
            print "Warning:  More than one directory to flatten ${name}_tmpdir_*\n";
            my $cnt = 1;
            foreach my $tmpdir (@dirs) {
                if (-d "$tmpdir") {
                    foreach my $f (<$tmpdir/*>) {
                        my $filename = basename($f);
                        my $newfilename = sprintf("%s_%03d_%s", $name, $cnt, $filename);
                        move($f, $newfilename);
                    }
                    rmdir "$tmpdir";
                }
                $cnt++;
            }
    }
    else {
        my $tmpdir = $dirs[0];
        if (-d "$tmpdir") {
            foreach my $f (<$tmpdir/*>) {
                my $filename = basename($f);
                my $newfilename = sprintf("%s_%s", $name, $filename);
                move ($f, $newfilename);
            }
            rmdir "$tmpdir";
        }
    }
}

################################################################
sub createConfigFilename {
    my ($archiveroot, $project, $fileclass, $run) = @_;

    my $config = "$archiveroot/$project/$fileclass/$run/runtime/uberctrl/config.des";
    return $config;
}

################################################################
sub epoch2str {
    my ($epoch) = @_;
    my $str = strftime( "%F %T%z",localtime($epoch));
    return $str;
}

1;
