########################################################################
#  $Id: utils.pm 4774 2009-12-01 18:51:04Z mgower $
#
#  $Rev:: 4774                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-12-01 11:51:04 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package Orchestration::Condor::utils;

use strict;
use warnings;

use Exporter   ();
our (@ISA, @EXPORT);

@ISA         = qw(Exporter);
@EXPORT      = qw(&getCondorVersion &currCondorVersionGE );

# returns the version in string format easy to compare
sub getCondorVersion {
    my $out = `condor_version 2>&1`;
    my $version = "";
    if ($out =~ /CondorVersion: (\d+)\.(\d+)\.(\d+)/) {
        $version = sprintf("%03d.%03d.%03d", $1, $2, $3);
    }
#    print "version = '$version'\n";
    return $version;
}

sub currCondorVersionGE {
    my ($v2) = @_; 
    my $ret = 0;

    if ($v2 =~ /(\d+)\.(\d+)\.(\d+)/) {
        $v2 = sprintf("%03d.%03d.%03d", $1, $2, $3);
    }

    my $currver = getCondorVersion();
    if ($currver ge $v2) {
        $ret = 1;
    }
    
    return $ret;
}

1;
