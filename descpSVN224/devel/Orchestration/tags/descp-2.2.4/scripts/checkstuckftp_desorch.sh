#!/bin/csh
foreach job (`ps -ef|grep gridftp|awk '{print $2}'`)
echo $job
	echo "sleeping 60 seconds"
	sleep 60
	set b = `ps -ef|grep gridftp|grep $job|awk '{print $2}'`
	echo $b
	if ($b > 0) then
	   kill -9 $job
	endif 		
end
