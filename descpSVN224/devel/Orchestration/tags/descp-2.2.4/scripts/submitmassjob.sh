#!/bin/csh
# example of a shell-script which does mass production of DES jobs
# this may need lots of tweaks depending on what needs to be done.
# this one ensures that only 1 job is running on deslogin
# to run it use submitmassjob.sh tile.list where tile.list contains list of tiles to be processed
# inp is the input file containing the list of catalogs
set inp = $argv[1]
echo $inp
set default = coadd_xxxx.bcs
foreach tile (`cat $inp`)
	check_run:
	#check for number jobs to see if it is less than 5
#	set runs = `condor_q -l | grep "des_run =" | sort -u | wc -l`
	set runs = `desstat|grep deslogin|wc -l `
	set tilefile = `echo $default | sed -e "s/xxxx/$tile/g"` 	
	if (($runs < 1) && !(-f $tilefile))  then
		#create submit file
		sed -e "s/xxxx/$tile/g" $default > $tilefile  
		#now submit the file
		echo "Now submitting" $tilefile
		dessubmit $tilefile
		sleep 120
		@ runs = $runs + 1 
	else
		sleep 5
	endif
 # check again if tile file exists and if not go back to begining of loop
	if (! (-f $tilefile)) then
		goto check_run
	endif  
end	
