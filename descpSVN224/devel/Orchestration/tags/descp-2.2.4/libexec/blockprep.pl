#!/usr/bin/env perl
########################################################################
#  $Id: blockprep.pl 6979 2011-07-06 19:12:46Z mgower $
#
#  $Rev:: 6979                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2011-07-06 12:12:46 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use Cwd;
use FileHandle;
use File::Copy;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::submitfunc;
use Orchestration::misc;
use Orchestration::hist;

my $DebugFH;
my $debugfile = 'blockprep.out';
open $DebugFH, "> $debugfile" or die "blockprep: Error cannot open log";

local *STDOUT = $DebugFH;
local *STDERR = $DebugFH;
$DebugFH->autoflush(1);

print "$0 @ARGV\n";
print "\tcurrent working directory = ", cwd(), "\n";


if (scalar(@ARGV) != 1) {
    print "Usage: blockprep.pl configfile\n";
    close $DebugFH;
    exit $FAILURE;
}

my (@modules_pre, @modules_post);
my $DESFile = $ARGV[0];

if (! -r $DESFile) {
    print "Error:  Cannot read config file ($DESFile)\n";
    close $DebugFH;
    exit $FAILURE;
}

my $Config = new Orchestration::desconfig({configfile=>$DESFile});
my $uberctrl = $Config->value('uberctrl_dir');
$Config->setBlockInfo();
$Config->save_file($DESFile);

my $block = $Config->value("block");

print "\tblock = $block\n";
logEvent($Config, $block,'blockprep','j','pretask');
updateBlockStart($Config);

if (! -d "../$block") {
    print "blockprep: Error cannot chdir to block subdirectory (../$block)\n";
    print "cwd = ", cwd(), "\n";
#    close $DebugFH;
#    exit $FAILURE;
    mkdir "../$block";
}

chdir "../$block" or die "Could not chdir to block subdirectory (../$block) from ".cwd();
my $blockdir = cwd();

close $DebugFH;
move("$uberctrl/$debugfile", $debugfile);
open $DebugFH, ">> $debugfile";
local *STDOUT = $DebugFH;
local *STDERR = $DebugFH;
$DebugFH->autoflush(1);

my $deshome = $Config->value("des_home");
my $project = $Config->value("project");
my $run = $Config->value("submit_run");
my $fileclass = $Config->value('fileclass');
my $runsite = $Config->value("run_site");
my $targetnode = $Config->value("target_node");
my $submitnode = $Config->value("submit_node");
my $notarget = $Config->value('notarget');
my $workdir = $Config->value('work_dir');
my $blockobj = $Config->obj('block')->obj($block);
my $dbid_block = $Config->value('dbid_block');
my %dbid_orchtasks;

my $ingest_wall = 66;

if ($Config->exists('reserve_wall')) {
   # reserve_wall overrides all job wallclocks including ingest_wall
   $ingest_wall = $Config->value('reserve_wall');
}
elsif ($Config->exists('ingest_wall')) {
   $ingest_wall = $Config->value('ingest_wall');
}

my $postscript = createPostIngestScript($Config);
createJobScript($Config);
#createTimestampScript($Config);

## create dummy DAG that runs pipeline jobs
my $dag = "pipelinesmngr.dag";
my $success = open DAG, "> $dag";
if (!$success) {
    print STDERR getTimeStamp(), "blockprep: Error Can't create pipelinesmngr dag : $!";
    exit $FAILURE;
}
print DAG <<EOF;
DOT pipelinesmngr.dot
JOB 0001 runjob.condor
SCRIPT pre 0001 $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
SCRIPT post 0001 $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN 
EOF
close DAG;
addInfoToDag($Config, $dag, \*STDOUT);


$dag = "blockmngr.dag";
my $tasklist = "";

if (! -r $dag) {
    ## create real block DAG
    $success = open DAG, "> $dag";
    if (!$success) {
        print STDERR getTimeStamp(), "blockprep: Error Can't create block dag file $dag : $!";
        exit $FAILURE;
    }
    DAG->autoflush(1);



    print DAG <<EOF;
    DOT block.dot
    JOB runqueries $deshome/share/condor/localjob.condor
    VARS runqueries deshome="$deshome" descfg="../uberctrl/config.des" 
    VARS runqueries project="$project" run="$run" runsite="$runsite"
    VARS runqueries block="$block" jobname="runqueries"
    VARS runqueries exec="\$(deshome)/libexec/runqueries.pl" args="\$(descfg) \$(Cluster)"
    SCRIPT pre runqueries $deshome/libexec/logpre.pl  ../uberctrl/config.des $block j \$JOB 
    SCRIPT post runqueries $deshome/libexec/logpost.pl ../uberctrl/config.des $block j \$JOB \$RETURN  
    
EOF
    $dbid_orchtasks{'runqueries'} = insertOrchTask($Config, $dbid_block, 'runqueries', $deshome);
    $tasklist = "runqueries";
    
    
    if (!$notarget) {
        print DAG <<EOF;
    JOB stagearchive $deshome/share/condor/localjob.condor
    RETRY stagearchive 2
    VARS stagearchive deshome="$deshome" descfg="../uberctrl/config.des" 
    VARS stagearchive project="$project" run="$run" runsite="$runsite"
    VARS stagearchive block="$block" jobname="stagearchive"
    VARS stagearchive exec="\$(deshome)/bin/arcp"
    VARS stagearchive args="-keep-logs -show-file-list -v -v -nclients 1 -rwd ${blockdir} -wdp stagearchive_tmpdir -filelist=stagearchive.list any $targetnode"
    SCRIPT pre stagearchive $deshome/libexec/logpre.pl ../uberctrl/config.des $block j \$JOB 
    SCRIPT post stagearchive $deshome/libexec/dafpost.pl ../uberctrl/config.des $block j \$JOB \$RETURN  
    
EOF
        $dbid_orchtasks{'stagearchive'} = insertOrchTask($Config, $dbid_block, 'stagearchive', $deshome);
        $tasklist .= ",stagearchive";
    }
    
    print DAG <<EOF;
    JOB blockmain $deshome/share/condor/localjob.condor
    VARS blockmain deshome="$deshome" descfg="../uberctrl/config.des" 
    VARS blockmain project="$project" run="$run" runsite="$runsite"
    VARS blockmain block="$block" jobname="blockmain"
    VARS blockmain exec="\$(deshome)/libexec/blockmain.pl"
    VARS blockmain args="\$(descfg) \$(Cluster)"
    SCRIPT pre blockmain $deshome/libexec/logpre.pl ../uberctrl/config.des $block j \$JOB 
    SCRIPT post blockmain $deshome/libexec/logpost.pl ../uberctrl/config.des $block j \$JOB \$RETURN  
    
EOF
    $dbid_orchtasks{'blockmain'} = insertOrchTask($Config, $dbid_block, 'blockmain', $deshome);
    $tasklist .= ",blockmain";
    
    
    if ($Config->exists('module_list_pre')) {
        @modules_pre = split /\s*,\s*/, $Config->value('module_list_pre');
        my $uber_module_obj = $Config->obj('module');
        foreach my $mod (@modules_pre) {
    print "Working on pre module '$mod'\n";
            if (!$uber_module_obj->exists($mod)) {
                print STDERR "Error: Could not find module '$mod'\n";
                exit $FAILURE;
            }
            my $module_obj = $uber_module_obj->obj($mod);
            my ($exec,$args);
            if ($module_obj->exists('exec')) {
                $exec = $Config->interpolate($module_obj->value('exec')); 
                if (! -x $exec) {
                    print STDERR "Error: Could not find executable for module $mod\n";
                    print STDERR "\texec = '$exec'\n";
                    exit $FAILURE;
                }
            }
            else {
                print STDERR "Error: Could not find exec value for module $mod\n";
                exit $FAILURE;
            }
    
            if ($module_obj->exists('args')) {
                $args = $Config->interpolate($module_obj->value('args')); 
            }
            else {
                print STDERR "Warning: Could not find args value for module $mod\n";
                $args="";
            }
            printf DAG <<EOF;
    JOB pre_$mod $deshome/share/condor/localjob.condor
    VARS pre_$mod deshome="$deshome" descfg="jobinfo.des" 
    VARS pre_$mod project="$project" run="$run" runsite="$runsite"
    VARS pre_$mod block="$block" jobname="pre_$mod"
    VARS pre_$mod exec="$exec" args="$args" 
    SCRIPT pre pre_$mod $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
    SCRIPT post pre_$mod $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN 
    
EOF
            $dbid_orchtasks{"pre_$mod"} = insertOrchTask($Config, $dbid_block, "pre_$mod", $deshome);
            $tasklist .= ",pre_$mod";
        }
    }
    
    if (!$notarget) {
        print DAG <<EOF;
    JOB targetsetup $deshome/share/condor/localjob.condor
    VARS targetsetup deshome="$deshome" descfg="jobinfo.des" 
    VARS targetsetup project="$project" run="$run" runsite="$runsite"
    VARS targetsetup block="$block" jobname="targetsetup"
    VARS targetsetup exec="\$(deshome)/libexec/targetsetup.pl"
    VARS targetsetup args="\$(descfg) \$(Cluster)"
    SCRIPT pre targetsetup $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
    SCRIPT post targetsetup $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN  
    
    JOB stagelocal $deshome/share/condor/localjob.condor
    RETRY stagelocal 2
    VARS stagelocal deshome="$deshome" descfg="jobinfo.des" 
    VARS stagelocal project="$project" run="$run" runsite="$runsite"
    VARS stagelocal block="$block" jobname="stagelocal"
    VARS stagelocal exec="\$(deshome)/bin/arcp"
    VARS stagelocal args="-keep-logs -show-file-list -v -v -nclients 1 -rwd ${blockdir} -wdp stagelocal_tmpdir -filelist=stagelocal.list $submitnode $targetnode"
    SCRIPT pre stagelocal $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
    SCRIPT post stagelocal $deshome/libexec/dafpost.pl jobinfo.des $block j \$JOB \$RETURN  
    
    JOB maketimestamp $deshome/share/condor/localjob.condor
    VARS maketimestamp deshome="$deshome" descfg="jobinfo.des" 
    VARS maketimestamp project="$project" run="$run" runsite="$runsite"
    VARS maketimestamp block="$block" jobname="maketimestamp"
    VARS maketimestamp exec="\$(deshome)/libexec/maketimestamp.pl"
    VARS maketimestamp args="\$(descfg) \$(Cluster)"
    SCRIPT pre maketimestamp $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
    SCRIPT post maketimestamp $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN  
    
    JOB pipelinesmngr pipelinesmngr.dag.condor.sub
    SCRIPT pre pipelinesmngr $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
    SCRIPT post pipelinesmngr $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN  
    
EOF
        $dbid_orchtasks{'targetsetup'} = insertOrchTask($Config, $dbid_block, 'targetsetup', $deshome);
        $dbid_orchtasks{'stagelocal'} = insertOrchTask($Config, $dbid_block, 'stagelocal', $deshome);
        $dbid_orchtasks{'maketimestamp'} = insertOrchTask($Config, $dbid_block, 'maketimestamp', $deshome);
        $dbid_orchtasks{'pipelinesmngr'} = insertOrchTask($Config, $dbid_block, 'pipelinesmngr', $deshome);
        $tasklist .= ",targetsetup,stagelocal,maketimestamp,pipelinesmngr";
    
        if (!$Config->exists('ingest') || $Config->value('ingest')) {
            my $targetdir = $Config->value('archive_root').'/'.$Config->value('run_dir');
            my $runtimeprefix = "$targetdir/runtime/$block";
            my $xmlprefix = "$targetdir/xml/$block";
    
            my $args = "$runtimeprefix $xmlprefix ingest 1 ingest";
    
    
            print DAG <<EOF;
    JOB findingest runjob.condor
    VARS findingest deshome="$deshome" descfg="jobinfo.des" 
    VARS findingest project="$project" run="$run" runsite="$runsite"
    VARS findingest block="$block" jobid="ingest"
    VARS findingest wall="$ingest_wall" args="$args"
    SCRIPT pre findingest $deshome/libexec/logpre.pl jobinfo.des $block j ingest
    SCRIPT post findingest $deshome/libexec/jobpost.pl jobinfo.des ingest \$RETURN  
    
    JOB postingest $postscript
    VARS postingest deshome="$deshome" descfg="jobinfo.des" 
    VARS postingest project="$project" run="$run" runsite="$runsite"
    VARS postingest block="$block" jobid="postingest"
    VARS postingest targetnode="$targetnode"
    SCRIPT pre postingest $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB
    SCRIPT post postingest $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN  
    
EOF
            $dbid_orchtasks{'findingest'} = insertOrchTask($Config, $dbid_block, 'findingest', $deshome);
            $dbid_orchtasks{'postingest'} = insertOrchTask($Config, $dbid_block, 'postingest', $deshome);
            $tasklist .= ",findingest,postingest";
        }
    
        print DAG <<EOF;
    JOB jobscheck $deshome/share/condor/localjob.condor
    VARS jobscheck deshome="$deshome" descfg="jobinfo.des" 
    VARS jobscheck project="$project" run="$run" runsite="$runsite"
    VARS jobscheck block="$block" jobname="jobscheck"
    VARS jobscheck exec="\$(deshome)/libexec/jobscheck.pl" 
    VARS jobscheck args="\$(descfg) \$(Cluster)"
    SCRIPT pre jobscheck $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
    SCRIPT post jobscheck $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN  
    
EOF
        $dbid_orchtasks{'jobscheck'} = insertOrchTask($Config, $dbid_block, 'jobscheck', $deshome);
        $tasklist .= ",jobscheck";
    
        if ($Config->exists('module_list_post')) {
            @modules_post = split /\s*,\s*/, $Config->value('module_list_post');
            my $uber_module_obj = $Config->obj('module');
            foreach my $mod (@modules_post) {
                print "Working on post module '$mod'\n";
                if (!$uber_module_obj->exists($mod)) {
                    print STDERR "Error: Could not find module '$mod'\n";
                    exit $FAILURE;
                }
                my $module_obj = $uber_module_obj->obj($mod);
                my ($exec,$args);
                if ($module_obj->exists('exec')) {
                    $exec = $Config->interpolate($module_obj->value('exec')); 
                    if (! -x $exec) {
                        print STDERR "Error: Could not find executable for module $mod\n";
                        print STDERR "\texec = '$exec'\n";
                        exit $FAILURE;
                    }
                }
                else {
                    print STDERR "Error: Could not find exec value for module $mod\n";
                    exit $FAILURE;
                }
    
                if ($module_obj->exists('args')) {
                    $args = $Config->interpolate($module_obj->value('args')); 
                }
                else {
                    print STDERR "Warning: Could not find args value for module $mod\n";
                    $args="";
                }
                printf DAG <<EOF;
    JOB post_$mod $deshome/share/condor/localjob.condor
    VARS post_$mod deshome="$deshome" descfg="jobinfo.des" 
    VARS post_$mod block="$block" run="$run" runsite="$runsite"
    VARS post_$mod exec="$exec" args="$args" 
    VARS post_$mod jobname="post_$mod" 
    SCRIPT pre post_$mod $deshome/libexec/logpre.pl jobinfo.des $block j \$JOB 
    SCRIPT post post_$mod $deshome/libexec/logpost.pl jobinfo.des $block j \$JOB \$RETURN 
    
EOF
                $dbid_orchtasks{"post_$mod"} = insertOrchTask($Config, $dbid_block, "post_$mod", $deshome);
                $tasklist .= ",post_$mod";
            }
        }
    }
    
    # output DAGMan node relationship
    
    if (!$notarget) {
        printf DAG "PARENT runqueries CHILD stagearchive\n";
        printf DAG "PARENT stagearchive CHILD blockmain\n";
    }
    else {
        printf DAG "PARENT runqueries CHILD blockmain\n";
    }
    my $lastparent="";
    if ($Config->exists('module_list_pre')) {
        printf DAG "PARENT blockmain CHILD pre_%s\n", lc($modules_pre[0]);
        for (my $j = 1; $j < $#modules_pre; $j++) {
            printf DAG "PARENT pre_%s CHILD pre_%s\n", lc($modules_pre[$j]), lc($modules_pre[$j+1]);
        }
        $lastparent = "pre_".lc($modules_pre[$#modules_pre]);
    }
    else {
        $lastparent = "blockmain";
    }
    
    
    if (!$notarget) {
        print DAG <<EOF;
    PARENT $lastparent CHILD targetsetup
    PARENT targetsetup CHILD stagelocal
    PARENT stagelocal  CHILD maketimestamp
    PARENT maketimestamp  CHILD pipelinesmngr
EOF
    
        if (!$Config->exists('ingest') || $Config->value('ingest')) {
            print DAG <<EOF;
    PARENT pipelinesmngr CHILD findingest
    PARENT findingest CHILD postingest
    PARENT postingest CHILD jobscheck
EOF
        }
        else {
            print DAG "PARENT pipelinesmngr CHILD jobscheck\n";
        }
    
        if ($Config->exists('module_list_post')) {
            printf DAG "PARENT jobscheck CHILD post_%s\n", lc($modules_post[0]);
            for (my $j = 1; $j < $#modules_post; $j++) {
                printf DAG "PARENT post_%s CHILD post_%s\n", lc($modules_post[$j]), lc($modules_post[$j+1]);
            }
        }
    }
    
    close DAG;
}
    
    

updateOrchTaskList($Config, $tasklist);
$blockobj->value('tasklist', $tasklist);
$blockobj->value('dbid_orchtasks', \%dbid_orchtasks);

logEvent($Config, $block,'blockprep','j','posttask', $SUCCESS);
chdir $uberctrl;
logEvent($Config, $block,'mngr','j','pretask');

$Config->save_file($DESFile);

exit $SUCCESS;
