#!/usr/bin/env perl
########################################################################
#  $Id: blockmain.pl 6584 2011-03-17 18:13:24Z mgower $
#
#  $Rev:: 6584                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2011-03-17 11:13:24 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::blockmain;
use Orchestration::submitfunc;
use Orchestration::hist;
use Orchestration::misc;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

print "$0 @ARGV\n";

if (scalar(@ARGV) != 2) {
    print "Usage: blockmain.pl configfile condorjobid\n";
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $CondorID = $ARGV[1];

if (! -r $DESFile) {
    print STDERR "Error: Can not open config file '$DESFile'\n";
    exit $FAILURE;
}
my $Config = new Orchestration::desconfig({configfile=>$DESFile});

my $block = $Config->value('block');

# log condor jobid
$CondorID = sprintf("%d", $CondorID);
logEvent($Config, $block, 'blockmain', 'j', 'cid', $CondorID);

if (!$Config->exists('module_list')) {
    print "Error:  No modules to run.\n";
    exit $FAILURE;
}

# read xml output of queries
readXML($Config); 

my $fullnames = {};
if (!$Config->exists('notarget') || !$Config->value('notarget') ) {
    $fullnames = parseStageOutput('stagearchive.out');
}
assignFullnames($Config, $fullnames);

if (!$Config->exists('ingest_pastrun') || $Config->value('ingest_pastrun')) {
    savePastRun($Config);
}
outputFullList($Config);
processLoops($Config);
processDivideBy($Config); 
my $numjobs = calcNumJobs($Config); 
createSublists($Config, $numjobs);
my $jobinfo = assignWork($Config, $numjobs);
calcWall($Config, $jobinfo);
outputJobInputLists($Config, $jobinfo);
outputIndependentLists($Config);
createJobDirs($block, $jobinfo);
my $jobdesfile = saveJobInfo($Config, $jobinfo, $DESFile);
$Config = new Orchestration::desconfig({configfile=>$jobdesfile});
createRunElf($Config);
createElfFiles($Config);
createPipelinesMngrDAG($Config,$jobinfo);
insertJobs($Config);
$Config->save_file($jobdesfile);
updateBlockMid($Config, $block);

exit $SUCCESS;
