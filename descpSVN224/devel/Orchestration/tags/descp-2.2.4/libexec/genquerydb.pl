#!/usr/bin/env perl
########################################################################
#  $Id: genquerydb.pl 6327 2011-01-04 15:58:36Z mgower $
#
#  $Rev:: 6327                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2011-01-04 08:58:36 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################

use strict;
use warnings;

print $0, @ARGV,"\n";
use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::desconfig;
use Orchestration::filelistfunc;
use Orchestration::misc;


my ($outputxml, $configfile, $queryfields, $modulename, $listname, $filename, $searchname) = undef;

use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'config=s'  =>  \$configfile,
    'query_fields=s' => \$queryfields,
    'module=s' => \$modulename,
    'list=s' => \$listname,
    'file=s' => \$filename,
    'search=s' => \$searchname,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($queryfields)) {
    print "Error: Must specify query_fields\n";
    exit $FAILURE;
}

if (!defined($modulename)) {
    print "Error: Must specify module\n";
    exit $FAILURE;
}

my $config = new Orchestration::desconfig({configfile=>$configfile});
my $block = $config->value('block');

if (!$config->obj('module')->exists($modulename)) {
    print "Error: module '$modulename' does not exist.\n";
    exit $FAILURE;
}
my $module_obj = $config->obj('module')->obj($modulename);

my $search_obj;
my $nickname="";
if (defined($listname)) {
    $search_obj = $module_obj->obj('list')->obj($listname);
    $nickname = $listname;
}
elsif (defined($filename)) {
    $search_obj = $module_obj->obj('file')->obj($filename);
    $nickname = $filename;
}
elsif (defined($searchname)) {
    if ($module_obj->obj('list')->exists($searchname)) {
        $search_obj = $module_obj->obj('list')->obj($searchname);
    }
    elsif ($module_obj->obj('file')->exists($searchname)) {
        $search_obj = $module_obj->obj('file')->obj($searchname);
    }
    else {
        print "Error: Could not find either list or file by name $searchname in module $modulename\n";
        exit $FAILURE;
    }
    $nickname = $searchname;
}
else {
    print "Error: need to define either list or file or search\n";
    exit $FAILURE;
}

my $curr_run = $config->value('run');
my ($curr_date, $curr_rest) = split /_/, $config->value('run');

$queryfields = lc($queryfields);
my @fields = split /\s*,\s*/, $queryfields;

my %key_vals = ();

if ($config->exists('query_run') &&
    $search_obj->exists('fileclass') &&
    $config->exists('fileclass') &&
    $search_obj->value('fileclass') eq $config->value('fileclass')) {
    my $query_run = $config->value('query_run');
    if (lc($query_run) eq 'current') {
        push(@fields, 'run');
    }
    elsif (lc($query_run eq 'allbutfirstcurrent')) {
        if (!$config->exists('current')) {
            print "Internal Error:  Current object doesn't exist\n";
            exit $FAILURE;
        }
        elsif (!$config->obj('current')->exists('block_num')) {
            print "Internal Error:  current->block_num doesn't exist\n";
            exit $FAILURE;
        }
        else {
            my $block_num = $config->obj('current')->value('block_num');
            if ($block_num > 0) {
                push(@fields, 'run');
            }
        }
    }
}

my %query = ();
#$query{'location'}->{'key_vals'} = \%key_vals;
foreach my $f (@fields) {
    my $value = "";
    if ($search_obj->exists($f)) {
        $value = $search_obj->value($f);
    }
    elsif ($module_obj->exists($f)) {
        $value = $module_obj->value($f);
    }
    elsif ($config->exists($f)) {
        $value = $config->value($f);
    }
    else {
        print STDERR "Error: blockmain could not find value for query field $f\n";
        exit $FAILURE;
    }

    $value = $config->interpolate($value);

    # make an array ref if list of values
    if ($value =~ /,/) {
        my @arr = split /\s*,\s*/, $value;
        $value = \@arr;
    }

    if ($f =~ /\./) {
        my ($l, $r) = split /\./, $f;
        $query{$l}->{'key_vals'}->{$r} = $value;
    }
    else {
        $query{'location'}->{'key_vals'}->{$f} = $value;
    }
}


# Always get all location table fields
$query{'location'}->{'select_fields'} = 'all';


# if specified, insert join into query hash
if ($search_obj->exists('join')) {
    my $join = $search_obj->value('join');
    my @joins = split /\s*,\s*/, $join;
    foreach my $j (@joins) {
        if ($j =~ /(\S+)\.(\S+)\s*=\s*(\S+)/) {
            my $jtable = $1;
            my $jfield = $2;
            my $rfield = $3;
            $query{$jtable}->{'join'}->{$jfield} = $rfield;
        }
    }
}

# check output fields for fields from other tables.
if ($search_obj->exists('output_fields')) {
    my $outfields = lc($search_obj->value('output_fields'));
    my @outfarr = split /\s*,\s*/, $outfields;
    foreach my $of (@outfarr) {
        if ($of =~ /(\S+)\.(\S+)/) {
            my $table = $1;
            my $field = $2;
            if ($table !~ /location/i) {
                push(@{$query{$table}->{'select_fields'}}, $field); 
            }
        }
        
    }
}

print "Calling getFileList with the following query\n";
print Dumper(\%query), "\n";

my $files = getFileList(\%query);

if (scalar(keys %$files) == 0) {
    print STDERR "blockmain-queryDB: query returned zero results\n";
    print STDERR "Aborting\n\n";
#    print STDERR "query = \n";
#    print STDERR Dumper(\%query), "\n";
    exit $FAILURE;
}


## if asked, parse values from filenames
#    set up pattern outside loop
my @vars = ();
my $parsename = undef;
if ($search_obj->exists('parsename')) {
    $parsename = $search_obj->value('parsename');

    while ($parsename =~ /\$\{(\w+)\}/) {
        my $var = $1;
        $parsename =~ s/\$\{$var\}/\(\\S+\)/;
        push(@vars, $var);
    }
}


while (my ($fname, $fhref) = each (%$files)) {
    if (defined($parsename)) {
        my @vals = $fhref->{'filename'} =~ m/$parsename/;
        for (my $i=0; $i < scalar(@vals); $i++) {
            my $var = $vars[$i];
            if (!defined($fhref->{$var}) || $fhref->{$var} !~ /\S/ || $fhref->{$var} == 0) {
                if (lc($var) eq 'ccd') {
                    $fhref->{$var} = sprintf("%02d", $vals[$i]);
                }
                else {
                    $fhref->{$var} = $vals[$i];
                }
#print "Saving $var ", $vals[$i], "\n";
            }
            else {
                print "Var already exists: $var '", $fhref->{$var}, "'\n";
            }
        }
    }
}

## output xml list
my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);
