#!/usr/bin/perl
########################################################################
#  $Id: makerundir.pl 2436 2008-11-18 15:52:47Z mgower $
#
#  $Rev:: 2436                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2008-11-18 09:52:47 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################


# Minimally requires project, fileclass, run
# Assumes find in path

use strict;
use warnings;

use Data::Dumper;
use File::Path;
use POSIX qw(strftime);
use File::Basename;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::FileUtils;
use Orchestration::desconfig;
use Orchestration::misc;
use Orchestration::hist;
use DAF::Transfer;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (scalar(@ARGV) != 2) {
    print "Usage: targetsetup.pl configfile condorjobid\n";
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $CondorID = $ARGV[1];

if (! -r $DESFile) {
    print STDERR "Error: Can not open config file '$DESFile'\n";
    exit $FAILURE;
}
my $config = new Orchestration::desconfig({configfile=>$DESFile});
my $block = $config->value("block");

# log condor jobid
$CondorID = sprintf("%d", $CondorID);
logEvent($config, $block, 'makerundir', 'j', 'cid', $CondorID);

makeRunDirs($config);
#createStampFile($block);
copyRunElf($config);
ingestSubmit($config);

exit $SUCCESS;


######################################################################
sub makeRunDirs {
    my ($config) = @_;

    print "makeRunDirs - BEG\n";
    my $rundir = $config->value("run_dir");
    my $archiveroot = $config->value("archive_root");
    my $basedir = $archiveroot ."/".$rundir;
    print "\trundir = $rundir\n";
    print "\tarchiveroot = $archiveroot\n";
    print "\tbasedir = $basedir\n";

    my @dirarray = ();

    # create log and runtime dirs
    push (@dirarray, $basedir."/log");
    push (@dirarray, $basedir."/runtime");
    push (@dirarray, $basedir."/runtime/$block");

    # create runtime dirs
    my $jobobj = $config->obj("job");
    my @jobarray = $jobobj->keys();
    foreach my $j (sort @jobarray) {
        print "\t--- Adding directory for job $j ---\n";
        my $dir = $basedir . "/runtime/".$block."_".$j;
        push (@dirarray, $dir);
    }

    # create ingest dir
    my $dir = $basedir . "/runtime/${block}_ingest";
    push (@dirarray, $dir);


    # create any dirs needed by modules
    my $uber_module_obj = $config->obj('module');
    my $module_list = $config->value('module_list');
    my @module_array = split /\s*,\s*/, $module_list;
    foreach my $module_name (@module_array) {
        print "\tChecking $module_name for directories to make\n";
        if (! $uber_module_obj->exists($module_name)) {
            print "\tError: Could not find module description for module $module_name\n";
            exit $FAILURE;
        }
        my $module_obj = $uber_module_obj->obj($module_name);
        if ($module_obj->exists('mkdirs')) {
            my $mkdirs = $module_obj->value('mkdirs');
            my @mkdirs_list = split /\s*,\s*/, $mkdirs;
            foreach my $dir (@mkdirs_list) {
                if ($dir =~ /\$\{/) {
                    $dir = $config->interpolate($dir, {currentvals=>{module=>$module_name}});
                }
                push (@dirarray, $dir);
            }
        }
    }

    my %cmdargs = ();
    $cmdargs{"target_server"} = $config->value("gridftp_host");
    if ($config->exists("gridftp_port")) {
        $cmdargs{"target_port"} = $config->value("gridftp_port");
    }

    print "--- dirarray ---\n";
    foreach my $d (@dirarray) {
        print "\t$d\n";
    }
    print "\n";

    my $submitnode = $config->value('submit_node');
    my $targetnode = $config->value('target_node');

    if ($submitnode eq $targetnode) {
        print "\tSubmit and target same node. Using local mkpath commands\n";
        foreach my $d (@dirarray) {
            print "\t$d\n";
            eval { mkpath("$d") };
            if ($@) {
                print "Error\nCouldn’t create $d: $@\n";
                exit $FAILURE;
            }
        }
    }
    else {
        print "\tCalling createRemoteDirs\n";
        print "--- cmdargs ---\n";
        foreach my $k (keys %cmdargs) {
            print "\t$k = ", $cmdargs{$k}, "\n";
        }
        print "\n";
        my $tries = 0;
        my $maxtries = 5;
        my $stat = 1;
        while ($stat and $tries < $maxtries) {
            my $transfer_obj = new DAF::Transfer();
            $tries++;
            eval {                          # try
                $transfer_obj->createRemoteDirs(\@dirarray, \%cmdargs);
                $stat = 0;
                1;
            } or do {                       # catch
                print "Error: $@\n";
                $stat = 1;
                if ($tries < $maxtries) {
                    print "Retrying createRemoteDirs\n";
                
                }
                else {
                    print "Could not create remote dirs.   Aborting\n";
                }
            };
        }
    }
    print "makeRunDirs - END\n\n";
}

######################################################################
#sub createStampFile {
#    my ($block) = @_;
#
#    print "createStampFile - BEG\n";
#    my $now_string = strftime "%a %b %e %H:%M:%S %Y", localtime;
#
#    open FH, "> ${block}_timestamp.txt";
#    print FH "$now_string\n";
#    close FH;
#    print "createStampFile - END\n\n";
#}

######################################################################
sub ingestSubmit {
    my ($config) = @_;

    print "ingestSubmit - BEG\n";
    my $rundir = $config->value('run_dir');
    my $submitpath = $config->value('work_dir');
    my $newerstr = "-newer $submitpath/runtime/$block/blockmngr.dag";
    my $ingestfile = "stagelocal.list";
    
    my $findcmd = "cd $submitpath; find . $newerstr -type f -print";
    print "\tfindcmd = $findcmd\n";
    my $findout = `$findcmd 2>&1`;
    my $findstat = $?;
    my ($fexit, $fstr) = exitCodeInfo($findstat);
    $findout =~ s/\n/\n\t\t/g;
    print "\tfindout = $findout\n";
    print "\tfindstat = $findstat, ($fexit, $fstr)\n";

    if ($findstat == 0) {
        my @files = split /\n/, $findout;
    
        my $FH;
        open $FH, ">> $ingestfile";
    
        foreach my $f (@files) {
            $f =~ s/\s+//g;
            if (($f =~ /\S/) && (($f !~ /runtime/) || ($f =~ /timestamp/) || ($f =~ /run_elf.sh/))) {
                $f =~ s/^\.\///;
                my ($name,$path,$suffix) = fileparse($f);
                $path =~ s/\/$//;
    
#                print "\tCalling filenameResolve($rundir/$f)\n";
#                my $pieces = filenameResolve($rundir.'/'.$f);
    #print Dumper($pieces),"\n";
    
                print $FH "<file>\n";
                print $FH "\tfileid=0\n";
                print $FH "\tlocalpath=$submitpath/$path\n";
                print $FH "\tlocalfilename=$name\n";
#                while (my ($key, $val) = each(%$pieces)) {
#                    if (defined($val) && $val =~ /\S/) { 
#                        print $FH "\t",lc($key),"=$val\n";
#                    }
#                }
    
                print $FH "</file>\n";
            }
        }
        close $FH;

        my $deshome = $config->value("des_home");
        my $verbose = 3;
        my $submitnode = $config->value('submit_node');
        my $ingestcmd = "$deshome/bin/file_ingest.pl -filelist $ingestfile -verbose $verbose -archivenode $submitnode ";
        print "\tingestcmd = $ingestcmd\n";
        my $ingestout = `$ingestcmd 2>&1`;
        my $ingeststat = $?;
        my ($iexit, $istr) = exitCodeInfo($ingeststat);
        $ingestout =~ s/\n/\n\t\t/g;
        print "\tingestout = $ingestout\n";
        print "\tingeststat = $ingeststat ($iexit, $istr)\n";
        if (($ingeststat != 0) || ($ingestout =~ /STATUS5/i)) {
            exit $FAILURE;
        }
    }
    else {
        print STDERR "findcmd = $findcmd\n";
        print STDERR "findout = $findout\n";
        print STDERR "findstat = $findstat ($fexit, $fstr)\n";
        exit $FAILURE;
    }
    print "ingestSubmit - END\n\n";
}

######################################################################
# DAF doesn't keep execute permissions
sub copyRunElf {
    my ($config) = @_;

    print "copyRunElf - BEG\n";
    my $submitnode = $config->value('submit_node');
    my $targetnode = $config->value('target_node');

    if ($submitnode eq $targetnode) { 
        print "\tSkipping copy.  Submit and target same node.\n";
    }
    else {
        my $block = $config->value('block');
        my $chmodcmd = "uberftp ";
        if ($config->exists('gridftp_port')) {
            $chmodcmd .= "-P ".$config->value('gridftp_port');
        }
        my $gridftp_host = $config->value('gridftp_host');
        if (!defined($gridftp_host)) {
            print "ERROR: Could not find gridftp_host for archive_node ".$config->value('target_node')."\n";
            exit $FAILURE;
        }
        my $targetdir = $config->value('archive_root').'/'.$config->value('run_dir').'/runtime/'.$block;
        my $submitdir = $config->value('work_dir').'/runtime/'.$block;
        $chmodcmd .= " $gridftp_host \"cd $targetdir; lcd $submitdir; put run_elf.sh; chmod 744 run_elf.sh; ls run_elf.sh\"";
        print "\tchmodcmd = $chmodcmd\n";
        my $chmodout = `$chmodcmd 2>&1`;
        my $chmodstat = $?;
        my ($cexit, $cstr) = exitCodeInfo($chmodstat);
        $chmodout =~ s/\n/\n\t\t/g;
        print "\tchmodout = $chmodout\n";
        print "\tchmodstat = $chmodstat ($cexit, $cstr)\n";
        if ($chmodstat != 0) {
            exit $FAILURE;
        }
    }
    print "copyRunElf - END\n\n";
}
