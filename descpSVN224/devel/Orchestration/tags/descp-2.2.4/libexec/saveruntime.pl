#!/usr/bin/env perl
########################################################################
#  $Id: saveruntime.pl 3263 2009-03-06 19:51:04Z mgower $
#
#  $Rev:: 3263                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-03-06 13:51:04 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#   Ingest the main submit runtime dir (run/runtime) and replicate
#   it if requested.
#
########################################################################
use strict;
use warnings;

use Data::Dumper;
use Cwd;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::hist;
use Orchestration::misc;
use Orchestration::email;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (scalar(@ARGV) != 3) {
    print "Usage: saveruntime.pl configfile dagid condorjobid\n";
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $DagID = $ARGV[1];
my $CondorID = $ARGV[2];

if (! -r $DESFile) {
    print STDERR "Error: Can not open config file '$DESFile'\n";
    exit $FAILURE;
}
my $config = new Orchestration::desconfig({configfile=>$DESFile});
my $run = $config->value('submit_run');
my $deshome = $config->value('des_home');
my $runtimedir = $config->value('work_dir').'/runtime';
my $submitnode = $config->value('submit_node');
my $verbose = $config->value('verbose');

my $msg1 = "";
my $msg2 = "";
my $exit = $SUCCESS;

# wait until main dag is complete
my $out = `condor_q $DagID 2>&1`;
while ($out =~ /$DagID/) {
    sleep(60);
    $out = `condor_q $DagID 2>&1`;
}

# Wait for a little bit more to make sure Condor is done with the runtime dir.
sleep(120);

my $cwd = cwd();

# ingest runtime dir
print "Ingesting runtime dir on submit machine\n";
my $ingestCmd = "$deshome/bin/find_ingest.pl -ingestfile ${run}_ingest.list -archivenode ${submitnode} -verbose ${verbose} -cleanup ${runtimedir}";
print "$ingestCmd\n";
print "See find_ingest.log for output\n";
`$ingestCmd > find_ingest.log 2>&1`;
my $ingeststat = $?;

if ($ingeststat != 0) {
    my ($ingestexit, $ingeststr) = exitCodeInfo($ingeststat);
    $msg2 .= "Error:  ingestion of the runtime dir exited with non-zero exit code ($ingestexit, $ingeststr).\n    See $cwd for logs.\n";
    print STDERR "Error:  ingestion of the runtime dir exited with non-zero exit code ($ingestexit, $ingeststr).\n    See $cwd for logs.\n";
    $exit=$FAILURE; 
}

if ($config->exists('replica_nodes')) {
    print "\n\nCopying runtime dir to other archive nodes: ", $config->value('replica_nodes'), "\n";
    my $msg;
    ($exit, $msg) = replicateRun($config, $run, $deshome, $runtimedir);
    $msg2 .= $msg;
}
else {
    print "\n\nOperator did not specify replica_nodes, so don't need to copy runtime dir to other archive nodes.\n";
}


# send final email
my $subject = "";
if ($exit == $SUCCESS) {
    $msg1 = "End run tasks have completed successfully.\nSee $cwd for saveruntime logs.";
    $subject = "";
}
else {
    $msg1 = "End run tasks have not completed successfully.\nSee $cwd for saveruntime logs.";
    $subject = "[FAILED]";
}
sendEmail($config, "endrun", $exit, $subject, $msg1, $msg2);

exit $exit;

sub replicateRun {
    my ($config, $run, $deshome, $runtimedir) = @_;
    my $msg = "";
    my $cwd = cwd();
    my $listnodes = $config->value('replica_nodes');
    foreach my $node (split /\s*,\s*/, $listnodes) {
        my $exit = $FAILURE;
        my $try = 1;
        while ($exit && ($try < 4)) { 
            my $cpcmd = "${deshome}/bin/arcp -keep-logs -v -v -nclients 1 -rwd $cwd -wdp ${run}_replicate_${node}_${try} -run ${run} any ${node}";
            `$cpcmd > ${run}_replicate_${node}_${try}.out 2>&1`;
            my $cpstat = $?;
            if ($cpstat != 0) {
                my ($cpexit, $cpstr) = exitCodeInfo($cpstat);
                $msg .= "Error:  arcp to archive node $node exited with non-zero exit code ($cpexit, $cpstr).\n    See /tmp/${run}_replicate_${node}_${try} for logs.\n";
                print STDERR "Error:  arcp to archive node $node exited with non-zero exit code ($cpexit, $cpstr)\n    See /tmp/${run}_replicate_${node}_${try} for logs.\n";
                $exit=$FAILURE; 
            }
            else {
                $exit=$SUCCESS;
            }
            $try++;
        }
    }
    return ($exit, $msg);
}
