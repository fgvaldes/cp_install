#!/usr/bin/env perl
########################################################################
#  $Id: forksaveruntime.pl 3724 2009-07-15 21:06:29Z mgower $
#
#  $Rev:: 3724                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-07-15 16:06:29 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use File::Path;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::misc;
use Orchestration::hist;


if (scalar(@ARGV) != 2) {
    print "Usage: forksaveruntime.pl configfile condorjobid\n";
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $CondorID = $ARGV[1];

my $config = new Orchestration::desconfig({configfile=>$DESFile});

# log condor jobid
$CondorID = sprintf("%d", $CondorID);
my $block = $config->value("block");
logEvent($config, $block, 'forksaveruntime', 'j', 'cid', $CondorID);
my $run = $config->value("submit_run");


# create working directory in /tmp...not ingesting post output and logs
my $tmpdir = "/tmp/${run}_saveruntime";
if (! -d $tmpdir) {
    print "Making post directory...";
    eval { mkpath($tmpdir) };
    if ($@) {
        print "Error\nCouldn’t create $tmpdir: $@\n";
        exit $FAILURE;
    }
    else {
        print "done\n";
    }
}

chdir $tmpdir;

my $dagid = $ENV{'CONDOR_ID'};
my $deshome = $config->value('des_home');
my $project = $config->value('project');
my $submitnode = $config->value('submit_node');
my $uberdir = $config->value('uberctrl_dir');

# create condor file
open FH, "> $tmpdir/saveruntime.condor" or die "Error:  Cannot open $tmpdir/saveruntime.condor";
print FH <<EOF;
universe=local
executable=$deshome/libexec/saveruntime.pl
arguments = $uberdir/$DESFile $dagid \$(Cluster)
getenv=true
initialdir=$tmpdir
notification=never
output=$tmpdir/saveruntime.out
error=$tmpdir/saveruntime.err
log=$tmpdir/saveruntime.log
+des_isdesjob=TRUE
+des_project="$project"
+des_run="$run"
+des_runsite="$submitnode"
+des_block="saveruntime"
+des_subblock=""
queue

EOF
close FH;

my $cmd = "condor_submit $tmpdir/saveruntime.condor";
my $out = `$cmd 2>&1`;
my $stat = $?;
print "$cmd\n";
print "$out\n";
my ($exit, $str) = exitCodeInfo($stat);
print "condor_submit exit code: $stat, ($exit, $str)\n";

exit $exit;
