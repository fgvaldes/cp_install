#!/usr/bin/env perl
########################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

use strict;
use warnings;

my $debugfh;
open $debugfh, "> logpre.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;
$debugfh->autoflush(1);
print "$0 @ARGV\n";

use File::Copy;
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::desconfig;

my ($configfile, $config);
my ($block, $subblocktype, $subblock);
if (scalar(@ARGV) >= 4)
{
  $configfile = $ARGV[0];
  $block = $ARGV[1];
  $subblocktype = $ARGV[2];
  $subblock = $ARGV[3];
}
else
{
  print "Usage: logpre configfile block subblocktype subblock\n";
  close $debugfh;
  exit $FAILURE;
}

close $debugfh;
move("logpre.out", "logpre.$subblock.out");
open $debugfh, ">> logpre.$subblock.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;
$debugfh->autoflush(1);


# read sysinfo file
$config = new Orchestration::desconfig({configfile=>$configfile});

logEvent($config, $block, $subblock, $subblocktype, 'pretask');

if ($config->exists('dbid_orchtasks')) {
    my $dbid_orchtasks = $config->value('dbid_orchtasks');
    if (exists($dbid_orchtasks->{$subblock})) {
        my $dbid = $dbid_orchtasks->{$subblock};
        updateOrchTaskSubmit($config, $subblock, $dbid);
    }
    else {
        print "Could not find dbid for orchtask $subblock\n";
    }
}
else {
    print "dbid_orchtasks doesn't exist in config\n";
}

close $debugfh;

exit $SUCCESS;
