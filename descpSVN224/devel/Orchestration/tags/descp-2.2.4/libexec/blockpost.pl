#!/usr/bin/env perl
########################################################################
#  $Id: blockpost.pl 4383 2009-09-24 16:05:40Z mgower $
#
#  $Rev:: 4383                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-09-24 11:05:40 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use FindBin;
use File::Copy;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::email;

my $debugfh;
open $debugfh, "> blockpost.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;

print "$FindBin::Bin/blockpost.pl ";
foreach (@ARGV) {
    print $_, " ";
}
print "\n";


if (scalar(@ARGV) < 1) {
    print "Usage: blockpost.pl configfile status\n";
    close $debugfh;
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $status = $FAILURE;
my ($msg1, $msg2);

if (scalar(@ARGV) == 2) {
    $status = $ARGV[1];
    # dagman always exits with values 0 or 1
    if ($status == 1) {
       $status = $FAILURE; 
    }
}

my $config = new Orchestration::desconfig({configfile=>$DESFile});

my $run = $config->value('submit_run');
my $block = $config->value('block');

close $debugfh;
move "blockpost.out", "blockpost_$block.out";
open $debugfh, ">> blockpost_$block.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;

my $notarget = $config->value('notarget');

if ($status == $FAILURE) {
    print "Block failed\nAlready sent email\n";  
} 
elsif ($notarget) {
    print "Sending notarget email\n";
    $msg1 = "$run:  In notarget mode, block $block has finished successfully.";
    $msg2 = "";
    sendEmail($config, $block, $SUCCESS, "[NOTARGET]", $msg1, $msg2);
    $status = $NOTARGET;
}
elsif (-r "../$block/JOBS_WARNINGS.txt") {
    print "JOBS_WARNINGS.txt exists.  Sending warning email\n";
    $msg1 = "$run:  At least one job in block $block has status4 messages."; 
    $msg2 = "";

    my %jobid;
    open FH, "< ../$block/JOBS_WARNINGS.txt";
    while (my $j = <FH>) {
        chomp($j); 
        $jobid{$j} = 1;
    }
    close FH; 
    $msg2 .= "Check the following jobs:\n";
    foreach my $j (sort keys %jobid) {
        $msg2 .= "\t$j\n";
    }
    close FH;
    $msg2 .= "\n\n".getJobInfo($block);

    sendEmail($config, $block, $WARNINGS, "[WARNINGS]", $msg1, $msg2);
    $status = $SUCCESS;
}
elsif ($status == $SUCCESS) {
    print "Sending success email\n";
    $msg1 = "$run:  block $block has finished successfully.";
    $msg2 = "";
    $msg2 .= "\n\n".getJobInfo($block);

    sendEmail($config, $block, $status, "", $msg1, $msg2);
}
else {
    print "Not sending block email\n";
    print "status = $status\n";
    if (-r "../$block/JOBS_FAILED.txt") {
        print "../$block/JOBS_FAILED.txt exists, so email should have already been sent\n";
    }
}

# Store values in DB and hist file 
updateBlockEnd($config, $status);
logEvent($config, $block, 'mngr', 'j', 'posttask', $status);

if ($status == $SUCCESS) {
    # Get ready for next block
    $config->incBlock();
    print "After blocknum = ", $config->value("block_num"), "\n";
    $config->save_file($DESFile);
}

close $debugfh;

exit $status;
