#!/usr/bin/env perl
########################################################################
#  $Id: summary.pl 4774 2009-12-01 18:51:04Z mgower $
#
#  $Rev:: 4774                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-12-01 11:51:04 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::email;

my $ConfigFile;
my $status = undef;

my $DebugFH;
open $DebugFH, "> summary.out";
local *STDOUT = $DebugFH;
local *STDERR = $DebugFH;

if (scalar(@ARGV) < 1) {
    print "Usage: summary.pl configfile status\n";
    close $DebugFH;
    exit $FAILURE;
}

$ConfigFile = $ARGV[0];
debug("summary", "Begin");
if (scalar(@ARGV) > 1) {
    $status = $ARGV[1];
    # dagman always exits with 0 or 1
    if ($status == 1) {
        $status = $FAILURE;
    }
}
else {
    debug("summary", "Missing status value");
}


# read sysinfo file
my $Config = new Orchestration::desconfig({configfile=>$ConfigFile});

logEvent($Config, 'process', 'mngr', 'j', 'posttask', $status);

my $str = "";
$str = "End of run tasks (replicating data, ingesting submit runtime, etc) are starting.";

my $Msg1 = "";
my $Msg2 = "";
my $subject = "";
if (!defined($status)) {
    $Msg1 = "Processing finished with unknown results.\n$str";
    $subject = "[Unknown]";
    $status = $FAILURE;
    $status = updateRunStatus($Config, $status);
}
else {
    $status = updateRunStatus($Config, $status);

    if ($status == $SUCCESS) {
        $str = "End of run tasks (replicating data, ingesting submit runtime, etc) are starting.";
        $Msg1 = "Processing is complete.\n$str"; 
        $subject = "";
    }
    else {
        $Msg1 = "Processing aborted.\n"; 
        $subject = "[FAILED]";
    }
}
#my $RunID = $Config->getValueReq("runid");
#my $Msg2 = `$DESHome/bin/desstat -l $Nite $RunID 2>&1`;
sendEmail($Config, "processing", $status, $subject, $Msg1, $Msg2);

debug("summary", "status = '$status'");
debug("summary", $Msg1);
debug("summary", "End");
close $DebugFH;

exit $status;

