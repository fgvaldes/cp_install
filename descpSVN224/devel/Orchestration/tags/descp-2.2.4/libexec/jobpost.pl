#!/usr/bin/env perl
########################################################################
#  $Id: jobpost.pl 6584 2011-03-17 18:13:24Z mgower $
#
#  $Rev:: 6584                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-03-17 11:13:24 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

use strict;
use warnings;

my $debugfh;
open $debugfh, "> jobpost.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;

print "$0 @ARGV\n";

use Data::Dumper;
use Fcntl qw(:flock);
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::email;
use Orchestration::desconfig;

my ($configfile, $config, $jobid);
my $retval = '';

if (scalar(@ARGV) >= 2) {
  $configfile = $ARGV[0];
  $jobid = $ARGV[1];
}
else {
  print "Usage: jobpost configfile jobid retval\n";
  close $debugfh;
  exit $FAILURE;
}

if (scalar(@ARGV) == 3) {
    $retval = $ARGV[2];
}


# read sysinfo file
$config = new Orchestration::desconfig({configfile=>$configfile});
my $block = $config->value('block');

close $debugfh;
my $dfile = "../${block}_tjobs/$jobid.jobpost.out";
open $debugfh, "> $dfile" or die "Cannot open debug file ($dfile)";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;

logEvent($config, $block, $jobid, 'j', 'posttask', $retval);
logEvent($config, $block, $jobid, 'c', 'pretask');

my $file = "../${block}_tjobs/$jobid.pipeline.log";
print "Checking file '$file'\n";
if (! -s $file) {
    print "Error: Could not find pipeline log file: $file\n";
    close $debugfh;
    exit $FAILURE;
}

my $out = `grep -E PIPELINE $file 2>&1`;
#print "Results of grep: \n$out\n";

my %modinfo = ();
my %jobinfo = ();
my %foundmod = ();
my $failreason = "";
my $finished = 0;
my $started = 0;

my @jobids = ();
my $module_list = $config->value('module_list');
print "module_list = $module_list\n";

# if ingest job, only ingest module
if ($jobid =~ /ingest/i) {
    $module_list = 'ingest';
    push (@jobids, 'ingest');
}
elsif ($config->exists('tjobinfo')) {
    my $tjobinfo = $config->value('tjobinfo');
    my $tjname = lc($jobid);
    if (!defined($tjobinfo->{$tjname})) {
        print "Could not find information for target job $jobid inside tjobinfo hash\n";
    }
    elsif (defined($tjobinfo->{$tjname}{'start_jobid'}) && defined($tjobinfo->{$tjname}{'end_jobid'})) {
        my ($startjobid, $endjobid);
        $startjobid = $tjobinfo->{$tjname}{'start_jobid'};
        $endjobid = $tjobinfo->{$tjname}{'end_jobid'};
        for (my $j = $startjobid; $j <= $endjobid; $j++) {
            push(@jobids, sprintf("%04d", $j));
        } 
    }
}

my @module_array = split /\s*,\s*/, $module_list;

print "initializing jobinfo for jobs = ";
foreach my $j (@jobids) {
    print "$j ";
    $jobinfo{$j}{'runelfbeg'} = 0;
    $jobinfo{$j}{'runelfend'} = 0;
    $jobinfo{$j}{'elfbeg'} = 0;
    $jobinfo{$j}{'elfend'} = 0;
    $jobinfo{$j}{'elfstatus'} = -1;
    $jobinfo{$j}{'status4'} = 0;
    $jobinfo{$j}{'status5'} = 0;
    $jobinfo{$j}{'msg'} = ' ';

    foreach my $m (@module_array) {
        $modinfo{$j}{$m}{'status4'} = 0;
        $modinfo{$j}{$m}{'status5'} = 0;
        $modinfo{$j}{$m}{'msg'} = ' ';
    }
}
print "\n";

my %tjobinfo;

my @info = split /\n/,$out;
foreach my $i (@info) {
    my ($k, $v) = $i =~ /PIPELINE\s+(\w+)\s+(.+)$/;
    $k = lc($k);
    print "Found PIPELINE key/value pair: $k '$v'\n";
    if ($k =~ /batchbeg/i) {
        $started=1;
        $tjobinfo{'batchbeg'} = $v;
    }
    elsif ($k =~ /batchend/i) {
        $finished=1;
        $tjobinfo{'batchend'} = $v;
    }
    elsif ($k =~ /batchhost/i) {
        $tjobinfo{'batchhost'} = $v;
    }
    elsif ($k =~ /batchid/i) {
        $tjobinfo{'batchid'} = $v;
    }
    elsif ($k =~ /runelf/i) {
        if ($v =~ /(\S+)\s+(\S+)\s+(\S+)/) {  # desjob_name beg/end/status value
            if (lc($2) eq 'beg') {
                $jobinfo{$1}{'runelfbeg'} = $3;
            }
            elsif (lc($2) eq 'end') {
                $jobinfo{$1}{'runelfend'} = $3;
            }
            elsif (lc($2) eq 'status') {
                $jobinfo{$1}{'status'} = $3;
            }
            else {
                print "unknown runelf status ($2)\n";
            }
        }
    }
    elsif ($k =~ /elfbegin/i) {
        $jobinfo{$v}{'elfbeg'} = 1;
        $jobinfo{$v}{'elfend'} = 0;
        $jobinfo{$v}{'status4'} = 0;
        $jobinfo{$v}{'status5'} = 0;
    }
    elsif ($k =~ /elfend/i) {
        $jobinfo{$v}{'elfbeg'} = 1;
        $jobinfo{$v}{'elfend'} = 1;
    }
    elsif ($k =~ /elfstatus/) {
        my @sinfo = split /\s+/, $v;
        $jobinfo{$v}{'elfstatus'} = $sinfo[1];
    }
    elsif ($k =~ /modstatus/i) {
        my ($j, $mod, $stat) = $v =~ m/(\S+)\s+(\S+)\s+(.+)$/;
        if ($stat =~ /success/i) {
            if ((defined($modinfo{$j}{$mod}{'status5'})) && ($modinfo{$j}{$mod}{'status5'} > 0)) {
                $modinfo{$j}{$mod}{'msg'} = 'FAILURE';
            }
            elsif ((defined($modinfo{$j}{$mod}{'status4'})) && ($modinfo{$j}{$mod}{'status4'} > 0)) {
                $modinfo{$j}{$mod}{'msg'} = 'WARNINGS';
            }
            else {
                $modinfo{$j}{$mod}{'msg'} = 'SUCCESS';
            }
        }
        else {
            $modinfo{$j}{$mod}{'msg'} = $stat;
        }
        $jobinfo{$j}{'elfbeg'} = 1;
    }
    elsif ($k =~ /cntStatus4/i) {
        my ($j, $mod, $cnt) = $v =~ m/(\S+)\s+(\S+)\s+(.+)$/;
        $modinfo{$j}{$mod}{'status4'} = $cnt;
        if (($cnt > 0) && ($modinfo{$j}{$mod}{'msg'} =~ /success/i)) {
            $modinfo{$j}{$mod}{'msg'} = 'WARNINGS';
        }
        $jobinfo{$j}{'elfbeg'} = 1;
    }
    elsif ($k =~ /cntStatus5/i) {
        my ($j, $mod, $cnt) = $v =~ m/(\S+)\s+(\S+)\s+(.+)$/;
        $modinfo{$j}{$mod}{'status5'} = $cnt;
        if (($cnt > 0) && ($modinfo{$j}{$mod}{'msg'} =~ /success/i)) {
            $modinfo{$j}{$mod}{'msg'} = 'FAILURE';
        }
        $jobinfo{$j}{'elfbeg'} = 1;
    }
    elsif ($k =~ /totStatus4/i) {
        my ($j, $cnt) = $v =~ m/(\S+)\s+(.+)$/;
        $jobinfo{$j}{'status4'} = $cnt;
        $jobinfo{$j}{'elfbeg'} = 1;
    }
    elsif ($k =~ /totStatus5/i) {
        my ($j, $cnt) = $v =~ m/(\S+)\s+(.+)$/;
        $jobinfo{$j}{'status5'} = $cnt;
        $jobinfo{$j}{'elfbeg'} = 1;
    }
    logEvent($config, $block, $jobid, 'j', $k, $v);
}


if (!$started) {
    open FH, ">> JOBS_FAILED.txt" || die("Cannot open file JOBS_FAILED.txt");
    flock(FH, LOCK_EX);
    print FH "DIDNT_START ($jobid)";
    close FH;
    close $debugfh;
    exit $FAILURE;
}

if (scalar(@jobids) == 0) {
    if (scalar keys %jobinfo > 0) {
        print "Could not determine DES jobids from jobinfo.des\n";
        foreach my $j (sort keys %jobinfo) {
            push (@jobids, $j);
        }
    }
    else {
        print "Cannot determine DES jobids inside this job\n";
        open FH, ">> JOBS_FAILED.txt" || die("Cannot open file JOBS_FAILED.txt");
        flock(FH, LOCK_EX);
        print FH "NO_JOB_INFO ($jobid)";
        close FH;
        close $debugfh;
        exit $FAILURE;
    }
}

# check if jobs actually finished before hitting wallclock
if (!$finished) {
    my $cnt = 0;
    foreach my $j (@jobids) {
        if (defined($jobinfo{$j}) && $jobinfo{$j}{'elfend'}) {
            $cnt++;
        }
    }
    if ($cnt == scalar @jobids) {
        print "All $cnt elf jobs finished, but target job wallclock'd.\n";
        print "Assuming wallclock didn't cause problem.\n";
        logEvent($config, $block, $jobid, 'j', 'wallclock', 'false');
        $finished = 1;
    }
    else {
        print "Job $jobid hit wallclock limit\n";
        logEvent($config, $block, $jobid, 'j', 'wallclock', 'true');
    }
}
else {
    logEvent($config, $block, $jobid, 'j', 'wallclock', 'false');
}

foreach my $j (@jobids) {
    $jobinfo{$j}{'retval'} = $SUCCESS;
    $jobinfo{$j}{'msg'} = ' ';
    if (defined($modinfo{$j}) && scalar(keys %{$modinfo{$j}}) > scalar(@module_array)) {
        $jobinfo{$j}{'retval'} = $FAILURE;
        print "Extra modules in job $j:\n";
        foreach my $m (keys %{$modinfo{$j}}) {
            print "\t$m\n";
        }
        $jobinfo{$j}{'msg'} = 'Extra modules';
    }
    elsif (!$finished && !$jobinfo{$j}{'runelfend'}) {
        $jobinfo{$j}{'retval'} = $FAILURE;
        $jobinfo{$j}{'msg'} = 'wallclock';
    }
    elsif ($jobinfo{$j}{'status5'} > 0) {
        $jobinfo{$j}{'retval'} = $FAILURE;
        $jobinfo{$j}{'msg'} = ' ';
    }
    elsif (!$jobinfo{$j}{'runelfbeg'}) {
        $jobinfo{$j}{'retval'} = $FAILURE;
        $jobinfo{$j}{'msg'} = 'Died before elf';
    }
    elsif (!$jobinfo{$j}{'elfbeg'}) {
        $jobinfo{$j}{'retval'} = $FAILURE;
        $jobinfo{$j}{'msg'} = 'Did elf run?';
    }
    elsif (!$jobinfo{$j}{'elfend'}) {
        $jobinfo{$j}{'retval'} = $FAILURE;
        $jobinfo{$j}{'msg'} = 'elf died';
    }
    elsif ($jobinfo{$j}{'status4'} > 0) {
        $jobinfo{$j}{'msg'} = ' ';
    }
    else {
        $jobinfo{$j}{'msg'} = ' ';
    }
}

open INFOFH, "> ../${block}_tjobs/$jobid.jobinfo.out";
foreach my $j (@jobids) {
    printf INFOFH "%6s\t%25s\t%7s\t%7s\t%s\n", $j, " ", " ", " ", $jobinfo{$j}{'msg'};
    foreach my $mod (@module_array) {
        if (defined($modinfo{$j}{$mod})) {
            if ($modinfo{$j}{$mod}{'msg'} eq 'SUCCESS') {
                $modinfo{$j}{$mod}{'msg'} = ' ';
            }
            printf INFOFH "%6s\t%25s\t%7d\t%7d\t%s\n", " ", $mod, $modinfo{$j}{$mod}{'status4'}, $modinfo{$j}{$mod}{'status5'}, $modinfo{$j}{$mod}{'msg'};
        }
        elsif ($jobinfo{$j}{'msg'} =~ /\S/) {
            printf INFOFH "%6s\t%25s\t%7s\t%7s\t%s\n", " ", $mod, ' ', ' ', $jobinfo{$j}{'msg'};
        }
        else {
            printf INFOFH "%6s\t%25s\t%7s\t%7s\t%s\n", " ", $mod, ' ', ' ', 'MISSING';
            $jobinfo{$j}{'retval'} = $FAILURE;
        }
    }
}
close INFOFH;

# If failed, file so blockcheck can quickly tell
my $fstr = "";
foreach my $j (sort keys %jobinfo) {
    if ($jobinfo{$j}{'retval'} != $SUCCESS) {
        $fstr .= "$j ($jobid)\n";
        $retval = $FAILURE;
    }
}
if (length($fstr) > 0) {
    print "Adding the following to JOBS_FAILED.txt\n";
    print "$fstr\n";
    open FH, ">> JOBS_FAILED.txt" || die("Cannot Open File JOBS_FAILED.txt");
    flock(FH, LOCK_EX);
    print FH "$fstr";
    close FH;
}

# If status4, file so blockcheck can quickly tell
$fstr = "";
foreach my $j (sort keys %jobinfo) {
    if ($jobinfo{$j}{'status4'} > 0) {
        $fstr .= "$j ($jobid)\n";
    }
}

if (length($fstr) > 0) {
    print "Adding the following to JOBS_WARNINGS.txt\n";
    print "$fstr\n";
    open FH, ">> JOBS_WARNINGS.txt" || die("Cannot Open File JOBS_WARNINGS.txt");
    flock(FH, LOCK_EX);
    print FH "$fstr";
    close FH;
}

# temporary fix to continue in DAG to get failure email since ingest job is different
if ($jobid =~ /ing/) {
    $retval = $SUCCESS;
}


### update DB records
print "jobid = $jobid\n";
my $config_tjobinfo = $config->value('tjobinfo');
print "config_tjobinfo = ",Dumper($config_tjobinfo), "\n";
my $j = lc($jobid);
#$j =~ s/TJ//;
my $dbid_tjob;
if ($j ne 'ingest') {
   $dbid_tjob = $config_tjobinfo->{$j}{'dbid_tjob'};
}
else {
   $dbid_tjob = $config->value('dbid_ingest_tjob');
}
if (!defined($tjobinfo{'batchend'})) {
   $tjobinfo{'batchend'} = time;
}
updateVal($config, 'tjob', $dbid_tjob, {
        start_time => $tjobinfo{'batchbeg'},
        end_time => $tjobinfo{'batchend'},
        exec_host => $tjobinfo{'batchhost'},
        target_jobid => $tjobinfo{'batchid'},
        wall_time => $tjobinfo{'batchend'} - $tjobinfo{'batchbeg'}, 
        status => $retval
    });

my $config_jobinfo = $config->value('job');

foreach my $j (sort keys %jobinfo) {
    my $dbid_desjob;
    if ($j ne 'ingest') {
        $dbid_desjob = $config_jobinfo->{$j}{'dbid_desjob'}; 
    }
    else {
print "ingest\n";
print Dumper($jobinfo{$j}), "\n";
        $dbid_desjob = $config->value('dbid_ingest_desjob');
    }
    updateVal($config, 'desjob', $dbid_desjob, {
        start_time => $jobinfo{$j}{'runelfbeg'},
        end_time => $jobinfo{$j}{'runelfend'},
        wall_time => $jobinfo{$j}{'runelfend'} - $jobinfo{$j}{'runelfbeg'},
        status4 => $jobinfo{$j}{'status4'},
        status5 => $jobinfo{$j}{'status5'}
    });
}




logEvent($config, $block, $jobid, 'c', 'posttask', $retval);


print "jobpost.pl exiting with status $retval\n";
close $debugfh;
exit $retval;
