########################################################################
#  $Id: config.des 6837 2011-06-01 15:03:45Z mgower $
#
#  $Rev:: 6837                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-06-01 08:03:45 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
################################################################################
################################################################################

<<include AppModules/*.des>>

verbose   1
submit_node desjobc
event_host deslogin.cosmology.illinois.edu
elf_delay 30
dagman_max_post   2
dagman_max_pre   2
min_wall 10
max_wall 1800
tmp_object_table = TMP_${run}
object_table = OBJECTS_CURRENT
<software>
<ranger_stable>
        location_name ranger_stable
        software_root /work/01298/descom/des_home/stable
        prereq_root  /share/home/00342/ux453102/des_prereq
        psn  TG-AST060036N
        site_id 1
	<environment>
                oracle_home  /import/data1/descom/oracle/instantclient_11_1
                perl5lib /share/home/00342/ux453102/des_prereq/lib/perl5
                elf_home   /share/home/00342/ux453102/des_prereq/elf
                java_home /share/home/00342/ux453102/java/jdk1.6.0_14
                ld_library_path /share/apps/teragrid/globus-4.0.8-r1/myproxy-3.4/lib:/share/apps/teragrid/globus-4.0.8-r1/lib:/share/apps/teragrid/srb-client-3.4.1-r1/lib:/opt/apps/pgi7_2/mvapich/1.0.1/lib:/opt/apps/pgi7_2/mvapich/1.0.1/lib/shared:/opt/apps/pgi/7.2-5/linux86-64/7.2-5/libso:/opt/gsi-openssh-4.3/lib:/opt/apps/binutils-amd/070220/lib64:/share/home/00342/ux453102/des_prereq/lib:/share/apps/intel/10.1/cc/lib/:/share/home/00342/ux453102/oracle/instantclient_11_1
		path    /opt/sge6.2/bin/lx24-amd64:/opt/apps/tar-lustre/1.22/bin:/opt/apps/gzip-lustre/1.3.12/bin:/share/apps/teragrid/globus-4.0.8-r1/myproxy-3.4/bin:/share/apps/teragrid/globus-4.0.8-r1/bin:/share/apps/teragrid/globus-4.0.8-r1/sbin:/share/apps/teragrid/tginfo-1.0.1-r1/bin:/share/apps/teragrid/uberftp-client-2.4/bin:/share/apps/teragrid/tgusage-3.0-r3/bin:/share/apps/teragrid/tgresid-2.3.4-r1/bin:/share/apps/teragrid/tg-policy-0.2/bin:/share/apps/teragrid/srb-client-3.4.1-r1/bin:/share/apps/teragrid/gx-map-0.5.3.3-r1/bin:/share/apps/teragrid/gx-map-0.5.3.3-r1/sbin:/opt/apps/binutils-amd/070220/bin:/opt/apps/pgi7_2/mvapich/1.0.1/bin:/opt/apps/pgi/7.2-5/linux86-64/7.2-5/bin:/usr/local/first:/opt/sge6.2/bin/lx24-amd64:/usr/kerberos/bin:/tmp/1525695.1.development:/sbin:/usr/sbin:/bin:/usr/bin:/usr/X11R6/bin:/opt/ofed/bin:/opt/ofed/sbin:/opt/rocks/bin:/opt/rocks/sbin:/usr/local/bin:/share/sge6.2/default/pe_scripts:.:/opt/apps/pki_apps:/share/home/00342/ux453102/oracle/instantclient_11_1:/share/home/00342/ux453102/des_prereq/bin
            </environment>
            </ranger_stable>
    <cosmo_stable>
        location_name   cosmo_stable
        software_root   /home/bcs/DES_HOME/stable
        prereq_root   /home/bcs/des_prereq
        site_id   1024
        <environment>
            omp_num_threads 1
            elf_home   /home/bcs/des_prereq/elf
            perl5lib   /home/bcs/des_prereq/lib/perl5
            java_home   /home/opt/jdk1.6.0_06
            oracle_home   /home/oracle/client/11.1.0
            oracle_base   /home/oracle/client
            ld_library_path /home/bcs/des_prereq/lib:/home/opt/globus-4.2.1/lib:/home/opt/intel/cce/10.1.021/lib:/home/oracle/client/11.1.0/lib:/home/opt/intel/mkl/10.0.5.025/lib/em64t
        </environment>
    </cosmo_stable>
    <cosmo_devel>
        location_name   cosmo_devel
        software_root   /home/bcs/DES_HOME/devel
        prereq_root   /home/bcs/des_prereq
        site_id   1024
        <environment>
            omp_num_threads 1
            elf_home   /home/bcs/des_prereq/elf
            perl5lib   /home/bcs/des_prereq/lib/perl5
            java_home   /home/opt/jdk1.6.0_06
            oracle_home   /home/oracle/client/11.1.0
            oracle_base   /home/oracle/client
            ld_library_path /home/bcs/des_prereq/lib:/home/opt/globus-4.2.1/lib:/home/opt/intel/cce/10.1.021/lib:/home/oracle/client/11.1.0/lib:/home/opt/intel/mkl/10.0.5.025/lib/em64t
        </environment>
    </cosmo_devel>
    <abe_stable>
        location_name abe_stable
        software_root /u/ac/bcs/des_home/stable
        prereq_root /u/ac/bcs/classic/des_prereq
        site_id 2
        <environment>
            elf_home  /u/ac/bcs/stack/des_prereq/elf-2.8.0
            java_home /usr/local/jdk1.5.0_12
            oracle_base /usr/local/oracle_client/product/11.1.0
            oracle_home /usr/local/oracle_client/product/11.1.0/client_1
            perl5lib /u/ac/bcs/classic/des_prereq/lib/perl5
	    malloc_check_ 0
	    ld_library_path /u/ac/bcs/classic/des_prereq/lib:/usr/local/oracle_client/product/11.1.0/client_1/lib:/usr/local/mvapich2-1.2-intel-ofed-1.2.5.5/lib:/usr/local/Python-2.5.2/lib:/usr/local/lib64:/usr/local/lib:/lib:/lib64:/usr/lib64:/usr/lib:/usr/local/X11R6/lib:/usr/local/X11R6/lib64:/usr/local/expat-2.0.1/lib:/usr/local/expat-2.0.1/lib64:/usr/local/tcl8.5.3/lib:/usr/local/tcl8.5.3/lib64:/usr/local/tk8.5.3/lib:/usr/local/tk8.5.3/lib64:/usr/local/intel/10.1.017/lib:/usr/local/ofed-1.2.5.5/lib64:/usr/local/moab/lib:/usr/local/postgresql-8.2.3/lib:/usr/local/globus-4.0.8-r2/lib:/usr/local/srb-client-3.4.1-r2/lib:/usr/apps/hdf/hdf4/v423/lib:/usr/apps/hdf/hdf5/v167/lib:/usr/apps/hdf/phdf5/v167/lib
        </environment>
    </abe_stable>
   <abe_devel>
        location_name abe_devel
        software_root /u/ac/bcs/des_home/devel
        prereq_root /u/ac/bcs/classic/des_prereq
        site_id 2
        <environment>
            elf_home  /u/ac/bcs/stack/des_prereq/elf-2.8.0
            java_home /usr/local/jdk1.5.0_12
            oracle_base /usr/local/oracle_client/product/11.1.0
            oracle_home /usr/local/oracle_client/product/11.1.0/client_1
            perl5lib /u/ac/bcs/classic/des_prereq/lib/perl5
	    malloc_check_ 0
	    ld_library_path /u/ac/bcs/classic/des_prereq/lib:/usr/local/oracle_client/product/11.1.0/client_1/lib:/usr/local/mvapich2-1.2-intel-ofed-1.2.5.5/lib:/usr/local/Python-2.5.2/lib:/usr/local/lib64:/usr/local/lib:/lib:/lib64:/usr/lib64:/usr/lib:/usr/local/X11R6/lib:/usr/local/X11R6/lib64:/usr/local/expat-2.0.1/lib:/usr/local/expat-2.0.1/lib64:/usr/local/tcl8.5.3/lib:/usr/local/tcl8.5.3/lib64:/usr/local/tk8.5.3/lib:/usr/local/tk8.5.3/lib64:/usr/local/intel/10.1.017/lib:/usr/local/ofed-1.2.5.5/lib64:/usr/local/moab/lib:/usr/local/postgresql-8.2.3/lib:/usr/local/globus-4.0.8-r2/lib:/usr/local/srb-client-3.4.1-r2/lib:/usr/apps/hdf/hdf4/v423/lib:/usr/apps/hdf/hdf5/v167/lib:/usr/apps/hdf/phdf5/v167/lib
        </environment>
    </abe_devel>
<queenbee_stable>
    location_name queenbee_stable
    software_root /home/bcs/des_home/stable
    prereq_root /home/bcs/classic/des_prereq
    site_id 21
    <environment>
       path /home/bcs/classic/des_prereq/bin:/home/bcs/oracle_pkg/instantclient_11_1:/usr/kerberos/bin:/usr/local/packages/mycluster-v0.9.9/mycluster:/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/X11R6/bin:/usr/local/packages/softenv/bin:/home/packages/teragrid/tg-policy-0.2-r1/bin:/home/packages/teragrid/tgresid-2.3.3-r1/bin:/home/packages/teragrid/tginfo-1.1.0-r1/bin:/home/packages/tgusage-3.0-r0/bin:/home/packages/gx-map-0.5.3.3-r1/bin:/usr/local/compilers/jdk/bin:/home/packages/globus/globus-4.0.8-r2/bin:/home/packages/uberftp-2.4/bin:/usr/local/packages/tgcp-1.0.0-r2/bin:/usr/local/packages/srb-client-3.4.1-r2/bin:/home/packages/condor-7.2.1-r1/bin:/usr/local/packages/condor-7.2.1-r1/sbin:/home/packages/mcp-200907281503-r1:/home/packages/mcp-200907281503-r1/expect/usr/bin:/home/packages/gur-200902051237-r1:/usr/local/compilers/Intel/intel_cc_11.1/bin/intel64:/usr/local/compilers/Intel/intel_fc_11.1/bin/intel64:/usr/local/packages/atlas/3.8.2/intel-11.1/bin:/usr/local/packages/mvapich2/1.4/intel-11.1/bin://usr/local/packages/mvapich2/1.4/intel-11.1/sbin:/usr/local/packages/apache_ant/1.7.1/bin:/usr/local/packages/hdf5/1.8.2/intel-11.1-mvapich2-1.4/bin:/home/packages/teragrid/pacman-3.26-r1/src:/usr/local/ofed/bin:/usr/local/ofed/sbin:/home/bcs/classic/des_prereq/eups/1.1.1/bin:/home/bcs/bin
        oracle_base /home/bcs/oracle_pkg/instantclient_11_1
        oracle_home /home/bcs/oracle_pkg/instantclient_11_1
        ld_library_path /home/bcs/classic/des_prereq/lib:/home/bcs/oracle_pkg/instantclient_11_1:/usr/local/compilers/jdk/lib:/home/packages/globus/globus-4.0.8-r2/lib:/usr/local/packages/srb-client-3.4.1-r2/lib:/home/packages/mcp-200907281503-r1/expect/usr/lib64:/usr/local/compilers/Intel/intel_cc_10.1/lib:/usr/local/compilers/Intel/intel_fc_10.1/lib:/usr/local/compilers/Intel/mkl-10.1.0.015/lib/em64t:/usr/local/packages/ATLAS-gcc3.4.6/lib:/usr/local/packages/mvapich2-0.98-intel10.1/lib:/usr/local/packages/apache-ant-1.6.5/lib:/usr/local/packages/hdf5-1.8.1-intel10.1/lib
        perl5lib /home/bcs/classic/des_prereq/lib/perl5
        elf_home /home/bcs/classic/des_prereq/elf
        java_home /usr/local/compilers/jdk
	malloc_check_ 0
    </environment>
</queenbee_stable>
<steele_stable>
	location_name steele_stable
    	software_root /usr/rmt_share/scratch95/d/desai1/des_home/stable
    	prereq_root /autohome/u121/daues/stack/des_prereq
    	psn  TG-AST060036N
     	site_id 22
        <environment>
		oracle_base /autohome/u121/daues/oracle/instantclient_11_1
        	oracle_home  /autohome/u121/daues/oracle/instantclient_11_1
        	perl5lib /autohome/u121/daues/stack/des_prereq/lib/perl5
        	elf_home   /autohome/u121/daues/stack/des_prereq/elf
        	java_home /apps/rhel5/jdk1.6.0_14
    	</environment>
</steele_stable>
<lonestar_stable>
	location_name lonestar_stable
	software_root /share1/projects/tg/descom/des_home/stable
	prereq_root /share1/projects/tg/descom/des_prereq
	psn TG-AST060036N
	site_id 9
	<environment>
		oracle_home /share1/projects/tg/descom/instant/instantclient_11_1
		perl5lib /share1/projects/tg/descom/des_prereq/lib/perl5
		elf_home /share1/projects/tg/descom/des_prereq/elf
		java_home /share1/apps/teragrid/jdk1.6.0_17
		ld_library_path /share1/projects/tg/descom/des_prereq/lib:/share1/projects/tg/descom/instant/instantclient_11_1:/opt/apps/intel11_1/mvapich2/1.6/lib:/opt/apps/intel11_1/mvapich2/1.6/lib/shared:/opt/apps/intel/11.1/lib/intel64
		 path /share1/apps/teragrid/jdk1.6.0_17/bin:/share1/projects/tg/descom/des_prereq/bin:/share1/projects/tg/descom/instant/instantclient_11_1:/share1/projects/tg/descom/des_prereq/elf:/opt/apps/subversion-tacc/1.6.15/bin:/opt/apps/tar-lustre/1.22/bin:/opt/apps/gzip-lustre/1.3.12/bin:/opt/apps/intel11_1/mvapich2/1.6/bin:/opt/apps/intel/11.1/bin/intel64:/opt/sge6.2/bin/lx24-amd64:/usr/lib64/qt-3.3/bin:/usr/kerberos/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/gsi-openssh-4.3/bin:/usr/X11R6/bin:/opt/ofed/bin:/opt/ofed/sbin:/usr/local/bin:/sge_common/default/pe_scripts:.:/share1/projects/tg/descom/des_prereq/eups/1.1.1/bin:/share1/projects/tg/descom/des_home/stable/bin
</environment>
</lonestar_stable>
<usm_stable>
	location_name   usm_stable
        software_root   /big/des_home/stable
        prereq_root   /big/des_prereq
        site_id   1026
        <environment>
            omp_num_threads 1
            elf_home   /big/des_prereq/elf
            perl5lib   /big/des_prereq/lib/perl5
            java_home   /usr
            oracle_home   /big/Archive/stack/instant/instantclient_11_1
	    oracle_base   /big/Archive/stack/instant/instantclient_11_1
	    ld_library_path /big/des_prereq/lib:/big/Archive/stack/instant/instantclient_11_1:/home/moon/desdm/globus/install/lib:/home/moon/desdm/packages/openssl/install/lib
        </environment>
    </usm_stable>
<lrz_stable>
        location_name   lrz_stable
        software_root   /home/cluster/pr42ga/lu64nej5/des/stable
        prereq_root     /home/cluster/pr42ga/lu64nej5/des/prereq
        site_id   4
        <environment>
            omp_num_threads 1
            elf_home     /home/cluster/pr42ga/lu64nej5/des/prereq/elf
            perl5lib     /home/cluster/pr42ga/lu64nej5/des/prereq/lib/perl5
            java_home    /lrz/sys/compilers/java/jdk1.6.0_23
            oracle_home  /home/cluster/pr42ga/lu64nej5/des/instantclient_11_1
            oracle_base  /home/cluster/pr42ga/lu64nej5/des/instantclient_11_1
	    ld_library_path /home/cluster/pr42ga/lu64nej5/des/prereq/lib:/home/cluster/pr42ga/lu64nej5/des/instantclient_11_1:/lrz/sys/globus/lib:/usr/lib           
	path  /home/cluster/pr42ga/lu64nej5/des/prereq/elf:/home/cluster/pr42ga/lu64nej5/des/prereq/bin:/home/cluster/pr42ga/lu64nej5/des/stable/bin:/home/cluster/pr42ga/lu64nej5/des/instantclient_11_1:/lrz/sys/globus/bin:/lrz/sys/globus/sbin:/lrz/sys/compilers/java/jdk1.6.0_23/bin:/lrz/sys/tools/ant/apache-ant-1.8.1/bin:/sge/ar/bin:/lrz/mnt/sge_root/sge62u2/bin/lx26-amd64:/lrz/sys/tools/tv/totalview.8.7.0-7/bin:/lrz/sys/intel/icc_111/bin/intel64:/lrz/sys/intel/ifort_111/bin/intel64:/lrz/sys/tools/binutils/2.19.1/bin:/lrz/sys/bin:/usr/local/bin:/usr/bin:/usr/X11R6/bin:/bin:/usr/games:/opt/gnome/bin:/opt/kde3/bin:/usr/lib/mit/bin:/usr/lib/mit/sbin:/usr/lib/qt3/bin:/home/cluster/pr42ga/lu64nej5/des/prereq/eups/1.1.1/bin 		
        </environment>
    </lrz_stable>

</software>
