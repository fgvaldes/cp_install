#!/usr/bin/env perl

###############################################################################
#
#   desdm SVN Checkout script
#
#   usage: perl SVNCheckout.pl <projectname>
#   where projectname is an entry in the hash table of checkouts hCheckouts
#
#   This script will create the devel directory and all sub directories so run 
#   above where the devel directory should be created
#
#   To add another project to checkout, add an entry to hCheckouts with all 
#   dependencies listed
#
#   To add a new dependency, if necessary check its environment variable and 
#   add the dependency to the hash of modules, hModules
#
###############################################################################

use strict;
use warnings;

my %hCheckouts = (
#cp => ["AppModules", "Database", "ImageProc", "Orchestration","CommPipe","imsupport","imdetrend","qatools"],
cp => ["AppModules", "Database", "ImageProc", "Orchestration","CommPipe","imsupport","imdetrend"],
desdm => ["AppModules", "Database", "ImageProc", "Orchestration", "photoz", "DiffImg","PSM"],
);

my $argc = scalar (@ARGV);
if ($argc < 1)
{
    die "Please enter which project to checkout";
}

my $project = $ARGV[0];
if (!exists ($hCheckouts{$project}))
{
    die "Project not found in available checkout options";
}

my $BRANCH = $ENV{'BRANCH'};
my $ORCH_BASE = $ENV{'ORCH_BASE'};
my $DATA_BASE = $ENV{'DATA_BASE'};
my $APPMOD_BASE = $ENV{'APPMOD_BASE'};
my $PHOTOZ_BASE = $ENV{'PHOTOZ_BASE'};
my $IMAGEPROC_BASE = $ENV{'IMAGEPROC_BASE'};
my $DIFFIMG_BASE = $ENV{'DIFFIMG_BASE'};
my $PSM_BASE = $ENV{'PSM_BASE'};
my $COMMPIPE_BASE = $ENV{'COMMPIPE_BASE'};
my $IMDETREND_BASE = $ENV{'IMDETREND_BASE'};
my $IMSUPPORT_BASE = $ENV{'IMSUPPORT_BASE'};
my $QATOOLS_BASE = $ENV{'QATOOLS_BASE'};

if (!$BRANCH)
{
    $BRANCH = "trunk";
    print "BRANCH environment variable undefined, using trunk\n"; 
}
if (!$ORCH_BASE)
{
    $ORCH_BASE = $BRANCH;
    print "ORCH_BASE environment variable undefined, using BRANCH\n"; 
}
if (!$DATA_BASE)
{
    $DATA_BASE = $BRANCH;
    print "DATA_BASE environment variable undefined, using BRANCH\n"; 
}
if (!$APPMOD_BASE)
{
    $APPMOD_BASE = $BRANCH;
    print "APPMOD_BASE environment variable undefined, using BRANCH\n"; 
}
if (!$PHOTOZ_BASE)
{
    $PHOTOZ_BASE = $BRANCH;
    print "PHOTOZ_BASE environment variable undefined, using BRANCH\n"; 
}
if (!$IMAGEPROC_BASE)
{
    $IMAGEPROC_BASE = $BRANCH;
    print "IMAGEPROC_BASE environment variable undefined, using BRANCH\n"; 
}

if (!$DIFFIMG_BASE)
{
    $DIFFIMG_BASE = $BRANCH;
    print "DIFFIMG_BASE environment variable undefined, using BRANCH\n";
}
if (!$PSM_BASE)
{
    $PSM_BASE = $BRANCH;
    print "PSM_BASE environment variable undefined, using BRANCH\n";
}
if (!$COMMPIPE_BASE)
{
    $COMMPIPE_BASE = $BRANCH;
    print "COMMPIPE_BASE environment variable undefined, using BRANCH\n";
}
if (!$IMDETREND_BASE)
{
    $IMDETREND_BASE = $BRANCH;
    print "IMDETREND_BASE environment variable undefined, using BRANCH\n";
}
if (!$IMSUPPORT_BASE)
{
    $IMSUPPORT_BASE = $BRANCH;
    print "IMSUPPORT_BASE environment variable undefined, using BRANCH\n";
}
if (!$QATOOLS_BASE)
{
    $QATOOLS_BASE = $BRANCH;
    print "QATOOLS_BASE environment variable undefined, using BRANCH\n";
}

my %hModules = (
    'AppModules' => $APPMOD_BASE,
    'Database' => $DATA_BASE,
    'ImageProc' => $IMAGEPROC_BASE,
    'Orchestration' => $ORCH_BASE,
    'photoz' => $PHOTOZ_BASE,
    'DiffImg' => $DIFFIMG_BASE,
    'PSM' => $PSM_BASE,
    'CommPipe' => $COMMPIPE_BASE,
    'imdetrend' => $IMDETREND_BASE,
    'imsupport' => $IMSUPPORT_BASE,
    'qatools' => $QATOOLS_BASE,
);

###################  BEGIN SVN CHECKOUT #################
system ("svn checkout https://desweb.cosmology.illinois.edu/svn/desdm/devel --depth immediates");
system ("svn up --set-depth infinity devel/CONFIG/");

foreach (@{ $hCheckouts{$project} })
{
    if ($hModules{$_} eq "trunk")
    {
        system ("svn up --set-depth infinity devel/$_/$hModules{$_}/");
    }
    else
    {
        system ("svn up --set-depth immediates devel/$_/");
        system ("svn up --set-depth infinity devel/$hModules{$_}/");
    }
}

