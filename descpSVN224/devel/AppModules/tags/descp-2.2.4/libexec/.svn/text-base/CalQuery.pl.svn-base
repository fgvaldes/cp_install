#!/usr/bin/env perl
########################################################################
#  $Id: genquerydb.pl 4383 2009-09-24 16:05:40Z mgower $
#
#  $Rev:: 4383                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-09-24 11:05:40 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
#use lib ("/home/mgower/desdm/devel/Database/branches/postgres/lib/DB","$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use lib ("/home/mgower/desdm/devel/Database/branches/postgres/lib", "/home/mgower/desdm/devel/Orchestration/branches/postgres/lib");
use DB::DESUtil;
#use Orchestration::desconfig;
use Orchestration::filelistfunc;
#use Orchestration::misc;


my ($outputxml, $project, $nite, $run, $detector, $filetype, $debug) = undef;

use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s' => \$outputxml,
    'project=s'   => \$project,
    'nite=s'      => \$nite,
    'run=s'       => \$run,
    'detector=s'  => \$detector,
    'filetype=s'  => \$filetype,
    'debug'       => \$debug,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($project)) {
    print "Error: Must specify project\n";
    exit 1;
}
if (!defined($nite) && !defined($run)) {
    print "Error: Must specify nite or run\n";
    exit 1;
}
if (!defined($filetype)) {
    print "Error: Must specify filetype\n";
    exit 1;
}
if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    exit 1;
}


$debug = 0 if !defined($debug);
my $dbh = DB::DESUtil->new('verbose'=>0, 'debug'=>$debug);
my ($dbq, $files, $results_aref);

my $cond_project = $dbh->makeWhereCondition('project', $project);
my $cond_filetype = $dbh->makeWhereCondition('filetype', $filetype);
my $cond_fileclass = $dbh->makeWhereCondition('fileclass', 'cal');
my $cond_archivesites = $dbh->makeWhereCondition('archivesites', '[^N]');

if (defined($run)) {
    my $cond_run = $dbh->makeWhereCondition('run',$run);
    $dbq = "select location.* from location where $cond_filetype and $cond_fileclass and $cond_project and $cond_archivesites and $cond_run";
    print "Query = $dbq\n\n" if $debug > 2;
    $results_aref = $dbh->selectall_arrayref($dbq,  { Slice => {} });
    $dbh->disconnect();

    print "Number of files in list from run specific query = ", scalar(@$results_aref), "\n";
    print Dumper(@$results_aref), "\n" if $debug > 2;

    if (scalar(@$results_aref) == 0) {
        print STDERR "calquery: query with run specified returned zero results\n";
        print STDERR "Aborting\n\n";
        print STDERR "query = $dbq\n";
        exit 1;
    }
}
else {
    $dbq = "select location.* from location, (select  max(run) as maxrun,filename from location where $cond_project and $cond_fileclass and $cond_filetype and run<='$nite' group by filename) tbl1 where tbl1.maxrun = location.run and tbl1.filename = location.filename and $cond_filetype and $cond_fileclass and $cond_project and $cond_archivesites";

    print "Query = $dbq\n\n" if $debug > 2;
    $results_aref = $dbh->selectall_arrayref($dbq,  { Slice => {} });

    print "Number of files in list from backwards query= ", scalar(@$results_aref), "\n";
    print Dumper(@$results_aref), "\n" if $debug > 2;

    if (scalar(@$results_aref) == 0) {
        $dbq = "select location.* from location, (select  min(run) as minrun,filename from location where $cond_project and $cond_fileclass and $cond_filetype and run>='$nite' group by filename) tbl1 where tbl1.minrun = location.run and tbl1.filename = location.filename and $cond_filetype and $cond_fileclass and $cond_project and $cond_archivesites";

        print "Query = $dbq\n\n" if $debug > 2;
        $results_aref = $dbh->selectall_arrayref($dbq,  { Slice => {} });
        $dbh->disconnect();

        print "Number of files in list from forward query = ", scalar(@$results_aref), "\n";
        print Dumper(@$results_aref), "\n" if $debug > 2;

        if (scalar(@$results_aref) == 0) {
            print STDERR "calquery: query with run specified returned zero results\n";
            print STDERR "Aborting\n\n";
            print STDERR "query = $dbq\n";
            exit 1;
        }
    }
    else {
        $dbh->disconnect();
    }
}

$files = makeFilesUniq($results_aref, undef, $debug);
my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);
