#!/usr/bin/perl -w
use warnings;
use strict;

use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::filelistfunc;

$| = 1;

my $image_table = 'IMAGE';
my $location_table = 'LOCATION';
my $catalog_table = 'CATALOG';

my (%opts, %red_ids);

my $db = DB::DESUtil->new('verbose' => 3);

#my %location_cols = map{$_ => 1} @{$db->getColumnNames->($location_table)};
#my %image_cols = map {$_ => 1} @{$db->getColumnNames->($image_table)};

# From list of accesable location table columns(below), contruct GetOpt definition
# that allows multiple values to be interpreted as 'OR' ie "-band -band g".
# Add other command line dfs in the call to getopt:
#
my %location_cols = (
 id => 1,
 fileclass => 1,
 filename => 1,
 filedate => 1,
 run  => 1,
 nite => 1,
 band => 1,
 tilename  => 1,
 exposurename => 1,
 ccd => 1,
 project => 1,
);

my @DB_opts = map {lc($_)."=s@"} keys %location_cols;

Getopt::Long::GetOptions (\%opts,
  'current_run|current-run|cr|curr_run=s',
  'output_file|output-file|out-file|of|outputxml|output-xml|out-xml=s',
  'stage_file|stage-file|sf=s',
  'help|h|?',
  'man',
  'verbose|V=i',
  'query_run=s',
  'block_num=i',
   @DB_opts
) or Pod::Usage::pod2usage(-verbose => 0,-exitval => 2,-output => \*STDERR);

# Display documentation and exit if requested:
if ($opts{'help'}) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($opts{'man'}) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

Pod::Usage::pod2usage(-message => 'Must provide a tilename.'-verbose => 0,-exitval => 2,-output => \*STDERR) if (! $opts{'nite'});

# Construct basic remap queryDB call then add a "key_vals"
# argument for everything that came in on the command line:
#
my %query = ( 
  $location_table => {
      'join' => {'id' => 'image.id'},
      'select_fields' => 'all',
      'key_vals' => {
	  'filetype' => 'red',
      'archivesites' => '[^N]'
    }
  },
  $image_table => {
    'select_fields' => ['parentid']
  },
  'hash_key' => 'id'
);


setQueryRun($opts{'query_run'}, $opts{'block_num'}, $opts{'current_run'}, $query{$location_table}->{'key_vals'});

foreach my $key (keys %location_cols) {
  if (exists $opts{$key}) {
    $query{$location_table}->{'key_vals'}->{$key} = $opts{$key};
  }
}

## Get remap list, filter to latest run and print message:

my $RedRows = $db->queryDB2(%query);
#print Dumper($RedRows);
#
removeFilesFromOldRuns($RedRows);
#print Dumper($RedRows);
my $N_reds = scalar keys %$RedRows;
#print "\nNumber of matched \"latest\" remap and (parent) red files: $N_reds\n";
#
## Create a unique list of red location.ids
## Retrive some location table information about the red files
## then print count message:
##

foreach my $Fkeys (values %$RedRows) {
  $red_ids{$Fkeys->{'id'}} = 1 if ($Fkeys->{'archivesites'} =~ /[^N]/);
}

my $CatRows = $db->queryDB2 (
    $catalog_table => {
    'key_vals' => {
    'parentid' => [keys %red_ids]
    },
    'join' => {'id' => 'location.id'},
    'select_fields' => ['parentid']
  },
  $location_table => {
    'select_fields' => 'all',
    'key_vals' => {
     'filetype' => 'red_cat',
      'archivesites' => '[^N]'
    }
  },
  'hash_key' => 'id'
);

removeFilesFromOldRuns($CatRows);
my $N_redcats = scalar keys %$CatRows;
print "\nNumber of matched \"latest\" red_cat files: $N_redcats\n";

# Die if unable to return a complete set of matching red/red_cat files:
if ($N_reds != $N_redcats) {
  die "ERROR: Found non matching number of red and red_cat files.\n";
}

# Generate XML output file:
if ($opts{'output_file'}) {
  my $outputxml = $opts{'output_file'};
  outputXMLList_WL($outputxml, $CatRows,$RedRows);

}

if ($opts{'stage_file'}) {
  my $file = $opts{'stage_file'};
  open FH, "> $file";
   while ((my $id, my $keys) = each %$CatRows) {
     my $cat = $id;
     my $red = $keys->{'parentid'};
     print FH "<file>\n","fileid=$cat\n","</file>\n";
     print FH "<file>\n","fileid=$red\n","</file>\n";
  }
  foreach my $id (keys %$RedRows) {
   print FH "<file>\n","fileid=$id\n","</file>\n";
  }
  close FH;
}
#
#
sub removeFilesFromOldRuns {
    my ($Rows, $CurrentRun) = @_;

  my @unique_keys = ('project', 'fileclass', 'filetype', 'exposurename', 'filename', 'tilename', 'band', 'ccd');
  my %List;

  if (ref $Rows eq 'ARRAY') { 
    for (my $i=0; $i<=$#$Rows; $i++ ) {
      my $File = $Rows->[$i];
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }
      if (exists $List{"$str"}) {
        my ($this_date, $junk1) = split (/_/, $File->{'run'});
        my ($other_date, $junk2) = split (/_/, $Rows->[$List{"$str"}]->{'run'});
        if ($this_date gt $other_date) {
          $List{"$str"} = $i;
        }
      }
      else {
        $List{"$str"} = $i;
      }
    }
  }

  
  elsif (ref $Rows eq 'HASH') {
    while ((my $id, my $File) = each %$Rows) {
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }
      if (exists $List{"$str"}) {
	  my $existing_id = $List{"$str"};
#        warn "\nWARNING: Should not the same red file from multiple runs at this stage.\n";
#        warn "This means the set of remaps contains to parents that are the same red file\n";
#        warn "but from different runs.\n";
#        warn "Keeping latest red and continuing.\n";
        my ($this_date, $junk1) = split (/_/, $File->{'run'});
        my ($existing_date, $junk2) = split (/_/, $Rows->{$existing_id}->{'run'});
        if ($this_date gt $existing_date) {
          $List{"$str"} = $id;
          print "Kept: ",$Rows->{$id}->{'run'},"  Discarded: ", $Rows->{$existing_id}->{'run'},"\n";
          delete $Rows->{$existing_id};
        }
        else {
          print "Kept: ",$Rows->{$existing_id}->{'run'},"  Discarded: ", $Rows->{$id}->{'run'},"\n";
          delete $Rows->{$id};
          
        }
      }
      else {
        $List{"$str"} = $id;
      }
    }
  }

  return \%List;

}
#
exit 0;








sub outputXMLList_WL {
    my ($outputxml, $red_catfiles, $redfiles) = @_;

    open FH, "> $outputxml" or die "Error: Could not write xml to file $outputxml";
    print FH "<list>\n";
    while (my ($id, $fhref) = each (%$red_catfiles) ) {
	print FH "\t<line>\n";
	print FH "\t\t<file nickname='red_cat'>\n";
	foreach my $key (keys %$fhref) {
	    if (defined($fhref->{$key})) {
		if (lc($key) eq 'ccd') {
		    print FH "\t\t\t<$key>", sprintf("%02d",$fhref->{$key}), "</$key>\n";
		} 
		else {
		    print FH "\t\t\t<$key>", $fhref->{$key}, "</$key>\n";
		}
	    }
	    else {
#                    print "Undefined key $key for file ", $fhref->{'id'}, "\n";
	    }
	}
	print FH "\t\t\t<fileid>", $fhref->{'id'}, "</fileid>\n";
	print FH "\t\t</file>\n";
	

	print FH "\t\t<file nickname='red'>\n";

	my $red_id = $fhref->{'parentid'};
	my $red=$redfiles->{$red_id};
	
	foreach my $key (keys %$red) {
	    if (defined($red->{$key})) {
		if (lc($key) eq 'ccd') {
		    print FH "\t\t\t<$key>", sprintf("%02d",$red->{$key}), "</$key>\n";
		} 
		else {
		    print FH "\t\t\t<$key>", $red->{$key}, "</$key>\n";
		}
	}
	else {
#                    print "Undefined key $key for file ", $fhref->{'id'}, "\n";
	}
	}
	print FH "\t\t\t<fileid>", $red->{'id'}, "</fileid>\n";
	print FH "\t\t</file>\n";
	
	my $oldrun=$red->{'run'};
	print FH "\t\t<oldrun>$oldrun</oldrun>\n";

	print FH "\t</line>\n";
    }
    
    print FH "</list>\n";
    close FH;
}
















