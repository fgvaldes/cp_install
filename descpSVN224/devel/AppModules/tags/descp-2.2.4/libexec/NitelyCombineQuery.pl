#!/usr/bin/env perl
########################################################################
#  $Id: DiffImgQuery1.pl 3264 2009-03-06 19:56:20Z dadams $
#
#  $Rev:: 3264                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-03-06 13:56:20 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#     outputpath is considered to be equivalent to the run directory
#     The directory structure underneath will match archive structure
#      
#
########################################################################

use warnings;
use strict;

use Getopt::Long;
use File::Basename;
use Data::Dumper;
use XML::Simple;
use FindBin;
use Carp;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::filelistfunc;
use ImageProc::DB;
use ImageProc::SWARP qw(:all);
use ImageProc::Util qw(removeFilesFromOldRuns);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

# List of location table keys that are availiable on the command line
# in the generic way they in in the DAF
my %location_cols = (
 id => 1,
 fileclass => 1,
 filename => 1,
 filedate => 1,
 filetype => 1,
 run  => 1,
 nite => 1,
 band => 1,
 tilename  => 1,
 exposurename => 1,
 ccd => 1,
);
my @DB_opts = map {lc($_)."=s@"} keys %location_cols;

my ($stat);
my $dra = 0.003; 
my $ddec = 0.003;

my %opts;
$opts{'fileclass'} = 'red';
$opts{'filetype'} = 'red';

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
$stat = Getopt::Long::GetOptions(\%opts,
      'project=s',
      'query_run=s',
      'block_num=i',
      'curr_run=s',
      'outputxml=s',
      'verbose=i',
      'dra=s',
      'ddec=s',
      'debug',
       @DB_opts
);

# Set defaults:
$opts{'verbose'} = 1 if (!defined $opts{'verbose'});
$opts{'debug'} = 0 if (!defined $opts{'debug'});

my $verbose = $opts{'verbose'};
my $debug = $opts{'debug'};

# Check inputs
if (!defined($opts{'project'})) {
    print STDERR "Must specify project.\n";
    exit 1;
}

if (!defined($opts{'nite'})) {
    print STDERR "Must specify nite.\n";
    exit 1;
}

### Database query for search images.  
# This is done using the SQL builder (queryDB2) so that command line arguments can be
# translated into useful SQL (%-> like, ! ->not, etc. )
my %key_vals = ( 'project' => $opts{'project'},
                 'archivesites' => '[^N]'
               );
setQueryRun($opts{'query_run'}, $opts{'block_num'}, $opts{'curr_run'}, \%key_vals);

my %query = (
  'location' => {
    'join' => {'id' => 'image.id'},
    'select_fields' => 'all',
    'key_vals' => \%key_vals,
  },
  'image' => {
    'select_fields' => ['exposureid'],
    'join' => {'exposureid' => 'exposure.id'}
  },
  'exposure' => {
    'select_fields' => ['telra','teldec']
  },
);
# Hammer cmd-line args into query call:
foreach my $key (keys %location_cols) {
  if (exists $opts{$key}) {
    $query{'location'}->{'key_vals'}->{$key} = $opts{$key};
  }
}

my $redlist = getFileList(\%query);
#print Dumper($redlist), "\n\n";

### get distinct telra,teldec pairs for each band
my %centerPts;
while (my ($key, $redfh) = each %$redlist) { 
    my $band = $redfh->{'band'};
    my $telra = $redfh->{'telra'};
    my $teldec = $redfh->{'teldec'};
    my $uniqkey = $telra.'__'.$teldec;
    $centerPts{$band}->{$uniqkey}->{'telra'} = $telra;
    $centerPts{$band}->{$uniqkey}->{'teldec'} = $teldec;
}

print "Number center point bands = ", scalar(keys %centerPts), "\n";
print "Number distinct pointings by band:\n";
foreach my $b (keys %centerPts) {
    print "\t$b\t", scalar(keys %{$centerPts{$b}}), "\n";
}
#print Dumper(\%centerPts), "\n\n";

### place red images into groups based upon band, ccd, telra, teldec
my %redgroups;
if (defined($opts{'dra'})) {
    $dra = $opts{'dra'};
}
if (defined($opts{'ddec'})) {
    $ddec = $opts{'ddec'};
}

while (my ($key, $redfh) = each %$redlist) { 
    my $ccd = $redfh->{'ccd'};
    my $band = $redfh->{'band'};
    my $telra = $redfh->{'telra'};
    my $teldec = $redfh->{'teldec'};

    foreach my $centerPt (values %{$centerPts{$band}}) {
        my $key = ${band}."__".${ccd}."__".$centerPt->{'telra'}."__".$centerPt->{'teldec'};

        if (($centerPt->{'telra'}-$dra <= $telra) && 
            ($telra <= $centerPt->{'telra'}+$dra) && 
            ($centerPt->{'teldec'}-$ddec <= $teldec) && 
            ($teldec <= $centerPt->{'teldec'}+$ddec) ) {
            push(@{$redgroups{$key}}, $redfh);
        }
        else {
            print "$telra,$teldec: not in bucket $key";
        }
    }
}

#print Dumper(\%redgroups), "\n";

### generate filelist of reds with appropriate pointing group label
# note some red files may appear in more than 1 list
my $files;
my $cnt = 0;
foreach my $group (keys %redgroups) {
    foreach my $fhref (@{$redgroups{$group}}) {

        # deep copy since file can appear in more than one group
        my $newfh;
        foreach my $k (keys %$fhref) {
            $newfh->{$k} = $fhref->{$k};
        }
        $newfh->{'ptgroup'} = $group;
        $files->{$cnt} = $newfh;
        $cnt++;
    }
}
#print Dumper($files), "\n";
print "Total number files (including duplicates across groups) = ", scalar(keys %$files), "\n";

# output file information in XML format
my $lines = convertSingleFilesToLines($files);
outputXMLList($opts{'outputxml'}, $lines);

exit 0;
