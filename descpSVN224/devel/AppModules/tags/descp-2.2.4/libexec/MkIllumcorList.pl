#!/usr/bin/env perl
########################################################################
#  $Id: MkIllumcorList.pl 3381 2012-07-26 20:28:07Z ankitc $
#
#  $Rev:: 3381                             $:  # Revision of last commit.
#  $LastChangedBy:: Ankit Chandra                 $:  # Author of last commit.
#  $LastChangedDate:: 2012-07-26 15:28:07 #$:  # Date of last commit.
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#     Creates master list of input files for mkillumcor
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;
use Orchestration::filelistfunc;
use DB::EventUtils;

my ($outputxml, $nite, $project, $curr_run, $query_run, $block_num, $sat_pixels, $min_exposures, $min_ccds,$nstars,$filter_run) = undef;
my $debug = 0;

use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'nite=s' => \$nite,
    'nstars=s' => \$nstars,
    'project=s' => \$project,
    'curr_run=s' => \$curr_run,
    'query_run=s' => \$query_run,
    'filter_run=s' => \$filter_run,
    'block_num=i' => \$block_num,
    'debug=i' => \$debug,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($nite)) {
    print "Error: Must specify nite\n";
    usage();
    exit $FAILURE;
}

if (!defined($project)) {
    print "Error: Must specify project\n";
    usage();
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    usage();
    exit $FAILURE;
}

if (!defined($nstars)) {
    print "Error: Must specify maximum number of stars\n";
    usage();
    exit $FAILURE;
}


if (!defined($filter_run)) {
    print "Error: Must specify filter_run\n";
    usage();
    exit $FAILURE;
}

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new();



# values to use in 'where' portion of DB query
my %key_vals = (project => $project, fileclass => 'red', nite=>$nite);
$key_vals{'filetype'} = ['red'];

setQueryRun($query_run, $block_num, $curr_run, \%key_vals);

my %query = ();
$query{'location'}->{'key_vals'} = \%key_vals;
my $files = getFileList(\%query, $debug);

#print "\n the file list ";
#print scalar (keys %$files);
#print Dumper($files);

my ($FileIds,$exposureIds,$tempExp);
$FileIds = '';
foreach my $fval (values %$files) {

    $FileIds .= $fval->{'id'}.', ';
    #print "\n ",$fval->{'id'};
}


$FileIds = substr $FileIds, 0 , -2;

if($FileIds !~ /\S+/){
###
## Usage: verbosity, type of message, level, string 
###
DB::EventUtils::reportEvent( 2, 'STATUS', 3, "No files to work on. Exiting with non zero status code" );
$desdbh->disconnect();;
exit(1);
}

my $sqlFilter = "Select id from Image where id in ($FileIds) and nobject_scamp < $nstars and run = '$filter_run'";

#print "\n the query $sqlFilter ";
my $sthFilter = $desdbh->prepare($sqlFilter);
$sthFilter->execute();
#print "\n found ",$sthFilter->rows," rows ";

my $bandExposureHashRef;
my ($bandHashRef, $fileInfoHashref, $fvalDel,$i);
my @removeFileIds;
$i=0;
while (my $rowFilter = $sthFilter->fetchrow_hashref()){
	# store the rows selected into a fileinfoHashref
	$fileInfoHashref->{$rowFilter->{'id'}} = $rowFilter;
}
##print "\n fileinfohas ", scalar(keys %$fileInfoHashref);
# Loop over all the files found in the first filter. in the loop remove all those files which belong to a band which has less than predetermined minimum number of exposures
foreach my $fval (keys %$files) {

	#$FileIds .= $fval->{'id'}.', ';
#	print "\n evaluating for run ",$files->{$fval}->{'run'}," against $filter_run";
	if($files->{$fval}->{'run'} eq $filter_run){
#	print "\n looking for,", Dumper($fileInfoHashref->{$files->{$fval}->{'id'}});
		unless (exists $fileInfoHashref->{$files->{$fval}->{'id'}}){
			print "\n STATUS3BEG removing file for stars(nobject_scamp) greater than $nstars. ID: ",$files->{$fval}->{'id'}," STATUS3END\n";
			delete($files->{$fval});
		}else{
			#print "\n keeping the id ",$files->{$fval}->{'id'};
		}
	}else{
		print "\n STATUS3BEG removing the id for run.  ",$files->{$fval}->{'id'}," STATUS3END\n";
		delete($files->{$fval});
	}
}

#print "\n final size, ",scalar(keys %$files);
# count flat types by bands
my %byband = ();
#foreach my $fval (values %$files) {
#    my $band = $fval->{'band'};
#    if ($fval->{'filetype'} eq 'raw_dflat') {
#        $byband{$band}{'flatcor'}++;
#    }
#    elsif ($fval->{'filetype'} eq 'raw_tflat') {
#        $byband{$band}{'tflatcor'}++;
#    }
#    else {
#        print STDERR "Error: Unknown filetype '", $fval->{'filetype'}, "' for file ", $fval->{'filename'}, " (id=", $fval->{'id'},")\n";
#        exit $FAILURE;
#    }
#}

#print "BYBAND\n";
#print Dumper(\%byband), "\n";

# select flat type to use for each band
#foreach my $band (keys %byband) {
#    if (defined($byband{$band}{'flatcor'}) && ($byband{$band}{'flatcor'} > 0)) {
#        $byband{$band}{'flattype'} = 'flatcor';
#    }
#    elsif (defined($byband{$band}{'tflatcor'}) && ($byband{$band}{'tflatcor'} > 0)) {
#        $byband{$band}{'flattype'} = 'tflatcor';
#    }
#    else {
#        print STDERR "Error: Could not determine flat type for band '$band'\n";
#        exit $FAILURE;
#    }
#}
#
# save flattype so that orch can pass on to science code
#foreach my $fval (values %$files) {
#    $fval->{'flattype'} = $byband{$fval->{'band'}}{'flattype'};
#}

#print "\n the file list in the end";
#print Dumper($files);

my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);


$desdbh->disconnect();


sub usage
{

	print "\n perl MkIllumcorList.pl -nite <nite> -project <project> -nstars <max number of stars in a ccd> -outputxml <name of output xml file> -curr_run <run (optional)> -query_run <optional> -block_num <optional>\n";

}
