#!/usr/bin/env perl
########################################################################
#  $Id: CoaddSwarpQuery.pl 5764 2010-08-03 20:51:03Z rearmstr $
#
#  $Rev:: 5764                             $:  # Revision of last commit.
#  $LastChangedBy:: rearmstr               $:  # Author of last commit.
#  $LastChangedDate:: 2010-08-03 15:51:03 #$:  # Date of last commit.
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#     Since coadd pipeline currently has issues if only 1 remap,
#     this query for dummy block checks that > 1 remap
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;
use Orchestration::filelistfunc;

my ($outputxml, $tilename, $project, $minexptime, $red_run, $curr_run, $query_run, $block_num) = undef;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'tilename=s' => \$tilename,
    'project=s' => \$project,
    'minexptime=s' => \$minexptime,
    'red_run=s' => \$red_run,
    'curr_run=s' => \$curr_run,
    'query_run=s' => \$query_run,
    'block_num=i' => \$block_num,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($tilename)) {
    print "Error: Must specify tilename\n";
    exit $FAILURE;
}

if (!defined($project)) {
    print "Error: Must specify project\n";
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    exit $FAILURE;
}

# values to use in 'where' portion of DB query
my %key_vals = ( project => $project, tilename => $tilename, fileclass=>'red', filetype=>'remap' );
setQueryRun($query_run, $block_num, $curr_run, \%key_vals);

if (defined($red_run)) {
    my $value = $red_run;
    # make an array ref if list of values
    if ($red_run =~ /,/) {
        my @arr = split /\s*,\s*/, $value;
        $value = \@arr;
    }   
    $key_vals{'run'} = $value;
}

my %imagevals = (scampflg => 0) ;
if (defined($minexptime) && ($minexptime > 0)) {
   $imagevals{'exptime'} = ">$minexptime";
}

my %query;
%query = (
    'location' => {
        'join' => {'id' => 'image.id'},
        'key_vals' => \%key_vals,
    },
    'image' => {
        'key_vals' => \%imagevals,    
    } 
);

my $remapfiles = getFileList(\%query);
my $numremaps = scalar(keys %$remapfiles);

my %exposures = ();
foreach my $fkey (keys %$remapfiles) {
    $exposures{$remapfiles->{$fkey}->{'exposurename'}} = 1;
}

print "\nNumber of exposures = ", scalar(keys %exposures), "\n";
if (scalar(keys %exposures) < 2) {
    print STDERR "\nError: Need at least 2 exposures currently to do coadd pipeline.\n\n";
    print STDERR "Aborting\n";
    exit $FAILURE;
}

my $lines = convertSingleFilesToLines($remapfiles);
outputXMLList($outputxml, $lines);
exit 0;
