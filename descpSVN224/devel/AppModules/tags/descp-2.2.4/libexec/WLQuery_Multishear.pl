#!/usr/bin/perl -w
use warnings;
use strict;

use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::filelistfunc;

$| = 1;

my $image_table = 'IMAGE';
my $location_table = 'LOCATION';
my $wl_table = 'WL';

my (%opts, %red_ids,%red_expids,%red_bkg_ids,%shpltall_ids,
    %shpltpsf_ids,
    %shpltpsfmodel_ids,%shear_ids);

my $db = DB::DESUtil->new('verbose' => 3);

#my %location_cols = map{$_ => 1} @{$db->getColumnNames->($location_table)};
#my %image_cols = map {$_ => 1} @{$db->getColumnNames->($image_table)};

# From list of accesable location table columns(below), contruct GetOpt definition
# that allows multiple values to be interpreted as 'OR' ie "-band -band g".
# Add other command line dfs in the call to getopt:
#
my %location_cols = (
 id => 1,
 fileclass => 1,
 filename => 1,
 filedate => 1,
 run  => 1,
 nite => 1,
 band => 1,
 tilename  => 1,
 exposurename => 1,
 ccd => 1,
 project => 1,
);

my @DB_opts = map {lc($_)."=s@"} keys %location_cols;

Getopt::Long::GetOptions (\%opts,
  'current_run|current-run|cr|curr_run=s',
  'output_file|output-file|out-file|of|outputxml|output-xml|out-xml=s',
  'remap_run|=s',			  
  'stage_file|stage-file|sf=s',
  'help|h|?',
  'man',
  'verbose|V=i',
  'query_run=s',
  'block_num=i',
   @DB_opts
) or Pod::Usage::pod2usage(-verbose => 0,-exitval => 2,-output => \*STDERR);

# Display documentation and exit if requested:
if ($opts{'help'}) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($opts{'man'}) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

Pod::Usage::pod2usage(-message => 'Must provide a tilename.'-verbose => 0,-exitval => 2,-output => \*STDERR) if (! $opts{'tilename'});
my $value = $opts{'remap_run'} ;


if ($value =~ /,/) {
    my @arr = split /\s*,\s*/, $value;
    $value = \@arr;
}

# Construct basic remap queryDB call then add a "key_vals"
# argument for everything that came in on the command line:
#
my %query = ( 
  $location_table => {
      'join' => {'id' => 'image.id'},
      'select_fields' => 'all',
      'key_vals' => {
	  'filetype' => 'remap',
          'archivesites' => '[^N]'
    }
  },
  $image_table => {
    'select_fields' => ['parentid']
  },
  'hash_key' => 'id'
);

my $curr_run =  $value ;
setQueryRun($opts{'query_run'}, $opts{'block_num'},$curr_run, $query{$location_table}->{'key_vals'});

foreach my $key (keys %location_cols) {
  if (exists $opts{$key}) {
    $query{$location_table}->{'key_vals'}->{$key} = $opts{$key};
  }
}

## Get remap list, filter to latest run and print message:
my $RemapRows = $db->queryDB2(%query);

removeFilesFromOldRuns($RemapRows);
#print Dumper($RedRows);
my $N_remaps = scalar keys %$RemapRows;
print "\nNumber of matched \"latest\" remap files: $N_remaps\n";

## Create a unique list of red location.ids
## Retrive some location table information about the red files
## then print count message:
##

foreach my $Fkeys (values %$RemapRows) {
    $red_ids{$Fkeys->{'parentid'}} = 1 
        if ($Fkeys->{'archivesites'} =~ /[^N]/);
}


my $RedRows = $db->queryDB2 (
  $location_table => {
      'join' => {'id' => 'image.id'},
      'key_vals' => {
          'id' => [keys %red_ids],
          'archivesites' => '[^N]'
      },
     'select_fields' => 'all'
  },
    $image_table => {
    'select_fields' => ['exposureid','nite']
  },
  'hash_key' => 'id'
);

removeFilesFromOldRuns($RedRows);
my $N_reds = scalar keys %$RedRows;
print "\nNumber of matched \"latest\" red files: $N_reds\n";

foreach my $Fkeys (values %$RedRows) {
    $red_expids{$Fkeys->{'exposureid'}} = 1 
}


# get red_bkg ids these will be staged 

my $BkgRows = $db->queryDB2 (
    $location_table => {
        'join' => {'id' => 'image.id'},
        'select_fields' => 'all',
        'key_vals' => {
            'filetype' => 'red_bkg',
            'archivesites' => '[^N]'
        }
    },
    $image_table => {
        'select_fields' => ['parentid','nite'],
        'key_vals' => {
            'exposureid' => [keys %red_expids]
        },
    },
    'hash_key' => 'id'
);

my $RedBkgRows=MatchBkg($RedRows,$BkgRows);
foreach my $Fkeys (values %$RedBkgRows) {
  $red_bkg_ids{$Fkeys->{'id'}} = 1 if ($Fkeys->{'archivesites'} =~ /[^N]/);
}
my $N_bkg = scalar keys %$BkgRows;
print "\nNumber of matched red_bkg files: $N_bkg\n";


# Get shaplt_all ids

my $ShpltAllRows = $db->queryDB2 (
    $wl_table => {
        'key_vals' => {
            'parentid' => [keys %red_ids]
        },
        'join' => {'id' => 'location.id'},
        'select_fields' => ['parentid']
    },
    $location_table => {
        'select_fields' => 'all',
        'key_vals' => {
            'filetype' => 'shapelet_shpltall',
            'archivesites' => '[^N]'
        }
    },
    'hash_key' => 'id'
);

removeFilesFromOldRuns($ShpltAllRows);
foreach my $Fkeys (values %$ShpltAllRows) {
  $shpltall_ids{$Fkeys->{'id'}} = 1 if ($Fkeys->{'archivesites'} =~ /[^N]/);
}
my $N_shpltall = scalar keys %$ShpltAllRows;
print "\nNumber of matched shpltall files: $N_shpltall\n";

# Get shaplt_psf ids

my $ShpltPsfRows = $db->queryDB2 (
    $wl_table => {
        'key_vals' => {
            'parentid' => [keys %shpltall_ids]
        },
        'join' => {'id' => 'location.id'},
        'select_fields' => ['parentid']
    },
    $location_table => {
        'select_fields' => 'all',
        'key_vals' => {
            'filetype' => 'shapelet_shpltpsf',
            'archivesites' => '[^N]'
        }
    },
    'hash_key' => 'id'
);

removeFilesFromOldRuns($ShpltPsfRows);
foreach my $Fkeys (values %$ShpltPsfRows) {
  $shpltpsf_ids{$Fkeys->{'id'}} = 1 if ($Fkeys->{'archivesites'} =~ /[^N]/);
}
my $N_shpltpsf = scalar keys %$ShpltPsfRows;
print "\nNumber of matched shpltpsf files: $N_shpltpsf\n";



# Get shaplt_psfmodel ids
# index psfmodel by parentid instead of id
# to make easy matching
my $ShpltPsfModelRows = $db->queryDB2 (
    $wl_table => {
        'key_vals' => {
            'parentid' => [keys %shpltall_ids]
        },
        'join' => {'id' => 'location.id'},
        'select_fields' => ['parentid']
    },
    $location_table => {
        'select_fields' => 'all',
        'key_vals' => {
            'filetype' => 'shapelet_psfmodel',
            'archivesites' => '[^N]'
        }
    },
    'hash_key' => 'parentid'
);

removeFilesFromOldRuns($ShpltPsfModelRows);
foreach my $Fkeys (values %$ShpltPsfModelRows) {
  $shpltpsfmodel_ids{$Fkeys->{'id'}} = 1 if ($Fkeys->{'archivesites'} =~ /[^N]/);
}
my $N_shpltpsfmodel = scalar keys %$ShpltPsfModelRows;
print "\nNumber of matched shpltpsfmodel files: $N_shpltpsfmodel\n";



# Get shaplt_shear ids
my $ShearRows = $db->queryDB2 (
    $wl_table => {
        'key_vals' => {
            'parentid' => [keys %shpltpsf_ids]
        },
        'join' => {'id' => 'location.id'},
        'select_fields' => ['parentid']
    },
    $location_table => {
        'select_fields' => 'all',
        'key_vals' => {
            'filetype' => 'shapelet_shear',
            'archivesites' => '[^N]'
        }
    },
    'hash_key' => 'id'
);

removeFilesFromOldRuns($ShearRows);
foreach my $Fkeys (values %$ShearRows) {
  $shear_ids{$Fkeys->{'id'}} = 1 if ($Fkeys->{'archivesites'} =~ /[^N]/);
}
my $N_shear = scalar keys %$ShearRows;
print "\nNumber of matched shear files: $N_shear\n";


#
## Die if unable to return a complete set of matching red/red_cat files:
##if ($N_reds != $N_redcats) {
##  die "ERROR: Found non matching number of red and red_cat files.\n";
##}
#
## Generate XML output file:
if ($opts{'output_file'}) {
  my $outputxml = $opts{'output_file'};
  outputXMLList_WL($outputxml, $RedRows,$RedBkgRows,$ShpltAllRows,
                   $ShpltPsfRows,$ShpltPsfModelRows,$ShearRows);

}

if ($opts{'stage_file'}) {
    my $file = $opts{'stage_file'};
    open FH, "> $file";
    while ((my $id, my $keys) = each %$ShearRows) {
        my $shear = $id;
        my $psf = $keys->{'parentid'};
        my $all = $ShpltPsfRows->{$psf}->{'parentid'};
        my $psfmodel = $ShpltPsfModelRows->{$all}->{'id'};
        my $red = $ShpltAllRows->{$all}->{'parentid'};
        my $red_bkg = $RedBkgRows->{$red}->{'id'};

        print FH "<file>\n","fileid=$shear\n","</file>\n";
        print FH "<file>\n","fileid=$psf\n","</file>\n";
        print FH "<file>\n","fileid=$psfmodel\n","</file>\n";
        print FH "<file>\n","fileid=$red\n","</file>\n";
        print FH "<file>\n","fileid=$red_bkg\n","</file>\n";
    }
    close FH;
}

#
#
sub removeFilesFromOldRuns {
    my ($Rows, $CurrentRun) = @_;

  my @unique_keys = ('project', 'fileclass', 'filetype', 'exposurename', 'filename', 'tilename', 'band', 'ccd');
  my %List;

  if (ref $Rows eq 'ARRAY') { 
    for (my $i=0; $i<=$#$Rows; $i++ ) {
      my $File = $Rows->[$i];
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }
      if (exists $List{"$str"}) {
        my ($this_date, $junk1) = split (/_/, $File->{'run'});
        my ($other_date, $junk2) = split (/_/, $Rows->[$List{"$str"}]->{'run'});
        if ($this_date gt $other_date) {
          $List{"$str"} = $i;
        }
      }
      else {
        $List{"$str"} = $i;
      }
    }
  }

  
  elsif (ref $Rows eq 'HASH') {
    while ((my $id, my $File) = each %$Rows) {
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }
      
      #Add a check for undef band which occur in the wl tables
      if(!$File->{"band"}) {
	  delete $Rows->{$id};
      }
      elsif (exists $List{"$str"}) {
	  my $existing_id = $List{"$str"};
#        warn "\nWARNING: Should not the same red file from multiple runs at this stage.\n";
#        warn "This means the set of remaps contains to parents that are the same red file\n";
#        warn "but from different runs.\n";
#        warn "Keeping latest red and continuing.\n";
        my ($this_date, $junk1) = split (/_/, $File->{'run'});
        my ($existing_date, $junk2) = split (/_/, $Rows->{$existing_id}->{'run'});
        if ($this_date gt $existing_date) {
          $List{"$str"} = $id;
          print "Kept: ",$Rows->{$id}->{'run'},"  Discarded: ", $Rows->{$existing_id}->{'run'},"\n";
          delete $Rows->{$existing_id};
        }
        else {
          print "Kept: ",$Rows->{$existing_id}->{'run'},"  Discarded: ", $Rows->{$id}->{'run'},"\n";
          delete $Rows->{$id};
          
        }
      }
      else {
        $List{"$str"} = $id;
      }
    }
  }

  return \%List;

}
#
exit 0;

sub MatchBkg {
    my ($red_files, $bkg_files) = @_;
    my @unique_keys = ('project', 'exposurename', 'band', 'ccd');
    my (%red_list,%bkg_list);

    # Fill List with identifier from reds
    while ((my $id, my $File) = each %$red_files) {
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }
      $red_list{"$str"} = $id;
    }

    # Try to match each bkg to a red, if it is not
    # in the list delete it.  Also reassign the
    # red id to the new list index
    while ((my $id, my $File) = each %$bkg_files) {
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }

      if (exists $red_list{"$str"}) {
	  my $red_id = $red_list{"$str"};
          $bkg_list{$red_id}=$File;
      }
      else {
          delete $bkg_files->{$id};
      }
    }
    return \%bkg_list;
    
}
#
exit 0;






sub outputXMLList_WL {
    my ($outputxml, $redfiles, $redbkgfiles,$allfiles,$psffiles, 
        $psfmodelfiles,$shearfiles) = @_;

    open FH, "> $outputxml" or die "Error: Could not write xml to file $outputxml";
    print FH "<list>\n";
    while (my ($id, $shear) = each (%$shearfiles) ) {
	print FH "\t<line>\n";
	print FH "\t\t<file nickname='shear'>\n";
	foreach my $key (keys %$shear) {
	    if (defined($shear->{$key})) {
		if (lc($key) eq 'ccd') {
		    print FH "\t\t\t<$key>", sprintf("%02d",$shear->{$key}), "</$key>\n";
		} 
		else {
		    print FH "\t\t\t<$key>", $shear->{$key}, "</$key>\n";
		}
	    }
	    else {
#                    print "Undefined key $key for file ", $shear->{'id'}, "\n";
	    }
	}
	print FH "\t\t\t<fileid>", $shear->{'id'}, "</fileid>\n";
	print FH "\t\t</file>\n";
	

	my $psf_id = $shear->{'parentid'};
	my $psf=$psffiles->{$psf_id};
        my $all_id=$psf->{'parentid'};
	my $all=$allfiles->{$all_id};
	my $psfmodel=$psfmodelfiles->{$all_id};
	my $psfmodel_id=$psfmodel->{'id'};
	
        my $red_id=$all->{'parentid'};
        my $red=$redfiles->{$red_id};
        my $red_bkg=$redbkgfiles->{$red_id};


        print FH "\t\t<file nickname='psfmodel'>\n";
	foreach my $key (keys %$psfmodel) {
	    if (defined($psfmodel->{$key})) {
		if (lc($key) eq 'ccd') {
		    print FH "\t\t\t<$key>",sprintf("%02d",$psfmodel->{$key}),"</$key>\n";
		} 
		else {
		    print FH "\t\t\t<$key>", $psfmodel->{$key}, "</$key>\n";
		}
	    }
	    else {
#                    print "Undefined key $key for file ", $red->{'id'}, "\n";
	    }
	}
	print FH "\t\t\t<fileid>", $psfmodel->{'id'}, "</fileid>\n";
	print FH "\t\t</file>\n";
	

       
       print FH "\t\t<file nickname='red'>\n";
       foreach my $key (keys %$red) {
           if (defined($red->{$key})) {
       	if (lc($key) eq 'ccd') {
       	    print FH "\t\t\t<$key>", sprintf("%02d",$red->{$key}), "</$key>\n";
       	} 
       	else {
       	    print FH "\t\t\t<$key>", $red->{$key}, "</$key>\n";
       	}
           }
           else {
#                   print "Undefined key $key for file ", $red->{'id'}, "\n";
           }
       }
       print FH "\t\t\t<fileid>", $red->{'id'}, "</fileid>\n";
       print FH "\t\t</file>\n";
	

       print FH "\t\t<file nickname='red_bkg'>\n";
       foreach my $key (keys %$red_bkg) {
           if (defined($red_bkg->{$key})) {
       	if (lc($key) eq 'ccd') {
       	    print FH "\t\t\t<$key>", sprintf("%02d",$red_bkg->{$key}), "</$key>\n";
       	} 
       	else {
       	    print FH "\t\t\t<$key>", $red_bkg->{$key}, "</$key>\n";
       	}
           }
           else {
#                   print "Undefined key $key for file ", $red->{'id'}, "\n";
           }
       }
       print FH "\t\t\t<fileid>", $red_bkg->{'id'}, "</fileid>\n";
       print FH "\t\t</file>\n";
	
	print FH "\t</line>\n";
    }
    
    print FH "</list>\n";
    close FH;
}
















