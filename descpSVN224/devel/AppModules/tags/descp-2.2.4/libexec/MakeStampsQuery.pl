#!/usr/bin/env perl
########################################################################
#  $Id:                                               $
#
#  $Rev:: 3263                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-03-06 13:51:04 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2009 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################

use strict;
use warnings;

use Getopt::Long;
use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use DB::EventUtils;
use Orchestration::misc;
use Orchestration::filelistfunc;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($project, $nite, $difftype, $outputxml, $verbose, $debug) = undef;
my ($query_run, $curr_run, $block_num) = undef;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'project=s' => \$project,
    'nite=s' => \$nite,
    'difftype=s' => \$difftype,
    'outputxml=s' => \$outputxml,
    'verbose=i' => \$verbose,
    'debug=i' => \$debug,
    'query_run=s' => \$query_run,
    'curr_run=s' => \$curr_run,
    'block_num=i' => \$block_num,
);

if (!defined($project)) {
    print "Error: Must specify project\n";
    exit $FAILURE;
}

if (!defined($nite)) {
    print "Error: Must specify nite\n";
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    exit $FAILURE;
}

if (!defined($difftype)) {
    print "Error: Must specify difftype\n";
    exit $FAILURE;
}
$difftype = lc($difftype);
if (($difftype ne 'red') && ($difftype ne 'nitecmb')) {
    print "Error: Invalid difftype value.  Must be either 'red' or 'nitecmb'\n";
    exit $FAILURE;
}

if (!defined($verbose)) {
    $verbose = 1;
}
if (!defined($debug)) {
    $verbose = 0;
}

my %keyvals = ('project' => $project,
            'nite' => $nite,
            'fileclass' => 'diff',
);

setQueryRun($query_run, $block_num, $curr_run, \%keyvals);

if ($difftype eq 'red') {
    $keyvals{'filetype'} = 'diff';
}
else {
    $keyvals{'filetype'} = 'diff_nc';
}

# get list of difference images 
my %queryDiff = (
    'location' => { 
        'join' => {'id' => 'image.id'},
        'select_fields' => 'all',
        'key_vals' => \%keyvals,
    },
    'image' => {
        'select_fields' => ['parentid'],
    }
);

my $listDiff = getFileList(\%queryDiff, $debug);
my $numDiff = scalar(keys %$listDiff);
reportEvent($verbose, "STATUS", 1, "Original number of diff files = $numDiff");

my $db = DB::DESUtil->new('verbose'=>$verbose, 'debug'=>$debug);

# get info for parents which should be distorted template
my @idsDiff;
foreach my $fhref (values %$listDiff) {
    if (defined($fhref->{'parentid'}) && $fhref->{'parentid'} != 0) {
        push(@idsDiff, $fhref->{'parentid'});
    }
}
if (scalar(@idsDiff) == 0) {
    reportEvent($verbose, "STATUS", 5, "Error: parentid=0 for all diff files");
    $db->disconnect();
    exit $FAILURE;
}

my %queryTmpl = (
    'location' => { 
        'join' => {'id' => 'image.id'},
        'select_fields' => 'all',
        'key_vals' => {
            'id' => \@idsDiff,
            'archivesites' => '[^N]',
        }
    },
    'image' => {
        'select_fields' => ['parentid'],
    },
    'hash_key' => 'id'
);
my $listTmpl = $db->queryDB2(%queryTmpl);
my $numTmpl = scalar(keys %$listTmpl);
reportEvent($verbose, "STATUS", 1, "Original number of template files (distmp) = $numTmpl");

# get info for parents which should be reds or nitely combines
my @idsTmpl;
foreach my $fhref (values %$listTmpl) {
    if ($fhref->{'parentid'} != 0) {
        push(@idsTmpl, $fhref->{'parentid'});
    }
}
my %querySearch = (
    'location' => { 
        'join' => {'id' => 'image.id'},
        'select_fields' => 'all',
        'key_vals' => {
            'id' => \@idsTmpl,
            'archivesites' => '[^N]',
        }
    },
    'image' => {
        'select_fields' => ['parentid'],
    },
    'hash_key' => 'id'
);
my $listSearch = $db->queryDB2(%querySearch);
$db->disconnect();
my $numSearch = scalar(keys %$listSearch);
reportEvent($verbose, "STATUS", 1, "Original number of search files (red/nitecmb) = $numSearch");

# Combine lists into 1
my %lines = ();
my $count = 1;
foreach my $fhref (values %$listDiff) {
    if (defined($fhref->{'parentid'})) {
        if (defined($listTmpl->{$fhref->{'parentid'}})) {
            my $distmp = $listTmpl->{$fhref->{'parentid'}};
            if (defined($distmp->{'parentid'})) {
                my $search = $listSearch->{$distmp->{'parentid'}};
                $lines{$count}{'diff'} = $fhref;
                $lines{$count}{'distmp'} = $distmp;
                $lines{$count}{'search'} = $search;
                $count++;
            }
        }
    }
}

reportEvent($verbose, "STATUS", 1, "Final number of lines = ".scalar(keys %lines));
if (scalar(keys %lines) != $numDiff) {
    reportEvent($verbose, "STATUS", 4, "Warning final number of lines does not equal number of diff files in DB");
}

outputXMLList($outputxml, \%lines);
