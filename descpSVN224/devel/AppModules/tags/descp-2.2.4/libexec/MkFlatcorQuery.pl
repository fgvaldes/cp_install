#!/usr/bin/env perl
########################################################################
#  $Id: MkFlatCor.pl 3381 2009-04-02 20:28:07Z mgower $
#
#  $Rev:: 3381                             $:  # Revision of last commit.
#  $LastChangedBy:: Ankit Chandra                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-04-02 15:28:07 #$:  # Date of last commit.
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#     Creates master list of input files for mkflatcor
#     For each band, 
#         if dome flats
#             add to master list 
#         else if twilight sky flats 
#             add to master list 
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;
use Orchestration::filelistfunc;
use DB::EventUtils;

my ($outputxml, $nite, $project, $curr_run, $query_run, $block_num, $sat_pixels, $min_exposures, $min_ccds) = undef;
my $debug = 0;

use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'nite=s' => \$nite,
    'project=s' => \$project,
    'curr_run=s' => \$curr_run,
    'query_run=s' => \$query_run,
    'block_num=i' => \$block_num,
    'sat_pixels=i' => \$sat_pixels,
    'min_exposures=i' => \$min_exposures,
    'min_ccds=i' => \$min_ccds,
    'debug=i' => \$debug,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($nite)) {
    print "Error: Must specify nite\n";
    usage();
    exit $FAILURE;
}

if (!defined($project)) {
    print "Error: Must specify project\n";
    usage();
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    usage();
    exit $FAILURE;
}

if (!defined($sat_pixels)) {
    print "Error: Must specify maximum saturated pixels in a ccd\n";
    usage();
    exit $FAILURE;
}
if (!defined($min_exposures)) {
    print "Error: Must specify minimum number of exposures in a band\n";
    usage();
    exit $FAILURE;
}

if (!defined($min_ccds)) {
    print "Error: Must specify minimum number of ccds\n";
    usage();
    exit $FAILURE;
}



#
# Make a database connection
#
my $desdbh = DB::DESUtil->new();



# values to use in 'where' portion of DB query
my %key_vals = ( project => $project, fileclass => 'red', nite=>$nite);
$key_vals{'filetype'} = ['raw_dflat', 'raw_tflat'];

setQueryRun($query_run, $block_num, $curr_run, \%key_vals);

my %query = ();
$query{'location'}->{'key_vals'} = \%key_vals;
my $files = getFileList(\%query, $debug);

#print "\n the file list ";
#print Dumper($files);

my ($FileIds,$exposureIds,$tempExp);
$exposureIds = '';
$FileIds = '';
foreach my $fval (values %$files) {

    $FileIds .= $fval->{'id'}.', ';
    $tempExp = $fval->{'exposurename'};
    if($exposureIds !~ /$tempExp/){
	    $exposureIds .= $tempExp.", ";
	}
}


$FileIds = substr $FileIds, 0 , -2;

if($FileIds !~ /\S+/){
###
## Usage: verbosity, type of message, level, string 
###
DB::EventUtils::reportEvent( 2, 'STATUS', 3, "No files to work on. Exiting with non zero status code" );
$desdbh->disconnect();;
exit(1);
}

$exposureIds = substr $exposureIds, 0 , -2;
###
## Usage: verbosity, type of message, level, string 
###
DB::EventUtils::reportEvent( 2, 'STATUS', 3, "All exposurenames before filtering on nsatpix: $exposureIds" );
# select id from image where id in (get the image ids where nsatpix is less than the prescribed cut off and also filter out all the files whose exposureids are equal to those of the fileids found here.) and (filter out those bands whose total number of exposures are lesser than the prescribed number. you can get this number by doing a group by on band)

my $sqlFilter = "Select a.id as id ,a.exposureid as exposureid ,a.band as band from Image a, Image b where a.id in ($FileIds) and b.id = a.id and a.nsatpix < $sat_pixels and b.exposureid in ( select exposureid from Image where id in ($FileIds) and nsatpix < $sat_pixels group by exposureid having count(id) >= $min_ccds)";

#print "\n the query $sqlFilter ";
my $sthFilter = $desdbh->prepare($sqlFilter);
$sthFilter->execute();

my $bandExposureHashRef;
my ($bandHashRef, $fileInfoHashref, $fvalDel,$i);
my @removeFileIds;
$i=0;
$tempExp = '';
while (my $rowFilter = $sthFilter->fetchrow_hashref()){

	if(! exists $bandExposureHashRef->{$rowFilter->{'band'}.'_'.$rowFilter->{'exposureid'}}){
		$bandHashRef->{$rowFilter->{'band'}}++;
		#print "\n logging exposure ",$rowFilter->{'exposureid'}, "for band ",$rowFilter->{'band'};
		$bandExposureHashRef->{$rowFilter->{'band'}.'_'.$rowFilter->{'exposureid'}} = 1;
		$tempExp .= $rowFilter->{'exposureid'}.",";
	}

	#print "\n band ",$rowFilter->{'band'}," ";
	
	# store the rows selected into a fileinfoHashref
	$fileInfoHashref->{$rowFilter->{'id'}} = $rowFilter;
}

$tempExp = substr $tempExp, 0 , -2;
###
## Usage: verbosity, type of message, level, string 
###
DB::EventUtils::reportEvent( 2, 'STATUS', 3, "got ".$sthFilter->rows." rows after filtering files on the basis of NSATPIX , min. number of CCDs. The list of exposures ids which qualify: $tempExp" );


$tempExp = '';
$exposureIds = '';
# Loop over all the files found in the first filter. in the loop remove all those files which belong to a band which has less than predetermined minimum number of exposures
foreach my $fileId (keys %$fileInfoHashref) {

#	print "\n checking for band ",$bandHashRef->{$fileInfoHashref->{$fileId}->{'band'}}, "against min exp $min_exposures ";

	# Look up the band for this file. Use the band to look up the bandHashref and see if that band has less than the minimum number of exposures. if it does, then remove the file.	
	if (exists $bandHashRef->{$fileInfoHashref->{$fileId}->{'band'}} && $bandHashRef->{$fileInfoHashref->{$fileId}->{'band'}} < $min_exposures){
	
		foreach my $fvalDel (keys %$files) {
    			if( exists $files->{$fvalDel}->{'id'} && $files->{$fvalDel}->{'id'}== $fileId){
				$tempExp = $files->{$fvalDel}->{'exposurename'};
				if ($exposureIds !~ /$tempExp/){  
					$exposureIds .= $tempExp.", ";
				}
				delete ($files->{$fvalDel});
				$i++;
			}
		}


	}else{
		#print "\n ",$bandHashRef->{$fileId}->{'band'}, " was fine...";
	}

}

$exposureIds = substr $exposureIds, 0 , -2;
###
## Usage: verbosity, type of message, level, string 
###
DB::EventUtils::reportEvent( 2, 'STATUS', 3, "Deleted $i files from the list, which belonged to a band with less than $min_exposures exposures . The list of exposure names removed: $exposureIds" );

# count flat types by bands
my %byband = ();
foreach my $fval (values %$files) {
    my $band = $fval->{'band'};
    if ($fval->{'filetype'} eq 'raw_dflat') {
        $byband{$band}{'flatcor'}++;
    }
    elsif ($fval->{'filetype'} eq 'raw_tflat') {
        $byband{$band}{'tflatcor'}++;
    }
    else {
        print STDERR "Error: Unknown filetype '", $fval->{'filetype'}, "' for file ", $fval->{'filename'}, " (id=", $fval->{'id'},")\n";
        exit $FAILURE;
    }
}

#print "BYBAND\n";
#print Dumper(\%byband), "\n";

# select flat type to use for each band
foreach my $band (keys %byband) {
    if (defined($byband{$band}{'flatcor'}) && ($byband{$band}{'flatcor'} > 0)) {
        $byband{$band}{'flattype'} = 'flatcor';
    }
    elsif (defined($byband{$band}{'tflatcor'}) && ($byband{$band}{'tflatcor'} > 0)) {
        $byband{$band}{'flattype'} = 'tflatcor';
    }
    else {
        print STDERR "Error: Could not determine flat type for band '$band'\n";
        exit $FAILURE;
    }
}

# save flattype so that orch can pass on to science code
foreach my $fval (values %$files) {
    $fval->{'flattype'} = $byband{$fval->{'band'}}{'flattype'};
}

#print "\n the file list in the end";
#print Dumper($files);

my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);


$desdbh->disconnect();


sub usage
{

	print "\n perl MkFlatcorQuery.pl -nite <nite> -project <project> -sat_pixels <max number of saturated pixels> -min_exposures <min number of exposures necessary in a band> -min_ccds <min number of ccds necessary in an exposure> -outputxml <name of output xml file> -curr_run <run (optional)> -query_run <optional> -block_num <optional>\n";

}
