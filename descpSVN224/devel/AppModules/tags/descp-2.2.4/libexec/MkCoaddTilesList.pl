#!/usr/bin/env perl
########################################################################
#  $Id: MkFlatCor.pl 3381 2009-04-02 20:28:07Z mgower $
#
#  $Rev:: 3381                             $:  # Revision of last commit.
#  $LastChangedBy:: Ankit Chandra                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-04-02 15:28:07 #$:  # Date of last commit.
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
# Create a list for tiles which can be used as an input to the mass submit scripts
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5" , "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;
use Orchestration::filelistfunc;
use DB::EventUtils;

my ($outputxml, $nite, $project, $curr_run, $query_run, $block_num, $sat_pixels, $min_exposures, $min_ccds,$remap_run,$output_file) = undef;
my $debug = 0;

use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'project=s' => \$project,
    'remap_runs=s' => \$remap_run,
    'output_file=s' => \$output_file,
    'debug=i' => \$debug,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($project)) {
    print "Error: Must specify project\n";
    usage();
    exit $FAILURE;
}

if (!defined($remap_run)) {
    print "Error: Must specify the remap run\n";
    usage();
    exit $FAILURE;
}
if (!defined($output_file)) {
    print "Error: Must specify output file\n";
    usage();
    exit $FAILURE;
}

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new();

$remap_run =~ s/(\w+)\s*(,?)/'$1'$2/g;

my $sqlFilter = " Select distinct(tilename, run) as combination,run,tilename from location where run in ($remap_run) and filetype = 'remap' and project = '$project'";

#print "\n the query $sqlFilter ";
my $sthFilter = $desdbh->prepare($sqlFilter);
$sthFilter->execute();
#print "\n found ",$sthFilter->rows," rows ";

my ($fileInfoHashref);
while (my $rowFilter = $sthFilter->fetchrow_hashref()){
        # store the rows selected into a fileinfoHashref
        $fileInfoHashref->{$rowFilter->{'combination'}} = $rowFilter;
}

open(FH,">$output_file");

foreach my $distinctRunTile (keys %$fileInfoHashref){
	
	print FH $fileInfoHashref->{$distinctRunTile}->{'tilename'},"	",$fileInfoHashref->{$distinctRunTile}->{'run'},"\n";

}

#print Dumper($fileInfoHashref);

close(FH);
$desdbh->disconnect();


sub usage
{

	print "\n perl MkCoaddTilesList.pl  -project <project> -output_file <name of output file> -remap_runs <comma separated list of all remap runs>\n";

}
