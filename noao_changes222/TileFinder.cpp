#include "TileFinder.h"
/*
 * The program  detects natural crowding of observation bore sights and indicate them
 *  as tile pointing. The input list of data files and their bore sight Ra,Dec coordinates
 *  is processed to produce a new list containing information necessary for cp_remap program
 *  The accuracy of the tile positioning is  about +/- 0.125 degree. This can be improved
 *  if corresponding request will be given.
 *
 *   Input parameters: -input <input list of files: format <file name> <Ra> <Dec>
 *                                                        <exp.time> <seeing.fwhm> <Zero Point> >
 *                     -output <filename including path where result will be stored >
 *                     -mapRes < Healpix map resolution in degrees 1.0 recommended >
 *                     -verbose <verbose level 0 - 3 >
 *                     -config < file containing configuration parameters as (key value) pairs >
 *                             < radius 2.0  - radius of search>
 *                             <exp_cut 0.1 - low exposure cut as fraction of maximum exposure >
 *                             <seeing_cut 2.0  - seeing level to cut from median fwhm >
 *                             <zp_cut 2.0 - zero_point level  to cut from median zero point >
 *          Note: file is selected if exposure time is higher that minimal exposure
 *                                 if seeing fwhm is less than maximal seeing fwhm
 *                                 if zero point is higher than minimal zero point level.
 *
 *         To switch off selection on a given parameter set the cut level to negative value.
 *
 *
 * The program perform filtering input data to cut-off files with bad observing conditions before
 * the search of tiling centers begins.
 * On the first step the program finds tiles on the bases of the WCS coordinates.
 * To deal with too big amount of exposures assigned to the same tile the  tile
 * is sub-divided on bases of exposure time modifying the tile name adding _V, _S,  _M or _L
 * depending of the exposure time. If any sub-tile still contain more that 50 exposures it
 * will be divided on a number of subtiles with adding _00, .. to the tile name.
 * As a result the tile name will look like DECAM_36031_64_S_00. Here 36031 is the pixel number,
 * 64 is the map resolution parameter, S indicate short exposure and 01 indicate the second
 * subset of exposures.
 *
 * The tile finding is working in following sequence of operations:
 *   1. The whole sky is divided on  12*nside^2 pixels (49152 in case of nside=64 and
 * 786432 in case of nside=256) the size of each pixel is 0.83 sq.deg if nside=64 and
 * 0.0525 sq.deg if nside=256.
 *   2. We suppose that observations are performed by staggered exposures with shift in pointings
 * less than or comparable with the size of the pixel.
 *   3. For each pointing we can calculate pixel number it falls on. So, several pointings
 * that belong to same coadd tile will have the same pixel number.
 *   4. We create a list of pointings with pixel numbers they belong to. Then we find pixel
 * containing maximum number of pointings in it and assume it is a center of search.
 *   5. We  search pixel numbers surrounding this center pixel within the search radius,
 * that should be of order of maximum shift in staggered exposures. Created list of pixels
 * will have pixel coordinates and number of pointings in each. Then we calculate
 * weighted center of the area, where the weight is the number of pointings.
 * The pixel containing weighted center will be assigned as the tile center and will be used
 * in the tile name.
 *   6. If the pointing list contains more than one coadd the search will repeat after excluding
 * already used pointings until all pointings will be assigned to a tile.
 *   7. A single exposure that has no neighbors within the search radius will be assigned to a
 * tile with name created using the pixel number it belong to.
 *
 *
 * Author: Nikolay Kuropatkin    05/10/2012
 * Modified: Nikolay Kuropatkin 04/10/2013
 *
 */

using namespace std;

/* function to remove proper key value from a list */

 void removeMember(std::vector<int>  *list, int key) {
	int i, nk =0;
	nk = list->size();
	for ( i=0; i<nk; i++) {
		if (list->operator [](i) == key) {
			list->erase(list->begin()+i);
			break;
		}
	}


}
 /* split string on tokens */
 void split(vector<string> &tokens, const string &text, char sep) {
   unsigned int start = 0;
   unsigned int end = 0;
//   while ((end = (unsigned int)text.find(sep, start)) <= string::npos) {
   while ((end = (unsigned int)text.find(sep, start)) <= text.length()) {
	   tokens.push_back(text.substr(start, end - start));
	   start = end + 1;
   }
   tokens.push_back(text.substr(start));
 }

 /* process string file name to extract exposure ID */
 int getExpId(string inputimage){
	 int res = 0;
	 std::vector<string> tokens;
	 split(tokens,inputimage,'/');
	 int last = tokens.size() -1;
	 string filename = tokens.operator[](last);
	 std::vector<string> tokens2;
	 split(tokens2,filename,'_');
	 string subName = tokens2.operator[](0);
	 string str3 = subName.substr(3);
//
	 char *cstr = new char[str3.length() + 1];
	 cstr = strcpy(cstr,str3.c_str());

	 res = atoi(cstr);
//
	 delete cstr;
	 return res;
 }

 /* check if the id is unique */
 bool  isUniqueId(std::vector<int> *expids, int expId){
	 	bool res = false;
		int i, nk =0;
		nk = expids->size();
		int found = 0;
		for ( i=0; i<nk; i++) {
			if (expids->operator [](i) == expId) {
				found = 1;
				break;
			}
		}
		if (found == 0) res = true;
		return res;
 }

 /* add new member to the list if the member is unique */
  void addUniqueId(std::vector<int> *expids, int expId){
		int i, nk =0;
		if (expids == NULL) cout<<" addUniqueId input vector does not exists"<<endl;
		nk = expids->size();
		if (nk <= 0){
			expids->push_back(expId);
		} else {
			int found = 0;
			for ( i=0; i<nk; i++) {
				if (expids->operator [](i) == expId) {
					found = 1;
					break;
				}
			}
			if (found == 0) expids->push_back(expId);
		}
  }
/* string trimming functions */

 const std::string trim(const std::string& pString,
                        const std::string& pWhitespace = " \t")
 {
     const size_t beginStr = pString.find_first_not_of(pWhitespace);
     if (beginStr == std::string::npos)
     {
         // no content
         return "";
     }

     const size_t endStr = pString.find_last_not_of(pWhitespace);
     const size_t range = endStr - beginStr + 1;

     return pString.substr(beginStr, range);
 }

 const std::string reduce(const std::string& pString,
                          const std::string& pFill = " ",
                          const std::string& pWhitespace = " \t")
 {
     // trim first
     std::string result(trim(pString, pWhitespace));

     // replace sub ranges
     size_t beginSpace = result.find_first_of(pWhitespace);
     while (beginSpace != std::string::npos)
     {
         const size_t endSpace =
                         result.find_first_not_of(pWhitespace, beginSpace);
         const size_t range = endSpace - beginSpace;

         result.replace(beginSpace, range, pFill);

         const size_t newStart = beginSpace + pFill.length();
         beginSpace = result.find_first_of(pWhitespace, newStart);
     }

     return result;
 }

/* double rounding  */

double dround(double input) {

	 double val = input;
	 double res;
	 res = (double)floor(val + 0.5);
	 return res;
}

/* convert pixel index into RA,Dec  */

skyPoint getRaDec(int index, PixTools *pt, int nside) {
	skyPoint res;
	double *ang = pt->pix2ang_ring(nside, index);
	double *radec = new double[2];
	radec = pt->PolarToRaDec(ang);

	res.Dec = radec[1];
	res.RA = radec[0];
	delete radec;
	return res;
}
/*  convert Ra,Dec into Healpix index */

int getIndex(skyPoint point, PixTools *pt, int nside) {
	double *radec = new double[2];
	double *ang = new double[2];
	radec[0] = point.RA;
	radec[1] = point.Dec;
	ang = pt->RaDecToPolar(radec);

	int pixel = pt->ang2pix_ring(nside, ang[0], ang[1]);
	delete radec;
	delete ang;
	return pixel;

}



/*
 * Search in disc of radius neighbor clusters and sum entries
 * then search maximum one, put it in result table and remove from search.
 * repeat search until number of entries in remaining clusters will not be <=1
 *
 */

std::vector<Tile*>*  findTile(std::map<int,int> pointTab, std::vector<int>cand,
		double radius, PixTools *pt, int nside){
	std::vector<Tile*> *res = new std::vector<Tile*>();
	 map<int,int>::iterator it;

	vector<int> *pixelList = new vector<int>();
	Tile *tile = NULL;
	int key,  ctInd,  selKey ;
	int nest = 0;
	int inclusive = 1; // yes to inclusive search
	double   avRa, avDec, sumTp, sumRa, sumDec;
	int i,nk, sum,max,  nsel, point;
	skyPoint sp,  currCent;


	nk = cand.size();


	/*
	 *  Loop on the table untill all tiles are selected
	 *
	 */

	 while ( cand.size() > 0) {
		 nk = cand.size();
		 selKey = 0;
//
		 /* create candidate tile */
		 tile = new Tile();


		 max = 0;
		 for (i=0; i<nk; i++){    /* loop on candidates */
			 sum = 0;
			 nsel = 0;


			 ctInd = cand[i];
			 Vector3D vect = pt->pix2vect_ring(nside,ctInd);
//			 printf(" processing tile %d \n",ctInd);

			 currCent = getRaDec(ctInd, pt, nside);           /* get Ra,Dec from index */
//			 printf(" current index %d  Current RA =%f\n",ctInd, currCent.RA);



			 pixelList = pt->query_disc(nside,vect, radius,nest, inclusive);
			 for (size_t i=0; i< pixelList->size(); i++) {

					 key = pixelList->operator [](i);

					 it=pointTab.find(key);


					 if (it != pointTab.end() && pointTab[key] > 0) {
//						 printf(" ra=%f dec=%f key=%d entries=%d\n",ra,dec,key,pointTab[key]);
						 sum += pointTab[key];

						 nsel++;

					 }
				 }


			 if (sum >= max) {
				 max = sum;
				 selKey = ctInd;

			 }

		 }                         /* end loop on cells  */
		 if (max > 1) {

			 sumRa = 0.;
			 sumDec = 0.;
			 sumTp =0.;
//
			 /* now search for neighbors of the candidate and add them to array */
			 /* here we need to find weighted average as a center of the tile   */
			 /* to make it more precise we need to use actual center of pixel not from index */
//

			 Vector3D vect = pt->pix2vect_ring(nside,ctInd);
			 pixelList = pt->query_disc(nside,vect, radius, nest, inclusive);
			 for (size_t j=0; j< pixelList->size(); j++) {
				    key = pixelList->operator [](j);
				    skyPoint radec = getRaDec(key, pt, nside);
 					 it = pointTab.find(key);

 					if (it != pointTab.end() ) {
 						point = pointTab[key];
 						sumRa += radec.RA*point;
 						sumDec += radec.Dec*point;
 						sumTp += point;
 						tile->neighbors.push_back(key);
// 						cout<<" added to tile neighbours pixel "<<key<<endl;
 					    removeMember(&cand,key);
 						nsel++;

 					}

			 }
			 avRa = sumRa/sumTp;
			 avDec = sumDec/sumTp;
			 sp.Dec =avDec;
			 sp.RA = avRa;
			 selKey = getIndex(sp, pt, nside);
//
			 /* find real Ra,Dec for the tangent point */
			 skyPoint sp;
			 sp =  getRaDec(selKey, pt,  nside);
			 tile->raC = sp.RA;
			 tile->decC = sp.Dec;
			 tile->tileID = selKey;
			 sprintf(tile->name,"DECAM_%d_%d",selKey,nside);
//			 cout<<" created tile "<<tile->name<<endl;
			 tile->nP = max;
			 res->push_back(tile);
		 } else {							/* just remove those unused entries */

			 Vector3D vect = pt->pix2vect_ring(nside,ctInd);
			 pixelList = pt->query_disc(nside,vect, radius,nest, inclusive);

			 for (size_t j=0; j< pixelList->size(); j++) {
				    key = pixelList->operator [](j);

 					it = pointTab.find(key);

 					if (it != pointTab.end() ){


 						removeMember(&cand,key);
 						nsel++;

 					}

			 }
			 delete(tile);

		 }

	 }  /* end of while loop */

	 delete pixelList;
	return res;
}




/* search for tile that contain given cell index */

Tile* getTile(std::vector<Tile*> * tiles, int index){
	Tile * tile=NULL;
	int ntiles = tiles->size();
	int found = 0;
	int i, k, ncells;

	for (i=0; i<ntiles; i++) {
		found = 0;
		tile = tiles->operator [](i);
		ncells = tile->neighbors.size();
		for (k=0; k<ncells; k++) {
			if (index == tile->neighbors[k]) {
				found = 1;
				break;
			}
		}
		if (found) break;
	}
	if (!found) tile = NULL;
	return tile;
}

/* search Ra,Dec coordinates of a tile containing given pixel */

 skyPoint getRaDecFromIndex(std::vector<Tile*> *tiles, int index) {
	 skyPoint res;
		res.RA = -1.0;
		res.Dec = -100.0;
		Tile * tile=NULL;
		int ntiles = tiles->size();
		int found = 0;
		int i, k, ncells;
		for (i=0; i<ntiles; i++) {
			found = 0;
			tile = (Tile*) tiles->operator [](i);
			ncells = tile->neighbors.size();
			for (k=0; k<ncells; k++) {
				if (index == tile->neighbors[k]) {
//
					res.RA = tile->raC;
					res.Dec = tile->decC;
//
					found = 1;
					break;
				}
			}
			if (found) break;
		}
//
		return res;
 }

 /* create string extension to the tile name */

string getExpExtName(std::vector<int>* expTot, int expId, int subset) {
	string res = "_00";

	int ntot = expTot->size();

	int nsec = 1;

	stringstream ss;
	int section = 0;
	int ncoll=0;
	for ( int i=0; i< ntot; i++) {
		if(expTot->operator [](ncoll) == expId) break;
		ncoll++;
	}
	if (ntot > subset) {
		nsec = ntot/subset + 1;
		int quant = ntot/nsec;
		section = ncoll/quant;

		if (ncoll >= (nsec -1)*quant) section = nsec -1;
		if (section <=9) {
			ss << "_0"<<section;
			res = ss.str();
			ss.clear();  // to reuse the object
			ss.str("");  // clean it
		} else {
			ss << "_"<<section;
			res = ss.str();
			ss.clear();  // to reuse the object
			ss.str("");  // clean it
		}
	}
	return res;
}
  int main(int argc, char *argv[]) {
	  	  	pt = new PixTools();
	  	  	double angRes = 1.0*3600.; // map resolution in arcsec (1.0 degree)
	  	    int nside = 64;
    	 	std::vector<int> cand;
    	 	vector<int>::iterator cit;
    	 	std::vector<Tile*> *tiles;
    	 	vector<int>::iterator tit;
    	 	vector<double> exptms;
    	 	vector<double> fwhms;
    	 	vector<double> zps;
    	 	vector<int> expids;
    	 	vector<skyPoint*> bss;
    	 	//
    	 	/* maps counting selected exposures */
    	 	std::map<int,std::vector<int>* > pixV;
    	 	map<int,std::vector<int>* >::iterator itPV; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retPV;
    	 	std::map<int,std::vector<int>* > pixS;
    	 	map<int,std::vector<int>* >::iterator itPS; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retPS;
    	 	std::map<int,std::vector<int>* > pixM;
    	 	map<int,std::vector<int>* >::iterator itPM; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retPM;
    	 	std::map<int,std::vector<int>* > pixL;
    	 	map<int,std::vector<int>* >::iterator itPL; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retPL;
    	 	//
    	 	std::map<int,std::vector<int>* > tileV;
    	 	map<int,std::vector<int>* >::iterator itTV; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retTV;
    	 	std::map<int,std::vector<int>* > tileS;
    	 	map<int,std::vector<int>* >::iterator itTS; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retTS;
    	 	std::map<int,std::vector<int>* > tileM;
    	 	map<int,std::vector<int>* >::iterator itTM; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retTM;
    	 	std::map<int,std::vector<int>* > tileL;
    	 	map<int,std::vector<int>* >::iterator itTL; //
    	 	std::pair<map<int,std::vector<int>* >::iterator,bool> retTL;
    	 	// map containing exposure number and exposure time
    	 	std::map<int,float > exTm; //
    	 	map<int,float >::iterator itET; //
    	 	std::pair<map<int,float >::iterator,bool> retET;
    	  	// create map with hp index key and number of pointings in it
    	    std::map<int,int> pointTab; //
    		map<int,int>::iterator itpT;
    		std::pair<map<int,int>::iterator,bool> retTp;
    	 	//

    	 int N, i, flag_verbose=2,flag_input=0, config_flag=1;
    	 double exp_cut = 0.0, seeing_cut = 0.5, zp_cut = 0.5;
    	 double exptime, fwhm, zp, sum_bright, sum_faint;
    	 double expmax = -1.0, zpmed = 0.0,fwhmmed = 0.0;
    	 float timeV = 10.0; float timeS = 60.0; float timeM = 300.0;
    	 float expMax = 50.;
    	 stringstream ss;

     	 double exp_min_cut,fwhm_max_cut,zp_min_cut;
    	 int flag_outpath=1;
    	 int npoint;
    	 int newKey;
    	 double DegToRad = M_PI/180.;
    	 double radius=2.0*DegToRad;
//

    	 double PixSize = 0.27;
    	 skyPoint  pointing;
    	 Tile* tile;

    	 FILE  *pip,*fin,  *conf;

    	 char command[1000],configfile[1000],inpline[100],event[2000],inlist[1000],
    	 outfile[1000],inputimage[1000], par_name[50],par_val[50];
    	 float RA,Dec, remapRA, remapDec;

    	  if (argc<2) {
    	    printf("Usage: %s <required inputs> \n",argv[0]);
    	    printf("    Required Inputs:\n");
    	    printf("       -input <list (format: <FileNameIncludingPath> <RA>  <Dec> <sky FWHM > <zero Point> \n");
    	    printf("       -output <output file including path>  \n");
    	    printf("       -config <config file name including path>  \n");
    	    printf("       -mapRes <map resolution in degrees> \n");
    	    printf("       -verbose <verbose level 0 - 3 \n");
    	    printf("\n");
    	        exit(0);
    	  }

    	  // Process command line
    	  for (i=2;i<argc;i++) {
    	    if (!strcmp(argv[i],"-verbose")) {
    	      sscanf(argv[++i],"%d",&flag_verbose);
    	      if (flag_verbose<0 || flag_verbose>3) {
    	        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
    	        flag_verbose=2;
    	      }
    	    }
    	  }
    	  // now go through again
    	  for(i=1;i<argc;i++) {
    	    if (!strcmp(argv[i],"-input")) {
    	      flag_input=1;
    	      sscanf(argv[i+1],"%s",inlist);
//    	      cout<<" inlist="<<inlist<<endl;
    	    }

      	  //

      	    if (!strcmp(argv[i],"-mapRes")) {
//      	      flag_map=1;
      	      angRes = 0.0;
      	      sscanf(argv[i+1],"%lf",&angRes);
      	      if (flag_verbose >=2) cout<<" map resolution="<<angRes<<endl;
      	      angRes *=3600.;   // resolution in arcsec
  	  	  	  nside = pt->GetNSide(angRes);
  	  	  	  if (flag_verbose >=2) cout<<" Healpix map resolution nside="<<nside<<endl;
      	    }


    	    if (!strcmp(argv[i],"-output")) {
    	      flag_outpath=1;
    	      sscanf(argv[i+1],"%s",outfile);
//    	      cout<<" outfile="<<outfile<<endl;
    	    }

    	    if (!strcmp(argv[i],"-config")) {
    	    	sscanf(argv[i+1],"%s",configfile);
//    	    	cout<<" configFile="<<configfile<<endl;
  	    }

  	  }
    	    // quit if required inputs are not set
    	    if(!flag_input || !flag_outpath ) {
    	      printf("Required inputs are not set \n");

    	      exit(0);
    	    }
    	    if (!config_flag) {
    	    	cout<<"TileFinder -- configuration file is not set using default values "<<endl;
    	    }
    	    /* read config file to get selection parameters   */
    	    conf=fopen(configfile,"r");
    	        	  if (conf==NULL) {
    	        		  printf("File %s not found  using default selection parameters\n",configfile);
    	        		  printf(" radius=%lf : exposure cut= %lf : seeing cut= %lf : zero point cut= %lf : timeV=%f : timeS=%f : timeM-%f \n",
    	        				  radius, exp_cut, seeing_cut, zp_cut, timeV, timeS, timeM);

    	        	    exit(0);
    	        	  }

    	     while (fgets ( inpline, sizeof inpline, conf ) != NULL) {

    	      if (!strncmp((char *)&inpline,"#",1)){
    	       	printf("Skip comment \n");
    	       	continue;
    	       }    // skip comments
    	      sscanf(inpline,"%s %s ",par_name, par_val);
    	      string par = trim(par_name);
    	      if (!par.compare("radius")) {
    	    	  radius = atof(par_val);
    	    	  radius *=DegToRad;
    	      }
    	      if (!par.compare("exp_cut")) {
    	    	  exp_cut = atof(par_val);
    	      }
    	      if (!par.compare("seeing_cut")) {
    	    	  seeing_cut = atof(par_val);
    	      }
    	      if (!par.compare("zp_cut")) {
    	    	  zp_cut = atof(par_val);
    	      }
    	      if (!par.compare("pix_scale")) {
    	    	  PixSize = atof(par_val);
    	      }
    	      if (!par.compare("timeV")) {
    	    	  timeV = atof(par_val);
    	      }
    	      if (!par.compare("timeS")) {
    	    	  timeS = atof(par_val);
    	      }
    	      if (!par.compare("timeM")) {
    	    	  timeM = atof(par_val);
    	      }
    	      if (!par.compare("expMax")) {
    	    	  expMax = atof(par_val);
    	      }
    	     }
    	  fclose(conf);
    	  if (flag_verbose >= 2) {
    		  cout<<"TileFinder using selection parameters: radius="<<radius<<" : exposure_cut="<<exp_cut;
    		  cout<<" : seeing_cut="<<seeing_cut<<" : Zero_Point_cut ="<<zp_cut<<" pixSize="<<PixSize<<endl;
    		  cout<<" exposure time Very short: "<<timeV<<" Short: "<<timeS<<" time Medium: "<<timeM<<endl;
    	  }


//

  	    // *** Find out the number of lines from input file ***
  	    sprintf(command,"wc -l %s",inlist);
  	    pip=popen(command,"r");
  	    if (pip==NULL) {
  	    	printf("Pipe failed: %s \n",command);
  	      exit(0);
  	    }
  	    fscanf(pip,"%d",&N);
  	    pclose(pip);
  	    printf("Number of images in %s = %d \n",inlist,N);

  	  // *** Input data  ***
  	  fin=fopen(inlist,"r");
  	  if (fin==NULL) {
    	    printf("File %s not found \n",inlist);
  	    exit(0);
  	  }
  	  // loop on input files

 	  for(i=0;i<N;i++) {
 	    fscanf(fin,"%s %f %f %lf %lf %lf",inputimage,&RA,&Dec,&exptime,&fwhm,&zp);
  	   exptms.push_back(exptime);  // exposure time vector
  	   fwhms.push_back(fwhm);      // seeing FWHM vector
  	   zps.push_back(zp);          // sero point vector

  	   skyPoint *spi = new skyPoint();
  	   spi->RA = RA;
  	   spi->Dec = Dec;
  	   bss.push_back(spi);			// sky point vector

 	  }
 	 fclose(fin);
 	  cout<<"End of input read "<<endl;
 //
 	  /*
 	   * here we will calculate parameter limits to select useful data
 	   */

 	  for (i=0; i< N; i++) {
 		  zpmed += zps[i];
 		  fwhmmed += fwhms[i];
 		  if(expmax <= exptms[i]) {
 			  expmax = exptms[i];
 		  }
 	  }

 	  fwhmmed /= (double)N;
 	  zpmed /= (double)N;

 	  if (flag_verbose >= 2) cout<<"exp time max ="<<expmax<<" fwhmmed="<<fwhmmed<<" zpmed="<<zpmed<<" vectlength="<<zps.size()<<endl;
 	  exp_min_cut = exp_cut*expmax;
 	  fwhm_max_cut = 2.0*fwhmmed  - seeing_cut*fwhmmed;
 	  zp_min_cut = zp_cut*zpmed;

 	  if (flag_verbose >= 2) cout<<"exp_cut="<<exp_min_cut<<" seeing cut="<<fwhm_max_cut<<" zp cut="<<zp_min_cut<<endl;

 	  /*
 	   * calculate sum of faint and bright images
 	   */
 	  sum_faint = 0.0;
 	  sum_bright = 0.0;
 	  for (i=0; i<N; i++) {
 		 if (exptms[i] <= exp_min_cut ) {
 			 sum_faint += exptms[i];
 		 } else {
 			 sum_bright += exptms[i];
 		 }
 	  }
 	  if (flag_verbose >= 2) cout<<"TileFinder sum of faint exposures ="<<sum_faint<<" vs sum of bright ="<<sum_bright<<endl;


//
	 	// reopen input file
	 	fin=fopen(inlist,"r");
	 	if (fin==NULL) {
	 		printf("File %s not found \n",inlist);
	 		exit(0);
	 	 }
	 	int selExp = 0;
	 	int expId=0;
     	int first = 1;
     	for(i=0;i<N;i++) {  // loop on images
     		fscanf(fin,"%s %f %f %lf %lf %lf",inputimage,&RA,&Dec,&exptime,&fwhm,&zp);

     		newKey = getIndex(*bss[i], pt, nside);  // get healpix pixel
//
     		if(( exp_min_cut<0.0 || exptms[i]>= exp_min_cut) &&
     			  ( fwhm_max_cut <0.0 || fwhms[i]<= fwhm_max_cut) &&
     			  ( zp_min_cut <0.0 || zps[i] >= zp_min_cut )) {

     		    if (first == 1){
     			  npoint = 1;
     			  pointTab.insert(pair<int,int>(newKey,npoint));
     			  expId = getExpId(inputimage);
     			  if (isUniqueId(&expids,expId)) exTm[expId] = exptime;
     			  addUniqueId(&expids,expId);
     			  first = 0;
     			  continue;
     		  }




     		  if (pointTab.count(newKey) == 0) {
     			  npoint = 1;
     			  pointTab.insert(pair<int,int>(newKey,npoint));
     			  expId = getExpId(inputimage);
     			  if (isUniqueId(&expids,expId)) exTm[expId] = exptime;
     			  addUniqueId(&expids,expId);
//
     		  } else {
     			  npoint = pointTab[newKey];
     			  npoint++;
     			  itpT=pointTab.find(newKey);
     			  pointTab.erase(itpT);
     			  pointTab.insert(pair<int,int>(newKey,npoint));
     			  expId = getExpId(inputimage);
     			  if (isUniqueId(&expids,expId)) exTm[expId] = exptime;
     			  addUniqueId(&expids,expId);
//    	    	printf("Increment tab value %d for key= %d \n",npoint,newKey);


     		  }
     		 selExp++;
        	 }
     	  }
     	fclose(fin);
     	if (flag_verbose >= 2) cout<<"Number of selected files="<<selExp<<endl;
//
     	cout<<" Number of exposures ="<<exTm.size()<<endl;


    	  for ( itpT=pointTab.begin() ; itpT != pointTab.end(); itpT++ ) {
     		cand.push_back(itpT->first);
     	 }
     	 sort (cand.begin(), cand.end());
     	  // Now we have a table with pixel numbers and number of pointings in each
     	  //  lets find clusters of given raduis starting from most significant one
     	  //
     	  tiles = findTile(pointTab,cand, radius, pt, nside);
     	  //
     	  //  Now reopen the input file again, read entries, find tile number it
     	  // belongs to, find exposure id and exposure time, count number of S,M,L
     	  //  exposures in the tile
     	  //
     	  //

     	 fin=fopen(inlist,"r");
    	 if (fin==NULL) {
    		 printf("File %s open failed",inlist);
    	    exit(-1);
    	 }
	for (i = 0; i < N; i++) {
		fscanf(fin, "%s %f %f %lf %lf %lf", inputimage, &RA, &Dec, &exptime,
				&fwhm, &zp);
		if ((exp_min_cut < 0.0 || exptms[i] >= exp_min_cut)
				&& (fwhm_max_cut < 0.0 || fwhms[i] <= fwhm_max_cut)
				&& (zp_min_cut < 0.0 || zps[i] >= zp_min_cut)) {
			expId = getExpId(inputimage);
			if ((itET = exTm.find(expId)) != exTm.end()) {
				exptime = exTm[expId];
			} else {
				exptime = -1;
			}
			pointing.RA = RA;
			pointing.Dec = Dec;
			int index = getIndex(pointing, pt, nside);

			tile = getTile(tiles, index);

			if (tile != NULL) {
				remapRA = tile->raC;
				remapDec = tile->decC;
				pointing.RA = remapRA;
				pointing.Dec = remapDec;
				index = getIndex(pointing, pt, nside);  // index of the tile
				if ((exptime >= 0.) && (exptime < timeV)) {
					if ((itTV = tileV.find(index)) != tileV.end()) {
						std::vector<int>* tmp = tileV[index];
						addUniqueId(tmp, expId);
					} else {
						vector<int>* tmp = new vector<int>();
						tmp->push_back(expId);
						tileV[index] = tmp;
					}

				} else if ((exptime >= timeV) && (exptime < timeS)) {
					if ((itTS = tileS.find(index)) != tileS.end()) {
						std::vector<int>* tmp = tileS[index];
						addUniqueId(tmp, expId);
					} else {
						vector<int>* tmp = new vector<int>();
						tmp->push_back(expId);
						tileS[index] = tmp;
					}

				} else if ((exptime >= timeS) && (exptime < timeM)) {
					if ((itTM = tileM.find(index)) != tileM.end()) {
						std::vector<int>* tmp = tileM[index];
						addUniqueId(tmp, expId);
					} else {
						vector<int>* tmp = new vector<int>();
						tmp->push_back(expId);
						tileM[index] = tmp;
					}

				} else if (exptime >= timeM) {
					if ((itTL = tileL.find(index)) != tileL.end()) {
						std::vector<int>* tmp = tileL[index];
						addUniqueId(tmp, expId);
					} else {
						vector<int>* tmp = new vector<int>();
						tmp->push_back(expId);
						tileL[index] = tmp;
					}

				}

			} // if tile
		}
	}
      	fclose(fin);

     	  //
     	  //  Now reopen the input file read entries find tile number it belongs to
     	  //  and write output file with RA,Dec of the re-mapping pointing
     	  //  that will be input for cp_remap.c
     	  //
     	  //

     	 fin=fopen(inlist,"r");
    	 if (fin==NULL) {
    		 printf("File %s open failed",inlist);
    	    exit(-1);
    	 }

//
     	 ofstream outFile;
     	 outFile.open(outfile);

     	 if (outFile == NULL) {
     		 printf("File %s open failed",outfile);
     	    exit(-1);
     	 }

	for (i = 0; i < N; i++) {
		fscanf(fin, "%s %f %f %lf %lf %lf", inputimage, &RA, &Dec, &exptime,
				&fwhm, &zp);
		if ((exp_min_cut < 0.0 || exptms[i] >= exp_min_cut)
				&& (fwhm_max_cut < 0.0 || fwhms[i] <= fwhm_max_cut)
				&& (zp_min_cut < 0.0 || zps[i] >= zp_min_cut)) {

		pointing.RA = RA;
		pointing.Dec = Dec;
		int index = getIndex(pointing, pt, nside);
		int centPix = 0;

		tile = getTile(tiles, index);

		if (tile != NULL) {
			remapRA = tile->raC;
			remapDec = tile->decC;
			pointing.RA = remapRA;
			pointing.Dec = remapDec;
			centPix = getIndex(pointing, pt, nside);
			expId = getExpId(inputimage);

			vector<int>* expTot;

			string extExp="";
			string extTim = "";

			if ((exptime >= 0.) && (exptime < timeV)) {
				extTim = "_V";
				expTot = tileV[centPix];
				if (expTot != NULL) {
					extExp = getExpExtName(expTot,expId,expMax);
				}
			} else if ((exptime >= timeV) && (exptime < timeS)) {
				extTim = "_S";
				expTot = tileS[centPix];
				if (expTot != NULL) {

					extExp = getExpExtName(expTot,expId,expMax);

				}
			} else if ((exptime >= timeS) && (exptime < timeM)) {
				extTim = "_M";
				expTot = tileM[centPix];
				if (expTot != NULL) {
					extExp = getExpExtName(expTot,expId,expMax);
				} else {
					cout<<"tile M not found"<<endl;
				}
			} else if (exptime >= timeM) {
				extTim = "_L";
				expTot = tileL[centPix];
				if (expTot != NULL) {
					extExp = getExpExtName(expTot,expId,expMax);
				} else {
					cout<<"tile not found"<<endl;
				}
			}

			if (expTot->size() > 0) {
				ss << tile->name<< extTim<<extExp;
				string tileName = ss.str();
				ss.clear();  // to reuse the object
				ss.str("");  // clean it
//
				outFile << inputimage << " " << tileName << " " << PixSize << " "
					<< remapRA << " " << remapDec << endl;
			}

		} else {
			printf("file %s has no associated tile, skipping \n", inputimage);
		}
		}

	}
	fclose(fin);

     outFile.close();
     size_t l = 0;
    	  // free memory
    for(l=0; l< tiles->size(); l++) {
     	delete(tiles->operator [](l));
     }

    delete(tiles);
    for(l=0; l< bss.size(); l++) {
     	delete(bss.operator [](l));
     }

    for ( itTV=tileV.begin() ; itTV != tileV.end(); itTV++ ) {
    	   delete((*itTV).second);
    }

    for ( itTS=tileS.begin() ; itTS != tileS.end(); itTS++ ) {
    	   delete((*itTS).second);
    }

    for ( itTM=tileM.begin() ; itTM != tileM.end(); itTM++ ) {
    	   delete((*itTM).second);
    }

    for ( itTL=tileL.begin() ; itTL != tileL.end(); itTL++ ) {
    	   delete((*itTL).second);
    }

    for ( itPV=pixV.begin() ; itPV != pixV.end(); itPV++ ) {
    	   delete((*itPV).second);
    }

    for ( itPS=pixS.begin() ; itPS != pixS.end(); itPS++ ) {
    	   delete((*itPS).second);
    }

    for ( itPM=pixM.begin() ; itPM != pixM.end(); itPM++ ) {
    	   delete((*itPM).second);
    }

    for ( itPL=pixL.begin() ; itPL != pixL.end(); itPL++ ) {
    	   delete((*itPL).second);
    }
    delete pt;
    	  return 0;
}

