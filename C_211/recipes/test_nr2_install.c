/* Tiny program to test installation of Numerical Recipes Library */
/* Program should compile and link successfully                   */
/* Output is two columns of numbers from 1 - 25                   */

#include <stdio.h>
int main() {
    void shell();  /* command from NR2 */

    const int NUM = 25;
    float a[NUM+1];
    int i;

    for (i = 1; i <= NUM; i++) {
        a[i] = NUM+1 - i;
    }

    shell(NUM, a);

    for (i = 1; i <= NUM; i++) {
        printf("%02d %02.0f\n", i, a[i]);
    }

    return 0;
}
