#!/usr/bin/env perl
########################################################################
#    $Id$
#
#    $Rev::                                                              $:    # Revision of last commit.
#    $LastChangedBy::                                        $:    # Author of last commit. 
#    $LastChangedDate::                      $:    # Date of last commit.
#
#    Author: 
#                 Darren Adams (dadams@ncsa.uiuc.edu)
#
#    Developed at: 
#    The National Center for Supercomputing Applications (NCSA)
#
#    Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#    All rights reserved.
#
#    DESCRIPTION:
#
#         outputpath is considered to be equivalent to the run directory
#         The directory structure underneath will match archive structure
#            
#
########################################################################

use warnings;
use strict;

use Getopt::Long;
use File::Basename;
use Data::Dumper;
use Storable qw(dclone);
use XML::Simple;
use FindBin;
use Carp;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::filelistfunc;
use ImageProc::DB;
use ImageProc::SWARP qw(:all);
use ImageProc::Util qw(removeFilesFromOldRuns);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my $image_table = 'image';
my $location_table = 'location';
my $exposure_table = 'exposure';

# List of location table keys that are availiable on the command line
# in the generic way they in in the DAF
my %location_cols = (
    id => 1,
    fileclass => 1,
    filename => 1,
    filedate => 1,
    filetype => 1,
    run    => 1,
    nite => 1,
    band => 1,
    tilename    => 1,
    exposurename => 1,
    ccd => 1,
);
my @DB_opts = map {lc($_)."=s@"} keys %location_cols;

my ($stat);
my (@ImageList);

my %opts;

# default coadd_run to all runs
$opts{'coadd_run'} = '%';

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
$stat = Getopt::Long::GetOptions(\%opts,
            'detector=s',
            'project=s',
            'difftype=s',
            'parent_run=s',
            'coadd_run=s',
            'coadd_project|coadd-project=s',
            'outputxml=s',
            'coadd_outputxml|coadd-outputxml=s',
            'image_outputxml|image-outputxml=s',
            'help|h',
            'man' =>,
            'version|v',
            'verbose|V=i',
            'debug',
            'query_run=s',
            'curr_run=s',
            'block_num=i',
             @DB_opts
);

# Set defaults:
$opts{'verbose'} = 1 if (!defined $opts{'verbose'});
$opts{'debug'} = 0 if (!defined $opts{'debug'});

my $verbose = $opts{'verbose'};
my $debug = $opts{'debug'};

# Check inputs
if (!defined($opts{'detector'})) {
    print STDERR "Must be define detector (either DECam or Mosaic2)\n";
    exit 1;
}

if (($opts{'detector'} ne 'DECam') && ($opts{'detector'} ne 'Mosaic2')) {
    print STDERR "Invalid detector '$opts{'detector'}'\n";
    print STDERR "Must be either DECam or Mosaic2\n";
    exit 1;
}

if (!defined($opts{'coadd_project'})) {
    print STDERR "Must specify coadd project.\n";
    exit 1;
}

if (!defined($opts{'project'})) {
    print STDERR "Must specify project.\n";
    exit 1;
}

if (!defined($opts{'nite'})) {
    print STDERR "Must specify nite.\n";
    exit 1;
}

if (!defined($opts{'difftype'})) {
    print STDERR "Must specify difftype.\n";
    exit 1;
}
if (($opts{'difftype'} ne 'red') && ($opts{'difftype'} ne 'nitecmb')) {
    print STDERR "Invalid difftype. Must be either 'red' or 'nitecmb'.\n";
    exit 1;
}

if (!defined($opts{'coadd_outputxml'})) {
    print STDERR "Must specify outputxml file.\n";
    exit 1;
}

# Establish DB connection and get tile and offset info:
my $db = ImageProc::DB->new('verbose'=>$verbose,'debug'=>$debug);
    
print "getTileSizeInfo...\n";
my $TileInfo= $db->getTileSizeInfo({
    'project' => $opts{'coadd_project'}
});

print "getCCDOffsets...\n";
my $Offsets = $db->getCCDOffsets({
    'detector' => $opts{'detector'}
});

# Database query for reduced images.    This is done using the
# SQL builder (queryDB2) so that command line arguments can be
# translated into useful SQL (%-> like, ! ->not, etc. )
my %query = (
    $location_table => {
        'join' => {'id' => 'image.id'},
        'key_vals' => {
            'project' => $opts{'project'},
        }
    },
    $image_table => {
        'select_fields' => ['exposureid'],
        'join' => {'exposureid' => 'exposure.id'}
    },
    $exposure_table => {
        'select_fields' => ['telra','teldec']
    },
);

setQueryRun($opts{'query_run'}, $opts{'block_num'}, $opts{'curr_run'}, $query{$location_table}->{'key_vals'});
if ($opts{'difftype'} eq 'red') {
    $query{$location_table}->{'key_vals'}->{'filetype'} = 'red';
}
else {
    $query{$location_table}->{'key_vals'}->{'filetype'} = 'diff_nitecmb';
}

# Hammer cmd-line args into query call:
foreach my $key (keys %location_cols) {
    if (exists $opts{$key}) {
        $query{$location_table}->{'key_vals'}->{$key} = $opts{$key};
    }
}

print "\nGetting search image list...";
my $Images = getFileList(\%query);
print "Done.\n";
print "Number of search images: ", scalar keys %$Images,"\n";

# Copy of query to preserve for write-out to xml.    This will help
# keep a lid on the size of the xml file by not writing all the
# additional keys that get inserted during tile match calculations.
# Need hash key to be id for later

print "\nCopying image list...";
my $copy_images;
while ((my $key, my $value) = each %$Images) {
  $copy_images->{$value->{'id'}} = {%$value};
}

$Images = dclone($copy_images);
print "Done.\n";

# Sequence of function calls on the image list that add offset and tile data
# then use that to query for the matching set of tiles:
print "\nUpdating Image Coords for Offset for each image...";
updateImageCoordsForOffset ({
    'images' => $Images,
    'offsets' => $Offsets
});
print "Done.\n";

print "\nGetting Tile Search Range for each image...";
getTileSearchRange ({
    'images' => $Images,
    'pixelsize' => $TileInfo->{'pixelsize'},
    'npix_ra' => $TileInfo->{'npix_ra'},
    'npix_dec' => $TileInfo->{'npix_dec'}
});
print "Done.\n";

print "\nGetting Tile Matches for each image...";
$db->getTileMatchInfo ({
    'images' => $Images,
    'coadd_project' => $opts{'coadd_project'},
    'quiet' => 1
});
print "Done.\n";
$db->disconnect;    # All done with DB.


print "After other funcs, # search images = ", scalar keys %$Images, "\n";

# Build data structure to organize image list by exposure.    This 
# probably isn't strictly needed now, but it remains as an artifact
# of the original program design that only needed to form a list for
# each exposure:
my %exposures;
foreach my $image (values %$Images) {
    # define band for the exposure and pull in the images
    my $exposure = $image->{'exposurename'};
    $exposures{$exposure}->{'band'} = $image->{'band'} if (! defined $exposures{$exposure}->{'band'});
    push(@{$exposures{$exposure}->{'ImageList'}},$image);
    my $tiledata = $image->{'tiles_info'};
    foreach my $tilehash (@{$image->{'tiles_info'}}) {
        $exposures{$exposure}->{'TileList'}->{$tilehash->{'tilename'}} = 1;
    }
}

print "\nTotal number of exposures in imagelist: ", scalar keys %exposures,"\n\n";

# Build up a coadd_list for each exposure.    This function also adds
# "exposurename" to the set of file keys so that orch. can divide-by
# exposurename for the coadd lists.    so, "exposurename" in that context
# is a sort-of dummy key to divide by.    Other arbitrary dummy keys could be used
# as well for a particular file list.
my $BandTileCoadds;
if (defined($opts{'parent_run'})) {
   $BandTileCoadds = getCoaddListsByParents($opts{'project'}, $opts{'coadd_run'}, 
                                    $opts{'parent_run'}, $opts{'curr_run'}, \%exposures);
}
else {
   $BandTileCoadds = getCoaddLists($opts{'project'}, $opts{'coadd_run'}, \%exposures);
}

# Build up list of all coadds to hand to orchestration.    Also dump some
# output here like all the found tiles for a exposure, the list of coadds
# per exposure.
my %coadds;

# Cycle through the lists marking "bad" exposures and "covered" images:
my %bad_exposures;
my %CoveredImages;
foreach my $exposure (keys %exposures) {

    # Dump tile list for each exposure
    if ($verbose > 2) { 
        print "\nTile list for exposure: $exposure\n";
        foreach my $tile (keys %{$exposures{$exposure}->{'TileList'}}) {
            print "$tile\n";
        }
        print "\n";
    }

    # If the coadd list is undefined or empty, mark as a "bad" exposure
    if (defined $exposures{$exposure}->{'CoaddList'}) {
        if (scalar keys %{$exposures{$exposure}->{'CoaddList'}} <= 0) {
            #warn "No coadds found for $exposure\n";
            $bad_exposures{$exposure} = 1;
            next;
        }
        while ((my $key, my $val) = each %{$exposures{$exposure}->{'CoaddList'}}) {
            $coadds{$key} = $val;
        }
    }
    else {
        #warn "No coadds found for $exposure\n";
        $bad_exposures{$exposure} = 1;
        next;
    }

    # For all the images in this exposure, mark as "covered" if there
    # is a coadd that was found this band that covers a tile in the list
    # of overlapping tiles:
    foreach my $image (@{$exposures{$exposure}->{'ImageList'}}) {
        foreach my $tile (@{$image->{'tiles_info'}}) {
            my $key = $image->{'band'}.'_|_'.$tile->{'tilename'};
            my $coadd_id = $BandTileCoadds->{$key}->{'id'};
            push (@{$CoveredImages{$image->{'id'}}},$coadd_id) if ($coadd_id);
        }
    }    

    #print "\n";
}

# If we have bad exposures, report about them and remove them and their
# associated lists from the master exposure data structure:
if (%bad_exposures) {
    #warn "\nERROR: Unable to find coadds for the following exposures:\n";
    foreach my $exposure (keys %bad_exposures) {
        #warn "$exposure\n";
        delete $exposures{$exposure};
    }
    print "\nTotal: ",scalar keys %bad_exposures," \"bad\" exposures.\n";
    print "Remaining \"good\" exposures: ",scalar keys %exposures,"\n";
}

print "\n";

# Now build up simple hash lists of all images and coadds.    The image list
# is unique by the image id and the coadds are duplicated for each exposure (band)
# and ccd (image) they were found to overlap.
my %GoodImages;
my %GoodCoadds;
while ((my $exposure, my $data) = each %exposures) {
    my $nimages = scalar @{$data->{'ImageList'}};
    my $ncoadds = scalar keys %{$data->{'CoaddList'}};
    croak ("Zero length coadd list") if ($ncoadds <= 0);
#    print "$exposure    # images: ",scalar @{$data->{'ImageList'}},"    # coadds: ",scalar keys %{$data->{'CoaddList'}},"\n";
    foreach my $image (sort {$a->{'id'} <=> $b->{'id'}} @{$data->{'ImageList'}}) {
        if (defined($CoveredImages{$image->{'id'}}) && defined($copy_images->{$image->{'id'}})) {
            $GoodImages{$image->{'id'}} = $copy_images->{$image->{'id'}};
        }
    }
}

# Here, the "Good" coadd list consists of duplicates to associate the right
# set of coadds for each image.
while ((my $ImageID, my $CoaddIDs) = each %CoveredImages) {
    foreach my $CoaddID (@$CoaddIDs) {
        my $unique_key = $ImageID.$CoaddID;
        $GoodCoadds{$unique_key} = {%{$coadds{$CoaddID}}};
        $GoodCoadds{$unique_key}->{'ccd'} = $Images->{$ImageID}->{'ccd'};
        $GoodCoadds{$unique_key}->{'exposurename'} = $Images->{$ImageID}->{'exposurename'};
        #$GoodCoadds{$unique_key}->{'testkey'} = $Images->{$ImageID}->{'ccd'}.'__'.$Images->{$ImageID}->{'exposurename'};
    }
}

print "\n# of images with overlapping coadds: ",scalar keys %GoodImages,"\n";
print "# of coadds to map to images: ",scalar keys %GoodCoadds,"\n";

print "\n";

# Convert internal data structure and write:
print "Writing XML to: ",$opts{'image_outputxml'},"...";
my $lines = convertSingleFilesToLines(\%GoodImages);
outputXMLList($opts{'image_outputxml'}, $lines);
print "Done.\n\n";

print "Writing XML to: ",$opts{'coadd_outputxml'},"...";
$lines = convertSingleFilesToLines(\%GoodCoadds);
outputXMLList($opts{'coadd_outputxml'}, $lines);
print "Done.\n\n";

exit 0;



#######################################################################
# 
#######################################################################
sub getCoaddLists {
    my $project = shift;
    my $coadd_run = shift;
    my $exposures = shift;

    my %BandTileCoadds;

#SELECT location.* from location,coadd 
#WHERE coadd.id=location.id 
#AND location.project='$project'
#AND location.band=?
#AND location.tilename=?
#AND location.run IN (SELECT max(run) from coadd WHERE tilename=?)
#AND regexp_like(location.archivesites, '[^N]')
#STR

    # values to use in 'where' portion of DB query
    my %query = ();
    $query{'location'}->{'key_vals'} = { project => $project, fileclass => 'coadd', filetype => 'coadd' };
    $query{'coadd'}->{'key_vals'} = {project => $project};
    $query{'coadd'}->{'join'} = {'id' => 'location.id'};
    if ($coadd_run ne '%') {
	my $coaddvalue = $coadd_run ;
    if ($coadd_run =~ /,/) {
        my @arr = split /\s*,\s*/, $coaddvalue;
        $coaddvalue = \@arr;
    }
        $query{'location'}->{'key_vals'}->{'run'} = $coaddvalue;
        $query{'coadd'}->{'key_vals'}->{'run'} = $coaddvalue;
    }
    my $files = getFileList(\%query);

    if (scalar(keys %$files) == 0) {
        print "Found 0 coadds for $project $coadd_run\n";
        exit 1;
    }
    else {
        print "Found ", scalar keys %$files," coadds for $project $coadd_run\n";
    }

    my $BandTileCoadds = getBandTileCoadds($files, $exposures);
    return $BandTileCoadds;
}


#######################################################################
# 
#######################################################################
sub getCoaddListsByParents {
    my $project = shift;
    my $coadd_run = shift;
    my $parent_run = shift;
    my $curr_run = shift;
    my $exposures = shift;

    my $files;

    my $dbq = "select L2.* from location L1, coadd C, coadd_src S, location L2 where C.id=S.coadd_imageid and S.src_imageid=L1.id and L2.run=C.run and L1.project='$project' and L1.run='$parent_run' and L2.filetype='coadd' and REGEXP_LIKE(L2.archivesites,'[^N]')";
    print "dbq = $dbq\n";

    my $desdb = new DB::DESUtil(verbose=>0);
    @$files = @{$desdb->selectall_arrayref($dbq,{Slice => {}})};
    $desdb->disconnect();

    print "Number of coadd files in list = ", scalar(@$files), "\n";

    $files = makeFilesUniq($files, $curr_run, $debug);

    if (scalar(keys %$files) == 0) {
        print "Found 0 coadds for $project $coadd_run\n";
        exit 1;
    }
    else {
        print "Found ", scalar keys %$files," coadds for $project $coadd_run\n";
    }

    my $BandTileCoadds = getBandTileCoadds($files, $exposures);
    return $BandTileCoadds;
}


sub getBandTileCoadds { 
    my ($files, $exposures) = @_;

    # Make fast lookup
    my %lookup = ();
    while (my ($key, $file) = each %$files) {
#        print "band = ", $file->{'band'}, " tilename = ", $file->{'tilename'}, " id = ", $file->{'id'}, "\n";
        $lookup{$file->{'band'}}{$file->{'tilename'}}{$file->{'id'}} = $file;
    }
    print "\n\n";

    my %BandTileCoadds;
    foreach my $exposure (keys %$exposures) {
        my %Coadds;
        foreach my $tile (keys %{$exposures->{$exposure}->{'TileList'}}) {
            my $band = $exposures->{$exposure}->{'band'};
            print "Searching for coadds for band: $band, tile: $tile ... ";
            if (! defined($lookup{$band}{$tile})) {
                warn "Unable to find coadds\n";
                next;
            }
            my $Rows = $lookup{$band}{$tile};
            my $nrows = scalar keys %$Rows;
            print "Found ",$nrows,"\n";
            next if ($nrows <= 0);
            croak ("Found $nrows coadds for the same band tile and max run.\n") if ($nrows > 1);

            # Add exposurename to hash for divide work:
            foreach my $val (values %$Rows) {
                $val->{'exposurename'} = $exposure;
                my $key = $band.'_|_'.$tile;
                $BandTileCoadds{$key} = $val;
            }
            if (%Coadds) {
                %Coadds = (%Coadds,%$Rows);
            }
            else {
                %Coadds = %$Rows;
            }
        }
        if (%Coadds) {
            $exposures->{$exposure}->{'CoaddList'} = \%Coadds;
        }
    }

    return \%BandTileCoadds;
}


