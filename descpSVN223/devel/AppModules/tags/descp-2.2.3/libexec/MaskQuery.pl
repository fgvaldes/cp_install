#!/usr/bin/env perl
########################################################################
#  $Id: MaskQuery.pl 3381 2009-04-02 20:28:07Z mgower $
#
#  $Rev:: 3381                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-04-02 13:28:07 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;
use Orchestration::filelistfunc;

my ($outputxml, $nite, $tilename, $project, $curr_run, $query_run, $block_num, $filetype) = undef;


# filetype defaults to red if nite defined, remap for tilename
# use filetype=remap with nite
use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'nite=s' => \$nite,
    'tilename=s' => \$tilename,
    'project=s' => \$project,
    'curr_run=s' => \$curr_run,
    'query_run=s' => \$query_run,
    'block_num=i' => \$block_num,
    'filetype=s' => \$filetype,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($nite) && !defined($tilename)) {
    print "Error: Must specify either nite or tilename\n";
    exit $FAILURE;
}

if (!defined($project)) {
    print "Error: Must specify project\n";
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    exit $FAILURE;
}

# values to use in 'where' portion of DB query
my %key_vals = ( project => $project, fileclass => 'red' );

setQueryRun($query_run, $block_num, $curr_run, \%key_vals);

if (defined($nite)) {
    $key_vals{'filetype'} = 'red';
    if (defined($filetype)) {
        $key_vals{'filetype'} = $filetype;
    }
    $key_vals{'nite'} = $nite;
}
elsif (defined($tilename)) {
    $key_vals{'filetype'} = 'remap';
    $key_vals{'tilename'} = $tilename;
}

my $desDBI = DB::DESUtil->new('verbose'=>0);
my $location_table = $desDBI->LOCATION_TABLE()->{'table_name'};
$desDBI->disconnect();
my %query = ();
$query{$location_table}->{'key_vals'} = \%key_vals;
my $files = getFileList(\%query);

if (scalar(keys %$files) == 0) {
    print STDERR "MaskQuery returned zero results\n";
    print STDERR "Aborting\n";
    print STDERR "key_vals = \n";
    print STDERR Dumper(\%key_vals), "\n";
    exit $FAILURE;
}
my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);
