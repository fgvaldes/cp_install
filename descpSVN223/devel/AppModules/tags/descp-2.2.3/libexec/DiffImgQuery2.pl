#!/usr/bin/env perl
########################################################################
#  $Id: DiffImgQuery2.pl 3242 2009-03-04 20:01:25Z dadams $
#
#  $Rev:: 3242                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-03-04 13:01:25 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#     outputpath is considered to be equivalent to the run directory
#     The directory structure underneath will match archive structure
#      
#
########################################################################

use warnings;
use strict;

use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

# turn buffering off on STDOUT by making the filehandle hot
$|=1;


my %opts;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
my $stat = Getopt::Long::GetOptions(\%opts,
      'outputxml=s',
      'inputxml=s',
);


# Check inputs
if (!defined($opts{'inputxml'})) {
    print STDERR "Must specify inputxml.\n";
    exit 1;
}

if (!defined($opts{'outputxml'})) {
    print STDERR "Must specify outputxml file.\n";
    exit 1;
}
if (! -r $opts{'inputxml'}) {
    print STDERR "Cannot read ",$opts{'inputxml'},"\n";
    exit 1;
}

system("mv ".$opts{'inputxml'}.' '.$opts{'outputxml'});

exit 0;
