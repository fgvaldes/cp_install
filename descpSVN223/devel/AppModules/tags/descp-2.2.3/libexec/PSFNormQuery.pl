#!/usr/bin/env perl
########################################################################
#  $Id: PSFNormQuery.pl 5806 2010-08-13 00:43:53Z desai $
#
#  $Rev:: 5806                             $:  # Revision of last commit.
#  $LastChangedBy:: desai                  $:  # Author of last commit.
#  $LastChangedDate:: 2010-08-12 17:43:53 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;
use Orchestration::filelistfunc;

my ($outputxml, $nite, $tilename, $project, $filetype, $curr_run, $query_run, $block_num) = undef;

use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'project=s' => \$project,
    'tilename=s' => \$tilename,
    'nite=s' => \$nite,
    'outputxml=s'  =>  \$outputxml,
    'filetype=s' => \$filetype,
    'curr_run=s' => \$curr_run,
    'query_run=s' => \$query_run,
    'block_num=i' => \$block_num,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($project)) {
    print "Error: Must specify project\n";
    exit $FAILURE;
}

if (!defined($tilename)) {
    print "Error: Must specify tilename\n";
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    exit $FAILURE;
}

# values to use in 'where' portion of DB query
my %key_vals = ( project => $project );
if (defined($nite))
{
    $key_vals{'nite'}=$nite;
}
my %image_key_vals = (scampflg => 0, exptime => '>30');
my $value = $curr_run;
# make an array ref if list of values
if ($value =~ /,/) {
    my @arr = split /\s*,\s*/, $value;
    $value = \@arr;
}
$curr_run = $value;
setQueryRun($query_run, $block_num, $curr_run, \%key_vals);


my $desDBI = DB::DESUtil->new('verbose'=>0);
my $location_table = $desDBI->LOCATION_TABLE()->{'table_name'};
my $image_table = $desDBI->IMAGE_META_TABLE()->{'table_name'};
$desDBI->disconnect();

my $files = {};
my %query = ();
$query{$location_table}->{'key_vals'} = \%key_vals;
$query{$image_table}->{'select_fields'} = ['fwhm'];
$query{$image_table}->{'join'} = {'id' => "$location_table.id"};
$query{$image_table}->{'key_vals'}  = \%image_key_vals ;

# always get remaps if for nothing else than to compare number to mask files
$key_vals{'fileclass'} = 'red';
$key_vals{'filetype'} = 'remap';
$key_vals{'tilename'} = $tilename;
my $remapfiles = getFileList(\%query);
my $numremaps = scalar(keys %$remapfiles);

if (!defined($filetype) || lc($filetype) eq 'mask') { 
    $key_vals{'fileclass'} = 'coadd';
    $key_vals{'filetype'} = 'mask';
    $key_vals{'tilename'} = $tilename;
    my $maskfiles = getFileList(\%query);
    my $nummasks = scalar(keys %$maskfiles);

    if ($nummasks != $numremaps) {
        print "Warning: Number of mask files ($nummasks) does not match number of remap files ($numremaps)\n";
        if (defined($filetype)) {
            print "Continuing processing with mask files\n";
            $files = $maskfiles;
        }
        else {
            print "Warning: Defaulting to remap files instead of mask files\n";
            $files = $remapfiles;
        }
    }
    else {
        $files = $maskfiles;
    }
}
elsif (lc($filetype) eq 'remap') {
    $files = $remapfiles;
}

if (scalar(keys %$files) == 0) {
    print STDERR "Error: PFSNormQuery returned zero results\n";
    print STDERR "Aborting\n";
    exit $FAILURE;
}

my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);
