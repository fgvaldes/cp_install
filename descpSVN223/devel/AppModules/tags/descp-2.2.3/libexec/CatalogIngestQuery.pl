#!/usr/bin/env perl
########################################################################
#  $Id: MkFlatCor.pl 3381 2009-04-02 20:28:07Z mgower $
#
#  $Rev:: 3381                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-04-02 15:28:07 #$:  # Date of last commit.
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#      Get imageid corresponding to catalog
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;
use Orchestration::filelistfunc;

my ($outputxml, $nite, $project, $filetype, $curr_run, $query_run, $block_num) = undef;
my $debug = 0;

use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'project=s' => \$project,
    'nite=s' => \$nite,
    'filetype=s' => \$filetype,
    'curr_run=s' => \$curr_run,
    'query_run=s' => \$query_run,
    'block_num=i' => \$block_num,
    'debug=i' => \$debug,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($project)) {
    print "Error: Must specify project\n";
    exit $FAILURE;
}

if (!defined($nite)) {
    print "Error: Must specify nite\n";
    exit $FAILURE;
}

if (!defined($filetype)) {
    $filetype = 'red_cat';
}
elsif ($filetype ne 'red_cat' and $filetype ne 'red_ccat') {
    print "Error: Must invalid filetype ($filetype)\n";
    exit $FAILURE;
}



if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    exit $FAILURE;
}


# if red_ccats, get parent red_cat parent ids to be used for image id
my $dbq;
if ($filetype eq 'red_cat') {
    $dbq = "select catalog.id,parentid as imageid from catalog where project='$project' and nite='$nite'";
}
elsif ($filetype eq 'red_ccat') {
    $dbq = "select cc.id,c.parentid as imageid,cc.parentid as redcatid from catalog cc, catalog c where c.id = cc.parentid and cc.project='$project' and cc.nite='$nite' and cc.catalogtype='red_ccat'";
}
my $dbh = new DB::DESUtil(verbose=>3,debug=>1);
my $rs = $dbh->selectall_hashref($dbq, "id");
$dbh->disconnect();

# values to use in 'where' portion of DB query
my %key_vals = ( project => $project, fileclass => 'red', nite=>$nite, filetype=>$filetype);
setQueryRun($query_run, $block_num, $curr_run, \%key_vals);

my %query = ();
$query{'location'}->{'key_vals'} = \%key_vals;

my $files = getFileList(\%query, $debug);
print "Number of files = ", scalar keys %$files,"\n";

# add imageid to files
foreach my $f (keys %$files) {
    my $fref = $files->{$f};
#    print "\n\nfref\n", Dumper($fref), "\n\n\n";
    if (!defined($fref->{'id'})) {
        print "Error: Cannot find id in:\n";
        print Dumper($fref),"\n";
        exit 0;
    }
    elsif (!defined($rs->{$fref->{'id'}})) {
        print "Error: Cannot set imageid for file ", $fref->{'id'}, "\n";
        print Dumper($fref),"\n";
        exit 0;
    }
    else {
#    print Dumper($rs->{$fref->{'id'}}), "\n";
        $fref->{'imageid'} = $rs->{$fref->{'id'}}{'imageid'};
    }
}

my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);
