#!/usr/bin/env perl
########################################################################
#  $Id: CoaddSwarpQuery.pl 6429 2011-01-28 22:48:37Z mgower $
#
#  $Rev:: 6429                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2011-01-28 15:48:37 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use Exception::Class('bail');
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use ImageProc::DB;
use Orchestration::misc;
use Orchestration::filelistfunc;

my ($outputxml, $tilename, $project, $minexptime, $red_run, $curr_run, $query_run, $block_num, $caltype) = undef;
my $use_less = 0;
my $no_scamp_check = 0;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'tilename=s' => \$tilename,
    'project=s' => \$project,
    'minexptime=s' => \$minexptime,
    'caltype=s' => \$caltype,
    'red_run=s' => \$red_run,
    'curr_run=s' => \$curr_run,
    'query_run=s' => \$query_run,
    'block_num=i' => \$block_num,
    'use_less' => \$use_less,
    'no_scamp_check|no_scamp|nsc' => \$no_scamp_check,
);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($tilename)) {
    print "Error: Must specify tilename\n";
    exit $FAILURE;
}

if (!defined($project)) {
    print "Error: Must specify project\n";
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    exit $FAILURE;
}

if (!defined($caltype)) {
    $caltype = 'tilebytile';
}
$caltype = lc($caltype);

# values to use in 'where' portion of DB query
my %key_vals = ( project => $project, tilename => $tilename );
my %imagevals = (scampflg => 0);
if (defined($minexptime) && ($minexptime > 0)) {
    $imagevals{'exptime'} = ">$minexptime";
}

my $desDBI = DB::DESUtil->new('verbose'=>0);
my $location_table = $desDBI->LOCATION_TABLE()->{'table_name'};
$desDBI->disconnect();

my $files = {};
my %query = ();
$query{$location_table}->{'key_vals'} = \%key_vals;

# get list of remaps for baseline number of files 
$key_vals{'fileclass'} = 'red';
$key_vals{'filetype'} = 'remap';

if (defined($red_run)) {
    my $value = $red_run;
    # make an array ref if list of values
    if ($red_run =~ /,/) {
        my @arr = split /\s*,\s*/, $value;
        $value = \@arr;
    }   
    $key_vals{'run'} = $value;
}
my %queryCoadd;

if($no_scamp_check) {
    %queryCoadd= ('location' => {
        'select_fields' => 'all',
        'key_vals' => \%key_vals,
                  });
    
}
else {
    %queryCoadd= ('location' => {
        'join' => {'id' => 'image.id'},
        'select_fields' => 'all',
        'key_vals' => \%key_vals,
                  },
                  'image' => {
                      'key_vals' => \%imagevals,    } );
}
my $remapfiles = getFileList(\%queryCoadd);
my $numremaps = scalar(keys %$remapfiles);

if ($numremaps == 0) {
    print STDERR "Warning: CoaddSwarpQuery returned zero remap files\n";
    if (!$use_less) {
        print STDERR "Aborting\n";
        exit $FAILURE;
    }
}

# first check for norm files
$key_vals{'fileclass'} = 'coadd';
$key_vals{'filetype'} = 'norm';
setQueryRun($query_run, $block_num, $curr_run, \%key_vals);
my $normfiles = getFileList(\%queryCoadd);
my $numnorm = scalar(keys %$normfiles);

if ($numnorm == $numremaps) {
    $files = $normfiles;
}
else {
    print "Warning: Number of norm files ($numnorm) does not match number of remap files ($numremaps)\n";

    if ($use_less) {
        print "Warning: use_less flag given, so continuing processing using norm files\n";
        $files = $normfiles;
    }
    else {
        # check for mask files
        $key_vals{'fileclass'} = 'coadd';
        $key_vals{'filetype'} = 'mask';
        my $maskfiles = getFileList(\%queryCoadd);
        my $nummask = scalar(keys %$maskfiles);
        if ($nummask == $numremaps) {
            print "Warning: Continuing processing using mask files\n";
            $files = $maskfiles;
        }
        else {
            print "Warning: Number of mask files ($nummask) does not match number of remap files ($numremaps)\n";
            print "Warning: Continuing processing using remap files\n";
            
            $files = $remapfiles;
        }
    }
}

if (scalar(keys %$files) == 0) {
    print STDERR "Error: CoaddSwarpQuery returned zero results\n";
    print STDERR "Aborting\n";
    exit $FAILURE;
}

# add zeropoints from DB to file information
# Get database connection and execute zeropoint data retrival
# routine, then disconnect from the db:

eval {
    my $db = ImageProc::DB->new();
    $db->getZeroPoints({'remap_list' => $files, 'caltype'=>$caltype},$tilename);
    $db->disconnect();
};
if (my $e = Exception::Class->caught()) {
    if (ref $e) {
        warn "\nFatal internal error:\n";
        warn $e->error, "\n", $e->trace->as_string, "\n";
        exit 255;
    }
    else {
        die $e;
    }
}

# Remove files from list that don't have zeropoint information
print "\n\nRemoving files from list that don't have zeropoint information\n";
while ( my ($key, $fhref) = each(%$files) ) {
    if (!defined($fhref->{'mag_zero'})) {
        printf "File missing mag_zero: orig file id=%d, red file id=%d\n", $fhref->{'id'}, $fhref->{'parentid'};
        delete($files->{$key});
    }
    elsif (!defined($fhref->{'sigma_mag_zero'})) {
        printf "File missing sigma_mag_zero: orig file id=%d, red file id=%d\n", $fhref->{'id'}, $fhref->{'parentid'};
        delete($files->{$key});
    }
}

# output file information in XML format
my $lines = convertSingleFilesToLines($files);
outputXMLList($outputxml, $lines);
