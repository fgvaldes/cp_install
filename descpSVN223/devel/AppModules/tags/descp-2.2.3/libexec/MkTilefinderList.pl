#!/usr/bin/env perl
########################################################################
#  $Id: MkTilefinderList.pl 3381 2012-07-26 20:28:07Z ankitc $
#
#  $Rev:: 3381                             $:  # Revision of last commit.
#  $LastChangedBy:: Ankit Chandra                 $:  # Author of last commit.
#  $LastChangedDate:: 2012-07-26 15:28:07 #$:  # Date of last commit.
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#     Creates master list of input files for cp_remap and runs tilefinder to do that in addition to queries
#
########################################################################

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use DB::FileUtils;
use Orchestration::misc;
use Orchestration::filelistfunc;
use DB::EventUtils;
use DB::IngestUtils;
use DAF::Archive::Tools qw(getArchiveLocation);


my ($outputxml, $nite, $project, $curr_run, $query_run, $block_num, $sat_pixels, $min_exposures, $min_ccds,$nstars,$filter_run,$DES_HOME,$tilefinder_input,$tilefinder_output,$confFile,$cpRemapInputHash,$cpremapInputFile,$archiveNode,$mapRes) = undef;
my $debug = 0;
#$mapRes = 64;
use Getopt::Long;
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
    'outputxml=s'  =>  \$outputxml,
    'nite=s' => \$nite,
    'project=s' => \$project,
    'mapRes=s' => \$mapRes,
    'filter_run=s' => \$filter_run,
    'tilefinder_input=s' => \$tilefinder_input,
    'tilefinder_output=s' => \$tilefinder_output,
    'tilefinder_conf_file=s' => \$confFile,
    'archive_node=s' => \$archiveNode,
    'debug=i' => \$debug,
);

$cpremapInputFile = "cp_remap_input.xml";
# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (!defined($tilefinder_output)) {
    print "Error: Must specify fullpath for tilefinder output file\n";
    usage();
    exit $FAILURE;
}
if (!defined($tilefinder_input)) {
    print "Error: Must specify fullpath of tilefinder input file\n";
    usage();
    exit $FAILURE;
}
if (!defined($archiveNode)) {
    print "Error: Must specify archive node\n";
    usage();
    exit $FAILURE;
}
if (!defined($confFile)) {
    print "Error: Must tile finder config file\n";
    usage();
    exit $FAILURE;
}
if (!defined($nite)) {
    print "Error: Must specify nite\n";
    usage();
    exit $FAILURE;
}

if (!defined($project)) {
    print "Error: Must specify project\n";
    usage();
    exit $FAILURE;
}

if (!defined($outputxml)) {
    print "Error: Must specify outputxml\n";
    usage();
    exit $FAILURE;
}


if (!defined($filter_run)) {
    print "Error: Must specify filter_run\n";
    usage();
    exit $FAILURE;
}

if (!defined($mapRes)) {
    $mapRes = 1;
}
#
# Make a database connection
#
my $desdbh =  DB::DESUtil->new();

$DES_HOME = $ENV{"DES_HOME"};

my $tempRun = $filter_run;
my $tmpNite = $nite;

$tempRun =~ s/([a-zA-Z0-9_-]+)\s*(,?)/'$1'$2/g;
$tmpNite =~ s/([a-zA-Z0-9_-]+)\s*(,?)/'$1'$2/g;

my $pairupRunsNites = "select run,nite from location where run in ($tempRun) and nite in ($tmpNite)";
my $pairupRunsNitesSth = $desdbh->prepare($pairupRunsNites);


$pairupRunsNitesSth->execute();
my $runNiteHash;
while (my $pairupRunsNitesRows = $pairupRunsNitesSth->fetchrow_hashref()){
	$runNiteHash->{$pairupRunsNitesRows->{'run'}} = $pairupRunsNitesRows->{'nite'};
}

#print "\n the run nite hash: ",Dumper($runNiteHash);
### run this for all the nites that there can be in the command line option
my @allNites = split (',',$nite);
my @allRuns  = split(',',$filter_run);

my ($files_temp,%query,%key_vals,$files);
$files = {};
# Run this loop through allRuns so that the correct nite and run can be matched and queried upon by the getFileList function
for my $run_iter (@allRuns){

	$files_temp = {};
	# values to use in 'where' portion of DB query
	%key_vals = (project => $project, fileclass => 'red', nite=>$runNiteHash->{$run_iter});
	$key_vals{'filetype'} = ['red'];
	
	$curr_run = $run_iter; 
	$query_run = 'current';

	setQueryRun($query_run, $block_num, $curr_run, \%key_vals);

	%query = ();
	$query{'location'}->{'key_vals'} = \%key_vals;
	$files_temp = getFileList(\%query, $debug);
	foreach my $key_files_temp (keys %$files_temp){
		# since mktilefinder needs to accept multiple nites and runs, the original 'unique' key for the hashref $files_temp (returned by getFileList) is not unique anymore. So iterate through all the keys and add the run to the key to make it unique	
		$files->{$key_files_temp."_".$curr_run} = $files_temp->{$key_files_temp};
	}
	#$files = {%$files_temp,%$files};
}

	#print Dumper($files);

#print "\n the file list ";
#print scalar (keys %$files);
#print Dumper($files);

my ($FileIds,$exposureIds,$tempExp);
$FileIds = '';
foreach my $fval (values %$files) {

    $FileIds .= $fval->{'id'}.', ';
    #print "\n ",$fval->{'id'};
}


$FileIds = substr $FileIds, 0 , -2;

if($FileIds !~ /\S+/){
###
## Usage: verbosity, type of message, level, string 
###
DB::EventUtils::reportEvent( 2, 'STATUS', 3, "No files to work on. Exiting with non zero status code" );
$desdbh->disconnect();;
exit(1);
}

##
# get the information about the archive root
##
my ( $locationId, $archiveHost, $archiveRoot, $archiveSiteStr ) =
  getArchiveNodeInfo( $desdbh, $archiveNode )
  if defined $archiveNode;


$filter_run =~ s/(\w+)\s*(,?)/'$1'$2/g;

my $sqlFilter = "Select i.id as id ,i.fwhm as fwhm, i.exptime as exptime ,z.mag_zero as zp ,e.telra as telra ,e.teldec as teldec from Image i, Exposure e, Zeropoint z, Location l where  i.run in ($filter_run) and i.exposureid = e.id and i.scampflg = 0 and l.id=i.id and z.imageid=i.id and l.filetype ='red' and l.fileclass = 'red' and l.project = '$project' ";

#print "\n the query $sqlFilter ";
my $sthFilter = $desdbh->prepare($sqlFilter);
$sthFilter->execute();
#print "\n found ",$sthFilter->rows," rows ";

my $bandExposureHashRef;
my ($bandHashRef, $fileInfoHashref, $fvalDel,$i);
my @removeFileIds;
$i=0;
while (my $rowFilter = $sthFilter->fetchrow_hashref()){
	# store the rows selected into a fileinfoHashref
	$fileInfoHashref->{$rowFilter->{'id'}} = $rowFilter;
}
#print "\n fileinfohas ", Dumper($fileInfoHashref);
# Loop over all the files found in the first filter. in the loop remove all those files which belong to a band which has less than predetermined minimum number of exposures
my ($tmpRun,$allFilesDets,$fwhm,$telra,$teldec,$zp,$exptime,$inputTileFinder,$outputTileFinder,$cpRemapUsedFiles);
#$inputTileFinder = "tilefinder_input.txt";
#$outputTileFinder = "tilefinder_output.txt";
open (FH,">$tilefinder_input");
foreach my $fval (keys %$files) {

	#$FileIds .= $fval->{'id'}.', ';
	#print "\n evaluating for run ",$files->{$fval}->{'run'}," against $filter_run";
	$tmpRun = $files->{$fval}->{'run'};
	# throw away the runs which might be redundant 
	if($filter_run =~ /$tmpRun/){
	#print "\n looking for,", Dumper($fileInfoHashref->{$files->{$fval}->{'id'}});
		# As long as the file does not exist in the second query, remove it from files hashref
		unless (exists $fileInfoHashref->{$files->{$fval}->{'id'}}){
		print "\n STATUS3BEG removing the id for run.  ",$files->{$fval}->{'id'},", irrelevant run.  STATUS3END\n";
			delete($files->{$fval});
		}else{
			#print Dumper($files->{$fval});
			$allFilesDets->{'project'} = $files->{$fval}->{'project'};
			$allFilesDets->{'fileclass'} = $files->{$fval}->{'fileclass'};
			$allFilesDets->{'filetype'} = $files->{$fval}->{'filetype'};
			$allFilesDets->{'filename'} = $files->{$fval}->{'filename'};
			$allFilesDets->{'run'} = $files->{$fval}->{'run'};
			$allFilesDets->{'nite'} = $files->{$fval}->{'nite'};
			$allFilesDets->{'band'} = $files->{$fval}->{'band'};
			$allFilesDets->{'tilename'} = $files->{$fval}->{'tilename'};
			$allFilesDets->{'exposurename'} = $files->{$fval}->{'exposurename'};
			$allFilesDets->{'ccd'} = $files->{$fval}->{'ccd'};
			$allFilesDets->{'archivesites'} = $files->{$fval}->{'archivesites'};
			my $finalDirName = getArchiveLocation($allFilesDets);

			#print "\n keeping the id ",$files->{$fval}->{'id'};
			$fwhm = $fileInfoHashref->{$files->{$fval}->{'id'}}->{'fwhm'}; 
			$telra = $fileInfoHashref->{$files->{$fval}->{'id'}}->{'telra'}; 
			$teldec = $fileInfoHashref->{$files->{$fval}->{'id'}}->{'teldec'}; 
			$exptime = $fileInfoHashref->{$files->{$fval}->{'id'}}->{'exptime'}; 
			$zp = $fileInfoHashref->{$files->{$fval}->{'id'}}->{'zp'}; 
			# Print the values in to the output file
			print FH "$archiveRoot/$finalDirName/",$files->{$fval}->{'filename'}," ",sprintf('%0.9f100', $telra)," ",sprintf('%0.9f100',$teldec)," ",sprintf('%0.9f100', $exptime)," ",sprintf('%0.9f100', $fwhm)," ",sprintf('%0.9f100',  $zp),"\n";
			# assign it to a hashref for later use 
			$files->{$fval}->{'complete_path'} = $finalDirName."/".$files->{$fval}->{'filename'};
			# create a hashref to reverse map the files hashref so that we can query the keys by knowing the fullpath of a file
			$cpRemapUsedFiles->{$finalDirName."/".$files->{$fval}->{'filename'}} = $fval;
		}
	}else{
		print "\n STATUS3BEG removing the id for run.  ",$files->{$fval}->{'id'},", Run mismatch. STATUS3END\n";
		delete($files->{$fval});
	}
}
#print "\n the cpremapusedfiles",Dumper($cpRemapUsedFiles);
##
# Run the TileFinder
##
#$DES_HOME = "/home/rgruendl/DES_HOME/develCP"; 

# perl MkTilefinderList.pl  -nite 20040911 -outputxml tile.xml -curr_run 20120725151642_20040911 -query_run current -project CP2 -filter_run 20120711141036_20110726

#Change2.2 remove the -tpfile option : Done
#Change2.2 add the -mapRes option : Done

print "\n STATUS3BEG Executing tilefinder: $DES_HOME/bin/TileFinder -input $tilefinder_input -output $tilefinder_output -config $confFile STATUS3END";
my $retTileFinder = readpipe( "$DES_HOME/bin/TileFinder -input $tilefinder_input -output $tilefinder_output -config $confFile -mapRes $mapRes" );

if ($retTileFinder !~ /\S*/ || $retTileFinder =~ /could not/i || $retTileFinder =~ /failed/i){
	print "\n STATUS5BEG Tilefinder failed!: $!. Exiting with code 1 STATUS5END\n";
	exit(1);
}else{
	print "\n STATUS3BEG tilefinder complete: $retTileFinder STATUS3END";
}


open(FH_tilefinderoutput,"<$tilefinder_output");
#open(FH_cpremapinput,">$cpremapInputFile");
my @tilefinderOutput = <FH_tilefinderoutput>;

my (@TFinputArr,$TFFullpath,$TFRA,$TFDEC,$TFTilename,$TFPixelsize,$tmpPath,@tilefinderinputArr,$finalFiles);
#open(FH_remapinput,">$outputxml");
foreach my $tilefinderOutputline (@tilefinderOutput){

	@tilefinderinputArr = split (' ',$tilefinderOutputline);
	$TFFullpath = $tilefinderinputArr[0];
	$TFTilename = $tilefinderinputArr[1];
	$TFPixelsize = $tilefinderinputArr[2];
	$TFRA = $tilefinderinputArr[3];
	$TFDEC = $tilefinderinputArr[4];

	$tmpPath = $TFFullpath;
	$tmpPath =~ s/^(.*?)\/Archive\///;
	#print "\n trying path $tmpPath from $TFFullpath";
	$finalFiles->{$cpRemapUsedFiles->{$tmpPath}} = $files->{$cpRemapUsedFiles->{$tmpPath}};
	$finalFiles->{$cpRemapUsedFiles->{$tmpPath}}->{'tilename'} = $TFTilename;
	$finalFiles->{$cpRemapUsedFiles->{$tmpPath}}->{'pixelsize'} = $TFPixelsize;
	$finalFiles->{$cpRemapUsedFiles->{$tmpPath}}->{'ra'} = $TFRA;
	$finalFiles->{$cpRemapUsedFiles->{$tmpPath}}->{'dec'} = $TFDEC;
	#print FH_remapinput "$TFFullpath $TFTilename $TFPixelsize $TFRA $TFDEC \n";	
	#$TFFileinfoHashref = filenameResolve($TFFullpath);
}


#print "\n final hashref of final files, ",Dumper($finalFiles);

my $lines = convertSingleFilesToLines($finalFiles);
#print Dumper($lines);

# comment this out for testing 
outputXMLList($outputxml, $lines);

$desdbh->disconnect();


sub usage
{


	print "\n perl MkTilefinderList.pl  -nite <comma separated nites>  -outputxml <full path to output xml to be used as input for cp_remap.pl> -project <project> -filter_run <comma separated runs to be queried upon> -tilefinder_conf_file <fullpath to the config file> -archive_node <archive_name> -tilefinder_input <input filename for tilefinder> -tilefinder_output <output filename for tilefinder>\n"; 

}
