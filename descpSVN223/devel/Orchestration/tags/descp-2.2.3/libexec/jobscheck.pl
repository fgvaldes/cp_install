#!/usr/bin/env perl
########################################################################
#  $Id: jobscheck.pl 4600 2009-10-20 21:50:23Z mgower $
#
#  $Rev:: 4600                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-10-20 14:50:23 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::email;


if (scalar(@ARGV) != 2) {
    print "Usage: jobscheck.pl configfile condorjobid\n";
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $CondorID = $ARGV[1];

my $Config = new Orchestration::desconfig({configfile=>$DESFile});

# log condor jobid
$CondorID = sprintf("%d", $CondorID);
my $block = $Config->value("block");
logEvent($Config, $block, 'jobscheck', 'j', 'cid', $CondorID);

my $run = $Config->value("submit_run");

my ($Msg1, $Msg2, $Status);
$Status = $SUCCESS;

if (-r "JOBS_FAILED.txt") {
    $Msg1 = "$run:  At least one job in block $block has failed.  Aborting processing.";
    $Status = $FAILURE;

    my %jobid;
    $Msg2 = "List of failed jobs:\n";
    open FH, "< JOBS_FAILED.txt";
    while (my $j = <FH>) {
        chomp($j);
        $jobid{$j} = 1;
    }
    close FH;
    foreach my $j (sort keys %jobid) {
        $Msg2 .= "\t$j\n";
    }
    close FH;
    $Msg2 .= "\n\n".getJobInfo($block);

    sendEmail($Config, $block, $Status, "[FAILED]", $Msg1, $Msg2);
}

exit $Status;
