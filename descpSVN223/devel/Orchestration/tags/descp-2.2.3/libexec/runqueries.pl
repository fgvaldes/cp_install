#!/usr/bin/env perl
########################################################################
#  $Id: blockmain.pl 3263 2009-03-06 19:51:04Z mgower $
#
#  $Rev:: 3263                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-03-06 13:51:04 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use Cwd;
use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::blockmain;
use Orchestration::hist;
use Orchestration::misc;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;


if (scalar(@ARGV) != 2) {
    print "Usage: runqueries.pl configfile condorjobid\n";
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $CondorID = $ARGV[1];

if (! -r $DESFile) {
    print STDERR "Error: Can not open config file '$DESFile'\n";
    exit $FAILURE;
}
my $Config = new Orchestration::desconfig({configfile=>$DESFile});

my $block = $Config->value('block');

# log condor jobid
$CondorID = sprintf("%d", $CondorID);
logEvent($Config, $block, 'runqueries', 'j', 'cid', $CondorID);

if (!$Config->exists('module_list')) {
    print "Error:  No modules to run.\n";
    exit $FAILURE;
}

### Get master lists and files calling external codes when needed

my $uber_module_obj = $Config->obj('module');
my $module_list = $Config->value('module_list');
my @module_array = split /\s*,\s*/, $module_list;

my %modules_prev_in_list = ();
foreach my $module (@module_array) {
    if (! $uber_module_obj->exists($module)) {
        print "Error: Could not find module description for module $module\n";
        exit $FAILURE;
    }
    my $module_obj = $uber_module_obj->obj($module);
    my $uber_list_obj = $module_obj->obj('list');
    my @listorder;
    if ($module_obj->exists('list_order')) {
        @listorder = split /\s*,\s*/, $module_obj->value('list_order');
    }
    else {
        @listorder = $uber_list_obj->keys();
    }
    foreach my $listname (@listorder) {
        my $list_obj = $uber_list_obj->obj($listname);
        if (!$list_obj->exists('depends') ||
            !defined($modules_prev_in_list{ $list_obj->value('depends') }) ) {

            print "\t$module-$listname: creating master list\n";
            createMasterList($Config, $DESFile, $module, $module_obj, $listname, $list_obj);
        }
    }

    # process each "file" in each module
    my $uber_file_obj = $module_obj->obj('file');
    foreach my $filename ($uber_file_obj->keys()) {
        my $file_obj = $uber_file_obj->obj($filename);

#print "module = $module\n";
#print "filename = $filename\n";
#print "depends = ", $file_obj->value('depends'), "\n" if $file_obj->exists('depends');
#print "modules_prev_in_list = ";
#foreach my $m (keys %modules_prev_in_list) {
#    print "$m   ";
#}
#print "\n";

        if (!$file_obj->exists('depends') ||
            !defined($modules_prev_in_list{ $file_obj->value('depends') }) ) {

            print "\t$module-$filename: creating master list\n";
            createMasterList($Config, $DESFile, $module, $module_obj, $filename, $file_obj);
        }
    }
    $modules_prev_in_list{$module} = 1;
} 

readXML($Config);
createStageArchiveList($Config);

exit $SUCCESS;



###########################################################
sub createMasterList {
    my ($Config, $DESFile, $module_name, $module_obj, $search_name, $search_obj) = @_;

    print "\tcreateMasterList: BEG\n";
    my $cwd = cwd();
    my $outputxml = "${module_name}_${search_name}.xml";
    my $output = "${module_name}_${search_name}.out";

    my ($exec, $args);
    if ($search_obj->exists('query_fields')) {
        $exec = "$FindBin::Bin/../libexec/genquerydb.pl";
        my $queryfields = $search_obj->value('query_fields');
        $args = "-outputxml $outputxml -config $DESFile -query_fields \"$queryfields\" -module $module_name -search $search_name";
    }
    elsif ($search_obj->exists('exec')) {
        $exec = $search_obj->value('exec');
        if (!$search_obj->exists('args')) {
            print "\t\tWarning:  $search_name in module $module_name does not have args defined\n";
            $args = "";
        }
        else {
            $args = $search_obj->value('args');
        }
    }
    else {
        print "\tError: $search_name in module $module_name does not have exec or query_fields defined\n";
        exit $FAILURE;
    }

    $args = $Config->interpolate($args, {currentvals=>{module=>$module_name, outputxml=>$outputxml},searchobj=>$search_obj});
    $exec = $Config->interpolate($exec, {currentvals=>{module=>$module_name, outputxml=>$outputxml},searchobj=>$search_obj});

    if (! -x $exec) {
        print "\tError: Could not find executable '$exec'\n";
        exit $FAILURE;
    }

    # call code
    print "\t\tCalling code to create master list for obj $search_name in module $module_name\n";
    print "\t\t$exec $args\n";
    print "\t\tSee output in $cwd/$output\n";
    print "\t\tSee master list will be in $cwd/$outputxml.\n";

    print "\t\tCreating master list - start ", time, "\n";
    my $out = `$exec $args > $output 2>&1`;
    my $stat = $?;
    my ($exit, $exitstr) = exitCodeInfo($stat);
    print "\t\texit=$exit, exitstr=$exitstr\n";
    print "\t\tCreating master list - end ", time, "\n";

    if ((! -r $outputxml) || $stat) {
        print "Error: problem creating master list\n";
        print "$exec $args\n";
        exit $FAILURE;
    }

    print "\tcreateMasterList: END\n\n";
} ## end createMasterList
