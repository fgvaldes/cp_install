#!/usr/bin/env perl
########################################################################
#  $Id: logpost.pl 6584 2011-03-17 18:13:24Z mgower $
#
#  $Rev:: 6584                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-03-17 11:13:24 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

use strict;
use warnings;

my $debugfh;
open $debugfh, "> logpost.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;
$debugfh->autoflush(1);

print "$0 @ARGV\n";

use Data::Dumper;
use File::Copy;
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::email;
use Orchestration::desconfig;
use Orchestration::Condor::status;

my ($configfile, $config);
my ($block, $subblocktype, $subblock);
my $retval = '';


if (scalar(@ARGV) >= 4)
{
  $configfile = $ARGV[0];
  $block = $ARGV[1];
  $subblocktype = $ARGV[2];
  $subblock = $ARGV[3];
}
else
{
  print "Usage: logpost configfile block subblocktype subblock retval\n";
  close $debugfh;
  exit $FAILURE;
}

close $debugfh;
move("logpost.out", "logpost.$subblock.out");
open $debugfh, ">> logpost.$subblock.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;
$debugfh->autoflush(1);


if (scalar(@ARGV) == 5) {
    $retval = $ARGV[4];
}

# read sysinfo file
$config = new Orchestration::desconfig({configfile=>$configfile});

logEvent($config, $block, $subblock, $subblocktype, 'posttask', $retval);


my $dbid_orchtasks = $config->value('dbid_orchtasks');
if (exists($dbid_orchtasks->{$subblock})) {
    my $dbid = $dbid_orchtasks->{$subblock};
    my $logfile;
    if ($subblock eq 'pipelinesmngr') {
        $logfile = 'pipelinesmngr.dag.dagman.log';
    }
    else {
        $logfile = "condorjobs.log";
    }
    open CONDORLOG, "< $logfile";
    my $Jobs = parseCondorUserLog(\*CONDORLOG);
    #print Dumper($Jobs),"\n\n";
    my ($j, $jinfo);
    while ((($j, $jinfo) = each(%$Jobs)) && ($jinfo->{'dagnode'} ne $subblock)) {}

    if (defined($jinfo)) {
#    if ($jinfo->{'dagnode'} eq $subblock) {
        print Dumper($jinfo), "\n";
        #my %sethash = (
        #   submit_time => $jinfo->{'submittime'},  
        #   start_time => $jinfo->{'starttime'},
        #   end_time => $jinfo->{'endtime'},
        #   orch_id => $jinfo->{'clusterid'},
        #   status => $jinfo->{'retval'}
        #);
        updateOrchTaskEnd($config, $dbid, $jinfo->{'clusterid'}, $jinfo->{'retval'} );
    }
    else {
        updateOrchTaskEnd($config, $dbid, undef, $retval );
    }
}
else {
    print "Could not find dbid for orchtask $subblock\n";
}


# In order to continue, make pipelines dagman jobs exit with success status
if ($subblock =~ /pipelinesmngr/) {
    $retval = $SUCCESS;
}

if (($retval != $SUCCESS) && ($subblock !~ /jobscheck/) && ($subblock !~ /mngr/)) {
    sendSubblockEmail($config, $block, $subblock, $retval);
}

close $debugfh;

exit $retval;
