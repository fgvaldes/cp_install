#!/usr/bin/env perl
########################################################################
#  $Id: runpost.pl 3263 2009-03-06 19:51:04Z mgower $
#
#  $Rev:: 3263                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-03-06 13:51:04 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################
use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::desconfig;
use Orchestration::hist;
use Orchestration::misc;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (scalar(@ARGV) != 2) {
    print "Usage: runpost.pl configfile condorjobid\n";
    exit $FAILURE;
}

my $DESFile = $ARGV[0];
my $CondorID = $ARGV[1];

if (! -r $DESFile) {
    print STDERR "Error: Can not open config file '$DESFile'\n";
    exit $FAILURE;
}
my $config = new Orchestration::desconfig({configfile=>$DESFile});
my $run = $config->value('submit_run');
my $project = $config->value('project');
my $deshome = $config->value('des_home');
my $uberdir = $config->value('uberctrl_dir');

# log condor jobid
$CondorID = sprintf("%d", $CondorID);
logEvent($config, 'runpost', 'main', 'j', 'cid', $CondorID);

my $exit=$SUCCESS;
if (!$config->exists('notarget') || !$config->value('notarget') ) {
    # cleanup files on target machine
    if ($config->exists('cleanup') && $config->value('cleanup')) {
        $exit = cleanupRun($config, $project, $run, $deshome, $uberdir);
    }
    else {
        print "Operator specified not to cleanup run.  Skipping cleanup.\n";
    }

    if (!$exit) {
        # replicate run
        if ($config->exists('replica_nodes')) {
            $exit = replicateRun($config, $run, $deshome, $uberdir);
        }
        else {
            print "Operator did not specify replica_nodes.\n";
        }
    }
}
else {
    print "Operator specified notarget.  No target files to cleanup or replicate\n";
    $exit=$SUCCESS; 
}

exit $exit;


sub cleanupRun {
    my ($config, $project, $run, $deshome, $uberdir) = @_;

    print "cleanupRun: BEG\n";
    my $rmfiletype = 'raw%';
    my $exit = $SUCCESS;

    if ($config->exists('rm_filetype')) {
        $rmfiletype = $config->value('rm_filetype');
    }

    my $listnodes = $config->value('listtargets');
    foreach my $node (split /\s*,\s*/, $listnodes) {
##        my $rmcmd = "echo 'Y' | ${deshome}/bin/arrm -keep-logs -v -v -rwd ${uberdir} -wdp arrm_${node}_tmpdir -project ${project} -run ${run} -filetype $rmfiletype ${node}";
        my $rmcmd = "echo 'Y' | ${deshome}/bin/arrm -v -v -rwd ${uberdir} -wdp arrm_${node}_tmpdir -project ${project} -run ${run} -filetype $rmfiletype ${node}";
        print "$rmcmd\n";
        my $rmout = `$rmcmd 2>&1`;
        my $rmstat = $?;
        print "$rmout\n";
        my ($rmexit, $rmstr) = exitCodeInfo($rmstat);
        print "arrm exited with exit code ($rmexit, $rmstr)\n";

        # flatten DAF logdir
        flattenDir("arrm_$node");

        if ($rmstat != 0) {
            print STDERR "Error:  arrm exited with non-zero exit code ($rmexit, $rmstr)\n";
            $exit=$FAILURE; 
        }

        print "\n\n\n";

    }
    print "cleanupRun: END\n";
    return $exit;
}

sub replicateRun {
    my ($config, $run, $deshome, $uberdir) = @_;
    my $listnodes = $config->value('replica_nodes');
    foreach my $node (split /\s*,\s*/, $listnodes) {
        my $exit=$FAILURE;
        my $try = 1;
        while ($exit && ($try < 4)) {
            my $cpcmd = "${deshome}/bin/arcp -keep-logs -v -v -nclients 1 -rwd ${uberdir} -wdp replicate_${node}_${try}_tmpdir -run ${run} any ${node}";
            my $cpout = `$cpcmd 2>&1`;
            my $cpstat = $?;
            my ($cpexit, $cpstr) = exitCodeInfo($cpstat);
            print "\n\n$cpcmd\n";
            print "$cpout\n";
            print "arcp exited with exit code $cpstat ($cpexit, $cpstr)\n";
        
            if ($cpstat != 0) {
                print STDERR "Error:  arcp exited with non-zero exit code ($cpexit, $cpstr)\n";
                $exit=$FAILURE; 
            }
            else {
                $exit=$SUCCESS; 
            }

            # flatten DAF logdir
            flattenDir("replicate_${node}_$try");
            
            $try++;
        }
    }
    return $exit;
}
