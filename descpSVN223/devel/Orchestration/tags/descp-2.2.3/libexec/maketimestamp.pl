#!/usr/bin/env perl
########################################################################
#  $Id: logpre.pl 1983 2008-10-08 18:19:33Z mgower $
#
#  $Rev:: 1983                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-10-08 13:19:33 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

use strict;
use warnings;

use POSIX qw(strftime);
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::desconfig;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

if (scalar(@ARGV) != 2) {
    print "Usage: maketimestamp.pl configfile condorjobid\n";
    exit $FAILURE;
}

my ($cmd, $out, $stat, $cexit, $cstr);
my $DESFile = $ARGV[0];
my $CondorID = $ARGV[1];

if (! -r $DESFile) {
    print STDERR "Error: Can not open config file '$DESFile'\n";
    exit $FAILURE;
}
my $config = new Orchestration::desconfig({configfile=>$DESFile});
my $block = $config->value('block');

# log condor jobid
$CondorID = sprintf("%d", $CondorID);
logEvent($config, $block, 'maketimestamp', 'j', 'cid', $CondorID);
print "maketimestamp - BEG\n";

# add sleep to ensure at least 1 minute difference between timestamp and aux/xml files
# because find command works at minute level
print "\tsleeping 1 minute...";
sleep(62);
print "done\n";


# create stamp file
print "\tcreating stamp file and ingest list...";
my $stamp = "${block}_timestamp.txt";
my $now_string = strftime "%a %b %e %H:%M:%S %Y", localtime;

open FH, "> $stamp";
print FH "$now_string\n";
close FH;

my $submit = $config->value('submit_node');
my $target = $config->value('target_node');
my $targetdir = $config->value('archive_root').'/'.$config->value('run_dir').'/runtime/'.$block;
my $submitdir = $config->value('work_dir').'/runtime/'.$block;

# ingest timestamp file
my $ingestfile = "ingest_stamp.list";
open FH, "> $ingestfile";
print FH "<file>\n";
print FH "\tfileid=0\n";
print FH "\tlocalpath=$submitdir\n";
print FH "\tlocalfilename=$stamp\n";
print FH "</file>\n";
close FH;

print "done\n";

my $deshome = $config->value("des_home");
my $verbose = 3;

$cmd = "$deshome/bin/file_ingest.pl -filelist $ingestfile -verbose $verbose -archivenode $submit";
print "\tingestcmd = $cmd\n";
$out = `$cmd 2>&1`;
$stat = $?;
($cexit, $cstr) = exitCodeInfo($stat);
$out =~ s/\n/\n\t\t/g;
print "\tout = $out\n";
print "\tstat = $stat ($cexit, $cstr)\n";
if (($stat != 0) || ($out =~ /STATUS5/i)) {
    exit $FAILURE;
}

if ($submit ne $target) {
    # copy timestamp file to target machine
    my $tries = 0;
    my $maxtries = 5;
    $stat = 1;
    while ($tries < $maxtries && $stat) {
        my $cmd = "$deshome/bin/arcp -v -v -nclients 1 -filelist $ingestfile $submit $target";
        print "arcp cmd: $cmd\n";
        $out = `$cmd 2>&1`;
        $stat = $?;
        ($cexit, $cstr) = exitCodeInfo($stat);
        $out =~ s/\n/\n\t\t/g;
        print "\tout = $out\n";
        print "\tstat = $stat ($cexit, $cstr)\n";
        $tries++;
        if ($stat != 0) {
            print "Failed to copy timestamp file\n";
            if ($tries >= $maxtries) {
                print "Tried $maxtries times.  Aborting\n";
                exit $FAILURE;
            }
            else {
                print "Retrying arcp ----------\n";
            }
        }
    }
}

print "maketimestamp - END\n\n";

exit $SUCCESS;
