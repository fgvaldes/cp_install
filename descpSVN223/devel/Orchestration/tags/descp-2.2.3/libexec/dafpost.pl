#!/usr/bin/env perl
########################################################################
#  $Id: logpost.pl 1983 2008-10-08 18:19:33Z mgower $
#
#  $Rev:: 1983                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-10-08 13:19:33 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

use strict;
use warnings;

use Getopt::Long;
use File::Basename;
use File::Copy;

use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::email;
use Orchestration::desconfig;

my ($configfile, $config);
my ($block, $subblocktype, $subblock);
my $retval = '';

my $debugfh;
open $debugfh, "> dafpost.out";
local *STDOUT = $debugfh;
local *STDERR = $debugfh;

if (scalar(@ARGV) >= 4)
{
  $configfile = $ARGV[0];
  $block = $ARGV[1];
  $subblocktype = $ARGV[2];
  $subblock = $ARGV[3];
}
else
{
  print "Usage: postdaf configfile block subblocktype subblock retval\n";
  close $debugfh;
  exit $FAILURE;
}

if (scalar(@ARGV) == 5) {
    $retval = $ARGV[4];
}

# read sysinfo file
$config = new Orchestration::desconfig({configfile=>$configfile});

logEvent($config, $block, $subblock, $subblocktype, 'posttask', $retval);

# Since DAF writes logs inside subdirs which we cannot have inside
# archive directory structure.   So move files up a dir renaming them and 
# remove subdir
flattenDir(${subblock});

if ($retval != $SUCCESS) {
    sendSubblockEmail($config, $block, $subblock, $retval);
}

close $debugfh;

exit $retval;
