########################################################################
#  $Id: submitfunc.pm 7492 2012-02-10 22:06:49Z mgower $
#
#  $Rev:: 7492                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2012-02-10 15:06:49 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package Orchestration::submitfunc;

use strict;
use warnings;

use Exporter   ();
our (@ISA, @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw(&addInfoToDag &addInfoToCondor &runChecks &saveSubmitProv 
                  &createMainDAG &createProcessDAG &createEndRunDAG &createBlockDAG 
                  &createJobScript &createPostIngestScript &createRunElf &checkSubmit);

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::Condor::submit;
use Orchestration::Condor::utils;
use Orchestration::misc;


######################################################################
# For status commands, add some attributes to DAGMan submit file 
# before submiting
sub addInfoToDag {
    my ($config, $dagfile, $debugfh) = @_;
    my ($maxpre, $maxpost, $maxidle, $maxjobs);
    my ($block, $cmd, $out, $stat, $addexit, $cmdexit, $cmdstr);

    $addexit = 0;

    $block = "";
    if ($dagfile =~ /(\S+)mngr.dag/) {
        $block = $1;
        if ($block eq "block") {
            $block = $config->value("block");
        }
    }

    $maxpre = $config->value("dagman_max_pre");
    $maxpost = $config->value("dagman_max_post");
    $maxidle = $config->value("dagman_max_idle");
    $maxjobs = $config->value("dagman_max_jobs");

    $cmd = "condor_submit_dag -f -no_submit -notification never ";
    if (currCondorVersionGE("7.1.0")) {
        $cmd .= " -oldrescue 1 -autorescue 0 -no_recurse ";
    }
    if ($maxpre =~ /\d/) {
        $cmd .= " -MaxPre $maxpre ";
    }
    if ($maxpost =~ /\d/) {
        $cmd .= " -MaxPost $maxpost ";
    }
    if ($maxjobs =~ /\d/) {
        $cmd .= " -maxjobs $maxjobs ";
    }
    if ($maxidle =~ /\d/) {
        $cmd .= " -maxidle $maxidle ";
    }

    print $debugfh "cmd> $cmd ${dagfile}\n";
    $out = `$cmd ${dagfile} 2>&1`;
    $stat = $?;
    print $debugfh "$out\n";
    ($cmdexit, $cmdstr) = exitCodeInfo($stat);
    print $debugfh "condor_submit_dag exit code: $stat ($cmdexit, $cmdstr)\n";

    if ($stat == 0) {
        $dagfile .= ".condor.sub";
        print $debugfh "Calling addInfoToCondor with '$dagfile'\n";
        $addexit = addInfoToCondor($config, $dagfile, $block, "mngr", $debugfh);
    }
    else {
        $addexit = $FAILURE;
    }

    return $addexit;
}

######################################################################
# add some attributes to DAGMan submit file before submitting
sub addInfoToCondor {
    my ($config, $condorfile, $block, $subblock, $debugfh) = @_;
    my $addexit = $SUCCESS;
    
    print $debugfh "addInfoToCondor: $condorfile, $block, $subblock\n";

    my $out = `pwd 2>&1`;
    chomp ($out);
    print $debugfh "addInfoToCondor pwd: $out\n";

    my $project = $config->value("project");
    my $run = $config->value("submit_run");
    my $RunSite = $config->value("run_site");
    my $operator = $config->value("operator");

    # read condor submit file
    my $opensuccess = open(CONDOR, "<${condorfile}");
    if (!$opensuccess) {
        print $debugfh "addInfoToCondor: Error can't open submission file ${condorfile}: $!";
        $addexit = $FAILURE;
    }
    else {
        undef $/;
        my $data = <CONDOR>;
        close CONDOR;
        $/ = "\n";

        print $debugfh "read condor file\n";
        print $debugfh "============\n";
        print $debugfh "$data\n";
        print $debugfh "============\n";

        #  Work around condor_submit_dag bug (6.7.20, 6.8.0-6.8.3, 6.9.1)
        # "The OnExitRemove expression generated for DAGMan by condor_submit_dag evaluated 
        #  to UNDEFINED for some values of ExitCode, causing condor_dagman to go on hold."
        if ($data =~ /on_exit_remove/) {
            if ($data =~ /on_exit_remove\s*=\s*\(\s*ExitSignal\s*==\s*11\s*||\s*\(ExitCode\s*>=0\s*&&\s*ExitCode\s*<=\s*2\)\)/) {
                $data =~ s/on_exit_remove\s+=[^\n]+\n/on_exit_remove  = ( ExitSignal =?= 11 || (ExitCode =!= UNDEFINED && ExitCode >=0 && ExitCode <= 2))\n/; 
#               $data =~ s/queue/on_exit_remove  = ( ExitSignal =?= 11 || (ExitCode =!= UNDEFINED && ExitCode >=0 && ExitCode <= 2))\nqueue/;
            }
        }

        # add DESDM information to condor submit file 
        $data =~ s/\nqueue$/\n+des_isdesjob=TRUE\n+des_project="${project}"\n+des_run="${run}"\n+des_block="$block"\n+des_subblock="$subblock"\n+des_runsite="$RunSite"\n+des_operator="$operator"\nqueue/;

        # to allow inner dags to run inside subdirectories, need to set initialdir
        if ($condorfile =~ /^blockmngr/) {
            $data =~ s/\nqueue$/\ninitialdir=..\/$block\nqueue/;
        }

        # rewrite condor submit file
        $opensuccess = open(CONDOR,">${condorfile}");
        if (!$opensuccess) { 
            print $debugfh "addInfoToCondor:  Error cannot open file ${condorfile} for writing\n";
            $addexit = $FAILURE;
        }
        else {
            print CONDOR $data,"\n";
            close CONDOR;
            my $cout = `cat ${condorfile}`;
            print $debugfh "$cout\n";
        }
    }
    return $addexit;
}



######################################################################
sub saveSubmitProv {
    my ($config,$LogFile) = @_;

    open my $oldout, ">&STDOUT"     or die "Can't dup STDOUT: $!";
    open my $olderr, ">&STDERR"     or die "Can't dup STDERR: $!";

    open STDOUT, '>>', "$LogFile" or die "Can't redirect STDOUT: $!";
    open STDERR, ">&STDOUT"     or die "Can't dup STDOUT: $!";

    select STDERR; $| = 1;      # make unbuffered
    select STDOUT; $| = 1;      # make unbuffered

    my $deshome = $config->value('des_home');
    my $dbid_run = $config->value('dbid_run');

    my $cmd = "$deshome/libexec/logversion.pl -des_home $deshome -system SUBMIT -id $dbid_run";
    print "$cmd\n";
    my $out = `$cmd 2>&1`;
    my $stat = $?;
    print "$out\n";
    print "stat = $stat\n";
    close STDOUT;
    close STDERR;

    open STDOUT, ">&", $oldout or die "Can't dup \$oldout: $!";
    open STDERR, ">&", $olderr or die "Can't dup \$olderr: $!";

    return $stat;
}

######################################################################
sub createProcessDAG {
    my ($config, $dag) = @_;
    my $stat = $SUCCESS;

print "dag = $dag\n";

    my $opensuccess = open PDAG, "> $dag";
    if (!$opensuccess) {
        print STDERR getTimeStamp(), "dessubmit: ERROR Can't open $dag : $!";
        $stat = $FAILURE;
    }
    else {
        print PDAG "DOT process.dot\n";

        my $deshome = $config->value('des_home');
        my $project = $config->value('project');
        my $run = $config->value('submit_run');
        my $runsite = $config->value('submit_node');

        my @blockarray=();
        if ($config->is_array("block_array")) {
            @blockarray = $config->array("block_array");
        }
        else {
            @blockarray = ( $config->value("block_array") );
        }
        $config->setBlockInfo();
        for (my $i = 0; $i < scalar(@blockarray); $i++) { 
            my $b = $blockarray[$i];
            createBlockDAG($config, $b, \*STDOUT);
            print PDAG <<EOF;
JOB $b ../$b/blockmngr.dag.condor.sub
SCRIPT pre $b $deshome/libexec/blockprep.pl config.des
SCRIPT post $b $deshome/libexec/blockpost.pl config.des \$RETURN
EOF

            # don't call these functions past end of blockarray
            if ($i != scalar(@blockarray) - 1) { 
               $config->incBlock();
               $config->setBlockInfo();
            }
        }
        $config->resetBlock();
        $config->setBlockInfo();

        for (my $i=0; $i<scalar(@blockarray)-1; $i++) {
            print PDAG "PARENT ", $blockarray[$i], " CHILD ", $blockarray[$i+1], "\n";
        }
        close PDAG;
        $stat = addInfoToDag($config, $dag, \*STDOUT);
    }
    return $stat;
}

######################################################################
sub createEndRunDAG {
    my ($config, $dag) = @_;

    my $stat = $SUCCESS;
    my $deshome = $config->value('des_home');

    my $opensuccess = open DAG, "> $dag";
    if (!$opensuccess) {
        print STDERR getTimeStamp(), "dessubmit: ERROR Can't open $dag : $!";
        $stat = $FAILURE;
    }
    else {
        my $runsite = $config->value('submit_node');
        my $project = $config->value('project');
        my $run = $config->value('submit_run');
        print DAG <<EOF;
DOT endrun.dot
JOB runpost $deshome/share/condor/localjob.condor
VARS runpost deshome="$deshome" descfg="config.des"
VARS runpost project="$project" run="$run" runsite="$runsite"
VARS runpost block="endrun" jobname="runpost"
VARS runpost exec="\$(deshome)/libexec/runpost.pl"
VARS runpost args="\$(descfg) \$(Cluster)"
SCRIPT pre runpost $deshome/libexec/logpre.pl config.des endrun j \$JOB
SCRIPT post runpost $deshome/libexec/logpost.pl config.des endrun j \$JOB \$RETURN

JOB forksaveruntime $deshome/share/condor/localjob.condor
VARS forksaveruntime deshome="$deshome" descfg="config.des"
VARS forksaveruntime project="$project" run="$run" runsite="$runsite"
VARS forksaveruntime block="endrun" jobname="forksaveruntime"
VARS forksaveruntime exec="\$(deshome)/libexec/forksaveruntime.pl"
VARS forksaveruntime args="\$(descfg) \$(Cluster)"
SCRIPT pre forksaveruntime $deshome/libexec/logpre.pl config.des endrun j \$JOB
SCRIPT post forksaveruntime $deshome/libexec/logpost.pl config.des endrun j \$JOB \$RETURN

PARENT runpost CHILD forksaveruntime
EOF
        close DAG;
        $stat = addInfoToDag($config, $dag, \*STDOUT);
    }

    return $stat;
}


######################################################################
sub createMainDAG {
    my ($config, $maindag, $processdag, $endrundag) = @_;
    my $stat = $SUCCESS;

    my $deshome = $config->value('des_home');
    my $project = $config->value('project');
    my $run = $config->value('submit_run');
    my $runsite = $config->value('submit_node');

    my $opensuccess = open DAG, "> $maindag";
    if (!$opensuccess) {
        print STDERR getTimeStamp(), "dessubmit: ERROR Can't open $maindag : $!";
        $stat = $FAILURE;
    }
    else {
        print DAG <<EOF;
DOT main.dot

JOB processmngr $processdag.condor.sub
VARS processmngr deshome="$deshome" descfg="config.des" 
VARS processmngr project="$project" run="$run" runsite="$runsite"
VARS processmngr block="main" jobname="processmngr"
SCRIPT pre processmngr $deshome/libexec/logpre.pl config.des mainmngr j \$JOB 
SCRIPT post processmngr $deshome/libexec/summary.pl config.des \$RETURN

JOB endrunmngr $endrundag.condor.sub
VARS endrunmngr deshome="$deshome" descfg="config.des" 
VARS endrunmngr project="$project" run="$run" runsite="$runsite"
VARS endrunmngr block="main" jobname="endrunmngr"
SCRIPT pre endrunmngr $deshome/libexec/logpre.pl config.des mainmngr j \$JOB 
SCRIPT post endrunmngr $deshome/libexec/logpost.pl config.des mainmngr j \$JOB \$RETURN

PARENT processmngr CHILD endrunmngr
EOF
        close DAG;
        $stat = addInfoToDag($config, $maindag, \*STDOUT);
    }

    return $stat;
}



######################################################################
sub createBlockDAG  {
    my $config = shift;
    my $block = shift;
    my $DebugFH = shift;

    if (!defined($DebugFH)) {
        $DebugFH = \*STDERR;
    }

    my $deshome;
    if ($config->exists('des_home')) {
        $deshome = $config->value('des_home');
    }
    else {
        print "Error: des_home not defined in config.des\n";
        exit $FAILURE;
    }
 
my $out = `pwd 2>&1`;
chomp ($out);
print $DebugFH "createBlockDAG pwd: $out\n";

    chomp($block);

    if (! -d "../$block") {
        mkdir "../$block";
    }
    chdir "../$block";

    ## create dummy block DAG
    my $dag = "blockmngr.dag";
    my $success = open DAG, "> $dag";
    if (!$success) {
        print STDERR getTimeStamp(), "Error: Can't create block dag file $dag : $!";
        exit $FAILURE;
    }

    print DAG <<EOF;
DOT block.dot
JOB blockmain $deshome/share/condor/localjob.condor
SCRIPT pre blockmain $deshome/libexec/logpre.pl ../config.des $block j \$JOB 
SCRIPT post blockmain $deshome/libexec/logpost.pl ../config.des $block j \$JOB \$RETURN  
EOF
    close DAG;
    addInfoToDag($config, $dag, $DebugFH);

    unlink "$dag";
    chdir "../uberctrl";
}


######################################################################
sub createPostIngestScript {
    my ($config) = @_;

    my $block = $config->value("block");
    my $rundir = $config->value("run_dir");
    my $archive_root = $config->value("archive_root");
    my $target_node = $config->value("target_node");
    my $bindir = $config->value("software_root"). "/libexec";
    my $ingestdir = $config->value('work_dir')."/runtime/${block}";

    if (! -d $ingestdir) {
        mkdir $ingestdir;
    }
    my $script = "$ingestdir/postingest.condor";
    my $exec = "$bindir/post_ingest.pl";
    my $args = "--archivenode $target_node --block $block --rundir $archive_root/$rundir";
    my $remote_initialdir = "$archive_root/$rundir/runtime";
    my $batchtype=$config->value('batch_type');
    my $gridtype=$config->value('grid_type');
    if ($gridtype !~ /condor/i) {
        $batchtype = 'fork';    # use fork jobmanager for GRAM jobs
    }
    my $newinfo = { name => 'postingest', exec=>$exec, args=>$args, remote_initialdir=>$remote_initialdir,
                   count => 1, jobtype=>'single', gridtype=>$gridtype, batchtype=>$batchtype, initialdir=>$ingestdir, log=>'condorjobs.log'};
    createScript($config, $script, $newinfo);
    addInfoToCondor($config, $script, $block, "postingest", \*STDOUT);
    return $script;
}


######################################################################
sub createJobScript {
    my ($config) = @_;

    my $block = $config->value("block");
    my $rundir = $config->value("run_dir");
    my $archive_root = $config->value("archive_root");
    my $workdir = $config->value("work_dir");
    my $softroot;
    if ($config->exists('software_root')) {
        $softroot = $config->value('software_root');
    }
    else {
        print "Error: software_root not defined in config.des\n";
        exit $FAILURE;
    }

    my $script = 'runjob.condor';
    my $exec = "$archive_root/$rundir/runtime/$block/run_elf.sh";
    my $args = "\$(args)";
    my $remote_initialdir = "$archive_root/$rundir/runtime/$block";
    my $newinfo = {name=>'runjob', exec=>$exec, args=>$args, 
#remote_initialdir=>$remote_initialdir, 
                   initialdir=>"../${block}_tjobs",
                   transfer_output_files=>"\$(jobid).pipeline.log",
                   output=>"\$(jobid).runjob.out", error=>"\$(jobid).runjob.err", 
                   count=>1, maxwalltime=>'$(wall)', jobtype=>'single'};

    createScript($config, $script, $newinfo);
    addInfoToCondor($config, $script, $block, '$(jobid)', \*STDOUT);
}


######################################################################
sub getGridInfo {
    my ($config) = @_;
    my $vals = {};

    foreach my $key ('stdout', 'stderr', 'queue', 'psn', 'job_type', 
                    'max_wall_time', 'max_time', 'max_cpu_time', 'max_memory', 'min_memory',
                    'host_types', 'host_xcount', 'xcount', 'count', 'host_count', 'reservation_id', 
                    'grid_resource', 'grid_type', 'grid_host', 'grid_port', 'batch_type', 'globus_extra',
                    'environment') {
        my $newkey = $key;
        $newkey =~ s/_//g;
        if ($config->exists($key)) {
            $vals->{$newkey} = $config->value($key);
        }
        elsif ($key =~ /_/) {
            if ($config->exists($newkey)) {
                $vals->{$newkey} = $config->value($newkey);
            }
        }
    }

print "\ngetGridInfo:  ",Dumper($vals),"\n";

    return $vals;
}


######################################################################
sub createScript {
    my ($config, $script, $newinfo) = @_;

print "createScript\n";
print Dumper($newinfo), "\n\n\n";

    my $name = $newinfo->{"name"};
    my $block = $config->value("block");
    my $run = $config->value("submit_run");
    my $submituser = $ENV{"USER"};
    my $rundir = $config->value("run_dir");
    my $archive_root = $config->value("archive_root");

    my $info = getGridInfo($config);
    $info = { %$info, %$newinfo };

    # default some values if not given
    if (!defined($info->{'log'})) {
        $info->{'log'} = "$name.log";
    }
    if (!defined($info->{'output'})) {
        $info->{'output'} = "$name.out";
    }
    if (!defined($info->{'error'})) {
        $info->{'error'} = "$name.err";
    }

    my $requirements = "((JobRunCount =?= Undefined) || (JobRunCount == 1 && JobStatus == 2))";

    open FILE, "> $script";

    if ($info->{'gridtype'} =~ /condor/) {
        # create a vanilla universe job script
        print FILE <<EOF;
universe = vanilla
getenv = TRUE
should_transfer_files = YES
WhenToTransferOutput = ON_EXIT
EOF
        if (defined($info->{'transfer_output_files'})) {
            my $tof = $info->{'transfer_output_files'};
            print FILE "transfer_output_files = $tof\n";
        }
    
        if ($config->exists('concurrency_limits')) {
            print FILE "concurrency_limits = ", $config->value('concurrency_limits'), "\n";
        }

        if (defined($info->{'environment'})) {
            my $env = createCondorEnv($info->{'environment'});
            print FILE "environment = $env\n";
        }

        ########## Custom condor submissions ##########

        if ($info->{'batchtype'} =~ /localcondor/i) {
            # *.cosmology machines do not have shared data filesystem.   Must submit to specific machine
            my $machine; 
            if ($config->exists('login_host')) {
                $machine = $config->value('login_host');
            }
            elsif ($config->exists('grid_host')) {
                $machine = $config->value('grid_host');
            }
            else {
                print "Error:  Cannot determine machine name (missing login_host and grid_host)\n";
                exit $FAILURE;
            }
            $requirements .= "&& (machine == \"$machine\")";
        }
        elsif ($info->{'batchtype'} =~ /dynslots/i) {
            # with dynamic slots, job tells condor how many cpus or how much memory it needs
	        if ($config->exists('request_memory')) {
	            print FILE "request_memory = ",$config->value('request_memory'),"\n";
	        }
	        if ($config->exists('request_cpus')) {
	            print FILE "request_cpus = ",$config->value('request_cpus'),"\n";
	        }
        }
    }
    else {
        # create a grid universe job script
        my $gridresource = createResource($info);
        print FILE <<EOF;
universe = grid
grid_resource = $gridresource
stream_output = False
stream_error  = False
EOF
        my ($batchinfostr, $batchinfotype) = createBatchInfo($info);
        if ($batchinfostr =~ /\S/) {
            print FILE "$batchinfotype = $batchinfostr\n";
        }
        if (defined($info->{'transfer_output_files'})) {
            my $tof = $info->{'transfer_output_files'};
            print FILE <<EOF;
should_transfer_files = YES
WhenToTransferOutput = ON_EXIT
transfer_output_files = $tof
EOF
        }
        else {
            print FILE <<EOF;
should_transfer_files = no
EOF
        }
    }


    ########## lines common across all universe types
    if (defined($info->{'args'})) {
        print FILE "arguments = $info->{'args'}\n";
    }

    if (defined($info->{'remote_initialdir'})) {
        print FILE "remote_initialdir = $info->{'remote_initialdir'}\n";
    }

    if (defined($info->{'initialdir'})) {
        print FILE "initialdir = $info->{'initialdir'}\n";
    }

    print FILE <<EOF;
executable = $info->{'exec'}
transfer_executable = false
log = $info->{'log'}
output = $info->{'output'}
error = $info->{'error'}
periodic_release = ((CurrentTime - EnteredCurrentStatus) > 1800) && (HoldReason =!= "via condor_hold (by user $submituser)")
Requirements = $requirements
periodic_remove = ((JobStatus == 1) && (JobRunCount =!= Undefined))
notification = never
queue
EOF
    close FILE;
}


######################################################################
sub runChecks {
   my $config = $_[0];
   my ($Out, $stat);

   # warn user if proxy expiring
   $Out = `which grid-proxy-info 2>&1`;
   $stat = $?;
   if ($stat == 0) {
      print "Checking for valid proxy....";
      $Out = `grid-proxy-info -timeleft 2>&1`;
      $stat = $?;
      if (($Out =~ /ERROR/) || ($stat != 0)) {
         print "ERROR\n";
         print "Proxy has expired or other problem with proxy.\n";
         print "(run grid-proxy-init or myproxy-logon)\n";
      }
      elsif ($Out < 3600*6) {
         my $hrsleft = $Out/3600;
         print "ERROR\n";
         printf("Proxy is expiring (< %0.1f hours left).  Run grid-proxy-init or myproxy-logon\n", $hrsleft);
      }
      elsif ($Out < 3600*12) {
         my $hrsleft = $Out/3600;
         print "WARNING\n";
         printf("Proxy is expiring (< %0.1f hours left).  Run grid-proxy-init or myproxy-logon\n", $hrsleft);
      }
      else {
         print "PASSED\n";
      }
   }



   ### Check for Condor in path as well as daemons running
   print "Checking for Condor....";
   $Out = `condor_version 2>&1`;
   if ($Out !~ /\$CondorVersion: (\d+.\d+)/) {
     print "ERROR\nCouldn't figure out Condor version\n";
     print "Make sure Condor binaries are in your path\n";
     exit $FAILURE;
   }

   $Out = `ps -ef | grep -i condor_master | grep -v grep 2>&1`;
   if ($Out !~ /condor_master/) {
     print "ERROR\nCondor is not running on this machine\n";
     print "Contact your condor administrator\n";
     exit $FAILURE;
   }
   else {
     print "PASSED\n";
   }
}


######################################################################
sub createRunElf {
    my $config = $_[0];
    my $workdir = $config->value('work_dir');
    my $block = $config->value('block');

    my $script = "$workdir/runtime/$block/run_elf.sh";

    open RUN, "> $script";
    print RUN <<EOF;
#!/bin/sh
echo "\$0 \$@"
EOF

    # set environment
    if ($config->exists('environment')) {
        my $env = $config->value('environment');
        if (ref($env) eq "HASH") {
            while (my ($k,$v)=each(%{$env})) {
                $k = uc($k);
                print RUN "export $k=$v\n";
            }
        }
        else {
            print RUN "export $env\n";
        }
    }

    my $deshome = $config->value('software_root');
    my $prereq = $config->value('prereq_root');

    my $elfdelay = 1;
    if ($config->exists('elf_delay')) {
        $elfdelay = $config->value('elf_delay');
    }
    
    print RUN <<EOF;
export DES_HOME=$deshome
export DES_PREREQ=$prereq

scratchdir=`pwd`
startdate=`date +%s`

if [ \$# -lt 5 ]; then
    echo "Usage: run_elf.sh <runtimeprefix> <xmlprefix> <targetid> <numjobs> <startjobid> <firstjobid> <dbid>";
    echo "Current args: \$@";
    exit 1;
fi
runtimeprefix=\$1
xmlprefix=\$2
tid=\$3
numjobs=\$4
startjobid="\$(echo \$5 | sed 's/0*//')"


echo "PIPELINE BATCHBEG \$startdate" 
echo "PIPELINE BATCHBEG \$startdate" >> \${scratchdir}/\${tid}.pipeline.log

### Output batch jobid for record keeping
### specific to batch scheduler
BATCHID=0
if test -n "\$PBS_JOBID"; then
   BATCHID=`echo \$PBS_JOBID | cut -d'.' -f1`
   NP=`awk 'END {print NR}' \$PBS_NODEFILE`
fi
if test -n "\$LSB_JOBID"; then
   BATCHID=\$LSB_JOBID
fi
if test -n "\$LOADL_STEP_ID"; then
   BATCHID=`echo \$LOADL_STEP_ID | /bin/awk -F "." '{ print \$(NF-1) "." \$(NF) }'`
fi
if test -n "\$CONDOR_ID"; then
   BATCHID=\$CONDOR_ID
fi
  
if test -n "\$HOSTNAME"; then
    HOSTNAME=`/bin/hostname`;
fi

echo "PIPELINE BATCHHOST \$HOSTNAME";
echo "PIPELINE BATCHHOST \$HOSTNAME" >> \${scratchdir}/\${tid}.pipeline.log 

echo "PIPELINE BATCHID \$BATCHID"
echo "PIPELINE BATCHID \$BATCHID" >> \${scratchdir}/\${tid}.pipeline.log 

echo ""
echo ""
echo "Checking proxy information"
echo "X509_USER_PROXY=" \$X509_USER_PROXY
echo "% grid-proxy-info"
grid-proxy-info
echo ""
echo ""

echo ""
echo ""
echo "Checking memory and swap"
echo "% free"
free
echo ""
echo ""


startelf ()
{
    jid=\$1
    tid=\$2

    if [ ! -d \${runtimeprefix}_\${jid} ]; then
        mkdir \${runtimeprefix}_\${jid}
    fi
    cd \${runtimeprefix}_\${jid}
    date
    (btime=`date +%s`; \\
     echo "PIPELINE runelf \${jid} BEG \${btime}" > pipeline_\${jid}.log; \\
     echo "PIPELINE runelf \${jid} BEG \${btime}"; \\
     (\${ELF_HOME}/elf -containerPath \${xmlprefix}_\${jid}.xml -bootstrapProperties \${xmlprefix}_\${jid}.bootstrap.properties 2>&1; \\
     myst=\$?; \\
     etime=`date +%s`; \\
     echo "PIPELINE runelf \${jid} STATUS \${myst}" >> pipeline_\${jid}.log; \\
     echo "PIPELINE runelf \${jid} STATUS \${myst}"; \\
     echo "PIPELINE runelf \${jid} END \${etime}" >> pipeline_\${jid}.log; \\
     echo "PIPELINE runelf \${jid} END \${etime}";) | tee elf_\${jid}.log; \\
     echo "elf done" > done_\${jid}.log; \\
     lockfile=\${runtimeprefix}/\${tid}_runelf.lock; \\
     let "tsl1=0"; while [ -r \$lockfile ] && [ \$tsl1 -lt 600 ] ; do let "sl1 = \$RANDOM % 10 +1"; sleep \$sl1; let "tsl1 = \$tsl1 + \$sl1"; done; \\
     touch \$lockfile; \\
     cat pipeline_\${jid}.log >> \${scratchdir}/\${tid}.pipeline.log; \\
     rm -f \$lockfile ) &
}


status=1
if ! test -x \${ELF_HOME}/elf; then
    echo "Error: Could not execute elf: \${ELF_HOME}/elf"
    status=1
else

    if [ \$startjobid == 'ingest' ]; then
        padid='ingest'
        startelf \$padid \$tid
    else
        if [ \$# -lt 6 ]; then
            echo "Usage: run_elf.sh <runtimeprefix> <xmlprefix> <targetid> <numjobs> <startjobid> <firstjobid> <dbid>";
            echo "Current args: \$@";
            exit 1;
        fi
        firstjobid="\$(echo \$6 | sed 's/0*//')"

        if [ \$startjobid -eq \$firstjobid ]; then
            if [ \$# -lt 7 ]; then
                echo "Usage: run_elf.sh <runtimeprefix> <xmlprefix> <targetid> <numjobs> <startjobid> <firstjobid> <dbid>";
                echo "Current args: \$@";
                exit 1;
            fi
            id="\$(echo \$7 | sed 's/0*//')"
            echo "Gathering software provenance information"
            echo "\${DES_HOME}/libexec/logversion.pl -system TARGET -des_home \${DES_HOME} -id \$id"
            \${DES_HOME}/libexec/logversion.pl -system TARGET -des_home \${DES_HOME} -id \$id
            echo "VERSTATUS \$?"
            echo ""
            echo ""
        fi

        myvar=0
        jobid=\$startjobid
        while [ \$myvar -ne \$numjobs ]
        do
            padid=`printf %04d \$jobid`
            startelf \$padid \$tid
            myvar=\$(( \$myvar + 1 ))
            jobid=\$(( \$jobid + 1 ))
            if [ \$myvar -ne \$numjobs ]; then
                sleep $elfdelay
            fi
        done
    fi
    wait
fi

enddate=`date +%s`
echo "PIPELINE BATCHEND \$enddate" 
echo "PIPELINE BATCHEND \$enddate" >> \${scratchdir}/\${tid}.pipeline.log 
echo "End of script"
EOF
    close(RUN);
    chmod 0744, $script;
}

sub checkSubmit {
    my ($config) = @_;

    my $hostname = `/bin/hostname -f`;
    chomp($hostname);

    my $submit_node = $config->value('submit_node');
    if (!$config->obj('archive')->exists($submit_node)) {
        print "Error:  Could not find archive information for submit node $submit_node\n";
        print "\nABORTING SUBMISSION\n";
        exit $FAILURE;
    }

    my $submit_obj = $config->obj('archive')->obj($submit_node);

    my $uber_site_obj = $config->obj('site');
    my %site_id2name = ();
    foreach my $site_name ($uber_site_obj->keys()) {
        my $site_obj = $uber_site_obj->obj($site_name);
        $site_id2name{$site_obj->value('site_id')} = $site_name;
    }
    $config->value('site_id2name', \%site_id2name);

    my $siteid = $submit_obj->value('site_id');
    if (!defined($site_id2name{$siteid})) {
        print "Error: Could not find site information for site $siteid from submit node info.\n";
        print "\nABORTING SUBMISSION\n";
        exit $FAILURE;
    }

    my $sitename = $site_id2name{$siteid};
    my $site_obj = $config->obj('site')->obj($sitename);
    if (!$site_obj->exists('login_host')) {
        print "Error:  login_host is not defined for site $sitename ($siteid).\n";
        print "\nABORTING SUBMISSION\n";
        exit $FAILURE;
    }
    my $site_hostname = $site_obj->value('login_host');
    if ($hostname ne $site_hostname) {
        print "Error:  submit node $submit_node ($site_hostname) doesn't match submit host ($hostname).\n";
        print "Debugging tips: Check submit_node value, check correct site_id defined for submit_node,\n";
        print "\tcheck login_host defined for site linked to submit_node\n";
        print "\nABORTING SUBMISSION\n";
        exit $FAILURE;
    }

    if ($submit_obj->exists('archive_root')) {
        my $archiveroot = $submit_obj->value('archive_root');
        if (! -d $archiveroot) {
            print "Error: archive_root ($archiveroot) doesn't exist\n";
            print "\nABORTING SUBMISSION\n";
            exit $FAILURE;
        }
    }
    else {
        print "Error: Problem finding archive_root for submit_node $submit_node\n";
        print "\nABORTING SUBMISSION\n";
        exit $FAILURE;
    }
}




1;
