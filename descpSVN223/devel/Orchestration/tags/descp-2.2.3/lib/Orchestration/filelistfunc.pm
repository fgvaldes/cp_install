#!/usr/bin/env perl
########################################################################
#  $Id: filelistfunc.pm 2770 2008-12-19 19:34:33Z mgower $
#
#  $Rev:: 2770                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit.
#  $LastChangedDate:: 2008-12-19 13:34:33 #$:  # Date of last commit.
#
#  Authors:
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
########################################################################

package Orchestration::filelistfunc;

use strict;
use warnings;

use Exporter   ();
our (@ISA, @EXPORT);
@ISA         = qw(Exporter);
@EXPORT      = qw(&getFileList &convertSingleFilesToLines &outputXMLList &printFile &makeFilesUniq &setQueryRun); 

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;
use Orchestration::misc;

my ($outputxml, $nite, $tilename, $project, $curr_run, $query_run) = undef;

###########################################################
sub getFileList {
    my ($query, $debug) = @_;
    $debug = 3 if !defined($debug);

    my $desDBI = DB::DESUtil->new('verbose'=>3, 'debug'=>$debug);
    my $location_table = $desDBI->LOCATION_TABLE()->{'table_name'};

    $query->{$location_table}->{'key_vals'}->{'archivesites'} = '[^N]';
    $query->{$location_table}->{'select_fields'} = 'all';
    $query->{$location_table}->{'hash_key'} = 'id';

    if ($debug > 0) {
        print "getFileList: calling queryDB2 with\n";
        print Dumper($query), "\n";
    }

    my $results_aref = $desDBI->queryDB2(%$query);
    $desDBI->disconnect();

    print "getFileList: number of files in list from queryDB2 = ", scalar(@$results_aref), "\n" if $debug > 0;
    print Dumper(@$results_aref), "\n" if $debug > 2;

    my $uniq_files = makeFilesUniq($results_aref, $curr_run, $debug);
    return $uniq_files;
}

###########################################################
sub convertSingleFilesToLines {
    my ($files) = @_;
    my %lines = ();
    my $count = 1;
    foreach my $fhref (values %$files) {
        my $fname = sprintf("file%05d", $count);
        $lines{$count}{$fname} = $fhref;
        $count++;
    }
    return \%lines;
}


###########################################################
sub outputXMLList {
    my ($outputxml, $lines) = @_;

    open FH, "> $outputxml" or die "Error: Could not write xml to file $outputxml";
    print FH "<list>\n";
    while (my ($k, $lhref) = each (%$lines)) {
        print FH "\t<line>\n";
        while (my ($name, $fhref) = each (%$lhref)) {
            print FH "\t\t<file nickname='$name'>\n";
            foreach my $key (keys %$fhref) {
                if (defined($fhref->{$key})) {
                    if (lc($key) eq 'ccd') {
                        print FH "\t\t\t<$key>", sprintf("%02d",$fhref->{$key}), "</$key>\n";
                    }
                    else {
                        print FH "\t\t\t<$key>", $fhref->{$key}, "</$key>\n";
                    }
                }
                else {
#                    print "Undefined key $key for file ", $fhref->{'id'}, "\n";
                }
            }
            print FH "\t\t\t<fileid>", $fhref->{'id'}, "</fileid>\n";
            print FH "\t\t</file>\n";
        }
        print FH "\t</line>\n";
    }

    print FH "</list>\n";
    close FH;
}

###########################################################
sub printFile {
    my $f = $_[0];

    foreach my $k (('filename','project','fileclass','filetype','nite','tilename','band','ccd','exposurename','run'))
{
        if (!defined($f->{$k})) {
            $f->{$k} = ' ';
        }
    }

    printf("%3s %5s %20s %8s %25s %s %s %2s %1s %s\n",  $f->{'project'}, $f->{'fileclass'}, $f->{'run'},
$f->{'filetype'},
                           $f->{'filename'}, $f->{'nite'}, $f->{'exposurename'}, $f->{'ccd'}, $f->{'band'},
$f->{'tilename'});

}

###########################################################
## make results unique on filename
sub makeFilesUniq {
    my ($results_aref, $curr_run, $debug) = @_;

    $debug = 0 if !defined($debug);
    
    my ($curr_date, $curr_rest) = undef;
    if (defined($curr_run)) {
        ($curr_date, $curr_rest) = split /_/, $curr_run;
    }

    #print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n";
    # Initialize saved set of files to empty
    my %uniq_files = ();
    foreach my $f (@$results_aref) {
        if (!defined($f->{'filename'}) || !defined($f->{'filetype'}) ||
            (!defined($f->{'run'}) && ($f->{'filetype'} ne 'src'))) {

            printf STDERR "ERROR:  File from DB missing filename, filetype, or run\n";
            printf STDERR Dumper($f), "\n";
            exit(1);
        }

        my $file_name = $f->{'filename'};
        my $uniq_str = '';
        foreach my $col (('filename','project','fileclass','filetype','nite','tilename','band','ccd','exposurename')) {
            if (defined($f->{$col})) {
                $uniq_str .= $f->{$col}.'_';
            }
            else {
                $uniq_str .= '_';
            }
        }

        if (!exists($uniq_files{$uniq_str})) {
            $uniq_files{$uniq_str} = $f;
        }
        else {
#           print "Have file match: $uniq_str\n"; 
            my $file_run = $f->{'run'};
            my ($file_date,$rest) = split /_/, $file_run;
#print "curr_run = $curr_run\nfile_run = $file_run\n";
            if (!defined($curr_date) || ($file_date le $curr_date)) {
                my ($uniq_date,$rest2) = split /_/, $uniq_files{$uniq_str}->{'run'};
                if ($file_date gt $uniq_date) {
                    printf "\tSkipping %s from run %s\n", $uniq_str, $uniq_files{$uniq_str}->{'run'} if $debug > 0;
                    $uniq_files{$uniq_str} = $f;
                }
                else {
                    printf "\tSkipping %s from run %s\n", $uniq_str, $f->{'run'} if $debug > 0;
                }
            }
        }
    }
#   print Dumper(%uniq_files), "\n";
    return \%uniq_files;
}

sub setQueryRun {
    my ($query_run, $block_num, $curr_run, $key_vals) = @_;

    if (defined($query_run)) {
        if (lc($query_run) eq "current") {
            if (defined($curr_run)) {
                $key_vals->{'run'} = $curr_run;
            }
            else {
                print "Error: Must specify curr_run if query_run == current\n";
                exit $FAILURE;
            }
        }
        elsif (lc($query_run) eq "allbutfirst")  {
            if (defined($block_num) && ($block_num > 0)) {
                if (defined($curr_run)) {
                    $key_vals->{'run'} = $curr_run;
                }
                else {
                    print "Error: Must specify curr_run if query_run == allbutfirst\n";
                    exit $FAILURE;
                }
            }
            else {
                print "Error: Must specify block_num if query_run == allbutfirst\n";
                exit $FAILURE;
            }
        }
    }
}


1;
