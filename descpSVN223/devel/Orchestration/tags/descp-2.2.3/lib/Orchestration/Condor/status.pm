########################################################################
#  $Id: status.pm 2995 2009-01-28 17:44:33Z mgower $
#
#  $Rev:: 2995                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2009-01-28 10:44:33 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package Orchestration::Condor::status;

use Switch;

use Exporter   ();
our (@ISA, @EXPORT);

@ISA         = qw(Exporter);
@EXPORT      = qw(&parseCondorUserLog &parseCondorQOutput);

sub parseCondorUserLog
{
  local *FH = shift;
  my $line;
  %jobs = ();

  $/ = "\n...";
  while ($line = <FH>)
  {
     $line =~ s/^\n//;
     if ($line =~ /\S/)
     {
        @line = split /\n/, $line;
        ($code, $jobnum, $time, $desc) = $line[0] =~ m/(\d+)\s+\((\d+).\d+.\d+\)\s+(\d+\/\d+\s+\d+:\d+:\d+)\s+(.+)/;

        $jobnum =~ s/^0+//;

        switch ($code)
        {
           case "000"  {
              $jobs{"$jobnum"}{"jobid"} = $jobnum;
              $jobs{"$jobnum"}{"clusterid"} = $jobnum;
              ($jobs{"$jobnum"}{"dagnode"}) = $line[1] =~ m/DAG Node: (\S+)/;
              $jobs{"$jobnum"}{"machine"} = "";
              $jobs{"$jobnum"}{"jobstat"} = "UNSUB";
              $jobs{"$jobnum"}{"submittime"} = $time;
           }
           case "001"  { # Job executing on host:
              $jobs{"$jobnum"}{"jobstat"} = "RUN";
              $jobs{"$jobnum"}{"starttime"} = $time;
           }
           #case "002"  { } # Error in executable
           #case "003"  { } # Job was checkpointed
           #case "004"  { } # Job evicted from machine
           case "005"  {
              $jobs{"$jobnum"}{"jobstat"} = "DONE";
              ($jobs{"$jobnum"}{"retval"}) = $line[1] =~ m/return value (\d+)/;
              $jobs{"$jobnum"}{"endtime"} = $time;
           }
           #case "006"  { } # Image size of job updated
           #case "007"  { } # Shadow threw an exception
           #case "008"  { } # Generic Log Event
           case "009"
           {
              $jobs{"$jobnum"}{"jobstat"} = "FAIL";
              $jobs{"$jobnum"}{"endtime"} = $time;
           }
           #case "010"  { } # Job was suspended
           #case "011"  { } # Job was unsuspended
           case "012"
           {
              $jobs{"$jobnum"}{"jobstat"} = "ERR";
              $jobs{"$jobnum"}{"holdreason"} = $line[1];
              $jobs{"$jobnum"}{"holdreason"} =~ s/^\s+//;
           }
           case "013"  { $jobs{"$jobnum"}{"jobstat"} = "UNSUB";}
           #case "014"  { } # Parallel Node executed
           #case "015"  { } # Parallel Node terminated

           case "016"
           {
         #016 (471.000.000) 04/11 11:48:08 POST Script terminated.
         #        (1) Normal termination (return value 100)
         #    DAG Node: fail
         #...
              $jobs{"$jobnum"}{"endtime"} = $time;
              ($retval) = $line[1] =~ m/return value\s+(\d+)/;
              if ($retval == 100)
              {
                $jobs{"$jobnum"}{"jobstat"} = "FAIL";
              }
              else
              {
                $jobs{"$jobnum"}{"jobstat"} = "DONE";
              }
           }
           case "017"  { #  Job submitted to Globus
              #  Beware of out of order log entries
              if (!exists($jobs{"$jobnum"}{"starttime"}) || ($jobs{"$jobnum"}{"starttime"} ne $time))
              {
                 $jobs{"$jobnum"}{"jobstat"} = "PEND";
              }
              ($jobs{"$jobnum"}{"gridresource"}) =
                                     $line[1] =~ m/RM-Contact:\s+(\S+)/;
           }
           #case "018"  { } # Globus Submit failed
           #case "019"  { } # Globus Resource Up
           #case "020"  { } # Globus Resource Down
           #case "021"  { } # Remote Error
           case "027"  { } # Job submitted to grid resource, same info as case 017
           else { $jobs{"$jobnum"}{"jobstat"} = "U$code"; }
        } # end switch
     }
  } # end reading file
  $/ = "\n";
  return \%jobs;
}

# parse condor_q output into hash table qjobs
sub parseCondorQOutput
{
  my $condorq_out = $_[0];

  $condorq_out =~ s/^\s+(\S)/$1/g;     # removing leading blank lines
  $condorq_out =~ s/^--[^\n]+\n//g;    # remove line starting with --

  @lines = split /\n/, $condorq_out;
  foreach $line (@lines)
  {
  #  print "$line\n";
    if ($line !~ /\S/)
    {
      # blank lines separate jobs
      foreach $k (keys %job)
      {
        $qjobs{"$id"}{"$k"}=$job{"$k"};
      }
      undef %job;
    }
    else
    {
      ($left,$right) = split / = /, $line;
      $left =~ tr/A-Z/a-z/; # convert key to all lowercase
      $right =~ s/"//g;
      if (($left =~ /args/) && ($right =~ /^-f/))
      {
        $left =~ "condorargs";
      }
      $job{"$left"} = $right;
      if ($left =~ /^clusterid$/)
      {
        $id = $right;
      }
    }
  }
  # don't forget to save the last job into big hash table
  foreach $k (keys %job)
  {
    $qjobs{"$id"}{"$k"}=$job{"$k"};
  }
  undef %job;
  return %qjobs;
}

1;
