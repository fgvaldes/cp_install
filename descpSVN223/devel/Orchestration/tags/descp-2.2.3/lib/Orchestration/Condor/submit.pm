########################################################################
#  $Id: submit.pm 6584 2011-03-17 18:13:24Z mgower $
#
#  $Rev:: 6584                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-03-17 11:13:24 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package Orchestration::Condor::submit;

use Carp;
use Data::Dumper;
use Exporter   ();
our (@ISA, @EXPORT);

@ISA         = qw(Exporter);
@EXPORT      = qw(&createResource &createRSL &createXML &createBatchInfo &createCondorEnv &createGlideinSubmitScript);


sub createResource {
    my $args = @_[0];
    my $gridresource = "";

print "\ncreateResource: ", Dumper($args), "\n";

    if (exists($args->{"gridresource"})) {
        $gridresource .= $args->{"gridresource"};
    }
    else {
        # check for needed info
        if (exists($args->{"gridtype"})) {
            $gridtype = lc($args->{"gridtype"});
            if (($gridtype ne "prews") && ($gridtype ne "ws") && ($gridtype ne "gt2") && ($gridtype ne "gt4") && ($gridtype ne "gt5")) {
                croak "Invalid gridtype: '$gridtype'";
            }
        }
        else {
            $gridtype = "prews";
        }

        if (exists($args->{'gridhost'})) {
            $gridhost = $args->{'gridhost'};
        }
        else {
            croak "Missing gridhost";
        }

        if (exists($args->{'gridport'})) {
            $gridport = $args->{'gridport'};
        }
        else {
            $gridport = "";
        }

        if (exists($args->{'batchtype'})) {
            $batchtype = $args->{'batchtype'};
        }
        else {
            croak "Missing batchtype";
        }

        # create gridresource string
        if (($gridtype eq "prews") || ($gridtype eq "gt2")) {
            $gridresource = "gt2 ".$gridhost;
            if ($gridport =~ /\d/) {
                $gridresource .= ":".$gridport;
            }
            $gridresource .= "/jobmanager-".lc($batchtype);
        }
        elsif ($gridtype eq "gt5") {
            $gridresource = "gt5 ".$gridhost;
            if ($gridport =~ /\d/) {
                $gridresource .= ":".$gridport;
            }
            $gridresource .= "/jobmanager-".lc($batchtype);
        }
        else {
            $gridresource = "gt4 https://".$gridhost;
            if ($port =~ /\d/) {
                $gridresource .= $gridport;
            }   
	        if (lc($batchtype) eq "fork"){
		        $gridresource .= "/wsrf/services/ManagedJobFactoryService Fork";
	        }
	        else {
		        $gridresource .= "/wsrf/services/ManagedJobFactoryService ".uc($batchtype);
	        }
        }
    }
    return $gridresource;
}

######################################################################
sub createBatchInfo {
    my ($args) = @_;
    my $str = "";
    my $type = "";

    if (exists($args->{"gridtype"})) {
        if (lc($args->{"gridtype"}) eq 'prews' || 
            lc($args->{"gridtype"}) eq 'gt2' ||
            lc($args->{"gridtype"}) eq 'gt5' ) {
            $type = 'globusrsl';
            $str = createRSL($args);
        }
        else {
            $type = 'globusxml';
            $str = createXML($args);
        }
    }

    return ($str, $type);
}

sub createRSL {
    my $args = @_[0];
    my $rsl = "";

    my $fork = (exists($args->{'batchtype'}) && (lc($args->{'batchtype'}) eq 'fork'));

    if (exists($args->{'stdout'})) {
        $rsl .= "(stdout=".$args->{'stdout'}.")";
    }
    if (exists($args->{'stderr'})) {
        $rsl .= "(stderr=".$args->{'stderr'}.")";
    }
    if (!$fork && exists($args->{'maxwalltime'})) {
        $rsl .= "(maxwalltime=".$args->{'maxwalltime'}.")";
    }
    if (!$fork && exists($args->{'maxtime'})) {
        $rsl .= "(maxtime=".$args->{'maxtime'}.")";
    }
    if (!$fork && exists($args->{'queue'})) {
        $rsl .= "(queue=".$args->{'queue'}.")";
    }
    if (!$fork && exists($args->{'psn'})) {
        $rsl .= "(project=".$args->{'psn'}.")";
    }
    if (!$fork && exists($args->{'jobtype'})) {
        $rsl .= "(jobtype=".$args->{'jobtype'}.")";
    }
    if (!$fork && exists($args->{'maxmemory'})) {
        $rsl .= "(maxmemory=".$args->{'maxmemory'}.")";
    }
    if (!$fork && exists($args->{'minmemory'})) {
        $rsl .= "(minmemory=".$args->{'minmemory'}.")";
    }
    if (!$fork && exists($args->{'hostxcount'})) {
        $rsl .= "(hostxcount=".$args->{'hostxcount'}.")";
    }
    if (!$fork && exists($args->{'xcount'})) {
        $rsl .= "(xcount=".$args->{'xcount'}.")";
    }
    if (!$fork && exists($args->{'hosttypes'})) {
        $rsl .= "(hosttypes=".$args->{'hosttypes'}.")";
    }
    if (!$fork && exists($args->{'count'})) {
        $rsl .= "(count=".$args->{'count'}.")";
    }
    if (!$fork && exists($args->{'reservationid'})) {
        $rsl .= "(reservationid=".$args->{'reservationid'}.")";
    }
    if (!$fork && exists($args->{'globusextra'})) {
        $rsl .= $args->{'globusextra'};
    }
    if (exists($args->{'environment'})) {
        my $env = "";
        if (ref($args->{'environment'}) eq "HASH") {
            while (my ($k,$v)=each(%{$args->{'environment'}})) {
                $k = uc($k);
                $env .= "($k $v)";
            }
        }
        else {
            $env = $args->{'environment'};
        }
        $rsl .= "(environment=".$env.")";
    }

    return $rsl;
}


# Note required order for xml
sub createXML {
    my $args = @_[0];
    my $xml = "";

    if (exists($args{'stdout'})) {
        $xml .= "(stdout=".$args{'stdout'}.")";
    }
    if (exists($args{'stderr'})) {
        $xml .= "(stderr=".$args{'stderr'}.")";
    }
    if (exists($args{'count'})) {
        $xml .= "(count=".$args{'count'}.")";
    }
    if (exists($args{'hostcount'})) {
        $xml .= "(hostcount=".$args{'hostcount'}.")";
    }
    if (exists($args{'psn'})) {
        $xml .= "<project>".$args{'psn'}."</project>";
    }
    if (exists($args{'queue'})) {
        $xml .= "<queue>".$args{'queue'}."</queue>";
    }
    if (exists($args{'maxtime'})) {
        $xml .= "<maxTime>".$args{'maxtime'}."</maxTime>";
    }
    if (exists($args{'maxwalltime'})) {
        $xml .= "<maxWallTime>".$args{'maxwalltime'}."</maxWallTime>";
    }
    if (exists($args{'maxcputime'})) {
        $xml .= "<maxCpuTime>".$args{'maxCputime'}."</maxCpuTime>";
    }
    if (exists($args{'maxmemory'})) {
        $xml .= "<maxMemory>".$args{'maxmemory'}."</maxMemory>";
    }
    if (exists($args{'minmemory'})) {
        $xml .= "<minMemory>".$args{'minmemory'}."</minMemory>";
    }
    if (exists($args{'jobtype'})) {
        $xml .= "<jobtype>".$args{'jobtype'}."</jobtype>";
    }
}


###########################################################
sub createCondorEnv {
    my ($envvars) = @_;
    my $env = "";
    if (ref($envvars) eq "HASH") {
        $env = '"';
        while (my ($k,$v)=each(%{$envvars})) {
            $k = uc($k);
            if ($v =~ /'/) {
                $v = "'$v'";
            }
            elsif ($v =~ /"/) {
                $v = "\"$v\"";
            }
            $env .= "$k=$v ";
        }
        $env .= '"';
    }
    else {
        $env = "\"$envvars\"";
    }
    return $env;
}

sub createGlideinSubmitScript {
   my ($args) = @_;
  
   my $file = $args->{"file"};
   my $psn = $args->{"psn"};
   my $queue = $args->{"queue"};
   my $useppn = $args->{"useppn"};
   my $totalncpus = $args->{"totalncpus"};
   my $condorconfig = $args->{"condorconfig"};
   my $glideinexe = $args->{"glideinexe"};
   my $sbinpath = $args->{"sbinpath"};
   my $localdir = $args->{"localdir"};
   my $minutes = $args->{"minutes"};
   my $idle = $args->{"idle"};
   my $jobmanager = $args->{"jobmanager"};
 
   my $hostname = `hostname -f 2>&1`;
   chomp $hostname;
 
   # use IP address instead of name to avoid rare problem where compute node
   # can't look up the IP address 
   $out = `host $hostname 2>&1`;
   my ($hostip) = $out =~ m/has address ([\d\.]+)/;
 
   # restrict this glidein to only run jobs submitted by same user
   # otherwise, sharing access to remote machine similar to sharing password
   my $owner = $ENV{"USER"};
 
   my $rsl = "";

   # if using machine where ppn is applicable
   if (($useppn != 0) && ($totalncpus != 1))
   {
      $hostxcount = ceil($totalncpus/$useppn);
      $rsl = "(hostxcount = $hostxcount)(xcount = 1)";
      if ($hostxcount > 1)
      {
         $rsl .= "(jobtype=multiple)";
      }
      else
      {
         $rsl .= "(jobtype=single)";
      }
   }
   else
   {
      $rsl = "(count = $totalncpus)(jobtype=single)";
   }
 
   if ($queue =~ /\S/)
   {
      $rsl .= "(queue=$queue)";
   }
   if ($psn =~ /\S/)
   {
      $rsl .= "(project=$psn)";
   }
   if ($minutes =~ /\d/)
   {
     $rsl .= "(maxwalltime=$minutes)";
   }

   # Need local version of condor.  Will use it to use matching version glidein executables
   $out = `condor_version 2>&1`;
   if ($out =~ /CondorVersion: (\d+)\.(\d+)\.(\d+)/)
   {
      $version = sprintf("%d.%d.%d", $1, $2, $3);
   }
   print "Local Condor version = '$version'\n";
 
#   # Assumes softline/renaming of full arch name directory on remote machine that is default
#   $binpath = $args->{"binpath"};
#   $sbinpath = $binpath."/".$version;
 
   # Have daemons shut down gracefully 1 minute before hit wallclock limit
   $runmin = $minutes - 1;
 
   $environment = "CONDOR_CONFIG=$condor_config;_condor_CONDOR_HOST=$hostip;_condor_GLIDEIN_HOST=$hostip;_condor_LOCAL_DIR=$localdir;_condor_NUM_CPUS=$useppn;_condor_SBIN=$sbinpath;_condor_START_owner=$owner;_condor_DaemonStopTime=DaemonStartTime+".($runmin*60).";_condor_STARTD_NOCLAIM_SHUTDOWN=$idle";
 
   open FILE, "> $filename";
   print FILE <<EOF;
universe = grid
grid_type = gt2
globusscheduler = $jobmanager
globusrsl = $rsl
executable = $glideinexe
arguments = -dyn -f -r $runmin
transfer_executable = false
environment = $environment
notification = error
EOF
 
  if (defined($debug))
  {
    print FILE "output = $filename.\$(Cluster).out\n";
    print FILE "error = $filename.\$(Cluster).err\n";
    print FILE "log = $filename.log\n";
  }
 
  print FILE "queue\n";
  close FILE;
}


1;
