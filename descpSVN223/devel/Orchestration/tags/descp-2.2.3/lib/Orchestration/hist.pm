########################################################################
#  $Id: hist.pm 6584 2011-03-17 18:13:24Z mgower $
#
#  $Rev:: 6584                                        $:  # Revision of last commit.
#  $LastChangedBy:: mgower                         $:  # Author of last commit. 
#  $LastChangedDate:: 2011-03-17 11:13:24 -0700 (#$:  # Date of last commit.
#
#  Authors: 
#            Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package Orchestration::hist;

use strict;
use warnings;

use Data::Dumper;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::desconfig;
use Orchestration::provenance;
use DB::DESUtil;
use DB::Util;

use Exporter    ();
our (@ISA, @EXPORT);
@ISA     = qw(Exporter);
@EXPORT  = qw(&logEvent &parseHistLog &makeKeyStr &logOrchEvent
              &insertRun &updateRunStart &updateRunStatus
              &insertBlock &updateBlockStart &updateBlockMid &updateBlockEnd &updateOrchTaskList
              &insertOrchTask &updateOrchTaskSubmit &updateOrchTaskStart &updateOrchTaskEnd 
              &insertJobs &updateVal &connectProcDB
             );


sub logEvent {
    my $Config = shift;
    my $Block = shift; 
    my $SubBlock = shift;
    my $SubBlockType = shift;

    if (defined($Block)) {
        $Block =~ s/"//g;
    }
    else {
        $Block = '';
    }
    if (defined($SubBlockType)) {
        $SubBlockType =~ s/"//g;
    }
    else {
        $SubBlockType = '';
    }
    if (defined($SubBlock)) {
        $SubBlock =~ s/"//g;
    }
    else {
        $SubBlock= '';
    }
  
    my $RunSite = $Config->value('run_site');
    my $Run = $Config->value('submit_run');

    my $LogDir = $Config->value('uberctrl_dir');

    my $DagID = 0;
    if (defined($ENV{'CONDOR_ID'})) {
        $DagID = $ENV{'CONDOR_ID'};
    }
    $DagID = sprintf("%d", $DagID);

    open LOG, ">> $LogDir/$Run.deslog";
    print LOG getTimeStamp(), ",$DagID,$Run,$RunSite,$Block,$SubBlockType,$SubBlock";

    # print rest of arguments to log
    for (my $i = 0; $i < scalar(@_); $i++) {
        print LOG ",",$_[$i];
    }
    print LOG "\n";
    close LOG;
}

##==============================================================================
##==============================================================================
sub parseHistLog {
    my $Run = shift;
    my $Iwd = shift;
    my $BlockInfoArrRef = shift;

    my ($Date, $DagID, $RunSite, $Block, $SubBlockType, $SubBlock, $Type);
    my (%BlockNum, $InfoNum, $NextInfoNum);

    my ($dir1,$dir2) = split /\Q$Run\E/, $Iwd;

    $NextInfoNum = 0;

    # read .deslog
    my $DesLog = "$dir1/$Run/log/$Run.deslog";
    my $OpenSuccess = open DESLOG, "< $DesLog";
    if (!$OpenSuccess) {
        print STDERR "Could not open $DesLog for reading\n";
    }
    while (my $Line = <DESLOG>) {
        chomp($Line);
        $Line =~ s/,,/, ,/g;
        my @Info = split /,/, $Line;
        $Date = $Info[0];
        $DagID = $Info[1];
        $Run = $Info[2];
        $RunSite = $Info[3];
        $Block = $Info[4];
        $Block =~ s/mngr//; # for backwards compatibility

        if (($Block !~ /submit/i) && ($Block !~ /complete/i) && ($Block !~ /restart/i)) {
            $SubBlockType = $Info[5];
            $SubBlock = $Info[6];
            $Type = $Info[7];

            my $KeyStr = makeKeyStr("", $Run, $RunSite, $Block, $SubBlock, $SubBlockType);
            if ($Type =~ /pretask/) {
                $BlockNum{$KeyStr} = $NextInfoNum;
                $InfoNum = $NextInfoNum;

                $$BlockInfoArrRef[$InfoNum]{"starttime"} = $Date;
                $$BlockInfoArrRef[$InfoNum]{"subblocktype"} = $SubBlockType;
                $$BlockInfoArrRef[$InfoNum]{"subblock"} = $SubBlock;
                $$BlockInfoArrRef[$InfoNum]{"run"} = $Run;
                $$BlockInfoArrRef[$InfoNum]{"runsite"} = $RunSite;
                $$BlockInfoArrRef[$InfoNum]{"block"} = $Block;
                $$BlockInfoArrRef[$InfoNum]{"parent"} = $DagID;
                $$BlockInfoArrRef[$InfoNum]{"jobstat"} = "PRE";
                $$BlockInfoArrRef[$InfoNum]{"clusterid"} = "";
                $$BlockInfoArrRef[$InfoNum]{"jobid"} = "";
                $$BlockInfoArrRef[$InfoNum]{"exitval"} = $FAILURE;
                $$BlockInfoArrRef[$InfoNum]{"endtime"} = "";

                $NextInfoNum++;
            }
            elsif ($Type =~ /cid/) {
                if (defined($BlockNum{$KeyStr})) {
                    $InfoNum = $BlockNum{$KeyStr};
                    $$BlockInfoArrRef[$InfoNum]{"clusterid"} = $Info[8];
                }
                else {
                    print "Error: Could not find matching job $KeyStr for cid line\n'$Line'\n";
                    exit $FAILURE;
                }
            }
            elsif ($Type =~ /jobid/) {
                if (defined($BlockNum{$KeyStr})) {
                    $InfoNum = $BlockNum{$KeyStr};
                    $$BlockInfoArrRef[$InfoNum]{"jobid"} = $Info[8];
                }
                else {
                    print "Error: Could not find matching job $KeyStr for jobid line\n'$Line'\n";
                    exit $FAILURE;
                }
            }
            elsif ($Type =~ /posttask/) {
                if (defined($BlockNum{$KeyStr})) {
                    $InfoNum = $BlockNum{$KeyStr};
                    $$BlockInfoArrRef[$InfoNum]{"exitval"} = $Info[8];
                    $$BlockInfoArrRef[$InfoNum]{"endtime"} = $Date;
                }
                else {
                    print "Error: Could not find matching job $KeyStr for posttask line\n";
                    exit $FAILURE;
                }

            }
        }
    }
    close DESLOG;
}


#==============================================================================
#==============================================================================
sub makeKeyStr {
    my $Num = shift;
    my $Run = shift;
    my $RunSite = shift;
    my $Block = shift;
    my $SubBlock = shift;
    my $SubBlockType = shift;

    my $Key = "";

    if (defined($Num)) {
        $Key .= $Num;
    }
    $Key .= "__";
    if (defined($Run)) {
        $Key .= $Run;
    }
    $Key .= "__";
    if (defined($RunSite)) {
        $Key .= $RunSite;
    }
    $Key .= "__";
    if (defined($Block)) {
        $Key .= $Block;
    }
    $Key .= "__";
    if (defined($SubBlock)) {
        $Key .= $SubBlock;
    }
    $Key .= "__";
    if (defined($SubBlockType)) {
        $Key .= $SubBlockType;
    }

    return $Key;
}

##########################################################################
# Create a DB entry to log each submission
##########################################################################
sub insertRun {
    my ($config) = @_;

    my ($all_config_text, $submit_config_text, $field_str, $value_str);

    # Open top-level config file and store contents to a string:
    my $config_filename = $config->config_filename();
    open(FH,"<$config_filename") or die "Unable to open $config_filename";
    $submit_config_text = '';
    while (my $str = <FH>) {
        $submit_config_text .= $str;
    }
    close FH;

    # Create hash with the key->values that comprise the DB row:
    my %Row;

    $Row{'run_submit'} = epoch2str($config->value('submit_epoch'));
    if ($config->exists('project')) {
        $Row{'project'} = $config->value('project');
    }
    else {
        print STDERR "Error: Could not find project\n";
        exit $FAILURE;
    }
    if ($config->exists('submit_run')) {
        $Row{'run'} = $config->value('submit_run');
    }
    else {
        print STDERR "Error: Could not find run\n";
        exit $FAILURE;
    }
    if ($config->exists('operator')) {
        $Row{'operator'} = $config->value('operator');
    }
    else {
        print STDERR "Error: Could not find operator\n";
        exit $FAILURE;
    }

    $Row{'fileclass'} = $config->value('fileclass');
    if ($config->exists('target_node')) {
        $Row{'archive_node'} = $config->value('target_node');
    }
    $Row{'event_tag'} = $config->value('event_tag');

    $Row{'submit_node'} = $config->value('submit_node');
    $Row{'submit_host'} = $config->value('submit_host');
    $Row{'submit_dir'} = $config->value('submit_dir');
    $Row{'block_list'} = $config->value('block_list');

    my $runcomment = "";
    if ($config->exists('run_comment')) {
        $runcomment = $config->value('run_comment');
        if (length($runcomment) > 255) {
            print "WARNING: truncating run_comment to first 255 characters\n";
            $runcomment = substr($runcomment, 0, 255);
        }
    }
    $Row{'run_comment'} = $runcomment;

    my $notes = "";
    if ($config->exists('notes')) {
        $notes = $config->value('notes');
        if (length($notes) > 1000) {
            print "WARNING: truncating notes to first 1000 characters\n";
            $notes = substr($notes, 0, 1000);
        }
    }
    $Row{'notes'} = $notes;

    $Row{'submit_config_text'} = $submit_config_text;
    $Row{'all_config_text'} = $config->save_string();


    my $dbh = new DB::DESUtil(verbose=>0,debug=>0);

    # Check that run is unique
    my $dbq = "SELECT ID FROM RUN WHERE RUN='".$Row{'run'}."' and PROJECT='".$Row{'project'}."'";
    my $runrows = $dbh->selectall_hashref($dbq, 'id');
    if (scalar keys %$runrows != 0) {
        print "run value (", $Row{'project'}, " ", $Row{'run'}, ") is not unique\n";
        print "ABORTING\n";
        exit $FAILURE;
    }

    ## get next id and save it
    $field_str = 'id';
    $value_str = $dbh->getNextSequenceID('RUN_SEQ');
    $config->value('dbid_run', $value_str);

    # Create SQL statement with placeholders:
    my @fields;
    foreach my $key (keys %Row) {
        if ($Row{"$key"}) {
            push(@fields,$key);
            $field_str = join(',',$field_str,$key);
            $value_str = join(',',$value_str,'?');
        }
    }
    my $dbi = "INSERT INTO RUN ($field_str) VALUES ($value_str)";

    # Prepare SQL, bind values and execute the insert:
    my $sth = $dbh->prepare($dbi);
    for (my $i=1; $i<=scalar @fields; $i++) {
        $sth->bind_param($i,$Row{"$fields[$i-1]"});
    }
    $sth->execute();
    $sth->finish();

    # insert each block
    my $blocklist = $config->value('block_list');
    my @blocks = split /\s*,\s*/, $blocklist;
    foreach my $bl (@blocks) {
        insertBlock($config, $dbh, $bl);
    }

    $dbh->commit();
    $dbh->disconnect();
} # end insertRun


###########################################################
sub insertBlock {
    my ($config, $dbh, $block) = @_;

    my $dbid_run = $config->value('dbid_run');

    # Check that run is unique
    my $dbq = "SELECT BLOCK_ID FROM BLOCK WHERE RUN_ID='$dbid_run' and BLOCK_NAME='$block'";
    my $runrows = $dbh->selectall_hashref($dbq, 'block_id');
    if (scalar keys %$runrows != 0) {
        print "block_name value ($block, $dbid_run) is not unique\n";
        print "ABORTING\n";
        exit $FAILURE;
    }

    my $submit_str = epoch2str($config->value('submit_epoch'));

    my $dbid_block = $dbh->getNextSequenceID('BLOCK_SEQ');
    my $block_obj = $config->obj('block')->obj($block);
    $block_obj->value('dbid_block', $dbid_block);

    my %Row;
    $Row{'block_name'} = "'".$block."'";
    $Row{'name'} = "'".$block."'";
    $Row{'block_submit'} = "'".$submit_str."'";
    $Row{'module_list'} = "'".$block_obj->value('module_list')."'";
    $Row{'run_id'} = $dbid_run;
    $Row{'id'} = $dbid_block;

    # Create SQL statement 
    my $field_str = "block_id";
    my $value_str = $dbid_block;
    foreach my $key (keys %Row) {
        if ($Row{$key}) {
            $field_str .= ",$key";
            $value_str .= ",".$Row{$key};
        }
    }
    my $dbi = "INSERT INTO BLOCK ($field_str) VALUES ($value_str)";

    eval {
        my $sth = $dbh->prepare($dbi);
        $sth->execute(); 
        $sth->finish();
    };

    if ($@) {
        print "$dbi\n";
        die($@);
    }
} # end insertBlock


sub updateBlockStart {
    my ($config) = @_;

    print "updateBlockStart: BEG\n";

    my $blocknum = $config->value('block_num');
    if ($blocknum == 0) {
        updateRunStart($config);
    }

    my $block = $config->value('block');
    my $dbid_block = $config->value('dbid_block');
    my $deshome = $config->value('des_home');

    my %sethash = ( block_start=>time,
                    start_time=>time);
    updateVal($config, 'block', $dbid_block, \%sethash);


    %sethash = ( active_block_name => $block,
                 active_block_id => $dbid_block
               );
    my $dbid_run = $config->value('dbid_run');
    updateVal($config, 'run', $dbid_run, \%sethash);

    print "updateBlockStart: END\n";
} # end updateBlockStart


sub updateBlockMid {
    my ($config) = @_;

    print "updateBlockMid: BEG\n";
    my $block = $config->value('block');
    my $dbid_block = $config->value('dbid_block');

    my %sethash;
    if ($config->exists('module_list')) {
        $sethash{'module_list'}=$config->value('module_list');
    }
    if ($config->exists('target_node')) {
        $sethash{'target_node'}=$config->value('target_node');
    }
    if ($config->exists('start_jobid')) {
        $sethash{'start_jobid'}=$config->value('start_jobid');
    }
    if ($config->exists('end_jobid')) {
        $sethash{'end_jobid'}=$config->value('end_jobid');
    }
    
    updateVal($config, 'block', $dbid_block, \%sethash);

    print "updateBlockMid: END\n";
} # end updateBlockMid

sub updateBlockEnd {
    my ($config, $status) = @_;

    print "updateBlockEnd: BEG\n";

    if (!defined($status)) {
        $status = 'NULL';
    }
    my %sethash = ( block_end => time, 
                    end_time => time,
                    status => $status);
    my $dbid_block = $config->value('dbid_block');
    updateVal($config, 'block', $dbid_block, \%sethash);


    %sethash = ( active_block_id => 'NULL',
                 active_block_name => 'NULL'); 
    my $dbid_run = $config->value('dbid_run');
    updateVal($config, 'run', $dbid_run, \%sethash);


    print "updateBlockEnd: END\n";
} # end updateBlockEnd



sub updateRunStart {
    my ($config) = @_;

    print "updateRunStart: BEG\n";

    my %sethash = ( run_start=>time,
                    start_time=>time);
    my $dbid_run = $config->value('dbid_run');
    updateVal($config, 'run', $dbid_run, \%sethash);

    print "updateRunStart: END\n";
}

sub updateRunStatus {
    my ($config, $status) = @_;

    print "updateRunStatus: BEG\n";

    if (!defined($status)) {
        $status = 'NULL';
    }
    my %sethash = ( run_end=>time, end_time=>time, status=>$status); 
    my $dbid_run = $config->value('dbid_run');
    updateVal($config, 'run', $dbid_run, \%sethash);

    print "updateRunStatus: END\n";

    return $status;
}


sub updateOrchTaskList {
    my ($config, $OrchTaskList) = @_;

    print "updateOrchTaskList: BEG\n";

    my %sethash = ( 'orchtask_list'=>$OrchTaskList );
    my $dbid_block = $config->value('dbid_block');
    updateVal($config, 'block', $dbid_block, \%sethash);

    print "updateOrchTaskList: END\n";
} # end OrchTaskList


#################################################
### ORCHTASK TABLE
#################################################

sub insertOrchTask {
    my ($config, $dbid_block, $name, $deshome) = @_;

    my $dbh = connectProcDB();

    # Check that dbid_block exists
    my $dbq = "SELECT NAME FROM BLOCK WHERE ID='$dbid_block'";
    my $rows = $dbh->selectall_hashref($dbq, 'name');
    if (scalar keys %$rows == 0) {
        print "id value ($dbid_block) does not exist\n";
        print "ABORTING\n";
        exit $FAILURE;
    }
    if (scalar keys %$rows != 1) {
        print "id value ($dbid_block) is not unique\n";
        print "ABORTING\n";
        exit $FAILURE;
    }

    my $dbid_orchtask = $dbh->getNextSequenceID('ORCHTASK_SEQ');
    my $osubmit = epoch2str(time);

    my $provstr = getProvStr($deshome);

#    my $dbi = "INSERT INTO ORCHTASK (ID, BLOCK_ID, NAME, SUBMIT_TIME, PROVENANCE)";
#    $dbi .= " VALUES ($dbid_orchtask, $dbid_block, '$name', '$osubmit', '$provstr')";
    my $dbi = "INSERT INTO ORCHTASK (ID, BLOCK_ID, NAME)";
    $dbi .= " VALUES ($dbid_orchtask, $dbid_block, '$name')";
    logOrchEvent($config, $dbi);

    my $sth = $dbh->prepare($dbi);
    $sth->execute();
    $dbh->commit();
    $dbh->disconnect();

    print "insertOrchTask: End\n";
    return $dbid_orchtask;
}


sub updateOrchTaskSubmit {
    my ($config, $taskname, $dbid_task) = @_;
    print "updateOrchTaskSubmit: BEG\n";

    my $deshome = $config->value('des_home');

    my $provstr = getProvStr($deshome);
    my %sethash = ( submit_time=>time, 
                     provenance=>$provstr );

    updateVal($config, 'orchtask', $dbid_task, \%sethash);


    %sethash = ( active_orchtask_id => $dbid_task,
                 active_orchtask_name => $taskname);
    my $dbid_block = $config->value('dbid_block');
    updateVal($config, 'block', $dbid_block, \%sethash);


    print "updateOrchTaskSubmit: END\n";
}

sub updateOrchTaskStart {
    my ($config, $dbid_task, $orchid) = @_;
    print "updateOrchTaskStart: BEG\n";

    my %sethash = ( 'start_time'=>time );
    if (defined($orchid)) {
        $sethash{'orch_id'} = $orchid;
    }

    updateVal($config, 'orchtask', $dbid_task, \%sethash);

    print "updateOrchTaskStart: END\n";
}

sub updateOrchTaskEnd {
    my ($config, $dbid_task, $orchid, $exit) = @_;

    print "updateOrchTaskEnd: BEG\n";

    my %sethash = ( 'end_time'=>time);
    if (defined($orchid)) {
        $sethash{'orch_id'} = $orchid;
    }
    if (defined($exit)) {
        $sethash{'status'} = $exit;
    }
    updateVal($config, 'orchtask', $dbid_task, \%sethash);

#    %sethash = ('wall_time'=> '@(end_time-start_time)*24*60*60');
#    updateOrchTask($config,$dbid_task, \%sethash);

    %sethash = ( active_orchtask_id => 'NULL',
                 active_orchtask_name => 'NULL');
    my $dbid_block = $config->value('dbid_block');
    updateVal($config, 'block', $dbid_block, \%sethash);

    print "updateOrchTaskEnd: END\n";
}


sub insertJobs {
    my ($config) = @_;

    print "insertJobs: BEG\n";
    my $dbh = connectProcDB();

    my $dbid_block = $config->value('dbid_block');
    my $dbid_orchtasks = $config->value('dbid_orchtasks');
    my $dbid_parent = $dbid_orchtasks->{'pipelinesmngr'};
    
    my $tjobinfo = $config->value('tjobinfo');
print "tjobinfo ", Dumper($tjobinfo), "\n";
    foreach my $tj (sort keys %$tjobinfo) {
        print "$tj\n";
        my $dbid_tjob = $dbh->getNextSequenceID('TJOB_SEQ');
        $tjobinfo->{$tj}{'dbid_tjob'} = $dbid_tjob;
        my $firstid_desjob = $tjobinfo->{$tj}{'start_jobid'};
        my $lastid_desjob = $tjobinfo->{$tj}{'end_jobid'};
        my $dbi = "INSERT INTO TJOB (ID,NAME,BLOCK_ID,ORCHTASK_ID,FIRSTID_DESJOB,LASTID_DESJOB) ";
        $dbi .= " VALUES ($dbid_tjob,'$tj',$dbid_block,$dbid_parent,'$firstid_desjob','$lastid_desjob')";
        logOrchEvent($config, $dbi);
        my $sth = $dbh->prepare($dbi);
        $sth->execute();

        my $jobinfo = $config->value('job');
        for (my $jobid=$firstid_desjob; $jobid <= $lastid_desjob; $jobid++) {
#        foreach my $jobid (keys %$jobinfo) {
            my $jobidstr = sprintf("%04d", $jobid);
            my $dbid_desjob = $dbh->getNextSequenceID('DESJOB_SEQ');
            $jobinfo->{$jobidstr}{'dbid_desjob'} = $dbid_desjob;
            $dbi = "INSERT INTO DESJOB (ID,NAME,TJOB_ID,BLOCK_ID) ";
            $dbi .= " VALUES ($dbid_desjob,'$jobidstr',$dbid_tjob,$dbid_block)";
            logOrchEvent($config, $dbi);
            $sth = $dbh->prepare($dbi);
            $sth->execute();
        }
    }

    # handle ingest tjob/desjob separately
    my $dbid_ingest_tjob = $dbh->getNextSequenceID('TJOB_SEQ');
    $dbid_parent = $dbid_orchtasks->{'findingest'};
    my $dbi = "INSERT INTO TJOB (ID,NAME,BLOCK_ID,ORCHTASK_ID,FIRSTID_DESJOB,LASTID_DESJOB) ";
    $dbi .= " VALUES ($dbid_ingest_tjob,'ingest',$dbid_block,$dbid_parent,'ingest','ingest')";
    logOrchEvent($config, $dbi);
    my $sth = $dbh->prepare($dbi);
    $sth->execute();

    my $dbid_ingest_desjob = $dbh->getNextSequenceID('DESJOB_SEQ');
    $dbi = "INSERT INTO DESJOB (ID,NAME,TJOB_ID,BLOCK_ID) ";
    $dbi .= " VALUES ($dbid_ingest_desjob,'ingest',$dbid_ingest_tjob,$dbid_block)";
    logOrchEvent($config, $dbi);
    $sth = $dbh->prepare($dbi);
    $sth->execute();

    # save ingest dbids in block since different than normal jobs
    my $blockobj = $config->obj('block')->obj($config->value('block'));
    $blockobj->value('dbid_ingest_desjob', $dbid_ingest_desjob);
    $blockobj->value('dbid_ingest_tjob', $dbid_ingest_tjob);

    $dbh->commit();
    $dbh->disconnect();

    print "insertJobs: END\n";
}


# requires table that has ID and NAME columns
sub updateVal {
    my ($config, $table, $dbid, $sethash) = @_;

    my $setstr = createSetStr($sethash);
    my $dbi = "UPDATE $table SET $setstr WHERE ID=$dbid";
    logOrchEvent($config, $dbi);


    my $dbh = connectProcDB();

    # Check that row already exists
    my $dbq = "SELECT NAME FROM $table WHERE ID='$dbid'";
    my $dbrows = $dbh->selectall_hashref($dbq, 'name');
    if (scalar keys %$dbrows == 0) {
        print "Error: Could not find DB entry for $table $dbid\n";
        $dbh->disconnect();
        exit $FAILURE;
    }

    eval {
        my $sth = $dbh->prepare($dbi);
        $sth->execute();
        $sth->finish();
    };
    if ($@) {
        print "$dbi\n";
        die($@);
    }
    else {
        $dbh->commit();
    }
    $dbh->disconnect();
}

sub logOrchEvent {
    my ($config, $event) = @_;

    print "$event\n";
    my $uberdir = $config->value('uberctrl_dir');
    open FH, ">> $uberdir/orchevents.log" or die "Error:  Cannot append to orchevents log ($uberdir/dessubmit.log)";
    print FH getTimeStamp(),":   $event\n";
    close FH;
}


sub connectProcDB {
    my $dbh = new DB::Util(db_config_file=>"$ENV{'HOME'}/.desdm",which_db=>'proc',verbose=>0,debug=>0,attr=> {
      'AutoCommit' => 0,
      'RaiseError' => 0,
      'PrintError'  => 0,
      'HandleError' => Exception::Class::DBI->handler,
      'FetchHashKeyName' => 'NAME_lc',
    });

    $dbh->setTimeFormat();

    return $dbh;
}

sub createSetStr {
    my ($sethash) = @_;
    my $setstr = "";

#    # connect to database in order to use quote command
#    my $dbh = connectProcDB();

    while (my ($k,$v) = each(%$sethash)) {
        $k = lc($k);
        if ($v =~ /^@/) {
            $v = substr($v,1);
            $setstr .= "$k = $v";
        }
        elsif ($v eq 'NULL') {
            $setstr .= "$k = NULL";
        }
        elsif (($k eq "submit_time") || ($k eq "start_time") || ($k eq "end_time") ||
               ($k eq "run_submit") || ($k eq "run_start") || ($k eq "run_end") ||
               ($k eq "block_submit") || ($k eq "block_start") || ($k eq "block_end")   ) {
            if ($v =~ /\//) {  # if already in time format
                $setstr .= "$k ='$v'";
            }
            else { # otherwise was given epoch value
                $setstr .= "$k ='".epoch2str($v)."'";
            }
        }
        else {
            $setstr .= "$k='$v'";
        }
        $setstr .= ",";
    }

#    $dbh->disconnect();

    $setstr =~ s/,$//;
    
    return $setstr;
}


#sub diffTimes {
#    my ($Date1,$Date2,$Year1,$Year2) = @_;
#    my ($Mon, $MDay, $Year, $Hours, $Min, $Secs);
#    my ($Difference, $Seconds, $Minutes, $Days);
#
#    $Year = undef;
#    if ($Date1 =~ /(\d+)\/(\d+)\/(\d+)\s+(\d+):(\d+):(\d+)/) {
#        ($Mon, $MDay, $Year, $Hours, $Min, $Secs) = ($1, $2, $3, $4, $5, $6);
#    }
#    elsif ($Date1 =~ /(\d+)\/(\d+)\s+(\d+):(\d+):(\d+)/) {
#        ($Mon, $MDay, $Hours, $Min, $Secs) = ($1, $2, $3, $4, $5);
#    }
#    if (!defined($Year)) {
#        $Year = $Year1;
#    }
#    $EpochDate1 = timelocal($Secs, $Min, $Hours, $MDay, $Mon-1, $Year-1900);
#    
#    $Year = undef;
#    if ($Date2 =~ /(\d+)\/(\d+)\/(\d+)\s+(\d+):(\d+):(\d+)/) {
#        ($Mon, $MDay, $Year, $Hours, $Min, $Secs) = ($1, $2, $3, $4, $5, $6);
#    }
#    elsif ($Date2 =~ /(\d+)\/(\d+)\s+(\d+):(\d+):(\d+)/) {
#        ($Mon, $MDay, $Hours, $Min, $Secs) = ($1, $2, $3, $4, $5);
#    }
#    if (!defined($Year)) {
#        $Year = $Year2;
#    }
#    $EpochDate2 = timelocal($Secs, $Min, $Hours, $MDay, $Mon-1, $Year-1900);
#
#    $Difference = $Date1 - $Date2;
#    return $Difference;
#}
#




1;
