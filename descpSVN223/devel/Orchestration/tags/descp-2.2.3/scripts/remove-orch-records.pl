#!/usr/bin/env perl
########################################################################
#  $Id: logpre.pl 1983 2008-10-08 18:19:33Z mgower $
#
#  $Rev:: 1983                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-10-08 13:19:33 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

use strict;
use warnings;

print "$0 @ARGV\n";

use Data::Dumper;
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Orchestration::misc;
use Orchestration::hist;
use Orchestration::desconfig;


my ($project, $run);
if (scalar(@ARGV) == 2) {
  $project = $ARGV[0];
  $run = $ARGV[1];
}
else {
  print "Usage: remove-orch-records project run\n";
  exit $FAILURE;
}

my ($dbh, $dbq, $sth);

$dbh = connectProcDB();

$dbq = "select id from run where project='$project' and run='$run'";
my $runs = $dbh->selectcol_arrayref($dbq, { Columns=>[1] });
if (scalar(@$runs) == 0) {
    print "Error:  Didn't find any run records for project='$project' and run='$run'\n";
    $dbh->disconnect();
    exit 1;
}
if (scalar(@$runs) > 1) {
    print "Error:  Found more than 1 run record for project='$project' and run='$run'\n";
    print Dumper($runs),"\n";
    $dbh->disconnect();
    exit 1;
}
#print Dumper($runs),"\n";

my $dbid_run = $runs->[0];
#print "dbid_run = $dbid_run\n";

my $rows = getRunEntries($dbid_run);
printRun($rows);

my $ans = 'x';
while (($ans ne 'y') && ($ans ne 'n')) {
    print "Are you sure you want to delete these records [Y/N]?  ";
    my $line = <STDIN>; 
    chomp($line); 
    $ans = lc(substr($line,0,1));
    if (($ans ne 'y') && ($ans ne 'n')) {
        print "\tInvalid input ($ans).  Try again\n";
    }
}

if ($ans eq 'n') {
    print "\nQuiting without modifying database\n\n";
    $dbh->disconnect();
    exit 0;
}

deleteRun($dbid_run, $rows);

$ans = 'x';
while (($ans ne 'y') && ($ans ne 'n')) {
    print "Do you want to commit these deletions [Y/N]?  ";
    my $line = <STDIN>; 
    chomp($line); 
    $ans = lc(substr($line,0,1));
    if (($ans ne 'y') && ($ans ne 'n')) {
        print "\tInvalid input ($ans).  Try again\n";
    }
}

if ($ans eq 'y') {
    $dbh->commit();
    $dbh->disconnect();
}
else {
    exit 2;
}


sub getRunEntries {
    my ($dbid_run) = @_;

    my (%rows);

print "dbid_run = $dbid_run\n";

    $rows{'block'} = $dbh->selectall_hashref("select block.block_name,block.id from block where block.run_id=$dbid_run", "id");
    $rows{'orchtask'} = $dbh->selectall_hashref("select count(*),block.block_name,block.block_id from orchtask,block where orchtask.block_id=block.id and block.run_id=$dbid_run group by block.block_name,block.block_id", "block_id");

    $rows{'tjob'} = $dbh->selectall_hashref("select count(*),block.block_name,block.block_id from tjob,block where tjob.block_id=block.id and block.run_id=$dbid_run group by block.block_name,block.block_id", "block_id");

    $rows{'desjob'} = $dbh->selectall_hashref("select count(*),block.block_name,block.block_id from desjob,block where desjob.block_id=block.id and block.run_id=$dbid_run group by block.block_name,block.block_id", "block_id");

    $rows{'orchmsg'} = $dbh->selectall_hashref("select count(*),block.block_name,block.block_id from orchmsg,block where orchmsg.block_id=block.id and block.run_id=$dbid_run group by block.block_name,block.block_id", "block_id");

    return \%rows;
}

sub printRun {
    my ($rows) = @_;

    print "\n\nNumber of records to be deleted\n";
    print "1 run record plus...\n";

    if (scalar(keys %{$rows->{'block'}}) > 0) {
        printf "%30s (%4s) %5s %5s %5s %5s\n", 'block', 'blid','OT', 'OM', 'TJ', 'DJ';
        printf "%30s  %4s  %5s %5s %5s %5s\n", '-----', '----', '--', '--', '--', '--';
    }
    else {
        print "0 block records\n";
    }

    foreach my $dbid_block (sort keys %{$rows->{'block'}}) {
        printf "%30s (%4d) %5d %5d %5d %5d\n", 
            $rows->{'block'}->{$dbid_block}{'block_name'}, $dbid_block,
            $rows->{'orchtask'}->{$dbid_block}{'count'} ? $rows->{'orchtask'}->{$dbid_block}{'count'} : 0,
            $rows->{'orchmsg'}->{$dbid_block}{'count'} ? $rows->{'orchmsg'}->{$dbid_block}{'count'} : 0,
            $rows->{'tjob'}->{$dbid_block}{'count'} ? $rows->{'tjob'}->{$dbid_block}{'count'} : 0,
            $rows->{'desjob'}->{$dbid_block}{'count'} ? $rows->{'desjob'}->{$dbid_block}{'count'} : 0;
    }    
    print "\n\n";
}


sub deleteRun {
    my ($dbid_run, $rows) = @_;
    my $delcnt;

    $delcnt = $dbh->do("delete from run where id=$dbid_run"); 
    print "Deleting run $dbid_run\n";
    print "\tDeleted $delcnt record(s) from run table\n";
    if ($delcnt != 1) {
        print "\tError delete number ($delcnt) doesn't match query number (1)\n";
        print "\tABORTING\n";
        exit 1;
    }

    foreach my $dbid_block (sort keys %{$rows->{'block'}}) {
        print "Deleting block $dbid_block\n";
        $delcnt = $dbh->do("delete from block where id=$dbid_block"); 
        print "\tDeleted $delcnt record(s) from block table\n";

        if (defined($rows->{'orchtask'}->{$dbid_block}{'count'}) && $rows->{'orchtask'}->{$dbid_block}{'count'} != 0) {
            $delcnt = $dbh->do("delete from orchtask where block_id=$dbid_block"); 
            print "\tDeleted $delcnt record(s) from orchtask table\n";
            if ($delcnt != $rows->{'orchtask'}->{$dbid_block}{'count'}) {
                print "\tError delete number ($delcnt) doesn't match query number (",$rows->{'orchtask'}->{$dbid_block}{'count'},")\n";
                print "\tABORTING\n";
                exit 1;
            }
        }
    
        if (defined($rows->{'orchmsg'}->{$dbid_block}{'count'}) && $rows->{'orchmsg'}->{$dbid_block}{'count'} != 0) {
            $delcnt = $dbh->do("delete from orchmsg where block_id=$dbid_block"); 
            print "\tDeleted $delcnt record(s) from orchmsg table\n";
            if ($delcnt != $rows->{'orchmsg'}->{$dbid_block}{'count'}) {
                print "\tError delete number ($delcnt) doesn't match query number (",$rows->{'orchmsg'}->{$dbid_block}{'count'},")\n";
                print "\tABORTING\n";
                exit 1;
            }
        }

        if (defined($rows->{'tjob'}->{$dbid_block}{'count'}) && $rows->{'tjob'}->{$dbid_block}{'count'} != 0) {
            $delcnt = $dbh->do("delete from tjob where block_id=$dbid_block"); 
            print "\tDeleted $delcnt record(s) from tjob table\n";
            if ($delcnt != $rows->{'tjob'}->{$dbid_block}{'count'}) {
                print "\tError delete number ($delcnt) doesn't match query number (",$rows->{'tjob'}->{$dbid_block}{'count'},")\n";
                print "\tABORTING\n";
                exit 1;
            }
        }

        if (defined($rows->{'desjob'}->{$dbid_block}{'count'}) && $rows->{'desjob'}->{$dbid_block}{'count'} != 0) {
            $delcnt = $dbh->do("delete from desjob where block_id=$dbid_block"); 
            print "\tDeleted $delcnt record(s) from desjob table\n";
            if ($delcnt != $rows->{'desjob'}->{$dbid_block}{'count'}) {
                print "\tError delete number ($delcnt) doesn't match query number (",$rows->{'desjob'}->{$dbid_block}{'count'},")\n";
                print "\tABORTING\n";
                exit 1;
            }
        }
    }
}
