package NSAConverter;

# This module contains funtions that help process FITS files by splitting them up  
# into the correct format

use 5.008008;
use strict;
use warnings;



use Astro::FITS::CFITSIO qw( :longnames ) ; 
use Astro::FITS::CFITSIO qw( :constants );
use Carp;
use Getopt::Long;
use Cwd;
use Data::Dumper;
use IPC::Open3;

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use FITS_Header ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
split_in_filename
name_of_output_file_red
name_of_output_file_cat
open_fits_file
create_primary_header_red
create_primary_header_cat
create_extension_header_red
move_to_last_hdu
append_hdus_red
append_hdus_cat
append_hdus_flatcor
append_hdus_biascor
append_hdus_supersky
append_hdus_illumcor
append_hdus_fringecor
last_hdu_for_file_fast
last_hdu_for_file
create_hash
usage
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';

#our $red_primary_headers = '/home/ankitc/abhijeet/perl/NSAConverter/lib/primary_headers_red';
#our $red_extension_headers = '/home/ankitc/abhijeet/perl/NSAConverter/lib/extension_headers_red';
#our $cat_primary_headers = '/home/ankitc/abhijeet/perl/NSAConverter/lib/primary_headers_cat';
#our $cat_extension_headers = '/home/ankitc/abhijeet/perl/NSAConverter/lib/extension_headers_red';

our $home_dir = "";#"/home/ankitc/abhijeet/perl/NSAConverter/header_files/version_11/release";
#our $home_dir = "/home/ankitc/abhijeet/perl/NSAConverter/lib";

our $red_primary_headers = $home_dir.'primary_headers_red';
our $red_extension_headers = $home_dir.'extension_headers_red';
our $cat_primary_headers = $home_dir.'primary_headers_cat';
our $cat_extension_headers = $home_dir.'extension_headers_red';

###
#	Filename prefix to be added to the name of the output file
###
our $filenameprefix;
our $numPrimHeaders = 0 ; #number of primary headers based on file type
our $outputdir = '';
# Create a global Image Extension Hashref which can be referred to when needed. This hashref will be used to fetch values for specific keywords
our $ImageKeywordHashref;
our $nextend;
our $fileDetails;
our $filetype_Keyword_DB_Hash = {
'norm_kern' => {},
'raw_bias' => {},
'coadd' => {},
'norm_psfcat' => {},
'remap.bak' => {},
'shapelet_mes' => {},
'supersky' => {},
'coadd_cat' => {},
'bpm' => {},
'raw_dflat' => {},
'red_scamp' => {},
'photoz_cat' => {},
'diff_nitecmb_temp' => {},
'red_head' => {},
'coadd_psf' => {},
'log' => {},
'flatcor' => {},
'red_bkg' => {},
'coadd_psfcat' => {},
'aux' => {},
'diff_nitecmb_srch' => {},
'shapelet_shpltall' => {},
'raw_obj' => {},
'pupil' => {},
'norm' => {},
'remap' => {},
'red_psfcat' => {},
'red_cat' => {},
'QA' => {},
'shapelet_psfmodel' => {},
'xtalk' => {},
'shapelet_shear' => {},
'biascor' => {},
'photflatcor' => {},
'runtime' => {},
'diff_nitecmb_diff' => {},
'src' => {},
'aux_astrostds' => {},
'fringecor' => {},
'red_ahead' => {},
'photoz_zcat' => {},
'xml' => {},
'red' => {},
'illumcor' => {},
'coadd_det' => {},
'shapelet_shpltpsf' => {}
};


our $filetype_Keyword_Mapping_Hash = {
'red' => {
'MAGZERO' => {'keyword' => 'ZP','source' => 'image'}, #                         MAP to ZP column of our files. Average it out if different ccd extensions have differnet values
'PHTMTRC' => {'keyword' => 'PHOTFLAG', 'source' => 'image'},#                         MAP to PHOTFLAG of our files.
'SCMPNTOT' => {'keyword' => 'SCAMPNUM', 'source' => 'image'},#                        MAP to SCAMPNUM in the file
'SEEINGP' => {'keyword' => 'FWHM','source' => 'image'}, #      MAP to FWHM in the file
'SISCOVER' => {'keyword' => 'CONSTVER','source' => 'image'}, #                        MAP to CONSTVER in the file
'SISSWVER' => {'keyword' => 'SISPIVER', 'source' => 'image'},#                        MAP to SISPIVER in the file
'BIASCOR' => {'keyword' => 'DESBIAS', 'source' => 'image'},#                         MAPPED to DESBIAS from Image extension header
},
'norm_kern' => {},
'raw_bias' => {},
'coadd' => {},
'norm_psfcat' => {},
'remap.bak' => {},
'shapelet_mes' => {},
'supersky' => {

'MAGZERO' => {'keyword' => 'ZP','source' => 'image'}, #                         MAP to ZP column of our files. Average it out if different ccd extensions have differnet values
'PHTMTRC' => {'keyword' => 'PHOTFLAG', 'source' => 'image'},#                         MAP to PHOTFLAG of our files.
'SCMPNTOT' => {'keyword' => 'SCAMPNUM', 'source' => 'image'},#                        MAP to SCAMPNUM in the file
'SEEINGP' => {'keyword' => 'FWHM','source' => 'image'}, #      MAP to FWHM in the file
'SISCOVER' => {'keyword' => 'CONSTVER','source' => 'image'}, #                        MAP to CONSTVER in the file
'SISSWVER' => {'keyword' => 'SISPIVER', 'source' => 'image'},#                        MAP to SISPIVER in the file
'BIASCOR' => {'keyword' => 'DESBIAS', 'source' => 'image'},#                         MAPPED to DESBIAS from Image extension header

},
'coadd_cat' => {},
'bpm' => {},
'raw_dflat' => {},
'red_scamp' => {},
'photoz_cat' => {},
'diff_nitecmb_temp' => {},
'red_head' => {},
'coadd_psf' => {},
'log' => {},
'flatcor' => {
'MAGZERO' => {'keyword' => 'ZP','source' => 'image'}, #                         MAP to ZP column of our files. Average it out if different ccd extensions have differnet values
'PHTMTRC' => {'keyword' => 'PHOTFLAG', 'source' => 'image'},#                         MAP to PHOTFLAG of our files.
'SCMPNTOT' => {'keyword' => 'SCAMPNUM', 'source' => 'image'},#                        MAP to SCAMPNUM in the file
'SEEINGP' => {'keyword' => 'FWHM','source' => 'image'}, #      MAP to FWHM in the file
'SISCOVER' => {'keyword' => 'CONSTVER','source' => 'image'}, #                        MAP to CONSTVER in the file
'SISSWVER' => {'keyword' => 'SISPIVER', 'source' => 'image'},#                        MAP to SISPIVER in the file
'BIASCOR' => {'keyword' => 'DESBIAS', 'source' => 'image'},#                         MAPPED to DESBIAS from Image extension header
},
'red_bkg' => {},
'coadd_psfcat' => {},
'aux' => {},
'diff_nitecmb_srch' => {},
'shapelet_shpltall' => {},
'raw_obj' => {},
'pupil' => {},
'norm' => {},
'remap' => {},
'red_psfcat' => {},
'red_cat' => {},
'QA' => {},
'shapelet_psfmodel' => {},
'xtalk' => {},
'shapelet_shear' => {},
'biascor' => {
'MAGZERO' => {'keyword' => 'ZP','source' => 'image'}, #                         MAP to ZP column of our files. Average it out if different ccd extensions have differnet values
'PHTMTRC' => {'keyword' => 'PHOTFLAG', 'source' => 'image'},#                         MAP to PHOTFLAG of our files.
'SCMPNTOT' => {'keyword' => 'SCAMPNUM', 'source' => 'image'},#                        MAP to SCAMPNUM in the file
'SEEINGP' => {'keyword' => 'FWHM','source' => 'image'}, #      MAP to FWHM in the file
'SISCOVER' => {'keyword' => 'CONSTVER','source' => 'image'}, #                        MAP to CONSTVER in the file
'SISSWVER' => {'keyword' => 'SISPIVER', 'source' => 'image'},#                        MAP to SISPIVER in the file
'BIASCOR' => {'keyword' => 'DESBIAS', 'source' => 'image'},#                         MAPPED to DESBIAS from Image extension header
},
'photflatcor' => {},
'runtime' => {},
'diff_nitecmb_diff' => {},
'src' => {},
'aux_astrostds' => {},
'fringecor' => {
'MAGZERO' => {'keyword' => 'ZP','source' => 'image'}, #                         MAP to ZP column of our files. Average it out if different ccd extensions have differnet values
'PHTMTRC' => {'keyword' => 'PHOTFLAG', 'source' => 'image'},#                         MAP to PHOTFLAG of our files.
'SCMPNTOT' => {'keyword' => 'SCAMPNUM', 'source' => 'image'},#                        MAP to SCAMPNUM in the file
'SEEINGP' => {'keyword' => 'FWHM','source' => 'image'}, #      MAP to FWHM in the file
'SISCOVER' => {'keyword' => 'CONSTVER','source' => 'image'}, #                        MAP to CONSTVER in the file
'SISSWVER' => {'keyword' => 'SISPIVER', 'source' => 'image'},#                        MAP to SISPIVER in the file
'BIASCOR' => {'keyword' => 'DESBIAS', 'source' => 'image'},#                         MAPPED to DESBIAS from Image extension header
},
'red_ahead' => {},
'photoz_zcat' => {},
'xml' => {},
'illumcor' => {
'MAGZERO' => {'keyword' => 'ZP','source' => 'image'}, #                         MAP to ZP column of our files. Average it out if different ccd extensions have differnet values
'PHTMTRC' => {'keyword' => 'PHOTFLAG', 'source' => 'image'},#                         MAP to PHOTFLAG of our files.
'SCMPNTOT' => {'keyword' => 'SCAMPNUM', 'source' => 'image'},#                        MAP to SCAMPNUM in the file
'SEEINGP' => {'keyword' => 'FWHM','source' => 'image'}, #      MAP to FWHM in the file
'SISCOVER' => {'keyword' => 'CONSTVER','source' => 'image'}, #                        MAP to CONSTVER in the file
'SISSWVER' => {'keyword' => 'SISPIVER', 'source' => 'image'},#                        MAP to SISPIVER in the file
'BIASCOR' => {'keyword' => 'DESBIAS', 'source' => 'image'},#                         MAPPED to DESBIAS from Image extension header
},
'coadd_det' => {},
'shapelet_shpltpsf' => {}
};



# simply copy the value of SIMPLE from this hashref. Since this is only needed to create a valid fits file. This keyword has a standard value to be put in
our $filetype_Keyword_Simplycopy_Hash = {
'biascor' => {
'SIMPLE' => {'primary' =>{'val' => '1', 'comment' => 'Conforms to FITS standard'}},
'NAXIS' => {'primary' =>{'val' => '0', 'comment' => 'Number of data axes'}},
'EXTEND' => {'primary' =>{'val' => 1, 'comment' => 'There are extensions'}},
'NEXTEND' => {'primary' =>{'val' => '8', 'comment' => 'Number of extensions'}},
'GCOUNT' => {'primary' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'},'extension' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'}},
'END' => {'primary' =>{'val' => '', 'comment' => ''},'extension' => {'val' => '', 'comment' => ''}}
},
'supersky' => {
'SIMPLE' => {'primary' =>{'val' => '1', 'comment' => 'Conforms to FITS standard'}},
'NAXIS' => {'primary' =>{'val' => '0', 'comment' => 'Number of data axes'}},
'EXTEND' => {'primary' =>{'val' => 1, 'comment' => 'There are extensions'}},
'NEXTEND' => {'primary' =>{'val' => '8', 'comment' => 'Number of extensions'}},
'GCOUNT' => {'primary' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'},'extension' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'}},
'END' => {'primary' =>{'val' => '', 'comment' => ''},'extension' => {'val' => '', 'comment' => ''}}
},
'illumcor' => {
'SIMPLE' => {'primary' =>{'val' => '1', 'comment' => 'Conforms to FITS standard'}},
'NAXIS' => {'primary' =>{'val' => '0', 'comment' => 'Number of data axes'}},
'EXTEND' => {'primary' =>{'val' => 1, 'comment' => 'There are extensions'}},
'NEXTEND' => {'primary' =>{'val' => '8', 'comment' => 'Number of extensions'}},
'GCOUNT' => {'primary' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'},'extension' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'}},
'END' => {'primary' =>{'val' => '', 'comment' => ''},'extension' => {'val' => '', 'comment' => ''}}
},
'flatcor' => {
'SIMPLE' => {'primary' =>{'val' => '1', 'comment' => 'Conforms to FITS standard'}},
'NAXIS' => {'primary' =>{'val' => '0', 'comment' => 'Number of data axes'}},
'EXTEND' => {'primary' =>{'val' => 1, 'comment' => 'There are extensions'}},
'NEXTEND' => {'primary' =>{'val' => '8', 'comment' => 'Number of extensions'}},
'GCOUNT' => {'primary' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'},'extension' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'}},
'END' => {'primary' =>{'val' => '', 'comment' => ''},'extension' => {'val' => '', 'comment' => ''}}
},
'fringecor' => {
'SIMPLE' => {'primary' =>{'val' => '1', 'comment' => 'Conforms to FITS standard'}},
'NAXIS' => {'primary' =>{'val' => '0', 'comment' => 'Number of data axes'}},
'EXTEND' => {'primary' =>{'val' => 1, 'comment' => 'There are extensions'}},
'NEXTEND' => {'primary' =>{'val' => '8', 'comment' => 'Number of extensions'}},
'GCOUNT' => {'primary' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'},'extension' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'}},
'END' => {'primary' =>{'val' => '', 'comment' => ''},'extension' => {'val' => '', 'comment' => ''}}
},
'red' => {
'SIMPLE' => {'primary' =>{'val' => '1', 'comment' => 'Conforms to FITS standard'}},
'NAXIS' => {'primary' =>{'val' => '0', 'comment' => 'Number of data axes'}},
'EXTEND' => {'primary' =>{'val' => 1, 'comment' => 'There are extensions'}},
'NEXTEND' => {'primary' =>{'val' => '8', 'comment' => 'Number of extensions'}},
'GCOUNT' => {'primary' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'},'extension' =>{'val' => '1', 'comment' => 'GCOUNT must be = 1'}},
'END' => {'primary' =>{'val' => '', 'comment' => ''},'extension' => {'val' => '', 'comment' => ''}}
},
'red_cat' => {
'SIMPLE' => {'primary' =>{'val' => '1', 'comment' => 'Conforms to FITS standard'}},
'NAXIS' => {'primary' =>{'val' => '0', 'comment' => 'Number of data axes'}},
'EXTEND' => {'primary' =>{'val' => 'T', 'comment' => 'There are extensions'}},
'NEXTEND' => {'primary' =>{'val' => '8', 'comment' => 'Number of extensions'}},
'END' => {'primary' =>{'val' => '', 'comment' => ''},'extension' => {'val' => '', 'comment' => ''}}
}};

### Hashref holding the keywords we must remove from primary or extension HDUs
our $filetype_Keyword_Simplyremove_Hash = {
'supersky' => {
'SIMPLE' => {'extension' => {'val' => '1'}},
},
'illumcor' => {
'SIMPLE' => {'extension' => {'val' => '1'}},
},
'biascor' => {
'SIMPLE' => {'extension' => {'val' => '1'}},
},
'fringecor' => {
'SIMPLE' => {'extension' => {'val' => '1'}},
},
'flatcor' => {
'SIMPLE' => {'extension' => {'val' => '1'}},
},
'red' => {
'SIMPLE' => {'extension' => {'val' => '1'}},
},
'red_cat' => {
'SIMPLE' => {'extension' => {'val' => '1'},
}
}};
# This hashref is used to store any specific function call or code which we might need to run to process some data piece, so that we can get the value for the variable 
our $filetype_Keyword_eval_Hash = {
'supersky' => {
'NEXTEND' => {'primary' =>{'val' => '$value = $nextend', 'comment' => 'Number of extensions'}},
},
'illumcor' => {
'NEXTEND' => {'primary' =>{'val' => '$value = $nextend', 'comment' => 'Number of extensions'}},
},
'fringecor' => {
'NEXTEND' => {'primary' =>{'val' => '$value = $nextend', 'comment' => 'Number of extensions'}},
},
'biascor' => {
'NEXTEND' => {'primary' =>{'val' => '$value = $nextend', 'comment' => 'Number of extensions'}},
},
'flatcor' => {
'NEXTEND' => {'primary' =>{'val' => '$value = $nextend', 'comment' => 'Number of extensions'}},
},
'red_cat' => {
#'EXPNUM' => {'primary' =>{'val' => '$value = $fileDetails->{\'EXPOSURENAME\'}', 'comment' => 'DECam exposure number'}},
'NEXTEND' => {'primary' =>{'val' => '$value = $nextend', 'comment' => 'Number of extensions'}},
},
'red' => {
#'EXPNUM' => {'primary' =>{'val' => '$value = $fileDetails->{\'EXPOSURENAME\'}', 'comment' => 'DECam exposure number'}},
'NEXTEND' => {'primary' =>{'val' => '$value = $nextend', 'comment' => 'Number of extensions'}},
},
};

my $in_fits_filename;
my $status=0;
my $infptr=0;

our $debug = 0;
our $HDU_toggle;
our $detector;
my @location_array;

my $project_name =0;
my $class =0;
my $run =0;
my $type_file =0;
my $exposure =0;
my $in_chdu_type="";

my %last_hdu_hash =();

#GET RID OF THIS LATER
my $dev_flag = 0;

sub last_hdu_for_file
{
    # This function takes the pointer to the output file and returns the
    # number of the last HDU in the file by calling a CFITSIO function
    # Because of this library call, this funtion is relatively slow
    # NOTE: Once the bugs have been worked out, it will be prefereable to use
    # last_hdu_for_file_fast, instead of this function

    my ($outfptr) = @_;
    my $total_out_hdu;
    $status = 0;
    Astro::FITS::CFITSIO::fits_get_num_hdus ($outfptr, $total_out_hdu, $status);	
    if ($status) {print "ERROR $status Could not get number of HDUs in the output file\n";}
#    elsif ($debug && $dev_flag) {print "Successfully obtained $total_out_hdu from the file\n";}
    elsif ($debug) {print "Successfully obtained $total_out_hdu from the file\n";}
    return $total_out_hdu;
}


sub last_hdu_for_file_fast
{
    # This function returns the number of the last HDU in the file, by using a hash lookup
    # NOTE: This function might still have some bugs

    my ($outfptr, $out_fits_filename) = @_;
    
    # If the file exists in the hash, increment its number of HDUs as one more will be appended,
    # and return the old number of HDUs

    if (defined $last_hdu_hash{$out_fits_filename})
    {
	if ($debug) {print " getting the last_hdu_for_file_fast, FOUND the filename in the hash: from the file $out_fits_filename\n";}
	return $last_hdu_hash{$out_fits_filename};
	#return $last_hdu_hash{$out_fits_filename}+1;
    }

    # If the file does not exist, then find out how many files exist in the 
    else 
    {
	if ($debug) {print "\n\t\tlast_hdu_type{$out_fits_filename} not found, creating new entry...\n";}

	$status = 0;
	Astro::FITS::CFITSIO::fits_get_num_hdus ($outfptr, $last_hdu_hash{$out_fits_filename}, $status );	
	if($status) {print "ERROR $status Could not get number of HDUs in the output file\n";}
	elsif ($debug) {print "Successfully obtained HDUs=$last_hdu_hash{$out_fits_filename} from the file $out_fits_filename\n";}
	return $last_hdu_hash{$out_fits_filename};
    }
    
}

sub split_in_filename
{
    my ($in_fits_filename)=@_;

    chomp($in_fits_filename);
    my @location_array= split('/', $in_fits_filename);

    return @location_array;
}


#This function creates the name for the output fitsfile based on the input
sub name_of_output_file_red
{
    my ($in_fits_filename, $infptr,$prefix) = @_ ;

	if (defined $prefix && $prefix ne ''){
		$filenameprefix = $prefix;	
	}elsif($filenameprefix ne ''){
		$prefix = $filenameprefix;	
	}
	
    $status = 0;
    my $in_chdu_type = "";
    
    #DES_EXT holds the correct type of the input HDU
    Astro::FITS::CFITSIO::fits_read_keyword ($infptr, "DES_EXT", $in_chdu_type, my $comment, $status);
    if ($in_chdu_type eq "") 
    {
	#Skip the primary header on the input fits file
	$status=0;
	Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);
	if ($status) {print "ERROR $status moving past the primary header failed!\n";}
	elsif ($debug && $dev_flag) {print "Moved past the primary successfully!\n";}
	
	# Read the file type again
	Astro::FITS::CFITSIO::fits_read_keyword ($infptr, "DES_EXT", $in_chdu_type, my $comment, $status);
    }
    elsif ($debug && $dev_flag) {print "DES_EXT=$in_chdu_type\n";}
    $in_chdu_type= substr $in_chdu_type, 1, -1;
    $in_chdu_type =~ s/\s+$//;
    
    my $filename = "";
    my $path;
    if ( substr($in_fits_filename, 0, 1) eq '/')
    {
	$filename = substr $in_fits_filename, rindex($in_fits_filename, '/') + 1;
	$path = substr $in_fits_filename, 0,  rindex($in_fits_filename, '/');
    }

    else 
    {
	print "ERROR $in_fits_filename is not a valid path to an input file\n";
    }

    #Strip the index from the filename
    $filename = substr($filename, 0, rindex ($filename, "_")) . substr($filename, rindex ($filename, "_") +3);

    ### If the output directory is provided, set the path to that directory. If not, continue with the same directory as the original file
    if ($outputdir ne ''){
	$path = $outputdir;
	if ($debug){
		print "\n\t\t Changing the output directory to $path";
	}
    }

    my $out_fits_filename = "$path/$prefix"."_$in_chdu_type"."_"."$filename";

    if ($debug) 
    {
	print "Input file is called $in_fits_filename\n";
	print "Output file will be placed in directory $path\n";
    }

    return $out_fits_filename;
}

#Create the name for the output fitsfile based on the input
sub name_of_output_file_cat
{
    my ($in_fits_filename, $infptr,$prefix) = @_ ;

	if (defined $prefix && $prefix ne ''){
                $filenameprefix = $prefix;
        }       
	elsif($filenameprefix ne ''){
                $prefix = $filenameprefix;
	}

    $status = 0;
    my $in_chdu_type = "";

    #The extension of the file will be prefixed to the filename
    $status = 0;
    Astro::FITS::CFITSIO::fits_read_keyword ($infptr, "EXTNAME", $in_chdu_type, my $comment, $status);
    if ($status) {print "ERROR $status Reading keyword EXTNAME failed\n";}
    if ($debug) {print "EXTNAME=$in_chdu_type\n";}
    # Remove quotes and whitespace from CHDU type
    $in_chdu_type = substr $in_chdu_type, 1, -1;
    $in_chdu_type =~ s/\s+$//;
   
    my $path; 
    my $filename = "";
    if ( substr($in_fits_filename, 0, 1) eq '/')
    {
	$filename = substr $in_fits_filename, rindex($in_fits_filename, '/') + 1;
	$path = substr $in_fits_filename, 0,  rindex($in_fits_filename, '/');
    }

    else 
    {
	print "ERROR $in_fits_filename is not a valid path to an input file\n";
    }

    ### If the GLOBAL VARIABLE output directory is provided, set the path to that directory. If not, continue with the same directory as the original file
    if ($outputdir ne ''){
	$path = $outputdir;
	if ($debug){
		print "\n\t\t Changing the output directory to $path";
	}
    }
    
    #Strip the index from the filename
    $filename = substr($filename, 0, rindex ($filename, "_")-3). substr($filename, rindex ($filename, "_"));
    my $out_fits_filename = "$path/$prefix"."_$in_chdu_type.$filename";

    if ($debug && $dev_flag) 
    {
	print "Input file is called $in_fits_filename\n";
	print "Output file will be placed in directory $path\n";
    }

    return $out_fits_filename;
}


#Open the output file corresponding to the input file
sub open_fits_file
{
    my ($fits_filename) = @_;
    my $fptr;

    $status =0;
    Astro::FITS::CFITSIO::fits_open_file($fptr, "$fits_filename", Astro::FITS::CFITSIO::READWRITE(), $status);
#    if ($debug==1 && $status ==0 && $dev_flag) {print "File $fits_filename opened sucessfully!\n";} 
    if ($debug==1 && $status ==0) {print "File $fits_filename opened sucessfully!\n";} 
    if ($status)  {print "ERROR $status File $fits_filename failed to open sucessfully!\n";} 

    return $fptr;
}


sub create_primary_header_red
{
    my ($outfptr, $infptr,$filetype) = @_;
    my ($status);
	$HDU_toggle = 'primary'; #SET THE TYPE OF HDU WE ARE WORKING WITH
    if ($debug) {print "\n\t Writing primary header for red...\n";}

	my @primary_headers; # this array is being initialized here. Ideally it should have been passed to the function, but that will take a little more time because there is a little bit of structural change needed in the Headersplit file
    if (scalar(@primary_headers)==0)
    {
	open FILE, $red_primary_headers or print "Failed to open extension headers file:$!\n";
	@primary_headers = <FILE>;

    }
    
    foreach (@primary_headers)
    {
	@_=split(/ | /);
	#print "\t ",$_, @_[0], "\n";
	#Astro::FITS::CFITSIO::first_read_key_str
    }
    
	###
        # Get the hashref of the keywords and their values for the current HDU, which is the first HDU (Image HDU)This is an assumption we make that Image HDU will always be the first one. Then use this hashref to overwrite the keyword's values from sample_extension_headers file.  
        ###
        my $HDU_keywordHashref = getHeaderHash($infptr);

        ###
        # If this is the Image HDU, we will need to use it's values for substitution in some cases. Keep the keyword-value pair to use it 
        ###
        if($HDU_keywordHashref->{'DES_EXT'} eq 'IMAGE'){

                $ImageKeywordHashref = $HDU_keywordHashref;
        }




    # removing this for now. Ideally we should read the existing primary hdu and then superimpose its values into the converted file's primary HDU. create_header($outfptr, \@primary_headers,getHeaderHash($infptr));
#	my $hdunum; Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr, $hdunum); print "\n the current HDU number in create_primary_header function is  $hdunum";
	#	print "\n in primary header create, prim headers are ", Dumper(@primary_headers);;
	  Astro::FITS::CFITSIO::fits_create_img( $outfptr, 8, 0, 0, $status);
	if ($debug){
		print "\n\t\t created a primary header for output file with status $status";
	}
    create_header($outfptr, \@primary_headers,$HDU_keywordHashref,$filetype);
}

sub create_primary_header_cat
{
    my ($outfptr, $infptr,$filetype) = @_;
	$HDU_toggle = 'primary'; #SET THE TYPE OF HDU WE ARE DEALING WITH 
    if ($debug) {print "\n\tWriting primary header for cat file...\n";}

	 my @primary_headers = (); # this array is being initialized here. Ideally it should have been passed to the function, but that will take a little more time because there is a little bit of structural change needed in the Headersplit file 
    if (scalar(@primary_headers)==0)
    {
	open FILE, $cat_primary_headers or print "Failed to open primary headers file: $!\n";
	@primary_headers = <FILE>;
    }

	###
        # Get the hashref of the keywords and their values for the current HDU, which is the Image HDU. Then use this hashref to overwrite the keyword's values from sample_extension_headers file.  
        ###
        my $HDU_keywordHashref = getHeaderHash($infptr);

        ###
        # If this is the Image HDU, we will need to use it's values for substitution in some cases. Keep the keyword-value pair to use it 
        ###
        if($HDU_keywordHashref->{'DES_EXT'} eq 'IMAGE'){

                $ImageKeywordHashref = $HDU_keywordHashref;
        }

    # same reason as in the other create primary function. create_header($outfptr, \@primary_headers,getHeaderHash($infptr))
    Astro::FITS::CFITSIO::fits_create_img( $outfptr, 8, 0, 0, $status);
    if($debug){
	print "\n\t\t created a new catalog primary header image with status $status , calling create_header with filetype $filetype";
    }
    create_header($outfptr, \@primary_headers,$HDU_keywordHashref,$filetype);
}

sub create_extension_header_red
{
    if ($debug) {print "\n\t\t Writing extension header for reduced file...\n";}
    my ($outfptr,$HDU_keywordHashref,$filetype) = @_;
	$HDU_toggle = 'extension'; # SET THE TYPE OF HDU WE ARE DEALING WITH
    my $hdunum;
	my @primary_headers; # this array is being initialized here. Ideally it should have been passed to the function, but that will take a little more time because there is a little bit of structural change needed in the Headersplit file
    if (scalar(@primary_headers)==0)
    {
	open FILE, $red_extension_headers or print "Failed to open extension headers file:$!\n";
	@primary_headers = <FILE>;

    }
    #Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr, $hdunum);
#	if($debug){ print "\n the current HDU number is $hdunum";}
    create_header($outfptr, \@primary_headers,$HDU_keywordHashref,$filetype);
}


###
# this function opens the text file with headers as given by NOAO. we loop through all the keywords available in the file and put them into the primary/extension header of the new file.  There are cases where some keywords requested might not exist in our files by the same name. In such cases, we map them to existing keywords which provide the same value. There are plans to read some similar values from Database and writing them in here.
# Input	: Outfptr: this is the file handle for the output file. The file we are creating to deliver to NOAO
#	: headers: the list of keywords REQUIRED in the output file. this list is coming from the header keywords requirements file provided by NOAO
#	: HDU_keywordHashred_new: this is the list of Keywords which the input DESDM file, the one that is being converted, contains. This list of keywords will be filtered and only the keywords which are required in the converted file (the requirements are given in keywords file circulated by Betty Stobie) will be written in the output file. 
###
sub create_header
{
    #my ($outfptr, @headers,$hello,$HDU_keywordHashref_new) = @_;
    my ($outfptr, $headers,$HDU_keywordHashref_new,$filetype) = @_;
#	print "\n creating primary file filetype check $filetype, hdu keyword  ";
	my ($keysexist,$morekeys,$status_hdrspace);
    if ( ref $headers eq 'ARRAY' && scalar(@$headers) == 0)
    {
	print "WARNING: No header list passed, using reduced file primary list by default\n";
	open FILE, $red_primary_headers or print "Failed to open extension headers file:$!\n";
	my @file_content = <FILE>;
	$headers = \@file_content;
    }

    my $keyword;
    my $datatype;
    my $value;
    my $comment;
    my $header_loop;
    my $empty_val;

#	print "\n BEGINNING HDU TOGGLE IS $HDU_toggle";

	# the variable $headers comes as either a string or an array. I am trying to create a single way of handling them both by forcing the string to come in as an array. I then pass the reference to the array to the loop below
	if (ref $headers eq 'ARRAY')
	{
		$header_loop = $headers;
		#print "\n found array for headers $headers ", Dumper($headers);
	}else{
		
		$header_loop = [$headers];
		#print "\n not found array ";
	}


	# some keywords are inserted from the input file, which are not needed. we need to remove them from the HDU	
	#print "\n going to remove Headers. for toggle $HDU_toggle, filetype $filetype";
	my ($removableKeywordHash,$removableKeyword,$removableKeywordToggleHash);
	foreach  $removableKeywordHash  (values %$filetype_Keyword_Simplyremove_Hash){
		#print "\n\t Dump of removableKeywordHash", Dumper($removableKeywordHash);
		#foreach $removableKeyword (values %$removableKeywordHash){
		while (($removableKeyword,$removableKeywordToggleHash) = each(%$removableKeywordHash)){
			#print "\n\t Dump of removableKeyword ONLY ", Dumper($removableKeyword);
			#if($filetype_Keyword_Simplyremove_Hash->{$filetype}->{$removableKeyword}->{$HDU_toggle} eq 'extension')
			if(exists $removableKeywordToggleHash->{$HDU_toggle})
			{
				Astro::FITS::CFITSIO::fits_delete_key($outfptr, $removableKeyword, $status);
				if($debug){
					print "\n\t\t deleting keyword $removableKeyword with status $status";
				}
			}
			#print "\n\t 11111111 skipping keyword ",$removableKeyword;
		}
	}
	#if ($HDU_toggle eq 'extension'){
	#	print "returning"; return;
	#}
#	print "\n the header loop new ",Dumper($headers);
    # Split each line of the primary header list along the | mark and write it to the primary header
    foreach my $line (@{$header_loop})
    {
	($keyword, $datatype, $value, $comment ) = split(/\|/, $line);	
	$keyword =~ s/^\s+|\s+$//g;
	$datatype =~ s/^\s+|\s+$//g; 
	$value =~ s/^\s+|\s+$//g; 
	$comment =~ s/^\s+|\s+$//g;
	#print "\n the datatype initially is $datatype for line $line ";

	#TSTRING, TLOGICAL (== int), TBYTE, TSHORT, TUSHORT, TINT, TUINT, TLONG, TLONGLONG, TULONG, TFLOAT, TDOUBLE.	
	if ($datatype =~ /tstring/i){
		#print "\n\t\t DATATYPE SETTING string";
		$empty_val = '';
	}elsif( $datatype =~ /tlogical/i){
	#	print "\n\t\t DATATYPE SETTING logical";
		# Convert true or false values
		if ($value eq "T" ) {$value = 1;}
		elsif ($value eq "F" ) {$value = 0;}
		else{$value = 0;}
	}elsif($datatype =~ /tshort|tushort|tint|tuint|tlong|tlonglong|tulong|tfloat|tdouble/i){
	#	print "\n\t\t DATATYPE SETTING all numbers";
		$empty_val = 0;
	}else{
	#	print "\n\t\t DATATYPE SETTING all OTHERS";
		$empty_val = undef;
	}
	

	# Convert true or false values
	if ($value eq "T" ) {$value = 1;}
	elsif ($value eq "F" ) {$value = 0;}
	$datatype = eval $datatype;




	###
	#	In the if elses below, we try and populate the new HDU in the output file with the right keyword value pair. In case the keyword doesn't already exist in the current HDU of the input file, we try and look for it in different places in a specific order:
	# 	1 we look into the Image HDU
	# 	2 we look into the current HDU itself
	# 	3 we look into values from the DB
	# 	4 we mark it as Not Yet Available since we couldn't find it anywhere
	###

#	print Dumper($HDU_keywordHashref_new);
	if (exists $filetype_Keyword_eval_Hash->{$filetype}->{$keyword}->{$HDU_toggle}){
		$value = calcKeywordVal($filetype,$keyword,$HDU_toggle);
		#eval{$filetype_Keyword_eval_Hash->{$filetype}->{$keyword}->{$HDU_toggle}->{'val'}};
		$comment = $filetype_Keyword_eval_Hash->{$filetype}->{$keyword}->{$HDU_toggle}->{'comment'};
	#	print "\n\t\t EVAL $nextend", $filetype_Keyword_eval_Hash->{$filetype}->{$keyword}->{$HDU_toggle}->{'val'}," the value $value";
	}
	###
	#	If the value exists in simply copy hashref, we just need to copy it here and move on. This is done because the keyword only plays a small role in conforming to a standard fits file format
	###
	elsif(exists $filetype_Keyword_Simplycopy_Hash->{$filetype}->{$keyword}->{$HDU_toggle}){

		$value = $filetype_Keyword_Simplycopy_Hash->{$filetype}->{$keyword}->{$HDU_toggle}->{'val'};
		$comment = $filetype_Keyword_Simplycopy_Hash->{$filetype}->{$keyword}->{$HDU_toggle}->{'comment'};
	#	print "\n\t\t SIMPLY COPY";
		#print "\n\t ###### replacing keyword: $keyword with value from Simply Copy Hashref. HDU Toggle $HDU_toggle final val: $value and comment $comment";

	}
	###
	#	If the keyword requsted exists in the current HDU, we can go ahead and write it.
	###
	elsif(exists $HDU_keywordHashref_new->{$keyword}){
		$value = $HDU_keywordHashref_new->{$keyword}->{'val'};
		$comment = $HDU_keywordHashref_new->{$keyword}->{'comment'};
	#	print "\n\t\t CURRENT HDU REPLACE";
		#print "\n\t 11111111 replacing keyword: $keyword with value from keyword. final val: $value and comment $comment";
	}
	###
	#	If the keyword requsted does not exist in the current HDU but exists in the mapping Hashref we can go find out the mapping keyword and put it's value in to this keyword. This will happen when we are say working on Mask or Weight HDU and we need a specific keyword to be populated, which the current input file's Weight HDU does not have. In this case, we will refer to the Image HDU to see if this value exists there.
	###
	elsif(exists $ImageKeywordHashref->{$keyword}){
	#	print "\n\t\t IMAGE HDU REPLACE";

		$value = $ImageKeywordHashref->{$keyword}->{'val'};
		$comment = $ImageKeywordHashref->{$keyword}->{'comment'};
		#$comment = $HDU_keywordHashref_new->{$filetype_Keyword_Image_Mapping_Hash->{$filetype}->{$keyword}}->{'comment'};
		#print "\n\t 0000000 Mapping the keyword from Image HDU: $keyword : final val: $value and comment $comment";
	}
	###
	#	If the keyword requsted does not exist in the current HDU and Image Hashref,  but exists in the mapping Hashref for this HDU we can go find out the mapping keyword and put it's value in to this keyword.
	###
	elsif(exists $filetype_Keyword_Mapping_Hash->{$filetype}->{$keyword}){
	#	print "\n\t\t KEYWORD MAPPING HASH";

		if ($filetype_Keyword_Mapping_Hash->{$filetype}->{$keyword}->{'source'} eq 'image'){
			$value = $ImageKeywordHashref->{$keyword}->{'val'};
			$comment = $ImageKeywordHashref->{$keyword}->{'comment'};
		}
		else{
			$value = $HDU_keywordHashref_new->{$filetype_Keyword_Mapping_Hash->{$filetype}->{$keyword}->{'keyword'}}->{'val'};
			$comment = $HDU_keywordHashref_new->{$filetype_Keyword_Mapping_Hash->{$filetype}->{$keyword}->{'keyword'}}->{'comment'};
		}
		#$comment = $HDU_keywordHashref_new->{$filetype_Keyword_Image_Mapping_Hash->{$filetype}->{$keyword}}->{'comment'};
		#print "\n\t 0000000 Mapping the keyword from Image HDU: $keyword : final val: $value and comment $comment";
		#$value = $HDU_keywordHashref_new->{$filetype_Keyword_Mapping_Hash->{$filetype}->{$keyword}}->{'val'};
		#$comment = $HDU_keywordHashref_new->{$filetype_Keyword_Mapping_Hash->{$filetype}->{$keyword}}->{'comment'};
		#print "\n\t 22222222 Mapping the keyword: $keyword from Image : final val: $value and comment $comment";
	}
	###
	#	If the keyword requsted does not exist in the current HDU but exists in the Database table Hashref, we can go find out the mapping keyword and put it's value in to this keyword.
	###
	elsif(exists $filetype_Keyword_DB_Hash->{$filetype}->{$keyword}){
	#	print "\n\t\t DB HASH";

		$value = $HDU_keywordHashref_new->{$filetype_Keyword_DB_Hash->{$filetype}->{$keyword}}->{'val'};
		$comment = $HDU_keywordHashref_new->{$filetype_Keyword_DB_Hash->{$filetype}->{$keyword}}->{'comment'};
		#print "\n\t 22222222 Mapping the keyword: $keyword : final val: $value and comment $comment";
	}
	###
	#	If we don't have a value for this keyword currently either in the existing keywords in the HDU of DESDM files, nor in the mapped keywords, we need to mark that keyword as a 'Not Yet Available'. so that NOAO knows that this keyword will be coming up later
	###
	else{
	#	print "\n\t\t NULL";
		$value = $empty_val;# changed this from 'Not Yet Available' in the value to blank. This was causing inconsistency while dealing with those keyword whose datatype is not string. $HDU_keywordHashref_new->{$keyword}->{'val'};
		$comment = 'Not Yet Available';#$HDU_keywordHashref_new->{$keyword}->{'comment'};
		#print "\n\t\t 33333333 NULLifying the keyword: $keyword : final val: $value and comment $comment";
	}
	if($debug){
		#print "\n\t\t keyword $keyword , comment $comment, datatype $datatype ";
		if(defined $value){
			#print " value $value";
		}
	}
	# Convert true or false values
	if (defined  $value && $value eq "T" ) {$value = 1;}
	elsif (defined  $value  && $value eq "F" ) {$value = 0;}
	$datatype = eval $datatype;
		
	#Write/replace each keyword to the header
	$status =0;
	Astro::FITS::CFITSIO::fits_update_key($outfptr,$datatype, $keyword, $value, $comment, $status);
	if ($debug && $status != 0 ) {print "ERROR $status: Failed to write keyword $keyword to the primary header\n";}
	
	#Astro::FITS::CFITSIO::fits_get_hdrspace($outfptr, my $keysexist, my $morekeys, my $status_hdrspace);
	#print "\n\t\t headerspace result: keys exist: $keysexist, morekeys: $morekeys, status: $status_hdrspace";
	
    }


}

sub create_hash
{
    my ($infptr) = @_;
    
    # Get the number of header keywords
    # Create a hash where $keyword|$datatype => ($value)
    	
    my $keysexist;
    my $keynum;
    
    while (fits_get_hdrpos($infptr, $keysexist, $keynum, $status))
    {
	print $keynum;
    }
    
}
    
    
sub move_to_last_hdu
{
    my ($outfptr, $out_fits_filename) = @_;
    
    my $total_out_hdu;
    my $total_out_hdu_fast;
    my $status = 0;
    my $in_chdu_type;

#    $total_out_hdu = last_hdu_for_file($outfptr);
    $total_out_hdu_fast = last_hdu_for_file_fast($outfptr, $out_fits_filename);
#    if ($debug) {print "Total HDUs as determined by last_hdu_for_file is $total_out_hdu\n";}
    
#    if($total_out_hdu_fast > 0|| $total_out_hdu > 0)
    if($total_out_hdu_fast > 0)
    {
	    if ($debug) {print "\n\t\t Moving to last HDU. Total HDUs in file $out_fits_filename as determined by last_hdu_for_file_fast is $total_out_hdu_fast\n";}
	$status=0;
#	Astro::FITS::CFITSIO::fits_movabs_hdu($outfptr, $total_out_hdu, $in_chdu_type, $status);
	Astro::FITS::CFITSIO::fits_movabs_hdu($outfptr, $total_out_hdu_fast, $in_chdu_type, $status);
	if ($status) {print "ERROR $status moving to last HDU failed!\n";}
    }

}


###
# this function parses through a current HDU of a fits file and creates a hashref of keywords and their values in the fits file. Depending on when this function is called, its context will either be the primary HDU or an extension HDU
### 
sub getHeaderHash
{
	my ($infptr) = @_;
	my ($keysexist, $morekeys, $status,$card,$HDU_keywordHashref,$keyname,$value,$comment);
	###
        # Read the input file's Current HDU and get all the keywords from it. These keywords will then be used as a master list, fromwhich we will filter out the keywords which are ultimately wanted in the converted file. There will also be a sub section where some keywords from the original file will need to be translated into another keyword wanted in the converted file.
        ###

        Astro::FITS::CFITSIO::fits_get_hdrspace($infptr, $keysexist, $morekeys, $status);
        for (my $iter_keywords = 1; $iter_keywords <= $keysexist; $iter_keywords++)
        {
                #Astro::FITS::CFITSIO::fits_read_record($infptr, $iter_keywords,  $card, $status);
		Astro::FITS::CFITSIO::fits_read_keyn($infptr, $iter_keywords, $keyname, $value,$comment, $status);
                #if($card =~ /\s*(\S*)\s*=\s*(.*)\/?(.*)$/){
                #print "\n MATCHED CARD $card : $1 : $2 : $3";
                $HDU_keywordHashref->{$keyname}->{'val'} = $value;
                $HDU_keywordHashref->{$keyname}->{'comment'} = $comment;
                #}
        }

	return $HDU_keywordHashref;

}



sub append_hdus
{
    #my ($in_fits_filename, $in_file_type, %outfptr_hash) = @_;
    my ($in_fits_filename, $in_file_type, $outfptr_hash) = @_;
    my $i;


	#print "\n in the append_hdus the outfptr is ,", Dumper(%outfptr_hash);
    my $out_fits_file;
    my $out_fits_filename;
    my $outfptr=0;
    my $total_out_hdu;
    my $total_in_hdu = 0;
    my $cur_in_hdu_num=0;
    my $infptr = 0;
    my $keysexist;
    my $morekeys;
    my $status;
    my $card;
    my $HDU_keywordHashref;
    my $temp_hdus;	
    #Open the input file
    $infptr = open_fits_file($in_fits_filename);

    #Loop through and copy each header into the correct file
    Astro::FITS::CFITSIO::fits_get_num_hdus($infptr, $total_in_hdu, $status);
    #for ($i = 1; $i< $total_in_hdu; $i++ ) Changing the iteration to start from 0. this might affect the DECam reduced files
    for ($i = 0; $i< $total_in_hdu-$numPrimHeaders; $i++ )
    {
    	if ($debug)
	{
	    print "\n\t\t  ### Looping through HDU number $i of total $total_in_hdu HDUS in input file $in_fits_filename ###\n";
	}

	#Name the output file
	$out_fits_filename = name_of_output_file_red($in_fits_filename, $infptr);
	#$outfptr = $outfptr_hash{$out_fits_filename};
	$outfptr = $outfptr_hash->{$out_fits_filename};
	if ($debug) {print "\n\t\t Copying HDU to output file  $out_fits_filename, fptr is $outfptr \n";}

	###
	# Read the input file's Current HDU and get all the keywords from it. These keywords will then be used as a master list, fromwhich we will filter out the keywords which are ultimately wanted in the converted file. There will also be a sub section where some keywords from the original file will need to be translated into another keyword wanted in the converted file.
	###
	
	#Astro::FITS::CFITSIO::fits_get_hdrspace($infptr, $keysexist, $morekeys, $status);
	#for (my $iter_keywords = 1; $iter_keywords <= $keysexist; $iter_keywords++)
	#{
	#	Astro::FITS::CFITSIO::fits_read_record($infptr, $iter_keywords,  $card, $status);
	#	print "\n the card value ",$card;
	#	if($card =~ /\s*(\S*)\s*=\s*(\S)/){
	#	print "\n MATCHED CARD";
	#	$HDU_keywordHashref->{$1} = $2;
	#	}
	#}

	$status=0;

#	my $hdunum; Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr, $hdunum); 
#	if ($debug){print "\n\t\t the current HDU number in append_hdu BEFORE COPY is $hdunum";}

	if($debug){
		Astro::FITS::CFITSIO::fits_get_hdu_num($infptr,$temp_hdus);
		print "\n\t\t copying HDU Number $temp_hdus in input file ";
		Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr,$temp_hdus);
		print "\n\t\t to HDU Number $temp_hdus of output file ";
	}
	Astro::FITS::CFITSIO::fits_copy_hdu($infptr, $outfptr, 0, $status);
	if ($debug && $status ==0) {print "\t\t HDU copied successfully!\n";} 
	elsif ($status) { print "ERROR $status in copying HDU\n";}

#    Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr, $hdunum);
#	if ($debug){ print "\n\t\t the current HDU number in append_hdu AFTER COPY is $hdunum";}

	###
	# Get the hashref of the keywords and their values for the current HDU. Then use this hashref to overwrite the keyword's values from sample_extension_headers file.  
	###
	$HDU_keywordHashref = getHeaderHash($infptr);

	###
	# If this is the Image HDU, we will need to use it's values for substitution in some cases. Keep the keyword-value pair to use it 
	###
	if($HDU_keywordHashref->{'DES_EXT'} eq 'IMAGE'){
		
		$ImageKeywordHashref = $HDU_keywordHashref;
	}

	###
	# After the HDU has been copied from the input file, now go and modify/ add headers to the HDU as per the requirements given in the requirements excel sheet.
	###
	if ($in_file_type eq "red") {create_extension_header_red($outfptr,$HDU_keywordHashref,$in_file_type);}
	if ($in_file_type eq "biascor") {create_extension_header_red($outfptr,$HDU_keywordHashref,$in_file_type);}
	if ($in_file_type eq "flatcor") {create_extension_header_red($outfptr,$HDU_keywordHashref,$in_file_type);}
	if ($in_file_type eq "supersky") {create_extension_header_red($outfptr,$HDU_keywordHashref,$in_file_type);}
#	Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr, $hdunum); print "\n the current HDU number in append_hdu function is  $hdunum";

	#If we are not at the first HDU in the output file, move to the last one to avoid overwriting 
	if($debug){print "\n\t\t Moving to the Last HDU... ";}
	move_to_last_hdu($outfptr, $out_fits_filename);
	if($debug){
		Astro::FITS::CFITSIO::fits_get_hdu_num($infptr,$temp_hdus);
		print "\n\t\t copied HDU Number $temp_hdus in the reduced input file ";
		Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr,$temp_hdus);
		print "\n\t\t to HDU Number $temp_hdus of output reduced file ";
	}
	
	#Copy the HDU from the input to the output file
	#fits_create_hdu / ffcrhd
	#      (fitsfile *fptr, > int *status)


	#Increment the HDU hash to account for another HDU
	$last_hdu_hash{$out_fits_filename} +=1;

	#Move to the next input HDU
	Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);

    }
    
    #Close the input file
    $status=0;
    Astro::FITS::CFITSIO::fits_close_file($infptr, $status);
	if ($status) {print "ERROR $status closing input file failed!\n";}
    elsif ($debug) {print "Closed input file successfully!\n";}

	#open (my $FH_ERR, '>listerr.log');
	#open (my $FH_out, '>listout.log');
	#my $fh;
	#my $pid = open3($fh, $FH_out, $FH_ERR,"/home/ricardoc/bin/listhead $out_fits_filename ");
	#my @args = ("/home/ricardoc/bin/listhead ","$out_fits_filename");
	##system  (@args);
	#    if ($? == -1) {
    #prin#t "failed to execute: $!\n";
    #}
    #elsif ($? & 127) {
    #printf "child died with signal %d, %s coredump\n",
    #($? & 127), ($? & 128) ? 'with' : 'without';
    #}
    #else {
    #printf "child exited with value %d\n", $? >> 8;
    #}
}

sub append_hdus_cat
{
    #my ($in_fits_filename, %outfptr_hash) = @_;
    my ($in_fits_filename, $outfptr_hash) = @_;
    my $i;
    my $in_chdu_type;
    my $out_fits_file;
    my $out_fits_filename;
    my $outfptr=0;
    my $total_out_hdu;
    my $total_in_hdu = 0;
    my $infptr = 0;
    my $temp_hdus = 0; 
    #Open the input file
    $infptr = open_fits_file($in_fits_filename);
    
    #Loop through and copy each header into the correct file
    Astro::FITS::CFITSIO::fits_get_num_hdus($infptr, $total_in_hdu, $status);
	if ($debug){
		print "\n\t\t there are $total_in_hdu HDUs in input file";
	}

    #Skip the Primary header on the input catalog file                                                                             
    $status=0;
    Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);
    if ($status) {print "\n\t\tERROR $status moving past the primary header failed!\n";}
    elsif ($debug) {print "\n\t\t Appending HDUs in catalog file. Skipping the primary HDU of input file. \n";}
    $total_in_hdu--;


    #Subtract one from the total number of HDUs in the input file to account for the primary header
    if ($debug) {
		print "\n\t\t before Looping through iterations subtracting $numPrimHeaders in catalog file";
    }
    for ($i = 0; $i<= $total_in_hdu-$numPrimHeaders; $i++ )
    {
    	if ($debug)
	{
	    print "\n\t\t  ### Looping through iteration $i for  input file $in_fits_filename with reduced total HDUs $total_in_hdu ###\n";
	}

	#Find which output file we are operating on and get the appropriate filehandle
	$out_fits_filename = name_of_output_file_cat($in_fits_filename, $infptr);
	if ($debug) {print "\n\t\t Copying HDU to output file  $out_fits_filename\n";}
	#$outfptr = $outfptr_hash{$out_fits_filename};
	$outfptr = $outfptr_hash->{$out_fits_filename};

	    Astro::FITS::CFITSIO::fits_get_num_hdus($outfptr, $temp_hdus, $status);
		if($debug){
			print "\n\t\t current number of HDUs in ouput file: $temp_hdus ";
		}
# 	#Open the output fits file or create it if it does not exist
# 	if ( -e $out_fits_filename)
# 	{
# 	    if ($debug && $dev_flag) { print "$out_fits_filename exists, opening file...\n"; }
# 	    $outfptr = open_fits_file($out_fits_filename);
# 	}

# 	else 
# 	{
# 	    if ($debug) { print "$out_fits_filename does NOT exist, creating file...\n"; }
# 	    $outfptr = create_primary_header_cat($out_fits_filename);

# 	    #Increment the HDU hash to account for another HDU
# 	    $last_hdu_hash{$out_fits_filename} +=1;
# 	}


	
	#If we are not at the first HDU in the output file, move to the last one to avoid overwtiting 
	if($debug){print "\n\t\t Moving to the Last HDU... ";}
	move_to_last_hdu($outfptr, $out_fits_filename);
	
	if($debug){
		Astro::FITS::CFITSIO::fits_get_hdu_num($infptr,$temp_hdus);
		print "\n\t\t copying HDU Number $temp_hdus in input catalog file ";
		Astro::FITS::CFITSIO::fits_get_hdu_num($outfptr,$temp_hdus);
		print "\n\t\t to HDU Number $temp_hdus of output  catalog.. file ";
	}
	#Copy the HDU from the input to the output file
	Astro::FITS::CFITSIO::fits_copy_hdu($infptr, $outfptr, 0, $status);
	if ($debug && $status eq 0) {print "\n\t\t HDU copied successfully!\n";} 
	elsif ($status) { print "ERROR $status in copying HDU\n";}

	#Increment the HDU hash to account for another 
	$last_hdu_hash{$out_fits_filename} +=1;

# 	if ($i < $total_in_hdu-1)
# 	{
# 	    $status=0;
# 	    Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);
# 	    if ($status) {print "ERROR $status moving to the next input HDU failed!\n";}
# 	    elsif ($debug) {print "Moved to the next input HDU successfully!\n";}
# 	}

	Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);

# 	#Close the output file
# 	$status=0;
# 	Astro::FITS::CFITSIO::fits_close_file($outfptr, $status);
# 	if ($status) {print "ERROR $status closing output file failed!\n";}
# 	elsif ($debug) {print "Closed output file successfully!\n";}
    }

    #Close the input file
    $status=0;
    Astro::FITS::CFITSIO::fits_close_file($infptr, $status);
    if ($status) {print "ERROR $status closing input file failed!\n";}
    elsif ($debug) {print "Closed input file successfully!\n";}
}



sub append_hdus_red
{
    #my ($in_fits_filename, %outfptr_hash) = @_;
    my ($in_fits_filename, $outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    #append_hdus($in_fits_filename, "red", %outfptr_hash);
    append_hdus($in_fits_filename, "red", $outfptr_hash);
}


sub append_hdus_biascor
{
    #my ($in_fits_filename, %outfptr_hash) = @_;
    my ($in_fits_filename, $outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    #append_hdus($in_fits_filename, "biascor", %outfptr_hash);
    append_hdus($in_fits_filename, "biascor", $outfptr_hash);
}

sub append_hdus_flatcor
{
    #my ($in_fits_filename, %outfptr_hash) = @_;
    my ($in_fits_filename, $outfptr_hash) = @_;

	#print "\n the outfptr hash ",Dumper(%outfptr_hash);
    # Reuse the reduced function because both files are laid out similarly
    #append_hdus($in_fits_filename, "flatcor", %outfptr_hash);
    append_hdus($in_fits_filename, "flatcor", $outfptr_hash);
}


sub append_hdus_supersky
{
    #my ($in_fits_filename, %outfptr_hash) = @_;
    my ($in_fits_filename, $outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    #append_hdus($in_fits_filename, "supersky", %outfptr_hash);
    append_hdus($in_fits_filename, "supersky", $outfptr_hash);
}

sub append_hdus_illumcor
{
    #my ($in_fits_filename, %outfptr_hash) = @_;
    my ($in_fits_filename, $outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    #append_hdus($in_fits_filename, "illumcor", %outfptr_hash);
    append_hdus($in_fits_filename, "illumcor", $outfptr_hash);
}

sub append_hdus_fringecor
{
    #my ($in_fits_filename, %outfptr_hash) = @_;
    my ($in_fits_filename, $outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    #append_hdus($in_fits_filename, "fringecor", %outfptr_hash);
    append_hdus($in_fits_filename, "fringecor", $outfptr_hash);
}

sub setup {

	my ($detectorVal,$filetype,$fileDetailsVal,$globalVars) = @_;


	$detector = $detectorVal;
	$fileDetails = $fileDetailsVal;
	$outputdir = $globalVars->{'outputdir'};
#	print "\n\t\t the detector in setup is ",$detector;
	
	if ($detector =~ /mosaic/i){
		$nextend = 8;
#		print "\n\t\t setting detector to mosaic, $nextend";
	}else{
		$nextend = 62;
	}
	
	if($detector =~ /mosaic/i && $filetype =~ /cat/i){
		$numPrimHeaders = 1;
	}
	elsif($detector =~ /mosaic/i && $filetype =~ /red/i){
		$numPrimHeaders = 1;
	}
	elsif($detector !~ /mosaic/i && $filetype =~ /red/i){
		$numPrimHeaders = 1;
	}
	elsif($detector =~ /mosaic/i && $filetype =~ /cat/i){
		$numPrimHeaders = 1;
	}
	elsif($detector =~ /decam/i && $filetype =~ /biascor/i){
		$numPrimHeaders = 0;
	}
	elsif($detector =~ /decam/i && $filetype =~ /flatcor/i){
		$numPrimHeaders = 0;
	}
	elsif($detector =~ /decam/i && $filetype =~ /fringecor/i){
		$numPrimHeaders = 1;
	}
	elsif($detector =~ /decam/i && $filetype =~ /illumcor/i){
		$numPrimHeaders = 1;
	}
	elsif($detector =~ /decam/i && $filetype =~ /supersky/i){
		$numPrimHeaders = 1;
	}

}

sub calcKeywordVal {

	my ($filetype, $keyword, $HDU_toggle) = @_;

	my ($value);
	if($keyword =~ /nextend/i){
		if ($detector =~ /mosaic/i){
			$value = 8;
		}else{
			$value = 62;
		}
	}	

	if($keyword =~ /expnum/i){
		$value = $fileDetails->{'EXPOSURENAME'}	;
	}

	return $value;

}


# Preloaded methods go here.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

FITS_Header - Perl extension for converting the DESDM files into NSA format. 

=head1 SYNOPSIS

  use NSAConverter;

=head1 DESCRIPTION

NSAConverter is the main library of functions used to convert a DESDM file into a format called NSA File format, described by NOAO. 
NSAConverter works on a set of 62 DESDM files at at time, one each for each of the 62 CCDs in the DECam. It slices off the different HDU types (Image, Weight, Mask) contained in each of these 62 files into a new file of their own. So by the end of the conversion job, we end up with 3 new files (in case of reduced files for instance), for Image, Weight and Mask, each containing 62 HDUs in them corresponding to each CCD. Think of it like transposing a matrix: where we transpose rows into columns.



=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

A. U. Thor, E<lt>asutrave@localdomainE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by A. U. Thor

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
