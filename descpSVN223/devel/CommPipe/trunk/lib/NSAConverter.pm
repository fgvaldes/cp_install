package NSAConverter;

# This module contains funtions that help process FITS files by splitting them up  
# into the correct format

use 5.008008;
use strict;
use warnings;

use Astro::FITS::CFITSIO; 
use Astro::FITS::CFITSIO qw( :constants);
use Carp;
use Getopt::Long;
use Cwd;

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use FITS_Header ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
split_in_filename
name_of_output_file_red
name_of_output_file_cat
open_fits_file_1
create_primary_header_red
create_primary_header_cat
create_extension_header_red
move_to_last_hdu
append_hdus_red
append_hdus_cat
append_hdus_flatcor
append_hdus_biascor
append_hdus_supersky
append_hdus_illumcor
append_hdus_fringecor
last_hdu_for_file_fast
last_hdu_for_file
usage
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.01';

my $in_fits_filename;
my $status=0;
my $infptr=0;

our $debug = 0;

my @location_array;

my $project_name =0;
my $class =0;
my $run =0;
my $type_file =0;
my $exposure =0;
my $in_chdu_type="";

my %last_hdu_hash =();

#GET RID OF THIS LATER
my $dev_flag = 0;

sub last_hdu_for_file
{
    # This function takes the pointer to the output file and returns the
    # number of the last HDU in the file by calling a CFITSIO function
    # Because of this library call, this funtion is relatively slow
    # NOTE: Once the bugs have been worked out, it will be prefereable to use
    # last_hdu_for_file_fast, instead of this function

    my ($outfptr) = @_;
    my $total_out_hdu;
    $status = 0;
    Astro::FITS::CFITSIO::fits_get_num_hdus ($outfptr, $total_out_hdu, $status );	
    if($status) {print "ERROR $status Could not get number of HDUs in the output file\n";}
    elsif ($debug && $dev_flag) {print "Successfully obtained $total_out_hdu from the file\n";}

    return $total_out_hdu;
}


sub last_hdu_for_file_fast
{
    # This function returns the number of the last HDU in the file, by using a hash lookup
    # NOTE: This function might still have some bugs

    my ($outfptr, $out_fits_filename) = @_;
    
    # If the file exists in the hash, increment its number of HDUs as one more will be appended,
    # and return the old number of HDUs

    if (defined $last_hdu_hash{$out_fits_filename})
    {
	return $last_hdu_hash{$out_fits_filename};
    }

    # If the file does not exist, then find out how many files exist in the 
    else 
    {
	if ($debug) {print "last_hdu_type{$out_fits_filename} not found, creating new entry...\n";}

	$status = 0;
	Astro::FITS::CFITSIO::fits_get_num_hdus ($outfptr, $last_hdu_hash{$out_fits_filename}, $status );	
	if($status) {print "ERROR $status Could not get number of HDUs in the output file\n";}
	elsif ($debug) {print "Successfully obtained HDUs=$last_hdu_hash{$out_fits_filename} from the file\n";}
	
	return $last_hdu_hash{$out_fits_filename};
    }
    
}

sub split_in_filename
{
    my ($in_fits_filename)=@_;

    chomp($in_fits_filename);
    my @location_array= split('/', $in_fits_filename);

    return @location_array;
}


#This function creates the name for the output fitsfile based on the input
sub name_of_output_file_red
{
    my ($in_fits_filename, $infptr) = @_ ;

    $status = 0;
    my $in_chdu_type = "";
    
    #DES_EXT holds the correct type of the input HDU
    Astro::FITS::CFITSIO::fits_read_keyword ($infptr, "DES_EXT", $in_chdu_type, my $comment, $status);
    if ($in_chdu_type eq "") 
    {
	#Skip the primary header on the input fits file
	$status=0;
	Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);
	if ($status) {print "ERROR $status moving past the primary header failed!\n";}
	elsif ($debug && $dev_flag) {print "Moved past the primary successfully!\n";}
	
	# Read the file type again
	Astro::FITS::CFITSIO::fits_read_keyword ($infptr, "DES_EXT", $in_chdu_type, my $comment, $status);
    }
    elsif ($debug && $dev_flag) {print "DES_EXT=$in_chdu_type\n";}
    $in_chdu_type= substr $in_chdu_type, 1, -1;
    $in_chdu_type =~ s/\s+$//;
    
    my $filename = "";
    my $path;
    if ( substr($in_fits_filename, 0, 1) eq '/')
    {
	$filename = substr $in_fits_filename, rindex($in_fits_filename, '/') + 1;
	$path = substr $in_fits_filename, 0,  rindex($in_fits_filename, '/');
    }

    else 
    {
	print "ERROR $in_fits_filename is not a valid path to an input file\n";
    }

    #Strip the index from the filename
    $filename = substr($filename, 0, rindex ($filename, "_")) . substr($filename, rindex ($filename, "_") +3);

    my $out_fits_filename = "$path/type_$in_chdu_type.$filename";

    if ($debug && $dev_flag) 
    {
	print "Input file is called $in_fits_filename\n";
	print "Output file will be placed in directory $path\n";
    }

    return $out_fits_filename;
}

#Create the name for the output fitsfile based on the input
sub name_of_output_file_cat
{
    my ($in_fits_filename, $infptr) = @_ ;

    $status = 0;
    my $in_chdu_type = "";

    #The extension of the file will be prefixed to the filename
    $status = 0;
    Astro::FITS::CFITSIO::fits_read_keyword ($infptr, "EXTNAME", $in_chdu_type, my $comment, $status);
    if ($status) {print "ERROR $status Reading keyword EXTNAME failed\n";}
    if ($debug) {print "EXTNAME=$in_chdu_type\n";}
    # Remove quotes and whitespace from CHDU type
    $in_chdu_type = substr $in_chdu_type, 1, -1;
    $in_chdu_type =~ s/\s+$//;
    
    my $filename = "";
    my $path;
    if ( substr($in_fits_filename, 0, 1) eq '/')
    {
	$filename = substr $in_fits_filename, rindex($in_fits_filename, '/') + 1;
	$path = substr $in_fits_filename, 0,  rindex($in_fits_filename, '/');
    }

    else 
    {
	print "ERROR $in_fits_filename is not a valid path to an input file\n";
    }

    
    #Strip the index from the filename
    $filename = substr($filename, 0, rindex ($filename, "_")-3). substr($filename, rindex ($filename, "_"));
    my $out_fits_filename = "$path/type_$in_chdu_type.$filename";

    if ($debug && $dev_flag) 
    {
	print "Input file is called $in_fits_filename\n";
	print "Output file will be placed in directory $path\n";
    }

    return $out_fits_filename;
}


#Open the output file corresponding to the input file
sub open_fits_file_1
{
    my ($fits_filename) = @_;
    my $fptr;

    $status =0;
    Astro::FITS::CFITSIO::fits_open_file($fptr, "$fits_filename", Astro::FITS::CFITSIO::READWRITE(), $status);
    if ($debug==1 && $status ==0 && $dev_flag) {print "File $fits_filename opened sucessfully!\n";} 
    if ($status)  {print "ERROR $status File $fits_filename failed to open sucessfully!\n";} 

    return $fptr;
}


sub create_primary_header_red
{
    if ($debug) {print "Writing primary header...\n";}
    my ($outfptr, @primary_headers) = @_;

    if (scalar(@primary_headers)==0)
    {
	open FILE, '/home/asutrave/perl/NSAConverter/lib/primary_headers_red';
	@primary_headers = <FILE>;

    }
    create_header($outfptr, @primary_headers);
}

sub create_primary_header_cat
{
    if ($debug) {print "Writing primary header for cat file...\n";}
    my ($outfptr, @primary_headers) = @_;

    if (scalar(@primary_headers)==0)
    {
	open FILE, '/home/asutrpave/perl/NSAConverter/lib/primary_headers_cat';
	@primary_headers = <FILE>;

    }
    create_header($outfptr, @primary_headers);
}

sub create_extension_header_red
{
    if ($debug && $dev_flag) {print "Writing extension header for reduced file...\n";}
    my ($outfptr, @primary_headers) = @_;
    if (scalar(@primary_headers)==0)
    {
	open FILE, '/home/asutrave/perl/NSAConverter/lib/extension_headers_red';
	@primary_headers = <FILE>;

    }
    create_header($outfptr, @primary_headers);
}

sub create_header
{
    my ($outfptr, @headers) = @_;
    if (scalar(@headers) == 0)
    {
	print "WARNING: No header list passed, using reduced file primary list by default\n";
	open FILE, '/home/asutrave/perl/NSAConverter/lib/primary_headers_red';
	@headers = <FILE>;
    }

    my $keyword;
    my $datatype;
    my $value;
    my $comment;

# Irrelevent now
#    if ( !(-e $out_fits_filename))
#    {

# 	#Create the file
# 	$status=0;
# 	Astro::FITS::CFITSIO::fits_create_file($outfptr, $out_fits_filename, $status);
# 	if ($debug==1 && $status==0 && $dev_flag) { print "File $out_fits_filename created sucessfully!\n";} 
#	elsif ($status) { print "ERROR $status File $out_fits_filename failed to create sucessfully!\n";}
#	print "File $out_fits_filename failed to create sucessfully!\n";
#    }
    

    # Split each line of the primary header list along the | mark and write it to the primary header
    foreach my $line (@headers)
    {
	($keyword, $datatype, $value, $comment ) = split(/\|/, $line);	
	$keyword =~ s/^\s+|\s+$//g;
	$datatype =~ s/^\s+|\s+$//g; 
	$value =~ s/^\s+|\s+$//g; 
	$comment =~ s/^\s+|\s+$//g;

	# Convert true or false values
	if ($value eq "T" ) {$value = 1;}
	elsif ($value eq "F" ) {$value = 0;}
	$datatype = eval $datatype;
	
	#Write each keyword to the header
	$status =0;
#	Astro::FITS::CFITSIO::fits_write_key($outfptr,$datatype, $keyword, $value, $comment, $status);
	if ($debug && $status) {print "ERROR $status: Failed to write keyword $keyword to the primary header\n";}
    }
}


sub move_to_last_hdu
{
    my ($outfptr, $out_fits_filename) = @_;
    
    my $total_out_hdu;
    my $total_out_hdu_fast;
    my $status = 0;
    my $in_chdu_type;

    $total_out_hdu = last_hdu_for_file($outfptr);
    $total_out_hdu_fast = last_hdu_for_file_fast($outfptr, $out_fits_filename);
    if ($debug) {print "Total HDUs as determined by last_hdu_for_file is $total_out_hdu\n";}
    if ($debug) {print "Total HDUs as determined by last_hdu_for_file_fast is $total_out_hdu_fast\n";}
    
    if($total_out_hdu_fast > 0|| $total_out_hdu > 0)
    {
	$status=0;
	Astro::FITS::CFITSIO::fits_movabs_hdu($outfptr, $total_out_hdu, $in_chdu_type, $status);
#	Astro::FITS::CFITSIO::fits_movabs_hdu($outfptr, $total_out_hdu_fast, $in_chdu_type, $status);
	if ($status) {print "ERROR $status moving to last HDU failed!\n";}
    }

}


sub append_hdus
{
    my ($in_fits_filename, $in_file_type, %outfptr_hash) = @_;
    my $i;

    my $out_fits_file;
    my $out_fits_filename;
    my $outfptr=0;
    my $total_out_hdu;
    my $total_in_hdu = 0;
    my $cur_in_hdu_num=0;
    my $infptr = 0;

    #Open the input file
    $infptr = open_fits_file_1($in_fits_filename);

    #Loop through and copy each header into the correct file
    Astro::FITS::CFITSIO::fits_get_num_hdus($infptr, $total_in_hdu, $status);
    for ($i = 1; $i< $total_in_hdu; $i++ )
    {
    	if ($debug)
	{
	    print "**************\n";
	    print "HDU $i for in_fits_file=$in_fits_filename\n";
	}

	#Name the output file
	$out_fits_filename = name_of_output_file_red($in_fits_filename, $infptr);
	$outfptr = $outfptr_hash{$out_fits_filename};

# 	#Open the output fits file or create it if it does not exist
# 	if ( -e $out_fits_filename)
# 	{
# 	    if ($debug) { print "$out_fits_filename exists, opening file...\n"; }
	    
# 	    # Open the file and write the extension header
# 	    if ($in_file_type eq "red") {$outfptr = create_extension_header_red($out_fits_filename);}
# # 	    if ($in_file_type eq "biascor") {$outfptr = create_extension_header_biascor($out_fits_filename);}
# # 	    if ($in_file_type eq "flatcor") {$outfptr = create_extension_header_flatcor($out_fits_filename);}
# #	    if ($in_file_type eq "supersky") {$outfptr = create_extension_header_supersky($out_fits_filename);}
# 	    if ($in_file_type eq "biascor") {$outfptr = create_extension_header_red($out_fits_filename);}
# 	    if ($in_file_type eq "flatcor") {$outfptr = create_extension_header_red($out_fits_filename);}
# 	    if ($in_file_type eq "supersky") {$outfptr = create_extension_header_red($out_fits_filename);}
# 	}

# 	else 
# 	{
# 	    if ($debug) { print "$out_fits_filename does NOT exist, creating file...\n"; }
# 	    if ($in_file_type eq "red") {$outfptr = create_primary_header_red($out_fits_filename);}
# # 	    if ($in_file_type eq "biascor") {$outfptr = create_primary_header_biascor($out_fits_filename);}
# # 	    if ($in_file_type eq "flatcor") {$outfptr = create_primary_header_flatcor($out_fits_filename);}
# #	    if ($in_file_type eq "supersky") {$outfptr = create_primary_header_supersky($out_fits_filename);}
# 	    if ($in_file_type eq "biascor") {$outfptr = create_primary_header_red($out_fits_filename);}
# 	    if ($in_file_type eq "flatcor") {$outfptr = create_primary_header_red($out_fits_filename);}
# 	    if ($in_file_type eq "supersky") {$outfptr = create_primary_header_red($out_fits_filename);}

# 	    # Increment the HDU hash to account for another HDU
# 	    $last_hdu_hash{$out_fits_filename} +=1;

# 	}


	if ($in_file_type eq "red") {create_extension_header_red($outfptr);}
# 	if ($in_file_type eq "biascor") {create_extension_header_biascor($outfptr);}
# 	if ($in_file_type eq "flatcor") {create_extension_header_flatcor($outfptr);}
# 	if ($in_file_type eq "supersky") {create_extension_header_supersky($outfptr);}
	if ($in_file_type eq "biascor") {create_extension_header_red($outfptr);}
	if ($in_file_type eq "flatcor") {create_extension_header_red($outfptr);}
	if ($in_file_type eq "supersky") {create_extension_header_red($outfptr);}

	#If we are not at the first HDU in the output file, move to the last one to avoid overwtiting 
	move_to_last_hdu($outfptr, $out_fits_filename);
	
	#Copy the HDU from the input to the output file
	$status=0;
	Astro::FITS::CFITSIO::fits_copy_hdu($infptr, $outfptr, 0, $status);
	if ($debug && $status ==0) {print "HDU copied successfully!\n";} 
	elsif ($status) { print "ERROR $status in copying HDU\n";}

	#Increment the HDU hash to account for another HDU
	$last_hdu_hash{$out_fits_filename} +=1;

# 	#Move to next HDU in the input file if it is not the last input file
# 	Astro::FITS::CFITSIO::fits_get_hdu_num ($infptr, $cur_in_hdu_num ); 
# 	if ($status) {print "ERROR $status Getting CHDU number failed!\n";}
# 	if ($cur_in_hdu_num < $total_in_hdu)
# 	{
# 	    $status=0;
# 	    Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);
# 	    if ($status) {print "ERROR $status moving to the next input HDU failed!\n";}
# 	    elsif ($debug && $dev_flag) {print "Moved to the next input HDU successfully!\n";}
# 	}

	#Move to the next input HDU
	Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);

# 	#Close the output file
# 	$status=0;
# 	Astro::FITS::CFITSIO::fits_close_file($outfptr, $status);
# 	if ($status) {print "ERROR $status closing output file failed!\n";}
# 	elsif ($debug && $dev_flag) {print "Closed output file successfully!\n";}

    }
    
    #Close the input file
    $status=0;
    Astro::FITS::CFITSIO::fits_close_file($infptr, $status);
    if ($status) {print "ERROR $status closing input file failed!\n";}
    elsif ($debug && $dev_flag) {print "Closed input file successfully!\n";}
}

sub append_hdus_cat
{
    my ($in_fits_filename, %outfptr_hash) = @_;
    my $i;
    my $in_chdu_type;
    my $out_fits_file;
    my $out_fits_filename;
    my $outfptr=0;
    my $total_out_hdu;
    my $total_in_hdu = 0;
    my $infptr = 0;
    
    #Open the input file
    $infptr = open_fits_file_1($in_fits_filename);
    
    #Loop through and copy each header into the correct file
    Astro::FITS::CFITSIO::fits_get_num_hdus($infptr, $total_in_hdu, $status);

    #Skip the Primary header on the input catalog file                                                                             
    $status=0;
    Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);
    if ($status) {print "ERROR $status moving past the primary header failed!\n";}
    elsif ($debug) {print "Moved past the primary successfully!\n";}
    $total_in_hdu--;


    #Subtract one from the total number of HDUs in the input file to account for the primary header
    for ($i = 1; $i<= $total_in_hdu-1; $i++ )
    {
    	if ($debug)
	{
	    print "**************\n";
	    print "Iter:$i for input=$in_fits_filename\n";
	}

	#Find which output file we are operating on and get the appropriate filehandle
	$out_fits_filename = name_of_output_file_cat($in_fits_filename, $infptr);
	$outfptr = $outfptr_hash{$out_fits_filename};

# 	#Open the output fits file or create it if it does not exist
# 	if ( -e $out_fits_filename)
# 	{
# 	    if ($debug && $dev_flag) { print "$out_fits_filename exists, opening file...\n"; }
# 	    $outfptr = open_fits_file_1($out_fits_filename);
# 	}

# 	else 
# 	{
# 	    if ($debug) { print "$out_fits_filename does NOT exist, creating file...\n"; }
# 	    $outfptr = create_primary_header_cat($out_fits_filename);

# 	    #Increment the HDU hash to account for another HDU
# 	    $last_hdu_hash{$out_fits_filename} +=1;
# 	}


	
	#If we are not at the first HDU in the output file, move to the last one to avoid overwtiting 
	move_to_last_hdu($outfptr, $out_fits_filename);
	
	#Copy the HDU from the input to the output file
	Astro::FITS::CFITSIO::fits_copy_hdu($infptr, $outfptr, 0, $status);
	if ($debug && $status eq 0) {print "HDU copied successfully!\n";} 
	elsif ($status) { print "ERROR $status in copying HDU\n";}

	#Increment the HDU hash to account for another 
	$last_hdu_hash{$out_fits_filename} +=1;

# 	if ($i < $total_in_hdu-1)
# 	{
# 	    $status=0;
# 	    Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);
# 	    if ($status) {print "ERROR $status moving to the next input HDU failed!\n";}
# 	    elsif ($debug) {print "Moved to the next input HDU successfully!\n";}
# 	}

	Astro::FITS::CFITSIO::fits_movrel_hdu($infptr, 1, $in_chdu_type, $status);

# 	#Close the output file
# 	$status=0;
# 	Astro::FITS::CFITSIO::fits_close_file($outfptr, $status);
# 	if ($status) {print "ERROR $status closing output file failed!\n";}
# 	elsif ($debug) {print "Closed output file successfully!\n";}
    }

    #Close the input file
    $status=0;
    Astro::FITS::CFITSIO::fits_close_file($infptr, $status);
    if ($status) {print "ERROR $status closing input file failed!\n";}
    elsif ($debug) {print "Closed input file successfully!\n";}
}



sub append_hdus_red
{
    my ($in_fits_filename, %outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    append_hdus($in_fits_filename, "red", %outfptr_hash);
}


sub append_hdus_biascor
{
    my ($in_fits_filename, %outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    append_hdus($in_fits_filename, "biascor", %outfptr_hash);
}

sub append_hdus_flatcor
{
    my ($in_fits_filename, %outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    append_hdus_red($in_fits_filename, "flatcor", %outfptr_hash);
}


sub append_hdus_supersky
{
    my ($in_fits_filename, %outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    append_hdus_red($in_fits_filename, "supersky", %outfptr_hash);
}

sub append_hdus_illumcor
{
    my ($in_fits_filename, %outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    append_hdus_red($in_fits_filename, "illumcor", %outfptr_hash);
}

sub append_hdus_fringecor
{
    my ($in_fits_filename, %outfptr_hash) = @_;

    # Reuse the reduced function because both files are laid out similarly
    append_hdus_red($in_fits_filename, "fringecor", %outfptr_hash);
}



# Preloaded methods go here.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

FITS_Header - Perl extension for blah blah blah

=head1 SYNOPSIS

  use FITS_Header;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for FITS_Header, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.



=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 AUTHOR

A. U. Thor, E<lt>asutrave@localdomainE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2011 by A. U. Thor

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut
