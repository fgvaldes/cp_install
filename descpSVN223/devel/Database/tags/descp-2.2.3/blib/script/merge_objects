#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#
#  $Id: arrm 1577 2008-08-14 20:04:19Z dadams $
#
#  $Rev:: 1577                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-08-14 15:04:19 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
################################################################################
use strict;
use warnings;
use Carp;
use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use DB::DESUtil;
use DB::EventUtils;

my ($project,$run,$target_table,$src_table,$verbose);
my $validate = undef;

Getopt::Long::GetOptions(
   'project=s' => \$project,
   'run=s' => \$run,
   'source-table|stbl=s' => \$src_table,
   'target-table|target_table|object-table|object_table|ttbl=s' => \$target_table,
   'validate' => \$validate,
   'verbose=i' => \$verbose
);

if (defined($validate) && ! $project) {
    Pod::Usage::pod2usage (
    -verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide a project argument.'
  );
}

if (defined($validate) && ! $run) {
    Pod::Usage::pod2usage (
    -verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide a run argument.'
  );
}

if (! $src_table) {
  if ($run) {
    $src_table = "PAR_$run";
  }
  else {
    Pod::Usage::pod2usage (
    -verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide a source table or run argument.'
  );
  }
}

if (! $target_table) {
 Pod::Usage::pod2usage (
    -verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide a target (to be merged to) database table name.'
  );
}

# Make a new partition and index name
my $partition_name = 'P'.$src_table;
my $index_name = 'I'.$src_table;

my $db = DB::DESUtil->new();


# Do some checking:
if (defined($validate)) {
    print "Validating Contents of $src_table.\n";

    my $start = time;
    my $cats = $db->selectall_hashref( "select a.id,a.objects from catalog a,location b where a.project='$project' and a.run='$run' and a.catalogtype='red_cat' and a.id=b.id and regexp_like(b.archivesites,'[^N]')",'id') ; 
    print "Catalog query took ", time - $start, " seconds\n";

    $start = time;
    my $tmpobjs = $db->selectall_hashref( "select catalogid, count(*) from $src_table group by catalogid", 'catalogid'); 
    print "Tmp table query took ", time - $start, " seconds\n";

    my $ncats = scalar keys %$cats;
    print "Number of catalogs for run: $ncats\n";

    my $ntmpobjs = scalar keys %$tmpobjs;
    print "Number of catalogs in src_table: $ntmpobjs\n";


    my $error = 0;

    my %allcatids = ();
    foreach my $id (keys %$cats) {
       $allcatids{$id} = 1;
    }
    foreach my $id (keys %$tmpobjs) {
       $allcatids{$id} = 1;
    }

    my $sumactual = 0;
    my $sumexpected = 0;
    foreach my $id (keys %allcatids) {
        my $Nactual = -1;
        my $Nexpected = -1;
        if (defined($cats->{$id})) {
            $Nexpected = $cats->{$id}{'objects'};
            $sumexpected += $Nexpected;
        }
        if (defined($tmpobjs->{$id})) {
            $Nactual = $tmpobjs->{$id}{'count(*)'};
            $sumactual += $Nactual;
        }


        if ($Nexpected != 0) {
            printf("CatalogID: %10d  Expected Objects: %10d  Ingested Objects: %10d   ", $id, $Nexpected, $Nactual);
            if ($Nexpected != $Nactual) {
                print "***";
                $error = 1;
            }
            print "\n";
        }
    }

    print "\n\n";
    reportEvent($verbose,"STATUS",1,"Total expected = $sumexpected, Total actual = $sumactual");

    if ($error) {
        reportEvent($verbose,"STATUS",5,"Exiting due to errors caught by validation");
        $db->disconnect;
        exit 255;
    }
}
else {
    print "Skipping validation of $src_table.\n";
}


#print "Calling mergeTmpTable\n";
#$db->mergeTmpTable ({
#  'source_table' => $src_table,
#  'source_drop_index' => $index_name,
#  'target_table' => $target_table,
#  'partition_name' => $partition_name,
#  'drop' => 1
#});

print "Calling mergeTmpTablePrc\n";
$db->mergeTmpTablePrc ({
  'source_table' => $src_table,
  'dest_table' => $target_table
});

$db->disconnect;

exit 0;

################################################################################
# Documentation
################################################################################

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 OPTIONS
