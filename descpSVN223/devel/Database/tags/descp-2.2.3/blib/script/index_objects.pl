#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell
#
# Create indices on a DESCP objects table
#
# Usage: reindex_objects.pl -table <table>
#
use strict;
use Benchmark;
use FindBin;
use Getopt::Long;
use Exception::Class::DBI;
use DBI;

use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use DB::DESUtil;

my ($table,$debug);

# read from command line
Getopt::Long::GetOptions(
   "table=s" => \$table,
   "debug" => \$debug
) or usage("Invalid command line options\n");

usage("\nYou must supply a table name") unless defined $table;

#
# Make a database connection
#
my $dbh = DB::DESUtil->new(
    DBIattr => {
        AutoCommit => 0,
        RaiseError => 1,
        PrintError => 0
    }
);

#
# do index command(s)
#
my $t0 = new Benchmark;

my $indexName = $table."_object_idx";
my $query = "CREATE INDEX $indexName ON $table (object_id)";
my $retVal = $dbh->do( $query ) or die "DBI::errstr";

my $t1 = new Benchmark;

$indexName = $table."_catalogid_idx";
$query = "CREATE INDEX $indexName ON $table (catalogid)";
$retVal = $dbh->do( $query ) or die "DBI::errstr";

my $t2 = new Benchmark;

$indexName = $table."_ra_dec_idx";
$query = "CREATE INDEX $indexName ON $table (ra, dec)";
$retVal = $dbh->do( $query );

my $t3 = new Benchmark;

$indexName = $table."_imageid_idx";
$query = "CREATE INDEX $indexName ON $table (imageid)";
$retVal = $dbh->do( $query );

my $tf = new Benchmark;

$dbh->commit;
$dbh->disconnect;
#
# Report times
#
if ($debug) {
    my $diff = timediff($t1,$t0);
    print "object_id time:  ",timestr($diff,'all'),"\n";
    $diff = timediff($t2,$t1);
    print "catalog_id time:  ",timestr($diff,'all'),"\n";
    $diff = timediff($t3,$t2);
    print "ra_dec time:  ",timestr($diff,'all'),"\n";
    $diff = timediff($tf,$t3);
    print "image_id time:  ",timestr($diff,'all'),"\n";

    $diff = timediff($tf,$t0);
    print "total time:  ",timestr($diff,'all'),"\n";
}

sub usage {
   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\nusage:\n" .
      "   $command " .
      " -table table\n" .
      "       table: the name of an objects table in the database\n"
   );

   die("\n")
}
