#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

use strict;
use FindBin;
use Data::Dumper;
use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use DB::DESUtil;
use DB::IngestUtils;
use DB::FileUtils;

#
# Make a database connection
#
my $dbh = DESUtil->new
  (
             DBIattr => {
              AutoCommit => 0,
              RaiseError => 1
             }
  );

my $sql = qq{SELECT * FROM LOCATION where id=?};
my $sth = $dbh->prepare($sql);

my $fileId=5854780;
my $table = 'CATALOG';

$sth->execute($fileId);

my $locationResults = $sth->fetchall_arrayref({});
$sth->finish();

$sql = qq{SELECT * FROM $table where id=?};

$sth = $dbh->prepare($sql);

$sth->execute($fileId);

my $results = $sth->fetchall_arrayref({});
$sth->finish();

print Dumper($locationResults);
print Dumper($results);

#foreach my $key (keys %$locationResults){
#  print "$key => $locationResults->{$key}\n";
#}
#
#foreach my $key (keys %$results){
#  print "$key => $locationResults->{$key}\n";
#}

exit(0);
