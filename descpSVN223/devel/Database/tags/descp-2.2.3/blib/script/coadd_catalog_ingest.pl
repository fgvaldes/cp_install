#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# coadd_catalog_ingest.pl
#
# DESCRIPTION:
#
# AUTHOR:
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev:: 5741                                            $ rev of latest commit
# $LastChangedBy:: bcs                                   $ Author of last rev
# $LastChangedDate:: 2010-07-21 11:33:41 -0700 (Wed, 21 #$ Date of last rev
#

use strict;
use Cwd;
use Data::Dumper;
use FindBin;
use File::Path;
use File::stat;
use Getopt::Long;
use Time::localtime;

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use DB::DESUtil;
use DB::FileUtils;
use DB::EventUtils;
use DB::IngestUtils;

my ( $fileList, $archiveNode, $imageId, $band, $equinox, $isCoadd, $pastRun ) =
  undef;
my $verboseLevel = 2;
my $eventStr     = q{};

Getopt::Long::GetOptions(
    "filelist=s"     => \$fileList,
    "archivenode=s"  => \$archiveNode,
    "pastrun=s"      => \$pastRun,
    "verboselevel=i" => \$verboseLevel,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply a filelist") unless defined $fileList;

#
# Read in the filelist
#
my @files;
readFileList( $fileList, \@files );

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new(
    DBIattr => {
        AutoCommit => 0,
        printError => 0,
        RaiseError => 1
    }
);

my ( $locationId, $archiveHost, $archiveRoot, $archiveSiteStr ) =
  getArchiveNodeInfo( $desdbh, $archiveNode )
  if defined $archiveNode;

my $objTableInfoHashRef = getTableInfo( $desdbh, 'COADD_OBJECTS' );

my ( $resolvedFilesArrRef, $runIDSHashRef, $nitesHashRef, $project ) =
  parseFilelist( \@files, $archiveSiteStr, 0, 0, 0 );

#
# Make sure we have all five bands
#
my $hasAllFiles = verifyCoaddList( \@files, $project );
if ($project ne 'SPT') {
if ( !$hasAllFiles ) {
    $eventStr = qq{There were not enough bands present in the filelist
         Please make sure all bands are present before ingesting
         coadd catalogs.
  };
    reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
}
}
#
# Main file loop
#
my $numFiles  = 0;
my $numObjs   = 0;
my $localPath = q{};
my $catFile   = q{};
my $nites;
my @resolvedFilenames = ();
my $ingestedIds;
my $isIngested = 0;
my $insertHashRef;

foreach my $file (@files) {

    my $catFile        = $file->{'localfilename'};
    my $localPath      = $file->{'localpath'};
    my $coaddCatalogId = $file->{'fileid'};

    if ( !$coaddCatalogId ) {
        $eventStr = qq{fileid not present in filelist for $catFile};
        reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
        exit(1);
    }

    #
    # if localpath is not absolute, set it to the current dir and append
    # localpath to cwd.
    #

    if ( defined $localPath ) {
        if ( $localPath !~ m{^\/} ) {
            $localPath = cwd() . qq{/$localPath};
        }
    }
    else {
        $localPath = cwd();
    }

    my $resolvePath = setUpResolvePath($file);

    my $fileInfo = filenameResolve(qq{$resolvePath/$catFile});

    my $fileType = $fileInfo->{'FILETYPE'};
    my $runId    = $fileInfo->{'RUN'};
    my $band     = $fileInfo->{'BAND'};
    my $tileName = $fileInfo->{'TILENAME'};

    #
    # Check if this is a supported filetype
    #
    if ( ( $fileType ne 'coadd_cat' ) && ( $fileType ne 'catalog' ) ) {

        $eventStr = qq{Not supported filetype:  $fileType
      Currently ingesting only catalogs of type 'coadd_cat'
      };
        reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
    }

    #
    # Read in this coadd tile
    #

    $eventStr = "Reading catalog: $catFile";
    reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );

    my $catHashRef = readCatalog(qq{$localPath/$catFile});
    my $nRows      = scalar( @{ $catHashRef->{'OBJECT_NUMBER'} } );

#
# Issue a status 5 event if there are no objects in this catalog
#
    if (!$nRows){
      $eventStr = "There are no objects in $catFile";
      reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
      exit(1);
    }

    #
    # Get coadd image and catalog information from location table
    #
    my $queryHashRef;
    $queryHashRef->{'TILENAME'} = $tileName;
    $queryHashRef->{'RUN'}      = $runId;
    $queryHashRef->{'BAND'}     = $band;

    my $imageFileName = $catFile;
    $imageFileName =~ s/$band\_cat//;

    $queryHashRef->{'FILENAME'} = $catFile;
    $queryHashRef->{'FILETYPE'} = 'coadd_cat';

    my $coaddImageId = getCoaddCatParentId( $desdbh, $coaddCatalogId );

    if ( !$coaddImageId ) {
        $eventStr = qq{Parent id not defined for $catFile (ID=$coaddCatalogId)};
        reportEvent( $verboseLevel, 'STATUS', 4, $eventStr );
    }

    my @imageIds   = ($coaddImageId) x $nRows;
    my @catalogIds = ($coaddCatalogId) x $nRows;

 #
 # Check if this catalog has been ingested.  Only do this once for one band.
 # If ids exist for one band, then they exist for all of them.
 #
 #if ( uc($band) eq 'G'){
 #    $ingestedIds = checkIfCoaddCatalogIngested($desdbh,$coaddCatalogId,$band);
 #    $isIngested = $ingestedIds->[0][0];
 #}

    #
    # Deal with keywords that are not band-specific
    #
    foreach my $key ( keys %$catHashRef ) {

        my $band = uc($band);
        if (   ( $key eq 'EQUINOX' )
            || ( $key eq 'NUMBER' )
            || ( $key eq 'HTMID' )
            || ( $key eq 'DR' )
            || ( $key eq 'CX' )
            || ( $key eq 'CY' )
            || ( $key eq 'CZ' )
            || ( $key eq 'SOFTID' )
            || ( $key eq 'ALPHA_J2000' )
            || ( $key eq 'DELTA_J2000' )
            || ( $key eq 'ALPHAPEAK_J2000' )
            || ( $key eq 'DELTAPEAK_J2000' )
            || ( $key eq 'X2_WORLD' )
            || ( $key eq 'Y2_WORLD' )
            || ( $key eq 'XY_WORLD' )
            || ( $key eq 'THRESHOLD' )
            || ( $key eq 'MU_THRESHOLD' )
            || ( $key eq 'X_IMAGE' )
            || ( $key eq 'Y_IMAGE' )
            || ( $key eq 'A_IMAGE' )
            || ( $key eq 'B_IMAGE' )
            || ( $key eq 'A_WORLD' )
            || ( $key eq 'B_WORLD' )
            || ( $key eq 'ERRX2_WORLD' )
            || ( $key eq 'ERRY2_WORLD' )
            || ( $key eq 'XMIN_IMAGE' )
            || ( $key eq 'YMIN_IMAGE' )
            || ( $key eq 'XMAX_IMAGE' )
            || ( $key eq 'YMAX_IMAGE' )
            || ( $key eq 'XPEAK_IMAGE' )
            || ( $key eq 'YPEAK_IMAGE' )
            || ( $key eq 'THETA_IMAGE' )
            || ( $key eq 'ELONGATION' )
            || ( $key eq 'LOCATION' )
            || ( $key eq 'KRON_RADIUS' )
            || ( $key eq 'PETRO_RADIUS' )
            || ( $key eq 'NLOWDWEIGHT_ISO' )
            || ( $key eq 'R_OBJECT_NUMBER' ) )
        {

            next if not $objTableInfoHashRef->{$key};
            $insertHashRef->{$key} = $catHashRef->{$key};

            if ( $key eq 'ALPHAPEAK_J2000' ) {
                $insertHashRef->{'RA'} = $catHashRef->{$key};
            }
            if (   ( $key eq 'ALPHA_J2000' )
                && ( !exists $catHashRef->{'ALPHAPEAK_J2000'} ) )
            {
                $insertHashRef->{'RA'} = $catHashRef->{$key};
            }

            if ( $key eq 'DELTAPEAK_J2000' ) {
                $insertHashRef->{'DEC'} = $catHashRef->{$key};
            }
            if (   ( $key eq 'DELTA_J2000' )
                && ( !exists $catHashRef->{'DELTAPEAK_J2000'} ) )
            {
                $insertHashRef->{'DEC'} = $catHashRef->{$key};
            }

        }
        elsif ( $key eq 'OBJECT_NUMBER' ) {
            $insertHashRef->{'R_OBJECT_NUMBER'} = $catHashRef->{$key};
        }
        else {

            my $bandKey;
            if ( $key =~ m/(MAG|MAGERR)_APER_/ ) {
                $bandKey = $key;
                $bandKey =~ s/APER_/APER/;
                $bandKey .= "_" . $band;
            }
            else {
                $bandKey = $key . "_" . $band;
            }

            next if not $objTableInfoHashRef->{$bandKey};
            $insertHashRef->{$bandKey} = $catHashRef->{$key};

        }
    }

    #
    # Insert the CATALOGID and IMAGEID for this band
    #
    my $catalogIdKey = "CATALOGID_" . uc($band);
    my $imageIdKey   = "IMAGEID_" . uc($band);
    $insertHashRef->{$imageIdKey}   = \@imageIds;
    $insertHashRef->{$catalogIdKey} = \@catalogIds;

    #
    # Insert the zeropoints and zeropoint ids
    #
    my $zpBandKey    = "ZEROPOINT_" . uc($band);
    my $zpIdBandKey  = "ZEROPOINTID_" . uc($band);
    my $zpErrBandKey = "ERRZEROPOINT_" . uc($band);

    my $zpHashRefArr = getCoaddZeropoint( $desdbh, $coaddImageId );

    my $zpHashRef = $zpHashRefArr->[0];
    my @zps       = ( $zpHashRefArr->[0]->{'mag_zero'} ) x $nRows;
    my @zpErrs    = ( $zpHashRefArr->[0]->{'sigma_mag_zero'} ) x $nRows;
    my @zpIds     = ( $zpHashRefArr->[0]->{'id'} ) x $nRows;

    $insertHashRef->{$zpBandKey}    = \@zps;
    $insertHashRef->{$zpIdBandKey}  = \@zpIds;
    $insertHashRef->{$zpErrBandKey} = \@zpErrs;

    #
    # Fill in miscellaneous keys
    #

    my @equinoxes = ('2000') x $nRows;
    my @softIds   = ('1000') x $nRows;
    $insertHashRef->{'EQUINOX'} = \@equinoxes;
    $insertHashRef->{'SOFTID'}  = \@softIds;

    #
    # Get CX,CY,CZ
    #

    my @cxs    = ();
    my @cys    = ();
    my @czs    = ();
    my @htmIDs = ();
    my ( $cx, $cy, $cz ) = 0;
    my $count =
        $#{ $insertHashRef->{'ALPHAPEAK_J2000'} }
      ? $#{ $insertHashRef->{'ALPHAPEAK_J2000'} }
      : $#{ $insertHashRef->{'ALPHA_J2000'} };

    my $raKey =
      exists $insertHashRef->{'ALPHAPEAK_J2000'}
      ? 'ALPHAPEAK_J2000'
      : 'ALPHA_J2000';

    my $decKey =
      exists $insertHashRef->{'DELTAPEAK_J2000'}
      ? 'DELTAPEAK_J2000'
      : 'DELTA_J2000';

    for ( my $i = 0 ; $i <= $count ; $i++ ) {

        my $htmDepth = 20;
        my $ra       = $insertHashRef->{$raKey}->[$i];
        my $dec      = $insertHashRef->{$decKey}->[$i];
        ( $cx, $cy, $cz ) = getXYZ( $ra, $dec );

        my $htmID = 0;

        push @cxs,    $cx;
        push @cys,    $cy;
        push @czs,    $cz;
        push @htmIDs, $htmID;

    }

    $insertHashRef->{'CX'}    = \@cxs;
    $insertHashRef->{'CY'}    = \@cys;
    $insertHashRef->{'CZ'}    = \@czs;
    $insertHashRef->{'HTMID'} = \@htmIDs;

    $numFiles++;

    #printHashRef($insertHashRef);
    #my $numKeys = scalar(keys %$insertHashRef);
    #print "$numKeys\n";
    #exit(0);

}

if ($isIngested) {
    $eventStr = qq{"Updating objects is not currently supported in 
   coadd_catalog_ingest
   Please delete objects first, then reingest...};
    reportEvent( 2, 'STATUS', 5, $eventStr );
    exit(0);
}
else {
    $eventStr = qq{Starting batch ingest};
    reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );
    my $numObjIngested = ingestCoaddObjectsHashRef( $desdbh, $insertHashRef,
        $objTableInfoHashRef );
    $eventStr = "Ingested $numObjIngested objects into COADD_OBJECTS\n";
    reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );
}

$desdbh->commit();
$desdbh->disconnect;

exit(0);

#
# Subroutines
#

sub usage {

    my $message = $_[0];
    if ( defined $message && length $message ) {
        $message .= "\n"
          unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

    print STDERR (
        $message,
        "\n"
          . "usage: $command "
          . " -filelist files -archivenode archive\n"
          . "       filelist contains the list of files along with the full path\n"
          . "       archivenode corresponds to one of the known archive nodes:\n"
          . "          bcs, des1, etc...\n"
    );

    die("\n")

}

