#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# wl_catalog_ingest.pl
#
# DESCRIPTION:
#
# AUTHOR:
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev:: 5210                                            $ rev of latest commit
# $LastChangedBy:: tdarnell                              $ Author of last rev
# $LastChangedDate:: 2010-03-03 13:31:05 -0700 (Wed, 03 #$ Date of last rev
#

use strict;
use Cwd;
use Data::Dumper;
use FindBin;
use Getopt::Long;
use Time::localtime;

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use DB::DESUtil;
use DB::FileUtils;
use DB::EventUtils;
use DB::IngestUtils;

$| = 1;

my ( $fileList, $archiveNode, $imageId, $tableName, $pastRun ) = undef;
my $verboseLevel = 2;
my $eventStr     = q{};

Getopt::Long::GetOptions(
    "filelist=s"     => \$fileList,
    "archivenode=s"  => \$archiveNode,
    "verboselevel=i" => \$verboseLevel,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply a filelist")     unless defined $fileList;
usage("\n\nYou must supply an archivenode") unless defined $archiveNode;

#
# Read in the filelist
#
my @files;
readFileList( $fileList, \@files );

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new(
    DBIattr => {
        AutoCommit => 0,
        printError => 0,
        RaiseError => 1
    }
);

my ( $locationId, $archiveHost, $archiveRoot, $archiveSiteStr ) =
  getArchiveNodeInfo( $desdbh, $archiveNode )
  if defined $archiveNode;

my ( $resolvedFilesArrRef, $runIDSHashRef, $nitesHashRef, $project ) =
  parseFilelist( \@files, $archiveSiteStr, 0, 0, 0 );

#
# Main file loop
#
my $numFiles  = 0;
my $numObjs   = 0;
my $localPath = q{};
my $catFile   = q{};
my $nites;
my @resolvedFilenames = ();
my $ingestedIds;
my $isIngested = 0;

#
# $resolvedTypes are sorted and ingested as follows:
#
#  1. WL_FINDSTARS : shpltall
#  2. WL_PSF       : shpltpsf
#  3. WL_SHEAR     : shear
#

foreach my $resolvedTypes (@$resolvedFilesArrRef) {

    my $tableName = $resolvedTypes->[0]->{'WLTABLE'};
    my $insertHashRef;

    next if not $tableName;

    foreach my $file (@$resolvedTypes) {

        my $catFile            = $file->{'FILENAME'};
        my $localPath          = $file->{'LOCALPATH'};
        my $wlCatalogId        = $file->{'FILEID'};
        my $wlTableInfoHashRef = getTableInfo( $desdbh, $tableName );

        #
        # Not Ingesting psfmodel files
        #
        next if $catFile =~ /_psfmodel/;

        if ( !$wlCatalogId ) {
            $eventStr = qq{fileid not present in filelist for $catFile};
            reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
            exit(1);
        }

        #
        # if localpath is not absolute, set it to the current dir and append
        # localpath to cwd.
        #

        if ( defined $localPath ) {
            if ( $localPath !~ m{^\/} ) {
                $localPath = cwd() . qq{/$localPath};
            }
        }
        else {
            $localPath = cwd();
        }

        my $resolvePath = setUpResolvePath($file);

        my $fileInfo = filenameResolve(qq{$resolvePath/$catFile});

        my $fileType = $fileInfo->{'FILETYPE'};
        my $runId    = $fileInfo->{'RUN'};
        my $band     = $fileInfo->{'BAND'};
        my $tileName = $fileInfo->{'TILENAME'};
        my $ccdNo    = $fileInfo->{'CCD'};

        #
        # Check if this is a supported filetype
        #
        if (   ( $fileType ne 'shapelet_shpltpsf' )
            && ( $fileType ne 'shapelet_shpltall' )
            && ( $fileType ne 'shapelet_shear' ) )
        {

            $eventStr =
              qq{shear_catalog_ingest: Not supported filetype, $fileType };
            reportEvent( $verboseLevel, 'STATUS', 5, $eventStr );
        }

        #
        # Read in this wl catalog
        #

        $eventStr = "Reading catalog: $catFile";
        reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );

        my $catHashRef;
        $catHashRef = readWLCatalog( qq{$localPath/$catFile}, $fileType );

        my $nRows      = scalar( @{ $catHashRef->{'OBJECT_NUMBER'} } );
        my @catalogIds = ($wlCatalogId) x $nRows;

        my $ingestedIds =
          checkIfWLCatalogIngested( $desdbh, $wlCatalogId, $tableName );
        $isIngested = $ingestedIds->[0][0];

        if ($isIngested) {
            $eventStr =
              qq{Contents of id $wlCatalogId already exist in the database\n};
            $eventStr .= qq{Moving to the next catalog...};
            reportEvent( 2, 'STATUS', 4, $eventStr );
            next;
        }

        #
        # Build the insert hash ref based on the table columns
        #
        foreach my $key ( keys %$catHashRef ) {

            next if not $wlTableInfoHashRef->{$key};
            $insertHashRef->{$key} = $catHashRef->{$key};

        }

        #
        # Insert the CATALOGID.  This is the id of the current
        # catalog.  Taken from the filelist.
        #
        $insertHashRef->{'CATALOGID'} = \@catalogIds;

        #
        # Insert the OBJECT_ID.  This is the object ID in the OBJECTS
        # table belonging to the red_cat from which this was made
        #
        my $catObjects = getWLObjects( $desdbh, $wlCatalogId );
        $insertHashRef->{'OBJECT_ID'} = $catObjects;

        my $numObjIngested =
          ingestWLHashRef( $desdbh, $insertHashRef, $wlTableInfoHashRef,
            $tableName );
        $eventStr = "Ingested $numObjIngested objects into $tableName\n";
        reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );
        $desdbh->commit();

    }

    #
    # Commit after each filetype
    #
    #$desdbh->commit();

}

$desdbh->disconnect;

exit(0);

#
# Subroutines
#

sub usage {

    my $message = $_[0];
    if ( defined $message && length $message ) {
        $message .= "\n"
          unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

    print STDERR (
        $message,
        "\n"
          . "usage: $command "
          . " -filelist files -archivenode archive\n"
          . "       filelist contains the list of files along with the full path\n"
          . "       archivenode corresponds to one of the known archive nodes:\n"
          . "          bcs, des1, etc...\n"
    );

    die("\n")

}

