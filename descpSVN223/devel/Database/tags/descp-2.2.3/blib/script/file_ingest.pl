#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# file_ingest.pl
#
# DESCRIPTION:
#
# AUTHOR:
# Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev: 12309 $
# $LastChangedBy:Ankit Chandra $
# $LastChangedDate: 2013-06-21 10:22:23 -0700 (Fri, 21 Jun 2013) $
#

use strict;
use Astro::FITS::CFITSIO qw( :constants );
use Astro::FITS::CFITSIO qw( :longnames );
use Benchmark;
use Cwd;
use Data::Dumper;
use FindBin qw($Bin);
use File::Basename;
use File::stat;
use Getopt::Long;
use Time::localtime;

$| = 1;

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use DB::DESUtil;
use DB::IngestUtils;
use DB::FileUtils;
use DB::EventUtils;

# Change: Added by Ankit Chandra to include checksum code into fileingest
my $VERSION = '$Rev: 12309 $';

my ( $fileList, $archiveNode, $pastRuns ) = undef;
my $eventStr     = q{};
my $dateTime     = q{};
my $verboseLevel = 2;
my $exitStatus   = 0;
my ( $xcen, $ycen );

my $runTimeDir = cwd();

Getopt::Long::GetOptions(
    "filelist=s"    => \$fileList,
    "archivenode=s" => \$archiveNode,
    "verbose=i"     => \$verboseLevel,
    "pastrun=s"     => \$pastRuns,
) or usage("Invalid command line options\n");

usage("\n\nYou must supply a filelist")     unless defined $fileList;
usage("\n\nYou must supply an archivenode") unless defined $archiveNode;

print "STATUS1BEG Using version $VERSION of file_ingest.pl STATUS1END\n";

my $t1  = new Benchmark;
my $now = `date`;
chomp($now);
print "STATUS1BEG file_ingest started:  " . $now . " STATUS1END\n";

$eventStr = qq{*** FILE INGEST START ***};
reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );

#
# Set up wcs array
#
my @wcsParams = (
    'NAXIS',   'NAXIS1', 'NAXIS2', 'CRVAL1', 'CRVAL2', 'CRPIX1',
    'CRPIX2',  'CD1_1',  'CD1_2',  'CD2_1',  'CD2_2',  'OBSDATE',
    'EQUINOX', 'PV1_0',  'PV1_1',  'PV1_2',  'PV1_3',  'PV1_4',
    'PV1_5',   'PV1_6',  'PV1_7',  'PV1_8',  'PV1_9',  'PV1_10',
    'PV2_0',   'PV2_1',  'PV2_2',  'PV2_3',  'PV2_4',  'PV2_5',
    'PV2_6',   'PV2_7',  'PV2_8',  'PV2_9',  'PV2_10'
);

my $wcsBaseName = basename($fileList);
my $wcsFile     = "$runTimeDir/$wcsBaseName.wcs.txt";
my $flagWCSWritten;
open( my $WCSFILE, ">$wcsFile" );

#
# Read in the filelist
#
my @files = ();
readFileList( $fileList, \@files );

#
# Make a database connection
#
my $desdbh = DB::DESUtil->new(

    DBIattr => {
        debug => 1,
        AutoCommit => 0,
        RaiseError => 1,
        PrintError => 0
    }
);

#
# Set default date format for this transaction
#
#my $timeSql = qq{alter session set nls_date_format='Dy Mon DD HH24:MI:SS YYYY'};
#my $timeSth = $desdbh->prepare($timeSql);
#$timeSth->execute();
#$timeSth->finish();

my ( $locationId, $archiveHost, $archiveRoot, $archiveSiteStr ) =
  getArchiveNodeInfo( $desdbh, $archiveNode )
  if defined $archiveNode;

my $locationTableHashRef = getTableInfo( $desdbh, 'LOCATION' );
my $imageTableHashRef    = getTableInfo( $desdbh, 'IMAGE' );
my $exposureTableHashRef = getTableInfo( $desdbh, 'EXPOSURE' );
my $catalogTableHashRef  = getTableInfo( $desdbh, 'CATALOG' );
my $coaddTableHashRef    = getTableInfo( $desdbh, 'COADD' );
my $wlTableHashRef       = getTableInfo( $desdbh, 'WL' );
my $metaTablesHashRef;

$metaTablesHashRef->{'IMAGE'}    = $imageTableHashRef;
$metaTablesHashRef->{'EXPOSURE'} = $exposureTableHashRef;
$metaTablesHashRef->{'CATALOG'}  = $catalogTableHashRef;
$metaTablesHashRef->{'COADD'}    = $coaddTableHashRef;
$metaTablesHashRef->{'WL'}       = $wlTableHashRef;

#
# $resolvedFilenamesArrRef is an arraywlTRef of arrayRefs sorted by filetype
#
my $getKeywords  = 1;
my $skipOnFileId = 1;
my ( $resolvedFilenamesArrRef, $runIDS, $nites, $project ) =
  parseFilelist( \@files, $archiveSiteStr, $getKeywords, $skipOnFileId );

my $numRuns  = scalar keys %$runIDS;
my $numNites = scalar keys %$nites;

my @pastRuns = split /,/, $pastRuns if $pastRuns;

if (@pastRuns) {
    foreach (@pastRuns) {
        $runIDS->{$_} = 1;
    }
}

#
# Get all files for this run id and any pastruns as well.  This will be used
# to check if files are ingested and get their parent and exposure ids.
#
$eventStr = qq{Doing the Big Query...};
reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );

my $ingestedFiles = getFilesForRunID( $desdbh, $runIDS, $nites, $project )
  if ( keys %$runIDS || keys %$nites );

#
# Main Loop
#
my $filestart  = new Benchmark;
$now = `date`;
chomp($now);
print "STATUS1BEG Main file loop  started:  " . $now . " STATUS1END\n";

my $tCount = 0;
my $updateHashRef;
my $insertHashRef;
my $zeropointHashRef;
my $bandFromParent; # Ankit: new variable to catch band from parent
my $globalParentId;
my $getParentInfoHash; # the array used to get information from parent

foreach my $resolvedTypes (@$resolvedFilenamesArrRef) {

    my $numFiles = scalar(@$resolvedTypes);

    foreach my $fileInfoHashRef (@$resolvedTypes) {

        my $baseName     = $fileInfoHashRef->{'FILENAME'};
        my $localPath    = $fileInfoHashRef->{'LOCALPATH'};
        my $fileClass    = $fileInfoHashRef->{'FILECLASS'};
        my $fileType     = $fileInfoHashRef->{'FILETYPE'};
        my $exposureName = $fileInfoHashRef->{'EXPOSURENAME'};
        my $run          = $fileInfoHashRef->{'RUN'};
        my $nite         = $fileInfoHashRef->{'NITE'};
        my $fileName     = $baseName;
        my $doUpdate     = 0;
        my $badFile      = 0;
	my $positionInMainLoop = $fileInfoHashRef->{'FILE_LOOP_POSITION'};

        print "Processing $baseName\n";

        #
        # get rid of any compression extension
        #

        $fileName =~ s/\.(g|f)z$//;
        $fileInfoHashRef->{'FILENAME'} = $fileName;

        my $compressionType = ( $baseName =~ m/(gz$|fz$)/ ) ? $1 : $2;

        #
        # Check if file has been ingested
        # update archivesites to reflect compression type
        # Get the filesystem date and size
        #

        my $thisArchiveSiteStr;
        if ( $fileClass eq 'src' ) {

            my $tFileName = $fileName;
            $tFileName =~ s/\.(f|g)z$//;
            if ( exists $ingestedFiles->{$fileType}->{$tFileName}->{$nite} ) {
                $eventStr = sprintf( "$baseName has been ingested with id = %d",
                    $ingestedFiles->{$fileType}->{$fileName}->{$nite}->{'id'} );

                #reportEvent(2,'STATUS',1,$eventStr);
                $thisArchiveSiteStr =
                  $ingestedFiles->{$fileType}->{$fileName}->{$nite}
                  ->{'archivesites'};
                $doUpdate = 1;
            }
            else {
                $thisArchiveSiteStr = $archiveSiteStr;
            }

        }
        else {

            if ( $fileType eq 'raw' ) {

                for
                  my $rawType ( 'raw_obj', 'raw_bias', 'raw_dflat', 'raw_dark', 'raw_tflat' )
                {

                    if (
                        exists $ingestedFiles->{$rawType}->{$fileName}->{$run}
                        ->{$exposureName} )
                    {
                        $eventStr =
                          sprintf( "$baseName has been ingested with id = %d",
                            $ingestedFiles->{$rawType}->{$fileName}->{$run}
                              ->{$exposureName}->{'id'} );

                        #reportEvent(2,'STATUS',1,$eventStr);
                        $thisArchiveSiteStr =
                          $ingestedFiles->{$rawType}->{$fileName}->{$run}
                          ->{$exposureName}->{'archivesites'};
                        $doUpdate = 1;
                    }
                    else {
                        $thisArchiveSiteStr = $archiveSiteStr;
                    }

                }

            }
            else {

                if (
                    ( defined $exposureName && $exposureName ne q() )
                    ? exists $ingestedFiles->{$fileType}->{$fileName}->{$run}
                    ->{$exposureName}
                    : exists $ingestedFiles->{$fileType}->{$fileName}->{$run}
                  )
                {
                    if ( defined $exposureName && $exposureName ne q() ) {
                        $eventStr =
                          sprintf( "$baseName has been ingested with id = %d",
                            $ingestedFiles->{$fileType}->{$fileName}->{$run}
                              ->{$exposureName}->{'id'} );

                        #reportEvent(2,'STATUS',1,$eventStr);
                        $thisArchiveSiteStr =
                          $ingestedFiles->{$fileType}->{$fileName}->{$run}
                          ->{$exposureName}->{'archivesites'};
                    }
                    else {
                        $eventStr =
                          sprintf( "$baseName has been ingested with id = %d",
                            $ingestedFiles->{$fileType}->{$fileName}->{$run}
                              ->{'id'} );

                        #reportEvent(2,'STATUS',1,$eventStr);
                        $thisArchiveSiteStr =
                          $ingestedFiles->{$fileType}->{$fileName}->{$run}
                          ->{'archivesites'};
                    }
                    $doUpdate = 1;

                }
                else {
                    $thisArchiveSiteStr = $archiveSiteStr;
                }

            }

        }

        $thisArchiveSiteStr = $archiveSiteStr if not $thisArchiveSiteStr;
        $fileInfoHashRef->{'ARCHIVESITES'} =
          updateArchiveSitesStr( $archiveSiteStr, $thisArchiveSiteStr,
            $compressionType );

        if ( !-e "$localPath/$baseName" ) {
            $eventStr =
              "$localPath/$baseName does not exist on this filesystem";
            reportEvent( 2, 'STATUS', 4, $eventStr );
            $exitStatus = 1;
            next;
        }

        my $currFileDate = ctime( stat(qq{$localPath/$baseName})->mtime );
        my $currFileSize = stat(qq{$localPath/$baseName})->size;

        if ( !$currFileDate && !$currFileSize ) {
            $eventStr =
              qq{Filedate and Filesize are not available for $baseName\n};
            reportEvent( 2, 'STATUS', 4, $eventStr );
            $exitStatus = 1;
        }

        #
        # Populate the latest filedate and filesize.
        # Filesize can go into one of three different spots in the LOCATION
        # table depending on compressionType.
        #
        # if compressionType = fz, place size in FILESIZE_FZ column
        # if compressionType = gz, place size in FILESIZE_GZ column
        #
        $fileInfoHashRef->{'FILEDATE'} = $currFileDate;
	if ( defined $compressionType ) {
            if ( $compressionType eq 'fz' ) {
                $fileInfoHashRef->{'FILESIZE_FZ'} = $currFileSize;
            }
            elsif ( $compressionType eq 'gz' ) {
                $fileInfoHashRef->{'FILESIZE_GZ'} = $currFileSize;
            }
        }
        else {
            $fileInfoHashRef->{'FILESIZE'} = $currFileSize;
        }

        #
        # Check if this is a fits file
        #
        my $isFits =
          ( $baseName =~ m/(fits|fit|fits\.gz|fts|fts\.gz|fits\.fz|fts\.fz)$/ )
          ? 1
          : 0;

        my $hdrHashRef;

        my $metaTable;
        my $fileid;
        $metaTable = getMetaTable($fileType)
          if ( ( $fileType eq 'diff_cat' ) || ( $fileType eq 'diff_nccat' ) );

        #
        # If isFits, read header, run headerResolve
        #
        if ($isFits) {

            $metaTable = getMetaTable($fileType);
            my $status = 0;
            my $nHdus;
            my $fptr =
              Astro::FITS::CFITSIO::open_file( "$localPath/$baseName", READONLY,
                $status );

            if ($status) {
                $eventStr = "Problem opening $baseName:  $status";
                reportEvent( $verboseLevel, 'STATUS', 4, $eventStr );

                $status     = 0;
                $exitStatus = 1;
            }

            #
            # Check for number of hdus for reduced files, issues
            # status 4 event if all three are not there.
            #
            $status = 0;
            Astro::FITS::CFITSIO::fits_get_num_hdus( $fptr, $nHdus, $status );
            if ( $fileType eq 'red' && $nHdus < 3 ) {
                $eventStr = "$baseName does not have 3 hdus: $nHdus";
                reportEvent( $verboseLevel, 'STATUS', 4, $eventStr );
            }

            my $naxes;
            my ( $bitpix, $naxis );
            my $hduType = 0;
            $status = 0;
            if (
                (
                       $compressionType
                    && $compressionType eq 'fz'
                    && $fileClass ne 'src'
                )
                || ( $fileType eq 'red_psf' || $fileClass eq 'wl' || $fileType =~ /shapelet/ )
              )
            {
                Astro::FITS::CFITSIO::fits_movabs_hdu( $fptr, 2, $hduType,
                    $status );
            }

            #
            # Get the image dimensions
            #
            Astro::FITS::CFITSIO::fits_get_img_parm( $fptr, $bitpix, $naxis,
                $naxes, $status )
              if ( ( $fileType ne 'red_psf' )
                && ( $fileClass ne 'wl' )
                && ( $fileType !~ m/photoz/ ) );

            $hdrHashRef = $fptr->read_header();

            #
            # Resolve the header for ingestion
            #
            $hdrHashRef = cleanHeader($hdrHashRef);
		
            my $notSrc = 0 if ( $fileClass eq 'src' );
            $hdrHashRef =
              headerResolve( $desdbh, $hdrHashRef,
                $metaTablesHashRef->{$metaTable},
                $baseName, $notSrc )
              if ( $fileType !~ m/photoz/ );

            if ( $fileType ne 'red_psf' ) {
                $hdrHashRef->{'NAXIS1'} = $naxes->[0];
                $hdrHashRef->{'NAXIS2'} = $naxes->[1];
            }

            my ( $cx, $cy, $cz ) = 0.0;
            if ( $fileType eq 'src' ) {
                my $telra  = $hdrHashRef->{'TELRA'};
                my $teldec = $hdrHashRef->{'TELDEC'};
                ( $cx, $cy, $cz ) = getXYZ( $telra, $teldec )
                  if ( $telra && $teldec );
            }
            else {
                my $crval1 = $hdrHashRef->{'CRVAL1'};
                my $crval2 = $hdrHashRef->{'CRVAL2'};
                ( $cx, $cy, $cz ) = getXYZ( $crval1, $crval2 )
                  if ( $crval1 && $crval2 );
            }

            $hdrHashRef->{'CX'} = $cx;
            $hdrHashRef->{'CY'} = $cy;
            $hdrHashRef->{'CZ'} = $cz;

            #
            # Sync header with location table information
            #
            foreach my $key ( keys %$fileInfoHashRef ) {
                my $hdrKey;
                if ( $key eq 'FILENAME' ) {
                    if ( $metaTable ne 'COADD' ) {
                        $hdrKey = $metaTable . 'NAME';
                        $hdrHashRef->{$hdrKey} = $fileInfoHashRef->{'FILENAME'};
                    }
                    else {
                        $hdrHashRef->{'IMAGENAME'} =
                          $fileInfoHashRef->{'FILENAME'};
                    }
                }
                elsif ( $key eq 'FILETYPE' ) {
                    if ( $metaTable !~ m/COADD|EXPOSURE/ ) {
                        $hdrKey = $metaTable . 'TYPE';
                        if ( $fileInfoHashRef->{'FILETYPE'} eq 'raw' ) {
                            $fileInfoHashRef->{'FILETYPE'} =
                              $hdrHashRef->{$hdrKey};
                        }
                        else {
                            $hdrHashRef->{$hdrKey} =
                              $fileInfoHashRef->{'FILETYPE'};
                        }
                    }
                    else {
                        if ( $metaTable eq 'COADD' ) {
                            $hdrHashRef->{'IMAGETYPE'} =
                              $fileInfoHashRef->{'FILETYPE'};
                        }
                        else {
                            $hdrHashRef->{'EXPOSURETYPE'} =
                              $fileInfoHashRef->{'FILETYPE'};
                        }
                    }
                }
                elsif ( $key eq 'CCD' ) {
                    if ( $hdrHashRef->{'CCD'} ) {
                        $fileInfoHashRef->{'CCD'} = $hdrHashRef->{'CCD'};
                    }
                    else {
                        $hdrHashRef->{'CCD'} = $fileInfoHashRef->{'CCD'};
                    }
                }
                elsif ( $key eq 'TILENAME' ) {
                    if ( !$hdrHashRef->{'TILENAME'} ) {
                        $hdrHashRef->{'TILENAME'} =
                          $fileInfoHashRef->{'TILENAME'};
                    }
                }
                elsif ( $key eq 'RUN' ) {
                    if ( !$hdrHashRef->{'RUN'} ) {
                        $hdrHashRef->{'RUN'} = $fileInfoHashRef->{'RUN'};
                    }
                }
                elsif ( $key eq 'BAND' ) {
                    if ( $hdrHashRef->{'BAND'} ) {
                        $fileInfoHashRef->{'BAND'} = $hdrHashRef->{'BAND'};
                    }
                    else {
                        $hdrHashRef->{'BAND'} = $fileInfoHashRef->{'BAND'};
                    }
                }
                elsif ( $key eq 'TELRA' ) {
                    $fileInfoHashRef->{'TELRA'} = $hdrHashRef->{'TELRA'};
                }
                elsif ( $key eq 'TELDEC' ) {
                    $fileInfoHashRef->{'TELDEC'} = $hdrHashRef->{'TELDEC'};
                }
                elsif ( $key eq 'RA' ) {
                    $fileInfoHashRef->{'RA'} = $hdrHashRef->{'RA'};
                }
                elsif ( $key eq 'DEC' ) {
                    $fileInfoHashRef->{'DEC'} = $hdrHashRef->{'DEC'};
                }
                elsif ( $key eq 'TELEQUIN' ) {
                    next;
                }
                elsif ( $key eq 'TELFOCUS' ) {
                    next;
                }
                else {
                    $hdrHashRef->{$key} = $fileInfoHashRef->{$key}
                      if defined $hdrHashRef->{$key};
                }
            }

            #
            # Update filetype now in case this has changed since the header was
            # read in.
            #
            $fileType = $fileInfoHashRef->{'FILETYPE'};

        #
        # Check fileInfoHashRef for overrides from the filelist and use those if
        # present, right now only doing CCD, BAND but more may be added
        #
            if ( exists $fileInfoHashRef->{'CCD_FLAG'} ) {
                $hdrHashRef->{'CCD'} = $fileInfoHashRef->{'CCD'};
            }
            if ( exists $fileInfoHashRef->{'BAND_FLAG'} ) {
                $hdrHashRef->{'BAND'} = $fileInfoHashRef->{'BAND'};
            }

            #
            # If this is a catalog file, get the number of objects it has
            #
            if ( $fileType =~ m/\_cat|scamp|shapelet|norm_psfcat|\_zcat/ ) {
                my ( $hduType, $nRows ) = 0;
                $status = 0;
                Astro::FITS::CFITSIO::fits_get_num_hdus( $fptr, $nHdus,
                    $status );

                if ( $nHdus == 2 ) {
                    Astro::FITS::CFITSIO::fits_movabs_hdu( $fptr, 2, $hduType,
                        $status );
                }
                elsif ( $nHdus > 2 ) {
                    Astro::FITS::CFITSIO::fits_movabs_hdu( $fptr, 3, $hduType,
                        $status );
                }
                else {
                    $eventStr =
"$baseName not in FITS_1.0 or FITS_LDAC format, not ingesting\n";
                    reportEvent( $verboseLevel, 'STATUS', 4, $eventStr );

                    $exitStatus = 1;
                }

                Astro::FITS::CFITSIO::fits_get_num_rows( $fptr, $nRows,
                    $status );
                $nRows = 0 if !$nRows;
                if (!$nRows){
                    $eventStr = qq{There are no rows in this catalog: $fileName}; 
                    reportEvent( $verboseLevel, 'STATUS', 3, $eventStr );
                }
                $hdrHashRef->{'OBJECTS'} = $nRows;

                #
                # Get the parent id of the photoz_cat file
                #
                if ( $fileType eq 'photoz_cat' ) {
                    my ( $colNum, $colName, $typeCode, $width, $repeat,
                        $nullPointer, $anyNul )
                      = 0;
                    my @colVals;
                    fits_get_colname( $fptr, CASEINSEN, 'CATALOGID_*', $colName,
                        $colNum, $status );
                    fits_get_coltype( $fptr, $colNum, $typeCode, $repeat,
                        $width, $status );
                    fits_read_col(
                        $fptr,   $typeCode, $colNum, 1,
                        1,       $nRows,    0,       \@colVals,
                        $anyNul, $status
                    );
                    $fileInfoHashRef->{'PARENTID'} = $colVals[0];
                    $hdrHashRef->{'PARENTID'}      = $colVals[0];
                }

            }

            Astro::FITS::CFITSIO::fits_close_file( $fptr, $status );

          #
          # Get the right numbers to put into NAXIS1 and NAXIS2 if this is a src
          # image. Taken from the DETSIZE keyword.
          #
            if ( $fileType eq 'src' ) {
                my $tmpStr = $hdrHashRef->{'DETSIZE'};
                $tmpStr =~ m/\[.:(\d+),.:(\d+)\]/;
                my $naxis1 = $1;
                my $naxis2 = $2;
                $hdrHashRef->{'NAXIS1'} = $naxis1;
                $hdrHashRef->{'NAXIS2'} = $naxis2;
            }

            #
            # Get the parentID of the image.
            # This is based on filetype.
            #

            if (
                   ( $fileType !~ m/src|raw|etc|log|xml|aux|qa/ )
                && ( $fileType !~ m/runtime|qcdiff|qcremap|qcred|head/ )
                && ( $fileType !~
                    m/red_fullscamp|bpm|biascor|flatcor|darkcor|supersky|lintable/ )
                && ( $fileType !~ m/flat|bias|pupil|scamp/ )
                && ( $fileType ne 'coadd' )
                && ( $fileType ne 'coadd_det' )
                && ( $fileType ne 'diff_nitecmb' )
              )
            {

                #start of if ( $fileType ne 'photoz_cat' )
                if ( $fileType ne 'photoz_cat' ) {

                    my $queryHashRef;
                    $queryHashRef->{'FILETYPE'}  = $fileType;
                    $queryHashRef->{'FILECLASS'} = $fileClass;
                    $queryHashRef->{'FILENAME'}  = $fileName;
                    
                    if ( $fileClass eq 'src' ) {
                        $queryHashRef->{'RUN'} = $fileInfoHashRef->{'NITE'};
                        $queryHashRef->{'EXPOSURENAME'} =
                          $fileInfoHashRef->{'NITE'};
                    }
                    else {
                        $queryHashRef->{'RUN'} = $fileInfoHashRef->{'RUN'};
                        $queryHashRef->{'EXPOSURENAME'} =
                          $fileInfoHashRef->{'EXPOSURENAME'};
                    }
                    if (   $fileType eq 'remap'
                        || $fileType eq 'remap_psfcat'
                        || $fileType =~ /norm/
                        || $fileClass eq 'coadd' )
                    {
                        $queryHashRef->{'TILENAME'} =
                          $fileInfoHashRef->{'TILENAME'};
                    }
                    elsif ( $fileType =~ m/illumcor|fringecor/ ) {
                        $queryHashRef->{'BAND'} = $fileInfoHashRef->{'BAND'};
                    }
	
		# added by Ankit to get band information from parent in case of red_ccat and red_cat files. the function getParentIdFromHashRef has been modified to cater to this request	
	          if($fileType eq 'red_ccat' || $fileType eq 'red_cat')
		   {	
		   	$getParentInfoHash->{'band'} = undef;
		   }                 
   		my $parentId =
                      getParentIdFromHashRef( $ingestedFiles, $queryHashRef,
                        $runIDS, $getParentInfoHash );
		    $globalParentId = $parentId; # added to take the parentId to a bigger scope. a quick fix for now.. Change: Ankit
                    
		    $fileInfoHashRef->{'PARENTID'} = $parentId;
                    $hdrHashRef->{'PARENTID'}      = $parentId;
                }
				#end of if ( $fileType ne 'photoz_cat' )
				
				#start of if ( $fileType =~ m/shapelet/ )
                if ( $fileType =~ m/shapelet/ ) {

                    my $wlInfo =
                      getWLinfo( $ingestedFiles, $fileInfoHashRef, $runIDS );

                    $fileInfoHashRef->{'IMAGEID'}   = $wlInfo->{'imageid'};
                    $fileInfoHashRef->{'CATALOGID'} = $wlInfo->{'catalogid'};
                    $fileInfoHashRef->{'CCD'}       = $wlInfo->{'ccd'};
                    $fileInfoHashRef->{'BAND'}      = $wlInfo->{'band'};

                    $hdrHashRef->{'IMAGEID'}   = $wlInfo->{'imageid'};
                    $hdrHashRef->{'CATALOGID'} = $wlInfo->{'catalogid'};
                    $hdrHashRef->{'CCD'}       = $wlInfo->{'ccd'};
                    $hdrHashRef->{'BAND'}      = $wlInfo->{'band'};


                    if (   not defined $wlInfo->{'imageid'}
                        || not defined $wlInfo->{'catalogid'} )
                    {

                        my ( $eventStr1, $eventStr2 ) = q();

                        $eventStr1 =
                          qq{imageid not found for WL file: $fileName}
                          if not( $wlInfo->{'imageid'} );
                        $eventStr2 =
                          qq{catalogid not found for WL file: $fileName}
                          if not( $wlInfo->{'catalogid'} );
                        $eventStr = qq{$eventStr1\n$eventStr2};
                        reportEvent( $verboseLevel, 'STATUS', 4, $eventStr );

                    }

                }
                #end of if ( $fileType =~ m/shapelet/ )
                
                
            }

        #
        # Get the EXPOSUREID for all inserts except for the EXPOSURE and
        # COADD tables.
        # Also forget about biascor, flatcor, supersky, illumcor, fringecor, etc
        #

            if (
			($fileType =~ m/^raw_/) || # Ankit: added this to remove the bug where fileingest wasn't storing exposureids for raw_* files.
                   (( $fileClass ne 'src' )
                && ( $fileClass ne 'coadd' )
                && ( $fileType !~ m/bpm|bias|flat|dark|illumcor|coadd/ )
                && ( $fileType !~
m/fringecor|supersky|pupil|astrostds|shapelet|diff_nccat|diff_cat/
                )
              ))
            {

                my $exposureId = undef;
                my $exposureName;

                foreach my $i (1 .. 30){
                   $exposureName = $fileInfoHashRef->{'EXPOSURENAME'};
		   $exposureName .= "-$i" if ( $fileType eq 'diff_nitecmb' );
		   $exposureName .= "-$i" if ( $fileType eq 'diff_ncdistmp' );
		   $exposureName .= "-$i" if ( $fileType eq 'diff_nc' );
		   $exposureName .= '.fits';

		   $exposureId =
				$ingestedFiles->{'src'}->{$exposureName}->{$nite}->{'id'};
                   last if $exposureId;
                }

			if ( not $exposureId ) {
				my $eventStr =
					qq{WARNING:  EXPOSURE ID does not exist in the EXPOSURE table for nite $nite and exposurename $exposureName};
						#print Dumper($ingestedFiles);
				reportEvent( 2, 'STATUS', 4, $eventStr );
			}

                $hdrHashRef->{'EXPOSUREID'}      = $exposureId;
                $fileInfoHashRef->{'EXPOSUREID'} = $exposureId;

            }

            $xcen = $hdrHashRef->{'NAXIS1'} / 2 if $hdrHashRef->{'NAXIS1'};
            $ycen = $hdrHashRef->{'NAXIS2'} / 2 if $hdrHashRef->{'NAXIS2'};

        }    # isFits block



      #
      # Get the parent id of the diffcat file from the difference image pipeline
      # They are not using fits files for their catalogs.  This is special stuff # I had to do for the diff imaging pipeline only.
      #
        if ( ( $fileType eq 'diff_cat' ) || ( $fileType eq 'diff_nccat' ) ) {

            my $queryHashRef;
            $queryHashRef->{'FILETYPE'}  = $fileType;
            $queryHashRef->{'FILECLASS'} = $fileClass;
            $queryHashRef->{'FILENAME'}  = $fileName;
            $queryHashRef->{'RUN'}       = $fileInfoHashRef->{'RUN'};
            $queryHashRef->{'EXPOSURENAME'} = $fileInfoHashRef->{'EXPOSURENAME'};

            foreach my $key ( keys %$fileInfoHashRef ) {
                $hdrHashRef->{$key} = $fileInfoHashRef->{$key};
            }

            my $parentId =
              getParentIdFromHashRef( $ingestedFiles, $queryHashRef, $runIDS );
		$globalParentId = $parentId; # Change: Ankit. to take parent id out of this scope. a quick fix
            
	    #$exposureName .= '.fits';
            #my $exposureId =
            #  $ingestedFiles->{'src'}->{$exposureName}->{$nite}->{'id'};

            #if (not $exposureId){
            #  my $eventStr =
            #    qq{WARNING:  EXPOSURE ID does not exist in the EXPOSURE table};
            #  reportEvent(2,'STATUS',4,$eventStr);
            #}

            $fileInfoHashRef->{'PARENTID'} = $parentId;

            #$fileInfoHashRef->{'EXPOSUREID'}=$exposureId;
            $hdrHashRef->{'PARENTID'} = $parentId;

            #$hdrHashRef->{'EXPOSUREID'}=$exposureId;

        }

	# Change: Ankit. added the case to reassign BAND information from parent in case of filetype being red_cat or red_ccat
	if($fileType eq 'red_ccat' || $fileType eq 'red_cat')
	{
		$bandFromParent = $getParentInfoHash->{'band'};
		$hdrHashRef->{'BAND'} = $bandFromParent;
		$fileInfoHashRef->{'BAND'} = $bandFromParent;
	}

        #
        # Get the fileid
        #
        if ($doUpdate) {

            if ( $fileClass ne 'src' ) {

                if ( defined $exposureName && $exposureName ne q() ) {
                    $fileid =
                      $ingestedFiles->{$fileType}->{$fileName}->{$run}
                      ->{$exposureName}->{'id'};
                }
                else {
                    $fileid =
                      $ingestedFiles->{$fileType}->{$fileName}->{$run}->{'id'};
                }

            }
            else {

                $fileid =
                  $ingestedFiles->{$fileType}->{$fileName}->{$nite}->{'id'};

            }

        }
        else {
            $fileid = getNextFileID($desdbh);
        }

 # Change: Ankit. added this if else condition to avoid running complete loop to find the file entry in the main $files array. If the positionInMainLoop variable has the value for this file entry, just use that to access the file entry in the files array, and update the fileid there.
        if($positionInMainLoop >= 0)
        {
               $files[$positionInMainLoop]->{'fileid'} = $fileid;
                $eventStr = qq{The iterator for loop position was found :) in its loop: $fileName};
                #reportEvent( $verboseLevel, 'STATUS', 3, $eventStr );
        	#print "the iterator";   print Dumper($files[$positionInMainLoop]->{'fileid'});
        }
        else
        {
                updateFilelist( \@files, $baseName, $localPath, $fileid );
                $eventStr = qq{The iterator for loop position did not have this in its loop: $fileName};
                reportEvent( $verboseLevel, 'STATUS', 3, $eventStr );
        }
	#Change end

        my $metaHashRef = $metaTablesHashRef->{$metaTable} if $metaTable;

        #
        # Now that we have the fileid, write the wcs info to a text file for
        # use by pix2wcs in updating center pixel RA/DEC
        #
        if (
            $isFits
            && (   ( $fileType eq 'red' )
                || ( $fileType eq 'remap' )
                || ( $fileType eq 'raw_obj' )
                || ( $fileType eq 'coadd' )
                || ( $fileType eq 'coadd_det' ) )
          )
        {
	    $flagWCSWritten = 1;
            print $WCSFILE "$fileid,$xcen,$ycen,";
            foreach my $param (@wcsParams) {
                if ( $param eq 'NAXIS' ) {
                    print $WCSFILE "2,";
                }
                elsif ( $param eq 'OBSDATE' ) {
                    print $WCSFILE "0.0,";
                }
                else {
                    if ( $hdrHashRef->{$param} ) {
                        print $WCSFILE $hdrHashRef->{$param} . ",";
                    }
                    else {
                        print $WCSFILE "0.0,";
                    }
                }
            }
            print $WCSFILE '2000.0,';
            print $WCSFILE $metaTable;
            print $WCSFILE "\n";
        }

        #
        # If ingested, populate updateHashRef
        #
        if ($doUpdate) {

            foreach my $col ( keys %$locationTableHashRef ) {
                if ( $col eq 'ID' ) {
                    push @{ $updateHashRef->{'LOCATION'}->{'ID'} }, $fileid;
                }
                elsif ( $fileInfoHashRef->{$col} ) {
                    push @{ $updateHashRef->{'LOCATION'}->{$col} },
                      $fileInfoHashRef->{$col};
                }
                else {
                    if ( $locationTableHashRef->{$col}->{'type'} eq 'VARCHAR2' )
                    {
                        #push @{ $updateHashRef->{'LOCATION'}->{$col} }, q{};
                        push @{ $updateHashRef->{'LOCATION'}->{$col} }, undef;
                    }
                    else {
                        push @{ $updateHashRef->{'LOCATION'}->{$col} }, 0;
                    }
                }
            }

            foreach my $col ( keys %$metaHashRef ) {
                if ( $col eq 'ID' ) {
                    push @{ $updateHashRef->{$metaTable}->{'ID'} }, $fileid;
                }
                elsif ( $col =~ m/(IMAGETYPE|EXPOSURETYPE|CATALOGTYPE)/i ) {
                    push @{ $updateHashRef->{$metaTable}->{$1} }, $fileType;
                }
                elsif ( $col =~ m/(IMAGENAME|EXPOSURENAME|CATALOGNAME)/ ) {
                    push @{ $updateHashRef->{$metaTable}->{$1} },
                      $fileInfoHashRef->{'FILENAME'};
                }
                elsif ( $hdrHashRef->{$col} ) {
                    push @{ $updateHashRef->{$metaTable}->{$col} },
                      $hdrHashRef->{$col};
                }
                else {
                    if ( $metaHashRef->{$col}->{'type'} eq 'VARCHAR2' ) {
                        push @{ $updateHashRef->{$metaTable}->{$col} }, undef;
                    }
                    else {
			###
                        # Added this check for the update hashref to skip adding the column, if the column is nobject_scamp. This is a hack. And we must see how can we remove this later.
                        ###
                        if($col !~ m/nobject_scamp/i){
				push @{ $updateHashRef->{$metaTable}->{$col} }, 0;
			}
                    }
                }
            }

            #
            # Update zeropoint table if coadd
            #
            if ( $fileType eq 'coadd' ) {
                my $zpHashRefArr = getCoaddZeropoint( $desdbh, $fileid );
                push @{ $updateHashRef->{'ZEROPOINT'}->{'ID'} },
                  $zpHashRefArr->[0]->{'id'};
                push @{ $updateHashRef->{'ZEROPOINT'}->{'ORIGINID'} }, 0;
                push @{ $updateHashRef->{'ZEROPOINT'}->{'IMAGEID'} },  $fileid;
                push @{ $updateHashRef->{'ZEROPOINT'}->{'INSERT_DATE'} },
                  ctime();
                push @{ $updateHashRef->{'ZEROPOINT'}->{'MAG_ZERO'} },
                  defined $hdrHashRef->{'SEXMGZPT'}
                  ? $hdrHashRef->{'SEXMGZPT'}
                  : 0.0;
                push @{ $updateHashRef->{'ZEROPOINT'}->{'SIGMA_MAG_ZERO'} },
                  defined $hdrHashRef->{'MGZPTERR'}
                  ? $hdrHashRef->{'MGZPTERR'}
                  : 0.0;
            }
        }

        $tCount++;
        next if $doUpdate;    # Go to next file in the list

        #
        # Not ingested, fill in the insertHashRef now
        #
        foreach my $col ( keys %$locationTableHashRef ) {
            if ( $col eq 'ID' ) {
                push @{ $insertHashRef->{'LOCATION'}->{'ID'} }, $fileid;
            }
            elsif ( $fileInfoHashRef->{$col} ) {
                push @{ $insertHashRef->{'LOCATION'}->{$col} },
                  $fileInfoHashRef->{$col};
            }
            else {
                if ( $locationTableHashRef->{$col}->{'type'} eq 'VARCHAR2' ) {
                    #push @{ $insertHashRef->{'LOCATION'}->{$col} }, q{};
                    push @{ $insertHashRef->{'LOCATION'}->{$col} }, undef;
                }
                else {
                    push @{ $insertHashRef->{'LOCATION'}->{$col} }, 0;
                }
            }
        }

        foreach my $col ( keys %$metaHashRef ) {
            if ( $col eq 'ID' ) {
                push @{ $insertHashRef->{$metaTable}->{'ID'} }, $fileid;
            }
            elsif ( $col =~ m/(IMAGETYPE|EXPOSURETYPE|CATALOGTYPE)/i ) {
                push @{ $insertHashRef->{$metaTable}->{$1} }, $fileType;
            }
            elsif ( $col =~ m/(IMAGENAME|EXPOSURENAME|CATALOGNAME)/ ) {
                push @{ $insertHashRef->{$metaTable}->{$1} },
                  $fileInfoHashRef->{'FILENAME'};
            }
            elsif ( $hdrHashRef->{$col} ) {    # Put what's in the header here
                push @{ $insertHashRef->{$metaTable}->{$col} },
                  $hdrHashRef->{$col};
            }
            else {                             # Handle non-defined values
                if ( $metaHashRef->{$col}->{'type'} eq 'VARCHAR2' ) {
                    #push @{ $insertHashRef->{$metaTable}->{$col} }, q{};
                    push @{ $insertHashRef->{$metaTable}->{$col} }, undef;
                }
                else {
                    push @{ $insertHashRef->{$metaTable}->{$col} }, 0;
                }
            }
        }

        #
        # If file is a coadd, make the zeropoint table insertion hash ref
        #

        if ( $fileType eq 'coadd' ) {
            my $zpId = getNextZPID($desdbh);
            push @{ $insertHashRef->{'ZEROPOINT'}->{'ID'} },          $zpId;
            push @{ $insertHashRef->{'ZEROPOINT'}->{'ORIGINID'} },    0;
            push @{ $insertHashRef->{'ZEROPOINT'}->{'IMAGEID'} },     $fileid;
            push @{ $insertHashRef->{'ZEROPOINT'}->{'INSERT_DATE'} }, ctime();
            push @{ $insertHashRef->{'ZEROPOINT'}->{'MAG_ZERO'} },
              defined $hdrHashRef->{'SEXMGZPT'}
              ? $hdrHashRef->{'SEXMGZPT'}
              : 0.0;
            push @{ $insertHashRef->{'ZEROPOINT'}->{'SIGMA_MAG_ZERO'} },
              defined $hdrHashRef->{'MGZPTERR'}
              ? $hdrHashRef->{'MGZPTERR'}
              : 0.0;

            #     $insertHashRef->{'ZEROPOINT'}->{'B'} = 0.0;
            #     $insertHashRef->{'ZEROPOINT'}->{'BERR'} = 0.0;
        }

   #
   # Convert fileInfoHashRef to lowercase keys so they can be dereferenced later
   #
        foreach my $key ( keys %{$fileInfoHashRef} ) {
            $fileInfoHashRef->{ lc($key) } = $fileInfoHashRef->{$key};
            delete( $fileInfoHashRef->{$key} );
        }

        $fileInfoHashRef->{'id'} = $fileid;

        #
        # Update the filelist with the fileID
        #
        if (
            ( defined($exposureName) && $exposureName ne q() )
            ? exists $ingestedFiles->{$fileType}->{$fileName}->{$run}->{$exposureName}
            : exists $ingestedFiles->{$fileType}->{$fileName}->{$run}
          )
        {
            $eventStr = qq{This file exists in the Big Query: $fileName};
            reportEvent( $verboseLevel, 'STATUS', 4, $eventStr );
        }
        else {
            if ( $fileClass eq 'src' ) {
                $ingestedFiles->{$fileType}->{$fileName}->{$nite} =
                  $fileInfoHashRef;
            }
            elsif ( defined $exposureName && $exposureName ne q() ) {
                $ingestedFiles->{$fileType}->{$fileName}->{$run}
                  ->{$exposureName} = $fileInfoHashRef;
            }
            else {
                $ingestedFiles->{$fileType}->{$fileName}->{$run} =
                  $fileInfoHashRef;
            }
        }
    }

}    # Big foreach loop by filetype

close($WCSFILE);
my $fileend  = new Benchmark;

my $updateRows = 0;
my $insertRows = 0;

print "STATUS1BEG Doing the batch insertions/updates...STATUS1END\n";
my $filediff = timediff( $fileend, $filestart);
print "STATUS1BEG time to read/process $tCount files:  ", timestr( $filediff, 'all' ),
  " STATUS1END\n";


if ($updateHashRef) {
    $updateRows = scalar( @{ $updateHashRef->{'LOCATION'}->{'FILETYPE'} } );
}

if ($insertHashRef) {
    $insertRows = scalar( @{ $insertHashRef->{'LOCATION'}->{'FILETYPE'} } );
}

#$insertHashRef->{'EXPOSURE'}=$updateHashRef->{'EXPOSURE'};
#$insertRows = scalar(@{$insertHashRef->{'EXPOSURE'}->{'EXPOSURETYPE'}}) ;
#delete %$updateHashRef->{'EXPOSURE'};


$now = `date`;
chomp($now);
print "STATUS1BEG Starting batch ingest: " . $now . " STATUS1END\n";

#print "\n \nthe updated hash ref:"; print Dumper($updateHashRef);
print "Updating...\n";
my $numRowsUpdated = batchIngest( $desdbh, $updateHashRef, 1 )
 if $updateRows > 0;
#print "\n the insert hash ref:"; print Dumper($insertHashRef);
print "Inserting...\n";
my $numRowsIngested = batchIngest( $desdbh, $insertHashRef, 0 )
  if $insertRows > 0;

print "Done!\n";
#
# Run pix2wcs to ingest the center pixel RA/DECs of all images
#

my @wcsLines = `$Bin/pix2wcs $wcsFile` if ( -s $wcsFile );
if ($@) {
    print "STATUS4BEG Problem running pix2wcs $@ STATUS4END\n";
}
elsif(!defined $flagWCSWritten)
{
	unlink($wcsFile);
}

my $updateRADECHashRef;
foreach my $line (@wcsLines) {
    chomp($line);
    my ( $imageid, $ra, $dec, $metaTable ) = split /,/, $line;
print "==> $metaTable $imageid $ra $dec\n";
    push @{ $updateRADECHashRef->{$metaTable}->{'ID'} },  $imageid;
    push @{ $updateRADECHashRef->{$metaTable}->{'RA'} },  $ra;
    push @{ $updateRADECHashRef->{$metaTable}->{'DEC'} }, $dec;
}

print "Size of hash: " . keys( %$updateRADECHashRef ) . "\n";
$numRowsUpdated = batchIngest( $desdbh, $updateRADECHashRef, 1 );

$desdbh->commit();
$desdbh->disconnect;

#
# Write the modified config file back out to disk
#
writeFileList( $fileList, \@files );

$eventStr = qq{*** FILE INGEST COMPLETE ***};
reportEvent( $verboseLevel, 'STATUS', 1, $eventStr );

$now = `date`;
chomp($now);
print "STATUS1BEG file_ingest end " . $now . " STATUS1END\n";

my $t2 = new Benchmark;
my $diff = timediff( $t2, $t1 );
print "STATUS1BEG file_ingest total time:  ", timestr( $diff, 'all' ),
  " STATUS1END\n";

exit($exitStatus);

#
# Subroutines
#

sub usage {

    my $message = $_[0];
    if ( defined $message && length $message ) {
        $message .= "\n"
          unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

    print STDERR (
        $message,
        "\n"
          . "usage: $command "
          . " -filelist files -archivenode archive checksum i\n"
          . "       filelist contains the list of files along with the full path\n"
          . "       archivenode corresponds to one of the known archive nodes:\n"
          . "          bcs, des1, etc...\n"
    );

    die("\n")

}
