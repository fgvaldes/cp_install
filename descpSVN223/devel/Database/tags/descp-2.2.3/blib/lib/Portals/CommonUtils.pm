package Portals::CommonUtils;

use strict;
use warnings;
use Data::Dumper;

require Exporter;
our @ISA = qw(Exporter);

our @EXPORT = qw{
  containsValue
  HelloWorld
};

## Uncomment following to run all tests
#AllTests();

sub AllTests{
	testContainsValue();
	HelloWorld();
}

##Test print routine
sub HelloWorld{
	return 'Hello World';
}
## containsValue function tests whether $value string is contained in $arrayRef.
sub containsValue {
	my ($arrayRef, $value ) = @_;
	my @array = ();
	foreach my $arrayElement (@{$arrayRef}) {
		push( @array, $arrayElement->[0] );
	}
	undef my %contains_element;
	for (@array) { $contains_element{$_} = 1; }
	return $contains_element{$value};		
}

sub testContainsValue {
	my $projects = [ ['DES'], ['BCS'], ['SCS'], ['SPT'], ['CFH'] ];
	my $defaultProject = 'DESa';
	my $contains_project = containsValue($projects, $defaultProject );
	if($contains_project){
		print 'contains ' . $defaultProject;
	}
	else {
		print 'does not contains ' . $defaultProject;
	}
}

1;
