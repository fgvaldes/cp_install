#######################################################################
#  $Id: Uberftp.pm 6763 2011-05-18 16:46:06Z mgower $
#
#  $Rev:: 6763                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-05-18 09:46:06 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package NCSA::Transfer::Uberftp;
use strict;
use warnings;
use Carp;
use FindBin;
use Data::Dumper;
use Exception::Class('bail');
use lib ("$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib");
use base qw(NCSA::Common);
use NCSA::Transfer::Util qw(:all);
use NCSA::Transfer::Exceptions;
use NCSA::String::Util;
use List::Util qw(sum shuffle);

#use vars qw(@ISA);
#our @ISA = qw(NCSA::Common);

my %ClassData = (
  'uberftp_client' => 'uberftp',
);

#################################################################################
# uber_scan_dir
#
#
#################################################################################
sub uber_scan_dir {
  my $this = shift;
  my $args = shift;
  my ($cmd, @names, $path, @err);
  my $stat = 0;

  ###########################################################################
  # Args:
  # scan_dir: Absolute path of directory to be scanned.
  # start_dir: Same as scan_dir, but will remain static upon recursive calling.
  # filenames_ref: Array ref to store file names.
  # sizes_ref: Array ref to store file sizes.
  # dlist_ref: Array ref containing at least scan_dir.
  # ls_filter: String to match as *<string>* is ls command.
  # file_exclude: regex of file names to exclude (optional).
  # dir_excludes: Array ref of regex of directory names to exclude (optional).
  # recursive: Define this key to enable recursive parsing (optional).
  # uberftp: string that will invoke a installed uberftp command, along with 
  # target server. (e.g. uberftp mss.ncsa.uiuc.edu) 
  #
  ###########################################################################

  if ($args->{'recursive'}) {
    if ($args->{'ls_filter'}) {
      $cmd = $args->{'uberftp'} . " \"ls -r $args->{'scan_dir'}/*$args->{'ls_filter'}*\"";
    }
    else {
      $cmd = $args->{'uberftp'} . " \"ls -r $args->{'scan_dir'}\"";
    }
  }
  else {
    if ($args->{'ls_filter'}) {
      $cmd = $args->{'uberftp'} . " \"ls $args->{'scan_dir'}/*$args->{'ls_filter'}*\"";
    }
    else {
      $cmd = $args->{'uberftp'} . " \"ls $args->{'scan_dir'}\"";
    }
  }

  print "\n$cmd\n" if ($args->{'verbose'});

  my $timeout = 120;
  
  eval {
    local $SIG{ALRM} = sub { die "alarm\n" };
    alarm $timeout;

    open (OUT, "$cmd 2>&1 |");

    # Skip ahead to actual listing
    while (<OUT> !~ /230 User \S* logged in./){}

    LINE:while (<OUT>) {
      if (/No such file or directory/ || /^500/) {
        $stat = 2;
        last;
      }
      my @line = split /\s+/;
      next if ($line[0] =~ m/\d/);
      my $name = $line[$#line];
#      next if ($name eq '.');  # Skip current dir.
#      next if ($name eq '..'); # Skip "up one" dir.
      my $size = $line[$#line-4];
      if ($line[0] =~ m/^d/) {
        foreach my $exclude (@{$args->{'dir_excludes'}}) {
           next LINE if ($name =~ /$exclude/);
        }
        push (@{$args->{'dlist_ref'}},$name);
      }
      else {
        foreach my $exclude (@{$args->{'file_excludes'}}) {
          next LINE if ($name =~ /$exclude/);
        }
        push (@{$args->{'filenames_ref'}},$name);
        push (@{$args->{'sizes_ref'}},$size);
      }
    }
    close OUT;
    alarm 0;
   };

return $stat;

}

#################################################################################
# getUberOptionCommands
#
#  Parse command line options from uberftp options hash
#################################################################################
sub getUberOptionCommands {
  my $this = shift;
  my $OptsRef = shift;
  my $cmd;
    while ((my $key, my $val) = each %$OptsRef) {
      if ($key !~ /^-/) {    
        if ($cmd) {
          $cmd = $cmd."$key $val\n";
        }
        else {
          $cmd = "$key $val\n";
        }
      }
    }
  return $cmd;
}


#################################################################################
#
#################################################################################
sub verifyRemoteFiles {
  my $this = shift;
  my $lsArgsRef = shift;
  my $ArgsRef = shift;

  ################################################################
  # ARGUMENTS:
  # verifyRemoteFiles({lsargs},{Args});
  # The first hashref "lsargsRef" holds arguments specific
  # to the verification process:
  #   top_dir
  #   files
  #   list_method
  # The second hashref argument is the usual uberftp argument hash.
  # See runUberCommandFile  and transferUber.
  #
  ################################################################

  my $cmds;
  # Set uberftp to use debug output and combine
  # STDOUT and STDERR to one file ('out_file').
  my $Base;
  if (! $ArgsRef->{'BaseFileName'}) {
    $ArgsRef->{'BaseFileName'} = 'uber-verify';
  }
  $Base = $ArgsRef->{'BaseFileName'};
  if (! $ArgsRef->{'cmd_file'}) {
    $ArgsRef->{'cmd_file'}  = join('.',$Base,'cmds');
  }
  if (! $ArgsRef->{'out_file'}) {
    $ArgsRef->{'out_file'}  = join('.',$Base,'out');
  }
  if (! $ArgsRef->{'err_file'}) {
    $ArgsRef->{'err_file'}  = join('.',$Base,'err'); 
  }
  if ($ArgsRef->{'log_prefix'}) {
    $ArgsRef->{'cmd_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'cmd_file'});
    $ArgsRef->{'out_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'out_file'});
    $ArgsRef->{'err_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'err_file'});
  }

  # Need to check to see if this can be turned of for MLST:
  $ArgsRef->{'options'}->{'-d'}  = ''; 

  # Check "list_method" provide default if needed.
  if (defined $lsArgsRef->{'list_method'}) {
    if ($lsArgsRef->{'list_method'} !~/LIST|MLST/) {
      bail->throw("verifyRemoteFiles: Unknown list_method option provided: $lsArgsRef->{'list_method'}\n")
    }
  }
  else {
    $lsArgsRef->{'list_method'} = 'LIST';
  }


  #################################################
  # Begin retry loop, for timed out connections
  #################################################
  my $trys = 10;
  my $try = 0;
  my $retry_flag = 0;


  for (my $try_num = 0; $try_num < $trys; $try_num++) {
  # Do a relative set of list commands if "top_dir"
  # is specified

  $cmds = '';
  if ($ArgsRef->{'options'}) {
    #$cmds = getUberOptionCommands($ArgsRef->{'options'});
    $cmds = $this->getUberOptionCommands($ArgsRef->{'options'});
  }
  if ($lsArgsRef->{'top_dir'}) {
    if ($cmds) {
      $cmds = $cmds."cd $lsArgsRef->{'top_dir'}\n";
    }
    else {
      $cmds = "cd $lsArgsRef->{'top_dir'}\n";
    }
  }
  
  
  # Write a list command for every key in the provided hash.  If we're
  # on a second or nth try, the file should not try to re-verify anything
  # already succesfully checked.
  if ($lsArgsRef->{'files'}) {
    if ($lsArgsRef->{'list_method'} eq 'LIST') {
      foreach my $file (keys %{$lsArgsRef->{'files'}} ) {
        if (! exists $lsArgsRef->{'files'}->{"$file"}->{'checked_size'}) {
          if ($cmds) {
            $cmds = $cmds."ls $file\n";
          }
          else {
            $cmds = "ls $file\n";
          }
        }
      }
    }
    elsif($lsArgsRef->{'list_method'} eq 'MLST') {
      foreach my $file (keys %{$lsArgsRef->{'files'}} ) {
        if (! exists $lsArgsRef->{'files'}->{"$file"}->{'checked_size'}) {
          if ($cmds) {
            $cmds = $cmds."quote MLST $file\n";
          }
          else {
            $cmds = "quote MLST $file\n";
          }
        }
      }
    }

  }
  else {
    bail->throw("Must provide a hash of files to verify.\n");
  }

  # Tag output file with try number:
  if ($ArgsRef->{'cmd_file'}) {
    if ($ArgsRef->{'cmd_file'} =~ /\./) {
      $ArgsRef->{'cmd_file'} =~ /(\S+)\.([^\.]+)$/;
      my $new = join('_',$1,$try_num);
      $ArgsRef->{'cmd_file'} = join('.',$new,$2);
    }
    else {
      $ArgsRef->{'cmd_file'} = join('_',$ArgsRef->{'cmd_file'},$try_num);
    }
  }
  if ($ArgsRef->{'out_file'}) {
    if ($ArgsRef->{'out_file'} =~ /\./) {
      $ArgsRef->{'out_file'} =~ /(\S+)\.([^\.]+)$/;
      my $new = join('_',$1,$try_num);
      $ArgsRef->{'out_file'} = join('.',$new,$2);
    }
    else {
      $ArgsRef->{'out_file'} = join('_',$ArgsRef->{'out_file'},$try_num);
    }
  }
  if ($ArgsRef->{'err_file'}) {
    if ($ArgsRef->{'err_file'} =~ /\./) {
      $ArgsRef->{'err_file'} =~ /(\S+)\.([^\.]+)$/;
      my $new = join('_',$1,$try_num);
      $ArgsRef->{'err_file'} = join('.',$new,$2);
    }
    else {
      $ArgsRef->{'err_file'} = join('_',$ArgsRef->{'err_file'},$try_num);
    }
  }
  
  # Write command file and run.
  open (FH,">$ArgsRef->{'cmd_file'}");
  print FH $cmds;
  close FH;
  #my $pid = runUberCommandFile($ArgsRef);
  my $pid = $this->runUberCommandFile($ArgsRef);
  waitpid($pid,0);

  # Parse output and verify
  open (FH, "<$ArgsRef->{'out_file'}") or bail->throw("Unable to open: $ArgsRef->{'out_file'}");
  my @outfile = <FH>;
  close FH;


  # Check for uberftp errors:
  my $errcheck;
  if ($ArgsRef->{'err_file'}) {
    open (FH, "<$ArgsRef->{'err_file'}") or bail->throw("Unable to open: $ArgsRef->{'err_file'}");
    my @errfile = <FH>;
    close FH;
    $errcheck = \@errfile;
  }
  elsif($ArgsRef->{'out_file'}) {
    #checkUberErrors(\@outfile);
    $errcheck = \@outfile;
  }
  eval {
    $this->checkUberErrors($errcheck);
  };
 
  my $e;
  my $etmo;
  # If we catch an error that a retry might help, go ahead and set the retry
  # flag.
  if ($e = Exception::Class->caught()) {
    if ($etmo = NCSA::Transfer::Exceptions::UberTimeout->caught()) {
      warn "$etmo\n";
      $retry_flag = 1;
    }
    else {
      $retry_flag = 0;
      ref $e ? $e->rethrow : die $e;
    }
  }
  my @cmdLines;

  #####################################################################
  # Verify, parsing.  These parsing section result in additional data 
  # being added to the hash that was passed in.  For each key's value
  # becomes a hashref:
  #   checked_size => filesize
  #           or...
  #   errstr => error
  # This data must be used in the calling program to assess verfication 
  # status.
  #####################################################################

  if ($lsArgsRef->{'list_method'} eq 'LIST') {
    # Pass 1: Get LIST command line numbers and filename keys:  
    for(my $ln=0; $ln<=$#outfile; $ln++) {
      my $line = $outfile[$ln];
      if ($line =~ /cmd:LIST/) {
         push(@cmdLines,$ln);
      }
    }
    # Pass 2: Check to see how each command did:
    CMD: for(my $cmd=0; $cmd<=$#cmdLines; $cmd++) {
      my $start = $cmdLines[$cmd];
      my $stop;
      if ($cmd == $#cmdLines) {
        $stop = $#outfile;
      }
      else {
        $stop = $cmdLines[$cmd+1]-1;
      }
#      print "\ncmd $cmd:\n";
      my $file = '';
      for(my $ln=$start; $ln<=$stop; $ln++) {
        my $line = $outfile[$ln];
        if ($line =~ /^cmd:LIST\s*(.*)$/) {
          $file = $1;
#          print "$file\n";
          next;
        }
        elsif ($line =~ /^-/) {
          my @line = split /\s+/, $line;
#          print $line; 
          $lsArgsRef->{'files'}->{"$file"}->{'checked_size'} = $line[$#line-4];      
          next CMD;
        }
        elsif ($line =~ /^500-Command failed/) {
          if ($line =~ /Permission denied/) {
#            print $line;
            $lsArgsRef->{'files'}->{"$file"}->{'errstr'} = 'Permission denied';      
            next CMD;
          }
          elsif ($line =~ /No such file or directory/) {
#            print $line;
            $lsArgsRef->{'files'}->{"$file"}->{'errstr'} = 'No such file or directory';      
            $lsArgsRef->{'files'}->{"$file"}->{'NotThere'} = 1;      
            next CMD;
          }
        }
        #elsif ($line =~ /Could not list.*451.*not found$/) {
        elsif ($line =~ /451\s*$file\s*not\s*found\s*$/) {
          $lsArgsRef->{'files'}->{"$file"}->{'errstr'} = 'No such file or directory';      
          $lsArgsRef->{'files'}->{"$file"}->{'NotThere'} = 1;      
          next CMD;
        }
        
      }
    }
  }
  elsif ($lsArgsRef->{'list_method'} eq 'MLST') {
    # Pass 1: Get MSLT command line numbers and provides filename keys:  
    for(my $ln=0; $ln<=$#outfile; $ln++) {
      my $line = $outfile[$ln];
      if ($line =~ /cmd:MLST/) {
         push(@cmdLines,$ln);
      }
    }

    # Pass 2: Check to see how each command did:
    CMD: for(my $cmd=0; $cmd<=$#cmdLines; $cmd++) {
      my $start = $cmdLines[$cmd];
      my $stop;
      if ($cmd == $#cmdLines) {
        $stop = $#outfile;
      }
      else {
        $stop = $cmdLines[$cmd+1]-1;
      }
#      print "\ncmd $cmd:\n";
      my $file = '';
      for(my $ln=$start; $ln<=$stop; $ln++) {
        my $line = $outfile[$ln];
        if ($line =~/.*cmd:MLST\s*(.*)$/) {
          $file = $1;
#          print "$file\n";
        }
        elsif($line =~ /^\s/) {
          (my $space, my $facts, my $path) = split(/\s/, $line);
          #if ($path =~/No such file or directory/) {
          #  $lsArgsRef->{'files'}->{"$file"}->{'errstr'} = 'No such file or directory';
          #  next CMD;
          #}
          my @factlist = split(/;/,$facts);
          my %FactHash;
          foreach (@factlist) {
            (my $key, my $val) = split /=/;
            $FactHash{"$key"} = $val;
          }
          if (exists $FactHash{'Size'}) {
            $lsArgsRef->{'files'}->{"$file"}->{'checked_size'} = $FactHash{'Size'};
          } 
          next CMD; 
        }

        elsif ($line =~ /^500-Command failed/) {
          if ($line =~ /Permission denied/) {
#            print $line;
            $lsArgsRef->{'files'}->{"$file"}->{'errstr'} = 'Permission denied';
            next CMD;
          }
          elsif ($line =~ /No such file or directory/) {
            #print $line;
            $lsArgsRef->{'files'}->{"$file"}->{'errstr'} = 'No such file or directory';
            $lsArgsRef->{'files'}->{"$file"}->{'NotThere'} = 1;      
            next CMD;
          }
        }
      }
    } # End command loop

  } # End list_method switch.

   last if (! $retry_flag);

  } # End retry loop

}


#################################################################################
#################################################################################
sub stageUber {
  my $this = shift;
  my $StageArgs = shift;
  my $Args = shift;

  my $cmds;
  # Set uberftp to use debug output and combine
  # STDOUT and STDERR to one file ('out_file').
  if (! $Args->{'cmd_file'}) {
    $Args->{'cmd_file'}  = 'uber-stage.cmds';
  }
  if (! $Args->{'out_file'}) {
    $Args->{'out_file'}  = 'uber-stage.out';
  }
  if (! $Args->{'err_file'}) {
    $Args->{'err_file'}  = 'uber-stage.err';
  }
  if ($Args->{'log_prefix'}) {
    $Args->{'cmd_file'} = join('/',$Args->{'log_prefix'},$Args->{'cmd_file'});
    $Args->{'out_file'} = join('/',$Args->{'log_prefix'},$Args->{'out_file'});
    $Args->{'err_file'} = join('/',$Args->{'log_prefix'},$Args->{'err_file'});
  }

  $Args->{'options'}->{'-d'}  = '';

  # Do a relative set of list commands if "top_dir"
  # is specified
  if ($Args->{'options'}) {
    $cmds = $this->getUberOptionCommands($Args->{'options'});
  }
  if ($StageArgs->{'top_dir'}) {
    if ($cmds) {
      $cmds = $cmds."cd $StageArgs->{'top_dir'}\n";
    }
    else {
      $cmds = "cd $StageArgs->{'top_dir'}\n";
    }
  }

  if ($StageArgs->{'files'}) {
    foreach my $file (@{$StageArgs->{'files'}} ) {
      if ($cmds) {
        $cmds = $cmds."quote site stage 0 $file\n";
      }
      else {
        $cmds = "quote site stage 0 $file\n";
      }
    }
  }
  else {
    bail->throw("Must provide an array of files to remove.\n");
  }

 # Write command file and run.
  open (FH,">$Args->{'cmd_file'}");
  print FH $cmds;
  close FH;
  #my $pid = runUberCommandFile($ArgsRef);
  my $pid = $this->runUberCommandFile($Args);
  waitpid($pid,0);

}

#################################################################################
#################################################################################
sub removeRemoteFiles{
  my $this = shift;
  my $rmArgsRef = shift;
  my $ArgsRef = shift;

  my $cmds;
  # Set uberftp to use debug output and combine
  # STDOUT and STDERR to one file ('out_file').
  if (! $ArgsRef->{'cmd_file'}) {
    $ArgsRef->{'cmd_file'}  = 'uber-remove-cmds';
  }
  if (! $ArgsRef->{'out_file'}) {
    $ArgsRef->{'out_file'}  = 'uber-remove.out';
  }
  if (! $ArgsRef->{'err_file'}) {
    $ArgsRef->{'err_file'}  = 'uber-remove.err';
  }
  if ($ArgsRef->{'log_prefix'}) {
    $ArgsRef->{'cmd_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'cmd_file'});
    $ArgsRef->{'out_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'out_file'});
    $ArgsRef->{'err_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'err_file'});
  }

  $ArgsRef->{'options'}->{'-d'}  = '';

  # Do a relative set of list commands if "top_dir"
  # is specified
  if ($ArgsRef->{'options'}) {
    #$cmds = getUberOptionCommands($ArgsRef->{'options'});
    $cmds = $this->getUberOptionCommands($ArgsRef->{'options'});
  }
  if ($rmArgsRef->{'top_dir'}) {
    if ($cmds) {
      $cmds = $cmds."cd $rmArgsRef->{'top_dir'}\n";
    }
    else {
      $cmds = "cd $rmArgsRef->{'top_dir'}\n";
    }
  }

  if ($rmArgsRef->{'files'}) {
    foreach my $file (keys %{$rmArgsRef->{'files'}} ) {
      if ($cmds) {
        #$cmds = $cmds."rm $file\n";
        $cmds = $cmds."quote DELE $file\n";
      }
      else {
        #$cmds = "rm $file\n";
        $cmds = "quote DELE $file\n";
      }
    }
  }
  else {
    bail->throw("Must provide a hash of files to remove.\n");
  }

 # Write command file and run.
  open (FH,">$ArgsRef->{'cmd_file'}");
  print FH $cmds;
  close FH;
  #my $pid = runUberCommandFile($ArgsRef);
  my $pid = $this->runUberCommandFile($ArgsRef);
  waitpid($pid,0);
    
  # Parse output and verify
  open (FH, "<$ArgsRef->{'out_file'}") or bail->throw("Unable to open: $ArgsRef->{'out_file'}");
  my @outfile = <FH>;
  close FH;
  
  if ($ArgsRef->{'err_file'}) {
    open (FH, "<$ArgsRef->{'err_file'}") or bail->throw("Unable to open: $ArgsRef->{'err_file'}");
    my @errfile = <FH>;
    close FH;
    #checkUberErrors(\@errfile);
    $this->checkUberErrors(\@errfile);
  }
  elsif($ArgsRef->{'out_file'}) { 
    #checkUberErrors(\@outfile);
    $this->checkUberErrors(\@outfile);
  }
  
  my @cmdLines;
 
#for(my $ln=0; $ln<=$#outfile; $ln++) {
#      my $line = $outfile[$ln];
#      if ($line =~ /cmd:MLST/) {
#         push(@cmdLines,$ln);
#      }
#    }
#
}
#################################################################################
#################################################################################
sub checkUberErrors {
  my $this = shift;
  my $errARef = shift;

  for(my $ln=0;$ln<=$#$errARef;$ln++) {
    my $line = $errARef->[$ln];
    my @err_lines;
    my $new_line;
    if ($line =~/globus_ftp_control: gss_init_sec_context failed/) {
      $new_line = $line;
      my $nln = $ln;
      while ($new_line !~ /Closing connection to service/ && $nln <=$#$errARef) {
        push (@err_lines, $new_line);
        $nln++;
        $new_line = $errARef->[$nln]; 
      }
      NCSA::Transfer::Exceptions::runUber->throw(error => "Uberftp Globus connection failure.\n", err_lines => \@err_lines);
    }
    elsif ($line =~/530-Login incorrect/) {
      $new_line = $line;
      my $nln = $ln;
      while ($nln <=$#$errARef) {
        push (@err_lines, $new_line);
        $nln++;
        $new_line = $errARef->[$nln];
      }
      NCSA::Transfer::Exceptions::runUber->throw(error => "Uberftp Login failure.\n", err_lines => \@err_lines);
    }

    elsif ($line =~/Resource temporarily unavailable/) {
      $new_line = $line;
      my $nln = $ln;
      #while ($nln <=$#$errARef) {
      #  push (@err_lines, $new_line);
      #  $nln++;
      #  $new_line = $errARef->[$nln];
      #}
      NCSA::Transfer::Exceptions::runUber->throw(error => "Uberftp: \"Resource temporarily unavailable\" Error.\n");
    }
    elsif ($line =~/Timeout waiting for server response./ || $line =~/Timeout on data channel/) {
      $new_line = $line;
      my $nln = $ln;
      NCSA::Transfer::Exceptions::UberTimeout->throw(error => "$line\n");
    }
    elsif ($line =~/530-globus_xio:/) {
      $new_line = $line;
      my $nln = $ln;
      while ($nln <=$#$errARef) {
        push (@err_lines, $new_line);
        $nln++;
        $new_line = $errARef->[$nln];
      }
      NCSA::Transfer::Exceptions::runUber->throw(error => "Uberftp Globus connection failure.\n", err_lines => \@err_lines);
    }

    elsif ($line =~/530 Login incorrect/) {
      $new_line = $line;
      my $nln = $ln;
      while ($nln <=$#$errARef) {
        push (@err_lines, $new_line);
        $nln++;
        $new_line = $errARef->[$nln];
      }
      NCSA::Transfer::Exceptions::runUber->throw(error => "Uberftp Globus connection failure.\n", err_lines => \@err_lines);
    }


  }
}


#################################################################################
# createRemoteDirs:
#
#    Create directories on a remote system given an array
#################################################################################
sub createRemoteDirs {
  my $this = shift;
  my $dlist = shift;
  my $ArgsRef = shift;

  my $cmds;

  # print "\nEntered routine to create remote working directories. \n";

  if (! $ArgsRef->{'cmd_file'}) {
    $ArgsRef->{'cmd_file'}  = "uber-mkdir-cmds";
  }
  if (! $ArgsRef->{'out_file'}) {
    $ArgsRef->{'out_file'}  = "uber-mkdir.out";
  }
  if (! $ArgsRef->{'err_file'}) {
    $ArgsRef->{'err_file'}  = "uber-mkdir.err";
  }
  if ($ArgsRef->{'log_prefix'}) {
    $ArgsRef->{'cmd_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'cmd_file'});
    $ArgsRef->{'out_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'out_file'});
    $ArgsRef->{'err_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'err_file'});
  }
#  if ($ArgsRef->{'options'}) {
#    $cmds = getUberOptionCommands($ArgsRef->{'options'});
#  }
  if ($cmds) {
    $cmds = $cmds."\n".getMkdirCommands($dlist);
  }
  else {
    $cmds = getMkdirCommands($dlist);
  }
  open (FH,">$ArgsRef->{'cmd_file'}");
  print FH $cmds;
  close FH;

  my %newArgs = %{$ArgsRef};
  delete $newArgs{'options'};
  #my $pid = runUberCommandFile(\%newArgs);

 
  my $createSuccess = 0;
  my $createCount   = 0;
  my $createRetries = 8;

  while ( ($createSuccess == 0) && ($createCount < $createRetries) ) {
  
    my $pid = $this->runUberCommandFile(\%newArgs);
    waitpid($pid,0);
  
    # Parse log and die if a mkdir returns "permission denied":
    if ($ArgsRef->{'err_file'}) {
      open (FH,"<$ArgsRef->{'err_file'}") or bail->throw("Unable to open $ArgsRef->{'err_file'}\n");
    }
    else {
      open (FH,"<$ArgsRef->{'out_file'}") or bail->throw("Unable to open $ArgsRef->{'out_file'}\n");
    }
    my @file = <FH>;
    close FH;

    $createSuccess = 1;
    $createCount++;

    foreach(@file) {
      if (/^500/) {
        if (/System error in mkdir: Permission denied/) {
          # bail->throw("A bad mkdir command was issued:\n$_\n");
          $createSuccess = 0;
        }
        elsif (/No such file or directory/) {
          # bail->throw("Bad uber error: No such file or directory\n");
          $createSuccess = 0;
        }
      }
    }

    if($createSuccess == 0) { 
      print "An Uberftp Command failed: Retry ${createCount} \n";
      sleep(10); 
    }

  }
   
  if ( ($createSuccess == 0) && ($createCount == $createRetries) ) {
          bail->throw("Repeated Fatal Uberftp error \n$_\n");
  }

  # print "\nFinished making remote working directories $createSuccess $createCount \n";
  print "\nFinished making remote working directories.\n";


}

#################################################################################
# removeRemoteDirs:
#
#    Remove empty directories on a remote system given an array
#################################################################################
sub removeRemoteDirs {
  my $this = shift;
  my $dlist = shift;
  my $ArgsRef = shift;

  my $cmds;
  if (! $ArgsRef->{'cmd_file'}) {
    $ArgsRef->{'cmd_file'}  = "uber-rmdir-cmds";
  }
  if (! $ArgsRef->{'out_file'}) {
    $ArgsRef->{'out_file'}  = "uber-rmdir.out";
  }
  if (! $ArgsRef->{'err_file'}) {
    $ArgsRef->{'err_file'}  = "uber-rmdir.err";
  }
  if ($ArgsRef->{'log_prefix'}) {
    $ArgsRef->{'cmd_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'cmd_file'});
    $ArgsRef->{'out_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'out_file'});
    $ArgsRef->{'err_file'} = join('/',$ArgsRef->{'log_prefix'},$ArgsRef->{'err_file'});
  }
#  if ($ArgsRef->{'options'}) {
#    $cmds = getUberOptionCommands($ArgsRef->{'options'});
#  }
  if ($cmds) {
    $cmds = $cmds."\n".getRmdirCommands($dlist);
  }
  else {
    $cmds = getRmdirCommands($dlist);
  }
  open (FH,">$ArgsRef->{'cmd_file'}");
  print FH $cmds;
  close FH;

  my %newArgs = %{$ArgsRef};
  delete $newArgs{'options'};
  #my $pid = runUberCommandFile(\%newArgs);
  my $pid = $this->runUberCommandFile(\%newArgs);
  waitpid($pid,0);
}

#################################################################################
#  getUberCommand:
#
#    Translate command line options and target server into a system command.
#################################################################################

sub getUberCommmand {
  my $this = shift;
  my $ArgsRef = shift;

#  if (! $ArgsRef->{'uberftp'}) {
#    bail->throw("Must Provide valid uberftp command.");
#  }
  if (! $ArgsRef->{'target_server'}) {
    bail->throw("Must provide \'dest_server\'\n.");
  } 
#  my $uberftp = $ArgsRef->{'uberftp'};
  my $uberftp = $this->uberftp_client();
  my $ts = $ArgsRef->{'target_server'};
  my $opts = $ArgsRef->{'options'} if $ArgsRef->{'options'}; 
 
  # Compose uberftp system command.
  my $cmd = "$uberftp";
  if ($opts) {
    while ((my $key, my $val) = each %$opts) {
      if ($key =~ /^-/) {    
        $cmd = $cmd." $key $val";
      }
    }
  }
  $cmd = $cmd." $ArgsRef->{target_server}";
#  print "$cmd\n";
  return $cmd;
}


#################################################################################

#################################################################################
#################################################################################

sub transferUber {
  my $this = shift;
  my $Xfer;          
  my $Args = shift;

  my $ArgsARef = $Args->{'xferArgs'};
  my $verbose;
  if ($Args->{'verbose'}) {
    $verbose = $Args->{'verbose'};
  }
  else {
    $verbose = 1;
  }
  my $try = 1;
  $try = $this->try() if ($this->{'try'});

  # Set output file names:
  my $base_cmd_file = 'uber-transfer-cmds';
  my $base_out_file = 'uber-transfer';
  my $trynum = sprintf("%0*d", 2, $try);
  $base_cmd_file = join('_',$base_cmd_file,$trynum);
  $base_out_file = join('_',$base_out_file,$trynum);

  my $ctr = 0;
  my (@pids, @outfiles, @errfiles);

  prepareTransfers($ArgsARef, $base_cmd_file, $base_out_file, $verbose);

  foreach my $ArgsRef (@$ArgsARef) {
    my $srcFilesRef = $ArgsRef->{'srcfiles'};
    my $destFilesRef = $ArgsRef->{'destfiles'};
    my $FileIndexLists = $ArgsRef->{'file_index_lists'};
    for(my $ln=0;$ln<=$#$FileIndexLists;$ln++) {
      my $list = $FileIndexLists->[$ln];
      my $cmds;
      push(@outfiles, $ArgsRef->{'out_file'});
      push(@errfiles, $ArgsRef->{'err_file'}) if $ArgsRef->{'err_file'};

      # Set first commands in command file to be "options" stuff:
      if ($ArgsRef->{'src_server'}) {
        $ArgsRef->{'options'}->{'lopen'} = "$ArgsRef->{'src_server'}"; 
      }
      if ($ArgsRef->{'options'}) {
        #$cmds = getUberOptionCommands($ArgsRef->{'options'});
        $cmds = $this->getUberOptionCommands($ArgsRef->{'options'});
      }

      # Generate put commands and put them in the cmd_file:
      print "\nGenerating uberftp transfer commands" if ($verbose >= 5);
      for(my $i=0; $i<=$#$list;$i++) {
        if ($cmds) {
          $cmds = $cmds."put ".$srcFilesRef->[$list->[$i]]." ".$destFilesRef->[$list->[$i]]."\n";
        }
        else {
          $cmds = "put ".$srcFilesRef->[$list->[$i]]." ".$destFilesRef->[$list->[$i]]."\n";
        }
      }
      open (FH,">$ArgsRef->{'cmd_file'}");
      print FH $cmds;
      close FH;
      print "Done!\n" if ($verbose >= 5);
      $ArgsRef->{'verbose'} = $verbose if (defined $verbose);
      #my $pid = runUberCommandFile($ArgsRef);
      my $pid = $this->runUberCommandFile($ArgsRef);
      push(@pids, $pid);
    }
  }
  #Wait on transfer(s) to finish:
  foreach(@pids) {
    waitpid($_,0);
  }

  ##########################################
  # Verify transfer(s) output.
  ##########################################

 my @errs;
 if (@errfiles) {
   @errs = @errfiles;
 }
 else {
   @errs = @outfiles;
 }

 foreach my $file (@errs) {
    open (FH, "<$file") or bail->throw("Unable to open: $file");
    my @errfile = <FH>;
    close FH;
    $this->checkUberErrors(\@errfile);
  }
 

  my $AllAggbytes = 0;
  my $AllAggtime = 0;
  my $AllAggFileCount = 0;
  foreach my $uberout (@outfiles) {
    open(FH, "<$uberout") or bail->throw("Unable to open $uberout\n");
    my @outfile = <FH>;
    close FH;
    my (@xfer_sizes, @xfer_times, @verified);
    foreach(@outfile) { 
      #if (/.*uberftp>\s*([^:\s]+):\s*([^\s]+)\s*bytes in\s*([^\s]+)\s*(.*$)/){
      if (/.*\s*([^:\s]+):\s*([^\s]+)\s*bytes in\s*([^\s]+)\s*(.*$)/){
        # Looks like we moved a file...
        my $f = $1;
        my $s = $2;
        my $t = $3;
        my $rest = $4;
        push(@xfer_sizes, $s) if ($s);
        push(@xfer_times, $t) if ($t);
        push(@verified, $f) if ($f);
        $f =~ /([^\/]+)$/;
        my $fn = $1;
        $fn =~ /(.*):$/;
        $fn = $1;
      }
      elsif (/500-Command failed./) {
        bail->throw("A file transfer failed.\n$_\n");
      }
    }

    my $Aggbytes = List::Util::sum(@xfer_sizes);
    my $Aggtime = List::Util::sum(@xfer_times);
    $AllAggbytes = $AllAggbytes + $Aggbytes;
    $AllAggtime = $AllAggtime + $Aggtime;
    $AllAggtime = 0.00000000001 if (! $AllAggtime);
    $AllAggFileCount = $AllAggFileCount + $#verified+1;
    #printf ("\nVerified %s of %s files transferred.\n",$#verified+1, $#$srcFilesRef+1);
    if ($verbose >= 2) {
      print "\n$uberout:\n";
      printf ("Verified %s files, %4.2f %s transferred in %s seconds.\n",$#verified+1, NCSA::String::Util::make_human($Aggbytes), $Aggtime);
    }
  }
  if ($verbose >= 1) {
    (my $hrval , my $hrsuf) = NCSA::String::Util::make_human($AllAggbytes);
    print "\nTransfer Statistics:\n";
    printf ("Transferred %s files totalling %4.2f %s in %s seconds.\n",$AllAggFileCount,$hrval,$hrsuf, $AllAggtime);
    printf ("Aggregate data rate: %4.2f %s/s.\n", $hrval/$AllAggtime, $hrsuf);
  }
}

#################################################################################
#################################################################################
sub runUberCommandFile {
  my $this = shift;
  my $ArgsRef = shift;

  ##########################################
  # Arguments:
  #  uberftp
  # *target_server
  #  target_port
  #  options
  # *cmd_file
  #  out_file
  # where "*" => required.
  ##########################################

#  if (! $ArgsRef->{'uberftp'}) {
#    $ArgsRef->{'uberftp'} = 'uberftp';
#  }
  if (! $ArgsRef->{'target_server'}) {
    bail->throw("Must provide \'target_server\'.\n");
  } 
  if (! $ArgsRef->{'cmd_file'}) {
    bail->throw("Must provide \'cmd_file\'.\n");
  }
  if (! $ArgsRef->{'out_file'}) {
    bail->throw("Must provide \'out_file\'.\n");
  }

  # Compose uberftp system command,  Fork uberftp process
  # and return log file in case they were not specified:
  #my $cmd = getUberCommmand($ArgsRef);

  my $cmd = $this->getUberCommmand($ArgsRef);

  if ($this->debug()){
    print "\nBase Uberftp command:\n";
    print "$cmd\n\n";
  }

  if ($ArgsRef->{'err_file'}) {
    $cmd = "cat $ArgsRef->{'cmd_file'} | ".$cmd." 1>$ArgsRef->{'out_file'} 2>$ArgsRef->{'err_file'}";
  }
  else {
    $cmd = "cat $ArgsRef->{'cmd_file'} | ".$cmd." > $ArgsRef->{'out_file'} 2>&1";
  }
  my $pid = fork_process($cmd);
  if (exists $ArgsRef->{'verbose'}) {
    if ($ArgsRef->{'verbose'} >= 2) {
      print "Executing command:\n$cmd\npid: $pid\n";
    }
  }

  return $pid;
}

sub uberftp_client {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    # set locally first
    $ClassData{'uberftp_client'} = $newvalue;

    # then pass the buck up to the first 
    # overridden version, if there is one
    if ($self->can("SUPER::uberftp_client")) {
      $self->SUPER::uberftp_client($newvalue);
    }
  }
  return $ClassData{'uberftp_client'};
}

1;


=head1 NAME

NCSA::Transfer::Uberftp

=head1 SYNOPSIS

use NCSA::Transfer::Uberftp;
use NCSA::Transfer::Uberftp qw(:all);

=head1 DESCRIPTION

A collection of routines for using uberftp in perl scripts.

=head1 FUNCTIONS

=over 4

=item C<runUberCommandFile>

Gerneric uberftp execution routine.  This routine will take a command file and pipe 
it to the STDIN of a uberftp process.

=item C<createRemoteDirs>

Creates directories on a remote filesystem.

=item C<removeRemoteFiles>

Delete a list of file from the remote file system.

=item C<transferFiles>

Fork a process to transfer a list of files.

=item C<verifyRemoteFiles>

Verify that file exist on a remote file system.

=back
