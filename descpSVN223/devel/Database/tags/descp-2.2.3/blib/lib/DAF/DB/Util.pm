########################################################################
#  $Id:                                    $
#
#  $Rev:: 552                              $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2007-12-05 12:5      $:  # Date of last commit.
#
#  Authors: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package DAF::DB::Util;
use strict;
use warnings;
use Switch;
use Carp;
use List::Util;
use FindBin;
use Exception::Class('bail');
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use NCSA::String::Util;
use DAF::Archive::Tools;
use base qw(DB::DESUtil);

#######################################################################
# Constructor.
#######################################################################
sub new {
  my ($this, %Args) = @_;

  # Add hashkey case conversion:
  $Args{'DBIattr'}->{'FetchHashKeyName'} = 'NAME_lc';

  my $class = ref($this) || $this;
  my $self  = $class->SUPER::new(%Args);
  $self->verbose($Args{'verbose'}) if ($Args{'verbose'});
  return $self;
}

package DAF::DB::Util::db;
use DAF::Archive::Tools qw(getArchiveLocation);
use Data::Dumper;
use Carp;
our @ISA = qw(DB::DESUtil::db);

#######################################################################
# Class Data with default values:
#######################################################################
{
  my %ClassData = (
    'LFileTable' => 'location',
    'FlavorData' => {
      'F'=>{'extension' => 'fz',  'size_field' => 'filesize_fz'},
      'G'=>{'extension' => 'gz',  'size_field' => 'filesize_gz'},
      'Y'=>{'extension' => undef, 'size_field' => 'filesize'},
      'N'=>{'extension' => undef, 'size_field' => undef},
    },
    'FileData' => {},
    'SiteData' => {},
  );

  ################################################################################
  # Data accesor methods
  ################################################################################
  sub LFileTable {
    my($self, $newvalue) = @_;
    if (@_ > 1) {
      # set locally first 
      $ClassData{'LFileTable'} = $newvalue;

      # then pass the buck up to the first 
      # overridden version, if there is one
      if ($self->can("SUPER::LFileTable")) {
        $self->SUPER::LFileTable($newvalue);
      }
    }
    return $ClassData{'LFileTable'};
  }

  sub FlavorData {
    my($self, $newvalue) = @_;
    if (@_ > 1) {
      # set locally first 
      $ClassData{'FlavorData'} = $newvalue;

      # then pass the buck up to the first 
      # overridden version, if there is one
      if ($self->can("SUPER::FlavorData")) {
        $self->SUPER::FlavorData($newvalue);
      }
    }
    return $ClassData{'FlavorData'};
  }
}

################################################################################
# SUBROUTINE: getDBTableInfo
#
# To make this generic actually look up the primary key from the DB 
# rather than hard coding what we know it to be
################################################################################
sub getDBTableInfo {
   my $desdb = shift;
   my $tablestr = shift;
   my $RowsRef = shift;

   my $pkey;
   $tablestr =~ tr/A-Z/a-z/;
   switch ($tablestr) {
      case /site/ { 
        $tablestr = "SITES"; 
        $pkey = 'SITE_NAME';
      }
      case /archive/ { 
        $tablestr = "ARCHIVE_SITES"; 
        $pkey = 'LOCATION_NAME';
      }
      case /software/ { 
        $tablestr = "SOFTWARE_LOCATIONS"; 
        $pkey = 'LOCATION_NAME';
      }
      else {
         print STDERR "ERROR: unknown table ($tablestr)\n";
      }
   }

   my $dbq = <<STR;
SELECT * FROM $tablestr
STR

   @$RowsRef = @{$desdb->selectall_arrayref($dbq,{Slice => {}})};
  return $pkey;
}


#######################################################################
# Return refernce to an array of valid archive location names.
#######################################################################
sub getArchiveSiteNames {
  my $desdb = shift;

  my $dbq = "SELECT LOCATION_NAME FROM ARCHIVE_SITES";
  my $names_ref = $desdb->selectcol_arrayref($dbq);
  croak ("DB failure on SQL:\n$dbq") if (! $names_ref);

  return $names_ref;
}

#######################################################################
# Check validity of an archive site name.
#######################################################################
sub isvalidArchiveSiteName {
  my $desdb = shift;
  my $name = shift;

  my $names = getArchiveSiteNames($desdb) or 
  croak("checkArchiveSiteName didn't get and site names");
  my $valid = 0;

  foreach (@$names) {
     if ($_ eq $name) {
       $valid = 1;
       last;
     }
  }
  return $valid;
}

#######################################################################
#######################################################################
sub getBasicFileInfo {
  my $desdb = shift;
  my $oRef = shift;

  my $dbq = <<STR;
SELECT IMAGEID, IMAGENAME, ARCHIVESITES
FROM FILES 
STR

  my @where;
  push (@where, join('\'','IMAGECLASS=',$oRef->{'imclass'},'') ) if ($oRef->{'imclass'});
  push (@where, join('\'','IMAGETYPE=',$oRef->{'imtype'},'') ) if ($oRef->{'imtype'});
  push (@where, join('\'','NITE=',$oRef->{'nite'},'') ) if ($oRef->{'nite'});
  push (@where, join('\'','RUNID',$oRef->{'runid'},'') ) if ($oRef->{'runid'});
  push (@where, join('\'','IMAGENAME',$oRef->{'imname'},'') ) if ($oRef->{'imname'});

  if (@where) {
    $dbq .= "WHERE ".shift(@where);
    foreach (@where) {
      $dbq .= " \nAND ".$_;
    } 
   }
  if ($desdb->verbose) {
    print "$dbq\n" if ($desdb->verbose >= 1);
  }

  my %data;

  my $start = time;
  my $sth = $desdb->prepare($dbq);
  $data{'imid'} = $desdb->selectcol_arrayref($sth,{Columns=>[1]});
  $data{'imname'} = $desdb->selectcol_arrayref($sth,{Columns=>[2]});
  $data{'asites'} = $desdb->selectcol_arrayref($sth,{Columns=>[3]});
  my $end = time;


  printf ("\n\nReturned $#{$data{'imid'}} rows in %s seconds.\n\n",$end - $start);

  return \%data;
}

#######################################################################
#
#######################################################################
sub getArchiveSiteInfo {
  my $desdb = shift;
  my $asite = shift;

  my %data;
  my $err;

  my @asites;
  if ($asite) { 
    push (@asites, $asite);
  }
  else {
    my $asitesRef = getArchiveSiteNames($desdb);
    @asites = @$asitesRef;
  }
  
  my $dbq = <<STR;
SELECT 
  ARCHIVE_SITES.LOCATION_ID, ARCHIVE_SITES.LOCATION_NAME, SITES.IS_ARCHIVE,SITES.SITE_ID,
  SITES.GRIDFTP_HOST, SITES.GRIDFTP_PORT, ARCHIVE_SITES.ARCHIVE_ROOT, SITES.SITE_NAME 
FROM SITES,ARCHIVE_SITES 
WHERE
  SITES.SITE_ID=ARCHIVE_SITES.SITE_ID 
  AND ARCHIVE_SITES.LOCATION_NAME=?
STR
  my $sth = $desdb->prepare($dbq);
  foreach my $ThisSite (@asites) {
    chomp($ThisSite);               # Remove trailing newline.
    $ThisSite =~ s/^\s*//g;         # Remove leading whitespace.
    $ThisSite =~ s/\s*$//g;         # Remove trailing whitespace.
    $sth->execute($ThisSite);
    my $row = $sth->fetchrow_hashref();

    # Translate results to a constant data structure:
    # location_id
    # location_name
    # archive_root
    # gridftp_host
    # site_name
    $data{"$row->{'location_id'}"}->{'location_id'} = $row->{'location_id'};
    $data{"$row->{'location_id'}"}->{'location_name'} = $row->{'location_name'};
    $data{"$row->{'location_id'}"}->{'gridftp_host'} = $row->{'gridftp_host'};
    $data{"$row->{'location_id'}"}->{'gridftp_port'} = $row->{'gridftp_port'};
    $data{"$row->{'location_id'}"}->{'archive_root'} = $row->{'archive_root'};
    $data{"$row->{'location_id'}"}->{'site_name'} = $row->{'site_name'};
    $data{"$row->{'location_id'}"}->{'site_id'} = $row->{'site_id'};
    $data{"$row->{'location_id'}"}->{'is_archive'} = $row->{'is_archive'};
    if ($desdb->verbose >= 4) {
      print "\nRetrieved the following \"Archive Site\" information:\n";
      while((my $k, my $v) = each %{$data{"$row->{'location_id'}"}}) {
        print "$k = $v\n";
      }
      print "\n";
    }  
  }
  $desdb->commit;

  # Just return a single set of SiteData rather than a hash of "sites"
  if ($#asites == 0) {
    my @keys = keys %data;
    return $data{"$keys[0]"};
  }
  else {
    return \%data;
  }
}

#######################################################################
# SUBROUTINE: getAllFileKeys
#######################################################################
sub getAllFileKeys {
  my $desdb = shift;
  my $Args = shift;

  # Using "LOCATION_TABLE" method name from DES databse class to pick up correct
  # name for "Location" table:
  my $table = 'LOCATION_TABLE'; 

  my %queryDB_Args = (
    'table' => $table,
    'select_fields' =>['id','project','fileclass','filetype','filename','filesize',
                       'filesize_fz','filesize_gz','run','nite','exposurename','archivesites']
  );

  my %queryDB2_Args = (
    $table => {
      'select_fields' =>['id','project','fileclass','filetype','filename','filesize',
                       'filesize_fz','filesize_gz','run','nite','exposurename','archivesites'],
      
    }
  );

  #########################################################
  #
  # Process arguments
  # 
  #########################################################
  my ($opts, $SiteInfo,$all,$ThisSiteInfo, $max_asite, $min_asite, $niter);
  my @keys = ();

  my $islist = 1;


  if (exists $Args->{'FileKeys'}) {
    $opts = $Args->{'FileKeys'};
  }
  else {
    croak("getAllFileKeys: Must provide a set of file keys");
  }

  if ($Args->{'SiteInfo'}) {
    $ThisSiteInfo = $Args->{'SiteInfo'};
    @keys = keys %$ThisSiteInfo;
    foreach (@keys) {
      if (/\D/) {  # Since a list would have all numeric keys...
       $islist = 0;
        last;
      }
    }
    if ($islist) {
      $SiteInfo = $ThisSiteInfo;
    }
    else {
      my $id = $ThisSiteInfo->{'location_id'};
      $SiteInfo = {"$id" => $ThisSiteInfo};
      @keys = keys %$SiteInfo;
    }
    $max_asite = List::Util::max(@keys);
    $min_asite = List::Util::min(@keys);
    $niter = $#keys;
  }

  $all = 1 if ($Args->{'all'}); 

  #########################################################
  #
  # Build arguments for queryDB row selection call. Apply
  # special processing to options that are not actual column
  # names.
  #
  #########################################################
  while (( my $key, my $val) = each %$opts) {
    if ($key eq 'filelist') {
      my $id_Aref;
      for(my $file=0; $file<=$#$val; $file++) {              # Kind of a hack to have to make this copy
        $id_Aref->[$file] = $val->[$file]->{'fileid'};
      }
      $queryDB2_Args{$table}->{'key_vals'}->{'ID'} = $id_Aref;
    }
    elsif ($key eq 'asite') {
      next;
    }
    elsif ($key ne 'where') {
      $queryDB2_Args{$table}->{'key_vals'}->{$key} = $val;              # These all should be columns in the location table
    }
    else {
      $queryDB2_Args{$table}->{$key} = $val;
    }
  }
  if (! $all) {
    $queryDB2_Args{$table}->{'key_vals'}->{'archivesites'} = '[^N]';    # Keep non-existant copies out of the lists
  }

  # Additional SQL for munging a "site" request into a archivesites string lookup:
  if ($opts->{'asite'}) {
    foreach my $anode (@{$opts->{'asite'}}) {
      if ($anode =~ /!/) {
        $anode =~ s/!//;
        bail->throw("$anode is not a valid archive site") if (!$desdb->isvalidArchiveSiteName($anode));
        my $asiteid = $desdb->selectrow_array("SELECT location_id from archive_sites where location_name='$anode'");
        if ($queryDB_Args{'where'}) {
          push(@{$queryDB2_Args{$table}->{'where'}},"substr(archivesites,$asiteid,1)='N'");
        }
        else {
          $queryDB2_Args{$table}->{'where'} = ["substr(archivesites,$asiteid,1)='N'"];
        }
      }
      else {
        bail->throw("$anode is not a valid archive site") if (!$desdb->isvalidArchiveSiteName($anode));
        my $asiteid = $desdb->selectrow_array("SELECT location_id from archive_sites where location_name='$anode'");
        if ($queryDB_Args{'where'}) {
          #push(@{$queryDB2_Args{$table}->{'where'}},"regexp_like(substr(archivesites,$asiteid,1),'[^N]')");
          push(@{$queryDB2_Args{$table}->{'where'}},"substr(archivesites,$asiteid,1) != 'N'");
        }
        else {
          #$queryDB2_Args{$table}->{'where'} = ["regexp_like(substr(archivesites,$asiteid,1),'[^N]')"];
          $queryDB2_Args{$table}->{'where'} = ["substr(archivesites,$asiteid,1) != 'N'"];
        }
      }
    }
  }

  print "\nExecuting Database Query...";
  my $Rows = $desdb->queryDB2 (%queryDB2_Args);
  my $N_Rows = scalar @$Rows;
  print " Retrieved $N_Rows rows.\n";

#  print "\nSleeping inside DAF::DB::Util::getAllfileKeys just after query...\n";
#  sleep 15;

  #########################################################
  #
  # Process Results
  # 
  #########################################################

  print "Processing file data...";
 
  # Look for a local file if a filelist was used:
  if (exists $opts->{'filelist'} ){
    my $flist = $opts->{'filelist'};
    for(my $i=0; $i<= $#$flist; $i++) {
      if (exists $flist->[$i]->{'srcfile'}) {
        $Rows->[$i]->{'srcfile'} = $flist->[$i]->{'srcfile'};
      }
    }
  }

  my %Duplicates;
  if ($#$Rows > -1) {
    if ($desdb->verbose() >=3 ) {
      print "\nFile listing:\n";
      print "ID          Size                FileName\n";
    }

    my %DuplicateCheckHash;
    for(my $i=0; $i<=$#$Rows; $i++){
      my $FileKeys = $Rows->[$i];

      # New condensed "row":
      $Rows->[$i] = {
        'archive_location' => DAF::Archive::Tools::getArchiveLocation($FileKeys),
        'id' => $FileKeys->{'id'},
        'filename' => $FileKeys->{'filename'},
        'filesize' => $FileKeys->{'filesize'},
        'filesize_fz' => $FileKeys->{'filesize_fz'},
        'filesize_gz' => $FileKeys->{'filesize_gz'},
        'archivesites' => $FileKeys->{'archivesites'},
      };

      # Look for duplicate database rows:
      my $relfile = $Rows->[$i]->{'archive_location'}.'/'.$Rows->[$i]->{'filename'};
      if (exists $DuplicateCheckHash{$relfile}) {
        my $in = $DuplicateCheckHash{$relfile};
        $Duplicates{$relfile}->{$i} = $Rows->[$i];
        $Duplicates{$relfile}->{$in} = $Rows->[$in];
      }
      $DuplicateCheckHash{$relfile} = $i;

      # Count replicas and create a new parameter to store this count:
      my @match = $Rows->[$i]->{'archivesites'}=~/[^N]/g;
      $Rows->[$i]->{'ncopies'} = scalar @match;

      # Grab local file location if provided:
      $Rows->[$i]->{'srcfile'} = $FileKeys->{'srcfile'} if ($FileKeys->{'srcfile'});

      # Store list of files the archivesites string says are there in site data:
      foreach my $asiteid (@keys) {
        my $char = substr ($FileKeys->{'archivesites'}, $asiteid-1,1);
        $SiteInfo->{$asiteid}->{'DBVerified'}->{$i} = 1 if ($char ne 'N');
      }
      my $size = '';

      # Walk through flavors and grab the biggest one:
      while((my $flavor, my $FlavorData) = each %{$desdb->FlavorData()}) {           
        if (my $sfield = $FlavorData->{'size_field'}) {
          if ($FileKeys->{$sfield}) {
            if ($size) {
              $size = $FileKeys->{$sfield} if ($FileKeys->{$sfield} > $size);
            }
            else {
              $size = $FileKeys->{$sfield};
            }
          }
        }
      }
      print "$FileKeys->{id}:  $size  $FileKeys->{'filename'}\n"  if ($desdb->verbose() >=3 );
    }
  }
  if (%Duplicates && scalar keys %Duplicates > 0) {
    DAF::Exception::FoundDuplicates->throw(
      duplicates => \%Duplicates,
      error => "\n\nERROR: Duplicate database entries found.\nThe file listing resulted in a list of files, whose database keys\nresolve to the same exact location and file name.\n"
    );
  }

  # Get file size and possibly print full listing

  print " Done\n";

  print "Processing size data...";
  my ($Aggbytes, $Maxbytes, $Minbytes, $Avgbytes, $nfiles) = $desdb->getFileSizeInfo($Rows);
  print " Done\n";
    # Print summary of listing:
    if ($desdb->verbose() >=1) {
      print "\nNumber of files: $nfiles\n";
      #  printf ("Est. aggregate size: %4.2f %s.\n",NCSA::String::Util::make_human($Aggbytes)) if $Aggbytes; 
    }

    # if ($desdb->verbose() >=2) {
    #   printf ("Maximum file size: %4.2f %s.\n",NCSA::String::Util::make_human($Maxbytes)) if $Maxbytes;
    #   printf ("Minimum file size: %4.2f %s.\n",NCSA::String::Util::make_human($Minbytes)) if $Minbytes;
    #   printf ("Average file size: %4.2f %s.\n",NCSA::String::Util::make_human($Avgbytes)) if $Avgbytes;
    # }


  if($SiteInfo) {
    if ($desdb->verbose() >= 1) {
      if (! $Args->{'ShowSiteID'}) {
        print "\nDatabase records indicate files from this listing exist at the following sites:\n"
      }
    }
    # I would like to optimize the way sizes are calculated to avoid walking the array again.
    foreach my $SiteDataRef(values %$SiteInfo) {
      if ($Args->{'ShowSiteID'}) {
        next if ($SiteDataRef->{'location_id'} != $Args->{'ShowSiteID'});
        print "\nDatabase records from this listing for archive site: ", $SiteDataRef->{'location_name'},"\n"
      }
      # If we found some for this site, report it:
      if (exists $SiteDataRef->{'DBVerified'}) {
 
       my @keys = keys %{$SiteDataRef->{'DBVerified'}};
        my $Nfiles = $#keys;
        my ($SiteAggbytes, $SiteMaxbytes, $SiteMinbytes, $SiteAvgbytes, $Sitenfiles) = $desdb->getFileSizeInfo($Rows, $SiteDataRef->{'DBVerified'});
        if ($Nfiles >= -1) {
          if ($desdb->verbose() >= 1) {
            # printf ("%s: %s of %s  - %4.2f %s\n",$SiteDataRef->{'location_name'},$Nfiles+1, $#$Rows+1,NCSA::String::Util::make_human($SiteAggbytes));  
            printf ("%s: %s of %s\n", $SiteDataRef->{'location_name'}, $Nfiles+1, $#$Rows+1);
          }
        }
      }
    }

  }

  return $Rows;

}

#######################################################################
#
# Compute filesize size information from a Array of Location table hashes.
#
#######################################################################
sub getFileSizeInfo {
  my $desdb = shift;
  my $FileList = shift;

  my $IndexList;
  if (scalar @_ > 0) {
    $IndexList = shift;
  }

  my ($Aggbytes, $Maxbytes, $Minbytes, $Avgbytes, $nfiles, @sizes);

  $nfiles = scalar @$FileList;

  if ($IndexList) {
    my @List = keys %$IndexList;
    for (my $i=0; $i<=$#List; $i++) {
      my $FileKeys = $FileList->[$List[$i]];
      if ( $FileKeys->{'filesize'} ) {
        push(@sizes,$FileKeys->{'filesize'});
      }
      elsif ( $FileKeys->{'filesize_gz'} ) {
        push(@sizes,$FileKeys->{'filesize_gz'});
      }
      elsif ( $FileKeys->{'filesize_fz'} ) {
        push(@sizes,$FileKeys->{'filesize_fz'});
      }
    }
  }
  else {
    foreach my $FileKeys (@$FileList) {
     if ( $FileKeys->{'filesize'} ) {
        push(@sizes,$FileKeys->{'filesize'});
      }
      elsif ( $FileKeys->{'filesize_gz'} ) {
        push(@sizes,$FileKeys->{'filesize_gz'});
      }
      elsif ( $FileKeys->{'filesize_fz'}  ) {
        push(@sizes,$FileKeys->{'filesize_fz'});
      }
    }
  }
  $Aggbytes = List::Util::sum(@sizes);
  $Maxbytes = List::Util::max(@sizes);
  $Minbytes = List::Util::min(@sizes);

  if ($nfiles > 0) {
      $Avgbytes = $Aggbytes/$nfiles;
  }
  else {
    $Avgbytes = 0;
  }

  return ($Aggbytes, $Maxbytes, $Minbytes, $Avgbytes, $nfiles);

  
}

#######################################################################
#
#######################################################################
sub updateFileSizeForSite {
  my ($self, %Args) = @_;

  my $FileList = $Args{'ASiteFileList'} || bail->throw("Must provide a 'ASiteFileList'.\n");
  my $AllFilesARef = $Args{'AllFilesARef'} || bail->throw("Must provide 'AllFilesARef'.\n");
  my $ASiteID = $Args{'ASiteID'} || bail->throw("Must provide a archive site ID 'ASiteID.\n");

  my ($tuples, $sql, @tuple_status, @ids, @sizes);
  $sql = "UPDATE ".$self->LFileTable();

  # Need to make a list foreach flavor
  my %FlavorLists;
  foreach my $i (keys %{$Args{'ASiteFileList'}}) {
    my $str = substr ($AllFilesARef->[$i]->{'archivesites'}, $Args{'ASiteID'}-1, 1);
    foreach my $flavor (keys %{$self->FlavorData()}) {
      my $SizeField = $self->FlavorData()->{$flavor}->{'size_field'};
      if ($str eq $flavor) {
        my $size = $AllFilesARef->[$i]->{$SizeField};
        push(@{$FlavorLists{$flavor}->{'ids'}},$Args{'AllFilesARef'}->[$i]->{'id'});
        push(@{$FlavorLists{$flavor}->{'sizes'}},$Args{'ASiteFileList'}->{$i}->{'checked_size'});
      }
    }
  }

  print "\n";
  foreach my $flavor (keys %{$self->FlavorData()}) {
    next if (! $FlavorLists{$flavor});
    my $SizeField = $self->FlavorData()->{$flavor}->{'size_field'};
    my $nrows = scalar @{$FlavorLists{$flavor}->{'ids'}};

    my $this_sql = $sql . " set $SizeField=? WHERE id=?";

    print "Updating $nrows values for '$flavor' flavor files.\n";
    my $sth = $self->prepare($this_sql);

    $tuples = undef;
    $tuples = $sth->execute_array(
        { ArrayTupleStatus => \@tuple_status },
        $FlavorLists{$flavor}->{'sizes'},
        $FlavorLists{$flavor}->{'ids'}
    );
    if ($tuples) {
    $self->commit();
      print "Successfully updated $tuples records\n";
    }
  }

  return $tuples;
}

#######################################################################
#
#######################################################################
sub updateArchiveSiteStrings {
  my $self = shift;
  my $Args = shift;

  my $FileList = $Args->{'FileList'} if ($Args->{'FileList'});
  my $AllFilesARef = $Args->{'AllFilesARef'} if ($Args->{'AllFilesARef'});
  my $checkSiteID = $Args->{'checkSiteID'} if ($Args->{'checkSiteID'});
  my $updateSiteID = $Args->{'updateSiteID'} if ($Args->{'updateSiteID'});
  my $updateVal = $Args->{'updateVal'} if ($Args->{'updateVal'});
  my $updateFileSize = $Args->{'updateFileSize'} if ($Args->{'updateFileSize'});

  my ($SizeField);

  # Required Argments:
  bail->throw("Must provide a FileList.\n") if (! $FileList); 
  bail->throw("Must provide AllfileARef\n") if (! $AllFilesARef); 
  bail->throw("Must provide updateSiteID\n") if (! $updateSiteID); 

  # Should have EITHER updateVal OR checkSiteID, not both.
  if ($updateVal) {
    bail->throw ("Cannot use both a explicit updataVal AND a checkSiteID") if ($checkSiteID);
    # Check that a valid update flavor was provided:
    my $validflavor = 0;
    foreach my $flavor (keys %{$self->FlavorData()}) {
      if ($updateVal eq $flavor) {
        $validflavor = 1;
        $SizeField = $self->FlavorData()->{$flavor}->{'size_field'};
        last;
      }
    }
    bail->throw("Update flavor: $updateVal is not a valid flavor type.") if (!$validflavor);
    if ($updateVal ne 'N') {
      bail->throw("Update flavor: $updateVal is does not have a 'size_field' defined.") if (! $SizeField);
    }
  }
  if ($checkSiteID) {
    bail->throw ("Cannot use both a explicit updataVal AND a checkSiteID") if ($updateVal);
    bail->throw ("FileSize updates only for explicit falvor 'updateVal'.") if ($updateFileSize);
  }

  my $len = 30;   # Length of archivesites string hardwired!
  my $before = $updateSiteID - 1;
  my $after = $updateSiteID + 1;
  my $rest = $len - $updateSiteID;

  my ($tuples, $sql, @tuple_status, @ids, @sizes);

  $sql = "UPDATE ".$self->LFileTable();

  if ($updateVal) {
    $sql .= " set archivesites=substr(archivesites,1,$before)||'$updateVal'||substr(archivesites,$after,$rest)";
    if ($updateFileSize) {
        $sql .= ",$SizeField=?";
    }
    $sql .= " where id=?";
  }
  else {
    $sql .= " set archivesites=substr(archivesites,1,$before)||?||substr(archivesites,$after,$rest) where id=?";
  }

  my @keys = keys %$FileList;
  my $sth = $self->prepare($sql);

  # Put the same value in for all ids:
  if ($updateVal) {
    my @bound_sets = undef;
    if ($updateFileSize && $updateVal ne 'N') {
      foreach my $key (@keys) {
        push (@ids, $AllFilesARef->[$key]->{'id'});
        push (@sizes, $AllFilesARef->[$key]->{'checked_size'});
      }
      @bound_sets = (\@sizes,\@ids);
    }
    else {
      foreach my $key (@keys) {
        push (@ids, $AllFilesARef->[$key]->{'id'});
      }
      @bound_sets = (\@ids);
    }
    $tuples = $sth->execute_array(
        { ArrayTupleStatus => \@tuple_status },
        @bound_sets
    );
    foreach my $key (@keys) {
      $AllFilesARef->[$key]->{'archivesites'} = substr($AllFilesARef->[$key]->{'archivesites'},0,$before).$updateVal.substr($AllFilesARef->[$key]->{'archivesites'},$after-1,$rest);
      $AllFilesARef->[$key]->{$SizeField} = $AllFilesARef->[$key]->{'checked_size'} if ($AllFilesARef->[$key]->{'checked_size'});
    }
  }

  else {
    my @ar_vals;
    foreach my $key (@keys) {
      push (@ar_vals,substr $AllFilesARef->[$key]->{'archivesites'}, $checkSiteID-1, 1);
      push (@ids, $AllFilesARef->[$key]->{'id'});
    }

    $tuples = $sth->execute_array(
        { ArrayTupleStatus => \@tuple_status },
        \@ar_vals,
        \@ids
    );

    # Now update local data copy:
    foreach my $key (@keys) {
      my $val = substr $AllFilesARef->[$key]->{'archivesites'}, $checkSiteID-1, 1;
      $AllFilesARef->[$key]->{'archivesites'} = substr($AllFilesARef->[$key]->{'archivesites'},0,$before).$val.substr($AllFilesARef->[$key]->{'archivesites'},$after-1,$rest);
      
    }
  }

  if ($tuples) {
    $self->commit();
    print "Successfully updated $tuples records\n";

  }

  # Maybe should throw exception here:
  else {
      for my $tuple (0..@keys-1) {
          my $status = $tuple_status[$tuple];
          $status = [0, "Skipped"] unless defined $status;
          next unless ref $status;
#          printf "Failed to update (%s, %s): %s\n",
#              $asite_strings[$tuple], $fileids[$tuple], $status->[1];
          print "Rolling back all updates."
      }
      $self->rollback();
  }


}


#######################################################################
#
#######################################################################
sub insertArchiveSiteStrings {
  my $desdb = shift;
  my $Args = shift;

 # Get file id list and current archive string list.
  my @fileids;
  my @asite_strings; 
  my $dbi = <<STR;
UPDATE LOCATION SET ARCHIVESITES=? WHERE ID=?
STR
  my $SiteID = $Args->{'SiteData'}->{'location_id'};

  my $update_string;
  my $update_data;
  if ($Args->{'action'}) {
    if ($Args->{'action'} eq 'add') {
      $update_string = 'Y';
      $update_data = 'FSVerified';
    }
    elsif ($Args->{'action'} eq 'remove') {
      $update_string = 'N';
      $update_data = 'FSNotVerified';
    }
    else {
      croak ("insertArchiveSiteStrings:\nMust provide an action agument of either \'add\' or \'remove\'.\n");
    }
  }
  else {
    croak ("insertArchiveSiteStrings:\nMust provide an action agument of either \'add\' or \'remove\'.\n");
  }

  foreach my $fi (keys %{$Args->{'SiteData'}->{"$update_data"}}) {
    my $FileKeys = $Args->{'AllFiles'}->[$fi];
    #print"$FileKeys->{'id'}\n";
    if ($Args->{'action'} eq 'add') {
      if (exists $Args->{'SiteData'}->{'DBVerified'}->{"$fi"}) {
        next;
      }
    }
    push(@fileids, $FileKeys->{'id'});
    my $site_key = $FileKeys->{'archivesites'};
    my $offset = $SiteID-1;
    substr($site_key,$offset,1) = $update_string;
    push(@asite_strings, $site_key);
 }
  if ($#fileids == -1) {
    return;
  }
  #if ($desdb->verbose() >= 1) {
    if ($Args->{'action'} eq 'add') {
      print "\nAdding ArchiveSite location records for site: $Args->{'SiteData'}->{'location_name'}\n";
    }
    elsif ($Args->{'action'} eq 'remove') {
      print "\nRemove ArchiveSite location records for site: $Args->{'SiteData'}->{'location_name'}\n";
    }
  #}

  my $sth = $desdb->prepare($dbi);

  my $tuples = $sth->execute_array(
      { ArrayTupleStatus => \my @tuple_status },
      \@asite_strings,
      \@fileids,
  );
  if ($tuples) {
    $desdb->commit();
    print "Successfully updated $tuples records\n";
  }
  else {
      for my $tuple (0..@asite_strings-1) {
          my $status = $tuple_status[$tuple];
          $status = [0, "Skipped"] unless defined $status;
          next unless ref $status;
          printf "Failed to update (%s, %s): %s\n",
              $asite_strings[$tuple], $fileids[$tuple], $status->[1];
          print "Rolling back all updates."
      }
      $desdb->rollback();
  }

  

}

#######################################################################
#######################################################################
#######################################################################
#######################################################################

package DAF::DB::Util::st;
our @ISA = qw(DB::DESUtil::st);

=head1 NAME

DAF::DB::Util

=head1 SYNOPSIS

use DAF::DB::Util;
my $desdb = new DAF::DB::Util({\%attributes});

$desdb->getArchiveSiteInfo($site);
$desdb->isvalidArchiveSiteName($name);

=head1 Method Descriptions

=over 4

=item C<getArchiveSiteInfo>

  $info = $desdb->getArchiveSiteInfo($site);

Returns a reference to a hash of values returned by a query to the DES Archive database.  The values are placed
in a standard data structure (hash) that looks like:

  C<location_id> - 
  C<location_name> - 
  C<archive_root> - 
  C<gridftp_host> - 
  C<gridftp_port> - 
  C<site_name> - 

If the SQL needs to retrive this information fro mthe database should change, populating (or if needed amending) the data structure
above should insulate calling applications from DB schema changes.

=back
