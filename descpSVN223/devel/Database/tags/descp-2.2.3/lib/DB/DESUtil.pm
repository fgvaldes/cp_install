########################################################################
#  $Id: desDBI.pm 2559 2008-12-03 21:21:13Z dadams $
#
#  $Rev:: 2559                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-12-03 15:21:13 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package DB::DESUtil;
use strict;
use warnings;
use Exception::Class::DBI;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use base qw(DB::Util);

sub new {
  my ($this, %Args) = @_;
  my %ConnectArgs = (
    'attr' => {
      'AutoCommit' => 0,
      'RaiseError' => 0,
      'PrintError'  => 0,
      'HandleError' => Exception::Class::DBI->handler,
      'FetchHashKeyName' => 'NAME_lc',
    },
  );

  my $dbfile = $ENV{"DESDB_PARAMS"};
  if (!defined( $dbfile )) {
     $dbfile = $ENV{"HOME"}.'/.desdm';
  }
  #$ConnectArgs{'db_config_file'} = $dbfile if (-r $dbfile);

  if(-r $dbfile){
	$ConnectArgs{'db_config_file'} = $dbfile if (-r $dbfile);
  }else{
	print "\nSTATUS3BEG DESUTIL ISSUE: DBFILE is $dbfile STATUS3END";
	print "\nSTATUS3BEG DESUTIL ISSUE: ENVHOME is ".$ENV{'HOME'}." STATUS3END";
	my $time = `date`;
	print "\nSTATUS3BEG DESUTIL ISSUE: TIME is $time STATUS3END";
	my $ls_l = `ls -l $dbfile`;
	print "\nSTATUS3BEG DESUTIL ISSUE: ls -l for $dbfile is $ls_l STATUS3END";
	print "\nSTATUS3BEG DESUTIL ISSUE: DBFILE is $dbfile STATUS3END";
	my $dbfile_content = `grep -v 'PASS' $dbfile`;
	print "\nSTATUS3BEG DESUTIL ISSUE: DBFILE content is $dbfile STATUS3END";
  }

  # Override Default connect args with anything passed in:
  if (%Args) {
    while ((my $k, my $v) = each %Args) {
      if ($k =~ /DBIattr/i) {
        while ((my $k2, my $v2) = each %$v) {
          $ConnectArgs{'attr'}->{$k2} = $v2;
        }
      }
      else {
        $ConnectArgs{$k} = $v;
      }
    }
  }

  my $class = ref($this) || $this;
  my $self = $class->SUPER::new(%ConnectArgs);
  return $self;
}

package DB::DESUtil::db;
use Exception::Class::DBI;
use Data::Dumper;
use strict;
use warnings;
use base qw(DB::Util::db);

my %ClassData = ();

sub _init {
  my ($this, %Args) = @_;
  # Default set of file-related tables.  Arguments passed in to this classes' contructor
  # that coorespond to the keys of the following hash with "_NAME" appended will override the
  # default set of tables named here.
  my %Tables = ();
  $Tables{'LOCATION_TABLE'} = 'location';
  $Tables{'IMAGE_META_TABLE'} = 'image';
  $Tables{'EXPOSURE_META_TABLE'} = 'exposure';
  $Tables{'CATALOG_META_TABLE'} = 'catalog';
  $Tables{'COADD_META_TABLE'} = 'coadd';
  $Tables{'ZEROPOINT_TABLE'} = 'zeropoint';

  # Look for alternant tables names passed in:
  foreach my $key (keys %Tables) {
    my $name_key = join('_',$key,'NAME');
    $Tables{"$key"} = $Args{$name_key} if ($Args{$name_key});
  }

  $this->LOCATION_TABLE( {
    'table_name' => $Tables{'LOCATION_TABLE'}, 
    'cols' => $this->getColumnNames($Tables{'LOCATION_TABLE'}),
    'primary_key_cols' => $this->getPrimaryKey($Tables{'LOCATION_TABLE'})
  });

  $this->IMAGE_META_TABLE( {
    'table_name' => $Tables{'IMAGE_META_TABLE'}, 
    'cols' => $this->getColumnNames($Tables{'IMAGE_META_TABLE'}),
    'primary_key_cols' => $this->getPrimaryKey($Tables{'IMAGE_META_TABLE'})
  });

  $this->EXPOSURE_META_TABLE( {
    'table_name' => $Tables{'EXPOSURE_META_TABLE'}, 
    'cols' => $this->getColumnNames($Tables{'EXPOSURE_META_TABLE'}),
    'primary_key_cols' => $this->getPrimaryKey($Tables{'EXPOSURE_META_TABLE'})
  });

  $this->CATALOG_META_TABLE( {
    'table_name' => $Tables{'CATALOG_META_TABLE'}, 
    'cols' => $this->getColumnNames($Tables{'CATALOG_META_TABLE'}),
    'primary_key_cols' => $this->getPrimaryKey($Tables{'CATALOG_META_TABLE'})
  });

  $this->ZEROPOINT_TABLE( {
    'table_name' => $Tables{'ZEROPOINT_TABLE'},
    'cols' => $this->getColumnNames($Tables{'ZEROPOINT_TABLE'}),
    'primary_key_cols' => $this->getPrimaryKey($Tables{'ZEROPOINT_TABLE'})
  });

 if ($this->isOracle()) {
# commented out for postgres/CP
  $this->COADD_META_TABLE( {
    'table_name' => $Tables{'COADD_META_TABLE'}, 
    'cols' => $this->getColumnNames($Tables{'COADD_META_TABLE'}),
    'primary_key_cols' => $this->getPrimaryKey($Tables{'COADD_META_TABLE'})
  });
 
  }
#
}


# Accessor methods for important table data:
sub LOCATION_TABLE {
  my($self, $newvalue) = @_;
  $ClassData{'tables'}->{'LOCATION_TABLE'} = $newvalue if (@_ > 1);
  return $ClassData{'tables'}->{'LOCATION_TABLE'};
}

sub IMAGE_META_TABLE {
  my($self, $newvalue) = @_;
  $ClassData{'tables'}->{'IMAGE_META_TABLE'} = $newvalue if (@_ > 1);
  return $ClassData{'tables'}->{'IMAGE_META_TABLE'};
}

sub EXPOSURE_META_TABLE {
  my($self, $newvalue) = @_;
  $ClassData{'tables'}->{'EXPOSURE_META_TABLE'} = $newvalue if (@_ > 1);
  return $ClassData{'tables'}->{'EXPOSURE_META_TABLE'};
}

sub CATALOG_META_TABLE {
  my($self, $newvalue) = @_;
  $ClassData{'tables'}->{'CATALOG_META_TABLE'} = $newvalue if (@_ > 1);
  return $ClassData{'tables'}->{'CATALOG_META_TABLE'};
}

sub COADD_META_TABLE {
  my($self, $newvalue) = @_;
  $ClassData{'tables'}->{'COADD_META_TABLE'} = $newvalue if (@_ > 1);
  return $ClassData{'tables'}->{'COADD_META_TABLE'};
}

sub ZEROPOINT_TABLE {
  my($self, $newvalue) = @_;
  $ClassData{'tables'}->{'ZEROPOINT_TABLE'} = $newvalue if (@_ > 1);
  return $ClassData{'tables'}->{'ZEROPOINT_TABLE'};
}


sub mergeTmpTablePrc {
	my $dbh = shift;
	my $Args = shift;

	my ($sourceTable, $destTable, $sql);
	$sourceTable = $Args->{'source_table'} if($Args->{'source_table'});
	$destTable   = $Args->{'dest_table'}   if($Args->{'dest_table'});

	$sql = qq{
	BEGIN DES_ADMIN.pMergeObjects(sourceTable=>'$sourceTable',destTable=>'$destTable');END;
	};
  print "$sql\n";
  my $start = time;
  my $sth = $dbh->do($sql);
  print "\t$sth\n";
  print "\tTime ", time-$start, " seconds\n";
  
	$dbh->commit();
  print "\n";
}


################################################################################
################################################################################
sub mergeTmpTable {
  my $this = shift;
  my $Args = shift;

  my ($tmp_table, $obj_table, $partition_name, $drop, $tmp_table_index);
  $tmp_table_index = $Args->{'source_drop_index'} if ($Args->{'source_drop_index'});
  $tmp_table = $Args->{'source_table'} if ($Args->{'source_table'});
  $obj_table = $Args->{'target_table'} if ($Args->{'target_table'});
  $partition_name = $Args->{'partition_name'} if ($Args->{'partition_name'});
  $drop = $Args->{'drop'} if ($Args->{'drop'});

  my $PKey_name='K'.$tmp_table;

  my @dbq;

  if ($this->verbose() >=1) {
    print "\nmergeTmpTable: creating partition $partition_name and merging $tmp_table to $obj_table.\n";
  }

  if ($tmp_table_index) {
      # check if index exists in db
      my ( $count ) = $this->selectrow_array( "select count(*) from dba_indexes where index_name = '$tmp_table_index'" );
      if ($count > 0) {
          print "Found index on tmp table.   Dropping it.\n";
          push(@dbq, "DROP INDEX $tmp_table_index");
      }
      else {
          print "Did not find index on tmp table.\n";
      }
  }

#  push(@dbq,"alter table $tmp_table add constraint $PKey_name primary key (object_id) disable validate");

  push(@dbq,"alter table $obj_table add partition $partition_name values ('$tmp_table')");
  push(@dbq,"alter table des_admin.$obj_table exchange partition $partition_name with table $tmp_table including indexes without validation update global indexes");
  if ($drop) {
    push(@dbq,"DROP TABLE $tmp_table PURGE");
  }

  foreach my $dbq (@dbq) {
    print "$dbq\n";
    my $start = time;
    my $stat = $this->do($dbq);
    print "\t$stat\n";
    print "\tTime ", time-$start, " seconds\n";
  }
  $this->commit();
  print "\n";
}

################################################################################
## SUBROUTINE: queryDB
#################################################################################
sub queryDB {
    my $this = shift;
    my $Args = shift;

    if (exists($Args->{'table'}) && ($Args->{'table'} =~ /location/i)) {
        if (exists($Args->{'key_vals'}) && exists($Args->{'key_vals'}->{'detector'})) {
            if (exists($Args->{'key_vals'}->{'run'})) {
                print "Warning - desDBI::queryDB both detector and run exist in key_vals.\n";
                print "          detector is part of run in location table.\n";
                print "          Leaving run the same in key_vals\n";
                print "          deleting detector from key_vals\n"; 
                delete($Args->{'key_vals'}->{'detector'});
            }
            else {
                print "Warning - desDBI::queryDB detector exists in key_vals.\n";
                print "          detector is part of run in location table.\n";
                print "          creating run in key_vals with value '%_<detector>'\n";
                print "          deleting detector from key_vals\n";
                $Args->{'key_vals'}->{'run'} = "%_".$Args->{'key_vals'}->{'detector'};
                delete($Args->{'key_vals'}->{'detector'});
            }
        }
    }
    return $this->SUPER::queryDB($Args);
}

################################################################################
## SUBROUTINE: queryDB2
#################################################################################
sub queryDB2 {
    my ($this, %Args) = @_;

    # Modify table names here.  The table names that are part of this class
    # definition can be used in both the keys to the queryDB2 argument hash
    # (table names) and the "join" argument, and will be substituted with the
    # correct corresponding DB table name befor being passed to the queryDB2
    # in the parent class.
    my %NewArgs;
    foreach my $arg (keys %Args) {
      if (exists $ClassData{'tables'}->{$arg}) {
         my $key; 
         if ($ClassData{'tables'}->{$arg}->{'table_name'}) {
            $key = $ClassData{'tables'}->{$arg}->{'table_name'};
            $NewArgs{"$key"} = $Args{"$arg"};
          }
          if (exists $NewArgs{"$key"}->{'join'}) {
            foreach my $table_key (keys %{$ClassData{'tables'}}) {
              my $table_name = ClassData{'tables'}->{$table_key}->{'table_name'};
              $NewArgs{"$key"}->{'join'} =~ s/$table_key/$table_name/;
            }
          }   
      }
      else {
        $NewArgs{$arg} = $Args{$arg};
      }
    }

    foreach my $table (keys %NewArgs) {

        if ($table =~ /location/i) {

            if (exists($NewArgs{$table}->{'key_vals'}) && exists($NewArgs{$table}->{'key_vals'}->{'detector'})) {
                if (exists($NewArgs{$table}->{'key_vals'}->{'run'})) {
                    print "Warning - desDBI::queryDB both detector and run exist in key_vals.\n";
                    print "          detector is part of run in location table.\n";
                    print "          Leaving run the same in key_vals\n";
                    print "          deleting detector from key_vals\n";
                    delete($NewArgs{$table}->{'key_vals'}->{'detector'});
                }
                else {
                    print "Warning - desDBI::queryDB detector exists in key_vals.\n";
                    print "          detector is part of run in location table.\n";
                    print "          creating run in key_vals with value '%_<detector>'\n";
                    print "          deleting detector from key_vals\n";
                    $NewArgs{$table}->{'key_vals'}->{'run'} = "%_".$NewArgs{$table}->{'key_vals'}->{'detector'};
                    delete($NewArgs{$table}->{'key_vals'}->{'detector'});
                }
            }
            last;
        }
        else {
            next;
        }
    }

    return $this->SUPER::queryDB2(%NewArgs);
} 


################################################################################
# SUBROUTINE: getDBTableInfo
# 
# To make this generic actually look up the primary key from the DB 
# rather than hard coding what we know it to be
################################################################################
sub getDBTableInfo {
   my $desdb = shift;
   my $tablestr = shift;
   my $RowsRef = shift;
    
   my $pkey; 
   $tablestr =~ tr/A-Z/a-z/;
   if ($tablestr =~ m/site/) {
      $tablestr = "SITES";
      $pkey = 'SITE_NAME';
   }
   elsif ($tablestr =~ m/archive/) {
      $tablestr = "ARCHIVE_SITES"; 
      $pkey = 'LOCATION_NAME';
   }
   elsif ($tablestr =~ m/software/) {
      #$tablestr = "SOFTWARE_LOCATIONS"; 
      #$pkey = 'LOCATION_NAME';
      $pkey = '';
      $tablestr = undef;
   }
   else {
      # calling program expected to figure out if this is a problem or not
      $pkey = '';
      $tablestr = undef;
   }

   if ($tablestr) {
     my $dbq = <<STR;
  SELECT * FROM $tablestr
STR

    @$RowsRef = @{$desdb->selectall_arrayref($dbq,{Slice => {}})};
  }

  return lc($pkey);
}


package DB::DESUtil::st;
our @ISA = qw(DB::Util::st);

1;

