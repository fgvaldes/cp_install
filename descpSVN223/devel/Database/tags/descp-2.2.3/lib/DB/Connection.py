#!/usr/bin/env /usr/local/bin/python2.5

# This script hardcodes the path to the version of python to use in
# order to ensure that the modules cx_Oracle and sqlparse are available.
# Until these are included in the eups distribution of the DES prereqs,
# this line will have to be adjusted to local conditions.

import sys
import cx_Oracle
import sqlparse

import readline
import atexit
import os
import cmd
import string

def do_query(query, show=1):
	cur = con.cursor()
	try:
		if show: print query
		cur.execute(query)
		return cur
	except Exception, e:
		print e
		return None

def query_and_print(query):
	cur = do_query(query)
	if cur:
		try:
			for c in cur:
				print c
		except KeyboardInterrupt:
			pass
	else: pass

def query_single_result(query):
	cur = do_query(query)
	if cur:
		try:
			for c in cur:
				return c[0]
		except KeyboardInterrupt:
			pass
	else: pass

def complete_via_query(text, query):
	"""
	This function queries and then generates a list
	useable by the redline completer function.
	It's important to always return a list.
	"""
	try:
		query = query % (text)
		opts = do_query(query, 0)
		retlist = []
		for o in opts: retlist.append(o[0])
		if len(realist) == 1: return [ retlist[0] + " " ]
			# must be it -- blank pad to move on
		return realist
	except Exception, e:
		print e
		return []
	return []

def parse_db_config_file(config_file):
	"""
	Parse the file named as an argument and return a connection for the user named,
	with the password named, to the database named. The file should consist of lines
	of the form
	  [optional whitespace]<key><whitespace><value>[optional whitespace]"
	key and value must not contain any whitespace. Blank lines and lines beginning
	with a hash (#), or whitespace followed by a hash, are ignored. If a line has 
	any other form, or if key is not in a list of allowed keys, a warning will be
	returned.
	"""

	dbconfig = {}
	try:
		lines = open(config_file).readlines()
	except IOError:
		print "Cannot read DB configuration file", config_file
		pass
	else:
		keylist = ["DB_USER", "DB_PASSWD", "DB_SERVER", "DB_NAME", 
				   "DB_SID", "DB_SERVER_STANDBY", "DB_NAME_STANDBY" ]
		for l in lines:
			parts = l.split(None,2)
			if len(parts) == 0 or parts[0][0] == '#':  #blank or comment line
				continue
			elif len(parts) != 2:
				print "Ignoring improper line:", line
				continue
			elif not parts[0] in keylist:
				print "Ignoring improper key:", parts[0]
			else:
				dbconfig[parts[0]] = parts[1]

	conn_str = dbconfig["DB_USER"] + "/" + dbconfig["DB_PASSWD"] + \
			   "@" + dbconfig["DB_SERVER"] + "/" + dbconfig["DB_NAME"]
	print conn_str
	con = cx_Oracle.connect(conn_str)
	return con

class orc(cmd.Cmd):
	"""
	Class orc (Oracle client) inherits from class cmd.Cmd, which
	provides functionality related to command completion and
	command entry in a perpetual loop.  The work is done in the
	end by calls to the external function do_query, which essentially
	takes an SQL query and returns con.cursor.execute(query). con
	is defined (in external code) to be a connection to an Oracle
	database, cx_Oracle.connect('...'). The scope of the query can be
	restricted to certain tables by defining a "table clause" to be
	appended to the queries.
	"""
	intro = 'Welcome to the oracle shell. Type help or ? to list commands.\n'
	prompt = '(oracle) '
	file = None
	def __init__(self):
		cmd.Cmd.__init__(self)
		self.tableclause = " "
		self.query = ""
		self.last_result = []

	def default(self, query):
		"anything else is thought to be SQL"
		query_and_print(query)

	#
	# Queries with tab completion for table names and column names
	# -- for dummies  :)
	#

	def sql_completer(self, text, line, begidx, endidx):
		"""
		Return a list of completions for the columns of the
		tables and the table names passed in by setqscope.
		"""

		and_owner_not_in = "AND OWNER NOT IN ('SYS','HR'," +\
			"'SYSTEM','SYSMAN','DMSYS','ORDSYS'," +\
			"'WKSYS','EXFSYS','MDSYS','CTXSYS'," +\
			"'OLAPSYS','DBSNMP','TSMSYS','WMSYS')"
		query="SELECT DISTINCT COLUMN_NAME " +\
			"FROM ALL_TAB_COLUMNS" +\
			"WHERE COLUMN_NAME LIKE '%s%%' " +\
			and_owner_not_in +\
			self.tableclause +\
			"ORDER BY COLUMN_NAME"
		columns = complete_via_query(text, query)
		query="SELECT TABLE_NAME " +\
			"FROM ALL_TABLES" +\
			"WHERE TABLE_NAME LIKE '%s%%' " +\
			and_owner_not_in +\
			self.tableclause +\
			"ORDER BY TABLE_NAME"
		tables = complete_via_query(text, query)
		return columns + tables

	def do_setqscope(self, tables):
		"""
		Enable restriction of the scope of queries by defining
		a "table clause" based on the given list of tables.
		"""
		if tables:
			tabletext = string.join(string.split(tables),"','")
			self.tableclause = "AND TABLE_NAME IN ('%s') " %\
				(tabletext)
		else: self.tableclause = " "

	def complete_setqscope(self, text, line, begidx, lastidx):
		"""
		Generate a list of table names for completion.
		"""
		return complete_via_query(text, \
			"select TABLE_NAME from ALL_TABLES " +\
			"where TABLE_NAME like '%s%%'")

	def do_showqscope(self, arg):
		"""
		Show the table name used for the tab completion of
		column names." 
		"""
		print "Completions of columns are now from table(s) '%s'" \
			% self.tableclause

	def do_q(self, query):
		"""
		Do a query with completion of table and column names.
		Completion ecludes system names.
		"""
		query_and_print(query)

	def complete_q( self, text, line, begidx, lastidx ):
		try:
			if line in "q select": return ["select"]
			return self.sql_completer( \
				text, line, begidx, lastidx )
		except Exception, e:
			print e

	def do_tables( self, arg ):
		"""
		Lists tables matching an oracle pattern.
		Case sensitive, e.g. %LOCA%.
		"""
		query = "SELECT TABLE_NAME FROM ALL_TABLES WHERE " +\
			"TABLE_NAME LIKE '%s' ORDER BY TABLE_NAME" % arg
		query_and_print(query)

	def do_columns( self, arg ):
		"""
		Lists tables having a column matching an oracle pattern.
		Case sensitive, e.g. %SITE%.
		"""
		query = "SELECT TABLE_NAME, COLUMN_NAME FROM " +\
			"ALL_TAB_COLUMNS WHERE COLUMN_NAME LIKE '%s'" % arg
		query_and_print(query)

	def do_queries( self, arg ):
		"Show queries retained in the system."
		query = "SELECT SQL_TEXT FROM V$SQLAREA WHERE " +\
			"SQL_TEXT LIKE '%s%%'" % arg
		query_and_print(query)

	def do_describe_table( self, arg ):
		"""
		Lists tables having a column matching an Oracle table name.
		Case sensitive, e.g. LOCATION
		"""
		query = "SELECT COLUMN_NAME, NULLABLE, DATA_TYPE FROM " +\
			"ALL_TAB_COLUMNS WHERE TABLE_NAME = '%s'" % arg
		query_and_print(query)

	def complete_describe_table( self, arg ):
		return complete_via_query( text, \
			"SELECT TABLE_NAME FROM ALL_TABLES WHERE " +\
			"TABLE_NAME LIKE '%s%%'")

	def do_count_files_only( self, arg ):
		"""
		Count the lies on an ARCHIVE_SITE. Takes one parameter,
		e.g. abe (with tab completion). Extra input tokens are
		taken to be clauses for the end of the query.
		E.g. do_count_files abe AND FILECLASS = 'DES'
		"""
		if not arg:
			print "Supply the LOCATION_NAME, e.g. abe."
			return

		delim = string.find(arg," ") # Not -1 if contains a blank
		if delim == -1:
			rest = ""
		else:
			arg = arg[:delim]
			rest = arg[delim:]

		query = "SELECT LOCATION_ID FROM ARCHIVE_SITES WHERE " +\
			"LOCATION_NAME = '%s'" % (arg)
		site_id = query_single_result(query)
		if not site_id:
			print "Supply the LOCATION_NAME, e.g. abe."
			return
		site_id = int(site_id)

		site_Y_mask = ''
		site_F_mask = ''
		site_G_mask = ''
		for i in range(1,31):
			if i == site_id:
				site_Y_mask = site_Y_mask + "Y"
				site_F_mask = site_Y_mask + "F"
				site_G_mask = site_Y_mask + "G"
			else:
				site_Y_mask = site_Y_mask + "N"
				site_F_mask = site_F_mask + "N"
				site_G_mask = site_G_mask + "N"

		query = "SELECT COUNT(*) FROM LOCATION WHERE ( " +\
			"ARCHIVESITES = '%s' OR " +\
			"ARCHIVESITES = '%s' OR " +\
			"ARCHIVESITES = '%s' ) %s " \
			% (site_Y_mask, site_F_mask, site_G_mask, rest)
		query_and_print(query)

	def complete_count_files_only( self, text, line, begidx, lastidx ):
		return complete_via_query( text, \
			"SELECT LOCATION_NAME FROM ARCHIVE_SITES " +\
			"WHERE LOCATION_NAME LIKE '%s%%'")

	def do_site_archive_report( self, arg ):
		" Report out on the archive state of the survey."
		#query = "SELECT SITE_ID FROM ARCHIVE_SITES"
		#cur = do_query(query)
		print "unimplemented"
		return
		query = "SELECT ARCHIVESTIES FROM LOCATION"
		cur = do_query(query)
		for c in cur:
			i = 1
			for char in c[0]:
				if char == 'N': pass
				elif char == 'Y': i = i + 1
				else: print char

	def do_EOF( self, arg ):
		"Exit program on ^D."
		print ""
		sys.exit(0)

	def do_shel( self, arg ):
		"Pass the command line to a shell."
		import subprocess
		subprocess.call( arg, shell=True )

#
# Read in history file, if present and arrange for history to be
# written out at exit.
#
history_file = os.path.join( os.environ["HOME"], ".oracle_monitor_hostory")
try:
	readline.read_history_file(history_file)
except Exception:
	pass
atexit.register(readline.write_history_file, history_file)

#
# Connect to Oracle and make the command interpreter
#

config_file = os.path.join(os.environ["HOME"], ".desdm")
con = parse_db_config_file(config_file)
cmdinterp = orc()

#
# Read an initfile. If present, execute it.
#

init_file = os.path.join(os.environ["HOME"], ".oracle_monitor_init")
try:
	initial_lines = open(init_file).readlines()
except IOError:
	pass # no init file
else:
	for l in initial_lines:
		cmdinterp.onecmd(l)

while 1:
	try:
		cmdinterp.cmdloop()
	except (EOFError):
		sys.exit(0)
	except (KeyboardInterrupt):
		print "^D exits the program, ^C just interrupts queries"
