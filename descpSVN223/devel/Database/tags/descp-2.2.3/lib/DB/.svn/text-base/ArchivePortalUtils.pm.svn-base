#
# ArchivePortalUtils.pm
#
# DESCRIPTION:
#
# This module contains methods for general DB interaction used by
# all ingestion codes.
#
# AUTHOR:  Tony Darnell (tdarnell@uiuc.edu)
#
# $Rev$
# $LastChangedBy$
# $LastChangedDate$
#

package DB::ArchivePortalUtils;

use strict;
use Benchmark;
use Astro::FITS::CFITSIO qw( :constants );
use Astro::FITS::CFITSIO qw( :longnames );
use Data::Dumper;
use Exception::Class::DBI;
use FileHandle;
use DBD::Oracle qw(:ora_types);

require Exporter;
our @ISA = qw(Exporter);

our @EXPORT = qw{
  makeFitsBinaryTable
  makeFitsBinaryTableFromSth
  makeVOTable
  makeVOTableFromSth
};

sub makeVOTable{

  my ($allLinesArrRef, $tableInfoHashRef, $outFile) = @_;

  my @all_lines = @$allLinesArrRef;
  my $colNames = shift(@all_lines);
  chomp ($colNames);
  my @columns = split /\|/,$colNames;

  $outFile .= '.vot' if ($outFile !~ m/vot$/);

  my $VOTableDataTypes;
  $VOTableDataTypes->{'BINARY_FLOAT'} = "float";
  $VOTableDataTypes->{'BINARY_DOUBLE'} = "double";
  $VOTableDataTypes->{'VARCHAR2'} = 'char';
  $VOTableDataTypes->{'CHAR'} = 'char';

  my $VOTableHeader = qq{
<?xml version="1.0"?>
<VOTABLE version="1.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:noNamespaceSchemaLocation="http://www.ivoa.net/xml/VOTable/v1.2" 
 xmlns:stc="http://www.ivoa.net/xml/STC/v1.30" >
  <RESOURCE name="DESArchive">
    <TABLE name="ArchivePortalResults">
    <DESCRIPTION>Des Archive Portal Results</DESCRIPTION>
};
  
#
# Make the fields section
#
  my $VOTableFields=q{};
  my $i = 1;
  foreach my $column (@columns){

    my $width;
    my $precision;
    my $dataType;
    my $type = $tableInfoHashRef->{$column}->{'type'};

    if ($type eq 'NUMBER'){

       $width = $tableInfoHashRef->{$column}->{'precision'};
       $precision = $tableInfoHashRef->{$column}->{'scale'};

       if ($precision){
         $dataType = 'float';
       } else {
        $dataType =  'long';
       }
       $VOTableFields .= qq{
      <FIELD name="$column" ID="col$i" datatype="$dataType" unit=""
             width="$width" precision="$precision" /> };

    } else {

      $dataType = $VOTableDataTypes->{$type};
    $VOTableFields .= qq{
      <FIELD name="$column" ID="col$i" datatype="$dataType" unit="" /> };
    }

    $i++;
  }

#
# Make the tabledata section
#
  my $VOTableData = qq{
    <DATA>
      <TABLEDATA>};

  foreach my $dataRow (@all_lines){

    chomp($dataRow);
    my @row = split(/\|/,$dataRow);
    my @tableRow = map{"<TD>$_</TD>"} @row;
    my $tableRow = join("",@tableRow);

    $VOTableData .= qq{
        <TR>
          $tableRow
        </TR>};

  }

  my $VOTableFooter = qq{
       </TABLEDATA>
     </DATA>
  </TABLE>
</RESOURCE>
</VOTABLE>
};

  my $VOTable = $VOTableHeader . $VOTableFields . $VOTableData . $VOTableFooter;

  open my $VOTABLE, ">$outFile";
  print $VOTABLE $VOTable;
  close($VOTABLE);

  return;

}

sub makeVOTableFromSth{

  my ($sth,$tableInfoHashRef,$maxRows,$outFile) = @_;

  my $colsArrRef = $sth->{NAME};
  my @columns = @$colsArrRef;

  my $numCols = $sth->{NUM_OF_FIELDS};

  $outFile .= '.vot' if ($outFile !~ m/vot$/);

  open (my $VOTABLE, ">$outFile");
  $VOTABLE->autoflush(1);

  my $VOTableHeader = qq{
<?xml version="1.0"?>
<VOTABLE version="1.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:noNamespaceSchemaLocation="http://www.ivoa.net/xml/VOTable/v1.2" 
 xmlns:stc="http://www.ivoa.net/xml/STC/v1.30" >
  <RESOURCE name="DESArchive">
    <TABLE name="ArchivePortalResults">
    <DESCRIPTION>Des Archive Portal Results</DESCRIPTION>
};

  print $VOTABLE $VOTableHeader;
  
#
# Make the fields section
#
  my $VOTableFields=q{};
  my $i = 0;
  #for ( my $i = 0; $i <= $numCols; $i++){
  foreach my $column (@columns){

    #my $column = $sth->{NAME}->[$i];
    my $dataType;
    my $type = $sth->{TYPE}->[$i];

    if ( $type != 1 || 
         $type != 9 || 
         $type != 10 || 
         $type != 11 || 
         $type != 12
       ){

       my $width = $sth->{PRECISION}->[$i];
       my $precision = $sth->{SCALE}->[$i];

       if ($type == 3 || $type == 6 ){
         $dataType = 'float';
       } elsif ($type == 8) {
         $dataType = 'double';
       } else {
        $dataType =  'long';
       }
       $VOTableFields .= qq{
      <FIELD name="$column" ID="col$i" datatype="$dataType" unit=""
             width="$width" precision="$precision" /> };

    } elsif ($type == 1) {
        $dataType =  'char';
       $VOTableFields .= qq{
      <FIELD name="$column" ID="col$i" datatype="$dataType" unit="" /> };
    }

    $i++;
  }

  print $VOTABLE $VOTableFields;

#
# Make the tabledata section
#
  my $VOTableData = qq{
    <DATA>
      <TABLEDATA>
  };

  print $VOTABLE $VOTableData;


  my $rows = [];
  my $fRow = 0;
  while ( my $row = $sth->fetchrow_arrayref() ){

    my @tableRow = map{defined($_) ? "<TD>$_</TD>\n" : "<TD>0</TD>\n"} @$row;
    my $tableRow = join("",@tableRow);

    $VOTableData = qq{
        <TR>
          $tableRow
        </TR>
    };

    print $VOTABLE $VOTableData;

  }

  my $VOTableFooter = qq{
       </TABLEDATA>
     </DATA>
  </TABLE>
</RESOURCE>
</VOTABLE>
};

  print $VOTABLE $VOTableFooter;
  close($VOTABLE);

  return;

}

#
# $allLinesArrRef is an array ref with the first line the list of columns
# delimited by | and each element thereafter is a | delimited line of the 
# values
#
sub makeFitsBinaryTable{

  my ($allLinesArrRef, $tableName, $tableInfoHashRef, $outFile) = @_;

  my @all_lines = @$allLinesArrRef;

  my $status=0;
  my $colNames = shift(@all_lines);
  chomp ($colNames);
  my @columns = split /\|/,$colNames;

  my $numCols = scalar(@columns);
  my $numRows = scalar(@all_lines);

  $outFile .= '.fits' if ($outFile !~ m/fits$/);

#
# These are fits standard datatypes
#
  my $char   = '1A1';
 #my $string = '1PA';
  my $int32  = '1J';
  my $float  = '1E';
  my $double = '1D';
  my $string = '';

  if ($tableName eq 'DC4_TRUTH'){
    $string = '60A';
  } else {
    $string = '25A';
  }

  my $tTypes;
  $tTypes->{$char}   = TSTRING;
  $tTypes->{$string} = TSTRING;
  $tTypes->{$int32}  = TLONG;
  $tTypes->{$float}  = TFLOAT;
  $tTypes->{$double} = TDOUBLE;

  my $tForms;
  foreach my $key (keys %$tableInfoHashRef){

    my $tableType = $tableInfoHashRef->{$key}->{'type'};
    my $tableScale = $tableInfoHashRef->{$key}->{'scale'};
    $tableScale = 0 if not defined $tableScale;

    if ($tableType eq 'VARCHAR2'){
      $tForms->{$key}  = $string; 
    } elsif ($tableType eq 'CHAR') {
      $tForms->{$key}  = $char; 
    } elsif ($tableType eq 'NUMBER') {
      if ($tableScale > 0){
        $tForms->{$key}  = $double;
      } else {
        $tForms->{$key}  = $int32;
      }
    } elsif ($tableType eq 'BINARY_FLOAT') {
      $tForms->{$key}  = $float;
    } elsif ($tableType eq 'BINARY_DOUBLE') {
      $tForms->{$key}  = $double;
    }

  }

  my @tForms = ();
  my @tUnits = ();
  my $ind = 0;
  foreach my $col (@columns){
    $col =~ s/\s+$//; #strip whitespace
      $tForms[$ind] = $tForms->{$col};
    $tUnits[$ind] = q{};
    $ind++;
  }

  my $fptr = Astro::FITS::CFITSIO::create_file("!$outFile",$status);
  print "status: $status\n";
  $fptr->create_tbl(BINARY_TBL,$numRows,$numCols,\@columns,
      \@tForms,\@tUnits,'DESARCHIVERESULTS',$status);

#
# Build the columns to be inserted
#
  my $tableHashRef;
  foreach my $line (@all_lines){
    chomp($line);
    my @elems = split /\|/,$line;
    my $ind = 0;
    foreach my $colName (@columns){
      my $value = $elems[$ind];
      $value =~ s/\s+//;
      my $type = $tableInfoHashRef->{$colName}->{'type'};
      if (not $value){
        if ($type eq 'VARCHAR2'){
          $value = q{};
        } else {
          $value = 0.0;
        }
      }
      push @{$tableHashRef->{$colName}},$value;
      $ind++;
    }
  }

#
# Insert the columns into the binary table
#
  my $colNum = 1;
  foreach my $colName (@columns){

    $status = 0;
    my $tForm = $tForms->{$colName};
    my $dataType = $tTypes->{$tForm};
    my $colData = $tableHashRef->{$colName};

    Astro::FITS::CFITSIO::fits_write_col($fptr,$dataType,$colNum,1,1,
        $numRows,$colData,$status);
    $colNum++;
  }

  $fptr->close_file($status);

  return;

}

sub makeFitsBinaryTableFromSth{

  my ($sth,$tableName,$tableInfoHashRef,$outFile) = @_;

  my $status=0;
  my $colsArrRef = $sth->{NAME};
  my @columns = @$colsArrRef;

  my $numCols = scalar(@columns);

  $outFile .= '.fits' if ($outFile !~ m/fits$/);

#
# These are fits standard datatypes
#
  my $char   = '1A1';
 #my $string = '1PA';
  my $int32  = '1J';
  my $float  = '1E';
  my $double = '1D';
  my $string = '';

  if ($tableName eq 'DC4_TRUTH'){
    $string = '60A';
  } else {
    $string = '25A';
  }


  my $tTypes;
  $tTypes->{$char}   = TSTRING;
  $tTypes->{$string} = TSTRING;
  $tTypes->{$int32}  = TLONG;
  $tTypes->{$float}  = TFLOAT;
  $tTypes->{$double} = TDOUBLE;

  my $tForms;
  foreach my $key (keys %$tableInfoHashRef){

    my $tableType = $tableInfoHashRef->{$key}->{'type'};
    my $tableScale = $tableInfoHashRef->{$key}->{'scale'};
    $tableScale = 0 if not defined $tableScale;

    if ($tableType eq 'VARCHAR2'){
      $tForms->{$key}  = $string; 
    } elsif ($tableType eq 'DATE') {
      $tForms->{$key}  = $char; 
    } elsif ($tableType eq 'CHAR') {
      $tForms->{$key}  = $char; 
    } elsif ($tableType eq 'NUMBER') {
      if ($tableScale > 0){
        $tForms->{$key}  = $double;
      } else {
        $tForms->{$key}  = $int32;
      }
    } elsif ($tableType eq 'BINARY_FLOAT') {
      $tForms->{$key}  = $float;
    } elsif ($tableType eq 'BINARY_DOUBLE') {
      $tForms->{$key}  = $double;
    }

  }

  my @tForms = ();
  my @tUnits = ();
  my $ind = 0;
  foreach my $col (@columns){
    $col =~ s/\s+$//; #strip whitespace
    $tForms[$ind] = $tForms->{$col};
    $tUnits[$ind] = q{};
    $ind++;
  }


  my $fptr = Astro::FITS::CFITSIO::create_file("!$outFile",$status);
  $fptr->create_tbl(BINARY_TBL,0,$numCols,\@columns,
      \@tForms,\@tUnits,'DESARCHIVERESULTS',$status);

#
# Build the columns to be inserted
#

  my $rows = [];
  my $fRow = 1;
  while ( my $row = $sth->fetchrow_hashref() ){

    my $colNum = 1;

    fits_insert_rows($fptr,$fRow,1,$status);

    foreach my $colName ( @columns ){

      $status = 0;
      my $tForm = $tForms->{uc($colName)};
      my $dataType = $tTypes->{$tForm};
      $row->{lc($colName)} = 0 if not defined $row->{lc($colName)};

      Astro::FITS::CFITSIO::fits_write_col($fptr,$dataType,$colNum,$fRow,1,1,
         $row->{lc($colName)},$status);

      $colNum++;
    }

    $fRow++; 

  }

  $fptr->close_file($status);

  return;

}

1;
