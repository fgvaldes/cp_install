########################################################################
#  $Id: Tools.pm 1127 2008-06-09 18:51:08Z dadams $
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Authors: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################

package DAF::Transfer;
use strict;
use warnings;
use Carp;
use Data::Dumper;
use List::Util;
use POSIX;
use FindBin;
use Exception::Class('bail');
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use base qw(NCSA::Transfer);
use DAF::Archive::Tools;
use DAF::Exception;
$SIG{INT} = \&DAF_catch_INT;

my %ClassData;

sub new {
  my $this = shift;
  my $argsRef = shift;
  my $class = ref($this) || $this;
  my $self = {};
  bless $self, $class;
  $self  = $class->SUPER::new($argsRef);
  $self->verbose($argsRef->{'verbose'}) if ($argsRef->{'verbose'});
  $self->LogDir($argsRef->{'LogDir'}) if ($argsRef->{'LogDir'});
  $self->try($argsRef->{'try'}) if ($argsRef->{'try'});
  $self->debug($argsRef->{'debug'}) if ($argsRef->{'debug'});
  return $self;
}

#################################################################################
# SUBROUTINE: verifyArchiveFiles
#
# DESCRIPTION:
#   Verify a list of files at a particular Archive site.
#################################################################################
sub verifyFileList {
  my $this = shift;
  my $Args = shift;

  my ($SiteDataRef, $AllFilesARef, $FileCheckList, @ArchiveLocations, $nfiles, $NF);
  my ($e, $verbose);

  $verbose = $this->verbose();
  $verbose = $Args->{'verbose'} if (defined $Args->{'verbose'});
  my $throw = 1;

  if ($Args->{'SiteData'}) {
    $SiteDataRef = $Args->{'SiteData'};
  }
  else {
    bail->throw("verifyArchiveFiles: Must provide Site data hash ref.");
  }
  if ($Args->{'AllFiles'}) {
    $AllFilesARef = $Args->{'AllFiles'};
  }
  else {
  bail->throw ("verifyArchiveFiles: Must provide The \"All files\" Array ref.");
  }

  # Do I really have site data?
  my ($archive_root,$SiteID,$files_to_check);
  if  ($SiteDataRef->{'archive_root'}) {
    $archive_root = $SiteDataRef->{'archive_root'};
  }
  else {
    bail->throw("No archive_root provided in site data.");
  }
  if ($SiteDataRef->{'location_id'}) {
    $SiteID = $SiteDataRef->{'location_id'};
  }
  else {
    bail->throw("No location_id provided in site data.");
  }
  if ($SiteDataRef->{'files_to_check'}) {
    $files_to_check = $SiteDataRef->{'files_to_check'};
  }
  if ($Args->{'FileList'}) {
    $files_to_check = $Args->{'FileList'};
  }
#  else {
    bail->throw("No 'files_to_check' hashref provided in site data.") if (! $files_to_check);
#  }
  $nfiles = scalar keys %$files_to_check;
  $NF = $nfiles -1;
  
  if ($Args->{'NoThrow'}) {
    $throw = 0;
  }

  # Grab file locations and build the file list.  Work from the list
  # of files that are said to exist at this site. The SiteData hash
  # should have this list attached as "files_to_check", which is a 
  # list of array indicies in $AllFilesARef.  The result is a local hash
  # with each key being an Absolote path/filename to verify.

  my $checkSiteID = $SiteID;
  if ($Args->{'checkSiteID'}) {
    $checkSiteID = $Args->{'checkSiteID'};
  }
  my %buildFileListForSiteArgs = (
    'FileList' => $files_to_check,
    'AllFiles' => $AllFilesARef,
    'SiteID' => $checkSiteID,
    'ArchiveRoot' => $archive_root, 
    'Return' => 'HASH'
  );

  if ($Args->{'flavor'}) {
    $buildFileListForSiteArgs{'flavor'} = $Args->{'flavor'};
  }

  $FileCheckList = $this->buildFileListForSite(\%buildFileListForSiteArgs);

#  print "Sleeping in DAF::Transfer::verifyFileList after buiding site list...";
#  sleep 15;


  # Build "verifyRemoteFiles" arguments, depending on site:
  my $VerifyMethod;
  if ($SiteDataRef->{'site_name'} eq 'mss' || $SiteDataRef->{'site_name'} eq 'fnal' || $SiteDataRef->{'site_id'} == 18) {
    $VerifyMethod = 'LIST';
  }
  else {
    $VerifyMethod = 'MLST';
  }
  # Verify:
  print "\nVerifying file list on the file system at site: $SiteDataRef->{'location_name'}... \n" if ($verbose >= 1);

  my %verifyArgs = (
    'SiteData' => $SiteDataRef,
    'FileList' => $FileCheckList,
    'list_method' => $VerifyMethod
  );

  #print "\n",Dumper(\%verifyArgs),"\n";

  #$verifyArgs{'LogPrefix'} = $this->LogDir() if ($this->LogDir());
  #$verifyArgs{'verbose'} = $this->verbose() if ($this->verbose());
  $verifyArgs{'BaseFileName'} = $Args->{'BaseFileName'} if $Args->{'BaseFileName'};

  # Call Generic verify routine:
  $this->SUPER::verifyFileList( \%verifyArgs ); 

  # Check results and place verification info into the SiteData hash.  
  # Throw an exception if any files were not verified for any reason, 
  # or the verification code itself fails for any other reason.
  # Clean up old lists:
  delete $SiteDataRef->{'FSVerified'} if (exists $SiteDataRef->{'FSVerified'});
  delete $SiteDataRef->{'FSNotVerified'} if (exists $SiteDataRef->{'FSNotVerified'});
  delete $SiteDataRef->{'FSNotThere'} if (exists $SiteDataRef->{'FSNotThere'});
  delete $SiteDataRef->{'FSBadSize'} if (exists $SiteDataRef->{'FSBadSize'});
  delete $SiteDataRef->{'FSBadPerms'} if (exists $SiteDataRef->{'FSBadPerms'});

  my %failed_files;

  # Put resutls of verification back into site data:

  # ** Need to not use the "$val" referece here so perl
  # can garbage collect the filechecklist stuff better. ** --not sure I can do that...

  my $avg = 0.0;
  my $tott = 0.0;
  my $countt = 0;

  while ((my $key, my $val) = each %$FileCheckList) {
    my $i = $val->{'RefIndex'};
    if (exists $val->{'checked_size'}) {
      if ($val->{'size'}  == $val->{'checked_size'}) {
        $tott += $val->{'checked_size'};
        $countt++;
        $SiteDataRef->{'FSVerified'}->{$i} = $val;
      }
      else {
        $val->{'errstr'} = "File size does not match DB value.";
        $SiteDataRef->{'FSNotVerified'}->{$i} = $val;
        $SiteDataRef->{'FSBadSize'}->{$i} = $val;
        $failed_files{"$key"} = $val;
      }
    }
    else {
      $SiteDataRef->{'FSNotVerified'}->{$i} = $val;
      $failed_files{"$key"} = $val;
      #print Dumper ($val),"\n";
      if ($val->{'errstr'}) {
        if ($val->{'errstr'} eq 'No such file or directory') {
          $SiteDataRef->{'FSNotThere'}->{$i} = $val;
        }
        elsif ($val->{'errstr'} eq 'Permission denied') {
          $SiteDataRef->{'FSBadPerms'}->{$i} = $val;
        }
      }
    }
  }  

  # $avg = $tott/$countt;
  # my $megab = substr ( $avg/1000000.0 , 0, 8);
  my $gigab = substr ( $tott/1000000000.0, 0, 10);
  print "\nTotal number of checked files on $SiteDataRef->{'location_name'} is $countt\n";
  # print "DAF: Transfer : verifyFileList : Average Size $megab MB \n";
  print "Total Estimated Size on $SiteDataRef->{'location_name'} is $gigab GB \n";
  print "\n";

  my $N_FSVerified = scalar keys %{$SiteDataRef->{'FSVerified'}}       || 0;
  my $N_FSNotVerified = scalar keys %{$SiteDataRef->{'FSNotVerified'}} || 0;
  my $N_FSNotThere = scalar keys %{$SiteDataRef->{'FSNotThere'}}       || 0;
  my $N_FSBadSize = scalar keys %{$SiteDataRef->{'FSBadSize'}}         || 0;
  my $N_FSBadPerms = scalar keys %{$SiteDataRef->{'FSBadPerms'}}       || 0;

  print "Verification Summary for Archive Site '$SiteDataRef->{'location_name'}':\n";
  print "$N_FSVerified of $nfiles exist and match the file size recorded in the database.\n";
  if ($N_FSNotVerified > 0) {
    print "$N_FSNotVerified of $nfiles could not be verified.\n";
    print "$N_FSNotThere of $N_FSNotVerified unverified DO NOT EXIST on the remote file system.\n" if ($N_FSNotThere > 0); 
    print "$N_FSBadSize of $N_FSNotVerified unverified exist but DO NOT match the DB filesize field.\n" if ($N_FSBadSize > 0);
    print "$N_FSBadPerms of $N_FSNotVerified could not be verified due to UNIX file permission denial.\n" if ($N_FSBadPerms > 0);
  }
  print "\n";

  if ($Args->{'interactive'} && $Args->{'DAFDB'}) {
    if ($N_FSBadSize > 0) {
      while (1) {
        print "\nReplace the database size field with the current file size for the $N_FSBadSize files?\n";
        print "Y - Go ahead and update the appropriate filesize fields.\n";
        print "N - Don't do any database updates here.\n";
        print "P - Print file listing and ask again.\n";
        print ':';
        my $ans = <STDIN>;
        print "\n";
        chomp $ans;
        if (lc($ans) eq 'y') {
          print "\nUpdating file size records...\n"; 
          # Update appropriate size fields:
          if ($Args->{'DAFDB'}->updateFileSizeForSite(
            'ASiteFileList' => $SiteDataRef->{'FSBadSize'},
            'AllFilesARef' => $AllFilesARef,
            'ASiteID' => $SiteDataRef->{'location_id'}
            ) ) {
          # Modify "Verified" lists: 
            foreach my $i (keys %{$SiteDataRef->{'FSBadSize'}}) {
              delete $SiteDataRef->{'FSNotVerified'}->{$i} if (exists $SiteDataRef->{'FSNotVerified'}->{$i});
              $SiteDataRef->{'FSVerified'}->{$i} = $SiteDataRef->{'FSBadSize'}->{$i};
              delete $SiteDataRef->{'FSBadSize'}->{$i};
            }
            my $N_FSVerified = scalar keys %{$SiteDataRef->{'FSVerified'}}       || 0;
            my $N_FSNotVerified = scalar keys %{$SiteDataRef->{'FSNotVerified'}} || 0;
            my $N_FSBadSize = scalar keys %{$SiteDataRef->{'FSBadSize'}}         || 0;
            
            last;
          }
          else {
            bail->throw("ERROR: Unable to update records.\n");
          }
        }
        elsif(lc($ans) eq 'p') {
          print "\n";
          while((my $key, my $val) = each %{$SiteDataRef->{'FSBadSize'}}) {
            print $AllFilesARef->[$key]->{'id'},'  ',$AllFilesARef->[$key]->{'archivesites'},'  ',$AllFilesARef->[$key]->{'ncopies'},'  ',$SiteDataRef->{'FSBadSize'}->{$key}->{'size'},'  ',$SiteDataRef->{'FSBadSize'}->{$key}->{'checked_size'},'  ',$AllFilesARef->[$key]->{'archive_location'},'/',$AllFilesARef->[$key]->{'filename'},"\n";
          }
          print "\n";
          next;
        }
        else {
          last;
        }
      }
    } # End Bad Size condition
  }

  if ($N_FSVerified  != $nfiles  && $throw == 1) {
    DAF::Exception::VerifyFilesList->throw(
      error => "$N_FSNotVerified of $nfiles of the requested files could not be verified at site: $SiteDataRef->{'location_name'}\n",
      failed_files => \%failed_files
    );
  }

}

#################################################################################
# SUBROUTINE: transferArchiveToArchive
#
# DESCRIPTION:
#   Workhorse of arcp.  Used to move file lists between defined archive sites.
#################################################################################
sub transferFileList {
  my $this = shift;
  my $Args = shift;

  my ($ArgsARef, $AllFilesARef, @UberXferArgs, @destdirs, $TransferEngine, $try,@Transfers);

  # Make sure we have arguments:
  if ($Args->{'XferArray'}) {
    $ArgsARef = $Args->{'XferArray'};
  }
  else {
    bail->throw("transferArchiveToArchive: Must provide an array of tranfer arguments.");
  }
  if ($Args->{'AllFilesARef'}) {
    $AllFilesARef = $Args->{'AllFilesARef'};
  }
  else {
    bail->throw("transferArchiveToArchive: Must provide The \"All files\" Array ref.");
  }

  #################################################################
  # Loop over provided argument sets.  At this level, the provided list
  # should contain the required data to move a set of files from
  # one site to another.
  #################################################################
  my (@srclist, @destlist); # Just for a record of the sites used.

  foreach my $XferArgs (@$ArgsARef) {
    my ($src, $dest, $SrcSiteName, $DestSiteName, $nsrchosts, $ndesthosts,
      %ClientArgs, $nclients, @ClientFileLists, @FileListKeys
    );
    # Set a local variable for the relevant SiteData hashes.
    # Use 'local' for the SiteName if islocal.
    $src = $XferArgs->{'SrcSiteData'};
    $dest = $XferArgs->{'DestSiteData'};
    $ClientArgs{'SrcSiteData'} = $XferArgs->{'SrcSiteData'};
    $ClientArgs{'DestSiteData'} = $XferArgs->{'DestSiteData'};

    if ($src->{'islocal'}) {
      $SrcSiteName = 'local';
    }
    else {
      $SrcSiteName = $src->{'location_name'};
    }
    $DestSiteName = $dest->{'location_name'};

    push(@srclist,$SrcSiteName);
    push(@destlist, $DestSiteName);

    ###############################################################
    # Set site-level arguments:
    # Get inter-site tcp buffer and other transfer settings 
    # information from the DB for this transfer:
    ###############################################################

    # $xferSettings = $desdb->getXferSettings($srcSiteName, $destSiteName)

    $ClientArgs{'log_prefix'} = $this->LogDir() if ($this->LogDir());

    # Pick appropriate transfer engine:
    if ( ($SrcSiteName =~ /^mss/ && $SrcSiteName ne 'mss2') || ($DestSiteName =~ /^mss/ && $DestSiteName ne 'mss2')) {
#    if ($SrcSiteName =~ /^mss/ ) {
      $ClientArgs{'TransferMethod'} = 'uberftp';
    }
    else {
      $ClientArgs{'TransferMethod'} = 'guc';
    }

    # Set options for transfer client at site level:
    my %XferSettings;
    $XferSettings{'tcpbuf'} = 9000000;
    $XferSettings{'parallel_streams'} = 4;
    if ( ($DestSiteName =~/^mss/ && $DestSiteName ne 'mss2') || ($SrcSiteName =~/^mss/ && $SrcSiteName ne 'mss2') ) {
      $XferSettings{'MSS'}='';
      $XferSettings{'MSS_Family'}='des';
    }
    if ($SrcSiteName =~/^mss/ && $SrcSiteName ne 'mss2') {
      $XferSettings{'MSS'}='';
      $XferSettings{'MSS_Stage'}='';
      $XferSettings{'MSS_Wait'}='';
    }
    if ($SrcSiteName eq 'fnal' || $DestSiteName eq 'fnal') {
      $XferSettings{'ENSTORE'}='';
    }
    if ($src->{'site_id'} == 18 || $dest->{'site_id'} == 18) {
      $XferSettings{'nodcau'}=1;
    }

    if (! $this->MaxClients()) {
      if ($dest->{'gridftp_host'} =~ /.*cosmology.*/ || $src->{'gridftp_host'} =~ /.*cosmology.*/) {
        $this->MaxClients(1);
      }
    }

    $ClientArgs{'TransferSettings'} = \%XferSettings;    
    
    ###############################################################
    # Set up real physical file lists with absolute paths
    # for source and destination.  The file lists get attached to 
    # SiteData.  
    ###############################################################
    delete $src->{'AbsFileList'};
    delete $dest->{'AbsFileList'};

    my $srcHash;

    if ($SrcSiteName eq 'local') {
      while ((my $key, my $val) = each %{$src->{'ToSend'}} ) {
        push(@{$src->{'AbsFileList'}},$AllFilesARef->[$key]->{'srcfile'});
        push(@{$dest->{'AbsFileList'}},join('/',$dest->{'archive_root'},$AllFilesARef->[$key]->{'archive_location'},$AllFilesARef->[$key]->{'filename'}));
      }
    }
    else {

      $srcHash = $this->buildFileListForSite({
        'FileList' => $src->{'ToSend'},
        'AllFiles' => $AllFilesARef,
        'SiteID' => $src->{'location_id'},
        'ArchiveRoot' => $src->{'archive_root'}, 
        'Return' => 'HASH'
      });

      $src->{'AbsFileList'} = [keys %$srcHash];
      while ( (my $key, my $val) = each %$srcHash ) {
        my $FileIndex = $val->{'RefIndex'};
        my $absfile = join('/',$dest->{'archive_root'},$AllFilesARef->[$FileIndex]->{'archive_location'},$val->{'filename'});
        push (@{$dest->{'AbsFileList'}},$absfile);
      }
    }

    ###############################################################
    # Push all of the "client" arguments into and array.  This array
    # of arguments can represent multiple distinct source-dest pairs
    # In version 0.1 of arcp, the loop in arcp itself kind of trumps 
    # and makes it more likely that this array will always only have
    # one element.
    ###############################################################
    if ($this->debug()) {
      print "Source file list is ",scalar @{$src->{'AbsFileList'}}," files long\n"; 
    }
    push(@Transfers,\%ClientArgs);
  }

    my %transferArgs = (
      'Transfers' => \@Transfers
    );

    $transferArgs{'LogPrefix'} = $this->LogDir() if ($this->LogDir());
    $transferArgs{'verbose'} = $this->verbose() if ($this->verbose());
    $transferArgs{'verbose'} = $Args->{'try'} if ($Args->{'try'});

    $this->SUPER::transferFileList(\%transferArgs);

}


#################################################################################
# SUBROUTINE: removeArchiveFiles
#
#
#################################################################################
sub removeFileList {
  my $this = shift;
  my $Args = shift;

  my $SiteDataRef;
  my $AllFilesARef;

  if ($Args->{'SiteData'}) {
    $SiteDataRef = $Args->{'SiteData'};
  }
  else {
    bail->throw ("verifyArchiveFiles: Must provide Site data hash ref.");
  }
  if ($Args->{'AllFiles'}) {
    $AllFilesARef = $Args->{'AllFiles'};
  }
  else {
    bail->throw ("verifyArchiveFiles: Must provide The \"All files\" Array ref.");
  }

  my $NF = $#$AllFilesARef;
  my $nfiles = $NF+1;

  # Grab file locations and build the file list.  Work from the list
  # of files that are said to exist at this site. The SiteData hash
  # should have this list attached as "files_to_remove", which is a 
  # list of array indicies in $AllFilesARef.  The result is a local hash
  # with each key being an Absolote path/filename to verify.

  my $FileCheckList = $this->buildFileListForSite( {
    'FileList' => $SiteDataRef->{'files_to_remove'},
    'AllFiles' => $AllFilesARef,
    'SiteID' => $SiteDataRef->{'location_id'},
    'ArchiveRoot' => $SiteDataRef->{'archive_root'},
    'Return' => 'HASH'
  });

  print "\nRemoving file list on the file system at site: ",$SiteDataRef->{'location_name'},"... \n";
  my %removeArgs = ('SiteData' => $SiteDataRef,'FileList' => $FileCheckList);
  $removeArgs{'LogPrefix'} = $this->LogDir() if ($this->LogDir());
  $removeArgs{'verbose'} = $this->verbose() if ($this->verbose());

  $this->SUPER::removeFileList( \%removeArgs );
 
}


#################################################################################
# SUBROUTINE: getSrcSiteList
# Use the SiteData data structures to create a ordered list of source sites
# to use for the current transfer
#################################################################################
sub getSrcSiteList {
  my $this = shift;
  my $SiteInfo = shift;

  my $OnlyList;
  if (scalar @_ > 0) {
    $OnlyList = shift;
  }

  my %Sites;
  # Start with all sites that have data:
  foreach my $SiteID (keys %$SiteInfo) {
    if (my @files = keys %{$SiteInfo->{"$SiteID"}->{'DBVerified'}}) {
      my $nfiles = 0;
      if ($OnlyList) {
        foreach my $key (keys %$OnlyList) {
          $nfiles++ if (exists $SiteInfo->{"$SiteID"}->{'DBVerified'}->{"$key"}); 
        }
      }
      else {
        $nfiles = $#files;
      }
      push(@{$Sites{"$nfiles"}->{'sites'}}, $SiteID);
    }
  }

  my @Ordered_Sites;
  # Order first by number of filesc
  my @list_lengths = sort {$b <=> $a} keys %Sites;
  
  foreach my $length(@list_lengths) {  
    foreach my $site (@{$Sites{"$length"}->{'sites'}}) {
      push(@Ordered_Sites, $site);
    }
  }

  if ($this->debug) {
    print "\nArchive Site list, ordered by number of files:\n";
    print Dumper \@Ordered_Sites,"\n\n";
  }

  # Knock slow sites to the end of the list:
  my %last;
  my @lastRating;
  for(my $i=0;$i<=$#Ordered_Sites;$i++) {
    my $SiteID= $Ordered_Sites[$i];
    my $is = $SiteInfo->{"$SiteID"}->{'is_archive'};
    if ($is) {
      push(@{$last{$is}},$SiteID);
    }
  } 

  foreach my $israting (sort {$a <=> $b} keys %last) {
    foreach my $SiteID (@{$last{$israting}}) {
    #my $SiteID = $last{$israting};
      for(my $i=0;$i<=$#Ordered_Sites;$i++) {
        if ($Ordered_Sites[$i] == $SiteID) {
          splice(@Ordered_Sites,$i,1);
          last;
        }
      }
      push(@Ordered_Sites, $SiteID)
    }
  }

  if ($this->debug) {
    print "\nOrdered Archive Site list, ordered by is_archive:\n";
    print Dumper \@Ordered_Sites,"\n\n";
  }
  
  return \@Ordered_Sites;
}

#################################################################################
#
#################################################################################
sub buildFileListForSite {
  my $this = shift;
  my $Args = shift;

  # Arguments:
  my $IndexList = $Args->{'FileList'} if ($Args->{'FileList'});
  my $AllFilesARef = $Args->{'AllFiles'} if ($Args->{'AllFiles'});
  my $SiteID = $Args->{'SiteID'} if ($Args->{'SiteID'});
  my $ArchiveRoot = $Args->{'ArchiveRoot'} if ($Args->{'ArchiveRoot'});
  my $Return = $Args->{'Return'} if ($Args->{'Return'});
  my $showlist = 1 if ($Args->{'showlist'});
  my $new_showlist = 1 if ($Args->{'showFileList'});
  my $flavor = $Args->{'flavor'} if ($Args->{'flavor'});

  if ($Return) {
    if ($Return ne 'HASH' && $Return ne 'ARRAY') {
      warn ("Bad return argument, defaulting back to HASH\n");
      $Return = 'HASH';
    }
  }
  
  # Check arguments:
  bail->throw("No IndexList") if (! $IndexList);
  bail->throw("No AllFilesARef") if (! $AllFilesARef);
  bail->throw("No SiteID") if (! $SiteID);
  bail->throw("No ArchiveRoot") if (! $ArchiveRoot);

  my (%FileCheckList,%duplicates);

  foreach my $FileIndex (keys %$IndexList) {
    my ($absfile, $size);
    my $FileKeys = $AllFilesARef->[$FileIndex];
    my $filename = $FileKeys->{'filename'};

    my $suffix_type;
    if ($flavor) {
      $suffix_type = $flavor;
    }
    else {
      $suffix_type = substr $FileKeys->{'archivesites'}, $SiteID-1, 1;
    }  
 
    if ( $suffix_type eq 'F' ) {
      $filename .= '.fz';
      $size = $FileKeys->{'filesize_fz'};
    }
    elsif ( $suffix_type eq 'G'  ) {
      $filename .= '.gz';
      $size = $FileKeys->{'filesize_gz'};
    } 
    else {
      $size = $FileKeys->{'filesize'};
    }

    $absfile = join('/',$ArchiveRoot, $FileKeys->{'archive_location'}, $filename);

    if (exists $FileCheckList{"$absfile"}) {
      my $in = $FileCheckList{"$absfile"}->{'RefIndex'};
      $duplicates{$FileIndex} = $FileKeys;
      $duplicates{$FileIndex}->{'archive_location'} = $absfile;
      $duplicates{$in} = $AllFilesARef->[$in];
      $duplicates{$in}->{'archive_location'} = $absfile;
    }

    # Perhaps I should reverse this and make the index the key...:
    $FileCheckList{"$absfile"} = {'RefIndex' => $FileIndex, 'filename' => $filename, 'size' => $size};

  }
  if (%duplicates) {
    DAF::Exception::FoundDuplicates->throw(
      duplicates => \%duplicates,
      error => "\n\nERROR: Duplicate database entries found.\nThe file listing resulted in a list of files, whose database keys\nresolve to the same exact location and file name.\n"
    );
  }

  if ($showlist) {
    #print "\n\nBEGIN DESTINATION FILE LISTING:\n";
    print "Archive Root: ",$ArchiveRoot,"\n";

    foreach my $val (values %FileCheckList) {
      print join('/',$AllFilesARef->[$val->{'RefIndex'}]->{'archive_location'},$val->{'filename'})."\n";
    }

    #print "END DESTINATION FILE LISTING\n";
  }
  if ($new_showlist) {
    #print "\n\nBEGIN DESTINATION FILE LISTING\n";
    foreach my $val (values %FileCheckList) {
      my $i = $val->{'RefIndex'};
      print $AllFilesARef->[$i]->{'id'},' ',$ArchiveRoot,'/',$AllFilesARef->[$i]->{'archive_location'},'/',$val->{'filename'},"\n";
    }
    #print "END DESTINATION FILE LISTING\n";
  }

  if ($Return) {
    if ($Return eq 'HASH') {
      return \%FileCheckList;
    }
    elsif ($Return eq 'ARRAY') {
      return [keys %FileCheckList];
    }
  }

}

#################################################################################
#
#################################################################################
sub verifyFlavors {
  my $self = shift;
  my $Args = shift;

  my $SiteData = $Args->{'SiteData'} if ($Args->{'SiteData'});
  my $AllFilesARef = $Args->{'AllFiles'} if ($Args->{'AllFiles'});
  my $interactive = $Args->{'interactive'} if ($Args->{'interactive'});
  my $DAFDB = $Args->{'dafdb'} if ($Args->{'dafdb'});
  my $update_BadSize_NoSize = 1 if (exists $Args->{'update_BadSize_NoSize'});
  my $update_BadSize_OverrideSize = 1 if (exists $Args->{'update_BadSize_OverrideSize'});
  my $update_ThereVerified = 1 if (exists $Args->{'update_ThereVerified'});
  my $update_There = 1 if (exists $Args->{'update_There'});
  my $update_NotThere = 1 if (exists $Args->{'update_NotThere'});
  my $bailOnSizeMismatch = 1 if (exists $Args->{'bailOnSizeMismatch'});
  my $verbose = $self->verbose();
  $verbose = $Args->{'verbose'} if ($Args->{'verbose'});

  if ($SiteData) {
    bail->throw("Provided 'SiteData' argument does not contain a 'files_to_check' list.") if (!$SiteData->{'files_to_check'});
  }
  else {
    bail->throw("Must provide a SiteData hash referece as 'SiteData' argument.");
  }
  bail->throw("Must provide AllFiles array reference as 'AllFiles' argument.") if (! $AllFilesARef);

  my $FlavorDataHashRef;
  if ($DAFDB) {
    $FlavorDataHashRef = $DAFDB->FlavorData();
  }
  else {
    bail->throw("Must provide a DAF database object as argument 'dafdb'.");
  }

  my %NotThere = %{$SiteData->{'files_to_check'}};
  my $N_NotThere = scalar keys %NotThere;
  my $N_AllNotThere = $N_NotThere;

  ##################################################
  # Begin flavor checking
  ##################################################
  while ((my $flavor, my $FlavorData) = each %$FlavorDataHashRef) {
    next if ($flavor eq 'N');
    my (%FThere, %FNotThere,%BadSize, %FThereVerified);
    my $N_FThere = 0; 
    my $N_FNotThere = 0;
    my $N_BadSize = 0;
    my $N_FThereVerified = 0;
    $SiteData->{'files_to_check'} = {%NotThere}; # Copy for this iteration.
    foreach my $fi (keys %NotThere) {
      my $FileKeys = $AllFilesARef->[$fi];
      my $DBflavor = substr $FileKeys->{'archivesites'}, $SiteData->{'location_id'} - 1, 1;
      if ($DBflavor eq $flavor) {
        delete $SiteData->{'files_to_check'}->{$fi};
      }
    }

    #$SiteData->{'FSNotVerified'} = $SiteData->{'files_to_check'};

    if (scalar keys %{$SiteData->{'files_to_check'}} <= 0) {
      print "Nothing to check for '$flavor' flavor.\n";
      next;
    }

    print "\nChecking the Not verified list for $flavor flavor...\n";
    #print "# '$flavor' to check: ",scalar keys %{$SiteData->{'files_to_check'}} ,"\n";
    #print "# total to check: ", scalar keys %NotThere, "\n";

    eval {
       $self->verifyFileList(
        {
          'SiteData' => $SiteData,
          'AllFiles' => $AllFilesARef,
          'flavor' => $flavor,
          'verbose' => 0
        }
      );
    };

    # Catch not verified files. Add them to the "Flavor-Not-There" list if verified
    # that: "No such file or directory". If a non zero file size is returned, add them
    # to the "Flavor-There" list and remiove from the "Flavor-Not-There" list:
    if (my $e = DAF::Exception::VerifyFilesList->caught()) {
      foreach my $val (values %{$e->failed_files()}) {
        my $FileKeys = $AllFilesARef->[$val->{'RefIndex'}];
        if ($verbose >= 2) {
          printf ("%-10d %s %s\n", $FileKeys->{'id'},join('/',$FileKeys->{'archive_location'},$val->{'filename'}), $val->{'errstr'});
        }
        if ($val->{'errstr'} =~ /No such file or directory/) {
          $FNotThere{$val->{'RefIndex'}} = $FileKeys;
        }
        elsif (exists $val->{'checked_size'}) {
          if ($val->{'checked_size'} > 0) {
            $FThere{$val->{'RefIndex'}} = $FileKeys;
	    $FThere{$val->{'RefIndex'}}->{'checked_size'} = $val->{'checked_size'}; # Add found size in case needed for update.
	    delete $NotThere{$val->{'RefIndex'}} if ($NotThere{$val->{'RefIndex'}});
          }
        }
      }
      # Update the counts of the various file bins:
      $N_NotThere = scalar keys %NotThere;
      $N_FNotThere = scalar keys %FNotThere;
      $N_FThere = scalar keys %FThere;
    }

    if ($N_FThere > 0) {
      print "$N_FThere of $N_AllNotThere are there as $flavor flavor with unverified size.\n";
    }

    # If the file happened to actually pass verification as another flavor, delete those from
    # the "Flavor-Not-There" list as well.
    if ($SiteData->{'FSVerified'}) {
      foreach my $i (keys %{$SiteData->{'FSVerified'}}) {
        delete $NotThere{$i};
      }
      $N_FThereVerified = scalar keys %{$SiteData->{'FSVerified'}};
      $N_NotThere = scalar keys %NotThere;
    }
    print "$N_FThereVerified of $N_AllNotThere are there as $flavor flavor and match existing size information." if ($N_FThereVerified > 0);

    # For the files found to actually be there as a different flavor:
    # Check for size mismatches.  If the verified size does not match a existing size entry for that flavor,
    # remove the files from the "There" list and add them to the "Bad-Size" list.
    if ($N_FThere > 0 || $N_FThereVerified > 0) {
      print "\nChecking for size mismatches for already logged files of this flavor...\n";
      foreach my $i (keys %FThere) {
        if (defined $AllFilesARef->[$i]->{$FlavorData->{'size_field'}} && $AllFilesARef->[$i]->{$FlavorData->{'size_field'}} > 0) {
           if (defined $AllFilesARef->[$i]->{'checked_size'}) {
             if ($AllFilesARef->[$i]->{$FlavorData->{'size_field'}} != $AllFilesARef->[$i]->{'checked_size'}) {
               $BadSize{$i} = $AllFilesARef->[$i];
               delete $FThere{$i};
             }
          }
        }
      }
      # Update counts and print message:
      $N_BadSize = scalar keys %BadSize;
      $N_FThere = scalar keys %FThere;
      if ($N_BadSize <= 0) {
        print "No size mismatches found.\n";
      }
      else {
        if ($bailOnSizeMismatch) {
          bail->throw("\nERROR: Found $N_BadSize size mismatches, Exiting...\n");
        }
        else {
          warn "WARNING: Found $N_BadSize size mismatches.\n"
        }
      }
    }
    
    if ($N_FThere > 0) {
      if ($SiteData->{'FSFlavorVerified'}) {
        $SiteData->{'FSFlavorVerified'} = {%{$SiteData->{'FSFlavorVerified'}},%FThere};
      }
      else {
        $SiteData->{'FSFlavorVerified'} = {%FThere};
      }
    }
    if ($N_FThereVerified > 0) {
      if ($SiteData->{'FSFlavorVerified'}) {
        $SiteData->{'FSFlavorVerified'} = {%{$SiteData->{'FSFlavorVerified'}},%{$SiteData->{'FSVerified'}}};
      }
      else {
        $SiteData->{'FSFlavorVerified'} = {%{$SiteData->{'FSVerified'}}};
      }
    }

    if ($DAFDB) {
      # update DB records:
      if ($N_FThere > 0) {
        my $ans = '';
        if ($interactive) {
          print "\nUpdate $N_FThere DB records to change flavor to '$flavor' AND add current filesize for these $N_FThere files (y/n)?";
          $ans = <STDIN>;
          print "\n";
          chomp $ans;
        }
        if (lc($ans) eq 'y' || $update_There) {
          $DAFDB->updateArchiveSiteStrings(
            {
              'FileList' => \%FThere,
              'AllFilesARef' => $AllFilesARef,
              'updateSiteID' => $SiteData->{'location_id'},
              'updateVal' => $flavor,
              'updateFileSize' => 1,
            }
        );
        }
      }

      if ($SiteData->{'FSVerified'}) {
        my $N_Verified = scalar keys %{$SiteData->{'FSVerified'}};
        if ($N_Verified > 0) {
          my $ans = '';
          if ($interactive) {
            print "\nUpdate $N_Verified DB records to change flavor to '$flavor' for these $N_Verified files (y/n)?";
            $ans = <STDIN>;
            print "\n";
            chomp $ans;
          }
          if (lc($ans) eq 'y' || $update_ThereVerified) {
            $DAFDB->updateArchiveSiteStrings(
              {
                'FileList' => $SiteData->{'FSVerified'},
                'AllFilesARef' => $AllFilesARef,
                'updateSiteID' => $SiteData->{'location_id'},
                'updateVal' => $flavor,
              }
            );
          }
        }
      }

      if ($N_BadSize > 0) {
      while(1) {
        my $ans = '';
        if ($interactive) {
          print "\nThere are $N_BadSize with existing size information that does not match the DB.\n";
          print "Note that these records are in a bad state, a filesize for this flavor already existed\n";
          print "and did not match the size that has just been verified.  Corrective action should be taken.\n";
          print "If the copy being verified is the only copy of this flavor, selecting overide (o) may be ok.\n";
          print "If there are other copies of this flavor that have the size in the DB, overriding the size will\n";
          print "invalidate those copies.\n";
          print "Select option:\n";
          print "U - Only update archivesites string to '$flavor' for this copy.\n";
          print "S - Skip all updates for these files.\n";
          print "O - Override size and update archivesites string.\n";
          print "P - Print file listing and ask again.\n";
          print "(o/s/u)?";
          $ans = <STDIN>;
          print "\n";
          chomp $ans;
        }
        my %UpdateArgs = (
          'FileList' => \%BadSize,
          'AllFilesARef' => $AllFilesARef,
          'updateSiteID' => $SiteData->{'location_id'},
          'updateVal' => $flavor,
        );
        $UpdateArgs{'updateFileSize'} = 1 if (lc($ans) eq 'o' || $update_BadSize_OverrideSize);
        if (lc($ans) eq 's') {
         last;
        }
        elsif (lc($ans) eq 'o' || lc($ans) eq 'u' || $update_BadSize_OverrideSize || $update_BadSize_NoSize) {
          $DAFDB->updateArchiveSiteStrings(\%UpdateArgs);
          last;
        }
        elsif (lc($ans) eq 'w') {
          foreach my $file (keys %BadSize) {
            
          }
        }
        elsif (lc($ans) eq 'p') {
          print "\n";
          foreach my $file (keys %BadSize) {
            print $AllFilesARef->[$file]->{id},'  ',$AllFilesARef->[$file]->{archivesites},'  ',$AllFilesARef->[$file]->{ncopies},'  ',$AllFilesARef->[$file]->{$FlavorData->{'size_field'}},'  ',$AllFilesARef->[$file]->{checked_size},'  ',$AllFilesARef->[$file]->{archive_location},'/',$AllFilesARef->[$file]->{filename},"\n";
          }
          print "\n";
          next;
        }
        else {
           print "Inavaid response: $ans\n";
           next;
        }
 }
      }
    }

    last if ($N_NotThere <= 0); # No need to check other flavors if we got'em all.
  } # End Flavor loop.

  # After checking all flavors, some files really aren't there:
  if ($DAFDB) {
    if ($N_NotThere > 0) {
      my $ans = '';
      if ($interactive) {
        print "\nUpdate the archive site information to 'N' for the $N_NotThere files not found (y/n)?[n]>";
        $ans = <STDIN>;
        print "\n";
        chomp $ans;
      }
      if (lc($ans) eq 'y' || $update_NotThere) {
        $DAFDB->updateArchiveSiteStrings({
            'FileList' => \%NotThere,
            'AllFilesARef' => $AllFilesARef,
            'updateSiteID' => $SiteData->{'location_id'},
            'updateVal' => 'N'
          });
      }
    }
  }
}



#################################################################################
#
#################################################################################
sub DAF_catch_INT {
  DAF::Exception::Uncatchable->throw('error' => "\nCaught interrupt\nOk...I'm dead now...");
}

1;

=head1 NAME

DAF::DB::Util

=head1 SYNOPSIS

use DAF::DB::Util;
my $desdb = new DAF::DB::Util({\%attributes});

$desdb->getArchiveSiteInfo($site);
$desdb->isvalidArchiveSiteName($name);

=head1 Method Descriptions

=over 4

=item C<verifyArchiveFiles>

=item C<removeArchiveFiles>

=item C<transferArchiveFiles>

=back
