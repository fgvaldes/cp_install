#######################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit. 
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
# 
#######################################################################

package DAF::Archive::Tools;

use strict;
use warnings;
use Carp;
use FindBin;

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS, $VERSION);
BEGIN {
  $VERSION = "0.0.0";
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&verifyArchiveLocation &getArchiveLocation);
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}

#################################################################################
#
#################################################################################
sub getArchiveLocation {
  my $KeysRef = shift;

  verifyArchiveLocation($KeysRef);

  my $dir = join('/',$KeysRef->{'project'},$KeysRef->{'fileclass'});

  if ($KeysRef->{'fileclass'} eq 'src') {
    $dir = join('/',$dir,$KeysRef->{'nite'});
  }
  else {
    $dir = join('/',$dir,$KeysRef->{'run'});
  }

  my $shKey = $KeysRef->{'filetype'};

  if ( ($shKey eq 'diff_nitecmb_diff') || ($shKey eq 'diff_nitecmb_srch') || ($shKey eq 'diff_nitecmb_temp') ) { 
     $dir = join('/',$dir,$KeysRef->{'filetype'});
  }
  else {

    if ($KeysRef->{'filetype'} =~ /_/) {
       $KeysRef->{'filetype'} =~ /^([^_]+)/;     
       $dir = join('/',$dir,$1);
    }
    else {
      $dir = join('/',$dir,$KeysRef->{'filetype'});
    }

  }

  # Do not add exposurename for fileclass DIFF types
  if ( ($shKey eq 'diff_nitecmb_diff') || ($shKey eq 'diff_nitecmb_srch') || ($shKey eq 'diff_nitecmb_temp') ) { 
      if ( $KeysRef->{'exposurename'}=~/decam/ ) { 
            my $aa = 1;
      }
      else {
            $dir = join('/',$dir,$KeysRef->{'exposurename'});
      }
      #       print "EXPOSURENAME  $KeysRef->{'exposurename'}) \n"; 
  }
  else {

    if (defined $KeysRef->{'exposurename'} 
           && ($KeysRef->{'filetype'} =~ /^raw/ || $KeysRef->{'filetype'} =~/^red/ 
            || $KeysRef->{'filetype'} =~/^qcred/ || $KeysRef->{'filetype'} =~/^remap/ 
            || $KeysRef->{'filetype'} =~/^qcremap/ || $KeysRef->{'filetype'} =~/^diff/ 
            || $KeysRef->{'filetype'} =~/^qcdiff/ || $KeysRef->{'filetype'} =~ /^runtime/
            || $KeysRef->{'filetype'} eq 'QA')) {

      $dir = join('/',$dir,$KeysRef->{'exposurename'});

    }

  }

  # }
  
  # print "DAF Archive Tools getArchiveLocation : 4 dir $dir \n";
  
#  print "DIR: $dir\n";
  return $dir;
}

#################################################################################
#
#################################################################################
sub verifyArchiveLocation {
  my $KeysRef = shift;

  if ($KeysRef->{'project'}) {
    if ($KeysRef->{'fileclass'}) {
      if ($KeysRef->{'fileclass'} eq 'src') {
        if (! $KeysRef->{'nite'}) {
          croak("Must specify \'nite\'.\n");
        }
      }
      else {
        if (! $KeysRef->{'run'}) {
          croak("Must specify \'run\'.\n");
        }
      }
      if ($KeysRef->{'filetype'}) {
        if ($KeysRef->{'filetype'} =~ /^red/ || $KeysRef->{'filetype'} =~ /^qcred/ || $KeysRef->{'filetype'} =~ /^remap/ || $KeysRef->{'filetype'} =~ /^qcremap/ || $KeysRef->{'filetype'} =~ /^diff/ || $KeysRef->{'filetype'} =~ /^qcdiff/ ) {
          if (! $KeysRef->{'exposurename'}) {
            croak("Must specify \'exposurename\'.\n"); 
          }
        }
      }
      else {
        croak("Must specify \'filetype\'.\n");
      }
    }
    else {
      croak("Must specify \'fileclass\'.\n");
    }
  }
  else {
    croak("Must specify \'project\'.\n");
  }

}


1;
