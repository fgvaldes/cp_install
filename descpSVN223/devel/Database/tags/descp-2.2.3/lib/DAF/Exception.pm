#######################################################################
#  $Id: Exception.pm 1180 2008-06-24 20:08:46Z dadams $
#
#  $Rev:: 1180                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-06-24 13:08:46 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#######################################################################

package DAF::Exception;

use Exception::Class (
   DAF::Exception::Verify => {'description' => 'Exceptions for the DAF.'},

   DAF::Exception::VerifyFilesList => {
     'description' => 'File verification related exceptions for DAF.',
     'isa' => 'DAF::Exception::Verify',
     'fields' => ['failed_files']
   },

   DAF::Exception::FoundDuplicates => {
     'isa' => 'DAF::Exception::Verify',
     'fields' => ['duplicates']
   },
#   DAF::Exception::Uncatchable => {
#     'isa' => 'DAF::Exception::Uncatchable'
#   }

);

package DAF::Exception::Uncatchable;
  use Exception::Class (
  DAF::Exception::Uncatchable => {}
);
sub isa {shift->rethrow};

1;

