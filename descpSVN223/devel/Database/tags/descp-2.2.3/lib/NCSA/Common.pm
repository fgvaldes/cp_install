#######################################################################
#  $Id: Exception.pm 1180 2008-06-24 20:08:46Z dadams $
#
#  $Rev:: 1180                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-06-24 15:08:46 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#######################################################################

#
# Data and access methods common to all DAF objects
#

package NCSA::Common;
use NEXT;
{
my %ClassData = (
);

sub verbose {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    $ClassData{'verbose'} = $newvalue;
  }
  return $ClassData{'verbose'};
}

sub debug {
  my($self, $newvalue) = @_;
  if (@_ > 1) {
    $ClassData{'debug'} = $newvalue;
  }
  return $ClassData{'debug'};
}
}

sub new {
  my $this = shift;
  my $Args = shift if (@_);
  $Args = {} if (! defined $Args);
  my $class = ref($this) || $this;
  my $self = {};
  bless $self, $class;
  #$self->_init($Args);
  $self->EVERY::LAST::_init($Args);
  return $self;
}

sub _init {
  my $self = shift;
  my $Args = shift if (@_);
  $Args = {} if (! defined $Args);
  if ($Args->{'verbose'}) {
    $self->verbose($Args->{'verbose'});
  }
  else {
    $self->verbose(1);
  }
  if ($Args->{'debug'}) {
    $self->verbose($Args->{'debug'});
  }
  return $self;
}

1;
