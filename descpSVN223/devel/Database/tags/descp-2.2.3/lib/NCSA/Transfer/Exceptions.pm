#######################################################################
#  $Id: Exceptions.pm 1576 2008-08-14 20:02:59Z dadams $
#
#  $Rev:: 1576                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-08-14 13:02:59 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#######################################################################

package NCSA::Transfer::Exceptions;
use Exception::Class (

  NCSA::Transfer::Exceptions::runUber => {
    'description' => 'Generic uberftp run related exceptions.',
    'fields' => ['err_lines']
  }, 
  NCSA::Transfer::Exceptions::UberTimeout => {
    'description' => 'Timeout error.',
    'fields' => ['err_lines']
  } 

);


1;
