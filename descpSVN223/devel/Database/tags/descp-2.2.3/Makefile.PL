
use File::Find;
use ExtUtils::MakeMaker;

my %REQUIRED_MODULES = (
        'DBI'            => 1.602,
        'DBD::Oracle'    => 1.20,
        'List::Util' => 1.19,
        'Exception::Class' => 1.23,
        'Config::General' => 2.38,
);

WriteMakefile(
    NAME            => 'DAA',
    EXE_FILES   => [
                     'bin/file_ingest.pl',
                     'bin/find_ingest.pl',
                     'bin/catalog_ingest.pl',
                     'bin/arcp',
                     'bin/arrm',
                     'bin/arverify',
                     'bin/merge_objects',
                     'bin/create_ingest_table',
                     'bin/compress_files',
                     'bin/uncompress_files',
                     'bin/rm_files',
                     'libexec/post_ingest.pl',
                     'libexec/logversion.pl',
                     'scripts/remove-file-records'
                   ],
    PMLIBDIRS => ["./lib"],
    PREREQ_PM => \%REQUIRED_MODULES,
    PL_FILES => {},
);
