#!/usr/bin/perl

use strict;
use warnings;
use Carp;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use DB::DESUtil;
use Data::Dumper;



my $Rows;

#my $db = DB::DESUtil->new('verbose'=>3);
my $db = DB::DESUtil->new();

print Dumper($db->LOCATION_TABLE),"\n";

print $db->LOCATION_TABLE,"\n";

print $db->verbose(),"\n";

exit;

$Rows = $db->queryDB2(
  'ARCHIVE_SITES' => {
     'join' => {'site_id' => 'sites.site_id'}
   },
  'SITES' => {
   }
  
);
#show_results($Rows);


print "\n****************  Test 1 ****************\n";
$Rows = $db->queryDB(
  {'table' => 'ARCHIVE_SITES'}
);

#show_results($Rows);


print "\n****************  Test 2 ****************\n";
$Rows = $db->queryDB(
  {
    'table' => 'ARCHIVE_SITES',
    'key_vals' => {'archive_root' => '%UROOT%'}
  }
);
#show_results($Rows);

print "\n****************  Test 3 ****************\n";
$Rows = $db->queryDB(
  {
    'table' => 'ARCHIVE_SITES',
    'key_vals' => {'archive_root' => '%UROOT%','site_name' => 'mss'}
  }
);
#show_results($Rows);

print "\n****************  Test 4 ****************\n";
$Rows = $db->queryDB(
  {
    'table' => 'ARCHIVE_SITES',
    'key_vals' => {'site_name' => ['mss','mercury']},
    'select_fields' => ['site_name', 'archive_root']
  }
);
#show_results($Rows);

#die;
#print "\n****************  Test 5 ****************\n";
#$Rows = $db->queryDB(
#  {
#    'table' => 'ARCHIVE_SITES',
#    'where' => ['archive_root LIKE \'%UROOT%\'','site_name=\'mss\''],
#    'select_fields' => ['site_name', 'archive_root']
#  }
#);
#show_results($Rows);


print "\n****************  Test 6 ****************\n";
$Rows = $db->queryDB(
  {
    'table' => 'LOCATION',
    'key_vals' => {
        'fileclass' => 'red',
        'filetype' => 'red',
        'band' => ['g','i'],
        'project' => ['DES','DKA'],
        'ccd' => [1,2,3],
        'nite' => '20081003'
      }
  }
);
#show_results($Rows);

die;

print "\n****************  Test 7 ****************\n";
$Rows = $db->queryDB(
  {
    'table' => 'LOCATION',
    'key_vals' => {'project' =>'DKA', 'id' => [6574146,6574147]},
  }
);

print "\nFound ",scalar @$Rows," rows.\n";
############################################################
sub show_results {
  my $Rows = shift;

  foreach my $Row (@$Rows) {
    print "\n\n";
    while ((my $k, my $v) = each %$Row) {
      if ($v) {
        print "$k = $v\n";
      }
      else {
        print "$k =\n";
      }
    }
  }
}
