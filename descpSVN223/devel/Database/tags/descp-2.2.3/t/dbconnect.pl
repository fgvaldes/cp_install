#!/usr/bin/perl -w
use strict;
use warnings;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use DAF::DB::Util;
use NCSA::DB::wrapDBI;
my $desdb;

print "\nTest 1\n:";
$desdb = new NCSA::DB::wrapDBI( 
  {
    'db_type'=>'ORACLE',
    'db_server' => 'desorch.cosmology.illinois.edu',
    'db_name' => 'des',
    'db_user' => 'des_admin',
    'db_pass' => 'desmgr',
    'verbose' => 3
  }
);

print "\nTest 2\n:";

$desdb = new DAF::DB::Util({'verbose' =>3});
