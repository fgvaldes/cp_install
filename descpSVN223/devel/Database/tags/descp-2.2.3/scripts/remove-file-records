#!/usr/bin/perl

use strict;
use warnings;
use FindBin;
use lib ("$FindBin::Bin/../lib/perl5","$FindBin::Bin/../lib");
use DAF;
use DB::DESUtil;

my $DAF = new DAF();
my $stat;
my @idlist;
my @metadata_tables = ('IMAGE', 'EXPOSURE', 'COADD', 'CATALOG');
my $parent_table = 'LOCATION';

# Get file listing:
my $AllFilesARef = $DAF->DB()->getAllFileKeys (
  {
    'FileKeys' => $DAF->FileQueryKeys(),
    'all' => 1
  }
);

# push ids into an array:
foreach my $file (@$AllFilesARef) {
  push (@idlist,$file->{'id'});
}

if ($#idlist < 0) {
die "\nNothing to delete...\n";
}

print "\nAbout to remove all records for the result of the above query. Proceed?\nY/N [N]:";
my $ans = <STDIN>;
chomp $ans;
if (uc($ans) ne 'Y') {
  print "Exiting...\n";
  exit 0;
}
print "\n";

my $object_table = "OBJECTS_CURRENT";
my $delete_objects=0;
print "\nRemove all associated object records in $object_table?\nY/N [Y]:";
my $obj_ans = <STDIN>;
chomp $obj_ans;
if (uc($obj_ans) eq 'Y') {
  $delete_objects=1;
}
print "\n";

# Delete records from child tables first, then parent:
foreach my $table (@metadata_tables) {
  print "\nChecking for child records in $table\n";
  my $valid_rows = check_for_rows($table,\@idlist,$DAF->DB());

  my $nvalid = scalar @$valid_rows;
  print "Found ",$nvalid," valid rows in the $table table.\n";
  
  if ($nvalid > 0) {
    $stat = delete_records($table, $valid_rows, 'ID',$DAF->DB());
    if ($stat == -1) {
      die;
    }
    else {
      $DAF->DB()->commit();
    }
  }
#  else {
#    print "Nothing to do for the $table table.\n";
#  }
}

if ($delete_objects) {

  print "\nChecking for object records in $object_table\n";
  my $valid_rows = check_for_object_rows($object_table,\@idlist,$DAF->DB());
  my $nvalid = scalar @$valid_rows;
  if ($nvalid > 0) {
    $stat =
         delete_records($object_table, $valid_rows, 'OBJECT_ID',$DAF->DB());
    if ($stat == -1) {
      die;
    }
    else {
      $DAF->DB()->commit();
    }
  }

  foreach my $year ('2009','2008','2007'){
    my $object_table_initial ='OBJECTS';
    $object_table = $object_table_initial."_$year";
    print "\nChecking for object records in $object_table\n";
    $valid_rows = check_for_object_rows($object_table,\@idlist,$DAF->DB());
    $nvalid = scalar @$valid_rows;
    if ($nvalid > 0) {
      $stat = 
           delete_records($object_table, $valid_rows, 'OBJECT_ID',$DAF->DB());
      if ($stat == -1) {
        die;
      }
      else {
        $DAF->DB()->commit();
      }
    }

  }
}

$stat = delete_records($parent_table, \@idlist, 'ID',$DAF->DB());
if ($stat == -1) {
  die;
}
else {
  $DAF->DB()->commit();
}


sub check_for_rows {
  my $table = shift;
  my $idlist = shift;
  my $db = shift;

  my $dbq = "SELECT ID FROM $table WHERE ID=?";
  my $sth = $db->prepare($dbq);
  my @valid;
  foreach my $id (@$idlist) {
    $sth->execute($id);
    if (my $row = $sth->fetchrow_hashref()) {
      push(@valid,$row->{'id'});
    }
  }
  return \@valid;

}

sub check_for_object_rows {
  my $table = shift;
  my $idlist = shift;
  my $db = shift;

  my $dbq = "SELECT OBJECT_ID FROM $table WHERE CATALOGID=?";
  my $sth = $db->prepare($dbq);
  my @valid;
  foreach my $id (@$idlist) {
    $sth->execute($id);
    if (my $row = $sth->fetchrow_hashref()) {
      push(@valid,$row->{'id'});
    }
  }
  return \@valid;

}

sub delete_records {
  my $table = shift;
  my $idlist = shift;
  my $idfield = shift;
  my $db = shift;

#  my $csvlist = join(',',@$idlist);
#  my $dbq = "SELECT COUNT (ID) FROM $table WHERE ID IN ($csvlist)";
#  my $rows = $db->selectrow_array($dbq);
#
#  if ($rows <=0) {
#    print "\nNothing to delete in $table.\n";
#    return 0;
#  }  
#  print "\nDeleting $rows records from $table...\n";
  print "\nDeleting records from $table...\n";

  my $dbq = "DELETE FROM $table WHERE $idfield=?";
  
  my $sth = $db->prepare($dbq);
  my $tuples = $sth->execute_array(
      { ArrayTupleStatus => \my @tuple_status },
      $idlist
  );
  if ($tuples) {
      print "Successfully deleted $tuples records\n";
      return 0;
  }
  else {
    return -1;
  }
}
