-- PORTALS_ADMIN originally defined in ../desdb...sql

-- 2010-03-16 - bbaker & kkotwani
-- add SORT_ORDER column to PORTAL_ADMIN.SAMPLE_QUERIES

alter table sample_queries add SORT_ORDER integer;
create sequence sample_q_temp;
update sample_queries set sort_order=sample_q_temp.nextval;
drop sequence sample_q_temp;

-- 2010-03-26 - bbaker
-- add permission columns to PORTAL_ADMIN.USER_ACCESS
alter table user_access add HAS_ARCHIVEPORTAL_ACCESS char(1) default('N');
alter table user_access add HAS_WEBSERVICE_ACCESS char(1) default('N');
alter table user_access add HAS_CONTROLPORTAL_ACCESS char(1) default('N');
alter table user_access add MYDB_QUOTA integer default(0);

alter table user_access modify HAS_ARCHIVEPORTAL_ACCESS default('Y');

-- 2010-05-17 - bbaker
-- add db_credential table to PORTALS_ADMIN
create table db_credential(
    id               integer       not null,
    symbolic_name    varchar2(200) not null,
    role             varchar2(20)  not null,
    description      varchar2(200),
    host             varchar2(100) not null,
    sid              varchar2(50)  not null,
    db_name          varchar2(50),
    service_name     varchar2(50),
    login            varchar2(50)  not null,
    pwd              varchar2(50),
    constraint PK_DB_CREDENTIAL primary key(id)
);

------------------------------------------
-- 2010-06-16 - bbaker                  --
-- Update to new schema for Admin2      --
-- Adapted from portals_admin_devel.sql --
------------------------------------------

-- Create shared ID sequence
create sequence portals_admin.hibernate_sequence;

-- Alter Tables
-- -- add ID primary key to dictionary
alter table dictionary add id integer;
update dictionary set id=hibernate_sequence.nextval;
alter table dictionary modify id not null;
alter table dictionary drop constraint PK_DICTIONARY;
alter table dictionary add constraint PK_DICTIONARY_2 primary key(id);
-- -- add a tablename column
alter table dictionary add tablename varchar2(40);
create index IDX_DICTIONARY on dictionary(tablename, colname, colvalue);

-- -- add ID primary key to sample_queries
alter table sample_queries add id integer;
update sample_queries set id=hibernate_sequence.nextval;
alter table sample_queries modify id not null;
alter table sample_queries add constraint PK_SAMPLE_QUERIES primary key(id);

-- -- add valid_time and expire_time to db_credential
alter table db_credential add valid_time date default sysdate not null;
alter table db_credential add expire_time date;

-- Create new tables

-- -- derive des_user from user_profile, rather than creating from scratch
create table DES_USER as
 select hibernate_sequence.nextval id,
        user_profile.first_name, user_profile.last_name, user_profile.nvo_user,
        user_profile.create_time, user_profile.affiliation,
        user_profile.telephone, user_profile.email, user_profile.country
   from user_profile;
alter table des_user modify id not null;
alter table des_user modify (create_time date default sysdate);
alter table des_user add constraint PK_DES_USER primary key(id);

create table USER_DETAIL(
    ID               integer      not null,
    DES_USER_ID      integer      not null,
    AUTHOR_ID        integer,
    CREATE_TIME      date         default sysdate not null,
    VALID_TIME       date         default sysdate not null,
    EXPIRE_TIME      date,
    NAME             varchar2(100),
    SUBNAME          varchar2(100),
    VALUE            varchar2(4000),
    constraint PK_USER_DETAIL primary key(id),
    constraint FK_USER_DETAIL_USER
        foreign key(des_user_id) references des_user(id) on delete cascade,
    constraint FK_USER_DETAIL_AUTHOR
        foreign key(author_id) references des_user(id)
);

-- -- create the rest of the tables from scratch
create table DES_GROUP(
    ID               integer      not null,
    NAME             varchar2(100),
    constraint PK_DES_GROUP primary key(id)
);

create table USER_GROUP_MAP(
    DES_USER_ID        integer      not null,
    DES_GROUP_ID       integer      not null,
    constraint PK_USER_GROUP_MAP primary key(des_user_id, des_group_id),
    constraint FK_USER_GROUP_MAP_USER
        foreign key(des_user_id) references des_user(id) on delete cascade,
    constraint FK_USER_GROUP_MAP_GROUP
        foreign key(des_group_id)    references des_group(id)
);

create table MYDB_ACCOUNT(
    id               integer      not null,
    des_user_id      integer      not null,
    db_credential_id integer      not null,
    quota            integer,
    pwd              varchar2(100),
    valid_time       date         default sysdate not null,
    expire_time      date,
    constraint PK_MYDB_ACCOUNT primary key(id),
    constraint FK_MYDB_ACCOUNT_USER
        foreign key(des_user_id) references des_user(id) on delete cascade,
    constraint FK_MYDB_ACCOUNT_CREDENTIAL
        foreign key(db_credential_id) references db_credential(id)
);

create table ARCHIVESITE_PERMISSION(
    ID                        integer      not null,
    DES_USER_ID               integer      not null,
    ARCHIVESITE_LOCATION_ID   integer      not null,
    PERMIT                    char(1),
    constraint PK_ARCHIVESITE_PERMISSION primary key(id),
    constraint FK_ARCHIVESITE_PERMISSION_USER
        foreign key(des_user_id) references des_user(id) on delete cascade
);

create table ARCHIVESITE_LEGACY(
    ID                     integer      not null,
    DES_USER_ID            integer      not null,
    ARCHIVESITES           varchar2(30) not null,
    constraint PK_ARCHIVESITE_LEGACY primary key(id),
    constraint FK_ARCHIVESITE_LEGACY_USER
        foreign key(des_user_id) references des_user(id) on delete cascade
);

create table PORTAL(
    ID               integer         not null,
    NAME             varchar2(100)   not null,
    URL              varchar2(200),
    constraint PK_PORTAL primary key(id)
);

create table PORTAL_PERMISSION(
    DES_USER_ID           integer      not null,
    PORTAL_ID             integer      not null,
    constraint PK_PORTAL_PERMISSION primary key(des_user_id, portal_id),
    constraint FK_PORTAL_PERMISSION_USER
        foreign key(des_user_id) references des_user(id) on delete cascade,
    constraint FK_PORTAL_PERMISSION_PORTAL
        foreign key(portal_id) references portal(id)
);

-- copy data

-- a. portal permissions

-- -- hard-coded portals
--    (could use user_admin_seq.nextval instead of hard-coded ints,
--     but that would make inserts harder)
insert into portal values (0, 'admin',      null);
insert into portal values (1, 'descap',     null);
insert into portal values (2, 'control',    null);
insert into portal values (3, 'webservice', null);
insert into portal values (4, 'archive',    null);
insert into dictionary (id, tablename, colname, colvalue, prettyname, description)
   values (hibernate_sequence.nextval, 'PORTAL', 'NAME', 'admin', 'Admin Portal', 
           'Manage users and portals.');
insert into dictionary (id, tablename, colname, colvalue, prettyname, description)
   values (hibernate_sequence.nextval, 'PORTAL', 'NAME', 'descap', 'DESCap Portal',
           'TODO: get description');
insert into dictionary (id, tablename, colname, colvalue, prettyname, description)
   values (hibernate_sequence.nextval, 'PORTAL', 'NAME', 'control', 'Control Portal',
           'TODO: get description');
insert into dictionary (id, tablename, colname, colvalue, prettyname, description)
   values (hibernate_sequence.nextval, 'PORTAL', 'NAME', 'webservice', 'Web Service', 
           'Access DES resources and services via a machine-accessible web service.');
insert into dictionary (id, tablename, colname, colvalue, prettyname, description)
   values (hibernate_sequence.nextval, 'PORTAL', 'NAME', 'archive', 'Archive Portal',
           'TODO: get description');

-- -- admin portal
insert into portal_permission (des_user_id, portal_id)
    select des_user.id, 0
    from user_access, des_user
    where user_access.nvo_user=des_user.nvo_user
        and has_admin_access='Y';
-- -- descap
insert into portal_permission (des_user_id, portal_id)
    select des_user.id, 1
    from user_access, des_user
    where user_access.nvo_user=des_user.nvo_user
        and has_operator_access='Y';
-- -- control portal
insert into portal_permission (des_user_id, portal_id)
    select des_user.id, 2
    from user_access, des_user
    where user_access.nvo_user=des_user.nvo_user
        and has_controlportal_access='Y';
-- -- web services
insert into portal_permission (des_user_id, portal_id)
    select des_user.id, 3
    from user_access, des_user
    where user_access.nvo_user=des_user.nvo_user
        and has_webservice_access='Y';
-- -- archive portal
insert into portal_permission (des_user_id, portal_id)
    select des_user.id, 4
    from user_access, des_user
    where user_access.nvo_user=des_user.nvo_user
        and has_archiveportal_access='Y';

-- b. mydb_account
-- TODO: handle non-desorch mydb accounts
insert into db_credential (id, symbolic_name, role, 
                           description, host, sid, 
                           login)
    values (hibernate_sequence.nextval, 'portals.mydb.ncsa.1', 'super',
            'MyDB database on desorch', 'desorch.cosmology.illinois.edu', 'mydb',
            'des_portal');

insert into mydb_account (id, des_user_id, db_credential_id, quota)
    select hibernate_sequence.nextval, des_user.id, db_credential.id,
           user_access.mydb_quota
    from user_access, des_user, db_credential
    where user_access.nvo_user=des_user.nvo_user
        and has_mydb_account='Y'
        and db_credential.description='MyDB database on desorch';

-- c. api keys
insert into user_detail(id, des_user_id, name, value)
    select hibernate_sequence.nextval, des_user.id, 'api key', user_access.api_keys
    from user_access, des_user
    where user_access.nvo_user=des_user.nvo_user
        and length(api_keys) > 2;

-- d. DES groups
create table tmp_group as select unique des_group from des_accounts;
insert into des_group (id, name)
    select hibernate_sequence.nextval, des_group from tmp_group;
drop table tmp_group;
insert into USER_GROUP_MAP (des_user_id, des_group_id)
    select des_user.id, des_group.id
    from des_user, des_group, des_accounts
    where des_group.name = des_accounts.des_group
      and des_user.nvo_user = des_accounts.nvo_user;

-- e. NVO identity
insert into user_detail (id, des_user_id, name, subname, value)
    select hibernate_sequence.nextval, des_user.id,
           'identity', 'NVO', des_user.nvo_user
    from des_user;

-- f. archivesite legacy
insert into archivesite_legacy
    select hibernate_sequence.nextval, des_user.id, archivesites
    from user_access, des_user
    where des_user.nvo_user = user_access.nvo_user
      and archivesites is not null;

alter table des_user drop column nvo_user;

-- g. User attributes (email, telephone, etc.) - copy into user_detail
insert into user_detail (id, des_user_id, author_id, name, value, create_time)
    select hibernate_sequence.nextval, des_user.id, des_user.id,
           'affiliation', des_user.affiliation,
           case when des_user.create_time is null 
               then sysdate else des_user.create_time end
    from des_user where des_user.affiliation is not null;
insert into user_detail (id, des_user_id, author_id, name, value, create_time)
    select hibernate_sequence.nextval, des_user.id, des_user.id,
           'telephone', des_user.telephone,
           case when des_user.create_time is null 
               then sysdate else des_user.create_time end
    from des_user where des_user.telephone is not null;
insert into user_detail (id, des_user_id, author_id, name, value, create_time)
    select hibernate_sequence.nextval, des_user.id, des_user.id,
           'email', des_user.email,
           case when des_user.create_time is null 
               then sysdate else des_user.create_time end
    from des_user where des_user.email is not null;
insert into user_detail (id, des_user_id, author_id, name, value, create_time)
    select hibernate_sequence.nextval, des_user.id, des_user.id,
           'country', des_user.country,
           case when des_user.create_time is null 
               then sysdate else des_user.create_time end
    from des_user where des_user.country is not null;

alter table des_user drop column affiliation;
alter table des_user drop column country;
alter table des_user drop column telephone;
alter table des_user drop column email;

-- replace user_access
alter table user_access rename to USER_ACCESS_OLD;

create view USER_ACCESS as
select 
   des_user.id DES_USER_ID,
   identity.value NVO_USER, ARCHIVESITES, 
   case when mydb_credential.host is null then 'N' else 'Y' end HAS_MYDB_ACCOUNT,
   mydb_credential.host MYDB_HOST,
   case when   admin_perm.des_user_id is null then 'N' else 'Y' end
      HAS_ADMIN_ACCESS,
   case when    oper_perm.des_user_id is null then 'N' else 'Y' end
      HAS_OPERATOR_ACCESS,
   case when archive_perm.des_user_id is null then 'N' else 'Y' end
      HAS_ARCHIVEPORTAL_ACCESS,
   case when      ws_perm.des_user_id is null then 'N' else 'Y' end
      HAS_WEBSERVICE_ACCESS,
   case when    ctrl_perm.des_user_id is null then 'N' else 'Y' end
      HAS_CONTROLPORTAL_ACCESS,
   case when api_key.value is null then ' ' else api_key.value end API_KEYS,
   mydb_account.quota MYDB_QUOTA
from des_user
  left outer join user_detail identity
      on identity.des_user_id = des_user.id
         and identity.name = 'identity' and identity.subname = 'NVO'
  left outer join archivesite_legacy
      on archivesite_legacy.des_user_id = identity.des_user_id
  left outer join mydb_account
      on mydb_account.des_user_id = archivesite_legacy.des_user_id
  left outer join db_credential mydb_credential
      on mydb_credential.id = mydb_account.db_credential_id
  left outer join portal_permission admin_perm
      on admin_perm.des_user_id = des_user.id and admin_perm.portal_id = 0
  left outer join portal_permission oper_perm
      on oper_perm.des_user_id = des_user.id and oper_perm.portal_id = 1
  left outer join user_detail api_key
      on api_key.des_user_id = des_user.id and api_key.name = 'api key'
  left outer join portal_permission archive_perm
      on archive_perm.des_user_id = des_user.id and archive_perm.portal_id = 4
  left outer join portal_permission ws_perm
      on ws_perm.des_user_id = des_user.id and ws_perm.portal_id = 3
  left outer join portal_permission ctrl_perm
      on ctrl_perm.des_user_id = des_user.id and ctrl_perm.portal_id = 2;

-- replace user_profile
alter table user_profile rename to USER_PROFILE_OLD;

create view USER_PROFILE as
select identity.value nvo_user, first_name, last_name,
       affil.value affiliation, phone.value telephone,
       email.value email, country.value country,
       des_user.create_time
from des_user
  left outer join user_detail identity
     on identity.des_user_id = des_user.id
        and identity.name='identity' and identity.subname='NVO'
  left outer join user_detail affil
     on affil.des_user_id=des_user.id and affil.name='affiliation'
  left outer join user_detail email
     on email.des_user_id=des_user.id and email.name='email'
  left outer join user_detail phone
     on phone.des_user_id=des_user.id and phone.name='telephone'
  left outer join user_detail country
     on country.des_user_id=des_user.id and country.name='country';


-- -------------------------------------------
-- 2010-08-24 - bbaker
-- replace des_accounts with view based on des_group
-- -------------------------------------------

-- add des_user_id to des_accounts
alter table des_accounts add des_user_id integer;
update des_accounts
  set des_user_id =
    (select user_detail.des_user_id 
       from user_detail 
       where des_accounts.nvo_user=value 
         and name='identity' and subname='NVO');

-- create a view from user_group_map joined with user_detail and des_group
-- Note: MyDB columns (HAS_MYDB_ACCOUNT, MYDB_HOST) are unnecessary, so skip them
alter table des_accounts rename to des_accounts_old;
create view des_accounts as 
select
  user_group_map.des_user_id   des_user_id,
  user_detail.value            nvo_user,
  user_group_map.des_group_id  des_group_id,
  des_group.name               des_group
from user_group_map, user_detail, des_group
  where user_group_map.des_user_id = user_detail.des_user_id
    and user_detail.name = 'identity' and user_detail.subname = 'NVO'
    and user_group_map.des_group_id = des_group.id;

-- permissions
grant select on user_access to PROC_READER;
grant select on user_profile to PROC_READER;
grant select on des_accounts to PROC_READER;
create or replace public synonym user_access for portals_admin.user_access;
create or replace public synonym user_profile for portals_admin.user_profile;
create or replace public synonym des_accounts for portals_admin.des_accounts;

-- -------------------------------------------
-- 2010-05-17 bbaker
-- fix portal access
-- -------------------------------------------
-- MyDB access based on user_access_old.has_mydb_account
--   (was based on has_operator_access)
-- create operator portal
--   grant access based on user_access.has_operator_access
update portal set name='operator' where name='descap';
insert into portal (id, name) values (5, 'descap');
insert into portal_permission (des_user_id, portal_id)
    select des_user_id, 5
    from user_access_old, user_detail
    where user_detail.name='identity' and user_detail.subname='NVO'
      and user_access_old.nvo_user=user_detail.value
      and has_mydb_account='Y';
commit;
-- grant everyone except guests archiveportal access
insert into portal_permission
    (des_user_id, portal_id) 
    select id, 4 from des_user 
       where id not in (select des_user_id from user_group_map, des_group
                        where des_group.id=des_group_id
                          and des_group.name='guest')
         and id not in (select des_user_id from portal_permission 
                        where portal_id=4);
commit;

-- -----------------------
-- 2010-09-13 bbaker
-- create user_query table
-- -----------------------
create table user_query(
    -- primary key
    id               integer      not null,
    -- foreign keys
    des_user_id      integer      not null,
    des_group_id     integer,
    mydb_account_id  integer,
    db_credential_id integer,
    latest_status_id integer,
    -- regular fields
    service_query    varchar2(4000) not null,
    submit_time      date         default sysdate not null,
    users_title      varchar(500),
    users_description varchar(4000),
    estimated_area_sq_deg float,
    estimated_plan_seconds float,
    db_query         varchar(4000),
    db_serial        integer,
    db_session_id    integer,
    num_db_rows      integer,
    num_db_bytes     integer,
    served_bytes     integer,
    served_format    varchar(50),
    served_identifier varchar(1000),
    result_table_name varchar(100),
    constraint pk_user_query primary key(id),
    constraint fk_user_query_user
        foreign key(des_user_id) references des_user(id),
    constraint fk_user_query_group
        foreign key(des_group_id) references des_group(id),
    constraint fk_user_query_mydb_account
        foreign key(mydb_account_id) references mydb_account(id),
    constraint fk_user_query_db_credential
        foreign key(db_credential_id) references db_credential(id)
);

create table query_status (
    id               integer      not null,
    user_query_id    integer      not null,
    name             varchar2(50) not null,
    subname          varchar2(50),
    notes            varchar2(500),
    constraint pk_query_status primary key(id),
    constraint fk_query_status_user_query
        foreign key(user_query_id) references user_query(id) on delete cascade
);

alter table user_query add
    constraint fk_user_query_latest_status
        foreign key(latest_status_id) references query_status(id);
