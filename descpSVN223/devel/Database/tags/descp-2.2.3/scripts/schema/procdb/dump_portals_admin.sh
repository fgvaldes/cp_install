#! /bin/bash

# Save a dump of PORTALS_ADMIN
# run this script as oracle on desproc

# assumes that the following steps have already been done:
# 1. create virtual directory
# [oracle@desproc]$ mkdir /data/oracle/backup/portals_admin
# SQL> create directory portals_admin_dump_dir as '/data/oracle/backup/portals_admin';
# SQL> grant read,write on directory portals_admin_dump_dir to portals_admin;

TIME="`date +%Y%m%d_%H_%M_%S`"
DUMP_DIR="/data/oracle/backup/portals_admin"
DUMP_FILE="portals_admin.$TIME.dmp"
LOG_FILE="portals_admin.$TIME.log"

expdp portals_admin/portalsmgr directory=portals_admin_dump_dir \
      dumpfile=$DUMP_FILE logfile=$LOG_FILE job_name=portals_admin_dump \
      schemas=portals_admin

cp $DUMP_DIR/$DUMP_FILE /home/bbaker/portals_admin_dump

cat $DUMP_DIR/$LOG_FILE | mail -s "Daily dump of PORTALS_ADMIN" "kkotwani@ncsa.illinois.edu,bbaker@ncsa.uiuc.edu"

# deployment:
# this script is run daily by user oracle on desproc.cosmology.illinois.edu,
# via crontab
