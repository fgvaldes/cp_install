Procedure to build the new production Fermi Standby database. Assume the new standby is called fmdes, ORACLE_HOME=/oracle/11.1.0, ORACLE_BASE=/oracle.

P = Primary
S = Standby

  Site	To Do
------------------------------------------------
*   S   Check and install required software/library packages (See Oracle's instruction)
*   S   Create Oracle groups and user account
*   S   Create Directories: (If your file system does not match those 
        directories, you can create these directories using symbolic links.)
        /oracle 
        /oracle/data
        /oracle/data/des
        /oracle/data/desdr
        /oracle/data/des_temp
        /oracle/redo
        /oracle/redo/des
        /oracle/archive
        /oracle/archive/des
*   S   Configure Linux kernel parameters (See Oracle's instruction)
*   S   Install Oracle software
*   S   Create a new database using $ORACLE_HOME/bin/dbca:
        -- In Database Templates page
        General Purpose = (checked)
        -- In Database Identification page
        Global Database Name = des
        SID = des
        -- In Management Options page
        Configure Enterprise Manager = (unchecked)
        -- In Database Credentials page
        Use the Same Administrative Password for All Accounts = (checked)
		(the password you set will be overwritten after the database is replicated)
        -- In Storage Options page
        File System = (checked)
        -- In Database File Locaions page
        Use Common Location for All Database Files = (checked)
        Database Files Location = /oracle/data
        -- In Recovery Configuration
        Specify Flash Recovery Area = (checked)
        Flash Recovery Area = {ORACLE_BASE}/flash_recovery_area
        Flash Recovery Area Size = 20480 M Bytes
        Enable Archiving = (checked)
        -- Click the button for "Edit Archive Mode Parameters"
        -- In Edit Archive Mode Parameters page
        Automatic Archiving = (checked)
        Archive Log File Format = %t_%s_%r.dbf 
        Archive Log Destinations = /oracle/archive/des
        -- In Database Content page / Sample Schemas folder
        Sample Schemas = (unchecked)
        -- In Database Content page / Customer Scripts folder
        No Scripts to Run = (checked)
        -- In Initialization Parameters page / Memory folder
        Typical = (checked)
        Memory Size (SGA and PGA) = 40% (or less) of your RAM
        Use Automatic Memory Management = (checked)   
        -- In Initialization Parameters page / Sizing folder
        Block Size = 8192 Bytes
        Processes = 500
        -- In Initialization Parameters page / Character Sets folder
        Use Default = (checked) (WE8MSWIN1252)
        National Character Set = AL16UTF16 - Unicode UTF-16 Univewrsal character set
	Default Language = American
        Default Territory = United States
        -- In Initialization Parameters page / Connection Mode folder
        Dedicated Server Mode = (checked)
        -- In Security Settings page 
        Keep the enhanced 11g default security setting = (checked)
        -- In Automatic Maintenance Tasks page
        Enable automatic maintenance tasks = (unchecked)
        -- In Database Storage page
        Controlfile = (use all defaults)
        Datafiles = (use all defaults)
        Redo Log Groups = Configure it as the following: 
          (1) We need 12 redo groups: from redo01.log to redo12.log.
          (2) Each group has one member. 
          (3) The size of each redo log is 400 M Bytes. 
          (4) The file location for redo log is /oracle/redo/des
*   S   Your database should be in the open mode after running dbca. You can runsome queries on database catalog tables/views to test it.

*   P   Shutdown EM
*   P   Generate the current init file
        SQL> create pfile='/full_path/initdes.ora' from spfile='/full_path/spfiledes.ora'
*   P	Prepare initdes.ora (in $ORACLE_HOME/dbs)
	These parameters must be set up:
          log_archive_dest_n,
	  log_archive_dest_state_n 
          log_archive_config
*   P	Start instance by pfile
*   P   Create spfile from pfile
*   P	Modify listener.ora, tnsnames.ora, and sqlnet.ora in 
        $ORACLE_HOME/network/admin
*   P   restart lsnrctl
*   P   Run rman to delete expired archived log
          rman target /
          rman> crosscheck archivelog all;
          rman> delete expired archivelog all;
*   P   Create a backup for standby database using 
          /oracle/scripts/des/des_prim_rman.sh (took 6 hours for 1.2 TB db)
*   P   Copy all backup files and newly generated archived logfiles to the 
          standby server at same location. (took 12 hours for 1.2 TB db on a local standby database)
*   P   Copy $ORACLE_HOME/dbs/orapwdes to the standby server at same location.

*   S   Generate pfile from spfile
        SQL> create pfile='/oracle/11.1.0/dbs/initdes.ora' from spfile='/oracle/11.1.0/dbs/spfiledes.ora';
*   S   Shutdow database
        sqlplus / as sysdba
        SQL> shutdown immediate;
        SQL> exit;
*   S	Prepare initdes.ora (in $ORACLE_HOME/dbs) (Reference to the file partial_initfmdes.ora)
	These parameters must be set up:
          db_name, db_unique_name, 
          log_archive_config, log_archive_dest_1,
	  log_archive_dest_2, log_archive_dest_state_1, 
          log_archive_dest_state_2, remote_login_passwordfile,
          log_archive_format, log_archive_max_process, fal_server,
          fal_client, compatible (must be > 11.0.0.0),
          standby_file_management='AUTO',
          db_recovery_file_dest_size (must be >= 20GB)
*   S	Modify listener.ora, tnsnames.ora (in $ORACLE_HOME/network/admin) (Reference to the files: fmdes_listener.ora and fmdes_tnsnames.ora)
*   S   Ensure the file system for $ORACLE_HOME/flash_recovery_area has >= 20GB
*   S   Ensure the file system for $ORACLE_HOME/redo has >= 20GB
*   S	Start instance by pfile
        sqlplus / as sysdba
        SQL> startup open pfile='/oracle/11.1.0/dbs/initdes.ora';
*   S   Create spfile from pfile
        SQL> create spfile='/oracle/11.1.0/dbs/spfiledes.ora' from pfile='/oracle/11.1.0/dbs/initdes.ora';
        SQL> shutdown immediate;
*   S   Restart instance by spfile
        sqlplus / as sysdba
        SQL> startup open;
*   S   Create standby logfiles exactly same as the 
          primary db, check v$standby_log for the result.
        SQL> alter database add standby logfile ('/oracle/redo/des/slog01.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog02.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog03.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog04.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog05.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog06.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog07.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog08.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog09.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog10.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog11.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog12.rdo') size 400M;
        SQL> alter database add standby logfile ('/oracle/redo/des/slog13.rdo') size 400M;
        SQL> select * from v$standby_log;
        SQL> exit;

*   S   Shutdown database, then restart nomount
        sqlplus / as sysdba
        SQL> shutdown immediate;
        SQL> startup nomount;

*   P   Duplicate the database using RMAN
          (It took 6 hours for 1.2 TB db on a local standby database)

*   S   The standby database should be in the mount state after duplication.
*   S   Open database in Read Only mode
          sqlplus / as sysdba
          sql> alter database recover managed standby database cancel;
          sql> alter database open read only;
          sql> recover managed standby database disconnect 
               using current logfile;

*   P   Force a logfile switch
          sql> alter system switch logfile;

*   S   Monitor alert.log to trace-log transfer and redo-apply


Lesson learnt:

/02/18/2010. Fermi database server is crashed due to power outage. The server 
is not come up until 40 hours later. Have done the following to recover the 
standby database hosting on the server:

* setevn ORACLE_SID des
* scp missed archived redo logs from the primary db server, total 390 files.
* lsnrctl start
* sqlplus / as sysdba
  sql> startup mount;
  sql> recover managed standby database;
  sql> alter database recover managed standby database cancel;
  sql> alter database open read only;
  sql> recover managed standby database disconnect using current logfiles;

Notes:
* Do not need to manually scp missed archived redo logs from the primary db 
  server, because after recovery is started, Oracle automatically copies and 
  register all missing logs from the primary to the standby.
* The average time for applying logs is 1.5 min per log.
* The entire recovery process took 4.5 hours, because there were 390 missed 
  logs +  new logs generated during the recovery.
* Don't put the database in the "read only" mode, and don't use the command 
  "recover managed standby database disconnect using current logfiles;" 
  for the large number of redo log recovery. Do it in the "mount" mode, and
  using the command "recover managed standby database;", and cancel it 
  after the redo apply is catched up, and then use "recover managed standby 
  database disconnect using current logfiles;".
