# /usr/bin/perl

#ankit

#/home/ankitc/test/Ankit/devel/Database/trunk/scripts/maintenance/createMV_objects_dr002.sql

use strict;
use warnings;
use FileHandle;
use DBI;
use DBD::Oracle;


my $insertQuery ;
my $selectQuery ;
my $whereQuery ;
my $table_name = $ARGV[0];


my $fp_insert = FileHandle->new('maintenance/sqlInsert_ObjectsMV.sql','r');
if(defined $fp_insert){
my @lines = <$fp_insert>;
 $insertQuery = join("",@lines);
undef $fp_insert;
undef @lines;
}
else
{
        print " could not open the file name for insert query: the error is: $!";
        die " exiting...";
}

my $fp_select = FileHandle->new('maintenance/sqlSelect_ObjectsMV.sql','r');
if(defined $fp_select){
my @lines = <$fp_select>;
 $selectQuery = join("",@lines);
undef $fp_select;
undef @lines;
}
else
{
        print " could not open the file name for select query: the error is: $!";
        die " exiting...";
}

my $fp_where = FileHandle->new('maintenance/sqlWhere_ObjectsMV.sql','r');
if(defined $fp_where){
my @lines = <$fp_where>;
 $whereQuery = join("",@lines);
undef $fp_where;
undef @lines;
}
else
{
        print " could not open the file name for where query: the error is: $!";
        die " exiting...";
}

my $fileSQL = 'insert into '.$table_name.' '.$insertQuery.' '.$selectQuery;

############################################# INITIALIZATION OF DATABASES AND OTHER VARIABLES ########################################
my @lines;
my %queryParams;# = ('1','a','2','b');
my @newarr;
my $sourcefile;

# CONNECT TO THE DATABASE AND FIRE THE QUERY
my @driver_names = DBI->available_drivers;
my  %drivers      = DBI->installed_drivers;
my  @data_sources = DBI->data_sources('Oracle');

my $dsn = 'desdb.cosmology.illinois.edu';
my $user = 'des_admin';
my $password = 'deSadM1005';
my $pw = $password;



# CONFIG VARIABLES
=for
my $platform = "oracle";
my $database = "des";
my $host = $dsn;
#my $port = "3306";
#my $tablename = "inventory";
#my $user = "username";
my $pw = $password;
=cut

#DATA SOURCE NAME
#print $dsn = "dbi:Oracle:$database:$host";
 $dsn = "DBI:Oracle:host=desdb.cosmology.illinois.edu;sid=des";

# PERL DBI CONNECT
 my $dbh = DBI->connect($dsn, $user, $pw,{ AutoCommit => 0 }) or print "Found error in connecting ";
 #DBI->connect($dsn, $user, $password,
  #                    { RaiseError => 1, AutoCommit => 1 });
############################################ INITIALIZATION OF DATABASES AND OTHER VARIABLES DONE########################################

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}


=for
purpose of the script is:
1. (optionally) create the materialized view
2. get the params for the insert query; from the list provided by shantanu or from the result of a query
3. create the queries for insertion with the data from the source as described above.
4. execute the query 
5 commit.
6. repeat the loop for all the data from the input file
=cut

# create a materialized view: DONE FOR NOW: do not need to do this for this time

############################################### read data from the input file into an array ####################################################

	# the first param passed with the script is used as a file name
	
$sourcefile = $ARGV[1];

if($sourcefile eq '')
{
	die "File parameter not found! Please enter one...";
	print "\n should not see this!";
}
else
{
	print "\n\n file found, moving on... "; print $sourcefile;
}

print "\n continuing further... ";


# open the file and read it into a variable;

my $fp = FileHandle->new($sourcefile,'r');
if(defined $fp){
@lines = <$fp>;
undef $fp;
}
else
{
	print " could not open the file name: the error is: $!";
	die " exiting...";
}


############################################### read data from file complete ####################################################################


############################################ CREATE WHERE CLAUSE AND APPEND TO PARENT QUERY ########################################
my $i = 0;

=for
-- use the following query to generate a list of catalog ids which can be
-- used in building drNNN_objects.
select '20091123000000_20091001', id
from wl 
where run = '20091123000000_20091001'
and   wltype = 'shapelet_shpltall';
=cut

	my $val1 ;	
	my $val2 ;	
	my $WhereClause;# = $whereQuery; 

foreach my $filelist ( @lines)
{
	print "\n new item: $filelist";
	@newarr = split (/ +/,$filelist);
	print "\n newarray $i for 0: $newarr[0]<--";
	print "\n newarray $i for 1: $newarr[1]<--";
	$queryParams{'ankit'.$i}{'id'} = $newarr[0];
	$queryParams{'ankit'.$i}{'run'} = $newarr[1];

	$WhereClause = $whereQuery; 
 	$val1 = trim($newarr[0]);	
	$val2 = trim($newarr[1]);	
	#my $WhereClause = ' where  img.run = \''.trim($newarr[0]).'\' and   img.id = '.trim($newarr[1]).' and   img.imagetype = \'red\' and   img.scampflg = 0 and   obj.imageid = img.id ';
	$WhereClause =~ s/PARAM1/$val1/g; 
	$WhereClause =~ s/PARAM2/$val2/g; 
	$i++;
	
	
	my $fileSQL_final = $fileSQL.$WhereClause;
	print $fileSQL_final;
	my $stmt = $dbh->prepare($fileSQL_final)   or &displayError("Could not prepare queries aborting");
    $stmt->execute() or print "Found error ".$stmt->errstr; #&displayError(" Couldnt execute statement:  " .$stmt->errstr);
    my $rows_affected = $stmt->rows;
    print "\n\n rows affected: $rows_affected";
    if($rows_affected > 0)
    {
    	print "\n found rows success, comitting now..";
    	$dbh->commit;	
    }
    
	#die '\n looping\n';
}

print "\n did u see this?? ";
die "\n hmm..";
exit;
print "\n must not see this";

########################################### CREATE WHERE CLAUSE DONE ########################################

# Create the sql query to parse

########################################### EXECUTE QUERY ######################################## 
print "\n\n\n\n\n\n going to execute now!! \n\n\n\n";
sleep 5;
die "Done for now";
exit;
die" bass.....";

#my $stmt = $dbh->prepare($fileSQL_final)   or &displayError("Could not prepare queries aborting");
 #$stmt->execute() or print "Found error ".$stmt->errstr; #&displayError(" Couldnt execute statement:  " .$stmt->errstr);
#while (my @row = $stmt->fetchrow_array() ) {
 #   print " new row: @row\n";
#}
#$dbh->commit;

=for
$dbh = DBI->connect($sid, $user, $pwd, "Oracle")
     or &displayError("Couldn't connect to database: " .DBI->errstr);

  # Prepare the SQL statement
  $stmt = $dbh->prepare(' INSERT INTO guestbook_entry(name, email, comments, commentdate) VALUES ('
                          . $nameValue .','. $emailValue. ','.$commentsValue. ',sysdate )')
    or &displayError("Could not prepare queries aborting");

 #Execute the above SQL statement
 $stmt->execute()
    or &displayError(" Couldnt execute statement:  " .$stmt->errstr);

 # Submit the changes
 $dbh->commit;

 # Close up the cursor
 $stmt->finish;

 # Disconnect from the database
 $dbh->disconnect;

=cut

#$sth = $dbh->prepare("SELECT foo, bar FROM table WHERE baz=?");

#$sth->execute( my $baz );
=for
  while (my @row = my$sth->fetchrow_array ) {
    print "@row\n";
  }
=cut
print "\n done! \n";

