set pagesize 0
set linesize 1500
set trimspool on
set colsep '|'
set echo off
set term off
set feedback off
column RA format '999.99999'
column DEC format '999.99999'
spool dc5_tag.dat
select 
tilename, 
coadd_objects_id, 
zeropoint_g, zeropoint_r, zeropoint_i, 
zeropoint_z, zeropoint_y, 
mag_aper3_g, mag_aper3_r, mag_aper3_i, 
mag_aper3_z, mag_aper3_y, 
magerr_aper3_g, magerr_aper3_r, magerr_aper3_i, 
magerr_aper3_z, magerr_aper3_y, 
class_star_r, class_star_i, 
theta_image, 
a_image, b_image, 
kron_radius,
mag_model_g, mag_model_r, 
mag_model_i, mag_model_z, mag_model_y, 
magerr_model_g, magerr_model_r, 
magerr_model_i, magerr_model_z, magerr_model_y, 
spread_model_r, spread_model_i, 
spreaderr_model_r, spreaderr_model_i, 
mag_psf_g,
mag_psf_r,
mag_psf_i,
mag_psf_z,
mag_psf_y,
magerr_psf_g,
magerr_psf_r,
magerr_psf_i,
magerr_psf_z,
magerr_psf_y,
photoz_id, photoz, photozerr, photoz_flag, 
ra, dec
from dr004_coadd_objects;
exit;
