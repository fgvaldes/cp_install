#!/bin/bash
#
# rcat offline backup script create on 12/22/2008 by Dora Cai

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/rcat'
DATADIR='/oracle/oradata/rcat'
DBSDIR='/oracle/11.1.0/dbs'
NETDIR='/oracle/11.1.0/network/admin'
BUDIR='/oracle/backup/rcat.'$TODAY
mkdir $BUDIR
cp /dev/null $DIR/rcat_backup.log
source $DIR/rcat.bash

# shutdown database rcat using immediate option
sqlplus / as sysdba @$DIR/shutora_imm.sql
if test $? -eq 0 ; then
   echo `date`: RCAT database is shut down. >> $DIR/rcat_backup.log
else 
   echo `date`: Fail to shutdown rcat database immediately >> $DIR/rcat_backup.log
   echo "-----------" >> $DIR/rcat_backup.log
   mail -s"RCAT offline backup failed" ycai@ncsa.uiuc.edu <$DIR/rcat_backup.log
   exit
fi

echo `date`: RCAT offline backup is started >> $DIR/rcat_backup.log
cp -p $DBSDIR/* $BUDIR/.
cp -p $NETDIR/*.ora $BUDIR/.
cp -p $DATADIR/*.* $BUDIR/.
if test $? -eq 0 ; then
   echo `date`: RCAT all of files are copied. >> $DIR/rcat_backup.log
   sqlplus / as sysdba @$DIR/startora.sql
   if test $? -eq 0; then
      echo `date`: RCAT database is started up. >> $DIR/rcat_backup.log
   else
      echo `date`: RCAT database cannot be started up. >> $DIR/rcat_backup.log
      mail -s"RCAT Offline backup failed" ycai@ncsa.uiuc.edu <$DIR/rcat_backup.log
      exit
   fi
else # copy failed
   echo `date`: Fail to copy oracle database files >> $DIR/rcat_backup.log
   mail -s"RCAT Offline backup failed" ycai@ncsa.uiuc.edu <$DIR/rcat_backup.log
   exit
fi

SESSION=0
SESSION=`ps -ef | grep "_rcat" | wc -l`
if [ $SESSION -lt 15 ] ; then
    echo "RCAT database is failed to startup. " >> $DIR/rcat_backup.log
    mail -s"RCAT Offline backup failed" ycai@ncsa.uiuc.edu <$DIR/rcat_backup.log
    exit
fi

echo `date`: RCAT database backup complete. >> $DIR/rcat_backup.log
mail -s"RCAT Offline backup successful" ycai@ncsa.uiuc.edu <$DIR/rcat_backup.log
exit
