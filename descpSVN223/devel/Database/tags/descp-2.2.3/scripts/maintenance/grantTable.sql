-- Run this script as DES_ADMIN
-- 
-- Create public synonyms
--

drop public synonym des_exposures;
create public synonym des_exposures for des_admin.des_exposures;
grant all on des_exposures to des_writer;
grant select on des_exposures to des_reader;

drop public synonym device_list;
create public synonym device_list for des_admin.device_list;
grant all on device_list to des_writer;
grant select on device_list to des_reader;

drop public synonym software;
create public synonym software for des_admin.software;
grant all on software to des_writer;
grant select on software to des_reader;

drop public synonym survey_tiles;
create public synonym survey_tiles for des_admin.survey_tiles;
grant all on survey_tiles to des_writer;
grant select on survey_tiles to des_reader;

drop public synonym zeropoint;
create public synonym zeropoint for des_admin.zeropoint;
grant all on zeropoint to des_writer;
grant select on zeropoint to des_reader;

drop public synonym objects;
create public synonym objects for des_admin.objects;
grant all on objects to des_writer;
grant select on objects to des_reader;

drop public synonym objects_seq;
create public synonym objects_seq for des_admin.objects_seq;
grant all on objects_seq to des_writer;
grant select on objects_seq to des_reader;

drop public synonym test_des_stripe82_stds;
create public synonym test_des_stripe82_stds for des_admin.test_des_stripe82_stds;
grant all on test_des_stripe82_stds to des_writer;
grant select on test_des_stripe82_stds to des_reader;

drop public synonym dc0_truth;
create public synonym dc0_truth for des_admin.dc0_truth;
grant all on dc0_truth to des_writer;
grant select on dc0_truth to des_reader;

drop public synonym des_testobs;
create public synonym des_testobs for des_admin.des_testobs;
grant all on des_testobs to des_writer;
grant select on des_testobs to des_reader;

drop public synonym des_stripe82_stds_v1 ;
create public synonym des_stripe82_stds_v1 for des_admin.des_stripe82_stds_v1 ;
grant all on des_stripe82_stds_v1 to des_writer;
grant select on des_stripe82_stds_v1 to des_reader;

drop public synonym files;
create public synonym files for des_admin.files;
grant all on files to des_writer;
grant select on files to des_reader;

drop public synonym files_seq;
create public synonym files_seq for des_admin.files_seq;
grant all on files_seq to des_writer;
grant select on files_seq to des_reader;

drop public synonym wcsoffset;
create public synonym wcsoffset for pipeline.wcsoffset;
grant all on wcsoffset to des_writer;
grant select on wcsoffset to des_reader;

CREATE PUBLIC SYNONYM coaddtile FOR DES_ADMIN.coaddtile;
GRANT ALTER, DELETE, INSERT, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON  coaddtile TO DES_WRITER;
GRANT SELECT ON  coaddtile TO DES_READER;

drop public synonym object_check_maglength
CREATE PUBLIC SYNONYM OBJECT_CHECK_MAGLENGTH FOR DES_ADMIN.OBJECT_CHECK_MAGLENGTH
grant execute, debug on object_check_maglength to des_writer;
grant DEBUG on des_admin.object_check_maglength to des_writer;
grant execute, debug on object_check_maglength to des_reader;
grant DEBUG on des_admin.object_check_maglength to des_reader;

drop public synonym archive_sites;
create public synonym archive_sites for des_admin.archive_sites;
grant all on archive_sites to des_writer;
grant select on archive_sites to des_reader;

drop public synonym des_users;
create public synonym des_users for des_admin.des_users;
grant all on des_users to des_writer;
grant select on des_users to des_reader;

drop public synonym standard_stars;
create public synonym standard_stars for des_admin.standard_stars;
grant all on standard_stars to des_writer;
grant select on standard_stars to des_reader;

drop public synonym standard_stars_seq;
CREATE PUBLIC SYNONYM standard_stars_seq FOR des_admin.standard_stars_seq;
GRANT ALL ON standard_stars_seq TO des_writer;
GRANT SELECT ON standard_stars_seq TO des_reader;


drop public synonym ucac2;
CREATE PUBLIC SYNONYM UCAC2 FOR des_admin.UCAC2;
GRANT ALL ON UCAC2 TO des_writer;
GRANT SELECT ON UCAC2 TO des_reader;

drop public synonym ucac2_seq;
CREATE PUBLIC SYNONYM ucac2_seq FOR des_admin.ucac2_seq;
GRANT ALL ON ucac2_seq TO des_writer;
GRANT SELECT ON ucac2_seq TO des_reader;

drop public synonym des_accounts;
create public synonym des_accounts for des_admin.des_accounts;
grant all on des_accounts to des_writer;
grant select on des_accounts to des_reader;

drop public synonym nite_access;
create public synonym nite_access for des_admin.nite_access;
grant all on nite_access to des_writer;
grant select on nite_access to des_reader;

drop public synonym imagetype_access;
create public synonym imagetype_access for des_admin.imagetype_access;
grant all on imagetype_access to des_writer;
grant select on imagetype_access to des_reader;

drop public synonym runiddesc_access;
create public synonym runiddesc_access for des_admin.runiddesc_access;
grant all on runiddesc_access to des_writer;
grant select on runiddesc_access to des_reader;

drop public synonym user_registration;
create public synonym user_registration for des_admin.user_registration;
grant all on user_registration to des_writer;
grant select on user_registration to des_reader;

create public synonym fra_add_360 for des_admin.fra_add_360;
grant execute, debug on fra_add_360 to des_writer;
grant execute, debug on fra_add_360 to des_reader;

CREATE SEQUENCE IMSIM2_truth_seq NOCACHE START WITH 1 INCREMENT BY 1;
CREATE PUBLIC SYNONYM IMSIM2_truth_seq FOR des_admin.IMSIM2_truth_seq;
CREATE PUBLIC SYNONYM IMSIM2_truth FOR des_admin.IMSIM2_truth;
GRANT ALL ON IMSIM2_truth TO des_writer;
GRANT SELECT ON IMSIM2_truth TO des_reader; 
GRANT ALL ON IMSIM2_truth_seq TO des_writer;
GRANT SELECT ON IMSIM2_truth_seq TO des_reader;

create public synonym COADD_OBJECTS for des_admin.COADD_OBJECTS;
grant all on COADD_OBJECTS to des_writer;
grant select on COADD_OBJECTS to des_reader;
 
create sequence COADD_OBJECTS_seq cache 1000 start with 1 increment by 1;
create public synonym COADD_OBJECTS_seq for des_admin.COADD_OBJECTS_seq;
grant all on COADD_OBJECTS_seq to des_writer;
grant select on COADD_OBJECTS_seq to des_reader;

create public synonym BCS_Reduction_Status for des_admin.BCS_Reduction_Status;
grant all on BCS_Reduction_Status to des_writer;
grant select on BCS_Reduction_Status to des_reader;
 
create public synonym location for des_admin.location;
grant all on location to des_writer;
grant select on location to des_reader;
grant all on location_seq to des_writer;
grant select on location_seq to des_reader;

create public synonym BCS_Reduction_Status for des_admin.BCS_Reduction_Status;
grant all on BCS_Reduction_Status to des_writer;
grant select on BCS_Reduction_Status to des_reader;
 
create public synonym location for des_admin.location;
grant all on location to des_writer;
grant select on location to des_reader;
 
create public synonym image for des_admin.image;
grant all on image to des_writer;
grant select on image to des_reader;
 
create public synonym Catalog for des_admin.Catalog;
grant all on Catalog to des_writer;
grant select on Catalog to des_reader;
 
create public synonym Exposure for des_admin.Exposure;
grant all on Exposure to des_writer;
 
create public synonym image for des_admin.image;
grant all on image to des_writer;
grant select on image to des_reader;
 
create public synonym Catalog for des_admin.Catalog;
grant all on Catalog to des_writer;
grant select on Catalog to des_reader;
 
create public synonym Exposure for des_admin.Exposure;
grant all on Exposure to des_writer;
grant select on Exposure to des_reader;
 
create public synonym Coadd for des_admin.Coadd;
grant all on Coadd to des_writer;
grant select on Coadd to des_reader;
 
drop public synonym runiddesc_access;
create public synonym runiddesc_access for des_admin.runiddesc_access;
grant all on runiddesc_access to des_writer;
grant select on runiddesc_access to des_reader;

drop public synonym matches;
create public synonym matches for des_admin.matches;
grant all on matches to des_writer;
grant select on matches to des_reader;

drop public synonym spec_standards;
create public synonym spec_standards for des_admin.spec_standards;
grant all on spec_standards to des_writer;
grant select on spec_standards to des_reader;

drop public synonym spec_standards_seq;
create public synonym spec_standards_seq for des_admin.spec_standards_seq;
grant all on spec_standards_seq to des_writer;
grant select on spec_standards_seq to des_reader;

create public synonym snscan for des_admin.snscan;
create public synonym snscan_seq for des_admin.snscan_seq;
grant all on snscan to des_writer;
grant select on snscan to des_reader;
grant all on snscan_seq to des_writer;
grant select on snscan_seq to des_reader;

create public synonym snspect for des_admin.snspect;
create public synonym snspect_seq for des_admin.snspect_seq;
grant all on snspect to des_writer;
grant select on snspect to des_reader;
grant all on snspect_seq to des_writer;
grant select on snspect_seq to des_reader;

create public synonym snfit for des_admin.snfit;
create public synonym snfit_seq for des_admin.snfit_seq;
grant all on snfit to des_writer;
grant select on snfit to des_reader;
grant all on snfit_seq to des_writer;
grant select on snfit_seq to des_reader;

create public synonym sngals for des_admin.sngals;
create public synonym sngals_seq for des_admin.sngals_seq;
grant all on sngals to des_writer;
grant select on sngals to des_reader;
grant all on sngals_seq to des_writer;
grant select on sngals_seq to des_reader;

create public synonym snobs for des_admin.snobs;
create public synonym snobs_seq for des_admin.snobs_seq;
grant all on snobs to des_writer;
grant select on snobs to des_reader;
grant all on snobs_seq to des_writer;
grant select on snobs_seq to des_reader;

create public synonym sncand for des_admin.sncand;
create public synonym sncand_seq for des_admin.sncand_seq;
grant all on sncand to des_writer;
grant select on sncand to des_reader;
grant all on sncand_seq to des_writer;
grant select on sncand_seq to des_reader;

create public synonym matches for des_admin.matches;
grant all on matches to des_writer;
grant select on matches to des_reader;

create public synonym desdm_columns for des_admin.desdm_columns;
grant all on desdm_columns to des_writer;
grant select on desdm_columns to des_reader;

create public synonym control_users for des_admin.control_users;
grant all on control_users to des_writer;
grant select on control_users to des_reader;

create public synonym photo_z for des_admin.photo_z;
grant all on photo_z to des_writer;
grant select on photo_z to des_reader;
create public synonym photo_z_seq for des_admin.photo_z_seq;
grant all on photo_z_seq to des_writer;
grant select on photo_z_seq to des_reader;

drop public synonym objects_current;
create public synonym objects_current for des_admin.objects_current;
grant all on objects_current to des_writer;
grant select on objects_current to des_reader;

drop public synonym objects_2007;
create public synonym objects_2007 for des_admin.objects_2007;
grant all on objects_2007 to des_writer;
grant select on objects_2007 to des_reader;

drop public synonym objects_2008;
create public synonym objects_2008 for des_admin.objects_2008;
grant all on objects_2008 to des_writer;
grant select on objects_2008 to des_reader;

drop public synonym objects_2009;
create public synonym objects_2009 for des_admin.objects_2009;
grant all on objects_2009 to des_writer;
grant select on objects_2009 to des_reader;

drop public synonym block;
create public synonym block for des_admin.block;
grant all on block to des_writer;
grant select on block to des_reader;
drop public synonym block_seq;
create public synonym block_seq for des_admin.block_seq;
grant all on block_seq to des_writer;
grant select on block_seq to des_reader;

drop public synonym pfwtask;
create public synonym pfwtask for des_admin.pfwtask;
grant all on pfwtask to des_writer;
grant select on pfwtask to des_reader;
drop public synonym pfwtask_seq;
create public synonym pfwtask_seq for des_admin.pfwtask_seq;
grant all on pfwtask_seq to des_writer;
grant select on pfwtask_seq to des_reader;

drop public synonym pfwmsg;
create public synonym pfwmsg for des_admin.pfwmsg;
grant all on pfwmsg to des_writer;
grant select on pfwmsg to des_reader;
drop public synonym pfwmsg_seq;
create public synonym pfwmsg_seq for des_admin.pfwmsg_seq;
grant all on pfwmsg_seq to des_writer;
grant select on pfwmsg_seq to des_reader;

drop public synonym data_release;
create public synonym data_release for des_admin.data_release;
grant all on data_release to des_writer;
grant select on data_release to des_reader;

drop public synonym dc5_truth;
drop public synonym dc5_truth_seq;
create public synonym dc5_truth for des_admin.dc5_truth;
create public synonym dc5_truth_seq for des_admin.dc5_truth_seq;
grant all on dc5_truth to des_writer;
grant select on dc5_truth to des_reader;
grant all on dc5_truth_seq to des_writer;
grant select on dc5_truth_seq to des_reader;

drop public synonym dr001_objects;
create public synonym dr001_objects for des_admin.dr001_objects;
grant all on dr001_objects to des_writer;
grant select on dr001_objects to des_reader;

drop public synonym dr_access;
create public synonym dr_access for des_admin.dr_access;
grant all on dr_access to des_writer;
grant select on dr_access to des_reader;

drop public synonym dr002_coadd_objects;
create public synonym dr002_coadd_objects for des_admin.dr002_coadd_objects;
grant all on dr002_coadd_objects to des_writer;
grant select on dr002_coadd_objects to des_reader;

drop public synonym dr003_coadd_objects;
create public synonym dr003_coadd_objects for des_admin.dr003_coadd_objects;
grant all on dr003_coadd_objects to des_writer;
grant select on dr003_coadd_objects to des_reader;

drop public synonym dr002_files;
create public synonym dr002_files for des_admin.dr002_files;
grant all on dr002_files to des_writer;
grant select on dr002_files to des_reader;

drop public synonym dr003_files;
create public synonym dr003_files for des_admin.dr003_files;
grant all on dr003_files to des_writer;
grant select on dr003_files to des_reader;

drop public synonym dr002_objects;
create public synonym dr002_objects for des_admin.dr002_objects;
grant all on dr002_objects to des_writer;
grant select on dr002_objects to des_reader;

drop public synonym dr003_objects;
create public synonym dr003_objects for des_admin.dr003_objects;
grant all on dr003_objects to des_writer;
grant select on dr003_objects to des_reader;

drop public synonym dictionary;
create public synonym dictionary for des_admin.dictionary;
grant all on dictionary to des_writer;
grant select on dictionary to des_reader;

drop public synonym dc5sim_usnob;
drop public synonym dc5sim_usnob_seq;
create public synonym dc5sim_usnob for des_admin.dc5sim_usnob;
create public synonym dc5sim_usnob_seq for des_admin.dc5sim_usnob_seq;
grant all on dc5sim_usnob to des_writer;
grant select on dc5sim_usnob to des_reader;
grant all on dc5sim_usnob_seq to des_writer;
grant select on dc5sim_usnob_seq to des_reader;

create public synonym gsn_aug09_truth for des_admin.gsn_aug09_truth;
create public synonym gsn_aug09_truth_seq for des_admin.gsn_aug09_truth_seq;
grant all on gsn_aug09_truth to des_writer;
grant select on gsn_aug09_truth to des_reader;
grant all on gsn_aug09_truth_seq to des_writer;
grant select on gsn_aug09_truth_seq to des_reader;

create public synonym gsnsim_aug09_usnob for des_admin.gsnsim_aug09_usnob;
create public synonym gsnsim_aug09_usnob for des_admin.gsnsim_aug09_usnob_seq;
grant all on gsnsim_aug09_usnob to des_writer;
grant select on gsnsim_aug09_usnob to des_reader;
grant all on gsnsim_aug09_usnob_seq to des_writer;
grant select on gsnsim_aug09_usnob_seq to des_reader;

create public synonym snveto for des_admin.snveto;
create public synonym snveto_seq for des_admin.snveto_seq;
grant all on snveto to des_writer;
grant select on snveto to des_reader;
grant all on snveto_seq to des_writer;
grant select on snveto_seq to des_reader;
grant all on snveto to sn_writer_role;
grant all on snveto_seq to sn_writer_role;

drop public synonym dc5sim_usnob_oct11;
create public synonym dc5sim_usnob_oct11 for des_admin.dc5sim_usnob_oct11;
grant all on dc5sim_usnob_oct11 to des_writer;
grant select on dc5sim_usnob_oct11 to des_reader;

drop public synonym dc5sim_usnob_oct12;
create public synonym dc5sim_usnob_oct12 for des_admin.dc5sim_usnob_oct12;
grant all on dc5sim_usnob_oct12 to des_writer;
grant select on dc5sim_usnob_oct12 to des_reader;

drop public synonym dc5sim_usnob_oct13;
create public synonym dc5sim_usnob_oct13 for des_admin.dc5sim_usnob_oct13;
grant all on dc5sim_usnob_oct13 to des_writer;
grant select on dc5sim_usnob_oct13 to des_reader;

drop public synonym dc5sim_usnob_oct14;
create public synonym dc5sim_usnob_oct14 for des_admin.dc5sim_usnob_oct14;
grant all on dc5sim_usnob_oct14 to des_writer;
grant select on dc5sim_usnob_oct14 to des_reader;

drop public synonym dc5sim_usnob_oct15;
create public synonym dc5sim_usnob_oct15 for des_admin.dc5sim_usnob_oct15;
grant all on dc5sim_usnob_oct15 to des_writer;
grant select on dc5sim_usnob_oct15 to des_reader;

drop public synonym dr004_files;
create public synonym dr004_files for des_admin.dr004_files;
grant all on dr004_files to des_writer;
grant select on dr004_files to des_reader;

drop public synonym sample_queries;
create public synonym sample_queries for des_admin.sample_queries;
grant all on sample_queries to des_writer;
grant select on sample_queries to des_reader;

drop public synonym dr004_objects;
create public synonym dr004_objects for des_admin.dr004_objects;
grant all on dr004_objects to des_writer;
grant select on dr004_objects to des_reader;

drop public synonym dr004_coadd_objects;
create public synonym dr004_coadd_objects for des_admin.dr004_coadd_objects;
grant all on dr004_coadd_objects to des_writer;
grant select on dr004_coadd_objects to des_reader;

drop public synonym sextractor_magnitude;
create public synonym sextractor_magnitude for des_admin.sextractor_magnitude;
grant all on sextractor_magnitude to des_writer;
grant select on sextractor_magnitude to des_reader;

drop public synonym user_access;
create public synonym user_access for des_admin.user_access;
grant all on user_access to des_writer;
grant select on user_access to des_reader;

drop public synonym user_profile;
create public synonym user_profile for des_admin.user_profile;
grant all on user_profile to des_writer;
grant select on user_profile to des_reader;

drop public synonym standard_stars_dc5;
create public synonym standard_stars_dc5 for des_admin.standard_stars_dc5;
grant all on standard_stars_dc5 to des_writer;
grant select on standard_stars_dc5 to des_reader;

drop public synonym wl_me_shear;
create public synonym wl_me_shear for des_admin.wl_me_shear;
grant all on wl_me_shear to des_writer;
grant select on wl_me_shear to des_reader;

drop public synonym dr005_files;
create public synonym dr005_files for des_admin.dr005_files;
grant all on dr005_files to des_writer;
grant select on dr005_files to des_reader;

drop public synonym dr005_objects;
create public synonym dr005_objects for des_admin.dr005_objects;
grant all on dr005_objects to des_writer;
grant select on dr005_objects to des_reader;

drop public synonym wl_me_shear_seq;
create public synonym wl_me_shear_seq for des_admin.wl_me_shear_seq;
grant all on wl_me_shear_seq to des_writer;
grant select on wl_me_shear_seq to des_reader;

drop public synonym dr005_coadd_objects;
create public synonym dr005_coadd_objects for des_admin.dr005_coadd_objects;
grant all on dr005_coadd_objects to des_writer;
grant select on dr005_coadd_objects to des_reader;

drop public synonym dr004_coadd_objects_tag;
create public synonym dr004_coadd_objects_tag for des_admin.dr004_coadd_objects_tag;
grant all on dr004_coadd_objects_tag to des_writer;
grant select on dr004_coadd_objects_tag to des_reader;

drop public synonym dc5_tag;
create public synonym dc5_tag for des_admin.dc5_tag;
grant all on dc5_tag to des_writer;
grant select on dc5_tag to des_reader;

drop public synonym dr006_objects;
create public synonym dr006_objects for des_admin.dr006_objects;
grant all on dr006_objects to des_writer;
grant select on dr006_objects to des_reader;

drop public synonym dr006_files;
create public synonym dr006_files for des_admin.dr006_files;
grant all on dr006_files to des_writer;
grant select on dr006_files to des_reader;

drop public synonym dr006_coadd_objects;
create public synonym dr006_coadd_objects for des_admin.dr006_coadd_objects;
grant all on dr006_coadd_objects to des_writer;
grant select on dr006_coadd_objects to des_reader;

drop public synonym dr007_files;
create public synonym dr007_files for des_admin.dr007_files;
grant all on dr007_files to des_writer;
grant select on dr007_files to des_reader;

drop public synonym dr007_coadd_objects;
create public synonym dr007_coadd_objects for des_admin.dr007_coadd_objects;
grant all on dr007_coadd_objects to des_writer;
grant select on dr007_coadd_objects to des_reader;

drop public synonym dr007_coadd_objects_tag;
create public synonym dr007_coadd_objects_tag for des_admin.dr007_coadd_objects_tag;
grant all on dr007_coadd_objects_tag to des_writer;
grant select on dr007_coadd_objects_tag to des_reader;

drop public synonym parameter_definitions;
create public synonym parameter_definitions for des_admin.parameter_definitions;
grant all on parameter_definitions to des_writer;
grant select on parameter_definitions to des_reader;

drop public synonym coadd_truth_matches;
create public synonym coadd_truth_matches for des_admin.coadd_truth_matches;
grant all on coadd_truth_matches to des_writer;
grant select on coadd_truth_matches to des_reader;

drop public synonym new_dr007_coadd_objects_tag;
create public synonym new_dr007_coadd_objects_tag for des_admin.new_dr007_coadd_objects_tag;
grant all on new_dr007_coadd_objects_tag to des_writer;
grant select on new_dr007_coadd_objects_tag to des_reader;

