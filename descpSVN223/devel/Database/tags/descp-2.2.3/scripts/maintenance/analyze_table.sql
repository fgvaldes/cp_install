-- use the command: nohup sqlplus user/pass @name.sql >> analyze_table.lst 2>&1 &
spool analyze_table.lst;
set echo on;
set timing on;

exec dbms_stats.gather_table_stats(ownname=>'DES_ADMIN', tabname=>'DR007_COADD_OBJECTS_TAG', estimate_percent=>25, degree=>4);

exec dbms_stats.gather_table_stats(ownname=>'DES_ADMIN', tabname=>'DR007_COADD_OBJECTS', estimate_percent=>5, degree=>4);

exec dbms_stats.gather_table_stats(ownname=>'DES_ADMIN', tabname=>'OBJECTS_CURRENT', estimate_percent=>0.1, degree=>4);

exec dbms_stats.gather_table_stats(ownname=>'DES_ADMIN', tabname=>'OBJECTS_CURRENT', partname=>'PTMP_20100629120144_20051204', estimate_percent=>0.1, degree=>4);

exec dbms_stats.gather_table_stats(ownname=>'DES_ADMIN', tabname=>'PARAMETER_DEFINITIONS', estimate_percent=>100, degree=>2);

exec dbms_stats.gather_table_stats(ownname=>'DES_ADMIN', tabname=>'WL_ME_SHEAR', estimate_percent=>50, degree=>4);

exit;
