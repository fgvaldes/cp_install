connect "SYS"/"&&sysPassword" as SYSDBA
set echo on
spool /oracle/scripts/des//postDBCreation.log
connect "SYS"/"&&sysPassword" as SYSDBA
set echo on
create spfile='/oracle/10.2.0/dbs/spfiledes.ora' FROM pfile='/oracle/scripts/des/init.ora';
shutdown immediate;
connect "SYS"/"&&sysPassword" as SYSDBA
startup ;
alter user SYSMAN identified by "&&sysmanPassword" account unlock;
alter user DBSNMP identified by "&&dbsnmpPassword" account unlock;
select 'utl_recomp_begin: ' || to_char(sysdate, 'HH:MI:SS') from dual;
execute utl_recomp.recomp_serial();
select 'utl_recomp_end: ' || to_char(sysdate, 'HH:MI:SS') from dual;
host /oracle/10.2.0/bin/emca -config dbcontrol db -silent -DB_UNIQUE_NAME des -PORT 1521 -EM_HOME /oracle/10.2.0 -LISTENER LISTENER -SERVICE_NAME des -SYS_PWD &&sysPassword -SID des -ORACLE_HOME /oracle/10.2.0 -DBSNMP_PWD &&dbsnmpPassword -HOST desdb1.cosmology.uiuc.edu -LISTENER_OH /oracle/10.2.0 -LOG_FILE /oracle/scripts/des//emConfig.log -SYSMAN_PWD &&sysmanPassword;
spool /oracle/scripts/des//postDBCreation.log
