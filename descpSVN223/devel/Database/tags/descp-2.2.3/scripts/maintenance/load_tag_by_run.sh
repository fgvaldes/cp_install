#!/bin/bash
#
# incrementally loading dr007_coadd_objects_tag data by extracting
# from dr007_coadd_objects by run id.

DIR='/oracle/scripts/des/buildMV'
infile=$DIR/coadd_run.lst
basesql=$DIR/tag_base.sql
sqlfile=$DIR/load_tag_by_run.sql

source $DIR/des.bash

cat $infile | \
while read line
do
 cp $basesql $sqlfile
 runid=`echo $line | cut -c 1-27`
 echo `date` "Start loading run $runid" 
 echo "where run = '$runid';" >> $sqlfile
 echo "commit;" >> $sqlfile
 echo "exit;" >> $sqlfile

 sqlplus des_admin/passwd @$sqlfile 
 echo `date` "Finish loading run $line" 

done

exit

