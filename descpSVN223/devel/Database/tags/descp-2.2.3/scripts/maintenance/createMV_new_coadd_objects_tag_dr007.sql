-- script for 200 degree pieces (about 110 columns). Will eventually rename as 
-- dr007_coadd_objects_tag
create table new_dr007_coadd_objects_tag
  partition by range(ra)
    (partition ra100 values less than(100) tablespace desdr007, 
     partition ra200 values less than(200) tablespace desdr007, 
     partition ra300 values less than(300) tablespace desdr007, 
     partition ra310 values less than(310) tablespace desdr007, 
     partition ra311 values less than(311) tablespace desdr007, 
     partition ra312 values less than(312) tablespace desdr007, 
     partition ra313 values less than(313) tablespace desdr007, 
     partition ra314 values less than(314) tablespace desdr007, 
     partition ra315 values less than(315) tablespace desdr007, 
     partition ra316 values less than(316) tablespace desdr007, 
     partition ra317 values less than(317) tablespace desdr007, 
     partition ra318 values less than(318) tablespace desdr007, 
     partition ra319 values less than(319) tablespace desdr007, 
     partition ra320 values less than(320) tablespace desdr007, 
     partition ra321 values less than(321) tablespace desdr007, 
     partition ra322 values less than(322) tablespace desdr007, 
     partition ra323 values less than(323) tablespace desdr007, 
     partition ra324 values less than(324) tablespace desdr007, 
     partition ra325 values less than(325) tablespace desdr007, 
     partition ra326 values less than(326) tablespace desdr007, 
     partition ra327 values less than(327) tablespace desdr007, 
     partition ra328 values less than(328) tablespace desdr007, 
     partition ra329 values less than(329) tablespace desdr007, 
     partition ra330 values less than(330) tablespace desdr007, 
     partition ra331 values less than(331) tablespace desdr007, 
     partition ra332 values less than(332) tablespace desdr007, 
     partition ra333 values less than(333) tablespace desdr007, 
     partition ra334 values less than(334) tablespace desdr007, 
     partition ra335 values less than(335) tablespace desdr007, 
     partition ra336 values less than(336) tablespace desdr007, 
     partition ra337 values less than(337) tablespace desdr007, 
     partition ra338 values less than(338) tablespace desdr007, 
     partition ra339 values less than(339) tablespace desdr007, 
     partition ra340 values less than(340) tablespace desdr007, 
     partition ra341 values less than(341) tablespace desdr007, 
     partition ra342 values less than(342) tablespace desdr007, 
     partition ra343 values less than(343) tablespace desdr007, 
     partition ra344 values less than(344) tablespace desdr007, 
     partition ra345 values less than(345) tablespace desdr007, 
     partition ra346 values less than(346) tablespace desdr007, 
     partition ra347 values less than(347) tablespace desdr007, 
     partition ra348 values less than(348) tablespace desdr007, 
     partition ra349 values less than(349) tablespace desdr007, 
     partition ra350 values less than(350) tablespace desdr007, 
     partition ra351 values less than(351) tablespace desdr007, 
     partition ra352 values less than(352) tablespace desdr007, 
     partition ra353 values less than(353) tablespace desdr007, 
     partition ra354 values less than(354) tablespace desdr007, 
     partition ra355 values less than(355) tablespace desdr007, 
     partition ra356 values less than(356) tablespace desdr007, 
     partition ra357 values less than(357) tablespace desdr007, 
     partition ra358 values less than(358) tablespace desdr007, 
     partition ra359 values less than(359) tablespace desdr007, 
     partition ra360 values less than(360) tablespace desdr007, 
     partition rahigh values less than(MAXVALUE) tablespace desdr007) 
as
select 
cdd.tilename, 
cdd.id coadd_id, 
cob.coadd_objects_id, 
cob.zeropoint_g, cob.zeropoint_r, cob.zeropoint_i, 
cob.zeropoint_z, cob.zeropoint_y, 
cob.class_star_g, cob.class_star_r, cob.class_star_i, 
cob.class_star_z, cob.class_star_y, 
cob.theta_image, 
cob.a_image, cob.b_image, 
cob.kron_radius,
cob.mag_model_g, cob.mag_model_r, 
cob.mag_model_i, cob.mag_model_z, cob.mag_model_y, 
cob.magerr_model_g, cob.magerr_model_r, 
cob.magerr_model_i, cob.magerr_model_z, cob.magerr_model_y, 
cob.spread_model_g, cob.spread_model_r,
cob.spread_model_i, cob.spread_model_z, cob.spread_model_y, 
cob.spreaderr_model_g, cob.spreaderr_model_r,
cob.spreaderr_model_i, cob.spreaderr_model_z, cob.spreaderr_model_y, 
pho.photoz, pho.id photoz_id, pho.photozerr, pho.flag photoz_flag, 
cob.mag_psf_g, cob.mag_psf_r, cob.mag_psf_i,
cob.mag_psf_z, cob.mag_psf_y,
cob.magerr_psf_g, cob.magerr_psf_r, cob.magerr_psf_i,
cob.magerr_psf_z, cob.magerr_psf_y,
cob.mag_auto_g, cob.mag_auto_r, cob.mag_auto_i,
cob.mag_auto_z, cob.mag_auto_y,
cob.magerr_auto_g, cob.magerr_auto_r, cob.magerr_auto_i,
cob.magerr_auto_z, cob.magerr_auto_y,
cob.mag_aper2_g, cob.mag_aper2_r, cob.mag_aper2_i,
cob.mag_aper2_z, cob.mag_aper2_y,
cob.magerr_aper2_g, cob.magerr_aper2_r, cob.magerr_aper2_i,
cob.magerr_aper2_z, cob.magerr_aper2_y,
cob.mag_aper3_g, cob.mag_aper3_r, cob.mag_aper3_i, 
cob.mag_aper3_z, cob.mag_aper3_y, 
cob.magerr_aper3_g, cob.magerr_aper3_r, cob.magerr_aper3_i, 
cob.magerr_aper3_z, cob.magerr_aper3_y, 
cob.mag_aper6_g, cob.mag_aper6_r, cob.mag_aper6_i, 
cob.mag_aper6_z, cob.mag_aper6_y, 
cob.magerr_aper6_g, cob.magerr_aper6_r, cob.magerr_aper6_i, 
cob.magerr_aper6_z, cob.magerr_aper6_y, 
cob.flux_radius_g, cob.flux_radius_r, cob.flux_radius_i, 
cob.flux_radius_z, cob.flux_radius_y, 
cob.flags_g, cob.flags_r, cob.flags_i, 
cob.flags_z, cob.flags_y, 
wls.id wl_id, wls.shear_flags, wls.shear1, wls.shear2, 
wls.nu shear_signal_to_noise, mat.truth_id,
cob.ra, cob.dec
from coadd cdd, coadd_objects cob, photo_z pho, wl_me_shear wls, 
     coadd_truth_matches mat
where cdd.run = 'not applicable'
and   cdd.tilename = 'not applicable'
and   cob.imageid_g = cdd.id
and   cob.coadd_objects_id = pho.id
and   wls.coadd_object_id = cob.coadd_objects_id
and   cob.coadd_objects_id = mat.coadd_objects_id (+);

alter table new_dr007_coadd_objects_tag add (
cx binary_double,
cy binary_double,
cz binary_double,
htmid number(32,0));

alter table new_dr007_coadd_objects_tag
add constraint pk_new_dr007_coadd_objects_tag primary key (coadd_objects_id);

alter table new_dr007_coadd_objects_tag modify (
flags_g number(3),
flags_y number(3),
mag_model_g number(8,5),
mag_model_r number(8,5),
mag_model_i number(8,5),
mag_model_z number(8,5),
mag_model_y number(8,5),
class_star_g number(8,5),
class_star_r number(8,5),
class_star_i number(8,5),
class_star_z number(8,5),
class_star_y number(8,5),
spread_model_g number(8,5),
spread_model_r number(8,5),
spread_model_i number(8,5),
spread_model_z number(8,5),
spread_model_y number(8,5));

alter table new_dr007_coadd_objects_tag
pctfree 0;

-- analyze table
exec dbms_stats.gather_table_stats(ownname=>'DES_ADMIN', tabname=>'NEW_DR007_COADD_OBJECTS_TAG', estimate_percent=>5, degree=>4);

-- create index
create index new_cob007_tag_radec_idx on new_dr007_coadd_objects_tag(ra,dec) local
tablespace desdr007
parallel 4;

create index new_cob007_tag_magmodelg_idx on new_dr007_coadd_objects_tag(mag_model_g) local
tablespace desdr007
parallel 4;

create index new_cob007_tag_magmodelr_idx on new_dr007_coadd_objects_tag(mag_model_r) local
tablespace desdr007
parallel 4;

create index new_cob007_tag_magmodeli_idx on new_dr007_coadd_objects_tag(mag_model_i) local
tablespace desdr007
parallel 4;

create index new_cob007_tag_magmodelz_idx on new_dr007_coadd_objects_tag(mag_model_z) local
tablespace desdr007
parallel 4;

create index new_cob007_tag_magmodely_idx on new_dr007_coadd_objects_tag(mag_model_y) local
tablespace desdr007
parallel 4;

create index new_cob007_tag_classstari_idx on new_dr007_coadd_objects_tag(class_star_i) local
tablespace desdr007
parallel 4;

create index new_cob007_tag_spreadmdli_idx on new_dr007_coadd_objects_tag(spread_model_i) local
tablespace desdr007
parallel 4;

-- set default parallel degree
alter table new_dr007_coadd_objects_tag parallel (degree 4);

-- grant the user access to table
drop public synonym new_dr007_coadd_objects_tag;
create public synonym new_dr007_coadd_objects_tag for des_admin.new_dr007_coadd_objects_tag;
grant all on new_dr007_coadd_objects_tag to des_writer;
grant select on new_dr007_coadd_objects_tag to des_reader;

