-- run this script as des_admin
drop table shapelet_objects purge;
create table shapelet_objects(
object_id		number(11) not null,
mag_auto		binary_float,
flags			number(10),
sigma		     	binary_float,
coeff1			binary_float,
coeff2			binary_float,
coeff3			binary_float,
coeff4			binary_float,
coeff5			binary_float,
coeff6			binary_float)
storage (initial 100K);

drop public synonym shapelet_objects;
create public synonym shapelet_objects for des_admin.shapelet_objects;
grant all on shapelet_objects to des_writer;
grant select on shapelet_objects to des_reader;

