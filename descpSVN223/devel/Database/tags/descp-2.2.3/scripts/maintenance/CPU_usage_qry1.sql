select sum(t.value)
from v$session s, v$statname n, v$sesstat t
where s.username = 'PIPELINE'
and n.statistic# = t.statistic#
and n.name like '%cpu%'
and s.program like '%sql%'
and t.sid = s.sid
/
