Procedure to Set Up Primary/Standby database

P = Primary
S = Standby

  Site	To Do
------------------------------------------------
*   P   Shutdown EM
*   P	Prepare initdes.ora (in $ORACLE_HOME/dbs)
	These parameters must be set up:
          db_name, db_unique_name, log_archive_config, log_archive_dest_1,
	  log_archive_dest_2, log_archive_dest_state_1, 
          log_archive_dest_state_2, remote_login_passwordfile,
          log_archive_format, log_archive_max_process, fal_server,
          fal_client, standby_file_management,
          compatible (must be > 11.0.0.0)
*   P	Start instance by pfile
*   P   Create spfile from pfile
*   P   Create standby logfiles, need # of redo logfiles + 1 with same size.
          check v$log, v$standby_log, v$logfile
*   P	Modify listener.ora, tnsnames.ora, and sqlnet.ora in 
        $ORACLE_HOME/network/admin
        listener.ora and tnsnames.ora must have service names: prdes and stdes
        listener.ora may have timeout=300s (to avoid timeout errors),
        sqlnet.ora may have trace=on timeout=301s or over.
*   P   restart lsnrctl
*   P   Run rman to delete expired archived log
          rman target /
          rman> crosscheck archivelog all;
          rman> delete expired archivelog all;
*   P   Create a backup for standby database using 
          /oracle/scripts/des/des_prim_rman.sh (took 6 hours for 1.2 TB db)
*   P   Copy all backup files and newly generated archived logfiles to the 
          standby server at same location. (took 12 hours for 1.2 TB db)
*   P   Copy $ORACLE_HOME/dbs/orapwdes to the standby server at same location.

*   S   Shutdown EM
*   S	Prepare initdes.ora (in $ORACLE_HOME/dbs)
	These parameters must be set up:
          db_name, db_unique_name, log_archive_config, log_archive_dest_1,
	  log_archive_dest_2, log_archive_dest_state_1, 
          log_archive_dest_state_2, remote_login_passwordfile,
          log_archive_format, log_archive_max_process, fal_server,
          fal_client, standby_file_management,
          compatible (must be > 11.0.0.0)
          standby_file_management='AUTO'
          db_recovery_file_dest_size (must be >= 20GB)
*   S	Modify listener.ora, tnsnames.ora (in $ORACLE_HOME/network/admin)
*   S   Ensure the file system for $ORACLE_HOME/flash_recovery_area has >= 20GB
*   S   Ensure the file system for $ORACLE_HOME/redo has >= 20GB
*   S   Create all sub-directories for /oracle/data/des, /oracle/data/partdes,
          /oracle/data/des_temp, /oracle/redo/des, /oracle/archive/des 
*   S	Start instance by pfile
*   S   Create spfile from pfile
*   S   Create redo logfiles and standby logfiles exactly same as the 
          primary db, check v$log, v$standby_log, v$logfile
*   S   Shutdown database, then restart nomount

*   P   Duplicate the database using the command
          rman target sys/pass@prdes auxiliary sys/pass@stdes 
            cmdfile=duplicate_db_rman.cmd > recover_mmddyy.log
          (It took 6 hours for 1.2 TB db)

*   S   The standby database should be in the mount state after duplication.
*   S   Check v$log, v$standby_log, v$logfile
          If any logfile missing, add it. If any logfile's name is wrong,
          rename it.
          On standby database, redo logs are not used and updated, only slogs 
          are used
*   S   Start auto apply in mount mode (not in a read only mode):
          sql> alter database recover managed standby database using current
               logfile disconnect from session;
*   S   If a temp file has errors on SCN#, remove temp files from 
          /oracle/data. The next restart db will recreate a good temp files.

*   P   Force a logfile switch
          sql> alter system switch logfile;

*   S   Monitor the log transfer and apply from alter.log
*   S   Open database in Read Only mode
          sql> alter database recover managed standby database cancel;
          sql> alter database open read only;
          sql> recover managed standby database disconnect 
               using current logfile;

*   P   Rebuild EM:
        cd $ORACLE_HOME/bin
        ./emca -deconfig dbcontrol db -repos drop
        ./emca -config dbcontrol db -repos create
  
*   p   Put the primary database in force logging mode
        alter database force logging

Lesson learned

12/07/2009 built Fermi Standby
* Don't set up the relevant parameters on Primary before the backup and 
  file transfer are complete, because the database is large, these two steps 
  take long time. As soon as those parameters are set up, error messages 
  "connection lost contact" will appear until the standby db is built and 
  opened.
* For a 3.2 TB db, the backup took 24 hours, the restore took 12 hours, the
  file transfer took 10 - 20 hours depending on parallelism and tools used.
* Before doing RMAN backup, must do crosscheck on archived logs, and delete
  expired archived logs. This will make the archive-log backup set much smaller.
* During the process, should try to reduce the database changes to the minimum.
  This will make the archive-log backup set much smaller.
* After the restore process, all redo logs and standby logs went to flash 
  recovery area and registered as so in the v$logfile.
  To fix standby logs, write a script to do this:
  (1) cp /flash-recovery-area/DB/onlinelog/logfilename.log /oracle/redo/des/slog??.rdo
  (2) alter system set standby_file_management='MANUAL' scope=both;
  (3) alter database rename file '/flash-recovery-area/DB/onlinelog/logfilename.log' to '/oracle/redo/des/slog01.rdo';
  To fix redo logs, write a script to do this:
  (1) alter database drop logfile group 1 (up to 12)
  (2) if some logfile groups cannot be dropped, shutdown db and restart will 
release those logfile groups.
  (3) manually remove redo01.log -> redo12.log in /oracle/redo/des.
  (4) alter database add logfile '/oracle/redo/des/redo01.log' size 400m; (up
to /oracle/redo/des/redo12.log).
  (5) if some redo log is still associated with flash-recover files, run command
      alter database rename file 'flash-recovery-area/DB/onlinelog/logfilename.log' to '/oracle/redo/des/redo??.log) (?? here is the logfile not assigned)

