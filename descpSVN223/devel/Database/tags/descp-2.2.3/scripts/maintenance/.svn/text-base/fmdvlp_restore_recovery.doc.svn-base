-- This document summarizes the procedure we have done to restore and 
-- recover the Fermi development standby database.

-- Description of the problem: fmdvlp has not applied the redo logs 
-- for at lease 9 weeks. The database is behind of the primary database 
-- for thousands redo logs.

-- Step 1:
-- transfer the latest backup files from UIUC's stanby server to Fermi.
-- use symbolic links to make the location consistent for the backup files.
$ cd /decam-devdb/dbs_nas_backup01
$ mkdir backup
$ ln -s /decam-devdb/dbs_nas_backup01/backup /oracle/backup 
$ mkdir backup1
$ ln -s /decam-devdb/dbs_nas_backup01/backup1 /oracle/backup1

-- Step 2:
-- remove the previous backup
$ rm -rf /oracle/backup/dvlp.*
$ rm -rf /oracle/backup1/dvlp.*

-- Step 3:
$ scp -rp oracle@deslogin.cosmology.illinois.edu:/oracle/backup/dvlp.092610 /oracle/backup
$ scp -rp oracle@deslogin.cosmology.illinois.edu:/oracle/backup1/dvlp.092610 /oracle/backup1
$ scp -rp oracle@deslogin.cosmology.illinois.edu:/oracle/backup/readonly.061710 /oracle/backup

-- Step 4:
-- to stop Redo Apply, and shutdown the database
$ sqlplus / as sysdba
  SQL> alter database recover managed standby database cancel;
  SQL> shutdown immediate;

-- Step 5:
-- to start database in the nomount mode
  SQL> startup nomount;

-- Step 6
-- use RMAN to restore the control files
$ rman target / catalog rman/passwd@rcatd  
-- change the association of the backup
RMAN> change backup for db_unique_name stdvlp reset db_unique_name;
RMAN> restore controlfile;

-- Step 7:
-- change database into the mount mode
  SQL> alter database mount;

-- Step 8:
-- use RMAN to restore database files and readonly tablespace files
RMAN> restore database;
RMAN> restore tablespace 'DESDR';
RMAN> recover tablespace 'DESDR';
RMAN> recover database;
RMAN> exit;

-- Step 9:
-- start redo apply
$ sqlplus / as sysdba
  SQL> alter database recover managed standby database using current logfile disconnect from session;
  -- if everything goes OK, switch the database to the read-only mode.
  SQL> alter database recover managed standby database cancel;
  SQL> alter database open read only;
  SQL> recover managed standby database disconnect using current logfile;
  SQL> exit;

