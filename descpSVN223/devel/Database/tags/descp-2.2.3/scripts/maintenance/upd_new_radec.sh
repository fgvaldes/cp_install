#!/bin/bash
#
# Script for updating the values in ra_num and dec_num columns
# using the values in alpha_j2000 and delta_j2000
#
# Dated : Nov. 02, 2006

DIR='/oracle/scripts/des'
TBLNAME='objects'
INCREMENT=1000000
MINID=2000000
MAXID=3000000
STOPID=1100000000
SQLFILE='/oracle/scripts/des/upd_new_radec.sql'
source $DIR/des.bash

while [ $MINID -lt $STOPID ] ; do 
  cp /dev/null $SQLFILE
  echo "update objects" >> $SQLFILE
  echo "set ra_num = alpha_j2000, " >> $SQLFILE
  echo "dec_num = delta_j2000 " >> $SQLFILE
  echo "where object_id >= $MINID " >> $SQLFILE
  echo "and   object_id < $MAXID; " >> $SQLFILE
  echo "commit; " >> $SQLFILE
  echo "exit; " >> $SQLFILE
  cat $SQLFILE
  sqlplus des_admin/passwd @$SQLFILE
  MINID=$MAXID
  MAXID=`expr $MINID + $INCREMENT`
done

exit
