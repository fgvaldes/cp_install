set pagesize 1000;
select a.sid, a.serial#, a.username, a.osuser,  
       to_char(a.sql_exec_start, 'dd-mon-yy hh24:mi:ss') exec_start, s.piece, s.sql_text
from v$session a, v$sqltext s
where a.sql_address = s.address
and   a.username not like 'SYS%'
order by a.sid, a.serial#, s.piece;
