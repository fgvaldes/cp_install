create table dc5sim_usnob_oct11
as select * from dc5sim_usnob
where datafile_name = 'NA';

insert into dc5sim_usnob_oct11
select a.dc5sim_usnob_id, a.star_galaxy_id, a.class, 
       a.ra_dbl+(a.mura*(55835-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(55835-51544)*0.001/(3600*365.25)),
       a.u_mag, g_mag, r_mag, i_mag, z_mag, y_mag, 
       a.sra, a.sde, a.mura, a.mudec, 
       a.ra_dbl+(a.mura*(55835-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(55835-51544)*0.001/(3600*365.25)),
       datafile_name
from dc5sim_usnob a;
     
create table dc5sim_usnob_oct12
as select * from dc5sim_usnob
where datafile_name = 'NA';

insert into dc5sim_usnob_oct12
select a.dc5sim_usnob_id, a.star_galaxy_id, a.class, 
       a.ra_dbl+(a.mura*(56203-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(56203-51544)*0.001/(3600*365.25)),
       a.u_mag, g_mag, r_mag, i_mag, z_mag, y_mag, 
       a.sra, a.sde, a.mura, a.mudec, 
       a.ra_dbl+(a.mura*(56203-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(56203-51544)*0.001/(3600*365.25)),
       datafile_name
from dc5sim_usnob a;
     
create table dc5sim_usnob_oct13
as select * from dc5sim_usnob
where datafile_name = 'NA';

insert into dc5sim_usnob_oct13
select a.dc5sim_usnob_id, a.star_galaxy_id, a.class, 
       a.ra_dbl+(a.mura*(56570-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(56570-51544)*0.001/(3600*365.25)),
       a.u_mag, g_mag, r_mag, i_mag, z_mag, y_mag, 
       a.sra, a.sde, a.mura, a.mudec, 
       a.ra_dbl+(a.mura*(56570-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(56570-51544)*0.001/(3600*365.25)),
       datafile_name
from dc5sim_usnob a;
     
create table dc5sim_usnob_oct14
as select * from dc5sim_usnob
where datafile_name = 'NA';

insert into dc5sim_usnob_oct14
select a.dc5sim_usnob_id, a.star_galaxy_id, a.class, 
       a.ra_dbl+(a.mura*(56937-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(56937-51544)*0.001/(3600*365.25)),
       a.u_mag, g_mag, r_mag, i_mag, z_mag, y_mag, 
       a.sra, a.sde, a.mura, a.mudec, 
       a.ra_dbl+(a.mura*(56937-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(56937-51544)*0.001/(3600*365.25)),
       datafile_name
from dc5sim_usnob a;
     
create table dc5sim_usnob_oct15
as select * from dc5sim_usnob
where datafile_name = 'NA';

insert into dc5sim_usnob_oct15
select a.dc5sim_usnob_id, a.star_galaxy_id, a.class, 
       a.ra_dbl+(a.mura*(57304-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(57304-51544)*0.001/(3600*365.25)),
       a.u_mag, g_mag, r_mag, i_mag, z_mag, y_mag, 
       a.sra, a.sde, a.mura, a.mudec, 
       a.ra_dbl+(a.mura*(57304-51544)*0.001/(3600*365.25*cos(3.14159*a.dec_dbl/180))),
       a.dec_dbl+(a.mudec*(57304-51544)*0.001/(3600*365.25)),
       datafile_name
from dc5sim_usnob a;
     
