#!/bin/bash
#
# Creates dynamic sqlldr statements for loading dc4_truth galaxy redshift file

DIR='/oracle/scripts/des/load_dc4_redshift'
DATADIR='/home/bcs/TrueZ_DC4'
outfile=$DIR/galaxy.log
infile=$DIR/galaxy_in.lst
cp /dev/null $outfile

source $DIR/des.bash

cat $infile | \
while read line
do
 datafile=`echo $line | cut -c 21-101`
 echo "datafile is " $datafile
 ctrfile=$DIR/$datafile'.ctl'
 badfile=$DIR/$datafile'.bad'
 logfile=$DIR/$datafile'.log'
 parfile=$DIR/'common.par'

 echo `date` "Start loading $datafile" >> $outfile
 cp /dev/null $ctrfile


 echo "Load data" >>  $ctrfile
 echo "infile '$line'" >>  $ctrfile
 echo "append" >>  $ctrfile
 echo "into table dc4_truth_redshift" >>  $ctrfile
 echo "fields terminated by ','" >>  $ctrfile
 echo "trailing nullcols(" >>  $ctrfile
 echo "star_galaxy_id  integer external," >> $ctrfile
 echo "redshift        decimal external," >> $ctrfile
 echo "datafile_name   CONSTANT '$datafile')" >> $ctrfile
 
 sqlldr parfile=$parfile control=$ctrfile log=$logfile bad=$badfile
 echo `date` "Finish loading $datafile" >> $outfile

 done

exit

