#!/bin/bash
#
# Script for automatically adding an Oracle data file when needed
#
# Dated : March 27, 2006

DIR='/oracle/scripts/des'
DATADIR='/oracle/data/des1'
THRESHOLD=10000000
SQLFILE='/oracle/scripts/des/add_datafile.sql'
RMANFILE='/oracle/scripts/des/resync_catalog.cmd'
source $DIR/des.bash

# find the size of the last data file for des
# LAST=`ls -1s $DATADIR | grep des_[0,1,2,3,4,5,6,7,8,9] | tail -1`
LAST=`ls -1s $DATADIR/des_???.dbf | grep des_[0,1,2,3,4,5,6,7,8,9] | tail -1`
#echo "LAST is " $LAST
SIZE=`echo $LAST | awk -F" "  '{ print $1 }'`
echo "SIZE is " $SIZE
FILE=`echo $LAST | awk -F" "  '{ print $2 }'`
echo "FILE is " $FILE
FILENO=`echo $FILE | cut -c23-25`
echo "FILENO is " $FILENO
if [ $FILENO -lt "09" ] ; then
   NEWNO=0`expr $FILENO + 1`
   echo "NEWNO1 is " $NEWNO
else
   NEWNO=`expr $FILENO + 1`
   echo "NEWNO2 is " $NEWNO
fi
if [ $SIZE -gt $THRESHOLD ] ; then
   echo "Des data files are getting full. A new data file will be created."
   cp /dev/null $SQLFILE
   echo "connect / as sysdba" >> $SQLFILE
   echo "alter tablespace des" >> $SQLFILE
   echo "add datafile '$DATADIR/des_$NEWNO.dbf'" >> $SQLFILE
   echo "size 1000M autoextend on next 500M maxsize unlimited;" >> $SQLFILE
   echo "exit" >> $SQLFILE
   cat $SQLFILE
   sqlplus /nolog @$SQLFILE
   rman target / catalog rman/rmanmgr@rcat cmdfile=$RMANFILE
   mail -s"A new datafile has been added to desdb" desdm-db@cosmology.illinois.edu < $SQLFILE
fi

exit
