#!/bin/bash
#
# Script for updating the values in ra_num and dec_num columns in table image
# using the values of ra and dec
# range of id in image: 960 to 9318655 
#
# Dated : Nov. 05, 2006

DIR='/oracle/scripts/des'
TBLNAME='image'
INCREMENT=1000000
MINID=0
MAXID=1000000
STOPID=11000000
SQLFILE='/oracle/scripts/des/upd_new_radec.sql'
source $DIR/des.bash

while [ $MINID -lt $STOPID ] ; do 
  cp /dev/null $SQLFILE
  echo "update image" >> $SQLFILE
  echo "set ra_num = ra, " >> $SQLFILE
  echo "dec_num = dec " >> $SQLFILE
  echo "where id >= $MINID " >> $SQLFILE
  echo "and   id < $MAXID " >> $SQLFILE
  echo "and   ra >= 0 " >> $SQLFILE
  echo "and   ra <= 360; " >> $SQLFILE
  echo "commit; " >> $SQLFILE
  echo "exit; " >> $SQLFILE
  cat $SQLFILE
  sqlplus des_admin/passwd @$SQLFILE
  MINID=$MAXID
  MAXID=`expr $MINID + $INCREMENT`
done

exit
