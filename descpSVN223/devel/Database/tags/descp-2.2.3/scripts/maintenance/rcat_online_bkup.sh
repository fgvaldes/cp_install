#!/bin/bash
#
# RMAN online Backup run (not using a catalog) from the recovery-catalog 
# database
# Date: Nov. 11, 2009

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/rcat'
BUDIR='/oracle/backup/rcat.'$TODAY
LOG=$DIR/rcat_online_bkup.log
cp /dev/null $LOG
mkdir $BUDIR
source $DIR/rcat.bash

cp /dev/null $DIR/rcat_online_bkup.cmd
echo 'run {' >> $DIR/rcat_online_bkup.cmd
echo 'allocate channel t1 type disk MAXPIECESIZE = 5G; ' >> $DIR/rcat_online_bkup.cmd
echo "backup database format '$BUDIR/RCAT_%U'; " >> $DIR/rcat_online_bkup.cmd
echo "backup current controlfile format '$BUDIR/CTRFl_%U'; " >> $DIR/rcat_online_bkup.cmd
echo 'release channel t1;} ' >> $DIR/rcat_online_bkup.cmd


rman target / nocatalog cmdfile=$DIR/rcat_online_bkup.cmd >> $LOG

line_no=0
line_no=`grep 'ORA-' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"RCAT backup on desar failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

line_no=0
line_no=`grep 'fail' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"RCAT backup on desar failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

mail -s"RCAT backup on desar is completed successfully" desdm-db@cosmology.illinois.edu < $LOG
echo suceeded > $BUDIR/.status
exit

