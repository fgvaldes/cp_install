#!/bin/bash
#
# rcatd offline backup script create on 04/18/2009 by Dora Cai

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/rcatd'
DATADIR='/oracle/oradata/rcatd'
DBSDIR='/oracle/11.1.0/dbs'
NETDIR='/oracle/11.1.0/network/admin'
BUDIR='/oracle/backup/rcatd.'$TODAY
mkdir $BUDIR
cp /dev/null $DIR/rcatd_backup.log
source $DIR/rcatd.bash

# shutdown database rcatd using immediate option
sqlplus / as sysdba @$DIR/shutora_imm.sql
if test $? -eq 0 ; then
   echo `date`: RCATD database is shut down. >> $DIR/rcatd_backup.log
else 
   echo `date`: Fail to shutdown rcatd database immediately >> $DIR/rcatd_backup.log
   echo "-----------" >> $DIR/rcatd_backup.log
   mail -s"RCATD offline backup on destest failed" desdm-db@cosmology.illinois.edu <$DIR/rcatd_backup.log
   exit
fi

echo `date`: RCATD offline backup is started >> $DIR/rcatd_backup.log
cp -p $DBSDIR/* $BUDIR/.
cp -p $NETDIR/*.ora $BUDIR/.
cp -p $DATADIR/*.* $BUDIR/.
if test $? -eq 0 ; then
   echo `date`: RCATD all of files are copied. >> $DIR/rcatd_backup.log
   sqlplus / as sysdba @$DIR/startora.sql
   if test $? -eq 0; then
      echo `date`: RCATD database is started up. >> $DIR/rcatd_backup.log
   else
      echo `date`: RCATD database cannot be started up. >> $DIR/rcatd_backup.log
      mail -s"RCATD offline backup on destest failed" desdm-db@cosmology.illinois.edu <$DIR/rcatd_backup.log
      exit
   fi
else # copy failed
   echo `date`: Fail to copy oracle database files >> $DIR/rcatd_backup.log
   mail -s"RCATD offline backup on destest failed" desdm-db@cosmology.illinois.edu <$DIR/rcatd_backup.log
   exit
fi

SESSION=0
SESSION=`ps -ef | grep "_rcatd" | wc -l`
if [ $SESSION -lt 15 ] ; then
    echo "RCATD database is failed to startup. " >> $DIR/rcatd_backup.log
    mail -s"RCATD offline backup failed" desdm-db@cosmology.illinois.edu <$DIR/rcatd_backup.log
    exit
fi

echo `date`: RCATD database backup complete. >> $DIR/rcatd_backup.log
mail -s"RCATD Offline backup on destest successful" desdm-db@cosmology.illinois.edu <$DIR/rcatd_backup.log
exit
