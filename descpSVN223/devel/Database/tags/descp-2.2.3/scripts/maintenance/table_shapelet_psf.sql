-- run this script as des_admin
drop table shapelet_psf purge;
create table shapelet_psf(
object_id		number(11) not null,
mag_auto		binary_float,
flags			number(10),
sigma		     	binary_float,
coeff1			binary_float,
coeff2			binary_float,
coeff3			binary_float,
coeff4			binary_float,
coeff5			binary_float,
coeff6			binary_float,
coeff7			binary_float,
coeff8			binary_float,
coeff9			binary_float,
coeff10			binary_float,
coeff11			binary_float,
coeff12			binary_float,
coeff13			binary_float,
coeff14			binary_float,
coeff15			binary_float)
storage (initial 100K);

drop public synonym shapelet_psf;
create public synonym shapelet_psf for des_admin.shapelet_psf;
grant all on shapelet_psf to des_writer;
grant select on shapelet_psf to des_reader;

