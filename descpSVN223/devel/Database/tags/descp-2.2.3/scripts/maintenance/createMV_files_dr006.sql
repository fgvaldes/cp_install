create table dr006_files 
tablespace desdr
as
select loc.id, loc.project, loc.run, 
loc.nite, loc.band, loc.filetype, loc.filename, 
loc.filedate, loc.filesize, loc.filesize_gz, loc.filesize_fz,
loc.tilename, loc.ccd, img.exposureid image_exposureid, 
loc.exposurename file_exposure_name, 
img.airmass image_airmass, img.exptime image_exptime, img.device_id, 
img.gaina, img.rdnoisea, img.gainb, img.rdnoiseb, img.equinox, img.wcsdim, 
img.ctype1 image_ctype1, img.cunit1 image_cunit1, 
img.crval1 image_crval1, img.crpix1 image_crpix1, 
img.cd1_1 image_cd1_1, img.cd1_2 image_cd1_2,
img.pv1_0, img.pv1_1, img.pv1_2, img.pv1_3, img.pv1_4, img.pv1_5,
img.pv1_6, img.pv1_7, img.pv1_8, img.pv1_9, img.pv1_10,
img.ctype2 image_ctype2, img.cunit2 image_cunit2, 
img.crval2 image_crval2, img.crpix2 image_crpix2, 
img.cd2_1 image_cd2_1, img.cd2_2 image_cd2_2,
img.pv2_0, img.pv2_1, img.pv2_2, img.pv2_3, img.pv2_4, img.pv2_5,
img.pv2_6, img.pv2_7, img.pv2_8, img.pv2_9, img.pv2_10,
img.skybrite, img.skysigma, img.elliptic, img.fwhm, 
img.scampnum, img.scampchi, img.scampflg, 
img.naxis1 image_naxis1, img.naxis2 image_naxis2, img.nextend image_nextend,
img.ra image_ra, img.dec image_dec, img.parentid image_parentid,
img.pcount, img.gcount, img.bzero, img.bscale, img.bunit,
img.ccdname, img.detsec, img.ccdsum, img.datasec, img.trimsec, 
img.ampseca, img.ampsecb, img.biasseca, img.biassecb, img.saturata, 
img.saturatb, img.ltm1_1, img.ltm2_2, img.ltv1, img.ltv2, 
img.radesys image_radesys, img.wcsaxes, 
cdd.radecequin, cdd.wcstype, 
cdd.ctype1 coadd_ctype1, cdd.cunit1 coadd_cunit1, 
cdd.crval1 coadd_crval1, cdd.crpix1 coadd_crpix1, 
cdd.cd1_1 coadd_cd1_1, cdd.cd1_2 coadd_cd1_2,
cdd.ctype2 coadd_ctype2, cdd.cunit2 coadd_cunit2, 
cdd.crval2 coadd_crval2, cdd.crpix2 coadd_crpix2, 
cdd.cd2_1 coadd_cd2_1, cdd.cd2_2 coadd_cd2_2,
cdd.ellipticity coadd_ellipticity, cdd.fwhm coadd_fwhm, 
cdd.naxis1 coadd_naxis1, cdd.naxis2 coadd_naxis2, 
cdd.nextend coadd_nextend,
cdd.ra coadd_ra, cdd.dec coadd_dec,
clg.catalogname, clg.catalogtype, 
clg.exposureid catalog_exposureid, clg.objects catalog_objects,
clg.parentid catalog_parentid,
exp.exposurename, exp.exposuretype, exp.telradec,
exp.telequin, exp.ha, exp.zd, exp.airmass exposure_airmass, 
exp.telfocus, exp.object exposure_objects,
exp.observer, exp.propid, exp.detector, 
exp.detsize, exp.telescope, exp.observatory, 
exp.latitude, exp.longitude, exp.altitude,
exp.timesys, exp.date_obs, exp.time_obs, 
exp.mjd_obs, exp.exptime exposure_exptime, exp.expreq,
exp.darktime, exp.epoch, exp.windspd,
exp.winddir, exp.ambtemp, exp.humidity,
exp.pressure, exp.skyvar, exp.fluxvar, 
exp.dimmsee, exp.photflag, exp.imagehwv,
exp.imageswv, exp.naxis1 exposure_naxis1, exp.naxis2 exposure_naxis2,
exp.nextend exposure_nextend, exp.telra, exp.teldec, 
exp.obstype,
exp.expnum, exp.proctype, exp.prodtype, exp.proposer,exp.instrument,
exp.pixscal1, exp.pixscal2, exp.camshut, exp.radesys exposure_radesys, exp.filtid, 
exp.filpos, exp.telstat, exp.domestat, exp.domeshut, exp.domelamp,
exp.moonangle 
from location loc, image img, coadd cdd, catalog clg, exposure exp
where loc.run = 'not applicable'
and   loc.id = cdd.id
and   loc.id = img.id
and   loc.id = clg.id
and   loc.id = exp.id;


