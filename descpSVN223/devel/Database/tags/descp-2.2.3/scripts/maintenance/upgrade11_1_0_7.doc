* check timezone on all servers 
  select version from v$timezone_file;

  if it is version 4, no action is required.

* switch logfiles on primary, then shutdown primary, emctl, and lsnrctl, and do offline backup

* cancel redo apply and shutdown standby db, em, listner

* backup oracle software on all servers

* identify the Oracle database installation on both servers
/oracle/11.1.0/OPatch/opatch lsinventory -all

* download the patch set on both servers 
/oracle/staging

* use unzip command to unzip and extract the patch set files on both servers

* on dber, xhost to add both remote servers

* connect to standby as oracle user with correct DISPLAY 
  ssh -Y oracle@standby_searver 

* on standby, check ORACLE_HOME, ORACLE_SID, PATH are correct.

* on standby, install the patch 
  cd /patchset_dir/Disk1 
  ./runInstaller

* on standby, start listner, database mount and start redo apply
  startup mount;
  alter database recover managed standby database using current logfile 
    disconnect from session;

* connect to primary as oracle user with correct DISPLAY 
  ssh -Y oracle@primary_searver 

* add OPatch to path on primary
  export PATH=$ORACLE_HOME/OPatch:$PATH

* on primary, check ORACLE_HOME, ORACLE_SID, PATH are correct.

* on primary, install the patch 
  cd /patchset_dir/Disk1 
  ./runInstaller

* on primary, start listner

* on primary, run oracle database upgrade assistant
  $ $ORACLE_HOME/bin/dbua

* after upgrade, primary database is opened

* install patch-set and upgrade the recovery catalog database

* upgrade recovery catalog
  $ rman catalog user/pass@rcat
  RMAN> UPGRADE CATALOG;

* if a server, either primary or standby has other database instance (other
  than primary or standby) running, those databases must be upgraded by 
  running dbua. For example, the rcat db on moria. Otherwise, that database 
  cannot be opened.

Note:
* Before upgrading primary, a question "Oracle recommand to sync the standby
  db before continue. Are you sure you want to continue?". Answer "Yes".

* During the upgrade, if there is an error like "/oracle/bin/filename is busy",
  use "mv filename filename.backup", then retry the action.

* Encounter an error when upgrading Oracle OLAP API: "ORA-04063: package bpdy 
  'WKSYS.WKDS_ADM' has errors.", "ORA-06508: PL/SQL: could not find program unit
  being called: 'WKSYS.WKDS_ADM'", "ORA-06512: at line 248". Ignored the errors
  and continued.

* After upgrade the primary db, the database is open, but there are extra
  13 deamon processes like ora_pmon_DBUA0, ora_ckpt_DBUA0, ......, etc. Used
  kill -9 to kill those processes.

* The primary db was terminated after up and runing for 30 minutes. Got the
  following errors:

ORA-27103: internal error
Linux-x86_64 Error: 11: Resource temporarily unavailable
Additional information: -1
Additional information: 1
MMAN (ospid: 6405): terminating the instance due to error 27103
Instance terminated by MMAN, pid = 6405

  Searched Oracle's Metalink. This is caused by Oracle bug 7272646: ORA-27103 
  when memory_target > 3G.

  To fix the bug, download the interim patch 7272646 and applied to all 
  database by the following steps:

  (1) check ORACLE_HOME, ORACLE_SID, PATH are correct.
  (2) unzip the patch file
  (3) shutdown database, emctl, lsnrctl
  (4) cd patch_directory
  (5) opatch apply

* After upgrade the recovery catalog database, got the following error when 
  starting RMAN to run "upgrade catalog":

PL/SQL package RMAN.DBMS_RCVCAT version 11.01.00.06 in RCVCAT database is not current

DBGSQL:     RCVCAT> begin    :upgrade_completed := dbms_rcvcat.UPGRADE_COMPLETED;end;  [08:19:34.430]
DBGSQL:        sqlcode = 6550 [08:19:34.430]
DBGSQL:         B :upgrade_completed = 0
RMAN-00571: ===========================================================
RMAN-00569: =============== ERROR MESSAGE STACK FOLLOWS ===============
RMAN-00571: ===========================================================
RMAN-00554: initialization of internal recovery manager package failed
RMAN-00600: internal error, arguments [9302] [6550] [begin    :upgrade_completed := dbms_rcvcat.UPGRADE_COMPLETED;end; ] [] []
RMAN-06004: ORACLE error from recovery catalog database: ORA-06550: line 1, column 44:
PLS-00302: component 'UPGRADE_COMPLETED' must be declared
ORA-06550: line 1, column 10:
PL/SQL: Statement ignored

  After searching web, find this is an Oracle bug 7215146. 

  By doing the following steps, the problem is fixed.

On the Recovery Catalog Database server:

1. Connect as RMAN Catalog Owner using SQL*Plus

Note: be sure you login as catalog owner and not as sys or system.

2. Execute following SQL to re-create DBMS_RCVCAT package

$ sqlplus <rman-catalog-owner>/<password>
  -- must set ORACLE_SID correctly

create or replace package dbms_rcvcat authid current_user is
function getPackageVersion return varchar2;
function getCatalogVersion return varchar2;
-- Used to identify if the upgrade of catalog schema was not complete
UPGRADE_COMPLETED CONSTANT number := 0;
end;
/
create or replace package body dbms_rcvcat is
function getPackageVersion return varchar2 is
begin
return '11.01.00.07';
end;
function getCatalogVersion return varchar2 is
begin
return '11.01.00.07';
end;
end;
/

3. Now, Upgrade the Recovery Catalog Schema using 11.1.0.7.0 RMAN Executeable

$ rman catalog <rman-catalog-owner>/<password>
RMAN> upgrade catalog;
recovery catalog owner is RMAN
enter UPGRADE CATALOG command again to confirm catalog upgrade

RMAN> upgrade catalog;
recovery catalog upgraded to version 11.01.00.07
DBMS_RCVMAN package upgraded to version 11.01.00.07
DBMS_RCVCAT package upgraded to version 11.01.00.07

RMAN> exit

 
* When doing the backup on the standby database after upgrade, got error:
  RMAN-00571: ===========================================================
  RMAN-00569: =============== ERROR MESSAGE STACK FOLLOWS ===============
  RMAN-00571: ===========================================================
  RMAN-03002: failure of allocate command at 01/04/2009 01:36:07
  RMAN-03014: implicit resync of recovery catalog failed
  RMAN-03009: failure of partial resync command on default channel at 01/04/2009 01:36:07
  RMAN-20079: full resync from primary database is not done

  To fix this: (on standby server) 
  rman target / catalog rman/rmanpass@rcat
  RMAN> resync catalog;
  RMAN> exit;



