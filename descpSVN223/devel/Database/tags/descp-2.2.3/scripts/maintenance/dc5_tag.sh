#!/bin/bash
#
# loading dc5_tag data extracted from dr004_coadd_objects

DIR='/oracle/scripts/des/buildMV'
DATADIR='/oracle/scripts/des/buildMV'
outfile=$DIR/full_dc5_tag.log
cp /dev/null $outfile

source $DIR/des.bash

 datafile=$DIR/dc5_tag.dat
 ctrfile=$DIR/dc5_tag.ctl
 badfile=$DIR/dc5_tag.bad
 logfile=$DIR/dc5_tag.log
 parfile=$DIR/common.par

 echo `date` "Start loading " >> $outfile

 sqlldr parfile=$parfile control=$ctrfile log=$logfile bad=$badfile
 echo `date` "Finish loading " >> $outfile

 done

exit

