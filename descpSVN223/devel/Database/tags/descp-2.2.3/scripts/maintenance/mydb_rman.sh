#!/bin/bash
#
# RMAN Backup without catalog for mydb
# Date: Jul. 31, 2009
# 09/14/2010 Dora: change the backup directory to /oracle/data_raid/backup1

TODAY=`date '+%m%d%y'`
DIR='/oracle/scripts/mydb'
BUDIR='/oracle/data_raid/backup1/mydb.'$TODAY
LOG=$DIR/mydb_rman.log
cp /dev/null $LOG
mkdir $BUDIR
source $DIR/mydb.bash

cp /dev/null $DIR/mydb_rman.cmd
echo 'run {' >> $DIR/mydb_rman.cmd
echo 'crosscheck backup device type disk; ' >> $DIR/mydb_rman.cmd
echo 'delete noprompt expired backup; '  >> $DIR/mydb_rman.cmd
echo 'crosscheck archivelog all; ' >> $DIR/mydb_rman.cmd
echo "delete noprompt expired archivelog all;} " >> $DIR/mydb_rman.cmd
echo 'run {' >> $DIR/mydb_rman.cmd
echo 'allocate channel t1 type disk MAXPIECESIZE = 5G; ' >> $DIR/mydb_rman.cmd
echo "backup database format '$BUDIR/MYDB_%U'; " >> $DIR/mydb_rman.cmd
echo "backup current controlfile format '$BUDIR/CTRFl_%U'; " >> $DIR/mydb_rman.cmd
echo "backup archivelog all format '$BUDIR/ARC_%s_%p'; " >> $DIR/mydb_rman.cmd
echo 'release channel t1; } ' >> $DIR/mydb_rman.cmd

rman target / nocatalog cmdfile=$DIR/mydb_rman.cmd >> $LOG

line_no=0
line_no=`grep 'ORA-' $LOG | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"MYDB backup on desorch failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

line_no=0
line_no=`grep 'fail' $LOG | grep -v 'validation failed' | wc -l`
if [ $line_no -gt 0 ] ; then
  mail -s"MYDB backup on desorch failed" desdm-db@cosmology.illinois.edu < $LOG
  echo failed > $BUDIR/.status
  exit
fi

mail -s"MYDB backup on desorch is completed successfully" desdm-db@cosmology.illinois.edu < $LOG
echo suceeded > $BUDIR/.status
exit

