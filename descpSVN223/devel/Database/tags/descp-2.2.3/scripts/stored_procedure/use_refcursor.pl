#!/usr/bin/perl
use DBI;
use DBD::Oracle qw(:ora_types);
use FindBin;
use lib "$FindBin::Bin/../../lib";
use DB::DESUtil;

use warnings;
use strict;

my $platform = "Oracle";
my $host = "desdb.cosmology.uiuc.edu";
my $sid  = "des";
my $port = "1521";
my $username = "pipeline";
my $password   = "dc01user";
my $dsn = "dbi:$platform:$sid:$host:$port";
#my $dbh = DBI->connect($dsn, $username, $password);

my $dbh = DB::DESUtil->new(
  {
    'db_user' => $username,
    'db_pass' => $password,
    'db_name' => $sid,
    'db_server' => $host,
  }
);

# The following 3 lines are not necessary if your program already track these values 
# by local variables.
my $select_clause = "id, filetype, nite, archivesites, imagename, ra, dec";
my $table_list = "location, image";
my $where_clause = "id=1835";

my $result = $dbh -> prepare("
declare
  ret sys_refcursor;
begin
  :ret := ProcessQuery(:p1, :p2, :p3, :p4);
end;
");

$result->bind_param(":p1", $username);
$result->bind_param(":p2", $select_clause);
$result->bind_param(":p3", $table_list);
$result->bind_param(":p4", $where_clause);

my $result_cursor;
$result->bind_param_inout(":ret", \$result_cursor, 0, {ora_type => ORA_RSET });

$result->execute();

while (my $r = $result_cursor->fetchrow_hashref) {
  while((my $k, my $v) = each %$r) {
    print "$k = $v\n";
  }
}
