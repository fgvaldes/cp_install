-- compute the coordinate X for the given ra and dec
-- to run the function: select fEqToX(11.10, -2.30) from dual;
-- note: VARRAY subscript starts at 1, not 0
create or replace function fEqToX(
ra number,
dec number)
return float as

x float;
cd float;
pr float := 3.1415926535897932385E0/180.0;

begin
  cd := cos(dec * pr);
  x := cos(ra * pr) * cd;

  return x;  
  
end fEqToX;
/
