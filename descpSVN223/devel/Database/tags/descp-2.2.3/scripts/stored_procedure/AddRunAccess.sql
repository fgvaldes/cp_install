-- Check newly added run identifiers 
create or replace procedure AddRunAccess
as

run_id varchar(100);
project_id varchar(20);

cursor run_add_cr is 
  select distinct a.run, a.project
  from location a
  where a.filedate > (sysdate - 10)
  and   a.run is not null
  and   a.project in ('BCS', 'DES', 'SCS', 'CPT', 'SPT', 'CFH')
  and   regexp_like(archivesites, '[^N]')
  and   not exists (select distinct b.runiddesc
                    from runiddesc_access b
                    where a.run = b.runiddesc
                    and   b.des_group = 'all');

begin
  open run_add_cr;
  loop
    fetch run_add_cr into run_id, project_id;
    exit when run_add_cr%NOTFOUND;
    dbms_output.put_line ('    ' || run_id || ',' || project_id);
  end loop;
end AddRunAccess;
/
