-- This function updates the column "archivesites" in the table "files"
-- The function has 10 parameters in this order:
-- 1. archive site name: (full name or partial name, these value are valid: bcs, des, mss, fermi)
-- 2. action: either "add" or "remove"
-- 3. imageclass: such as "raw", "red", "coadd" or "cal"
-- 4. imagetype: such as "object", "reduced", "catalog", "zero", "flat", "bias", "remap", "dome flat"
-- 5. imagename
-- 6. nite
-- 7. runid
-- 8. tilename
-- 9. band
-- 10. ccd_number 
-- All parameters can use wildcard "%" except ccd_number.
-- All parameters are not case sensitive.
-- All parameters can be variables with null values. 
-- If ccd_number = 0 then all CCDs will be included.
-- to run the function: (This example registers all image files of nite='bcs051118' and imagetype='object' on archive_site 'bcs.cosmology.uiuc.edu'
-- sql> var cnt number;
-- sql> exec :cnt := upd_archivesites('bcs','add','%','object','%','bcs051118','%','%','%',0);
-- sql> print cnt;
create or replace function upd_archivesites(
archive_site_name varchar, action varchar,
imageclass varchar, imagetype varchar, imagename varchar, 
nite varchar, runid varchar, tilename varchar, band varchar,
ccd_number number) return number as 

locid number(2);
tmp_like varchar2(100);
mode_val char(1);
where_clause varchar2(500) := '';
update_clause varchar2(500) := '';
cnt number := 0;

begin
  tmp_like := upper(archive_site_name);
  select location_id
  into locid
  from archive_sites
  where upper(location_name) like tmp_like;   
  
  if (upper(action) = 'ADD') then
    mode_val := 'Y';
  elsif (upper(action) = 'REMOVE') then
    mode_val := 'N';
  else
    return -1;
  end if; 
  
  if ((imageclass is not null) and (imageclass != '%') and (length(imageclass) > 0)) then
    tmp_like := '''%' || upper(imageclass) || '%''';
    where_clause := where_clause || '(upper(imageclass) like ' || tmp_like || ') and '; 
  end if;
    
  if ((imagetype is not null) and (imagetype != '%') and (length(imagetype) > 0)) then
    tmp_like := '''%' || upper(imagetype) || '%''';
    where_clause := where_clause || '(upper(imagetype) like ' || tmp_like || ') and '; 
  end if;
    
  if ((imagename is not null) and (imagename != '%') and (length(imagename) > 0)) then
    tmp_like := '''%' || upper(imagename) || '%''';
    where_clause := where_clause || '(upper(imagename) like ' || tmp_like || ') and '; 
  end if;
    
  if ((nite is not null) and (nite != '%') and (length(nite) > 0)) then
    tmp_like := '''%' || upper(nite) || '%''';
    where_clause := where_clause || '(upper(nite) like ' || tmp_like || ') and '; 
  end if;
    
  if ((runid is not null) and (runid != '%') and (length(runid) > 0)) then
    tmp_like := '''%' || upper(runid) || '%''';
    where_clause := where_clause || '(upper(runiddesc) like ' || tmp_like || ') and '; 
  end if;
    
  if ((tilename is not null) and (tilename != '%') and (length(tilename) > 0)) then
    tmp_like := '''%' || upper(tilename) || '%''';
    where_clause := where_clause || '(upper(tilename) like ' || tmp_like || ') and '; 
  end if;
    
  if ((band is not null) and (band != '%') and (length(band) > 0)) then
    tmp_like := '''%' || upper(band) || '%''';
    where_clause := where_clause || '(upper(band) like ' || tmp_like || ') and '; 
  end if;
    
  if ((ccd_number is not null) and (ccd_number > 0)) then
    where_clause := where_clause || 'ccd_number = ' || ccd_number || ' and '; 
  end if;
    
  -- if where_clause is not empty, take out the last 'and'
  if (length(where_clause) > 0) then
    where_clause := substr(where_clause, 1, (length(where_clause)-4));
  end if;
  --dbms_output.put_line('where_clause is ' || where_clause);

  -- we support up to 25 archive sites 
  if (locid = 1) then
    update_clause := '''' || mode_val || ''' || substr(archivesites,' || 
                     to_char(locid+1) || ',' || to_char(25-locid) || ')'; 
  elsif (locid > 1 and locid < 25) then
    update_clause := 'substr(archivesites,1,' || to_char(locid-1) || ') || ' ||
                     '''' || mode_val || ''' || substr(archivesites,' || 
                     to_char(locid+1) || ',' || to_char(25-locid) || ')'; 
  elsif (locid = 25) then
    update_clause := 'substr(archivesites,1,' || to_char(locid-1) || ') || ' ||
                     '''' || mode_val || '''';
  else
    return -1;
  end if;
  --dbms_output.put_line('update_clause is ' || update_clause);
  
  execute immediate 'update files set archivesites = ' || update_clause ||
                    ' where ' || where_clause; 
  cnt := SQL%ROWCOUNT;
  commit;
  --dbms_output.put_line('count is ' || cnt);
  return cnt;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      return -1;

end upd_archivesites;
/
