-- find the distance (arc minutes) between two points (ra1,dec1) and (ra2,dec2)
-- run the procedure: select fGetDistanceEq(10.47,-2.9,10.48,-2.8) distance 
--                    from dual; 
create or replace function fGetDistanceEq(
ra1 float, -- smaller RA
dec1 float, -- smaller DEC
ra2 float, -- bigger RA
dec2 float) -- bigger DEC
return float
is
vec vector := vector(null,null,null);
vec1 vector := vector(null,null,null);
vec2 vector := vector(null,null,null);
nx1 float;
ny1 float;
nz1 float;
nx2 float;
ny2 float;
nz2 float;
distance float;

begin
    vec1 := fEqToXyz(ra1, dec1);
    nx1 := vec1(1);
    ny1 := vec1(2);
    nz1 := vec1(3); 
    --dbms_output.put_line('point1 is ' || nx1 || ', ' || ny1 || ', ' || nz1);

    vec2 := fEqToXyz(ra2, dec2);
    nx2 := vec2(1);
    ny2 := vec2(2);
    nz2 := vec2(3); 
    --dbms_output.put_line('point2 is ' || nx2 || ', ' || ny2 || ', ' || nz2);

    distance := fGetDistanceXYZ(nx1,ny1,nz1,nx2,ny2,nz2);
    return distance;
end fGetDistanceEq;
/
