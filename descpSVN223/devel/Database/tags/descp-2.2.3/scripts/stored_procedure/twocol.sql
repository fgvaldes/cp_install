-- this stored procedure matches the objects in two images
-- the program has 7 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. stella_lo (minimum value for class_star)
-- 4. stella_hi (maximum value for class_star)
-- 5. flag_upperlim (upperlim for flags)
-- 6. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- 7. magerr_filter (magnitude error filter used: magerr_auto, magerr_aper_1, etc)
-- 8. magerr_limit (magnitude error limit, only the objects with error < magerr_limit will be matched)
-- the program returns a table which has 36 columns:
-- object_id_1, band_1, class_star_1, flags_1, mag_auto_1, magerr_auto_1,
-- mag_aper_1_1, magerr_aper_1_1, mag_aper_2_1, magerr_aper_2_1,
-- mag_aper_3_1, magerr_aper_3_1, mag_aper_4_1, magerr_aper_4_1,
-- mag_aper_5_1, magerr_aper_5_1, mag_aper_6_1, magerr_aper_6_1,
-- object_id_2, band_2, class_star_2, flags_2, mag_auto_2, magerr_auto_2,
-- mag_aper_1_2, magerr_aper_1_2, mag_aper_2_2, magerr_aper_2_2,
-- mag_aper_3_2, magerr_aper_3_2, mag_aper_4_2, magerr_aper_4_2,
-- mag_aper_5_2, magerr_aper_5_2, mag_aper_6_2, magerr_aper_6_2
-- to match image 1627161 and image 1553539 with class_star from 0.75 to 1.0,
-- flag upperlimit 0, matching tolerance 2 arc seconds, using 'magerr_aper_5' as
-- error filter and magnitude error < 0.10:
-- select * from table(fmatchimages(1627161, 1553539, 0.75, 1.0, 0, 0.032, 'magerr_aper_5', 0.10));

create or replace function twocol(
image1 number,
image2 number,
distance_threshold float)
return MatchCoaddIDObjTab pipelined
as
pragma autonomous_transaction;

matchobj MatchCoaddIDObj := MatchCoaddIDObj(null, null);

min_ra1            float;
max_ra1            float;
min_dec1           float;
max_dec1           float;
min_ra2            float;
max_ra2            float;
min_dec2           float;
max_dec2           float;
min_ra             float;
max_ra             float;
min_dec            float;
max_dec            float;
tt            TIMESTAMP;
tt_old            TIMESTAMP;

i number := 0;

coadd_objects_id_1 number(11,0);
cx_1               number(10,6);
cy_1               number(10,6);
cz_1               number(10,6);
alpha_j2000_1      number(8,5);
delta_j2000_1      number(8,5);




cursor selectcsr is
  select /*+ INDEX (coadd_objects coaddobj_ra_dec_idx) */
        a.coadd_objects_id  coadd_objects_id_1, 
        a.cx             cx_1,
        a.cy                cy_1,               a.cz             cz_1,
        a.alpha_j2000       alpha_j2000_1,      a.delta_j2000      delta_j2000_1
        
        from coadd_objects a
	  where (image1 in (a.imageid_g, a.imageid_r, a.imageid_i, a.imageid_z))
	  and   a.alpha_j2000 between min_ra and max_ra
  and   a.delta_j2000 between min_dec and max_dec;


--        from coadd_objects a, files f
--	  where image1 = a.imageid_g
--	  and   a.alpha_j2000 between min_ra and max_ra
--  and   a.delta_j2000 between min_dec and max_dec
--and f.imagetype = 'coadd'
--and f.imageid = a.imageid_g;


cursor matchcsr is
  select /*+ INDEX (coadd_objects coaddobj_ra_dec_idx) */


        b.coadd_objects_id  coadd_objects_id_2,
  fgetdistancexyz(cx_1,cy_1,cz_1,b.cx,b.cy,b.cz) distance_2
      
from coadd_objects b
  
  where   (image2 in (b.imageid_g, b.imageid_r, b.imageid_i, b.imageid_z))
  and   b.alpha_j2000 between min_ra and max_ra
  and   b.delta_j2000 between min_dec and max_dec
  and   b.alpha_j2000 >= alpha_j2000_1 - (distance_threshold*0.1)
  and   b.alpha_j2000 <= alpha_j2000_1 + (distance_threshold*0.1)
  and   b.delta_j2000 >= delta_j2000_1 - (distance_threshold*0.1)
  and   b.delta_j2000 <= delta_j2000_1 + (distance_threshold*0.1)
  and   (fgetdistancexyz(cx_1,cy_1,cz_1,b.cx,b.cy,b.cz) < distance_threshold)
  
  order by distance_2;

-- end of setting up sql cursors

begin
  -- find the overlap part of these two images
  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra1, max_ra1, min_dec1, max_dec1
  from coadd_objects
  where image1 in (imageid_g, imageid_r, imageid_i, imageid_z);

  select min(alpha_j2000), max(alpha_j2000),
         min(delta_j2000), max(delta_j2000)
  into min_ra2, max_ra2, min_dec2, max_dec2
  from coadd_objects
  where image2 in (imageid_g, imageid_r, imageid_i, imageid_z);

  min_ra := greatest(min_ra1, min_ra2) - (distance_threshold * 0.1);
  max_ra := least(max_ra1, max_ra2) + (distance_threshold * 0.1);
  min_dec := greatest(min_dec1, min_dec2) - (distance_threshold * 0.1);
  max_dec := least(max_dec1, max_dec2) + (distance_threshold * 0.1);


  for selectcsr_rec in selectcsr
  loop
    begin
        coadd_objects_id_1    := selectcsr_rec.coadd_objects_id_1;
        cx_1                  := selectcsr_rec.cx_1;
        cy_1                  := selectcsr_rec.cy_1;
        cz_1                  := selectcsr_rec.cz_1;
        alpha_j2000_1         := selectcsr_rec.alpha_j2000_1;
        delta_j2000_1         := selectcsr_rec.delta_j2000_1;

--dbms_output.put_line('select(' || i || ')');
--i := i + 1;

--dbms_output.put_line('select : ' || min_ra );
--dbms_output.put_line('select : ' || max_ra );
--dbms_output.put_line('select : ' || min_dec );
--dbms_output.put_line('select : ' || max_dec );
--dbms_output.put_line('select : ' || (alpha_j2000_1 - (distance_threshold*0.1)));
--dbms_output.put_line('select : ' || (alpha_j2000_1 + (distance_threshold*0.1)));
--dbms_output.put_line('select : ' || (delta_j2000_1 - (distance_threshold*0.1)));
--dbms_output.put_line('select : ' || (delta_j2000_1 + (distance_threshold*0.1)));
--dbms_output.put_line('select : ' || cx_1 || ' ' || cy_1 || ' ' || cz_1 );

        for matchcsr_rec in matchcsr
        loop
                matchobj.coadd_objects_id_1 := coadd_objects_id_1;
                matchobj.coadd_objects_id_2 := matchcsr_rec.coadd_objects_id_2;

              pipe row (matchobj);
--select SYSTIMESTAMP 
--into tt
--from dual;
--dbms_output.put_line('select(' || i || ')' || tt || ' ' || (tt - tt_old));
--i := i + 1;

--tt_old := tt;
              exit;
        end loop;

--        exception
--          when no_data_found then
--            null;
        end;
        end loop;
  commit;
  return;

end twocol;
/
