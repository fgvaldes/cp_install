-- This program tries to do circle search by searching a rectangle.
-- This program is not efficient. We still need use HTMID for circular search.
-- find objects in a circle specified by ra and dec
-- run the procedure: select * from table(fGetObjCirc(10.48, -2.87, 0.5))
create or replace function fGetObjCirc(
ra float,
dec float,
r float)
return CircleObjTab pipelined
is
pragma autonomous_transaction;

cobj CircleObj := CircleObj(null,null,null,null,null,null);
vec vector := vector(null,null,null);
--r_ float;
--d float;
nx float;
ny float;
nz float;
ra_lo float;
ra_hi float;
dec_lo float;
dec_hi float;

cursor matchrc is
  select b.object_id, b.alpha_j2000, b.delta_j2000, b.band, b.imageid, 
         b.cx, b.cy, b.cz, 
         fGetDistanceXYZ(nx, ny, nz, b.cx, b.cy, b.cz) distance
  from objects b
  where b.alpha_j2000 >= ra_lo
  and   b.alpha_j2000 <= ra_hi
  and   b.delta_j2000 >= dec_lo
  and   b.delta_j2000 <= dec_hi;

begin
  vec := fEqToXyz(ra, dec);
  nx := vec(1);
  ny := vec(2);
  nz := vec(3);
  ra_lo := ra - r;
  ra_hi := ra + r;
  dec_lo := dec - r;
  dec_hi := dec + r; 
  for matchrc_rec in matchrc
  loop
    if (matchrc_rec.distance <= r) then
      cobj.object_id := matchrc_rec.object_id;
      cobj.alpha_j2000 := matchrc_rec.alpha_j2000;
      cobj.delta_j2000 := matchrc_rec.delta_j2000;
      cobj.band := matchrc_rec.band;
      cobj.imageid := matchrc_rec.imageid;
      cobj.distance := matchrc_rec.distance;
      pipe row (cobj);
    end if;
  end loop;
  commit;
  return;
end fGetObjCirc;
/

