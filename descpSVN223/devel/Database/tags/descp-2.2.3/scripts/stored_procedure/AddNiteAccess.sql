-- Check newly added nite identifiers
create or replace procedure AddNiteAccess
as

nite_id varchar(20);
project_id varchar(20);

cursor nite_add_cr is 
  select distinct a.nite, a.project
  from location a
  where a.filedate > (sysdate - 10)
  and   a.nite is not null
  and   a.project in ('BCS', 'DES', 'SCS', 'CPT', 'SPT', 'CFH')
  and   regexp_like(archivesites, '[^N]')
  and   not exists (select distinct b.nite, b.project
                    from nite_access b
                    where a.nite = b.nite
                    and   a.project = b.project
                    and   b.des_group = 'all');

begin
  open nite_add_cr;
  loop
    fetch nite_add_cr into nite_id, project_id;
    exit when nite_add_cr%NOTFOUND;
    dbms_output.put_line ('    ' || nite_id ||',' || project_id);
  end loop;
  
end AddNiteAccess;
/
