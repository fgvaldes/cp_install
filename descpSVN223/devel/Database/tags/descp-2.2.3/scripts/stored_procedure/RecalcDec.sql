-- recalculate and update min/max of DEC based on Objects table
create or replace procedure RecalcDec(
obs varchar) as

obs_like varchar(15) := obs || '%';
obs_id varchar(15);
col_n number(4);
h_dec number(10,6);
l_dec number(10,6);

cursor obs_cr is 
  select observation_id, column_n
  from ccd_columns
  where observation_id like obs_like;

begin
  --dbms_output.put_line ('obs_like is ' || obs_like);
  open obs_cr;
  loop
    fetch obs_cr into obs_id, col_n;
    exit when obs_cr%NOTFOUND;

    --dbms_output.put_line ('obs_id is ' || obs_id);
    --dbms_output.put_line ('col_n is ' || col_n);

    select /*+ INDEX(objects PK_OBJECTS) */
    max(dec), min(dec)
    into h_dec, l_dec
    from objects
    where observation_id = obs_id
    and   column_n = col_n;
    
    --dbms_output.put_line ('high_dec is ' || h_dec);
    --dbms_output.put_line ('low_dec is ' || l_dec);

    update ccd_columns
    set high_dec = h_dec,
        low_dec = l_dec
    where observation_id = obs_id
    and   column_n = col_n;

    commit;
  end loop;
  
end RecalcDec;
/
