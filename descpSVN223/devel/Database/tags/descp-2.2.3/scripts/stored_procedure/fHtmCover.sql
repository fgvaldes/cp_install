-- This function gets a list of HTM ranges.
-- This function is called from fGetNearbyXYZpre.
create or replace function fHtmCover(
cmd varchar2,
r number)
return rangeTab pipelined is

pragma autonomous_transaction;
level number(2);
shift number(16);
htm_range range := range(null, null); 
range_string varchar2(10000);
element number(4); -- number of HTMIDs in the range_string
start_pos number(4) := 1; -- start index of a HTM id in the range_string
end_pos number(4) := 1; -- end index of a HTM id in the range_string
end_pos1 number(4) := 1;
end_pos2 number(4) := 1;
start_HTM number(16); -- the start HTM in the HTM range
end_HTM number(16);  -- the end HTM in the HTM range
tmp_str varchar(80);
seq number(4) := 1;
cell number(16); -- a HTM id, either start_HTM, or end_HTM

begin
  level := 13 - (floor(log(10,r)/log(10,2.0)));
  --dbms_output.put_line('level is ' || level);
  if (level < 5) then
    level := 5;
  elsif (level > 13) then
    level := 13;
  end if;
  --dbms_output.put_line('level2 is ' || level);
  shift := power(4, 20-level);
  --dbms_output.put_line('shift is ' || shift);
  range_string := getRange(cmd);
  --dbms_output.put_line('range_string is ' || range_string);
  while (start_pos < length(range_string))
  loop
    end_pos1 := instr(range_string, ' ', start_pos, 1);
    end_pos2 := instr(range_string, chr(10), start_pos, 1);
    if (end_pos1 = 0) then -- reach the end, no more space symbol
      end_pos := length(range_string);
    elsif (end_pos1 <= end_pos2) then -- a space is before a new line symbol
      end_pos := end_pos1;
    elsif (end_pos1 > end_pos2) then -- a new line symbol is before a space
      end_pos := end_pos2;
    end if;
    tmp_str := substr(range_string, start_pos, end_pos-start_pos);
    --dbms_output.put_line('tmp_str for cell is ' || tmp_str);
    cell := to_number(tmp_str);
    if (seq = 1) then
      start_HTM := cell * shift;
      --dbms_output.put_line('start_HTM is ' || start_HTM);
      htm_range.htm_id_start := start_HTM;
      seq := 2;
    else -- seq = 2
      end_HTM := (cell+1) * shift; -- adding 1 to ensure the completeness
      --dbms_output.put_line('end_HTM is ' || end_HTM);
      seq := 1;
      htm_range.htm_id_end := end_HTM;
      pipe row (htm_range);
    end if;
    start_pos := end_pos + 1;
  end loop;
  commit;
  return;
end fHtmCover;
/

