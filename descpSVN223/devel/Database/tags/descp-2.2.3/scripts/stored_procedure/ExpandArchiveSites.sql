-- Expand the column ArchiveSites in table Files from varchar2(12) to varchar2(25)
-- To call the procedure: exec ExpandArchiveSites;
create or replace procedure ExpandArchiveSites
as

niteid varchar(30);
cnt number;

cursor nite_cr is 
  select distinct nite
  from files;

begin
  open nite_cr;
  loop
    fetch nite_cr into niteid;
    exit when nite_cr%NOTFOUND;

    update files
    set archivesites = archivesites || 'NNNNNNNNNNNNNNN'
    where nite = niteid
    and length(archivesites) = 10;   

    cnt := SQL%ROWCOUNT;
    dbms_output.put_line (cnt || ' rows are updated for night ' || niteid); 
    commit;
  end loop;
  
end ExpandArchiveSites;
/
