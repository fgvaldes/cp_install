-- recalculate cx, cy, cz values based on alphamodel_j2000 or deltamodel_j2000
-- (JIRA DES-1588)

create or replace procedure UpdateCXYZ(
runid varchar) as

img_id number(9);

cursor img_cr is
  select id
  from image
  where run = runid;

begin
  open img_cr;
  loop
    fetch img_cr into img_id;
    exit when img_cr%NOTFOUND;

    dbms_output.put_line('img_id is ' || img_id);

    update objects_current
    set cx = fEqToX(alphamodel_j2000, deltamodel_j2000), 
        cy = fEqToY(alphamodel_j2000, deltamodel_j2000), 
        cz = fEqToZ(alphamodel_j2000, deltamodel_j2000) 
    where imageid = img_id;
    commit;
  end loop;
  
end UpdateCXYZ;
/
