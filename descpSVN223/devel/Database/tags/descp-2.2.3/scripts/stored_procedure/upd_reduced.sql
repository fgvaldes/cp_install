-- update reduced's RA and DEC using object's RA and DEC 
-- The procedure takes one input parameter: night_identifier.
-- To run the procedure: exec upd_reduced('des20040501');

create or replace procedure upd_reduced(
nite_in varchar) as

imgname varchar(50);
cnt number := 0;

cursor img_cr is 
  select distinct imagename
  from files
  where nite = nite_in
  and   lower(imagetype) = 'reduced';

begin
  dbms_output.put_line ('nite is ' || nite_in);
  open img_cr;
  loop
    fetch img_cr into imgname;
    exit when img_cr%NOTFOUND;

    dbms_output.put_line ('imgname is ' || imgname);
    update files a
    set (a.ra, a.dec) = (select b.ra, b.dec
                         from files b
                         where b.imagename = a.imagename
                         and   b.nite =  nite_in
                         and   b.imagename = imgname
                         and   lower(b.imagetype) = 'object'
                         and   b.ccd_number = 1)
    where lower(a.imagetype) = 'reduced'
    and   a.nite = nite_in
    and   a.imagename = imgname
    and   a.ra = 0
    and   a.dec = 0;

    cnt := SQL%ROWCOUNT;
    dbms_output.put_line (cnt || ' rows are updated for image ' || imgname);

    commit;
  end loop;
  
end upd_reduced;
/
