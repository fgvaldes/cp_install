-- This is the script to call the procedure upd_archivesites
spool upd_archivesites.lst
set echo on;
set feedback on;

var cnt number
exec :cnt := upd_archivesites('&1','&2','&3','&4','&5','&6','&7','&8','&9','&10');
print cnt;
exit;
