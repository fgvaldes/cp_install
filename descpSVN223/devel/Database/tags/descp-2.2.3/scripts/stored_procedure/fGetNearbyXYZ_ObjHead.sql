-- find the nearby objects to the point specified by coordinates X, Y and Z 
-- this procedure outputs ra, dec, htmid, cx, cy, cz, distance.
-- run the procedure: select * from table(fGetNearbyXYZ_ObjHead(.9821, .1816, -.0506, 5))
create or replace function fGetNearbyXYZ_ObjHead(
nx float, 
ny float, 
nz float, 
r float) 
return objectHeadTab pipelined 
is
pragma autonomous_transaction;

objhead objectHead := objectHead(null,null,null,null,null,null,null,null);
r_ float;

cursor objrc is
  select object_id, htmid, ra, dec, cx, cy, cz, 
         distance
  from table(fGetNearbyXYZpre_ObjHead(nx,ny,nz,r_))
  where distance < r_
  order by distance;

begin
  r_ := r;
  if (r_ < 0.1) then
    r_ := 0.1;
  end if;

  for objrc_rec in objrc
  loop
     objhead.object_id := objrc_rec.object_id;
     objhead.htmid := objrc_rec.htmid;
     objhead.ra := objrc_rec.ra;
     objhead.dec := objrc_rec.dec;
     objhead.cx := objrc_rec.cx;
     objhead.cy := objrc_rec.cy;
     objhead.cz := objrc_rec.cz;
     objhead.distance := objrc_rec.distance;
     pipe row (objhead);
  end loop;
  commit;
  return;
end fGetNearbyXYZ_ObjHead;
/

