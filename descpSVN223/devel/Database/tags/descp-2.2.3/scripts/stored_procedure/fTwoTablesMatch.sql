-- This program is the main program for matching objects in two tables.
-- This program takes 7 parameters:
-- 1. the table name for the selected truth table
-- 2. the table name for to be matched object table
-- 3. min RA for the selected region
-- 4. min DEC for the selected region
-- 5. max RA for the selected region
-- 6. max DEC for the selected region
-- 7. distance threshold in arc minute (1 arc second = 0.01667 arc minute)
-- This program generate an output table that has tow columns:
-- 1. object_id from the selected truth table
-- 2. object_id from the matching object table 
-- This program calls different matching procedures depending on the two tables
-- requested.
-- to run the procedure: select * from table(fTwoTablesMatch('CatSim1_Truth',
-- 'Objects', 341, -34, 341.05, -33.95, 0.03333));
create or replace function fTwoTablesMatch(
select_table varchar,
match_table varchar,
ra_lo float,
dec_lo float,
ra_hi float,
dec_hi float,
threshold float)
return objectMapTab pipelined
as
pragma autonomous_transaction;

mapObj objectMap := objectMap(null,null);

cursor CatSim1Obj_Csr is 
  select select_object_id, match_object_id
  from table(fMatchCatSim1Obj(ra_lo, dec_lo, ra_hi, dec_hi, threshold));

begin
  if ((UPPER(select_table) = UPPER('CatSim1_Truth')) and 
     (UPPER(match_table) = UPPER('Objects'))) then
    for CatSim1Obj_Csr_rec in CatSim1Obj_Csr
    loop
      begin
        mapObj.select_object_id := CatSim1Obj_Csr_rec.select_object_id;  
        mapObj.match_object_id := CatSim1Obj_Csr_rec.match_object_id;  
        pipe row (mapObj);
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            null;
      end;
    end loop;

    commit;
  end if;
  return;
end fTwoTablesMatch;
/
