-- find the distance (arc minutes) between two points (nx,ny,nz) and (cx,cy,cz)
-- run the procedure: select fGetDistanceXYZ(.68,-.18,-.7,.681,-.181,-.701) distance 
--                    from dual; 
create or replace function fGetDistanceXYZ(
nx float, 
ny float, 
nz float, 
cx float,
cy float,
cz float)
return float 
is

distance float := 0.0;

begin
  --distance := (2*180/3.14159)*(asin(sqrt(power(nx-cx,2)+power(ny-cy,2)+power(nz-cz,2))/2))*60;
  distance := power(nx-cx,2)+power(ny-cy,2)+power(nz-cz,2);
  --dbms_output.put_line('sum of power is ' || distance);
  if (distance >= 0) then
    distance := sqrt(distance);
    --dbms_output.put_line('sqrt of power is ' || distance);
    if (distance <= 1) then
      distance := (2*180/3.1415926535897932385E0)*(asin(distance/2))*60;
    elsif (distance > 1) then -- data error, cannot asin on > 1
      distance := 999999;
    end if;    
  elsif (distance < 0) then -- data error, cannot sqrt on a negative number
    distance := 999999; 
  end if;
  --dbms_output.put_line('distance is ' || distance);
  return distance;
end fGetDistanceXYZ;
/

