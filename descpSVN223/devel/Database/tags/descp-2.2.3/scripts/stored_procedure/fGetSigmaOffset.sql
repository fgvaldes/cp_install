-- find the sigma_offset (in degree) between two points (ra,dec) and (ra,dec)
-- using known error X2 and error Y2
-- run the procedure: select fgetsigmaoffset(342.69251, -37.93571, 0, 0, 
--                    342.69351, -37.93471, 0, 0) sigma from dual
--                    from dual; 
create or replace function fGetSigmaOffset(
ra1 float, 
dec1 float, 
errx1 float, 
erry1 float, 
ra2 float, 
dec2 float, 
errx2 float, 
erry2 float) 
return float 
is

distance float := 0.0;
distance_sq float := 0.0;
sigma float := 0.0;
sigma_dist_sq float := 0.0;
sigma_sq_dist_sq float := 0.0;
seale float := 0.0;

begin
  seale := cos(0.5 * (dec1 + dec2) * 3.14159 / 180);
  --dbms_output.put_line('seale is ' || seale);
  distance_sq := power(seale, 2)*power((ra1-ra2),2) + power((dec1-dec2),2); 
  --dbms_output.put_line('distance_sq is ' || distance_sq);
  distance := sqrt(distance_sq);
  --dbms_output.put_line('distance is ' || distance);
  sigma_sq_dist_sq := (4*power(seale,4)*(power(ra1,2)+power(ra2,2))*(power(errx1,2)+power(errx2,2))) + (4*(power(dec1,2)+power(dec2,2))*(power(erry1,2)+power(erry2,2)));
  --dbms_output.put_line('sigma_sq_dist_sq is ' || sigma_sq_dist_sq);
  sigma_dist_sq := sqrt(sigma_sq_dist_sq);
  --dbms_output.put_line('sigma_dist_sq is ' || sigma_dist_sq);
  if (distance = 0) then
    sigma := sigma_dist_sq / 2;
    --dbms_output.put_line('sigma when d=0 is ' || sigma);
  else
    sigma := sigma_dist_sq / (2 * distance);
    --dbms_output.put_line('sigma when d<>0 is ' || sigma);
  end if;
  return sigma;


end fGetSigmaOffset;
/

