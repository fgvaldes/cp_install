declare
n1 binary_integer;
n2 number;
begin
  n1 := 11000110001011;
  n2 := n1;
  dbms_output.put_line(n2);
end;
/
