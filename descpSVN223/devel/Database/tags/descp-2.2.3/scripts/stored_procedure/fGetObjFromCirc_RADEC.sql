-- This function does not perform well comparing with fGetObjFromCirc
-- find objects in a circle specified by ra and dec
-- run the procedure: select * from table(fGetObjFromCirc(10.48, -2.87, 0.5))
create or replace function fGetObjFromCirc_RADEC(
ra float,
dec float,
r float)
return CircleObjTab pipelined
is
pragma autonomous_transaction;

cobj CircleObj := CircleObj(null,null,null,null,null,null);
vec vector := vector(null,null,null);
r_ float;
nx float;
ny float;
nz float;
ra_lo float;
ra_hi float;
dec_lo float;
dec_hi float;

cursor matchrc is
  select /*+ INDEX(b objects_ra_dec_idx) */
         b.object_id, b.alpha_j2000, b.delta_j2000, b.band, b.imageid, 
	 b.cx, b.cy, b.cz
  from objects b
  where b.alpha_j2000 >= ra_lo
  and   b.alpha_j2000 <= ra_hi
  and   b.delta_j2000 >= dec_lo
  and   b.delta_j2000 <= dec_hi;

begin
  -- r_ is used to define the RA range, r_ is adjusted by dec and r
  r_ := fGetRaOffset(dec, r);
  --dbms_output.put_line('r_ in circleObj is ' || r_ );
  vec := fEqToXyz(ra, dec);
  nx := vec(1);
  ny := vec(2);
  nz := vec(3);
  ra_lo := ra - r_;
  ra_hi := ra + r_;
  dec_lo := dec - r;
  dec_hi := dec + r;

  --dbms_output.put_line('xyz in circleObj is ' || nx ||',' || ny ||',' || nz);

  for matchrc_rec in matchrc
  loop
    cobj.distance := fGetDistanceXYZ(nx,ny,nz,matchrc_rec.cx,
                                     matchrc_rec.cy, matchrc_rec.cz); 
    if (cobj.distance <= r) then
      cobj.object_id := matchrc_rec.object_id;
      cobj.alpha_j2000 := matchrc_rec.alpha_j2000;
      cobj.delta_j2000 := matchrc_rec.delta_j2000;
      cobj.band := matchrc_rec.band;
      cobj.imageid := matchrc_rec.imageid;
      pipe row (cobj);
    end if;
  end loop;
  commit;
  return;
end fGetObjFromCirc_RADEC;
/

