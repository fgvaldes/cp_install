-- This is a sub-function used in fGetNearbyXYZ
create or replace function fGetNearbyXYZpre(
nx float, 
ny float, 
nz float, 
r float) 
return matchedObjTab pipelined 
is
pragma autonomous_transaction;

mobj matchedObj := matchedObj(null,null,null,null,null,null);

level number(2);
shift number(16);
cmd varchar2(200);

cursor matchrc is
  --select /*+ USE_NL(a, b)  INDEX(b idx_htmid) */
  select /*+ ordered USE_NL(b) INDEX(b objects_htmid_idx) */
         object_id, htmid, cx, cy, cz, 
         fGetDistanceXYZ(nx,ny,nz,cx,cy,cz) distance
  from table(fHtmCover(cmd,r)) a, objects b
  where b.htmid >= a.htm_id_start
  and   b.htmid <= a.htm_id_end;

begin
  level := 13 - (floor(log(10,r)/log(10,2.0)));
  --dbms_output.put_line('level is ' || level);
  if (level < 5) then
    level := 5;
  elsif (level > 13) then
    level := 13;
  end if;
  --dbms_output.put_line('level2 is ' || level);
  shift := power(4, 20-level);
  --dbms_output.put_line('shift is ' || shift);
  cmd := to_char(level) || ' ' || to_char(nx) || ' ' || to_char(ny) || ' ' || 
         to_char(nz) || ' ' || to_char(r);
  --dbms_output.put_line('cmd is ' || cmd);
  for matchrc_rec in matchrc
  loop
     mobj.object_id := matchrc_rec.object_id;
     mobj.htmid := matchrc_rec.htmid;
     mobj.cx := matchrc_rec.cx;
     mobj.cy := matchrc_rec.cy;
     mobj.cz := matchrc_rec.cz;
     mobj.distance := matchrc_rec.distance;
     pipe row (mobj);
  end loop;
  commit;
  return;
end fGetNearbyXYZpre;
/

