-- This stored procedure matches the objects in two images
-- The program has 8 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. stella_lo (minimum value for class_star)
-- 4. stella_hi (maximum value for class_star)
-- 5. flag_upperlim (upperlim for flags)
-- 6. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- 7. magerr_filter (magnitude error filter used: magerr_auto, magerr_aper_1, etc)
-- 8. magerr_limit (magnitude error limit, only the objects with error < magerr_limit will be matched)
-- The program returns a table which has 82 columns:
-- object_id_1, band_1, class_star_1, flags_1, mag_auto_1, magerr_auto_1, 
-- mag_aper_1_1, magerr_aper_1_1, mag_aper_2_1, magerr_aper_2_1,
-- mag_aper_3_1, magerr_aper_3_1, mag_aper_4_1, magerr_aper_4_1,
-- mag_aper_5_1, magerr_aper_5_1, mag_aper_6_1, magerr_aper_6_1,
-- mag_aper_7_1, magerr_aper_7_1, mag_aper_8_1, magerr_aper_8_1,
-- mag_aper_9_1, magerr_aper_9_1, mag_aper_10_1, magerr_aper_10_1,
-- mag_aper_11_1, magerr_aper_11_1, mag_aper_12_1, magerr_aper_12_1,
-- mag_aper_13_1, magerr_aper_13_1, mag_aper_14_1, magerr_aper_14_1,
-- mag_aper_15_1, magerr_aper_15_1, mag_aper_16_1, magerr_aper_16_1,
-- mag_aper_17_1, magerr_aper_17_1, 
-- object_id_2, band_2, class_star_2, flags_2, mag_auto_2, magerr_auto_2, 
-- mag_aper_1_2, magerr_aper_1_2, mag_aper_2_2, magerr_aper_2_2,
-- mag_aper_3_2, magerr_aper_3_2, mag_aper_4_2, magerr_aper_4_2,
-- mag_aper_5_2, magerr_aper_5_2, mag_aper_6_2, magerr_aper_6_2,
-- mag_aper_7_2, magerr_aper_7_2, mag_aper_8_2, magerr_aper_8_2,
-- mag_aper_9_2, magerr_aper_9_2, mag_aper_10_2, magerr_aper_10_2,
-- mag_aper_11_2, magerr_aper_11_2, mag_aper_12_2, magerr_aper_12_2,
-- mag_aper_13_2, magerr_aper_13_2, mag_aper_14_2, magerr_aper_14_2,
-- mag_aper_15_2, magerr_aper_15_2, mag_aper_16_2, magerr_aper_16_2,
-- mag_aper_17_2, magerr_aper_17_2 
-- to match image 12699134 and image 12699319 with class_star from 0.75 to 1.0,
-- flag upperlimit 0, matching tolerance 2 arc seconds, using 'magerr_aper_5' as
-- error filter and magnitude error < 0.10:
-- select * from table(fMatchImages_Model(12699134, 12699319, 0.75, 1.0, 0, 0.032, 'magerr_aper_5', 0.10));

create or replace function fMatchImages_Model(
image1 number,
image2 number, 
stella_lo float,
stella_hi float,
flag_upperlim number,
distance_threshold float,
magerr_filter varchar,
magerr_limit float)
return MatchImgObjTab pipelined
as
pragma autonomous_transaction;


matchObj MatchImgObj := MatchImgObj(null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null);
min_ra1 float;
max_ra1 float;
min_dec1 float;
max_dec1 float;
min_ra2 float;
max_ra2 float;
min_dec2 float;
max_dec2 float;
min_ra float;
max_ra float;
min_dec float;
max_dec float;
distance float;
type list is RECORD (
  imageid number(9), 
  object_id number(11), 
  mag binary_float, 
  magerr binary_float, 
  mag_aper_1 binary_float, 
  magerr_aper_1 binary_float,
  mag_aper_2 binary_float, 
  magerr_aper_2 binary_float,
  mag_aper_3 binary_float, 
  magerr_aper_3 binary_float,
  mag_aper_4 binary_float, 
  magerr_aper_4 binary_float,
  mag_aper_5 binary_float, 
  magerr_aper_5 binary_float,
  mag_aper_6 binary_float, 
  magerr_aper_6 binary_float,
  mag_aper_7 binary_float, 
  magerr_aper_7 binary_float,
  mag_aper_8 binary_float, 
  magerr_aper_8 binary_float,
  mag_aper_9 binary_float, 
  magerr_aper_9 binary_float,
  mag_aper_10 binary_float, 
  magerr_aper_10 binary_float,
  mag_aper_11 binary_float, 
  magerr_aper_11 binary_float,
  mag_aper_12 binary_float, 
  magerr_aper_12 binary_float,
  mag_aper_13 binary_float, 
  magerr_aper_13 binary_float,
  mag_aper_14 binary_float, 
  magerr_aper_14 binary_float,
  mag_aper_15 binary_float, 
  magerr_aper_15 binary_float,
  mag_aper_16 binary_float, 
  magerr_aper_16 binary_float,
  mag_aper_17 binary_float, 
  magerr_aper_17 binary_float,
  cx binary_float, 
  cy binary_float, 
  cz binary_float, 
  band varchar(10), 
  class_star number(3,2), 
  flags number(3), 
  alphamodel_j2000 binary_float, 
  deltamodel_j2000 binary_float
);

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;

cursor selectCsr_auto is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_auto < magerr_limit; 

cursor selectCsr_1 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_1 < magerr_limit; 

cursor selectCsr_2 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_2 < magerr_limit; 

cursor selectCsr_3 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_3 < magerr_limit; 

cursor selectCsr_4 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_4 < magerr_limit; 

cursor selectCsr_5 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_5 < magerr_limit; 

cursor selectCsr_6 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_6 < magerr_limit; 

cursor selectCsr_7 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_7 < magerr_limit; 

cursor selectCsr_8 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_8 < magerr_limit; 

cursor selectCsr_9 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_9 < magerr_limit; 

cursor selectCsr_10 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_10 < magerr_limit; 

cursor selectCsr_11 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_11 < magerr_limit; 

cursor selectCsr_12 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_12 < magerr_limit; 

cursor selectCsr_13 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_13 < magerr_limit; 

cursor selectCsr_14 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_14 < magerr_limit; 

cursor selectCsr_15 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_15 < magerr_limit; 

cursor selectCsr_16 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_16 < magerr_limit; 

cursor selectCsr_17 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from objects a
  where imageid = image1
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
  and   flags <= flag_upperlim
  and   magerr_aper_17 < magerr_limit; 

cursor matchCsr_auto is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_auto < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
; 

cursor matchCsr_1 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_1 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
; 

cursor matchCsr_2 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_2 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
; 

cursor matchCsr_3 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_3 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
; 

cursor matchCsr_4 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_4 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
; 

cursor matchCsr_5 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_5 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_6 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_6 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_7 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_7 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_8 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_8 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_9 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_9 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_10 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_10 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_11 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_11 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_12 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_12 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_13 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_13 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_14 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_14 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_15 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_15 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_16 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_16 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

cursor matchCsr_17 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         mag_aper_7, magerr_aper_7,
         mag_aper_8, magerr_aper_8,
         mag_aper_9, magerr_aper_9,
         mag_aper_10, magerr_aper_10,
         mag_aper_11, magerr_aper_11,
         mag_aper_12, magerr_aper_12,
         mag_aper_13, magerr_aper_13,
         mag_aper_14, magerr_aper_14,
         mag_aper_15, magerr_aper_15,
         mag_aper_16, magerr_aper_16,
         mag_aper_17, magerr_aper_17,
         cx, cy, cz, band, 
         class_star, flags, 
         alphamodel_j2000, deltamodel_j2000
  from  objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   magerr_aper_17 < magerr_limit
  and   ra between min_ra and max_ra
  and   dec between min_dec and max_dec
;

begin
  -- find the overlap part of these two images
  select min(ra), max(ra), 
         min(dec), max(dec)
  into min_ra1, max_ra1, min_dec1, max_dec1
  from objects
  where imageid = image1;
  select min(ra), max(ra), 
         min(dec), max(dec)
  into min_ra2, max_ra2, min_dec2, max_dec2
  from objects
  where imageid = image2;
  min_ra := greatest(min_ra1, min_ra2) - distance_threshold;
  max_ra := least(max_ra1, max_ra2) + distance_threshold;
  min_dec := greatest(min_dec1, min_dec2) - distance_threshold;
  max_dec := least(max_dec1, max_dec2) + distance_threshold;

  -- magerr_auto is used for filtering
  if (lower(magerr_filter) = 'magerr_auto') then
    open selectCsr_auto;
    open matchCsr_auto;

    fetch selectCsr_auto bulk collect into collect1;
    fetch matchCsr_auto bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistanceEQ(collect1(i).alphamodel_j2000,collect1(i).deltamodel_j2000,
                              collect2(j).alphamodel_j2000,collect2(j).deltamodel_j2000) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_1 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_1') then
    open selectCsr_1;
    open matchCsr_1;

    fetch selectCsr_1 bulk collect into collect1;
    fetch matchCsr_1 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(cos(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
			       sin(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
                               sin(0.017453*collect1(i).deltamodel_j2000),
			       cos(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
                               sin(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
			       sin(0.017453*collect2(i).deltamodel_j2000)) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_2 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_2') then
    open selectCsr_2;
    open matchCsr_2;

    fetch selectCsr_2 bulk collect into collect1;
    fetch matchCsr_2 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(cos(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
                               sin(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
                               sin(0.017453*collect1(i).deltamodel_j2000),
                               cos(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
                               sin(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
                               sin(0.017453*collect2(i).deltamodel_j2000)) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_3 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_3') then
    open selectCsr_3;
    open matchCsr_3;

    fetch selectCsr_3 bulk collect into collect1;
    fetch matchCsr_3 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(cos(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
                               sin(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
                               sin(0.017453*collect1(i).deltamodel_j2000),
                               cos(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
                               sin(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
                               sin(0.017453*collect2(i).deltamodel_j2000)) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;


  -- magerr_aper_4 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_4') then
    open selectCsr_4;
    open matchCsr_4;

    fetch selectCsr_4 bulk collect into collect1;
    fetch matchCsr_4 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(cos(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
                               sin(0.017453*collect1(i).alphamodel_j2000)*cos(0.017453*collect1(i).deltamodel_j2000),
                               sin(0.017453*collect1(i).deltamodel_j2000),
                               cos(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
                               sin(0.017453*collect2(i).alphamodel_j2000)*cos(0.017453*collect2(i).deltamodel_j2000),
                               sin(0.017453*collect2(i).deltamodel_j2000))< distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;


  -- magerr_aper_5 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_5') then
    open selectCsr_5;
    open matchCsr_5;

    fetch selectCsr_5 bulk collect into collect1;
    fetch matchCsr_5 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_6 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_6') then
    open selectCsr_6;
    open matchCsr_6;

    fetch selectCsr_6 bulk collect into collect1;
    fetch matchCsr_6 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_7 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_7') then
    open selectCsr_7;
    open matchCsr_7;

    fetch selectCsr_7 bulk collect into collect1;
    fetch matchCsr_7 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_8 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_8') then
    open selectCsr_8;
    open matchCsr_8;

    fetch selectCsr_8 bulk collect into collect1;
    fetch matchCsr_8 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistanceEQ(collect1(i).alphamodel_j2000,collect1(i).deltamodel_j2000,
                              collect2(j).alphamodel_j2000,collect2(j).deltamodel_j2000) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_9 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_9') then
    open selectCsr_9;
    open matchCsr_9;

    fetch selectCsr_9 bulk collect into collect1;
    fetch matchCsr_9 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_10 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_10') then
    open selectCsr_10;
    open matchCsr_10;

    fetch selectCsr_10 bulk collect into collect1;
    fetch matchCsr_10 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_11 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_11') then
    open selectCsr_11;
    open matchCsr_11;

    fetch selectCsr_11 bulk collect into collect1;
    fetch matchCsr_11 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_12 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_12') then
    open selectCsr_12;
    open matchCsr_12;

    fetch selectCsr_12 bulk collect into collect1;
    fetch matchCsr_12 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_13 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_13') then
    open selectCsr_13;
    open matchCsr_13;

    fetch selectCsr_13 bulk collect into collect1;
    fetch matchCsr_13 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_14 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_14') then
    open selectCsr_14;
    open matchCsr_14;

    fetch selectCsr_14 bulk collect into collect1;
    fetch matchCsr_14 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_15 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_15') then
    open selectCsr_15;
    open matchCsr_15;

    fetch selectCsr_15 bulk collect into collect1;
    fetch matchCsr_15 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_16 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_16') then
    open selectCsr_16;
    open matchCsr_16;

    fetch selectCsr_16 bulk collect into collect1;
    fetch matchCsr_16 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  -- magerr_aper_17 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_17') then
    open selectCsr_17;
    open matchCsr_17;

    fetch selectCsr_17 bulk collect into collect1;
    fetch matchCsr_17 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alphamodel_j2000 >= (collect1(i).alphamodel_j2000 - distance_threshold)
          and collect2(j).alphamodel_j2000 <= (collect1(i).alphamodel_j2000 + distance_threshold)
          and collect2(j).deltamodel_j2000 >= (collect1(i).deltamodel_j2000 - distance_threshold)
          and collect2(j).deltamodel_j2000 <= (collect1(i).deltamodel_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          if ((collect1(i).class_star >= stella_lo and 
               collect1(i).class_star <= stella_hi) or 
              (collect2(j).class_star >= stella_lo and 
               collect2(j).class_star <= stella_hi)) then 
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.mag_aper_7_1 := collect1(i).mag_aper_7;
          matchObj.magerr_aper_7_1 := collect1(i).magerr_aper_7;
          matchObj.mag_aper_8_1 := collect1(i).mag_aper_8;
          matchObj.magerr_aper_8_1 := collect1(i).magerr_aper_8;
          matchObj.mag_aper_9_1 := collect1(i).mag_aper_9;
          matchObj.magerr_aper_9_1 := collect1(i).magerr_aper_9;
          matchObj.mag_aper_10_1 := collect1(i).mag_aper_10;
          matchObj.magerr_aper_10_1 := collect1(i).magerr_aper_10;
          matchObj.mag_aper_11_1 := collect1(i).mag_aper_11;
          matchObj.magerr_aper_11_1 := collect1(i).magerr_aper_11;
          matchObj.mag_aper_12_1 := collect1(i).mag_aper_12;
          matchObj.magerr_aper_12_1 := collect1(i).magerr_aper_12;
          matchObj.mag_aper_13_1 := collect1(i).mag_aper_13;
          matchObj.magerr_aper_13_1 := collect1(i).magerr_aper_13;
          matchObj.mag_aper_14_1 := collect1(i).mag_aper_14;
          matchObj.magerr_aper_14_1 := collect1(i).magerr_aper_14;
          matchObj.mag_aper_15_1 := collect1(i).mag_aper_15;
          matchObj.magerr_aper_15_1 := collect1(i).magerr_aper_15;
          matchObj.mag_aper_16_1 := collect1(i).mag_aper_16;
          matchObj.magerr_aper_16_1 := collect1(i).magerr_aper_16;
          matchObj.mag_aper_17_1 := collect1(i).mag_aper_17;
          matchObj.magerr_aper_17_1 := collect1(i).magerr_aper_17;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          matchObj.mag_aper_7_2 := collect2(j).mag_aper_7;
          matchObj.magerr_aper_7_2 := collect2(j).magerr_aper_7;
          matchObj.mag_aper_8_2 := collect2(j).mag_aper_8;
          matchObj.magerr_aper_8_2 := collect2(j).magerr_aper_8;
          matchObj.mag_aper_9_2 := collect2(j).mag_aper_9;
          matchObj.magerr_aper_9_2 := collect2(j).magerr_aper_9;
          matchObj.mag_aper_10_2 := collect2(j).mag_aper_10;
          matchObj.magerr_aper_10_2 := collect2(j).magerr_aper_10;
          matchObj.mag_aper_11_2 := collect2(j).mag_aper_11;
          matchObj.magerr_aper_11_2 := collect2(j).magerr_aper_11;
          matchObj.mag_aper_12_2 := collect2(j).mag_aper_12;
          matchObj.magerr_aper_12_2 := collect2(j).magerr_aper_12;
          matchObj.mag_aper_13_2 := collect2(j).mag_aper_13;
          matchObj.magerr_aper_13_2 := collect2(j).magerr_aper_13;
          matchObj.mag_aper_14_2 := collect2(j).mag_aper_14;
          matchObj.magerr_aper_14_2 := collect2(j).magerr_aper_14;
          matchObj.mag_aper_15_2 := collect2(j).mag_aper_15;
          matchObj.magerr_aper_15_2 := collect2(j).magerr_aper_15;
          matchObj.mag_aper_16_2 := collect2(j).mag_aper_16;
          matchObj.magerr_aper_16_2 := collect2(j).magerr_aper_16;
          matchObj.mag_aper_17_2 := collect2(j).mag_aper_17;
          matchObj.magerr_aper_17_2 := collect2(j).magerr_aper_17;
          pipe row (matchObj);

          exit;
          end if;
        end if;
      end loop;
    end loop;

  end if;
  commit;
  return;
    
end fMatchImages_Model;
/
