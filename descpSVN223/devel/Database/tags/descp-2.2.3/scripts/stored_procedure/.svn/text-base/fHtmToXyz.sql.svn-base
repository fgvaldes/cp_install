-- compute the vector(X,Y,Z) for the given HTM ID
-- note: VARRAY subscript starts at 1, not 0
-- To run the function: select fHtmToXyz(12948186673324) from dual
create or replace function fHtmToXyz(
HId number)
return vector as

HName varchar2(80);
tri vectorTab;
v1 vector := vector(null,null,null);
v2 vector := vector(null,null,null);
v3 vector := vector(null,null,null);
w1 vector := vector(null,null,null);
w2 vector := vector(null,null,null);
w3 vector := vector(null,null,null);
t1 vector := vector(null,null,null);
t2 vector := vector(null,null,null);
t3 vector := vector(null,null,null);
result vector := vector(null,null,null);
tmp float := 0;
center_x float := 999;
center_y float := 999;
center_z float := 999;
sum_value float := 999999;
baseRec1 HTMBase;
baseRec2 HTMBase;
baseRec3 HTMBase;
baseRec4 HTMBase;
baseRec5 HTMBase;
baseRec6 HTMBase;
baseRec7 HTMBase;
baseRec8 HTMBase;
baseArray HTMBaseTab;
anchor1 vector;
anchor2 vector;
anchor3 vector;
anchor4 vector;
anchor5 vector;
anchor6 vector;
anchorArray vectorTab;



function nameToTriangle(htm_name varchar) return vectorTab is

anchor_offsets vector := vector(null,null,null);
k number(1);
offset number(2);
s char(1);
ret vectorTab;

-- copy the vector s to vector d
function copy_vec(s in vector) return vector is
new_vec vector := vector(null,null,null);
begin
  new_vec := vector(s(1), s(2), s(3));
  return new_vec;
end copy_vec;

begin
  k := ascii(substr(htm_name,2,1))-ascii('0');
  --dbms_output.put_line('k is '||k);

  if (substr(htm_name,1,1) = 'S') then
    anchor_offsets(1) := baseArray(k+1).v1;
    --dbms_output.put_line('anchor_offsets(1) is '||anchor_offsets(1));
    anchor_offsets(2) := baseArray(k+1).v2;
    --dbms_output.put_line('anchor_offsets(2) is '||anchor_offsets(2));
    anchor_offsets(3) := baseArray(k+1).v3;
    --dbms_output.put_line('anchor_offsets(3) is '||anchor_offsets(3));
  else -- for North half of sphere
    anchor_offsets(1) := baseArray(k+1+4).v1;
    --dbms_output.put_line('anchor_offsets(1) is '||anchor_offsets(1));
    anchor_offsets(2) := baseArray(k+1+4).v2;
    --dbms_output.put_line('anchor_offsets(2) is '||anchor_offsets(2));
    anchor_offsets(3) := baseArray(k+1+4).v3;
    --dbms_output.put_line('anchor_offsets(3) is '||anchor_offsets(3));
  end if;
  v1 := copy_vec(anchorArray(anchor_offsets(1)));
  v2 := copy_vec(anchorArray(anchor_offsets(2)));
  v3 := copy_vec(anchorArray(anchor_offsets(3)));

  offset := 3;
  while (offset < length(HName))
  loop
    -- m4_midpoint(v1, v2, w3)
    w3(1) := v1(1) + v2(1);
    w3(2) := v1(2) + v2(2);
    w3(3) := v1(3) + v2(3);
    tmp := sqrt(w3(1)*w3(1) + w3(2)*w3(2) + w3(3)*w3(3));
    w3(1) := w3(1)/tmp;
    w3(2) := w3(2)/tmp;
    w3(3) := w3(3)/tmp;
  
    -- m4_midpoint(v2, v3, w1)
    w1(1) := v2(1) + v3(1);
    w1(2) := v2(2) + v3(2);
    w1(3) := v2(3) + v3(3);
    tmp := sqrt(w1(1)*w1(1) + w1(2)*w1(2) + w1(3)*w1(3));
    w1(1) := w1(1)/tmp;
    w1(2) := w1(2)/tmp;
    w1(3) := w1(3)/tmp;
  
    -- m4_midpoint(v3, v1, w2)
    w2(1) := v3(1) + v1(1);
    w2(2) := v3(2) + v1(2);
    w2(3) := v3(3) + v1(3);
    tmp := sqrt(w2(1)*w2(1) + w2(2)*w2(2) + w2(3)*w2(3));
    w2(1) := w2(1)/tmp;
    w2(2) := w2(2)/tmp;
    w2(3) := w2(3)/tmp;
  
    --dbms_output.put_line('w1 is '||w1(1)||','||w1(2)||','||w1(3));
    --dbms_output.put_line('w2 is '||w2(1)||','||w2(2)||','||w2(3));
    --dbms_output.put_line('w3 is '||w3(1)||','||w3(2)||','||w3(3));
    s := substr(htm_name,offset,1);
    if s='0' then
      v2 := copy_vec(w3);
      v3 := copy_vec(w2);
    elsif s='1' then
      v1 := copy_vec(v2);
      v2 := copy_vec(w1);
      v3 := copy_vec(w3);
    elsif s='2' then
      v1 := copy_vec(v3);
      v2 := copy_vec(w2);
      v3 := copy_vec(w1);
    elsif s='3' then
      v1 := copy_vec(w1);
      v2 := copy_vec(w2);
      v3 := copy_vec(w3);
    end if;
    offset := offset + 1;
  end loop;
  ret := vectorTab(v1, v2, v3, null, null, null);
  --dbms_output.put_line('ret1 is '||ret(1)(1)||','||ret(1)(2)||','||ret(1)(3));
  --dbms_output.put_line('ret2 is '||ret(2)(1)||','||ret(2)(2)||','||ret(2)(3));
  --dbms_output.put_line('ret3 is '||ret(3)(1)||','||ret(3)(2)||','||ret(3)(3));
  return ret;
end nameToTriangle; 
    
function initHTMBase (i_name varchar, i_id number, i_v1 number, 
                      i_v2 number, i_v3 number) return HTMBase is
h HTMBase := HTMBase(null, null, null, null, null);
begin
  h.name := i_name;
  h.id := i_id;
  h.v1 := i_v1;
  h.v2 := i_v2;
  h.v3 := i_v3;
  return h;
end initHTMBase;

begin
  baseRec1 := initHTMBase('S0', 8, 2, 6, 3);
  baseRec2 := initHTMBase('S1', 9, 3, 6, 4);
  baseRec3 := initHTMBase('S2', 10, 4, 6, 5);
  baseRec4 := initHTMBase('S3', 11, 5, 6, 2);
  baseRec5 := initHTMBase('N0', 12, 2, 1, 5);
  baseRec6 := initHTMBase('N1', 13, 5, 1, 4);
  baseRec7 := initHTMBase('N2', 14, 4, 1, 3);
  baseRec8 := initHTMBase('N3', 15, 3, 1, 2);
  baseArray := HTMBaseTab(baseRec1, baseRec2, baseRec3, baseRec4,
                          baseRec5, baseRec6, baseRec7, baseRec8);

  anchor1 := vector(0, 0, 1);
  anchor2 := vector(1, 0, 0);
  anchor3 := vector(0, 1, 0);
  anchor4 := vector(-1, 0, 0);
  anchor5 := vector(0, -1, 0);
  anchor6 := vector(0, 0, -1); 

  anchorArray := vectorTab(anchor1, anchor2, anchor3, anchor4,
                                anchor5, anchor6);

  HName := fHtmToString(HId);
  --dbms_output.put_line('HName is ' || HName);
  tri := nameToTriangle(HName);
  t1 := tri(1);
  --dbms_output.put_line('t1 is '||t1(1)||','||t1(2)||','||t1(3));
  t2 := tri(2);
  --dbms_output.put_line('t2 is '||t2(1)||','||t2(2)||','||t2(3));
  t3 := tri(3);
  --dbms_output.put_line('t3 is '||t3(1)||','||t3(2)||','||t3(3));
  center_x := t1(1) + t2(1) + t3(1);
  center_y := t1(2) + t2(2) + t3(2);
  center_z := t1(3) + t2(3) + t3(3);
  sum_value := sqrt(center_x*center_x + center_y*center_y + center_z*center_z);
  center_x := center_x / sum_value;
  center_y := center_y / sum_value;
  center_z := center_z / sum_value;
  result := vector(center_x, center_y, center_z);
  return result;  

end fHtmToXyz;
/
