declare trObj truthObj := (null, null, null, null);
begin
select a.object_id, b.alpha_j2000, b.delta_j2000, b.g_mag_auto
into trObj
from table(fGetNearestEQ_truth(309.30845, -1.02886, 0.1)) a,
dc0_truth b
where a.distance < 0.1
and   a.object_id = b.truth_id;
end;
/
