-- compute the RA offset for the given DEC and radius
-- to run the program: select fGetRaOffset(1, 0.035) from dual;
create or replace function fGetRaOffset(
dec float,
radius float)
return float as

d2r float := 0.017453292519943295; -- factor to convert degree to radian
r2d float := 57.295779513082323; -- and back
Y float;
Z float;
X float;
sDECmax float;
cDECmax float;

begin
  if ((dec + radius) >= 89.999997) then
    return 1e15; -- At the poles of the sphere, you need infinite offsets!
  end if;

  Y := sin(dec * d2r);
  Z := cos(dec * d2r);
  X := cos(radius * d2r);
  sDECmax := Y/X;
  cDECmax := sqrt(1 - (sDECmax * sDECmax));
  
  return (r2d * acos((X - sDECmax * Y)/(Z * cDECmax)));
  
end fGetRaOffset;
/
