create or replace procedure count_file as

loc_id number(3);
loc_name varchar(100);
cnt number(11);

cursor name_cr is 
  select to_number(location_id), location_name
  from archive_sites;

begin
  open name_cr;
  loop
    fetch name_cr into loc_id, loc_name;
    exit when name_cr%NOTFOUND;

    dbms_output.put_line ('id is ' || loc_id);
    dbms_output.put_line ('name is ' || loc_name);
    select count(*)
    into cnt
    from location
    where substr(archivesites, loc_id, 1) != 'N';
    dbms_output.put_line (cnt || ' are not N in ' || loc_name);

    commit;
  end loop;
  
end count_file;
/
