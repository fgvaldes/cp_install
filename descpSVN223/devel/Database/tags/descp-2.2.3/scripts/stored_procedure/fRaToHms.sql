-- Convert right ascension in degrees to +hh:mm:ss.ss notation
-- To run the function: select fRaToHms(138.5) from dual
create or replace function fRaToHms(
dgr float) 
return varchar 
is
d float := abs(dgr/15.0);
nd number(3,0);
q varchar2(10);
t varchar2(16) := '';

begin
  -- get hours part
  nd := floor(d);
  if (nd < 10) then
    q := '0' || to_char(nd);
  else
    q := to_char(nd);
  end if;
  t := q;

  -- get minutes part
  d := 60.0 * (d - nd);
  nd := floor(d);
  if (nd < 10) then
    q := '0' || to_char(nd);
  else
    q := to_char(nd);
  end if;
  t := t || ':' || q;

  -- get seconds part
  d := 60.0 * (d - nd);
  if (d < 10) then 
    q := '0' || substr(to_char(d),1,4);
  else
    q := substr(to_char(d),1,5);
  end if;
  t := t || ':' || q;

  return t; 
end fRaToHms;
/

