-- This program first get the list of objects from the Objects table
-- which are located within the circle centered at the specified RA and
-- DEC with specified Radius (in Arc minute). The program then loops 
-- through this list and find the corresponding nearest object in the 
-- DC0_Truth table. The program outputs the following values:
-- truth object id, observed object id, truth RA, truth DEC, distance 
-- between RA, distance between DEC, observed object's magnitude and errors,
-- magnitude difference between two objects.
-- To run the procedure: select * from table(fMatchObjects(332.36783, -0.45885, 1));
create or replace function fMatchObjects(
ra float,
dec float,
r float)
return MatchTruthObjTab pipelined
is
pragma autonomous_transaction;
matchTrobj matchTruthObj := matchTruthObj(null,null,null,null,null,null,null,null,null);
trObj truthObj := truthObj(null,null,null,null);
vec vector := vector(null,null,null);
r_ float;
d float;
nx float;
ny float;
nz float;
close_r float := 1;
distance_ra float;
--scale_ra float;

cursor mstfrc is 
  select /*+ objects pk_objects */
       a.object_id, b.band, b.alpha_j2000, b.delta_j2000, 
       b.mag_auto, b.magerr_auto
  from table(fGetNearbyXYZpre(nx,ny,nz,r_)) a, objects b 
  where a.distance < r_
  and   a.object_id = b.object_id;
begin
    r_ := r;
    if (r_<0.1) then
      r_ := 0.1;
    end if;
    vec := fEqToXyz(ra, dec);
    nx := vec(1);
    ny := vec(2);
    nz := vec(3);
    for mstfrc_rec in mstfrc
    loop
      matchTrobj.o_obj_id := mstfrc_rec.object_id;
      matchTrobj.o_mag_auto := mstfrc_rec.mag_auto;
      matchTrobj.o_magerr_auto := mstfrc_rec.magerr_auto;
      if (mstfrc_rec.band = 'g') then
        select a.object_id, b.alpha_j2000, b.delta_j2000, b.g_mag_auto
        into trObj.t_obj_id, trObj.t_alpha_j2000, trObj.t_delta_j2000,
             trObj.t_mag_auto
        from table(fGetNearestEQ_truth(mstfrc_rec.alpha_j2000, 
                                       mstfrc_rec.delta_j2000, close_r)) a,
             dc0_truth b
        where a.distance < close_r
        and   a.object_id = b.truth_id;
      elsif (mstfrc_rec.band = 'r') then
        select a.object_id, b.alpha_j2000, b.delta_j2000, b.r_mag_auto
        into trObj.t_obj_id, trObj.t_alpha_j2000, trObj.t_delta_j2000,
             trObj.t_mag_auto
        from table(fGetNearestEQ_truth(mstfrc_rec.alpha_j2000, 
                                       mstfrc_rec.delta_j2000, close_r)) a,
             dc0_truth b
        where a.distance < close_r
        and   a.object_id = b.truth_id;
      elsif (mstfrc_rec.band = 'i') then
        select a.object_id, b.alpha_j2000, b.delta_j2000, b.i_mag_auto
        into trObj.t_obj_id, trObj.t_alpha_j2000, trObj.t_delta_j2000,
             trObj.t_mag_auto
        from table(fGetNearestEQ_truth(mstfrc_rec.alpha_j2000, 
                                       mstfrc_rec.delta_j2000, close_r)) a,
             dc0_truth b
        where a.distance < close_r
        and   a.object_id = b.truth_id;
      elsif (mstfrc_rec.band = 'z') then
        select a.object_id, b.alpha_j2000, b.delta_j2000, b.z_mag_auto
        into trObj.t_obj_id, trObj.t_alpha_j2000, trObj.t_delta_j2000,
             trObj.t_mag_auto
        from table(fGetNearestEQ_truth(mstfrc_rec.alpha_j2000, 
                                       mstfrc_rec.delta_j2000, close_r)) a,
             dc0_truth b
        where a.distance < close_r
        and   a.object_id = b.truth_id;
      elsif (mstfrc_rec.band = 'u') then
        select a.object_id, b.alpha_j2000, b.delta_j2000, b.u_mag_auto
        into trObj.t_obj_id, trObj.t_alpha_j2000, trObj.t_delta_j2000,
             trObj.t_mag_auto
        from table(fGetNearestEQ_truth(mstfrc_rec.alpha_j2000, 
                                       mstfrc_rec.delta_j2000, close_r)) a,
             dc0_truth b
        where a.distance < close_r
        and   a.object_id = b.truth_id;
      end if; 
      matchTrobj.t_obj_id := trObj.t_obj_id;
      matchTrobj.t_alpha_j2000 := trObj.t_alpha_j2000;
      matchTrobj.t_delta_j2000 := trObj.t_delta_j2000;
      matchTrobj.mag_diff := matchTrobj.o_mag_auto - trObj.t_mag_auto;
      -- all distance calculation is based on Joe Mohr's algorithm
      -- calculate the distance on DEC
      matchTrobj.distance_dec := mstfrc_rec.delta_j2000 - 
                                 matchTrobj.t_delta_j2000; 
      -- calculate the distance on RA
      distance_ra := mstfrc_rec.alpha_j2000 - matchTrobj.t_alpha_j2000;
      if (distance_ra > 180.0) then
        distance_ra := distance_ra - 360.0;
      elsif (distance_ra <= -180.0) then
        distance_ra := distance_ra + 360.0;
      end if;
      matchTrobj.distance_ra := distance_ra;

      --scale_ra := cos(0.5*(mstfrc_rec.delta_j2000 + matchTrobj.t_delta_j2000));
      --matchTrobj.distance := sqrt(
        --(matchTrobj.distance_dec * matchTrobj.distance_dec) +
        --(scale_ra * distance_ra * scale_ra * distance_ra)); 
    
      pipe row (matchTrobj);
    end loop;
    commit;
    return;
end fMatchObjects;
/
