-- This program matches the objects in two tables. 
-- This program is for select_table=catsim1_truth, match_table=objects
-- The program takes 5 parameters: min of ra, min of dec, max of ra, max of dec,
-- and threshold of radius)
-- The program outputs a list of matched objects: the selected object_id and
-- the matched object_id.
create or replace function fMatchCatSim1Obj(
ra_lo float,
dec_lo float,
ra_hi float,
dec_hi float,
threshold float)
return objectMapTab pipelined
as
pragma autonomous_transaction;

mapObj objectMap := objectMap(null,null);
vec vector := vector(null,null,null);
vec1 vector := vector(null,null,null);
vec2 vector := vector(null,null,null);
ra float;
dec float;
d1 float;
d2 float;
dot float;
nx float;
ny float;
nz float;
nx1 float;
ny1 float;
nz1 float;
nx2 float;
ny2 float;
nz2 float;
r1 float;
r2 float; 
rlevel number(2);
clevel number(2);
cmd1 varchar2(200);
cmd2 varchar2(200);
truth_id number(11);
select_x float;
select_y float;
select_z float;
distance float;

cursor selectCsr is 
  select /*+ ordered USE_NL(b) INDEX(b catsim1truth_htmid_idx) */
       catsim1_truth_id truth_id, cx select_x, cy select_y, cz select_z 
  from table(fHtmCover(cmd1,r1)) a, catsim1_truth b
  where b.htmid >= a.htm_id_start
  and   b.htmid <= a.htm_id_end
  and   (cz>nz1) 
  and   (cz<nz2)
  and   (-cx*ny1 + cy*nx1) > 0
  and   (cx*ny2 - cy*nx2) > 0;

cursor matchCsr is 
  select /*+ ordered USE_NL(b) INDEX(b objects_htmid_idx) */
       object_id, cx match_x, cy match_y, cz match_z
  from table(fHtmCover(cmd2,r2)) a, objects b
  where b.htmid >= a.htm_id_start
  and   b.htmid <= a.htm_id_end;

begin
    if (threshold < 0.1) then
      r2 := 0.1;
    else
      r2 := threshold;
    end if;
    vec1 := fEqToXyz(ra_lo, dec_lo);
    nx1 := vec1(1);
    ny1 := vec1(2);
    nz1 := vec1(3); 

    vec2 := fEqToXyz(ra_hi, dec_hi);
    nx2 := vec2(1);
    ny2 := vec2(2);
    nz2 := vec2(3); 

    -- dealing the wrap around problem, such as ra_lo=355, ra_hi=12
    if (ra_lo < ra_hi) then
      ra := (ra_lo + ra_hi)/2;
    else
      ra := (360 - ra_lo + ra_hi)/2;
      -- case (ra_lo=355, ra_hi=12) 
      if (ra < ra_hi) then
        ra := ra_hi - ra; -- 8.5 < 12, ra=12-8.5
      -- case (ra_lo=300, ra_hi=6)
      else 
        ra := ra_lo + ra; -- 33 > 6, ra=300+33
      end if;
    end if; 
    dec := (dec_lo + dec_hi)/2;
    vec := fEqToXyz(ra, dec);
    nx := vec(1);
    ny := vec(2);
    nz := vec(3); 

    d1 := nx1*nx + ny1*ny + nz1*nz;
    d2 := nx2*nx + ny2*ny + nz2*nz;

    if (d1<d2) then
      dot := d1;
    else
      dot := d2;
    end if;

    r1 := acos(dot)/(3.1415926535897932385E0/180)*60; 

    -- determine the level of HTMID for rectangle search on the select table
    rlevel := 13 - (floor(log(10,r1)/log(10,2.0)));
    if (rlevel < 5) then
      rlevel := 5;
    elsif (rlevel > 13) then
      rlevel := 13;
    end if;
    cmd1 := to_char(rlevel) || ' ' || to_char(nx) || ' ' || to_char(ny) || ' ' 
            || to_char(nz) || ' ' || to_char(r1);

    for selectCsr_rec in selectCsr
    loop
      begin
        truth_id := selectCsr_rec.truth_id;
        select_x := selectCsr_rec.select_x;
        select_y := selectCsr_rec.select_y;
        select_z := selectCsr_rec.select_z;

    	-- determine the level of HTMID for circle search on the match table
        clevel := 13 - (floor(log(10,r2)/log(10,2.0)));
        if (clevel < 5) then
          clevel := 5;
        elsif (clevel > 13) then
          clevel := 13;
        end if;
        cmd2 := to_char(clevel) || ' ' || to_char(select_x) || ' ' 
             || to_char(select_y) || ' ' || to_char(select_z) || ' ' 
             || to_char(r2);
        for matchCsr_rec in matchCsr
        loop
          begin 
            distance := fGetDistanceXYZ(select_x,select_y,select_z,
                      matchCsr_rec.match_x,matchCsr_rec.match_y,
                      matchCsr_rec.match_z);
            if (distance < threshold) then
              mapObj.select_object_id := truth_id;
              mapObj.match_object_id := matchCsr_rec.object_id; 
              pipe row (mapObj);
            end if;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                NULL;
          end;
        end loop;

        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            NULL;
      end;
    end loop;
    commit;
    return;
    
end fMatchCatSim1Obj;
/
