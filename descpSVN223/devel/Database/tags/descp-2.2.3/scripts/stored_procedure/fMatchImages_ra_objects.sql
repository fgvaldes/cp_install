-- This stored procedure matches the objects in two images
-- The program has 7 input parameters:
-- 1. image1 (imageid for the first image)
-- 2. image2 (imageid for the second image)
-- 3. stella_lo (minimum value for class_star)
-- 4. stella_hi (maximum value for class_star)
-- 5. flag_upperlim (upperlim for flags)
-- 6. distance_threshold (matching tolerance in arc minute, 0.034 = 2 arc seconds)
-- 7. magerr_filter (magnitude error filter used: magerr_auto, magerr_aper_1, etc)
-- 8. magerr_limit (magnitude error limit, only the objects with error < magerr_limit will be matched)
-- The program returns a table which has 36 columns:
-- object_id_1, band_1, class_star_1, flags_1, mag_auto_1, magerr_auto_1, 
-- mag_aper_1_1, magerr_aper_1_1, mag_aper_2_1, magerr_aper_2_1,
-- mag_aper_3_1, magerr_aper_3_1, mag_aper_4_1, magerr_aper_4_1,
-- mag_aper_5_1, magerr_aper_5_1, mag_aper_6_1, magerr_aper_6_1,
-- object_id_2, band_2, class_star_2, flags_2, mag_auto_2, magerr_auto_2, 
-- mag_aper_1_2, magerr_aper_1_2, mag_aper_2_2, magerr_aper_2_2,
-- mag_aper_3_2, magerr_aper_3_2, mag_aper_4_2, magerr_aper_4_2,
-- mag_aper_5_2, magerr_aper_5_2, mag_aper_6_2, magerr_aper_6_2
-- to match image 1627161 and image 1553539 with class_star from 0.75 to 1.0,
-- flag upperlimit 0, matching tolerance 2 arc seconds, using 'magerr_aper_5' as
-- error filter and magnitude error < 0.10:
-- select * from table(fMatchImages_ra_objects(1627161, 1553539, 0.75, 1.0, 0, 0.032, 'magerr_aper_5', 0.10));

create or replace function fMatchImages_ra_objects(
image1 number,
image2 number, 
stella_lo float,
stella_hi float,
flag_upperlim number,
distance_threshold float,
magerr_filter varchar,
magerr_limit float)
return MatchImgObjTab pipelined
as
pragma autonomous_transaction;


matchObj MatchImgObj := MatchImgObj(null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null,null,null,
                                    null,null,null,null,null,null);
min_ra1 float;
max_ra1 float;
min_dec1 float;
max_dec1 float;
min_ra2 float;
max_ra2 float;
min_dec2 float;
max_dec2 float;
min_ra float;
max_ra float;
min_dec float;
max_dec float;
distance float;
type list is RECORD (
  imageid number(9), 
  object_id number(11), 
  mag number(7,4), 
  magerr number(7,4), 
  mag_aper_1 number(7,4), 
  magerr_aper_1 number(7,4),
  mag_aper_2 number(7,4), 
  magerr_aper_2 number(7,4),
  mag_aper_3 number(7,4), 
  magerr_aper_3 number(7,4),
  mag_aper_4 number(7,4), 
  magerr_aper_4 number(7,4),
  mag_aper_5 number(7,4), 
  magerr_aper_5 number(7,4),
  mag_aper_6 number(7,4), 
  magerr_aper_6 number(7,4),
  cx number(10,6), 
  cy number(10,6), 
  cz number(10,6), 
  band varchar(10), 
  class_star number(3,2), 
  flags number(3), 
  alpha_j2000 number(8,5), 
  delta_j2000 number(8,5)
);

type obj_collection is table of list;

collect1 obj_collection;
collect2 obj_collection;

cursor selectCsr_auto is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from ra_objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_auto < magerr_limit; 

cursor selectCsr_1 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from ra_objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_1 < magerr_limit; 

cursor selectCsr_2 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from ra_objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_2 < magerr_limit; 

cursor selectCsr_3 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from ra_objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_3 < magerr_limit; 

cursor selectCsr_4 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from ra_objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_4 < magerr_limit; 

cursor selectCsr_5 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from ra_objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_5 < magerr_limit; 

cursor selectCsr_6 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from ra_objects a
  where imageid = image1
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
  and   flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_6 < magerr_limit; 

cursor matchCsr_auto is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from  ra_objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_auto < magerr_limit
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
; 

cursor matchCsr_1 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from  ra_objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_1 < magerr_limit
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
; 

cursor matchCsr_2 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from  ra_objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_2 < magerr_limit
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
; 

cursor matchCsr_3 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from  ra_objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_3 < magerr_limit
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
; 

cursor matchCsr_4 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from  ra_objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_4 < magerr_limit
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
; 

cursor matchCsr_5 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from  ra_objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_5 < magerr_limit
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
;

cursor matchCsr_6 is 
  select imageid, object_id, 
         mag_auto, magerr_auto, 
         mag_aper_1, magerr_aper_1,
         mag_aper_2, magerr_aper_2,
         mag_aper_3, magerr_aper_3,
         mag_aper_4, magerr_aper_4,
         mag_aper_5, magerr_aper_5,
         mag_aper_6, magerr_aper_6,
         cx, cy, cz, band, 
         class_star, flags, 
         alpha_j2000, delta_j2000
  from  ra_objects b
  where b.imageid = image2 
  and   b.flags <= flag_upperlim
  and   class_star >= stella_lo
  and   class_star <= stella_hi
  and   magerr_aper_6 < magerr_limit
  and   alpha_j2000 between min_ra and max_ra
  and   delta_j2000 between min_dec and max_dec
;

begin
  -- find the overlap part of these two images
  select min(alpha_j2000), max(alpha_j2000), 
         min(delta_j2000), max(delta_j2000)
  into min_ra1, max_ra1, min_dec1, max_dec1
  from ra_objects
  where imageid = image1;
  select min(alpha_j2000), max(alpha_j2000), 
         min(delta_j2000), max(delta_j2000)
  into min_ra2, max_ra2, min_dec2, max_dec2
  from ra_objects
  where imageid = image2;
  min_ra := greatest(min_ra1, min_ra2) - distance_threshold;
  max_ra := least(max_ra1, max_ra2) + distance_threshold;
  min_dec := greatest(min_dec1, min_dec2) - distance_threshold;
  max_dec := least(max_dec1, max_dec2) + distance_threshold;

  -- magerr_auto is used for filtering
  if (lower(magerr_filter) = 'magerr_auto') then
    open selectCsr_auto;
    open matchCsr_auto;

    fetch selectCsr_auto bulk collect into collect1;
    fetch matchCsr_auto bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alpha_j2000 >= (collect1(i).alpha_j2000 - distance_threshold)
          and collect2(j).alpha_j2000 <= (collect1(i).alpha_j2000 + distance_threshold)
          and collect2(j).delta_j2000 >= (collect1(i).delta_j2000 - distance_threshold)
          and collect2(j).delta_j2000 <= (collect1(i).delta_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          pipe row (matchObj);

          exit;
        end if;
      end loop;
    end loop;

  -- magerr_aper_1 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_1') then
    open selectCsr_1;
    open matchCsr_1;

    fetch selectCsr_1 bulk collect into collect1;
    fetch matchCsr_1 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alpha_j2000 >= (collect1(i).alpha_j2000 - distance_threshold)
          and collect2(j).alpha_j2000 <= (collect1(i).alpha_j2000 + distance_threshold)
          and collect2(j).delta_j2000 >= (collect1(i).delta_j2000 - distance_threshold)
          and collect2(j).delta_j2000 <= (collect1(i).delta_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          pipe row (matchObj);

          exit;
        end if;
      end loop;
    end loop;

  -- magerr_aper_2 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_2') then
    open selectCsr_2;
    open matchCsr_2;

    fetch selectCsr_2 bulk collect into collect1;
    fetch matchCsr_2 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alpha_j2000 >= (collect1(i).alpha_j2000 - distance_threshold)
          and collect2(j).alpha_j2000 <= (collect1(i).alpha_j2000 + distance_threshold)
          and collect2(j).delta_j2000 >= (collect1(i).delta_j2000 - distance_threshold)
          and collect2(j).delta_j2000 <= (collect1(i).delta_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          pipe row (matchObj);

          exit;
        end if;
      end loop;
    end loop;

  -- magerr_aper_3 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_3') then
    open selectCsr_3;
    open matchCsr_3;

    fetch selectCsr_3 bulk collect into collect1;
    fetch matchCsr_3 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alpha_j2000 >= (collect1(i).alpha_j2000 - distance_threshold)
          and collect2(j).alpha_j2000 <= (collect1(i).alpha_j2000 + distance_threshold)
          and collect2(j).delta_j2000 >= (collect1(i).delta_j2000 - distance_threshold)
          and collect2(j).delta_j2000 <= (collect1(i).delta_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          pipe row (matchObj);

          exit;
        end if;
      end loop;
    end loop;


  -- magerr_aper_4 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_4') then
    open selectCsr_4;
    open matchCsr_4;

    fetch selectCsr_4 bulk collect into collect1;
    fetch matchCsr_4 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alpha_j2000 >= (collect1(i).alpha_j2000 - distance_threshold)
          and collect2(j).alpha_j2000 <= (collect1(i).alpha_j2000 + distance_threshold)
          and collect2(j).delta_j2000 >= (collect1(i).delta_j2000 - distance_threshold)
          and collect2(j).delta_j2000 <= (collect1(i).delta_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          pipe row (matchObj);

          exit;
        end if;
      end loop;
    end loop;


  -- magerr_aper_5 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_5') then
    open selectCsr_5;
    open matchCsr_5;

    fetch selectCsr_5 bulk collect into collect1;
    fetch matchCsr_5 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alpha_j2000 >= (collect1(i).alpha_j2000 - distance_threshold)
          and collect2(j).alpha_j2000 <= (collect1(i).alpha_j2000 + distance_threshold)
          and collect2(j).delta_j2000 >= (collect1(i).delta_j2000 - distance_threshold)
          and collect2(j).delta_j2000 <= (collect1(i).delta_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          pipe row (matchObj);

          exit;
        end if;
      end loop;
    end loop;

  -- magerr_aper_6 is used for filtering
  elsif (lower(magerr_filter) = 'magerr_aper_6') then
    open selectCsr_6;
    open matchCsr_6;

    fetch selectCsr_6 bulk collect into collect1;
    fetch matchCsr_6 bulk collect into collect2;

    for i in 1 .. collect1.count
    loop
      for j in 1 .. collect2.count
      loop

        if( collect2(j).alpha_j2000 >= (collect1(i).alpha_j2000 - distance_threshold)
          and collect2(j).alpha_j2000 <= (collect1(i).alpha_j2000 + distance_threshold)
          and collect2(j).delta_j2000 >= (collect1(i).delta_j2000 - distance_threshold)
          and collect2(j).delta_j2000 <= (collect1(i).delta_j2000 + distance_threshold)
          and (fgetdistancexyz(collect1(i).cx,collect1(i).cy,
                               collect1(i).cz,collect2(j).cx,
                               collect2(j).cy,collect2(j).cz) < distance_threshold)
)       then
          matchObj.imageid_1 := collect1(i).imageid;
          matchObj.object_id_1 := collect1(i).object_id;
          matchObj.band_1 := collect1(i).band; 
          matchObj.class_star_1 := collect1(i).class_star; 
          matchObj.flags_1 := collect1(i).flags; 
          matchObj.mag_auto_1 := collect1(i).mag;
          matchObj.magerr_auto_1 := collect1(i).magerr;
          matchObj.mag_aper_1_1 := collect1(i).mag_aper_1;
          matchObj.magerr_aper_1_1 := collect1(i).magerr_aper_1;
          matchObj.mag_aper_2_1 := collect1(i).mag_aper_2;
          matchObj.magerr_aper_2_1 := collect1(i).magerr_aper_2;
          matchObj.mag_aper_3_1 := collect1(i).mag_aper_3;
          matchObj.magerr_aper_3_1 := collect1(i).magerr_aper_3;
          matchObj.mag_aper_4_1 := collect1(i).mag_aper_4;
          matchObj.magerr_aper_4_1 := collect1(i).magerr_aper_4;
          matchObj.mag_aper_5_1 := collect1(i).mag_aper_5;
          matchObj.magerr_aper_5_1 := collect1(i).magerr_aper_5;
          matchObj.mag_aper_6_1 := collect1(i).mag_aper_6;
          matchObj.magerr_aper_6_1 := collect1(i).magerr_aper_6;
          matchObj.imageid_2 := collect2(j).imageid;
          matchObj.object_id_2 := collect2(j).object_id;
          matchObj.band_2 := collect2(j).band; 
          matchObj.class_star_2 := collect2(j).class_star; 
          matchObj.flags_2 := collect2(j).flags; 
          matchObj.mag_auto_2 := collect2(j).mag;
          matchObj.magerr_auto_2 := collect2(j).magerr;
          matchObj.mag_aper_1_2 := collect2(j).mag_aper_1;
          matchObj.magerr_aper_1_2 := collect2(j).magerr_aper_1;
          matchObj.mag_aper_2_2 := collect2(j).mag_aper_2;
          matchObj.magerr_aper_2_2 := collect2(j).magerr_aper_2;
          matchObj.mag_aper_3_2 := collect2(j).mag_aper_3;
          matchObj.magerr_aper_3_2 := collect2(j).magerr_aper_3;
          matchObj.mag_aper_4_2 := collect2(j).mag_aper_4;
          matchObj.magerr_aper_4_2 := collect2(j).magerr_aper_4;
          matchObj.mag_aper_5_2 := collect2(j).mag_aper_5;
          matchObj.magerr_aper_5_2 := collect2(j).magerr_aper_5;
          matchObj.mag_aper_6_2 := collect2(j).mag_aper_6;
          matchObj.magerr_aper_6_2 := collect2(j).magerr_aper_6;
          pipe row (matchObj);

          exit;
        end if;
      end loop;
    end loop;

  end if;
  commit;
  return;
    
end fMatchImages_ra_objects;
/
