-- compute the HTM ID for the given ra and dec
-- note: VARRAY subscript starts at 1, not 0
-- To run the function: select fEqToHtm(345, -45) from dual
create or replace function fEqToHtm(
ra number,
dec number)
return number as

depth number(2) := 20; 
x float;
y float;
z float;
HName varchar2(80);
HId number;
cd float;
v1 vector;
v2 vector;
v3 vector;
w1 vector := vector(null,null,null);
w2 vector := vector(null,null,null);
w3 vector := vector(null,null,null);
tvec vector;
p vector;
tmp float;
baseindex number(2);
baseID number(2);
baseRec1 HTMBase;
baseRec2 HTMBase;
baseRec3 HTMBase;
baseRec4 HTMBase;
baseRec5 HTMBase;
baseRec6 HTMBase;
baseRec7 HTMBase;
baseRec8 HTMBase;
baseArray HTMBaseTab;
anchor1 vector;
anchor2 vector;
anchor3 vector;
anchor4 vector;
anchor5 vector;
anchor6 vector;
anchorArray vectorTab;

-- constants
pr number(21,20) := 3.1415926535897932385E0/180;
gEpsilon number(17,15) := 1.0E-15;
iS2 number(1) := 1;
iN1 number(1) := 2;
iS1 number(1) := 3;
iN2 number(1) := 4;
iS3 number(1) := 5;
iN0 number(1) := 6;
iS0 number(1) := 7;
iN3 number(1) := 8;

-- Is the given vector p (made of x, y, and z) is contained in the triangle 
-- whose corners are given by the vectors a1, a2 and a3?
function isinside (p vector, a1 vector, 
                   a2 vector, a3 vector) return Boolean is
crossp vector := vector(null,null,null);
begin
  crossp(1) := a1(2)*a2(3) - a2(2)*a1(3);
  crossp(2) := a1(3)*a2(1) - a2(3)*a1(1);
  crossp(3) := a1(1)*a2(2) - a2(1)*a1(2);
  if ((p(1)*crossp(1) + p(2)*crossp(2) + p(3)*crossp(3)) < (-1*gEpsilon)) then
    return FALSE;
  end if;
  crossp(1) := a2(2)*a3(3) - a3(2)*a2(3);
  crossp(2) := a2(3)*a3(1) - a3(3)*a2(1);
  crossp(3) := a2(1)*a3(2) - a3(1)*a2(2);
  if ((p(1)*crossp(1) + p(2)*crossp(2) + p(3)*crossp(3)) < (-1*gEpsilon)) then
    return FALSE;
  end if;
  crossp(1) := a3(2)*a1(3) - a1(2)*a3(3);
  crossp(2) := a3(3)*a1(1) - a1(3)*a3(1);
  crossp(3) := a3(1)*a1(2) - a1(1)*a3(2);
  if ((p(1)*crossp(1) + p(2)*crossp(2) + p(3)*crossp(3)) < (-1*gEpsilon)) then
    return FALSE;
  end if;
  return TRUE;
end isinside;

-- copy the vector s to vector d
function copy_vec(s in vector) return vector is
new_vec vector := vector(null,null,null);
begin
  new_vec := vector(s(1), s(2), s(3));
  return new_vec;
end copy_vec;

-- convert a binary string to a decimal number
function bin2int( bin in varchar2 ) return number
is
bin_number number default 0;
bin_bytes number default length(bin);
begin
 --dbms_output.put_line('bin string is ' || bin);
 for i in 1 .. bin_bytes loop
   bin_number := bin_number + (ascii(substr(bin,i,1))-ascii('0')) * 
   power(2,(bin_bytes-i));
 end loop;
 return bin_number;
end bin2int; 

-- convert HTM name to HTM id in decimal string
function name2id (HTMname varchar) return number is
HTMid varchar(80);
HTMnum number;
begin
  for i in 1 .. length(HTMname) loop
    if substr(HTMname,i,1) = 'N' then
      HTMid := HTMid || '11';
    elsif substr(HTMname,i,1) = 'S' then
      HTMid := HTMid || '10';
    elsif substr(HTMname,i,1) = '0' then
      HTMid := HTMid || '00';
    elsif substr(HTMname,i,1) = '1' then
      HTMid := HTMid || '01';
    elsif substr(HTMname,i,1) = '2' then
      HTMid := HTMid || '10';
    elsif substr(HTMname,i,1) = '3' then
      HTMid := HTMid || '11';
    end if;
  end loop;
  HTMnum := bin2int(HTMid);
  return (HTMnum);
end;

function initHTMBase (i_name varchar, i_id number, i_v1 number, 
                      i_v2 number, i_v3 number) return HTMBase is
h HTMBase := HTMBase(null, null, null, null, null);
begin
  h.name := i_name;
  h.id := i_id;
  h.v1 := i_v1;
  h.v2 := i_v2;
  h.v3 := i_v3;
  return h;
end;

begin
  baseRec1 := initHTMBase('S2', 10, 4, 6, 5);
  baseRec2 := initHTMBase('N1', 13, 5, 1, 4);
  baseRec3 := initHTMBase('S1', 9, 3, 6, 4);
  baseRec4 := initHTMBase('N2', 14, 4, 1, 3);
  baseRec5 := initHTMBase('S3', 11, 5, 6, 2);
  baseRec6 := initHTMBase('N0', 12, 2, 1, 5);
  baseRec7 := initHTMBase('S0', 8, 2, 6, 3);
  baseRec8 := initHTMBase('N3', 15, 3, 1, 2);
  baseArray := HTMBaseTab(baseRec1, baseRec2, baseRec3, baseRec4,
                          baseRec5, baseRec6, baseRec7, baseRec8);
  anchor1 := vector(0, 0, 1);
  anchor2 := vector(1, 0, 0);
  anchor3 := vector(0, 1, 0);
  anchor4 := vector(-1, 0, 0);
  anchor5 := vector(0, -1, 0);
  anchor6 := vector(0, 0, -1); 

  anchorArray := vectorTab(anchor1, anchor2, anchor3, anchor4,
                                anchor5, anchor6);

  cd := cos(dec * pr);
  x := cos(ra * pr) * cd;
  y := sin(ra * pr) * cd;
  z := sin(dec * pr);

  p := vector(x, y, z);

  if ((x > 0) and (y >= 0)) then
    if (z >= 0) then
      baseindex := iN3;
    else
      baseindex := iS0;
    end if;
  elsif ((x <= 0) and (y > 0)) then
    if (z >= 0) then
      baseindex := iN2;
    else
      baseindex := iS1;
    end if;
  elsif ((x < 0) and (y <= 0)) then
    if (z >= 0) then
      baseindex := iN1;
    else
      baseindex := iS2;
    end if;
  elsif ((x >= 0) and (y < 0)) then
    if (z >= 0) then
      baseindex := iN0;
    else 
      baseindex := iS3;
    end if;
  end if;
  
  baseID := baseArray(baseindex).ID;

  tvec := vector((anchorArray(baseArray(baseindex).v1)(1)),
                          (anchorArray(baseArray(baseindex).v1)(2)),  
                          (anchorArray(baseArray(baseindex).v1)(3)));  
  v1 := vector(tvec(1), tvec(2), tvec(3));

  tvec := vector((anchorArray(baseArray(baseindex).v2)(1)),
                          (anchorArray(baseArray(baseindex).v2)(2)),  
                          (anchorArray(baseArray(baseindex).v2)(3)));  
  v2 := vector(tvec(1), tvec(2), tvec(3));
  
  tvec := vector((anchorArray(baseArray(baseindex).v3)(1)),
                          (anchorArray(baseArray(baseindex).v3)(2)),  
                          (anchorArray(baseArray(baseindex).v3)(3)));  
  v3 := vector(tvec(1), tvec(2), tvec(3));
  
  HName := baseArray(baseindex).name;

  while(depth > 0)
  loop
    -- m4_midpoint(v1, v2, w3)
    w3(1) := v1(1) + v2(1);
    w3(2) := v1(2) + v2(2);
    w3(3) := v1(3) + v2(3);
    tmp := sqrt(w3(1)*w3(1) + w3(2)*w3(2) + w3(3)*w3(3));
    w3(1) := w3(1)/tmp;
    w3(2) := w3(2)/tmp;
    w3(3) := w3(3)/tmp;
  
    -- m4_midpoint(v2, v3, w1)
    w1(1) := v2(1) + v3(1);
    w1(2) := v2(2) + v3(2);
    w1(3) := v2(3) + v3(3);
    tmp := sqrt(w1(1)*w1(1) + w1(2)*w1(2) + w1(3)*w1(3));
    w1(1) := w1(1)/tmp;
    w1(2) := w1(2)/tmp;
    w1(3) := w1(3)/tmp;
  
    -- m4_midpoint(v3, v1, w2)
    w2(1) := v3(1) + v1(1);
    w2(2) := v3(2) + v1(2);
    w2(3) := v3(3) + v1(3);
    tmp := sqrt(w2(1)*w2(1) + w2(2)*w2(2) + w2(3)*w2(3));
    w2(1) := w2(1)/tmp;
    w2(2) := w2(2)/tmp;
    w2(3) := w2(3)/tmp;
  
    if (isinside(p, v1, w3, w2)) then
      HName := HName || '0';
      v2 := copy_vec(w3);
      v3 := copy_vec(w2);
    elsif (isinside(p, v2, w1, w3)) then
      HName := HName || '1';
      v1 := copy_vec(v2);
      v2 := copy_vec(w1);
      v3 := copy_vec(w3);
    elsif (isinside(p, v3, w2, w1)) then
      HName := HName || '2';
      v1 := copy_vec(v3);
      v2 := copy_vec(w2);
      v3 := copy_vec(w1);
    elsif (isinside(p, w1, w2, w3)) then
      HName := HName || '3';
      v1 := copy_vec(w1);
      v2 := copy_vec(w2);
      v3 := copy_vec(w3);
    end if; 
    depth := depth - 1;
  end loop;

  HId := name2id(HName);
  return HId;   

end fEqToHtm;
/
