SQL> update files
  2  set archivesites = 'NNNNNNNNNNNNNNNNNNNNNNNNN';

SQL> var cnt number;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'src', '%', 'SPT2007%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'src', '%', 'bcs05%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'zero', '%', 'bcs05%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'object', '%', 'bcs05%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'dome flat', '%', 'bcs05%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'src', '%', 'bcs06%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'zero', '%', 'bcs06%', '%', '%', '%', 0)

SQL> print cnt

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'focus', '%', 'bcs06%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'object', '%', 'bcs06%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'dome flat', '%', 'bcs06%', '%', '%', '%', 0)
SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'src', '%', 'des2007%', '%', '%', '%', 0)

SQL> print cnt

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'zero', '%', 'des2007%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'object', '%', 'des2007%', '%', '%', '%', 0)

SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'dome flat', '%', 'des2007%', '%', '%', '%', 0)
SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'src', '%', 'scs2006%', '%', '%', '%', 0)
SQL> print cnt;

SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'zero', '%', 'scs2006%', '%', '%', '%', 0)
SQL> print cnt;

             CNT                                                                
----------------                                                                
             432                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'focus', '%', 'scs2006%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              24                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'object', '%', 'scs2006%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.38
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            2728                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'dome flat', '%', 'scs2006%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.29
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1440                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'catalog', '%', '%', 'DES20070907_des20070907', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.01
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1916                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'reduced', '%', '%', 'DES20070907_des20070907', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.41
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             992                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'remap', '%', '%', 'DES20070907_des20070907', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1916                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'catalog', '%', '%', 'DES20071016_des20071005%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.48
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           51060                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'reduced', '%', '%', 'DES20071016_des20071005%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.93
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           17608                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'remap', '%', '%', 'DES20071016_des20071005%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.06
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           33452                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'catalog', '%', '%', 'DES20071017_des20071006%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.59
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           51342                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'reduced', '%', '%', 'DES20071017_des20071006%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.87
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           17608                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('mss', 'add', '%', 'remap', '%', '%', 'DES20071017_des20071006%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.23
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           33734                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'src', '%', 'bcs0511%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.25
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1057                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'zero', '%', 'bcs0511%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.25
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             656                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'object', '%', 'bcs0511%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.34
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            5192                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'dome flat', '%', 'bcs0511%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.26
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt'
ERROR:
ORA-00923: FROM keyword not found where expected 


SQL> print cnt;

             CNT                                                                
----------------                                                                
            2144                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'src', '%', 'bcs0512%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.26
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
             825                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'zero', '%', 'bcs0512%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.25
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
             544                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'object', '%', 'bcs0512%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.34
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            3832                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'dome flat', '%', 'bcs0512%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.30
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            2184                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'src', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.30
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1979                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'zero', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.25
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1512                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'focus', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.24
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              16                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'object', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.36
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            8656                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'dome flat', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            5640                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'src', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.29
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             793                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'focus', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.24
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              88                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'zero', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.25
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             320                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'object', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.33
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            2848                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'dome flat', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.29
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1184                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'zero', '%', 'scs200710%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.25
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              80                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'object', '%', 'scs200710%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.33
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             600                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'dome flat', '%', 'scs200710%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.28
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             288                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'src', '%', 'bcs05%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.28
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1882                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'zero', '%', 'bcs05%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.29
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1200                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'object', '%', 'bcs05%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            9024                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'dome flat', '%', 'bcs05%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:02.83
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            4328                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'src', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.30
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1979                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'zero', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.29
SQL> /

Commit complete.

Elapsed: 00:00:00.01
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1512                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'focus', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              16                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'object', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.46
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            8656                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'dome flat', '%', 'bcs06%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.34
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            5640                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'src', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.30
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             793                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'focus', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.28
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              88                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'zero', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.30
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             320                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'object', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:02.89
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            2848                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'dome flat', '%', 'bcs07%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.28
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1184                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'src', '%', 'scs200%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:02.93
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print src
SP2-0552: Bind variable "SRC" not declared.
SQL> print cnt;

             CNT                                                                
----------------                                                                
            2502                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'zero', '%', 'scs200%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.40
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             512                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'focus', '%', 'scs200%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.37
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              24                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'object', '%', 'scs200%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.13
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            3328                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'dome flat', '%', 'scs200%', '%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.29
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1728                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'BCS2007 05%_bcs0%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
               0                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'BCS200705%_bcs0%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.70
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           22351                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'reduced', '%', '%', 'BCS200705%_bcs0%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.47
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           12264                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'remap', '%', '%', 'BCS200705%_bcs0%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.72
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           22351                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'BCS2007060%_bcs0%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            8681                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'reduced', '%', '%', 'BCS2007060%_bcs0%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.48
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            4752                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'remap', '%', '%', 'BCS2007060%_bcs0%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.46
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            8681                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'BCS20070615', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.42
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
               0                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'coadd', '%', '%', 'BCS20070627', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.28
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             148                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'BCS20070627', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.44
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             148                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'coadd', '%', '%', 'BCS20070811', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
               0                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'coadd', '%', '%', 'DES20070811', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              36                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'DES20070811', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.38
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              36                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'coadd', '%', '%', 'DES20070812', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.29
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              36                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'DES20070812', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.38
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              36                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'DES20070813', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.37
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              36                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'coadd', '%', '%', 'DES20070813', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'coadd', '%', '%', 'DES20070814', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              36                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'add', '%', 'catalog', '%', '%', 'DES20070814', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
           35416                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'delete', '%', 'catalog', '%', '%', 'DES20070814', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:00.01
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
              -1                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des1', 'remove', '%', 'catalog', '%', '%', 'DES20070814', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           35416                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des2', 'add', '%', 'catalog', '%', '%', 'DES20071017_des20071006%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.73
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           51342                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des2', 'add', '%', 'reduced', '%', '%', 'DES20071017_des20071006%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.99
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           17608                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des2', 'add', '%', 'remap', '%', '%', 'DES20071017_des20071006%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.66
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           33734                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des2', 'add', '%', 'coadd', '%', '%', 'DES20071128%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:06.14
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             192                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des2', 'add', '%', 'catalog', '%', '%', 'DES20071128%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.47
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             184                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des4', 'add', '%', 'catalog', '%', '%', 'DES20071017_des20071006_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.79
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           51342                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des4', 'add', '%', 'reduced', '%', '%', 'DES20071017_des20071006_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.94
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           17608                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('des4', 'add', '%', 'remap', '%', '%', 'DES20071017_des20071006_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.27
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           33734                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'BCS2007%_bcs0511%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.42
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            9991                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'BCS2007%_bcs0511%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.18
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            9008                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'BCS2007%_bcs0511%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
           19698                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'DES20071002_des20071002_01%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.60
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             562                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'DES20071002_des20071002_01%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.48
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'DES20071002_des20071002_01%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             562                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'DES20071002_des20071002_02%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             530                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'DES20071002_des20071002_02%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.46
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'DES20071002_des20071002_02%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'DES20071002_des20071002_03%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.50
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             578                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'DES20071002_des20071002_03%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.39
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'DES20071002_des20071002_03%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             578                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'DES20071002_des20071002_05%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.58
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             510                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'DES20071002_des20071002_05%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.47
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'DES20071002_des20071002_05%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             510                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'DES20071002_des20071002_06%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.63
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             586                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'DES20071002_des20071002_06%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'DES20071002_des20071002_06%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.51
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             586                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'DES20071002_des20071002_07%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.50
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             522                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'DES20071002_des20071002_07%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.40
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'DES20071002_des20071002_07%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.45
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             522                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'catalog', '%', '%', 'DES20071002_des20071002_09%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.50
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             572                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'reduced', '%', '%', 'DES20071002_des20071002_09%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.39
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', 'remap', '%', '%', 'DES20071002_des20071002_09%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.45
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
             572                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_10%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1320                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_11%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1428                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_12%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1284                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_15%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.58
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1252                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_16%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.62
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1468                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_17%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.59
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1304                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_22%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.79
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1468                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_23%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.61
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1328                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_27%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1392                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_28%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1324                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_29%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.58
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1408                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_30%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1272                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_01%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.50
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1468                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_02%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.50
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1428                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_03%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.71
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1416                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_05%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1248                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_06%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1476                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_07%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.01
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1372                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_09%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.51
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1436                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_10%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.50
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1404                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_11%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt;

             CNT                                                                
----------------                                                                
            1392                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_12%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1376                                                                

SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_15%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1204                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_16%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1448                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_17%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1340                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_21%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.51
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1284                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_23%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1408                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_27%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.51
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1320                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_28%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1344                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_29%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1344                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_30%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1280                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_25%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.50
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1336                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_32%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.48
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1424                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_34%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1384                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_35%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1324                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_36%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1328                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_37%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1300                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_41%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1292                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_42%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1332                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_43%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:03.79
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1324                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_47%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1376                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_48%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1460                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_49%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1412                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_52%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1456                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_53%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1368                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_54%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1436                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_55%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1392                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_57%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1336                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_58%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1356                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_59%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1336                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_01%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1668                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_04%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1800                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_05%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.59
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1616                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_07%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1668                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_12%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1496                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_18%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1616                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_24%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.61
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1712                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_45%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1540                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_46%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1716                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_47%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1668                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_51%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1604                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_52%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.20
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1668                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_56%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1820                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_57%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1632                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_60%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.58
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1580                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_08%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1664                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_09%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1640                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_13%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1572                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_14%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1716                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_15%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1556                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_19%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1604                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_20%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1732                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_21%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.33
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1592                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_24%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1684                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_25%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1660                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_26%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1676                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_27%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1656                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_30%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1588                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_31%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1812                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_32%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1648                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_33%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1664                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_34%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1640                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_37%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1520                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_38%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.58
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1772                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_44%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.43
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1672                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_50%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1632                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_55%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1616                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_08%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1624                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_09%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1612                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_13%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1480                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_14%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1752                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_15%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1544                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_19%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1600                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_20%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.58
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1828                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_21%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1624                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_24%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1696                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_25%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1608                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_26%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.52
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1592                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_27%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1604                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_30%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.58
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1516                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_31%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1760                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_32%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1644                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_33%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.55
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1628                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_34%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.57
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1644                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_37%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.53
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1548                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_38%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.59
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1856                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_44%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1588                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_50%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.56
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1652                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_55%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.54
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1528                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'SCS20071027_scs20071020%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.40
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
               0                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exec :cnt := upd_archivesites('bcs', 'add', '%', '%', '%', '%', 'SCS20071028_scs20071012%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.41
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
            1240                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071002_des20071002_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.66
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
           84868                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071005_des20071003_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.85
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
           85830                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071006_des20071004_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:04.84
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
           85670                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071016_des20071005_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:05.14
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
          102120                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071017_des20071006_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:05.69
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
          102684                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071023_des20071007_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:05.33
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
          102180                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071024_des20071008_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:05.39
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
          102320                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071025_des20071009_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:05.67
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
          102276                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071030_des20071010_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:09.36
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
          101696                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071031_des20071011_%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:06.22
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
          101876                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> @t.sql
SQL> exec :cnt := upd_archivesites('gpfs-wan', 'add', '%', '%', '%', '%', 'DES20071128%', '%', '%', 0)

PL/SQL procedure successfully completed.

Elapsed: 00:00:01.49
SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> print cnt

             CNT                                                                
----------------                                                                
             376                                                                

SQL> /

Commit complete.

Elapsed: 00:00:00.00
SQL> exit
