-- This procedure is similar to fGetNearbyXYZpre, but it is used in the
-- DES portal.
create or replace function fGetNearbyXYZpre_ObjHead(
nx float, 
ny float, 
nz float, 
r float) 
return objectHeadTab pipelined 
is
pragma autonomous_transaction;

objhead objectHead := objectHead(null,null,null,null,null,null,null,null);

level number(2);
shift number(16);
cmd varchar2(200);

cursor objrc is
  --select /*+ USE_NL(a, b)  INDEX(b idx_htmid) */
  select /*+ ordered USE_NL(b) INDEX(b idx_htmid) */
         object_id, htmid, alpha_j2000 ra, delta_j2000 dec, cx, cy, cz, 
         fGetDistanceXYZ(nx,ny,nz,cx,cy,cz) distance
  from table(fHtmCover(cmd,r)) a, objects b
  where b.htmid >= a.htm_id_start
  and   b.htmid <= a.htm_id_end;

begin
  level := 13 - (floor(log(10,r)/log(10,2.0)));
  --dbms_output.put_line('level is ' || level);
  if (level < 5) then
    level := 5;
  elsif (level > 13) then
    level := 13;
  end if;
  --dbms_output.put_line('level2 is ' || level);
  shift := power(4, 20-level);
  --dbms_output.put_line('shift is ' || shift);
  cmd := to_char(level) || ' ' || to_char(nx) || ' ' || to_char(ny) || ' ' || 
         to_char(nz) || ' ' || to_char(r);
  --dbms_output.put_line('cmd is ' || cmd);
  for objrc_rec in objrc
  loop
     objhead.object_id := objrc_rec.object_id;
     objhead.htmid := objrc_rec.htmid;
     objhead.ra := objrc_rec.ra;
     objhead.dec := objrc_rec.dec;
     objhead.cx := objrc_rec.cx;
     objhead.cy := objrc_rec.cy;
     objhead.cz := objrc_rec.cz;
     objhead.distance := objrc_rec.distance;
     pipe row (objhead);
  end loop;
  commit;
  return;
end fGetNearbyXYZpre_ObjHead;
/

