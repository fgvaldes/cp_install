#!/usr/bin/perl
########################################################################
#  $Id$
#
#  $Rev::                                  $:  # Revision of last commit.
#  $LastChangedBy::                        $:  # Author of last commit.
#  $LastChangedDate::                      $:  # Date of last commit.
#
#  Authors: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois.
#  All rights reserved.
#
#  DESCRIPTION:
#
#    simple script to check correct number of files in location table in DB
#    at various stages in the nightly processing
#      
########################################################################
      
use strict;
use warnings;

use Getopt::Long;
use Data::Dumper;
use File::Basename;
use File::Path;

use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::DESUtil;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($project, $run, $numbands, $numccds, $verbose) = undef;
Getopt::Long::GetOptions(
    "project=s" => \$project,
    "run=s" => \$run,
    "numccds=s" => \$numccds,
    "verbose" => \$verbose,
);

if (!defined($run) || !defined($project)) {
    print "Usage: check_db_nitely -project <prj> -run <run>\n";
    exit 1;
}

my ($date,$nite)=split/_/, $run;
if (!defined($numbands)) {
    $numbands=4;
}
if (!defined($numccds)) {
    $numccds=62;
}


my $desDBI = DB::DESUtil->new();
my $table = 'LOCATION';

my (%key_vals, $results_aref);
my ($num_src, $num_src_bias, $num_src_flat, $num_src_obj);
my ($num_raw, $num_raw_obj, $num_raw_bias, $num_raw_flat);
my ($num_biascor, $num_flatcor);
my ($num_red);
my ($num_sky, $num_illumcor, $num_fringecor);
my ($num_ahead, $num_astrostds, $num_head, $num_scamp, $num_fullscamp);
my ($num_red_cat);
my ($num_remap);

#######################
print "SOURCE\n";
%key_vals = ();

$key_vals{'project'} = $project;
$key_vals{'fileclass'} = 'src';
$key_vals{'nite'} = $nite;
$key_vals{'filetype'} = 'src';

$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_src = scalar(@$results_aref);
my %bands = ();
foreach my $fhref (@$results_aref) {
    if (defined($fhref->{'band'}) && ($fhref->{'band'} =~ /\w/)) {
        $bands{$fhref->{'band'}} = 1;
    }
}
$numbands = scalar(keys %bands);

print "\tNumber of source files = $num_src\n";
print "\tNumber of bands = $numbands\n";


$key_vals{'filename'} = '%bias%';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_src_bias = scalar(@$results_aref);
print "\tNumber of bias source files = $num_src_bias\n";
dumpFiles();

$key_vals{'filename'} = '%flat%';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_src_flat = scalar(@$results_aref);
print "\tNumber of flat source files = $num_src_flat\n";
dumpFiles();

$num_src_obj = $num_src - $num_src_bias - $num_src_flat;
print "\tNumber of obj source files = $num_src_obj\n";


#######################
print "\nCROSSTALK\n";
%key_vals = ();

$key_vals{'fileclass'} = 'red';
$key_vals{'run'} = $run;
$key_vals{'filetype'} = 'raw%';

#print "key_vals: ", Dumper(\%key_vals), "\n";
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_raw = scalar(@$results_aref);
print "\tNumber of raw_* files = $num_raw ";
if ($num_raw != $num_src * $numccds) {
    print "*** ERROR *** (expecting ", $num_src*$numccds,")";
}
print "\n";
dumpFiles();

$key_vals{'filetype'} = 'raw_obj';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_raw_obj =  scalar(@$results_aref);
print "\tNumber of raw_obj files = $num_raw_obj ";
if ($num_raw_obj != $num_src_obj * $numccds) {
    print "*** ERROR *** (expecting ", $num_src_obj * $numccds,")";
}
print "\n";
dumpFiles();

$key_vals{'filetype'} = 'raw_bias';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_raw_bias =  scalar(@$results_aref);
print "\tNumber of raw_bias files = $num_raw_bias ";
if ($num_raw_bias != $num_src_bias * $numccds) {
    print "*** ERROR *** (expecting ", $num_src_bias * $numccds,")";
}
print "\n";
dumpFiles();

$key_vals{'filetype'} = 'raw_dflat';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_raw_flat =  scalar(@$results_aref);
print "\tNumber of raw_dflat files = $num_raw_flat ";
if ($num_raw_flat != $num_src_flat * $numccds) {
    print "*** ERROR *** (expecting ", $num_src_flat * $numccds, ")";
}
print "\n";
dumpFiles();




#######################
print "CREATECOR\n";
$key_vals{'filetype'} = 'biascor';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_biascor =  scalar(@$results_aref);
print "\tNumber of biascor files = $num_biascor ";
#if ($num_biascor != $num_src_bias) {
#    print "*** ERROR *** (expecting ", $num_src_bias, ")";
#}
print "\n";
dumpFiles();

$key_vals{'filetype'} = 'flatcor';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_flatcor =  scalar(@$results_aref);
print "\tNumber of flatcor files = $num_flatcor ";
#if ($num_flatcor != $num_src_flat) {
#    print "*** ERROR *** (expecting ",$num_src_flat, ")";
#}
print "\n";
dumpFiles();

#######################
print "DETREND\n";
$key_vals{'filetype'} = 'red';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_red =  scalar(@$results_aref);
print "\tNumber of red files = $num_red ";
if ($num_red != $num_raw_obj) {
    print "*** ERROR *** (expecting ",$num_raw_obj,")";
}
print "\n";
dumpFiles();

#######################
print "CREATE_ILLUMCOR\n";
$key_vals{'filetype'} = 'supersky';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_sky =  scalar(@$results_aref);
print "\tNumber of supersky files = $num_sky ";
if ($num_sky != $numccds*$numbands) {
    print "*** ERROR *** (expecting ",$numccds*$numbands,")";
}
print "\n";
dumpFiles();

$key_vals{'filetype'} = 'illumcor';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_illumcor =  scalar(@$results_aref);
print "\tNumber of illumcor files = $num_illumcor ";
if ($num_illumcor != $numccds*$numbands) {
    print "*** ERROR *** (expecting ",$numccds*$numbands,")";
}
print "\n";
dumpFiles();

$key_vals{'filetype'} = 'fringecor';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_fringecor =  scalar(@$results_aref);
print "\tNumber of fringecor files = $num_fringecor ";
if ($num_illumcor != $numccds*$numbands) {
    print "*** ERROR *** (expecting ",$numccds*$numbands,")";
}
print "\n";
dumpFiles();

#######################
# illum_correct
print "ILLUM_CORRECT (can only check number of red/red files)\n";
$key_vals{'filetype'} = 'red';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_red =  scalar(@$results_aref);
print "\tNumber of red files = $num_red ";
if ($num_red != $num_raw_obj) {
    print "*** ERROR *** (expecting ",$num_raw_obj,")";
}
print "\n";
dumpFiles();

#######################
print "ASTRO_REFINE\n";

$key_vals{'filetype'} = 'red_ahead';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_ahead =  scalar(@$results_aref);
print "\tNumber of ahead files = $num_ahead ";
if ($num_ahead != $num_src_obj) {
    print "*** ERROR *** (expecting ",$num_src_obj,")";
}
print "\n";

$key_vals{'filetype'} = 'aux_astrostds';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_astrostds =  scalar(@$results_aref);
print "\tNumber of astrostds files = $num_astrostds ";
if ($num_astrostds != 1) {
    print "*** ERROR *** (expecting ",1,")";
}
print "\n";

$key_vals{'filetype'} = 'red_head';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_head =  scalar(@$results_aref);
print "\tNumber of head files = $num_head ";
if ($num_head != $num_src_obj) {
    print "*** ERROR *** (expecting ",$num_src_obj,")";
}
print "\n";

$key_vals{'filetype'} = 'red_scamp';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_scamp =  scalar(@$results_aref);
print "\tNumber of scamp files = $num_scamp ";
if ($num_scamp != $num_src_obj) {
    print "*** ERROR *** (expecting $num_src_obj)";
}
print "\n";

$key_vals{'filetype'} = 'red_fullscamp';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_fullscamp =  scalar(@$results_aref);
print "\tNumber of fullscamp files = $num_fullscamp ";
if ($num_fullscamp != 0) {
    print "*** ERROR *** (expecting 0)";
}
print "\n";

$key_vals{'filetype'} = 'red';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_red =  scalar(@$results_aref);
print "\tNumber of red files = $num_red ";
if ($num_red != $num_raw_obj) {
    print "*** ERROR *** (expecting ",$num_raw_obj,")";
}
print "\n";

#######################
print "CATALOG RED\n";
$key_vals{'filetype'} = 'red_cat';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_red_cat =  scalar(@$results_aref);
print "\tNumber of red_cat files = $num_red_cat ";
if ($num_red_cat != $num_red) {
    print "*** ERROR *** (expecting ",$num_red,")";
}
print "\n";




#######################
print "REMAP\n";
$key_vals{'filetype'} = 'remap';
$results_aref = $desDBI->queryDB({table=>$table, key_vals=>\%key_vals});
$num_remap =  scalar(@$results_aref);
print "\tNumber of remap files = $num_remap ";
#if ($num_remap != $num_red) {
#    print "*** ERROR *** (expecting ",$num_red,")";
#}
print "\n";


$desDBI->disconnect();
exit 0;

#############################################################################################################
sub dumpFiles {
    if (defined($verbose)) {
        foreach my $fhref (@$results_aref) {
            printFile($fhref);
        }
    }
}

#############################################################################################################
# id fileclass filetype filename filedate filesize run nite band tilename exposurename ccd project archivesites
sub printFile {
    my $fhref = $_[0]; 
    foreach my $col (keys %$fhref) {
        if (defined($fhref->{$col})) {
            $fhref->{$col} =~ s/\s+$//;
            $fhref->{$col} =~ s/^\s+//;
        }
        else {
            $fhref->{$col} = "NULL";
        }
    }
    printf("%7d %3s %3s %s %10s %25s %s %s %10s %2s %s %s %s\n", 
                substr($fhref->{'id'}, 0, 7),
                substr($fhref->{'project'}, 0, 3),         
                substr($fhref->{'fileclass'}, 0, 3),
                substr($fhref->{'run'}, 0, 12),
                substr($fhref->{'filetype'}, 0, 10),
                substr($fhref->{'filename'}, 0, 25),
                substr($fhref->{'archivesites'}, 0, 25),
                substr($fhref->{'filesize'}, 0, 7),
                substr($fhref->{'nite'}, 0, 8),
                substr($fhref->{'ccd'}, 0, 2),
                substr($fhref->{'band'}, 0, 1),
                substr($fhref->{'exposurename'}, 0, 25),
                substr($fhref->{'tilename'}, 0, 25)
          ); 
}
