
Some imageproc executable programs 
==================================

cp_remap.pl
-----------

.. program-output:: perldoc -t "../bin/cp_remap.pl"


mkbiascor
---------

.. include:: ../src/mkbiascor.c
   :start-after: /*--main
   :end-before: */
 

mkflatcor
---------

.. include:: ../src/mkflatcor.c
   :start-after: /*--main
   :end-before: */

get_corners_from_image
----------------------

.. include:: ../src/get_corners_from_image.c
   :start-after: /*--main
   :end-before: */


cp_remap.pl
-----------
It's a mystery why this document is not picked up.

.. program-output:: perldoc -t ../bin/cp_remap.pl


runSCAMP.pl 
-----------

.. program-output:: perldoc -t ../bin/runSCAMP.pl

runSWARP_remap.pl 
-----------------

.. program-output:: perldoc -t ../bin/runSWARP_remap.pl








