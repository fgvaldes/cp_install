#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell


#
# runBKG.pl
#
# DESCRIPTION:
#
# AUTHOR:
# Bob Armstrong (rearmstr@illinois.edu)
#

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use Data::Dumper;
use FindBin;
use File::Path;
use File::Basename;
use Getopt::Long;
use Cwd;

use lib ("$FindBin::Bin/../lib", 
         "$FindBin::Bin/../lib/perl5",
         "$FindBin::Bin/../../../Database/trunk/lib"
        );

use DB::FileUtils;
use DB::EventUtils;

$| = 1;

my($eventStr,$fileList,$etcPath,$teraDir,$outputDir) = q();
my $verboseLevel = 2;

Getopt::Long::GetOptions(
   "filelist=s"    => \$fileList,
   "terapixpath=s" => \$teraDir,
   "etcpath=s"     => \$etcPath,
   "outputdir=s"     => \$outputDir,
   "verbose=s"     => \$verboseLevel,
) or usage("Invalid command line options\n");




my @files;
my $runtimeDir = cwd();

my $opensuccess = open FH, "< $fileList";
if (!$opensuccess) {
    $eventStr = qq{Error: Could not open list $fileList};
    reportEvent($verboseLevel,"STATUS",5,$eventStr);
    exit 1;
}


foreach my $fileName (<FH>) {

  chomp($fileName);

  # skip blank lines
  if ($fileName !~ /\S/) {
      next;
  }
  
#
#  # check if the file is compressed
  my $compFilename = sniffForCompressedFile($fileName);
  my $compressionType ;
  if (length($compFilename)>0){
      $compressionType = ($compFilename =~ m/(gz$|fz$)/) ? $1 : $2;
  }
  else
  {
      $compressionType = "fits" ;
  }
  if ($compressionType eq 'fz'){
      $eventStr = qq{funpacking: $compFilename};
      reportEvent($verboseLevel,"STATUS",1,$eventStr);
      system("$teraDir/funpack $compFilename");
  } elsif ($compressionType eq 'gz'){
      system("gunzip $compFilename");
  }
  
  my $baseName = basename($fileName);
  my $dirName = dirname($fileName);
  
  my $resolvePath = $dirName;
  $resolvePath =~ s/^(.*?)\/Archive\///;
  my $fileInfoHashRef = filenameResolve("$resolvePath/$baseName");


  
  if ($fileInfoHashRef == -1){
      $eventStr = qq{FileName parsing error:  $fileName is not a valid DES fileName};
      reportEvent($verboseLevel,'STATUS',4,$eventStr);
  }
  
  my $fileType = $fileInfoHashRef->{'FILETYPE'};
  my $oldRun = $fileInfoHashRef->{'RUN'};
  my $exposure=$fileInfoHashRef->{'EXPOSURENAME'};

  
  ## Append exposure name to outputdir
  my $dir=$outputDir."/".$exposure;
  print $exposure,"\n";

  mkpath($dir);

  
  my $bkgFile=$dir."/".$baseName;
  $bkgFile =~ s/\.fits/_bkg\.fits/;  
  
  
  

##
## Run sextractor to create the background image
##

  my $sex = qq{sex $fileName\[0\] -c $etcPath/default.sex };
     $sex .= qq{-PARAMETERS_NAME $etcPath/sex.param_bkg };
     $sex .= qq{-FILTER N -DETECT_THRESH 1000 };
     $sex .= qq{-CHECKIMAGE_TYPE BACKGROUND };
     $sex .= qq{-CHECKIMAGE_NAME $bkgFile};

  #print "Executing:\n$sex\n";
  my $stat = system("$teraDir/$sex") ;

 unlink($fileName) if (-e $compFilename);

  if (! -e "$bkgFile" || $stat != 0){
      
      $eventStr = "WARNING: sextractor failed for image:\n$baseName\nskipping to next image.\n";
      reportEvent($verboseLevel,"STATUS",4,$eventStr);
      next;
  }
  

## Delete the uncompressed image only if the corresponding compressed image
## exists on disk.
##


}
close FH;
#
##

exit(0);

#
# Subroutines
#
sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -filelist <files.list> -etcpath <etcpath>" .
       " -terapixpath <terapixpath> -outputdir <outputdir>\n"
   );

   die("\n")

}

