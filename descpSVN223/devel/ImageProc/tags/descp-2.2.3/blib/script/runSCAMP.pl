#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#
#  $Id: runSCAMP.pl 9749 2012-11-15 05:32:43Z daues $
#
#  $Rev:: 9749                             $:  # Revision of last commit.
#  $LastChangedBy:: daues                  $:  # Author of last commit. 
#  $LastChangedDate:: 2012-11-14 22:32:43 #$:  # Date of last commit.
#
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use warnings;
use Getopt::Long;
use Pod::Usage;
use File::Copy;
use File::Path;
use Astro::FITS::CFITSIO qw(:constants);
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use ImageProc::SCAMP qw(runSCAMP);
use ImageProc::Util qw(getExposureList getScampHeadValues updateFitsHeaderKeys appendArgs);
use DB::EventUtils;

$|=1;

my $maxtries = 1;
my $verbose = 1;
my $scamp = 'scamp';
my ($help,$man,$stat) = undef;
my ($list,$bindir,$qadir) = undef;
my ($astromSigmaTol,$astromSigmaHighsnTol,$nstarsRatioTol) = undef;
my ($update, $force_update)=undef;
my ($config);
my $maxpv = 10;


# SCAMP header keyword definitions:
my %head_defs = (
  'SCAMPFLG' => {'dtype' => TINT,   'comment' => 'Flag for SCAMP; 0=no WARNING; 1=WARNING exists'},
  'SCAMPNUM' => {'dtype' => TINT,   'comment' => 'number of matched stars from SCAMP'},
  'SCAMPCHI' => {'dtype' => TFLOAT, 'comment' => 'chi2 value from SCAMP'},
);

# Definitions of Keywords to extract from the scamp .head files and 
# place into the image file headers:
my %extract = (
  'EQUINOX' =>  {'dtype' => TFLOAT,  'comment' => 'Equinox'}, 
  'RADECSYS' => {'dtype' => TSTRING, 'comment' => 'Astrometric system'},
  'CTYPE1' =>   {'dtype' => TSTRING, 'comment' => 'WCS projection type for this axis'}, 
  'CTYPE2' =>   {'dtype' => TSTRING, 'comment' => 'WCS projection type for this axis'}, 
  'CUNIT1' =>   {'dtype' => TSTRING, 'comment' => 'Axis unit'}, 
  'CUNIT2' =>   {'dtype' => TSTRING, 'comment' => 'Axis unit'}, 
  'CRVAL1' =>   {'dtype' => TDOUBLE, 'comment' => 'World coordinate on this axis'},
  'CRVAL2' =>   {'dtype' => TDOUBLE, 'comment' => 'World coordinate on this axis'}, 
  'CRPIX1' =>   {'dtype' => TDOUBLE, 'comment' => 'Reference pixel on this axis'},
  'CRPIX2' =>   {'dtype' => TDOUBLE, 'comment' => 'Reference pixel on this axis'},
  'CD1_1' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'CD1_2' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'CD2_1' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'CD2_2' =>    {'dtype' => TDOUBLE, 'comment' => 'Linear projection matrix'},
  'PV1_0' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_1' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_2' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_3' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_4' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_5' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_6' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_7' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_8' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_9' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV1_10' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_0' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_1' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_2' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_3' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_4' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_5' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_6' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_7' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_8' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_9' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'},
  'PV2_10' =>       {'dtype' => TDOUBLE, 'comment' => 'Projection distortion parameter'}
);

# Grab the wrapper options and the SCAMP options that are also
# handles and set by this code.  the rest of the command line
# (whatever's left) will be assumed to be additional SCAMP arguments:
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev', 'pass_through');
$stat = Getopt::Long::GetOptions(
      'list=s' => \$list,
      'astromSigmaTol=s' => \$astromSigmaTol,
      'astromSigmaHighsnTol=s' => \$astromSigmaHighsnTol,
      'nstarsRatioTol=s' => \$nstarsRatioTol,
      'bindir=s' => \$bindir,
      'c=s'=> \$config,
      'update-headers|uh' => \$update,
      'force-update|fu' => \$force_update,
      'maxtries=s' => \$maxtries,   # automatically retry scamp execution for temporary failure
      'maxpv=s' => \$maxpv,
      'qadir=s' => \$qadir,
      'verbose|V=s' => \$verbose,
      'help|h|?' => \$help,
      'man' => \$man
);


# Display documantation and exit if requested:
if ($help) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($man) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($list)) {
    reportEvent($verbose, "STATUS",5,"Must provide an input list of images.");
    exit 1;
}

if (!defined($astromSigmaTol)) {
    $astromSigmaTol = 2.0; 
    print "astromSigmaTol not defined on commmand line: using default value $astromSigmaTol \n"; 
}
else {
    print "astromSigmaTol set on command line: $astromSigmaTol \n"; 
}

if (!defined($astromSigmaHighsnTol)) {
    $astromSigmaHighsnTol = 2.0; 
    print "astromSigmaHighsnTol not defined on commmand line: using default value $astromSigmaHighsnTol \n"; 
}
else {
    print "astromSigmaHighsnTol set on command line: $astromSigmaHighsnTol \n"; 
}

if (!defined($nstarsRatioTol)) {
    $nstarsRatioTol = 10000000000.0 ;
    print "nstarsRatioTol not defined on commmand line: using default value $nstarsRatioTol \n"; 
}
else {
    print "nstarsRatioTol set on command line: $nstarsRatioTol \n"; 
}


if (! -r $list) {
    reportEvent($verbose, "STATUS",5,"Could not read image list: '$list'.");
    exit 1;
}

if (!$config) {
    reportEvent($verbose, "STATUS",5,"Error: Must specify a config file (-c <configfile>).");
    exit 1;
}
if (! -r $config) {
    reportEvent($verbose, "STATUS",5,"Error: Cannot read config file (-c $config).");
    exit 1;
}

if ($bindir) {
  $scamp = join('/',$bindir,$scamp);
}

if (! -x $scamp) {
  print STDERR "Cannot execute $scamp\n";
  exit 1;
}

# Read list of images and grab exposure names:
my $exposures = getExposureList($list);


########################################################################
# Begin loop over exposures:
########################################################################
foreach my $exposure (keys %$exposures) {

    my ($astromSigma1, $astromSigma2, $astromChi, $nstars, $astromSigmaH1, $astromSigmaH2, $chi2, $nstarsH);
    # my ($nstars, $chi2);
  
    # make exposure subdir
    my $expqadir = "$qadir/$exposure";
    eval { mkpath($expqadir) };
    if ($@) {
         reportEvent($verbose, "STATUS",5,"Error: Couldn’t create exposure QA dir: $expqadir: $@");
         exit 1;
    }

    # Peel the exposure directory off of the list to use for
    # finding the fullscamp and ahead files.
    my $onelist = $exposures->{$exposure};
    my $onefile = $onelist->[0];
    $onefile =~s/\/[^\/]+$//;
    my $exp_dir = $onefile;

    my $input_catalog = join('/',$exp_dir,join('_',$exposure,'scamp.fits'));
    if (! -r $input_catalog) {
        reportEvent($verbose, "STATUS", 5, "Cannot read input catalog ($input_catalog)");
        exit 1;
    }

    my $head_out_filename = join('_',$exposure,'scamp.head');
    my $head_out = join('/',$exp_dir,$head_out_filename);

    #Build Scamp command line:
    $scamp = join(' ', $scamp, $input_catalog);  #First arg. ->  input cat.
    $scamp = join(' ', $scamp, @ARGV);
    my %args = ();
    $args{'-c'} = $config;
    if ($qadir) {
        $args{'-XML_NAME'} = "${expqadir}/${exposure}_scamp.xml";
    }
    else {
        $args{'-XML_NAME'} = "${exposure}_scamp.xml";
    }
    my $scamp_cmd = appendArgs($scamp,\%args);
    my $stat = 0;  # $status variable to indicate whether a retry of scamp is needed.

    ($stat, $astromSigma1, $astromSigma2, $astromChi, $nstars, $astromSigmaH1, $astromSigmaH2, $chi2, $nstarsH) = runScampCheckHead($scamp_cmd, $head_out, $verbose);
    # ($stat,$nstars,$chi2) = runScampCheckHead($scamp_cmd, $head_out, $verbose);
    #
    # This was a STATUS5 under legacy behavior: we lessen to a STATUS4  SV  Nov 13 2012 
    #
    if ($stat) {
        reportEvent($verbose, "STATUS", 4, "Problem with running SCAMP with exposure $exposure.");
        exit 1;
    }
    else {
        reportEvent($verbose, "STATUS", 1, "Successful scamp run for exposure $exposure.");
    }

    if ($update) {

        $astromSigma1  =~ s/"//g;
        $astromSigma2  =~ s/"//g;
        $astromSigmaH1 =~ s/"//g;
        $astromSigmaH2 =~ s/"//g;

        print "Retrieved QA_values for  $exposure \n";

        print "$astromSigma1, $astromSigma2, $astromChi, $nstars \n$astromSigmaH1, $astromSigmaH2, $chi2, $nstarsH\n";
        # Define header key-val hash for passing scamp data into the calibrated images:
        my %head = ( 'SCAMPFLG' => 0 );
        $head{'SCAMPFLG'} = 1 if ($stat && !$force_update);


        # Set SCAMPFLG to 1 on Other conditions 
        my $scampCondition = 0;

        # $astromSigmaTol,$astromSigmaHighsnTol,$nstarsRatioTol

        my $astromRms = sqrt($astromSigma1*$astromSigma1 + $astromSigma2*$astromSigma2);
        if ($astromRms > $astromSigmaTol) {
           $scampCondition = 1;
           print "Setting scampCondition : astromSigmaTol $astromSigmaTol exceeded! : $astromRms \n"; 
        }

        my $astromHRms = sqrt($astromSigmaH1*$astromSigmaH1 + $astromSigmaH2*$astromSigmaH2);
        if ($astromHRms > $astromSigmaHighsnTol) {
           $scampCondition = 1;
           print "Setting scampCondition : astromSigmaHighsnTol $astromSigmaHighsnTol exceeded! : $astromHRms \n"; 
        }
        if ($nstars == 0) {
           $scampCondition = 1;
           print "Setting scampCondition : nstars is zero! \n"; 
        }
        if ($nstarsH > 0.5) {
           my $nstarsRatio = $nstars / $nstarsH ;
           if ( $nstarsRatio > $nstarsRatioTol ) {
              $scampCondition = 1;
              print "Setting scampCondition : nstarsRatioTol $nstarsRatioTol exceeded! : $nstarsRatio \n";
           }
        }

        $head{'SCAMPFLG'} = 1 if ($scampCondition);

        $head{'SCAMPNUM'} = $nstars if $nstars;
        $head{'SCAMPCHI'} = $chi2 if $chi2;
        my $imagelist = $exposures->{$exposure};
        my $value_sets;

        # gather values from scamp .head file if success or force_update
        if (!$stat || $force_update) {
            $value_sets = getScampHeadValues($head_out, \%extract);
        }

        for (my $i=0; $i<=$#$imagelist; $i++) {
            my $image = $imagelist->[$i];
            my $values = $value_sets->[$i];

            # Check for bad PV values and printout of values extracted from the scamp output file;
            my $pvprob=0;
            my $ccd = sprintf("%02d",$i);
            if ($verbose >= 3) {
                print "\nValues to update from .head file for CCD $ccd:\n";
            }
            while ((my $k, my $v) = each %{$values}) {
                if (($k =~ m/^PV/i) && (abs($v) > $maxpv)) {
                    $pvprob++;
                }
                if ($verbose >= 3) {
                    print "\t$k = '$v'\n";
                }
            }
            print "\n";

            if ($pvprob) {
                reportEvent($verbose, "STATUS", 4, "Warning:  ${exposure}'s head file for CCD $ccd has $pvprob PV values > $maxpv.   NOT updating headers.");
            }
            else {
                my $start=time;
                updateFitsHeaderKeys({
                    'fits_file' => $image, 
                    'values' => \%head,
                    'key_defs' => \%head_defs, 
                    'hdu' => 1
                });
                print "Changing ", scalar keys %head, " values in hdu=1 took ", time-$start, " secs\n";

                $start=time;
                updateFitsHeaderKeys({
                    'fits_file' => $image, 
                    'values' => $values,
                    'key_defs' => \%extract, 
                    'hdu' => [1,2]
                });
                print "Changing ", scalar keys %$values, " values in hdu=1,2 took ", time-$start, " secs\n";
            }
        } # end for each image
    } # end update headers is true

    # if collecting QA files into another dir
    if ($qadir) {
        # move *.ps files to exposure qa dir
        foreach my $qafile (<*.ps>) {
            move($qafile, "$expqadir/$qafile");
        } 
    } # end QA file collection 
} # end exposure list loop.

exit 0;

###########################################################
sub runScampCheckHead {
    my ($scamp_cmd, $head_out, $verbose) = @_;
    my ($stat, $astromSigma1, $astromSigma2, $astromChi, $nstars, $astromSigmaH1, $astromSigmaH2, $chi2, $nstarsH);

    my $numtries = 1;
    my $done = 0;
    while ($numtries <= $maxtries && !$done) { 
        if ($verbose >= 1) {
            reportEvent($verbose, "STATUS", 1, "Running SCAMP command:\n$scamp_cmd");
        }

        $stat = runSCAMP($scamp_cmd, \$astromSigma1, \$astromSigma2, \$astromChi, \$nstars, \$astromSigmaH1, \$astromSigmaH2, \$chi2, \$nstarsH, $verbose);

        if ($stat) {
            reportEvent($verbose, "STATUS", 3, "SCAMP exited with non-zero exit code.");
        }
        elsif (! -r $head_out) {
            reportEvent($verbose, "STATUS", 4, "SCAMP did not produce a .head file");
        }
        else {
            $done = 1;
        }

        if (!$done) {
            $numtries++;
            if ($numtries <= $maxtries) {
                reportEvent($verbose, "STATUS", 3, "Running SCAMP again to see if temporary problem.")
            }
        }
    }
    if ($done) {
        $stat = 0;
    }
    else {
        $stat = 1;
    }
    return ($stat, $astromSigma1, $astromSigma2, $astromChi, $nstars, $astromSigmaH1, $astromSigmaH2, $chi2, $nstarsH);
    # return ($stat, $nstars, $chi2);
}

########################################################################
# Documentation
########################################################################


=head1 NAME B<runSCAMP.pl> - Wrapper to run TeraPix SCAMP software on DESDM data sets.

=head1 SYNOPSIS

B<runSCAMP.pl> -list <file list> -[bindir <bin directory with swarp>] [Additional runSCAMP options] [-- [Additional swarp options] ]

=head1 DESCRIPTION

B<runSCAMP.pl> is a Perl wrapper progam for the TeraPix SCAMP astrometric calibration software package.  B<runSCAMP.pl> is designed
to be an interface between SCAMP and the DESDM archive data.  Input lists are transformed into correct fully-specified inputs for 
SCAMP.  The input file listing is used to construct the correct file naming for scamp outputs.  The SCAMP output stream is monitored for
warnings and B<runSCAMP.pl> will issued status and qa event strings if called with B<-verbose> >= 2.

=head1 OPTIONS

=over 4

=item B<-bindir>

The Directory containing the Terapix B<scamp> executable.

=item B<-help>, B<-h>, B<-?>

Display brief help and exit.

=item B<-man>

Display full manual page and exit.

=item B<-update-headers>, B<-uh>

Update the original image list of reduced images with selected keywords from the SCAMP *.head file as well as some
other scamp keywords.

=item B<-verbose>, B<-V> I<Number>

Verbose level 0-5. Default is 1.  Level 2 and above will turn on status event syntax.

=item B<-astromSigmaTol>

Tolerance for setting SCAMPFLG to 1 for FWHM rms

=item B<-astromSigmaHighsnTol>

Tolerance for setting SCAMPFLG to 1 for FWHM rms for highsn

=item B<-nstarsRatioTol>

Tolerance for setting SCAMPFLG to 1 for ratio of detections, standard to highsn case

=back

=head1 EXAMPLES
