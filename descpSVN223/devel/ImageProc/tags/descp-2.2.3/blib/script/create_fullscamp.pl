#!/usr/bin/env perl
########################################################################
#  $Id                                     $
#
#  $Rev:: 8730                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-25 22:15:19 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  DESCRIPTION:
#      Given a list of images, checks the run to see if different from current
#      run.  If different copies to current run.
#
#  COMMENTS:
#     Still uses fits combine c code.
#
#######################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking



use strict;
use warnings;

use Data::Dumper;

use Getopt::Long;
use File::Basename;
use File::Path;
use FindBin;

use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use Astro::FITS::CFITSIO;
use DB::EventUtils;
use DB::FileUtils;
use DB::DESUtil;
use ImageProc::Util qw(getExposureList);

my ($list, $binpath, $outputpath, $cleanup) = undef;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'list|i=s'  => \$list,
      'binpath|i=s'  => \$binpath,
      'outputpath|i=s'  => \$outputpath,
      'cleanup' => \$cleanup
);

if (!defined($list)) {
    print STDERR "Usage: create_fullscamp.pl -list <inputlist> -binpath <path_to_fitscombine>\n";
    exit 1;
}

if (! -r $list) {
    print STDERR "Cannot read list: $list\n";
    exit 1;
}

if (! defined($binpath)) {
    $binpath = $FindBin::Bin;
}

if (! -x "$binpath/fitscombine") {
    print STDERR "Cannot locate fitscombine executable: $binpath/fitscombine\n";
    exit 1;
}

if (! defined($outputpath)) {
    $outputpath = '.';
}
if (! -d $outputpath) {
    mkdir $outputpath;
}

# read list of images and grab exposure names
my $exposures = getExposureList($list);

#foreach my $e (sort keys %$exposures) {
#    print "$e\n";
#    print Dumper($exposures->{$e}), "\n";
#}

my $command;
foreach my $e (keys %$exposures) {
    $command = "$binpath/fitscombine ";
    $command .= '-cleanup ' if ($cleanup);
    foreach my $filename (@{$exposures->{$e}}) {
        my ($basename,$path,$suffix) = fileparse($filename,('.fits','.fits.gz'));
        $path =~ s/\/$//;
        my $newname = "$path/${basename}_scamp$suffix";
        if (! -r $newname) {
            print STDERR "Error: Cannot read $newname\n";
            exit 1;
        }
	###
	# Jira 2413: Added this code to read each temp file xxxxx_[01-62]_scamp.fits and extract the number of stars from it
	###
	populateNStarsFromFile($newname);
	###
	# Jira 2413 end
	###
        $command .= "$newname ";
    }
    if (! -d "$outputpath/${e}" ) {
        mkdir "$outputpath/${e}";
    }
    $command .= " $outputpath/${e}/${e}_scamp.fits";

    open(RUN, " $command 2>&1 |");
    while (my $line = <RUN>) {
        print $line;
    }
    close(RUN);
}

exit 0;


sub populateNStarsFromFile{

	my ($fitsFileName) = @_;
	my ($fptr,$status,$chduType,$value,$comment,$eventStr);
	# ensure status is unset (it causes troubles sometimes when the CFITSIO function call returns no error, but the $status has the error from a previously called function
	$status = 0; 
	Astro::FITS::CFITSIO::fits_open_file($fptr, "$fitsFileName", Astro::FITS::CFITSIO::READWRITE(), $status);
	if($status){
		$eventStr = "Could not open $fitsFileName for populating nobject_scamp column in IMAGE table";
		reportEvent( 2, 'STATUS', 3, $eventStr );
	}else{
		$status = 0;
		$chduType = ''; 
		# Move to the 3rd HDU because thats the one which has the NSTARS info in the NAXIS2 keyword
		Astro::FITS::CFITSIO::fits_movabs_hdu($fptr, 3, $chduType, $status);
		if($status){
			$eventStr = "Could not move to the 3rd HDU for populating nobject_scamp column in IMAGE table";
			reportEvent( 2, 'STATUS', 3, $eventStr );
		}else{
			$status = 0;
			$value = ''; 
			$comment = '' ;
			# read the NAXIS2 keyword which contains the value to be inserted in the nobject_scamp column
			Astro::FITS::CFITSIO::fits_read_keyword($fptr, "NAXIS2", $value, $comment, $status);
			if($status){
				$eventStr = "Could not read NAXIS2 keyword for populating nobject_scamp column";
				reportEvent( 2, 'STATUS', 3, $eventStr );
			}else{
				insertIntoImageTable($fitsFileName,$value,"nobject_scamp");
			}
		}
	}
}

sub insertIntoImageTable {

	my ($fileName,$value,$column) = @_;
	my ($tempFilename,$fileinfoHashref,$sqlUpdate,$desdbh,$sqlSth,$project_name,$fileclass,$run,$filetype,$exposure,$filename);

	$desdbh = DB::DESUtil->new();
	$tempFilename = $fileName;
	$tempFilename =~ s/.*?Archive\/// ;
        $fileinfoHashref = DB::FileUtils::filenameResolve($tempFilename);	

	$project_name = $fileinfoHashref->{'PROJECT'};
	$fileclass =  $fileinfoHashref->{'FILECLASS'};
	$run =  $fileinfoHashref->{'RUN'};
	$filetype =  $fileinfoHashref->{'FILETYPE'};
	$exposure =  $fileinfoHashref->{'EXPOSURENAME'};
	$filename = $fileinfoHashref->{'FILENAME'};

	###
	# to get to the corresponding file for this scamp file, remove _scamp from filename and filetype and the _XX from exposure
	###
	$filename =~ s/_scamp//;
	$filetype =~ s/_scamp//;
	$exposure =~ s/_\d+//;

	$sqlUpdate = "update image set $column = '$value' where id = (select id from location where run = '$run' and filetype = '$filetype' and filename = '$filename' and exposurename = '$exposure' and fileclass = '$fileclass' and project = '$project_name')";
	#print "\n the sql $sqlUpdate";
	$sqlSth = $desdbh->prepare($sqlUpdate);
	my $ret = $sqlSth->execute() or reportEvent(2, 'STATUS', 3, "could not update image with nobject_scamp for $fileName" );
	#print "\n rows affected $ret";
	$desdbh->commit();
	$sqlSth->finish();
	$desdbh->disconnect();

}

