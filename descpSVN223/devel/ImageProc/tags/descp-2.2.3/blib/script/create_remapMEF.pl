#!/usr/bin/env perl
########################################################################
#  $Id                                     $
#
#  $Rev:: 8733                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-25 22:17:39 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  DESCRIPTION:
#      Given a list of images, checks the run to see if different from current
#      run.  If different copies to current run.
#
#######################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking



use strict;
use warnings;

use Data::Dumper;

use Getopt::Long;
use File::Basename;
use File::Path;
use Astro::FITS::CFITSIO qw(:constants);
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use ImageProc::SWARP qw(readSWARPInputFile);
use ImageProc::Util qw(updateFitsHeaderKeys);

my ($list, $binpath, $outdir, $cleanup) = undef;
my $verbose = 1;

# Define DES header keywords to be inserted into remap MEF:
my %key_defs = (
  'DES_EXT' => {'dtype' => TSTRING, 'comment' => 'Image Extension'},
  'OBSTYPE' => {'dtype' => TSTRING, 'comment' => 'Observation type'}
);

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'list=s'  => \$list,
      'binpath|bindir=s'  => \$binpath,
      'outputpath|outdir|i=s'  => \$outdir,
      'cleanup' => \$cleanup,
      'verbose' => \$verbose
);

if (!defined($list)) {
    print STDERR "Must specify -list\n";
    exit 1;
}

if (! -r $list) {
    print STDERR "Cannot read list: $list\n";
    exit 1;
}

if (! defined($binpath)) {
    $binpath = $FindBin::Bin;
}

if (! -x "$binpath/fitscombine") {
    print STDERR "Cannot locate fitscombine executable: $binpath/fitscombine\n";
    exit 1;
}

if (! defined($outdir)) {
    $outdir = '.';
}
if (! -d $outdir) {
    mkdir $outdir;
}

# read list of images and grab exposure names
my $lines = readSWARPInputFile($list);
my $command = "$binpath/fitscombine ";
$command .= '-cleanup ' if ($cleanup);
my $err_flag = 0;

foreach my $line (@$lines) {
  my $go = 1;
  my $filename = $line->[0];
  my $tilename = $line->[1];

  # Get remap location from red location:
  my ($basename,$path,$suffix) = fileparse($filename,('.fits','.fits.gz'));
  $path =~ s/\/\s*$//g;
  $path =~ /\/([^\/]+)$/;
  my $exposurename = $1;
  $path =~ s/\/[^\/]+$//;
  $path =~ s/\/[^\/]+$//;
  #$path .= '/remap/'.$exposurename;
  $path = $outdir."/$exposurename";

  my $bpm_basename = $basename;
  $basename .= '_'.$tilename;
  $bpm_basename .='.0001_'.$tilename;
  print "$basename\n";
  my $im_basename = $basename.'_im';
  my $imvar_basename = $basename.'_im.weight';
  $bpm_basename .='_bpm';
  my $bpmvar_basename = $bpm_basename.'.weight';

  # Just hardwire the .fits for now:
  my $remap_name = $basename.'.fits';
  my $im_name = $im_basename.'.fits';
  my $imvar_name = $imvar_basename.'.fits';
  my $bpm_name = $bpm_basename.'.fits';
  my $bpmvar_name = $bpmvar_basename.'.fits';

  if (! -r "$path/$im_name" ) {
    warn "Unable to read: $path/$im_name\n";
    $err_flag = 1;
    next;
  }
  if (! -r "$path/$bpm_name" ) {
    warn "Unable to read: $path/$bpm_name\n";
    $err_flag = 1;
    next;
  }
  if (! -r "$path/$imvar_name") {
    warn "Unable to read: $path/$imvar_name\n";
    $err_flag = 1;
    next;
  }

  # Combine:
  my $this_command = join(' ',$command, "$path/$im_name", "$path/$bpm_name", "$path/$imvar_name", "$path/$remap_name");
  print "\nCombining remapped image, bpm and variance";
  print "$this_command";
  my $stat = system ("$this_command");
  if ($stat) { 
    $err_flag = 1;
    print STDERR "Error: $command exited with non-zero exit code ($stat, $filename, $tilename)\n";
  }

  # Remove bpm weight map:
  my $rm_cmd = "rm -f $path/$bpmvar_name";
  print "Removing bpm weight map.\n";
  print "$rm_cmd\n";
  $stat = system ("$rm_cmd");


  # Insert some DES header keywords:
  updateFitsHeaderKeys ({
    'fits_file' => "$path/$remap_name",
    'key_defs' => \%key_defs,
    'values' => {'DES_EXT' => 'IMAGE','OBSTYPE'=>'remap'},
    'hdu' => 1,
    'verbose' => $verbose
  });
  updateFitsHeaderKeys ({
    'fits_file' => "$path/$remap_name",
    'key_defs' => \%key_defs,
    'values' => {'DES_EXT' => 'MASK','OBSTYPE'=>'remap'},
    'hdu' => 2,
    'verbose' => $verbose
  });
  updateFitsHeaderKeys ({
    'fits_file' => "$path/$remap_name",
    'key_defs' => \%key_defs,
    'values' => {'DES_EXT' => 'WEIGHT','OBSTYPE'=>'remap'},
    'hdu' => 3,
    'verbose' => $verbose
  });

}

if ($err_flag) {
  print STDERR "Exiting create_remapMEF.pl with non-zero exit code at end of main loop\n"; 
  exit 1;
}

print STDERR "Exiting create_remapMEF.pl with zero exit code at end of main loop\n"; 

exit 0;
