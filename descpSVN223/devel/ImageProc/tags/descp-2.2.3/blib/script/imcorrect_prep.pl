#!/usr/bin/env perl
########################################################################
#  $Id: imcorrect_prep.pl 8755 2012-09-27 02:29:29Z donaldp $
#
#  $Rev:: 8755                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-26 19:29:29 #$:  # Date of last commit.
#
#  Authors: 
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  DESCRIPTION:
#         Given an input list for imcorrect, it creates the output list
#         Needs to also know the paths for the output files.
#
#######################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking



use strict;
use warnings;

use Getopt::Long;
use File::Basename;
use File::Path;

my ($input_list, $output_list, $output_path) = undef;

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'inputlist|i=s'  => \$input_list,
      'outputlist|o=s'  => \$output_list,
      'outputpath|p=s' => \$output_path,
);

if (scalar(@ARGV) == 3) {
    $input_list = $ARGV[0];
    $output_list = $ARGV[1];
    $output_path = $ARGV[2];
}

if (!defined($input_list) || !defined($output_list) || !defined($output_path)) {
    print STDERR "Usage: imcorrect_prep.pl inputlist outputlist outputpath\n";
    print STDERR "\n";
    print STDERR "or\n";
    print STDERR "Usage: imcorrect_prep.pl [args]\n";
    print STDERR "       -inputlist <filename> (required)\n"; 
    print STDERR "       -outputlist <filename> (required)\n"; 
    print STDERR "       -outputpath <path> (required)\n"; 
    exit 1;
}

open INPUTLIST, "< $input_list";
open OUTPUTLIST, "> $output_list";
while (my $line = <INPUTLIST>) {
    chomp($line);
    if ($line =~ /\S/) {
        my ($name,$path,$suffix) = fileparse($line,('.fits','.fits.gz'));
        $path =~ s/\/$//;
        my $exposure = basename($path);

        print OUTPUTLIST $output_path.'/'.$exposure.'/'.$name.$suffix,"\n";
        eval { mkpath($output_path.'/'.$exposure) };
        if ($@) {
            print "Error\nCouldn't create $output_path.'/'.$exposure: $@\n";
            exit 1;
        }
    }
}
close INPUTLIST;
close OUTPUTLIST;
