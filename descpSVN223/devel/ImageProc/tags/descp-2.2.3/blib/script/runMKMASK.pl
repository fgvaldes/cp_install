#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell
#
# runMKMASK.pl
#
# DESCRIPTION:
#
# AUTHOR:
# Tony Darnell (tdarnell@illinois.edu)
#
# $Rev:: 8754                                           $ Rev of latest commit
# $LastChangedBy:: donaldp                              $ Author of last commit
# $LastChangedDate:: 2012-09-26 19:02:29 -0700 (Wed, 26#$ Date of last commit
# 

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use Data::Dumper;
use FindBin;
use Astro::FITS::CFITSIO qw( :constants );
use Astro::FITS::CFITSIO qw( :longnames );
use File::Path;
use File::Basename;
use Getopt::Long;
use Cwd;

use lib ("$FindBin::Bin/../lib", 
         "$FindBin::Bin/../lib/perl5",
         "$FindBin::Bin/../../../Database/trunk/lib"
        );

use DB::FileUtils;
use DB::EventUtils;

$| = 1;

my($eventStr,$fileList,$etcPath,$binPath,$astroStd,$maskDir,$teraDir,$useVariableRet,$noCosmicRayMask,$noBrightStarMask,$useSantiCode,$nointerpolate) = q();
my $verboseLevel = 2;

Getopt::Long::GetOptions(
   "filelist=s"    => \$fileList,
   "outputpath=s"  => \$maskDir,
   "terapixpath=s" => \$teraDir,
   "etcpath=s"     => \$etcPath,
   "binpath=s"     => \$binPath,
   "astrostd=s"    => \$astroStd,
   "usevariableretina=s" => \$useVariableRet,
   "nocosmicray=s" => \$noCosmicRayMask,
   "usesanticode=s" => \$useSantiCode,
   "nobrightstar=s"=>\$noBrightStarMask,
   "nointerpolate" => \$nointerpolate,
   "verbose=s"     => \$verboseLevel,
) or usage("Invalid command line options\n");




my @files;
my $runtimeDir = cwd();

my $opensuccess = open FH, "< $fileList";
if (!$opensuccess) {
    $eventStr = qq{Error: Could not open list $fileList};
    reportEvent($verboseLevel,"STATUS",5,$eventStr);
    exit 1;
}

my %retinahash = (
               3.76 => "dessn_03.ret",
	       3.85 => "dessn_04.ret",
	       4.07 => "dessn_05.ret",
	       4.34 => "dessn_06.ret",
	       4.63 => "dessn_07.ret",
	       4.94 => "dessn_08.ret",
	       5.25 =>  "dessn_09.ret",
	       5.57 =>  "dessn_10.ret",
    ) ;

#
# Need to go the second hdu in mkmask
#
$astroStd .= '[2]';

foreach my $fileName (<FH>) {
  chomp($fileName);

  # skip blank lines
  if ($fileName !~ /\S/) {
    next;
  }

#
# Check if the file exists on the filesystem
# Need to check if it exists as a fz or gz file
#
  my $compFilename = sniffForCompressedFile($fileName);
  my $compressionType ;
  if (length($compFilename)>0){
  $compressionType = ($compFilename =~ m/(gz$|fz$)/) ? $1 : $2;
  }
  else
  {
      $compressionType = "fits" ;
  }
  if ($compressionType eq 'fz'){
    $eventStr = qq{funpacking: $compFilename};
    reportEvent($verboseLevel,"STATUS",1,$eventStr);
    system("$teraDir/funpack $compFilename");
  } elsif ($compressionType eq 'gz'){
    system("gunzip $compFilename");
  }

  my $baseName = basename($fileName);
  my $dirName = dirname($fileName);
  
  my $resolvePath = $dirName;
  $resolvePath =~ s/^(.*?)\/Archive\///;
  print "$resolvePath\n";
  print "$baseName\n";
  my $fileInfoHashRef = filenameResolve("$resolvePath/$baseName");

  if ($fileInfoHashRef == -1){
    $eventStr = qq{FileName parsing error:  $fileName is not a valid DES fileName};
    reportEvent($verboseLevel,'STATUS',4,$eventStr);
  }

  my $fileType = $fileInfoHashRef->{'FILETYPE'};
  my $oldRun = $fileInfoHashRef->{'RUN'};
  my $outputName = ($fileType eq 'remap') ? $oldRun . "_$baseName" : $baseName;
  my $checkFile = $baseName;
  $checkFile =~ s/\.fits/_check\.fits/;

#
# Check if mask directory exists.  Create if not.
#
  my $outfile ;
  if (defined($maskDir)){
      mkpath($maskDir) if (!-d $maskDir);
        $outfile =  "$maskDir/$outputName" ;
  } else
  { 
        $outfile = $fileName;
  }

# Decide which retina file to use
  my $retinafile ;
  if ($useVariableRet)
     { 
	 my $status = 0;
	 my ($fwhm,$comment) = undef ;
	 my $fptr = Astro::FITS::CFITSIO::open_file(
        "$fileName",
        READONLY,
        $status
	     );

	 if ($status){
	     warn "Problem opening $fileName:  $status\n";
	     $status=0;
	 }
    
	 Astro::FITS::CFITSIO::fits_read_key_flt($fptr,'FWHM',$fwhm,$comment,$status);

	 $eventStr = qq{READ FWHM value=$fwhm from file : $fileName};
	 reportEvent($verboseLevel,"STATUS",1,$eventStr);
	 my @fwhmvals = keys %retinahash ;
	 @fwhmvals = sort @fwhmvals ;
	 my $i ;
	 for ($i=0;$i<=7;$i++)
	  {   
	    last if ($fwhm < $fwhmvals[$i]) ; 
	   }  
	   $i = 1 if ($i<1) ;
	 $retinafile = $retinahash{$fwhmvals[$i-1]};
	# $retinafile = "dessn_01.ret" ;
	#now using Emmanuel's newest retina file in both cases
	 $retinafile = "cosmic_des_9x9.ret" ;
     }
    else
    { # $retinafile = "descosmicray.ret" ; 
	$retinafile = "cosmic_des_9x9.ret" ;
}

# read FWHM header keyword
      

#
# Run sextractor to create the defects file for the masking routine
#
if (!$noCosmicRayMask) 
{
  my $sexName = qq{$fileName} . '[0]';
  my $sex = qq{sex -FILTER_NAME $etcPath/$retinafile };
     $sex .= qq{$sexName -CHECKIMAGE_TYPE OBJECTS };
     $sex .= qq{-CHECKIMAGE_NAME $runtimeDir/$checkFile };
     $sex .= qq{-STARNNW_NAME $etcPath/sex.nnw };
     $sex .= qq{-PARAMETERS_NAME $etcPath/sex.param_nopsfex };
     $sex .= qq{-c $etcPath/sexdescosmic.config};

  print "Executing:\n$sex\n";
  my $starttime = scalar time;
  my $stat = system("$teraDir/$sex");
  my $endtime = scalar time ;
  if (! -e "$runtimeDir/$checkFile" || $stat != 0){
    $eventStr = "WARNING: sextractor failed for image:\n$baseName\nskipping to next image.\n";
    reportEvent($verboseLevel,"STATUS",4,$eventStr);
    next;
  }
  my $processTime = $endtime - $starttime ;
    $eventStr = qq{Running Sextractor took $processTime seconds} ;
    reportEvent($verboseLevel,"STATUS",1,$eventStr);
}
#
# Now run mkmask using the check file and the image to be masked.
#
if ($useSantiCode) 
{ print "running Santi's cosmic ray masking code\n" ;
  my $stat =system("$binPath/cosmics.py $fileName");
  if ($stat !=0) {
      $eventStr = "WARNING: Santi's cosmic ray masking code failed for image:\n$fileName\n";
      reportEvent($verboseLevel,"STATUS",4,$eventStr);
  }
}
  my $mkmask = qq{mkmask $fileName };
  if (!$noCosmicRayMask){$mkmask .= qq{ -crays $runtimeDir/$checkFile -srcgrowrad 2.0 };}
  if (!$noBrightStarMask){$mkmask .= qq{ -stars $astroStd };}
if ($nointerpolate){$mkmask .= qq{-nointerpolate};}
  $mkmask .= qq{ -output $outfile -verbose 3 };

  print "Executing:\n$binPath/$mkmask\n";
  my $stat = system("$binPath/$mkmask");

  if (! -e $outfile || $stat != 0){
    $eventStr = "WARNING: mkmask failed for image:\n$outfile\n";
    reportEvent($verboseLevel,"STATUS",4,$eventStr);
  }

#
# Delete the check file created by sextractor.  This is now part of the
# bad pixel mask of the image so we don't need to keep it around.
#
  unlink("$runtimeDir/$checkFile");

#
# Delete the uncompressed image only if the corresponding compressed image
# exists on disk.
#
  unlink($fileName) if (-e $compFilename);

}
close FH;

#
# Cleanup any remaining checkfiles
#
unlink ("$runtimeDir/*check.fits");

exit(0);

#
# Subroutines
#
sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -filelist <files.list> -etcpath <etcpath>" .
      " -etcpath <etcpath> -binpath <binpath> -terapixpath <terapixpath>\n" .
      " -astrostd <astrostds file> -outputpath <outputpath>\n"
   );

   die("\n")

}

