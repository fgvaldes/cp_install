#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
########################################################################
#
#  $Id: runWL.pl 8754 2012-09-27 02:02:29Z donaldp $
#
#  $Rev:: 8754                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-26 19:02:29 #$:  # Date of last commit.
#
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use warnings;
use File::Basename;
use File::Path;
use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

$|=1;

my $verbose = 1;
my (%opts, $findstars, $measurepsf, $measureshear, $wl_config);

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
my $stat = Getopt::Long::GetOptions(\%opts,
      'list=s',
      'bindir=s',
      'etcdir=s',
      'outdir|output-dir|od=s',
      'config=s',
      'verbose|V=s',
      'help|h|?',
      'man'
);

# Display documentation and exit if requested:
if ($opts{'help'}) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($opts{'man'}) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($opts{'list'})) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide an input list of images.'
  );
}

if (! -r $opts{'list'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Could not read image list: '".$opts{'list'}."'."
  );
}

if (! -r $opts{'list'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Could not read image list: '".$opts{'list'}."'."
  );
}

if ($opts{'outdir'}) {
  if (! -d $opts{'outdir'}) {
    mkpath($opts{'outdir'});
  }
}
else {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide output directory '-outdir'."
  );
}
if (! $opts{'bindir'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide bin directory '-bindir'."
  );
}

if ($opts{'config'}) {
  $wl_config = $opts{'config'};
}
elsif ($opts{'etcdir'}) {
  $wl_config = join('/',$opts{'etcdir'},'wl.config');
}
else {
  $wl_config = "$FindBin::Bin/../etc/wl.config";
}
die "Cannot read config file: $wl_config\n" if (! -r $wl_config);

$findstars = join('/',$opts{'bindir'},'findstars');
$measurepsf = join('/',$opts{'bindir'},'measurepsf');
$measureshear = join('/',$opts{'bindir'},'measureshear');
die "Cannot execute $findstars" if (! -x $findstars);
die "Cannot execute $measurepsf" if (! -x $measurepsf);
die "Cannot execute $measureshear" if (! -x $measureshear);
$findstars .= " $wl_config";
$measurepsf .= " $wl_config";
$measureshear .= " $wl_config";

open (FH,"<".$opts{'list'});
my @list = <FH>;
close FH;

my $N_files = 0;
foreach my $line (@list) {
  # Should make sure no whitespace creaps in to the values:
  chomp $line;
  my @args = split(/\s+/,$line);
  die "\nERROR:Invalid input list provided.\n"if ($#args != 2);
  my $absred = $args[0];
  my $abscat = $args[1];
  my $oldrun = $args[2];
  my ($base_imagename, $image_prefix, $image_ext) = fileparse($absred,'.fits','.fits.fz','.fits.gz');
  my $stars_file = $oldrun.'_'.$base_imagename.'_shpltall'.'.fits';
  my $fitpsf_file = $oldrun.'_'.$base_imagename.'_psfmodel'.'.fits';
  my $psf_file = $oldrun.'_'.$base_imagename.'_shpltpsf'.'.fits';
  my $shear_file = $oldrun.'_'.$base_imagename.'_shear'.'.fits';
  my $abs_stars_file = $opts{'outdir'}.'/'.$stars_file;
  my $abs_fitpsf_file = $opts{'outdir'}.'/'.$fitpsf_file;
  my $abs_psf_file = $opts{'outdir'}.'/'.$psf_file;
  my $abs_shear_file = $opts{'outdir'}.'/'.$shear_file;

  my %FindStarsArgs = (
    'root' => $base_imagename,
    'input_prefix' => $image_prefix,
    'cat_file' => $abscat,
    'stars_file' => $abs_stars_file 
  );
  my $FindStarsCmd = build_cmd($findstars, %FindStarsArgs);
  
  print "\nExecuting:\n$FindStarsCmd\n";
  my $stat = system("$FindStarsCmd");

  if (! -e $abs_stars_file || $stat != 0) {
    warn "\n\nSTATUS4BEG\n";
    warn "WARNING: findstars failed for image:\n$absred\nusing catalog:\n$abscat\nskipping to next image.\n";
    warn "STATUS4END\n\n";
    next;
  }

  my %MeasurePsfArgs = (
    'root' => $base_imagename,
    'input_prefix' => $image_prefix,
    'cat_file' => $abscat,
    'stars_file' => $abs_stars_file,
    'psf_file' =>  $abs_psf_file,
    'fitpsf_file' =>  $abs_fitpsf_file
  );
  my $MeasurePsfCmd = build_cmd($measurepsf, %MeasurePsfArgs);

  print "\nExecuting:\n$MeasurePsfCmd\n";
  $stat = system("$MeasurePsfCmd");

  if (! -e $abs_fitpsf_file || $stat != 0) {
    warn "\n\nSTATUS4BEG\n";
    warn "WARNING: measurepsf failed for image:\n$absred\nusing catalog:\n$abscat\nskipping to next image.\n";
    warn "STATUS4END\n\n";
    next;
  }

  my %MeasureShearArgs = (
    'root' => $base_imagename,
    'input_prefix' => $image_prefix,
    'cat_file' => $abscat,
    'stars_file' => $abs_stars_file,
    'fitpsf_file' =>  $abs_fitpsf_file,
    'shear_file' => $abs_shear_file
  );
  my $MeasureShearCmd = build_cmd($measureshear, %MeasureShearArgs);

  print "\nExecuting:\n$MeasureShearCmd\n";
  $stat = system("$MeasureShearCmd");

  if (! $stat) {
  }
  if (! -e $abs_shear_file) {
  }

  if (! -e $abs_shear_file || $stat != 0) {
    warn "\n\nSTATUS4BEG\n";
    warn "WARNING: measureshear failed for image:\n$absred\nusing catalog:\n$abscat\nskipping to next image.\n";
    warn "STATUS4END\n\n";
    next;
  }
}

sub build_cmd{
  my ($cmd, %Args) = @_;

  while ((my $key, my $val) = each %Args) {
    $cmd .= " $key=$val";
  }

  return $cmd
}

exit 0;

########################################################################
# Documentation
########################################################################


=head1 NAME B<runWL.pl> - Wrapper to run Weak Lensing software on DESDM data sets.

=head1 SYNOPSIS

runWL.pl [options] -list <inputlist> 

=head1 DESCRIPTION

=head1 OPTIONS

=over 4

=item B<-bindir> <path>

Path to WL executables such as findstars, measurepsf, and measureshear

=item B<-config> <fullname wl config> 

Defaults to des_home/etc/wl.config

=item B<-etcdir> <path>

Path to wl.config.   Deprecated.   Use -config

=item B<-help>, B<-h>, B<-?>

Display brief help and exit.

=item B<-man>

Display full manual page and exit.

=item B<-verbose>, B<-V> I<Number>

Verbose level 0-3. Default is 1.  Level 2 and above will turn on status event syntax.

=back

=head1 EXAMPLES
