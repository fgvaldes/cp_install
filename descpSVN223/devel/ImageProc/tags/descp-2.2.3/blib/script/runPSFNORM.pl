#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# runPSFNORM.pl
#
# DESCRIPTION:
#
# AUTHOR:
# Tony Darnell (tdarnell@illinois.edu)
#
# $Rev:: 3508                                           $ Rev of latest commit
# $LastChangedBy:: tdarnell                             $ Author of last commit
# $LastChangedDate:: 2009-04-27 14:37:25 -0700 (Mon, 27#$ Date of last commit
# 

use strict;
use Data::Dumper;
use FindBin;
use File::Path;
use File::Basename;
use Getopt::Long;
use POSIX qw(floor);

use lib ("$FindBin::Bin/../lib", 
         "$FindBin::Bin/../lib/perl5",
         "$FindBin::Bin/../../../Database/trunk/lib"
        );

use DB::FileUtils;
use DB::EventUtils;

$| = 1;

my($eventStr,$fileList,$etcPath,$binPath,$normDir,$teraDir) = q();
my $verboseLevel = 2;
my $fwhmFrac = 0.5;

Getopt::Long::GetOptions(
   "filelist=s"    => \$fileList,
   "outputpath=s"  => \$normDir,
   "terapixpath=s" => \$teraDir,
   "etcpath=s"     => \$etcPath,
   "binpath=s"     => \$binPath,
   "fwhmfrac=s"    => \$fwhmFrac,
   "verbose=s"     => \$verboseLevel,
) or usage("Invalid command line options\n");

my (@files,@fwhms);

readFileList($fileList,\@files);

#
# Go through filelist and select FWHM
#
foreach my $file (@files){
  
  push @fwhms,$file->{'fwhm'};
  
}

my $psfSize = 31;

my $fwhm = getFWHM($fwhmFrac,\@fwhms);

#
# Loop through all files and do the PSF homogenization
#
foreach my $file (@files){

  my $fileName = $file->{'fullname'};

#
# Check if the file exists on the filesystem
# Need to check if it exists as a fz or gz file
# uncompress if necessary
#
  my $compFilename = sniffForCompressedFile($fileName);
  my $compressionType = ($compFilename =~ m/(gz$|fz$)/) ? $1 : $2;
  $compressionType = q() if !defined $compressionType;
  if ($compressionType eq 'fz'){
    $eventStr = qq{funpacking: $compFilename};
    reportEvent($verboseLevel,"STATUS",1,$eventStr);
    system("$teraDir/funpack $compFilename");
  } elsif ($compressionType eq 'gz'){
    system("gunzip $compFilename");
  }

  my $baseName = basename($fileName);
  my $dirName = dirname($fileName);
  
  my $resolvePath = $dirName;
  $resolvePath =~ s/^(.*?)\/Archive\///;
  my $fileInfoHashRef = filenameResolve("$resolvePath/$baseName");

  if ($fileInfoHashRef == -1){
    $eventStr = qq{FileName parsing error:  $fileName is not a valid DES fileName};
    reportEvent($verboseLevel,'STATUS',4,$eventStr);
  }

  my $oldRun = $fileInfoHashRef->{'RUN'};
  my $fileType = $fileInfoHashRef->{'FILETYPE'};
  my $outputName = ($fileType eq 'remap') ? $oldRun . "_$baseName" : $baseName;

#
# Check if norm directory exists.  Create if not.
#
  mkpath($normDir) if (!-d $normDir);

#
# Run sextractor to create the norm_psfcat file
#
  my $psfcatName = $outputName;
  $psfcatName =~ s/(\.fits$|\_mask\.fits$)/_norm_psfcat\.fits/;

  my $sex = qq{sex $fileName\[0\] };
     $sex .= qq{-CATALOG_NAME $normDir/$psfcatName };
     $sex .= qq{-FLAG_IMAGE $fileName\[1\] };
     $sex .= qq{-WEIGHT_IMAGE $fileName\[2\] };
     $sex .= qq{-CATALOG_TYPE FITS_LDAC -WEIGHT_TYPE MAP_WEIGHT };
     $sex .= qq{-FILTER_NAME $etcPath/sex.conv };
     $sex .= qq{-PARAMETERS_NAME $etcPath/sex.param_psfex };
     $sex .= qq{-STARNNW_NAME $etcPath/sex.nnw };
     $sex .= qq{-c $etcPath/default.sex};

  print "Executing:\n$sex\n";
  my $stat = system("$teraDir/$sex");

  if (! -e "$normDir/$psfcatName" || $stat != 0){
    $eventStr = "WARNING: sextractor failed for image:\n$baseName\nskipping to next image.\n";
    reportEvent($verboseLevel,"STATUS",4,$eventStr);
    next;
  }

#
# Now run psfex on norm_psfcat to generate kernel
#

  my $psfkernName = $outputName;
  $psfkernName =~ s/(\.fits$|\_mask\.fits$)/\_norm_kern\.fits/;
  my $psfex = qq{psfex $normDir/$psfcatName };
     $psfex .= qq{-HOMOBASIS_TYPE GAUSS-LAGUERRE };
     $psfex .= qq{-HOMOPSF_PARAMS $fwhm,3.5 -PSF_SIZE $psfSize  };
     $psfex .= qq{-HOMOKERNEL_DIR $normDir };
     $psfex .= qq{-PSF_DIR $normDir };
     $psfex .= qq{-HOMOKERNEL_SUFFIX _norm_kern.fits };
     $psfex .= qq{-c $etcPath/psfnorm.psfex};

  print "Executing:\n$teraDir/$psfex\n";
  $stat = system("$teraDir/$psfex");

  my $tmpCatName = $psfcatName;
  $tmpCatName =~ s/\.fits$//;
  $tmpCatName .= qq{_norm_kern.fits};

  `mv $normDir/$tmpCatName $normDir/$psfkernName`;

  if (! -e "$normDir/$psfkernName" || $stat != 0){
    $eventStr = "WARNING: psfex failed for image:\n$psfkernName\n";
    reportEvent($verboseLevel,"STATUS",4,$eventStr);
    next;
  }

#
# Do the convolution
#
  my $normName = $outputName;
  my $psfnorm = qq{psfnormalize $fileName };
     $psfnorm .= qq{-v -kernel $normDir/$psfkernName };
     $psfnorm .= qq{-output $normDir/$normName };

  print "Executing:\n$binPath/$psfnorm\n";
  $stat = system("$binPath/$psfnorm");

  if (! -e "$normDir/$normName" || $stat != 0){
    $eventStr = "WARNING: psnormalize failed for image:\n$normName\n";
    reportEvent($verboseLevel,"STATUS",4,$eventStr);
  }

#
# Delete the uncompressed image only if the corresponding compressed image
# exists on disk.
#
  unlink($fileName) if (-e $compFilename);

}

exit(0);

#
# Subroutines
#

sub usage {

   my $message = $_[0];
   if (defined $message && length $message) {
      $message .= "\n"
         unless $message =~ /\n$/;
   }

   my $command = $0;
   $command =~ s#^.*/##;

   print STDERR (
      $message,
      "\n" .
      "usage: $command " .
      " -filelist <files.list> -etcpath <etcpath>" .
      " -binpath <binpath> -terapixpath <terapixpath>\n" .
      " -outputpath <outputpath>\n"
   );

   die("\n")

}

sub getFWHM {

      my ($frac, $rpole) = @_;
      my @pole = sort(@$rpole);

      my $nelem = scalar(@pole);

      my $fwhm;

      my $index=floor((($nelem+1) * $frac))-1;
      $index = ($index < 0) ? 0 : $index;
      $index = ($index >= $nelem) ? $nelem-1 : $index;
      $fwhm = $pole[$index];

      return $fwhm;
}

