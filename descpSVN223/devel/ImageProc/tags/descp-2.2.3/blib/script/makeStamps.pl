#!/usr/bin/env perl 
########################################################################
#
#  $Id:                                           $
#
#  $Rev:: 2817                             $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit.
#  $LastChangedDate:: 2009-01-05 12:59:24 #$:  # Date of last commit.
#
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking


use strict;
use warnings;

use Data::Dumper;
use File::Copy;
use File::Basename;
use File::Path;
use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

use DB::EventUtils;
use DB::FileUtils;

$|=1;

my $webhost = "desweb.cosmology.uiuc.edu";
my $webpath = "/data1/SNWG/stamps";

my ($list, $binpath, $logprefix, $des_prereq,$outpath, $verbose, $nocleanup) = undef;
my ($out, $cmd, $stat);

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
Getopt::Long::GetOptions(
      'list=s' => \$list,
      'binpath=s' => \$binpath,
      'des_prereq=s' => \$des_prereq,
      'logprefix=s' => \$logprefix,
      'outpath=s' => \$outpath,
      'nocleanup' => \$nocleanup,
      'verbose|V=i' => \$verbose,
);


# set defaults
if (!defined($verbose)) {
    $verbose = 1;
}
if (!defined($nocleanup)) {
    $nocleanup = 0;
}
else {
    $nocleanup = 1;
}
if (!defined($outpath)) {
    $outpath = '.';
}
if (!defined($logprefix)) {
    $logprefix = 'makestamps_';
}

# check for required arguments
if (!defined($des_prereq)) {
    if (defined($ENV{'DES_PREREQ'})) {
        $des_prereq = $ENV{'DES_PREREQ'};
    }
    else {
        reportEvent($verbose, "STATUS", 5, "Error:  des_prereq needs to be specified on command line or in env");
        exit 1;
    }
}
if (!defined($list)) {
    reportEvent($verbose, "STATUS", 5, "Error:  Need to specify list");
    exit 1;
}
if (!defined($binpath)) {
    reportEvent($verbose, "STATUS", 5, "Error:  Need to specify binpath");
    exit 1;
}

my $opensuccess = open FH, "< $list";
if (!$opensuccess) {
    reportEvent($verbose, "STATUS", 5, "Error:  Could not open file '$list'");
    exit 1;
}
my $linecnt = 0;
foreach my $line (<FH>) {
    $linecnt++;
    if ($line =~ /\S/) {
        $line =~ s/^\s+//;
        $line =~ s/\s+$//;

        my ($search, $template, $difference, $id) = undef;
        if ($line =~ /^(\S+)\s(\S+)\s+(\S+)\s+(\d+)$/) {
            $search=$1;
            $template=$2;
            $difference=$3;
            $id=$4;
        }
        else {
            reportEvent($verbose, "STATUS", 5, "List format problem (line $linecnt)");
            exit 1;
        }

        if (! -r $search) {
            my $compressed = sniffForCompressedFile($search);
            if ($compressed =~ /\S/) {
                $search = $compressed;
            }
            else {
                reportEvent($verbose, "STATUS", 4, "Could not find search image: $search");
                next;
            }
        }

        if (! -r $template) {
            my $compressed = sniffForCompressedFile($template);
            if ($compressed =~ /\S/) {
                $template = $compressed;
            }
            else {
                reportEvent($verbose, "STATUS", 4, "Could not find template image: $template");
                next;
            }
        }

        if (! -r $difference) {
            my $compressed = sniffForCompressedFile($difference);
            if ($compressed =~ /\S/) {
                $difference = $compressed;
            }
            else {
                reportEvent($verbose, "STATUS", 4, "Could not find difference image: $difference");
                next;
            }
        }

        # 1. Copy the 3 fits files to a convenient local directory.
        my ($searchcopy, $templatecopy, $differencecopy) = undef;        
        my ($basename,$basename2, $path,$suffix); 
        ($basename,$path,$suffix) = fileparse($template, qr{\.fits.*$});
        $basename =~ s/_(nc)*distmp//;
        $templatecopy = "${basename}_temp.fits";

#        ($basename2,$path,$suffix) = fileparse($search, qr{\.fits.*$});
#        if ($basename2 ne $basename) {
#            reportEvent($verbose, "STATUS", 4, "Warning: basename of template ($basename) and search ($basename2) files do not match.  Skipping.");
#            next;
#        }
        $searchcopy = "${basename}_srch.fits";

#        ($basename2,$path,$suffix) = fileparse($difference, qr{\.fits.*$});
#        if ($basename2 ne $basename) {
#            reportEvent($verbose, "STATUS", 4, "Warning: basename of template ($basename) and difference ($basename2) files do not match.  Skipping.");
#            next;
#        }
        $differencecopy = "${basename}_diff.fits";

        print "$linecnt: Copying/compressing files.";
        reportEvent($verbose, "STATUS", 1, "Start application copyUncompressed") if $verbose > 3;
        copyUncompressed($search,$searchcopy);
        copyUncompressed($template,$templatecopy);
        copyUncompressed($difference,$differencecopy);
        reportEvent($verbose, "STATUS", 1, "End application copyUncompressed") if $verbose > 3;
        
        #2. Call program "makeStamps 
        my $fdir = $id; 
        mkdir $fdir;
        my $cmd = "$binpath/makeStamps fimg=$basename comb=$id fdir=$fdir log=${logprefix}$basename.log";
        print "$linecnt: Executing $cmd\n";
        reportEvent($verbose, "STATUS", 1, "Start application makeStamps") if $verbose > 3;
        system($cmd);
        my $stat = $?;
        reportEvent($verbose, "STATUS", 1, "End application makeStamps") if $verbose > 3;


        # 3. The files in the output directory should be copied to
        # desweb.cosmology.uiuc.edu:/data1/SNWG/stamps/id
        if (($stat == 0) || (($stat>>8) == 22)) {
            if (($stat>>8) == 22) {
                reportEvent($verbose, "STATUS", 4, "Image limit hit for id $id.  SN processing expert needs to check."); 
            }

            my $lsout = `ls -1 $fdir/* 2>&1`;
            my $stat = $?;
            if ($stat != 0) {   
                reportEvent($verbose, "STATUS", 4, "0 output files for id $id"); 
            }
            else {
                my @files = split /\n/, $lsout;
                my $numfiles = scalar(@files);
                print "$linecnt: $numfiles output files\n";

                print "$linecnt: Copying output to $webhost...";
                reportEvent($verbose, "STATUS", 1, "Start application webcopy") if $verbose > 3;

                # globus-url-copy -r -cd file:///home/bcs/mmgtests/teststamps/
                #   gsiftp://des1.cosmology.uiuc.edu/home/bcs/mmgtests/tstamps2/
                # my $cmd = "uberftp $webhost \"cd $webpath; bput -r $fdir\"";
                
                my $out2 = `which gsiscp 2>&1`;
                $stat = $?;
                if ($stat != 0) {
                    reportEvent($verbose, "STATUS", 5, "gsiscp not in path");
                    $out2 = `printenv DES_HOME 2>&1`;
                    print "DES_HOME= ", $out2,"\n";
                    $out2 = `printenv PATH 2>&1`;
                    print "PATH= ",$out2,"\n";
                    $out2 = `which gsissh 2>&1`;
                    print $out2,"\n";
                    $out2 = `which gsiscp 2>&1`;
                    print $out2,"\n";
                    exit 1;
                }

                my $cmd = "gsiscp -r $fdir $webhost:$webpath";
                $out = `$cmd 2>&1 > ${fdir}_ftp.log`;
                $stat = $?; 
                if ($stat != 0) {
                    reportEvent($verbose, "STATUS", 5, "$id: Error copying files");
                    print "ERROR\n$out\n";
                }

                $cmd = "gsissh $webhost ls -1 $webpath/$fdir/\\*";
                $out = `$cmd 2>&1`;
                my $stat = $?; 
                if ($stat == 0) {
                    my @copyfiles = split /\n/, $out;
                    my $copynumfiles = scalar(@copyfiles);
                    if ($numfiles != $copynumfiles) {
                        reportEvent($verbose, "STATUS", 5, "$id: Not all files were copied ($copynumfiles/$numfiles)");
                        exit 1;
                    }
                }
                else {
                    reportEvent($verbose, "STATUS", 4, "$id: Could not check how many files were copied ($numfiles)");
                    print "ERROR\n$out\n";
                }
                reportEvent($verbose, "STATUS", 1, "End application webcopy") if $verbose > 3;
            }
        }
        else {
            print "$linecnt: Non-zero exit code ($stat). Skipping copy\n";
        }

        # 4. The files copied in step 1 may be deleted. 
        # check before deleting to make sure not accidentally deleting real file
        if (!$nocleanup) {
            print "$linecnt: deleting copies\n"; 
            reportEvent($verbose, "STATUS", 1, "Start application cleanup") if $verbose > 3;
            if (($searchcopy !~ /^\//) && ($searchcopy =~ /_srch.fits/)) {
                print `ls -l $searchcopy 2>&1`, "\n";
                unlink $searchcopy; 
            }
            if (($templatecopy !~ /^\//) && ($templatecopy =~ /_temp.fits/)) {
                print `ls -l $templatecopy 2>&1`, "\n";
                unlink $templatecopy; 
            }
            if (($differencecopy !~ /^\//) && ($differencecopy =~ /_diff.fits/)) {
                print `ls -l $differencecopy 2>&1`, "\n";
                unlink $differencecopy; 
            }
            if (-d $fdir) {
                rmtree($fdir);
            }
            reportEvent($verbose, "STATUS", 1, "End application cleanup") if $verbose > 3;
        }
    }
}
close FH;

sub copyUncompressed {
    my ($src, $dest) = @_;

    if (! -r $src) {
        reportEvent($verbose, "STATUS", 4, "copyUncompressed:  src doesn't exist ($src)");
    }
    elsif ($src =~ /\.fz$/) {
        $dest =~ s/\.fz$//;
        my $cmd = "$des_prereq/bin/funpack -O $dest $src";
        print "copyUncompressed: \n";
        print "    $cmd\n";
        my $out = `$cmd 2>&1`;
        my $stat = $?;
        if ($stat != 0) {
            print "    Error: $out\n";
        }
    }
    elsif ($src =~ /\.gz$/) {
        $dest =~ s/\.gz$//;
        system("gunzip -c $src > $dest");
    }
    else {
        copy($src,$dest);
    }
}
