#!/usr/bin/env perl
########################################################################
#  $Id: create_SWARP_inputs.pl 8754 2012-09-27 02:02:29Z donaldp $
#
#  $Rev:: 8754                             $:  # Revision of last commit.
#  $LastChangedBy:: donaldp                $:  # Author of last commit. 
#  $LastChangedDate:: 2012-09-26 19:02:29 #$:  # Date of last commit.
#
#  Authors: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#         Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#     outputpath is considered to be equivalent to the run directory
#     The directory structure underneath will match archive structure
#      
#
########################################################################

use warnings;
use strict;

use Getopt::Long;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use ImageProc::DB;
use ImageProc::SWARP qw(:all);

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($stat);
my ($man, $help, $version, $verbose,$debug);
my ($detector, $coadd_project);
my ($output_path, $input_path, $input_base, $output_base, $minimum_overlap);
my (@ImageList);
my ($bin_path);

# Set defaults:
$verbose = 1;
$input_base = 'reduced_science';
$output_base = 'swarp_input';
$input_path = 'aux';
$output_path = 'aux';
$minimum_overlap = 0.5; # in arcmin^2

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
$stat = Getopt::Long::GetOptions(
      'detector=s'  => \$detector,
      'input-base=s' => \$input_base,
      'output-base=s' => \$output_base,
      'coadd-project=s' => \$coadd_project,
      'output-path=s' => \$output_path,
      'bin-path=s' => \$bin_path,
      'input-path=s' => \$input_path,
      'minimum-overlap=s' => \$minimum_overlap,
      'debug|d' => \$debug,
      'help|h'  => \$help,
      'man' => \$man,
      'version|v' => \$version,
      'verbose|V=i' => \$verbose
);

# Check inputs
if (!defined($detector)) {
    print STDERR "Must be define detector (either DECam or Mosaic2)\n";
    exit 1;
}

if (($detector ne 'DECam') && ($detector ne 'Mosaic2')) {
    print STDERR "Invalid detector '$detector'\n";
    print STDERR "Must be either DECam or Mosaic2\n";
    exit 1;
}

if (!defined($coadd_project)) {
    print STDERR "Must specify coadd project\n";
    exit 1;
}

# Read image file lists:
opendir DH, "$input_path";
my @imagelists = grep { /$input_base.\d+/ && -f "$input_path/$_" } readdir DH;
close DH;

print "\nFound ",scalar @imagelists, " image lists in the $input_path directory.\n";

# Parse each list of science files for info and place info
# (as a hashref) into the @ImageList array:
foreach my $listfile (@imagelists) {
  # Grab job number from filename:
  $listfile =~ /$input_base.(\d+)/;
  my $jobnum = $1;
  open FH, "<$input_path/$listfile" or die "\nUnable to open $input_path/$listfile\n";

  # Grab keys (from right up to project) from each full path archive 
  # file name entry (line) in the file:
  while (my $file = <FH>) {;
    chomp $file;
    $file =~/\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)$/;
    my $coadd_project = $1;
    my $fileclass = $2;
    my $run =$3;
    my $filetype = $4;
    my $exposurename = $5;
    my $filename = $6;

    # Push hasref of needed keys into image info list:
    push(@ImageList, {
      'jobnum' =>$jobnum,
      'filename' => $filename,
      'exposurename' => $exposurename,
      'run' => $run,
      'absfile' => $file
    }
    );
  }
  close FH;
  
}
########################################################################
# Main function calls of the code:
# 1) Get database connection, then get the tile size information for this 
#    "coadd-project": 
# 2) Get the rest of the info we need from the DB for each image:
# 3) For each image loop over all tiles in the project to find
#    which tiles have the minimum overlap with the image
# 8) Generate input lists for SWARP in format:
#    <abs input file> <tilename> <pixelsize> <tile ra> <tile dec> <tile npix_ra> <tile npix_dec>
#
########################################################################

my $db = ImageProc::DB->new('verbose'=>$verbose);

my $ImageList = $db->getRedImageListInfo (\@ImageList);

getMinMax ({'images' => \@ImageList},$bin_path);

$db->getNewTileMatchInfo ({
    'images' => \@ImageList,
    'coadd_project' => $coadd_project,
    'min_overlap' => $minimum_overlap,
    'debug' => 1
});

createSWARPInputFiles({
  'images' => \@ImageList,
  'output_path' => $output_path,
  'output_base' => $output_base
});



# Output a summary of image info:
if ($verbose >= 3) {
  foreach (@ImageList) {
    while((my $k, my $v) = each %$_) {
      if ($k eq 'tiles_info') {
        print "$k\n";
        foreach my $tile (@$v) {
          print "$tile ";
          while((my $k2, my $v2) = each %$tile) {
            print "$k2 = $v2 ";
          }
          print "\n";
        }
      }
      #if ($k eq 'ccd') {
      #  print "Offset info:\n";
      #  while((my $k2, my $v2) = each %{$Offsets->{$v}}) {
      #      print "$k2 = $v2 ";
      #    }
      #  print "End offset info\n";
      #}
      #else {
        print "$k = $v\n";
      #}
    }
    print "\n";
  }
}


$db->disconnect();

exit 0;


=head1 
