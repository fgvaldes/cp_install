#!/usr/bin/env perl
use strict;
use warnings;

use Getopt::Long;
use File::Basename;
use File::Path;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($verbose,$bin_path,$detector,$quiet,$outpath) = undef;
my ($bias_list,$flat_list,$flat_max,$flat_min,$bias_max,$mask_edges,$edgesize,$respectmask,$template,$ignorebias) = undef;
Getopt::Long::GetOptions(
    "outputpath=s" => \$outpath,
    "biaslist=s" => \$bias_list,
    "flatlist=s" => \$flat_list,
    "verb|v=s" => \$verbose,
    "binpath=s" => \$bin_path,
    "detector=s" => \$detector,
    "quiet" => \$quiet,
    "flatmax|fma=s" =>  \$flat_max,
    "flatmin|fmi=s" =>  \$flat_min,
    "biasmax|bm=s" =>  \$bias_max,
    "mask_edges" => \$mask_edges,
    "edgesize=s" => \$edgesize,
    "respectmask=s" => \$respectmask,
    "image_compare=s" => \$template,
    "ignorebiasimage=s" => \$ignorebias,
);

if (defined($quiet)) {
    $verbose = 0;
}

if (!defined($verbose)) {
    $verbose = 1;
}


my @bias = ();
my @flats = ();

# get list of bias
if (defined($bias_list)) {
    if (! -r $bias_list) {
        print STDERR "STATUS4BEG\n" if ($verbose >= 2);
        print STDERR "Error: Cannot read image list '$bias_list'\n";
        print STDERR "STATUS4END\n" if ($verbose >= 2);
        exit 1;
    }
    open FILE, "< $bias_list" || die "Error: Could not open list file '$bias_list'\n";
    while (my $line = <FILE>) {
        my $bias_name = $line;
        chomp($bias_name);
        push (@bias, $bias_name);
    }
    close FILE
}
else {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: must provide a file containing a list of bias.\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}


printf "  Discovered %d bias\n", scalar(@bias) if ($verbose > 0);


if (scalar(@bias) == 0) {
    print "STATUS4BEG\n" if ($verbose >= 2);
    print "Error: 0 images to determine bad pixel max\n";
    print "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}



# get list of flats
if (defined($flat_list)) {
    if (! -r $flat_list) {
        print STDERR "STATUS4BEG\n" if ($verbose >= 2);
        print STDERR "Error: Cannot read image list '$flat_list'\n";
        print STDERR "STATUS4END\n" if ($verbose >= 2);
        exit 1;
    }
    open FILE, "< $flat_list" || die "Error: Could not open list file '$flat_list'\n";
    while (my $line = <FILE>) {
        my $flat_name = $line;
        chomp($flat_name);
        push (@flats, $flat_name);
    }
    close FILE
}
else {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: must provide a file containing a list of flats.\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}


printf "  Discovered %d images\n", scalar(@flats) if ($verbose > 0);


if (scalar(@flats) == 0) {
    print "STATUS4BEG\n" if ($verbose >= 2);
    print "Error: 0 images to determine bad pixel mask\n";
    print "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}

#If flat list contains more than 1 filter for each ccd, create a single list
#for each filter. Create a bpm for each filter to compare.
#foreach my $flats_filters (@flats) {
#    if ($verbose > 0) {
#        print "separating all the flats by filter name \n";
#    }
#    my ($name, $filter, $ccd, $fits) = ($flats_filters =~ m/(_|.)/);
    #Create a list for each filter
#    if ($filter == 

# for each image
foreach my $biasline (@bias) {
    if ($verbose > 0) {
        print "Creating command line for each ccd \n";
    }
#    my $exposure_name = basename($image,(".fits",".fits.gz",".fits.fz"));

    #find in the flat list the same ccd number to create the bpm
    my ($biasname, $biasccd, $fits) = ($biasline =~ m/(\S+)_(\d+)\.(fits|fits\.fz)/);

    if ($verbose > 0) {
        print "bias lines", $biasline,"\n";
        print "bias name", $biasname,"\n";
        print "bias ccd", $biasccd,"\n";
        print "last part", $fits,"\n";
    }

    foreach my $flatline (@flats) {
        my ($flatname, $filter, $flatccd, $fits) = ($flatline =~  m/(\S+)_(\w)_(\d+)\.(fits|fits\.fz)/);

        if ($verbose > 0) {
            print "flat lines ", $flatline,"\n";
            print "flat name ", $flatname,"\n";
            print "flat filter ", $filter,"\n";
            print "flat ccd ", $flatccd,"\n";
        }

        #select the correct ccd number to create bpm
        if ($biasccd  == $flatccd) {
            print "Creating bpm for ccd '$biasccd'\n";
            my $bpmfile = $outpath.'/'.'bpm_'.$flatccd.'.fits';
            
            my $cmd = "";

            if (defined($bin_path)) {
                $cmd .= "$bin_path/";
            }            
            
            $cmd .= "mkbpm $flatline $biasline $bpmfile";
            
            if (defined($flat_max)) {
                $cmd .= " -flat_max $flat_max";
            }
            if (defined($flat_min)) {
                $cmd .= " -flat_min $flat_min";
            }
            if (defined($bias_max)) {
                $cmd .= " -biasmax $bias_max";
            }
            if (defined($mask_edges)) {
                $cmd .= " -mask_edges";
            }
            if (defined($edgesize)) {
                $cmd .= " -edgesize $edgesize";
            }
            if (defined($respectmask)) {
                $cmd .= " -respectmask";
            }
            if (defined($template)) {
                $cmd .= " -image_compare $template";
            }
            if (defined($ignorebias)) {
                $cmd .= " -ignorebiasimage";
            }


            if (defined($verbose)) {
                $cmd .= " -v $verbose";
            }

       
            print "bpm command: $cmd\n" if ($verbose > 0);
            
            system($cmd);
            if ($? == -1) {
                print STDERR "Failed to execute: $!\n";
            }
            elsif ($? & 127) {
                printf STDERR "Child died with signal %d, %s coredump\n", ($? & 127),  ($? & 128) ? 'with' : 'without';
            }
            else {
                printf STDERR "Child exited with value %d\n", $? >> 8;
            }
        }
    }
}
        
print "\n" if ($verbose >= 0);
        
exit 0;
