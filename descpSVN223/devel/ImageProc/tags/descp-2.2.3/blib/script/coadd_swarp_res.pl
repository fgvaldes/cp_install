#!/usr/bin/perl -w

eval 'exec /usr/bin/perl -w -S $0 ${1+"$@"}'
    if 0; # not running under some shell

#
# coadd_swarp_res.ppl
#
# DESCRIPTION:
#
# AUTHOR:
# Ankit Chandra (ankitc@ncsa.illinois.edu)
#
# $Rev:$
# $LastChangedBy:Ankit Chandra $
# $LastChangedDate: 2010-12-03 11:32:15 -0600 (Thu, 11 Nov 2010) $
#

use strict;
use Cwd;
use Data::Dumper;
use FindBin qw($Bin);
use File::Basename;
use File::stat;
use Getopt::Long;
use Time::localtime;

print "$0 @ARGV\n";

my $destinationfile;
my $tilename;
my $coadd_project;
Getopt::Long::GetOptions(
    "destinationfile=s"    => \$destinationfile,
    "tilename=s"    => \$tilename,
    "coadd_project=s"    => \$coadd_project,
) or usage("Invalid command line options\n");


my $fp;
$| = 1;

use lib ( "$FindBin::Bin/../lib/perl5", "$FindBin::Bin/../lib" );
use DB::DESUtil;


my $dbh = new DB::DESUtil(verbose=>3,debug=>1);

# ANY CHANGE TO THE QUERY BELOW MUST BE REFLECTED IN THE FILE /devel/ImageProc/branches/postgres/src/coadd_swarp.c 
my $dbq = "SELECT RA,DEC,NPIX_RA,NPIX_DEC,PIXELSIZE FROM COADDTILE WHERE tilename='$tilename' and project = '$coadd_project'";
my $rs = $dbh->selectrow_arrayref($dbq);
#print Dumper(@$rs[0]), "\n";

my $ra = @$rs[0];
my $dec = @$rs[1];
my $npix_ra = @$rs[2];
my $npix_dec = @$rs[3];
my $pixelsize = @$rs[4];

#print "$ra, $dec, $npix_ra, $npix_dec, $pixelsize";
$dbh->disconnect();


open ( $fp, ">$destinationfile");
print $fp "$ra $dec $npix_ra $npix_dec $pixelsize";
close($fp);


#
# Subroutines
#

sub usage {

    my $message = $_[0];
    if ( defined $message && length $message ) {
        $message .= "\n"
          unless $message =~ /\n$/;
    }

    my $command = $0;
    $command =~ s#^.*/##;

    print STDERR (
        $message,
        "\n"
          . "usage: $command "
          . " -filelist files -archivenode archive checksum i\n"
          . "       filelist contains the list of files along with the full path\n"
          . "       archivenode corresponds to one of the known archive nodes:\n"
          . "          bcs, des1, etc...\n"
          . "          checksum is optional. To turn checksumming on, supply 1 in place of i. To switch off checksum, supply 0. However, checksum is off by default, so you won't need to explicitly turn it off\n"
    );

    die("\n")

}

