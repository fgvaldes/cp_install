#!/usr/bin/env perl
########################################################################
#  $Id: makebleedmask.pl 7602 2012-03-19 19:27:10Z ricardoc $
#
#  $Rev:: 7602                             $:  # Revision of last commit.
#  $LastChangedBy:: ricardoc               $:  # Author of last commit. 
#  $LastChangedDate:: 2012-03-19 14:27:10 #$:  # Date of last commit.
#
#  Authors: 
#         Ricardo Covarrubias (riccov@illinois.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#       Given a list of images, run mkbleedmask to find all bleedtrails
#       and mask them out. Input images get overwritten.
#      
#
########################################################################

use strict;
use warnings;

use Getopt::Long qw(:config no_ignore_case);
use File::Basename;
use File::Path;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($in_list,$out_list,$verbose,$bin_path,$detector,$quiet) = undef;
my ($interpolate,$interpolate_stars,$b_factor,$f_scale,$l_value,$n_pixel,$qadir,$output_objects,$output_trails,$r_factor,$s_number,$t_npixels,$mask,$w_growrad,$z_zerostar) = undef;
my ($global,$trail_rej,$hole_level,$expand_flag,$trail_weight) = undef;
Getopt::Long::GetOptions(
    "list=s" => \$in_list,
    "outlist=s" => \$out_list,
    "verbose|v=i" => \$verbose,
    "binpath=s" => \$bin_path,
    "detector=s" => \$detector,
    "quiet" => \$quiet,
    "interpolate|i" =>  \$interpolate,
    "interpolate-stars|a" => \$interpolate_stars,
    "starmask|m" =>  \$mask,
    "scalefactor|f=s" =>  \$f_scale,
    "bgreject|b=s" =>  \$b_factor,
    "starlevel|l=s" =>  \$l_value,
    "numtrail|n=s" =>  \$n_pixel,
    "qadir|q=s" =>  \$qadir,
    "saturated_objects|o=s" =>  \$output_objects,
    "trailboxes|x=s" =>  \$output_trails,
    "growbox|r=s" =>  \$r_factor,
    "bgiters|s=s" =>  \$s_number,
    "trail_length|t=s" =>  \$t_npixels,
    "growrad|w=s" => \$w_growrad,
    "zerostarweights|z" => \$z_zerostar,
    "global_only|g" => \$global,
    "trailreject|j=i" => \$trail_rej,
    "holelevel|y=s" => \$hole_level,
    "Expand|E" => \$expand_flag,
    "trail_weight_factor|W=s" => \$trail_weight,

#mkbleedmask [-aeghimzDE] [-b <factor> -d <npix> -f <value> -j <npix> -l <value> -n <npix> -o <filename> -r <factor> -s <n> -t <npixels> -v <level> -w <value> -x <filename> -y <value> -W <factor> ] <infile> <outfile> 

);

if (defined($quiet)) {
    $verbose = 0;
}

if (!defined($verbose)) {
    $verbose = 1;
}


my @images = ();

# get list of src files
if (defined($in_list)) {
    if (! -r $in_list) {
        print STDERR "STATUS4BEG\n" if ($verbose >= 2);
        print STDERR "Error: Cannot read image list '$in_list'\n";
        print STDERR "STATUS4END\n" if ($verbose >= 2);
        exit 1;
    }
    open FILE, "< $in_list" || die "Error: Could not open list file '$in_list'\n";
    while (my $line = <FILE>) {
        my $in_image_name = $line;
        chomp($in_image_name);
        push (@images, $in_image_name);
    }
    close FILE
}
else {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: must provide a file containing a list of images.\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}

my @outimages = ();

# get list of src files
if (defined($out_list)) {
    if (! -r $out_list) {
        print STDERR "STATUS4BEG\n" if ($verbose >= 2);
        print STDERR "Error: Cannot read image list '$out_list'\n";
        print STDERR "STATUS4END\n" if ($verbose >= 2);
        exit 1;
    }
    open FILE, "< $out_list" || die "Error: Could not open list file '$out_list'\n";
    while (my $line = <FILE>) {
        my $out_image_name = $line;
        chomp($out_image_name);
        push (@outimages, $out_image_name);
    }
    close FILE
}
else {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: must provide a file containing a list of images.\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}


printf "  Discovered %d images\n", scalar(@images) if ($verbose > 0);


if (scalar(@images) == 0) {
    print "STATUS4BEG\n" if ($verbose >= 2);
    print "Error: 0 images to detect saturated objects\n";
    print "STATUS4END\n" if ($verbose >= 2);
    exit 1;

}

if (scalar(@outimages) == 0) {
    print "STATUS4BEG\n" if ($verbose >= 2);
    print "Error: 0 images to detect saturated objects\n";
    print "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}

# for each image
foreach my $image (@images) {
    if ($verbose > 0) {
        print "Creating command line for image: $image\n";
    }
#    my $exposure_name = basename($image,(".fits",".fits.gz",".fits.fz"));


    # call convert program to makebleedmask
    my $cmd = "";

    # include path to convert program if specified
    if (defined($bin_path)) {
        $cmd .= "$bin_path/";
    }

    $cmd .= "mkbleedmask";

    my $input_image = $image;
   
    my $tmp_input_image = $input_image; 
    $tmp_input_image =~ s/.*Archive\/\S*\/\S*\/\S*\/\S*\///;

    foreach my $output_image (@outimages) {
	
        my $tmp_outimage = $output_image;
        $tmp_outimage =~ s/.*Archive\/\S*\/\S*\/\S*\/\S*\///;
        if ($tmp_outimage ne $tmp_input_image) {
            next;
        }
    
       

    # add arguments 
    if (defined($interpolate)) {
        $cmd .= " -i";
    }
    if (defined($mask)) {
        $cmd .= " -m";
    }
    if (defined($interpolate_stars)) {
        $cmd .= " -a";
    }
    
     if (defined($b_factor)) {
        $cmd .= " -b $b_factor";
    }

    if (defined($f_scale)) {
        $cmd .= " -f $f_scale";
    }

    if (defined($l_value)) {
        $cmd .= " -l $l_value";
    }

    if (defined($n_pixel)) {
        $cmd .= " -n $n_pixel";
    }

    if (defined($r_factor)) {
        $cmd .= " -r $r_factor";
    }
    
    if (defined($s_number)) {
        $cmd .= " -s $s_number";
    }

    if (defined($t_npixels)) {
        $cmd .= " -t $t_npixels";
    }

    #created the name of the output saturated stars fits table
    if (defined($output_objects)) {
        my  ($tmp_imagename, $path, $sufix) = fileparse($image,qr/\.[^.]*/);
        my @tmp_parts = split("_", $tmp_imagename);
        my $tmp_expos = $tmp_parts[0]."_".$tmp_parts[1];
        my $centers = $qadir."/".$tmp_expos."/".$tmp_imagename."_".$output_objects.".fits";
        $cmd .= " -o $centers";
    }
    #created the name of the output bleed trail fits table
    if (defined($output_trails)) {
        my  ($tmp_imagename, $path, $sufix) = fileparse($image,qr/\.[^.]*/);
        my @tmp_parts = split("_", $tmp_imagename);
        my $tmp_expos = $tmp_parts[0]."_".$tmp_parts[1];
        my $bleedtrails = $qadir."/".$tmp_expos."/".$tmp_imagename."_".$output_trails.".fits";
        $cmd .= " -x $bleedtrails";
    }
        
    if (defined($verbose)) {
        $cmd .= " -v $verbose";
    }

    if (defined($w_growrad)) {
        $cmd .= " -w $w_growrad";
    }

    if (defined($z_zerostar)) {
        $cmd .= " -z";
    }
    
    if (defined($global)) {
        $cmd .= " -g";
    }

    if (defined($trail_rej)) {
        $cmd .= " -j $trail_rej";
    }

    if (defined($hole_level)) {
        $cmd .= " -y $hole_level";
    }

    if (defined($expand_flag)) {
        $cmd .= " -E";
    }

    if (defined($trail_weight)) {
        $cmd .= " -W $trail_weight ";
    }


    $cmd .= " $input_image $output_image";

    print "mkbleedmask command: $cmd\n" if ($verbose > 0);

    system($cmd);
    if ($? == -1) {
        print STDERR "Failed to execute: $!\n";
    }
    elsif ($? & 127) {
        printf STDERR "Child died with signal %d, %s coredump\n", ($? & 127),  ($? & 128) ? 'with' : 'without';
    }
    else {
        printf STDERR "Child exited with value %d\n", $? >> 8;
    }
    }
}

print "\n" if ($verbose >= 0);

exit 0;
