#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell
#$Id: runWL_nite.pl 8754 2012-09-27 02:02:29Z donaldp $#

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking

use strict;
use warnings;
use File::Basename;
use File::Path;
use Getopt::Long;
use Pod::Usage;
use FindBin;
use lib ("$FindBin::Bin/../lib",
         "$FindBin::Bin/../lib/perl5",
         "$FindBin::Bin/../../../Database/trunk/lib"
    );
use DB::FileUtils;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

$|=1;

my $verbose = 1;
my (%opts, $findstars, $measurepsf, $measureshear);

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
my $stat = Getopt::Long::GetOptions(\%opts,
      'list=s',
      'bindir=s',
      'outdir|output-dir|od=s',
#       'config_science=s',
      'config_dict=s',
      'config_desdm=s',
      'config_ops=s',
      'verbose|V=s',
      'debug_wl=s',
      'help|h|?',
      'man'
);

# Display documentation and exit if requested:
if ($opts{'help'}) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($opts{'man'}) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($opts{'list'})) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide an input list of images.'
  );
}

if (! -r $opts{'list'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Could not read image list: '".$opts{'list'}."'."
  );
}

if (! -r $opts{'list'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Could not read image list: '".$opts{'list'}."'."
  );
}

if ($opts{'outdir'}) {
  if (! -d $opts{'outdir'}) {
    mkpath($opts{'outdir'});
  }
}
else {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide output directory '-outdir'."
  );
}
if (! $opts{'bindir'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide bin directory '-bindir'."
  );
}

if (! $opts{'config_dict'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide WL dictionary configuration file '-config_dict'."
  );
}

if (! $opts{'config_desdm'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide DESDM-specific configuration file '-config_desdm'."
  );
}

if (! $opts{'config_ops'}) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => "Must provide operations level WL configuration file '-config_ops'."
  );
}

$findstars = join('/',$opts{'bindir'},'findstars');
$measurepsf = join('/',$opts{'bindir'},'measurepsf');
$measureshear = join('/',$opts{'bindir'},'measureshear');
die "Cannot execute $findstars" if (! -x $findstars);
die "Cannot execute $measurepsf" if (! -x $measurepsf);
die "Cannot execute $measureshear" if (! -x $measureshear);
open (FH,"<".$opts{'list'});
my @list = <FH>;
close FH;

my $N_files = 0;
foreach my $line (@list) {
  # Should make sure no whitespace creaps in to the values:
  chomp $line;
  my @args = split(/\s+/,$line);
  die "\nERROR:Invalid input list provided.\n"if ($#args != 1);
  my $absred = $args[0];
  my $abscat = $args[1];
  my ($base_imagename, $image_prefix, $image_ext) = fileparse($absred,'.fits','.fits.fz','.fits.gz');
#  my $compressed = sniffForCompressedFile($absred);
#  my $compressionType = ($compressed =~ m/(gz$|fz$)/) ? $1 : $2;
#
#  if ((-e $absred) && ($absred !~ m/fz$/))
#  {   $wl_config = join('/',$config_dir,'wl_nocompression.config');
#     }  
# elsif ($compressionType eq 'fz'){
#       $wl_config = join('/',$config_dir,'wl.config');
#  }
  $N_files ++ ;
  my $configstr = sprintf(" %s +%s +%s", $opts{'config_dict'}, $opts{'config_desdm'}, $opts{'config_ops'});

 if ($N_files < 2)
{
 $findstars .= $configstr;
 $measurepsf .= $configstr;
 $measureshear .= $configstr;
}
  my $stars_file = $base_imagename.'_shpltall'.'.fits';
  my $fitpsf_file = $base_imagename.'_psfmodel'.'.fits';
  my $psf_file = $base_imagename.'_shpltpsf'.'.fits';
  my $shear_file = $base_imagename.'_shear'.'.fits';
  my $abs_stars_file = $opts{'outdir'}.'/'.$stars_file;
  my $abs_fitpsf_file = $opts{'outdir'}.'/'.$fitpsf_file;
  my $abs_psf_file = $opts{'outdir'}.'/'.$psf_file;
  my $abs_shear_file = $opts{'outdir'}.'/'.$shear_file;

  my %FindStarsArgs = (
    'root' => $base_imagename,
    'input_prefix' => $image_prefix,
    'cat_file' => $abscat,
    'stars_file' => $abs_stars_file 
  );
  if (defined($opts{'debug_wl'})) {
    $FindStarsArgs{'verbose'} = $opts{'debug_wl'};
  }
  my $FindStarsCmd = build_cmd($findstars, %FindStarsArgs);

  
  print "\nExecuting:\n$FindStarsCmd\n";
  my $stat = system("$FindStarsCmd");

  if (! -e $abs_stars_file || $stat != 0) {
    warn "\n\nSTATUS4BEG\n";
    warn "WARNING: findstars failed for image:\n$absred\nusing catalog:\n$abscat\nskipping to next image.\n";
    warn "STATUS4END\n\n";
    next;
  }

  my %MeasurePsfArgs = (
    'root' => $base_imagename,
    'input_prefix' => $image_prefix,
    'cat_file' => $abscat,
    'stars_file' => $abs_stars_file,
    'psf_file' =>  $abs_psf_file,
    'fitpsf_file' =>  $abs_fitpsf_file
  );
  if (defined($opts{'debug_wl'})) {
    $MeasurePsfArgs{'verbose'} = $opts{'debug_wl'};
  }
  my $MeasurePsfCmd = build_cmd($measurepsf, %MeasurePsfArgs);

  print "\nExecuting:\n$MeasurePsfCmd\n";
  $stat = system("$MeasurePsfCmd");

  if (! -e $abs_fitpsf_file || $stat != 0) {
    warn "\n\nSTATUS4BEG\n";
    warn "WARNING: measurepsf failed for image:\n$absred\nusing catalog:\n$abscat\nskipping to next image.\n";
    warn "STATUS4END\n\n";
    next;
  }

  my %MeasureShearArgs = (
    'root' => $base_imagename,
    'input_prefix' => $image_prefix,
    'cat_file' => $abscat,
    'stars_file' => $abs_stars_file,
    'fitpsf_file' =>  $abs_fitpsf_file,
    'shear_file' => $abs_shear_file
  );
  if (defined($opts{'debug_wl'})) {
    $MeasureShearArgs{'verbose'} = $opts{'debug_wl'};
  }
  my $MeasureShearCmd = build_cmd($measureshear, %MeasureShearArgs);

  print "\nExecuting:\n$MeasureShearCmd\n";
  $stat = system("$MeasureShearCmd");

  if (! $stat) {
  }
  if (! -e $abs_shear_file) {
  }

  if (! -e $abs_shear_file || $stat != 0) {
    warn "\n\nSTATUS4BEG\n";
    warn "WARNING: measureshear failed for image:\n$absred\nusing catalog:\n$abscat\nskipping to next image.\n";
    warn "STATUS4END\n\n";
    next;
  }
}

sub build_cmd{
  my ($cmd, %Args) = @_;

  while ((my $key, my $val) = each %Args) {
    $cmd .= " $key=$val";
  }

  return $cmd
}

exit 0;

########################################################################
# Documentation
########################################################################


=head1 NAME B<runWL.pl> - Wrapper to run Weak Lensing software on DESDM data sets.

=head1 SYNOPSIS

runWL.pl [options] -list <inputlist> 

=head1 DESCRIPTION

=head1 OPTIONS

=over 4

=item B<-bindir> <path>

Path to WL executables such as findstars, measurepsf, and measureshear

=item B<-config_science> <configfile> 

Full path to WL config file (typically the one in the WL install)

=item B<-config_desdm> <configfile> 

Full path to DESDM-specific config file (i.e. operator and framework settings) 

=item B<-help>, B<-h>, B<-?>

Display brief help and exit.

=item B<-man>

Display full manual page and exit.

=item B<-verbose>, B<-V> I<Number>

Verbose level 0-3. Default is 1.  Level 2 and above will turn on status event syntax.

=back

=head1 EXAMPLES
