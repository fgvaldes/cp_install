#!/usr/bin/env perl
########################################################################
#  $Id: create_SCAMP_inputs.pl 9403 2012-10-26 20:41:48Z mgower $
#
#  $Rev:: 9403                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2012-10-26 13:41:48 #$:  # Date of last commit.
#
#  DESCRIPTION:
#
#     outputpath is considered to be equivalent to the run directory
#     The directory structure underneath will match archive structure
#      
#
########################################################################

$0 = $0 . "";   # re-set $0 so that shows up in Oracle tracking
print "cmd: $0 @ARGV\n\n";   # echo command-line so shows up in log

use warnings;
use strict;

use Getopt::Long;
use File::Basename;
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");

use ImageProc::Util qw(getExposureList getDistinctPointings);
use ImageProc::SCAMP qw(:all);
use ImageProc::DB;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($astrostd, $detector, $template, $nite, $run, $project, $list, $truthtable, $outputpath);
my ($dra,$ddec);
my $useDB = 1;
my ($stat, $help, $man, $version);
my $write_ahead = 'y';

# Not used yet.
#my @std_attribute_keys = ('ra','dec','sra','sde','r2','b2','i2');

&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
$stat = Getopt::Long::GetOptions(
      'astrostd=s'  => \$astrostd,
      'detector=s'  => \$detector,
      'template=s' => \$template,
      'list=s' => \$list,
      'nite=s' => \$nite,
      'run=s' => \$run,
      'project=s' => \$project,
      'outputpath=s' => \$outputpath,
      'truthtable=s' => \$truthtable,
      'help|h'  => \$help,
      'man' => \$man,
      'version|v' => \$version,
      'dra|delta_ra=s' => \$dra,
      'ddec|delta_dec=s' => \$ddec,
      'useDB=i' => \$useDB,
      'write_ahead=s' => \$write_ahead
);


print "useDB = $useDB\n";

# Check inputs
if (!defined($list)) {
    print STDERR "Must provide an input list of images\n";
    exit 1;
}
if (! -r $list) {
    print STDERR "Could not read image list: '$list'\n";
    exit 1;
}

if (!defined($detector)) {
    print STDERR "Must be define detector (either DECam or Mosaic2)\n";
    exit 1;
}
if (($detector ne 'DECam') && ($detector ne 'Mosaic2')) {
    print STDERR "Invalid detector '$detector'\n";
    print STDERR "Must be either DECam or Mosaic2\n";
    exit 1;
}

if (lc($write_ahead) ne 'y' && lc($write_ahead) ne 'n') {
    print STDERR "Invalid write_ahead value.   Must be y/n\n";
    exit 1;
}
$write_ahead = lc($write_ahead);
if ($write_ahead eq 'y') {
    if (!defined($template)) {
        $template = "$FindBin::Bin/../etc/${detector}_template.ahead";
        print "Defaulting ahead template to $template\n";
    }
    if (! -r $template) {
        print STDERR "Could not read template file: '$template'\n";
        exit 1;
    }
}

if (!defined($project)) {
    print STDERR "Must specify project\n";
    exit 1;
}

if (!defined($nite)) {
    print STDERR "Must specify nite\n";
    exit 1;
}
if (!defined($run) && !defined($astrostd)) {
    print STDERR "Must specify astrostd or run\n";
    exit 1;
}
if (!defined($outputpath)) {
    $outputpath = '.';
}

if (!defined($truthtable)) {
    $truthtable ='USNOB_CAT1' ;
}
my $truthtableid ;
if ($truthtable =~ m/DC5SIM_USNOB/) {
    $truthtableid = 'DC5SIM_USNOB_ID' ;
}
elsif ($truthtable eq 'DC6SIM_USNOB') {
    $truthtableid = 'DC6SIM_USNOB_ID' ;
}
elsif ($truthtable eq 'GSNSIM_AUG09_USNOB') {
    $truthtableid = 'GSNSIM_AUG09_USNOB_ID' ;
}
elsif ($truthtable eq 'GSN_OCT10SIM_USNOB') {
    $truthtableid = 'GSN_OCT10SIM_USNOB_ID' ;
}
else {$truthtableid = 'USNOB_CAT1_ID' ;}


print "\n";

# read list of images and grab exposure names
my $exposures = getExposureList($list);

print "\nFound ",scalar keys %$exposures," exposures in file list.\n";

#foreach my $e (sort keys %$exposures) {
#    print "$e\n";
#}

# Get database connection, then get "Pointing Data" which is just
# the telra and teldec for each exposure: 
my $db = ImageProc::DB->new();

$db->getPointingData( {
  'project' => $project, 
  'nite' => $nite,
  'exposure_list' => $exposures
});


if ($write_ahead eq 'y') {
    # Create the SCAMP .ahead files:
    createAhead($template, $exposures, $outputpath);
}

########################################################################
# Create a list of unique telra, teldec "pointings" to use
# in the query of the standard star catalog, and call the
# createAstroStds routine to execute those queries and place the
# results in a binary fits catalog:
########################################################################

my $pointings = getDistinctPointings($exposures);

print "\nProcessing ", scalar keys %$pointings," distinct pointings.\n\n";

# Set file name for archive astrostds file:
my ($outdir, $outfile);


if (defined($astrostd)) {
    my ($filename, $path, $suffix) = fileparse($astrostd);
    chop($path);   # remove trailing / so next fileparse will get filetype
    my ($filetype, $path2, $suffix2) = fileparse($path);
    $outdir = $outputpath.'/'.$filetype;
    $outfile = $outdir.'/'.$filename.$suffix;
}
else {
    $outdir = $outputpath.'/'.'aux';
    $outfile = $outdir.'/'.$run.'_'.'astrostds.fits';
}
mkdir $outdir;

# Set size of search window, depending on detector
if ($detector =~ m/DECAM/i) { 
  $dra = 1.1 if (! $dra);
  $ddec = 1.1 if (! $ddec);
}
elsif ($detector =~ m/MOSAIC/i) {
  warn "$detector, not yet supported, using DECam window.\n";
}
else {
  croak("Invalid detector: $detector\n");
}

# Call routine to execute DB query and fits file creation:
print "useDB = $useDB\n";
createAstroStds( {
  'pointing_list' => $pointings,
  'delta_ra' => $dra,
  'delta_dec' => $ddec,
  'dbh' => $db,
  'outfile' => $outfile,
  'std_table_name' => $truthtable,
  'std_table_IDfield' => $truthtableid,
  'useDB' => $useDB,
});


$db->disconnect();

exit 0;
