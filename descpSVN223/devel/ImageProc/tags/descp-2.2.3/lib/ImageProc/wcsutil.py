################################################################################
#
# $Id: wcsutil.py 3199 2009-02-23 20:43:37Z dadams $
#
# wcsutils.py
#
#  $Rev:: 3199                                    $:  # Revision of last commit.
#  $LastChangedBy:: dadams                        $:  # Author of last commit. 
#  $LastChangedDate:: 2009-02-23 13:43:37 -0700 (#$:  # Date of last commit.
#
#  Author: 
#       Erin Sheldon
#       Brookhaven National Laboratory
#
#  DESCRIPTION:
#       Set of tools for WCS transformations.  
#
################################################################################

import numpy
import math

import os

r2d = 180.0/math.pi
d2r = math.pi/180.0

# map the odd scamp naming scheme onto a matrix
# I didn't figure out the formula

_scamp_max_order = 3
_scamp_max_ncoeff = 11
_scamp_skip = [3]

_scamp_map = {}
_scamp_map['pv1_0'] = (0,0)
_scamp_map['pv1_1'] = (1,0)
_scamp_map['pv1_2'] = (0,1)
_scamp_map['pv1_4'] = (2,0)
_scamp_map['pv1_5'] = (1,1)
_scamp_map['pv1_6'] = (0,2)
_scamp_map['pv1_7'] = (3,0)
_scamp_map['pv1_8'] = (2,1)
_scamp_map['pv1_9'] = (1,2)
_scamp_map['pv1_10'] = (0,3)

_scamp_map['pv2_0'] = (0,0)
_scamp_map['pv2_1'] = (0,1)
_scamp_map['pv2_2'] = (1,0)
_scamp_map['pv2_4'] = (0,2)
_scamp_map['pv2_5'] = (1,1)
_scamp_map['pv2_6'] = (2,0)
_scamp_map['pv2_7'] = (0,3)
_scamp_map['pv2_8'] = (1,2)
_scamp_map['pv2_9'] = (2,1)
_scamp_map['pv2_10'] = (3,0)

_allowed_projections = ['-TAN','-TAN-SIP']

_ap = {}
dname='-TAN'
_ap[dname] = {}
_ap[dname]['name'] = 'scamp'
_ap[dname]['aprefix'] = 'pv1'
_ap[dname]['bprefix'] = 'pv2'
_ap[dname]['apprefix'] = 'pvi1'
_ap[dname]['bpprefix'] = 'pvi2'

dname='-TAN-SIP'
_ap[dname] = {}
_ap[dname]['name'] = 'sip'
_ap[dname]['aprefix'] = 'a'
_ap[dname]['bprefix'] = 'b'
_ap[dname]['apprefix'] = 'ap'
_ap[dname]['bpprefix'] = 'bp'

_allowed_units = ['deg']

# same mapping for the inverse
for key in _scamp_map.keys():
    newkey = key.replace('pv','pvi')
    _scamp_map[newkey] = _scamp_map[key]





class WCS:
    """
    A class to do WCS transformations.  Currently supports TAN projections
    for RA---TAN and DEC--TAN and RA---TAN--SIP,DEC--TAN--SIP ctypes in 
    degrees.  Supports distortion models in the SCAMP PV polynomial formalism 
    and SIP.

    Usage:

    import wcsutil
    wcs = wcsutil.WCS(wcs_structure, longpole=180.0, latpole=90.0, theta0=90.0)

    The input is a wcs structure.  This could be a dictionary or numpy array
    that can be addressed like wcs['cunit1'] for example, or have an items()
    method such as for a pyfits header.  It is converted to a dictionary 
    internally.  The structure is exactly that as would be written to a
    FITS header, so for example distortion fields are not converted to a
    matrix form, rather each field in the header gets a field in this
    structure.

    When there is a distortion model the inverse transformation is gotten
    by solving the for the roots of the transformation by default.  This
    is slow, so if you care about speed and not precision you can set
    find=False in sky2image() and it will use a polynomial fit the inverse.
    If the inverse is not in the header, it is fit for.

    Examples:
        # Use a fits header as initialization to a WCS class and convert
        # image (x,y) to equatorial longitude,latitude (ra,dec)
        import wcsutil
        import pyfits
        hdr=pyfits.getheader(fname)
        wcs = wcsutil.WCS(hdr)
        ra,dec = wcs.image2sky(x,y)

        # the inverse. 
        x,y = wcs.sky2image(ra,dec)

    """
    def __init__(self, wcs, longpole=180.0, latpole=90.0, theta0=90.0):

        # Convert to internal dictionary and set some attributes of this
        # instance
        self.wcs = self.ConvertWCS(wcs)

        # Set these as attributes, either from above keywords or from the
        # wcs header
        self.SetAngles(longpole, latpole, theta0)

        # Now set a bunch more instance attributes from the wcs in a form
        # that is easier to work with 
        self.ExtractFromWCS()



    def image2sky(self, xin, yin, distort=True):
        x = numpy.array(xin, ndmin=1, dtype='f8')
        y = numpy.array(yin, ndmin=1, dtype='f8')
        xdiff = x - self.crpix[0]
        ydiff = y - self.crpix[1]

        #print 'diffs going in:',xdiff,ydiff
        p=self.projection.upper()
        if p == '-TAN':
            u,v = self.ApplyCDMatrix(xdiff, ydiff)
            if distort:
                # Assuming PV distortions
                #print 'uv going in: ',u,v
                u, v = self.Distort(u, v)

        elif p == '-TAN-SIP':
            if distort:
                u,v = self.Distort(xdiff, ydiff)
            u, v = self.ApplyCDMatrix(u,v)
        else:
            raise ValueError, "projection '"+p+"' not supported"

        longitude,latitude = self.image2sph(u, v)

        return longitude, latitude

    def sky2image(self, lonin, latin, distort=True, find=True):
        # Only do this if there is distortion
        if find and (self.distort is not None):
            return self._findxy(lonin, latin)

        longitude  = numpy.array(lonin, ndmin=1, dtype='f8')
        latitude = numpy.array(latin, ndmin=1, dtype='f8')

        u, v = self.sph2image(longitude, latitude)

        p=self.projection.upper()
        if p == '-TAN':
            if distort:
                u,v = self.Distort(u, v, inverse=True)
                #print 'uv coming out: ',u,v
            xdiff, ydiff = self.ApplyCDMatrix(u, v, inverse=True)
            #print 'diffs coming out:',xdiff,ydiff

        elif p == '-TAN-SIP':
            u,v = self.ApplyCDMatrix(u, v, inverse=True)
            if distort:
                xdiff, ydiff = self.Distort(u, v, inverse=True)
            else:
                xdiff,ydiff = u,v

        else:
            raise ValueError, "projection '"+p+"' not supported"

        x = xdiff + self.crpix[0]
        y = ydiff + self.crpix[1]

        return x,y

    def ExtractProjection(self, wcs):
        projection = wcs['ctype1'][4:].upper()
        if projection not in _allowed_projections:
            err="Projection type %s unsupported.  Only [%s] projections " + \
                    "currently supported" % (projection,', '.join(_allowed_projections))
            raise ValueError, err

        return projection

    def ApplyCDMatrix(self, x, y, inverse=False):
        if not inverse:
            cd = self.cd
            xp = cd[0,0]*x + cd[0,1]*y
            yp = cd[1,0]*x + cd[1,1]*y
        else:
            cdinv = self.cdinv
            xp = cdinv[0,0]*x + cdinv[0,1]*y
            yp = cdinv[1,0]*x + cdinv[1,1]*y

        return xp, yp

    def image2sph(self, xin, yin):
        """
        Convert x,y projected coordinates to spherical coordinates
        Currently only supports tangent plane projections.
        The conventions assumed are that of the WCS
        Works in the native system currently
        """

        p = self.projection.upper()
        if p == '-TAN' or p == '-TAN-SIP':
            # Make sure ndmin=1 to avoid messed up scalar arrays
            x = numpy.array(xin, ndmin=1, dtype='f8')
            y = numpy.array(yin, ndmin=1, dtype='f8')

            if x.size != y.size:
                raise ValueError,'x and y must be the same size'

            latitude = numpy.zeros_like(x)
            longitude   = numpy.zeros_like(x)

            latitude[:] = math.pi/2

            # radius in radians
            r = numpy.sqrt(x*x + y*y)*math.pi/180.0

            w, = numpy.where( r > 0 )
            if w.size > 0:
                latitude[w] = numpy.arctan(1.0/r[w])

            longitude = numpy.arctan2(x,-y)

            longitude *= r2d
            latitude *= r2d

            longitude,latitude=self.Rotate(longitude,latitude,reverse=True)    
        else:
            raise ValueError, \
                    'Unsupported projection type: %s' % self.projection


        # Make sure the result runs from 0 to 360
        w, = numpy.where(longitude < 0.0)
        if w.size > 0:
            longitude[w] += 360.0
        w, = numpy.where(longitude >= 360.0)
        if w.size > 0:
            longitude[w] -= 360.0

        return longitude,latitude

    
    def sph2image(self, longitude_in, latitude_in):
        p=self.projection.upper()
        if p == '-TAN' or p == '-TAN-SIP':
            longitude=numpy.array(longitude_in, ndmin=1, dtype='f8')
            latitude =numpy.array(latitude_in,  ndmin=1, dtype='f8')

            longitude,latitude = \
                    self.Rotate(longitude, latitude)
            longitude *= d2r
            latitude  *= d2r

            if longitude.size != latitude.size:
                raise ValueError,'long,lat must be the same size'

            x = numpy.zeros_like(longitude)
            y = numpy.zeros_like(longitude)

            w, = numpy.where(latitude > 0.0)
            if w.size > 0:
                rdiv= r2d/numpy.tan(latitude[w])
                x[w] =  rdiv*numpy.sin(longitude[w])
                y[w] = -rdiv*numpy.cos(longitude[w])

        else:
            raise ValueError, \
                    'Unsupported projection type: %s' % self.projection

        return x,y

    def Rotate(self, lon, lat, reverse=False, origin=False):

        longitude = numpy.array(lon, ndmin=1, dtype='f8')*d2r
        latitude  = numpy.array(lat, ndmin=1, dtype='f8')*d2r

        r = self.rotation_matrix
        if reverse:
            r = r.transpose()

        return self._rotate(longitude, latitude, r)

    def CreateRotationMatrix(self):
        # If Theta0 = 90 then CRVAL gives the coordinates of the origin in the
        # native system.   This must be converted (using Eq. 7 in Greisen & 
        # Calabretta with theta0 = 0) to give the coordinates of the North 
        # pole (longitude_p, latitude_p)

        # Longpole is the longitude in the native system of the North Pole in 
        # the standard system (default = 180 degrees).
        sp = math.sin(self.longpole*d2r)
        cp = math.cos(self.longpole*d2r)

        sa = math.sin(self.native_longpole)
        ca = math.cos(self.native_longpole)
        sd = math.sin(self.native_latpole)
        cd = math.cos(self.native_latpole)

        # calculate rotation matrix 

        # IDL array construction is transposed compared to python apparently
        # So this is reversed from the idl routines
        r = numpy.array([[-sa*sp - ca*cp*sd,   sa*cp - ca*sp*sd, ca*cd ] , 
                         [ ca*sp - sa*cp*sd , -ca*cp - sa*sp*sd, sa*cd ] , 
                         [ cp*cd           ,   sp*cd           , sd    ] ], 
                        dtype='f8')

        return r


    def _rotate(self, longitude, latitude, r):
        """
        Apply a rotation matrix to the input longitude and latitude
        inputs must be numpy arrays
        """
        l = numpy.cos(latitude)*numpy.cos(longitude)
        m = numpy.cos(latitude)*numpy.sin(longitude)
        n = numpy.sin(latitude)

        # find solution to the system of equations and put it in b
        # Can't use matrix notation in case l,m,n are rrays

        b0 = r[0,0]*l + r[1,0]*m + r[2,0]*n
        b1 = r[0,1]*l + r[1,1]*m + r[2,1]*n
        b2 = r[0,2]*l + r[1,2]*m + r[2,2]*n

        # Account for possible roundoff
        w, = numpy.where( b2 > 1.0 )
        if w.size > 0:
            b2[w] = 1.0
        w, = numpy.where( b2 < -1.0 )
        if w.size > 0:
            b2[w] = -1.0

        lat_new = numpy.arcsin(b2)*r2d
        lon_new = numpy.arctan2(b1, b0)*r2d

        return lon_new, lat_new


    def _lonlatdiff(self,xy):
        x=numpy.array(xy[0])
        y=numpy.array(xy[1])
        lon,lat=self.image2sky(x,y)
        lonlat=numpy.array([lon[0],lat[0]],dtype='f8')
        diff = lonlat-self.lonlat_answer
        #print diff
        return diff


    def _findxy(self, lonin, latin):
        """ 
        This is the simplest way to do the inverse of the (x,y)->(lon,lat)
        transformation when there are distortions.  Simply find the x,y 
        that give the input lon,lat from the actual distortion function.  
        """
        import scipy.optimize
        lon = numpy.array(lonin, ndmin=1, dtype='f8')
        lat = numpy.array(latin, ndmin=1, dtype='f8')
        if lon.size != lat.size:
            raise ValueError, 'lon and lat must be same size'

        x = numpy.zeros_like(lon)
        y = numpy.zeros_like(lon)
        xyguess = numpy.zeros(2,dtype='f8')
        self.lonlat_answer = numpy.zeros(2,dtype='f8')
        for i in range(lon.size):
            self.lonlat_answer[0],self.lonlat_answer[1] = lon[i],lat[i]

            # Use inversion without distortio as our guess
            xyguess[0], xyguess[1] = \
                    self.sky2image(lon[i], lat[i], find=False, distort=False)
            xy = scipy.optimize.fsolve(self._lonlatdiff, xyguess)
            x[i],y[i] = xy[0], xy[1]
            loncheck,latcheck = self.image2sky(x[i],y[i])
            lonerr, laterr = loncheck-lon[i], latcheck-lat[i]
            #print 'lonerr,laterr =',lonerr,laterr

        return x,y

    def Distort(self, xin, yin, inverse=False):
        """
        Apply a distortion map to the data.  This follows the SIP convention, 
        but if the scamp PV coefficients were found by the ConvertWCS code 
        they are converted to the SIP convention.  The only difference is 
        the order of operations:  for image to sky PV distortions come after 
        the application of the CD matrix as opposed to SIP.  

        """

        x = numpy.array(xin, ndmin=1, dtype='f8')
        y = numpy.array(yin, ndmin=1, dtype='f8')
        # Sometimes there is no distortion model present
        if self.distort is None:
            return x, y
    
        if x.size != y.size:
            raise ValueError, 'x must be same size as y'

        if inverse:
            a = self.distort['ap']
            b = self.distort['bp']
        else:
            a = self.distort['a']
            b = self.distort['b']

        sx,sy = a.shape

        if self.distort['name'] == 'scamp':
            xp = numpy.zeros_like(x)
            yp = numpy.zeros_like(x)
        elif self.distort['name'] == 'sip':
            xp = x.copy()
            yp = y.copy()
        else:
            raise ValueError,\
                    "Unsupported distortion model '"+self.distort['name']+"'"

        xp += Apply2DPolynomial(a, x, y)
        yp += Apply2DPolynomial(b, x, y)

        return xp, yp


    def _compare_inversion(self, x, y, xback, yback, 
                           verbose=False, doplot=False, units=''):
        # Get rms differences
        t=(xback-x)**2 + (yback-y)**2
        rms = numpy.sqrt( t.sum()/t.size )
        if verbose:
            mess='rms error'
            if units != '':
                mess += '('+units+')'
            mess += ':'
            print mess, rms
        if doplot:
            import pylab
            pylab.clf()
            pylab.hist(x-xback,50,edgecolor='black',fill=False)
            pylab.hist(y-yback,50,edgecolor='red',fill=False)
            pylab.show()

        return rms

    def InvertDistortion(self, fac=5, order_increase=1, 
                         verbose=False,doplot=False):
        if self.distort['name'] == 'scamp':
            return self.InvertPVDistortion(fac=fac, 
                                           order_increase=order_increase, 
                                           verbose=verbose,doplot=doplot)
        elif self.distort['name'] == 'sip':
            return self.InvertSipDistortion(fac=fac, 
                                            order_increase=order_increase, 
                                            verbose=verbose,doplot=doplot)
        else:
            raise ValueError,'Can only invert scamp and sip distortions'

    def InvertPVDistortion(self, fac=5, order_increase=1, 
                           verbose=False, doplot=False):
        """
        Invert the distortion model.  Must contain a,b matrices
        """

        if self.distort.has_key('ap'):
            apold = self.distort['ap']
            bpold = self.distort['bp']

        # Order of polynomial
        sx,sy = self.distort['a'].shape
        porder = sx-1

        ng = 2*(porder+2)
        ng *= fac

        # Assuming 1 offset
        xrang=numpy.array([1.0, self.naxis[0]], dtype='f8') - self.crpix[0]
        yrang=numpy.array([1.0, self.naxis[1]], dtype='f8') - self.crpix[1]

        xdiff,ydiff = make_xy_grid(ng, xrang, yrang)

        # same to here
        u,v = self.ApplyCDMatrix(xdiff,ydiff)

        # This is what we will invert
        #up,vp = self.Distort(u,v)
        up = Apply2DPolynomial(self.distort['a'], u, v)
        vp = Apply2DPolynomial(self.distort['b'], u, v)



        # Find polynomial from up,vp to u,v
        ainv, binv = Invert2DPolynomial(up, vp, u, v, porder+order_increase)
        self.distort['ap'] = ainv
        self.distort['bp'] = binv

        #newu, newv = self.Distort(up, vp, inverse=True)
        newu = Apply2DPolynomial(ainv, up, vp)
        newv = Apply2DPolynomial(binv, up, vp)
        ufrac=(u-newu)/u
        vfrac=(v-newv)/v
        if verbose:
            print
            print 'testing inverse now:'
            print
            print '  ufrac=',ufrac
            print '  vfrac=',vfrac
            print
            print '  median ufrac=',numpy.median(ufrac)
            print '  median vfrac=',numpy.median(vfrac)
            print

        uvrms = self._compare_inversion(u,v,newu, newv, 
                                        verbose=verbose,doplot=doplot)

        x = xdiff + self.crpix[0]
        y = ydiff + self.crpix[1]
        lon, lat = self.image2sky(x, y)
        xback, yback = self.sky2image(lon, lat, find=False)

        rms = self._compare_inversion(x, y, xback, yback, verbose=verbose,
                                      doplot=doplot,units='pixels')
        return rms



    def InvertSipDistortion(self, fac=5, 
                            verbose=False, doplot=False, order_increase=1):
        """
        Invert the distortion model.  Must contain a,b matrices
        """

        # Order of polynomial
        sx,sy = self.distort['a'].shape
        porder = sx-1

        ng = 2*(porder+2)
        ng *= fac

        xrang=numpy.array([1.0, self.naxis[0]])
        yrang=numpy.array([1.0, self.naxis[1]])

        x,y = make_xy_grid(ng, xrang, yrang)

        # Use distortion for getting sky coords
        lon, lat = self.image2sky(x, y)
        # Don't use distortion to get back image coords.  We will use
        # the difference to fit for new coefficients. 
        xback, yback = self.sky2image(lon, lat, distort=False, find=False)
        self._compare_inversion(x, y, xback, yback, 
                                verbose=verbose, doplot=doplot)
       
        xdiff = xback - self.crpix[0]
        ydiff = yback - self.crpix[1]

        constant=False
        ainv, binv = \
                Invert2DPolynomial(xdiff, ydiff, x-xback, y-yback, 
                                   porder+order_increase, 
                                   constant=constant)

        if self.distort.has_key('ap'):
            apold = self.distort['ap']
            bpold = self.distort['bp']
            xback2, yback2 = self.sky2image(lon, lat, find=False)
            #print 'Testing existing inversion'
            rms = self._compare_inversion(x, y, xback2, yback2, 
                                          verbose=verbose,
                                          doplot=doplot, units='pixels')
            #print

        self.distort['ap'] = ainv
        self.distort['bp'] = binv

        #print 'ainv=',ainv
        #print 'old ainv=',apold
        #print 'binv=',binv
        #print 'old binv=',bpold
        xback2, yback2 = self.sky2image(lon, lat, find=False)

        rms = self._compare_inversion(x, y, xback2, yback2, 
                                      verbose=verbose,doplot=doplot)
        return rms









    def GetPole(self):

        longitude_0 = self.wcs['crval1']*d2r
        latitude_0 = self.wcs['crval2']*d2r

        if self.theta0 == 90.0:
            return longitude_0, latitude_0

        # Longpole is the longitude in the native system of the North Pole 
        # in the standard system (default = 180 degrees).
        phi_p = self.longpole/radeg
        sp = math.sin(phi_p)
        cp = math.cos(phi_p)
        sd = math.sin(latitude_0)
        cd = math.cos(latitude_0)
        tand = math.tan(latitude_0)

        if self.theta0 == 0.0:
            if latitude_0 == 0 and self.longpole == 90.0:
                latitude_p = self.latpole
            else:
                latitude_p = math.acos( sd/cp )

            if self.latpole != 90.0:
                if math.fabs(self.latpole + latitude_p) < math.fabs(self.latpole - latitude_p):
                    latitude_p = - latitude_p

            if (self.longpole == 180.0) or (cd == 0.0):
                longitude_p = longitude_0
            else:
                longitude_p = longitude_0 - math.atan2(sp/cd, -math.tan(latitude_p)*tand )
        else:
            ctheta = math.cos(self.theta0*d2r)
            stheta = math.sin(self.theta0*d2r)

            term1 = math.atan2( stheta, ctheta*cp )
            term2 = math.acos( sd/( math.sqrt(1.0-ctheta*ctheta*sp*sp) ))

            if term2 == 0.0:
                latitude_p = term1
            else:
                latitude_p1 = math.fabs( (term1+term2)*r2d )
                latitude_p2 = math.fabs( (term1-term2)*r2d )

                if (latitude_p1 > 90.0) and (latitude_p2 > 90.0):
                    raise ValueError, 'No valid solution'
                elif (latitude_p1 < 90.0) and (latitude_p2 > 90.0):
                    latitude_p = term1+term2
                elif (latitude_p1 > 90.0) and (latitude_p2 < 90.0):
                    latitude_p = term1-term2
                else:
                    # Two valid solutions
                    latitude_p1 = (term1+term2)*r2d
                    latitude_p2 = (term1-term2)*r2d
                    if math.fabs(self.latpole-latitude_p1) < \
                       math.fabs(self.latpole-latitude_p2):
                        latitude_p = term1+term2
                    else:
                        latitude_p = term1-term2

                if (cd == 0.0):
                    longitude_p = longitude_0
                else:
                    sdelt = math.sin(latitude_p)
                    if (sdelt == 1.0):
                        longitude_p = longitude_0 - phi_p - math.pi
                    else:
                        if sdelt == -1.0:
                            longitude_p = longitude_0 - phi_p
                        else:
                            sdp = math.sin(latitude_p)
                            cdp = math.cos(latitude_p)
                            longitude_p = longitude_0 - \
                                    math.atan2( (stheta-sdp*sd)/(cdp*cd), 
                                                sp*ctheta/cd )
        return longitude_p, latitude_p



    def ConvertWCS(self, wcs_in):
        """
        Convert to a dictionary
        """

        self.wcs=None
        self.distort={'name':'none'}
        self.cd=None
        self.crpix=None
        self.crval=None
        self.projection=None

        # Convert the wcs to a local dictionary

        wcs = {}
        if type(wcs_in) == numpy.ndarray:
            if wcs_in.dtype.fields is None:
                raise ValueError, 'wcs array must have fields'

            for f in wcs_in.dtype.fields:
                fl = f.lower()
                val = wcs_in[f]
                if val.ndim == 0:
                    wcs[fl] = val
                else:
                    # only scalars
                    wcs[fl] = val[0]

        elif type(wcs_in) == type({}):
            wcs = wcs_in.copy()
        else:
            # Try to use the items() method to get what we want
            wcs={}
            try:
                for k,v in wcs_in.items():
                    wcs[k.lower()] = v
            except:
                raise ValueError,\
                        'Input wcs must be a numpy array with fields or a dictionary or have an items() method'


        return wcs


    def SetAngles(self, longpole, latpole, theta0):
        # These can get set if they were not in the WCS header
        if not self.wcs.has_key('longpole'):
            self.longpole=longpole
        else:
            self.longpole=self.wcs['longpole']

        if not self.wcs.has_key('latpole'):
            self.latpole=latpole
        else:
            self.latpole=self.wcs['latpole']

        if not self.wcs.has_key('theta0'):
            self.theta0=theta0
        else:
            self.theta0=self.wcs['theta0']


    def ExtractUnits(self, wcs):

        units  =wcs['cunit1'].strip().lower() 
        if units not in _allowed_units:
            err='Unsupported units %s.  Only [%s] supported'
            raise ValueError, err % (units, ', '.join(_allowed_units))
        return units

    def ExtractDistortCoeffs(self, dname, wcs, prefix):
        if dname == 'scamp':
            return self.ExtractPVCoeffs(wcs, prefix)
        elif dname == 'sip':
            return self.ExtractSIPCoeffs(wcs, prefix)

    def ExtractPVCoeffs(self, wcs, prefix):
        order=_scamp_max_order
        dim=order+1
        matrix=numpy.zeros((dim,dim), dtype='f8') 
        count=0
        for i in range(_scamp_max_ncoeff):
            if i not in _scamp_skip:
                key = prefix+'_' +str(i)
                if wcs.has_key(key):
                    indices = _scamp_map[key]
                    matrix[indices[0], indices[1]] = wcs[key]
                    count += 1
        return matrix, count, order

    def ExtractSIPCoeffs(self, wcs, prefix):
        order=_dict_get(wcs, prefix+'_order')
        matrix = numpy.zeros((order+1,order+1), dtype='f8')
        count=0
        for ix in range(order+1):
            for iy in range(order+1):
                key=prefix+'_'+str(ix)+'_'+str(iy)
                if wcs.has_key(key):
                    matrix[ix,iy] = wcs[key]
                    count += 1
        return matrix, count, order


    def ExtractDistortionModel(self):
        if self.projection not in _allowed_projections:
            raise ValueError,"Projection must be on of %s " % \
                    ", ".join(_allowed_projections)
        else:
            # look for forward coeffs first
            dinfo = _ap[self.projection]
            dname = dinfo['name']
            a,ca,aorder = self.ExtractDistortCoeffs(dname,
                                                    self.wcs,
                                                    dinfo['aprefix'])

            if ca != 0:
                self.distort['name'] = dname

                b,cb,border = \
                        self.ExtractDistortCoeffs(dname, 
                                                  self.wcs,
                                                  dinfo['bprefix'])
                ap,cap,aporder = \
                        self.ExtractDistortCoeffs(dname, 
                                                  self.wcs,
                                                  dinfo['apprefix'])
                bp,cbp,bporder = \
                        self.ExtractDistortCoeffs(dname, 
                                                  self.wcs,
                                                  dinfo['bpprefix'])

                self.distort['a'] = a
                self.distort['a_order'] = aorder
                self.distort['b'] = b
                self.distort['b_order'] = border

                # these coeffs will be zeros if not found above
                self.distort['ap'] = ap
                self.distort['ap_order'] = aporder
                self.distort['bp'] = bp
                self.distort['bp_order'] = bporder

                # If inverse not there, calculate it
                if cap == 0 or cbp == 0:
                    #print 'Inverse distortion transformation not '+\
                            #        'found, attempting to calculate'
                    self.InvertDistortion()
                    self.distort['ap_order'] = self.distort['a_order']+1
                    self.distort['bp_order'] = self.distort['b_order']+1



    def ExtractFromWCS(self):

        # for easier notation
        wcs = self.wcs

        # set these to little arrays
        self.crpix = numpy.array([wcs['crpix1'],
                                  wcs['crpix2']], dtype='f8')
        self.crval = numpy.array([wcs['crval1'],
                                  wcs['crval2']], dtype='f8')
        self.ctype = numpy.array([wcs['ctype1'],
                                  wcs['ctype2']])
        self.naxis = numpy.array([wcs['naxis1'],
                                  wcs['naxis2']])

        # Get the projection from ctype
        self.projection = self.ExtractProjection(wcs)

        # Get units
        self.units = self.ExtractUnits(wcs)

        # CTYPE[0] - first four characters specify standard system
        #       ('RA--','GLON' or 'ELON' for right ascension, galactic
        #       longitude or ecliptic longitude respectively), second four
        #       letters specify the type of map projection (eg '-AIT' for
        #       Aitoff projection)
        # CTYPE[1] - first four characters specify standard system
        #       ('DEC-','GLAT' or 'ELAT' for declination, galactic latitude
        #       or ecliptic latitude respectively; these must match
        #       the appropriate system of ctype1), second four letters of
        #       ctype2 must match second four letters of ctype1.

        system1 = self.wcs['ctype1'][0:4]
        system2 = self.wcs['ctype2'][0:4]
        self.system = numpy.array([system1, system2], dtype='S4')

        # Add a 2x2 array for the cd matrix
        if wcs.has_key('cd1_1'):
            cd = numpy.zeros( (2,2), dtype='f8')
            cd[0,0] = wcs['cd1_1']
            cd[0,1] = wcs['cd1_2']
            cd[1,0] = wcs['cd2_1']
            cd[1,1] = wcs['cd2_2']
            self.cd = cd

            try:
                self.cdinv = numpy.linalg.inv(cd)
            except:
                raise ValueError, 'Could not find inverse of CD matrix'



        # Get the poles for the inputs.  Assumes we already ran
        # SetAngles() before calling this method
        self.native_longpole, self.native_latpole = self.GetPole()

        # Create the rotation matrix for later.  Requires that the
        # native system be set up using GetPole()
        self.rotation_matrix = self.CreateRotationMatrix()

        # Extract the distortion model
        self.ExtractDistortionModel()

        crap="""
        if self.projection == '-TAN':
            # Look for the PV terms for scamp style distortions
            a,ca = self.ExtractPVCoeffs(wcs,'pv1')
            if ca != 0:
                self.distort['name'] = 'scamp'

                b,cb = self.ExtractPVCoeffs(wcs,'pv2')
                ap,cap = self.ExtractPVCoeffs(wcs,'pvi1')
                bp,cbp = self.ExtractPVCoeffs(wcs,'pvi2')

                self.distort['a'] = a
                self.distort['b'] = b
                # these will be zeros if not found above
                self.distort['ap'] = ap
                self.distort['bp'] = bp

                # If inverse not there, calculate it
                if cap == 0 or cbp == 0:
                    print 'Inverst distortion transformation not '+\
                            'found, attempting to calculate'
                    InvertDistortion()

        elif self.projection == '-TAN-SIP': 
            # Sip style distortions
            a,ca = self.ExtractSIPCoeffs(wcs, 'a')
            if ca != 0:
                self.distort['name'] = 'sip'

                b,cb = self.ExtractSIPCoeffs(wcs, 'b')
                ap,cap = self.ExtractSIPCoeffs(wcs, 'ap')
                bp,cbp = self.ExtractSIPCoeffs(wcs, 'bp')

                self.distort['a'] = a
                self.distort['b'] = b
                # these will be zeros if not found above
                self.distort['ap'] = ap
                self.distort['bp'] = bp

                # If inverse not there, calculate it
                if cap == 0 or cbp == 0:
                    print 'Inverst distortion transformation not '+\
                            'found, attempting to calculate'
                    InvertDistortion()
                    self.distort['ap_order'] = self.distort['a_order']+1
                    self.distort['bp_order'] = self.distort['b_order']+1
        """


def _dict_get(d, key, default=None):
    if not d.has_key(key):
        if default is not None:
            return default
        else:
            raise ValueError, "key '"+key+"' must be present"
    return d[key]




def arrscl(arr, minval, maxval, arrmin=None, arrmax=None):
    # makes a copy either way (asarray would not if it was an array already)
    output = numpy.array(arr)
    
    if arrmin == None: arrmin = output.min()
    if arrmax == None: arrmax = output.max()
    
    if output.size == 1:
        return output
    
    if (arrmin == arrmax):
        print 'arrmin must not equal arrmax'
        return None

    try:
        a = (maxval - minval)/(arrmax - arrmin)
        b = (arrmax*minval - arrmin*maxval)/(arrmax - arrmin)
    except:
        print "Error calculating a,b: ", \
              sys.exc_info()[0], sys.exc_info()[1]
        return None

    # in place
    numpy.multiply(output, a, output)
    numpy.add(output, b, output)
    
    return output



def Apply2DPolynomial(a, x, y):
    v=numpy.zeros_like(x)

    sx,sy = a.shape
    for ix in range(sx):
        for iy in range(sy):
            xpow = x**ix
            ypow = y**iy
            if a[ix,iy] != 0.0:
                addval = a[ix,iy]*xpow*ypow
                #print 'a=',a[ix,iy],'ix=',ix,'iy=',iy
                v += addval

    return v

def make_xy_grid(n, xrang, yrang):
    # Create a grid on input ranges
    rng = numpy.arange(n, dtype='f8')
    ones = numpy.ones(n, dtype='f8')

    x = arrscl(rng, xrang[0], xrang[1])
    y = arrscl(rng, yrang[0], yrang[1])

    x= numpy.outer(x, ones)
    y= numpy.outer(ones, y)
    x = x.flatten(1)
    y = y.flatten(1)

    return x,y

def make_amatrix(u, v, order, constant=True):
    # matrix for inversion.  
    # coeffs_u = A^{-1} x = (a^Ta)^{-1} A^T x
    # coeffs_v = A^{-1} v

    #n = (order+1)*2
    #n = n*n
    n=u.size
    
    tshape = [ (order+1)*(order+2)/2-1, n ]
    if constant:
        # Extra column with ones in it for the constant term
        tshape[0] += 1
        kstart=1
    else:
        kstart=0
    #amatrix = numpy.zeros( tshape )
    amatrix = numpy.ones( tshape )

    kk=kstart
    for order in range(1,order+1):
        for jj in range(order+1):
            amatrix[kk,:] = u**(order-jj)*v**jj
            kk += 1

    #print 'amatrix=',amatrix
    return amatrix

def invert_for_coeffs(amatrix, x, y, lsolve=True):
    # a^T a
    ata = numpy.inner( amatrix, amatrix )
    # a^T x
    atx = numpy.inner( amatrix, x )
    # a^T y
    aty = numpy.inner( amatrix, y )

    if lsolve:
        # More stable solver
        xcoeffs = numpy.linalg.solve( ata, atx )
        ycoeffs = numpy.linalg.solve( ata, aty )

    else:
        atainv = numpy.linalg.inv(ata)
        #atainv = numpy.linalg.pinv(ata)
        xcoeffs = numpy.inner( atainv, atx )
        ycoeffs = numpy.inner( atainv, aty )

    return xcoeffs, ycoeffs

def pack_coeffs(xcoeffs, ycoeffs, porder, constant=True):
    """
    pack coeffs into a matrix form
    """

    if constant:
        ostart=0
    else:
        ostart=1

    kk=0
    shape = (porder+1, porder+1)
    ainv = numpy.zeros(shape)
    binv = numpy.zeros(shape)
    for order in range(ostart,porder+1):
        for jj in range(order+1):
            ainv[order-jj, jj] = xcoeffs[kk]
            binv[order-jj, jj] = ycoeffs[kk]
            kk += 1
    return ainv, binv


# Find the polynomial coeffs that take us from u,v to x,y
def Invert2DPolynomial(u, v, x, y, porder, pack=True, constant=True):
    # matrix for inversion.  
    # coeffs_u = A^{-1} x = (A^TA)^{-1} A^T x
    # coeffs_v = A^{-1} v 
    amatrix = make_amatrix(u, v, porder, constant=constant)
 
    # Now we know the inverse must equal x,y so we use that as the
    # constraint vector
    xcoeffs, ycoeffs = invert_for_coeffs(amatrix, x, y)

    if pack:
        # now pack the coefficients into a matrix
        ainv, binv = pack_coeffs(xcoeffs, ycoeffs, porder, constant=constant)
        return ainv, binv
    else:
        return xcoeffs, ycoeffs

def Ncoeff(order, constant=True):
    ncoeff = (order+1)*(order+2)/2
    if not constant:
        ncoeff -= 1
    return ncoeff


def test_invert_2dpoly(porder, fac=5, constant=True, order_increase=0,
                       inverse=False):

    import pylab

    # total number of constraints should be at least equal to the
    # number of coeffs.  If constant term is included, ncoeff is
    #  (order+1)*(order+2)/2 < (order+2)^2/2 < (order+2)^2
    # So let's do a lot: 20*(order+2)^2

    if porder > 3:
        raise ValueError,'Only testing up to order 3 right now'

    # in making the grid we will square this n
    n = 2*(porder+2)
    n*=fac

    cen = [500.0,1000.0]
    u,v = make_xy_grid(n, [1.0,1000.0], [1.0,2000.0])
    u -= cen[0]
    v -= cen[1]

    if constant:
        x0=2.0
        y0=3.0
        start=0
    else:
        start=1
        x0=0.0
        y0=0.0

    ucoeffs_in = numpy.array(
        [x0, 0.1,0.2, 0.05,0.03,0.04, 0.005,0.004,0.001,0.0009], dtype='f8')
    vcoeffs_in = numpy.array(
        [y0, 0.3,0.5, 0.06,0.05,0.06, 0.004,0.008,0.003,0.002], dtype='f8')
    ucoeffs_in = numpy.array(
        [x0, 
         1.0,1.e-2, 
         5.e-3,3.e-3,4.e-3, 
         0.000,0.000,0.000,0.0000], dtype='f8')
    vcoeffs_in = numpy.array(
        [y0, 
         1.0,2.e-2, 
         6.e-3,5.5e-3,4.e-3, 
         0.000,0.000,0.000,0.000], dtype='f8')

    # number to actuall use
    ncoeff = (porder+1)*(porder+2)/2
    keep = range(start,ncoeff)
    ucoeffs_in = ucoeffs_in[keep]
    vcoeffs_in = vcoeffs_in[keep]

    ain, bin = pack_coeffs(ucoeffs_in, vcoeffs_in, porder,constant=constant)
    x = Apply2DPolynomial(ain, u, v)
    y = Apply2DPolynomial(bin, u, v)

    pylab.clf()

    if not inverse:
        # get poly from u,v to x,y
        ucoeffs,vcoeffs = Invert2DPolynomial(u, v, x, y, porder, pack=False, 
                                             constant=constant)
        ucoeffsp,vcoeffsp = Invert2DPolynomial(u, v, x, y, porder, pack=True, 
                                               constant=constant)
        newx = Apply2DPolynomial(ucoeffsp, u, v)
        newy = Apply2DPolynomial(vcoeffsp, u, v)

        w,=numpy.where( (numpy.abs(x) > 5) & (numpy.abs(y) > 5) )
        xfrac=(x[w]-newx[w])/x[w]
        yfrac=(y[w]-newy[w])/y[w]

        print 'umax,umin=',u.max(),',',u.min(),'vmax,vmin=',v.max(),',',v.min()
        print
        #print 'xfracerr=',xfrac
        #print 'yfracerr=',yfrac
        print 'median(xfracerr)=',numpy.median(xfrac)
        print 'median(abs(xfracerr))=',numpy.median(numpy.abs(xfrac))
        print 'sdev(xfracerr)=',xfrac.std()
        print 'median(yfracerr)=',numpy.median(yfrac)
        print 'median(abs(yfracerr))=',numpy.median(numpy.abs(yfrac))
        print 'sdev(yfracerr)=',yfrac.std()
        print
        pylab.subplot(2,1,1)
        pylab.hist(xfrac,50)
        pylab.hist(yfrac,50,edgecolor='red',fill=False)


        print 'ucoeffs_in =',ucoeffs_in
        print 'ucoeffs_found =',ucoeffs
        print 'vcoeffs_in =',vcoeffs_in
        print 'vcoeffs_found =',vcoeffs

    else:
        # Now test the inverse, from x,y to u,v
        print '\nTesting inverse'
        xcoeffs,ycoeffs = \
                Invert2DPolynomial(x, y, u, v, porder+order_increase,
                                   pack=False, constant=constant)
        xcoeffsp,ycoeffsp = \
                Invert2DPolynomial(x, y, u, v, porder+order_increase, 
                                   pack=True, constant=constant)
        newu = Apply2DPolynomial(xcoeffsp, x, y)
        newv = Apply2DPolynomial(ycoeffsp, x, y)

        print u[0:25]
        print newu[0:25]

        w,=numpy.where( (numpy.abs(u) > 5) & (numpy.abs(v) > 5) )
        ufrac=(u[w]-newu[w])/u[w]
        vfrac=(v[w]-newv[w])/v[w]

        print 'xcoeffs=',xcoeffs
        print 'ycoeffs=',ycoeffs
        print
        print 'median(ufracerr)=',numpy.median(ufrac)
        print 'median(abs(ufracerr))=',numpy.median(numpy.abs(ufrac))
        print 'sdev(ufracerr)=',ufrac.std()
        print 'median(vfracerr)=',numpy.median(vfrac)
        print 'median(abs(vfracerr))=',numpy.median(numpy.abs(vfrac))
        print 'sdev(vfracerr)=',vfrac.std()
        print
        #pylab.subplot(2,1,2)
        pylab.hist(ufrac,50)
        pylab.hist(vfrac,50,edgecolor='red',fill=False)

    pylab.show()


