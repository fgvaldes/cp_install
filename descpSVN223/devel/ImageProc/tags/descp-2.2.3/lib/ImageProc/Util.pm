#######################################################################
#  $Id: Util.pm 6968 2011-06-23 15:25:32Z mgower $
#
#  $Rev:: 6968                             $:  # Revision of last commit.
#  $LastChangedBy:: mgower                 $:  # Author of last commit. 
#  $LastChangedDate:: 2011-06-23 08:25:32 #$:  # Date of last commit.
#
#  Author: 
#    Darren Adams (dadams@ncsa.uiuc.edu)
#    Michelle Gower (mgower@ncsa.uiuc.edu)
#
#  Developed at: 
#    The National Center for Supercomputing Applications (NCSA).
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#
#######################################################################
package ImageProc::Util;

use strict;
use warnings;
use Carp;
use File::Basename;
use Math::Trig;
use Astro::FITS::CFITSIO;
use Astro::FITS::CFITSIO qw(:constants &fits_get_errstatus);

use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5",);
use DB::EventUtils;

our (@ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
BEGIN {
  use Exporter   ();
  @ISA         = qw(Exporter);
  @EXPORT      = qw();
  @EXPORT_OK   = qw(&getExposureList &getDistinctPointings &getSkyWindow 
                    &writeStandardsCat &getScampHeadValues &updateFitsHeaderKeys
                    &appendArgs &removeFilesFromOldRuns &readFitsHeaderKeys
                    &getDesHdus
                 );
  %EXPORT_TAGS = ( 'all' => [ @EXPORT_OK, @EXPORT ] );
}

#######################################################################
#######################################################################
#sub combineFitsFiles {
#  my $Args = shift;
#
#}

#######################################################################
#######################################################################
sub removeFilesFromOldRuns {
  my ($Rows, $CurrentRun) = @_;

  my @unique_keys = ('project', 'fileclass', 'filetype', 'exposurename', 'filename', 'tilename', 'band', 'ccd');
  my %List;

  if (ref $Rows eq 'ARRAY') {
    for (my $i=0; $i<=$#$Rows; $i++ ) {
      my $File = $Rows->[$i];
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }
      if (exists $List{"$str"}) {
        my ($this_date, $junk1) = split (/_/, $File->{'run'});
        my ($other_date, $junk2) = split (/_/, $Rows->[$List{"$str"}]->{'run'});
        if ($this_date gt $other_date) {
          $List{"$str"} = $i;
        }
      }
      else {
        $List{"$str"} = $i;
      }
    }
  }


  elsif (ref $Rows eq 'HASH') {
    while ((my $id, my $File) = each %$Rows) {
      my $str = '';
      foreach my $key (@unique_keys) {
        $str .= $File->{"$key"} if ($File->{"$key"});
      }
      if (exists $List{"$str"}) {
        my $existing_id = $List{"$str"};
#        warn "\nWARNING: Should not the same red file from multiple runs at this stage.\n";
#        warn "This means the set of remaps contains to parents that are the same red file\n";
#        warn "but from different runs.\n";
#        warn "Keeping latest red and continuing.\n";
        my ($this_date, $junk1) = split (/_/, $File->{'run'});
        my ($existing_date, $junk2) = split (/_/, $Rows->{$existing_id}->{'run'});
        if ($this_date gt $existing_date) {
          $List{"$str"} = $id;
          print "Kept: ",$Rows->{$id}->{'run'},"  Discarded: ", $Rows->{$existing_id}->{'run'},"\n";
          delete $Rows->{$existing_id};
        }
        else {
          print "Kept: ",$Rows->{$existing_id}->{'run'},"  Discarded: ", $Rows->{$id}->{'run'},"\n";
          delete $Rows->{$id};

        }
      }
      else {
        $List{"$str"} = $id;
      }
    }
  }

  return \%List;

}


#######################################################################
#  SUBROUTINE: updateFitsHeaderKeys
#
#  INPUT:
#    String:  'fits_file' - Name of file to write to.
#    Hashref: 'values'
#    Hashref: 'key_defs' EX:
#      keyname => {'dtype' => TFLOAT,  'comment' => 'Equinox'}, ...
#    String or arrayref:  'hdu'
# 
#  DESCRIPTION
#    This subroutine take care of updating header keys, just pass in two
#    hashes, one with definitions and another with matching keys and values.
#    By default, it will apply the updates to evey hdu in the file.  
#    Alternatively an absolute hdu # or list of hdu #s (arrayref) can be 
#    passed in on the hdu option.
#
#######################################################################
sub updateFitsHeaderKeys {
  my $Args = shift;

  my ($File, $values, $key_defs, $nhdu);
  my $hdu = 'all';
  my $status = 0;
  my $verbose = 1;

  $File = $Args->{'fits_file'} if ($Args->{'fits_file'});
  $values = $Args->{'values'} if ($Args->{'values'});
  $key_defs = $Args->{'key_defs'} if ($Args->{'key_defs'});
  $hdu = $Args->{'hdu'} if ($Args->{'hdu'}); 
  $verbose = $Args->{'verbose'} if ($Args->{'verbose'});

  croak("Must provide fits file name.\n") if (! $File);
  croak("Unable to read $File") if (! -r $File);
  croak("Must provide a hasref of key-value pairs.\n") if (! $values);
  croak("Must provide a hasref of key-type definitions.\n") if (! $key_defs);

  local * update_keys = sub {
    my $fits = shift;

    foreach my $key (sort keys %$values) {
      next if (! defined $values->{$key});
      my $value = $values->{$key};
      my $type = $key_defs->{$key}->{'dtype'};
      my $comment = $key_defs->{$key}->{'comment'};
      $fits->update_key($type, $key, $value, $comment, $status);
      if ($status) {
            print "Error updating key $key\n";
            if ($verbose > 0) {
                print "\ttype = $type\n";
                print "\tkey = $key\n";
                print "\tvalue = $value\n";
                print "\tcomment = $comment\n";
                print "\tstatus = $status\n"; 
            }
            fitsbail($status, ('verbose'=>$verbose, 'level'=>5));
      }

    }

  };

  # Open file:
  print "\nOpening $File for header update...\n";
  my $fits = Astro::FITS::CFITSIO::open_file( 
    $File,READWRITE,$status
  );
  fitsbail($status) if ($status);

  $fits->get_num_hdus($nhdu,$status);
  if (ref $hdu eq 'ARRAY') {
    foreach my $hdu_num (@$hdu)  {
      $fits->movabs_hdu($hdu_num, ANY_HDU, $status);
      update_keys($fits);
    }
  }
  else {
    if ($hdu =~ /all/i) {
      for(my $hdu_num=1; $hdu_num <=$nhdu; $hdu_num++) {
        $fits->movabs_hdu($hdu_num, ANY_HDU, $status);
        update_keys($fits);
      }
    }
    else {
        if ($hdu <= $nhdu) {
            $fits->movabs_hdu($hdu, ANY_HDU, $status);
            update_keys($fits);
        }
        else {
           reportEvent($verbose,"STATUS",5,"updateFitsHeaderKeys:  hdu to change ($hdu) > num hdus ($nhdu)"); 
        }
    }
  }
}

#######################################################################
#######################################################################
sub getDesHdus {
  my ($File, %Args) = @_;

  
  my $verbose = 1;
  my $nhdu;
  my $status = 0;
  my @key_names = ('IMAGE','MASK','WEIGHT');
  my %DES_keys;
  if (%Args) {
    $verbose = $Args{'verbose'} if ($Args{'verbose'});
  }


  # Open file:
  print "\nOpening $File for DES_EXT discovery...\n" if ($verbose > 1);
  my $fits = Astro::FITS::CFITSIO::open_file( 
    $File,READONLY,$status
  );
  fitsbail($status) if ($status);

  $fits->get_num_hdus($nhdu,$status);

  for(my $hdu_num=1; $hdu_num <=$nhdu; $hdu_num++) {
    my $val;
    my $status = 0;
    $fits->movabs_hdu($hdu_num, ANY_HDU, $status);
    fitsbail($status) if ($status);
    $fits->read_key(TSTRING,'DES_EXT',$val,undef,$status);
    if ($status) {
      if ($status == KEY_NO_EXIST || $status == VALUE_UNDEFINED) {
        $val = undef;
        fitsbail($status,'action' => 'warn','quiet' => 1);
      }
      else {
        fitsbail($status);
      }
    }
    if (defined $val) {
      $DES_keys{uc($val)} = $hdu_num;
    }

  }

  # For backward compatibility - BPM and VARIANCE replaced by MASK and WEIGHT
  if ((!defined($DES_keys{'MASK'})) and (exists($DES_keys{'BPM'}))) {
      $DES_keys{'MASK'} = $DES_keys{'BPM'};
  }
  if ((!defined($DES_keys{'WEIGHT'})) and (exists($DES_keys{'VARIANCE'}))) {
      $DES_keys{'WEIGHT'} = $DES_keys{'VARIANCE'};
  }

  foreach my $key (@key_names) {
    if (! defined $DES_keys{$key}) {
      croak ("The DES_EXT parameter was not found for $key in $File\n");
    }
  }

  return \%DES_keys;

}

#######################################################################
#
# INPUT:
#
# DESCRIPTION:
#   Read a bunch of keys from a fits file and a return a key-value hash
#   of them.
#
#######################################################################
sub readFitsHeaderKeys {
  my $Args = shift;

  my ($File, %values, $key_defs, $nhdu);
  my $hdu = 1;
  my $status = 0;
  my $errmsg;

  $File = $Args->{'fits_file'} if ($Args->{'fits_file'});
  $key_defs = $Args->{'key_defs'} if ($Args->{'key_defs'});
  $hdu = $Args->{'hdu'} if ($Args->{'hdu'});

  croak("Must provide fits file name.\n") if (! $File);
  croak("Unable to read $File") if (! -r $File);
  croak("Must provide a hasref of key-type definitions.\n") if (! $key_defs);

  # Open file:
  print "\nOpening $File for header update...\n";
  my $fits = Astro::FITS::CFITSIO::open_file(
    $File,READONLY,$status
  );
  fitsbail($status) if ($status);

  $fits->movabs_hdu($hdu, ANY_HDU, $status);
  fitsbail($status) if ($status);

  foreach my $key (keys %$key_defs) {
    $status = 0;
    my $val;
    $fits->read_key($key_defs->{$key}->{'dtype'},$key,$val,undef,$status);
    if ($status) {
      if ($status == KEY_NO_EXIST || $status == VALUE_UNDEFINED) {
        $val = undef;
        fitsbail($status,'action' => 'warn','quiet' => 1);
      }
      else {
        fitsbail($status);
      }
    }
    $values{$key} = $val;
  }

  return \%values

}

#######################################################################
#######################################################################
sub getScampHeadValues {
  my $File = shift;
  my $List = shift;

  my @value_sets;

  open (FH, "<$File") or die "Unable to open $File\n";
    my $set = 0;
    while (<FH>) {
      $set++ if (/END/);
      foreach my $key (keys %$List) {
        if (/\s*$key\s*=\s*([^\/]+)/) {
          my $val = $1;
          $val =~ s/\'//g;           # Remove ' tics.
          $val =~ s/\s*$//g;         # Remove trailing whitespace.
          $value_sets[$set]->{"$key"} = $val; 
        }
      }
    }
  close FH;

  return \@value_sets;
}


#######################################################################
# SUBROUTINE: getExposureList
#
# INPUT:
#  string: A file name of a file containing a list of image files
#
# OUTPUT:
#  hashref: Unique set of exposrenames
#
# DESCRIPTION:
#  Open a list of image files (typically filetype='red') and return
#  a hashref list of exposurenames.
#######################################################################
sub getExposureList {
  my $fulllist = shift;

  my (%setexposures);

  open LIST, "< $fulllist" or die "Could not open fulllist '$fulllist'";
  while (my $line = <LIST>) {
     chomp($line);
     my ($filename, $path, $suffix) = fileparse($line);
     $path =~ s/\/$//;
     my $exposure = basename($path);
     push(@{$setexposures{$exposure}}, $line);
  }
  close LIST;

  # Should sort by CCD:
  foreach my $key (keys %setexposures) {
     $setexposures{$key} = [sort @{$setexposures{$key}}];
  }

  return \%setexposures;

}

#######################################################################
# SUBROUTINE: getDistinctPointings
#
# INPUT:
#   hashref: Exposure list with pointing (telra,teldec)
#      exposurelist = {
#        'exp1' => {'telra' => xxx.xx, 'teldec' => xxx.xx}, {}...
#       }
#
# OUTPUT: 
#  hashref: Distinct sets of pointing data.
#
#######################################################################
sub getDistinctPointings {
  my $exposure_list = shift;

  # get distinct list of ra, dec pairs:
  my %distinct_set;
  while (my ($exp, $vals) = each %$exposure_list) {
    my $key  =  join('',$vals->{'telra'}, $vals->{'teldec'});
    $distinct_set{"$key"} = {%$vals};
  }

  return \%distinct_set;
  
} 

#######################################################################
# SUBROUTINE: getSkyWindow
#
# INPUT: 
#   Telescope ra and dec (center of field) and "distances" from 
#   this center to edge of bounding box to search.
#
# OUTPUT: 
#   min and max ra and dec of a square region of sky.
#
#######################################################################
sub getSkyWindow {
  my $telra = shift;
  my $teldec = shift;
  my $dra = shift;
  my $ddec = shift;

  my %coords;

  my $scale = 1/(cos(deg2rad($teldec)));
  print "scale: $scale\n";

  # Set min and max ra, make sure it falls inside 0 and 360;
  $coords{'maxra'} = $telra + $dra*$scale;
  $coords{'minra'} = $telra - $dra*$scale;

  $coords{'maxra'} -= 360.0 if ($coords{'maxra'} > 360.0);
  $coords{'minra'} += 360.0 if ($coords{'minra'} < 0.0);
  $coords{'maxra'} += 360.0 if ($coords{'minra'} > $coords{'maxra'});

  # Set min and max dec:
  $coords{'maxdec'} = $teldec + $ddec;
  $coords{'mindec'} = $teldec - $ddec;

  return \%coords;

}

#######################################################################
# SUBROUTINE: writeStandardsCat
#
# INPUT:
#   hashref - Hash of array refs of standards data.
#   string - Output FITS standards catalog file.
# 
# OUTPUT:
#   File - Astro standards file to use with SCAMP.
#
#######################################################################
sub writeStandardsCat {
  my $standards = shift;
  my $outfile = shift;

  my ($ttype, $tform, $tunit);
  my $status = 0;

  print "Writing standards catalog to ", $outfile, "\n";

  if (-f $outfile) {
    warn "Removing $outfile so I can create it.\n";
    system("rm -f $outfile");
  }

  my $fits = Astro::FITS::CFITSIO::create_file($outfile, $status);
  fitsbail($status) if ($status);

  # Create and populate the first FITS table:
  $ttype = ['Field Header Card'];
  $tform = ['1680A'];
  $fits->create_tbl(BINARY_TBL, 1, 1, $ttype, $tform, undef,'LDAC_IMHEAD', $status);
  
  fitsbail($status) if ($status);

  my $str = 'SIMPLE  =                    T / This is a FITS file                              BITPIX  =                    0 /';
  $fits->write_col(TSTRING, 1, 1, 1, 1,$str, $status);
  fitsbail($status) if ($status);

  # Now create and populate the second table with the standards catalog data:
  my $nrows = scalar @{$standards->{'ra'}};
  my $magerr = 0.400000000000000;
  $ttype = ['X_WORLD', 'Y_WORLD', 'ERRA_WORLD', 'ERRB_WORLD', 'MAG', 'B2MAG', 'I2MAG', 'MAGERR'];
  $tform = ['1D', '1D', '1E', '1E', '1E', '1E', '1E', '1E'];
  $tunit = ['deg', 'deg', 'deg', 'deg', 'mag', 'mag', 'mag', 'mag'];
  $fits->create_tbl(BINARY_TBL, 0, 8, $ttype, $tform, $tunit,'LDAC_OBJECTS', $status);
  fitsbail($status) if ($status);

  # Main table data:
  $fits->write_col(TDOUBLE,1,1,1,$nrows,$standards->{'ra'},$status);
  fitsbail($status) if ($status);
  $fits->write_col(TDOUBLE,2,1,1,$nrows,$standards->{'dec'},$status);
  fitsbail($status) if ($status);
  $fits->write_col(TFLOAT,3,1,1,$nrows,[map {$_*=0.001/3600.0} @{$standards->{'sra'}}],$status);
  fitsbail($status) if ($status);
  $fits->write_col(TFLOAT,4,1,1,$nrows,[map {$_*=0.001/3600.0} @{$standards->{'sde'}}],$status);
  fitsbail($status) if ($status);
  $fits->write_col(TFLOAT,5,1,1,$nrows,$standards->{'r2'},$status);
  fitsbail($status) if ($status);
  $fits->write_col(TFLOAT,6,1,1,$nrows,$standards->{'b2'},$status);
  fitsbail($status) if ($status);
  $fits->write_col(TFLOAT,7,1,1,$nrows,$standards->{'i2'},$status);
  fitsbail($status) if ($status);
  for(my $i=1; $i<=$nrows; $i++) {
    $fits->write_col(TFLOAT,8,$i,1,1,$magerr,$status);
    fitsbail($status) if ($status);
  }


}

sub appendArgs {
  my $cmd = shift;
  my $args = shift;

  my $cargs = '';
  while ((my $k, my $v) = each %$args) {
    $cargs = join(' ',$cargs, $k, $v);
  }
  $cmd = join(' ',$cmd,$cargs);

  return $cmd;

}

sub fitsbail {
    my ($fits_status, %Args) = @_;

    my $quiet = 0;
    my $verbose = 1;
    my $bail = 1;
    my $action = 'bail';
    my $level = 4;
    my $errmsg;
  
    if (%Args) {
        if ($Args{'action'}) {
            if (lc($Args{'action'}) eq 'bail' || lc($Args{'action'}) eq 'exit') {
                $bail = 1;
            }
            else {
                $bail = 0;
            }
        }
        if ($Args{'quiet'}) {
            $quiet = 1;
            $verbose = 0;
        }
        if ($Args{'verbose'}) {
            $verbose = $Args{'verbose'};
        }
        if ($Args{'level'}) {
            $level = $Args{'level'};
        }
    }

    if ($fits_status) {  
        Astro::FITS::CFITSIO::fits_get_errstatus($fits_status, $errmsg);
        reportEvent($verbose,'STATUS',$level,"FITSIO ERROR $fits_status: $errmsg"); 
        if ($bail) {
            croak ("Exiting due to FITSIO ERROR.\n");
        }
    }
}


1;
