#!/bin/csh
if ( $HOST != desdb.cosmology.illinois.edu) then
    echo " this script can only be run on desdb"
    exit 
endif
setenv NITE $argv[1]
setenv PROJECT $argv[2]
# look for files with HDUs less than 8
find  /data2/Archive/$PROJECT/src/$NITE/src/ -name "*.fits.fz" -exec ~desai/test/checkbcshdu {} \;
foreach f (`ls /data2/Archive/$PROJECT/src/$NITE/src/*fits.fz`)
    set basename = `echo $f|awk -F "/" '{print $NF}'`
# find headers of 1st HDU
    ~desai/test/listhead $f\[3\] | awk '{print $1}' | sort > tmp1
# get rid of blank lines
    sed '/^$/d' tmp1 > tmp2
    comm -23 /home/desai/scripts/goodheaders3.list tmp2 > tmp3
    set cnt = `wc -l tmp3|awk '{print $1}'`
    if ($cnt > 0) then
	mv tmp3 $basename.missingheaders
    endif
     rm -f tmp2 tmp3
end
