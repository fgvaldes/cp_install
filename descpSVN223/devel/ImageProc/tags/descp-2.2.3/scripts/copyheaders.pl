#!/usr/bin/perl
$missingheaders=$ARGV[0];
$goodfile=$ARGV[1];
$badfile = $ARGV[2] ;
if (unshift(@ARGV) < 2) {
    print "Usage: ./copyheaders.pl <filecontiningmissingkeywords> <goodfile> <badfile>\n";
    exit;
    }

print "$missingheaders\n" ;
open (FH,$missingheaders) || die "cannot open $missingheaders" ;
foreach $keyword (<FH>) {
    chomp($keyword);
    $keyword =~ s/=//g ;
    if (($keyword =~ /OBSTYPE/) || ($keyword =~ /FILTER/)) {$start=0;}
    else
    {$start=1;}
    for ($i=$start;$i<=16;$i++) {
	$value = `~desai/test/headervalue $goodfile\[$i\] $keyword` ;
	chomp($value);
	if ($keyword =~ /FILTER/) {$value = substr($value,0,1); }
#       do not modify MJD-OBS or DATE-OBS
	if (($keyword !~ /MJD-OBS/i) && ($keyword !~ /DATE-OBS/i))
	{
	print "~desai/test/modhead $badfile\[$i\] $keyword $value\n" ;
	system("~desai/test/modhead $badfile\[$i\] $keyword $value");
	}
	}
}
