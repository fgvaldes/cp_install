/* createTemplate.c 

   Version History:
     1.0   --- initial version

   NOTE: This is a very preliminary version
*/

#include "imageproc.h"

#define VERSION "1.0"
#define BUFFER_DEG 6.0/3600 /* in arcsec */
#define PIXSCALE 0.27

main(int argc, char *argv[]) 
{
  char reduceimage[1000],temp[1000],temp1[1000],temp2[1000],event[1000];
  char project[10],workingnode[100],line[1000],longcomment[5000];
  char templatename[1000],templateshort[1000],templateweight[1000];
  char nitelist[1000],nite_start[100],nite_stop[1000],**nitearray;
  char comment[1000],band[10],telescope[100],detector[100];
  char expname_header[100],expname_image[100],exposurename[100];
  char dblogin[100],sqlcommand[1000],archroot[1000];
  char command[1000],swarpcall[5000],event[2000];
  char run[1000],expname[1000],filename[1000];
  int i,status,ccd_header,count,ccd_image,ccd,ID,imagecount,N_nite=0;
  int flag_verbose=2,flag_nitelist=0,flag_niterange=0,flag_project=0,flag_workingnode=0;
  int flag_template=0,flag_create=0;
  double CRVAL1,CRVAL2;
  float raoffset,decoffset;
  long NAXIS1,NAXIS2;

  FILE *pip,*flist,*fsqlout,*flistout,*fweightout,*fimageinfo;
  fitsfile *fptr;

  void reportevt(),printerror(),select_dblogin();

  if(argc<2) {
    printf("Usage: %s <reduced image> -project <project> -worknode <working node> [Options]",argv[0]);
    printf("\n");

    printf("\t Option:\n");
    printf("\t\t-nitelist <list>\n");
    printf("\t\t-niterange <nite_start> <nite_end>\n");
    printf("\t\t-create\n");
    printf("\t\t-verbose <0-3>\n");
    printf("\t\t-version\n");

    exit(0);
  }
  
  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    /* input project */
    if(!strcmp("-project",argv[i])) {
      flag_project=1;
      sprintf(project,"%s",argv[i+1]);
    }

    /* input working node */
    if(!strcmp("-worknode",argv[i])) {
      flag_workingnode=1;
      sprintf(workingnode,"%s",argv[i+1]);
    }

    /* input nite list */
    if(!strcmp("-nitelist",argv[i])) {
      flag_nitelist=1;
      sprintf(nitelist,"%s",argv[i+1]);
    }

    /* input nite range */
    if(!strcmp("-niterange",argv[i])) {
      flag_niterange=1;
      sprintf(nite_start,"%s",argv[i+1]);
      sprintf(nite_stop,"%s",argv[i+2]);
    }
    
    /* input create option */
    if(!strcmp("-create",argv[i])) 
      flag_create=1;

    /* printout version */
    if(!strcmp("-version",argv[i])) {
      printf("%s Version %s\n",argv[0],VERSION);
      exit(0);
    }
  }
    
  /* ------------------------------- */
  /* INPUT NITES FROM NITE_LIST      */
  /* ------------------------------- */

  if(flag_nitelist) {
    
    sprintf(command,"wc -l %s",nitelist);
    pip=popen(command,"r");
    fscanf(pip,"%d",&N_nite);
    pclose(pip);

    if(!N_nite) {
      sprintf(event," %s ERROR: input nitelist %s is empty, abort. ",argv[0],nitelist);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }

    /* memory allocation for nitearray */
    nitearray=(char **)calloc(N_nite+1,sizeof(char *));
    for(i=1;i<=N_nite;i++)
      nitearray[i]=(char *)calloc(100,sizeof(char));

    /* input the nites from nitelist */
    flist=fopen(nitelist,"r");
    for(i=1;i<=N_nite;i++)
      fscanf(flist,"%s",nitearray[i]);
    fclose(flist);
  } 

  /* ------------------------------- */
  /* CHECK INPUT REDUCED IMAGE       */
  /* ------------------------------- */

  /* input the reduced image and check if it is a fits file from the imagename */
  if (strncmp(&(argv[1][strlen(argv[1])-5]),".fits",5)  
      && strncmp(&(argv[1][strlen(argv[1])-8]),".fits.gz",8)) {
    sprintf(event " %s Error: input %s not end with .fits or .fits.gz (not a fits file?) ",argv[0],argv[1]);
    reportevt(flag_verbose,STATUS,3,event);
    exit(0);
  }
  else
    sprintf(reduceimage,"%s",argv[1]);
  
  /* ---------------------------------------- */
  /* READ IN INFORMATION FROM REDUCED IMAGE   */
  /* ---------------------------------------- */

  /* open the reduced fits image to read in necessary information */
  status=0;
  if(fits_open_file(&fptr,reduceimage,READONLY,&status)) printerror(status);

  /* get the ccd from image header and from input filename */
  status=0;
  if(fits_read_key(fptr,TINT,"CCDNUM",&ccd_header,comment,&status)) printerror(status);

  temp[0]=0;
  sprintf(temp,"%s",reduceimage);
  for(i=strlen(temp);i>=0;i--) {
    if(!strncmp(&(temp[i]),".fits",5))
      temp[i]=0;
    if(!strncmp(&(temp[i]),".fits.gz",8))
      temp[i]=0;
    if(!strncmp(&(temp[i]),"_",1)) {
      temp[i]=32;
      break;
    }
  }
  sscanf(temp,"%s %d",temp1,&ccd_image);
  
  if(ccd_header == ccd_image) 
    ccd=ccd_header;
  else {
    sprintf(event," %s Warning: CCDNUM not same from image name and image header; set CCDNUM to %d ",argv[0],ccd_image);
    reportevt(flag_verbose,STATUS,3,event);
    ccd=ccd_image;
  }

  /* get the exposurename from image header and from input filename */
  status=0;
  if (fits_read_key_str(fptr,"OBJECT",expname_header,comment,&status)) printerror(status);

  for(i=strlen(temp1);i>=0;i--) {
    if(!strncmp(&(temp1[i]),"/",1)) {
      temp1[i]=32;
      break;
    }
  }
  sscanf(temp1,"%s %s",temp2,expname_image);

  if(strcmp(expname_header,expname_image)) {
    sprintf(event," %s Warning: EXPOSURENAME not same from image name and image header; set EXPOSURENAME to %s ",argv[0],expname_image);
    reportevt(flag_verbose,STATUS,3,event);
    sprintf(exposurename,"%s",expname_image);
  }
  else
    sprintf(exposurename,"%s",expname_header);
 
  /* get the band from image header */
  status=0;
  if (fits_read_key_str(fptr,"FILTER",band,comment,&status)) printerror(status);
  
  /* get the telescope from image header */
  status=0;
  if (fits_read_key_str(fptr,"TELESCOP",telescope,comment,&status)) printerror(status);

  /* get the detector from image header */
  status=0;
  if (fits_read_key_str(fptr,"DETECTOR",detector,comment,&status)) printerror(status);

  /* get the CRVAL1 and CRVAL2 from the image header */
  status=0;
  if (fits_read_key_dbl(fptr,"CRVAL1",&CRVAL1,comment,&status)) printerror(status);

  status=0;
  if (fits_read_key_dbl(fptr,"CRVAL2",&CRVAL2,comment,&status)) printerror(status);

  /* get the NAXIS1 and NAXIS2 from the image header */
  status=0;
  if (fits_read_key_lng(fptr,"NAXIS1",&NAXIS1,comment,&status)) printerror(status);

  status=0;
  if (fits_read_key_lng(fptr,"NAXIS2",&NAXIS2,comment,&status)) printerror(status);

  /* close the fits file */
  status=0;
  if(fits_close_file(fptr,&status)) printerror(status);

  /* ------------------------------- */
  /* DATABASE QUERY SETUP            */
  /* ------------------------------- */
     
  /* grab dblogin */
  select_dblogin(dblogin);

  /* setup generil sql call */
  sprintf(sqlcommand,"sqlplus -S %s < getTemplate.sql",dblogin);

  /* ------------------------------------ */
  /* GET THE ARCHROOT FROM WORKING NODE   */
  /* ------------------------------------ */
 
  /* query DB to get archive_root */
  fsqlout=fopen("getTemplate.sql", "w");
  fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
  fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
  fprintf(fsqlout,"select archive_root from archive_sites where location_name='%s';\n",workingnode);
  fprintf(fsqlout,"exit;\n");
  fclose(fsqlout);

  pip=popen(sqlcommand,"r");
  fscanf(pip,"%s",archroot);
  pclose(pip);

  /* ------------------------------------- */
  /* CHECK IF TEMPLATE AVAILABLE FROM DB   */
  /* ------------------------------------- */

  /* construct the query to check the template images */
  fsqlout=fopen("getTemplate.sql", "w");
  fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
  fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
  fprintf(fsqlout,"select image.id,image.run,location.filename,location.exposurename from image,location where ");
  fprintf(fsqlout,"image.imagetype='diff_template' ");
  fprintf(fsqlout,"and image.id=location.id ");
  fprintf(fsqlout,"and image.project='%s' ",project);
  fprintf(fsqlout,"and image.band='%s' ",band);
  fprintf(fsqlout,"and image.CCD=%d ",ccd);
  fprintf(fsqlout,"and location.exposurename='%s' ",exposurename);
  fprintf(fsqlout,"and (image.CRVAL1 between %2.8f and %2.8f) and (image.CRVAL2 between %2.8f and %2.8f);\n ",
	  CRVAL1-BUFFER_DEG/cos(CRVAL2*M_PI/180.0),CRVAL1+BUFFER_DEG/cos(CRVAL2*M_PI/180.0),CRVAL2-BUFFER_DEG,CRVAL2+BUFFER_DEG);
  fprintf(fsqlout,"exit;\n");
  fclose(fsqlout);  
  
  count=0;
  pip=popen(sqlcommand,"r");
  while(fscanf(pip,"%d %s %s %s",&ID,run,filename,expname)!=EOF) {
    printf("%d\t%s/red/%s/red/%s/%s\n",ID,project,run,expname,filename);
    count++;
  }
  pclose(pip);

  if(count)
    flag_template=1; /* no need to create the template image */
  else
    flag_template=0; /* create the template image */

  /* ------------------------------------- */
  /* CHECK IF NEED TO CREATE TEMPLATE   */
  /* ------------------------------------- */

  if(flag_create)
    flag_template=0;

  /* -------------------------------------- */
  /* CREATE TEMPLATE IF NOT FOUND FROM DB   */
  /* -------------------------------------- */

  if(!flag_template) {
    
    /* construct the query to select the image list */
    fsqlout=fopen("getTemplate.sql", "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");  

    if(flag_nitelist) {
      for(i=1;i<=N_nite;i++) {
	fprintf(fsqlout,"select image.id,image.run,location.filename,location.exposurename from image,location where ");
	fprintf(fsqlout,"image.imagetype='red' ");
	fprintf(fsqlout,"and image.id=location.id ");
	fprintf(fsqlout,"and image.project='%s' ",project);
	fprintf(fsqlout,"and image.band='%s' ",band);
	fprintf(fsqlout,"and image.CCD=%d ",ccd);
	fprintf(fsqlout,"and (image.CRVAL1 between %2.8f and %2.8f) and (image.CRVAL2 between %2.8f and %2.8f) ",
		CRVAL1-BUFFER_DEG/cos(CRVAL2*M_PI/180.0),CRVAL1+BUFFER_DEG/cos(CRVAL2*M_PI/180.0),CRVAL2-BUFFER_DEG,CRVAL2+BUFFER_DEG);
	fprintf(fsqlout,"and image.nite='%s';\n",nitearray[i]);
      }
      fprintf(fsqlout,"exit;\n");
    }
    else {
      fprintf(fsqlout,"select image.id,image.run,location.filename,location.exposurename from image,location where ");
      fprintf(fsqlout,"image.imagetype='red' ");
      fprintf(fsqlout,"and image.id=location.id ");
      fprintf(fsqlout,"and image.project='%s' ",project);
      fprintf(fsqlout,"and image.band='%s' ",band);
      fprintf(fsqlout,"and image.CCD=%d ",ccd);
      fprintf(fsqlout,"and (image.CRVAL1 between %2.8f and %2.8f) and (image.CRVAL2 between %2.8f and %2.8f) ",
	      CRVAL1-BUFFER_DEG*cos(CRVAL2*M_PI/180.0),CRVAL1+BUFFER_DEG*cos(CRVAL2*M_PI/180.0),CRVAL2-BUFFER_DEG,CRVAL2+BUFFER_DEG);
      if(flag_niterange)
	fprintf(fsqlout,"and (image.nite between '%s' and '%s')",nite_start,nite_stop);
      fprintf(fsqlout,";\nexit;\n");
    }
    fclose(fsqlout);

    /* create lists for images to be coadded [and transfer the image?] */
    flistout=fopen("Image.list","w");
    fweightout=fopen("Weight.list","w");
    fimageinfo=fopen("ImageInfo.list","w");
    pip=popen(sqlcommand,"r");
    imagecount=0;
    while(fscanf(pip,"%d %s %s %s",&ID,run,filename,expname)!=EOF) {
      imagecount++;

      if(!flag_quiet) 
	printf("%d\t%s\t%s\n",ID,run,filename);  

      fprintf(fimageinfo,"%d\t%s\t%s\n",ID,run,filename);  
      
      /* copy the reduced images to local workspace, to avoid the reduced images that have the same name */
      /* or arcp? */
      sprintf(command,"cp %s/%s/red/%s/red/%s/%s image%02d.fits\n",archroot,project,run,expname,filename,imagecount);
      system(command);

      fprintf(flistout,"image%02d.fits[0]\n",imagecount);
      fprintf(fweightout,"image%02d.fits[2]\n",imagecount);  
    }
    pclose(pip); fclose(flistout); fclose(fweightout); fclose(fimageinfo);

    /* get the raoffset and decoffset from the wcsoffset table */
    fsqlout=fopen("getTemplate.sql", "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");  
    fprintf(fsqlout,"select raoffset,decoffset from wcsoffset ");
    fprintf(fsqlout,"where chipid=%d ",ccd);
    fprintf(fsqlout,"and detector='%s' and telescope='%s';\n",detector,telescope);
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);

    pip=popen(sqlcommand,"r"); 
    fscanf(pip,"%f %f",&raoffset,&decoffset);
    pclose(pip);
      
    /* create the template name */
    sprintf(templatename,"%s_%02d_template.fits",exposurename,ccd);
    sprintf(templateshort,"%s_%02d_template",exposurename,ccd);
    sprintf(templateweight,"%s_%02d_template.weight.fits",exposurename,ccd);
    
    /* construct swarp call here */
    sprintf(swarpcall,"$DES_PREREQ/bin/swarp @Image.list "); 
    sprintf(swarpcall,"%s -IMAGEOUT_NAME %s ",swarpcall,templatename);
    sprintf(swarpcall,"%s -WEIGHTOUT_NAME %s ",swarpcall,templateweight);
    //sprintf(swarpcall,"%s -IMAGEOUT_NAME coadd.fits ",swarpcall);
    //sprintf(swarpcall,"%s -WEIGHTOUT_NAME coadd.weight.fits ",swarpcall);
    sprintf(swarpcall,"%s -WEIGHT_TYPE MAP_WEIGHT ",swarpcall);
    sprintf(swarpcall,"%s -WEIGHT_IMAGE @Weight.list ",swarpcall);
    sprintf(swarpcall,"%s -COMBINE_TYPE MEDIAN ",swarpcall);
    sprintf(swarpcall,"%s -COPY_KEYWORDS NONE ",swarpcall);
    //sprintf(swarpcall,"%s -PIXELSCALE_TYPE MANUAL -PIXEL_SCALE %2.3f ",swarpcall,PIXSCALE);
    //sprintf(swarpcall,"%s -CENTER_TYPE MANUAL -CENTER %3.7f,%3.7f ",swarpcall,CRVAL1+raoffset/cos((CRVAL2+decoffset)*M_PI/180.0),CRVAL2+decoffset);
    //sprintf(swarpcall,"%s -IMAGE_SIZE %d,%d ",swarpcall,NAXIS2,NAXIS1);
    sprintf(swarpcall,"%s -RESAMPLE Y ",swarpcall);
    sprintf(swarpcall,"%s -SUBTRACT_BACK Y",swarpcall);
    sprintf(swarpcall,"%s -WRITE_XML N ",swarpcall);
    //sprintf(swarpcall,"%s -PROJECTION_TYPE NONE ",swarpcall);

    /* run swarp */
    if(!flag_quiet)
      printf(" SWARPcall: %s\n",swarpcall);
    system(swarpcall);

    /* combine the coadded images to MEF using fitscombine */
    //sprintf(command,"/oldhome/bcs/desdm/devel/ImageProc/trunk/bin_WS/fitscombine coadd.fits coadd.weight.fits %s",templatename); /* hardwired the binpath at the moment */
    //if(!flag_quiet) {
    //sprintf(command,"%s -quiet",command);
    //printf(" FITSCOMBINEcall: %s\n",command);
    //}
    //system(command);
    
    /* insert the list of reduced images to the template */
    status=0;
    if(fits_open_file(&fptr,templatename,READWRITE,&status)) printerror(status);

    status=0;
    if (fits_update_key_str(fptr,"OBJECT",templateshort,"Template name",&status)) printerror(status);
    
    status=0;
      if (fits_write_comment(fptr,"Reduced images used for creating the template",&status)) printerror(status);

    fimageinfo=fopen("ImageInfo.list","r");
    while(fgets(line,1000,fimageinfo)!=NULL) {
      sscanf(line,"%d %s %s",&ID,run,filename);
      sprintf(longcomment,"%d %s %s",ID,run,filename);
      status=0;
      if (fits_write_comment(fptr,longcomment,&status)) printerror(status);
    }
    fclose(fimageinfo);
      
    status=0;
    if(fits_close_file(fptr,&status)) printerror(status);

  }
  else {

    /* --------------------------------------------- */
    /* NEED TO CP/LINK/TRNSFER THE CHOSEN TEMPLATE   */
    /* --------------------------------------------- */
    
    printf(" template is ...\n");
  }


  // STEP FOR TRANSFER REDUCED IMAGES
  // STEP FOR MAKING .head 
  // STEP FOR SWARP AGAIN TO PRODUCE DISTORTED TEMPLATE
  // STEP FOR GETTING ZP
  
  /* free memory */
  if(flag_nitelist)
    free(nitearray);

  /* clean up */
  //system("rm *.list *.sql image*.fits coadd*.fits");
  system("rm *.list *.sql image*.fits");
}

#undef VERSION
#undef BUFFER_DEG
#undef PIXSCALE
