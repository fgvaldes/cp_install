/*
 * PixTools.cpp
 *
 *  Created on: Apr 3, 2012
 *      Author: kuropat
 */

#include "PixTools.h"

using namespace std;

static struct {

    int x2pix[PixTools::xmax];

    int y2pix[PixTools::xmax];

    int pix2x[PixTools::pixmax];

    int pix2y[PixTools::pixmax];
 } memAl;


/**
 *
 *  contains methods translated from HEALPix Fortran90 32 bit version
 *
 *
 * @author N Kuropatkin
 *
 * Created on April 03, 2012
 *
 *
 */

/* constructor  */
PixTools::PixTools() {

	setUp();
}
double PixTools::dround(double input) {

	 double val = input;
	 double res;
	 res = (double)floor(val + 0.5);
	 return res;
}
/**
 * swaps low and high bits in the word i
 * @param i  integer input word
 * @return  int a word with odd and even bits interchanged
 */
 int PixTools::swapLSBMSB(int i) {
	int res = 0;
	int lsb = (i & PixTools::magic1);
	int msb = (i & PixTools::magic2);
	res = msb/2 + lsb*2;
	return res;
}
 /**
  * returns NOT i with even and odd bit positions interchanged
  * @param i  int input word
  * @return  int NOT (i with LSBMSB)
  */
 int PixTools::invswapLSBMSB(int i) {
 	int res = 0;
 	int lsb = (i & PixTools::magic1);
 	int msb = (i & PixTools::magic2);
 	res = ~(msb/2+lsb*2);
 	return res;
 }
 /**
  * returns i with even bits inverted
  * @param i int input word
  * @return int word with modified bits
  */
  int PixTools::invLSB(int i) {
 	int res = 0;
 	res = (i ^ PixTools::magic1); // returns exclusive OR with odd bits
 	return res;
 }
 /**
  * returns i with odd bits inverted
  * @param i int input word
  * @return int word with modified bits
  */
 int PixTools::invMSB(int i) {
 	int res = 0;
 	res = (i ^ PixTools::magic2);
 	return res;
 }
 /**
  * simulates behaviour of fortran90 MODULO function
  * @param a  double
  * @param b  double
  * @return  double MODULO
  */
 float PixTools::MODULO(float a, float b) {
 	float res = 0.;
 	int k = 0;
 	if (a>0.) {
 		if (b>0.) {
 			k = (int) (a/b);
 			res = a - k*b;
 			return res;
 		}
 		if (b<0.) {
 			k = (int)PixTools::dround(a/b);
 			res = a - k*b;
 			return res;
 		}
 	}
 	if (a<=0.) {
 		if (b<=0.) {
 			k = (int)(a/b);
 			res = a - k*b;
 			return res;
 		}
 		if (b>0.) {
 			k = (int)PixTools::dround(a/b);
 			res = a - k*b;
 			return res;
 		}
 	}
 	return res;
 }


/**
 * fills arrays x2pix and y2pix giving the number of the pixel laying in
 * (x,y). x and y are in [1,512] the pixel number is in [0, 512**2 -1]
 *
 * if i-1 = sum_p=0 b_p*2^p then ix = sum+p=0 b_p*4^p iy = 2*ix ix + iy in
 * [0,512**2 -1]
 *
 */
void PixTools::mk_xy2pix() {
	int k, ip, id;
	for (int i = 1; i <= xmax; i++) {
		int j = i - 1;
		k = 0;
		ip = 1;
		while (j != 0) {
			id = (int) PixTools::MODULO((float)j, 2.);
			j /= 2;
			k += ip * id;
			ip *= 4;
		}

		memAl.x2pix[i] = k;
		memAl.y2pix[i] = 2 * k;

	}

}
/**
 * creates an array of pixel numbers pix2x from x and y coordinates in the
 * face. Suppose NESTED scheme of pixel ordering Bits corresponding to x and
 * y are interleaved in the pixel number in even and odd bits.
 */
	void PixTools::mk_pix2xy() {
	int kpix, jpix, ix, iy, ip, id;
	for (kpix = 0; kpix <= PixTools::pixmax; kpix++) { // loop on pixel numbers
		jpix = kpix;
		ix = 0;
		iy = 0;
		ip = 1; // bit position in x and y

		while (jpix != 0) { // go through all the bits

			id = (int) PixTools::MODULO((float)jpix, 2.); // bit value (in kpix), goes in x
			jpix /= 2;
			ix += id * ip;

			id = (int) PixTools::MODULO((float)jpix, 2.); // bit value, goes in iy
			jpix /= 2;
			iy += id * ip;

			ip *= 2; // next bit (in x and y )
		}

		memAl.pix2x[(int) kpix] = ix; // in [0,pixmax]
		memAl.pix2y[(int) kpix] = iy; // in [0,pixmax]


	}
}


/**
 * calculates a vector production of two vectors.
 *
 * @param v1
 *            Vector containing 3 elements of Number type
 * @param v2
 *            Vector containing 3 elements of Number type
 * @return Vector of 3 Objects of Double type
 * @throws Exception
 */
Vector3D PixTools::VectProd(Vector3D v1, Vector3D v2)  {
	Vector3D res(0.,0.,0.);
//
	double v1_element[3];
	double v2_element[3];
	for (int i = 0; i < 3; i++) {
			v1_element[i] =  v1.getInd(i);
			v2_element[i] = v2.getInd(i);
	}

	double value = v1_element[1] * v2_element[2] - v1_element[2]* v2_element[1];
	res.x = value;
	value = v1_element[1] * v2_element[2] - v1_element[2] * v2_element[1];
	res.y = value;
	value = v1_element[1] * v2_element[2] - v1_element[2] * v2_element[1];
	res.z =  value;
	return res;
}

/**
 * calculates a dot product (inner product) of two 3D vectors
 *  the result is double
 *
 * @param v1
 *            3d Vector of Number Objects (Double, int .. )
 * @param v2
 *            3d Vector
 * @return  double
 * @throws Exception
 */
double PixTools::dotProduct(Vector3D v1, Vector3D v2)  {

	double prod = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;

	return prod;
}
/*check if vector contains given value
 *
 */
bool PixTools::contains(vector<int> a, int b) {
	bool res = false;
	int l = (int) a.size();
	for (int i=0; i<l; i++) {
		if (b == a[i]) {
			res = true;
			break;
		}
	}
	return res;
}
/**
 * calculate cross product of two vectors
 *
 * @param v1
 *            Vector3D
 * @param v2
 *            Vector3D
 * @return  Vector3D result of the product
 */
Vector3D PixTools::crossProduct(Vector3D v1, Vector3D v2) {
	Vector3D res(0., 0., 0.);
	double x = v1.y * v2.z - v1.z * v2.y;
	double y = v1.z * v2.x - v1.x * v2.z;
	double z = v1.x * v2.y - v1.y * v2.x;
	res.x = x;
	res.y = y;
	res.z = z;
	return res;
}
/**
 * calculates angular resolution of the pixel map
 * in arc seconds.
 * @param nside
 * @return double resolution in arcsec
 */
double PixTools::PixRes(int nside) {
    double res = 0.;
    double raddeg = PixTools::RadToDeg;
    double skyArea = 4.*M_PI*raddeg*raddeg; // 4PI steredian in deg^2
    double arcSecArea = skyArea*3600.*3600.;  // 4PI steredian in (arcSec^2)
    double npixels = 12.*nside*nside;
    res = arcSecArea/npixels;       // area per pixel
    res = sqrt(res);           // angular size of the pixel arcsec
    return res;
}

/**
 * calculates angular distance (in radians) between 2 Vectors
 * v1 and v2 In general dist = acos(v1.v2) except if the vectors are almost
 * aligned
 *
 * @param v1 Vector3D
 * @param v2 Vector3D
 * @return double dist in radians
 *
 */
double PixTools::AngDist(Vector3D v1, Vector3D v2) {
	double dist = 0.;
	double aligned = 0.999;
	/* Normalize both vectors */
	Vector3D r1 = v1;
	Vector3D r2 = v2;
	r1.normalize();
	r2.normalize();
	double sprod = dotProduct(r1,r2);
	/* This takes care about the bug in vecmath method from java3d project */
	if (sprod > aligned) { // almost aligned
		r1 = r1-r2;
		double diff = r1.magnitude();
		dist = 2.0 * asin(diff / 2.0);

	} else if (sprod < -aligned) {
		r1 = r1+r2;
		double diff = r1.magnitude();
		dist = M_PI - 2.0 * asin(diff / 2.0);
	} else {
		dist = r1.angleBetween(r1,r2);
	}
	return dist;
}


/**
 * calculate required nside given pixel size in arcsec
 * @param pixsize in arcsec
 * @return int nside parameter
 */
int PixTools::GetNSide(double pixsize) {
	int nsidelist[] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048,
			4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576 };
	int res = 0;
	double pixelArea = pixsize*pixsize;
	double degrad = PixTools::RadToDeg;
	int ns_max =  PixTools::ns_max;
	double skyArea = 4.*M_PI*degrad*degrad*3600.*3600.;
	double npixels =  (skyArea/pixelArea);
	double nsidesq = npixels/12;
	double nside_req =  sqrt(nsidesq);
//	cout<<" skyArea="<<skyArea<<" pixelArea="<<pixelArea<<" nside_req="<<nside_req<<endl;
	double mindiff = ns_max;
	int indmin = 0;
	for (int i=0; i<21; i++) {
		if (fabs(nside_req - nsidelist[i]) <= mindiff) {
			mindiff = fabs(nside_req - nsidelist[i]);
//			cout<<" mindiff="<<mindiff<<endl;
			res = nsidelist[i];
			indmin = i;
		}
	}
		if ((nside_req > res) && (nside_req < ns_max)) res = nsidelist[indmin+1];
	   	if (nside_req > ns_max ) {
    		cout<<"nside cannot be bigger than "<<ns_max<<endl;
    		return ns_max;
    	}


	return res;
}
/**
 * returns polar coordinates in radians given ra, dec in degrees
 * @param radec double array containing ra,dec in degrees
 * @return res double array containing theta and phi in radians
 *             res[0] = theta res[1] = phi
 */
double* PixTools::RaDecToPolar(double radec[]) {
	double *res = new double[2];
	double degtorad = PixTools::DegToRad;
		double ra =  radec[0];
		double dec =  radec[1];
		double theta = M_PI/2. - dec*degtorad;
		double phi = ra*degtorad;
		res[0] = theta;
		res[1] = phi;

	return res;
}
/**
 * returns ra, dec in degrees given polar coordinates in radians
 * @param polar double array polar[0] = phi in radians
 *                           polar[1] = theta in radians
 * @return double array radec radec[0] = ra in degrees
 *                radec[1] = dec in degrees
 */
double* PixTools::PolarToRaDec(double polar[]) {
	double *radec = new double[2];
	double radtodeg = PixTools::RadToDeg;
	double piby2 = M_PI/2.;
		double phi =  polar[1];
		double theta = polar[0];
		double dec = (piby2 - theta)*radtodeg;
		double ra = phi*radtodeg;
		radec[0] = ra;
		radec[1] = dec;

	return radec;
}

/**
 * converts a Vector3D in a tuple of angles tup[0] = theta
 * co-latitude measured from North pole, in [0,PI] radians, tup[1] = phi
 * intitude measured eastward, in [0,2PI] radians
 *
 * @param v
 *            Vector3D
 * @return double[] out_tup out_tup[0] = theta out_tup[1] = phi
 */
double* PixTools::Vect2Ang(Vector3D v) {
	double *out_tup = new double[2];
	double norm = v.magnitude();
	double z = v.z / norm;
	double theta = acos(z);
	double phi = 0.;
	if ((v.x != 0.) || (v.y != 0)) {
		phi = atan2(v.y, v.x); // phi in [-pi,pi]
	}
	if (phi < 0)
		phi += 2.0 * PixTools::PI; // phi in [0, 2pi]
//		phi += Math.PI;
	out_tup[0] = theta;
	out_tup[1] = phi;
	return out_tup;
}

/**
 * returns polar coordinates of a point on unit sphere given Cartesian coordinates
 * @param x - Cartesian coordinate x of a point on unit sphere
 * @param y - y coordinate
 * @param z - z coordinate
 * @return double [] theta,phi
 */
double* PixTools::xyzToPolar(double x, double y, double z) {
	double *res = new double[2];
	Vector3D vv(x,y,z);

	res = Vect2Ang(vv);
	return res;
}
/*
 * combine two vectors
 */
vector<int>* PixTools::addAll(vector<int> *a, vector<int> *b) {
for (size_t i=0; i< b->size(); i++) {
	a->push_back(b->operator [](i));
}
return a;
}

/**
 * calculates the surface of spherical triangle defined by
 * vertices v1,v2,v3 Algorithm: finds triangle sides and uses l'Huilier
 * formula to compute "spherical excess" = surface area of triangle on a
 * sphere of radius one, see, eg Bronshtein, Semendyayev Eq 2.86 half
 * perimeter hp = 0.5*(side1+side2+side3) l'Huilier formula x0 = tan( hp/2.)
 * x1 = tan((hp - side1)/2.) x2 = tan((hp - side2)/2.) x3 = tan((hp -
 * side3)/2.)
 *
 * @param v1 Vector3D
 * @param v2 Vector3D
 * @param v3 Vector3D vertices of the triangle
 * @return  double the triangle surface
 *
 *
 */
double PixTools::SurfaceTriangle(Vector3D v1, Vector3D v2, Vector3D v3){
	double res = 0.;
	double side1 = AngDist(v2, v3) / 4.0;
	double side2 = AngDist(v3, v1) / 4.0;
	double side3 = AngDist(v1, v2) / 4.0;
	double x0 = tan(side1 + side2 + side3);
	double x1 = tan(side2 + side3 - side1);
	double x2 = tan(side1 + side3 - side2);
	double x3 = tan(side1 + side2 - side3);
	res = 4.0 * atan(sqrt(x0 * x1 * x2 * x3));

	return res;
}

/**
 * calculates npix such that npix = 12*nside^2 ,nside should be
 * a power of 2, and smaller than ns_max otherwise return -1
 *
 * @param nside
 *            int the map resolution
 * @return npix int the number of pixels in the map
 */
int PixTools::Nside2Npix(int nside) {

	int nsidelist[] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048,
			4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576,
            2097152, 4194304};

	int res = 0;
	string SID = "Nside2Npix:";
	bool found = false;
	for (int i = 0; i< 21; i++ ) {
		if (nside == nsidelist[i]) {
			found = true;
			break;
		}
	}

	if (! found) {
		cout<<SID<<" nside should be power of 2 and <= "<<PixTools::ns_max<<endl;
		res = -1;
	}
	res = 12 * nside * nside;

	return res;
}


/**
 * returns the ring number in {1, 4*nside - 1} calculated from z coordinate
 *
 * @param nside
 *            int resolution
 * @param z
 *            double z coordinate
 * @return int ring number
 */

int PixTools::RingNum(int nside, double z) {
	int iring = 0;
	double comp = PixTools::twothird;
	// equatorial region

	iring = (int) dround(nside * (2.0 - 1.5 * z));
	// north cap
	if (z > comp) {
		iring = (int) dround(nside * sqrt(3.0 * (1.0 - z)));
		if (iring == 0)
			iring = 1;
	}

	// south cap
	if (z < -comp) {
		iring = (int) dround(nside * sqrt(3.0 * (1.0 + z)));
		if (iring == 0)
			iring = 1;
		iring = 4 * nside - iring;
	}
	return iring;
}

/**
 * calculates vector corresponding to angles theta (co-latitude
 * measured from North pole, in [0,pi] radians) phi (longitude measured
 * eastward in [0,2pi] radians) North pole is (x,y,z) = (0, 0, 1)
 *
 * @param theta double
 * @param phi double
 * @return Vector3D
 *
 */
Vector3D PixTools::Ang2Vec(double theta, double phi)  {
	double PI = M_PI;
	string SID = "Ang2Vec:";
	Vector3D v(0.,0.,0.);
	if ((theta < 0.0) || (theta > PI)) {
		cout<<SID << " theta out of range [0.,PI]"<<endl;
		exit(-1);
	}
	double stheta = sin(theta);
	double x = stheta * cos(phi);
	double y = stheta * sin(phi);
	double z = cos(theta);
	v.x = x;
	v.y = y;
	v.z = z;
	return v;
}


/**
 * returns nside such that npix = 12*nside^2,  nside should be
 * power of 2 and smaller than ns_max if not return -1
 *
 * @param npix
 *            int the number of pixels in the map
 * @return int nside the map resolution parameter
 */
int PixTools::Npix2Nside(int npix) {
	int nside = 0;
	int ns_max = PixTools::ns_max;
	double npixmax = 12. * ns_max*ns_max;

	string SID = "Npix2Nside:";
	nside = (int) dround(sqrt((float)npix / 12.));
	if (npix < 12) {
		cout<<SID<<" npix is too small should be > 12"<<endl;
		exit(-1);
	}
	if (npix > npixmax) {
		cout<<SID << " npix is too large > 12 * ns_max^2"<<endl;
		exit(-1);
	}
	double fnpix = 12.0 * nside * nside;
	if (fabs(fnpix - npix) > 1.0e-2) {
		cout<<SID << "  npix is not 12*nside*nside"<<endl;
	}
	double flog = log((double) nside) / log(2.0);
	double ilog = dround(flog);
	if (fabs(flog - ilog) > 1.0e-6) {
		cout<<SID << "  nside is not power of 2"<<endl;
	}
	return nside;
}

/**
 * computes the intersection di of 2 intervals d1 (= [a1,b1])
 * and d2 (= [a2,b2]) on the periodic domain (=[A,B] where A and B
 * arbitrary) ni is the resulting number of intervals (0,1, or 2) if a1 <b1
 * then d1 = {x |a1 <= x <= b1} if a1>b1 then d1 = {x | a1 <=x <= B U A <=x
 * <=b1}
 *
 * @param d1 vector<double> * first interval
 * @param d2 vector<double> * second interval
 * @return vector<double>* one or two intervals intersections
 */
vector<double> *PixTools::intrs_intrv(vector<double>* d1, vector<double>* d2) {
	vector<double> *res; //

	double epsilon = 1.0e-10;


	int ik = 0;
	bool tr12, tr21, tr34, tr43, tr13, tr31, tr24, tr42, tr14, tr32;
	/*                                             */

	tr12 = (d1->operator [](0) < d1->operator [](1) + epsilon);
	tr21 = !tr12; // d1[0] >= d1[1]
	tr34 = (d2->operator [](0) < d2->operator [](1) + epsilon);
	tr43 = !tr34; // d2[0]>d2[1]
	tr13 = (d1->operator [](0) < d2->operator [](0) + epsilon); //  d2[0] can be in interval
	tr31 = !tr13; // d1[0] in interval
	tr24 = (d1->operator [](1) < d2->operator[](1) + epsilon); // d1[1] upper limit
	tr42 = !tr24; // d2[1] upper limit
	tr14 = (d1->operator [](0) < d2->operator [](1) + epsilon); // d1[0] in interval
	tr32 = (d2->operator [](0) < d1->operator [](1) + epsilon); // d2[0] in interval

	ik = 0;
	double dk[] = { -1.0e9, -1.0e9, -1.0e9, -1.0e9 };
	/* d1[0] lower limit case 1 */
	if ((tr34 && tr31 && tr14) || (tr43 && (tr31 || tr14))) {
		ik++; // ik = 1;
		dk[ik - 1] = d1->operator [](0); // a1

	}
	/* d2[0] lower limit case 1 */
	if ((tr12 && tr13 && tr32) || (tr21 && (tr13 || tr32))) {
		ik++; // ik = 1
		dk[ik - 1] = d2->operator [](0); // a2

	}
	/* d1[1] upper limit case 2 */
	if ((tr34 && tr32 && tr24) || (tr43 && (tr32 || tr24))) {
		ik++; // ik = 2
		dk[ik - 1] = d1->operator [](1); // b1

	}
	/* d2[1] upper limit case 2 */
	if ((tr12 && tr14 && tr42) || (tr21 && (tr14 || tr42))) {
		ik++; // ik = 2
		dk[ik - 1] = d2->operator [](1); // b2

	}
	res = new vector<double>();
	switch (ik) {

	case 2:
		res->push_back(dk[0] - epsilon);
		res->push_back( dk[1] + epsilon);
		break;
	case 4:
		res->push_back(dk[0] - epsilon);
		res->push_back(dk[3] + epsilon);
		res->push_back( dk[1] - epsilon);
		res->push_back( dk[2] + epsilon);
		break;
	}

	return res;
}
/**
 * converts from NESTED to RING pixel numbering
 *
 * @param nside
 *            int resolution
 * @param ipnest
 *            int NEST pixel number
 * @return ipring  int RING pixel number
 * @throws IllegalArgumentException
 */

int PixTools::nest2ring(int nside, int ipnest)  {
	int res = 0;
	int npix, npface, face_num, ncap, n_before, ipf, ip_low, ip_trunc;
	int ip_med, ip_hi, ix, iy, jrt, jr, nr, jpt, jp, kshift, nl4;
	float pixmax =  PixTools::pixmax;
//
	// coordinates of lowest corner of each face
	int jrll[] = { 0, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 }; // in units of
	// nside
	int jpll[] = { 0, 1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7 }; // in units of
	// nside/2
	string SID = "nest2ring:";
	//
	if ((nside < 1) || (nside > PixTools::ns_max)) {
		cout<<SID << " nside should be power of 2 >0 and < ns_max"<<endl;
		exit(-1);
	}
	npix = 12 * nside * nside;
	if ((ipnest < 0) || (ipnest > npix - 1)) {
		cout<<SID<<" ipnest out of range [0,npix-1]"<<endl;
		exit(-1);
	}
	if (memAl.pix2x[PixTools::pixmax-1] <= 0) mk_pix2xy();
	ncap = 2 * nside * (nside - 1); // number of points in the North polar
	// cap
	nl4 = 4 * nside;
	// finds the face and the number in the face
	npface = nside * nside;

	face_num = ipnest / npface; // face number in [0,11]
	if (ipnest >= npface) {
		ipf = (int) PixTools::MODULO((float)ipnest, (float)npface); // pixel number in the face
	} else {
		ipf = ipnest;
	}

	// finds the x,y on the face
	//  from the pixel number
	ip_low = (int) PixTools::MODULO((float)ipf, pixmax); // last 15 bits
	if (ip_low < 0)
		ip_low = -ip_low;

	ip_trunc = (int)(ipf / pixmax); // truncate last 15 bits
	ip_med = (int) PixTools::MODULO((float)ip_trunc, pixmax); // next 15 bits
	if (ip_med < 0)
		ip_med = -ip_med;
	ip_hi = (int)(ip_trunc / pixmax); // high 15 bits

	ix = (int)(pixmax * memAl.pix2x[(int) ip_hi] + PixTools::xmid * memAl.pix2x[(int) ip_med] +
			memAl.pix2x[(int) ip_low]);
	iy = (int)(pixmax * memAl.pix2y[(int) ip_hi] + PixTools::xmid * memAl.pix2y[(int) ip_med] +
			memAl.pix2y[(int) ip_low]);

	// transform this in (horizontal, vertical) coordinates
	jrt = ix + iy; // vertical in [0,2*(nside -1)]
	jpt = ix - iy; // horizontal in [-nside+1, nside - 1]
	// calculate the z coordinate on the sphere
	jr = jrll[(int) (face_num + 1)] * nside - jrt - 1; // ring number in [1,4*nside
	// -1]
	nr = nside; // equatorial region (the most frequent)
	n_before = ncap + nl4 * (jr - nside);
	kshift = (int) PixTools::MODULO((float)(jr - nside), 2.);
	if (jr < nside) { // north pole region
		nr = jr;
		n_before = 2 * nr * (nr - 1);
		kshift = 0;
	} else if (jr > 3 * nside) { // south pole region
		nr = nl4 - jr;
		n_before = npix - 2 * (nr + 1) * nr;
		kshift = 0;
	}
	// computes the phi coordinate on the sphere in [0,2pi]
	jp = (jpll[(int) (face_num + 1)] * nr + jpt + 1 + kshift) / 2; // 'phi' number
	// in ring
	// [1,4*nr]
	if (jp > nl4)
		jp -= nl4;
	if (jp < 1)
		jp += nl4;
	res =(int)(n_before + jp - 1); // in [0, npix-1]
	return res;
}

/**
 * converts pixel number from ring numbering schema to the nested one
 *
 * @param nside
 *            int resolution
 * @param ipring int pixel number in ring schema
 * @return int pixel number in nest schema
 * @throws IllegalArgumentException
 */

int PixTools::ring2nest(int nside, int ipring)  {
	int ipnest = 0;
	double fihip;
	double hip;
	float xmax = PixTools::xmax;
	int npix, nl2, nl4, ncap, ip, iphi, ipt, ipring1, kshift, face_num;
	int nr, irn, ire, irm, irs, irt, ifm, ifp, ix, iy, ix_low, ix_hi, iy_low;
	int iy_hi, ipf;
	int jrll[] = { 0, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 }; // in units of
	// nside
	int jpll[] = { 0, 1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7 }; // in units of
	// nside/2
	string SID = "ring2nest:";
	//
	face_num = 0;
	if ((nside < 1) || (nside > PixTools::ns_max)) {
		cout<<SID<<" nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
		exit(-1);
	}
	npix = 12 * nside * nside; // total number of points
//	cout<<" nside="<<nside<<" ipring="<<ipring<<" nside="<<nside<<endl;
	if ((ipring < 0) || (ipring > npix - 1)) {
		cout<<SID<<" ipring out of range [0,npix-1]"<<endl;
	}
	if (memAl.x2pix[PixTools::xmax-1] <= 0) mk_xy2pix();

	nl2 = 2 * nside;
	nl4 = 4 * nside;
	ncap = nl2 * (nside - 1); // points in each polar cap, =0 for nside = 1
	ipring1 = ipring + 1;
	// finds the ring number, the position of the ring and the face number
	if (ipring1 <= ncap) { // north polar cap
		hip = ipring1 / 2.0;
		fihip = floor(hip);
		irn = (int)floor( sqrt(hip - sqrt(fihip))) + 1; // counted from
		// north pole
		iphi = ipring1 - 2 * irn * (irn - 1);

		kshift = 0;
		nr = irn; // 1/4 of the number of points on the current ring
		face_num = (iphi - 1) / irn; // in [0,3 ]

	} else if (ipring1 <= nl2 * (5 * nside + 1)) { // equatorial region
		ip = ipring1 - ncap - 1;
		irn = (int)floor(ip / nl4) + nside; // counted from north pole
		iphi = (int) PixTools::MODULO((float)ip, (float)nl4) + 1;

		kshift = (int) PixTools::MODULO((float)(irn + nside), 2.); // 1 if odd 0
		// otherwise
		nr = nside;
		ire = irn - nside + 1; // in [1, 2*nside+1]
		irm = nl2 + 2 - ire;
		ifm = (iphi - ire / 2 + nside - 1) / nside; // face boundary
		ifp = (iphi - irm / 2 + nside - 1) / nside;
		if (ifp == ifm) {
			face_num = (int) PixTools::MODULO((float)ifp, 4.) + 4;
		} else if (ifp + 1 == ifm) { // (half-) faces 0 to 3
			face_num = ifp;
		} else if (ifp - 1 == ifm) { // (half-) faces 8 to 11
			face_num = ifp + 7;
		}


	} else { // south polar cap

		ip = npix - ipring1 + 1;
		hip = ip / 2.0;
		fihip = floor(hip);
		irs = (int)floor( sqrt(hip - sqrt(fihip))) + 1;
		iphi = 4 * irs + 1 - (ip - 2 * irs * (irs - 1));
		kshift = 0;
		nr = irs;
		irn = nl4 - irs;
		face_num = (int)((iphi - 1) / irs) + 8; // in [8,11]


	}
	// finds the (x,y) on the face


//
	irt = irn - jrll[(int) (face_num + 1)] * nside + 1; // in [-nside+1,0]
	ipt = 2 * iphi - jpll[(int) (face_num + 1)] * nr - kshift - 1; // in [-nside+1,
	// nside-1]
//
	if (ipt >= nl2){
		ipt = ipt - 8*nside; // for the face #4
	}
	ix = (ipt - irt) / 2;
	iy = -(ipt + irt) / 2;

	ix_low = (int) PixTools::MODULO((float)ix, xmax);
	ix_hi = (int)(ix / xmax);
	iy_low = (int) PixTools::MODULO((float)iy, xmax);
	iy_hi = (int)(iy / xmax);

      //

	ipf =(int)( (memAl.x2pix[(int) (ix_hi + 1)] + memAl.y2pix[(int) (iy_hi + 1)]) *xmax*xmax
			+ (memAl.x2pix[(int) (ix_low + 1)] + memAl.y2pix[(int) (iy_low + 1)])); // in [0, nside**2 -1]
	ipnest = ipf + face_num * nside * nside; // in [0, 12*nside**2 -1]

	return ipnest;

}
/**
 * gives the x,y coordinates in a face from pixel number within the face
 * (NESTED) schema.
 *
 * @param nside
 *            int resolution parameter
 * @param ipf
 *            int pixel number
 * @return ixiy  int[] contains x and y coordinates
 *
 */

int* PixTools::pix2xy_nest(int nside, int ipf)  {
	int *ixiy = new int[2];
	int ip_low, ip_trunc, ip_med, ip_hi;
	string SID = "pix2xy_nest:";
//        System.out.println(" ipf="+ipf+" nside="+nside);
	if ((nside < 1) || (nside > PixTools::ns_max)) {
		cout<<SID<<" nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
		exit(-1);
	}
	if ((ipf < 0) || (ipf > nside * nside - 1)) {
		cout<<SID<<" ipix out of range defined by nside"<<endl;
	}
	if (memAl.pix2x[PixTools::pixmax-1] <= 0)
		mk_pix2xy();
	ip_low = (int) PixTools::MODULO(ipf, PixTools::pixmax); // contents of last 15 bits
	ip_trunc = ipf / PixTools::pixmax; // truncation of the last 15 bits
	ip_med = (int) PixTools::MODULO(ip_trunc, PixTools::pixmax); // next 15 bits
	ip_hi = ip_trunc /PixTools::pixmax; // select high 15 bits

	int ix = PixTools::pixmax * memAl.pix2x[(int) ip_hi] +
			PixTools::xmid * memAl.pix2x[(int) ip_med] + memAl.pix2x[(int) ip_low];
	int iy = PixTools::pixmax * memAl.pix2y[(int) ip_hi] +
			PixTools::xmid * memAl.pix2y[(int) ip_med] + memAl.pix2y[(int) ip_low];
	ixiy[0] = ix;
	ixiy[1] = iy;
	return ixiy;
}

/**
 * gives the pixel number ipix (NESTED) corresponding to ix, iy and face_num
 *
 * @param nside
 *            the map resolution parameter
 * @param ix
 *            Integer x coordinate
 * @param iy
 *            Integer y coordinate
 * @param face_num
 *            int face number
 * @return  int pixel number ipix
 * @throws IllegalArgumentException
 */

int PixTools::xy2pix_nest(int nside, int ix, int iy, int face_num){
	int res = 0;
	int ix_low, ix_hi, iy_low, iy_hi, ipf;
	int xmax = PixTools::xmax;

	string SID = "xy2pix_nest:";
	//
	if ((nside < 1) || (nside > PixTools::ns_max)) {
		cout<<SID<<" nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
	}

	if (memAl.x2pix[PixTools::xmax-1] <= 0)
		mk_xy2pix();
	ix_low = (int) PixTools::MODULO(ix, xmax);
	ix_hi = ix / xmax;
	iy_low = (int) PixTools::MODULO(iy, xmax);
	iy_hi = iy / xmax;

	ipf = (memAl.x2pix[(int) (ix_hi + 1)] + memAl.y2pix[(int) (iy_hi + 1)]) * xmax*xmax
			+ (memAl.x2pix[(int) (ix_low + 1)] + memAl.y2pix[(int) (iy_low + 1)]);
	res = ipf + face_num * nside * nside; // in [0, 12*nside^2 - 1]

	return res;
}

/**
 * renders the pixel number pix (NESTED scheme) for a pixel which contains a
 * point on a sphere at coordinate vector (x,y,z), given the map resolution
 * parameter nside.
 *
 * The computation is made to the highest resolution available (nside=ns_max)
 * and then degraded to that requared (by Integer division) this doesn't
 * cost much, and it makes sure that the treatment of round-off will be
 * consistent for every resolution
 *
 * @param nside
 *            int the map resolution
 * @param vector
 *            Vewctor3d the input vector
 * @return pixel int
 * @throws IllegalArgumentException
 */

int PixTools::vect2pix_nest(int nside, Vector3D vector)  {
	int pix = 0;
	double z, za, tt, tp, tmp, dnorm, phi;
	float jp, jm, ifp, ifm,  ix, iy, ix_low, ix_hi;
	float iy_low, iy_hi, ipf, ntt;
	int face_num;
	float fns_max = PixTools::ns_max;
	float fxmax = PixTools::xmax;
	double twothird = PixTools::twothird;
	double twopi = PixTools::TWOPI;

	string SID = " vect2pix_nest:";
	//                      //
	if (nside < 1 || nside > PixTools::ns_max) {
		cout<<SID<<" nside should be power of 2 >0 and < ns_max"<<endl;
		exit(-1);
	}
	if (memAl.x2pix[PixTools::xmax-1] <= 0)
		mk_xy2pix();

	dnorm = vector.magnitude();
	z = vector.z / dnorm;
	phi = 0.; // phi in [-pi,pi]
	if (vector.x != 0. || vector.y != 0.)
		phi = atan2(vector.y, vector.x);
	za = fabs(z);

	if (phi < 0.)
		phi += twopi; // phi in [0,2pi]

	tt = 2. * phi / M_PI; // in [0,4]
	if (za <= twothird) { // Equatorial region
		//
		// the index of edge lines increases when the intitude = phi goes
		// up
		//
		jp = (int) (fns_max * (0.5 + tt - z * 0.75)); // ascending edge line
		// index
		jm = (int) (fns_max * (0.5 + tt + z * 0.75)); // descending edge line
		// index
		// find the face //
		ifp = (int)(jp / fns_max); // in [0,4]
		ifm = (int)(jm / fns_max);
		if (ifp == ifm) { // faces 4 to 7
			face_num = (int) PixTools::MODULO((float)ifp, 4.) + 4;
		} else if (ifp < ifm) { // (half-) faces 0 to 3
			face_num = (int) PixTools::MODULO((float)ifp, 4.);
		} else { // (half-) faces 8 to 11
			face_num = (int) PixTools::MODULO((float)ifm, 4.) + 8;
		}

		ix = (int) PixTools::MODULO((float)jm, fns_max);
		iy = fns_max - (int) PixTools::MODULO((float)jp, fns_max) - 1;
	} else { // polar region, za > 2/3
		ntt = (int) tt;
		if (ntt >= 4)
			ntt = 3;
		tp = tt - ntt;
		tmp = sqrt(3.0 * (1.0 - za)); // in [0,1]
		//
		// the index of edge lines increases when distance from the closest
		// pole goes up
		//
		jp = (int) (fns_max * tp * tmp); // line going toward the pole has
		// phi increases
		jm = (int) (fns_max * (1.0 - tp) * tmp); // that one goes away of the
		// closest pole
		jp = fmin(fns_max - 1, jp); // for points too close to the
		// boundary
		jm = fmin(fns_max - 1, jm);
		// finds the face and pixel's (x,y) //
		if (z >= 0) {
			face_num =(int) ntt; // in [0,3]
			ix = fns_max - jm - 1;
			iy = fns_max - jp - 1;
		} else {
			face_num = (int) (ntt + 8); // in [8,11]
			ix = jp;
			iy = jm;
		}
	}

	ix_low = (int) PixTools::MODULO((float)ix, fxmax);
	ix_hi = ix / fxmax;
	iy_low = (int) PixTools::MODULO((float)iy, fxmax);
	iy_hi = iy / fxmax;

	ipf = (memAl.x2pix[(int) (ix_hi + 1)] + memAl.y2pix[(int) (iy_hi + 1)]) * (fxmax * fxmax)
			+ (memAl.x2pix[(int) (ix_low + 1)] + memAl.y2pix[(int) (iy_low + 1)]);

	ipf = ipf / ((fns_max / nside) * (fns_max / nside)); // in [0, nside**2
	// -1]

	pix = (int)(ipf + face_num * nside * nside); // in [0, 12*nside**2 -1]
	return pix;
}




/**
 * calculates the pixel that lies on the East side (and the same
 * latitude) as the given NESTED pixel number - ipix
 *
 * @param nside
 *            int resolution
 * @param ipix
 *            int pixel number
 * @return  int next pixel in line
 * @throws IllegalArgumentException
 */

int PixTools::next_in_line_nest(int nside, int ipix)  {
	int npix, ipf, ipo, ix, ixp, iy, iym, ixo, iyo, face_num, other_face;
	int ia, ib, ibp, nsidesq;
	int ibm, ib2;
    int icase;
	int local_magic1, local_magic2;
	int *ixiy = new int[2];
	int inext = 0; // next in line pixel in Nest scheme
	string SID = "next_in_line:";
	if ((nside < 1) || (nside > PixTools::ns_max)) {
		cout<<SID<<" nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
		exit(-1);
	}
	nsidesq = nside * nside;
	npix = 12 * nsidesq; // total number of pixels
	if ((ipix < 0) || (ipix > npix - 1)) {
		cout<<SID<<" ipix out of range defined by nside"<<endl;
		exit(-1);
	}
	// initiates array for (x,y) -> pixel number -> (x,y) mapping
	if (memAl.x2pix[PixTools::xmax-1] <= 0)
		mk_xy2pix();
	local_magic1 = (nsidesq - 1) / 3;
	local_magic2 = 2 * local_magic1;
	face_num = ipix / nsidesq;
	ipf = (int) PixTools::MODULO(ipix, nsidesq); // Pixel number in face
	ixiy = pix2xy_nest(nside, ipf);
	ix = ixiy[0];
	iy = ixiy[1];
	ixp = ix + 1;
	iym = iy - 1;
	bool sel = false;
	icase = -1; // iside the nest flag
	// Exclude corners
	if (ipf == local_magic2) { // west coirner
		inext = ipix - 1;
		return inext;
	}
	if ((ipf == nsidesq - 1) && !sel) { // North corner
		icase = 6;
		sel = true;
	}
	if ((ipf == 0) && !sel) { // Siuth corner
		icase = 7;
		sel = true;
	}
	if ((ipf == local_magic1) && !sel) { // East corner
		icase = 8;
		sel = true;
	}
	// Detect edges
	if (((ipf & local_magic1) == local_magic1) && !sel) { // North-East
		icase = 1;
		sel = true;
	}
	if (((ipf & local_magic2) == 0) && !sel) { // South-East
		icase = 4;
		sel = true;
	}
	if (!sel) { // iside a face
		inext = xy2pix_nest(nside, ixp, iym, face_num);
		return inext;
	}
	//
	ia = face_num / 4; // in [0,2]
	ib = (int) PixTools::MODULO(face_num, 4.); // in [0,3]
	ibp = (int) PixTools::MODULO(ib + 1, 4.);
	ibm = (int) PixTools::MODULO(ib + 4 - 1, 4.);
	ib2 = (int) PixTools::MODULO(ib + 2, 4.);

	if (ia == 0) { // North pole region
		switch (icase) {
		case 1:
			other_face = 0 + ibp;
			ipo = (int) PixTools::MODULO(PixTools::swapLSBMSB( ipf), nsidesq);
			inext = other_face * nsidesq + ipo;
			break;
		case 4:
			other_face = 4 + ibp;
			ipo = (int) PixTools::MODULO(PixTools::invMSB( ipf), nsidesq); // SE-NW flip

			ixiy = pix2xy_nest(nside, ipo);
			ixo = ixiy[0];
			iyo = ixiy[1];

			inext = xy2pix_nest(nside, ixo + 1, iyo, other_face);

			break;
		case 6: // North corner
			other_face = 0 + ibp;
			inext = other_face * nsidesq + nsidesq - 1;
			break;
		case 7:
			other_face = 4 + ibp;
			inext = other_face * nsidesq + local_magic2 + 1;
			break;
		case 8:
			other_face = 0 + ibp;
			inext = other_face * nsidesq + local_magic2;
			break;
		}

	} else if (ia == 1) { // Equatorial region
		switch (icase) {
		case 1: // NorthEast edge
			other_face = 0 + ib;
//                System.out.println("ipf="+ipf+" nsidesq="+nsidesq+" invLSB="+BitManipulation.invLSB(ipf));
			ipo = (int) PixTools::MODULO((double)PixTools::invLSB( ipf), (double)nsidesq); // NE-SW flip
//                System.out.println(" ipo="+ipo);

			ixiy = pix2xy_nest(nside, ipo);
			ixo = ixiy[0];
			iyo = ixiy[1];
			inext = xy2pix_nest(nside, ixo, iyo - 1, other_face);
			break;
		case 4: // SouthEast edge
			other_face = 8 + ib;
			ipo = (int) PixTools::MODULO(PixTools::invMSB( ipf), nsidesq);
			ixiy = pix2xy_nest(nside, ipo);
			inext = xy2pix_nest(nside, ixiy[0] + 1, ixiy[1], other_face);
			break;
		case 6: // Northy corner
			other_face = 0 + ib;
			inext = other_face * nsidesq + local_magic2 - 2;
			break;
		case 7: // South corner
			other_face = 8 + ib;
			inext = other_face * nsidesq + local_magic2 + 1;
			break;
		case 8: // East corner
			other_face = 4 + ibp;
			inext = other_face * nsidesq + local_magic2;
			break;

		}
	} else { // South pole region
		switch (icase) {
		case 1: // NorthEast edge
			other_face = 4 + ibp;
			ipo = (int) PixTools::MODULO(PixTools::invLSB( ipf), nsidesq); // NE-SW flip
			ixiy = pix2xy_nest(nside, ipo);
			inext = xy2pix_nest(nside, ixiy[0], ixiy[1] - 1, other_face);
			break;
		case 4: // SouthEast edge
			other_face = 8 + ibp;
			ipo = (int) PixTools::MODULO(PixTools::swapLSBMSB( ipf), nsidesq); // E-W flip
			inext = other_face * nsidesq + ipo; // (8)
			break;
		case 6: // North corner
			other_face = 4 + ibp;
			inext = other_face * nsidesq + local_magic2 - 2;
			break;
		case 7: // South corner
			other_face = 8 + ibp;
			inext = other_face * nsidesq;
			break;
		case 8: // East corner
			other_face = 8 + ibp;
			inext = other_face * nsidesq + local_magic2;
			break;
		}
	}
	return inext;
}



/**
 * returns the list of pixels in RING or NEST scheme with latitude in [phi0 -
 * dpi, phi0 + dphi] on the ring iz in [1, 4*nside -1 ] The pixel id numbers
 * are in [0, 12*nside^2 - 1] the indexing is in RING, unless nest is set to
 * 1
 *
 * @param nside
 *            int the map resolution
 * @param iz
 *           int ring number
 * @param phi0
 *            double
 * @param dphi
 *            double
 * @param nest
 *            boolean format flag
 * @return ArrayList of pixels
 * @throws IllegalArgumentException
 *
 * Modified by N. Kuropatkin 07/09/2008  Corrected several bugs and make test of all cases.
 *
 */

vector<int>* PixTools::InRing(int nside, int iz, double phi0, double dphi,
		bool nest)  {
	bool take_all = false;
	bool to_top = false;
	bool do_ring = true;
	bool conservative = false;
	double twopi = PixTools::TWOPI;
//		String SID = "InRing:";
//	double epsilon = 1E-32; // the constant to eliminate
	// java calculation jitter
	if (nest)
		do_ring = false;
	double shift = 0.;
	int ir = 0;
	int kshift, nr, ipix1, ipix2, nir1, nir2, ncap, npix;
	int ip_low = 0, ip_hi = 0, in,  nir;
	vector<int> *res = new vector<int>();
	vector<int>::iterator i;
	npix = 12 * nside * nside; // total number of pixels
	ncap = 2 * nside * (nside - 1); // number of pixels in the north polar
									// cap
	double phi_low = PixTools::MODULO(phi0 - dphi, twopi); // phi min,
															  // excluding
															  // 2pi period
	double phi_hi = PixTools::MODULO(phi0 + dphi, twopi);

//
	if (dphi == M_PI )  take_all = true;

	// identifies ring number //
	if ((iz >= nside) && (iz <= 3 * nside)) { // equatorial region
		ir = iz - nside + 1; // in [1, 2*nside + 1]
		ipix1 = ncap + 4 * nside * (ir - 1); // lowest pixel number in the
										     // ring
		ipix2 = ipix1 + 4 * nside - 1; // highest pixel number in the ring
		kshift = (int) PixTools::MODULO((float)ir, 2.);

		nr = nside * 4;
	} else {
		if (iz < nside) { // north pole
			ir = iz;
			ipix1 = 2 * ir * (ir - 1); // lowest pixel number
			ipix2 = ipix1 + 4 * ir - 1; // highest pixel number
		} else { // south pole
			ir = 4 * nside - iz;

			ipix1 = npix - 2 * ir * (ir + 1); // lowest pixel number
			ipix2 = ipix1 + 4 * ir - 1;       // highest pixel number
		}
		nr = ir * 4;
		kshift = 1;
	}

	// Construct the pixel list
	if (take_all) {
		nir = ipix2 - ipix1 + 1;
		if (do_ring) {
			int ind = 0;
			for ( int j =  ipix1; j <= ipix2; j++) {
				res->push_back(j);
				ind++;
			}
		} else {


			for ( int i = 0; i < nir; i++) {
				in = ring2nest(nside, (ipix1 + i));
				res->push_back(in);

			}
		}

		return res;
	}

	shift = kshift / 2.0;

	// conservative : include every intersected pixel, even if the
	// pixel center is out of the [phi_low, phi_hi] region
	if (conservative) {
		ip_low = (int) dround((nr * phi_low) / twopi - shift);
		ip_hi = (int) dround((nr * phi_hi) / twopi - shift);

		ip_low = (int) PixTools::MODULO((float)ip_low,(float) nr); // in [0, nr - 1]
		ip_hi = (int) PixTools::MODULO((float)ip_hi, (float)nr); // in [0, nr - 1]
//			System.out.println("ip_low="+ip_low+" ip_hi="+ip_hi);
	} else { // strict: includes only pixels whose center is in
		//                                                    [phi_low,phi_hi]

		ip_low = (int) ceil((nr * phi_low) / twopi - shift);
		ip_hi = (int) floor((nr * phi_hi) / twopi - shift);
		if (ip_low == ip_hi + 1)
			ip_low = ip_hi;

		if ((ip_low - ip_hi == 1) && (dphi * nr < M_PI)) {
			// the interval is too small ( and away from pixel center)
			// so no pixels is included in the list
			cout<<"the interval is too small and away from center"<<endl;
			return res; // return empty list
		}

		ip_low =(int) fmin(ip_low, nr - 1.);
		ip_hi = (int)fmax(ip_hi, 0.);
	}

	//
	if (ip_low > ip_hi)
		to_top = true;

	if (to_top) {
		ip_low += ipix1;
		ip_hi += ipix1;
		nir1 = ipix2 - ip_low + 1;

		nir2 = ip_hi + 1;
		nir = nir1 + nir2;
		if (do_ring) {
			int ind = 0;
			for (int j =  ip_low; j <= ipix2; j++) {
				res->push_back(j);
				ind++;
			}
			//				ind = nir1;
			for (int j =  ipix1; j <= ip_hi; j++) {
				res->push_back(j);
				ind++;
			}
		} else {
			int ind = 0;
			for (int j = ip_low; j <= ipix2; j++) {
				in = ring2nest(nside, j);
				res->push_back(in);
				ind++;
			}
			for (int j =  ipix1; j <= ip_hi; j++) {
				in = ring2nest(nside, j);
				res->push_back(in);
				ind++;
			}

		}
	} else {
		nir = ip_hi - ip_low + 1;
//			System.out.println("nir="+nir+" ip_low="+ip_low+" ip_hi="+ip_hi+" ipix1="+
//					ipix1+" ipix2="+ipix2);
		//
		// Special case when ip_low < 0
		//
		if (ip_low < 0 ){
			ip_low = -(ip_low) ;
			nir1 = ip_low;
			nir2 = ip_hi + 1;
			nir = nir1 + nir2;
			if (do_ring) {
				int ind = 0;
				for (int j =  0; j < ip_low; j++) {
					res->push_back(ipix2 - j);
					ind++;
				}
				for (int j =  0; j <= ip_hi; j++) {
					res->push_back(ipix1 + j);
					ind++;
				}

			} else {

				int ind = 0;
				for (int j = 0; j < ip_low; j++) {
					in = ring2nest(nside, ipix2 - j);
					res->push_back(in);
					ind++;
				}
				for (int j =  0; j <= ip_hi; j++) {
					in = ring2nest(nside, ipix1 + j);
					res->push_back(in);
					ind++;
				}

			}

			return res;

		}
		ip_low += ipix1;
		ip_hi += ipix1;
		if (do_ring) {
			int ind = 0;
			for (int j =  ip_low; j <= ip_hi; j++) {
				res->push_back(j);
				ind++;
			}
		} else {

			int ind = 0;
			for (int j = ip_low; j <= ip_hi; j++) {
				in = ring2nest(nside, j);
				res->push_back(in);
				ind++;
			}
		}

	}

	return res;
}



























































	/**
	 * finds pixels having a colatitude (measured from North pole) :
	 * theta1 < colatitude < theta2 with 0 <= theta1 < theta2 <= Pi
	 * if theta2 < theta1
	 * then pixels with 0 <= colatitude < theta2 or theta1 < colatitude < Pi are
	 * returned
	 *
	 * @param nside
	 *            int the map resolution parameter
	 * @param theta1
	 *            lower edge of the colatitude
	 * @param theta2
	 *            upper edge of the colatitude
	 * @param nest
	 *            int if = 1 result is in NESTED scheme
	 * @return  ArrayList of  pixel numbers (int)
	 * @throws Exception
	 * @throws IllegalArgumentException
	 */

	vector<int>* PixTools::query_strip(int nside, double theta1, double theta2,int nest)  {
		vector<int> *res = new vector<int>();
		vector<int> *listir;
		int npix, nstrip;
		int iz,  irmin, irmax;
        int is;
		double phi0, dphi;
		double colrange[4];
		bool nest_flag = false;
		string SID = " QUERY_STRIP";
		// ---------------------------------------- //
		npix = Nside2Npix(nside);
		if (nest == 1)
			nest_flag = true;
		if (npix < 0) {
			cout<<SID<<" Nside should be power of 2"<<endl;
			exit(-1);
		}
		if ((theta1 < 0.0 || theta1 > M_PI) || (theta2 < 0.0 || theta2 > M_PI)) {
			cout<<SID<<" Illegal value of theta1, theta2"<<endl;
			exit(-1);
		}
		if (theta1 <= theta2) {
			nstrip = 1;
			colrange[0] = theta1;
			colrange[1] = theta2;
		} else {
			nstrip = 2;
			colrange[0] = 0.0;
			colrange[1] = theta2;
			colrange[2] = theta1;
			colrange[3] = M_PI;
		}
		// loops on strips //
		for (is = 0; is < nstrip; is++) {
			irmin = RingNum(nside, cos(colrange[2 * is]));
			irmax = RingNum(nside, cos(colrange[2 * is + 1]));
			// loop on ring number //
			for (iz = irmin; iz <= irmax; iz++) {
				phi0 = 0.;
				dphi = M_PI;
				listir = InRing(nside, iz, phi0, dphi, nest_flag);
				res= addAll(res,listir);
			}
		}
		return res;
	}

	/**
	 * generates a list of pixels that lay inside a triangle defined by
	 * the three vertex vectors
	 *
	 * @param nside
	 *            int map resolution parameter
	 * @param v1
	 *            Vector3D defines one vertex of the triangle
	 * @param v2
	 *            Vector3D another vertex
	 * @param v3
	 *            Vector3D yet another one
	 * @param nest
	 *            int 0 (default) RING numbering scheme, if set to 1 the NESTED
	 *            scheme will be used.
	 * @param inclusive
	 *            int 0 (default) only pixels whose centers are inside the
	 *            triangle will be listed, if set to 1 all pixels overlaping the
	 *            triangle will be listed
	 * @return ArrayList with pixel numbers
	 * @throws Exception
	 * @throws IllegalArgumentException
	 */

	vector<int>* PixTools::query_triangle(int nside, Vector3D v1,
			Vector3D v2,Vector3D v3, int nest, int inclusive)  {
		vector<int> *res = new vector<int>();

		vector<int> *listir;
		int npix, iz, irmin, irmax, n12, n123a, n123b, ndom = 0;
		bool test1, test2, test3;
		bool test1a, test1b, test2a, test2b, test3a, test3b;
		double twopi =  (double)PixTools::TWOPI;
		double dth1, dth2, determ, sdet;
		double zmax, zmin, z1max, z1min, z2max, z2min, z3max, z3min;
		double z, tgth, st, offset, sin_off;
		double phi_pos, phi_neg;
		Vector3D* vv[3];
		Vector3D* vo[3];
		double sprod[3];
		double sto[3];
		double phi0i[3];
		double tgthi[3];
		double dc[3];

		vector<double> dom[3];

		vector<double> *dom12; // [4];
		vector<double> *dom123a; // [4] ;
		vector<double> *dom123b; // [4];
		vector<double> *alldom; //[6];
		double a_i, b_i, phi0, dphiring;
		int idom;
//
		for (int i=0; i<3; i++) {
			dom[i].push_back(0.); dom[i].push_back(0.);dom[i].push_back(0.);dom[i].push_back(0.);
		}
		bool do_inclusive = false;
		bool do_nest = false;
		string SID = "QUERY_TRIANGLE";
		int nsidesq = nside * nside;
		//                                       //

//		cout<<"in query_triangle"<<endl;
		npix = Nside2Npix(nside);
		if (npix < 0) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		if (inclusive == 1)
			do_inclusive = true;
		if (nest == 1)
			do_nest = true;

		vv[0] = new Vector3D(v1);
		vv[0]->normalize();
		vv[1] = new Vector3D(v2);
		vv[1]->normalize();
		vv[2] = new Vector3D(v3);
		vv[2]->normalize();
		//                                  //
		dth1 = 1.0 / (3.0 * nsidesq);
		dth2 = 2.0 / (3.0 * nside);
		//
		// determ = (v1 X v2) . v3 determines the left ( <0) or right (>0)
		// handedness of the triangle
		//
		Vector3D vt(0., 0., 0.);
		vt = crossProduct(*vv[0], *vv[1]);
		determ = dotProduct(vt, *vv[2]);

		if (fabs((double)determ) < 1.0e-20) {
			cout<<SID<<": the triangle is degenerated - query cannot be performed"<<endl;
			exit(-1);
		}
		if (determ >= 0.) { // The sign of determinant
			sdet = 1.0;
		} else {
			sdet = -1.0;
		}

		sprod[0] = dotProduct(*vv[1], *vv[2]);
		sprod[1] = dotProduct(*vv[2], *vv[0]);
		sprod[2] = dotProduct(*vv[0], *vv[1]);
		// vector orthogonal to the great circle containing the vertex doublet //

		vo[0] = new Vector3D(crossProduct(*vv[1], *vv[2]));
		vo[1] = new Vector3D(crossProduct(*vv[2], *vv[0]));
		vo[2] = new Vector3D(crossProduct(*vv[0], *vv[1]));
		vo[0]->normalize();
		vo[1]->normalize();
		vo[2]->normalize();
		// test presence of poles in the triangle //
		zmax = -1.0;
		zmin = 1.0;
		test1 = (vo[0]->z * sdet >= 0.0); // north pole in hemisphere defined by
		// 2-3
		test2 = (vo[1]->z * sdet >= 0.0); // north pole in the hemisphere defined
		// by 1-2
		test3 = (vo[2]->z * sdet >= 0.0); // north pole in hemisphere defined by
		// 1-3
		if (test1 && test2 && test3)
			zmax = 1.0; // north pole in the triangle
		if ((!test1) && (!test2) && (!test3))
			zmin = -1.0; // south pole in the triangle
		// look for northenest and southernest points in the triangle //
		test1a = ((vv[2]->z - sprod[0] * vv[1]->z) >= 0.0); // segment 2-3
		test1b = ((vv[1]->z - sprod[0] * vv[2]->z) >= 0.0);
		test2a = ((vv[2]->z - sprod[1] * vv[0]->z) >= 0.0); // segment 1-3
		test2b = ((vv[0]->z - sprod[1] * vv[2]->z) >= 0.0);
		test3a = ((vv[1]->z - sprod[2] * vv[0]->z) >= 0.0); // segment 1-2
		test3b = ((vv[0]->z - sprod[2] * vv[1]->z) >= 0.0);

		// sin of theta for orthogonal vector //
		for (int i = 0; i < 3; i++) {
			sto[i] = sqrt((1.0 - vo[i]->z) * (1.0 + vo[i]->z));
		}
		//
		// for each segment ( side of the triangle ) the extrema are either -
		// -the 2 vertices - one of the vertices and a point within the segment
		//
		// segment 2-3
		z1max = vv[1]->z;
		z1min = vv[2]->z;

		// segment 1-3
		z2max = vv[2]->z;
		z2min = vv[0]->z;

		// segment 1-2
		z3max = vv[0]->z;
		z3min = vv[1]->z;


		zmax = fmax(fmax(z1max, z2max), fmax(z3max, zmax));
		zmin = fmin(fmin(z1min, z2min), fmin(z3min, zmin));

		//
		// if we are inclusive, move upper point up, and lower point down, by a
		// half pixel size
		 //
		offset = 0.0;
		sin_off = 0.0;
		if (do_inclusive) {
			offset = M_PI / (4.0 * nside); // half pixel size
			sin_off = sin(offset);
			zmax = fmin(1.0, cos(acos(zmax) - offset));
			zmin = fmax(-1.0, cos(acos(zmin) + offset));
		}

		irmin = RingNum(nside, zmax);
		irmax = RingNum(nside, zmin);



		// loop on the rings //
		for (int i = 0; i < 3; i++) {
			tgthi[i] = -1.0e30 * vo[i]->z;
			phi0i[i] = 0.0;
		}
		for (int j = 0; j < 3; j++) {
			if (sto[j] != 0.) {
				tgthi[j] = -vo[j]->z / sto[j]; // - cotan(theta_orth)

				phi0i[j] = atan2(vo[j]->y, vo[j]->x); // Should make it 0-2pi
														 // ?
				// Bring the phi0i to the [0,2pi] domain if need //

				if (phi0i[j] < 0.) {
					phi0i[j] = PixTools::MODULO(
							(atan2(vo[j]->y, vo[j]->x) + twopi), twopi); //  [0-2pi]
				}

			}

		}

		//
		// the triangle boundaries are geodesics: intersection of the sphere
		// with plans going through (0,0,0) if we are inclusive, the boundaries
		// are the intersection of the sphere with plains pushed outward by
		// sin(offset)
		//
//		double temp = 0.;
		bool found = false;
		for (iz = irmin; iz <= irmax; iz++) { // loop on ringz
			alldom = new vector<double>();
			found = false;
			if (iz <= nside - 1) { // North polar cap
				z = 1.0 - iz * iz * dth1;
			} else if (iz <= 3 * nside) { // tropical band + equator
				z = (2.0 * nside - iz) * dth2;
			} else {
				z = -1.0 + (4.0 * nside - iz) * (4.0 * nside - iz) * dth1;
			}

			// computes the 3 intervals described by the 3 great circles //
			st = sqrt((1.0 - z) * (1.0 + z));
			tgth = z / st; // cotan(theta_ring)
			for (int j = 0; j < 3; j++) {
				dc[j] = tgthi[j] * tgth - sdet * sin_off/ ((sto[j] + 1.0e-30) * st);

			}
			for (int k = 0; k < 3; k++) {
				if (dc[k] * sdet <= -1.0) { // the whole iso-latitude ring is on
					// right side of the great circle

					dom[k].operator[](0) = 0.0;
					dom[k].operator[](1) =twopi;

				} else if (dc[k] * sdet >= 1.0) { // all on the wrong side


					dom[k].operator[](0) = (-1.000001 * (k + 1));
					dom[k].operator[](1) = (-1.0 * (k + 1));
				} else { // some is good some is bad
					phi_neg = phi0i[k] - (acos(dc[k]) * sdet);
					phi_pos = phi0i[k] + (acos(dc[k]) * sdet);
					//
					if (phi_pos < 0.)
						phi_pos += twopi;
					if (phi_neg < 0.)
						phi_neg += twopi;

					//

					dom[k].operator[](0) =(PixTools::MODULO(phi_neg, twopi));
					dom[k].operator[](1) =(PixTools::MODULO(phi_pos, twopi));

				}
				//

			}

			// identify the intersections (0,1,2 or 3) of the 3 intervals //
			vector<double>*dom0 = (vector<double>*)&dom[0];
			vector<double>*dom1 =(vector<double>*) &dom[1];

			dom12 = intrs_intrv(dom0, dom1);

			n12 = (int)dom12->size()/2;



			if (n12 != 0) {
				if (n12 == 1) {
					vector<double>* tmp1 = new vector<double>();
					tmp1->push_back(dom[2].operator[](0)); tmp1->push_back(dom[2].operator[](1));

					dom123a = intrs_intrv(tmp1, dom12);
					n123a = ((int)dom123a->size())/2;


					if (n123a == 0)
						found = true;
					if (!found) {
						for (int l = 0; l <(int)dom123a->size(); l++) {
							alldom->push_back( dom123a->operator [](l));
						}

						ndom = n123a; // 1 or 2
					}
					delete(tmp1);
				}
				if (!found) {
					if (n12 == 2) {
						vector<double> * tmp =  new vector<double>();
						tmp->push_back( dom12->operator [](0)); tmp->push_back( dom12->operator [](1));
						dom123a = intrs_intrv((vector<double>*) &dom[2], tmp);
						vector<double>* tmp1 = new vector<double>();
						tmp1->push_back( dom12->operator [](2)); tmp1->push_back( dom12->operator [](3));
						dom123b = intrs_intrv((vector<double>*)&dom[2], tmp1);

						n123a =(int) dom123a->size()/2;  n123b = (int) dom123b->size()/2;
						ndom = n123a + n123b; // 0, 1, 2 or 3

						if (ndom == 0)
							found = true;
						if (!found) {
							if (n123a != 0) {
								for (int l = 0; l < 2 * n123a; l++) {
									alldom->push_back( dom123a->operator [](l));
								}
							}
							if (n123b != 0) {
								for (int l = 0; l < 2 * n123b; l++) {
									alldom->push_back(dom123b->operator [](l));
								}
							}
							if (ndom > 3) {
								cout<<SID<<": too many intervals found"<<endl;
								exit(-1);
							}
						}
						delete(tmp); delete(tmp1);

					}
				}
				if (!found) {
					for (idom = 0; idom < ndom; idom++) {
						a_i = alldom->operator []((int) (2 * idom));
						b_i = alldom->operator []((int) (2 * idom + 1));
						phi0 = (a_i + b_i) / 2.0;
						dphiring = fabs(b_i - a_i) / 2.0;

						if (dphiring < 0.0) {
							phi0 += M_PI;
							dphiring += M_PI;
						}

						// finds pixels in the triangle on that ring //


						listir = InRing(nside, iz, phi0, dphiring, do_nest);

						res=addAll(res,listir);

					}
				}
			}

			delete(alldom);
		}
		delete(vv[0]);
		delete(vv[1]);
		delete(vv[2]);
		delete(vo[0]);
		delete(vo[1]);
		delete(vo[2]);

		return res;
	}


	/**
	 * finds pixels that lay within a CONVEX polygon defined by its vertex on
	 * sphere
	 *
	 * @param nside
	 *            the map resolution
	 * @param vlist
	 *            ArrayList of vectors defining the polygon vertices
	 * @param nest
	 *            if set to 1 use NESTED scheme
	 * @param inclusive
	 *            if set 1 returns all pixels crossed by polygon boundaries
	 * @return  ArrayList of pixels
	 *
	 * algorithm: the polygon is divided into triangles vertex 0 belongs to all
	 * triangles
	 * @throws Exception
	 * @throws IllegalArgumentException
	 */

	vector<int>* PixTools::query_polygon(int nside, vector<Vector3D> vlist, int nest,int inclusive) {
		vector<int> *res = new vector<int>();
		int nv = vlist.size();
		vector<int>::iterator it;
		Vector3D vp0, vp1, vp2;
		Vector3D vo;
		vector<int> *vvlist = new vector<int>();
//		double surface, fsky;
		double hand;
		double ss[nv];
//		int n_in_trg, ilist, ntl;
        int npix;
		int ix = 0;

		int n_remain, np, nm, nlow;
		string SID = "QUERY_POLYGON";

		//		System.out.println("Start polygon");
		for (int k = 0; k < nv; k++)
			ss[k] = 0.;
		// -------------------------------------- //
		n_remain = nv;
		if (n_remain < 3) {
			cout<<SID<<" Number of vertices should be >= 3"<<endl;
		}
		//---------------------------------------------------------------- //
		// Check that the poligon is convex or has only one concave vertex //
		//---------------------------------------------------------------- //
		int i0 = 0;
		int i2 = 0;
		if (n_remain > 3) { // a triangle is always convex
			for (int i1 = 1; i1 <= n_remain - 1; i1++) { // in [0,n_remain-1]
				i0 = (int) PixTools::MODULO(i1 - 1, n_remain);
				i2 = (int) PixTools::MODULO(i1 + 1, n_remain);
				vp0 = (Vector3D) vlist[i0]; // select vertices by 3
												// neighbour
				vp1 = (Vector3D) vlist[i1];
				vp2 = (Vector3D) vlist[i2];
				// computes handedness (v0 x v2) . v1 for each vertex v1
				vo = new Vector3D(crossProduct(vp0, vp2));
				hand = dotProduct(vo, vp1);
				if (hand >= 0.) {
					ss[i1] = 1.0;
				} else {
					ss[i1] = -1.0;
				}

			}
			np = 0; // number of vert. with positive handedness
			for (int i = 0; i < nv; i++) {
				if (ss[i] > 0.)
					np++;
			}
			nm = n_remain - np;

			nlow = min(np, nm);

			if (nlow != 0) {
				if (nlow == 1) { // only one concave vertex
					if (np == 1) { // ix index of the vertex in the list
						for (int k = 0; k < nv - 1; k++) {
							if (fabs(ss[k] - 1.0) <= 1.e-12) {
								ix = k;
								break;
							}
						}
					} else {
						for (int k = 0; k < nv - 1; k++) {
							if (fabs(ss[k] + 1.0) <= 1.e-12) {
								ix = k;
								break;
							}
						}
					}

					// rotate pixel list to put that vertex in #0
					int n_rot = vlist.size() - ix;
//					int ilast = vlist.size() - 1;
					for (int k = 0; k < n_rot; k++) {

						Vector3D temp = (Vector3D) vlist[vlist.size() - 1];
						vlist.erase(vlist.end());
						vlist.push_back( temp);
					}
				}
				if (nlow > 1) { // more than 1concave vertex
					cout<<" The polygon has more than one concave vertex The result is unpredictable"<<endl;
				}
			}
		}
		// fill the polygon, one triangle at a time //
		npix = (int) Nside2Npix(nside);
		vector<int> *templist;
		while (n_remain >= 3) {
			vp0 = (Vector3D) vlist[0];
			vp1 = (Vector3D) vlist[n_remain - 2];
			vp2 = (Vector3D) vlist[n_remain - 1];

			// find pixels within the triangle //
			 // = new vector<int>();
			templist = query_triangle(nside, vp0, vp1, vp2, nest, inclusive);

			vvlist=addAll(vvlist,templist);
			n_remain--;
		}
		delete(templist);
		// make final pixel list //
		npix = (int)vvlist->size();
		int pixels[npix];
		for (int i = 0; i < npix; i++) {
			pixels[i] = vvlist->operator [](i);
		}
		 std::sort(pixels, pixels+npix);

		int k = 0;
//		res.add(k, new int(pixels[0]));
		for (int i = 0; i < npix; i++) {
			if (pixels[i] > pixels[i - 1]) {
				k++;
				res->push_back(pixels[i]);
			}
		}
		delete(vvlist);
		return res;
	}

	/**
	 * renders the pixel number ipix (RING scheme) for a pixel which contains a
	 * point on a sphere at coordinate vector (x,y,z), given the map resolution
	 * parameter nside
	 *
	 * @param nside
	 *            int map resolution
	 * @param vector
	 *            Vector3D of the point coordinates
	 * @return  int pixel number
	 * @throws IllegalArgumentException
	 */

	int PixTools::vect2pix_ring(int nside, Vector3D vector)  {
		int res = 0;
		int nl2, nl4, ncap, npix, jp, jm, ipix1;
		double z, za, tt, tp, tmp, dnorm, phi;
		float twopi = PixTools::TWOPI;
		float halfpi = PixTools::HALFPI;
		float twothird = PixTools::twothird;
		int ir, ip, kshift;
		string SID = " vect2pix_ring:";
		//                                      //
		if (nside < 1 || nside > PixTools::ns_max) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		dnorm = vector.magnitude();
		z = vector.z / dnorm;
		phi = 0.;
		if (vector.x != 0. || vector.y != 0.)
			phi = atan2(vector.y, vector.x); // phi in [-pi,pi]
		za = fabs((float)z);
		if (phi < 0.)
			phi += twopi; //  phi in [0, 2pi]
		tt = phi / halfpi; // in [0,4]

		nl2 = 2 * nside;
		nl4 = 4 * nside;
		ncap = nl2 * (nside - 1); // number of pixels in the north polar cap
		npix = 12 * nside * nside;
		if (za < twothird) { // equatorial region
			jp = (int) (nside * (0.5 + tt - 0.75 * z)); // index of ascending
			// edge line
			jm = (int) (nside * (0.5 + tt + 0.75 * z)); // index of descending
			// edge line

			ir = nside + 1 + jp - jm; // in [1,2n+1]
			kshift = 0;
			if ((int) PixTools::MODULO((float)ir, 2.) == 0)
				kshift = 1; // 1 if ir even, 0 otherwise
			ip = (int) ((jp + jm - nside + kshift + 1) / 2) + 1; // in [1,4n]
			ipix1 = ncap + nl4 * (ir - 1) + ip;
		} else { // North and South polar caps
			tp = tt - (int) tt;
			tmp = sqrt(3.0 * (1.0 - za));
			jp = (int) (nside * tp * tmp); // increasing edge line index
			jm = (int) (nside * (1.0 - tp) * tmp); // decreasing edge index

			ir = jp + jm + 1; // ring number counted from closest pole
			ip = (int) (tt * ir) + 1; // in [1,4*ir]
			if (ip > 4 * ir)
				ip = ip - 4 * ir;

			ipix1 = 2 * ir * (ir - 1) + ip;
			if (z <= 0.0)
				ipix1 = npix - 2 * ir * (ir + 1) + ip;
		}
		res = ipix1 - 1; // in [0, npix-1]
		return res;
	}

	/**
	 * renders vector (x,y,z) coordinates of the nominal pixel center for the
	 * pixel ipix (NESTED scheme ) given the map resolution parameter nside. This
	 * can be get using method pix2vect_nest Also calculates the (x,y,z)
	 * positions of 4 pixel vertices (corners) in the order N,W,S,E.
	 * The result can be used using method pix2vertex_nest.
	 * @param nside int the map resolution
	 * @param ipix int pixel number
	 * @return result
	 */

	PixInfo PixTools::makePix2Vect_Nest(int nside, int ipix)  {
		Vector3D pixVect(0.,0.,0.);

		double pixVertex[3][4];
		PixInfo res;
		int npix, npface, ipf, ip_low, ip_trunc, ip_med, ip_hi;
		int jrt, jr, nr, jpt, jp, kshift, nl4;
		double z, fn, fact1, fact2, sth, phi;
		int pixmax = PixTools::pixmax;
		int xmid =  PixTools::xmid;
		int ix, iy, face_num;
		int jrll[] = { 0, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 };
		int jpll[] = { 0, 1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7 };
		double phi_nv, phi_wv, phi_sv, phi_ev, phi_up, phi_dn;
		double z_nv, z_sv, sth_nv, sth_sv;
		double hdelta_phi;
		int iphi_mod, iphi_rat;
//		boolean do_vertex = true;
		string SID = "Pix2Vect_Nest:";
		z_nv = 0.;
		z_sv = 0.;
		//                                 //
		if (nside < 1 || nside > PixTools::ns_max) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		int nsidesq = nside * nside;
		npix = 12 * nsidesq;
		if (ipix < 0 || ipix > npix - 1) {
			cout<<SID<<" ipix out of range calculated from nside"<<endl;
			exit(-1);
		}
		// initiates the array for the pixel number -> (x,y) mapping //
		if (memAl.pix2x[PixTools::pixmax-1] <= 0)
			mk_pix2xy();
		fn = nside;
		fact1 = 1.0 / (3.0 * fn * fn);
		fact2 = 2.0 / (3.0 * fn);
		nl4 = 4 * nside;

		// finds the face and the number in the face //
		npface = nsidesq;
		//		System.out.println("ipix="+ipix+" npface="+npface);
		face_num = ipix / npface; // face number in [0, 11]
		ipf = (int) PixTools::MODULO(ipix, npface); // pixel number in the face [0,npface-1]
		//
		// finds the x,y on the face (starting from the lowest corner) from the
		// pixel number
		//
		ip_low = (int) PixTools::MODULO(ipf, pixmax); // last 18 bits
		ip_trunc = ipf / pixmax; // trancateion of the last 18 bits
		ip_med = (int) PixTools::MODULO(ip_trunc, pixmax); // next 18 bits
		ip_hi = ip_trunc / pixmax; // high 18 bits

		ix = pixmax * memAl.pix2x[(int)ip_hi] + xmid * memAl.pix2x[(int)ip_med] +
				memAl.pix2x[(int)ip_low];
		iy = pixmax * memAl.pix2y[(int)ip_hi] + xmid * memAl.pix2y[(int)ip_med] +
			    memAl.pix2y[(int)ip_low];

		// transform this in (vertical, horizontal) coordinates //
		jrt = ix + iy; //  vertical in [0,2*(nside-1)]
		jpt = ix - iy; //  horizontal in [ -nside+1, nside-1]
		// computes the z coordinate on the sphere //
		jr = jrll[(int) (face_num + 1)] * nside - jrt - 1; // ring number in
		// [1,4*nside-1]

		nr = nside; // equatorial region (the most frequent )
		z = (2.0 * nside - jr) * fact2;

		kshift = (int) PixTools::MODULO(jr - nside, 2);
		z_nv = (2.0 * nside - jr + 1.0) * fact2;
		z_sv = (2.0 * nside - jr - 1.0) * fact2;
		if (jr == nside) { // northen transition
			z_nv = 1.0 - (nside - 1.0) * (nside - 1.0) * fact1;
		} else if (jr == 3 * nside) { // southern transition
			z_sv = -1.0 + (nside - 1.0) * (nside - 1.0) * fact1;
		}

		if (jr < nside) { // north pole region
			nr = jr;
			z = 1.0 - nr * nr * fact1;
			kshift = 0;

			z_nv = 1.0 - (nr - 1) * (nr - 1) * fact1;
			z_sv = 1.0 - (nr + 1) * (nr + 1) * fact1;

		} else if (jr > 3 * nside) { // south pole region
			nr = nl4 - jr;
			z = -1.0 + nr * nr * fact1;
			kshift = 0;

			z_nv = -1.0 + (nr + 1) * (nr + 1) * fact1;
			z_sv = -1.0 + (nr - 1) * (nr - 1) * fact1;

		}
		// computes the phi coordinate on the sphere, in [0,2pi] //
		jp = (jpll[(int) (face_num + 1)] * nr + jpt + 1 + kshift) / 2; // phi in the ring in
		                                                       // [1,4*nr]
		if (jp > nl4)
			jp = jp - nl4;
		if (jp < 1)
			jp = jp + nl4;

		phi = (jp - (kshift + 1) / 2.) * (M_PI /(2.* nr));

		sth = sqrt((1.0 - z) * (1.0 + z));
		pixVect.x = sth * cos(phi);
		pixVect.y = sth * sin(phi);
		pixVect.z = z;

		phi_nv = phi;
		phi_sv = phi;

		phi_up = 0.0;
		iphi_mod = (int) PixTools::MODULO(jp - 1, nr); // in [0,1,...,nr-1]
		iphi_rat = (jp - 1) / nr; // in [0,1,2,3]
		if (nr > 1)
			phi_up = 0.5*M_PI * (iphi_rat + iphi_mod / (nr - 1.));
		phi_dn = 0.5*M_PI* (iphi_rat + (iphi_mod + 1) / (nr + 1.));

		if (jr < nside) { // north polar cap
			phi_nv = phi_up;
			phi_sv = phi_dn;
		} else if (jr > 3 * nside) { // south polar cap
			phi_nv = phi_dn;
			phi_sv = phi_up;
		} else if (jr == nside) { // north transition
			phi_nv = phi_up;
		} else if (jr == 3 * nside) { // south transition
			phi_sv = phi_up;
		}
		hdelta_phi = M_PI / (4.0 * nr);
		// west vertex //
//		phi_wv = phi = hdelta_phi;
		phi_wv = phi - hdelta_phi;
		pixVertex[0][1] = sth * cos(phi_wv);
		pixVertex[1][1] = sth * sin(phi_wv);
		pixVertex[2][1] = z;
		// east vertex //
		phi_ev = phi + hdelta_phi;
		pixVertex[0][3] = sth * cos(phi_ev);
		pixVertex[1][3] = sth * sin(phi_ev);
		pixVertex[2][3] = z;
		// north vertex //
		sth_nv = sqrt((1.0 - z_nv) * (1.0 + z_nv));
		pixVertex[0][0] = sth_nv * cos(phi_nv);
		pixVertex[1][0] = sth_nv * sin(phi_nv);
		pixVertex[2][0] = z_nv;
		// south vertex //
		sth_sv = sqrt((1.0 - z_sv) * (1.0 + z_sv));
		pixVertex[0][2] = sth_sv * cos(phi_sv);
		pixVertex[1][2] = sth_sv * sin(phi_sv);
		pixVertex[2][2] = z_sv;
		res.pixVect = pixVect;
		for (int i=0; i<3;i++) {
			for (int j=0; j<4; j++) {
				res.pixVertex[i][j] = pixVertex[i][j];
			}
		}
		return res;
	}

	/**
	 * renders the pixel number pix (NESTED scheme) for a pixel which contains a
	 * point on a sphere at coordinates theta and phi, given map resolution
	 * parameter nside.
	 *
	 * The computation is made to the highest resolution available and then
	 * degraded to required resolution by integer division. It makes sure that
	 * the treatment of round-off will be consistent for every resolution.
	 *
	 * @param nside the map resolution parameter
	 * @param theta double theta coordinate in radians
	 * @param phi double phi coordinate in radians
	 * @return pixel number int
	 * @throws IllegalArgumentException
	 */

    int PixTools::ang2pix_nest(int nside, double theta, double phi) {
		int pix = 0;
		float comp = PixTools::twothird;
		float twopi = PixTools::TWOPI;
		float halfpi = PixTools::HALFPI;
		float fxmax = PixTools::xmax;
		float fns_max = PixTools::ns_max;
//
		double z, za, tt, tp, tmp;
		double jp, jm, ifp, ifm, ix, iy, ix_low, ix_hi;
		double iy_low, iy_hi, ipf, ntt;
		int face_num;
//
		string SID = "ang2pix_nest:";
		//                              //
		if (nside < 1 || nside > PixTools::ns_max) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		if (theta < 0.0 || theta > M_PI) {
			cout<<SID<<" theta is out of range [0.,PI]"<<endl;
			exit(-1);
		}
		if (memAl.x2pix[PixTools::xmax-1] <= 0)
			 mk_xy2pix();

		z = cos(theta);
		za = fabs(z);


		if (phi < 0.)
			phi += twopi; // phi in [0,2pi]
		if (phi >= twopi ) phi -= twopi;
		tt = PixTools::MODULO(phi, twopi) / halfpi; // in [0,4]

//		tt = 2. * phi / PI; // in [0,4]

		if (za <= comp) { // Equatorial region
			//
			// the index of edge lines increases when the intitude = phi goes
			// up
			//

			jp = (int) (fns_max * (0.5 + tt - z * 0.75)); // ascending edge line
			// index
			jm = (int) (fns_max * (0.5 + tt + z * 0.75)); // descending edge line
			// index

			// find the face //
			ifp = (int)( jp / fns_max); // in [0,4]
			ifm = (int)(jm /fns_max);
			if (ifp == ifm) { // faces 4 to 7
				face_num = (int) PixTools::MODULO((float)ifp, 4.) + 4;
			} else if (ifp < ifm) { // (half-) faces 0 to 3
				face_num = (int) PixTools::MODULO((float)ifp, 4.);
			} else { // (half-) faces 8 to 11
				face_num = (int) PixTools::MODULO((float)ifm, 4.) + 8;
			}

			ix = (int)( PixTools::MODULO(jm, fns_max));
			iy = fns_max - (int) PixTools::MODULO(jp, fns_max) - 1;

		} else { // polar region, za > 2/3
			ntt = (int) tt;
			if (ntt >= 4)
				ntt = 3;
			tp = tt - ntt;
			tmp = sqrt(3.0 * (1.0 - za)); // in [0,1]
			//
			// the index of edge lines increases when distance from the closest
			// pole goes up
			//
			jp =  (fns_max * tp * tmp); // line going toward the pole has
			// phi increases
			jm =  (fns_max * (1.0 - tp) * tmp); // that one goes away of the
			// closest pole
			jp =  fmin(fns_max - 1, jp); // for pointss too close to the
			// boundary
			jm =  fmin(fns_max - 1, jm);
			// finds the face and pixel's (x,y) //
			if (z >= 0) {
				face_num =(int) ntt; // in [0,3]
				ix = fns_max - jm - 1;
				iy = fns_max - jp - 1;
			} else {
				face_num =(int) (ntt + 8); // in [8,11]
				ix = jp;
				iy = jm;
			}
		}

		ix_low =  PixTools::MODULO((float)ix, fxmax);

//		ix_hi = ix / PixTools::xmax;
		ix_hi = ix / fxmax;

		iy_low =  PixTools::MODULO((float)iy, fxmax);
//		iy_hi = iy / PixTools::xmax;
		iy_hi = iy / fxmax;
		ipf = (memAl.x2pix[(int) (ix_hi + 1)] + memAl.y2pix[(int) (iy_hi + 1.)]) * (fxmax * fxmax)
				+ (memAl.x2pix[(int) (ix_low + 1)] + memAl.y2pix[(int) (iy_low + 1)]);

		ipf = ipf / ((fns_max / nside) * (fns_max / nside)); // in [0, nside**2
		// -1]

		pix = (int) (ipf + face_num * nside * nside); // in [0, 12*nside**2 -1]
		return pix;
	}

	/**
	 * make the conversion NEST to RING
	 *
	 * @param nside the map resolution parameter
	 * @param map Object[] the map in NESTED scheme
	 * @return - Object[] a map in RING scheme
	 * @throws IllegalArgumentException
	 */

	int* PixTools::convert_nest2ring(int nside, int* map)  {

		int npix, ipn;
        int ipr;
		npix = 12 * nside * nside;
		int *res = new int[npix];
		for (ipn = 0; ipn < npix; ipn++) {
			ipr = (int) nest2ring(nside, ipn);
			res[ipr] = map[(int) ipn];
		}
		return res;
	}

	/**
	 * makes the conversion RING to NEST
	 *
	 * @param nside
	 *            int resolution
	 * @param map
	 *            map in RING
	 * @return  map in NEST
	 * @throws IllegalArgumentException
	 */

	 int* PixTools::convert_ring2nest(int nside, int* map)  {
		int npix, ipn, ipr;
		npix = 12 * nside * nside;
		int *res = new int[npix];
		for (ipr = 0; ipr < npix; ipr++) {
			ipn = ring2nest(nside, ipr);
			res[(int) ipn] = map[(int)ipr];
		}
		return res;
	}

	/**
	 * converts a 8 byte Object map from RING to NESTED and vice versa in place,
	 * ie without allocation a temporary map (Has no reason for Java). This
	 * method is more general but slower than convert_nest2ring.
	 *
	 * This method is a wrapper for functions ring2nest and nest2ring. Their
	 * names are supplied in the subcall argument.
	 *
	 * @param subcall
	 *            String name of the method to use.
	 * @param map
	 *            Object[] map
	 * @return  resulting Object[] map.
	 * @throws IllegalArgumentException
	 */
/*
	int* convert_inplace_long(string subcall, int map[]) {

		int npix, nside;
		int ilast, i1, i2;
		string SID = "convert_in_place:";
		int pixbuf1, pixbuf2;
		npix = map;
		nside = (int) sqrt(npix / 12.);
		if (nside > PixTools::ns_max) {
			throw new IllegalArgumentException(SID + " Map is too big");
		}
		bool check[(int) npix];
		for (int i = 0; i < npix; i++)
			check[i] = false;
		ilast = 0; // start from first pixel
		for (int i = 0; i < npix; i++) {
			pixbuf2 = map[(int) ilast];
			i1 = ilast;
			if (subcall.equalsIgnoreCase("ring2nest")) {
				i2 = ring2nest(nside, i1);
			} else {
				i2 = nest2ring(nside, i1);
			}
			while (!check[(int) i2]) {
				pixbuf1 = map[(int) i2];
				map[(int) i2] = pixbuf2;
				pixbuf2 = pixbuf1;
				i1 = i2;
				if (subcall.equalsIgnoreCase("ring2nest")) {
					i2 = ring2nest(nside, i1);
				} else {
					i2 = nest2ring(nside, i1);
				}
			}
			while (!(check[(int) ilast] && (ilast < npix - 1))) {
				ilast++;
			}
		}
		res = map;
		return res;
	}
*/

	/**
	 * generates in the RING or NESTED scheme all pixels that lays within an
	 * angular distance Radius of the center.
	 *
	 * @param nside
	 *            int map resolution
	 * @param vector
	 *            Vector3D pointing to the disc center
	 * @param radius
	 *            double angular radius of the disc (in RADIAN )
	 * @param nest
	 *            int 0 (default) if output is in RING scheme, if set to 1
	 *            output is in NESTED
	 * @param inclusive
	 *            int 0 (default) only pixels whose centers lay in the disc
	 *            are listed, if set to 1, all pixels overlapping the disc
	 *            are listed. In the inclusive mode the radius is increased by half the pixel size.
	 *            In this case most probably all neighbor pixels will be listed even with very small
	 *            radius.
	 *            In case of exclusive search and very small radius when the disc lays completely
	 *            inside a pixel the pixel number is returned using vector2pix method.
	 * @return  ArrayList of pixel numbers
	 *
	 * calls: RingNum(nside, ir) InRing(nside, iz, phi0, dphi,nest) vector2pix(nside,ipix)
	 */

	vector<int>* PixTools::query_disc(int nside, Vector3D vect, double radius, int nest, int inclusive) {
		vector<int> *res = new vector<int>();
		int irmin, irmax, iz,  npix, ilist;
//		double norm_vect0;
		double x0, y0, z0, radius_eff;
		double a, b, c, cosang;
		double dth1, dth2;
		double phi0, cosphi0, cosdphi, dphi;
		double rlat0, rlat1, rlat2, zmin, zmax, z;
		double comp;
		double twopi = PixTools::TWOPI;
		double halfpi = PixTools::HALFPI;
//
		bool do_inclusive = false;
		bool do_nest = false;
		string SID = "QUERY_DISC";
		//
		//
		int nsidesq = nside * nside;
		npix = 12 * nsidesq;
		double pixres = PixRes(nside);      // in arc seconds
//		cout<<"PixRes="<<pixres<<endl;
//
		double halfPix =  M_PI / (4.0 * nside);

		if (radius < 0.0 || radius > M_PI) {
			cout<<SID<<": angular radius is in RADIAN and should be in [0,pi]"<<endl;
			exit(-1);
		}

		if (inclusive == 1)
			do_inclusive = true;
		if (nest == 1)
			do_nest = true;

		dth1 = 1.0 / (3.0 * nside * nside);
		dth2 = 2.0 / (3.0 * nside);

		radius_eff = radius;

		if (do_inclusive)

		radius_eff += halfPix;   // increase radius by half pixel

		cosang = cos(radius_eff);
		//
		// disc center
		//
		vect.normalize();
		x0 = vect.x; // norm_vect0;
		y0 = vect.y; // norm_vect0;
		z0 = vect.z; // norm_vect0;
//		cout<<"x0="<<x0<<" y0="<<y0<<" z0="<<z0<<endl;
		phi0 = 0.0;
		dphi = 0.0;
		if (x0 != 0. || y0 != 0.)
			phi0 = PixTools::MODULO(atan2(y0, x0) + twopi, twopi);  // in [0, 2pi]
			cosphi0 = cos(phi0);
//			System.out.println("phi0="+phi0+" cosphi0="+cosphi0);
		a = x0 * x0 + y0 * y0;
		// coordinate z of highest and lowest points in the disc
		rlat0 = asin(z0); // latitude in RAD of the center
		rlat1 = rlat0 + radius_eff;
		rlat2 = rlat0 - radius_eff;
//		System.out.println("rlat0="+rlat0+" rlat1="+rlat1+" rlat2="+rlat2);
		//
		if (rlat1 >= halfpi) {
			zmax = 1.0;
		} else {
			zmax = sin(rlat1);
		}
		comp = halfpi;
		irmin = RingNum(nside, zmax);
		irmin = max(1, irmin - 1); // start from a higher point to be safe
		if (rlat2 <= -comp) {
			zmin = -1.0;
		} else {
			zmin = sin(rlat2);
		}
		irmax = RingNum(nside, zmin);
		irmax = min(4 * nside - 1, irmax + 1); // go down to a lower point
//		System.out.println(" irmax="+irmax+" irmin="+irmin);
		ilist = -1;



        //
		// loop on ring number
		int done = 0;
		for (iz = irmin; iz <= irmax; iz++) {
			if (iz <= nside - 1) { // north polar cap
				z = 1.0 - iz * iz * dth1;
			} else if (iz <= 3 * nside) { // tropical band + equator
				z = (2.0 * nside - iz) * dth2;
			} else {
				z = -1.0 + (4.0 * nside - iz) * (4.0 * nside - iz) * dth1;
			}
			// find phi range in the disc for each z
			b = cosang - z * z0;
			c = 1.0 - z * z;
			cosdphi = b / sqrt(a * c);
			done = 0;

			if (fabs(x0) <= 1.0e-12 && fabs(y0) <= 1.0e-12) {
				cosdphi = -1.0;
				dphi = M_PI;
				done = 1;
			}
			if (done == 0) {
				if (fabs(cosdphi) <= 1.0) {
					dphi = acos(cosdphi); // in [0,pi]
				} else {
					if (cosphi0 >= cosdphi) {
						dphi = M_PI; // all the pixels at this elevation are in
						// the disc
					} else {
						done = 2; // out of the disc
					}
				}

			}
			if (done < 2) { // pixels in disc
				// find pixels in the disc
				vector<int> *listir = InRing(nside, iz, phi0, dphi, do_nest);
				for (int i=0; i< (int)listir->size(); i++) {
					res->push_back(listir->operator [](i));
				}
				delete(listir);
			}

		}
//
// if  radius less than pixel size check that the pixel number is in the list
//  and add one if it is missing.
//

		int pixel = 0;
		if ( pixres > radius*PixTools::RadToDeg*3600. ) {
//		    cout<<"case with r < pix. size"<<endl;
			if (do_nest) {
				pixel = vect2pix_nest(nside,vect);
			} else {
				pixel = vect2pix_ring(nside,vect);
			}
			if (!contains(*res,pixel))
			               res->push_back(pixel);
		}
		return res;
	}



	/**
	 * an obsolete method. Use query_disc instead.
	 *
	 * @param nside
	 * @param vector0
	 * @param radius
	 * @return - ArrayList of int
	 */

 vector<int>* PixTools::getDisc_ring(int nside, Vector3D vector0, double radius) {
		vector<int> *res = new vector<int>();
		int nest = 0;
		int inclusive = 0;
		res = query_disc(nside, vector0, radius, nest, inclusive);
		return res;
	}


	/**
	 * renders theta and phi coordinates of the nominal pixel center for the
	 * pixel number ipix (RING scheme) given the map resolution parameter nside
	 *
	 * @param nside
	 *            int map resolution
	 * @param ipix
	 *            int pixel number
	 * @return double[] theta,phi
	 */
	double* PixTools::pix2ang_ring(int nside, int ipix)  {
		double *res = new double[2];
		int nl2, nl4, npix, ncap, iring, iphi, ip, ipix1;
		double fodd, hip, fihip, theta, phi;
		string SID = "pix2ang_ring:";
		/*                            */
		if (nside < 1 || nside > PixTools::ns_max) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		int nsidesq = nside * nside;
		npix = 12 * nsidesq; // total number of pixels
		if (ipix < 0 || ipix > npix - 1) {
			cout<<SID<<" ipix out of range calculated from nside"<<endl;
			exit(-1);
		}
		ipix1 = ipix + 1; //  in [1, npix]
		nl2 = 2 * nside;
		nl4 = 4 * nside;
		ncap = 2 * nside * (nside - 1); // points in each polar cap, =0 for
		// nside =1

		if (ipix1 <= ncap) { // North polar cap
			hip = ipix1 / 2.0;
			fihip = (int) hip; // get integer part of hip
			iring = (int) (sqrt(hip - sqrt(fihip))) + 1; // counted from north
			                                                       // pole
			iphi = ipix1 - 2 * iring * (iring - 1);
			theta = acos(1.0 - iring * iring / (3.0 * nsidesq));
			phi = ((double)iphi - 0.5) * M_PI / (2.0 * iring);


		} else if (ipix1 <= nl2 * (5 * nside + 1)) { // equatorial region
			ip = ipix1 - ncap - 1;
			iring = (int) (ip / nl4) + nside; // counted from North pole
			iphi = (int) PixTools::MODULO((float)ip,(float) nl4) + 1;
			fodd = 0.5 * (1. + PixTools::MODULO((float)(iring + nside), 2.)); // 1 if iring+nside
			                                                 // is odd, 1/2 otherwise
			theta = acos((nl2 - iring) / (1.5 * nside));
			phi = ((double)iphi - fodd) * M_PI / (2.0 * nside);

		} else { // South pole cap
			ip = npix - ipix1 + 1;
			hip = ip / 2.0;
			fihip = (int) hip;
			iring = (int) (sqrt(hip - sqrt(fihip))) + 1; // counted from South
			                                                       // pole
			iphi = 4 * iring + 1 - (ip - 2 * iring * (iring - 1));
			theta = acos(-1.0 + iring * iring / (3.0 * nsidesq));
			phi = ((double)iphi - 0.5) * M_PI / (2.0 * iring);

		}
		res[0] = theta;
		res[1] = phi;
		return res;
	}
	/**
	 * renders vector (x,y,z) coordinates of the nominal pixel center for pixel
	 * ipix (RING scheme) given the map resolution parameter nside. It also
	 * calculates (x,y,z) positions of the four vertices in order N,W,S,E.
	 * These results are returned in a PixInfo object.
	 * Those can be used using pix2Vect_ring and pix2vert_ring methods
	 *
	 * @param nside
	 *            int map resolution
	 * @param ipix
	 *            pixel number
	 * @return  result object
	 */
	PixInfo PixTools::makePix2Vect_ring(int nside, int ipix)  {
		PixInfo res;
		int nl2;
		Vector3D pixVect(0., 0., 0.);
		double pixVertex[3][4];

        int nl4;
        int iring, iphi, ip, ipix1;
        int npix,ncap;
		double phi_nv, phi_wv, phi_sv, phi_ev;
		double z_nv, z_sv, sth_nv, sth_sv, hdelta_phi;
		double fact1, fact2, fodd, hip, fihip, z, sth, phi;
		int iphi_mod;
        int iphi_rat;
//		boolean do_vertex = true;
		int nsidesq = nside * nside;
		string SID = " Pix2Vect_ring:";
		/*                                 */
		if (nside < 1 || nside > PixTools::ns_max) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}

		npix = 12 * nsidesq;
		if (ipix < 0 || ipix > npix - 1) {
			cout<<SID<<" ipix out of range calculated from nside"<<endl;
			exit(-1);
		}

		ipix1 = ipix + 1; //  in [1, npix]
		nl2 = 2 * nside;
		nl4 = 4 * nside;
		ncap = 2 * nside * (nside - 1); // points in each polar cap
		fact1 = 1.5 * nside;
		fact2 = 3.0 * nsidesq;
		phi_nv = 0.0;
		phi_sv = 0.0;
		if (ipix1 <= ncap) { // north polar cap
			hip = ipix1 / 2.0;
			fihip = (int) hip;
			iring = (int) (sqrt(hip - sqrt(fihip))) + 1; // counted from north
			                                                       // pole
			iphi = ipix1 - 2 * iring * (iring - 1);
			z = 1.0 - iring * iring / fact2;
			phi = (iphi - 0.5) * M_PI / (2.0 * iring);

			hdelta_phi = M_PI / (4.0 * iring); // half pixel width
			z_nv = 1.0 - (iring - 1) * (iring - 1) / fact2;
			z_sv = 1.0 - (iring + 1) * (iring + 1) / fact2;
			iphi_mod = (int) PixTools::MODULO(iphi - 1, iring); // in [0,1,...,iring-1]
			iphi_rat = (iphi - 1) / iring; // in [0,1,2,3]
			if (iring > 1)
				phi_nv = 0.5*M_PI * (iphi_rat + iphi_mod / (iring - 1.0));
			phi_sv = 0.5*M_PI * (iphi_rat + (iphi_mod + 1.0) / (iring + 1.0));
		} else if (ipix1 <= nl2 * (5 * nside + 1)) { // equatorial region
			ip =  (ipix1 - ncap - 1);
			iring = (int) (ip / nl4) + nside; // counted from North pole

			iphi = (int) PixTools::MODULO(ip, nl4) + 1;
			fodd = 0.5 * (1. + PixTools::MODULO(iring + nside, 2)); // 1 if iring+nside
			                                                 // is odd or 1/2
			z = (nl2 - iring) / fact1;
			phi = (iphi - fodd) * M_PI / (2.0 * nside);
			hdelta_phi = M_PI / (4.0 * nside); // half pixel width
			phi_nv = phi;
			phi_sv = phi;
			z_nv = (nl2 - iring + 1) / fact1;
			z_sv = (nl2 - iring - 1) / fact1;
			if (iring == nside) { // nothern transition
				z_nv = 1.0 - (nside - 1) * (nside - 1) / fact2;
				iphi_mod = (int) PixTools::MODULO(iphi - 1, nside); // in [0,1,...,nside-1]
				iphi_rat = (iphi - 1) / nside; // in [0,1,2,3]
				if (nside > 1)
					phi_nv = 0.5*M_PI * (iphi_rat + iphi_mod / (nside - 1.));
			} else if (iring == 3 * nside) { // southern transition
				z_sv = -1.0 + (nside - 1) * (nside - 1) / fact2;
				iphi_mod = (int) PixTools::MODULO(iphi - 1, nside); // in [0,1,... iring-1]
				iphi_rat = (iphi - 1) / nside; // in [0,1,2,3]
				if (nside > 1)
					phi_sv = 0.5*M_PI * (iphi_rat + iphi_mod / (nside - 1.0));
			}

		} else { // South polar cap
			ip = npix - ipix1 + 1;
			hip = ip / 2.0;
			fihip = (int) hip;
			iring = (int) (sqrt(hip - sqrt(fihip))) + 1; // counted from South
			                                                       // pole
			iphi = 4 * iring + 1 - (ip - 2 * iring * (iring - 1));
			z = -1.0 + iring * iring / fact2;
			phi = (iphi - 0.5) * M_PI / (2.0 * iring);
			hdelta_phi = M_PI / (4.0 * iring); // half pixel width
			z_nv = -1.0 + (iring + 1) * (iring + 1) / fact2;
			z_sv = -1.0 + (iring - 1) * (iring - 1) / fact2;
			iphi_mod = (int) PixTools::MODULO(iphi - 1, iring); // in [0,1,...,iring-1]
			iphi_rat = (iphi - 1) / iring; // in [0,1,2,3]
			phi_nv = 0.5*M_PI * (iphi_rat + (iphi_mod + 1) / (iring + 1.0));
			if (iring > 1)
				phi_sv = 0.5*M_PI * (iphi_rat + iphi_mod / (iring - 1.0));

		}
		/* pixel center */
		sth = sqrt((1.0 - z) * (1.0 + z));
		pixVect.x = sth * cos(phi);
		pixVect.y = sth * sin(phi);
		pixVect.z = z;
//		pixVect = new Vector3D(sth * Math.cos(phi), sth * Math.sin(phi), z);
		/* west vertex */
		phi_wv = phi - hdelta_phi;
		pixVertex[0][1] = sth * cos(phi_wv);
		pixVertex[1][1] = sth * sin(phi_wv);
		pixVertex[2][1] = z;
		/* east vertex */
		phi_ev = phi + hdelta_phi;
		pixVertex[0][3] = sth * cos(phi_ev);
		pixVertex[1][3] = sth * sin(phi_ev);
		pixVertex[2][3] = z;
		/* north vertex */
		sth_nv = sqrt((1.0 - z_nv) * (1.0 + z_nv));
		pixVertex[0][0] = sth_nv * cos(phi_nv);
		pixVertex[1][0] = sth_nv * sin(phi_nv);
		pixVertex[2][0] = z_nv;
		/* south vertex */
		sth_sv = sqrt((1.0 - z_sv) * (1.0 + z_sv));
		pixVertex[0][2] = sth_sv * cos(phi_sv);
		pixVertex[1][2] = sth_sv * sin(phi_sv);
		pixVertex[2][2] = z_sv;
		res.pixVect = pixVect;
		for (int i=0; i<3; i++) {
			for (int j=0; j<4; j++) {
				res.pixVertex[i][j] = pixVertex[i][j];
			}
		}
		return res;
	}
	/**
	 * returns the vector pointing in the center of the pixel ipix. The vector
	 * is calculated by makePix2Vect_ring method
	 *
	 * @param nside map resolution
	 * @param ipix pixel number
	 * @return Vector3D
	 */
	Vector3D PixTools::pix2vect_ring(int nside, int ipix)  {

		PixInfo pixInfo = makePix2Vect_ring(nside, ipix);
		Vector3D res(pixInfo.pixVect);
		return res;
	}

	/**
	 * returns double [][] with coordinates of the pixel corners. The array is
	 * calculated by makePix2Vect_ring method
	 *
	 * @param nside map resolution
	 * @param ipix pixel number
	 * @return  double[][] list of vertex coordinates
	 */
	double** PixTools::pix2vertex_ring(int nside, int ipix)  {
		double **res = new double*[4];
		PixInfo pixinfo = makePix2Vect_ring(nside, ipix);
		for (int i=0; i< 3; i++) {
			for (int j=0; j< 4; j++) {
				res[i][j] = pixinfo.pixVertex[i][j];
			}
		}

		return res;
	}



	/**
	 * renders the pixel number ipix (RING scheme) for a pixel which contains a
	 * point with coordinates theta and phi, given the map resolution parameter
	 * nside.
	 *
	 * @param nside
	 *            int map resolution parameter
	 * @param theta
	 *            double theta
	 * @param phi -
	 *            double phi
	 * @return  int ipix
	 */
	int PixTools::ang2pix_ring(int nside, double theta, double phi) {
		int nl4;
        int jp, jm, kshift;
        int ip;
        int ir;
        float twopi =  PixTools::TWOPI;
        float halfpi = PixTools::HALFPI;
        float twothird =  PixTools::twothird;
		double z, za, tt, tp, tmp;
		int pix = 0;
		int ipix1;
		int nl2,  ncap, npix;
		string SID = "ang2pix_ring:";
		/*                                       */
		if (nside < 1 || nside > PixTools::ns_max) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		if (theta < 0.0 || theta > M_PI) {
			cout<<SID<<" Theta out of range [0,pi]"<<endl;
			exit(-1);
		}

		z = cos(theta);
		za = fabs(z);



		if (phi >= twopi)  phi = phi - twopi ;

		if (phi < 0.)
			phi =phi + twopi; //  phi in [0, 2pi]
		tt = phi / halfpi; // in [0,4]
//		tt = BitManipulation.MODULO(phi, TWOPI) / HALFPI; // in [0,4]
		nl2 = 2 * nside;
		nl4 = 4 * nside;
		ncap = nl2 * (nside - 1); // number of pixels in the north polar cap
		npix = 12 * nside * nside;
		if (za < twothird) { // equatorial region
			jp = (int) (nside * (0.5 + tt - 0.75 * z)); // index of ascending
			// edge line
			jm = (int) (nside * (0.5 + tt + 0.75 * z)); // index of descending
			// edge line

			ir = nside + 1 + jp - jm; // in [1,2n+1]
			kshift = 0;
			if ((int) PixTools::MODULO((float)ir, 2.) == 0)
				kshift = 1; // 1 if ir even, 0 otherwise
			ip = (int) ((jp + jm - nside + kshift + 1) / 2) + 1; // in [1,4n]
			if (ip > nl4) ip = ip - nl4;
			ipix1 = ncap + nl4 * (ir - 1) + ip;

		} else { // North and South polar caps
			tp = tt - (int) tt;
			tmp = sqrt(3.0 * (1.0 - za));
			jp = (int) (nside * tp * tmp); // increasing edge line index
			jm = (int) (nside * (1.0 - tp) * tmp); // decreasing edge index

			ir = jp + jm + 1; // ring number counted from closest pole
			ip = (int) (tt * ir) + 1; // in [1,4*ir]
			if (ip > 4 * ir)
				ip = ip - 4 * ir;

			ipix1 = 2 * ir * (ir - 1) + ip;
			if (z <= 0.0)
				ipix1 = npix - 2 * ir * (ir + 1) + ip;

		}
		pix = ipix1 - 1; // in [0, npix-1]


		return pix;
	}


	/**
	 *
	 * Renders theta and phi coordinates of the normal pixel center for the
	 * pixel number ipix (NESTED scheme) given the map resolution parameter
	 * nside.
	 *
	 * @param nside
	 *            map resolution parameter - int
	 * @param ipix
	 *            int pixel number
	 * @return double[] (theta, phi)
	 * @throws IllegalArgumentException
	 */

	double* PixTools::pix2ang_nest(int nside, int ipix)  {
		double *res = new double[2];
		double theta = 0.;
		double phi = 0.;
		float pixmax = PixTools::pixmax;
		float halfpi = PixTools::HALFPI;
		int xmid = PixTools::xmid;
		int npix, npface, ipf, ip_low, ip_trunc, ip_med, ip_hi;
		int jrt, jr, nr, jpt, jp, kshift, nl4, ix, iy, face_num;
		double z, fn, fact1, fact2;
		string SID = "pix2ang_nest:";
		// coordinate of the lowest corner of each face
		int jrll[] = { 0, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 }; // in units of
		// nside
		int jpll[] = { 0, 1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7 }; // in units of
		// nside/2
		//                                                              //
		if (nside < 1 || nside > PixTools::ns_max) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		int nsidesq = nside * nside;
		npix = 12 * nsidesq;
		if (ipix < 0 || ipix > npix - 1) {
			cout<<SID<<" ipix out of range calculated from nside"<<endl;
			exit(-1);
		}
		if (memAl.pix2x[PixTools::xmax-1] <= 0)
			mk_pix2xy();
		fn = 1.*nside;
		fact1 = 1.0 / (3.0 * fn * fn);
		fact2 = 2.0 / (3.0 * fn);
		nl4 = 4 * nside;
		// findes the face, and the number in the face //
		npface = nside * nside;
		face_num = ipix / npface; // face number [0,11]
		ipf = (int) PixTools::MODULO((float)ipix, (float)npface); // pixel in the face [0, npface-1]
		//
		// finds x,y on the face (starting from the lowest corner) from pixel
		// number
		//
		ip_low = (int) PixTools::MODULO((float)ipf, pixmax);   // content of the last 18 bits
		ip_trunc =(int)(ipf / pixmax);                  // trancation of the last 18 bits
		ip_med = (int) PixTools::MODULO((float)ip_trunc, pixmax); // content of the next 18 bits
		ip_hi = (int)(ip_trunc / pixmax);                // content of the high wait 18 bits

		ix =(int)( pixmax * memAl.pix2x[(int)ip_hi] + xmid * memAl.pix2x[(int)ip_med] +
				memAl.pix2x[(int) ip_low]);
		iy = (int) (pixmax * memAl.pix2y[(int)ip_hi] + xmid * memAl.pix2y[(int)ip_med] +
				memAl.pix2y[(int)ip_low]);
		// transform these in (horizontal, vertical) coordinates //
		jrt = ix + iy; // [0,2*(nside-1)]
		jpt = ix - iy; // [ -nside+1, nside -1]
		// computes the z coordinate on the sphere //
		jr = jrll[(int) (face_num + 1)] * nside - jrt - 1; // ring number in [1,
		// 4*nside-1]

		nr = nside; // equatorial region (the most frequent )
		z = (2 * nside - jr) * fact2;
		kshift = (int) PixTools::MODULO((float)(jr - nside), 2.);
		if (jr < nside) { // north pole region
			nr = jr;
			z = 1.0 - nr * nr * fact1;
			kshift = 0;
		} else if (jr > 3 * nside) { // south pole region
			nr = nl4 - jr;
			z = -1.0 + nr * nr * fact1;
			kshift = 0;
		}
		theta = acos(z);
		// computes phi coordinate on the sphere, in [0,2pi] //
		jp = (jpll[(int) (face_num + 1)] * nr + jpt + 1 + kshift) / 2;
		if (jp > nl4)
			jp = jp - nl4;
		if (jp < 1)
			jp = jp + nl4;

		phi = (jp - (kshift + 1) * 0.5) * (halfpi / nr);
		res[0] = theta;
		res[1] = phi;
		return res;
	}

	/**
	 * renders vector (x,y,z) coordinates of the nominal pixel center for the
	 * pixel ipix (NESTED scheme ) given the map resolution parameter nside.
	 * Also calculates the (x,y,z) positions of 4 pixel vertices (corners) in
	 * the order N,W,S,E. These can be get using method pix2vertex_nest.
	 *
	 * @param nside the map resolution
	 * @param ipix int pixel number
	 * @return Vector3D
	 * @throws IllegalArgumentException
	 */

	Vector3D PixTools::pix2vect_nest(int nside, int ipix)  {

		PixInfo pixinfo = makePix2Vect_Nest(nside, ipix);
		return pixinfo.pixVect;
	}

	/**
	 * renders vector (x,y,z) coordinates of the nominal pixel center for the
	 * pixel ipix (NESTED scheme ) given the map resolution parameter nside.
	 * Also calculates the (x,y,z) positions of 4 pixel vertices (corners) in
	 * the order N,W,S,E.
	 *
	 * @param nside the map resolution
	 * @param ipix int pixel number
	 * @return double[3][4] 4 sets of vector components
	 * @throws IllegalArgumentException
	 */

	double** PixTools::pix2vertex_nest(int nside, int ipix) {
		double **res = new double*[4];
		PixInfo pixinfo = makePix2Vect_Nest(nside, ipix);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				res[i][j] = pixinfo.pixVertex[i][j];
			}
		}
		return res;
	}


	/**
	 * returns 7 or 8 neighbours of any pixel in the nested scheme The neighbours
	 * are ordered in the following way: First pixel is the one to the south (
	 * the one west of the south direction is taken for pixels that don't have a
	 * southern neighbour). From then on the neighbors are ordered in the
	 * clockwise direction.
	 *
	 * @param nside the map resolution
	 * @param ipix int pixel number
	 * @return ArrayList
	 * @throws IllegalArgumentException
	 */

	vector<int>* PixTools::neighbours_nest(int nside, int ipix)  {
		vector<int> *res = new vector<int>();
		int npix, ipf, ipo, ix, ixm, ixp, iy, iym, iyp, ixo, iyo;
		int face_num, other_face;
		int ia, ib, ibp, ibm, ib2,  nsidesq;
        int icase;
		int local_magic1, local_magic2;
		int arb_const = 0;
		int *ixiy; //= new int[2];
		int *ixoiyo; // = new int[2];
		string SID = "neighbours_nest:";
		// fill the pixel list with 0 //
		res->push_back(0);
		res->push_back(0);
		res->push_back(0);
		res->push_back(0);
		res->push_back(0);
		res->push_back(0);
		res->push_back(0);
		res->push_back(0);

		icase = 0;
		//                                 //
		if ((nside < 1) || (nside > PixTools::ns_max)) {
			cout<<SID<<" Nside should be power of 2 >0 and < "<<PixTools::ns_max<<endl;
			exit(-1);
		}
		nsidesq = nside * nside;
		npix = 12 * nsidesq; // total number of pixels
		if ((ipix < 0) || (ipix > npix - 1)) {
			cout<<SID<<" ipix out of range "<<endl;
		}
		if (memAl.x2pix[PixTools::xmax-1] <= 0)
			mk_xy2pix();

		local_magic1 = (nsidesq - 1) / 3;
		local_magic2 = 2 * local_magic1;
		face_num = ipix / nsidesq;
		ipf = (int) PixTools::MODULO(ipix, nsidesq); // Pixel number in face
		ixiy = pix2xy_nest(nside, ipf);
		ix = ixiy[0];
		iy = ixiy[1];
		//
		ixm = ixiy[0] - 1;
		ixp = ixiy[0] + 1;
		iym = ixiy[1] - 1;
		iyp = ixiy[1] + 1;

		icase = 0; // inside the face

		// exclude corners //
		if (ipf == local_magic2 && icase == 0)
			icase = 5; // West corner
		if (ipf == (nsidesq - 1) && icase == 0)
			icase = 6; // North corner
		if (ipf == 0 && icase == 0)
			icase = 7; // South corner
		if (ipf == local_magic1 && icase == 0)
			icase = 8; // East corner

		// detect edges //
		if ((ipf & local_magic1) == local_magic1 && icase == 0)
			icase = 1; // NorthEast
		if ((ipf & local_magic1) == 0 && icase == 0)
			icase = 2; // SouthWest
		if ((ipf & local_magic2) == local_magic2 && icase == 0)
			icase = 3; // NorthWest
		if ((ipf & local_magic2) == 0 && icase == 0)
			icase = 4; // SouthEast

		// iside a face //
		if (icase == 0) {
			res->operator [](0)=( xy2pix_nest(nside, ixm, iym, face_num));
			res->operator [](1)=( xy2pix_nest(nside, ixm, iy, face_num));
			res->operator [](2)=( xy2pix_nest(nside, ixm, iyp, face_num));
			res->operator [](3)=( xy2pix_nest(nside, ix, iyp, face_num));
			res->operator [](4)=( xy2pix_nest(nside, ixp, iyp, face_num));
			res->operator [](5)=( xy2pix_nest(nside, ixp, iy, face_num));
			res->operator [](6)=( xy2pix_nest(nside, ixp, iym, face_num));
			res->operator [](7)=( xy2pix_nest(nside, ix, iym, face_num));
		}
		//                 //
		ia = face_num / 4; // in [0,2]
		ib = (int) PixTools::MODULO(face_num, 4); // in [0,3]
		ibp = (int) PixTools::MODULO(ib + 1, 4);
		ibm = (int) PixTools::MODULO(ib + 4 - 1, 4);
		ib2 = (int) PixTools::MODULO(ib + 2, 4);

		if (ia == 0) { // North pole region
			switch (icase) {
			case 1: // north-east edge
				other_face = 0 + ibp;
				res->operator [](0)=( xy2pix_nest(nside, ixm, iym, face_num));
				res->operator [](1)=( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2)=( xy2pix_nest(nside, ixm, iyp, face_num));
				res->operator [](3)=( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](7)=( xy2pix_nest(nside, ix, iym, face_num));
				ipo = (int) PixTools::MODULO(PixTools::swapLSBMSB( ipf), nsidesq);
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](4)=( xy2pix_nest(nside, ixo + 1, iyo,other_face));
				res->operator [](5)=( (other_face * nsidesq + ipo));
				res->operator [](6)=( xy2pix_nest(nside, ixo - 1, iyo,other_face));
				delete(ixoiyo);
				break;
			case 2: // SouthWest edge
				other_face = 4 + ib;
				ipo = (int) PixTools::MODULO(PixTools::invLSB( ipf), nsidesq); // SW-NE flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0)=( xy2pix_nest(nside, ixo, iyo - 1,other_face));
				res->operator [](1)=( (other_face * nsidesq + ipo));
				res->operator [](2)=( xy2pix_nest(nside, ixo, iyo + 1,other_face));
				res->operator [](3)=( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](4)=( xy2pix_nest(nside, ixp, iyp, face_num));
				res->operator [](5)=( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6)=( xy2pix_nest(nside, ixp, iym, face_num));
				res->operator [](7)=( xy2pix_nest(nside, ix, iym, face_num));
				delete(ixoiyo);
				break;
			case 3: // NorthWest edge
				other_face = 0 + ibm;
				ipo = (int) PixTools::MODULO(PixTools::swapLSBMSB( ipf), nsidesq); // E-W flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0)=( xy2pix_nest(nside, ixm, iym, face_num));
				res->operator [](1)=( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2)=( xy2pix_nest(nside, ixo, iyo - 1,other_face));
				res->operator [](3)=( (other_face * nsidesq + ipo));
				res->operator [](4)=( xy2pix_nest(nside, ixo, iyo + 1,other_face));
				res->operator [](5)=( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6)=( xy2pix_nest(nside, ixp, iym, face_num));
				res->operator [](7)=( xy2pix_nest(nside, ix, iym, face_num));
				delete(ixoiyo);
				break;
			case 4: // SouthEast edge
				other_face = 4 + ibp;
				ipo = (int) PixTools::MODULO(PixTools::invMSB( ipf), nsidesq); // SE-NW flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0)=( xy2pix_nest(nside, ixo - 1, iyo,other_face));
				res->operator [](1)=( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2)=( xy2pix_nest(nside, ixm, iyp, face_num));
				res->operator [](3)=( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](4)=( xy2pix_nest(nside, ixp, iyp, face_num));
				res->operator [](5)=( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6)=( xy2pix_nest(nside, ixo + 1, iyo,other_face));
				res->operator [](7)=( (other_face * nsidesq + ipo));
				delete(ixoiyo);
				break;
			case 5: // West corner
				other_face = 4 + ib;
				arb_const = other_face * nsidesq + nsidesq - 1;
				res->operator [](0)=( (arb_const - 2));
				res->operator [](1)=(  arb_const);
				other_face = 0 + ibm;
				arb_const = other_face * nsidesq + local_magic1;
				res->operator [](2)=(  arb_const);
				res->operator [](3)=( arb_const + 2);
				res->operator [](4)=(  (ipix + 1));
				res->operator [](5)=(  (ipix - 1));
				res->operator [](6)=( (ipix - 2));
				res->erase (res->begin()+6);       // remove element 7 of the vector

				break;
			case 6: //  North corner
				other_face = 0 + ibm;
				res->operator [](0)= (ipix - 3);
				res->operator [](1)= (ipix - 1);
				arb_const = other_face * nsidesq + nsidesq - 1;
				res->operator [](2)= (arb_const - 2);
				res->operator [](3)= ( arb_const);
				other_face = 0 + ib2;
				res->operator [](4)= (other_face * nsidesq + nsidesq - 1);
				other_face = 0 + ibp;
				arb_const = other_face * nsidesq + nsidesq - 1;
				res->operator [](5)= ( arb_const);
				res->operator [](6)= (arb_const - 1);
				res->operator [](7)= (ipix - 2);
				break;
			case 7: // South corner
				other_face = 8 + ib;
				res->operator [](0)= (other_face * nsidesq + nsidesq - 1);
				other_face = 4 + ib;
				arb_const = other_face * nsidesq + local_magic1;
				res->operator [](1)= ( arb_const);
				res->operator [](2)= (arb_const + 2);
				res->operator [](3)= (ipix + 2);
				res->operator [](4) =(ipix + 3);
				res->operator [](5) = (ipix + 1);
				other_face = 4 + ibp;
				arb_const = other_face * nsidesq + local_magic2;
				res->operator [](6)= (arb_const + 1);
				res->operator [](7)= ( arb_const);
				break;
			case 8: // East corner
				other_face = 0 + ibp;
				res->operator [](1)= (ipix - 1);
				res->operator [](2)=(ipix + 1);
				res->operator [](3)= (ipix + 2);
				arb_const = other_face * nsidesq + local_magic2;
				res->operator [](4)= (arb_const + 1);
				res->operator [](5)= ( arb_const);
				other_face = 4 + ibp;
				arb_const = other_face * nsidesq + nsidesq - 1;
				res->operator [](0)=  (arb_const - 1);
				res->operator [](6)= ( arb_const);
				res->erase (res->begin()+6);
				break;
			}
		} else if (ia == 1) { // Equatorial region
			switch (icase) {
			case 1: // north-east edge
				other_face = 0 + ibp;
				res->operator [](0)= ( xy2pix_nest(nside, ixm, iym, face_num));
				res->operator [](1)= ( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2)= ( xy2pix_nest(nside, ixm, iyp, face_num));
				res->operator [](3)= ( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](7)= ( xy2pix_nest(nside, ix, iym, face_num));
				ipo = (int) PixTools::MODULO(PixTools::invLSB( ipf), nsidesq); // NE-SW flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](4) = ( xy2pix_nest(nside, ixo, iyo + 1,other_face));
				res->operator [](5) =( (other_face * nsidesq + ipo));
				res->operator [](6)= ( xy2pix_nest(nside, ixo, iyo - 1,other_face));
				delete(ixoiyo);
				break;
			case 2: // SouthWest edge
				other_face = 8 + ibm;
				ipo = (int) PixTools::MODULO(PixTools::invLSB( ipf), nsidesq); // SW-NE flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0)= ( xy2pix_nest(nside, ixo, iyo - 1,other_face));
				res->operator [](1) =((other_face * nsidesq + ipo));
				res->operator [](2) = ( xy2pix_nest(nside, ixo, iyo + 1,other_face));
				res->operator [](3) =( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](4) =( xy2pix_nest(nside, ixp, iyp, face_num));
				res->operator [](5) =( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6) = ( xy2pix_nest(nside, ixp, iym, face_num));
				res->operator [](7) =( xy2pix_nest(nside, ix, iym, face_num));
				delete(ixoiyo);
				break;
			case 3: // NortWest edge
				other_face = 0 + ibm;
				ipo = (int) PixTools::MODULO(PixTools::invMSB( ipf), nsidesq); // NW-SE flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](2) = ( xy2pix_nest(nside, ixo - 1, iyo,other_face));
				res->operator [](3) =( (other_face * nsidesq + ipo));
				res->operator [](4) = ( xy2pix_nest(nside, ixo + 1, iyo,other_face));
				res->operator [](0) = ( xy2pix_nest(nside, ixm, iym, face_num));
				res->operator [](1) = ( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](5) = ( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6) = ( xy2pix_nest(nside, ixp, iym, face_num));
				res->operator [](7) = (xy2pix_nest(nside, ix, iym, face_num));
				delete(ixoiyo);
				break;
			case 4: // SouthEast edge
				other_face = 8 + ib;
				ipo = (int) PixTools::MODULO(PixTools::invMSB( ipf), nsidesq); // SE-NW flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0) = ( xy2pix_nest(nside, ixo - 1, iyo,other_face));
				res->operator [](1) = ( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2) = ( xy2pix_nest(nside, ixm, iyp, face_num));
				res->operator [](3) = ( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](4) = ( xy2pix_nest(nside, ixp, iyp, face_num));
				res->operator [](5) = ( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6) = ( xy2pix_nest(nside, ixo + 1, iyo,other_face));
				res->operator [](7) = ( (other_face * nsidesq + ipo));
				delete(ixoiyo);
				break;
			case 5: // West corner
				other_face = 8 + ibm;
				arb_const = other_face * nsidesq + nsidesq - 1;
				res->operator [](0) = ( (arb_const - 2));
				res->operator [](1) = ( arb_const);
				other_face = 4 + ibm;
				res->operator [](2) = ( (other_face * nsidesq + local_magic1));
				other_face = 0 + ibm;
				arb_const = other_face * nsidesq;
				res->operator [](3) = ( arb_const);
				res->operator [](4) = ( (arb_const + 1));
				res->operator [](5) = ( (ipix + 1));
				res->operator [](6) = ( (ipix - 1));
				res->operator [](7) = ( (ipix - 2));
				break;
			case 6: //  North corner
				other_face = 0 + ibm;
				res->operator [](0) = ( (ipix - 3));
				res->operator [](1) = ( (ipix - 1));
				arb_const = other_face * nsidesq + local_magic1;
				res->operator [](2) = ( (arb_const - 1));
				res->operator [](3) = ( arb_const);
				other_face = 0 + ib;
				arb_const = other_face * nsidesq + local_magic2;
				res->operator [](4) =  ( arb_const);
				res->operator [](5) = ( (arb_const - 2));
				res->operator [](6) = ( (ipix - 2));
				res->erase (res->begin()+6);
				break;
			case 7: // South corner
				other_face = 8 + ibm;
				arb_const = other_face * nsidesq + local_magic1;
				res->operator [](0) = ( arb_const);
				res->operator [](1) = ( (arb_const + 2));
				res->operator [](2) = ( (ipix + 2));
				res->operator [](3) = ( (ipix + 3));
				res->operator [](4) = ( (ipix + 1));
				other_face = 8 + ib;
				arb_const = other_face * nsidesq + local_magic2;
				res->operator [](5) = ( (arb_const + 1));
				res->operator [](6) = ( arb_const);
				res->erase (res->begin()+6);
				break;
			case 8: // East corner
				other_face = 8 + ib;
				arb_const = other_face * nsidesq + nsidesq - 1;
				res->operator [](0) = ( (arb_const - 1));
				res->operator [](1) = ( (ipix - 1));
				res->operator [](2) = ( (ipix + 1));
				res->operator [](3) = ( (ipix + 2));
				res->operator [](7) = ( arb_const);
				other_face = 0 + ib;
				arb_const = other_face * nsidesq;
				res->operator [](4) = ( (arb_const + 2));
				res->operator [](5) = ( arb_const);
				other_face = 4 + ibp;
				res->operator [](6) = ( (other_face * nsidesq + local_magic2));
				break;
			}
		} else { // South pole region
			switch (icase) {
			case 1: // North-East edge
				other_face = 4 + ibp;
				res->operator [](0) = ( xy2pix_nest(nside, ixm, iym, face_num));
				res->operator [](1) = ( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2) = ( xy2pix_nest(nside, ixm, iyp, face_num));
				res->operator [](3) = ( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](7) = ( xy2pix_nest(nside, ix, iym, face_num));
				ipo = (int) PixTools::MODULO(PixTools::invLSB( ipf), nsidesq); // NE-SW flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](4) = ( xy2pix_nest(nside, ixo, iyo + 1,other_face));
				res->operator [](5) = ( (other_face * nsidesq + ipo));
				res->operator [](6) = ( xy2pix_nest(nside, ixo, iyo - 1,other_face));
				delete(ixoiyo);
				break;
			case 2: // SouthWest edge
				other_face = 8 + ibm;
				ipo = (int) PixTools::MODULO(PixTools::swapLSBMSB( ipf), nsidesq); // W-E flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0) = ( xy2pix_nest(nside, ixo - 1, iyo,other_face));
				res->operator [](1) = ( (other_face * nsidesq + ipo));
				res->operator [](2) = ( xy2pix_nest(nside, ixo + 1, iyo,other_face));
				res->operator [](3) = ( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](4) = ( xy2pix_nest(nside, ixp, iyp, face_num));
				res->operator [](5) = ( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6) = (xy2pix_nest(nside, ixp, iym, face_num));
				res->operator [](7) = ( xy2pix_nest(nside, ix, iym, face_num));
				delete(ixoiyo);
				break;
			case 3: // NorthWest edge
				other_face = 4 + ib;
				ipo = (int) PixTools::MODULO(PixTools::invMSB( ipf), nsidesq);
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0) = ( xy2pix_nest(nside, ixm, iym, face_num));
				res->operator [](1) = ( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2) = ( xy2pix_nest(nside, ixo - 1, iyo,other_face));
				res->operator [](3) = ( (other_face * nsidesq + ipo));
				res->operator [](4) = ( xy2pix_nest(nside, ixo + 1, iyo,other_face));
				res->operator [](5) = ( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6) = ( xy2pix_nest(nside, ixp, iym, face_num));
				res->operator [](7) = ( xy2pix_nest(nside, ix, iym, face_num));
				delete(ixoiyo);
				break;
			case 4: // SouthEast edge
				other_face = 8 + ibp;
				ipo = (int) PixTools::MODULO(PixTools::swapLSBMSB( ipf), nsidesq); // SE-NW
				// flip
				ixoiyo = pix2xy_nest(nside, ipo);
				ixo = ixoiyo[0];
				iyo = ixoiyo[1];
				res->operator [](0) = ( xy2pix_nest(nside, ixo, iyo - 1,other_face));
				res->operator [](1) = ( xy2pix_nest(nside, ixm, iy, face_num));
				res->operator [](2) = ( xy2pix_nest(nside, ixm, iyp, face_num));
				res->operator [](3) = ( xy2pix_nest(nside, ix, iyp, face_num));
				res->operator [](4) = ( xy2pix_nest(nside, ixp, iyp, face_num));
				res->operator [](5) = ( xy2pix_nest(nside, ixp, iy, face_num));
				res->operator [](6) = ( xy2pix_nest(nside, ixo, iyo + 1,other_face));
				res->operator [](7) = ( (other_face * nsidesq + ipo));
				delete(ixoiyo);
				break;
			case 5: // West corner
				other_face = 8 + ibm;
				arb_const = other_face * nsidesq + local_magic1;
				res->operator [](0) = ( (arb_const - 2));
				res->operator [](1) = ( arb_const);
				other_face = 4 + ib;
				res->operator [](2) = ( (other_face * nsidesq));
				res->operator [](3) = ( (other_face * nsidesq + 1));
				res->operator [](4) = ( (ipix + 1));
				res->operator [](5) = ( (ipix - 1));
				res->operator [](6) = ( (ipix - 2));
				res->erase (res->begin()+6);
				break;
			case 6: //  North corner
				other_face = 4 + ib;
				res->operator [](0) = ( (ipix - 3));
				res->operator [](1) = ((ipix - 1));
				arb_const = other_face * nsidesq + local_magic1;
				res->operator [](2) = ( (arb_const - 1));
				res->operator [](3) = ( arb_const);
				other_face = 0 + ib;
				res->operator [](4) = ( (other_face * nsidesq));
				other_face = 4 + ibp;
				arb_const = other_face * nsidesq + local_magic2;
				res->operator [](5) = ( arb_const);
				res->operator [](6) = ( (arb_const - 2));
				res->operator [](7) = ( (ipix - 2));
				break;
			case 7: // South corner
				other_face = 8 + ib2;
				res->operator [](0) = ( (other_face * nsidesq));
				other_face = 8 + ibm;
				arb_const = other_face * nsidesq;
				res->operator [](1) = ( arb_const);
				res->operator [](2) = ( (arb_const + 1));
				res->operator [](3) = ( (ipix + 2));
				res->operator [](4) = ( (ipix + 3));
				res->operator [](5) = ( (ipix + 1));
				other_face = 8 + ibp;
				arb_const = other_face * nsidesq;
				res->operator [](6) = ( (arb_const + 2));
				res->operator [](7) = ( arb_const);
				break;
			case 8: // East corner
				other_face = 8 + ibp;
				res->operator [](1) = ( (ipix - 1));
				res->operator [](2) = ( (ipix + 1));
				res->operator [](3) = ( (ipix + 2));
				arb_const = other_face * nsidesq + local_magic2;
				res->operator [](6) = ( arb_const);
				res->operator [](0) = ( (arb_const - 2));
				other_face = 4 + ibp;
				arb_const = other_face * nsidesq;
				res->operator [](4) = ( (arb_const + 2));
				res->operator [](5) = ( arb_const);
				res->erase(res->begin()+6);
				break;
			}
		}
		delete(ixiy);
		return res;
	}











 void	PixTools::setUp() {
		mk_xy2pix();
		mk_pix2xy();

	}

PixTools::~PixTools() {
	}
