/* The input file has to be the output .matches file from coadd_grabmatches */


#include "imageproc.h" 

#define MAGERRCUT 0.1
#define ACCURACY 1e-6 /* accuracy in the sigma clipping algorithm */

void iter_mean(float data[], float err[], int N, float *mean, float *rms, int *N_good, int *N_bad, int *N_iter, int flag, int flag_iter, int flag_Niterate, int Nmax_iterate, float THRESHOLD);

void bindata(float x[], float y[], float yerr[], int N, float binsize, int flag_quiet, int flag_output, char *outfile);

float getmean(float data[],float err[],int N,int flag);
float getrms(float data[],float err[],float mean,int N,int flag);
float getmedian(float data[],int N);

main(int argc, char *argv[])
{
  char infile[1000],command[1000],outroot[1000],outfile[1000];
  
  float temp;
  float magg,magr,magi,magz;
  float magerrg,magerrr,magerri,magerrz;
  float *tmagg,*tmagr,*tmagi,*tmagz;
  float tmagg_in,tmagr_in,tmagi_in,tmagz_in;
  float tmagerrg,tmagerrr,tmagerri,tmagerrz;
  float *dmagg,*dmagr,*dmagi,*dmagz;
  float *dmagerrg,*dmagerrr,*dmagerri,*dmagerrz;
  float binsize,magstart,magstop,sigma;
  float meang,meanr,meani,meanz;
  float iter_meang,iter_meanr,iter_meani,iter_meanz;
  float iter_rmsg,iter_rmsr,iter_rmsi,iter_rmsz;
  float mediang,medianr,mediani,medianz;
  int i,N,itemp;
  int countg,countr,counti,countz;
  int N_goodg,N_badg,N_iterg;
  int N_goodr,N_badr,N_iterr;
  int N_goodi,N_badi,N_iteri;
  int N_goodz,N_badz,N_iterz;
  int Nmax_iterate;
  int flag_quiet=0,flag_output=0,flag_magrange=0,flag_Niterate=0;

  void sort3();

  FILE *pip,*fin;

  if(argc<2) {
    printf("Usage: %s <output .matches file from coadd_grabmatches>\n",argv[0]);
    printf("\t\tOption:\n");
    printf("\t\t         -binsize <bin_size> (default: 0.5)\n");
    printf("\t\t         -outroot <output rootname>\n");
    printf("\t\t         -magrange <lower_mag> <upper_mag>\n");
    printf("\t\t         -sigmaclip <sigma> (default: 2.5)\n");
    printf("\t\t         -Nitermax <Niter_max> (default: 10)\n");					
    printf("\t\t         -quiet\n");
    exit(0);
  }
  if(!strcmp(argv[i],"-quiet"))
      flag_quiet=1;
  sprintf(infile,"%s",argv[1]);
  
  /* default value */
  binsize=0.5;
  sigma=2.5;
  Nmax_iterate=10;

  /* options */
  for(i=1;i<argc;i++) {
    if(!strcmp(argv[i],"-binsize"))
      sscanf(argv[i+1],"%f",&binsize);

    if (!strcmp(argv[i],"-sigmaclip")) 
      sscanf(argv[i+1],"%f",&sigma);

    if (!strcmp(argv[i],"-Nitermax")) {
      flag_Niterate=1;
      Nmax_iterate=atoi(argv[i+1]);
    }

    if(!strcmp(argv[i],"-quiet"))
      flag_quiet=1;

    if(!strcmp(argv[i],"-outroot")) {
      flag_output=1;
      sprintf(outroot,"%s",argv[i+1]);
    }

    if(!strcmp(argv[i],"-magrange")) {
      flag_magrange=1;
      sscanf(argv[i+1],"%f",&magstart);
      sscanf(argv[i+2],"%f",&magstop);
    }
  }

  /* count the number of line in infile */
  sprintf(command,"wc -l %s",infile);
  pip=popen(command,"r");
  if(pip==NULL) {
    printf(" %s Error: input file %s not found\n",infile);
    exit(0);
  }
  else {
    fscanf(pip,"%d",&N);
    pclose(pip);
  }

  if(N) {
    if(!flag_quiet)
      printf(" -- Number of data in file %s: %d\n",infile,N);
  }
  else {
    printf(" %s Error: input file %s empty\n",argv[0],infile);
    exit(0);
  }

  /* memory allocation */
  tmagg=(float *)calloc(N,sizeof(float));
  tmagr=(float *)calloc(N,sizeof(float));
  tmagi=(float *)calloc(N,sizeof(float));
  tmagz=(float *)calloc(N,sizeof(float));

  dmagg=(float *)calloc(N,sizeof(float));
  dmagr=(float *)calloc(N,sizeof(float));
  dmagi=(float *)calloc(N,sizeof(float));
  dmagz=(float *)calloc(N,sizeof(float));

  dmagerrg=(float *)calloc(N,sizeof(float));
  dmagerrr=(float *)calloc(N,sizeof(float));
  dmagerri=(float *)calloc(N,sizeof(float));
  dmagerrz=(float *)calloc(N,sizeof(float));

  /* initialize the arrays */
  for(i=0;i<N;i++) {
    tmagg[i]=tmagr[i]=tmagi[i]=tmagz[i]=0.0;
    dmagg[i]=dmagr[i]=dmagi[i]=dmagz[i]=0.0;
    dmagerrg[i]=dmagerrr[i]=dmagerri[i]=dmagerrz[i]=0.0;
  }

  /* input the data */
  countg=countr=counti=countz=0;
  fin=fopen(infile,"r");
  for(i=0;i<N;i++) {
    fscanf(fin,"%f %f %d %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
	   &temp,&temp,&itemp,&temp,&temp,&magg,&magerrg,&temp,&magr,&magerrr,&temp,&magi,&magerri,&temp,&magz,&magerrz,&temp,&temp,&temp,&temp,&temp,&tmagg_in,&tmagerrg,&tmagr_in,&tmagerrr,&tmagi_in,&tmagerri,&tmagz_in,&tmagerrz);

    if(magerrg<MAGERRCUT && tmagerrg<MAGERRCUT) { 
      
      if(flag_magrange) {
	if(magg >= magstart && magg <magstop) {
	  if(fabs(magg-tmagg_in)<5.0) {
	    tmagg[countg]=tmagg_in;
	    dmagg[countg]=magg-tmagg_in;
	    dmagerrg[countg]=sqrt(Squ(magerrg)+Squ(tmagerrg));
	    countg++;
	  }
	}
      }
      else {
	if(fabs(magg-tmagg_in)<5.0) {
	  tmagg[countg]=tmagg_in;
	  dmagg[countg]=magg-tmagg_in;
	  dmagerrg[countg]=sqrt(Squ(magerrg)+Squ(tmagerrg));
	  countg++;
	}
      }
    }

    if(magerrr<MAGERRCUT && tmagerrr<MAGERRCUT) { 

      if(flag_magrange) {
	if(magr >= magstart && magr < magstop) {
	  if(fabs(magr-tmagr_in)<5.0) {
	    tmagr[countr]=tmagr_in;
	    dmagr[countr]=magr-tmagr_in;
	    dmagerrr[countr]=sqrt(Squ(magerrr)+Squ(tmagerrr));
	    countr++;
	  }
	}
      }
      else {
	if(fabs(magr-tmagr_in)<5.0) {
	  tmagr[countr]=tmagr_in;
	  dmagr[countr]=magr-tmagr_in;
	  dmagerrr[countr]=sqrt(Squ(magerrr)+Squ(tmagerrr));
	  countr++;
	}
      }
    }

    if(magerri<MAGERRCUT && tmagerri<MAGERRCUT) { 

      if(flag_magrange) {
	if(magi >= magstart && magi < magstop) {
	  if(fabs(magi-tmagi_in)<5.0) {
	    tmagi[counti]=tmagi_in;
	    dmagi[counti]=magi-tmagi_in;
	    dmagerri[counti]=sqrt(Squ(magerri)+Squ(tmagerri));
	    counti++;
	  }
	}
      }
      else {
	if(fabs(magi-tmagi_in)<5.0) {
	  tmagi[counti]=tmagi_in;
	  dmagi[counti]=magi-tmagi_in;
	  dmagerri[counti]=sqrt(Squ(magerri)+Squ(tmagerri));
	  counti++;
	}
      }
    }

    if(magerrz<MAGERRCUT && tmagerrz<MAGERRCUT) { 

      if(flag_magrange) {
	if(magz >= magstart && magz < magstop) {
	  if(fabs(magz-tmagz_in)<5.0) {
	    tmagz[countz]=tmagz_in;
	    dmagz[countz]=magz-tmagz_in;
	    dmagerrz[countz]=sqrt(Squ(magerrz)+Squ(tmagerrz));
	    countz++;
	  }
	}
      }
      else {
	if(fabs(magz-tmagz_in)<5.0) {
	  tmagz[countz]=tmagz_in;
	  dmagz[countz]=magz-tmagz_in;
	  dmagerrz[countz]=sqrt(Squ(magerrz)+Squ(tmagerrz));
	  countz++;
	}
      }
    }
    
  }
  fclose(fin);
  
  /* calculate the median */
  mediang=getmedian(dmagg,countg);
  medianr=getmedian(dmagr,countr);
  mediani=getmedian(dmagi,counti);
  medianz=getmedian(dmagz,countz);

  /* calculate the mean for all (filtered) data */
  meang=getmean(dmagg,dmagerrg,countg,1);
  meanr=getmean(dmagr,dmagerrr,countr,1);
  meani=getmean(dmagi,dmagerri,counti,1);
  meanz=getmean(dmagz,dmagerrz,countz,1);
  
  /* calculate the mean from sigma clipping */
  iter_mean(dmagg,dmagerrg,countg,&iter_meang,&iter_rmsg,&N_goodg,&N_badg,&N_iterg,1,1,flag_Niterate,Nmax_iterate,sigma);
  iter_mean(dmagr,dmagerrr,countr,&iter_meanr,&iter_rmsr,&N_goodr,&N_badr,&N_iterr,1,1,flag_Niterate,Nmax_iterate,sigma);
  iter_mean(dmagi,dmagerri,counti,&iter_meani,&iter_rmsi,&N_goodi,&N_badi,&N_iteri,1,1,flag_Niterate,Nmax_iterate,sigma);
  iter_mean(dmagz,dmagerrz,countz,&iter_meanz,&iter_rmsz,&N_goodz,&N_badz,&N_iterz,1,1,flag_Niterate,Nmax_iterate,sigma);

  printf(" -- gband straight mean = %2.4f\tRMS = %2.4f\tN = %d\n",meang,getrms(dmagg,dmagerrg,meang,countg,1),countg);
  printf(" -- gband median        = %2.4f\tRMS = %2.4f\tN = %d\n",mediang,getrms(dmagg,dmagerrg,mediang,countg,1),countg);
  printf(" -- gband weighted mean = %2.4f\tRMS = %2.4f\tN_good = %d N_bad = %d N_iter = %d\n\n",iter_meang,iter_rmsg,N_goodg,N_badg,N_iterg);

  printf(" -- rband straight mean = %2.4f\tRMS = %2.4f\tN = %d\n",meanr,getrms(dmagr,dmagerrr,meanr,countr,1),countr);
  printf(" -- rband median        = %2.4f\tRMS = %2.4f\tN = %d\n",medianr,getrms(dmagr,dmagerrr,medianr,countr,1),countr);
  printf(" -- rband weighted mean = %2.4f\tRMS = %2.4f\tN_good = %d N_bad = %d N_iter = %d\n\n",iter_meanr,iter_rmsr,N_goodr,N_badr,N_iterr);

  printf(" -- iband straight mean = %2.4f\tRMS = %2.4f\tN = %d\n",meani,getrms(dmagi,dmagerri,meani,counti,1),counti);
  printf(" -- iband median        = %2.4f\tRMS = %2.4f\tN = %d\n",mediani,getrms(dmagi,dmagerri,mediani,counti,1),counti);
  printf(" -- iband weighted mean = %2.4f\tRMS = %2.4f\tN_good = %d N_bad = %d N_iter = %d\n\n",iter_meani,iter_rmsi,N_goodi,N_badi,N_iteri);

  printf(" -- zband straight mean = %2.4f\tRMS = %2.4f\tN = %d\n",meanz,getrms(dmagz,dmagerrz,meanz,countz,1),countz);
  printf(" -- zband median        = %2.4f\tRMS = %2.4f\tN = %d\n",medianz,getrms(dmagz,dmagerrz,medianz,countz,1),countz);
  printf(" -- zband weighted mean = %2.4f\tRMS = %2.4f\tN_good = %d N_bad = %d N_iter = %d\n\n",iter_meanz,iter_rmsz,N_goodz,N_badz,N_iterz);


  /* sort the data according to mag_standard */
  sort3((unsigned long)countg,tmagg-1,dmagg-1,dmagerrg-1);
  sort3((unsigned long)countr,tmagr-1,dmagr-1,dmagerrr-1);
  sort3((unsigned long)counti,tmagi-1,dmagi-1,dmagerri-1);
  sort3((unsigned long)countz,tmagz-1,dmagz-1,dmagerrz-1);
  
  /* bin the data and calculate mean/RMS in each bins */
  if(!flag_quiet) {
    printf(" -- For g band:\n");
    sprintf(outfile,"%s_g.dat",outroot);
    bindata(tmagg,dmagg,dmagerrg,countg,binsize,flag_quiet,flag_output,outfile);
    printf(" -- For r band:\n");
    sprintf(outfile,"%s_r.dat",outroot);
    bindata(tmagr,dmagr,dmagerrr,countr,binsize,flag_quiet,flag_output,outfile);
    printf(" -- For i band:\n");
    sprintf(outfile,"%s_i.dat",outroot);
    bindata(tmagi,dmagi,dmagerri,counti,binsize,flag_quiet,flag_output,outfile);
    printf(" -- For z band:\n");
    sprintf(outfile,"%s_z.dat",outroot);
    bindata(tmagz,dmagz,dmagerrz,countz,binsize,flag_quiet,flag_output,outfile);
  }

  /* free memory */
  free(tmagg); free(tmagr); free(tmagi); free(tmagz);
  free(dmagg); free(dmagr); free(dmagi); free(dmagz);
  free(dmagerrg); free(dmagerrr); free(dmagerri); free(dmagerrz);
}

float getmean(float data[],float err[],int N,int flag)
{
  int i;
  float sum,mean,sigsq;

  mean=0.0;
  sum=0.0;
  sigsq=0.0;
  for(i=0;i<N;i++) {
      sum+=data[i];
      sigsq+=1.0/(err[i]*err[i]);
  }

  if(flag==0)  /* unweighted average */  
    mean=sum/N;
  else {  /* weighted average */  
    sum = 0.0;
    for(i=0;i<N;i++)
      sum += data[i]/(err[i]*err[i]);
    mean = sum/sigsq;
  }

  return (mean);
}

float getrms(float data[],float err[],float mean,int N,int flag)
{
  int i;
  float var,s,ep,rms;

  //mean=getmean(data,err,N,flag);

  /* using two-pass formula as given in Numerical Recipes 14.1.8 */
  if(N>1) {
    var=ep=0.0;
    for(i=0;i<N;i++) { 
      s=data[i]-mean;
      ep+=s;
      var+=s*s;
    }
    rms=sqrt((var-ep*ep/N)/(float)(N-1));
  }
  else
    rms=0.0;

  return (rms);
}

float getmedian(float data[],int N)
{
  float *vecsort,median;
  int i;
  unsigned long n;
  void shell();

  n=(unsigned long)N;
  vecsort=(float *)calloc(N+1,sizeof(float));
  
  for(i=1;i<=N;i++)
    vecsort[i]=data[i-1];
  
  /* sort with N.R. subroutine */
  shell(n,vecsort);

  /* get median value */
  if(n%2)  median = vecsort[(n+1)/2];
  else median = 0.5 * (vecsort[n/2] + vecsort[(n/2)+1]);

  free(vecsort);
  return (median);
}



void iter_mean(float data[], float err[], int N, float *mean, float *rms, int *N_good, int *N_bad, int *N_iter, int flag, int flag_iter, int flag_Niterate, int Nmax_iterate, float THRESHOLD)
{
  int ii,Nt,Noutlier=0;
  float t_mean,t_rms,new_mean,old_mean;
  float *olddata, *olderr;
  float *newdata, *newerr;
  
  *N_good=N;
  *N_bad=0;
  *N_iter=0;
  
  /* first find the sample average and  rms */
  t_mean=getmean(data,err,N,flag);
  t_rms=getrms(data,err,t_mean,N,flag);

  /* memory allocation for the newdata and newerr */
  olddata=(float *)calloc(N+1,sizeof(float));
  olderr =(float *)calloc(N+1,sizeof(float));
  newdata=(float *)calloc(N+1,sizeof(float));
  newerr =(float *)calloc(N+1,sizeof(float));

  /* first find if any outliers when doing sigma-clipping */
  for(ii=0;ii<N;ii++) {
    if((fabs(data[ii]-t_mean) > THRESHOLD*t_rms)) 
      Noutlier++;
  }

  if(Noutlier==0 || flag_iter==0) { /* no outliers or not using sigma-clipping, simply return the results */
    *mean=t_mean;
    *rms=t_rms;
  }
  else { /* remove outliers and begin iterative process */

    /* initiallize the olddata array and parameters */
    for(ii=0;ii<N;ii++) {
      olddata[ii]=data[ii];
      olderr[ii]=err[ii];
    }
    old_mean=t_mean;
    new_mean=0.0;
    Nt = N;
  
    /* iterative procedure until the mean converge */
    while(fabs(old_mean-new_mean) >= ACCURACY) {

      /* get the mean and rms for the old data */
      t_mean=getmean(olddata,olderr,Nt,flag);
      t_rms =getrms(olddata,olderr,t_mean,Nt,flag);
      old_mean=t_mean;

      /* find the number of outliers and put data in new array without the outlier */
      *N_good=0;
      for(ii=0;ii<Nt;ii++) {
	if((fabs(olddata[ii]-t_mean) > THRESHOLD*t_rms)) 
	  *N_bad+=1;
	else {
	  newdata[*N_good]=olddata[ii];
	  newerr[*N_good]=olderr[ii];
	  *N_good+=1;
	}
      }

      /* update information */
      if(Nt != (*N_good))
	*N_iter+=1;

      Nt = (*N_good);

      t_mean=getmean(newdata,newerr,Nt,flag);
      t_rms =getrms(newdata,newerr,t_mean,Nt,flag);      
      new_mean = t_mean;

      /* put the newdata array back to olddata array */
      for(ii=0;ii<Nt;ii++) {
	olddata[ii]=newdata[ii];
	olderr[ii]=newerr[ii];
      }
     
      /* quick the loop if set the Nmax_iterate */
      if(flag_Niterate && *N_iter==Nmax_iterate) break;
    }

    if((*N_good)==0 || (*N_bad)==N) {
      t_mean=0.0;
      t_rms=0.0;
    }

    *mean=t_mean;
    *rms=t_rms;
  }

  /* free memory */
  free(olddata);free(olderr);
  free(newdata);free(newerr);
}

void bindata(float x[], float y[], float yerr[], int N, float binsize, int flag_quiet, int flag_output, char *outfile)
{
  int i,j,k,count,Nbin,flag,total,istart,istop;
  float binstart,binstop,binmidpt;
  float *y_in,*yerr_in;
  float mean;

  FILE *fout;

  Nbin=0; count=0; total=0; istart=1;
  binstart=x[0];
  binstop=binstart+binsize;
  
  if(flag_output)
    fout=fopen(outfile,"w");

  for(i=0;i<N;i++) {
   
    if(x[i]>=binstart && x[i]<binstop) {
      count++;
      flag=0;
      //printf(" in bin %d\n",Nbin+1);
    }
    else flag=1;

    if(i==N-1) flag=1;

    if(flag) {     
      Nbin++;
      if(i==N-1) istop=i+1;
      else istop=i;
      
      /* memory allocation */
      y_in=(float *)calloc(count,sizeof(float));
      yerr_in=(float *)calloc(count,sizeof(float));
      
      k=0;
      for(j=istart-1;j<istop;j++) {
	y_in[k]=y[j];
	yerr_in[k]=yerr[j];
	k++;
      }

      mean=getmean(y_in,yerr_in,count,1);
      
      /* output result */
      if(!flag_quiet) {
	printf("Bin %0d \t%2.4f - %2.4f  midpt = %2.4f\tN=%d\t",Nbin,binstart,binstop,0.5*(binstart+binstop),count);
	printf("mean = %2.4f\tRMS = %2.4f\n",mean,getrms(y_in,yerr_in,mean,count,1));
      }
      if(flag_output) 
	fprintf(fout,"%2.4f\t%2.4f\t%2.4f\n",0.5*(binstart+binstop),mean,getrms(y_in,yerr_in,mean,count,1));
	
      /* re-initialize */
      binstart=binstop;
      binstop=binstart+binsize;
      total+=count;
      count=1;
      istart=i+1;

      /* free memory */
      free(y_in); free(yerr_in);
    }

  }
  
  if(flag_output)
    fclose(fout);
}

#undef MAGERRCUT
#undef ACCURACY
