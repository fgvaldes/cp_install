/*--main 

Basic syntax: get_corners_from_image  <image> <options>
  Input data
    <image>   = FITS image with an astrometric solution 
  Options:
    -update   
    -verbose [0-3]

Summary:
  Calculates the coordinates of image corners and center based on WCS header
  using Terapix wcs library routines.  An option is provided that updates the 
  header of the image with standard NSA archive keywords: COR1RA(1-4) and
  COR1DE(1-4) for the corners, RA1 and DEC1 for the image center.  

Detailed Description:
  -update 
  Sets flag that causes the primary HDU of the image to have the COR1 keywords 
  and RA1,DEC1 keywords addded or updated with values for the corners.

Known Issues:
  - TPV convention is supported however the old format where a TAN projection
    accompanied by a set of PV keywords may not produce a correct result.
  - Does not work on fpack'd files.
  - This code uses structures from both the imageproc.h/fitscat.h (i.e. the
    CFITSIO convention) and fitswcs.h (Terapix).  There is currently an 
    inconsistancy of the definition of OFF_T between these two include files.
    No known affect other that compile warnings have been noted.

*/

#include <stdio.h>
#include <stdlib.h>
#include "imageproc.h"  
#include "fitswcs.h"
#define MAXPIXELS 32

int main(argc,argv)
   int argc;
   char *argv[];

{
   int  flag_update,flag_verbose=2,i,len,status=0;
   void printerror(),rd_desimage(),reportevt(),retrievescale();
   int  check_image_name();
   char comment[100],filename[1000],oldcomment[100];
   char catfile[10000],event[10000];
   int  nx,ny;
   int  anynul, hdutype, hdunum;
   double ra,dec,olddblkey;
   double rawpos[2],wcspos[2];

   FILE     *inp;
/*   desimage data;  */
   wcsstruct *wcs_in;
   catstruct *cat;
   tabstruct *tab;
   
   fitsfile *fptr;

   if (argc < 2){
     printf("%s <image> <options>\n",argv[0]);
     printf("  Input data\n");
     printf("    <image>   = FITS image with an astrometric solution \n");
     printf("  Options\n");
     printf("    -update  \n"); 
     printf("    -verbose [0-3]\n"); 
     exit(0);
   }

   /* ************************************************************ */
   /* ********* Extract verbose flag from Command Line *********** */
   /* ************************************************************ */
	
   for (i=2;i<argc;i++){
      if (!strcmp(argv[i],"-verbose")) {
         sscanf(argv[++i],"%d",&flag_verbose);
         if (flag_verbose<0 || flag_verbose>3) {
            sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
            flag_verbose=2;
	    reportevt(2,STATUS,3,event);
	 }
      }
    }
	
    /* ******************************************************* */
    /* ********* Handle Input Image and Catalog Files ******** */
    /* **** Expected to be the first and second arguments **** */
    /* ******************************************************* */

    /* copy input image name, check whether it is a  FITS file*/
    inp=fopen(argv[1],"r");
    if (inp==NULL) {
       sprintf(event,"Input image file not found: %s",argv[1]);
       reportevt(flag_verbose,STATUS,5,event);
       exit(1);
    }
    fclose(inp);
    if (check_image_name(argv[1],CHECK_FITS,flag_verbose)) {
      sprintf(filename,"%s",argv[1]);
    }

    /* ****************************************** */
    /* ********* Process Command Line *********** */
    /* ****************************************** */

    flag_update=0;
    for (i=2;i<argc;i++) {
       if (!strcmp(argv[i],"-update")){flag_update=1;}
    }

    /* *********************************************** */
    /* *********** Open image and read data ********** */
    /* ***** Open image get WCS (a la Emmanuel ) ***** */
    /* *********************************************** */

    if (!(cat = read_cat(filename)))
      error(EXIT_FAILURE, "*Error*: No such file: ", filename);

    tab=cat->tab;
    wcs_in=read_wcs(tab);

    /* *************************************************************** */
    /* ***** Setup an array of pointers to aid in image analysis ***** */
    /* *************************************************************** */

    nx=wcs_in->naxisn[0];
    ny=wcs_in->naxisn[1];

    printf(" %d %d %d \n",nx,ny,flag_update);

    /* *********************************** */
    /* ***** Find RA,Dec for corners ***** */
    /* *********************************** */

    if (flag_update){
      if (fits_open_file(&fptr,filename,READWRITE,&status)) printerror(status); 
    }

    rawpos[0]=1.0;
    rawpos[1]=1.0;
    raw_to_wcs(wcs_in,rawpos,wcspos);
    printf (" CORNER1: alpha,delta=%lf,%lf\n",wcspos[0],wcspos[1]);
    if (flag_update){
       status=0; 
       sprintf(comment,"RA of corner1 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1RA1",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1RA1",&wcspos[0],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1RA1",&wcspos[0],comment,&status)) printerror(status);
       }
       status=0;
       sprintf(comment,"DEC of corner1 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1DE1",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1DE1",&wcspos[1],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1DE1",&wcspos[1],comment,&status)) printerror(status);
       }
    }

    rawpos[0]=nx;
    rawpos[1]=1.0;
    raw_to_wcs(wcs_in,rawpos,wcspos);
    printf (" CORNER2: alpha,delta=%lf,%lf\n",wcspos[0],wcspos[1]);
    if (flag_update){
       status=0;
       sprintf(comment,"RA of corner2 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1RA2",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1RA2",&wcspos[0],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1RA2",&wcspos[0],comment,&status)) printerror(status);
       }
       status=0;
       sprintf(comment,"DEC of corner2 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1DE2",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1DE2",&wcspos[1],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1DE2",&wcspos[1],comment,&status)) printerror(status);
       }
    }
 
    rawpos[0]=nx;
    rawpos[1]=ny;
    raw_to_wcs(wcs_in,rawpos,wcspos);
    printf (" CORNER3: alpha,delta=%lf,%lf\n",wcspos[0],wcspos[1]);
    if (flag_update){
       status=0;
       sprintf(comment,"RA of corner3 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1RA3",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1RA3",&wcspos[0],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1RA3",&wcspos[0],comment,&status)) printerror(status);
       }
       status=0;
       sprintf(comment,"DEC of corner3 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1DE3",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1DE3",&wcspos[1],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1DE3",&wcspos[1],comment,&status)) printerror(status);
       }
    }
 
    rawpos[0]=1.0;
    rawpos[1]=ny;
    raw_to_wcs(wcs_in,rawpos,wcspos);
    printf (" CORNER4: alpha,delta=%lf,%lf\n",wcspos[0],wcspos[1]);
    if (flag_update){
       status=0;
       sprintf(comment,"RA of corner4 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1RA4",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1RA4",&wcspos[0],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1RA4",&wcspos[0],comment,&status)) printerror(status);
       }
       status=0;
       sprintf(comment,"DEC of corner4 calculated from WCS");
       if (fits_read_key_dbl(fptr,"COR1DE4",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"COR1DE4",&wcspos[1],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"COR1DE4",&wcspos[1],comment,&status)) printerror(status);
       }
    }

    rawpos[0]=nx/2.0;
    rawpos[1]=ny/2.0;
    raw_to_wcs(wcs_in,rawpos,wcspos);
    printf (" CENTER: alpha,delta=%lf,%lf\n",wcspos[0],wcspos[1]);
    if (flag_update){
       status=0;
       sprintf(comment,"RA of image center calculated from WCS");
       if (fits_read_key_dbl(fptr,"RA1",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"RA1",&wcspos[0],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"RA1",&wcspos[0],comment,&status)) printerror(status);
       }
       status=0;
       sprintf(comment,"DEC of image center calculated from WCS");
       if (fits_read_key_dbl(fptr,"DEC1",&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
          status=0;
          if (fits_write_key(fptr,TDOUBLE,"DEC1",&wcspos[1],comment,&status)) printerror(status);
       }else{
          if (fits_update_key(fptr,TDOUBLE,"DEC1",&wcspos[1],comment,&status)) printerror(status);
       }
    }


    if (flag_update){
      if (fits_close_file(fptr,&status)) printerror(status);
    }

    exit(0);
}



