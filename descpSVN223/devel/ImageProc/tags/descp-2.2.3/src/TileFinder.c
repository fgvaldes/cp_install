#include "TileFinder.h"
/*
 * The program  detects natural crowding of observation bore sights and indicate them
 *  as tile pointing. The input list of data files and their bore sight Ra,Dec coordinates
 *  is processed to produce a new list containing information necessary for cp_remap program
 *  The accuracy of the tile positioning is rough (about +/- 0.5 degree). This can be improved
 *  if corresponding request will be given.
 *  The program is based on GLib-2.0 objects and GLibmm-2.4 memory management.
 *  compilation of the program require following elements:
 *  gcc  -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -L/lib64 -lglib-2.0 -lglibmm-2.4 -lgobject-2.0
 *   -lsigc-2.0 -lm TileFinder.c -o TileFinder
 *   To find specific parameters for each platform use pkg-config
 *   pkg-config --cflags glib-2.0; pkg-config --libs glib-2.0
 *
 *   Input parameters: -input <input list of files: format <file name> <Ra> <Dec> >
 *                     -outfile <filename including path where result will be stored >
 *                     -tpfile < filename including path where tangent point grid is stored >
 *                     -radius <radius of search in degrees >
 *
 *
 * Author: Nikolay Kuropatkin    03/20/2012
 *
 *
 *
 *
 */
void iterator(gpointer key, gpointer value, gpointer user_data) {
	printf(user_data, *(gint*)key, *(gint*)value);
}
void iterator1(gpointer key, gpointer value, gpointer user_data) {
	printf(user_data, *(gint*)key, ((tangent_point*)value)->pointname);
}
void iterator2(gpointer key,  gpointer user_data) {
	printf(user_data, *(gint*)key);
}

void iterator3(gpointer key,  gpointer user_data) {
	gint *ind = g_new(gint,1);
	*ind = *(gint*)key;
	printf(" got key %d install ind %d\n",*(gint*)key, *(gint*)ind);
	user_data = g_list_append((GList*)user_data, ind);
	printf("new size of list is %d\n",g_list_length(user_data));

}
/* compare function used for sorting arrays of double1 */

gint CompareFunct(gconstpointer a, gconstpointer b){
	int *ac = (int *)a;
	int *bc = (int *)b;

	return *ac - *bc;
}
/* function to remove proper key value from list */
 void removeMember(GList * list, int key) {
	int i, nk =0;
	int * currKey;
	nk = g_list_length(list);

	/* search for proper key value */
	for (i=0; i< nk; i++){
		currKey = (int*)(g_list_nth(list,i)->data);
		if ( *currKey == key){

				 list = g_list_delete_link(list, g_list_nth(list, i));
			g_free(currKey);
			break;
		}

	}

}

 double dround(double input) {
//	 double sign = (input > 0. ? 1.0 : -1.0);
//	 double val = abs(input);
	 double val = input;
	 double res;
	 res = (double)floor(val + 0.5);
//	 res *=sign;
	 return res;
 }

/* convert tangent point index into RA,Dec  */
skyPoint getRaDec(int index) {
	skyPoint res;
	double shiftD = 90.;
	double myind = (double)index;
	double int_part, part, dec, ra;
	double divider;
	int deci;
	deci = (int)(myind/1000.);
	dec = (double) deci;  /* drop RA from index */
	divider = dec*1000.;
	if (dec > 0.) {
	  part  = (modf(myind/divider,&int_part));
	  ra =  (part*divider);
	} else {
		ra = myind;
	}
	res.Dec = dec - shiftD;
	res.RA = ra;
	return res;
}

/*  convert Ra,Dec into tangent point index */
int getIndex(skyPoint point) {
	double shiftD = 90.;

	   int val = (int)((dround(point.Dec + shiftD))*1000. + dround(point.RA));

	return val;
}
/*
 *  Search closest key in the GList to given value
 *
 */
int FindPoint(GList * list, int *key) {

	skyPoint sp, spI;
	int res = 0;
	int n = g_list_length(list);
	double dist = 0.;
	double min = 10e32;
	gint t;
	int i = 0;
	double val;
	int indMin = 0;
	int indMax = n;
	int indMed = 0;
	if (n >=4) {
		indMed = n/2;


	while ((indMax - indMin) > 4 ){
	val = *(int *) g_list_nth(list,indMed)->data;
	if (val >= *key) {
		indMax = indMed;
		indMed = indMin + (indMax - indMin)/2;
	} else {
		indMin = indMed;
		indMed = indMin +(indMax - indMin) /2;
	}
	}

    min = 10e32;
	for (i=indMin; i<=indMax; i++) {
		t = *(int*)(g_list_nth(list,i)->data);
		spI = getRaDec(*key);
		sp = getRaDec(t);
		dist = (sqrt((sp.RA - spI.RA)*(sp.RA - spI.RA)  + (sp.Dec - spI.Dec)*(sp.Dec - spI.Dec)));
		if (dist <= min) {
				min = dist;
				res = t;

			}
	}
	}else {
		min = 10e32;
		for (i=indMin; i<=indMax; i++) {
			t = *(int *)(g_list_nth(list,i)->data);
			spI = getRaDec(*key);
			sp = getRaDec(t);
			dist = (sqrt((sp.RA - spI.RA)*(sp.RA - spI.RA)  + (sp.Dec - spI.Dec)*(sp.Dec - spI.Dec)));
				if (dist <= min) {
					min = dist;
					res = t;

				}
		}
	}

	return res;
}
/*
 * Search in +/- radius square neighbor clusters and sum entries
 * then search maximum one, put it in result table and remove from search.
 * repeat search untill number of entries in remaining clusters will not be <=1
 *
 */
GArray * findTile(GHashTable *pointTab, GList* keysTP, GList* cand, GHashTable*skyGrid, double radius){
	GArray *res;
	res = g_array_new(FALSE, FALSE, sizeof(Tile *));
	Tile *tile = NULL;
	int key,  ctInd,  selKey,  tileInd ;
	double  ra, dec,  avRa, avDec, sumTp, sumRa, sumDec;
	int i,nk, sum,max,  nsel;
	skyPoint sp, center, currCent;
	center.RA=0.0;  center.Dec = 0.0;

 /* adding extra value to the list of candidates give us ability to make the
  *  loop on members with removing used values. Probably a bug in glib when removing
  *  elements from list or hash the returned index of the last one is 0.
  *  To turn around this I add extra element and terminate loop when the last element has left.
  */
	gint *one = g_new(gint,1);
	*one = -1.0;

	cand = g_list_append(cand, one);

	cand = g_list_sort(cand,(GCompareFunc)CompareFunct);
	nk = g_list_length(cand);
//	printf("number of candidates = %d \n",nk);

	/*
	 *  Loop on the table untill all tiles are selected
	 *
	 */
	 int ncycles = 0;

	 while ( g_list_length(cand) > 1) {
		 ncycles++;
		 nk = g_list_length(cand);

		 selKey = 0.0;
//
		 /* create candidate tile */
		 tile =(Tile *)calloc(1, sizeof(Tile));
		 tile->neighbors = g_array_new(FALSE, FALSE, sizeof(int));

		 max = 0;
		 for (i=0; i<nk; i++){    /* loop on candidates */
			 sum = 0;
			 nsel = 0;


			 ctInd = *(int*)(g_list_nth(cand,i)->data);
//			 printf(" processing tile %d \n",ctInd);

			 currCent = getRaDec(ctInd);           /* get Ra,Dec from index */
//			 printf(" current index %d  Current RA =%f\n",ctInd, currCent.RA);


			 for (ra = currCent.RA - radius; ra<= currCent.RA + radius; ra++) {
				 for (dec = currCent.Dec - radius; dec <= currCent.Dec + radius; dec++) {
					 sp.Dec = dec;
					 sp.RA = ra;
					 if( ra < 0.) sp.RA = ra +360.;
					 if(ra >360.) sp.RA = ra - 360.;
					 if (dec <-90.) sp.Dec = -90.;
					 if (dec >90.) sp.Dec = 90.;

					 key = getIndex(sp);
					 int* point = NULL;
					 int* entries = NULL;
					 int** key_ptr = &point;
					 int** value_ptr = &entries;

					 gboolean result = g_hash_table_lookup_extended(pointTab, &key,(gpointer*)key_ptr, (gpointer*)value_ptr);
					 if (result && (*entries >0)) {
//						 printf(" ra=%f dec=%f key=%ld entries=%d\n",ra,dec,key,*entries);
						 sum +=*entries;

						 nsel++;

					 }
				 }
			 }

			 if (sum >= max) {
				 max = sum;
				 selKey = ctInd;
				 center.RA = currCent.RA;
				 center.Dec = currCent.Dec;
//				 printf("got max %d for index %d \n",max,ctInd);
			 }

		 }                         /* end loop on cells  */
		 if (max > 1) {

			 sumRa = 0.;
			 sumDec = 0.;
			 sumTp =0.;
//			 printf("tile name DECam_%ld\n",tileInd);
			 /* now search for neighbors of the candidate and add them to array */
			 /* here we need to find weighted average as a center of the tile   */
			 /* to make it more precise we need to use actual center of pixel not from index */
			 tile->neighbors = g_array_new(FALSE, FALSE, sizeof(int));
			 for (ra = center.RA - radius; ra<= center.RA + radius; ra++) {
 				for (dec = center.Dec - radius; dec <= center.Dec + radius; dec++) {
					 sp.Dec = dec;
					 sp.RA = ra;
					 if( ra < 0.) sp.RA = ra +360.;
					 if(ra >360.) sp.RA = ra - 360.;
					 if (dec <-90.) sp.Dec = -90.;
					 if (dec >90.) sp.Dec = 90.;
 					key = getIndex(sp);
 					int* point = NULL;
 					int* entries = NULL;
 					int** key_ptr = &point;
 					int** value_ptr = &entries;

 					gboolean result = g_hash_table_lookup_extended(pointTab, &key,(gpointer*)key_ptr, (gpointer*)value_ptr);
 					if (result) {
//					 printf("sel Index to put is %ld \n",key);
 						sumRa += ra*(*point);
 						sumDec += dec*(*point);
 						sumTp += (*point);
 						g_array_append_val(tile->neighbors,key);
 					    removeMember(cand,key);
 						nsel++;

 					}
 				}
			 }
			 avRa = sumRa/sumTp;
			 avDec = sumDec/sumTp;
			 sp.Dec =avDec;
			 sp.RA = avRa;
			 selKey = getIndex(sp);
			 tileInd = FindPoint(keysTP,&selKey);
			 /* find real Ra,Dec for the tangent point */
			    int* point = NULL;
			   	tangent_point* tp = NULL;
			   	int** key_ptr = &point;
			   	tangent_point** value_ptr = &tp;
			   	 gboolean result = g_hash_table_lookup_extended(skyGrid, &tileInd,(gpointer*)key_ptr, (gpointer*)value_ptr);

			 tile->raC = tp->ra;
			 tile->decC =tp->dec ;
			 tile->tileID = tileInd;
			 sprintf(tile->name,"%s",tp->pointname);
//			 printf("tile name DECam_%d\n",tileInd);
			 tile->nP = max;
			 g_array_append_val(res,tile);
		 } else {							/* just remove those unused entries */


			 for (ra = center.RA - radius; ra<= center.RA + radius; ra++) {
 				for (dec = center.Dec - radius; dec <= center.Dec + radius; dec++) {
					 sp.Dec = dec;
					 sp.RA = ra;
					 if( ra < 0.) sp.RA = ra +360.;
					 if(ra >360.) sp.RA = ra - 360.;
					 if (dec <-90.) sp.Dec = -90.;
					 if (dec >90.) sp.Dec = 90.;
 					key = getIndex(sp);
 					int* point = NULL;
 					int* entries = NULL;
 					int** key_ptr = &point;
 					int** value_ptr = &entries;

 					gboolean result = g_hash_table_lookup_extended(pointTab, &key,(gpointer*)key_ptr, (gpointer*)value_ptr);
 					if (result) {
//					 printf("sel Index to remove is %ld \n",key);
 						removeMember(cand,key);
 						nsel++;

 					}
 				}
			 }
			 free(tile);

		 }


//		 if (ncycles >=7) break;
	 }  /* end of while loop */

	 g_free(one);
//	 printf("# selected tiles = %d\n",res->len);
	return res;
}
/*
 * Given skyPoint search in the list of tiles which one contains it
 *  and return skyPoint of the tile. The last one will be used for re-mapping
 *
 */
skyPoint getRaDecTile(GArray * tiles, skyPoint pointing){
	skyPoint res;
	res.RA = -1.0;
	res.Dec = -100.0;
	Tile * tile=NULL;
	int ntiles = tiles->len;
	int found = 0;
	int index = getIndex(pointing);
	int i, k, ncells;
	for (i=0; i<ntiles; i++) {
		found = 0;
		tile = (Tile*) g_array_index(tiles,Tile*,i);
		ncells = tile->neighbors->len;
		for (k=0; k<ncells; k++) {
			if (index == *(int*)g_array_index(tile->neighbors,int*,k)) {
				res.RA = tile->raC;
				res.Dec = tile->decC;
				found = 1;
				break;
			}
		}
		if (found) break;
	}
	return res;
}
/* search for tile that contain given cell index */
Tile* getTile(GArray * tiles, int* index){
	Tile * tile=NULL;
	int ntiles = tiles->len;
	int found = 0;
	int i, k, ncells;
	for (i=0; i<ntiles; i++) {
		found = 0;
		tile = (Tile*) g_array_index(tiles,Tile*,i);
		ncells = tile->neighbors->len;
		for (k=0; k<ncells; k++) {
			if (*index == g_array_index(tile->neighbors,int,k)) {
				found = 1;
				break;
			}
		}
		if (found) break;
	}
	if (!found) tile = NULL;
	return tile;
}
 skyPoint getRaDecFromIndex(GArray *tiles, int* in) {
	 skyPoint res;
	 int index = *in;
		res.RA = -1.0;
		res.Dec = -100.0;
		Tile * tile=NULL;
		int ntiles = tiles->len;
		int found = 0;
		int i, k, ncells;
		for (i=0; i<ntiles; i++) {
			found = 0;
			tile = (Tile*) g_array_index(tiles,Tile*,i);
			ncells = tile->neighbors->len;
			for (k=0; k<ncells; k++) {
				if (index == *(int*)g_array_index(tile->neighbors,int*,k)) {
//					printf(" index= %lf tile index= %lf \n",index,*(double*)g_array_index(tile->neighbors,double*,k));
					res.RA = tile->raC;
					res.Dec = tile->decC;
//					printf(" res RA= %f Dec= %f \n",res.RA, res.Dec);
					found = 1;
					break;
				}
			}
			if (found) break;
		}
//		printf("res RA= %f Dec= %f \n",res.RA, res.Dec);
		return res;
 }

 int main(argc,argv)
     int argc;
     char *argv[];
{

    	 tangent_point* tp;
    	 int N, Ntp,i, flag_verbose=2,flag_input=0,flag_tpfile=1,flag_radius=1;
    	 int flag_outpath=1;
    	 int *npoint;
    	 int newKey;
    	 double radius=1.0;
    	 double val;
    	 int *key,  *cell, *ind;
    	 double PixSize = 0.27;
    	 skyPoint sp,  pointing;
    	 Tile* tile;

    	 FILE *tpfile, *pip,*fin, *fout;
    	 char command[1000], tpfilename[1000],inpline[100],event[2000],inlist[1000],
    	 outfile[1000],inputimage[1000];
    	 float RA,Dec, remapRA, remapDec;
    	 void select_dblogin(),reportevt(),get_input(),printerror();
    	 npoint =(int *)calloc(1,sizeof(int ));
    	 sprintf(tpfilename,"tangent_grid_test.txt");
    	  if (argc<2) {
    	    printf("Usage: %s <required inputs> \n",argv[0]);
    	    printf("    Required Inputs:\n");
    	    printf("       -input <list (format: FileNameIncluding Path RA  Dec>  \n");
    	    printf("       -tpfile <tpfilename including path>  \n");
    	    printf("       -outfile <output file including path>  \n");
    	    printf("       -radius <radius>  \n");
    	    printf("\n");
    	        exit(0);
    	  }
    	  /* *** Process command line *** */
    	  for (i=2;i<argc;i++) {
    	    if (!strcmp(argv[i],"-verbose")) {
    	      sscanf(argv[++i],"%d",&flag_verbose);
    	      if (flag_verbose<0 || flag_verbose>3) {
    	        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
    	        flag_verbose=2;

//    	        reportevt(flag_verbose,STATUS,3,event);
    	      }
    	    }
    	  }
    	  /* now go through again */
    	  for(i=1;i<argc;i++) {
    	    if (!strcmp(argv[i],"-input")) {
    	      flag_input=1;
    	      sscanf(argv[i+1],"%s",inlist);
    	      printf(" %s \n",inlist);
//    	      get_input(argv[i],argv[i+1],"-input",inlist,flag_verbose);

    	    }


    	    if (!strcmp(argv[i],"-tpfile")) {
    	      flag_tpfile=1;
    	      sscanf(argv[i+1],"%s",tpfilename);
    	      printf(" %s \n",tpfilename);
//    	      get_input(argv[i],argv[i+1],"-tpfile",tpfilename,flag_verbose);
    	    }

    	    if (!strcmp(argv[i],"-outfile")) {
    	      flag_outpath=1;
    	      sscanf(argv[i+1],"%s",outfile);
    	      printf(" %s \n",outfile);
//    	      get_input(argv[i],argv[i+1],"-outpath",outpath,flag_verbose);
    	    }

    	    if (!strcmp(argv[i],"-radius")) {
    	    	flag_radius=1;
//    	    	printf("got flag -radius \n");
//    	    	printf("got value= %s\n",argv[i+1]);
    	    	radius = atof(argv[i+1]);
    	    	if (radius < 1.0) radius = 1.0;
    	    	printf("radius= %f \n",radius);
  	    }
  	  }
    	    /* quit if required inputs are not set */
    	    if(!flag_input || !flag_tpfile || !flag_outpath || !flag_radius ) {
    	      printf("Required inputs are not set \n");
//    	      reportevt(flag_verbose,STATUS,5,event);
    	      exit(0);
    	    }

    	  /* *** Find out the number of lines from tangent points file *** */
    	  sprintf(command,"wc -l %s",tpfilename);
    	  pip=popen(command,"r");
    	  if (pip==NULL) {
    	    printf("Pipe failed: %s ",command);
//    	    reportevt(flag_verbose,STATUS,5,event);
    	    exit(0);
    	  }
    	  fscanf(pip,"%d",&Ntp);
    	  pclose(pip);

//    	  printf("Number of lines in %s = %d \n",tpfilename,Ntp);
//    	  reportevt(flag_verbose,STATUS,1,event);
    	  tp=(tangent_point *)calloc(Ntp+1,sizeof(tangent_point ));
    	  if (tp==NULL) {
    		  printf("Calloc of tangent points failed \n");
//    	     reportevt(flag_verbose,STATUS,5,"Calloc of tangent points failed");
    	     exit(0);
    	   }
    	  /*    read the tangent points file    */

    	  tpfile=fopen(tpfilename,"r");
    	  if (tpfile==NULL) {
    		  printf("File %s not found \n",tpfilename);
//    	    sprintf(event,"File %s not found",tpfilename);
//    	    reportevt(flag_verbose,STATUS,5,event);
    	    exit(0);
    	  }
    	  i = 0;
    	//
    	   while (fgets ( inpline, sizeof inpline, tpfile ) != NULL) {

    		if (!strncmp((char *)&inpline,"#",1)){
    			printf("Skip comment \n");
//    			sprintf(event,"Skip comment \n");
//    			reportevt(flag_verbose,STATUS,5,event);
    			continue;
    		}    // skip comments

    		sscanf(inpline,"%s %lf %lf",tp[i].pointname,&tp[i].ra, &tp[i].dec);

    		i++;
    		if (i >=Ntp ) break;
    	  }
    	  fclose(tpfile);

    	 GHashTable *skyGrid = g_hash_table_new(g_int_hash, g_int_equal );
    	 GList *keysTP = NULL;
    	 int k;
    	 for ( k=0; k< Ntp; k++) {
    		 key = g_new(gint, 1);
    		 pointing.RA = tp[k].ra;
    		 pointing.Dec = tp[k].dec;
    		 val =getIndex(pointing);
    	     *key = val;
    	     g_hash_table_insert(skyGrid,key, &tp[k]);
    	     keysTP = g_list_append(keysTP,key);

    	 }
    	 /* there is no get keys for older version of glib do it by hand */

//    	 GList *keysTP =(GList*) g_hash_table_get_keys(skyGrid);
    	 keysTP = g_list_sort(keysTP,(GCompareFunc)CompareFunct);
//    	 g_list_foreach(keysTP,iterator2," value in the list %d\n");
//    	 printf(" created tangent points table \n");

    	  /*															*/
    	  /* Now we have tangent points let read input file and build
    	   *  hash with tangent point keys and number of pointings in each
    	   */
  	    /* *** Find out the number of lines from input file *** */
  	    sprintf(command,"wc -l %s",inlist);
  	    pip=popen(command,"r");
  	    if (pip==NULL) {
  	    	printf("Pipe failed: %s \n",command);
//  	      sprintf(event,"Pipe failed: %s ",command);
//  	      reportevt(flag_verbose,STATUS,5,event);
  	      exit(0);
  	    }
  	    fscanf(pip,"%d",&N);
  	    pclose(pip);
//  	  printf("Number of images in %s = %d \n",inlist,N);
//  	    sprintf(event,"Number of images in %s = %d",inlist,N);
//  	    reportevt(flag_verbose,STATUS,1,event);
  	  /* *** Input data  *** */
  	  fin=fopen(inlist,"r");
  	  if (fin==NULL) {
    	    printf("File %s not found \n",inlist);
//  	    sprintf(event,"File %s not found",inlist);
//  	    reportevt(flag_verbose,STATUS,5,event);
  	    exit(0);
  	  }


     	 GHashTable* pointTab = g_hash_table_new(g_int_hash, g_int_equal );
     	 GList* cand = NULL;
//    	printf("start read input file \n");
     	int first = 1;
     	  for(i=0;i<N;i++) {
     	    fscanf(fin,"%s %f %f ",inputimage,&RA,&Dec);
//     	    printf("%s %f %f \n",inputimage,RA,Dec);
     	   cell = g_new(gint, 1);
     	   ind = g_new(gint,1);
     	   sp.RA = RA; sp.Dec = Dec;
     	   int val = getIndex(sp);
     	   *cell = val;
      	    newKey = FindPoint(keysTP,cell);
      	    *cell = newKey;
      	    *ind = newKey;
     	    if (first == 1){
     	    	npoint = g_new(gint,1);
     	    	*npoint =1;
     	   	   g_hash_table_insert(pointTab,cell, npoint);
     	   	   cand = g_list_append(cand,ind);
//     	   	   printf(" insert first key %d  key= %lf \n",*npoint, *cell );
     	   	   first = 0;
     	   	   continue;
     	    }

     	    	 int* point = NULL;
     	    	 int* entries = NULL;
     	    	 int** key_ptr = &point;
     	    	 int** value_ptr = &entries;

     	    	gboolean result = g_hash_table_lookup_extended(pointTab, cell,(gpointer*)key_ptr, (gpointer*)value_ptr);

     	    if ( !result) {
     	    	npoint = g_new(gint,1);
     	    	*npoint =1;
     	    	g_hash_table_insert(pointTab,cell, npoint);
     	    	cand = g_list_append(cand,ind);               /* add only once */
//     	    	printf(" strat insertion in table %d  key= %lf \n",*npoint, *cell );
     	    } else {
//     	    	printf(" previous value in cell was %d\n",*entries);
     	    	npoint = (int*)entries;
     	    	(*npoint)++;
//     	    	printf("Increment tab value %d for key= %lf \n",*npoint,*cell);
     	    	g_hash_table_insert(pointTab, cell, npoint);

     	    }

     	  }
     	  fclose(fin);
     	 cand = g_list_sort(cand,CompareFunct);
//     	  printf("created table\n");
//     	  g_hash_table_foreach(pointTab,(GHFunc)iterator,"The value of key %d is %d \n");

     	  /* Now we have a table with pixel numbers and number of pointings in each
     	   *  lets find clusters of given raduis starting from most significant one
     	   */
//     	  printf(" start with tiles \n");
     	  GArray *tiles = findTile(pointTab,keysTP,cand, skyGrid, radius);

     	  /*
     	   *  Now reopen the input file read entries find tile number it beints to
     	   *  and write output file with RA,Dec of the remapping pointing
     	   *  that will be input for cp_remap.c
     	   *
     	   */

     	 fin=fopen(inlist,"r");
    	 if (fin==NULL) {
    		 printf("File %s open failed",inlist);
//     	    sprintf(event,"File %s open failed",imageidlist);
//     	    reportevt(flag_verbose,STATUS,5,event);
    	    exit(-1);
    	 }
     	 fout = fopen(outfile,"w");
     	 if (fout==NULL) {
     		 printf("File %s open failed",outfile);
     	    exit(-1);
     	 }
     	for(i=0;i< N;i++) {
     	 fscanf(fin,"%s %f %f ",inputimage,&RA,&Dec);
     	 pointing.RA = RA;
     	 pointing.Dec = Dec;
     	 int index = getIndex(pointing);
     	 newKey = FindPoint(keysTP,&index);
     	 tile = getTile(tiles, &newKey);
     	 if (tile != NULL) {
     		 remapRA = tile->raC;
     		 remapDec = tile->decC;
     		 fprintf(fout,"%s %s %f %f %f \n",inputimage,tile->name, PixSize, remapRA, remapDec);
     	 } else {
     		 printf("file %s has no associated tile, skipping \n",inputimage);
     	 }
     	}
     	fclose(fin);
     	fclose(fout);

    	  /* free memory  */
     	  for(i=0; i< tiles->len; i++) {
     		  free((Tile*)g_array_index(tiles,Tile*,i));
     	  }
    	  g_hash_table_destroy(skyGrid);
    	  g_hash_table_destroy(pointTab);
    	  g_array_free(tiles,TRUE);
    	  free(tp); free(npoint);
    	  g_list_free(cand);
    	  g_list_free(keysTP);

    	  return 0;
}
