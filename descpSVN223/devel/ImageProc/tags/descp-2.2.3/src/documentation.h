/** \file documentation.h
    \brief Notes from IllinoisRocstar
    
    \note {
     TODO Items for Wednesday Feb 8 meeting
     -# Find out what mkflatcor is doing
     -# Investigate interpolation in the toolchain, with particular interest in MASK-directed interpolation
     -# Investigate all bitmask setting and bitmask-directed operations in the toolchain 
     -# Track down bleed trail processing
    }
    \note {
    Some questions:
      - How detailed should this investigation be?  Do you want math for the operations, or a qualitative descriptions?
        - A mix; formulas where useful
      - Are there any more particular procedural or operational questions that I should track down before Wednesday? (e.g. like item (4) above)
        or are there particular questions about mkflatcor (or other tool) which I should focus on answering?
        - No, bleedtrail processing is the main concern right now
      - Is there a definitive list of a (tentative would be ok) sequence of procedures for the end-to-end processing chain?
      - Some functionality is implemented in more than one tool (do we know which ones we are targeting)?
      - What about the legacy end-to-end processing chain (is it available)?
        - Update from Ricardo to address above 3 bullets:
          - https://desweb.cosmology.illinois.edu:8443/confluence/display/~ricardoc/Manually+Running+Commands+for+Testing+Developed+Code
      - Is the team open to new/different implementations of some of these operations? (e.g. bleed trail mitigation)
        - Yes, needing bleedtrail overhaul 
      - I'm still looking for the final form of the input for normalization of flats [no rush, just reminder]
        - Michelle has them here (destest:/home/mgower/flatscale)
        - Are the input flats already bias subtracted (and overscanned)?
          -Yes
    }
    \note {
     TODO Items for Friday Feb 10 meeting
    -# Integrate Ricardo's new overscan
       - Please find Ricardo's overscan in these utilities:
         - Mosaic2XTalk (overscan-enabled Mosaic2_crosstalk)  Source: Mosaic2XTalk.c
         - DECamXTalk   (overscan-enabled DECam_crosstalk)    Source: DECamXTalk.c
         - roverscan    (command line interface)              Source: CallOverScan.c
    -# add command line options for specification of norm factor in flat norm code
       - chip
       - value
       - else... median
       .
    -# Investigate possible improvements to mkmask (star masking and bleedtrail processing)
       - Meeting with R.G., R.C. and Frank at 3 on Monday
         - Use segmentation instead of catalog masking
         - Morphological kernel (long thin) to detect bleedtrails
         - Beware of local variations in sky mean in segmentation 
    }
    \note {
     TODO Items for Wednesday Feb 15
    -# Investigate inter-chip flat normalization bug 
       - rd_desimage fails on Michelle's flat correction files
       - R.G.: Could be known problem with legacy library or files (remind team on Tuesday)
       - team open to new implementation of rd_desimage
       - Problems resolved
         - new version of rd_desimage handles compressed files more robustly
         - xchipfltnrm (xchip_flatnorm.C) available for testing (/work/deve/mtcampbe/export/xchipfltnrm)
    -# Think about/begin bleedtrail processing overhaul
    }
    \note {
     TODO Items for Friday Feb 17
    -# Begin bleedtrail coding
       - Star masking 
         - possibly use sources identified from other part of tool chain
         - new segmentation procedure
       - Bleed trail detection
         - Added morphological image filters for grayscale and binary images
         - Implemented bleed trail detection with 2 pass algorithm
           - Uses morphological filters with one structuring element for each of the trail and background
           - 1st pass identifies all local bright pixels
           - 2nd pass detects bleedtrails - excluding bright pixels in background estimation
    -# Need to think about hard test cases, metric for success
    }
    \note {
    -# Look at mksupersky and mkillumcor
       - Can improvements be made (speed/functionality)?
       - Profiling shows IO is the major bottleneck
         - Loads many images at the same time
         - May need to operate on subsets of all images at a time
         - Depends on how it is spawned - many per compute node, or one per node
    }
    \note {
    -# Fixed bug in imcorrect wherein incoming images which were already 
       trimmed (i.e. overscan correction) were not properly processed.
    -# Fixed bug in Mosaic2 Xtalk wherein off-by-one image sections caused
       artifacts in overscan corrected image
    -# Fixed Mosaic2XTalk status messages reporting spurious image open 
       failures. Problem was filename-specific failures.
    -# Fixed command-line parameter validation for Xtalk and downstream
       utils.
    -# Fixed Xtalk, MkBiasCor, MkDarkCor, MkFlatCor, imcorrect to correctly
       (dynamically) determine which AMP for a given pixel.   Xtalk utils
       need to use DATASEC[A,B] as those pertain to pre-trimmed indices, 
       while downstream from overscan correction AMP[A,B] may be used.
       - Problem will occur when overscan processing is skipped in xtalk utils
    }
    \todo {
    -# Saturated object detection
       - Create list of saturated object locations
       - Create bright star mask for saturated objects
       - Proposed procedure
         - Detect all saturated "blobs"
         - Erode saturated blobs to one pixel (these are the locations)
         - Dilate single pixel blobs to cover bright objects in orginal image (bright object mask)
    -# Create detailed, comprehensive change list for IR changes
    -# Fix xtalk/overscan bug in DECam leaving strip artifact in Amp 2 (i.e. right side of image)
    -# Fix interchip flat normalization to use header keywords instead of image list (option for either)
    -# Write Robert's header keywords from the appropriate utilities
       - new unified API for header ops
       - make sure to strip off leading paths for filenames going into header 
       - Preserve existing keywords
       - Replace command-line comments with HISTORY entries
    }
    \todo {
    -# Add options for median, mode scaling in mkflatcor
    -# Fix xchip_flatnorm so that it:
       - reads an input list (no default)
       - uses an output list (overwrite default)
       - uses current format as option
    -# Write two separate FITS table files from mkbleedmask for:
       - saturated objects
         - box corners
       - star centers
         - center + radius
         - enhanced radius determination (annuli) 
    }
    \todo {
    -# Look at codes for improvements
       - *coadd_swarp.c
       - *coadd_detection.c
       - dbutils.c
       - photcal
       - imcorrect
       - *runSExtractor
       - db_utils
       - get_*
       - scampmkcat
    }
    \todo {
       - add option to set weights for starmask to zero
       - mkbpm (read a list, or the masks)
    }
    \todo {
       - record the sky (get rid of outlyers before normalization)
       - add object masking to imreject
    }
   \todo {
       -# Need RA/DEC in mkbleedmask
          - check SCAMPFLG to see if wcs soln exists 
          - check get_corners_from_image.* for example on how to get ra/dec
       -# Possibly update overscan in legacy routines to do new overscan
          - rip out legacy overscan?
          - verify any perturbation on *SEC keywords
          - ensure AMP resolution is coming from properly formed AMPSEC keywords
       -# Fix spurious error messsages when trying files with different extensions
       -# Stop header streaming in crosstalk
       -# Documentation on mkbleedmask
          - see get_corners_from_image.* for example documentation
       -# Review image reading mechanics in get_corners_from_image
       -# Get rid of certain SEC keywords
       -# Set LTV to 0
       -# See FIXME in imcorrect to check for variance type
       -# Wrap roverscan with same keyword elimination and history update
   }
 */

/** \file imdetrend/imcorrect.c
    \brief Basic detrending of CCD data.
 
    The \em imcorrect utility takes a list of \em FITS (.fits) or
    compressed FITS (.fz) with an optional Bad Pixel Mask (BPM) as 
    input and performs the following possible operations:
    - OVERSCAN
    - BIAS SUBTRACTION
    - PUPIL CORRECTION
    - DARK CORRECTION
    - FLAT CORRECTION
    - ILLUMINATION CORRECTION
    - FRINGE CORRECTION
    - PHOTFLATTEN CORRECTION
    - SATURATION FLAGGING
    - BPM directed INTERPOLATION

    The "corrected" image and variance image are produced on 
    output.

    Usage: imcorrect \<image or file list\> \<options\>

    Command line options:
    - -bpm \<fits image\>\n
        Specifies an external Bad Pixel Mask. The 
        default behavior is to perform a bitwise OR
        with the native BPM (i.e. a BPM in the 
        input FITS file.) Sets flag_bpm=YES.
    - -bpm-override \<fits image\>\n
        Specifies an extern Bad Pixel Mask which is
        to be used as a repalacement for any native
        BPMs, should they exist. Sets flag_bpm=YES 
        and flag_bpm_override=YES.
    - -nooverscan\n
        Force imcorrect to not perform the overscan processing. Sets flag_nooverscan=YES.
    - -bias \<image\>\n
        Perform bias subtraction.  Sets bias.name=\<image\>, flag_bias=YES.
    - -pupil \<image\>\n
        Perform pupil correction. Sets flag_pupil=YES, pupil.name=\<image\>
    - -flatten \<image\>\n
        Perform flatten correction. Sets flag_flatten=YES, flat.name=\<image\>
    - -darkcor \<image\>\n
        Perform dark correction. Sets flag_dark=YES, dark.name=\<image\>
    - -photflatten  \<image\>\n
        Perform photflatten correction. Sets flag_photflatten=YES, phot.name=\<image\>
    - -illumination \<image\>\n
        Perform illumination correction. Sets flag_illumination=YES,illum.name=\<image\>
    - -fringe \<image\>\n
        Perform fring correction. Sets flag_fringe=YES, fringe.name=\<image\>
    - -interplate_col \<value\>\n
        Specifies noise scale for interpolated pixels. Sets flag_interpolate_col=YES, scale_interpolate=\<value\> \f$\in\f$ [1:10]
    - -scaleregion \<Xmin:Xmax,Ymin:Ymax\>\n 
        Specifies input region from which to determine image statistics. Sets scaleregionn, and defaults to [500:1500,1500:2500].
    - -output \<image|list\>\n
        Specifies output file(s).
    - -variancetype \<type\>\n
        Sets variance image type.  \<type\> must be one of SIGMA, WEIGHT, or INVERSE_VARIANCE, which sets 
        variance_type=DES_SIGMA,DES_WEIGHT, or DES_INVERSE_VARIANCE, respectively.
    - -noisemodel \<type\>\n
        Specifies type of noise model to use. \<type\> must be SKYONLY or FULL.  Sets flag_noisemodel=(DES_SKYONLY | DES_FULL).
    - -minsize \<pixels\>\n 
        Specifies minimum size (in pixels) of region used to determine sky background. Sets minsize parameter. (defaults to 4)
    - -maxsize \<pixels\>\n (defaults to 40)
        Specifies maximum size (in pixels) of region used to determine sky background. Sets maxsize parameter. (defaults to 4)
    - -ranseed \<value\>\n
        Specifies random seed used in noising sky background. 
    - -MEF\n 
        Indicates results should be written to single fits file. Sets flag_mef=YES.     
    - -verbose \<value\>\n
        Sets verbosity = \<value\>. [1-3], defaults to 1.
    - -overscantype \<type\>\n
        Sets overscantype to \<type\>. Must be \f$\in\f$ [0,2].

   High level procedure description is as follows:
-# Uses Native BPM if it exists and does bitwise OR with user-specified BPM
unless -bpm-override, in which case the user-specified BPM is used.  If
no BPM's are present, initialized to zero.
-# OVERSCAN Processing [if needed]
    imarithsubs.c::overscan
-# Uses existing variance image, init'd to zero if none and (flag_variance)
    (flag_variance=YES if native variance)
-# Mask Missing Image Section and Mark Saturated Pixels:
      - Get median,mode,fwhm (from scaleregionn)
      - Set BADPIX_LOW for pixels falling below threshold
      - Re-get median,mode,fwhm [if needed]
-# BIAS subtraction, PUPIL correction, and FLATTEN (flag_bias || flag_pupil || flag_flatten):
      - Set corrections with:\n
        FOREACH OUTPUT (PIXEL){:\n 
          scale  = flat_pixel (if it exists, else 1)\n
          offset = bias pixel\n
          offset = offset + pupil_pixel*mode\n
        }\n
      - Apply corrections with:\n
        FOREACH UNMASKED (PIXEL){:\n
          Set BADPIX_SATURATE for pixels exceeding saturation value\n
          Set pixel values as expected [i.e. (value - offset)/scale\n
        }\n
-# DARK correction (flag_dark)\n 
      - Set image = image - (exposuretime*dark_image)
-# VARIANCE Image Formation (flag_variance)\n
      - FOR UNMASKED PIXELS:\n
         gain = amp gain (A or B) [from header]\n
         rdnoise = amp rdnoise (A or B) [from header]\n
         sumval = (pixel/gain + (rdnoise/gain)^2)\n
         if(flag_flatten) sumval = sumval/(flat_pixel)^2\n
         if(flag_illumination) sumval = sumval/(illumination_pixel)^2\n
         if(flag_bias && bias_pixel > 0) sumval = sumval+ 1.0/bias_pixel\n
         if(flag_flatten && flat_pixel>0) sumval = sumval + 1.0/flat_pixel\n
         if (flag_variance = (DES_WIEGHT || DES_VARIANCE))output_pixel=1.0/sumval\n
         else (flag_variance == DES_SIGMA)output_pixel=\f$\sqrt{sumval}\f$\n
      - FOR MASKED PIXELS:\n   
         if(flag_variance == DES_SIGMA) output_pixel=1.0e+10\n
         else output.variance_pixel=0.0\n
      - NOISIFY VARIANCE IMAGE:\n
         nosource.image = smoothed (variance.image) [smoothed with kernel in minsize:maxsize]\n
         FOR UNMASKED PIXELS { variance_pixel = nosource.pixel }\n
-# INTERPOLATE\n
      - For output image and output variance image:\n
          Check for strips of BADPIX_BPM in X-direction, record the good pixels in xhi, xlow\n
          For BADPIX_BPM pixels, linearly interpolate (i.e. set to average) between [xlow:xhi]\n
          Set BADPIX_INTERP for interpolated pixels (leave rest of bitmask unperturbed)\n 
      - Rescale variance image\n
          if(flag_variance == (DES_WEIGHT || DES_VARIANCE)) variance_pixel = variance_pixel/scale_interpolate^2\n
	  else if(flag_variance == DES_SIGMA) variance_pixel = varaince_pixel*scale_interpolate\n
-# FRINGE Correction (flag_fringe)\n
      - Get output median,mode,fwhm\n
      - Output image = image - median*fringe_image\n
-# Illumination Correction (flag_illumination)\n
      - Output image = image/illumination_image\n
      - if(flag_variance) variance_image = variance_image/illumination_image^2\n
-# PhotFlatten Correction (flag_photflatten)\n
      - Output image = image/photflatten_image\n
      - if(flag_variance) variance_image = variance_image/photflatten_image^2\n
-# Output 


 */
/**
   \file imdetrend/mkmask.c
   \brief Masking/Interpolation for stars, bleedtrails, and cosmic rays.

   The \em mkmask utility can perform the following procedures:
   - Bitmask creation for known stars from USNO-B catalog (BADPIX_STAR)
   - Bleedtrails detection and correction                (BADPIX_TRAIL)
   - Cosmic ray detection and correction                 (BADPIX_CRAY)


   Usage: mkmask \<input image\> \<options\>\n

   Command Line Options:\n
  
   - -linterp\n
      Sets flag_interp = LINEAR_INTERP\n
   - -moffatinterp \<fwhm\>\n
      Sets flag_interp = MOFFAT_INTERP\n
   - -gaussinterp  \<fwhm\>\n
      Sets flag_interp = GAUSS_INTERP\n
      \note Interpolation options are currently UNUSED.\n
   - -interp_noise \<scale\>\n
      Sets interp_noise = \<scale\> (default = 1.0)\n 
   - -nointerpolate\n
      Sets flag_nointerpolate = 1\n
   - -flag_horiztrails\n
      Sets flag_horiztrails = YES; and if(Header::OBSTYPE==remap) horiztrails=1\n
   - -stars \<file\>\n
      Sets flag_stars = 1, astrodsfile=\<file\>\n
   - -crays \<file\>\n
      Sets flag_crays = 1, cray.name=\<file\>\n
   - -srcgrowrad \<rad\>\n
      Sets flag_srcgrowrad = 1, growrad=\<rad\>\n


   Procedure description:\n
-# Initialization
   - Copies native input mask
-# Star Masking (flag_stars)
   - Call mkstarmask()
-# Cosmic Ray    (flag_cray)
   - Read cosmic ray image from cray.name
   - Set BADPIX_CRAY for all non-masked pixels and cray pixel > 0
   - Set BADPIX_CRAY for any pixel within \em growrad of a BADPIX_CRAY
   - (median,mode,fwhm) determined from central part of image (less 50 pix boundary)
   - Set output pixel to (median+rand(-1:1)*fwhm)
-# Output
   
*/

/**
   \file imdetrend/mkstarmask.c
   \brief Creates star bitmask, smooths over star mask, bleedtrail processing
   \fn void mkstarmask(desimage *input,desimage *output,char *filter,char *astrodsfile,int horiztrails,float exposure,int flag_verbose,int flag_nointerpolate)
   \brief Create star bitmask , smooths over star mask, bleedtrail processing
   \param input The input image.
   \param output The output image.
   \param filter Filter vals 
   \param astrodsfile The filename from which to read the USNO-B Catalog
   \param horiztrails Detect trails in X? YES or NO
   \param exposure Exposure time 
   \param flag_verbose Verbosity level
   \param flag_nointerpolate Interpolate? YES or NO
 
   The mkstarmask function performs the following:\n
   - Create bitmask with stars from USNO-B catalog (sets BADPIX_STAR)
   - Smoothing in star regions
   - Bleedtrail processing (sets BADPIX_TRAIL)

   Procedural summary:\n
-# Initialization
   - Get star information from USNO-B catalog (readstdstable()) \note{Only looks inside image coordinates, need to consider partial collision}
   - Get filter-dependent magnitude (getmagnitude()) [Ignore all stars with mag > 13]
   - Record radii determined from: \f$\log{r_s} = -0.02382m_s + 2.14406\f$ 
-# Masking pass 1
   - For each star in image
     - Loop outward in radius (\em r_i) from star center with step .1
       - Loop around in angle with angular stepsize .25
         - Input image mean determined within \f$r_s\f$ (circlemean)
         - Set BADPIX_STAR for all output pixels within \f$r_s\f$ of star (ignores pixels with BADPIX_STAR already set)
         - Set output image weight (\em varim) to zero for flagged pixel
         - Set \em imageval integer pixel to star id (star id = index of matching star)
-# Get sky mean = mean value for \em all non-masked pixels 
-# Masking pass 2 and statistics
   - If exposure > 100, grow star mask by with (growcircleradius()) [modifies \em r_s]
   - Get median, and fwhm (retrievescale()) in 3\em r_s x 3\em r_s box (called median_s,fwhm_s) \note {comments indicate region should be 2*star region}
   - fwhm is floored to 7.0
   - reset circlemean to 0 if circlemean < median_s + fwhm_s
-# Interpolation (if enabled)
   - Set pixels inside star bitmask to median_s + rand(-1:1)*fwhm_s
-# Bleedtrails processing (if exposure > 10)
   - Call bleedingtrails() with +1 step
   - Call bleedingtrails() with -1 step
   - Call bleedtrailsx() with +1 step
   - Call bleedtrailsx() with -1 step
-# Output

   \fn void  growcircleradius(desimage *image,float *radius, float centerx,float centery,float skymean,float circlemean,int *imageval, int starno)
   \brief Grows a star's radius by a max factor of 1.7 by comparing image values with the skymean.
   \param image Input image
   \param radius original radius of star region (\em r_s) reset to new radius on output
   \param centerx X-position of star region center
   \param centery Y-position of star region center
   \param skymean Mean value for \em all non-masked pixels in image
   \param circlemean Mean value inside original star region
   \param imageval integer image with starno corresponding to flagged image pixels
   \param starno integer identifier for star

   Procedure:\n
   -# Loop over radius (\em r_i) from \em r_s to 1.7*\em r_s with stepsize .1
      - Step over circle with radius \em r_i with angular stepsize .25
        - Determine mean of region at \em r_i (possible bug here, see below)
        - Mark pixels in ring as candidates if mean > (skymean - \f$\sqrt{skymean}\f$)
        - If ring mean exceeds limit - then exit loop
   -# If \em bright pixels found out to (\em r_s + .4) then loop over candidate pixels
      - Set BADPIX_STAR for non-masked candidates
      - Set pixel imageval to starno
   -# Reset star radius to detected bright radius (regardless)

     \bug Uses sin(degrees) instead of sin(radians) in angle check
     \bug Resets radius regardless of whether additional pixels are flagged



   \fn void bleedingtrails(desimage *output,float *centerx,float *centery,double *circlemean,float *radvalues,int match,int step,int flag_nointerpolate)
   \brief Detects, masks, and interpolates over bleed trails.
   \param output Output image
   \param centerx X-coordinate array of star centers (in pixels)
   \param centery Y-coordinate array of star centers (in pixels)
   \param circlemean The mean within the "star region" (i.e. within \f${r_s}\f$)
   \param radvalues Star radii (in pixels)
   \param match Number of stars
   \param step Step (in pixels) for detecting bleed trails
   \param flag_nointerpolate Interpolate? YES or NO

   Procedure:\n
-# Loop over stars in image 
   - Starting at ymin = center_s + step*(\f${r_s}\f$ + 1), search to image edge, with stepsize=step
     - Loop over 20-pixel-wide strip in X centered on center_s
       - If pixel value > (circlemean - \f$\sqrt{\tt{circlemean}}\f$)
         - set BADPIX_TRAIL
         - set pixel weight = 0 
         - reset xmin xmax if necessary
         - if interpolation is disabled, set output image pixel to 0
-# Interpolate over bleed trails (if enabled, see flag_nointerpolate)
   - Form region enclosing bleedtrail: rectangular region with 3*(bleed trail width in X), Y extent in Y.
   - Determine median,mode,fwhm of non-masked pixels in rectangular region 3 * width bleed trail, enclosing bleedtrail
   - Set masked pixels to (median + rand(-1:1)*fwhm)
-# Return

   \note Each star's bleedtrail is limited to 19000 pixels
   \note Not sure what this will do if matching stars are close together 
   \note Could use mask-directed morphology to detect/correct star bleedtrails
   \note Possibly just threshholding and using binary morphology

*/

/**
   \file imdetrend/mkflatcor.c
   \brief Combine (dome) flat frames into a composite flat field correction. 

   The \em mkflatcor utility takes a list of input FITS flat images and combines them into
   a single flat field correction image.

   Usage: mkflatcor \<input list\> \<output flatcor image\> \<options\>\n

   Command line arguments:\n
   - -bias \<image\>\n
      Sets flag_bias=YES, bias.name=\<image\>\n
   - -pupil \<image\>\n
      Sets flat_pupil=YES, pupil.name=\<image\>\n
   - -flattype \<type\>\n
      Indicates whether to make a master twilight flat\n
      Must be one of "flatcor" or "tflatcor"; Sets flag_twilight if \<type\>="tflatcor"\n
   - -variancetype \<type\>\n
      Must be one of SIGMA, WEIGHT; Sets flag_variance=(DES_SIGMA | DES_WEIGHT)\n
   - -image_compare \<template image\>\n
      Specifies an image to compare against for verification.\n
      Sets flag_compare=YES, compare.name=\<template image\>\n
   - -verbose \<level\>\n
      Sets verbosity level; flag_verbose=\<level\> [0-3]\n
   - -overscantype \<type\>\n 
      Specify type of overscan; Sets overscantype;  Must be in \<0 - 4\>, defaults to 0.\n
      0=Median,1=3rd order poly, 2=2nd order poly,3=3rd order spline,4=3rd order spline (other=Mean)\n
   - -average\n
      Specify combine type. Sets flag_combine=AVERAGE\n
   - -avisgclip \<sigma\>\n
      Specify combine type. Sets flag_combine=AVISGCLIP; avisgclip_sigma=\<sigma\>\n
      /note UNUSED\n
   - -median\n
      Specify combine type. Sets flag_combine=MEDIAN\n; (this is the default)\n
   - -scale \<region\>="Xmin:Xmax,Ymin:Ymax", -noscale\n
      Sets flag_scale=YES; scaleregionn=[Xmin,Xmax,Ymin,Ymax]\n
   - -noscale
      Sets flag_scale=NO\n
   - -bpm \<image\>\n
      Sets bad pixel map image.\n  
      No native mask is respected. Sets flag_bpm=YES, bpm.name=\<image\>\n


      Procedure:\n
-# Initialization
   - Read input images, collect params from header
   - Copy BPM into place
-# Corrections applied if needed/specified
   - Call overscan() if not yet done
   - Bias subtraction if flag_bias
   - Pupil scaling if flag_pupil
-# Scaling if flag_scale
   - Get image statistics in scaleregion
   - For twilight flat: if image pixel outside "main lobe" statistics, set to median+rand(-1:1)*fwhm\n
-# Combine Flat frames
   - Straightforward combine pixels from all frames according to flag_combine (i.e. MEAN or MEDIAN)
   - if (!flag_twilight) Normalize resulting flat with final_median = median over combined flat
-# Form variance image\n
   - Variance image zeroed by default
   - Operate across all unmasked pixels on all images
     flag_variance_method==VARIANCE_DIRECT {\n
   - Hard coded central region: [800,1200)x[1500,2500)\n
   - VAL = input_image_pixel/(image_median*final_median) - output_image_pixel
   - Get stddev of VAL over hard coded central region over all input images; sigma1 = 3.5*stddev
   - Clip sigma1 to 3.5e-2
   - Hard coded kernel [(2*VARIANCE_DELTAPIXEL+1)x(2*VARIANCE_DELTAPIXEL+1)
   - Set stddev of VAL over hard coded kernel over all input images
   - Clip sigma2 to sigma1/3.5*Ninputimages, and lower limit 1e-6
   - if(flag_variance == (DES_VARIANCE || DES_WEIGHT)) output_variance_pixel = 1/sigma2\n
     if(flag_variance == DES_SIGMA) output_variance_pixel = sqrt(sigma2) w/lower limit = 1e-10\n
     } END VARIANCE_DIRECT SECTION\n
     flag_variance_method===VARIANCE_CCD {\n
   - rdnoise and gain specific to amp and chip
   - VAL = (input_pixel/gain + (rdnoise/gain)^2 + 1.0/bias_variance_pixel)/(input_image_median*final_median)^2
   - variance = (Mean of VAL over all images)/Nimages (contrained to lower limit 1e-6)
   - if(flag_variance == (DES_VARIANCE || DES_WEIGHT)) output_variance_pixel = 1/variance\n
     if(flag_variance == DES_SIGMA) output_variance_pixel = sqrt(variance)\n
     } END VARIANCE_CCD SECTION\n  
-# Test against Template if specified
   - call image_compare()
-# Output combined master flat

 */

/**
   \file imdetrend/mkbpm.c
   \brief Creates basic Bad Pixel Mask (sets BADPIX_BPM)
  
   The \em mkbpm utility takes a master flat image, a bias image, and user specified limits and 
   produces a Bad Pixel Mask (BPM) image.\n

   Usage: mkbpm \<flatcor input\> \<biascor input\> \<bpm output\> \<options\>
   
   Command line options:\n
   - -flatmax \<value\>\n
      Sets flatmax = \<value\>, defaults to 3.0\n
   - -flatmin \<value\>\n
      Sets flatmin = \<value\>, defaults to 0.1\n
   - -biasmax \<value\>
      Sets biasmax = \<value\>, defaults to 1000\n
   - -image_compare \<image\>\n
      Image to compare against for test.\n
      Sets flag_compare=YES; compare.name=\<image\>\n
   - -verbose \<verblevel\> [0-3]\n
   - -mask_edges Sets flag_mask_edges = YES\n

   Procedure:\n
-# Initialization
   - Read input flat
   - Read input bias
-# Masking
   - Set BADPIX_BPM for any flat image pixel falling outside (flatmin,flatmax)
   - if(flag_bias) Set BADPIX_BPM for any bias image pixel greater than biasmax
   - if(flag_mask_edges) Set BADPIX_BPM for 2 pixel border  
-# Comparison
   - Compare mask against comparison image, if it exists
-# Output
 */

/**
   \file utils/imarithsubs.c
   \brief Image arithmetic subroutines

   \fn void overscan(desimage *data,desimage *output,int flag_verbose,int overscantype)
   \brief Perform overscan processing
   \param data Input, unprocessed image
   \param output Output, overscan processed image
   \param flag_verbose Verbosity level
   \param overscantype Type of overscan: (0,1,2,3,4,)=(MEDIAN,Poly3,Poly2,Spline2,Spline3,MEAN)
 
   Trims off overscanned region and scales correct remaining pixels.
   
   \fn void OverScan(desimage *input_image,desimage *output_image,overscan_config osconfig,int flag_verbose)
   \brief Ricardo's new overscan method
   \param input_image Input, unprocessed image
   \param output_image Output, overscanned/trimmed image
   \param osconfig Overscan parameter object
   \param flag_verbose Verbosity level

   Trims off overscan region and scales correct remaining pixels.
   \verbatim 
    Ricardo's new overscan function.

    *******************************************************************
    ******************** Overscan correction ************************** 
    - osconfig.sample    -1  MEDIAN                                       
                          0  MEAN                                        
                          1  MEAN W/MIN MAX REJECTION                   
                                                                    
    - osconfig.function  -N  SPLINE FITTING W/ N lines/cols TO AVERAGE     
                             OR MEDIAN (given by overscansample)       
                          0  LINE_BY_LINE                                  
                          N  POLYNOMIAL FIT W/ N POINTS TO AVERAGE OR      
                             MEDIAN                                        
                                                                     
    - osconfig.order      1 < n < 6                                        
                                                                     
    - osconfig.trim       N TRIM THE TWO EDGES OF OVERSCAN BY N PIXELS     
    ******************************************************************* 
   \endverbatim


   \fn void retrievescale(desimage *image,int *scaleregionn,float *scalesort,int flag_verbose,
   float *scalefactor, float *mode, float *sigma)
   \brief Gets median,mode, and fwhm of unmasked pixels
   \param image Input image
   \param scaleregionn Indicates subregion of image [xmin,xmax,ymin,ymax]
   \param scalesort  Sorted vector of pixel values [lowest value...... highest value]
   \param flag_verbose Verbosity level 
   \param scalefactor Set to median on output
   \param mode Set to mode on output
   \param sigma Set to fwhm on output

   Procedure:
-# Populate scalesort with unsorted values
-# Sort scalesort with shellsort
-# Return with error if npixels < 100
-# Calculate median:
   - if 
 */
