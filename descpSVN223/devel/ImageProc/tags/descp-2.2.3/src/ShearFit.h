
#ifndef SHEAR_FIT2_H
#define SHEAR_FIT2_H
#include <vector>
#include "Math/Minimizer.h"

class ShearFit : public ROOT::Math::IMultiGenFunction {
//class ShearFit : public ROOT::Math::IBaseFunctionMultiDim {
   

public:
  ShearFit():quad(false) { }
  
  
  std::vector<double> Fit(const std::vector<double> *_dif,
			  const std::vector<double> *_x,
			  const std::vector<double> *_y,
			  std::vector<double> &errs);
  
  // Complete the IMultiGenFunction interface
  unsigned int                   NDim() const {return 3;}
  double                         DoEval(const double* p) const;
  ROOT::Math::IMultiGenFunction* Clone() const { return 0;}
  bool quad;  
private:
  std::vector<double> diff;
  std::vector<double> x;
  std::vector<double> y;
  std::vector<double> par;
  int entries;

  
  
};
  
#endif
////////////////////////////////////////////////////////////////////////
