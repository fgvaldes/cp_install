/*  This code is run after all the object ingestions for a particular
/*  nite are complete.
/*
*/
#include "imageproc.h"
#include <unistd.h>

main(argc,argv)
	int argc;
	char *argv[];
{
	char	sqlcall[800],binpath[800],nite[50],dblogin[500],sqlfile[100],
		dbtable[100];
	int	i,flag_quiet=0;
	FILE	*pip,*out;
	void	select_dblogin();

  	if ( argc < 2 ) {
    	  printf ("Usage: %s <nite>\n", argv [0]);
	  printf("  Options:\n");
	  printf("  -bin <path>\n");
	  printf("  -quiet\n");
    	  exit(0);
  	}
	
	/* ************************************************************* */
	/* *************  process the command line ********************* */
	/* ************************************************************* */
	sscanf(argv[1],"%s",nite);
	for (i=2;i<argc;i++) {
	  if (!strcmp(argv[i],"-quiet")) flag_quiet=1;
	  if (!strcmp(argv[i],"-bin")) {
	    i++;
	    /* read bin */
	    sscanf(argv[i],"%s",binpath);
	  }
	} 

	/* access dblogin */
	select_dblogin(dblogin,DB_READWRITE);

	sprintf(dbtable,"tmp_%s",nite);

	sprintf(sqlfile,"merge_nite_partition_%s.sqlquery",nite);
	sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S %s < %s",
	  dblogin,sqlfile);

	if (!flag_quiet) {
	  printf("  Adding primary key to %s\n",dbtable);
	  system("date");
	}
	out=fopen(sqlfile,"w");
	fprintf(out,"alter table %s add constraint pk_%s\n",
	  dbtable,nite);
	fprintf(out,"primary key (object_id) disable validate;\n");
	fclose(out);
	system(sqlcall);

	if (!flag_quiet) {
	  printf("  Merging %s into part_objects\n",dbtable);
	  system("date");
	}
	out=fopen(sqlfile,"w");
	fprintf(out,"alter table part_objects exchange partition %s\n",nite);
	fprintf(out,"with table %s\n",dbtable);
	fprintf(out,"including indexes without validation\n");
	fprintf(out,"update global indexes;\n");
	fprintf(out,"exit;\n");	
	fclose(out);
	system(sqlcall);

	if (!flag_quiet) {
	  printf("  Partition merging completen\n");
	  system("date");
	}

	return(0);
}

