/*$Id: runSExtractor.c 12836 2013-07-25 16:57:32Z ankitc $*/
/*  runSExtractor-
	a simple script that deals with compressed input data (if needed) and
	calls combinations of psfex and runSExtractor


*/

#include "imutils.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <errno.h>

#define VERSION 2.0
	/* added event calling and support for compressed files */
static const char *svn_id = "$Id: runSExtractor.c 12836 2013-07-25 16:57:32Z ankitc $";

static void system_abort_on_error(char * cmd){ 
  int status;
  errno = 0;
  status=system(cmd);
  if (status != 0) {
    /* Take all errors as fatal */
    fprintf(stderr,
	    "Error: %s:aborting... system  error: %s \n", 
	    cmd, strerror(status));
    if (errno != 0) if (errno != status) fprintf(stderr, "errno was: %s\n", strerror(errno));
    exit(status);
  }
}




int main(argc,argv)
	int argc;
	char *argv[];
{
	char	listname[1000],binpath[1000],etcpath[1000],terapixpath[1000],
	  start_outpath[1000],outpath[1000],outroot[1000],line[5000],
	  imagename[800],command[15000],root[1000],xmlfile[5000],
		comment[1000],filename[1000],exposurename[1000],event[5000],
	  linein[5000],tempname[1000],psfprofile[1000],xmlpath[5000];
        int     mkpath(),flag_scamp=0,flag_weightthreshold=0,
	  flag_detectthreshold=0,status=0,flag_gain=0,flag_xmlpath=0,
		flag_photoaperture=0,flag_detectminarea=0,
		flag_psfprofile=0,flag_version=0,i,ctr=0,
		mark1,mark2,len,imm,imnum=0,nslash=0,flag_verbose=2,
		flag_ascii=0,flag_nobpm=0,flag_check=0,flag_remap=0,
		flag_list=0,flag_reduce=0,flag_psfex=0,flag_binpath=0,
	  flag_etcpath=0,flag_terapixpath=0, flag_outpath=0,flag_fpack=0,
	  flag_model_fitting=0,flag_cp=0,flag_noweightmap=0;
	float   calc_pixscale(),weight_threshold,saturation,
		detect_threshold,gain,photoaperture,detectminarea;
	double  cd1_1,cd1_2,cd2_1,cd2_2,rho,rho_a,rho_b,cdelt1,cdelt2;
	float   pixscale1,pixscale2,fwhm,pixscale,fwhm_arcsec=0.0;
	time_t curtime=time (NULL), lsttime,seStart,seEnd;
	fitsfile *fptr;
  	FILE	*fin;
	FILE    *pip,*test;
	void  printerror();

  	if ( argc < 2 ) {
    	  printf ("Usage: %s <imagename.fits or list> -binpath <bin-path> -terapixpath <terapix-path> -etcpath <etc-path> [-outpath <output-path>]<-reduce and/or -remap and/or -scamp and/or -psfex> [-noweightmap]\n", argv [0]);
	  printf("  -psfex (output catalog for PSFEx)\n");
	  printf("  -scamp (output catalog for SCAMP)\n");
	  printf("  -reduce (use RED image)\n");
	  printf("  -remap (use REMAP image)\n");
	  printf("  For -reduce and -remap options:\n");
	  printf("          -noweightmap\n");
	  printf("          -model_fitting (do model fitting photometry)\n");
	  printf("          -ascii (output as ASCII)\n");
	  printf("          -no_bpm (not using bpm.fits as flag image)\n");
	  printf("          -check (output check images of reg.fits and obj.fits)\n");
	  printf("          -xmlpath <xmlpath>\n");
	  printf("  For -scamp option only:\n");
	  printf("          -noweightmap\n");
	  printf("          -weight_threshold <#>\n");
	  printf("          -detect_threshold <#>\n");
	  printf("  For -psfex option only:\n");
	  printf("          -photo_aperture <#>\n");
	  printf("          -detect_minarea <#>\n");
	  printf("          -gain <#>\n");
	  printf("  -version\n");
	  printf("  -verbose <#>\n");
    	  exit(0);
  	}
	
	/* ***************************************************** */
	/* ************** output version  ********************** */
	/* ***************************************************** */

	if (!strcmp(argv[1],"-version")) {
	    printf("%s: Version %2.2f\n",argv[0],VERSION);
	    exit(0);
	}


	/* ***************************************************** */
	/* ********************** set defaults ***************** */
	/* ***************************************************** */
	sprintf(listname, "%s", argv[1]);
	sprintf(psfprofile, "%s","DEVAUCOULEURS,EXPONENTIAL");

        /* turn off buffering on stdout to avoid output collision with
           SExtractor output */
        setbuf(stdout, 0);

	/* ***************************************************** */
	/* ************* process the command line ************** */
	/* ***************************************************** */

	for (i=2;i<argc;i++) {
	  if (!strcmp(argv[i],"-verbose")) {
	    sscanf(argv[++i],"%d",&flag_verbose);
            if (flag_verbose<0 || flag_verbose>3) {
              sprintf(event,"Verbose level out of range %d. Reset to 2", flag_verbose);
              flag_verbose=2;
              reportevt(2,STATUS,3,event);
	    }
	  }
	  if (!strcmp(argv[i],"-model_fitting")) flag_model_fitting=1;
	  if (!strcmp(argv[i],"-ascii")) flag_ascii=1;
	  if (!strcmp(argv[i],"-no_bpm")) flag_nobpm=1;
	  if (!strcmp(argv[i],"-reduce")) flag_reduce=1;
	  if (!strcmp(argv[i],"-check")) flag_check=1;
	  if (!strcmp(argv[i],"-remap")) flag_remap=1;
	  if (!strcmp(argv[i],"-scamp"))  flag_scamp=1;
	  if (!strcmp(argv[i],"-psfex")) flag_psfex=1;
	  if (!strcmp(argv[i],"-noweightmap")) flag_noweightmap=1;
	  
	  if (!strcmp(argv[i],"-binpath")) {
	      flag_binpath=1;
	      sprintf(binpath, "%s", argv[i+1]);
	  }
	  if (!strcmp(argv[i],"-etcpath")) {
	      flag_etcpath=1;
	      sprintf(etcpath, "%s", argv[i+1]);
	  }
	  if (!strcmp(argv[i],"-xmlpath")) {
	    flag_xmlpath=1;
	    sprintf(xmlpath, "%s", argv[i+1]);
          }

	  if (!strcmp(argv[i],"-terapixpath")) {
	      flag_terapixpath=1;
	      sprintf(terapixpath, "%s", argv[i+1]);
	  }
	  if (!strcmp(argv[i],"-outpath")) {
	      flag_outpath=1;
	      sprintf(start_outpath, "%s", argv[i+1]);
	  }

	  if (!strcmp(argv[i],"-weight_threshold")) {
	    flag_weightthreshold=1;
	    sscanf(argv[i+1],"%f",&weight_threshold);
	  }
	  if (!strcmp(argv[i],"-detect_threshold")) {
	    flag_detectthreshold=1;
	    sscanf(argv[i+1],"%f",&detect_threshold);
	  }
	  if (!strcmp(argv[i],"-gain")) {
	    flag_gain=1;
	    sscanf(argv[i+1],"%f",&gain);
	  }
	  if (!strcmp(argv[i],"-photo_aperture")) {
	    flag_photoaperture=1;
	    sscanf(argv[i+1],"%f",&photoaperture);
	  }
	  if (!strcmp(argv[i],"-detect_minarea")) {
	    flag_detectminarea=1;
	    sscanf(argv[i+1],"%f",&detectminarea);
	  }
	  if (!strcmp(argv[i],"-psfprofile")) {
	    flag_psfprofile=1;
	    sscanf(psfprofile,"%s",argv[i+1]);
	  }
	}

	/* ***************************************************** */
	/* ******************** basic checks ******************* */
	/* ***************************************************** */
	if(!flag_binpath) { 
	  sprintf(event,"%s Error: -binpath is not set",argv[0]);
	  reportevt(flag_verbose,STATUS,5,event);
	  exit(0);
	}
	if(!flag_etcpath) { 
	  sprintf(event,"%s Error: -etcpath is not set",argv[0]);
	  reportevt(flag_verbose,STATUS,5,event);
	  exit(0);
	}
	if(!flag_terapixpath) { 
	  sprintf(event,"%s Error: -terapixpath is not set",argv[0]);
	  reportevt(flag_verbose,STATUS,5,event);
	  exit(0);
	}

	/* print out the time of processing */
	sprintf(event,"Running %s at %s",
	  argv[0],asctime(localtime (&curtime)));
        reportevt(flag_verbose,STATUS,3,event);
	

	/* check if single image of any flavor or a list */
	if ( !strncmp(&(argv[1][strlen(argv[1])-5]),".fits",5) ||
	  !strncmp(&(argv[1][strlen(argv[1])-8]),".fits.fz",8) ||
	  !strncmp(&(argv[1][strlen(argv[1])-8]),".fits.gz",8)) {
	  sprintf(imagename,"%s",argv[1]);
	  sprintf (event,"Processing single image %s",imagename);
          reportevt(flag_verbose,STATUS,1,event);
	  imnum=1;
	}
	else { /* expect file containing list of images */

	  /* check list file to confirm it exists */
	  if ( (fin=fopen (listname, "r")) == NULL ) {
	    sprintf (event,"List file \"%s\" not found.", listname);
            reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }  
	  imnum=0;flag_list=1;
	  while (fscanf(fin,"%s",imagename)!=EOF) {
	    imnum++;
	    if (strncmp(&(imagename[strlen(imagename)-5]),".fits",5) ||
	      !strncmp(&(argv[1][strlen(imagename)-8]),".fits.fz",8) ||
	      !strncmp(&(argv[1][strlen(imagename)-8]),".fits.gz",8)) {
	      sprintf(event,"Input file %s must contain only FITS images",
	        listname);
              reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	  }
	  fclose(fin);
    
	  /* reopen for process */
	  fin=fopen(listname,"r");

	  sprintf(event,"List %s contains %d images",listname,imnum);
          reportevt(flag_verbose,STATUS,1,event);
	}

	/* ***************************************************** */
	/* ***************************************************** */
	/* **************** cycle through images  ************** */
	/* ***************************************************** */
	/* ***************************************************** */
	for (imm=0;imm<imnum;imm++) {  
	  
	  /* initiallize strings */
	  for(i=0;i<800;i++) {
	    root[i]=0;
	    if(flag_list) imagename[i]=0;
	  }

	  /* get next image name */
	  if (flag_list) fscanf(fin,"%s",imagename);

	  /* ***************************************************** */
	  /* ******** grab the root name of the image ************ */
	  /* ******** create directory for output catalog ******** */
	  /* ***************************************************** */
	  sprintf(root,"%s",imagename);
	  len=strlen(imagename);
	  for (i=len;i>0;i--) if (!strncmp(&(imagename[i]),".fits",5)) {
	    root[i]=0;
	    break;
	  }
          if (flag_outpath) {
            len=strlen(root);
            for(mark1=len;(mark1>0) && (root[mark1] != '/');mark1--);
            for(mark2=mark1-1;(mark2>0) && (root[mark2] != '/');mark2--);
            strncpy(filename, &(root[mark1+1]), len-mark1);
            strncpy(exposurename, &(root[mark2+1]), mark1-mark2-1);
            exposurename[mark1-mark2-1]=0;
            strcpy(outpath,start_outpath);
            strcat(outpath,"/");
            strcat(outpath,exposurename);
            strcpy(outroot,outpath);
            strcat(outroot,"/");
            strcat(outroot,filename);
            if (mkpath(outroot,2)) {
              sprintf(event,"Failed to create path: %s",outpath);
              reportevt(2,STATUS,5,event);
              exit(0);
            }
            else {
              sprintf(event,"Created path: %s",outpath);
              reportevt(2,STATUS,1,event);
            }
          }
          else sprintf(outroot,"%s",root);

	  /* ***************************************************** */
	  /* ******* uncompress image if needed ****************** */
	  /* ***************************************************** */
	  
	  /* handle gzip flavor if coded in input name */
	  if (!strncmp(imagename+strlen(imagename)-3,".gz",3)) {
	    sprintf(command,"gunzip %s",imagename);
	    sprintf(event,"Gunzip'ing  %s",imagename);
	    reportevt(flag_verbose,STATUS,3,event);
	    system_abort_on_error(command);
	  }

	  /* handle fpack flavor if coded in input name */
	  if (!strncmp(imagename+strlen(imagename)-3,".fz",3)) {
	    sprintf(command,"%s/funpack   %s",terapixpath,imagename);
	    sprintf(event,"Funpack'ing  %s",imagename);
	    reportevt(flag_verbose,STATUS,3,event);
	    flag_fpack=1;
	    system_abort_on_error(command);
	  }
	  /* confirm that uncompressed image is present */
	  test=fopen(imagename,"r");
	  if (test==NULL) {
	    /* look for flavor F */
	    sprintf(tempname,"%s.fz",imagename);
	    test=fopen(tempname,"r");
	    if (test==NULL) {
	      /* look for flavor G */
	      sprintf(tempname,"%s.gz",imagename);
	      test=fopen(tempname,"r");
	      if (test==NULL) {
	        sprintf(event,"File %s not found in any flavor",imagename);
	        reportevt(flag_verbose,STATUS,5,event);
	        exit(0);
	      }
	      else fclose(test); /* flavor G exists */
	      sprintf(command,"gunzip %s.gz",imagename);
	      sprintf(event,"Gunzip'ing  %s.gz",imagename);
	      reportevt(flag_verbose,STATUS,3,event);
	      system_abort_on_error(command);
	    }
	    else fclose(test); /* flavor F exists */
	    sprintf(command,"%s/funpack   %s.fz",terapixpath,imagename);
	    sprintf(event,"Funpack'ing  %s.fz",imagename);
	    reportevt(flag_verbose,STATUS,3,event);
	    flag_fpack=1;
	    system_abort_on_error(command);
	  }
	  else fclose(test); /* uncompressed flavor exists */

	  /* ***************************************************** */
	  /* ******* extract header information ****************** */
	  /* ***************************************************** */
 
	  status=0;
	  
	  /* open the (uncompressed) image */
	  sprintf(tempname,"%s.fits[0]",root);
	  if(fits_open_file(&fptr,tempname,READONLY,&status)) {
	    sprintf(event,"Image open failed: %s",imagename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

	  /* extract SATURATE value */
	  if(fits_read_key_flt(fptr,"SATURATE",&saturation,comment,&status)) {
	      sprintf(event,"Image %s keyword SATURATE missing",imagename);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	  }
	

	  if (!flag_scamp) { /* extract pixel scale */
	    /* extract WCS CD Matrix */
	    if(fits_read_key_dbl(fptr,"CD1_1",&cd1_1,comment,&status)==
	      KEY_NO_EXIST) {
	        sprintf(event,"Image %s keyword CD1_1 missing",imagename);
	        reportevt(flag_verbose,STATUS,3,event);
	        cd1_1=0.0;
	        status=0;
	    }
	    if(fits_read_key_dbl(fptr,"CD2_1",&cd2_1,comment,&status)==
	      KEY_NO_EXIST) {
	        sprintf(event,"Image %s keyword CD1_1 missing",imagename);
	        reportevt(flag_verbose,STATUS,3,event);
	        cd2_1=0.0;
	        status=0;
	    }
	    if(fits_read_key_dbl(fptr,"CD1_2",&cd1_2,comment,&status)==
	      KEY_NO_EXIST) {
	        sprintf(event,"Image %s keyword CD1_2 missing",imagename);
	        reportevt(flag_verbose,STATUS,3,event);
	        cd1_2=0.0;
	        status=0;
	    }
	    if(fits_read_key_dbl(fptr,"CD2_2",&cd2_2,comment,&status)==
	      KEY_NO_EXIST) {
	        sprintf(event,"Image %s keyword CD2_2 missing",imagename);
	        reportevt(flag_verbose,STATUS,3,event);
	        cd2_2=0.0;
	        status=0;
	    }
	    if(fits_read_key_flt(fptr,"FWHM",&fwhm,comment,&status)==
	      KEY_NO_EXIST) {
	        sprintf(event,"Image %s keyword FWHM missing",imagename);
	        reportevt(flag_verbose,STATUS,3,event);
	        fwhm=0.0;
	        status=0;
	    }
	    /* Grab PIXSCAL keywords if they exist */
	    if (fits_read_key_flt(fptr,"PIXSCAL1",&pixscale1,comment,&status)==
	      KEY_NO_EXIST){
	        sprintf(event,"Image %s keyword PIXSCAL1 missing",imagename);
	        reportevt(flag_verbose,STATUS,3,event);
	        pixscale1=0.0;
	        status=0;
	    }
	    if (fits_read_key_flt(fptr,"PIXSCAL2",&pixscale2,comment,&status)==
	      KEY_NO_EXIST){
	       sprintf(event,"Image %s keyword PIXSCAL2 missing",imagename);
	        reportevt(flag_verbose,STATUS,3,event);
	        pixscale2=0.0;
	        status=0;
	    }

	    /* convert the pixel to arcsec */
	    pixscale=calc_pixscale(cd1_1,cd1_2,cd2_1,cd2_2,pixscale1,pixscale2);
	    fwhm_arcsec=fwhm*pixscale;
	    sprintf(event,"Image= %s & Pixscale= %.3f &  FWHM = %.2f pixel or %.2f arcsec ",
	      imagename,pixscale,fwhm,fwhm_arcsec);
	    reportevt(flag_verbose,QA,1,event);
	  }

	  /* close the image */
	  if(fits_close_file(fptr,&status)) {
	    sprintf(event,"Image %s close failed",imagename);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

	  /* ***************************************************** */
	  /* ******** run SExtractor and PSFEX  ****************** */
	  /* ***************************************************** */


	  
	  if(flag_psfex) {
	    /* run Sextractor to prepare the catalogs for psfex */
	    sprintf(command,"%s/sex %s.fits[0] -c %s/default.sex -CATALOG_NAME %s_psfcat.fits -CATALOG_TYPE FITS_LDAC -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s.fits[2] -PARAMETERS_NAME %s/sex.param_psfex -FILTER_NAME %s/sex.conv  -STARNNW_NAME %s/sex.nnw",terapixpath,root,etcpath,outroot,root,etcpath,etcpath,etcpath);
	    sprintf(command,"%s -SATUR_LEVEL %5.4f ",command,saturation);
	    if(flag_gain)
	      sprintf(command,"%s -GAIN %5.4f ",command,gain);
	    if(flag_detectminarea)
	      sprintf(command,"%s -DETECT_MINAREA %2.2f ",command,detectminarea);
	    else
	      sprintf(command,"%s -DETECT_MINAREA 3 ",command);
	    sprintf(event,"%s",command);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* execute SExtractor */
	    /*  system_abort_on_error(command);	    */
	    pip = popen(strcat(command,"2>&1"),"r");
	    while (fgets(line,10000,pip)!=NULL) {
	      fputs(line,stdout);
	      if (strstr(line,"Error")!=NULL) {
		sprintf(event,"error found in first SExtractor call on image %s",imagename);
		reportevt(flag_verbose,STATUS,5,event);
              }
	    }
	    pclose(pip);
	    /* run the psfex here */
	    if (flag_xmlpath) sprintf (xmlfile,"%s/psfex_%s.xml",xmlpath,filename);
	    else sprintf (xmlfile,"psfex_%s.xml",filename);
	    sprintf(command,"%s/psfex %s_psfcat.fits -c %s/default.psfex -WRITE_XML Y -XML_NAME %s  ",terapixpath,outroot,etcpath,xmlfile);
	    
	    sprintf(event,"%s",command);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* execute psfex */
	    system_abort_on_error(command);
	  }

	  /* ***************************************************** */
	  /* ********** run SExtractor for REMAP images ********** */
	  /* ***************************************************** */
	  if(flag_remap) {
	    sprintf(command, "%s/sex %s.fits[0] -c %s/sex.config ", 
			terapixpath,root,etcpath);
	    sprintf(command, "%s -FILTER_NAME %s/sex.conv -STARNNW_NAME %s/sex.nnw   ", 
			command,etcpath,etcpath);
	    sprintf(command, "%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s.fits[2] ", 
			command,root);
	    if (fwhm_arcsec>0.0) sprintf(command, "%s -SEEING_FWHM %2.3f ", 
			command,fwhm_arcsec);
	    if(!flag_nobpm) sprintf(command, "%s -FLAG_IMAGE %s.fits[1] ", command, root);
	    sprintf(command, "%s -CATALOG_NAME %s_cat.fits", command,root);
	    sprintf(command,"%s -SATUR_LEVEL %5.4f ",command,saturation);
	    if(!flag_model_fitting) 
	      sprintf(command,"%s -PARAMETERS_NAME %s/sex.param_nopsfex ",
	    	command,etcpath);
	    else
	      sprintf(command,"%s -PARAMETERS_NAME %s/sex.param -PSF_NAME %s_psfcat.psf  ",
		command,etcpath,root);

	    sprintf(event,"%s",command);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* execute SExtractor */
	    /*  system (command); */
	    pip = popen(strcat(command,"2>&1"),"r");
	    while (fgets(line,10000,pip)!=NULL) {
	      fputs(line,stdout);
	      if (strstr(line,"Error")!=NULL) {
		sprintf(event,"error found in second SExtractor call on image %s",imagename);
		reportevt(flag_verbose,STATUS,5,event);
	      }
	    }
	    pclose(pip);

	  }
	  
	  /* ***************************************************** */
	  /* ********** run SExtractor for RED images ************ */
	  /* ***************************************************** */
	  if(flag_reduce) {
	    sprintf(command, "%s/sex %s.fits[0] -c %s/sex.config ", 
		    terapixpath,root,etcpath);
	    sprintf(command, "%s -FILTER_NAME %s/sex.conv -STARNNW_NAME %s/sex.nnw", 
		    command,etcpath,etcpath);
	    if (!flag_noweightmap)
	      sprintf(command,
	        "%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s.fits[2] ",
		command, root);
	    if (fwhm_arcsec>0.0) sprintf(command, "%s -SEEING_FWHM %2.3f ", 
		    command,fwhm_arcsec);

	    if(flag_ascii) sprintf(command, "%s -CATALOG_TYPE ASCII ", command);
	    if(!flag_nobpm) sprintf(command, "%s -FLAG_IMAGE %s.fits[1] ", command, root);
	    if(flag_ascii) sprintf(command, "%s -CATALOG_NAME %s.cat", command,outroot);
	    else sprintf(command, "%s -CATALOG_NAME %s_cat.fits", command,outroot);
	    sprintf(command,"%s -SATUR_LEVEL %5.4f ",command,saturation);
	    if(!flag_model_fitting) 
	      sprintf(command,"%s -PARAMETERS_NAME %s/sex.param_nopsfex ",command,etcpath);
	    else
	      sprintf(command,"%s -PARAMETERS_NAME %s/sex.param -PSF_NAME %s_psfcat.psf ",command,etcpath,outroot);
	    
	    sprintf(event,"%s",command);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* execute SExtractor */
	    time(&seStart);
	    /*	    system (command); */
	    pip = popen(strcat(command,"2>&1"),"r");
            while (fgets(line,10000,pip)!=NULL) {
              fputs(line,stdout);
              if (strstr(line,"Error")!=NULL) {
                sprintf(event,"error found in second SExtractor call on image %s",imagename);
                reportevt(flag_verbose,STATUS,5,event);
              }
            }
            pclose(pip);

          

	    time(&seEnd);
	    sprintf (event,"Running SExtractor on %s took %d seconds\n", imagename, (int) (seEnd - seStart));
	    reportevt(flag_verbose,STATUS,1,event);
	  }

	  /* ***************************************************** */
	  /* ******** run SExtractor for SCAMP catalogs  ********* */
	  /* ***************************************************** */
	  if(flag_scamp) {
	    sprintf(command,"%s/sex %s.fits[0] -c %s/sexforscamp.config -CATALOG_NAME %s_scamp.fits -CATALOG_TYPE FITS_LDAC -FLAG_IMAGE %s.fits[1] -PARAMETERS_NAME %s/sex.param_scamp -FILTER_NAME %s/sex.conv -STARNNW_NAME %s/sex.nnw ",
		 terapixpath,root,etcpath,outroot,root,etcpath,etcpath,etcpath);
	    if (!flag_noweightmap)
	      sprintf(command,
	        "%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s.fits[2] ",
		command, root);
	    if(flag_weightthreshold)
	      sprintf(command,"%s -WEIGHT_THRESH %2.4f ",command,
		weight_threshold);
	    sprintf(command,"%s -SATUR_LEVEL %5.4f ",command,saturation);
	    if(flag_detectthreshold)
	      sprintf(command,"%s -DETECT_THRESH %2.4f ",command,
	 	detect_threshold);
	    sprintf(event,"%s",command);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* execute SExtractor */
	    system_abort_on_error(command);	    

	    /* extract the FWHM (in pixel) from the catalog file and insert back to image using fwhm.c */
	    sprintf(command,"%s/fwhm %s_scamp.fits %s.fits[0]",binpath,outroot,root);
	    sprintf(event,"%s",command);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* execute fwhm */
	    system_abort_on_error(command);	    
	  }

	if (flag_fpack){
	  sprintf(command,"rm -f   %s",imagename);
	  sprintf(event,"Removing uncompressed image  %s",imagename);
	  reportevt(flag_verbose,STATUS,3,event);
	  system_abort_on_error(command);
	}
	flag_fpack=0;

	} /* end of image loop */

	/* close file if processing list */
  	if(flag_list) fclose(fin);

	lsttime=time (NULL);

	/*remove fits file */



	/* print out the time of processing */
	sprintf(event,"Finished %s at %s",
	  argv[0],asctime(localtime (&lsttime)));
        reportevt(flag_verbose,STATUS,3,event);
	
	return 0;
}


float calc_pixscale(cd1_1,cd1_2,cd2_1,cd2_2,pixscale1,pixscale2)
  double cd1_1, cd1_2, cd2_1, cd2_2;
  float pixscale1, pixscale2;
{
  int flag_pixscale_exist;
  float pixscale,pixscale_tem = 0.0;
  double rho_a,rho_b,rho,cdelt1,cdelt2;

  flag_pixscale_exist=NO;

  /* if the pixscal keywords exist, then take the average */
  if(pixscale1!=0.0 && pixscale1!=0.0) {
    pixscale_tem=0.5*(pixscale1+pixscale2);
    flag_pixscale_exist=YES;
  }

  /* evaluate rho_a and rho_b as in Calabretta & Greisen (2002), eq 191 */
  if(cd2_1>0) rho_a=atan(cd2_1/cd1_1);
  else if(cd2_1<0) rho_a=atan(-cd2_1/-cd1_1);
  else rho_a=0.0;
  
  if(cd1_2>0) rho_b=atan(cd1_2/-cd2_2);
  else if(cd1_2<0) rho_b=atan(-cd1_2/cd2_2);
  else rho_b=0.0;
  
  /* evaluate rho and CDELTi as in Calabretta & Greisen (2002), eq 193 */
  rho=0.5*(fabs(rho_a)+fabs(rho_b));
  //rho=0.5*(rho_a+rho_b);
  cdelt1=cd1_1/cos(rho);
  cdelt2=cd2_2/cos(rho);
  
  /* convert the pixel to arcsec */
  pixscale=0.5*(fabs(cdelt1)+fabs(cdelt2))*3600;

  if(flag_pixscale_exist==YES) { /* check if the pixscale is within 10% of the values given in header */
    if(fabs(pixscale_tem-pixscale)/pixscale_tem > 0.10) 
      pixscale=pixscale_tem;
  }

  return (pixscale);
}
#undef VERSION
