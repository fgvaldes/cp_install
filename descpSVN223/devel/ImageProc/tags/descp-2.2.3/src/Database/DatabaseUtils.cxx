#include "DatabaseUtils.h"

using namespace Db;


void Db::Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters)
{
  // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }



}

string Db::GetMaxCatRun(DatabaseInterface &db,const string &tile)
{
 string cmd=Form("select distinct(run) from catalog where "
                 "tilename='%s' order by run desc",
                 tile.c_str());
  DatabaseResult rec;
  rec.Query(db,cmd);
  bool ok=rec.NextRow();
  if(!ok) {
    cout<<"No run in catalog for tile:"<<tile<<endl;
    return "";
  }
  else return rec.GetString(0);
}


bool Db::IsPsf(DatabaseInterface &db,const string &run)
{

  //# TODO - what if the images come from a different run then
  //         the catalog.  Maybe in the getse it can check to find
  //         the latest one
  bool psf=false;
  string cmd=Form("select a.imagetype from image a,coadd_src b,coadd c where c.id=b.coadd_imageid "
                  "and a.id=b.src_imageid and c.run like '%s%%' and rownum <2",run.c_str());
  DatabaseResult rec;
  rec.Query(db,cmd);
  rec.NextRow();
  string parent=rec.GetString(0);
  if(parent=="remap") psf=false;
  else if(parent=="norm") psf=true;
  else {
    cout<<"IsPSF::Error Not a valid parent: "<<parent<<endl;
  }

  return psf;
}

bool Db::GetSEImages(DatabaseInterface &db,vector<DBImage> &vec,
                     const string &tile,const string &run,const string &band)
 {

   // if run is not in the coadd get the most recent previous
   // to this run
    DatabaseResult rec;
    string cmd=Form("select tilename from coadd where run='%s'",run.c_str());
    rec.Query(db,cmd);
    bool ok=rec.NextRow();
    // No Coadd from this run
    string coadd_run=run;

    if(!ok) {

      cmd=Form("select distinct(run) from coadd where "
               "tilename='%s' and run<'%s' order by run desc",
               tile.c_str(),run.c_str());
      rec.Query(db,cmd);
      bool found_run=rec.NextRow();
      if(!found_run) {
        cout<<"No run in coadd for tilename="<<tile<<endl; 
        return false;
      }
      else {
        coadd_run=rec.GetString(0);
        cout<<"Changing to latest previous run in coadd"<<endl;
        cout<<"  Coadd run="<<coadd_run<<endl;
      }

    } 


   bool psf=IsPsf(db,coadd_run);

   string db_cmd;
   if(!psf) {
     db_cmd=Form("from image, coadd_src b,coadd c,exposure d where "
                 "b.coadd_imageid=c.id and image.exposureid=d.id "
                 "and image.id=b.src_imageid and c.run like '%s%%' ",
                 coadd_run.c_str());
   }
   else {
     db_cmd=Form( "from image,coadd_src b,coadd c,exposure d,image e "
                "where b.coadd_imageid=c.id and image.exposureid=d.id "
                "and e.id=b.src_imageid and c.run like '%s%%' "
                "and image.id=e.parentid",
                coadd_run.c_str());
  }

   if(!band.empty()) db_cmd+=Form(" and image.band='%s'",band.c_str());
  // get list of images
   std::cout<<db_cmd<<endl;
   ok=FillVec(db,db_cmd,vec,true);

  return ok;
 }

bool Db::GetSEShearObjects(DatabaseInterface &db,const DBImage &image,
                      vector<DBObject > &ob_vec, vector<DBShear> &shear_vec, 
                      const bool cal)
{

  double zp=0;
  if(cal) {
    string cmd=Form("select a.mag_zero,a.sigma_mag_zero,c.photflag from "
                    " zeropoint a, image b, exposure c where a.imageid=%d and "
                    " b.id=a.imageid and b.exposureid=c.id order by insert_date",
                    image.id);

    DatabaseResult rec;
    rec.Query(db,cmd);
    bool ok=rec.NextRow();
    if(ok) {
      zp=rec.GetDouble(0);
    }
  }

  string where;
  if(image.imagetype=="remap") {
    where=Form("objects.imageid=%d and wl_shear.object_id=objects.object_id",image.parentid);
  }
  else {
    where=Form("objects.imageid=%d and wl_shear.object_id=objects.object_id ",image.id);
  }
  bool ok=FillVec(db,where,ob_vec,shear_vec);
  
  if(cal) {
    double val=zp-image.zp;
    for(int i=0;i<ob_vec.size();i++) {
      ob_vec[i].UpdateMags(val);
    }
  }

  return ok;
}

bool Db::GetSEShearObjects(DatabaseInterface &db,const DBImage &image,
                      vector<DBSObject > &ob_vec, vector<DBShear> &shear_vec, 
                      const bool cal)
{

  double zp=0;
  if(cal) {
    string cmd=Form("select a.mag_zero,a.sigma_mag_zero,c.photflag from "
                    " zeropoint a, image b, exposure c where a.imageid=%d and "
                    " b.id=a.imageid and b.exposureid=c.id order by insert_date",
                    image.id);

    DatabaseResult rec;
    rec.Query(db,cmd);
    bool ok=rec.NextRow();
    if(ok) {
      zp=rec.GetDouble(0);
    }
  }

  string where;
  if(image.imagetype=="remap") {
    where=Form("objects.imageid=%d and wl_shear.object_id=objects.object_id",image.parentid);
  }
  else {
    where=Form("objects.imageid=%d and wl_shear.object_id=objects.object_id ",image.id);
  }
  bool ok=FillVec(db,where,ob_vec,shear_vec);
  
  if(cal) {
    double val=zp-image.zp;
    for(int i=0;i<ob_vec.size();i++) {
      ob_vec[i].UpdateMags(val);
    }
  }

  return ok;
}

void Db::sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,double &tol)
{
  int N=array.size();
  if(!weighted) {
    mean=TMath::Mean(N,&array[0]);
  }
  else {
    mean=TMath::Mean(N,&array[0],&err[0]);
  }
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }
  good=N-bad;
  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;
  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    

    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();
    newerr_array.clear();

    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    curerr_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;


    
  }
 

  
  return;

}



