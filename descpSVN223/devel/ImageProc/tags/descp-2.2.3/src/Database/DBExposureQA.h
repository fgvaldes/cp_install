#ifndef DBEXPOSUREQA_H
#define DBEXPOSUREQA_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class DBExposureQA: public DatabaseBase {

public:

 DBExposureQA();
 ~DBExposureQA() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 float astromchi2_ref;
 float astromchi2_ref_highsn;
 float astromcorr_ref;
 float astromcorr_ref_highsn;
 int astromndets_ref;
 int astromndets_ref_highsn;
 float astromoffset_ref_1;
 float astromoffset_ref_2;
 float astromoffset_ref_highsn_1;
 float astromoffset_ref_highsn_2;
 float astromsigma_ref_1;
 float astromsigma_ref_2;
 float astromsigma_ref_highsn_1;
 float astromsigma_ref_highsn_2;
 int exposureid;
 string nite;
 string run;


ClassDef(DBExposureQA,1);
};
}
#endif
