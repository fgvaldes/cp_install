#ifndef DATABASERESULT_H
#define DATABASERESULT_H

#include "DatabaseInterface.h"
#include "TSQLStatement.h"
#include <string>

using namespace std;

namespace Db  {

class DatabaseResult {

 public:
  DatabaseResult();
  ~DatabaseResult() {
    if(!stmt) delete stmt;
  }

  bool NextRow();
  bool Query(DatabaseInterface &db,const string &cmd);
  bool Process(DatabaseInterface &db,const string &cmd);
  double GetDouble(const int &row) {return stmt->GetDouble(row);}
  long GetLong(const int &row) {return stmt->GetLong(row);}
  string GetString(const int &row) {string r= stmt->GetString(row);return r;}
  int GetInt(const int &row) {return stmt->GetInt(row);}
  double GetUInt(const int &row) {return stmt->GetUInt(row);}
  bool IsNull(const int &row) {return stmt->IsNull(row);}

  int GetNumCols() {return num_fields;}

  double GetDouble(const string &col);
  int GetInt(const string &col);
  string GetString(const string &col);
  long GetLong(const string &col);
  
private:

  TSQLStatement *stmt;
  int current_row;
  vector<string> columns;
  int num_fields;

  ClassDef(DatabaseResult,0)

};

}
#endif
