#ifndef DATABASEUTIL_H
#define DATABASEUTIL_H

#include "DatabaseInterface.h"
#include "DatabaseBase.h"
#include "DatabaseResult.h"
#include "DBImage.h"
#include "PGImage.h"
#include "PGObject.h"
#include "DBObject.h"
#include "DBSObject.h"
#include "DBShear.h"
#include "DBTruth.h"

#include "TTree.h"
#include "TFile.h"

#include <string>
#include <iostream>

#include <kdtree++/kdtree.hpp>

namespace Db {

  template<class T>
  bool FillVec(DatabaseInterface &db,const string &where,vector<T> &vec,
               const bool inc_tables=0)
  {
    //build the query
    T ob;
    string query="select ";
    for(int i=0;i<ob.tables.size();++i) {
      
      for(int j=0;j<ob.columns[i].size();++j) {
        
        query+=ob.tables[i]+"."+ob.columns[i][j]+",";
      }
    }
    query=query.substr(0,query.size()-1);
    
    if(!inc_tables) {
      query+=" from ";
      for(int i=0;i<ob.tables.size();++i) {
        query+=ob.tables[i]+",";
      }
      query=query.substr(0,query.size()-1);
     
      if(!where.empty()) {
        query+=" where "+where;
      }
    }
    else query+=" "+where;

    DatabaseResult result;
    bool ok=result.Query(db,query);
    if(!ok) return ok;
    
    while(result.NextRow()) {
      T gen_ob;
      
      gen_ob.SetVals(result);
      vec.push_back(gen_ob);
    }
    
    return true;
    
  }

  template<class T>
  bool FillTree(DatabaseInterface &db,const string &where,
                TTree *tree,T* ob,const bool inc_tables=0)
  {
    //build the query
    string query="select ";
    for(int i=0;i<ob->tables.size();++i) {
      
      for(int j=0;j<ob->columns[i].size();++j) {
        
        query+=ob->tables[i]+"."+ob->columns[i][j]+",";
      }
    }
    query=query.substr(0,query.size()-1);
    

    if(!inc_tables) {
      query+=" from ";
      for(int i=0;i<ob->tables.size();++i) {
        query+=ob->tables[i]+",";
      }
      query=query.substr(0,query.size()-1);
      if(!where.empty()) {
        query+=" where "+where;
      }
    }
    else query+=" "+where;
    
    DatabaseResult result;
    bool ok=result.Query(db,query);
    if(!ok) return ok;
    
    while(result.NextRow()) {
      T gen_ob;
      
      gen_ob.SetVals(result);
      *ob=gen_ob;
      tree->Fill();
    }
    return true;
    
  }

  template<class T1,class T2>
  bool FillVec(DatabaseInterface &db,const string &where,
               vector<T1> &vec1,vector<T2> &vec2,const bool inc_tables=0)
  {
    //build the query
    T1 ob1;
    T2 ob2;
    string query="select ";
    int first=0;
    for(int i=0;i<ob1.tables.size();++i) {
      
      for(int j=0;j<ob1.columns[i].size();++j) {
        
        query+=ob1.tables[i]+"."+ob1.columns[i][j]+",";
        first++;
      }
    }
    
    for(int i=0;i<ob2.tables.size();++i) {
      
      for(int j=0;j<ob2.columns[i].size();++j) {
        
        query+=ob2.tables[i]+"."+ob2.columns[i][j]+",";
      }
    }
    query=query.substr(0,query.size()-1);
    
    if(!inc_tables) {
      query+=" from ";
      for(int i=0;i<ob1.tables.size();++i) {
        query+=ob1.tables[i]+",";
      }
      
      for(int i=0;i<ob2.tables.size();++i) {
        query+=ob2.tables[i]+",";
      }
      
      query=query.substr(0,query.size()-1);
      query+=" where "+where;
    } 
    else query+=" "+where;
    
    DatabaseResult result;
    bool ok=result.Query(db,query);



    if(!ok) return ok;
    


    while(result.NextRow()) {
      T1 gen_ob1;
      T2 gen_ob2;
      
      gen_ob1.SetVals(result);
      gen_ob2.SetVals(result,first);
      vec1.push_back(gen_ob1);
      vec2.push_back(gen_ob2);
    }



    
    return true;
    
  }

  template<class T1,class T2,class T3>
  bool FillVec(DatabaseInterface &db,const string &where,
               vector<T1> &vec1,vector<T2> &vec2,vector<T3> &vec3,const bool inc_tables=0)
  {
    //build the query
    T1 ob1;
    T2 ob2;
    T2 ob3;
    string query="select ";
    int first=0;
    for(int i=0;i<ob1.tables.size();++i) {
      
      for(int j=0;j<ob1.columns[i].size();++j) {
        
        query+=ob1.tables[i]+"."+ob1.columns[i][j]+",";
        first++;
      }
    }
    
    int second=first;
    for(int i=0;i<ob2.tables.size();++i) {
      
      for(int j=0;j<ob2.columns[i].size();++j) {
        
        query+=ob2.tables[i]+"."+ob2.columns[i][j]+",";
        second++;
      }
    }

    for(int i=0;i<ob3.tables.size();++i) {
      
      for(int j=0;j<ob3.columns[i].size();++j) {
        
        query+=ob3.tables[i]+"."+ob3.columns[i][j]+",";
      }
    }
    query=query.substr(0,query.size()-1);
    
    if(!inc_tables) {
      query+=" from ";
      for(int i=0;i<ob1.tables.size();++i) {
        query+=ob1.tables[i]+",";
      }
      
      for(int i=0;i<ob2.tables.size();++i) {
        query+=ob2.tables[i]+",";
      }
      
      for(int i=0;i<ob3.tables.size();++i) {
        query+=ob3.tables[i]+",";
      }
      
      query=query.substr(0,query.size()-1);
      query+=" where "+where;
    }
    else query+=" "+where;

    DatabaseResult result;
    bool ok=result.Query(db,query);



    if(!ok) return ok;
    


    while(result.NextRow()) {
      T1 gen_ob1;
      T2 gen_ob2;
      T2 gen_ob3;
      
      gen_ob1.SetVals(result);
      gen_ob2.SetVals(result,first);
      gen_ob3.SetVals(result,second);
      vec1.push_back(gen_ob1);
      vec2.push_back(gen_ob2);
      vec3.push_back(gen_ob3);
    }



    
    return true;
    
  }

  template<class T1,class T2>
  bool FillTree(DatabaseInterface &db,const string &where,TTree *tree,
                T1 *ob1,T2 *ob2,const bool inc_tables=0)
  {
    //build the query
    string query="select ";
    int first=0;
    for(int i=0;i<ob1->tables.size();++i) {
      
      for(int j=0;j<ob1->columns[i].size();++j) {
        
        query+=ob1->tables[i]+"."+ob1->columns[i][j]+",";
        first++;
      }
    }
    
    for(int i=0;i<ob2->tables.size();++i) {
      
      for(int j=0;j<ob2->columns[i].size();++j) {
        
        query+=ob2->tables[i]+"."+ob2->columns[i][j]+",";
      }
    }
    query=query.substr(0,query.size()-1);
    

    if(!inc_tables) {
      query+=" from ";
      for(int i=0;i<ob1->tables.size();++i) {
        query+=ob1->tables[i]+",";
      }
      
      for(int i=0;i<ob2->tables.size();++i) {
        query+=ob2->tables[i]+",";
      }
   
      query=query.substr(0,query.size()-1);
      query+=" where "+where;
    }
    else query+=" "+where;
    
    DatabaseResult result;
    bool ok=result.Query(db,query);



    if(!ok) return ok;
    


    while(result.NextRow()) {
      T1 gen_ob1;
      T2 gen_ob2;
      
      gen_ob1.SetVals(result);
      gen_ob2.SetVals(result,first);
      *ob1=gen_ob1;
      *ob2=gen_ob2;
      tree->Fill();
    }



    
    return true;
    
  }

  template<class T1,class T2,class T3>
  bool FillTree(DatabaseInterface &db,const string &where,TTree *tree,
                T1 *ob1,T2 *ob2, T3 *ob3,const bool inc_tables=0)
  {
    //build the query
    string query="select ";
    int first=0;

    for(int i=0;i<ob1->tables.size();++i) {
      
      for(int j=0;j<ob1->columns[i].size();++j) {
        
        query+=ob1->tables[i]+"."+ob1->columns[i][j]+",";
        first++;
      }
    }

    int second=first;    
    for(int i=0;i<ob2->tables.size();++i) {
      
      for(int j=0;j<ob2->columns[i].size();++j) {
        
        query+=ob2->tables[i]+"."+ob2->columns[i][j]+",";
        second++;
      }
    }

    for(int i=0;i<ob3->tables.size();++i) {
      
      for(int j=0;j<ob3->columns[i].size();++j) {
        
        query+=ob3->tables[i]+"."+ob3->columns[i][j]+",";
      }
    }
    query=query.substr(0,query.size()-1);
    

    if(!inc_tables) {
      query+=" from ";
      for(int i=0;i<ob1->tables.size();++i) {
        query+=ob1->tables[i]+",";
      }
      
      for(int i=0;i<ob2->tables.size();++i) {
        query+=ob2->tables[i]+",";
      }
      
      for(int i=0;i<ob3->tables.size();++i) {
        query+=ob3->tables[i]+",";
      }
      
      query=query.substr(0,query.size()-1);
      query+=" where "+where;
    }  
    else query+=" "+where;
    
    DatabaseResult result;
    bool ok=result.Query(db,query);



    if(!ok) return ok;
    


    while(result.NextRow()) {
      T1 gen_ob1;
      T2 gen_ob2;
      T3 gen_ob3;
      
      gen_ob1.SetVals(result);
      gen_ob2.SetVals(result,first);
      gen_ob3.SetVals(result,second);
      *ob1=gen_ob1;
      *ob2=gen_ob2;
      *ob3=gen_ob3;
      tree->Fill();
    }



    
    return true;
    
  }

  template<class T1,class T2,class T3>
  bool FillTreeTruth(DatabaseInterface &db,const string &where,TTree *tree,
                     T1 *ob1,T2 *ob2, T3 *ob3,DBTruth *truth,
                     const bool inc_tables=0)
  {
    //build kdtree from file
    typedef KDTree::KDTree<3,DBTruth > DBT_KDTree;
    typedef std::pair<DBT_KDTree::iterator,double> DBT_Match;
    TFile truth_file("gsn_aug.root");
    TTree *truth_tree=(TTree*)truth_file.Get("tree");
    truth_tree->SetBranchAddress("n",&truth);
    bool t_match;
     tree->Branch("match",&t_match,"match/O");
     DBT_KDTree truth_vec;
    int i=0;
    cout<<"Loading Truth Table"<<endl;
    while(truth_tree->GetEntry(i)) {
      i++;
      if(i%100000==0) cout<<"On value "<<i<<endl;
      truth->SetRADec(truth->ra_dbl,truth->dec_dbl);
      truth_vec.insert(*truth);
    }
//     vector<DBTruth> tvec;
//    string twhere="gsn_oct10_truth.ra_dbl<335.410461  and  gsn_oct10_truth.ra_dbl>334.400574 and  gsn_oct10_truth.dec_dbl<-41.424419 and  gsn_oct10_truth.dec_dbl>-42.174351";
//    bool okd=FillVec(db,twhere,tvec);
   
//    for(int i=0;i<tvec.size();i++) {
     
//      truth_vec.insert(tvec[i]);
//    }
//    tvec.clear();


   double tol=1;
   tol*=1./3600*TMath::DegToRad();

   //build the query
   string query="select ";
   int first=0;
   
   for(int i=0;i<ob1->tables.size();++i) {
     
     for(int j=0;j<ob1->columns[i].size();++j) {
       
       query+=ob1->tables[i]+"."+ob1->columns[i][j]+",";
       first++;
     }
   }
   
   int second=first;    
   for(int i=0;i<ob2->tables.size();++i) {
     
     for(int j=0;j<ob2->columns[i].size();++j) {
       
       query+=ob2->tables[i]+"."+ob2->columns[i][j]+",";
       second++;
     }
   }
   
   for(int i=0;i<ob3->tables.size();++i) {
     
     for(int j=0;j<ob3->columns[i].size();++j) {
       
       query+=ob3->tables[i]+"."+ob3->columns[i][j]+",";
     }
   }
   query=query.substr(0,query.size()-1);
   

    if(!inc_tables) {
      query+=" from ";
      for(int i=0;i<ob1->tables.size();++i) {
        query+=ob1->tables[i]+",";
      }
      
      for(int i=0;i<ob2->tables.size();++i) {
        query+=ob2->tables[i]+",";
      }
      
      for(int i=0;i<ob3->tables.size();++i) {
        query+=ob3->tables[i]+",";
      }
      
      query=query.substr(0,query.size()-1);
      query+=" where "+where;
    }  
    else query+=" "+where;
   
    DatabaseResult result;
   
    bool ok=result.Query(db,query);
    if(!ok) return ok;
    
    while(result.NextRow()) {
      T1 gen_ob1;
      T2 gen_ob2;
      T3 gen_ob3;
      gen_ob1.SetVals(result);
      gen_ob2.SetVals(result,first);
      gen_ob3.SetVals(result,second);
      *ob1=gen_ob1;
      *ob2=gen_ob2;
      *ob3=gen_ob3;

      DBTruth dummy;
      dummy.SetRADec(gen_ob1.ra,gen_ob1.dec);
      DBT_Match match=truth_vec.find_nearest(dummy,tol);
    
      if(match.first!=truth_vec.end()) {
        *truth=*match.first;
        t_match=true;
      }
      else {
        DBTruth null;
        null.SetRADec(-999,-999);
        null.ra=-999;
        null.dec=-99;
        t_match=false;
        *truth=null;
      }
      
      
      tree->Fill();
    }
        
    return true;
    
  }
  
  
  
  void Tokenize(const string& str,
                vector<string>& tokens,
                const string& delimiters = " ");

  
  bool IsPsf(DatabaseInterface &db,const string &run);

  string GetMaxCatRun(DatabaseInterface &db,const string &tile);

  bool GetSEImages(DatabaseInterface &db,vector<DBImage> &vec,
                   const string &tile,const string &run,const string &band="");


  template<class T>
  bool GetSEObjects(DatabaseInterface &db,const DBImage &image,
                    vector<T> &vec, const string &tile="",bool &cal=false)
  {
    
    double zp=0;
    if(tile.empty()) {
      cout<<"Cannot calibrate without tilename"<<endl;
      cal=false;
    }

    string imtype=image.imagetype;

    if(cal) {
      int imid;
      if(imtype=="remap") imid=image.parentid;
      else imid=image.id;

      string cmd=Form("select a.mag_zero,a.sigma_mag_zero,c.photflag from "
                      " zeropoint a, image b, exposure c where a.imageid=%d and "
                      " b.id=a.imageid and b.exposureid=c.id and a.tilename='%s' "
                      " order by insert_date",
                      imid,tile.c_str());

      DatabaseResult rec;
      rec.Query(db,cmd);
      bool ok=rec.NextRow();
      if(ok) {
        zp=rec.GetDouble(0);
      }
      else {
        cout<<"No zeropoint for imageid:"<<image.id<<endl;
        zp=-999;
      }
    }
    
    string where;
    if(imtype=="remap") {
      where=Form("imageid=%d",image.parentid);
    }
    else {
      where=Form("imageid=%d",image.id);
    }
    bool ok=FillVec(db,where,vec);
    
    if(cal && zp>-999) {
      for(int i=0;i<vec.size();i++) {
        vec[i].UpdateMags(zp);
      }
    }
    
    return ok;
  }

//   bool GetSEObjectsPG(DatabaseInterface &db,const PGImage &image,
//                     vector<PGObject> &vec,const string &table="")
//   {
    
//     string where=Form("imageid=%d",image.id);
//     PGObject ob;
//     if(!table.empty()) ob.SetTable(table);
//     string query="select ";
//     for(int i=0;i<ob.tables.size();++i) {
      
//       for(int j=0;j<ob.columns[i].size();++j) {
        
//         query+=ob.tables[i]+"."+ob.columns[i][j]+",";
//       }
//     }
//     query=query.substr(0,query.size()-1);
    
//     query+=" from ";
//     for(int i=0;i<ob.tables.size();++i) {
//       query+=ob.tables[i]+",";
//     }
//     query=query.substr(0,query.size()-1);
    
//     query+=" where ";
//     query+=where;
    
//     DatabaseResult result;
//     bool ok=result.Query(db,query);
//     if(!ok) return ok;
    
//     while(result.NextRow()) {
            
//       ob.SetVals(result);
//       vec.push_back(ob);
//     }


//     return true;
//   }



  bool GetSEShearObjects(DatabaseInterface &db,const DBImage &image,
                         vector<DBObject> &ob_vec,
                         vector<DBShear> &shear_vec,const bool cal=true);

  bool GetSEShearObjects(DatabaseInterface &db,const DBImage &image,
                         vector<DBSObject> &ob_vec,
                         vector<DBShear> &shear_vec,const bool cal=true);

  

  void sigma_clip(int max_iter,double thresh,
                  const vector<double> &array,
                  const vector<double> &err,double &mean,
                  int &good, int &bad,double &rms,bool clip,
                  bool weighted,double &tol);


}

#endif
