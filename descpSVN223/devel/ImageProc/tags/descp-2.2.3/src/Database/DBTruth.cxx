#include "DBTruth.h"

using namespace Db;
ClassImp(DBTruth)

DBTruth::DBTruth():
  DatabaseBase()
{
 this->AddTable("gsn_oct10_truth","class,datafile_name,dec,dec_dbl,e,gamma1,gamma2,gsn_oct10_truth_id,g_mag,i_mag,kappa,magn,mudec,mura,pa,ra,ra_dbl,re,redshift,r_mag,sde,sersic_n,sra,star_galaxy_id,u_mag,y_mag,z_mag");
}

DBTruth::DBTruth(const DBTruth &cp) 
{

  sclass=cp.sclass;
  datafile_name=cp.datafile_name;
  dec=cp.dec;
  dec_dbl=cp.dec_dbl;
  e=cp.e;
  gamma1=cp.gamma1;
  gamma2=cp.gamma2;
  gsn_aug10_truth_id=cp.gsn_aug10_truth_id;
  g_mag=cp.g_mag;
  i_mag=cp.i_mag;
  kappa=cp.kappa;
  magn=cp.magn;
  mudec=cp.mudec;
  mura=cp.mura;
  pa=cp.pa;
  ra=cp.ra;
  ra_dbl=cp.ra_dbl;
  re=cp.re;
  redshift=cp.redshift;
  r_mag=cp.r_mag;
  sde=cp.sde;
  sersic_n=cp.sersic_n;
  sra=cp.sra;
  star_galaxy_id=cp.star_galaxy_id;
  u_mag=cp.u_mag;
  y_mag=cp.y_mag;
  z_mag=cp.z_mag;
  xyz[0]=cp.xyz[0];
  xyz[1]=cp.xyz[1];
  xyz[2]=cp.xyz[2];
}

bool DBTruth::SetVals(DatabaseResult &rec,const int &first) 
{

  sclass=rec.GetString(first+0);
  datafile_name=rec.GetString(first+1);
  dec=rec.GetDouble(first+2);
  dec_dbl=rec.GetDouble(first+3);
  e=rec.GetDouble(first+4);
  gamma1=rec.GetDouble(first+5);
  gamma2=rec.GetDouble(first+6);
  gsn_aug10_truth_id=rec.GetInt(first+7);
  g_mag=rec.GetDouble(first+8);
  i_mag=rec.GetDouble(first+9);
  kappa=rec.GetDouble(first+10);
  magn=rec.GetDouble(first+11);
  mudec=rec.GetInt(first+12);
  mura=rec.GetInt(first+13);
  pa=rec.GetDouble(first+14);
  ra=rec.GetDouble(first+15);
  ra_dbl=rec.GetDouble(first+16);
  re=rec.GetDouble(first+17);
  redshift=rec.GetDouble(first+18);
  r_mag=rec.GetDouble(first+19);
  sde=rec.GetInt(first+20);
  sersic_n=rec.GetDouble(first+21);
  sra=rec.GetInt(first+22);
  star_galaxy_id=rec.GetInt(first+23);
  u_mag=rec.GetDouble(first+24);
  y_mag=rec.GetDouble(first+25);
  z_mag=rec.GetDouble(first+26);

  this->SetRADec(ra_dbl,dec_dbl);
}

