#ifndef DBSCOADDSHEAR_H
#define DBSCOADDSHEAR_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class DBSCoaddShear: public DatabaseBase {

public:

 DBSCoaddShear();
 ~DBSCoaddShear() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 int coadd_object_id;
 float dec;
 int id;
 int nimages_found_g;
 int nimages_found_i;
 int nimages_found_r;
 int nimages_found_y;
 int nimages_found_z;
 float ra;
 float shapelet_sigma_g;
 float shapelet_sigma_i;
 float shapelet_sigma_r;
 float shapelet_sigma_y;
 float shapelet_sigma_z;
 float shear1_g;
 float shear1_i;
 float shear1_r;
 float shear1_y;
 float shear1_z;
 float shear2_g;
 float shear2_i;
 float shear2_r;
 float shear2_y;
 float shear2_z;
 float shear_cov00_g;
 float shear_cov00_i;
 float shear_cov00_r;
 float shear_cov00_y;
 float shear_cov00_z;
 float shear_cov01_g;
 float shear_cov01_i;
 float shear_cov01_r;
 float shear_cov01_y;
 float shear_cov01_z;
 float shear_cov11_g;
 float shear_cov11_i;
 float shear_cov11_r;
 float shear_cov11_y;
 float shear_cov11_z;
 int shear_flags_g;
 int shear_flags_i;
 int shear_flags_r;
 int shear_flags_y;
 int shear_flags_z;
 float shear_nu_g;
 float shear_nu_i;
 float shear_nu_r;
 float shear_nu_y;
 float shear_nu_z;


ClassDef(DBSCoaddShear,1);
};
}
#endif
