#ifndef DBLOCATION_H
#define DBLOCATION_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class DBLocation: public DatabaseBase {

public:

 DBLocation();
 ~DBLocation() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 string archivesites;
 string band;
 int ccd;
 string checksum;
 string checksum_fz;
 string checksum_gz;
 int dr;
 string exposurename;
 string fileclass;
 double filedate;
 string filename;
 int filesize;
 int filesize_fz;
 int filesize_gz;
 string filetype;
 int id;
 string nite;
 string project;
 string run;
 string tilename;


ClassDef(DBLocation,1);
};
}
#endif
