#include "DBLocation.h"

using namespace Db;
ClassImp(DBLocation)

DBLocation::DBLocation():
  DatabaseBase()
{
 this->AddTable("location","archivesites,band,ccd,checksum,checksum_fz,checksum_gz,dr,exposurename,fileclass,filedate,filename,filesize,filesize_fz,filesize_gz,filetype,id,nite,project,run,tilename");
}


bool DBLocation::SetVals(DatabaseResult &rec,const int &first) 
{

  archivesites=rec.GetString(first+0);
  band=rec.GetString(first+1);
  ccd=rec.GetInt(first+2);
  checksum=rec.GetString(first+3);
  checksum_fz=rec.GetString(first+4);
  checksum_gz=rec.GetString(first+5);
  dr=rec.GetInt(first+6);
  exposurename=rec.GetString(first+7);
  fileclass=rec.GetString(first+8);
  filedate=rec.GetDouble(first+9);
  filename=rec.GetString(first+10);
  filesize=rec.GetInt(first+11);
  filesize_fz=rec.GetInt(first+12);
  filesize_gz=rec.GetInt(first+13);
  filetype=rec.GetString(first+14);
  id=rec.GetInt(first+15);
  nite=rec.GetString(first+16);
  project=rec.GetString(first+17);
  run=rec.GetString(first+18);
  tilename=rec.GetString(first+19);
}

