#include "DatabaseInterface.h"
#include "DatabaseUtils.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Db;
int main(int argc,char* argv[])
{

  DatabaseInterface db("pg");
  MetaTable m_table;
  string table=argv[1];
  string class_name=argv[2];
  string vars;
  if(argc>3) vars=argv[3];
  db.GetMeta(table,m_table);
  cout<<m_table.col_map.size()<<endl;

  map<string,int> actual;
  if(!vars.empty()) {
    vector<string> ll;
    Tokenize(vars,ll,",");

    for(int i=0;i<ll.size();i++) {
      TString s(ll[i].c_str());
      s.ToUpper();
      string st(s.Data());
      actual[st]=1;

    }
  }

  map<string,ColType> cols=m_table.col_map;
  map<string,ColType>::iterator iter= cols.begin();
  
  string hfile=class_name+".h";
  string srcfile=class_name+".cxx";
  ofstream h_out(hfile.c_str());
  ofstream src_out(srcfile.c_str());
  TString upper(class_name.c_str());
  upper.ToUpper();
  h_out<<"#ifndef "<<upper<<"_H"<<endl;
  h_out<<"#define "<<upper<<"_H"<<endl;
  h_out<<endl;
  h_out<<"#include \"DatabaseBase.h\""<<endl;
  h_out<<"#include \"DatabaseResult.h\""<<endl<<endl;
  h_out<<"namespace Db {"<<endl;
  h_out<<"class "<<class_name<<": public DatabaseBase {"<<endl;
  h_out<<endl;
  h_out<<"public:"<<endl;
  h_out<<endl;
  h_out<<" "<<class_name<<"();"<<endl;
  //  h_out<<" "<<class_name<<"(const "<<class_name<<" &cp);"<<endl;
  h_out<<" ~"<<class_name<<"() {}"<<endl;
  h_out<<" virtual bool SetVals(DatabaseResult &rec,const int &first=0);"<<endl; 

  h_out<<endl<<endl;


  for(; iter!=cols.end();iter++) {

    if(actual.size()>0) {
      if(!actual[iter->first]) {
        continue;
      }
    }

    TString low(iter->first.c_str());
    low.ToLower();

    if(iter->second==kDBString) {
      h_out<<" string "<<low<<";"<<endl;
    }
    else if(iter->second==kDBDouble) {
      h_out<<" double "<<low<<";"<<endl;
    }
    else if(iter->second==kDBInt) {
      h_out<<" int "<<low<<";"<<endl;
    }
    else if(iter->second==kDBFloat) {
      h_out<<" float "<<low<<";"<<endl;
    }
  }
  h_out<<endl;
    
  h_out<<endl;
  h_out<<"ClassDef("<<class_name<<",1);"<<endl;

  src_out<<"#include \""<<hfile<<"\""<<endl;
  src_out<<endl;
  src_out<<"using namespace Db;"<<endl;
  src_out<<"ClassImp("<<class_name<<")"<<endl;

  src_out<<endl;
  src_out<<class_name<<"::"<<class_name<<"():"<<endl;
  src_out<<"  DatabaseBase()"<<endl;
  src_out<<"{"<<endl;
  string collist;
  iter=cols.begin();
  for(; iter!=cols.end();iter++) {

    if(actual.size()>0) {
      if(!actual[iter->first]) {
        continue;
      }
    }

    TString low(iter->first.c_str());
    low.ToLower();
    collist+=low+",";
  }
  collist=collist.substr(0,collist.size()-1);
  
  src_out<<" this->AddTable(\""<<table<<"\",\""<<collist<<"\");"<<endl;
  src_out<<"}"<<endl;
  src_out<<endl<<endl;

  // 
//   src_out<<class_name<<"::"<<class_name<<"(const "<<class_name<<" &cp) "<<endl;
//   src_out<<"{"<<endl;
//   src_out<<endl;
//   iter=cols.begin();
//   for(; iter!=cols.end();iter++) {

//     if(actual.size()>0) {
//       if(!actual[iter->first]) {
//         continue;
//       }
//     }

//     TString low(iter->first.c_str());
//     low.ToLower();
//     src_out<<"  "<<low<<"=cp."<<low<<";"<<endl;
//   }
//   src_out<<"}"<<endl<<endl;


  src_out<<"bool "<<class_name<<"::SetVals(DatabaseResult &rec,const int &first) "<<endl;
  src_out<<"{"<<endl;
  src_out<<endl;
  iter=cols.begin();
  int counter=0;
  for(; iter!=cols.end();iter++) {

    if(actual.size()>0) {
      if(!actual[iter->first]) {
        continue;
      }
    }

    TString low(iter->first.c_str());
    low.ToLower();

     if(iter->second==kDBFloat || iter->second==kDBDouble) {
       src_out<<"  "<<low<<"=rec.GetDouble(first+"<<counter<<");"<<endl;
     }
     else if(iter->second==kDBInt) {
       src_out<<"  "<<low<<"=rec.GetInt(first+"<<counter<<");"<<endl;
     }
     else if(iter->second==kDBString) {
       src_out<<"  if(!rec.IsNull(first+"<<counter<<")) "<<low<<"=rec.GetString(first+"<<counter<<");"<<endl;
       src_out<<"  else "<<low<<"=\"\";"<<endl;
     }
     counter++;
  }
  src_out<<"}"<<endl<<endl;

 //  out<<" void SetValue(const string &name,const float &val) {"<<endl;

//   iter= cols.begin();
//   bool first=true;
//   for(; iter!=cols.end();iter++) {
//     if(iter->second==kDBFloat) {
//       TString low(iter->first.c_str());
//       low.ToLower();
//       if(first) {
//         out<<"   if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//         first=false;
//       }
//       else  out<<"   else if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//     }
//   }
//   out<<"}"<<endl;

//   out<<endl;
//   out<<endl;
//   out<<" void SetValue(const string &name,const double &val) {"<<endl;

//   iter= cols.begin();
//   first=true;
//   for(; iter!=cols.end();iter++) {
//     if(iter->second==kDBDouble) {
//       TString low(iter->first.c_str());
//       low.ToLower();
//       if(first) {
//         out<<"   if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//         first=false;
//       }
//       else  out<<"   else if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//     }
//   }
//   out<<"}"<<endl;

//   out<<endl;
//   out<<endl;
//   out<<" void SetValue(const string &name,const string &val) {"<<endl;

//   iter= cols.begin();
//   first=true;
//   for(; iter!=cols.end();iter++) {
//     if(iter->second==kDBString) {
//       TString low(iter->first.c_str());
//       low.ToLower();
//       if(first) {
//         out<<"   if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//         first=false;
//       }
//       else  out<<"   else if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//     }
//   }
//   out<<"}"<<endl;

//    out<<endl;
//   out<<endl;
//   out<<" void SetValue(const string &name,const int &val) {"<<endl;

//   iter= cols.begin();
//   first=true;
//   for(; iter!=cols.end();iter++) {
//     if(iter->second==kDBInt) {
//       TString low(iter->first.c_str());
//       low.ToLower();
//       if(first) {
//         out<<"   if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//         first=false;
//       }
//       else  out<<"   else if(name==\""<<low<<"\") "<<low<<"=val;"<<endl;
//     }
//   }
//   out<<"}"<<endl;

  h_out<<"};"<<endl;
  h_out<<"}"<<endl;
  h_out<<"#endif"<<endl;
}


