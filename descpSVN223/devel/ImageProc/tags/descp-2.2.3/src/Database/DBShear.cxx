#include "DBShear.h"

using namespace Db;
ClassImp(DBShear)

DBShear::DBShear():
  DatabaseBase()
{
 this->AddTable("WL_SHEAR","catalogid,gal_order,id,object_id,object_number,shapelet_sigma,shear1,shear2,shear_cov00,shear_cov01,shear_cov11,shear_flags,shear_signal_to_noise");
}


bool DBShear::SetVals(DatabaseResult &rec,const int &first) 
{

  catalogid=rec.GetInt(first+0);
  gal_order=rec.GetInt(first+1);
  id=rec.GetInt(first+2);
  object_id=rec.GetLong(first+3);
  object_number=rec.GetInt(first+4);
  shapelet_sigma=rec.GetDouble(first+5);
  shear1=rec.GetDouble(first+6);
  shear2=rec.GetDouble(first+7);
  shear_cov00=rec.GetDouble(first+8);
  shear_cov01=rec.GetDouble(first+9);
  shear_cov11=rec.GetDouble(first+10);
  shear_flags=rec.GetInt(first+11);
  shear_signal_to_noise=rec.GetDouble(first+12);
}

