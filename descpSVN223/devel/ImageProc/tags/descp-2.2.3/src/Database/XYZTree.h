#ifndef XYZTREE_H
#define XYZTREE_H

#include "TMath.h"

class XYZTree{
public:
  typedef double value_type;

  XYZTree() {}//{xyz[0]=-999.;xyz[1]=-999;xyz[2]=-999;}
  virtual ~XYZTree() {}
   XYZTree(const XYZTree &ob)
   { 
     xyz[0]=ob.xyz[0];
     xyz[1]=ob.xyz[1];
     xyz[2]=ob.xyz[2];
   }
  XYZTree(const double &ra,const double &dec) {
    double cd=cos(dec*TMath::DegToRad());
    xyz[0]=cos(ra*TMath::DegToRad())*cd;
    xyz[1]=sin(ra*TMath::DegToRad())*cd;
    xyz[2]=sin(dec*TMath::DegToRad());
  }

 void  SetRADec(const double &ra, const double &dec) {
    double cd=cos(dec*TMath::DegToRad());
    xyz[0]=cos(ra*TMath::DegToRad())*cd;
    xyz[1]=sin(ra*TMath::DegToRad())*cd;
    xyz[2]=sin(dec*TMath::DegToRad());
  }

  double xyz[3];
  inline double operator[](int const N) const {return xyz[N];}
};

#endif
