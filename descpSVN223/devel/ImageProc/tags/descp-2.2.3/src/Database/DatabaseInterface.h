#ifndef DATABASEINTERFACE_H
#define DATABASEINTERFACE_H

#include <string>
#include <vector>
#include <map>
#include <utility>
#include <cstdlib>

#include "TSQLServer.h"
#include "TSQLStatement.h"

using std::string;
using std::vector;
using std::map;
using std::pair;

namespace Db {

  typedef enum ColType{
    kDBDouble,kDBInt,kDBString,kDBFloat
  };

    
  class MetaTable
  {
  public: 
    MetaTable() {}
    MetaTable(const string &_name):Name(_name) {}
    void AddCol(const string &col_name,const ColType &col_type) 
    {
      col_map[col_name]=col_type;
      //map<string,ColType>::iterator it=col_map.find(col);
      //if(it!=col_map.end()) return it->second;

    }
    
    ColType GetType(const string &col) 
    {
      // make sure column exists      
      map<string,ColType>::iterator it=col_map.find(col);
      if(it!=col_map.end()) return it->second;
      else return kDBString;
    }
    
    int Size() {return col_map.size();}
    
    int Ncols;
    string Name;
    map<string,ColType> col_map;
    
  };


class DatabaseInterface
{

public:
 typedef enum DBType{
    kDBOracle,kDBPsql,kDBMysql
  };



  DatabaseInterface(const string &db="",const string file="",
                    const bool &load_tables=true);

  ~DatabaseInterface() {
    this->Close();
    if(server) delete server;
  }

  bool Open();
  bool Close();
  bool IsClosed() { return !server; }
  bool LoadTable(const string &table);
  bool IsLoaded(const string &table);
  bool GetMeta(const string &table,MetaTable &meta);
  bool Commit();
  ColType StringToDB(const string &val);
  TSQLStatement* CreateStatement(const std::string& sql);
 


private:


  TSQLServer *server;

  string db_user;
  string db_passwd;
  string db_name;

  bool load_tables;

  DBType db_type;
  vector<MetaTable> meta_table;


  ClassDef(DatabaseInterface,0)
};


}

#endif
