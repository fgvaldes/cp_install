#ifndef DBSOBJECT_H
#define DBSOBJECT_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"
#include "XYZTree.h"

namespace Db {
  class DBSObject: public DatabaseBase,public XYZTree {

public:

 DBSObject();
 ~DBSObject() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);
  void UpdateMags(const double &val);
  DBSObject(const DBSObject &cp) ;

 double alphamodel_j2000;
 double alphapeak_j2000;
 double alphawin_j2000;
 double alpha_j2000;
 int catalogid;
 float class_star;
 float dec;
 double deltamodel_j2000;
 double deltapeak_j2000;
 double deltawin_j2000;
 double delta_j2000;
 float ellip1model_world;
 float ellip2model_world;
 int flags;
 float fwhm_world;
 int imageid;
 float magerr_aper_1;
 float magerr_aper_10;
 float magerr_aper_11;
 float magerr_aper_12;
 float magerr_aper_13;
 float magerr_aper_14;
 float magerr_aper_15;
 float magerr_aper_16;
 float magerr_aper_17;
 float magerr_aper_2;
 float magerr_aper_3;
 float magerr_aper_4;
 float magerr_aper_5;
 float magerr_aper_6;
 float magerr_aper_7;
 float magerr_aper_8;
 float magerr_aper_9;
 float magerr_auto;
 float magerr_model;
 float magerr_psf;
 float mag_aper_1;
 float mag_aper_10;
 float mag_aper_11;
 float mag_aper_12;
 float mag_aper_13;
 float mag_aper_14;
 float mag_aper_15;
 float mag_aper_16;
 float mag_aper_17;
 float mag_aper_2;
 float mag_aper_3;
 float mag_aper_4;
 float mag_aper_5;
 float mag_aper_6;
 float mag_aper_7;
 float mag_aper_8;
 float mag_aper_9;
 float mag_auto;
 float mag_model;
 float mag_psf;
 long object_id;
 int object_number;
 int parentid;
 float ra;
 float spreaderr_model;
 float spread_model;
 float threshold;
 float x_image;
 float y_image;
 float zeropoint;
    string band;


ClassDef(DBSObject,1);
};
}
#endif
