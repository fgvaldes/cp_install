#include "DatabaseInterface.h"
#include "DBSObject.h"
#include "DBTruth.h"
#include "DBSCoaddObject.h"
#include "DBSCoaddShear.h"
#include "DBCatalog.h"
#include "DBImage.h"
#include "DBShear.h"
#include "DatabaseUtils.h"
#include "DBTruth.h"
#include <iostream>
#include <fstream>
#include <kdtree++/kdtree.hpp>

#include "TTree.h"
#include "TFile.h"
#include "TStopwatch.h"
using namespace std;
using namespace Db;


typedef KDTree::KDTree<3,DBSObject > DB_KDTree;
typedef std::pair<DB_KDTree::iterator,double> DB_Match;

typedef KDTree::KDTree<3,DBTruth > DBTruth_KDTree;
typedef std::pair<DBTruth_KDTree::iterator,double> DBTruth_Match;

int main(int argc,char* argv[])
{
   
  // tolerance in arcseconds
   double tol=2;
   
   // convert to radians
   tol*=1./3600*TMath::DegToRad();
   TStopwatch timer;
   //  string run="20110111100523_BCS2335-5421";
   // string tile="BCS2336-5602";
//   string run="20101231234611_BCS0547-5332";//20090803201006_BCS0547-5332";
   //string run="";//"20100820122536_BCS0547-5332";//20090803201006_BCS0547-5332";
   //string tile="BCS0554-5332";

   string tile="DES2219-4147";
   string run="20101124104813_DES2219-4147";
   //string run="";
   
    DatabaseInterface db("pr");
    if(run.empty()) {
      run=GetMaxCatRun(db,tile);
    }
    cout<<"Using run: "<<run<<endl;

    string band="i";

    // Get Coadd objects
    string where=Form("from coadd_objects,catalog where "
                      "catalog.tilename='%s' and "
                      "catalog.id=coadd_objects.catalogid_g and "
                      "catalog.run='%s'",
                      tile.c_str(),run.c_str());

    vector<DBSCoaddObject> vec_coadd;
    DBSCoaddObject *coadd=new DBSCoaddObject;

    bool inc_table=true;
    bool ok=FillVec(db,where,vec_coadd,inc_table);


    // find min/max ra/dec
    double max_ra=-1e6,max_dec=-1e6,min_ra=1e6,min_dec=1e6;
    cout<<"coadd objects: "<<vec_coadd.size()<<" objects"<<endl;
    if(vec_coadd.size()==0) {
      exit(1);
    }


    for(int i=0;i<vec_coadd.size();++i) {
      max_ra=TMath::Max(max_ra,vec_coadd[i].alphapeak_j2000);
      max_dec=TMath::Max(max_dec,vec_coadd[i].deltapeak_j2000);
      min_ra=TMath::Min(min_ra,vec_coadd[i].alphapeak_j2000);
      min_dec=TMath::Min(min_dec,vec_coadd[i].deltapeak_j2000);
    }


    // Get Truth objects in range
    vector<DBTruth> vec_truth;
    DBTruth_KDTree obs_truth;
    where=Form("gsn_oct10_truth.ra<%f  and  gsn_oct10_truth.ra>%f "
               "and gsn_oct10_truth.dec<%f and "
               "gsn_oct10_truth.dec>%f",max_ra,min_ra,max_dec,min_dec);
    cout<<where<<endl;
    
    ok=FillVec(db,where,vec_truth);
    for(int j=0;j<vec_truth.size();j++) {
      obs_truth.insert(vec_truth[j]);
      
    }

    cout<<"Found "<<vec_truth.size()<<" truth objects"<<endl;

    vec_truth.clear();
 
    
    // create new file
    TFile newfile("newfile.root","recreate");

    
    TTree *imtree=new TTree("imtree","");
    DBImage *image=new DBImage;
    imtree->Branch("image",&image);

    vector<DBImage> vec_im;
    GetSEImages(db,vec_im,tile,run);
    cout<<"Found "<<vec_im.size()<<" images in the tile"<<endl;


    bool cal=true;
    DB_KDTree obs;
    for(int i=0;i<vec_im.size();++i) {
     vector<DBSObject> vec_obs;
     bool ok=GetSEObjects(db,vec_im[i],vec_obs,tile,cal);
     cout<<"Got image with "<<vec_obs.size()<<" objects"<<endl;
     for(int j=0;j<vec_obs.size();j++) {
       obs.insert(vec_obs[j]);
     }
   }

    
    // Fill the image tree
    for(int i=0;i<vec_im.size();++i) {
      *image=vec_im[i];
      imtree->Fill();
    }
    
    TTree *ntree=new TTree("tree","");
    int n_match;
    vector<DBImage> vec_imm;
    vector<DBSObject> vec_ob;
    vector<int> se_band;
    vector<double> se_fwhm;
    DBTruth truth;
    ntree->Branch("truth",&truth);
    ntree->Branch("coadd",&coadd);
    ntree->Branch("im",&vec_imm);
    ntree->Branch("se",&vec_ob);
    ntree->Branch("se_band",&se_band);
    DBTruth du;
    
    for(int i=0;i<vec_coadd.size();++i) {
      vector<DBSObject> matches;
    
      // reset vectors
      vec_ob.clear();
      vec_imm.clear();
      se_band.clear();
      *coadd=vec_coadd[i];

      // First match to truth, just use closest match
      // could try to do a better job here
      DBTruth dummy_truth;
      dummy_truth.SetRADec(vec_coadd[i].alphapeak_j2000,
                           vec_coadd[i].deltapeak_j2000);
      DBTruth_Match match=obs_truth.find_nearest(dummy_truth,tol);

      // if no truth match skip to the next object      
      if(match.first==obs_truth.end()) {
        //truth=du;
        //n_match=false;
        continue;
      } else {
        truth=*match.first;
        n_match=true;
      }


      // use i band to match to se, could do a seperate
      //       list for each band, but good enough 
      DBSObject dummy;
      dummy.SetRADec(truth.ra_dbl,truth.dec_dbl);
      obs.find_within_range(dummy,tol,std::back_inserter(matches));
      
      n_match=matches.size();
      vector<DBSObject>::iterator match_iter=matches.begin();
      for( ; match_iter!=matches.end(); ++match_iter) {
        vec_ob.push_back(*match_iter);

        // need seperate vector for band info since
        // ROOT has problem with the strings
        if(match_iter->band=="g") se_band.push_back(0);
        else if(match_iter->band=="r") se_band.push_back(1);
        else if(match_iter->band=="i") se_band.push_back(2);
        else if(match_iter->band=="z") se_band.push_back(3);
        else if(match_iter->band=="Y") se_band.push_back(4);
        else se_band.push_back(-1);



        // find single epoch image
        int id=match_iter->imageid;
        bool found=false;
        for(int j=0;j<vec_im.size();j++) {
          if(vec_im[j].parentid==id) {
            vec_imm.push_back(vec_im[j]);
            found=true;
            break;
          }
        }
        if(!found) {
          // if none was found create an empty one
          cout<<"Not found"<<endl;
          DBImage im;
          vec_imm.push_back(im);
        }
        
      }

      // require se objects
      if(n_match>0) {
        ntree->Fill();
      }
      
    }

    imtree->Write();
    ntree->Write();
    newfile.Close();
    timer.Print();
}
  

