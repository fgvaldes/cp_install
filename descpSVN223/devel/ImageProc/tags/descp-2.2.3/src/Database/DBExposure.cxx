#include "DBExposure.h"

using namespace Db;
ClassImp(DBExposure)

DBExposure::DBExposure():
  DatabaseBase()
{
 this->AddTable("EXPOSURE","airmass,altitude,ambtemp,band,camshut,cx,cy,cz,darktime,date_obs,detector,detsize,dimmsee,domelamp,domeshut,domestat,epoch,expnum,exposurename,exposuretype,expreq,exptime,exp_exclude,filpos,filtid,fluxvar,ha,htmid,humidity,id,imagehwv,imageswv,instrument,latitude,longitude,mjd_obs,moonangle,naxis1,naxis2,nextend,nite,object,observatory,observer,obstype,photflag,pixscal1,pixscal2,pressure,proctype,prodtype,project,propid,proposer,radesys,skyvar,teldec,telequin,telescope,telfocus,telra,telradec,telstat,timesys,time_obs,winddir,windspd,zd");
}


bool DBExposure::SetVals(DatabaseResult &rec,const int &first) 
{

  airmass=rec.GetDouble(first+0);
  if(!rec.IsNull(first+1)) altitude=rec.GetString(first+1);
  else altitude="";
  if(!rec.IsNull(first+2)) ambtemp=rec.GetString(first+2);
  else ambtemp="";
  if(!rec.IsNull(first+3)) band=rec.GetString(first+3);
  else band="";
  if(!rec.IsNull(first+4)) camshut=rec.GetString(first+4);
  else camshut="";
  cx=rec.GetDouble(first+5);
  cy=rec.GetDouble(first+6);
  cz=rec.GetDouble(first+7);
  darktime=rec.GetDouble(first+8);
  if(!rec.IsNull(first+9)) date_obs=rec.GetString(first+9);
  else date_obs="";
  if(!rec.IsNull(first+10)) detector=rec.GetString(first+10);
  else detector="";
  if(!rec.IsNull(first+11)) detsize=rec.GetString(first+11);
  else detsize="";
  dimmsee=rec.GetDouble(first+12);
  if(!rec.IsNull(first+13)) domelamp=rec.GetString(first+13);
  else domelamp="";
  if(!rec.IsNull(first+14)) domeshut=rec.GetString(first+14);
  else domeshut="";
  if(!rec.IsNull(first+15)) domestat=rec.GetString(first+15);
  else domestat="";
  epoch=rec.GetDouble(first+16);
  expnum=rec.GetInt(first+17);
  if(!rec.IsNull(first+18)) exposurename=rec.GetString(first+18);
  else exposurename="";
  if(!rec.IsNull(first+19)) exposuretype=rec.GetString(first+19);
  else exposuretype="";
  expreq=rec.GetDouble(first+20);
  exptime=rec.GetDouble(first+21);
  exp_exclude=rec.GetInt(first+22);
  filpos=rec.GetInt(first+23);
  if(!rec.IsNull(first+24)) filtid=rec.GetString(first+24);
  else filtid="";
  fluxvar=rec.GetDouble(first+25);
  if(!rec.IsNull(first+26)) ha=rec.GetString(first+26);
  else ha="";
  htmid=rec.GetInt(first+27);
  if(!rec.IsNull(first+28)) humidity=rec.GetString(first+28);
  else humidity="";
  id=rec.GetInt(first+29);
  if(!rec.IsNull(first+30)) imagehwv=rec.GetString(first+30);
  else imagehwv="";
  if(!rec.IsNull(first+31)) imageswv=rec.GetString(first+31);
  else imageswv="";
  if(!rec.IsNull(first+32)) instrument=rec.GetString(first+32);
  else instrument="";
  if(!rec.IsNull(first+33)) latitude=rec.GetString(first+33);
  else latitude="";
  if(!rec.IsNull(first+34)) longitude=rec.GetString(first+34);
  else longitude="";
  mjd_obs=rec.GetDouble(first+35);
  moonangle=rec.GetDouble(first+36);
  naxis1=rec.GetInt(first+37);
  naxis2=rec.GetInt(first+38);
  nextend=rec.GetInt(first+39);
  if(!rec.IsNull(first+40)) nite=rec.GetString(first+40);
  else nite="";
  if(!rec.IsNull(first+41)) object=rec.GetString(first+41);
  else object="";
  if(!rec.IsNull(first+42)) observatory=rec.GetString(first+42);
  else observatory="";
  if(!rec.IsNull(first+43)) observer=rec.GetString(first+43);
  else observer="";
  if(!rec.IsNull(first+44)) obstype=rec.GetString(first+44);
  else obstype="";
  photflag=rec.GetInt(first+45);
  pixscal1=rec.GetDouble(first+46);
  pixscal2=rec.GetDouble(first+47);
  if(!rec.IsNull(first+48)) pressure=rec.GetString(first+48);
  else pressure="";
  if(!rec.IsNull(first+49)) proctype=rec.GetString(first+49);
  else proctype="";
  if(!rec.IsNull(first+50)) prodtype=rec.GetString(first+50);
  else prodtype="";
  if(!rec.IsNull(first+51)) project=rec.GetString(first+51);
  else project="";
  if(!rec.IsNull(first+52)) propid=rec.GetString(first+52);
  else propid="";
  if(!rec.IsNull(first+53)) proposer=rec.GetString(first+53);
  else proposer="";
  if(!rec.IsNull(first+54)) radesys=rec.GetString(first+54);
  else radesys="";
  skyvar=rec.GetDouble(first+55);
  teldec=rec.GetInt(first+56);
  telequin=rec.GetDouble(first+57);
  if(!rec.IsNull(first+58)) telescope=rec.GetString(first+58);
  else telescope="";
  telfocus=rec.GetDouble(first+59);
  telra=rec.GetInt(first+60);
  if(!rec.IsNull(first+61)) telradec=rec.GetString(first+61);
  else telradec="";
  if(!rec.IsNull(first+62)) telstat=rec.GetString(first+62);
  else telstat="";
  if(!rec.IsNull(first+63)) timesys=rec.GetString(first+63);
  else timesys="";
  if(!rec.IsNull(first+64)) time_obs=rec.GetString(first+64);
  else time_obs="";
  if(!rec.IsNull(first+65)) winddir=rec.GetString(first+65);
  else winddir="";
  if(!rec.IsNull(first+66)) windspd=rec.GetString(first+66);
  else windspd="";
  zd=rec.GetDouble(first+67);
}

