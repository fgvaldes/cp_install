#ifdef __CINT__ 


#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;


#pragma link C++ class Db::DatabaseInterface+; 
#pragma link C++ class Db::DatabaseResult+; 
#pragma link C++ class Db::DatabaseBase+; 
#pragma link C++ class Db::DBObject+; 
#pragma link C++ class Db::DBCoaddObject+; 
#pragma link C++ class Db::DBTruth+; 
#pragma link C++ class Db::DBImage+; 
#pragma link C++ class Db::DBLocation+; 
#pragma link C++ class Db::DBCatalog+; 
#pragma link C++ class Db::DBShear+; 
#pragma link C++ class Db::DBCoaddShear+; 
#pragma link C++ class Db::DBSCoaddShear+; 
#pragma link C++ class Db::ExpQA+; 
#pragma link C++ class Db::DBExposureQA+; 
#pragma link C++ class Db::DBExposure+; 
#pragma link C++ class Db::Exp+; 
#pragma link C++ class Db::DBSObject+; 
#pragma link C++ class Db::DBSCoaddObject+; 
#pragma link C++ class Db::PGImage+; 
#pragma link C++ class Db::PGObject+; 
#pragma link C++ class vector<Db::DBImage>+;
#pragma link C++ class vector<Db::DBSObject>+;
//#pragma link C++ class XYZTree+; 

#endif
