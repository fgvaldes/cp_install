#ifndef DATABASEBASE_H
#define DATABASEBASE_H

#include "DatabaseInterface.h"
#include <string>
#include "TObject.h"

class DatabaseRecord;

namespace Db {

  class DatabaseBase: public TObject {

public:


    DatabaseBase() {}
    DatabaseBase(const string &_tables, const string &_columns="",
                  DatabaseInterface *db=0);

  
    virtual bool SetVals(DatabaseRecord &rec,const int &first=0) {}
    void AddTable(const string &tab,const string &cols);
    void SetTable(const string &tab) {tables.clear();tables.push_back(tab);}

public:
  
    
  vector<vector<string> > columns;
  vector<string> tables;
  bool GenericFill;
 
  ClassDef(DatabaseBase,1) // Abstract base class
              
};

}
#endif

