#ifndef PGIMAGE_H
#define PGIMAGE_H

#include "DatabaseBase.h"
#include "DatabaseResult.h"

namespace Db {
class PGImage: public DatabaseBase {

public:

 PGImage();
 ~PGImage() {}
 virtual bool SetVals(DatabaseResult &rec,const int &first=0);


 double airmass;
 string ampseca;
 string ampsecb;
 string band;
 string biasseca;
 string biassecb;
 double bscale;
 string bunit;
 double bzero;
 double ccd;
 string ccdname;
 string ccdsum;
 double cd1_1;
 double cd1_2;
 double cd2_1;
 double cd2_2;
 double crpix1;
 double crpix2;
 double crval1;
 double crval2;
 string ctype1;
 string ctype2;
 string cunit1;
 string cunit2;
 double cx;
 double cy;
 double cz;
 string datasec;
 double dec;
 string detsec;
 int device_id;
 double elliptic;
 double equinox;
 int exposureid;
 double exptime;
 double fwhm;
 double gaina;
 double gainb;
 double gcount;
 double htmid;
 int id;
 string imagename;
 string imagetype;
 double ltm1_1;
 double ltm2_2;
 double ltv1;
 double ltv2;
 int naxis1;
 int naxis2;
 double nextend;
 string nite;
 int parentid;
 int pcount;
 string project;
 double pv1_0;
 double pv1_1;
 double pv1_10;
 double pv1_2;
 double pv1_3;
 double pv1_4;
 double pv1_5;
 double pv1_6;
 double pv1_7;
 double pv1_8;
 double pv1_9;
 double pv2_0;
 double pv2_1;
 double pv2_10;
 double pv2_2;
 double pv2_3;
 double pv2_4;
 double pv2_5;
 double pv2_6;
 double pv2_7;
 double pv2_8;
 double pv2_9;
 double ra;
 string radesys;
 double rdnoisea;
 double rdnoiseb;
 string run;
 double saturata;
 double saturatb;
 double scampchi;
 double scampflg;
 int scampnum;
 double seesiga;
 double sky;
 double skybrite;
 double skysigma;
 string tilename;
 string trimsec;
 double wcsaxes;
 double wcsdim;
 double zero;
 double zp;


ClassDef(PGImage,1);
};
}
#endif
