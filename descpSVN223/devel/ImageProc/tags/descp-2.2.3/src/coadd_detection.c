/*
**
** coadd_detection.c
**
** DESCRIPTION:
**     Create chi2 detection image from a list of inputs
**     
**
** Last commit:
**     $Rev: 6645 $
**     $LastChangedBy: mgower $
**     $LastChangedDate: 2011-04-18 12:36:59 -0700 (Mon, 18 Apr 2011) $
**
*/

/* ASK MICHELLE THE INPUT LIST NEED TO HAVE ID */

#include "imageproc.h"

main(argc,argv)
     int argc;
     char *argv[];
{
  char tilename[1000],listname[1000],outpath[1000],outauxpath[1000],binpath[1000];
  char command[1000],comment[1000],event[1000];
  char terapixpath[1000],etcpath[1000];
  char swarpcall[1000],imageid[1000],outimage[1000],fitscombinecall[1000];
  char **image;

  int i,status,Nimage=0,*ID;
  int flag_list=0,flag_outpath=0,flag_tilename=0,flag_binpath=0;
  int flag_verbose=2,flag_terapixpath=0,flag_etcpath=0,flag_outauxpath=0;
  long npix_ra,npix_dec;

  float pixscale;
  double tile_ra,tile_dec;
  double cdelt1,cdelt2;
  double cd1_1,cd1_2,cd2_1,cd2_2;

  FILE *pip,*fin,*fimageid;
  fitsfile *fptr;
  
  /* external functions call */
  int  mkpath();
  void reportevt(),get_input();

  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <optional inputs>\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -list <list of coadd images for creating detection image, format: COADD_IMAGE COADD_ID>  \n");
    printf("       -tilename <tilename>  \n");
    printf("       -outpath <outpath>  \n");
    printf("       -outauxpath <outauxpath>  \n");
    printf("    Optional Inputs:\n");
    printf("       -terapixpath <full terapixpath>\n");
    printf("       -etcpath <full etcpath>\n");
    printf("       -binpath <full binpath>\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }


  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-list")) {
      flag_list=1;
      get_input(argv[i],argv[i+1],"-list",listname,flag_verbose);
    }

    if (!strcmp(argv[i],"-tilename")) {
      flag_tilename=1;
      get_input(argv[i],argv[i+1],"-tilename",tilename,flag_verbose);
    }

    if (!strcmp(argv[i],"-outpath")) {
      flag_outpath=1;
      get_input(argv[i],argv[i+1],"-outpath",outpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-outauxpath")) {
      flag_outauxpath=1;
      get_input(argv[i],argv[i+1],"-outauxpath",outauxpath,flag_verbose);
    }


    /* options */
    if (!strcmp(argv[i],"-terapixpath")) {
      flag_terapixpath=1;
      get_input(argv[i],argv[i+1],"-terapixpath",terapixpath,flag_verbose);
    }
    
    if (!strcmp(argv[i],"-etcpath")) {
      flag_etcpath=1;
      get_input(argv[i],argv[i+1],"-etcpath",etcpath,flag_verbose);
    }
    if (!strcmp(argv[i],"-binpath")) {
      flag_binpath=1;
      get_input(argv[i],argv[i+1],"-binpath",binpath,flag_verbose);
    }


  }

  /* quit if required inputs are not set */
  if(!flag_tilename || !flag_list || !flag_outpath || !flag_outauxpath) {
    sprintf(event,"Required inputs are not set ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  
  /* get the number of images in the input list */
  sprintf(command,"wc -l %s",listname);
  pip=popen(command,"r");
  fscanf(pip,"%d",&Nimage);
  pclose(pip);

  sprintf(event," Input list %s contains %d images ",listname,Nimage);

  if(Nimage) {
    reportevt(flag_verbose,STATUS,2,event);
  }
  else {
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  

  /* memory allocation and input the images */
  ID=(int *)calloc(Nimage+1,sizeof(int ));
  image=(char **)calloc(Nimage+1,sizeof(char *));
  for(i=1;i<=Nimage;i++)
    image[i]=(char *)calloc(1500,sizeof(char ));
  
  fin=fopen(listname,"r");
  for(i=1;i<=Nimage;i++)
    fscanf(fin,"%s %d",image[i],&ID[i]);
  fclose(fin);
  

  /* *** Get the WCS information from the first image *** */

  /* open the image */
  status=0;
  if(fits_open_file(&fptr,image[1],READONLY,&status)) {
    sprintf(event,"Image open failed: %s",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }

  /* extract RA and DEC */
  status=0;
  if(fits_read_key_dbl(fptr,"CRVAL1",&tile_ra,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword CRVAL1 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  status=0;
  if(fits_read_key_dbl(fptr,"CRVAL2",&tile_dec,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword CRVAL2 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }

  /* extract NAXIS */
  status=0;
  if(fits_read_key_lng(fptr,"NAXIS1",&npix_ra,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword NAXIS1 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  
  status=0;
  if(fits_read_key_lng(fptr,"NAXIS2",&npix_dec,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword NAXIS2 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }

  /* extract WCS CD Matrix */
  status=0;
  if(fits_read_key_dbl(fptr,"CD1_1",&cd1_1,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword CD1_1 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  status=0;
  if(fits_read_key_dbl(fptr,"CD2_1",&cd2_1,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword CD1_1 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  status=0;
  if(fits_read_key_dbl(fptr,"CD1_2",&cd1_2,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword CD1_2 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  status=0;
  if(fits_read_key_dbl(fptr,"CD2_2",&cd2_2,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword CD2_2 missing",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  
  
  /* calculate the pixscale */
  cdelt1=sqrt(Squ(cd1_1) + Squ(cd1_2));
  cdelt2=sqrt(Squ(cd2_1) + Squ(cd2_2));

  pixscale=0.5*(cdelt1+cdelt2)*3600.0;

  sprintf(event," For %s: CRVAL1 = %2.6f CRVAL2 = %2.6f\t NAXIS1,NAXIS2=%d,%d\tpixscale=%2.2f ",
	  image[1],tile_ra,tile_dec,npix_ra,npix_dec,pixscale);
  reportevt(flag_verbose,STATUS,2,event);


  /* close the image */
  status=0;
  if(fits_close_file(fptr,&status)) {
    sprintf(event,"Image %s close failed",image[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
  }
  
  /* make the output image path */
  sprintf(outimage,"%s/%s_det.fits",outpath,tilename);
  
  if (mkpath(outimage,flag_verbose)) {
    sprintf(event,"Failed to create path %s",outpath);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else {
    sprintf(event,"Created path %s",outpath);
    reportevt(flag_verbose,STATUS,2,event);
  }

  /* *** Set up the output imageid file *** */
  sprintf(imageid,"%s/%s_detid.dat",outauxpath,tilename);

  fimageid=fopen(imageid,"w");

  /* *** Construct swarp call and run swarp *** */
  if(flag_terapixpath) sprintf(command,"%s/swarp ",terapixpath);
  else sprintf(command,"swarp ");

  for(i=1;i<=Nimage;i++) { 
    
    fprintf(fimageid,"%d\t0.000\t0.000\n",ID[i]);
    
    if(i==1)
      sprintf(command,"%s %s[0]",command,image[i]);
    else
      sprintf(command,"%s,%s[0]",command,image[i]);
  }
  
  if(flag_etcpath) sprintf(command,"%s -c %s/default.swarp ",command,etcpath);
  else sprintf(command,"%s -c default.swarp ",command);

  /*  sprintf(command,"%s -IMAGEOUT_NAME %s/%s_det.fits ",command,outpath,tilename); */
  sprintf(command,"%s -IMAGEOUT_NAME tmp.fits ",command);  
  sprintf(command,"%s -RESAMPLE N -COMBINE Y -COMBINE_TYPE CHI-MEAN ",command);
  sprintf(command,"%s -SUBTRACT_BACK Y ",command);
  sprintf(command,"%s -DELETE_TMPFILES Y ",command);
  sprintf(command,"%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE ",command);
  for(i=1;i<=Nimage;i++) {
    if(i==1)
      sprintf(command,"%s %s[1]",command,image[i]);
    else
      sprintf(command,"%s,%s[1]",command,image[i]);
  }
  sprintf(command,"%s ",command);
  sprintf(command,"%s -IMAGE_SIZE %d,%d -PIXELSCALE_TYPE MANUAL -PIXEL_SCALE %2.3f ",command,npix_ra,npix_dec,pixscale); 
  sprintf(command,"%s -CENTER_TYPE MANUAL -CENTER %3.8f,%3.8f ",command,tile_ra,tile_dec);
  sprintf(command,"%s -HEADER_ONLY N ",command);
  sprintf(command,"%s -WRITE_XML N ",command);
  if(!flag_verbose)
    sprintf(command,"%s -VERBOSE_TYPE QUIET ",command);

  sprintf(event," SWARP Call: %s ",command);
  reportevt(flag_verbose,STATUS,2,event);
    
  system(command);
  /* run fitscombine */
  sprintf(outimage,"%s/%s_det.fits",outpath,tilename);
  if (flag_binpath)sprintf(fitscombinecall,"%s/fitscombine tmp.fits coadd.weight.fits %s -cleanup -verbose %d",binpath,outimage,flag_verbose);
  else sprintf(fitscombinecall,"fitscombine tmp.fits coadd.weight.fits %s -cleanup -verbose %d",outimage,flag_verbose);

  sprintf(event,"Fitscombine Call: %s ",fitscombinecall);
    reportevt(flag_verbose,STATUS,1,event);
    system(fitscombinecall);
    fclose(fimageid);
  /* clean up */
  //system("rm coadd.weight.fits");

  /* free memory */
  free(image); free(ID);
  return(0);
}
