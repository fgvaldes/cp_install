#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "kdtree.h"

#define MAXLINE 5000

typedef struct Star{
	int id;
	double ra;
	double dec;
	double class_star;
	char band;
}Star;

typedef struct match{
	Star coadd;
	Star *objs;
}match;

Star *readObjects(void);
Star *readCoadds(void);
void queryObjects(char *, char*);
void queryCoadds(char *,char *);
char *getRecentRun(char *);
char *randstr(int);
void cleanUp(void);
double *eqToXYZ(double, double);

match *find_matches(Star *, Star *);
double **averageClassStars(match *);
void printCsToFile(match *, double **, char *, char *);
void initFiles(void);

unsigned long numOfCoadds=0;
char *spoolfile, *queryfile, *pipefile;
int debug=0;

int main(int nargs, char *args[]){
	Star *coadds;
	Star *objs;
	match *matches;
	if (nargs<4){
		printf("\n  purpose: average spread_model from the single-epoch objects used to create the coadds. \n");
		printf("\n  usage: coadd2spread_model <project> <tilename> <band>\n"); 
		printf("\n  output: a .spread_<band> file with columns: ra,dec,coadd spread_model, average of singe epoch spread_models");
		printf("\n  a value of 0.0000 for the average means that no matches were found. \n");
		exit(1);
	}
	initFiles();
	if(debug){printf("\nqueryCoadds");fflush(stdout);}
	queryCoadds(getRecentRun(args[2]),args[3]);
	if(debug){printf("\nreadCoadds");fflush(stdout);}
	coadds=readCoadds();/*DO NOT CHANGE THE ORDER OF THESE COMMANDS*/
	if(debug){printf("\nqueryObjects");fflush(stdout);}
	queryObjects(args[1],args[2]);
	if(debug){printf("\nreadObjects");fflush(stdout);}
	objs = readObjects();
	if(debug){printf("\nfind_matches");fflush(stdout);}
	matches = find_matches(coadds, objs);
	if(debug){printf("\nprintCsToFile");fflush(stdout);}
	printCsToFile(matches, averageClassStars(matches), args[2], args[3]);
	if(debug){printf("\ncleanUp");fflush(stdout);}
	cleanUp();
	printf("done.\n");
	return 0;
}

void initFiles(void){
	char *suffix;
	srand(time(NULL));
	suffix = randstr(10);
	queryfile = calloc(MAXLINE, sizeof(char));
	spoolfile = calloc(MAXLINE, sizeof(char));
	pipefile = calloc(MAXLINE , sizeof(char));
	sprintf(queryfile, "/tmp/__find_matches_query_%s.sql", suffix);
	sprintf(spoolfile, "/tmp/__find_matches_spool_%s.lst", suffix);
	sprintf(pipefile, "/tmp/__find_matches_pipe_%s", suffix);
	free(suffix);
}

void cleanUp(void){
	//char *cm = calloc(MAXLINE, sizeof(char));
	//sprintf(cm, "rm %s", pipefile);   system(cm);
	//sprintf(cm, "rm %s", queryfile);  system(cm);
	//sprintf(cm, "rm %s", spoolfile);  system(cm);
    //free(cm);
	free(queryfile);
	free(spoolfile);
	free(pipefile);
	printf("\n");fflush(stdout);
}

char *randstr(int len) {
	const char *chars = "abcdefghijklmnopqrstuvwxyz"
 						"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
 						"0123456789_";
 	int max = strlen(chars);
 	int i;
 	char *str = calloc(len+1, sizeof(char));
 
 	for (i=0 ; i < len; ++i ) {
 		str[i] = chars[rand()%max];
 	}
 	str[++i] = '\0';
 	return str;
}

char *getRecentRun(char *tile){
    FILE *fp;
    char *line = calloc(MAXLINE, sizeof(char));
    char *run = calloc(MAXLINE, sizeof(char));
    int i;

    fp = fopen(queryfile, "w");
    fprintf(fp, "select distinct(run) from coadd where tilename='%s' order by run;", tile);
    fclose(fp);

    fp = fopen(pipefile, "w");
    fprintf(fp, "#!/bin/bash\n\n");
    fprintf(fp, "sqlplus -s pipeline/dc01user@desdb/prdes <<!\n");
    fprintf(fp, "set linesize 1000 trimspool on heading off echo off feedback off termout off verify off pagesize 0\n");
    fprintf(fp, "spool %s\n", spoolfile);
    fprintf(fp, "@%s\n", queryfile);
    fprintf(fp, "spool off\nexit\n!\n");
    fclose(fp);
    sprintf(line, "chmod +x %s", pipefile);
    system(line);
    system(pipefile);
    sprintf(line, "rm %s %s", pipefile, queryfile);
    system(line);

    fp=fopen(spoolfile, "r");
    for(i=0; ; i++){
        if (fgets(line, MAXLINE, fp)!=NULL) memcpy(run, line, MAXLINE);
        else break;
    }
    fclose(fp);
    sprintf(line, "rm %s", spoolfile);
    system(line);
    if (run[strlen(run)-1]=='\n') run[strlen(run)-1]='\0';
    if (debug) printf("\nusing run: %s", run);
    return run;
}

void queryCoadds(char *run, char *band){
	FILE *fp;
	int i;
	char *cm = calloc(MAXLINE, sizeof(char));
   
	printf("\nquerying db for coadd objects."); 
	fflush(stdout);
	if((fp = fopen(queryfile,"w"))==NULL){printf("\nfailed to open %s for writing, exiting.\n");exit(1);}
	fprintf(fp, "select a.ra,a.dec, a.spread_model_%s from coadd_objects a, coadd b where a.imageid_%s=b.id and b.run='%s';", band,band,run);
	fclose(fp);
	
	if((fp = fopen(pipefile, "w"))==NULL){printf("could not open %s for writing, exiting.", pipefile);exit(1);}
	fprintf(fp, "#!/bin/bash\n\n");
	fprintf(fp, "sqlplus -s pipeline/dc01user@desdb/prdes <<! >/dev/null\n");
	fprintf(fp, "set linesize 200 trimspool on heading off echo off feedback off termout off verify off pagesize 0\n");
	fprintf(fp, "spool %s\n", spoolfile);
	fprintf(fp, "@%s", queryfile);
	fprintf(fp, "\nspool off\nexit\n!\n");
	fclose(fp);

	sprintf(cm, "chmod +x %s", pipefile); 	system(cm);
	sprintf(cm, pipefile); 					system(cm);
	free(cm);
	return;
}

Star *readCoadds(void){
	Star *objects;
	long i;
	FILE *fp;
	char *line = calloc(MAXLINE, sizeof(char));
	char *cm = calloc(MAXLINE, sizeof(char));
	if ((fp=fopen(spoolfile, "r"))==NULL){printf("\n\nfailed to open %s for reading, exiting.\n",spoolfile);exit(1);}
	for(i=1; fgets(line,MAXLINE,fp)!=NULL; i++)
		;
	fclose(fp);
	objects = calloc(i+1, sizeof(Star));
	printf("read in %ld objects.",i);fflush(stdout);
	i=0;
	fp = fopen(spoolfile, "r");
	for(i=0; fgets(line, MAXLINE, fp)!=NULL; i++){
		objects[i].id = 0;
		objects[i].ra = strtod(strtok(line, " "), NULL);
		objects[i].dec = strtod(strtok(NULL, " "), NULL);
		objects[i].class_star=strtod(strtok(NULL, " "), NULL);
		//objects[i].band=strtok(NULL, " ")[0]; 
	}
	objects[i].id=-1;
	fclose(fp);
	free(line);
	sprintf(cm, "rm %s %s %s", pipefile,queryfile,spoolfile);
	system(cm);
	free(cm);
	return objects;
}

void queryObjects(char *project, char *tile){
	FILE *fp;
	int i;
	char *cm = calloc(MAXLINE, sizeof(char));
   
	printf("\nquerying db for single epoch objects."); 
	fflush(stdout);
	if((fp = fopen(queryfile,"w"))==NULL){printf("\nfailed to open %s for writing, exiting.\n");exit(1);}
	fprintf(fp, "select a.ra, a.dec, spread_model from objects a, image b where a.imageid=b.parentid and b.project='%s' and a.band=b.band and b.run >'200910'" 
		"and b.imagetype='remap' and b.tilename='%s';", project,tile);
	fclose(fp);	
	if((fp = fopen(pipefile, "w"))==NULL){printf("could not open %s for writing, exiting.", pipefile);exit(1);}
	fprintf(fp, "#!/bin/bash\n\n");
	fprintf(fp, "sqlplus -s pipeline/dc01user@desdb/prdes <<! >/dev/null\n");
	fprintf(fp, "set linesize 200 trimspool on heading off echo off feedback off termout off verify off pagesize 0\n");
	fprintf(fp, "spool %s\n", spoolfile);
	fprintf(fp, "@%s", queryfile);
	fprintf(fp, "\nspool off\nexit\n!\n");
	fclose(fp);

	sprintf(cm, "chmod +x %s", pipefile); 	system(cm);
	sprintf(cm, pipefile); 					system(cm);
	free(cm);
	return;
}



Star *readObjects(void){
	Star *objects;
	long i;
	FILE *fp;
	char *line = calloc(MAXLINE, sizeof(char));
	char *cm = calloc(MAXLINE, sizeof(char));
	if ((fp=fopen(spoolfile, "r"))==NULL){printf("\n\nfailed to open %s for reading, exiting.\n",spoolfile);exit(1);}
	for(i=1; fgets(line,MAXLINE,fp)!=NULL; i++)
		;
	fclose(fp);
	objects = calloc(i+1, sizeof(Star));
	printf("read in %ld objects.",i);fflush(stdout);
	i=0;
	fp = fopen(spoolfile, "r");
	for(i=0; fgets(line, MAXLINE, fp)!=NULL; i++){
		objects[i].id = 0;
		objects[i].ra = strtod(strtok(line, " "), NULL);
		objects[i].dec = strtod(strtok(NULL, " "), NULL);
		objects[i].class_star=strtod(strtok(NULL, " "), NULL);
		//objects[i].band=strtok(NULL, " ")[0]; 
	}
	objects[i].id=-1;
	numOfCoadds=i;
	fclose(fp);
	free(line);
	sprintf(cm, "rm %s %s %s", pipefile,queryfile,spoolfile);
	system(cm);
	free(cm);
	return objects;
}


match *find_matches(Star *coadds, Star *objects){
	match *m = calloc(numOfCoadds+4, sizeof(match));
	unsigned long i=1;
	FILE *fp;
	struct kdtree *kd = kd_create(3);
	struct kdres *neighbors;
	double *buf;
	char *line = calloc(MAXLINE, sizeof(char));
	int j;
	double distance_tol= 1.9392547e-05;/*2 arcsec in XYZ euclidean*/
	
	printf("\nmatching coadds with epoch objects...");fflush(stdout);
	for(i=0; objects[i].id!=-1; i++){
		buf = eqToXYZ(objects[i].ra, objects[i].dec);
		kd_insert(kd, buf, &(objects[i]));
		free(buf);
	}
    if(debug)printf("\ninsterted into kdtree\n"); fflush(stdout);
	for (i=0; coadds[i].id==0; i++) {
		buf=eqToXYZ(coadds[i].ra, coadds[i].dec);
		neighbors=kd_nearest_range(kd, buf, distance_tol);
		free(buf);
		memcpy(&m[i].coadd,&coadds[i],sizeof(Star));
		m[i].objs = (Star*)calloc((int)neighbors->size + 1, sizeof(Star));
		for (j=0; !kd_res_end(neighbors); j++){
			memcpy(&(m[i].objs[j]),kd_res_item_data(neighbors),sizeof(Star));
			kd_res_next(neighbors);
		}
		m[i].objs[j].id=-1;
		kd_res_free(neighbors);
	}
	m[i].coadd.id=-1;
	return m;
}

double **averageClassStars(match *m){
	long a,j=0;
	double **cs = calloc(numOfCoadds+1, sizeof(double *));
	double g,r,i,z;
	int gc,rc,ic,zc;
	
	for (a=0; m[a].coadd.id!=-1; a++){
		cs[a] = calloc(1, sizeof(double));
		gc=0;rc=0;ic=0;zc=0;
		g=0;r=0;i=0;z=0;
		for (j=0; m[a].objs[j].id!=-1; j++){
			/*if (m[a].objs[j].band=='g'){gc++; g+=m[a].objs[j].class_star;}
			if (m[a].objs[j].band=='r'){rc++; r+=m[a].objs[j].class_star;}
			if (m[a].objs[j].band=='i'){ic++; i+=m[a].objs[j].class_star;}
			if (m[a].objs[j].band=='z'){zc++; z+=m[a].objs[j].class_star;}*/
			g+=m[a].objs[j].class_star;
		}
		/*cs[a][0]=(gc ? (double)g/(double)gc : 0.0);
		cs[a][1]=(rc ? (double)r/(double)rc : 0.0);
		cs[a][2]=(ic ? (double)i/(double)ic : 0.0);
		cs[a][3]=(zc ? (double)z/(double)zc : 0.0);*/
		cs[a][0]=(j ? (double)g/(double)j : 0.0);
		
	}
	cs[a]=calloc(1,sizeof(double));
	cs[a][0]=-1;
	
	return cs;
}	



double *eqToXYZ(double ra, double dec){
	double *xyz=calloc(3, sizeof(double));
    double pr=3.141592653589793238/180.0;
    double cd=cos(dec*pr);
    xyz[0]=cos(ra*pr)*cd;
    xyz[1]=sin(ra*pr)*cd;
    xyz[2]=sin(dec*pr);
    return xyz;
}

void printCsToFile(match *m, double **cs, char *tilename, char *band){
	FILE *fp;
	char *filename=calloc(strlen(tilename)+10,sizeof(char));
    int i=0;
	//printf("\n%s, %c",band, band[0]);
	strcpy(filename, tilename);
	strcat(filename,".spread_");
	strcat(filename,band);
	fp = fopen(filename,"w");
	printf("\nwriting to file %s...",filename);
	while(m[i].coadd.id!=-1 && cs[i][0]!=-1){
		fprintf(fp, "%2.4lf %2.4lf %2.4lf %2.4lf\n",m[i].coadd.ra,m[i].coadd.dec,m[i].coadd.class_star,cs[i][0]);
		i++;
	}
	fclose(fp);
	free(filename);
}
