/*
** psfnormalize.c
**
** Test platform for Emmanuel Bertin's kernel solution
**
** This program will convolve an image with a kernel provided
** by psfex.
**
** AUTHORS:  Tony Darnell    (tdarnell@uiuc.edu)
**           Emmanuel Bertin (bertin@iap.fr)
** Last commit:
**     $Revision: 8189 $
**     $Author: mtcampbe $
**     $Date: 2012-07-19 12:19:21 -0700 (Thu, 19 Jul 2012) $
*/

#define USE_THREADS 1

#include <unistd.h>
#if defined(USE_THREADS) \
&& (defined(__APPLE__) || defined(FREEBSD) || defined(NETBSD))  /* BSD, Apple */
 #include       <sys/types.h>
 #include       <sys/sysctl.h>
#elif defined(USE_THREADS) && defined(HAVE_MPCTL)               /* HP/UX */
 #include       <sys/mpctl.h>
#endif

#define VARTHRESH	9e4	/* maximum variance factor compared to average*/
#define	LONG_LONG_TYPE		/* To avoid conflicts with CFITSIO */

#include "imageproc.h"
#include "define.h"
#include "fft.h"
#include "psfpoly.h"
#include "vignet.h"
#include "fitscat_defs.h"

static int	verbose_flag;
int		get_ncpus(void);
float		*psf_moffat(int w, int h, float fwhm, float moffat_beta);

int main(int argc, char *argv[])
{
  int   nthreads=0;
  int   status=0,naxis=0,anynull,npixels,npixelspsf,wflag=1,fwhmhomo=1;
  long  xsz,ysz, xszpsf,yszpsf,zszpsf, ldum, nw;
  int   a,c,d,g,i,k,p,x,y, numhdus,hdutype, bevel, xmargin,ymargin;
  char  *filename, *psffilename, *outfilename;
  char  newoutfilename[500], str[16], type[80];
  float *psfimage,*fimg,*finalimg,*convimg, *fwgh,*finalwgh,*convwgh, *var,
	*var2, *tarpsf, *convtar,*ftar;
  int   *polgrp, *poldeg, *polpow;
  int   polnaxis, polngrp, ppow;
  float *polzero, *polscale, *xline,*xl;
  float *delta[2], *deltax,*deltay,wtfactor;
  float yimage, fwhm,beta, dx,dy, step, yvar, varthresh, invvarthresh, wm;
  double oldnorm, newnorm, norm;
  long  fpixel[3]={1,1,1};
  polystruct *poly;
  fitsfile   *fptr, *outfptr;
  desimage   image;

  void rd_desimage(), printerror();

  /*
  ** Handle command line arguments
  */
  while (1){

    static struct option long_options[] =
    {
      {"verbose", no_argument,       &verbose_flag, 1},
      {"quiet",   no_argument,       &verbose_flag, 0},
      {"help",    no_argument,       0, 'h'},
      {"unweight",  no_argument, 0, 'u'},
      {"threads", required_argument, 0, 't'},
      {"output",  required_argument, 0, 'o'},
      {"kernel",  required_argument, 0, 'k'},
      {0,0,0,0}
    };

    int option_index=0;
    c = getopt_long_only (argc, argv, "ho:k:",long_options, &option_index);

    if (c == -1) 
      break;

    switch (c)
    {
      case 0:
        if (long_options[option_index].flag != 0) 
          break;
        printf ("option %s", long_options[option_index].name);
        if (optarg) 
          printf (" with arg %s", optarg); 
        printf ("\n"); 
        break;

      case 'h':
        printf ("\nTest bed program for convolving images with polynomial kernels\n");
        printf ("\nUsage:  psfnormalize <filename> -kernel <kernel>"
                                      " -output <outfilename>\n"
                "\nOptions:\n"
                "  -help\n"
                "  -quiet\n"
                "  -threads <#>\n"
                "  -unweight\n"
                "  -verbose\n");
        abort();
        break;

      case 'o':
        outfilename = optarg;
        printf ("convolved image will go to: %s\n",outfilename);
        break;

      case 'k':
        psffilename = optarg;
        printf ("kernel file: %s\n",psffilename);
        break;

      case 't':
        nthreads = atoi(optarg);
        break;

      case 'u':
        wflag = 0;
        break;

      default:
        abort();

    }

  }

  /*
  ** deal with non-option command line args
  ** all that's left should be the filename to convolve
  */

  if (optind < argc)
   {
      filename = argv[optind++];
   }
  else
    abort();

  if (!nthreads)
     nthreads = get_ncpus();
  if (!nthreads)
  {
    nthreads = 2;
    printf("Could not count CPUs on this system: "
        "number of threads defaulted to %d\n", nthreads);
  }
  else if (verbose_flag)
    printf("number of threads: %d\n", nthreads);

  if (verbose_flag)
    printf ("filename to convolve:  %s\n",filename);

  /*
  ** End of command line parsing
  */

  /*
  ** read in the des image
  */

  sprintf(image.name,"%s",filename);
  rd_desimage(&image,READONLY,0); 
  printf ("after reading bpm \n") ; 
  xsz = image.axes[0];
  ysz = image.axes[1];
  npixels = xsz*ysz;
  
  /*
  ** Read in the psf image
  */
  if (fits_open_file(&fptr,psffilename,READONLY,&status)) printerror(status);

  if (fits_get_num_hdus(fptr,&numhdus,&status)==KEY_NO_EXIST) 
  {
              printf("  Can't get number of hdus from %s\n",psffilename);
                          printerror(status);
  }

  /* Go to the first extension in the kernel MEF file
  ** Right now, I'm not handling this intelligently, I'm expecting
  ** an MEF with one extension
  ** TODO:  Make this smarter
  */

  if (fits_movabs_hdu(fptr,1,&hdutype,&status)) printerror(status);

  if (fits_get_img_dim(fptr,&naxis,&status) ) 
  {
     printf("  Can't get number of dims for%s\n ",psffilename);
                          printerror(status);
  }

  if ((naxis != 2) && (naxis != 3))
  {
    printf("  Wrong dimensionality of psf kernel %d\n",naxis);
    printerror(status);
    exit(1);
  }
  if (fits_read_key_lng(fptr,"NAXIS1",&xszpsf,NULL,&status)==KEY_NO_EXIST) 
  {
     printf("  Keyword NAXIS1 not defined in %s\n",psffilename);
                          printerror(status);
  }
  if (fits_read_key_lng(fptr,"NAXIS2",&yszpsf,NULL,&status)==KEY_NO_EXIST) 
  {
     printf("  Keyword NAXIS2 not defined in %s\n",psffilename);
                          printerror(status);
  }

  if (naxis == 3)
  {
    if (fits_read_key_lng(fptr,"NAXIS3",&zszpsf,NULL,&status)==KEY_NO_EXIST)
    {
      printf("  Keyword NAXIS3 not defined in %s\n",psffilename);
      printerror(status);
    }
  }
  else
     zszpsf = 1;

  if (zszpsf>1)
  {
     if (fits_read_key_lng(fptr,"POLNGRP",&ldum,NULL,&status)==KEY_NO_EXIST) 
     {
        printf("  Keyword POLNGRP not defined in %s\n",psffilename);
        printerror(status);
     }
     polngrp = ldum;
     QMALLOC(poldeg, int, polngrp);
     for (g=0; g<polngrp; g++)
        {
           sprintf(str, "POLDEG%d", g+1);
           if (fits_read_key_lng(fptr,str,&ldum,NULL,&status)==KEY_NO_EXIST) 
           {
             printf("  Keyword %s not defined in %s\n", str, psffilename);
             printerror(status);
           }
           poldeg[g] = ldum;
        }
     if (fits_read_key_lng(fptr,"POLNAXIS",&ldum,NULL,&status)==KEY_NO_EXIST) 
     {
        printf("  Keyword POLNAXIS not defined in %s\n",psffilename);
        printerror(status);
     }
     polnaxis = ldum;
     if (polnaxis>2)
     {
        printf("  Current version does not support POLNAXIS>2\n");
        printerror(status);
     }

     QMALLOC(polgrp, int, polnaxis);
     QMALLOC(polzero, float, polnaxis);
     QMALLOC(polscale, float, polnaxis);
     for (a=0; a<polnaxis; a++)
     {
        sprintf(str, "POLGRP%d", a+1);
        if (fits_read_key_lng(fptr, str, &ldum,NULL,&status)==KEY_NO_EXIST) 
        {
           printf("  Keyword %s not defined in %s\n", str, psffilename);
        }
        polgrp[a] = ldum;
        sprintf(str, "POLZERO%d", a+1);
        if (fits_read_key_flt(fptr, str, &polzero[a],NULL,&status)==KEY_NO_EXIST) 
        {
          printf("  Keyword %s not defined in %s\n", str, psffilename);
        }
        sprintf(str, "POLSCAL%d", a+1);
        if (fits_read_key_flt(fptr, str, &polscale[a],NULL,&status)==KEY_NO_EXIST) 
        {
          printf("  Keyword %s not defined in %s\n", str, psffilename);
        }
     }  
     poly = poly_init(polgrp, polnaxis, poldeg, polngrp);
     polpow = poly_powers(poly);
     free(polgrp);
     free(poldeg);
  }

  if (fits_read_key_flt(fptr, "PSF_FWHM", &fwhm,NULL,&status)==KEY_NO_EXIST) 
  {
    printf("  Keyword PSF_FWHM not defined in %s\n", psffilename);
  }
  if (fits_read_key_flt(fptr, "PSF_BETA", &beta,NULL,&status)==KEY_NO_EXIST) 
  {
    printf("  Keyword PSF_BETA not defined in %s\n", psffilename);
  }

  if (verbose_flag)
    printf("There are %d hdus in %s\n",numhdus,psffilename);

  if (verbose_flag)
    printf("There are %d terms in the psf datacube \n",naxis);

  npixelspsf = xszpsf*yszpsf;

  /*
  ** loop through each dimension in the kernel
  ** (or term in the polynomial)
  ** Read in current term
  ** convolve image with that term
  ** add result to previous convolution
  */
  QCALLOC(psfimage,float,npixelspsf);
  QCALLOC(convimg,float,npixels);
  QCALLOC(finalimg,float,npixels);
  if (image.varim && wflag)
  {
    QCALLOC(convwgh,float,npixels);
    QCALLOC(finalwgh,float,npixels);
  }

  if (zszpsf>1)
  {
     /*
     ** Create x gradient image
     */
     QCALLOC(delta[0],float,npixels);
     QCALLOC(xline,float,xsz);

     xl=xline;
     for (x=0; x<xsz; x++)
        *(xl++) = ((float)x-polzero[0])/polscale[0];

     deltax = delta[0];
     for (y=ysz; y--;)
     {
        xl = xline;
        for (x=xsz; x--;)
           *(deltax++) = *(xl++);
     }

     free(xline);

     /*
     ** Create y gradient image
     */

     QCALLOC(delta[1],float,npixels);
     deltay = delta[1];
     for (y=0; y<ysz; y++)
     {
        yimage = ((float)y-polzero[1])/polscale[1];
        for (x=xsz; x--;)
           *(deltay++) = yimage;
     }
  }

  fft_init(nthreads);
  fimg = fft_rtf(image.image,xsz,ysz);
  if (image.varim && wflag)
  {
  /*
  ** compute typical weight values and convert weights to variance
  */
    var = image.varim;
    wm = 0.0;
    nw = 0;
    for (p=ysz*xsz; p--; var++)
      if (*var>0.0)
        {
        wm += *var;
        nw++;
        }
    varthresh = (wm >0.0 && nw>0)? VARTHRESH*nw/wm : VARTHRESH;
    var = image.varim;
    invvarthresh = 1.0/varthresh;
    for (p=ysz*xsz; p--; var++)
      if (*var>invvarthresh)
        *var = 1.0 / *var;
      else
        *var = varthresh;
    fwgh = fft_rtf(image.varim,xsz,ysz);
 
 /*
  ** compute target PSF model
  */
    tarpsf = psf_moffat(xszpsf, yszpsf, fwhm, beta);
    QMALLOC(convtar, float, npixels);
    vignet_copy(tarpsf,xszpsf,yszpsf,convtar,xsz,ysz,0,0,VIGNET_CPY);
    fft_shift(convtar,xsz,ysz);
    ftar = fft_rtf(convtar,xsz,ysz);
    free(convtar);
  }

  /*
  ** Perform the actual convolution
  */

  for (k=0;k<zszpsf;k++)
  {
    fpixel[2]=k+1;

    if (fits_read_pix(fptr,TFLOAT,fpixel,npixelspsf, NULL,psfimage,&anynull,&status))
      printerror(status);

    if (verbose_flag)
      printf("Convolving image with term %d\n",k+1);

    /*
    ** Perform convolution
    */
    vignet_copy(psfimage,xszpsf,yszpsf,convimg,xsz,ysz,0,0,VIGNET_CPY);
    fft_shift(convimg,xsz,ysz);
    fft_conv(convimg,fimg,xsz,ysz);
    if (image.varim && wflag)
    {
    /*
    ** Convolve variance map with the square of (kernel convolved with PSF)
    */
      if (verbose_flag)
        printf("Convolving weight-map with squared term %d\n",k+1);
      vignet_copy(psfimage,xszpsf,yszpsf,convwgh,xsz,ysz,0,0,VIGNET_CPY);
      fft_shift(convwgh,xsz,ysz);
      fft_conv(convwgh,ftar,xsz,ysz);
      vignet_copy(convwgh,xsz,ysz,convwgh,xsz,ysz,0,0,VIGNET_MUL);
      fft_conv(convwgh,fwgh,xsz,ysz);
    }
    /*
    ** End of convolution
    */

    /*
    ** Apply variations in x and y to image as 
    ** specified in kernel
    */
    if (k)
    {
       for (d=0; d<poly->ndim; d++)
       {
          ppow = polpow[k*poly->ndim+d];
          for (p=ppow; p--;)
          {
            vignet_copy(delta[d],xsz,ysz,convimg,xsz,ysz,0,0,VIGNET_MUL);
            if (image.varim && wflag)
            {
              vignet_copy(delta[d],xsz,ysz,convwgh,xsz,ysz,0,0,VIGNET_MUL);
              vignet_copy(delta[d],xsz,ysz,convwgh,xsz,ysz,0,0,VIGNET_MUL);
            }
          }
       }
    }

    /*
    ** add the current term to the last
    */
    vignet_copy(convimg,xsz,ysz,finalimg,xsz,ysz,0,0,VIGNET_ADD);
    if (image.varim && wflag)
    {
      vignet_copy(convwgh,xsz,ysz,finalwgh,xsz,ysz,0,0,VIGNET_ADD);
    }
  }
  if (image.varim && wflag)
  {
  /*
  ** Use the square of the PSF as a norm
  */
    if (verbose_flag)
      printf("Normalizing weight-map");
    var = tarpsf;
    norm = 0.0;
    for (p=yszpsf*xszpsf; p--; var++)
      norm += *var**var;
    wm = norm > 0.0? 1.0/norm: 1.0;
    var = finalwgh;
    for (p=ysz*xsz; p--; var++)
      if (*var < varthresh)
        *var *= wm;
    }


  fft_end(nthreads);

  if (zszpsf>1)
  {
     /*
     ** Free some memory
     */
     free(polzero);
     free(polscale);
     free(polpow);
     for (a=0; a<polnaxis; a++)
       free(delta[a]);
     poly_end(poly);
  }
  free(psfimage);
  free(fimg);
  free(convimg);
  if (image.varim && wflag)
  {
    free(fwgh);
    free(convwgh);
    free(tarpsf);
    free(ftar);
  }

  if (image.varim)
  {
    if (wflag)
    {
  /*
  ** convert variance back to weights
  */
      newnorm = 0.0;
      var = finalwgh;
      for (p=ysz*xsz; p--; var++)
        if (*var < varthresh)
          newnorm += (*var = 1.0 / *var);
        else
          *var = 0.0;

  /*
  ** compute average weight-map rescaling factor
  */
      var = image.varim;
      oldnorm = 0.0;
      for (p=ysz*xsz; p--;)
        oldnorm += 1.0/ *(var++);

    printf("weight scaling factor: %10.4g\n",
	oldnorm > 1.0e-30? newnorm/oldnorm : 1.0e+30);
    }
  /*
  ** Trim the weight-map
  */
    bevel = 0 /*(int)(fwhm+1.4999)*/;	/* Removed smooth transitions: they */
    step = bevel>0? 1.0/bevel : 1.0;	/* do not represent true variances */
  /*
  ** Trim along x
  */
    if (verbose_flag)
      printf("Trimming weight-map\n");
    QCALLOC(xline,float,xsz);
    xmargin = xszpsf/2;
    if (xsz > 2*xmargin+(4+1)*bevel)
    {
      xl=xline+xmargin;
      dx = 0.0;
      for (x=bevel; x--; dx+=step)
        *(xl++) = 0.5*dx*dx;
      for (x=bevel; x--; dx-=step)
        *(xl++) = 1.0-0.5*dx*dx;
      for (x=xsz-2*xmargin-4*bevel; x--;)
        *(xl++) = 1.0;
      dx = 0.0;
      for (x=bevel; x--; dx+=step)
        *(xl++) = 1.0-0.5*dx*dx;
      for (x=bevel; x--; dx-=step)
        *(xl++) = 0.5*dx*dx;
      var = wflag? finalwgh : image.varim;
      for (y=ysz; y--;)
      {
        xl = xline;
        for (x=xsz; x--;)
          *(var++) *= *(xl++);
      }
    }
    free(xline);
  /*
  ** Trim along y
  */
    ymargin = yszpsf/2;
    if (ysz > 2*ymargin+(4+1)*bevel)
    {
      var = wflag? finalwgh : image.varim;
      for (y=ymargin; y--;)
      {
        for (x=xsz; x--;)
          *(var++) *= 0.0;
      }
      dy = 0.0;
      for (y=bevel; y--; dy+=step)
      {
        yvar = 0.5*dy*dy;
        for (x=xsz; x--;)
          *(var++) *= yvar;
      }
      for (y=bevel; y--; dy-=step)
      {
        yvar = 1.0-0.5*dy*dy;
        for (x=xsz; x--;)
          *(var++) *= yvar;
      }
      var = (wflag? finalwgh : image.varim) + (ysz-ymargin-2*bevel)*xsz;
      dy = 0.0;
      for (y=bevel; y--; dy+=step)
      {
        yvar = 1.0-0.5*dy*dy;
        for (x=xsz; x--;)
          *(var++) *= yvar;
      }
      for (y=bevel; y--; dy-=step)
      {
        yvar = 0.5*dy*dy;
        for (x=xsz; x--;)
          *(var++) *= yvar;
      }
      for (y=ymargin; y--;)
      {
        for (x=xsz; x--;)
          *(var++) *= 0.0;
      }
    }
  }

  fits_close_file(fptr,&status);

  /*
  ** save the resulting file
  **
  ** Write the convolved image to the first hdu
  ** and copy the rest over.
  ** 
  */
  sprintf(newoutfilename,"!%s",outfilename);

  if (fits_open_file(&fptr,filename,READONLY,&status))
    printerror(status);
  if (fits_get_num_hdus(fptr,&numhdus,&status))
    printerror(status);

  if (fits_create_file(&outfptr,newoutfilename,&status))
    printerror(status);
  if (fits_copy_hdu(fptr,outfptr,1,&status))
    printerror(status);
  wtfactor = oldnorm > 1.0e-30? newnorm/oldnorm : 1.0e+30 ;
  if (fits_write_key(outfptr,TFLOAT,"PSFSCALE",&wtfactor,
	"PSF homogenization weight factor",&status))
    printerror(status);
  if (fits_write_key(outfptr,TFLOAT,"PSF_FWHM",&fwhm,
	"FWHM of target PSF",&status))
    printerror(status);
  if (fits_write_key(outfptr,TINT,"FWHMHOMO",&fwhmhomo,
		     "",&status))
    printerror(status);
  if (fits_write_key(outfptr,TFLOAT,"PSF_BETA",&beta,
	"Moffat beta of target PSF",&status))
    printerror(status);

   if (fits_write_img(outfptr,TFLOAT,1,npixels,finalimg,&status))
    printerror(status);
  /*
  ** Go through the remaining hdus and copy them
  */
  for (i=1;i<numhdus;i++){
    if (fits_movrel_hdu(fptr,1,&hdutype,&status)) printerror(status);
    if (image.varim) {
      if (fits_read_key_str(fptr,"DES_EXT",type,NULL,&status)==KEY_NO_EXIST) {
        if (verbose_flag) {
	  printf("No DES_EXT keyword found in %s", filename);
	  printerror(status);
	  }
	sprintf(type,"IMAGE"); 
	}
      if (!strcmp(type,"VARIANCE") || !strcmp(type,"SIGMA")
	|| !strcmp(type,"WEIGHT")) {
        if (fits_copy_hdu(fptr,outfptr,1,&status)) printerror(status);
      if (fits_write_img(outfptr,TFLOAT,1,npixels, wflag? finalwgh:image.varim,
	&status)) printerror(status);
      } else if (fits_copy_hdu(fptr,outfptr,1,&status)) printerror(status);
    } else if (fits_copy_hdu(fptr,outfptr,1,&status)) printerror(status);
  }
  if (fits_get_num_hdus(outfptr,&numhdus,&status)) printerror(status);

  if (fits_close_file(fptr,&status)) printerror(status);
  if (fits_close_file(outfptr,&status)) printerror(status);

  /*
  ** Free more memory
  */
  free(finalimg);
  if (image.varim && wflag)
    free(finalwgh);

  return(0);

}

#ifdef USE_THREADS
/****** get_ncpus *************************************************************
PROTO	int get_ncpus(void)
PURPOSE Find the number of active CPU cores.
INPUT   -.
OUTPUT  Number of CPU found, or 0 if not possible.
NOTES   -
AUTHOR  E. Bertin (IAP)
VERSION 14/05/2009
 ***/
int	get_ncpus(void)
  {
   int	nproc;

/*-- Get the number of processors for parallel builds */
/*-- See, e.g. http://ndevilla.free.fr/threads */
    nproc = 0;
#if defined(_SC_NPROCESSORS_ONLN)               /* AIX, Solaris, Linux */
    nproc = (int)sysconf(_SC_NPROCESSORS_ONLN);
#elif defined(_SC_NPROCESSORS_CONF)
    nproc = (int)sysconf(_SC_NPROCESSORS_CONF);
#elif defined(__APPLE__) || defined(FREEBSD) || defined(NETBSD) /* BSD, Apple */
    {
     int        mib[2];
     size_t     len;

     mib[0] = CTL_HW;
     mib[1] = HW_NCPU;
     len = sizeof(nproc);
     sysctl(mib, 2, &nproc, &len, NULL, 0);
     }
#elif defined (_SC_NPROC_ONLN)                  /* SGI IRIX */
    nproc = sysconf(_SC_NPROC_ONLN);
#elif defined(HAVE_MPCTL)                       /* HP/UX */
    nproc =  mpctl(MPC_GETNUMSPUS_SYS, 0, 0);
#endif

  return nproc;
  }
#endif


/****** psf_moffat *********************************************************
PROTO	float	*psf_moffat(int w, int h, float fwhm, float moffat_beta)
PURPOSE	Generate an image of a Moffat PSF (Moffat 1969, A&A 3,455).
INPUT	Pointer to the PSF raster size array,
	PSF FWHM,
	PSF Moffat Beta
OUTPUT	Pointer to the generated image.
NOTES	-.
AUTHOR	E. Bertin (IAP)
VERSION	24/05/2011
 ***/
float	*psf_moffat(int w, int h, float fwhm, float moffat_beta)
  {
   double	dval;
   float	*psf,*psft,
		dx,dy,dy2, fac, cxx, a, beta, dx0,dy0, dxstep,dystep, xc,yc,
		r2, r2max;
   int		p, x,y, xd,yd, nsubpix, pixsize;

  xc = (float)(w/2);
  yc = (float)(h/2);
  nsubpix = 1;		/* PSF oversampling factor */
  pixsize = 1.0;	/* pixel footprint in sampling units */
  beta = -moffat_beta;
  fac = 4*(powf(2, -1.0/beta) - 1.0);
  cxx = fac/(fwhm*fwhm);
  r2max = xc<yc? xc*xc : yc*yc;
  dystep = dxstep = pixsize / nsubpix;
  dy0 = -yc - 0.5*(nsubpix - 1.0)*dystep;
  QCALLOC(psf, float, w*h); 
  psft = psf;
  for (yd=nsubpix; yd--; dy0+=dystep)
    {
    dx0 = -xc - 0.5*(nsubpix - 1.0)*dxstep;
    for (xd=nsubpix; xd--; dx0+=dxstep)
      {
      psft = psf;
      dy = dy0;
      for (y=h; y--; dy+=1.0)
        {
        dy2 = dy*dy;
        dx = dx0;
        for (x=w; x--; dx+=1.0)
          *(psft++) += (r2=dx*dx+dy2)<=r2max? powf(1.0+cxx*r2, beta) : 0.0;
        }
      }
    }

  dval = 0.0;
  psft = psf;
  for (p=w*h; p--;)
    dval += *(psft++);
  if (dval>0.0)
    {
    a = 1.0/dval;
    psft = psf;
    for (p=w*h; p--;)
      *(psft++) *= a;
    }

  return psf;
  }



