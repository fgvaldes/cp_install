/*$Id: extractstars_usnob.c 2542 2008-12-01 21:36:26Z desai $*/
#include "imageproc.h"


void extractstars_usnob(ramin,ramax,decmin,decmax,tra,tdec,b2,r2,i2,match)
float ramin,ramax,decmin,decmax, b2[], r2[], i2[] ;
int *match ;
double tra[],tdec[] ;
{
	char	sqlcall[200],tmp[10],command[100];
	int	i,j,flag_quiet=0,flag_ccd=0,flag_band=0,
		nomatches,nobjects,
		ntruth,flag_nite=0,flag_noquery=0;
	float temp ; 
	FILE	*pip,*out,*inp,*outnm;
	

/*	if (argc<4) {
	  printf("  grabmatches <RAmin> <RAmax> <DECmin> <DECmax>\n");
	  exit(0);
	}
	sscanf(argv[1],"%f",&ramin);
	sscanf(argv[2],"%f",&ramax);
	sscanf(argv[3],"%f",&decmin);
	sscanf(argv[4],"%f",&decmax); */
	/* make sure values are in proper order */
	if (ramax<ramin) {temp=ramin;ramin=ramax;ramax=temp;}
	if (decmax<decmin) {temp=decmin;decmin=decmax;decmax=temp;}
	
	/*  process the rest of the command line */
        /* set up generic db call */
        sprintf(sqlcall,"sqlplus -S pipeline/dc01user@desdb/prdes < grabmatches3.sql > grabmatches.tmp");


	/* construct a query */
	if (!flag_quiet && !flag_noquery) printf("  Constructing a query to return objects\n");
	out=fopen("grabmatches3.sql","w");
	fprintf(out,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000; \n");
	fprintf(out,"SET TERMOUT OFF;\n");
        fprintf(out,"SPOOL grabmatches.truth;\n");
	fprintf(out,"SELECT RA,DEC,B2,R2,I2 FROM USNOB_CAT1\n");
	fprintf(out,"WHERE (RA between %.7f and %.7f)\n",
	  ramin,ramax);
	fprintf(out,"and (DEC between %.7f and %.7f)\n",
	  decmin,decmax);
	fprintf(out," and ((B2 between 1 and 13)\n");
	fprintf(out," or (R2 between 1 and 13)\n");
	fprintf(out," or (I2 between 1 and 13))\n");
	fprintf(out,"and (I2 between 1 and 99)\n");
	fprintf(out,"and (R2 between 1 and 20)\n");
	fprintf(out,"and (B2 between 1 and 99)\n");
	fprintf(out,"order by RA, DEC ;\n");
        fprintf(out,"SPOOL OFF;\n"); 
	fprintf(out,"exit;\n");
	fclose(out);

	if (!flag_quiet && !flag_noquery) printf("Executing query of tables USNOB catalog\n");
/*	if (!flag_noquery) system(sqlcall); */
	system(sqlcall);
	/* count the table */
	sprintf(command,"wc  grabmatches.truth");
/*	pip=popen(command,"r");
	fscanf(pip,"%d",&ntruth);
	pclose(pip);
	if (!ntruth) {
	  printf("  ** There are no sources found in this region in \n");
	  exit(0);
	}

	else if (!flag_quiet) printf("  Reading %d objects in truth tables\n",ntruth);
*/	/* allocate space for this information */
/*	tra=(double *)calloc(ntruth,sizeof(double));
	tdec=(double *)calloc(ntruth,sizeof(double));
	tg=(float *)calloc(ntruth,sizeof(float));
	tindex=(unsigned long *)calloc(ntruth+1,sizeof(unsigned long)); 
*/
	/* read tables and start matching */
	inp=fopen("grabmatches.truth","r");
	printf("opening file %d\n",ntruth);
	i=0; 

	while (fscanf(inp,"%lf %lf %f %f %f\n",tra+i,tdec+i,b2+i,r2+i,i2+i)!=EOF)  
	{
	  i++; 
	} 
	*match = i ;
	fclose(inp);
/*	free(tra);
	free(tdec);
	free(tg);
	free(tindex); */
}



