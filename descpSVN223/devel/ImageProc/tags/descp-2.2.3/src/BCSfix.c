/* BCSfix.c
/*
/* This program reads photometric solutions for a nite from the psfmit table
/* and then applies them for calibration of the red and remap images from that
/* nite for a specific run 
/*
/* If a nite has been processed multiple times, then it uses the last available
/* fit for that nite.
*/



#include "imageproc.h"

main(argc,argv)
	int argc;
	char *argv[];
{
        char	command[500],sqlcall[500],nite[50],filter[6][10],project[10],
		line[1000],sgn[10],runid[100],dblogin[500],
		imagetype[100],run[100],runfilter[100],sqlfile[500];
	char    datein[100],timein[100],apm[5],temp[10],temp1[10];
	int	i,j,flag_quiet=0,nfit,nimages,nimages_image,flag,notfound=0,
		nobjects,imageid,objectid,maxzp_n,allobjects=0,nread,
		flag_ccd=0,ccdnum=0,flag_runid=0,rightfit,*imflags,
		maxfieldmatches,fieldmatches,*bestmatch,
		nnomatches,nmatches,nduplicates;
	float	zeropoint;
	db_psmfit 	*fit;
	db_zeropoint 	*zp;
	db_files	*im,newim;
	FILE	*out,*pip,*out2;
	void	select_dblogin();

	if (argc<2) {
	  printf("BCSfix <nite>\n");
	  exit(0);
	}

	sprintf(nite,"%s",argv[1]);
	if (!strncmp(nite,"bcs",3)) {
	  sprintf(project,"BCS");
	  sprintf(runfilter,"BCS200803");
	}
	if (!strncmp(nite,"scs",3)) {
	  sprintf(project,"SCS");
	  sprintf(runfilter,"SCS200804");
	}
	

	/* grab dblogin */
	select_dblogin(dblogin,DB_READONLY);

	/* set up generic db call */
	sprintf(sqlfile,"BCSfix.sql");
	sprintf(sqlcall,"sqlplus -S %s < %s",dblogin,sqlfile);



	/* ************************************************** */
	/* ******* QUERY for Images from FILES table ******** */
	/* ************************************************** */

	out=fopen(sqlfile,"w");
	fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	/* first find out how many images to expect */
	fprintf(out,"SELECT count(*)\n");
	fprintf(out,"FROM files\n");
	fprintf(out,"WHERE runiddesc like '%s\%%' AND nite='%s' and (imagetype='remap' or imagetype='reduced') ",runfilter,nite);
	fprintf(out,";\n");
	/* now ask for info on each image to be returned */
	fprintf(out,"SELECT imageID,CCD_NUMBER,BAND,NITE,IMAGETYPE,runiddesc,imagename,airmass,exptime,tilename\n");
	fprintf(out,"FROM FILES \n");
	fprintf(out,"WHERE runiddesc like '%s\%%' AND nite='%s' and (imagetype='remap' or imagetype='reduced') ;\n ",runfilter,nite);
	fprintf(out,"exit;\n");
	fclose(out);
	
	i=-1;
	pip=popen(sqlcall,"r");
	out=fopen("BCSfix_FILES.dat","w");
	while (fgets(line,1000,pip)!=NULL) {
	  if (i==-1) {
	    sscanf(line,"%d",&nimages);
	    printf("  Reading %d reduced and remap Images from FILES table\n",
	      nimages);
	    im=(db_files *)calloc(nimages,sizeof(db_files));
	    bestmatch=(int *)calloc(nimages,sizeof(int));
	    imflags=(int *)calloc(nimages,sizeof(int));
	    for (j=0;j<nimages;j++) {imflags[j]=0;bestmatch[j]=-1;}
	    zp=(db_zeropoint *)calloc(nimages,sizeof(db_zeropoint));
	    if (!flag_quiet) printf("  ** ###      IMAGEID   CCDID BAND        AIRMASS    EXPTIME     ZP     SIGMA\n");
	  }
	  else {
	    fprintf(out,"%4d %s\n",i,line); /* output current line */
	    sscanf(line,"%d %d %s %s %s %s %s %f %f %s",
		&(im[i].imageid),&(im[i].ccd),im[i].band,im[i].nite,
		im[i].imagetype,im[i].runiddesc,im[i].imagename,
		 &(im[i].airmass), &(im[i].exptime),im[i].tilename);
	  } 
	  i++;
	}
	pclose(pip);
	fclose(out);

	/* ************************************************** */
	/* ******* QUERY for Images from Image table ******** */
	/* ************************************************** */

	if (!strcmp(project,"BCS")) sprintf(nite,"20%s",nite+3);
	if (!strcmp(project,"SCS")) sprintf(nite,"%s",nite+3);

	out=fopen(sqlfile,"w");
	fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	sprintf(run,"%s",im[0].runiddesc+3);
	sprintf(run+8,"000000_%s",nite);
	fprintf(out,"SELECT count(*) FROM image where (imagetype='remap' or imagetype='red')  and nite='%s' and run='%s' and project='%s';\n",nite,run,project);
	for (i=0;i<nimages;i++) {
	  sprintf(run,"%s",im[i].runiddesc+3);
	  sprintf(run+8,"000000_%s",nite);
	  if (!strcmp(im[i].imagetype,"reduced")) {
	    sprintf(imagetype,"red");
	    fprintf(out,"SELECT ID,CCD,BAND,NITE,IMAGETYPE,run,imagename,airmass,exptime,tilename FROM image where imagetype='%s' and nite='%s' and run='%s' and project='%s' and imagename like '%s\%%' and ccd='%d' order by id;\n",imagetype,nite,run,project,im[i].imagename,im[i].ccd);
	  }
	  else {
	    sprintf(imagetype,"remap");
	  fprintf(out,"SELECT ID,CCD,BAND,NITE,IMAGETYPE,run,imagename,airmass,exptime,tilename FROM image where imagetype='%s' and nite='%s' and run='%s' and project='%s' and tilename='%s' and imagename like '%s\%%' and ccd='%d' order by id;\n",imagetype,nite,run,project,im[i].tilename,im[i].imagename,im[i].ccd);
	  }
	}
	fprintf(out,"exit;\n");
	fclose(out);

	
	out=fopen("BCSfix_IMAGE.dat","w");
	pip=popen(sqlcall,"r");
	fgets(line,1000,pip);
	sscanf(line,"%d",&nimages_image);
	nread=j=0;
	printf("  Reading %d reduced and remap Images from IMAGE table\n",
	  nimages_image);
	while(fgets(line,1000,pip)!=NULL) {
	  sscanf(line,"%d %d %s %s %s %s %s %f %f %s",
	    &(newim.imageid),&(newim.ccd),newim.band,newim.nite,
	    newim.imagetype,newim.runiddesc,newim.imagename,
	    &(newim.airmass), &(newim.exptime),newim.tilename);
	  j++;
	  fprintf(out,"%4d %s\n",j,line); /* output current line */
	  if (!strcmp(newim.imagetype,"red")) sprintf(imagetype,"reduced");
	  else if (!strcmp(newim.imagetype,"remap")) sprintf(imagetype,"remap");
	  if (!strncmp(newim.imagename+strlen(newim.imagename)-5,".fits",5)) {
	    if (!strcmp(imagetype,"reduced")) 
	      newim.imagename[strlen(newim.imagename)-8]=0;
	    if (!strcmp(imagetype,"remap")) 
	      newim.imagename[strlen(newim.imagename)-9-strlen(newim.tilename)]=0;
	  }

	  flag=0;
	/* now look for matches in the FILES table list */
	maxfieldmatches=0;
	for (i=0;i<nimages;i++) {
	  fieldmatches=0;
	  if (newim.ccd==im[i].ccd) fieldmatches++;
	  if (!strcmp(imagetype,im[i].imagetype)) fieldmatches++;
	  if (!strcmp(newim.band,im[i].band)) fieldmatches++;
	  if ((!strcmp(project,"BCS") && !strcmp(newim.nite+2,im[i].nite+3)) ||
	    (!strcmp(project,"SCS") && !strcmp(newim.nite,im[i].nite+3))) 
	    fieldmatches++;
	  if (!strcmp(newim.tilename,im[i].tilename) || !strcmp(imagetype,"reduced")) fieldmatches++;
	  if (fabs(newim.airmass-im[i].airmass)<1.0e-3) fieldmatches++;
	  if (fabs(newim.exptime-im[i].exptime)<1.0e-1) fieldmatches++;
	  if ( !strcmp(newim.imagename,im[i].imagename) ) fieldmatches++;
	  if (fieldmatches>maxfieldmatches && fieldmatches<8) { 
	    /* only some fields matched */
	    bestmatch[i]=j;
	    maxfieldmatches=fieldmatches;
	  }
	  else if (fieldmatches==8){ /* all fields matched */
	    imflags[i]++;
	    flag=1;
	    if (imflags[i]==1) {
	      if (im[i].imageid!=newim.imageid) printf("Match for files.imageid %d ==> image.id %d\n",im[i].imageid,newim.imageid);
	    }
	    else {
	      printf("Duplicate match # %d for %d ==> %d\n",imflags[i],
		im[i].imageid,newim.imageid);
	      printf("%s versus %s\n",im[i].tilename,newim.tilename);
	    }
	  }
	 }
	 if (!flag) notfound++;
	 nread++;
	}
	pclose(pip);
	fclose(out);
	printf("  Read %d files from image table\n",nread);
	printf("  Those in Files table without matches in Image:   %d\n",
	  notfound);
	nnomatches=nmatches=nduplicates=0;
	for (i=0;i<nimages;i++) {
	  if (imflags[i]==0)  {
	    nnomatches++;
	    printf("%d %s %s %s %s %d %s %s %.4f %.4f  == BEST %d\n",im[i].imageid,im[i].imagename,im[i].imagetype,im[i].band,im[i].tilename,im[i].ccd,im[i].nite,im[i].runiddesc,im[i].airmass,im[i].exptime,bestmatch[i]);
	  }
	  else if (imflags[i]==1)  nmatches++;
	  else if (imflags[i]>1) nduplicates++;
	}
	printf("  Matches: %d  No Matches: %d Duplicates: %d\n",
	  nmatches,nnomatches,nduplicates);
}


