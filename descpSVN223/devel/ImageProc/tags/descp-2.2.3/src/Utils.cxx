#include "Utils.h"
#include "TMath.h"
#include "TStyle.h"
#include "TROOT.h"
#include <map>
#include <fstream>
#include <iostream>

using namespace std;


bool get_dbinfo(string which_db,string &db,string &user,string &passwd,
                string file)
{
  if(file.empty()) file="~/.desdm";

  // read in key values from desdm file
  map<string,string> keys;

  ifstream infile(file.c_str());

  while(!infile.eof()) {
    
    string s1,s2;
    infile>>s1>>s2;
    keys[s1]=s2;
  }

  user=keys["DB_USER"];
  passwd=keys["DB_PASSWD"];

  if(which_db=="pr") {
    db="oracle://"+keys["DB_SERVER"]+"/"+keys["DB_NAME"];
  }
  else if(which_db=="st") {
    db="oracle://"+keys["DB_SERVER_STANDBY"]+"/"+
      keys["DB_NAME_STANDBY"];
  }
  else if(which_db=="fm") {
    db="oracle://"+keys["DB_SERVER_FM"]+"/"+
      keys["DB_NAME_FM"];
  }
  else if(which_db=="pg") {
    db="pgsql://"+keys["DB_SERVER_PG"]+"/"+
      keys["DB_NAME_PG"];
  }
  else {
    cout<<"Not a valid database: "<<which_db<<endl;
    exit(1);
  }

  return 1;
}


void Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters)
{
  // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}



void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,double &tol)
{
  int N=array.size();
  if(!weighted) {
    mean=TMath::Mean(N,&array[0]);
  }
  else {
    mean=TMath::Mean(N,&array[0],&err[0]);
  }
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }
  good=N-bad;
  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;
  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    

    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();
    newerr_array.clear();

    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    curerr_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;


    
  }
 

  
  return;

}


void report_evt(bool verbose,string type,int level,string mess)
{

  if( verbose) {
    if(type=="STATUS") {
      cout<<"STATUS"<<level<<"BEG "<<mess<<" STATUS"<<level<<"END"<<endl;
    }
    else if(type=="QA") {
      cout<<"QA"<<level<<"BEG "<<mess<<" QA"<<level<<"END"<<endl;
    }
    else {
      cout<<"STATUS5BEG Uknown event type "<<type<<endl;
    }
  }
  else {
    if(level==5) cout<<"** "<<mess<<" **"<<endl;
    else cout<<"  "<<mess<<endl;
  }
}



void LoadStyle()
{


  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  TStyle* miStyle = new  TStyle("miStyle", "MI Style");

  // Colors


  //  set the background color to white
  miStyle->SetFillColor(10);


  miStyle->SetFrameFillColor(10);
  miStyle->SetCanvasColor(10);
  miStyle->SetCanvasDefH(680);
  miStyle->SetCanvasDefW(700);
  miStyle->SetPadColor(10);
  miStyle->SetTitleFillColor(0);
  miStyle->SetStatColor(10);

  //  //dont put a colored frame around the plots
  miStyle->SetFrameBorderMode(0);
  miStyle->SetCanvasBorderMode(0);
  miStyle->SetPadBorderMode(0);

  //use the primary color palette
  miStyle->SetPalette(1);

  //set the default line color for a histogram to be black
  miStyle->SetHistLineColor(kBlack);

  //set the default line color for a fit function to be red
  miStyle->SetFuncColor(kBlue);

  //make the axis labels black
  miStyle->SetLabelColor(kBlack,"xyz");

  //set the default title color to be black
  miStyle->SetTitleColor(kBlack);

  // Sizes
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);


  //set the margins
  miStyle->SetPadBottomMargin(0.12);
  miStyle->SetPadTopMargin(0.1);
  miStyle->SetPadLeftMargin(0.14);
  miStyle->SetPadRightMargin(0.14);

  //set axis label and title text sizes
  miStyle->SetLabelSize(0.04,"x");
  miStyle->SetLabelSize(0.04,"y");
  miStyle->SetTitleSize(0.05,"xyz");
  miStyle->SetTitleOffset(1.1,"x");
  miStyle->SetTitleOffset(1.3,"yz");
  miStyle->SetLabelOffset(0.012,"y");
  miStyle->SetStatFontSize(0.025);
  miStyle->SetTextSize(0.02);
  miStyle->SetTitleBorderSize(0);

  //set line widths
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);

  // Misc

  //align the titles to be centered
  miStyle->SetTextAlign(22);

  //set the number of divisions to show
  // miStyle->SetNdivisions(506, "xy");

  //turn off xy grids
  miStyle->SetPadGridX(0);
  miStyle->SetPadGridY(0);

  //set the tick mark style
  miStyle->SetPadTickX(1);
  miStyle->SetPadTickY(1);

  //show the fit parameters in a box
  miStyle->SetOptFit(0111);
  miStyle->SetOptTitle(0);

  //turn off all other stats
  miStyle->SetOptStat(0);
  miStyle->SetStatW(0.20);
  miStyle->SetStatH(0.15);
  miStyle->SetStatX(0.94);
  miStyle->SetStatY(0.92);


  miStyle->SetFillStyle(0);

  //  // Fonts
  miStyle->SetStatFont(42);
  miStyle->SetLabelFont(42,"xyz");
  miStyle->SetTitleFont(42,"xyz");
  // miStyle->SetTextFont(40);

  //done

  miStyle->cd();


  // gROOT->ForceStyle(1);
 

}

void PrintSLRHelp()
{
  cout<<"Usage: slr_check -tile (tile) (options)\n";
  cout<<"Options (default):\n";
  cout<<"  -project    (none)       For looping over all tiles in BCS/DES.\n";
  cout<<"  -run        (latest)     Use a specific run.   \n";
  cout<<"  -color      (no)         Apply BCS color corrections\n";
  cout<<"  -mag        (auto)       Which magnitude to use\n";
  cout<<"  -star_cut   (0.7)        Value of class_star to determine stars\n";
  cout<<"  -err_cut    (0.2)        Maximum flux error\n";
  cout<<"  -outfile    (out.root)   Output file (only if using -project)\n";
  cout<<"  -extinction (no)         Apply extinction correction\n";
  cout<<"  -image_name ((tile).png) Name of output\n";
  cout<<"  -image_dir  (./)         Name of directory for output\n";
  cout<<"  -print      (no)         produce plots\n";
  cout<<"  -tol        (2)          matching radius\n";
  cout<<"  -mfp        (no)         use model fitting parameters\n";
}


void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol)
{
  int N=array.size();
  mean=TMath::Mean(N,&array[0]);
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  // initial flag of bad objs
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }

  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;


  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();
    newerr_array.clear();
    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;
    
  }

  

  
  return;

}
