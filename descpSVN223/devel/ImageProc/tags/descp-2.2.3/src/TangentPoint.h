/*
 * TangentPoint.h
 *
 *  Created on: Mar 22, 2012
 *      Author: kuropat
 */

#ifndef TANGENTPOINT_H_
#define TANGENTPOINT_H_
#include <iostream>
#include <string.h>

namespace std {

class TangentPoint {
public:
	char name_[50];
	double Ra;
	double Dec;
	TangentPoint();
	TangentPoint(char* , double , double );
	void set(char* , double , double);
	void print();
	virtual ~TangentPoint();
};

} /* namespace std */
#endif /* TANGENTPOINT_H_ */
