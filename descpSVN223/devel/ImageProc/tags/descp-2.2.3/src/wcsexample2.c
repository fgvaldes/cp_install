/*
**
** AUTHOR:  Emmanuel Bertin (bertin@iap.fr)
**
** Last commit:
**     $Rev: 5419 $
**     $LastChangedBy: bertin $
**     $LastChangedDate: 2010-04-18 07:15:51 +0200 (Sun, 18 Apr 2010) $
**
** Compile with gcc -O3 -g -Wall \
** 	-I../../../terapix/trunk/wcs -I../../../terapix/trunk/fits \
**	-o wcsexample2 wcsexample2.c  \
**	../../../terapix/trunk/fits/libfits.a \
**	../../../terapix/trunk/wcs/libwcs.a -lm
**
*/

#include <stdio.h>
#include <stdlib.h>
#include "fitswcs.h"

int main(argc,argv)
	int argc;
	char **argv;
{

  char *catname;
  double wcspos[2],rawpos[2];

  catstruct *cat;
  tabstruct *tab;
  wcsstruct *wcsin;

  if (argc<4)
    {
    fprintf(stderr, "SYNTAX: wcsexample2 <fits_image> <x> <y>\n");
    exit(0);
    }


  catname = argv[1];

  /*  Example image I used for testing:
  catname = "/data1/Archive/DES/red/20071031000000_20071011/red/decam--11--41-r-4/decam--11--41-r-4_37.fits";
  */

  /* calculate a wcs coord from a pixel */
  rawpos[0] = atof(argv[2]);
  rawpos[1] = atof(argv[3]);

  if (!(cat = read_cat(catname)))
     error(EXIT_FAILURE, "*Error*: No such file: ", catname);

  tab = cat->tab;

  wcsin = read_wcs(tab);
  /* x,y->alpha,delta */
  raw_to_wcs(wcsin,rawpos,wcspos);
  /* Print the results */
  printf ("x,y=%lf,%lf => alpha,delta=%lf,%lf\n",
	rawpos[0],rawpos[1],wcspos[0],wcspos[1]);

  /* alpha,delta->x,y */
  wcs_to_raw(wcsin,wcspos,rawpos);
  /* Print the results */
  printf ("alpha,delta=%lf,%lf => x,y=%lf,%lf\n",
	wcspos[0],wcspos[1],rawpos[0],rawpos[1]);

  free_cat(&cat, 1);

  return(0);
}
