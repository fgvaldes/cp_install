/*$Id: photcal.c 9099 2012-10-08 22:04:26Z rgruendl $*/
/* photcal.c
/*
/* This program reads photometric solutions for a nite from the psfmit table
/* and then applies them for calibration of the red and remap images from that
/* nite for a specific run 
/*
/* If a nite has been processed multiple times, then it uses the last available
/* fit for that nite.
*/


#include "imageproc.h"

#define BAND_ALL	5
#define BAND_g		0
#define BAND_r		1
#define BAND_i		2
#define BAND_z		3
#define BAND_Y		4
#define SOURCE_PSM	1
#define SOURCE_COADD	2
#define SOURCE_GCM	3

static const char *svn_id = "$Id: photcal.c 9099 2012-10-08 22:04:26Z rgruendl $";

main(argc,argv)
	int argc;
	char *argv[];
{
        char	command[500],sqlcall[500],nite[50],filter[6][10],project[10],
		line[1000],sgn[10],run[100],dblogin[500],srcrun[100],src[10],
		sqlfile[500],event[1000];
	char    datein[100],timein[100],apm[5],temp[10],temp1[10];
	int	i,j,flag_band=BAND_ALL,flag_quiet=0,nfit,nimages,
		nobjects,imageid,objectid,maxid,allobjects=0,nzeropoints=0,
		flag_ccd=0,ccdnum=0,flag_run=0,flag_verbose=2,nzp,toggle,
		flag_match,flag_src=SOURCE_PSM,flag_srcrun=0,rightfit,
		nmatchred=0,nmatchremap=0,nupdates=0;
     
	float	zeropoint;
	db_psmfit 	*fit;
	db_files 	*im;
	db_zeropoint 	*zp;
	FILE	*out,*pip,*out2;
	void	select_dblogin();
	time_t curtime=time (NULL), startTime,endTime;

	if (argc<2) {
	  printf("photcal <nite> <project>\n");
	  printf("  -run <run>\n");
	  printf("  -band <g r i z Y or all (default)>\n");
	  printf("  -ccd <# 0=all (default)> \n");
	  printf("  -src <PSM(default),COADD,GCM>\n");
	  printf("  -srcrun <run>\n");
	  printf("  -verbose <0-3>\n");
	  printf("  -version (print version and exit)\n");
	  exit(0);
	}

        if (!strcmp(argv[1],"-version")) {
          printf("Version: %s\n",svn_id);
          exit(0);
        }

        sprintf(event," svn_version: %s ",svn_id);
        reportevt(2,STATUS,1,event); 

	sprintf(nite,"%s",argv[1]);
	sprintf(project,"%s",argv[2]);

	for (i=2;i<argc;i++) {
          if (!strcmp(argv[i],"-verbose")) {
            sscanf(argv[++i],"%d",&flag_verbose);
            if (flag_verbose<0 || flag_verbose>3) {
              sprintf(event,"Verbose level out of range %d. Reset to 2", 
	        flag_verbose);
              flag_verbose=2;
              reportevt(flag_verbose,STATUS,3,event);
            }
          }
	  if (!strcmp(argv[i],"-band")) { 
	    i++;
	    if (i>=argc) {
	      printf("  **Must include band choice with -band directive\n");
	      exit(0);
	    }
	    if (!strcmp(argv[i],"g")) flag_band=BAND_g;
	    if (!strcmp(argv[i],"r")) flag_band=BAND_r;
	    if (!strcmp(argv[i],"i")) flag_band=BAND_i;
	    if (!strcmp(argv[i],"z")) flag_band=BAND_z;
	    if (!strcmp(argv[i],"Y")) flag_band=BAND_Y;
	    if (!strcmp(argv[i],"all")) flag_band=BAND_ALL;
	  }
	  if (!strcmp(argv[i],"-src")) {
	    sprintf(src,"%s",argv[i+1]);
	    if (!strcmp(src,"PSM")) flag_src=SOURCE_PSM;
	    else if (!strcmp(src,"COADD")) flag_src=SOURCE_COADD;
	    else if (!strcmp(src,"GCM")) flag_src=SOURCE_GCM;
	    else {
	      sprintf(event,"Zeropoint source not defined: %s",src);
	      reportevt(2,STATUS,5,event);
	      exit(0);
	    }
	  }
	  if (!strcmp(argv[i],"-run")) {
	    sprintf(run,"%s",argv[i+1]);
	    flag_run=1;
	  }
	  if (!strcmp(argv[i],"-srcrun")) {
	    sprintf(srcrun,"%s",argv[i+1]);
	    flag_srcrun=1;
	    sprintf(event,"Srcrun option not currently supported");
	    reportevt(flag_verbose,STATUS,4,event);
	  }
	  if (!strcmp(argv[i],"-ccd")) {
	    sscanf(argv[i+1],"%d",&ccdnum);
	    flag_ccd=1;
	  }
	  if (!strcmp(argv[i],"-version")) {
            printf("Version: %s\n",svn_id);
            exit(0);
	  }
	}



	sprintf(filter[BAND_g],"g");
	sprintf(filter[BAND_r],"r");
	sprintf(filter[BAND_i],"i");
	sprintf(filter[BAND_z],"z");
	sprintf(filter[BAND_Y],"Y");
	
	/* grab dblogin */
	select_dblogin(dblogin,DB_READWRITE);

	/* set up generic db call */
	sprintf(sqlfile,"photcal_%s_%s.sql",project,nite);
	sprintf(sqlcall,"sqlplus -S %s < %s",dblogin,sqlfile);

	/* ************************************************** */
	/* **************** SOURCE = PSM ******************** */
	/* ************************************************** */

	if (flag_src==SOURCE_PSM) {

	/* ************************************************** */
	/* *********** QUERY for PSM Solutions ************** */
	/* ************************************************** */
  
	  out=fopen(sqlfile,"w");
	  if (out==NULL) {
	    sprintf(event,"File open failed: %s",sqlfile);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	  fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	  /* first find out how many solutions to expect */
	  fprintf(out,"SELECT count(*)\n");
	  fprintf(out,"FROM psmfit\n");
	  /* NOTE:  CCDID=0 is when single solution for all CCD's is available */
	  fprintf(out,"WHERE NITE='%s' and PROJECT='%s' ",nite,project); 
	  //if (flag_ccd && ccdnum==0) fprintf(out," and CCDID=0 ");
	  if(flag_ccd) fprintf(out, " and CCDID=%d ",ccdnum);
	  if (flag_band!=BAND_ALL) 
	    fprintf(out," AND FILTER='%s';\n",filter[flag_band]);
	  else  fprintf(out,";\n");
  
	  /* now ask for line to be returned */
	  fprintf(out,"SELECT psmfit.PSMFIT_ID,psmfit.NITE,psmfit.FILTER,psmfit.CCDID,psmfit.A,psmfit.AERR,psmfit.B,psmfit.BERR,psmfit.K,");
	  fprintf(out,"psmfit.KERR,psmfit.RMS,psmfit.CHI2,psmfit.DOF,psmfit.STDCOLOR0,psmfit.PHOTOMETRICFLAG,psmfit.FIT_TIMESTAMP\n");
	  fprintf(out,"FROM psmfit ");
	  /* NOTE:  CCDID=0 is when single solution for all CCD's is available */
	  fprintf(out,"WHERE NITE='%s' and PROJECT='%s' ",nite,project);
	  //if (flag_ccd && ccdnum==0) fprintf(out," and CCDID=0 ");
	  if(flag_ccd) fprintf(out, " and CCDID=%d ",ccdnum);
	  if (flag_band!=BAND_ALL) 
	    fprintf(out," AND psmfit.FILTER='%s' ",filter[flag_band]);
	  fprintf(out,"order by fit_timestamp;\n");
	  fprintf(out,"exit;\n");
	  fclose(out);
  
	  
	  
	  i=-1;
	  pip=popen(sqlcall,"r");
	  if (pip==NULL) {
	    sprintf(event,"Pipe open failed: %s",sqlcall);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  while (fgets(line,1000,pip)!=NULL) {
	    if (i==-1) {
	      sscanf(line,"%d",&nfit);
	      if (flag_verbose) 
		  printf("  Reading %d solutions from psmfit\n",nfit);
	      fit=(db_psmfit *)calloc(nfit,sizeof(db_psmfit));
	      if (fit==NULL) {
	        sprintf(event,"Calloc failed: fit");
	        reportevt(flag_verbose,STATUS,5, event);
	        exit(0);
	      }
	      if (flag_verbose) 
	        printf("  ** %15s %8s %10s %23s %17s %20s %20s %20s %20s %20s %20s %22s %20s %20s %20s %20s\n",
	          "ID","Nite","FILTER","CCDID","A","AERR","B","BERR","K","KERR","RMS","CHI2","DOF","Color","PHOTFLAG","TIMESTAMP");
	    }
	    else {
	    if (flag_verbose) printf("  ** %s",line);
	    /* read information into local variables */
	    sscanf(line,"%d %s %s %d %f %f %f %f %f %f %f %f %d %f %d %s %s %s",
		   &(fit[i].fitid),fit[i].nite,fit[i].band,&(fit[i].ccd), 
		   &(fit[i].a), &(fit[i].aerr),
		   &(fit[i].b), &(fit[i].berr), &(fit[i].k), &(fit[i].kerr),
		   &(fit[i].rms), &(fit[i].chi2), &(fit[i].dof), 
		   &(fit[i].color), &(fit[i].photflag),
		   fit[i].date, fit[i].time, fit[i].pm);
	    }
	    i++;
	  }
	  pclose(pip);

	/* must filter fit array here unless we rely on db */

	}  /* end of SOURCE=PSM query section */

	/* ************************************************** */
	/* *************** SOURCE = COADD ******************* */
	/* ************ Query for Zeropoints  *************** */
	/* ************************************************** */
	if (flag_src==SOURCE_COADD) {
	  out=fopen(sqlfile,"w");
	  if (out==NULL) {
	    sprintf(event,"File open failed: %s",sqlfile);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	  fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	  fprintf(out,"select count(*) from\n");
	  fprintf(out,"zeropoint,image where zeropoint.source='COADD' and\n");
	  fprintf(out,"image.id=zeropoint.imageid and \n");
	  fprintf(out,"image.nite='%s' and image.project='%s'",nite,project);
	  if (flag_run) fprintf(out, " and image.run = '%s' ",run);
	  if (flag_ccd && ccdnum>0) fprintf(out," and image.ccd=%d ",ccdnum);
	  if (flag_band!=BAND_ALL) 
	    fprintf(out," and image.band like '%s%%';\n",filter[flag_band]);
	  else fprintf(out,";\n");
	  fprintf(out,"select zeropoint.id,zeropoint.imageid,zeropoint.mag_zero,zeropoint.sigma_mag_zero from\n");
	  fprintf(out,"zeropoint,image where zeropoint.source='COADD' and\n");
	  fprintf(out,"image.id=zeropoint.imageid\n");
	  fprintf(out,"and image.nite='%s' and image.project='%s'\n",
	    nite,project);
	  if (flag_run) fprintf(out, "and image.run = '%s'\n",run);
	  if (flag_ccd && ccdnum>0) fprintf(out,"and image.ccd=%d\n",ccdnum);
	  if (flag_band!=BAND_ALL) 
	    fprintf(out," and image.band like '%s%%'\n",filter[flag_band]);
	  fprintf(out,"order by zeropoint.imageid;\n");
	  fprintf(out,"exit;\n");
	  fclose(out);
	  
	  /* now read in the zeropoints sorted by imageid*/
	  pip=popen(sqlcall,"r");
	  if (pip==NULL) {
	    sprintf(event,"Pipe open failed: %s",sqlcall);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	  fgets(line,1000,pip);
	  sscanf(line,"%d",&nzeropoints);
	  sprintf(event,"Reading %d COADD zeropoints",nzeropoints);
	  reportevt(flag_verbose,STATUS,1,event);
	  /* allocate space for the zeropoints */
	  zp=(db_zeropoint *)calloc(nzeropoints,sizeof(db_zeropoint));
	  if (zp==NULL) {
	    sprintf(event,"Calloc failed: zp");
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  /* now read in the zeropoints */
	  i=0;
	  while(fgets(line,1000,pip)!=NULL) {
	    sscanf(line,"%d %d %f %f",&(zp[i].id),&(zp[i].imageid),
	      &(zp[i].mag_zero),&(zp[i].sigma_mag_zero));
	    i++;
	  }
	  if (i!=nzeropoints) {
	    sprintf(event,"Wrong number of zeropoints returned: %d",i);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	  pclose(pip);
	}

	/* ************************************************** */
	/* **************** SOURCE = GCM ******************** */
	/* ************************************************** */
	if (flag_src==SOURCE_GCM) {
	  reportevt(flag_verbose,STATUS,5,"GCM calibration not yet supported");
	  exit(0);
	}

	/* ************************************************** */
	/* *********** Build QUERY for Images *************** */
	/* ************************************************** */

	out=fopen(sqlfile,"w");
	if (out==NULL) {
	  sprintf(event,"File open failed: %s",sqlfile);
	  reportevt(flag_verbose,STATUS,5, event);
	  exit(0);
	}
	fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	/* first find out how many images to expect */
	fprintf(out,"SELECT count(*)\n");
	fprintf(out,"FROM image\n");
	fprintf(out,"WHERE NITE='%s' AND PROJECT='%s' ",nite,project);
	fprintf(out,"AND (imagetype='red' or imagetype='remap')\n");
	if (flag_run) fprintf(out, " and RUN = '%s' ",run);
	if (flag_ccd && ccdnum>0) fprintf(out,"AND CCD=%d ",ccdnum);
	if (flag_band!=BAND_ALL) 
	  fprintf(out,"AND BAND like '%s%%';\n",filter[flag_band]);
	else fprintf(out,";\n");
	/* now ask for info on each image to be returned */
	fprintf(out,"SELECT ID,CCD,AIRMASS,EXPTIME,PARENTID,BAND\n");
	fprintf(out,"FROM image \n");
	fprintf(out,"WHERE NITE='%s' AND PROJECT='%s' ",nite,project);
	fprintf(out,"AND (imagetype='red' or imagetype='remap')\n ");
	if (flag_run) fprintf(out, " and RUN = '%s'\n",run);
	if (flag_ccd && ccdnum>0) fprintf(out,"AND CCD=%d\n",ccdnum);
	if (flag_band!=BAND_ALL) 
	  fprintf(out,"AND BAND like '%s%%'\n",filter[flag_band]);
	fprintf(out,"order by id;\n");
	fprintf(out,"exit;\n");
	fclose(out);
	

	/* ************************************************** */
	/* ********* Execute QUERY for Images *************** */
	/* ************************************************** */
	i=-1;nzp=0;
	pip=popen(sqlcall,"r");
	if (pip==NULL) {
	  sprintf(event,"Pipe open failed: %s",sqlcall);
	  reportevt(flag_verbose,STATUS,5, event);
	  exit(0);
	}
	while (fgets(line,1000,pip)!=NULL) {
	  if (i==-1) { /* actions for the first line read */
	    sscanf(line,"%d",&nimages);
	    sprintf(event,"Reading %d red and remap Images from IMAGE table",
	      nimages);
	    reportevt(flag_verbose,STATUS,1,event);
	    im=(db_files *)calloc(nimages,sizeof(db_files));
	    if (im==NULL) {
	      sprintf(event,"Calloc failed: im");
	      reportevt(flag_verbose,STATUS,5, event);
	      exit(0);
	    }
	    if (flag_src==SOURCE_PSM) {/* allocate space for new zeropoints */
	      zp=(db_zeropoint *)calloc(nimages,sizeof(db_zeropoint));
	      if (zp==NULL) {
	        sprintf(event,"Calloc failed: zp");
	        reportevt(flag_verbose,STATUS,5, event);
	        exit(0);
	      }
	    }
	  }
	  else { /* read in the next image */
	    sscanf(line,"%d %d %f %f %d %s",&(im[i].imageid),&(im[i].ccd),
		   &(im[i].airmass), &(im[i].exptime), &(im[i].parentid),temp);
	    sscanf(temp,"%s %s %s",im[i].band,temp1,temp1);
	    printf("%s",line);
	    im[i].zeropointlink=-1; /* flag link as not existing */

	    if (flag_src==SOURCE_PSM) { /* actions for PSM call */
	      /* find the corresponding solution */
	      rightfit=-1;
	      for (j=0;j<nfit;j++) {
	        /* selects the last fit in this band/ccd */
	        if (!strcmp(im[i].band,fit[j].band)) { /* band matches */
	          if (im[i].ccd==fit[j].ccd) rightfit=j;
	          /* only use general fit if fit for ccd isn't available */
	          if (fit[j].ccd==0 && rightfit==-1) rightfit=j;
	        }
	      }
	      /* now search for general solution if specific one not found */
	      if (rightfit==-1)  /* then look for general solution */
	        for (j=0;j<nfit;j++) {
	          /* selects the last fit in this band/ccd */
	          if (!strcmp(im[i].band,fit[j].band) && fit[j].ccd==0) 
	            rightfit=j;
	        }
	      if (rightfit==-1) {
	        sprintf(event,"Did not find photometric fit for band %s ccd %d",
		  im[i].band,ccdnum);
	        reportevt(flag_verbose,STATUS,5,event);
	        exit(0);
	      }
	      sprintf(event," Im: %s/%d Fit %s/%d",im[i].band,
	        im[i].imageid,fit[rightfit].band,fit[rightfit].fitid);
	      im[i].zeropointlink=i; /* link image to zp vector*/
	      zp[i].imageid=im[i].imageid;
	      zp[i].mag_zero=-1.0*fit[rightfit].a+2.5*log10(im[i].exptime)-
	        fit[rightfit].k*im[i].airmass;
	      zp[i].sigma_mag_zero=sqrt(Squ(fit[rightfit].aerr)+
		  Squ(fit[rightfit].kerr));
	      zp[i].originid=fit[rightfit].fitid;
	      line[strlen(line)-1]=0; 
	      if (i%1==0) {
	        sprintf(event,"%s photcal: %3d- %s   %.4f %.4f",
	          event,i,line,zp[i].mag_zero,zp[i].sigma_mag_zero);
	        reportevt(flag_verbose,STATUS,1,event);
	      }
	    } /* end of PSM section */

	    /* begin COADD section */
	    else if (flag_src==SOURCE_COADD) { 
	      flag_match=0; /* reset match flag */
	      if (nzp<0) nzp=0;
	      else if (nzp>=nzeropoints) nzp=nzeropoints-1;
	      while (nzp>=0 && nzp<nzeropoints) {
	        toggle=0;
	        if (im[i].imageid<zp[nzp].imageid) {
		  nzp--;
		  toggle=1; /* note that change was made */
	        }
	        if (im[i].imageid>zp[nzp].imageid) {
		  nzp++;
		  if (toggle) break; /* no match in the list */
	        }
	        if (im[i].imageid==zp[nzp].imageid) {
		  flag_match=1;
	          break;  /*match has been found*/
	        }
	      };
	      if (flag_match) { /* store link to zeropoint */
		/* *************************************** */
		/* *************************************** */
	        /* case of multiple zeropoints not handled */
		/* *************************************** */
		/* *************************************** */
	        im[i].zeropointlink=nzp;
	  	nmatchred++;
	      }
	      else { /* search for match among parentid's */
	        for (j=0;j<nzeropoints;j++)  /* must search full table */
		  if (im[i].parentid==zp[j].imageid) {
		    /* *************************************** */
		    /* *************************************** */
	            /* case of multiple zeropoints not handled */
		    /* *************************************** */
		    /* *************************************** */
		    im[i].zeropointlink=j;
	  	    nmatchremap++;
		    break;
		  }
	      }
	    } /* end of COADD section */
	  }
	  i++;
	}
	pclose(pip); /* end of Image Query Section */
	if (flag_src==SOURCE_COADD) {
	  sprintf(event,"Found zeropoints for %d red and %d remap images",
	    nmatchred,nmatchremap);
	  reportevt(flag_verbose,STATUS,1,event);
	}


	/* ********************************************************* */
	/* ******************** If SOURCE=PSM ********************** */
	/* ***** now insert new ZP's into the zeropoint table ****** */
	/* ********************************************************* */
	if (flag_src==SOURCE_PSM) {
	  out=fopen(sqlfile,"w");
	  if (out==NULL) {
	    sprintf(event,"File open failed: %s",sqlfile);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	  fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	  fprintf(out,"select max(id) from zeropoint;\n");
	  fprintf(out,"exit;\n");
	  fclose(out);
	  pip=popen(sqlcall,"r");
	  if (pip==NULL) {
	    sprintf(event,"Pipe open failed: %s",sqlcall);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  fgets(line,1000,pip);
	  sscanf(line,"%d",&maxid);
	  pclose(pip);
	  if (flag_verbose) printf("  Current max zeropoint id is %d\n",maxid);
	  /* now set up ingestion script */
	  out=fopen(sqlfile,"w");
	  if (out==NULL) {
	    sprintf(event,"File open failed: %s",sqlfile);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	  fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	  for (i=0;i<nimages;i++) {
	      fprintf(out,"INSERT INTO zeropoint (ID,IMAGEID,INSERT_DATE,");
	      fprintf(out,"MAG_ZERO,SIGMA_MAG_ZERO,ORIGINID,SOURCE) \n");
	      fprintf(out,"VALUES ( zeropoint_seq.nextval, %d, sysdate,",
	        zp[i].imageid);
	      fprintf(out," %.6f, %.6f, %d, 'PSM');\n",
	        zp[i].mag_zero,zp[i].sigma_mag_zero,zp[i].originid);
	  }
	  fprintf(out,"exit;");
	  fclose(out);
	  
	  /* ingest zeropoints into zeropoint table */
	  sprintf(event,"Ingesting %d new zeropoints at %s",nimages,asctime(localtime (&curtime)));
	  reportevt(flag_verbose,STATUS,1,event);
	  time(&startTime);
	  system(sqlcall);
	  time(&endTime);
	  sprintf(event,"updating zeropoint table took %d seconds\n", (int) (endTime - startTime));
	  reportevt(flag_verbose,STATUS,1,event);
	  /* and read back the zeropoint ID's */
	  out=fopen(sqlfile,"w");
	  if (out==NULL) {
	    sprintf(event,"File open failed: %s",sqlfile);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	  fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	  fprintf(out,"select id,imageid from zeropoint\n");
	  fprintf(out,"where id>%d;\n",maxid);
	  fprintf(out,"exit;\n");
	  fclose(out);
	  pip=popen(sqlcall,"r");
	  if (pip==NULL) {
	    sprintf(event,"Pipe open failed: %s",sqlcall);
	    reportevt(flag_verbose,STATUS,5, event);
	    exit(0);
	  }
	  i=0;
	  while (fscanf(pip,"%d %d",&(zp[i].id),&imageid)!=EOF) {
	    if (imageid!=zp[i].imageid) {
	      sprintf(event,"ImageID mismatch in zeropoint table: %d %d",
	        zp[i].imageid,imageid);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    i++;  
	    if (i>nimages) {
	      sprintf(event,"Too many zeropoints returned");
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	  }
	  pclose(pip);
	  if (i<nimages) {
	    sprintf(event,"Too few zeropoints returned!  %d vs %d",i,nimages);
	    reportevt(flag_verbose,STATUS,5,event);
	    exit(0);
	  }
	} /* end of PSM zeropoint ingestion section */


	/* ************************************************** */
	/* ************* UPDATE Object Photometry *********** */
	/* ************************************************** */

	nobjects=nupdates=0;
	/* prepare for update query */
	out=fopen(sqlfile,"w");
	if (out==NULL) {
	  sprintf(event,"File open failed: %s",sqlfile);
	  reportevt(flag_verbose,STATUS,5, event);
	  exit(0);
	}
	fprintf(out,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
	fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	for (i=0;i<nimages;i++) {
	  /* update photometry if new zeropoint available */
	  if (im[i].zeropointlink!=-1) { 
	    nupdates++;
	    if (i%10==0 && i>0) {  /* trigger commit */
	      fprintf(out,"commit;\n");
	    }
	    j=im[i].zeropointlink; /* grab link to correct element in zp[] */
	    /* ****************************************************** */
	    /* ****************************************************** */
	    /* now objects_current is always the active objects table */
	    /* ****************************************************** */
	    /* ****************************************************** */
	    /* write update statements for current image */
	    /* ****************************************************** */
	    fprintf(out,"UPDATE OBJECTS_CURRENT\n SET\n");
	    fprintf(out,"  ZEROPOINTID=%d,\n",zp[j].id);
	    fprintf(out,"  ERRZEROPOINT=%.4f,\n",zp[j].sigma_mag_zero);
	    fprintf(out,"  MAG_AUTO=(MAG_AUTO-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_1=(MAG_APER_1-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_2=(MAG_APER_2-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_3=(MAG_APER_3-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_4=(MAG_APER_4-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_5=(MAG_APER_5-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_6=(MAG_APER_6-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_7=(MAG_APER_7-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_8=(MAG_APER_8-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_9=(MAG_APER_9-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_10=(MAG_APER_10-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_11=(MAG_APER_11-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_12=(MAG_APER_12-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_13=(MAG_APER_13-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_14=(MAG_APER_14-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_15=(MAG_APER_15-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_16=(MAG_APER_16-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  MAG_APER_17=(MAG_APER_17-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	     fprintf(out,"  MAG_MODEL=(MAG_MODEL-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	     fprintf(out,"  MAG_DISK=(MAG_DISK-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	     fprintf(out,"  MAG_SPHEROID=(MAG_SPHEROID-(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	     fprintf(out,"  MAG_PSF=(MAG_PSF -(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	     fprintf(out,"  MAG_ISO=(MAG_ISO -(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	     fprintf(out,"  MAG_PETRO=(MAG_PETRO -(ZEROPOINT-(%.4f))),\n",
	      zp[j].mag_zero);
	    fprintf(out,"  ZEROPOINT=%.4f\n",zp[j].mag_zero);
	    fprintf(out,"WHERE IMAGEID=%d and ZEROPOINTID!=%d;\n",
	      im[i].imageid,zp[j].id);
	  }
	}
	fprintf(out,"exit;\n");
	fclose(out);

	/* now make the update call */
 	sprintf(event,"Updating zeropoints in %d out of %d images",
	  nupdates,nimages);
        reportevt(flag_verbose,STATUS,1,event);
	  time(&startTime);
	system(sqlcall);
	time(&endTime);
 	sprintf(event,"Updates of objects_current table complete in %d seconds", (int) (endTime - startTime));
        reportevt(flag_verbose,STATUS,1,event);

	/* free vectors */
	free(im);free(zp);
	if (flag_src==SOURCE_PSM) free(fit);

	exit(0);
}


#undef BAND_ALL
#undef BAND_g
#undef BAND_r
#undef BAND_i
#undef BAND_z
#undef BAND_Y
#undef SOURCE_PSM
#undef SOURCE_COADD
#undef SOURCE_GCM

