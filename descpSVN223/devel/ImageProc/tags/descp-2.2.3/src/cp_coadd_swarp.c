/*
**
** coadd_swarp.c
**
** DESCRIPTION:
**     Input the ID and ZP, calculate the fluxscale and run swarp call
**     
**
** Last commit:
**     $Rev: 6261 $
**     $LastChangedBy: mgower $
**     $LastChangedDate: 2010-12-06 12:59:54 -0600 (Mon, 06 Dec 2010) $
**
*/

/* CAN USE THIS QUERY FOR REMAP ID
select id from image where imagetype='remap' and tilename='SDSS0340+0000' and run=(select run from image where id=5096636) a
nd imagename=(select imagename from image where id=5096636) and ccd=(select ccd from image where id=5096636);
*/

#include "string.h"
#include "imageproc.h"
#include "stdlib.h"

int main(argc,argv)
     int argc;
     char *argv[];
{
  char 	inlist[1000],tilename[50],tilename_copy[50],band[10],outpath[1000],outxmlpath[1000],outauxpath[1000],
	binpath[1000],etcpath[1000],terapixpath[1000],combinetype[20],
	coaddname[1000],coaddweight[1000],outimage[1000], tpfilename[1000],
	imagelist[1000],weightlist[1000],fluxscalelist[1000], inpline[100],
	command[1000],comment[1000],event[2000],checkimage[1000],
	coaddimage[1000],imageidlist[1000],inputimage[1000],tempimage[1000],
    swarpcall[5000],swarpexpmap[5000],fitscombinecall[1000],xmlfile[1000],expmaplist[1000],subbkg[10],
	dbcall[1000],dblogin[1000],sqlcall[1000],sqlfile[1000],sqlHEAD[1000],sqlqueryfile[1000],dosqlquery[1000], PixToRaDecCall[1000],
	**imagename;

  char * tilename_components;
  tangent_point* tp;

  char* result;
  float pixel_from_tilename,nside_from_tilename;


  int i,j,N,Ntp,status=0,hdunum,*id,hdu_im,hdu_wt,hdu_bpm,hdu_exp,nthread,
      flag_verbose=2,flag_input=0,flag_tilename=0,flag_outauxpath=0,
      flag_checkimage,flag_binpath=0,flag_etcpath=0,flag_terapixpath=0,
    flag_band=0,flag_outpath=0,flag_resample=0,flag_nthread=0,flag_badpixels=0,
    npix_ra,npix_dec,flag_xmlpath=0,flag_dosqlquery=1,flag_sqlqueryfile=0, tilename_component_one,tilename_component_two,tilename_components_iterator,flag_subbkg=0,
      mkpath(),cp_check_remap_image();

  float *zp,*zperr,fluxscale,pixscale,mag_base;
  double tile_ra,tile_dec;
  double ZPLO=27.0,ZPHI=34;

  fitsfile *fptr;
  void select_dblogin(),reportevt(),get_input(),printerror();
  FILE *pip,*fin,*fimage,*fweight,*fexpmap,*fsqlout,*fimageid,*ffluxscale,*fcheck,*fsqlquery;
  setbuf(stdout, 0);

  /* *** Set default *** */
  sprintf(combinetype,"WEIGHTED");
  flag_resample=0;

  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <optional inputs>\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -input <list (format: IMAGE ID ZP ZPerr>  \n");
    printf("       -band <band>  \n");
    printf("       -tilename <tilename>  \n");
    printf("       -outpath <outpath>  \n");
    printf("       -outauxpath <outauxpath>  \n");
    printf("       -outxmlpath <outxmlpath> \n");
    printf("    Optional Inputs:\n");
    printf("       -terapixpath <full terapixpath>\n");
    printf("       -binpath <full binpath>\n");
    printf("       -etcpath <full etcpath>\n");
    printf("       -combinetype <median,average,min,max,weighted,chi2,sum; default is weighted> \n");    
    printf("       -resample\n");
    printf("       -nthread <0-8>\n");
    printf("       -verbose <0-3>\n");
    printf("       -zp_hi   <34>\n");
    printf("       -zp_lo   <27>\n");
    printf("       -subbkg  <Y/N>\n");
    printf("\n");
    exit(0);
  }
  
  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-input")) {
      flag_input=1;
      get_input(argv[i],argv[i+1],"-input",inlist,flag_verbose);
    }

    if (!strcmp(argv[i],"-band")) {
      flag_band=1;
      get_input(argv[i],argv[i+1],"-band",band,flag_verbose);
    }
    if (!strcmp(argv[i],"-outxmlpath")) {
      flag_xmlpath=1;
      get_input(argv[i],argv[i+1],"-outxmlpath",outxmlpath,flag_verbose);
    }


    if (!strcmp(argv[i],"-tilename")) {
      flag_tilename=1;
      get_input(argv[i],argv[i+1],"-tilename",tilename,flag_verbose);
    }

	// this snippet is only temporary to have the code working. 
/**    if (!strcmp(argv[i],"-tilename")) {
      get_input(argv[i],argv[i+1],"-tilename",tilename_copy,flag_verbose);
    }
**/

    if (!strcmp(argv[i],"-outpath")) {
      flag_outpath=1;
      get_input(argv[i],argv[i+1],"-outpath",outpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-outauxpath")) {
      flag_outauxpath=1;
      get_input(argv[i],argv[i+1],"-outauxpath",outauxpath,flag_verbose);
    }

    /* options */
    if (!strcmp(argv[i],"-terapixpath")) {
      flag_terapixpath=1;
      get_input(argv[i],argv[i+1],"-terapixpath",terapixpath,flag_verbose);
    }
 
    if (!strcmp(argv[i],"-binpath")) {
      flag_binpath=1;
      get_input(argv[i],argv[i+1],"-binpath",binpath,flag_verbose);
    }
    
    if (!strcmp(argv[i],"-etcpath")) {
      flag_etcpath=1;
      get_input(argv[i],argv[i+1],"-etcpath",etcpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-resample")) 
      flag_resample=1;
    if (!strcmp(argv[i],"-usebadpixels"))
      flag_badpixels=1;

    if (!strcmp(argv[i],"-nthread")) {
      flag_nthread=1;
      nthread=atoi(argv[i+1]);
      if(nthread > 8 || nthread < 0) {
	sprintf(event," nthread out of range (0-8), reset to 0 ");
	reportevt(flag_verbose,STATUS,3,event);
	nthread=0;
      }

       }

    if (!strcmp(argv[i],"-combinetype")) {
      get_input(argv[i],argv[i+1],"-combinetype",combinetype,flag_verbose);
    
      if (!strcmp(argv[i+1],"median")) sprintf(combinetype,"MEDIAN");
      else if (!strcmp(argv[i+1],"average")) sprintf(combinetype,"AVERAGE");
      else if (!strcmp(argv[i+1],"min")) sprintf(combinetype,"MIN");
      else if (!strcmp(argv[i+1],"max")) sprintf(combinetype,"MAX");
      else if (!strcmp(argv[i+1],"weighted")) sprintf(combinetype,"WEIGHTED");
      else if (!strcmp(argv[i+1],"chi2")) sprintf(combinetype,"CHI2");
      else if (!strcmp(argv[i+1],"sum")) sprintf(combinetype,"SUM");
      else {
	sprintf(event," Wrong input for <combinetype>, reset to default ");
	reportevt(flag_verbose,STATUS,3,event);
	sprintf(combinetype,"WEIGHTED");
      }
    }

    if (!strcmp(argv[i],"-zp_lo")) {
      ZPLO=atof(argv[i+1]);
    }
    if (!strcmp(argv[i],"-zp_hi")) {
      ZPHI=atof(argv[i+1]);
    }
    if (!strcmp(argv[i],"-subbkg")) {
      flag_subbkg=1;
      get_input(argv[i],argv[i+1],"-subbkg",subbkg,flag_verbose);
    }



  }

  /* quit if required inputs are not set */
  if(!flag_input || !flag_tilename || !flag_band || !flag_outpath || !flag_outauxpath || !flag_xmlpath) {
    sprintf(event,"Required inputs are not set ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  
  /*       */
  /* *** Find out the number of lines from tangent points file *** *
  sprintf(command,"wc -l %s",tpfilename);
  pip=popen(command,"r");
  if (pip==NULL) {
    sprintf(event,"Pipe failed: %s ",command);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  fscanf(pip,"%d",&Ntp);
  pclose(pip);
**
  sprintf(event,"Number of lines in %s = %d",tpfilename,Ntp);
  reportevt(flag_verbose,STATUS,1,event);
  tp=(tangent_point *)calloc(Ntp+1,sizeof(tangent_point ));
  if (tp==NULL) {
     reportevt(flag_verbose,STATUS,5,"Calloc of tangent points failed");
     exit(0);
   }
**/
  /* *** Setup DB login and make generic sqlcall *** */
//  sprintf(sqlfile,"coadd_swarp_%s_%s.sql",tilename,band);
//  select_dblogin(dblogin,DB_READWRITE);
//  sprintf(sqlcall, "sqlplus -S %s < %s ",dblogin,sqlfile);
//  sprintf(sqlHEAD,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;");

  /* *** Find out the number of lines from input file *** */
  sprintf(command,"wc -l %s",inlist);
  pip=popen(command,"r");
  if (pip==NULL) {
    sprintf(event,"Pipe failed: %s ",command);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  fscanf(pip,"%d",&N);
  pclose(pip);

  sprintf(event,"Number of images in %s = %d",inlist,N);
  reportevt(flag_verbose,STATUS,1,event);

  /* memory allocation */
  id=(int *)calloc(N+1,sizeof(int ));
  if (id==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of id failed");
    exit(0);
  }
  zp=(float *)calloc(N+1,sizeof(float ));
  if (zp==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of zp failed");
    exit(0);
  }
  zperr=(float *)calloc(N+1,sizeof(float ));
  if (zperr==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of zperr failed");
    exit(0);
  }
  imagename=(char **)calloc(N+1,sizeof(char *));
  if (imagename==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of imagename failed");
    exit(0);
  }
  for(i=1;i<=N;i++)
    imagename[i]=(char *)calloc(1000,sizeof(char ));

  /* *** Input data  *** */
  fin=fopen(inlist,"r");
  if (fin==NULL) {
    sprintf(event,"File %s not found",inlist);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }

  for(i=1;i<=N;i++) {
    fscanf(fin,"%s %d %f %f",inputimage,&id[i],&zp[i],&zperr[i]);

    /* check the input list */
    flag_checkimage=0;
    if (!strncmp(&(inputimage[strlen(inputimage)-5]),".fits",5)) {
      flag_checkimage=1;
    } 
    /* strip off the .fz */
    else if(!strncmp(&(inputimage[strlen(inputimage)-8]),".fits.fz",8)) {
      flag_checkimage=1;
      inputimage[strlen(inputimage)-3]=0;
    }
    /* strip off the .gz */
    else if(!strncmp(&(inputimage[strlen(inputimage)-8]),".fits.gz",8)) {
      flag_checkimage=1;
      inputimage[strlen(inputimage)-3]=0;
    }
          
    if(!flag_checkimage) {
      sprintf(event,"Input file %s must contain only FITS images",inlist);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }
    else
      sprintf(imagename[i],"%s",inputimage);
  }
  fclose(fin);

  /* *** Assign the mag_base *** */
  for(i=1;i<=N;i++) {
    if(zp[i]>ZPLO && zp[i]<ZPHI) {
      mag_base=zp[i];
      break;
    }
  }

  sprintf(event," mag_base = %2.3f ",mag_base);
  reportevt(flag_verbose,STATUS,1,event);


  /* *** Setup the image.list and output.list *** */
  sprintf(imagelist,"%s/%s_%s_image.list",outauxpath,tilename,band);
  sprintf(weightlist,"%s/%s_%s_weight.list",outauxpath,tilename,band);
  sprintf(expmaplist,"%s/%s_%s_expmap.list",outauxpath,tilename,band);
  sprintf(fluxscalelist,"%s/%s_%s_fluxscale.list",outauxpath,tilename,band);
  sprintf(imageidlist,"%s/%s_%s_imageid.dat",outauxpath,tilename,band);
  sprintf (xmlfile,"%s/%s_%s_swarp.xml",outxmlpath,tilename,band);

  fimage=fopen(imagelist,"w");
  if (fimage==NULL) {
    sprintf(event,"File %s open failed",imagelist);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  fweight=fopen(weightlist,"w");
  if (fweight==NULL) {
    sprintf(event,"File %s open failed",weightlist);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
	
  fexpmap=fopen(expmaplist,"w");
  if (fexpmap==NULL) {
	 sprintf(event,"File %s open failed",expmaplist);
	 reportevt(flag_verbose,STATUS,5,event);
	 exit(0);
  }
	
  ffluxscale=fopen(fluxscalelist,"w");
  if (ffluxscale==NULL) {
    sprintf(event,"File %s open failed",fluxscalelist);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  fimageid=fopen(imageidlist,"w");
  if (fimageid==NULL) {
    sprintf(event,"File %s open failed",imageidlist);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  
  /* *** Loop over the images and calcuate the fluxscale *** */
  for(i=1;i<=N;i++) {
    
    /* ************************************************** */
    /* ********** uncompress image if needed ************ */
    /* ************************************************** */
    sprintf(checkimage,"%s",imagename[i]);
    fcheck=fopen(checkimage,"r");
    if (fcheck==NULL) { /* check for fpacked flavor...*/
      sprintf(checkimage,"%s.fz",imagename[i]);
      fcheck=fopen(checkimage,"r");
      if (fcheck==NULL) { /* check for gzipped flavor...*/
        sprintf(checkimage,"%s.gz",imagename[i]);
        fcheck=fopen(checkimage,"r");
        if (fcheck==NULL) { /* no flavor exists*/
	  sprintf(event,"File %s not found in any flavor",imagename[i]);
          reportevt(flag_verbose,STATUS,5,event);
          exit(0);
        }
	else fclose(fcheck); /* flavor G exists */
        sprintf(command,"gunzip %s.gz",imagename[i]);
        sprintf(event,"Gunzip'ing  %s.gz",imagename[i]);
        reportevt(flag_verbose,STATUS,3,event);
        system(command);
      }
      else fclose(fcheck); /* flavor F exists */
      sprintf(command,"%s/funpack %s.fz",terapixpath,imagename[i]);
      sprintf(event,"Funpack'ing  %s.fz",imagename[i]);
      reportevt(flag_verbose,STATUS,3,event);
      system(command);
    }
    else fclose(fcheck); /* uncompressed flavor exists */

    /* calculate fluxscale and report it */
    if(zp[i]<ZPLO || zp[i]>ZPHI) { /* crude filter on zeropoint */
      sprintf(event,"Image discarded (ZP = %2.4f): %s",zp[i],imagename[i]);
      reportevt(flag_verbose,STATUS,4,event);
    }
    else {
      	if (cp_check_remap_image(flag_verbose,imagename[i],0.27, 
      	  &hdu_im,&hdu_wt,&hdu_bpm,&hdu_exp)) {                
      	  sprintf(event,"Image discarded: %s",imagename[i]);  
      	  reportevt(flag_verbose,STATUS,4,event);             
      	}                                                      
      	else {                                                
	  /* ************************************************** */
	  /* *********** remap image passed all tests ********* */
	  /* **** add it to the input lists for SWarp call **** */
	  /* ************************************************** */
          fluxscale=pow(10.0,0.4*(mag_base-zp[i]));
	  sprintf(event,"Name = %s & Fluxscale = %6.4f & Mag_zero = %.4f",
	    imagename[i],fluxscale,zp[i]);
	  reportevt(flag_verbose,QA,1,event);
	  fprintf(fimage,"%s[%d]\n",imagename[i],hdu_im);
	  fprintf(fweight,"%s[%d]\n",imagename[i],hdu_wt);
	  fprintf(fexpmap,"%s[%d]\n",imagename[i],hdu_exp);
	  fprintf(ffluxscale,"%2.6f\n",fluxscale);
	  fprintf(fimageid,"%d\t%2.4f\t%2.4f\n",id[i],zp[i],zperr[i]);
        } 
      }
  }
	fclose(fimage); fclose(fweight); fclose(fexpmap); fclose(ffluxscale); fclose(fimageid);
    /**
    make a copy of the tilename to split on, so that we get the two numbers to feed to 
    **/
    strcpy(tilename_copy,tilename);
//    printf("check the tilename %s \n",tilename_copy);
    result = strtok( tilename_copy, "_" );
    result = strtok( NULL, "_" );
    sscanf( result, "%f", &pixel_from_tilename);
 //   printf("part 2%s\n",result);
    result = strtok( NULL, "_" );
  //  printf("part 3%s\n",result);
    sscanf( result, "%f", &nside_from_tilename);
    //printf("%f %f\n",pixel_from_tilename,nside_from_tilename);
    sprintf(PixToRaDecCall,"PixToRADec %f %f",pixel_from_tilename,nside_from_tilename);
//    printf(" the pixtoradec call %s\n",PixToRaDecCall);

    //system(const char *command)
    pip=popen(PixToRaDecCall,"r");
    if (pip==NULL) {
      sprintf(event,"Pipe failed: %s ",command);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }
    fscanf(pip,"%lf,%lf",&tile_ra,&tile_dec);
    pclose(pip);


    /**
        this is the place where we need to write code for parsing the tilename and getting the two numbers to feed to pixtora. that will give us the ra and dec numbers, which we can then feed to tile_ra and tile_dec. use the code in PixToRaDec and use the code in the library.
    **/

	/*  Find coadd parameters from tangent points list  

	for ( i=0; i< Ntp; i++) {
	      if( !strcmp((const char *)tp[i].pointname, (const char *)tilename)) {
	        sprintf(event,"tangent point %d %s %f %f",i, tp[i].pointname,tp[i].ra,tp[i].dec);
                         RAG Aug03,2012: Altered from STATUS5 to STATUS1 since this indicates information not failure 
			reportevt(flag_verbose,STATUS,1,event);
			tile_ra = tp[i].ra;
			tile_dec = tp[i].dec;
			npix_ra = 10000;
			npix_dec = 10000;
			pixscale = 0.27;
			break;
		}
	 } */

    npix_ra = 10000;
    npix_dec = 10000;
    pixscale = 0.27;

  printf("\nTileinfo: %lg %lg %d %d %f\n\n", tile_ra,tile_dec,npix_ra,npix_dec,pixscale);

  /* *** Construct swarp call *** */
  
  if(flag_terapixpath) sprintf(swarpcall,"%s/swarp ",terapixpath);
  else sprintf(swarpcall,"swarp ");
  
  sprintf(swarpcall,"%s @%s ",swarpcall,imagelist);
  
  if(flag_etcpath) sprintf(swarpcall,"%s -c %s/default.swarp ",swarpcall,etcpath);
  else sprintf(swarpcall,"%s -c default.swarp ",swarpcall);
  
  sprintf(swarpcall,"%s  ",swarpcall);
  /* RAG Aug06,2012: uncommented next two lines to allow coadd/stack to inherit the proper reference frame */
  sprintf(swarpcall,"%s -PIXELSCALE_TYPE MANUAL -PIXEL_SCALE %2.5f ",swarpcall,pixscale);
  sprintf(swarpcall,"%s -CENTER_TYPE ALL -CENTER %3.7f,%3.7f ",swarpcall,tile_ra,tile_dec);
////  sprintf(swarpcall,"%s -IMAGE_SIZE %d,%d ",swarpcall,npix_ra,npix_dec);
  sprintf(swarpcall,"%s -IMAGE_SIZE 0",swarpcall);
  if (flag_subbkg)
    sprintf(swarpcall,"%s -SUBTRACT_BACK %s ",swarpcall,subbkg);
  else
    sprintf(swarpcall,"%s -SUBTRACT_BACK Y ",swarpcall);
  if (!flag_badpixels)
  sprintf(swarpcall,"%s -BLANK_BADPIXELS N ",swarpcall);
  else
  sprintf(swarpcall,"%s -BLANK_BADPIXELS Y ",swarpcall);
  sprintf(swarpcall,"%s -DELETE_TMPFILES Y ",swarpcall);
  sprintf(swarpcall,"%s -FSCALASTRO_TYPE VARIABLE ",swarpcall);
  /* take out until swarp is fixed */
  sprintf(swarpcall,"%s -FSCALE_DEFAULT @%s ",swarpcall,fluxscalelist);
  sprintf(swarpcall,"%s -FSCALE_KEYWORD nokey ",swarpcall);
  if(!flag_resample)
    sprintf(swarpcall,"%s -RESAMPLE N ",swarpcall);
  else
    sprintf(swarpcall,"%s -RESAMPLE Y ",swarpcall);

  sprintf(swarpcall,"%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE @%s ",swarpcall,weightlist);
  sprintf(swarpcall,"%s -COMBINE Y ",swarpcall);
  sprintf(swarpcall,"%s -COMBINE_TYPE %s ",swarpcall,combinetype);
  sprintf(swarpcall,"%s -IMAGEOUT_NAME %s_%s.image.fits ",swarpcall,tilename,band);
  sprintf(swarpcall,"%s -WEIGHTOUT_NAME %s_%s.weight.fits ",swarpcall,tilename,band);
  sprintf(swarpcall,"%s -HEADER_ONLY N ",swarpcall);
  sprintf(swarpcall,"%s -WRITE_XML Y -XML_NAME %s",swarpcall,xmlfile);
  if(flag_nthread)
    sprintf(swarpcall,"%s -NTHREADS %d ",swarpcall,nthread);
  if(flag_verbose==0)
    sprintf(swarpcall,"%s -VERBOSE_TYPE QUIET ",swarpcall);
  sprintf(swarpcall, "%s\n",swarpcall);
  /* RAG Aug03,2012: Altered from STATUS5 to STATUS1 since this indicates information not failure */
  /* run the shell scripts */
  sprintf(event," SWARP Call: %s ",swarpcall);
  reportevt(flag_verbose,STATUS,1,event);
  system(swarpcall);
	
  /*Exposure map swarp */
	if(flag_terapixpath) sprintf(swarpexpmap,"%s/swarp ",terapixpath);
	else sprintf(swarpexpmap,"swarp ");
	
	sprintf(swarpexpmap,"%s @%s ",swarpexpmap,expmaplist);
	
	if(flag_etcpath) sprintf(swarpexpmap,"%s -c %s/default.swarp ",swarpexpmap,etcpath);
	else sprintf(swarpexpmap,"%s -c default.swarp ",swarpexpmap);
		
    sprintf(swarpexpmap,"%s  ",swarpexpmap);
        /* RAG Aug06,2012: uncommented next two lines to allow coadd/stack to inherit the proper reference frame */
	sprintf(swarpexpmap,"%s -PIXELSCALE_TYPE MANUAL -PIXEL_SCALE %2.5f ",swarpexpmap,pixscale);
	sprintf(swarpexpmap,"%s -CENTER_TYPE ALL -CENTER %3.7f,%3.7f ",swarpexpmap,tile_ra,tile_dec);
//	sprintf(swarpexpmap,"%s -IMAGE_SIZE %d,%d ",swarpexpmap,npix_ra,npix_dec);
	sprintf(swarpexpmap,"%s -IMAGE_SIZE 0 ",swarpexpmap);
	sprintf(swarpexpmap,"%s -SUBTRACT_BACK N ",swarpexpmap);
	sprintf(swarpexpmap,"%s -BLANK_BADPIXELS N ",swarpexpmap);
	sprintf(swarpexpmap,"%s -DELETE_TMPFILES Y ",swarpexpmap);
	sprintf(swarpexpmap,"%s -FSCALASTRO_TYPE NONE ",swarpexpmap);
	/* take out until swarp is fixed.*/
	/* ANKIT: for SWARP version 2.33.0: we have changed the following line from "RESAMPLE N" to "RESAMPLE Y"*/
	sprintf(swarpexpmap,"%s -RESAMPLE Y ",swarpexpmap);	
	sprintf(swarpexpmap,"%s -WEIGHT_TYPE NONE",swarpexpmap);
	sprintf(swarpexpmap,"%s -COMBINE Y ",swarpexpmap);
	sprintf(swarpexpmap,"%s -COMBINE_TYPE  SUM",swarpexpmap);
	sprintf(swarpexpmap,"%s -IMAGEOUT_NAME %s_%s.expmap.fits ",swarpexpmap,tilename,band);
	sprintf(swarpexpmap,"%s -WEIGHTOUT_NAME %s_%s.expweight.fits ",swarpexpmap,tilename,band);
	sprintf(swarpexpmap,"%s -HEADER_ONLY N ",swarpexpmap);
	sprintf(swarpexpmap,"%s -WRITE_XML N",swarpexpmap);
	if(flag_nthread)
		sprintf(swarpexpmap,"%s -NTHREADS %d ",swarpexpmap,nthread);
	if(flag_verbose==0)
		sprintf(swarpexpmap,"%s -VERBOSE_TYPE QUIET ",swarpexpmap);
	sprintf(swarpexpmap, "%s\n",swarpexpmap);
	/* run the shell scripts */
	sprintf(event," SWARP Call: %s ",swarpexpmap);
	reportevt(flag_verbose,STATUS,1,event);
	system(swarpexpmap);
	
	
  /* insert the mag_base to image header */
  sprintf(coaddimage,"%s_%s.image.fits",tilename,band);
  status=0;
  if(!fits_open_file(&fptr,coaddimage,READWRITE,&status)) {
    if(fits_update_key(fptr,TFLOAT,"SEXMGZPT",&mag_base,"Mag ZP",&status)) {
	sprintf(event,"Inserting SEXMGZPT failed: %s",coaddimage);
	reportevt(flag_verbose,STATUS,4,event);
	status=0;
    }
    if(fits_update_key(fptr,TSTRING,"FILTER",band,"Filter",&status)) {
	sprintf(event,"Inserting FILTER failed: %s",coaddimage);
	reportevt(flag_verbose,STATUS,4,event);
	status=0;
    }
    if(fits_close_file(fptr,&status)) {
	sprintf(event,"File close failed: %s",coaddimage);
	reportevt(flag_verbose,STATUS,5,event);
	status=0;
    }

    /* make the output image path */
    sprintf(outimage,"%s/%s_%s.fits",outpath,tilename,band);

    if (mkpath(outimage,flag_verbose)) {
      sprintf(event,"Failed to create path to file: %s",outimage);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }
    else {
      sprintf(event,"Created path to file %s",outimage);
      reportevt(flag_verbose,STATUS,1,event);
	}    
    /* combine the coadd image, weight maps, exposure maps */
    if(flag_binpath) 
      sprintf(fitscombinecall,"%s/fitscombine %s_%s.image.fits %s_%s.weight.fits %s_%s.expmap.fits %s -cleanup -verbose %d",
	      binpath,tilename,band,tilename,band,tilename,band,outimage,flag_verbose);
    else
      sprintf(fitscombinecall,"fitscombine %s_%s.image.fits %s_%s.weight.fits %s_%s.expmap.fits %s -cleanup -verbose %d",
	      tilename,band,tilename,band,tilename,band,outimage,flag_verbose);
      sprintf(event,"Fitscombine Call: %s ",fitscombinecall);
    reportevt(flag_verbose,STATUS,1,event);
    system(fitscombinecall);
  } 
  else { /* report that swarp coadd creation failed */
	sprintf(event,"Coadd creation failed: %s",coaddimage);
	reportevt(flag_verbose,STATUS,5,event);
	status=0;
  }

  /* remove the uncompressed versions of each image if */
  /* the compressed versions exist 			*/
  for(i=1;i<=N;i++) {
    sprintf(checkimage,"%s.fz",imagename[i]);
    fcheck=fopen(checkimage,"r");
    if (fcheck!=NULL) { /* fpacked version exists...*/
      fclose(fcheck);
      fcheck=fopen(imagename[i],"r");
      if (fcheck!=NULL) { /* uncompressed version exists, too */
	fclose(fcheck);
	sprintf(command,"rm -f %s",imagename[i]);
	system(command);
	sprintf(event,"Removing uncompressed file %s",imagename[i]);
        /* RAG Aug03,2012: Altered from STATUS5 to STATUS1 since this indicates information not failure */
	reportevt(flag_verbose,STATUS,1,event);
      }
    }
    else {
      sprintf(checkimage,"%s.gz",imagename[i]);
      fcheck=fopen(checkimage,"r");
      if (fcheck!=NULL) { /* gzipped version exists... */
        fclose(fcheck);
        fcheck=fopen(imagename[i],"r");
        if (fcheck!=NULL) { /* uncompressed version exists, too */
	  fclose(fcheck);
	  sprintf(command,"rm -f %s",imagename[i]);
	  system(command);
	  sprintf(event,"Removing uncompressed file %s",imagename[i]);
          /* RAG Aug03,2012: Altered from STATUS5 to STATUS1 since this indicates information not failure */
	  reportevt(flag_verbose,STATUS,1,event);
        }
      }
    }
  }

  /* free memory */
  free(id); free(zp); free(zperr); free(imagename); 
  //free(tp);
  return (0);
}

#undef ZPLO 
#undef ZPHI 
