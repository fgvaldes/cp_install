#include <fstream>
#include "TRandom3.h"
#include "TPaveStats.h"
#include "TMVA/Timer.h"
#include "SysErrFit.h"
#include "TF1.h"
#include "TGaxis.h"
#include "TVirtualFitter.h"
#include "TMatrixD.h"
#include "Object.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TCut.h"
#include "FitSLR.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TMath.h"
#include <string>
#include <iostream>
#include "TDirectory.h"
#include "TEventList.h"
#include "TFile.h"
#include "TH2D.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TGraph.h"
#include "TLine.h"
#include "TLatex.h"
#include "TDirectory.h"
#include "TSQLServer.h"
#include "TSQLStatement.h"
#include "TOracleStatement.h"
#include "ExtinctionMap.h"
#include "kdtree++/kdtree.hpp"
using namespace std;
using TMVA::Timer;

struct ObjPair{

  Object obj1;
  Object obj2;
  double zp_diff;
  double zp1;
  double zp2;
  int id1;
  int id2;
};

void LoadStyle();
void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol);

void plot_phot(const vector<ObjPair> &obj_pairs,
               string title="",string dir=".",
	       string which_band="",string which_mag="",
               string x_label="",string y_label="",
               bool debug=false,bool rm_zpdiff=false,
	       double sigma=3,int N_bin=1000);

int plot_phot_all(const vector<ObjPair> &obj_pairs,
                  TPad *can,int &cur_can,
                  string title,string dir,
                  string which_band,
                  bool debug,
                  string true_label,
                  string mag_label,
                  double &mean,double &sigma);
                  


int plot_astro(const vector<ObjPair> &obj_pairs,
               TPad *can,int &cur_can,
               string title,string dir,
	       string which_band,
               bool debug,
               double &mean_ra,double &sigmal_ra,double &sigmar_ra,
               double &mean_dec,double &sigmal_dec, double &sigmar_dec);
               
void Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters = " ");
void PrintHelp();
typedef KDTree::KDTree<3,Object> KD_Tree;
typedef std::pair<KD_Tree::const_iterator,double> KD_Match;




class ImagePair{
public:
  ImagePair(int _id1,int _id2):id1(_id1),id2(_id2) {}
  
  bool operator==(const ImagePair& other) const {
    if( (id1==other.id1 && id2==other.id2) ||
        (id1==other.id2 && id2==other.id1)) return true;
    else return false;
  }
        
  int id1;
  int id2;
  double mean;
  double rms;
  vector<ObjPair> obj_pairs;
};




map<int,ImageInfo> ImageList;
int main(int argc, char*argv[])
{
  LoadStyle();

//   gStyle->SetLabelSize(0.06,"xy");
//   gStyle->SetTitleSize(0.07,"xy");
//   gStyle->SetTitleOffset(0.87,"xy");
//   gStyle->SetTitleOffset(0.87,"x");
//   gStyle->SetTitleOffset(0.94,"y");
    

    char**argv_dummy;
    char char_dummy='p';
    char *char_dummy2=&char_dummy;
    argv_dummy=&char_dummy2;

    int int_dummy=1;
    //TApplication app("app",&int_dummy,argv_dummy);
    double zp_max=28;
  string project="BCS";
  string tile="";
  string sdss_datafile("/home/rearmstr/slr_data/covey_extinction.root");
  string mag="auto";
  bool force_mfp=false;
  bool mfp=false;
  string true_mag="modelmag";
  bool restrict_ccd=false;
  string run="";
  bool rm_zpdiff=true;
  string nite="";
  string which_band="all";
  string title="";
  string dir="./";
  double sigma=3;
  int N_bin=300;
  double star_cut_min=0;
  double star_cut_max=0.1;
  double mag_max=100;
  double mag_min=0;
  double err_cut=1;
  int cut_points=0;
  int min_pair=10;
  double cut_range=-1;
  bool extinction=false;
  bool applycolor=false;
  bool print=true;
  bool remove_outliers=false;
  bool debug=false;
  string outfile="out.root";
  double tol=2;
  string loaddata="";
  bool UseClip=true;
  bool UseWeighted=false;
  double ClipTol=1e-6;
  double Thresh=2.5;
  int MaxIter=3;
  bool UseColor=true;
  bool usnob=false;\
  bool psf;
  bool use_all_remaps=false;
  bool standard_stars=false;
  bool standard_gals=false;
  bool IsPhotometric=false;
  bool plot_pairs=false;
  int plot_pairs_min=100;
  bool force_band=false;
  bool cal=false;
  string zpfile="";
  string exp;
  string ccd_string;
  bool diff_nite=false;
  bool same_nite=false;
  bool same_ccd=false;
  double corr=-1;
  string suffix="";
  string which_db="fm";
  string star_sel="spread";
  bool slr_cal=false;
  int max_flag=3;
  if(argc==1) {
    PrintHelp();
    exit(1);
  }
    
  for(int ii=1;ii<argc;ii++) {
    string arg=argv[ii];
    if(arg=="-tile")            {tile=argv[ii+1];ii++;}
    else if(arg=="-run")        {run=argv[ii+1];ii++;}
    else if(arg=="-project")    {project=argv[ii+1];ii++;}
    else if(arg=="-nite")       {nite=argv[ii+1];ii++;}
    else if(arg=="-band")       {which_band=argv[ii+1];ii++;}
    else if(arg=="-loaddata")   {loaddata=argv[ii+1];ii++;}
    else if(arg=="-title")      {title=argv[ii+1];ii++;}
    else if(arg=="-star_sel")      {star_sel=argv[ii+1];ii++;}
    else if(arg=="-dir")        {dir=argv[ii+1];ii++;}
    else if(arg=="-zp_max")        {zp_max=atof(argv[ii+1]);ii++;}
    else if(arg=="-exp")        {exp=argv[ii+1];ii++;}
    else if(arg=="-ccd")        {ccd_string=argv[ii+1];ii++;}
    else if(arg=="-suffix")        {suffix=argv[ii+1];ii++;}
    else if(arg=="-restrict_ccd")  {restrict_ccd=true;}
    else if(arg=="-no_zpdiff")  {rm_zpdiff=false;}
    else if(arg=="-same_nite")  {same_nite=true;}
    else if(arg=="-same_ccd")  {same_ccd=true;}
    else if(arg=="-slr_cal")  {slr_cal=true;}
    else if(arg=="-diff_nite")  {diff_nite=true;}
    else if(arg=="-plot_pairs")  {plot_pairs=true;}
    else if(arg=="-extinction") {extinction=true;}
    else if(arg=="-debug")      {debug=true;}
    else if(arg=="-cal")      {cal=true;}
    else if(arg=="-print")      {print=true;}
    else if(arg=="-color")      {applycolor=true;}
    else if(arg=="-mfp")      {mfp=true;}
    else if(arg=="-force_mfp")      {force_mfp=true;}
    else if(arg=="-db")    {which_db=argv[ii+1];ii++;}
    else if(arg=="-use_all_remaps")      {use_all_remaps=true;}
    else if(arg=="-force_band")      {force_band=true;}
    else if(arg=="-sdss_file")  {sdss_datafile=argv[ii+1];ii++;}
    else if(arg=="-zp_file")  {zpfile=argv[ii+1];ii++;}
    else if(arg=="-mag")        {mag=argv[ii+1];ii++;}
    else if(arg=="-true_mag")        {true_mag=argv[ii+1];ii++;}
    else if(arg=="-star_cut")   {
      star_cut_min=atof(argv[ii+1]);
      star_cut_max=atof(argv[ii+2]);
      ii+=2;
    }
     else if(arg=="-mag_cut")   {
      mag_min=atof(argv[ii+1]);
      mag_max=atof(argv[ii+2]);
      ii+=2;
    }
    else if(arg=="-tol")        {tol=atof(argv[ii+1]);ii++;}
    else if(arg=="-sigma")      {sigma=atof(argv[ii+1]);ii++;}
    else if(arg=="-plot_pairs_min") {plot_pairs_min=atoi(argv[ii+1]);ii++;}
    else if(arg=="-max_flag") {max_flag=atoi(argv[ii+1]);ii++;}
    else if(arg=="-N_bin")      {N_bin=atoi(argv[ii+1]);ii++;}
    else if(arg=="-err_cut")    {err_cut=atof(argv[ii+1]);ii++;}
    else if(arg=="-corr")    {corr=atof(argv[ii+1]);ii++;}
    else if(arg=="-outfile")    {outfile=argv[ii+1];ii++;}
    else if(arg=="-cut_range")  {cut_range=atof(argv[ii+1]);ii++;}
    else if(arg=="-cut_points")    {cut_points=atoi(argv[ii+1]);ii++;}
    else if(arg=="-remove_outliers"){remove_outliers=true;}
    else {
      cerr<<"Not a valid command line entry: "<<arg<<endl;
      exit(1);
    }
  }

  // Do appropriate command line checks
  ExtinctionMap ExtMap("/home/zenteno/REDSHIFT_SURVEYS/EXTINCTION/MAPS_FITS/");
  tol*=1./3600*TMath::DegToRad();

  TString db_string,db_user,db_passwd;  
  if(which_db=="fm") {
    db_string="oracle://decam-devdb.fnal.gov/fmdes";
  }
  else if(which_db=="pr") {
    db_string="oracle://desdb/prdes";
  }
  else if(which_db=="st") {
    db_string="oracle://desdb/stdes";
  }
  
  
  if(star_sel!="spread" && star_sel !="class") {
    cout<<"Not a valid star selector: "<<star_sel<<endl;
    cout<<"Must be either 'spread' or 'class'"<<endl;
    exit(1);
  }
  
  TSQLServer *db = TSQLServer::Connect(db_string,
                                       "pipeline", "dc01user");
  string db_cmd;
  TSQLStatement *stmt;


  vector<string> match_exp;
  vector<string> match_ccd;
    if(!exp.empty()) {
      Tokenize(exp,match_exp,",");
    }
    if(!ccd_string.empty()) {
      Tokenize(ccd_string,match_ccd,",");
    }
  

  if(!tile.empty() && !nite.empty()) {
    cout<<"Can only specify a tile or nite, but not both"<<endl;
    exit(1);
  }

  if(nite.empty())  title=tile;
  else title=nite;


  if(run.empty() && !nite.empty()) {
    // Get the most recent run
    db = TSQLServer::Connect(db_string,
                             "pipeline", "dc01user");
    // get the most recent run
    db_cmd=Form("select distinct(run) from location where "
                "nite='%s' and project='%s' order by run desc",
                nite.c_str(),project.c_str());
    if(debug) cout<<db_cmd<<endl;
    
    stmt=db->Statement(db_cmd.c_str());
    
    stmt->Process();
    stmt->StoreResult();

    while(stmt->NextResultRow()) {
      run=stmt->GetString(0);
      break;
    }
    db->Close();
    
    
  } else if (run.empty() && !tile.empty()) {

     db = TSQLServer::Connect(db_string,
                              "pipeline", "dc01user");
     // get the most recent run
     db_cmd=Form("select distinct(run) from coadd where "
                "tilename='%s' "
                 "and project='%s' order by run desc",
                tile.c_str(),project.c_str());
    if(debug) cout<<db_cmd<<endl;
    
    stmt=db->Statement(db_cmd.c_str());
    
    stmt->Process();
    stmt->StoreResult();
    
    while(stmt->NextResultRow()) {
      run=stmt->GetString(0);
      
      break;
    }
    title=tile;
    db->Close();
  }
  
  if(run.empty()) {
    cout<<"Could not find a run...exiting\n"<<endl;
    exit(1);
  }
  cout<<"Using run "<<run<<endl;

  if(!tile.empty() && !force_mfp) {
    string parent;
    // find out if it is psf homogenized or not
    
    db = TSQLServer::Connect(db_string,
                             "pipeline", "dc01user");
    // get the most recent run
    db_cmd=Form("select a.imagetype from image a,coadd_src b,"
                "coadd c where c.id=b.coadd_imageid and "
                "a.id=b.src_imageid and c.run like '%s%%' and "
                "rownum <100",run.c_str());
    
    if(debug) cout<<db_cmd<<endl;
    
    stmt=db->Statement(db_cmd.c_str());
    
    stmt->Process();
    stmt->StoreResult();
    
    while(stmt->NextResultRow()) {
      parent=stmt->GetString(0);
      //cout<<parent<<endl;
      if(parent!="")break;
    }
    

    if(parent=="remap") psf=false;
    else if(parent=="norm") psf=true;
    else {
      cout<<"Not a valid coadd parent: "<<parent<<endl;
      exit(1);
    }
    if(debug) cout<<"PSF="<<psf<<endl;

    // find out if it model fitting
    //
    db->Close();
  }
    
  char b_nite[20];
  char b_band[5];
  double overlap_phot_mean,overlap_phot_sigma,overlap_ra_mean,
    overlap_ra_sigmal,overlap_ra_sigmar, overlap_dec_mean,
    overlap_dec_sigmal,overlap_dec_sigmar;
  double usnob_ra_mean,
    usnob_ra_sigmal,usnob_ra_sigmar, usnob_dec_mean,
    usnob_dec_sigmal,usnob_dec_sigmar;
  double star_phot_mean,star_phot_sigma,star_ra_mean,
    star_ra_sigmal,star_ra_sigmar, star_dec_mean,
    star_dec_sigmal,star_dec_sigmar;
  double gal_phot_mean,gal_phot_sigma,gal_ra_mean,
    gal_ra_sigmal,gal_ra_sigmar, gal_dec_mean,
    gal_dec_sigmal,gal_dec_sigmar;
    



//   TTree *tree=new TTree("tree","");
//   tree->Branch("nite",&b_nite,"nite/C");
//   tree->Branch("band",&b_band,"band/C");
//   tree->Branch("ov_phot_mean",&overlap_phot_mean,"ov_phot_mean/D");
//   tree->Branch("ov_phot_sigma",&overlap_phot_sigma,"ov_phot_sigma/D");
//   tree->Branch("ov_ra_mean",&overlap_ra_mean,"ov_ra_mean/D");
//   tree->Branch("ov_ra_sigmal",&overlap_ra_sigmal,"ov_ra_sigmal/D");
//   tree->Branch("ov_ra_sigmar",&overlap_ra_sigmar,"ov_ra_sigmar/D");
//   tree->Branch("ov_dec_mean",&overlap_dec_mean,"ov_dec_mean/D");
//   tree->Branch("ov_dec_sigmal",&overlap_dec_sigmal,"ov_dec_sigmal/D");
//   tree->Branch("ov_dec_sigmar",&overlap_dec_sigmar,"ov_dec_sigmar/D");
    
//   tree->Branch("usnob_ra_mean",&usnob_ra_mean,"usnob_ra_mean/D");
//   tree->Branch("usnob_ra_sigmal",&usnob_ra_sigmal,"usnob_ra_sigmal/D");
//   tree->Branch("usnob_ra_sigmar",&usnob_ra_sigmar,"usnob_ra_sigmar/D");
//   tree->Branch("usnob_dec_mean",&usnob_dec_mean,"usnob_dec_mean/D");
//   tree->Branch("usnob_dec_sigmal",&usnob_dec_sigmal,"usnob_dec_sigmal/D");
//   tree->Branch("usnob_dec_sigmar",&usnob_dec_sigmar,"usnob_dec_sigmar/D");
    
//   tree->Branch("star_phot_mean",&star_phot_mean,"star_phot_mean/D");
//   tree->Branch("star_phot_sigma",&star_phot_sigma,"star_phot_sigma/D");
//   tree->Branch("star_ra_mean",&star_ra_mean,"star_ra_mean/D");
//   tree->Branch("star_ra_sigmal",&star_ra_sigmal,"star_ra_sigmal/D");
//   tree->Branch("star_ra_sigmar",&star_ra_sigmar,"star_ra_sigmar/D");
//   tree->Branch("star_dec_mean",&star_dec_mean,"star_dec_mean/D");
//   tree->Branch("star_dec_sigmal",&star_dec_sigmal,"star_dec_sigmal/D");
//   tree->Branch("star_dec_sigmar",&star_dec_sigmar,"star_dec_sigmar/D");
    
//   tree->Branch("gal_phot_mean",&gal_phot_mean,"gal_phot_mean/D");
//   tree->Branch("gal_phot_sigma",&gal_phot_sigma,"gal_phot_sigma/D");
//   tree->Branch("gal_ra_mean",&gal_ra_mean,"gal_ra_mean/D");
//   tree->Branch("gal_ra_sigmal",&gal_ra_sigmal,"gal_ra_sigmal/D");
//   tree->Branch("gal_ra_sigmar",&gal_ra_sigmar,"gal_ra_sigmar/D");
//   tree->Branch("gal_dec_mean",&gal_dec_mean,"gal_dec_mean/D");
//   tree->Branch("gal_dec_sigmal",&gal_dec_sigmal,"gal_dec_sigmal/D");
//   tree->Branch("gal_dec_sigmar",&gal_dec_sigmar,"gal_dec_sigmar/D");



  // Get info grom coadd_calczp file
  map<int,double> file_zp;
  if(!zpfile.empty()) {
    ifstream file_in(zpfile.c_str());
    
    int id;
    double zp,zp_err;

    while(file_in>>id>>zp) {
      file_zp[id]=zp;
      if(debug)cout<<id<<" "<<zp<<endl;
    }
    if(debug>1)cout<<"Found "<<file_zp.size()<<" images from zp file"<<endl;
  }
   
    

  double slr_corr=0;
  if(slr_cal) {
    // get slr correction
    db = TSQLServer::Connect(db_string,
                             "pipeline", "dc01user");
    // get the most recent run
    db_cmd=Form("select %s_corr from slr_cal where "
                "run='%s'",
                which_band.c_str(),run.c_str());
    if(debug) cout<<db_cmd<<endl;
    
    stmt=db->Statement(db_cmd.c_str());
    
    stmt->Process();
    stmt->StoreResult();
    bool ok=stmt->NextResultRow();

    if(ok) {
      slr_corr=stmt->GetDouble(0);
    }
    else {
      cout<<"Could not find an entry for the run in slr_cal."
          <<" Making no correction"<<endl;
    }

    db->Close();
  }





  cout<<"\n\n******* Processing nite "<<nite<<" "<<run
      <<"************"<<endl;
  
  string bands[4]={"g","r","i","z"};
  int cur_band;
  int count=0;
  for(int cur_band=0;cur_band<4;cur_band++) {

    
    string band=bands[cur_band];
    strcpy(b_band,band.c_str());
    if(which_band==band || which_band=="all" || force_band) {

      if(force_band) band=which_band;
      if(count>0) break;
      count++;
      cout<<"\nRunning on band "<<band<<endl;
      
      // TCanvas *canvas=new TCanvas("canvas","",1080,1080);
//       canvas->SetLeftMargin(0);
//       canvas->SetRightMargin(0);
//       canvas->SetTopMargin(0);
//       canvas->SetBottomMargin(0);
//       TPad *pad=new TPad("pad","",0,0,1,0.955555);
//       pad->SetLeftMargin(0);
//       pad->SetRightMargin(0);
//       pad->SetTopMargin(0);
//       pad->SetBottomMargin(0);
//       pad->Draw();
//       int can_counter=1;
//       pad->Divide(3,4,0,0);
      
        
      // convert to xyz

        
      vector<ObjPair> obj_pairs,obj_pairs_ccd;
      db = TSQLServer::Connect(db_string,
                               "pipeline", "dc01user");
      if(!nite.empty()) {
      // get the list of red files
        db_cmd=Form("select a.id, a.ccd, c.exposurename,c.nite,"
                    "a.exptime, a.fwhm, a.skybrite,a.parentid,a.band,a.run "
                    " from image a, location c "
                    "where a.project='%s' and a.imagetype='red' and "
                    "c.band='%s' and a.id=c.id and a.band=c.band and "
                    "a.nite='%s' and c.run like '%s%%'",
                  //"      c.archivesites!=\'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN\'"
                  //"      and c.exposurename like \'BCS%%\'",
                  project.c_str(),band.c_str(),nite.c_str(),run.c_str());
      } else {
        if(use_all_remaps) {
          db_cmd=Form("select a.parentid, a.ccd, c.exposurename,a.nite,"
                      "a.exptime, a.fwhm, a.skybrite,a.parentid,a.band,a.run "
                      "from image a, location c "
                      "where a.project='%s' and a.imagetype='remap' "
                      "and a.id=c.id and a.band='%s' and a.tilename='%s' and "
                      "c.archivesites!='NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN'",
                      project.c_str(),band.c_str(),tile.c_str()); 
        }
        else if(!psf) {
          db_cmd=Form("select a.parentid, a.ccd, d.exposurename,a.nite,"
                    "a.exptime, a.fwhm, a.skybrite,a.parentid,a.band,a.run "
                    "from image a,coadd_src b,coadd c,exposure d where "
                    "b.coadd_imageid=c.id and a.exposureid=d.id "
                    "and a.id=b.src_imageid and c.run like '%s%%' and "
                    "a.project='%s' and a.band='%s'",
                    run.c_str(),project.c_str(),band.c_str());
        }
        else {
          db_cmd=Form("select a.parentid, a.ccd, d.exposurename,a.nite,"
                      "a.exptime, a.fwhm, a.skybrite,a.parentid,a.band,a.run "
                      "from image a,coadd_src b,coadd c,exposure d,image e "
                      "where b.coadd_imageid=c.id and a.exposureid=d.id "
                      "and e.id=b.src_imageid and c.run like '%s%%' and "
                      "a.project='%s' and a.band='%s' and a.id=e.parentid",
                    run.c_str(),project.c_str(),band.c_str());
        }

      }
                    
      
        
      if(debug) {
        cout<<"\nUsing the following command to get images from the database"
            <<endl;
        cout<<db_cmd<<endl;
      }
      
      stmt=db->Statement(db_cmd.c_str());
        
      // Check for errors in the statement
      if(!stmt->Process()) {
        cout<<stmt->GetErrorMsg()<<endl;
        exit(1);
      }
      stmt->StoreResult();
        

      int im_ccd,im_nite,im_id;
      string im_expname,im_run,im_band;
      double im_exptime,im_fwhm,im_skybrite;

      int imageid;// for later use in modelfitting
      while(stmt->NextResultRow()) {
        //if(ImageList.size()>50) break;
        im_id=stmt->GetInt(0);
        imageid=im_id;
        im_ccd=stmt->GetInt(1);
        im_expname=stmt->GetString(2);
        im_nite=stmt->GetInt(3);
        im_exptime=stmt->GetDouble(4);
        im_fwhm=stmt->GetDouble(5);
        im_skybrite=stmt->GetDouble(6);
        im_band=stmt->GetString(8);
        im_run=stmt->GetString(9);

        if(match_exp.size()>0) {
          bool match=false;
          for(int i=0;i<match_exp.size();i++) {
            if(((TString)im_expname).BeginsWith(match_exp[i].c_str())) match=true;
          }
          if(!match) continue;
        }
        if(match_ccd.size()>0) {
          bool match=false;
          for(int i=0;i<match_ccd.size();i++) {
            
            if(im_ccd==atoi(match_ccd[i].c_str())) {
              match=true;
              
            }
          }
          if(!match) continue;
        }
        
        if(debug)cout<<im_id<<" "<<im_run<<" "<<im_expname<<" "<<im_nite<<" "<<im_ccd<<endl;

        ImageInfo im(im_id,im_expname,im_exptime,im_ccd,
                     im_nite,im_run,im_fwhm,im_skybrite);
        im.band=im_band;
        //if(im_exptime<40) continue;
        // Check to see if we already have this particular nite/exposure
        // if multiple entries use the latest one.
        map<int,ImageInfo>::iterator iter=ImageList.begin();;
          
        bool insert=true;
        while (iter!=ImageList.end()) {
                  
          if(im_expname==iter->second.exp_name && im_nite==iter->second.nite &&
             im_ccd==iter->second.ccd ) {
            // use the one with the most recent id
            if(im_id>iter->second.id) {
              ImageList.erase(iter++);
            }
            else {
              insert=false;
              break;
            }
          }
          else ++iter;
        }
        if(insert) {
          ImageList[im_id]=im;     
        }
      }
      delete stmt;
      db->Close();
    

            // find out if model fitting was used
      db = TSQLServer::Connect(db_string,
                                 "pipeline", "dc01user");

       double alpha;
       db_cmd=Form("select a.alpha_j2000 from objects a "
                   "where a.imageid=%d and rownum<2",imageid);

       
       if(debug) cout<<db_cmd<<endl;
       
       stmt=db->Statement(db_cmd.c_str());
       
       stmt->Process();
       stmt->StoreResult();
       
       while(stmt->NextResultRow()) {
         alpha=stmt->GetDouble(0);
         break;
       }
       
       //if(alpha==0) mfp=true;
       //else mfp=false;
       if(force_mfp) mfp=true;
       if(debug) cout<<"MFP="<<mfp<<endl;
       db->Close();

        
      cout<<"Found "<<ImageList.size()<<" distinct single epoch images"<<endl;
        
      if(ImageList.size()==0) continue;
        
      // Get information on images  
        
      
      map<int,ImageInfo>::iterator image_iter=ImageList.begin();
      if(!cal && project!="BCS") {
        image_iter=ImageList.begin();
        for(; image_iter!=ImageList.end(); ++image_iter) {
          image_iter->second.zp=25;
        }
      }
      else if(zpfile.empty()) {
        for(; image_iter!=ImageList.end(); ++image_iter) {
          
          db = TSQLServer::Connect(db_string,
                                   "pipeline", "dc01user");
          
          db_cmd=Form("select a.mag_zero,a.sigma_mag_zero,c.photflag from "
                      "zeropoint a, image b, exposure c where a.imageid=%d and "
                      " b.id=a.imageid and a.tilename='%s' and "
                      "b.exposureid=c.id order by insert_date"
                      ,image_iter->second.id,tile.c_str());
          if(debug)cout<<db_cmd<<endl;
          stmt=db->Statement(db_cmd.c_str());
          
          // Check for errors in the statement
          if(!stmt->Process()) {
            cout<<stmt->GetErrorMsg()<<endl;
            exit(1);
          }
          stmt->StoreResult();
          
          // check how many results
          image_iter->second.zp=-1;
          while(stmt->NextResultRow()) {
          
            // the first entry should be the most recent
            image_iter->second.zp=stmt->GetDouble(0);
            image_iter->second.zp_err=stmt->GetDouble(1);
            image_iter->second.photometric=stmt->GetInt(2);
          }
        
          if(image_iter->second.zp==-1) {
          
            cout<<"zp does not exist in table assigning =0"<<endl;
            image_iter->second.zp=0;
            image_iter->second.zp_err=0;
                    
          }
          
          if(debug)cout<<" Got zp "<<image_iter->second.exp_name<<"_"
                       <<image_iter->second.ccd<<" ="<<image_iter->second.zp<<endl;
          delete stmt;
          db->Close();
        }
      
        // remove image with failed zeropoint
        image_iter=ImageList.begin();
        while( image_iter!=ImageList.end()) {
          if(image_iter->second.zp<10 || image_iter->second.zp>zp_max) {
            cout<<image_iter->second.exp_name<<"_"
                <<image_iter->second.ccd<<" had zeropoint out of bounds: "
                <<image_iter->second.zp<<endl;
            ImageList.erase(image_iter++);
          }
          else image_iter++;
        }
      }
      else {
        
        image_iter=ImageList.begin();
        //for(; image_iter!=ImageList.end(); ++image_iter) {
        while( image_iter!=ImageList.end()) {
          if(file_zp[image_iter->second.id]<10) {
            cout<<image_iter->second.id<<" "<<
              image_iter->second.exp_name<<"_"
                <<image_iter->second.ccd<<" had zeropoint out of bounds: "
                <<file_zp[image_iter->second.id]<<endl;
            ImageList.erase(image_iter++);
          }
          else {
            image_iter->second.zp=file_zp[image_iter->second.id];
            if(debug){
              cout<<" Got zp "<<image_iter->second.exp_name<<"_"
                  <<image_iter->second.ccd<<" ="<<image_iter->second.zp<<endl;
            }
            image_iter++;
          }
        }
      }
      
      
      
      // build image pairs 
      vector<ImagePair> ImagePairs;
      vector<ImagePair> ImagePairsCCD;
      vector<ImagePair>::iterator pair_iter;


      image_iter=ImageList.begin();

      for(; image_iter!=ImageList.end(); image_iter++) {
        
        map<int,ImageInfo>::iterator image_iter2=ImageList.begin();
        for(; image_iter2!=ImageList.end(); image_iter2++) {

      
          if(//image_iter->second.nite==image_iter2->second.nite &&
             image_iter->second.exp_name!=image_iter2->second.exp_name //&&
             //image_iter->second.ccd != image_iter2->second.ccd 
             ) {
            
            ImagePair pa(image_iter->first,image_iter2->first);
            
            
            // check to see if pair already exists
            pair_iter=find(ImagePairs.begin(),ImagePairs.end(),pa);
            if(pair_iter==ImagePairs.end() && 
               image_iter!=image_iter2 ) {

              if(same_ccd) {
                if(image_iter->second.ccd==image_iter2->second.ccd){
                  ImagePairs.push_back(pa);
                }
              }
              else {
                  ImagePairs.push_back(pa);
              }
            
              //cout<<"   Match"<<endl;
            }
          
            // build list of same ccd pairs
            // if(image_iter->second.ccd==image_iter2->second.ccd){
//               pair_iter=find(ImagePairsCCD.begin(),ImagePairsCCD.end(),pa);
//               if(pair_iter==ImagePairsCCD.end() && 
//                  image_iter!=image_iter2 ) {
//                 ImagePairsCCD.push_back(pa);
//               }
//             }
          }
        }
      }

     //  cout<<" Found "<<ImagePairs.size()<<" pairs of images"<<endl;
//       cout<<" Found "<<ImagePairsCCD.size()<<" pairs of images on the"
//           <<" same ccd"<<endl;

      // get a list of objects from each red image of of the 
      // objects current table
      map<int,vector<Object> > ImageObjs;
      map<int,vector<Object> > ImageObjsPhoto;
      image_iter=ImageList.begin();
    
      TFile tfile(Form("debug_%s_%s_all.root",title.c_str(),band.c_str()),
                  "recreate");
      Object *ob=new Object;
      TTree *tree_all=new TTree("tree","");
      char id_char[50],exp_char[100];
      tree_all->Branch("id",&id_char,"id/C");
      tree_all->Branch("exp",&exp_char,"exp/C");
      tree_all->Branch("ob",&ob);

      
      for(; image_iter!=ImageList.end(); ++image_iter) {
        if(debug)cout<<"Getting objects from image "<<image_iter->second.id<<endl;
        sprintf(id_char,"%d",image_iter->second.id);
        sprintf(exp_char,"%s",image_iter->second.exp_name.c_str());
        double max_ra=-1e6,max_dec=-1e6,min_ra=1e6,min_dec=1e6;
        vector<Object> vec;
        db = TSQLServer::Connect(db_string,
                                 "pipeline", "dc01user");
      
        if(star_sel=="spread") {

          db_cmd=Form("select a.ra,a.dec,"
                      "a.band,"
                      "a.catalogid,a.imageid,a.mag_\%s, " 
                      "a.magerr_%s,a.spread_model,a.flags,a.zeropoint,"
                      "a.x_image,a.y_image from objects a,"
                      "exposure b,image c "
                      " where "
                      " a.imageid=%d and c.exposureid=b.id and a.imageid=c.id",
                      mag.c_str(),mag.c_str(),
                      image_iter->second.id);
        } else {
          db_cmd=Form("select a.ra,a.dec,"
                      "a.band,"
                      "a.catalogid,a.imageid,a.mag_\%s, " 
                      "a.magerr_%s,a.class_star,a.flags,a.zeropoint,"
                      "a.x_image,a.y_image from objects a,"
                      "exposure b,image c "
                      " where "
                      " a.imageid=%d and c.exposureid=b.id and a.imageid=c.id",
                      mag.c_str(),mag.c_str(),
                      image_iter->second.id);
        }
        if(debug) {
          cout<<"Using following command to get objects from images:"<<endl;
          cout<<db_cmd<<endl;
        }
        stmt=db->Statement(db_cmd.c_str());
       
        // Check for errors in the statement
        if(!stmt->Process()) {
          cout<<stmt->GetErrorMsg()<<endl;
          exit(1);
        }
        stmt->StoreResult();
       
       
        double ra,dec;
        int catid,imageid;
        double obj_mag,obj_err,obj_class;
        int obj_flag;
        string im_band;
        double mag_zero;
        double x_image,y_image;
        while(stmt->NextResultRow()) {
          ra=stmt->GetDouble(0);       
          dec=stmt->GetDouble(1);       
          im_band=stmt->GetString(2);       
          catid=stmt->GetInt(3);       
          imageid=stmt->GetInt(4);       
          obj_mag=stmt->GetDouble(5);       
          obj_err=stmt->GetDouble(6);       
          obj_class=stmt->GetDouble(7);       
          obj_flag=stmt->GetInt(8);   
          mag_zero=stmt->GetDouble(9);
          x_image=stmt->GetDouble(10);
          y_image=stmt->GetDouble(11);
          //if (obj_class<star_cut || obj_mag>98 || obj_err>err_cut) continue;   
          if (obj_mag>98 || obj_err>err_cut || 
              obj_class<star_cut_min || obj_class>star_cut_max ||
              obj_mag > mag_max || obj_mag < mag_min || obj_flag>max_flag
              ) continue;   

          if(extinction) {
           
            double R_g=3.793;
            double R_r=2.751;
            double R_i=2.086;
            double R_z=1.479;
           
            double EBV=ExtMap.GetExtinction(ra,dec);
           
            /* apply extinction correction in each band */
            if(im_band=="g") obj_mag -= R_g * EBV;
            else if(im_band=="r") obj_mag -= R_r * EBV;
            else if(im_band=="i") obj_mag -= R_i * EBV;
            else if(im_band=="z") obj_mag -= R_z * EBV;
          }
         
          // if zp has been calculated by coadd calczp
          //if(!IsPhotometric && image_iter->zp!=25)

          if(ra>max_ra) max_ra=ra;
          if(dec>max_dec) max_dec=dec;
          
          if(ra<min_ra) min_ra=ra;
          if(dec<min_dec) min_dec=dec;
          
          Object obj(ra,dec,obj_mag,obj_err,im_band,
                     catid,imageid);
          obj.class_star=obj_class;
          obj.flag=obj_flag;
          obj.x_image=x_image;
          obj.y_image=y_image;
         
          double cd=cos(obj.Dec*TMath::DegToRad());
          obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
          obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
          obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());

         //  if(mag_zero>27 && project=="SPT") {
//             obj.mag-=mag_zero-25;
//           }
	  
          
          //cout<<image_iter->second.id<<" "<<obj.mag<<" ";
          if(cal) {
            obj.mag+=(image_iter->second.zp-mag_zero);
          }
          
          if(corr>0) {
            obj.mag+=corr;
          }

          if(slr_cal) {
            obj.mag+=slr_corr;
          }

          *ob=obj;
          tree_all->Fill();
//           if(project=="BCS") {
//             if(fabs(mag_zero-25)<0.0001) {
//               obj.mag+=(image_iter->second.zp-25);
//             }
//           }

          // cout<<image_iter->second.zp<<" "<<obj.mag<<" "<<mag_zero<<endl;

          vec.push_back(obj);
         
          
        }

        ImageObjs[image_iter->first]=vec;
        
        ImageList[image_iter->first].max_RA=max_ra;
        ImageList[image_iter->first].min_RA=min_ra;
        ImageList[image_iter->first].max_Dec=max_dec;
        ImageList[image_iter->first].min_Dec=min_dec;
        if(debug)cout<<"  Found "<<vec.size()<<" objects"<<endl;

        delete stmt;
        db->Close();

      }
      
      tree_all->Write();
      tfile.Close();

      
      Object pa1,pa2;
      double zp1,zp2;
      int id1,id2;
      TTree *tree=new TTree("tree","");
      tree->Branch("pa1",&pa1);
      tree->Branch("zp1",&zp1);
      tree->Branch("zp2",&zp2);
      tree->Branch("id1",&id1);
      tree->Branch("id2",&id2);
      tree->Branch("pa2",&pa2);
      
    
      // for all pairs of images find matching objects
      pair_iter=ImagePairs.begin();
    
      for(; pair_iter!=ImagePairs.end(); ++pair_iter) {
        
        id1=pair_iter->id1;
        id2=pair_iter->id2;

        if(same_nite) {
          if(ImageList[id1].nite!=ImageList[id2].nite) continue;
        }

        if(diff_nite) {
          if(ImageList[id1].nite==ImageList[id2].nite) continue;
        }

        KD_Tree obj_tree; // store first list in kd tree
        
        for(int i=0;i<ImageObjs[id1].size();i++) {
          obj_tree.insert(ImageObjs[id1][i]);
        }
        //if(debug)cout<<"Matching "<<id1<<" with "<<ImageObjs[id1].size()
        //           <<" to "<<id2<<" with "<<ImageObjs[id2].size()<<endl;

        vector<Object>::iterator obj_iter=ImageObjs[id2].begin();
        
        // Loop through objects from second image and
        // try to match from the first image
        for(; obj_iter!=ImageObjs[id2].end(); ++obj_iter) {
        
          // find closest match within tolerance
          KD_Match match=obj_tree.find_nearest(*obj_iter,tol);
        
          if(match.first!=obj_tree.end()) {
            //cout<<" Distance = "<<match.second;
            ObjPair pa;
            //cout<<match.second<<endl;
            pa.obj1=*match.first;
            pa.obj2=*obj_iter;
            pa.id1=id1;
            pa.id2=id2;
            pa1=pa.obj1;
            pa2=pa.obj2;
            zp1=ImageList[id1].zp;
            zp2=ImageList[id2].zp;
            tree->Fill();
           //  cout<<id1<<" "<<id2<<endl;
//             cout<<" "<<pa.obj1.mag<<" "<<pa.obj2.mag<<endl;
//             cout<<" "<<zp1<<" "<<zp2<<endl;
//               cout<<" "<<pa.obj1.RA<<" "<<pa.obj2.RA<<endl;
//               cout<<" "<<pa.obj1.Dec<<" "<<pa.obj2.Dec<<endl;
            //obj_pairs.push_back(pa);
            pair_iter->obj_pairs.push_back(pa);
          }
        }
      
      
      }


      // remove pairs with to few matches
      pair_iter=ImagePairs.begin();
      while ( pair_iter!=ImagePairs.end()) {
        if(pair_iter->obj_pairs.size()<min_pair) {
         ImagePairs.erase(pair_iter);
       }
       else pair_iter++;
      }



      pair_iter=ImagePairs.begin();
      for(; pair_iter!=ImagePairs.end(); ++pair_iter) {
         
        vector<ObjPair>::iterator obj_iter=pair_iter->obj_pairs.begin();
         for(; obj_iter!=pair_iter->obj_pairs.end();obj_iter++) {
           obj_pairs.push_back(*obj_iter);
         }
      }
      
      //exit(1);
       TCanvas cc;
      double dd;

      // calculate mean/rms of all objects from pairs
      pair_iter=ImagePairs.begin();
      for(; pair_iter!=ImagePairs.end(); ++pair_iter) {
      
        
        int id1=pair_iter->id1;
        int id2=pair_iter->id2;
        double zp1=ImageList[id1].zp;
        double zp2=ImageList[id2].zp;
        vector<double> diff,diff_err;
        vector<ObjPair>::iterator obj_iter=pair_iter->obj_pairs.begin();
        
        // TTree ttree("tree","");
//         ttree.Branch("dd",&dd);
        double mag_diff,mag_err;
        for(; obj_iter!=pair_iter->obj_pairs.end();obj_iter++) {
          
          //cout<<obj_iter->obj1.mag<<" "<<obj_iter->obj2.mag<<endl;
          mag_diff=(obj_iter->obj1.mag-obj_iter->obj2.mag);
          mag_err=TMath::Sqrt(obj_iter->obj1.mag_err*obj_iter->obj1.mag_err+
                              obj_iter->obj2.mag_err*obj_iter->obj2.mag_err);
          diff.push_back(mag_diff);
          diff_err.push_back(mag_err);
          //dd=mag_diff;
          //if(fabs(dd)<0.3)
          //ttree.Fill();
        }
        
        
        if(diff.size() !=0) {
          
          double mean;//=TMath::Mean(diff.size(),&diff[0],&diff

          double rms;//=TMath::RMS(diff.size(),&diff[0]);
          
          double m_mean=TMath::Mean(diff.size(),&diff[0]);
          
          int good,bad;
          sigma_clip(3,2.5,diff,diff_err,mean,
                     good,bad,rms,true,false,1e-6);
          pair_iter->mean=mean;
          pair_iter->rms=rms;

          if(debug) {
           //  cc.Clear();
//             TH1D h1("h1","",50,mean-5*rms,mean+5*rms);
//             ttree.Draw("dd>>h1");
//             h1.Fit("gaus");
//             TLine *hi=new TLine(mean,0,
//                                 mean,h1.GetMaximum());
//             hi->SetLineColor(2);
//             hi->Draw();
            
//             TLine *hi2=new TLine(m_mean,0,
//                                  m_mean,h1.GetMaximum());
//             hi2->SetLineColor(4);
//             hi2->Draw();
            
            //mean=m_mean;
            
            
            //mean=h1.GetFunction("gaus")->GetParameter(1);
            //mean=h1.GetMean();
            // double mean=TMath::Mean(diff.size(),&diff[0],&diff_err[0]);
            
            
            
            
            cout<<id1<<" "<<ImageList[id1].exp_name<<"_"
                <<ImageList[id1].ccd<<" "<<ImageList[id1].zp<<endl;
            cout<<id2<<" "<<ImageList[id2].exp_name<<"_"
                <<ImageList[id2].ccd<<" "<<ImageList[id2].zp<<endl;
            cout<<"  "<<diff.size()<<" "<<mean<<" "<<rms<<endl;
            cout<<"  "<<-mean<<" "<<ImageList[id1].zp-ImageList[id2].zp<<endl;
           //  dd=-mean-(ImageList[id1].zp-ImageList[id2].zp);
//             //ttree.Fill();
            
//             //TH1D h1("h1","",50,-0.2,0.2);
//             //ttree.Draw("dd>>h1");
            
//             cc.Update();
//             cc.WaitPrimitive();
          }
        }
        else {
          pair_iter->mean=-999;
          pair_iter->rms=999;
        }
      }



      //ttree.Draw("dd");
      //cc.WaitPrimitive();
      // insert zp diff info into obj_pairs vector
      vector<ObjPair>::iterator obj_iter=obj_pairs.begin();
      for(; obj_iter!=obj_pairs.end();obj_iter++) {
        
        // find the image pair
        ImagePair pa(obj_iter->id1,obj_iter->id2);
        vector<ImagePair>::iterator imagepair_iter=find(ImagePairs.begin(),
                                                        ImagePairs.end(),pa);
        if(imagepair_iter==ImagePairs.end()) {
          if(debug) cout<<"Warning: Could not find image pair:"
                        <<obj_iter->id1<<" "<<obj_iter->id2<<" for zp"
                        <<" information"<<endl;
          continue;
        }
        obj_iter->zp_diff=imagepair_iter->mean;
        obj_iter->zp1=ImageList[pa.id1].zp;
        obj_iter->zp2=ImageList[pa.id2].zp;
        
      }
        
      
      pair_iter=ImagePairs.begin();
      for(; pair_iter!=ImagePairs.end(); ++pair_iter) {
        // insert zp diff info into obj_pairs vector
        vector<ObjPair>::iterator obj_iter=pair_iter->obj_pairs.begin();
        for(; obj_iter!=pair_iter->obj_pairs.end();obj_iter++) {
          
          // find the image pair
          ImagePair pa(obj_iter->id1,obj_iter->id2);
          vector<ImagePair>::iterator imagepair_iter=find(ImagePairs.begin(),
                                                          ImagePairs.end(),pa);
          if(imagepair_iter==ImagePairs.end()) {
            cout<<"Warning: Could not find image pair:"
                <<obj_iter->id1<<" "<<obj_iter->id2<<" for zp"
                <<" information"<<endl;
            continue;
          }
          obj_iter->zp_diff=imagepair_iter->mean;
          obj_iter->zp1=ImageList[pa.id1].zp;
          obj_iter->zp2=ImageList[pa.id2].zp;
        }
      }
     


        

//       // ccd matches if non photometric
//       if(!IsPhotometric) {
//         pair_iter=ImagePairsCCD.begin();
    
//         for(; pair_iter!=ImagePairsCCD.end(); ++pair_iter) {
        
//           int id1=pair_iter->id1;
//           int id2=pair_iter->id2;
        
//           KD_Tree obj_tree; // store first list in kd tree
        
//           for(int i=0;i<ImageObjs[id1].size();i++) {
//             obj_tree.insert(ImageObjs[id1][i]);
//           }
//           //if(debug)cout<<"Matching "<<id1<<" with "<<ImageObjs[id1].size()
//           //<<" to "<<id2<<" with "<<ImageObjs[id2].size()<<endl;
        
//           vector<Object>::iterator obj_iter=ImageObjs[id2].begin();
        
//           // Loop through objects from second image and
//           // try to match from the first image
//           for(; obj_iter!=ImageObjs[id2].end(); ++obj_iter) {
          
//             // find closest match within tolerance
//             KD_Match match=obj_tree.find_nearest(*obj_iter,tol);
          
//             if(match.first!=obj_tree.end()) {
//               //cout<<" Distance = "<<match.second;
//               ObjPair pa;
            
//               pa.obj1=*match.first;
//               pa.obj2=*obj_iter;
//               //pa1=pa.obj1;
//               //pa2=pa.obj2;
//               //tree->Fill();
//               //cout<<" "<<pa.obj1.mag<<" "<<pa.obj2.mag<<endl;
//               // cout<<" "<<pa.obj1.RA<<" "<<pa.obj2.RA<<endl;
//               // cout<<" "<<pa.obj1.Dec<<" "<<pa.obj2.Dec<<endl;
//               obj_pairs_ccd.push_back(pa);
//               pair_iter->obj_pairs.push_back(pa);
//             }
//           }
//         }
//       }
      

      // calculate mean offset for all objects on pairs of
      // images



      //TFile tfile("test.root","recreate");
      //  tree->Write();
      //  tfile.Close();
    
      //     }
      //     else {
    
      //       TFile infile(loaddata.c_str());
      //       TTree *intree=(TTree*) infile.Get("tree");
      //       Object *pa1=new Object;
      //       Object *pa2=new Object;
      //       ObjPair obj_pair;
      //       intree->SetBranchAddress("pa1",&pa1);
      //       intree->SetBranchAddress("pa2",&pa2);
    
      //       int i=0;
      //       while(intree->GetEntry(i)) {
      //         i++;
      //         obj_pair.obj1=*pa1;
      //         obj_pair.obj2=*pa2;
      //         obj_pairs.push_back(obj_pair);
      //       }
      //     }
    
    
      cout<<"Found "<<obj_pairs.size()<<" overlapping matches"<<endl;
      cout<<"Found "<<obj_pairs_ccd.size()<<" overlapping matches on same"
          <<" ccd"<<endl;

     if(plot_pairs) {
       pair_iter=ImagePairs.begin();
       for(; pair_iter!=ImagePairs.end(); ++pair_iter) {
         string ti=Form("%s_%d_%d",title.c_str(),pair_iter->id1,
                        pair_iter->id2);

         if(pair_iter->obj_pairs.size()>plot_pairs_min) {
           if(debug) cout<<ti<<" "<<pair_iter->obj_pairs.size()<<endl;
           plot_phot(pair_iter->obj_pairs,ti,dir,band,mag,
                     Form("<%s_{%s}>",band.c_str(),mag.c_str()),
                     mag,debug,rm_zpdiff,sigma,50);
         }
         else {
           if(debug)cout<<ti<<" not enough matches"<<endl;
         }
       }
     }

      string ov_title="Overlapping";
      title+="_rel_"+mag;
      if(cal) title+="_cal";
      else title+="_uncal";
      if(!suffix.empty()) title+="_"+suffix;
      plot_phot(obj_pairs,title,dir,band,mag,
                Form("<%s_{%s}>",band.c_str(),mag.c_str()),
                mag,debug,rm_zpdiff,sigma,N_bin);



         
      vector<double> diff,diff_err;
      obj_iter=obj_pairs.begin();
      
      double mag_diff,mag_err;
      vector<double> mdiff,mdiff_err;
      for(; obj_iter!=obj_pairs.end();obj_iter++) {
        
        mag_diff=(obj_iter->obj1.mag-obj_iter->obj2.mag);
        mag_err=TMath::Sqrt(obj_iter->obj1.mag_err*obj_iter->obj1.mag_err+
                            obj_iter->obj2.mag_err*obj_iter->obj2.mag_err);
        mdiff.push_back(mag_diff);
        mdiff_err.push_back(1./mag_err);
      }

      double clipped_mean,clipped_rms;
      int good, bad;
      sigma_clip(3,2.5,mdiff,mdiff_err,clipped_mean,
                 good,bad,clipped_rms,true,false,1e-6);

      double mean=TMath::Mean(mdiff.size(),&mdiff[0]);
      double rms=TMath::RMS(mdiff.size(),&mdiff[0]);

      cout<<"Mean difference "<<mean<<endl;
      cout<<"RMS "<<rms<<endl;
      
      cout<<"Clipped Mean difference "<<clipped_mean<<endl;
      cout<<"Clipped RMS "<<clipped_rms<<endl;
    
//       if(obj_pairs.size()>0) {
//         if(IsPhotometric) {
 //           plot_phot_all(obj_pairs,pad,can_counter,
//                         ov_title+" Photometry",dir,band,debug,mag+"1",mag+"2",
//                          overlap_phot_mean,overlap_phot_sigma);
//         }
//         else {
//           plot_phot_all(obj_pairs_ccd,pad,can_counter,
//                         ov_title+" Photometry",dir,band,debug,mag+"1",mag+"2",
//                         overlap_phot_mean,overlap_phot_sigma);
//         }
//         plot_astro(obj_pairs,pad,can_counter,
//                    ov_title,dir,band,debug,
//                    overlap_ra_mean,overlap_ra_sigmal,
//                    overlap_ra_sigmar,
//                    overlap_dec_mean,overlap_dec_sigmal,
//                    overlap_dec_sigmar
//                    );
//       }
//       else {
//         can_counter+=3;
//         overlap_phot_mean=-999;
//         overlap_phot_sigma=-999;
//         overlap_ra_mean=-999;
//         overlap_ra_sigmal=-999;
//         overlap_ra_sigmar=-999;
//         overlap_dec_mean=-999;
//         overlap_dec_sigmal=-999;
//         overlap_dec_sigmar=-999;
//       }
    
//       bool found_usnob=false;
//       // USNOB comparison
//       if(usnob) {

      
//         vector<ObjPair> usnob_pairs;

//         // loop over all images and look for usnob matches
//         //map<int,ImageInfo>::iterator 
//         image_iter=ImageList.begin();
      
//         for(; image_iter!=ImageList.end(); image_iter++) {

//           db = TSQLServer::Connect("oracle://desdb/stdes",
//                                    "pipeline", "dc01user");
        
//           KD_Tree usnob_tree;
//           db_cmd=Form("select ra,dec from usnob_cat1"
//                       " where (ra between %f and %f) "
//                       " and (dec between %f and %f) ",
//                       image_iter->second.min_RA,image_iter->second.max_RA,
//                       image_iter->second.min_Dec,image_iter->second.max_Dec);

//           stmt=db->Statement(db_cmd.c_str());
        
//           // Check for errors in the statement
//           if(!stmt->Process()) {
//             cout<<stmt->GetErrorMsg()<<endl;
//             exit(1);
//           }
//           stmt->StoreResult();
        
        
//           double ra,dec;
//           while(stmt->NextResultRow()) {
//             ra=stmt->GetDouble(0);       
//             dec=stmt->GetDouble(1);       
          
//             Object obj(ra,dec,0.0,0.0,"",0,0);
//             double cd=cos(obj.Dec*TMath::DegToRad());
//             obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
//             obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
//             obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());
          
//             usnob_tree.insert(obj);
//           }
//           if(debug)cout<<"Found "<<usnob_tree.size()<<" usnob stars"
//                        <<" in image "<<image_iter->first<<" range"<<endl;
        
//           vector<Object>::iterator obj_iter=ImageObjs[image_iter->first].begin();
        
//           // Loop through objects from second image and
//           // try to match from the first image
//           for(; obj_iter!=ImageObjs[image_iter->first].end(); ++obj_iter) {
          
//             // find closest match within tolerance
//             KD_Match match=usnob_tree.find_nearest(*obj_iter,tol);
          
//             if(match.first!=usnob_tree.end()) {
            
//               ObjPair pa;
            
//               pa.obj1=*match.first;
//               pa.obj2=*obj_iter;
//               usnob_pairs.push_back(pa);
//             }
//           }

//           delete stmt; 
//           db->Close();

//         }

//         cout<<"Found "<<usnob_pairs.size()<<"  usnob matches"<<endl;      
//         can_counter++;
//         string usnob_title=title+"USNO-B";
//         if(usnob_pairs.size()>0) {
//           plot_astro(usnob_pairs,pad,can_counter,
//                      usnob_title,dir,band,debug,
//                      usnob_ra_mean,usnob_ra_sigmal,
//                      usnob_ra_sigmar,
//                      usnob_dec_mean,usnob_dec_sigmal,
//                      usnob_dec_sigmar
//                      );
//           found_usnob=true;
//         }
//         else {
//           can_counter+=2;
//           usnob_ra_mean=-999;
//           usnob_ra_sigmal=-999;
//           usnob_ra_sigmar=-999;
//           usnob_dec_mean=-999;
//           usnob_dec_sigmal=-999;
//           usnob_dec_sigmar=-999;
//         }

//       }


//       // Comparison with standard stars
//       bool found_stars=false;
//       if(standard_stars) {
      

//         vector<ObjPair> stdstar_pairs,stdstar_pairs_photo;

//         // loop over all images and look for usnob matches
//         //map<int,ImageInfo>::iterator 
//         image_iter=ImageList.begin();
      
//         for(; image_iter!=ImageList.end(); image_iter++) {



//           TString exp=image_iter->second.exp_name;
//           string im_band=image_iter->second.band;
        
//           if(exp.BeginsWith(project.c_str())) continue;

//           db = TSQLServer::Connect("oracle://desdb/stdes",
//                                    "pipeline", "dc01user");

//           KD_Tree stdstar_tree;
                
//           db_cmd=Form("select radeg,decdeg,stdmag_g,stdmag_r,stdmag_i,stdmag_z," 
//                       "stdmagerr_g,stdmagerr_r,stdmagerr_i,stdmagerr_z " 
//                       " FROM standard_stars "
//                       " where (radeg between %f and %f) "
//                       " and (decdeg between %f and %f)",
//                       image_iter->second.min_RA,image_iter->second.max_RA,
//                       image_iter->second.min_Dec,image_iter->second.max_Dec);

//           stmt=db->Statement(db_cmd.c_str());
        
//           // Check for errors in the statement
//           if(!stmt->Process()) {
//             cout<<stmt->GetErrorMsg()<<endl;
//             exit(1);
//           }
//           stmt->StoreResult();
        
        
//           double ra,dec;
//           double g_mag,r_mag,i_mag,z_mag;
//           double g_magerr,r_magerr,i_magerr,z_magerr;
//           while(stmt->NextResultRow()) {
//             ra=stmt->GetDouble(0);       
//             dec=stmt->GetDouble(1);       
//             g_mag=stmt->GetDouble(2);       
//             r_mag=stmt->GetDouble(3);       
//             i_mag=stmt->GetDouble(4);       
//             z_mag=stmt->GetDouble(5);       
//             g_magerr=stmt->GetDouble(6);       
//             r_magerr=stmt->GetDouble(7);       
//             i_magerr=stmt->GetDouble(8);       
//             z_magerr=stmt->GetDouble(9);       

          
//             double mag,magerr;
          
//             if(im_band=="g") {
//               mag=g_mag;
//               magerr=g_magerr;
//             }
//             else if(im_band=="r") {
//               mag=r_mag;
//               magerr=r_magerr;
//             }
//             else if(im_band=="i") {
//               mag=i_mag;
//               magerr=i_magerr;
//             }
//             else if(im_band=="z") {
//               mag=z_mag;
//               magerr=z_magerr;
//             }
          
//             Object obj(ra,dec,mag,magerr,im_band,0,0);
//             double cd=cos(obj.Dec*TMath::DegToRad());
//             obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
//             obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
//             obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());
          
//             stdstar_tree.insert(obj);
//           }

//           if(debug)cout<<"Found "<<stdstar_tree.size()<<" standard stars"
//                        <<" in image "<<image_iter->first<<" range"<<endl;
        
        
//           vector<Object>::iterator obj_iter=ImageObjs[image_iter->first].begin();
        
//           // Loop through objects and try to match
//           for(; obj_iter!=ImageObjs[image_iter->first].end(); ++obj_iter) {
          
//             // find closest match within tolerance
//             KD_Match match=stdstar_tree.find_nearest(*obj_iter,tol);
          
//             if(match.first!=stdstar_tree.end()) {
            
//               ObjPair pa;
//               pa.obj1=*match.first;
//               pa.obj2=*obj_iter;
//               stdstar_pairs.push_back(pa);
//             }
//           }
        

//           if(!IsPhotometric) {
//             obj_iter=ImageObjsPhoto[image_iter->first].begin();
          
//             // Loop through objects and try to match
//             for(; obj_iter!=ImageObjsPhoto[image_iter->first].end(); ++obj_iter) {
          
//               // find closest match within tolerance
//               KD_Match match=stdstar_tree.find_nearest(*obj_iter,tol);
            
//               if(match.first!=stdstar_tree.end()) {
              
//                 ObjPair pa;
//                 pa.obj1=*match.first;
//                 pa.obj2=*obj_iter;
//                 stdstar_pairs_photo.push_back(pa);
//               }
//             }
          
          
//           }

//           delete stmt;
//           db->Close();

//         }
//         cout<<"Found "<<stdstar_pairs.size()<<"  standard star matches"<<endl;    

//         if(stdstar_pairs.size()>0) {
//           string std_title=title+"Standard Stars";

//           if(IsPhotometric) {
//             plot_phot_all(stdstar_pairs,pad,can_counter,
//                           std_title,dir,band,false,"std",mag,
//                           star_phot_mean,star_phot_sigma);
//           }
//           else {
//             plot_phot_all(stdstar_pairs_photo,pad,can_counter,
//                           std_title,dir,band,false,"std",mag,
//                           star_phot_mean,star_phot_sigma);
//           }
//           plot_astro(stdstar_pairs,pad,can_counter,
//                      std_title,dir,band,debug,
//                      star_ra_mean,star_ra_sigmal,
//                      star_ra_sigmar,
//                      star_dec_mean,star_dec_sigmal,
//                      star_dec_sigmar
//                      );
//           found_stars=true;
//           //plot_phot(stdstar_pairs,std_title,dir,band,mag,debug,sigma,N_bin);
//         }
//         else {
//           cout<<"Not doing standard star comparison"<<endl;
//           can_counter+=3;
//           star_phot_mean=-999;
//           star_phot_sigma=-999;
//           star_ra_mean=-999;
//           star_ra_sigmal=-999;
//           star_ra_sigmar=-999;
//           star_dec_mean=-999;
//           star_dec_sigmal=-999;
//           star_dec_sigmar=-999;
//         }
      
     
//       }
    



//       // Comparison with standard galaxies
//       bool found_gals=false;
//       if(standard_gals) {
      
      
//         vector<ObjPair> stdgal_pairs,stdgal_pairs_photo;

//         // loop over all images and look for std galaxy matches
//         //map<int,ImageInfo>::iterator 
//         image_iter=ImageList.begin();
      
//         for(; image_iter!=ImageList.end(); image_iter++) {
        
        
        
//           TString exp=image_iter->second.exp_name;
//           string im_band=image_iter->second.band;
      
//           // ignore science pointings
//           if(exp.BeginsWith(project.c_str())) continue;

//           db = TSQLServer::Connect("oracle://desdb/stdes",
//                                    "pipeline", "dc01user");  
        
//           KD_Tree stdgal_tree;

//           db_cmd=Form("select ra,dec,%s_g,%s_r,%s_i,%s_z,"
//                       " %serr_g,%serr_r,%serr_i,%serr_z "
//                       " FROM galaxy_standards "
//                       " where (ra between %f and %f) "
//                       " and (dec between %f and %f)",
//                       true_mag.c_str(),true_mag.c_str(),
//                       true_mag.c_str(),true_mag.c_str(),
//                       true_mag.c_str(),
//                       true_mag.c_str(),true_mag.c_str(),
//                       true_mag.c_str(),true_mag.c_str(),
//                       true_mag.c_str(),
//                       image_iter->second.min_RA,image_iter->second.max_RA,
//                       image_iter->second.min_Dec,image_iter->second.max_Dec);
        
//           stmt=db->Statement(db_cmd.c_str());
        
//           // Check for errors in the statement
//           if(!stmt->Process()) {
//             cout<<stmt->GetErrorMsg()<<endl;
//             exit(1);
//           }
//           stmt->StoreResult();
        
        
//           double ra,dec;
//           double g_mag,r_mag,i_mag,z_mag;
//           double g_magerr,r_magerr,i_magerr,z_magerr;
//           while(stmt->NextResultRow()) {
//             ra=stmt->GetDouble(0);       
//             dec=stmt->GetDouble(1);       
//             g_mag=stmt->GetDouble(2);       
//             r_mag=stmt->GetDouble(3);       
//             i_mag=stmt->GetDouble(4);       
//             z_mag=stmt->GetDouble(5);       
//             g_magerr=stmt->GetDouble(6);       
//             r_magerr=stmt->GetDouble(7);       
//             i_magerr=stmt->GetDouble(8);       
//             z_magerr=stmt->GetDouble(9);       

          
//             double mag,magerr;
          
//             if(im_band=="g") {
//               mag=g_mag;
//               magerr=g_magerr;
//             }
//             else if(im_band=="r") {
//               mag=r_mag;
//               magerr=r_magerr;
//             }
//             else if(im_band=="i") {
//               mag=i_mag;
//               magerr=i_magerr;
//             }
//             else if(im_band=="z") {
//               mag=z_mag;
//               magerr=z_magerr;
//             }
          
//             Object obj(ra,dec,mag,magerr,im_band,0,0);
//             double cd=cos(obj.Dec*TMath::DegToRad());
//             obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
//             obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
//             obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());
          
//             stdgal_tree.insert(obj);
//           }

//           if(debug)cout<<"Found "<<stdgal_tree.size()<<" standard galaxies"
//                        <<" in image "<<image_iter->first<<" range "<<endl;
        
//           vector<Object>::iterator obj_iter=ImageObjs[image_iter->first].begin();
        
//           // Loop through objects and try to match
//           for(; obj_iter!=ImageObjs[image_iter->first].end(); ++obj_iter) {
          
//             // find closest match within tolerance
//             KD_Match match=stdgal_tree.find_nearest(*obj_iter,tol);
          
//             if(match.first!=stdgal_tree.end()) {
            
//               ObjPair pa;
//               pa.obj1=*match.first;
//               pa.obj2=*obj_iter;
//               stdgal_pairs.push_back(pa);
//             }
          
//           }
        

//           if(!IsPhotometric) {
//             obj_iter=ImageObjsPhoto[image_iter->first].begin();
          
//             // Loop through objects and try to match
//             for(; obj_iter!=ImageObjsPhoto[image_iter->first].end(); ++obj_iter) {
          
//               // find closest match within tolerance
//               KD_Match match=stdgal_tree.find_nearest(*obj_iter,tol);
            
//               if(match.first!=stdgal_tree.end()) {
              
//                 ObjPair pa;
//                 pa.obj1=*match.first;
//                 pa.obj2=*obj_iter;
//                 stdgal_pairs_photo.push_back(pa);
//               }
//             }
//           }

//           delete stmt;
//           db->Close();
          
//         }
      
//         cout<<"Found "<<stdgal_pairs.size()
//             <<"  standard galaxy matches"<<endl;    

//         if(stdgal_pairs.size()>0) {
//           string std_title=title+"Standard Galaxies";

//           if(IsPhotometric) {
//             plot_phot_all(stdgal_pairs,pad,can_counter,
//                           std_title,dir,band,debug,true_mag,mag,
//                           gal_phot_mean,gal_phot_sigma);
//           }
//           else {
//             plot_phot_all(stdgal_pairs_photo,pad,can_counter,
//                           std_title,dir,band,debug,true_mag,mag,
//                           gal_phot_mean,gal_phot_sigma);
//           }
//           plot_astro(stdgal_pairs,pad,can_counter,
//                      std_title,dir,band,debug,
//                      gal_ra_mean,gal_ra_sigmal,
//                      gal_ra_sigmar,
//                      gal_dec_mean,gal_dec_sigmal,
//                      gal_dec_sigmar
//                      );
//           found_gals=true;

//         }
//         else {
//           cout<<"Not doing standard galaxy comparison"<<endl;
//           can_counter+=3;
//           gal_phot_mean=-999;
//           gal_phot_sigma=-999;
//           gal_ra_mean=-999;
//           gal_ra_sigmal=-999;
//           gal_ra_sigmar=-999;
//           gal_dec_mean=-999;
//           gal_dec_sigmal=-999;
//           gal_dec_sigmar=-999;
//         }
      
     
//       }
    
    
//       canvas->cd();
//       TLatex *nite_label=new TLatex;
//       nite_label->SetNDC();
//       nite_label->SetTextSize(0.037);
//       nite_label->SetTextFont(22);
//       nite_label->SetTextAlign(22);
//       string nite_string=project+" Night: "+nite.substr(4,2)+"/"+
//         nite.substr(6,2)+"/"+nite.substr(0,4)+"    "
//         +"Band: "+band;
//       if(IsPhotometric) nite_string+="   Photometric: yes";
//       else nite_string+="   Photometric: no";
    
//       nite_label->DrawLatex(0.528,0.972,nite_string.c_str());
    
//       canvas->WaitPrimitive();
//       string print_string=dir+"/"+nite+"_"+band+".png";
    
//       if(obj_pairs.size()>0 || found_usnob || found_stars || found_gals) {
//         canvas->Print(print_string.c_str());
//       }
//       delete pad;
//       delete canvas;
    
//       tree->Fill();
     }
      
    
     }
    

//   TFile tfile("bcs_nite_out.root","recreate");
//   tree->Write();
//   tfile.Close();
    

} 
// Set style

void LoadStyle()
{


  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  TStyle* miStyle = new  TStyle("miStyle", "MI Style");

  // Colors


  //  set the background color to white
  miStyle->SetFillColor(10);


  miStyle->SetFrameFillColor(10);
  miStyle->SetCanvasColor(10);
  miStyle->SetCanvasDefH(680);
  miStyle->SetCanvasDefW(700);
  miStyle->SetPadColor(10);
  miStyle->SetTitleFillColor(0);
  miStyle->SetStatColor(10);

  //  //dont put a colored frame around the plots
  miStyle->SetFrameBorderMode(0);
  miStyle->SetCanvasBorderMode(0);
  miStyle->SetPadBorderMode(0);

  //use the primary color palette
  miStyle->SetPalette(1);

  //set the default line color for a histogram to be black
  miStyle->SetHistLineColor(kBlack);

  //set the default line color for a fit function to be red
  miStyle->SetFuncColor(kBlue);

  //make the axis labels black
  miStyle->SetLabelColor(kBlack,"xyz");

  //set the default title color to be black
  miStyle->SetTitleColor(kBlack);

  // Sizes
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);


  //set the margins
  miStyle->SetPadBottomMargin(0.16);
  miStyle->SetPadTopMargin(0.1);
  miStyle->SetPadLeftMargin(0.14);
  miStyle->SetPadRightMargin(0.06);

  //set axis label and title text sizes
  miStyle->SetLabelSize(0.04,"x");
  miStyle->SetLabelSize(0.04,"y");
  miStyle->SetTitleSize(0.05,"xyz");
  miStyle->SetTitleOffset(1.1,"x");
  miStyle->SetTitleOffset(1.3,"yz");
  miStyle->SetLabelOffset(0.012,"y");
  miStyle->SetStatFontSize(0.025);
  miStyle->SetTextSize(0.02);
  miStyle->SetTitleBorderSize(0);

  //set line widths
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);

  // Misc

  //align the titles to be centered
  miStyle->SetTextAlign(22);

  //set the number of divisions to show
  miStyle->SetNdivisions(506, "xy");

  //turn off xy grids
  miStyle->SetPadGridX(0);
  miStyle->SetPadGridY(0);

  //set the tick mark style
  miStyle->SetPadTickX(1);
  miStyle->SetPadTickY(1);

  //show the fit parameters in a box
  miStyle->SetOptFit(0111);
  miStyle->SetOptTitle(0);

  //turn off all other stats
  miStyle->SetOptStat(0);
  miStyle->SetStatW(0.20);
  miStyle->SetStatH(0.15);
  miStyle->SetStatX(0.94);
  miStyle->SetStatY(0.92);


  miStyle->SetFillStyle(0);

  //  // Fonts
  miStyle->SetStatFont(42);
  miStyle->SetLabelFont(42,"xyz");
  miStyle->SetTitleFont(42,"xyz");
  // miStyle->SetTextFont(40);

  //done

  miStyle->cd();


  // gROOT->ForceStyle(1);
 

}

void PrintHelp()
{
  cout<<"Usage: slr_check -tile (tile) (options)\n";
  cout<<"Options (default):\n";
  cout<<"  -project    (none)       For looping over all tiles in BCS/DES.\n";
  cout<<"  -run        (latest)     Use a specific run.   \n";
  cout<<"  -color      (no)         Apply BCS color corrections\n";
  cout<<"  -mag        (auto)       Which magnitude to use\n";
  cout<<"  -star_cut   (0.7)        Value of class_star to determine stars\n";
  cout<<"  -err_cut    (0.2)        Maximum flux error\n";
  cout<<"  -outfile    (out.root)   Output file (only if using -project)\n";
  cout<<"  -extinction (no)         Apply extinction correction\n";
  cout<<"  -image_name ((tile).png) Name of output\n";
  cout<<"  -image_dir  (./)         Name of directory for output\n";
}


int plot_phot_all(const vector<ObjPair> &obj_pairs,
                  TPad *can,int &cur_can,
                  string title,string dir,string band,
                  bool debug,string true_label,string mag_label,
                  double &mean,double &sigma)
{
  
  
  // Some formatting options
  gStyle->SetOptStat(1110);
  gStyle->SetOptFit(0010);
  gStyle->SetStatFormat("4.2g"); 
  gStyle->SetFitFormat("4.2g"); 
  //gStyle->SetTitleOffset(1.6,"y");


  vector<double> mag1,mag2,diff_mag,
    mag1_err,mag2_err;
  
  TCanvas *c1=new TCanvas("c1","");
  c1->SetLeftMargin(0.16);  
  
  vector<ObjPair>::const_iterator obj_iter=obj_pairs.begin();
  
  int count=0;
  for(; obj_iter!=obj_pairs.end(); ++obj_iter) {
    
    mag1.push_back(obj_iter->obj1.mag);
    mag2.push_back(obj_iter->obj2.mag);
    mag1_err.push_back(obj_iter->obj1.mag_err);
    mag2_err.push_back(obj_iter->obj2.mag_err);
    diff_mag.push_back(mag1[count]-mag2[count]);
    count++;
  }

  double mag_mean;//=TMath::Mean(diff_mag.size(),&diff_mag[0]);
  double mag_rms;//=TMath::RMS(diff_mag.size(),&diff_mag[0]);

  vector<double> dum(diff_mag.size());
  int good,bad;
  sigma_clip(3,2.5,diff_mag,dum,mag_mean,
             good,bad,mag_rms,true,false,1e-6);
  // Fill histogram
  double max_limit=mag_mean+10*mag_rms,min_limit=mag_mean-10*mag_rms;

  TH1D *Mag_diff=new TH1D("Mag_diff",
                          Form(";%s_{%s} - %s_{%s};Entries",
                               band.c_str(),mag_label.c_str(),
                               band.c_str(),true_label.c_str()),
                          50,min_limit,max_limit);
  Mag_diff->SetDirectory(0);
  int total=diff_mag.size();
  for(int i=0;i<diff_mag.size();++i) {
    Mag_diff->Fill(diff_mag[i]);
  }
  Mag_diff->GetXaxis()->CenterTitle();
  Mag_diff->GetYaxis()->CenterTitle();
  
  // fit to a bifurcated gaussian
  TF1 *func = new TF1("func","gaus",min_limit,max_limit);
  func->SetNpx(100000);
  func->SetParameters(100,0.0,0.1);
  func->SetParNames("Constant","Mean Fit","#sigma");
  //func->SetParLimits(2,0,10);
  //func->SetParLimits(3,0,10);
  Mag_diff->Fit(func,"RQ");
  Mag_diff->Draw();
  c1->Update();
    
  double gaus_const=func->GetParameter(0);
  double gaus_mean=func->GetParameter(1);
  double gaus_sigma=func->GetParameter(2);
    
  double cut=3*gaus_sigma;
  double min=gaus_mean-cut;
  double max=gaus_mean+cut;
  mean=gaus_mean;
  sigma=gaus_sigma;
  
  // 3sigma cut
    
  int inside=0;
  for(int i=0;i<diff_mag.size();++i) {
    if(diff_mag[i]<max && diff_mag[i]>min) inside++;
  }
    
  // calculate the outlier fraciton
  double outside=1-(1.*inside/total);


    
  // Find systematic error
  vector<double> fit_diff_mag,fit_mag1_err,fit_mag2_err;
    
  for(int i=0;i<diff_mag.size();i++) {
    if(diff_mag[i]>min && diff_mag[i]<max) {
      fit_diff_mag.push_back(diff_mag[i]);
      fit_mag1_err.push_back(mag1_err[i]);
      fit_mag2_err.push_back(mag2_err[i]);
    }
  }

  SysErrFit Mag_fit;
  double Mag_min_err;
  double Mag_min=Mag_fit.Fit(&fit_diff_mag,&fit_mag1_err,
                             Mag_min_err,&fit_mag2_err);
  if(debug)cout<<"Mag Minimization Minimum Found at = "<<Mag_min<<endl;


  Mag_diff->Draw();
    
  // Add text to statbox
  TPaveStats *ps_Mag = (TPaveStats*)c1->GetPrimitive("stats");
  ps_Mag->SetName("mystats_Mag");
  ps_Mag->SetX1NDC(0.675) ; 
  ps_Mag->SetX2NDC(0.976) ; 
  ps_Mag->SetY1NDC(0.334) ; 
  ps_Mag->SetY2NDC(0.871);
  TList *list_Mag = ps_Mag->GetListOfLines();
  TText *tconst_Mag = ps_Mag->GetLineWith("Constant"); 

  if(tconst_Mag) {
    TLatex *myt_Magsys = new 
      TLatex(0,0,Form("#sigma_{sys}= %2.2g",Mag_min));
    myt_Magsys->SetTextFont(tconst_Mag->GetTextFont());
    myt_Magsys->SetTextSize(tconst_Mag->GetTextSize());
    myt_Magsys->SetTextColor(2);
    list_Mag->Add(myt_Magsys);
      
    TLatex *myt_Mag = new 
      TLatex(0,0,Form("Frac > 3#sigma = %0.2f",outside));
    if(myt_Mag) {
      myt_Mag->SetTextFont(tconst_Mag->GetTextFont());
      myt_Mag->SetTextSize(tconst_Mag->GetTextSize());
      myt_Mag->SetTextColor(2);
      list_Mag->Add(myt_Mag);
    }
      
    list_Mag->Remove(tconst_Mag); 

  }
    
  ps_Mag->SetTextSize(0.053);
  ps_Mag->SetBorderSize(0);
  ps_Mag->SetTextColor(2);
  Mag_diff->SetStats(0);
  ps_Mag->Draw();
    
    
    
  // Draw top label
  TLatex *label=new TLatex;
  label->SetNDC();
  label->SetTextSize(0.1);
  label->SetTextFont(22);
  label->SetTextAlign(22);
  label->SetTextColor(kBlue);
  string Mag_label;
  Mag_label=title;
  label->DrawLatex(0.49,0.948,Mag_label.c_str());
    
  if(debug) {
    c1->Update();
    c1->WaitPrimitive();
  }
  // print out plot
  string id=title+"_"+band+"_Mag";
  string print=dir+"/"+id+".png";
  //c1->Print(print.c_str());
    
  string convert="convert "+id+".eps "+id+".png";
  //gSystem->Exec(convert.c_str());
    
  convert="rm "+print;
  //gSystem->Exec(convert.c_str());
  can->cd(cur_can);
  TObject *obj=c1->DrawClonePad();
  //Mag_diff->Draw();
  cur_can++;
  delete obj;
  delete label;
  delete c1;
  delete func;
  delete Mag_diff;
    
  return 1;
}

void plot_phot(const vector<ObjPair> &obj_pairs,
               string title,string dir,
	       string band,string mag,
               string x_label,string y_label,
               bool debug,bool rm_zpdiff,
	       double sigma,int N_bin)
{
  int min_entries=50; //minimum entries per bin
  int func_min=0;    // min function limit
  int func_max=1;   // max function limit
  double hist_fit_max; // max for histogram fitting the gaussian
  int func_fit=3; // this*rms is max for function fitting the gaussian
 
  // use Minuit2 to do the fitting
  // I found that it did better in some cases
  TVirtualFitter::SetDefaultFitter("Minuit2" );

   
  TCanvas *c1=new TCanvas("c1","");

  
  
  string cur_title=band+" band "+title+" ";
  	
  hist_fit_max=2;
  
  int N_entries = obj_pairs.size();

  
  // Sort tree into index according to mean mag
  vector<double>  mean_mag;

  vector<ObjPair>::const_iterator obj_iter=obj_pairs.begin();
  
  for(; obj_iter!=obj_pairs.end(); ++obj_iter) {
    double mean=(obj_iter->obj1.mag+
                 obj_iter->obj2.mag)/2;
    mean_mag.push_back(mean);
  }

  vector<double> ordered_mean_mag(mean_mag.size());


  int *index = new int[N_entries];
  TMath::Sort(N_entries,&mean_mag[0],index,false);

  // Fill ordered vectors
  vector<ObjPair> ordered_pair(N_entries);
  
  for(int i=0;i<N_entries;i++) {
    ordered_pair[i]=obj_pairs[index[i]];
    ordered_mean_mag[i]=mean_mag[index[i]];
  }
   
  int desired_bins=8;
  if(1.*N_entries/N_bin> desired_bins) {
    N_bin=(int)N_entries/desired_bins*1.;
  }
  
  TGraph *sigma_scatter=new TGraph;  
  TGraph *sigma_gr_scatter=new TGraph;  
  vector<double> bins,sys_error,tot_error,mean_error,mean_vec,frac_passed;
  int global_counter=0,sigma_counter=0,sigma_gr_counter=0;

  int bin=0;

  TFile tfile(Form("debug_%s_%s.root",title.c_str(),band.c_str()),
              "recreate");


  TTree *tree=new TTree("tree","");
  Object *ob1=new Object;
  Object *ob2=new Object;
  bool in;
  double zp1,zp2;
  char id1[50],id2[50];
  char exp1[100],exp2[100];
  char nite1[100],nite2[100];
  double m_mag,mag_diff;
  
  tree->Branch("ob1",&ob1);
  tree->Branch("ob2",&ob2);
  tree->Branch("in",&in,"in/B");
  tree->Branch("zp1",&zp1,"zp1/D");
  tree->Branch("zp2",&zp2,"zp2/D");
  tree->Branch("id1",&id1,"id1/C");
  tree->Branch("id2",&id2,"id2/C");
  tree->Branch("exp1",&exp1,"exp1/C");
  tree->Branch("exp2",&exp2,"exp2/C");
  tree->Branch("nite1",&nite1,"nite1/C");
  tree->Branch("nite2",&nite2,"nite2/C");
  tree->Branch("mean_mag",&m_mag,"m_mag/D");
  tree->Branch("mag_diff",&mag_diff,"mag_diff/D");
  
  


  vector<double> full_diff,full_mean;
  // Loop until the end of the vectors have been reached
  bool finished=false;
  while(!finished) {

    if(debug) cout<<"Bin "<<bin+1<<endl;
    TGraph scatter,scatter_clipped;

    
    // create sub list of vectors with either N_bin entries or
    // the number in a mag difference of 1
    vector<ObjPair> sub_pair(N_bin),cut_pair(N_bin);
     vector<double> sub_obs_mag,sub_true_mag,sub_diff,sub_err,sub_true_err;
     
    int current_min=global_counter;
    for(int entry=0; entry<N_bin ; entry++) {
      
      // check if at the end of the vector;
      if(global_counter  >= (N_entries-1)) {
        finished=true;
        break;
      }
      
      // check if the mag difference is greater than 1 between the
      // current and first of the bin
      if(entry>2) {
        if(ordered_mean_mag[global_counter]-ordered_mean_mag[current_min] >1.) {
          global_counter++;
          break;
        }
      }
      
      scatter.SetPoint(entry,ordered_mean_mag[global_counter],
                       ordered_pair[global_counter].obj1.mag-
                       ordered_pair[global_counter].obj2.mag);
      //      cout<<entry<<endl;
      sub_pair[entry]=ordered_pair[global_counter];
      sub_pair[entry]=ordered_pair[global_counter];
      if(rm_zpdiff) {
        sub_diff.insert(sub_diff.end(),
                        ordered_pair[global_counter].obj1.mag-
                        ordered_pair[global_counter].obj2.mag-
                        ordered_pair[global_counter].zp_diff);
      }
      else {
        sub_diff.insert(sub_diff.end(),
                        ordered_pair[global_counter].obj1.mag-
                        ordered_pair[global_counter].obj2.mag);
                        
      }
      global_counter++;
    }
    
    
    if(debug) cout<<"Subset Size "<<sub_pair.size()<<endl;
    
    // Make sure there at least min_entries
    if(sub_pair.size()<min_entries) {
      cout<<"Not enough entries.. Skipping this bin"<<endl;
      continue;
    }

    int N_actual=global_counter-current_min;

    sub_pair.resize(N_actual);

    vector<double> dum(sub_diff.size());
    int good,bad;
    double mean,rms;
    sigma_clip(3,2.5,sub_diff,dum,mean,
               good,bad,rms,true,false,1e-6);
    double hist_max=mean+10*rms;
    double hist_min=mean-10*rms;
    if(hist_max>hist_fit_max) hist_max=hist_fit_max;
    if(hist_min<-1*hist_fit_max) hist_min=-hist_fit_max;
    // fit to gaussian and to do  sigma clipping
    TH1D *h1=new TH1D(Form("fit%d",bin+1),"",35,
                      hist_min,hist_max);
    for(int i=0;i<sub_diff.size();i++) {
      h1->Fill(sub_diff[i]);
    }
    if(debug) cout<<"Clipped mean="<<mean<<" rms="<<rms<<endl;
    if(debug) cout<<"Hist range: "<<hist_min<<" "<<hist_max<<endl;
    if(h1->Integral()<min_entries) {
      if(debug) cout<<"Found less than the number of entries in "
                    <<"the histogram skipping entries in bin"<<endl;
      h1->SetName("");
      continue;
    }
    
    double hist_mean=h1->GetMean();
    double hist_rms=h1->GetRMS();
    if(debug) cout<<"Hist Mean "<<hist_mean<<" RMS "<<hist_rms<<endl;
    
    TF1 *f1=new TF1("f1","gaus",hist_mean-func_fit*hist_rms,
                    hist_mean+func_fit*hist_rms);
    
    f1->SetNpx(10000);
    f1->SetParameter(1,0);
    h1->Fit(f1,"QR");
    
    double gaus_const=f1->GetParameter(0);
    double gaus_mean=f1->GetParameter(1);
    double gaus_sigma=f1->GetParameter(2);
    
    // draw lines to indicate cuts
    if(debug) {
      
      TLine *hi=new TLine(gaus_mean+sigma*gaus_sigma,0,
                          gaus_mean+sigma*gaus_sigma,h1->GetMaximum());
      TLine *low=new TLine(gaus_mean-sigma*gaus_sigma,0,
                           gaus_mean-sigma*gaus_sigma,h1->GetMaximum());
      
      low->SetLineWidth(3);
      hi->SetLineWidth(3);
      hi->SetLineColor(2);
      low->SetLineColor(2);
      h1->Draw();
      hi->Draw();
      low->Draw();
    }
    
    // erase name to not have warning messages
    h1->SetName("");
    
    // cut out all values greater than sigma*gaus_sigma(from fit)
    vector<double> vec_mag1,vec_mag2,vec_diff,vec_err1,vec_err2,vec_mean;
      
    
    int clipped_counter=0;
    for(int entry=0; entry<sub_pair.size() ; entry++) {
      double diff;

      if(rm_zpdiff) {
        diff=sub_pair[entry].obj1.mag-sub_pair[entry].obj2.mag-
          sub_pair[entry].zp_diff;
      } else {
        diff=sub_pair[entry].obj1.mag-sub_pair[entry].obj2.mag;
      }
      double mean=(sub_pair[entry].obj1.mag+
                   +sub_pair[entry].obj2.mag)/2.;
      

      *ob1=sub_pair[entry].obj1;
      *ob2=sub_pair[entry].obj2;
      m_mag=mean;
      mag_diff=diff;
      zp1=sub_pair[entry].zp1;
      zp2=sub_pair[entry].zp2;
      sprintf(id1,"%d",sub_pair[entry].id1);
      sprintf(id2,"%d",sub_pair[entry].id2);
      sprintf(exp1,"%s",ImageList[sub_pair[entry].id1].exp_name.c_str());
      sprintf(exp2,"%s",ImageList[sub_pair[entry].id2].exp_name.c_str());
      sprintf(nite1,"%d",ImageList[sub_pair[entry].id1].nite);
      sprintf(nite2,"%d",ImageList[sub_pair[entry].id2].nite);


      full_diff.push_back(diff);
      full_mean.push_back(mean);
      if(fabs(diff-gaus_mean)<sigma*gaus_sigma) {
        in=1;
        vec_mag1.push_back(sub_pair[entry].obj1.mag);
        vec_mag2.push_back(sub_pair[entry].obj2.mag);
        vec_diff.push_back(diff);
        vec_err1.push_back(sub_pair[entry].obj1.mag_err);
        vec_err2.push_back(sub_pair[entry].obj2.mag_err);


        vec_mean.push_back(mean);
        cut_pair.push_back(sub_pair[entry]);
        scatter_clipped.SetPoint(clipped_counter,mean,diff);
        clipped_counter++;
        sigma_scatter->SetPoint(sigma_counter,mean,diff);
        sigma_counter++;
      }else {
        in=0;
        sigma_gr_scatter->SetPoint(sigma_gr_counter,mean,
                                   sub_diff[entry]);
        sigma_gr_counter++;
      }
      tree->Fill();
      
      
    }
    

    //h1->Write();
    
    
    // calculate how many events were cut
    
    if(debug) cout<<"Bin "<<bin+1<<" with "<<vec_diff.size()
                  <<"clipped entries"<<endl;      
    
    if(vec_diff.size()<min_entries) {
      cout<<"Not enough clipped entries in bin"<<endl;
      continue;
    }
    
    double frac=1.*vec_diff.size()/sub_pair.size();
    frac_passed.push_back(frac);
    
    if(debug) {
      cout<<"  Mag Range: "<<ordered_mean_mag[current_min]<<" to "    
          <<ordered_mean_mag[global_counter-1]<<endl;
    }
    
    scatter.SetName(Form("scatter%d",bin+1));
    //scatter.Write();
    
    scatter_clipped.SetName(Form("scatter_clipped%d",bin+1));
    //scatter_clipped.Write();
    
    
    SysErrFit fit;
    double err;
    double min=fit.Fit(&vec_diff,&vec_err1,err,&vec_err2);
    if(debug)cout<<"Minimization Minimum Found at = "<<abs(min)<<endl;
    if(min>gaus_sigma) {
      min=gaus_sigma;
      if(debug) {
        cout<<"Systematic error larger than stat."<<endl;
        cout<<"Setting it to the stat. error"<<endl;
      }
    }
      
    // Look at what the fitting is doing
    if(debug) {
      c1->Update();
      c1->WaitPrimitive();
    }
    
    
    
    // if minimization fails use 0.0 as default
    if(fabs(min)>func_max) {
      cout<<"Error: root finder failed"<<endl;
      min=0.0;
    };
    
    // set variables
    sys_error.push_back(min);
    tot_error.push_back(gaus_sigma);
    mean_error.push_back(hist_rms/sqrt(h1->Integral()));
    mean_vec.push_back(gaus_mean);
    
    
    // get the last of the observed magnitude for binning
    if(bin==0) bins.insert(bins.end(),ordered_mean_mag[current_min]);
    bins.insert(bins.end(),ordered_mean_mag[global_counter-1]);
    bin++;
    
  }
  if(bins.size()==0) return;
  // remove duplicate entries
  if(debug) {
    cout<<"Binning :"<<endl;
    for(int i=0;i<bins.size();i++) cout<<bins[i]<<" ";
    cout<<endl;
    cout<<"Finished Fitting"<<endl;
  }
  tree->Write();
  tfile.Close();
  TH1D *hist_sys_err=new TH1D("hist_sys_err",
                              ";mag_{mean};Systematic Error / 1000 events",
                              bins.size()-1,&bins[0]);
  TH1D *hist_tot_err=new TH1D("hist_tot_err",
                              ";mag_{mean};Systematic Error / 1000 events",
                              bins.size()-1,&bins[0]);
  TH1D *hist_mean_err=new TH1D("hist_mean_err",
                               ";mag_{true};Systematic Error / 1000 events",
                               bins.size()-1,&bins[0]);
  TH1D *hist_frac=new TH1D("hist_frac",
                           Form(";%s_{true};Fraction within 3#sigma;",
                                band.c_str()),
                           bins.size()-1,&bins[0]);

  double hist_min=1e6,hist_max=-1e6;

  for(int i=0;i<sys_error.size();i++) {
    hist_sys_err->SetBinContent(i+1,mean_vec[i]);
    hist_sys_err->SetBinError(i+1,sys_error[i]);
    
    hist_tot_err->SetBinContent(i+1,mean_vec[i]);
    hist_tot_err->SetBinError(i+1,tot_error[i]);
    
    hist_mean_err->SetBinContent(i+1,mean_vec[i]);
    hist_mean_err->SetBinError(i+1,mean_error[i]);
    
    hist_frac->SetBinContent(i+1,frac_passed[i]);
    
    if(mean_vec[i]+TMath::Max(tot_error[i],sys_error[i])>hist_max){
      hist_max=mean_vec[i]+TMath::Max(tot_error[i],sys_error[i]);
    }
    if(mean_vec[i]-TMath::Max(tot_error[i],sys_error[i])<hist_min){
      hist_min=mean_vec[i]-TMath::Max(tot_error[i],sys_error[i]);
    }
  }

  
  if(hist_max>2) hist_max=2;
  if(hist_min<-2) hist_min=-2;
  
  TLatex *label=new TLatex;
  label->SetNDC();
  label->SetTextSize(0.1);
  label->SetTextColor(kBlue);
  label->SetTextFont(22);
  label->SetTextAlign(22);
  
  // 	c1->SetLeftMargin(0.17);
  // 	hist_frac->GetYaxis()->CenterTitle();
  // 	hist_frac->GetYaxis()->SetRangeUser(0,1);
  // 	hist_frac->SetTitleOffset(1.6,"y");
  // 	hist_frac->GetXaxis()->CenterTitle();
  // 	hist_frac->Draw();
  // 	label->DrawLatex(0.49,0.948,cur_title.c_str());
  // 	c1->Update();
  // 	if(debug) c1->WaitPrimitive();
  
  string id=band+"_frac";
  string print=dir+"/"+id+".png";
  // 	if(debug)c1->WaitPrimitive();
  //c1->Print(print.c_str());
  // 	//hist_sys_err->Write();
  // 	//hist_frac->Write();
  label->SetTextSize(0.06);
  
  TCanvas *c2 = new TCanvas("c2","",0,0,772,802);
  TPad *pad1 = new TPad("pad1"," ",0.0,0.37,1.0,1,0);
  TPad *pad2 = new TPad("pad2"," ",0.0,0.0,1.0,0.37,0);
  
  pad1->SetBottomMargin(0.015);
  pad1->SetLeftMargin(0.15);
  pad2->SetLeftMargin(0.15);
  pad2->SetTopMargin(0.015);
  pad2->SetBottomMargin(0.3);
  pad1->SetTicks(1,0);
  pad1->SetTicky(0);
  pad1->Draw();
  pad2->Draw();
  pad1->cd();
  hist_tot_err->SetLabelOffset(1,"x");
  hist_tot_err->SetLabelSize(0.06,"y");
  
  
  hist_tot_err->GetYaxis()->SetRangeUser(hist_min-0.1,hist_max+0.1);
  hist_tot_err->SetYTitle("");
  
  hist_mean_err->SetLineColor(2);
  hist_sys_err->SetLineColor(4);
  
  hist_mean_err->SetFillColor(1);
  hist_sys_err->SetFillColor(2);
  hist_tot_err->SetFillColor(4);
  
  hist_mean_err->SetLineWidth(3);
  hist_sys_err->SetLineWidth(3);
  hist_tot_err->SetLineWidth(3);
  
  
  hist_tot_err->Draw("e1");
  hist_sys_err->Draw("e1,same");  
  hist_mean_err->Draw("e1,same");
  TF1 *f3=new TF1("f3","0",0,100);
  f3->SetLineColor(15);
  f3->SetLineStyle(2);
  f3->Draw("same");
  
  label->DrawLatex(0.49,0.948,cur_title.c_str());
  
  TLatex *ty1=new TLatex;
  ty1->SetNDC();
  ty1->SetTextSize(0.07);
  ty1->SetTextFont(42);
  ty1->SetTextAngle(90);
  ty1->SetTextAlign(22);
  if(rm_zpdiff) {
    ty1->DrawLatex(0.031,0.5,Form("< #Delta %s_{%s} - zp_{diff}>",band.c_str(),
                                  mag.c_str()));
  }
  else {
    ty1->DrawLatex(0.031,0.5,Form("< #Delta %s_{%s} >",band.c_str(),
                                  mag.c_str()));
  }
  
  TLegend *leg=new TLegend(0.18,0.04,0.46,0.25);
  leg->SetTextFont(42);
  leg->SetTextSize(0.06);
  leg->SetBorderSize(0);
  leg->AddEntry(hist_mean_err,"#sigma_{mean}","l");
  leg->AddEntry(hist_sys_err,"#sigma_{sys}","l");
  leg->AddEntry(hist_tot_err,"#sigma_{tot}","l");
  TLegendEntry *leg_entry=leg->AddEntry(hist_frac,
                                        "Fraction < 3#sigma_{tot}","l");
  leg_entry->SetTextSize(0.045);
  leg->Draw();
  bool draw_text=true;
   if(draw_text) {
        // Draw numbers
        TLatex *txt=new TLatex;
        txt->SetTextSize(0.025);
        for(int i=0;i<hist_mean_err->GetNbinsX();i++) {
          double x=hist_mean_err->GetXaxis()->GetBinCenter(i+1);
          double y=hist_tot_err->GetBinContent(i+1)+
            TMath::Max(hist_tot_err->GetBinError(i+1),
                       hist_sys_err->GetBinError(i+1));

          double sep=0.02*(1.1*(hist_max-hist_min)/0.42);
          if(y+2*sep<hist_max+0.1) {
            txt->SetTextColor(kRed);
            txt->DrawLatex(x,y+sep,
                           Form("%0.2f",hist_mean_err->GetBinContent(i+1)*1000));
            txt->SetTextColor(kBlue);
            txt->DrawLatex(x,y+2*sep,
                           Form("%0.1f",hist_sys_err->GetBinError(i+1)*1000));
            txt->SetTextColor(kBlack);
            txt->DrawLatex(x,y+3*sep,
                           Form("%0.1f",hist_tot_err->GetBinError(i+1)*1000));
          }
          
        }
   }       
  // draw fraction of values outside 3sigma
  pad1->Update();
  
  Float_t rightmax = 1;
  Float_t scale = pad1->GetUymax()/rightmax;
  hist_frac->SetLineColor(kMagenta);
  hist_frac->SetLineStyle(1);
  hist_frac->SetLineWidth(2);
  hist_frac->Scale(scale);
  hist_frac->Draw("same][");
  
  //draw an axis on the right side
  TGaxis *axis = new TGaxis(pad1->GetUxmax(),pad1->GetUymin(),
                            pad1->GetUxmax(), pad1->GetUymax(),
                            0.0001,rightmax,508,"+L");
  axis->SetLineColor(kMagenta);
  axis->SetLabelSize(0.04);
  axis->SetLabelOffset(0.009);
  axis->SetLabelFont(42);
  axis->SetLabelColor(kMagenta);
  axis->Draw();
  // 	global_file->cd();
  // 	hist_tot_err->Write();
  // 	hist_frac->Write();
  // 	global_file->Close();
  
  pad2->cd();
  // TGraph *full_scatter=new TGraph;
//   for(int i=0;i<full_diff.size();i++) {
//     full_scatter->SetPoint(i,full_mean[i],full_diff[i]);
//   }

  int max_scatter=5000;
  TGraph *full_scatter=new TGraph;
  TGraph *sample_all=new TGraph;
  TGraph *sample_sub=new TGraph;
  TGraph *sample_gr=new TGraph;
  
  

  double frac_accept=1;
  if(sigma_scatter->GetN()>max_scatter) {
    frac_accept=max_scatter*1.0/full_diff.size();
  }
  
  TRandom3 rand;
  for(int i=0;i<full_diff.size();i++) {
    full_scatter->SetPoint(i,full_mean[i],full_diff[i]);
    if(rand.Uniform()<frac_accept) {
      sample_all->SetPoint(i,full_mean[i],full_diff[i]);
    }
  }

        
  for(int i=0;i<sigma_scatter->GetN();i++) {
    double x,y;
    sigma_scatter->GetPoint(i,x,y);
    if(rand.Uniform()<frac_accept) {
      sample_sub->SetPoint(i,x,y);
    }
  }  
  
  for(int i=0;i<sigma_gr_scatter->GetN();i++) {
    double x,y;
    sigma_gr_scatter->GetPoint(i,x,y);
    if(rand.Uniform()<frac_accept) {
      sample_gr->SetPoint(i,x,y);
    }
  }
  

  

  vector<double> dum(full_diff.size());
  int good,bad;
  double mean,rms;
  sigma_clip(3,2.5,full_diff,dum,mean,
             good,bad,rms,true,false,1e-6);
  TH2D *h3=new TH2D("h3","",100,bins[0],
                    bins[bins.size()-1],100,-1,1);
  double max_scat=mean+7*rms;
  double min_scat=mean-7*rms;
  if(max_scat>2) max_scat=2;
  if(min_scat<-2) min_scat=-2;
  h3->GetYaxis()->SetRangeUser(min_scat,max_scat);
  
  //h3->SetNdivisions(506,"y");
  h3->Draw();
  h3->SetLabelSize(0.11,"xy");
  TF1 *f2=new TF1("f2","0",0,100);
  f2->SetLineColor(1);
  f2->Draw("same");
  sample_gr->SetMarkerColor(2);
  sample_gr->SetMarkerStyle(8);
  sample_gr->SetMarkerSize(0.3);
  sample_gr->Draw("p");
  sample_sub->SetMarkerColor(4);
  sample_sub->SetMarkerStyle(8);
  sample_sub->SetMarkerSize(0.3);
  sample_sub->Draw("p");
  
  
  TLatex *tx=new TLatex;
  tx->SetNDC();
  tx->SetTextSize(0.13);
  tx->SetTextFont(42);
  tx->SetTextAlign(22);
  tx->DrawLatex(0.55,0.079,Form("<%s_{%s}>",band.c_str(),
                                mag.c_str()));
  
  TLatex *ty2=new TLatex;
  ty2->SetNDC();
  ty2->SetTextSize(0.10);
  ty2->SetTextFont(42);
  ty2->SetTextAngle(90);
  ty2->SetTextAlign(22);
  if(rm_zpdiff) {
    ty2->DrawLatex(0.029,0.65,Form("#Delta %s_{%s}-zp_{diff}",band.c_str(),
                                   mag.c_str()));
  } else {
    ty2->DrawLatex(0.029,0.65,Form("#Delta %s_{%s}",band.c_str(),
                                   mag.c_str()));
  }

  
  
  TObject *dummy=0;
  TLegend *leg2=new TLegend(0.18,0.70,0.37,0.95);
  leg2->SetTextFont(42);
  leg2->SetTextSize(0.07);
  leg2->SetBorderSize(0);
  
  TLegendEntry *e1=leg2->AddEntry(dummy,"Total Sample","p");
  e1->SetMarkerSize(0.9);
  e1->SetMarkerStyle(20);
  e1->SetMarkerColor(2);
  
  TLegendEntry *e2=leg2->AddEntry(dummy,"Within 3#sigma","p");
  e2->SetMarkerSize(0.9);
  e2->SetMarkerStyle(20);
  e2->SetMarkerColor(4);
  leg2->Draw();
  
  
  //c2->Write("can");
  
  id=dir+"/"+title+"_"+band+"_phot";
  if(rm_zpdiff) id+="_rmzp";
  print=id+".png";
  if(debug)c2->WaitPrimitive();
  c2->Print(print.c_str());
    
  string convert="convert "+id+".eps "+id+".png";
  //gSystem->Exec(convert.c_str());
  
  convert="rm "+print;
  //gSystem->Exec(convert.c_str());
  
  pad2->cd();
  //TH2D *h2=new TH2D("h2","",100,bins[0],
  //bins[bins.size()-1],100,min-0.01,max+0.01);
  TH2D *h2=new TH2D("h2","",100,bins[0],
                    bins[bins.size()-1],100,-2,2);
  
  
  
  //h2->SetNdivisions(506,"y");
  h2->Draw();
  h2->SetLabelSize(0.11,"xy");
  TF1 *f4=new TF1("f4","0",0,100);
  f4->SetLineColor(1);
  f4->Draw("same");
  sample_gr->SetMarkerColor(2);
  sample_gr->SetMarkerStyle(4);
  sample_gr->SetMarkerSize(0.7);
  sample_gr->Draw("p");
  sample_sub->SetMarkerColor(4);
  sample_sub->SetMarkerStyle(4);
  sample_sub->SetMarkerSize(0.7);
  sample_sub->Draw("p");
  
  
  tx->DrawLatex(0.55,0.079,Form("<%s_{%s}>",band.c_str(),
                                mag.c_str()));
  ty2->DrawLatex(0.029,0.65,Form("#Delta %s_{%s}",band.c_str(),
                                 mag.c_str()));
  
  
  TLegend *leg3=new TLegend(0.18,0.36,0.375,0.62);
  leg3->SetTextFont(42);
  leg3->SetTextSize(0.07);
  leg3->SetBorderSize(0);
  
  
  e1=leg3->AddEntry(dummy,"Total Sample","p");
  e1->SetMarkerSize(0.9);
  e1->SetMarkerStyle(20);
  e1->SetMarkerColor(2);
  
  e2=leg3->AddEntry(dummy,"Within 3#sigma","p");
  e2->SetMarkerSize(0.9);
  e2->SetMarkerStyle(20);
  e2->SetMarkerColor(4);
  leg3->Draw();
  
  print=id+"_zoomout.png";
  c2->Print(print.c_str());
  
  
  convert="convert "+id+"_zoomout.eps "+id+"_zoomout.png";
  //gSystem->Exec(convert.c_str());
  
  convert="rm "+print;
  //gSystem->Exec(convert.c_str());
  
  delete f2; delete f3; delete hist_sys_err; delete hist_mean_err; delete hist_tot_err; 
  delete h2; delete h3; delete hist_frac; delete c2;delete f4;
  
  delete c1;



  







  
}






////////////////////////////////////////////////////////////////
//   Functions to plot astrometry
//
////////////////////////////////////////////////////////////////

double BifurGaus(double *x, double *par)
{
  double constant=par[0];
  double mean=par[1];
  double sigmaL=par[2];
  double sigmaR=par[3];
  

  
  double arg = x[0] - mean;
  double coef(0.0);

  if (arg < 0.0){
    if (TMath::Abs(sigmaL) > 1e-30) {
      coef = -0.5/(sigmaL*sigmaL);
    }
  } else {
    if (TMath::Abs(sigmaR) > 1e-30) {
      coef = -0.5/(sigmaR*sigmaR);
    }
  }

  return constant*exp(coef*arg*arg);
}



int plot_astro(const vector<ObjPair> &obj_pairs,
               TPad *can,int &cur_can,
               string title,string dir,string band,
               bool debug,
               double &mean_ra,double &sigmal_ra,double &sigmar_ra,
               double &mean_dec,double &sigmal_dec, double &sigmar_dec)
{
  
  // Some formatting options
  gStyle->SetOptStat(1110);
  gStyle->SetOptFit(0010);
  gStyle->SetStatFormat("4.2g"); 
  gStyle->SetFitFormat("4.2g"); 
  //gStyle->SetTitleOffset(1.6,"y");


  double RA,RA_true,Dec,Dec_true,x_pos,y_pos,RA_err,Dec_err;      
  char class_type;
  
  vector<double> ra1,ra2,dec1,dec2,diff_ra,diff_dec,
    ra1_err,dec1_err,ra2_err,dec2_err;  
  double conv=2.777e-4;
  
  TCanvas *c1=new TCanvas("c1","");
  c1->SetLeftMargin(0.16);  
  
  vector<ObjPair>::const_iterator obj_iter=obj_pairs.begin();
  
  int count=0;
  for(; obj_iter!=obj_pairs.end(); ++obj_iter) {

    ra1.push_back(obj_iter->obj1.RA/conv);
    ra2.push_back(obj_iter->obj2.RA/conv);
    ra1_err.push_back(obj_iter->obj1.RA_err/conv);
    ra2_err.push_back(obj_iter->obj2.RA_err/conv);

    dec1.push_back(obj_iter->obj1.Dec/conv);
    dec2.push_back(obj_iter->obj2.Dec/conv);
    dec1_err.push_back(obj_iter->obj1.Dec_err/conv);
    dec2_err.push_back(obj_iter->obj2.Dec_err/conv);
    
    double mean_dec=(dec1[count]+dec2[count])/2;

    diff_ra.push_back(cos(mean_dec*TMath::DegToRad())*(ra1[count]-ra2[count]));
    diff_dec.push_back(dec1[count]-dec2[count]);
    count++;
  }
  
  ///////////////////////////////////////////////////////////////
  // Do RA plot
  //////////////////////////////////////////////////////////////
    

  double ra_mean;//=TMath::Mean(diff_ra.size(),&diff_ra[0]);
  double ra_rms;//=TMath::RMS(diff_ra.size(),&diff_ra[0]);

  vector<double> dum(diff_ra.size());
  int good,bad;
  sigma_clip(3,2.5,diff_ra,dum,ra_mean,
             good,bad,ra_rms,true,false,1e-6);
  // Fill histogram
  double max_limit=ra_mean+10*ra_rms,min_limit=ra_mean-10*ra_rms;

    
    
  // Fill RA histogram
  //  double max_limit,min_limit;
  //  max_limit=0.8;min_limit=-0.8;
    

  TH1D *RA_diff=new TH1D("RA_diff",";RA - RA_{true} (arcseconds);Entries",
                         50,min_limit,max_limit);
  RA_diff->SetDirectory(0);
  int total=diff_ra.size();
  for(int i=0;i<diff_ra.size();++i) {
    RA_diff->Fill(diff_ra[i]);
  }
  RA_diff->GetXaxis()->CenterTitle();
  RA_diff->GetYaxis()->CenterTitle();
    
  // fit to a bifurcated gaussian
  TF1 *func = new TF1("bifurgaus",BifurGaus,min_limit,max_limit,4);
  func->SetNpx(100000);
  func->SetParameters(100,0.0,0.1,0.1);
  func->SetParNames("Constant","Mean Fit","#sigma_{Left}","#sigma_{Right}");
  func->SetParLimits(2,0,10);
  func->SetParLimits(3,0,10);
  RA_diff->Fit(func,"RQ");
  RA_diff->Draw();
  c1->Update();
    
  double gaus_const=func->GetParameter(0);
  double gaus_mean=func->GetParameter(1);
  double gaus_sigmaL=func->GetParameter(2);
  double gaus_sigmaR=func->GetParameter(3);
    
  mean_ra=gaus_mean;
  sigmal_ra=gaus_sigmaL;
  sigmar_ra=gaus_sigmaR;
    
  double min_cut=3*gaus_sigmaL;
  double max_cut=3*gaus_sigmaR;
  double min=gaus_mean-min_cut;
  double max=gaus_mean+max_cut;
    
    
  // 3sigma cut
    

  int inside=0;
  for(int i=0;i<diff_ra.size();++i) {
    if(diff_ra[i]<max && diff_ra[i]>min) inside++;
  }
    
  // calculate the outlier fraciton
  double outside=1-(1.*inside/total);





    
  // Find systematic error
  vector<double> fit_diff_ra,fit_ra1_err,fit_ra2_err;
    
  for(int i=0;i<diff_ra.size();i++) {
    if(diff_ra[i]>min && diff_ra[i]<max) {
      fit_diff_ra.push_back(diff_ra[i]);
      fit_ra1_err.push_back(ra1_err[i]);
      fit_ra2_err.push_back(ra2_err[i]);
    }
  }

  SysErrFit RA_fit;
  double RA_min_err;
  double RA_min=RA_fit.Fit(&fit_diff_ra,&fit_ra1_err,RA_min_err,&fit_ra2_err);
  if(debug)cout<<"RA Minimization Minimum Found at = "<<RA_min<<endl;




    
  RA_diff->Draw();

    
  // Add text to statbox
  TPaveStats *ps_RA = (TPaveStats*)c1->GetPrimitive("stats");
  ps_RA->SetName("mystats_RA");
  ps_RA->SetX1NDC(0.675) ; 
  ps_RA->SetX2NDC(0.976) ; 
  ps_RA->SetY1NDC(0.334) ; 
  ps_RA->SetY2NDC(0.871);
  TList *list_RA = ps_RA->GetListOfLines();
  TText *tconst_RA = ps_RA->GetLineWith("Constant"); 

  if(tconst_RA) {
    TLatex *myt_RAsys = new 
      TLatex(0,0,Form("#sigma_{sys}= %2.2g",RA_min));
    myt_RAsys->SetTextFont(tconst_RA->GetTextFont());
    myt_RAsys->SetTextSize(tconst_RA->GetTextSize());
    myt_RAsys->SetTextColor(2);
    list_RA->Add(myt_RAsys);
      
    TLatex *myt_RA = new
      TLatex(0,0,Form("Frac > 3#sigma = %0.2f",outside));
    myt_RA->SetTextFont(tconst_RA->GetTextFont());
    myt_RA->SetTextSize(tconst_RA->GetTextSize());
    myt_RA->SetTextColor(2);
    list_RA->Add(myt_RA);

      
    list_RA->Remove(tconst_RA); 
  }

  ps_RA->SetTextSize(0.053);
  ps_RA->SetBorderSize(0);
  ps_RA->SetTextColor(2);
  RA_diff->SetStats(0);
  ps_RA->Draw();

    
    
  // Draw top label
  TLatex *label=new TLatex;
  label->SetNDC();
  label->SetTextSize(0.1);
  label->SetTextColor(kBlue);
  label->SetTextFont(22);
  label->SetTextAlign(22);
    
  string RA_label;
  RA_label=title+" RA";
  label->DrawLatex(0.49,0.948,RA_label.c_str());
    
  if(debug) {
    c1->Update();
    c1->WaitPrimitive();
  }
  // print out plot
  string id=title+"_"+band+"_RA";
  string print=dir+"/"+id+".png";
  //c1->Print(print.c_str());
    
  string convert="convert "+id+".eps "+id+".png";
  //gSystem->Exec(convert.c_str());
    
  convert="rm "+print;
  //gSystem->Exec(convert.c_str());
  can->cd(cur_can);
  TObject *ob=c1->DrawClonePad();
  delete ob;
  //RA_diff->DrawClone();
  cur_can++;
    
  c1->cd();
    
  ///////////////////////////////////////////////////////////////
  // Do Dec plot
  //////////////////////////////////////////////////////////////
  double dec_mean;//=TMath::Mean(diff_dec.size(),&diff_dec[0]);
  double dec_rms;//=TMath::RMS(diff_dec.size(),&diff_dec[0]);



  sigma_clip(3,2.5,diff_dec,dum,dec_mean,
             good,bad,dec_rms,true,false,1e-6);
  // Fill histogram
  max_limit=dec_mean+10*dec_rms,min_limit=dec_mean-10*dec_rms;    


  // fill
  TH1D *Dec_diff=new TH1D("Dec_diff",
                          ";Dec - Dec_{true} (arcseconds);Entries",
                          50,min_limit,max_limit);
  Dec_diff->SetDirectory(0);
  Dec_diff->GetXaxis()->CenterTitle();
  Dec_diff->GetYaxis()->CenterTitle();
  for(int i=0;i<diff_dec.size();++i) {
    Dec_diff->Fill(diff_dec[i]);
  }
    
  Dec_diff->Fit(func,"QR");
  Dec_diff->Draw();
  c1->Update();
    
      
  gaus_const=func->GetParameter(0);
  gaus_mean=func->GetParameter(1);
  gaus_sigmaL=func->GetParameter(2);
  gaus_sigmaR=func->GetParameter(3);

  mean_dec=gaus_mean;
  sigmal_dec=gaus_sigmaL;
  sigmar_dec=gaus_sigmaR;
      
  min_cut=3*gaus_sigmaL;
  max_cut=3*gaus_sigmaR;
  min=gaus_mean-min_cut;
  max=gaus_mean+max_cut;

  inside=0;
  for(int i=0;i<diff_dec.size();++i) {
    if(diff_dec[i]<max && diff_dec[i]>min) inside++;
  }
       
  outside=1-(1.*inside/total);
      


      
    
  // Find systematic error
  vector<double> fit_diff_dec,fit_dec1_err,fit_dec2_err;
    
  for(int i=0;i<diff_dec.size();i++) {
    if(diff_dec[i]>min && diff_dec[i]<max) {
      fit_diff_dec.push_back(diff_dec[i]);
      fit_dec1_err.push_back(dec1_err[i]);
      fit_dec2_err.push_back(dec2_err[i]);
    }
  }

  SysErrFit Dec_fit;
  double Dec_min_err;
  double Dec_min=Dec_fit.Fit(&fit_diff_dec,&fit_dec1_err,Dec_min_err,&fit_dec2_err);
  if(debug)cout<<"Dec Minimization Minimum Found at = "<<Dec_min<<endl;
    
    
  // Add text to statbox
  TPaveStats *ps_Dec = (TPaveStats*)c1->GetPrimitive("stats");
  ps_Dec->SetName("mystats_Dec");
  ps_Dec->SetX1NDC(0.675) ; 
  ps_Dec->SetX2NDC(0.976) ; 
  ps_Dec->SetY1NDC(0.334) ; 
  ps_Dec->SetY2NDC(0.871);
  TList *list_Dec = ps_Dec->GetListOfLines();
  TText *tconst_Dec = ps_Dec->GetLineWith("Constant"); 

  if(tconst_Dec) {
    TLatex *myt_Decsys = new 
      TLatex(0,0,Form("#sigma_{sys}= %2.2g",Dec_min));
    myt_Decsys->SetTextFont(tconst_Dec->GetTextFont());
    myt_Decsys->SetTextSize(tconst_Dec->GetTextSize());
    myt_Decsys->SetTextColor(2);
    list_Dec->Add(myt_Decsys);
      
      
      
    TLatex *myt_Dec = 
      new TLatex(0,0,Form("Frac > 3#sigma = %0.2f",outside));
    myt_Dec->SetTextFont(tconst_Dec->GetTextFont());
    myt_Dec->SetTextSize(tconst_Dec->GetTextSize());
    myt_Dec->SetTextColor(2);
    list_Dec->Remove(tconst_Dec); 
    list_Dec->Add(myt_Dec);
  }

      
  ps_Dec->SetTextSize(0.053);
  ps_Dec->SetBorderSize(0);
  ps_Dec->SetTextColor(2);
  Dec_diff->SetStats(0);
  ps_Dec->Draw();
    
      

  string dec_label;

  dec_label=title+" Dec";

  label->DrawLatex(0.49,0.948,dec_label.c_str());
  c1->Update();

  if(debug) {
    c1->WaitPrimitive();
  }

      
  id=title+"_"+band+"_Dec";;
  print=dir+"/"+id+".png";
  //c1->Print(print.c_str());
      
  convert="convert "+id+".eps "+id+".png";
  //gSystem->Exec(convert.c_str());
      
  convert="rm "+print;
  //gSystem->Exec(convert.c_str());

  can->cd(cur_can);
  TObject *obj=c1->DrawClonePad();
  //Dec_diff->DrawClone();
  cur_can++;
  delete obj;
  delete label;
  delete RA_diff;
  delete Dec_diff;
      
  delete c1;



}
  
void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol)
{
  int N=array.size();
  if(!weighted) {
    mean=TMath::Mean(N,&array[0]);
  }
  else {
    mean=TMath::Mean(N,&array[0],&err[0]);
  }
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }
  good=N-bad;
  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;
  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    

    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();
    newerr_array.clear();

    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    curerr_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;


    
  }
 

  
  return;

}

void Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters)
{
  // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}
