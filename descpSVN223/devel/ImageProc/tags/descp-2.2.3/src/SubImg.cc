#include "FitsTools.hh"
#include "ComLine.hh"
#include "FitsTools.hh"
#include "ImageMorphology.hh"

///
/// \brief ComLineObject for subimg
///
/// ComLine is a convenience object to help
/// deal with command-line arguments and
/// options.
class SubImgComLine : public ComLineObject
{
public:
  SubImgComLine()
    : ComLineObject()
  { Initialize(); };
  SubImgComLine(const char *args[])
    : ComLineObject(args)
  { Initialize(); };
  void Initialize(){
    AddOption('h',"help");
    AddOption('v',"verb",2,"level");
    AddOption('l',"list",2,"filename");
    //    AddOption('y',"yrange",2,"range");
    AddHelp("help","Prints this long version of help.");
    AddHelp("verb","Verbosity level [0-3], default = 1.");
    AddHelp("list","Filename with range and output lists with format [x1 y1 x2 y2 output_filename]");
    AddArgument("infile",1);
    std::ostringstream Ostr;
    Ostr << "Input FITS file.\n";
    AddArgHelp("infile",Ostr.str());
    Ostr.str("");
    Ostr << "Reads input fits file and creates a new image with the indicated subset in the output files.\n";
    _description.assign(Ostr.str());
  };
};



int SubImg(const char *argv[])
{
  // ------- Setup and Command Line Parsing --------
  
  // Initialization of command line object
  SubImgComLine comline;
  int comline_status = comline.ProcessCommandLine((const char **)argv);

  std::string program_name(comline.ProgramName());
  
  // Just print help on stdout and quit if user
  // did -h or --help. 
  if(comline.Narg()==1){
    std::cout << comline.ShortUsage() << std::endl;
    return(1);
  }
  if(!comline.GetOption("help").empty()){
    std::cout << comline.LongUsage() << std::endl;
    return(1);
  }

  if(comline_status){
    std::cerr << comline.ErrorReport() << std::endl
	      << std::endl
	      << comline.ShortUsage()  << std::endl 
	      << std::endl;
    return(1);
  }
  
  // Find out what command line options were set
  std::string slist     =  comline.GetOption("list");
  //  std::string sxrange   =  comline.GetOption("xrange");
  //  std::string syrange   =  comline.GetOption("yrange");
  std::string sverb     =  comline.GetOption("verb");

  // Parse verbosity level
  int flag_verbose = 1;
  if(!sverb.empty()){
    std::istringstream Istr(sverb);
    Istr >> flag_verbose;
    if(flag_verbose < 0 || flag_verbose > 3)
      flag_verbose = 1;
  }
  
  // - Set up IO objects -
  //
  std::ostringstream Out;
  std::vector<std::string> infile_names(comline.GetArgs());
  if(infile_names.size() != 1){
    Out << comline.ShortUsage() << std::endl
	<< program_name << ": Requires input file name." << std::endl;
    LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
    return(1);
  }
//   if(sxrange.empty() || syrange.empty()){
//     Out << comline.ShortUsage() << std::endl
// 	<< program_name << ": Requires xrange and yrange to create subimage." << std::endl;
//     LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
//     return(1);
//   }

  // Set up a list of keywords we want
  // to exclude from our headers
  char **exclusion_list;
  exclusion_list = new char * [43];
  exclusion_list[0] = (char *)"SIMPLE";
  exclusion_list[1] = (char *)"BITPIX";
  exclusion_list[2] = (char *)"NAXIS";
  exclusion_list[3] = (char *)"NAXIS1";
  exclusion_list[4] = (char *)"NAXIS2";
  exclusion_list[5] = (char *)"EXTEND";
  exclusion_list[6] = (char *)"NEXTEND";
  exclusion_list[7] = (char *)"CHECKSUM";
  exclusion_list[8] = (char *)"DATASUM";
  exclusion_list[9] = (char *)"CHECKVER";
  exclusion_list[10] =(char *)"GCOUNT";
  exclusion_list[11] =(char *)"PCOUNT";
  exclusion_list[12] =(char *)"BZERO";
  exclusion_list[13] =(char *)"BSCALE";
  exclusion_list[14] =(char *)"INHERIT";
  exclusion_list[15] =(char *)"XTENSION";
  exclusion_list[16] =(char *)"TFIELDS";
  exclusion_list[17] =(char *)"TFORM1";
  exclusion_list[18] =(char *)"ZIMAGE";
  exclusion_list[19] =(char *)"ZTILE1";
  exclusion_list[20] =(char *)"ZTILE2";
  exclusion_list[21] =(char *)"ZCMPTYPE";
  exclusion_list[22] =(char *)"ZNAME1";
  exclusion_list[23] =(char *)"ZVAL1";
  exclusion_list[24] =(char *)"ZNAME2";
  exclusion_list[25] =(char *)"ZVAL2";
  exclusion_list[26] =(char *)"EXTNAME";
  exclusion_list[27] =(char *)"ZTENSION";
  exclusion_list[28] =(char *)"ZBITPIX";
  exclusion_list[29] =(char *)"ZNAXIS";
  exclusion_list[30] =(char *)"ZNAXIS1";
  exclusion_list[31] =(char *)"ZNAXIS2";
  exclusion_list[32] =(char *)"ZPCOUNT";
  exclusion_list[33] =(char *)"ZGCOUNT";
  exclusion_list[34] =(char *)"DCREATED";
  exclusion_list[35] =(char *)"TTYPE1";
  exclusion_list[36] =(char *)"ZHECKSUM";
  exclusion_list[37] =(char *)"TTYPE2";
  exclusion_list[38] =(char *)"TTYPE3";
  exclusion_list[39] =(char *)"ZSIMPLE";
  exclusion_list[40] =(char *)"ZEXTEND";
  exclusion_list[41] =(char *)"TFORM2";
  exclusion_list[42] =(char *)"TFORM3";

  // Declare and initialize FitsImage object which
  // will handle most of the FITS-specific interactions
  // under-the-covers.
  FitsTools::FitsImage Inimage;
  Inimage.SetOutStream(Out);
  Inimage.SetExclusions(exclusion_list,43);

  std::string infilename(infile_names[0]);
  std::string listfile(slist);
  
  std::ifstream ListIn(listfile.c_str());
  if(!ListIn){
    Out << comline.ShortUsage() << std::endl
	<< program_name << ": Could not open input list " 
	<< listfile << "." << std::endl;
    LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
    return(1);
  }

  std::string listline;

  while(std::getline(ListIn,listline)){
    std::istringstream ListStr(listline);
    std::string outfilename;
    
    std::vector<long> lx(2,0);
    std::vector<long> ux(2,0);
    
    ListStr >> lx[0] >> lx[1] >> ux[0] >> ux[1] >> outfilename;
    int status = 0;
    // If found, this will read the FITS file including
    // all data units, headers and images.
    if(Inimage.ReadSubImage(infilename,lx,ux,flag_verbose)){
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      Out.str("");
      Out << "Failed to read " << infilename << ".";
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    // Close the input FITS file, we're done with it.
    Inimage.Close();
    // Let stdout know what file was actually read.
    // (it can be different due to the various valid extensions)
    if(flag_verbose){
      Out.str("");
      Out << "Found " << Inimage.DES()->name << ".";
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
    }
    
    // How many HDU's were read?  Report it to stdout if verbose
    int number_of_hdus = Inimage.Headers().size();
    std::vector<std::string>::iterator hi = Inimage.Headers().begin();
    if(flag_verbose){
      Out.str("");
      Out << "Read " << number_of_hdus << " data units from " 
	  << infilename;
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
    }
    // If *really* verbose, dump the headers to stdout
    if(flag_verbose==3){
      while(hi != Inimage.Headers().end())
	{
	  Out.str("");
	  Out << "Size for image: " << Inimage.DES()->axes[0] << "X" 
	      << Inimage.DES()->axes[1] << ", number of pixels = " 
	      << Inimage.DES()->npixels << std::endl
	      << "Header " << hi-Inimage.Headers().begin()+1 << std::endl
	      << *hi << std::endl;
	  if(FitsTools::PutHeaderValue<float>(*hi,"CRPIX1",Inimage.DES()->crpix1)){
	    LX::ReportMessage(flag_verbose,STATUS,3,"CRPIX1 keyword not found for replacement.");
	  }
	  if(FitsTools::PutHeaderValue<float>(*hi,"CRPIX2",Inimage.DES()->crpix2)){
	    LX::ReportMessage(flag_verbose,STATUS,3,"CRPIX2 keyword not found for replacement.");
	  }
	  LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
	  hi++;
	}
    }
    
    
    if(flag_verbose==3){
      Out.str("");
      Out << "Writing subimage into " << outfilename << ".";
      LX::ReportMessage(flag_verbose,STATUS,1,Out.str());
    }
    if(Inimage.Write(outfilename,true,flag_verbose)){
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      Out << "Failed write output image into  " << outfilename << ".";
      Inimage.Close();
      LX::ReportMessage(flag_verbose,STATUS,5,Out.str());
      return(1);
    }
    // Close the output image.
    Inimage.Close();
  }
  return(0);
}

int main(int argc,char *argv[])
{
  return(SubImg((const char **)argv));
}
