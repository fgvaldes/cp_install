#include "imageproc.h"

#define LARGENUM 1E10

typedef struct {
  long NAXIS,NAXIS1,NAXIS2;
  double CRVAL1,CRVAL2,CRPIX1,CRPIX2;
  double CD1_1,CD1_2,CD2_1,CD2_2;
  double PV1_0,PV1_1,PV1_2,PV1_3,PV1_4,PV1_5,PV1_6,PV1_7,PV1_8,PV1_9,PV1_10;
  double PV2_0,PV2_1,PV2_2,PV2_3,PV2_4,PV2_5,PV2_6,PV2_7,PV2_8,PV2_9,PV2_10;
  float EQUINOX;
  char CTYPE1[10],CTYPE2[10],CUNIT1[10],CUNIT2[10],RADECSYS[10];
} wcshead;

void get_wcs_header(char *inputimage, wcshead *imgtemp, int flag_verbose);
void print_errmessage(char *image, char *key, int flag_verbose, int status);
double get_dblkey(fitsfile *fptr, char *keyname, char *image, int flag_verbose);
double calc_wcs_mean(double inwcs[], int N);
double getmedian(double data[],int N);

int main(int argc, char *argv[]) 
{
  char swarpcall[5000],fitscombinecall[1000],splitimagecall[1000];    
  char listname[1000],event[1000],command[1000];
  char outpath[1000],outauxpath[1000],terapixpath[1000],etcpath[1000],binpath[1000];
  char tempimg[1000],temp1[1000],temp2[1000],bpmname[1000];
  char expname[100],final_outname[1000],checkimage[1000];
  char img_inputlist[1000],weight_inputlist[1000],imageid_list[1000];
  char headname[1000],tempout[1500],tempweight[1500];
  char **imagelist;
  int *imageid;
  int i,N,Ngood,ccd,flag,status;
  int flag_verbose=2,flag_list=0,flag_terapixpath=0,flag_etcpath=0;
  int flag_binpath=0,flag_outpath=0,flag_outauxpath=0;
  double mediancrval1,mediancrval2;
  double MJDOBS;
  double *temarray;
  
  wcshead *img,*newimg;
  
  void reportevt(),get_input(),check_fits_extension(),printerror();
  int count_inputfile(),mkpath();

  FILE *flist,*foutimg,*foutweight,*fhead,*fcheck,*foutid,*pip;

  fitsfile *fptr,*fptrtemp;

  /* default */
  sprintf(terapixpath,"/home/bcs/des_prereq/bin");
  sprintf(etcpath,"/home/bcs/des_prereq/etc");

  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s [Required Inputs] [Optional Inputs]\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -list <reduced image list [format: image_fullpath imageid]>  \n");
    printf("       -outpath <outpath>  \n");
    printf("       -outauxpath <outauxpath>  \n");
    printf("    Optional Inputs:\n");
    printf("       -terapixpath <full terapixpath>\n");
    printf("       -binpath <full binpath>\n");
    printf("       -etcpath <full etcpath>\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }
  
  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-list")) {
      flag_list=1;
      get_input(argv[i],argv[i+1],"-list",listname,flag_verbose);
    }

    if (!strcmp(argv[i],"-outpath")) {
      flag_outpath=1;
      get_input(argv[i],argv[i+1],"-outpath",outpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-outauxpath")) {
      flag_outauxpath=1;
      get_input(argv[i],argv[i+1],"-outauxpath",outauxpath,flag_verbose);
    }

    /* options */
    if (!strcmp(argv[i],"-terapixpath")) {
      flag_terapixpath=1;
      get_input(argv[i],argv[i+1],"-terapixpath",terapixpath,flag_verbose);
    }
    
    if (!strcmp(argv[i],"-binpath")) {
      flag_binpath=1;
      get_input(argv[i],argv[i+1],"-binpath",binpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-etcpath")) {
      flag_etcpath=1;
      get_input(argv[i],argv[i+1],"-etcpath",etcpath,flag_verbose);
    }
  }

 /* quit if required inputs are not set */
  if(!flag_list || !flag_outpath || !flag_outauxpath) {
    sprintf(event," Not all required inputs are set ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }


  /* ------------------------------------------------- */
  /* CHECK INPUT REDUCED IMAGE LIST AND INPUT WCS INFO */
  /* ------------------------------------------------- */

  if(!strncmp(&(listname[0]),"-",1)) {
    reportevt(flag_verbose,STATUS,5,
	      "Image list not given? Double check command-line inputs");
    exit(0);
  }
  
  /* count the number of input images */
  N=count_inputfile(listname,flag_verbose);
  sprintf(event,"Number of reduced images in %s = %d",listname,N);
  reportevt(flag_verbose,STATUS,2,event);

  /* memory allocation */
  imagelist=(char **)calloc(N+1,sizeof(char *));
  if (imagelist==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of imagelist failed");
    exit(0);
  }
  for(i=1;i<=N;i++)
    imagelist[i]=(char *)calloc(1500,sizeof(char ));
  
  imageid=(int *)calloc(N+1,sizeof(int ));
  if (imageid==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of imageid failed");
    exit(0);
  }

  img=(wcshead *)calloc(N+1,sizeof(wcshead ));
  if (img==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of img failed");
    exit(0);
  }
  
  /* input the reduced image from imagelist */
  flist=fopen(listname,"r");
  if (flist==NULL) {
    sprintf(event,"File %s not found",listname);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else {
    for(i=1;i<=N;i++) {

      /* need to add the checking of the format later, make sure is [imagename imageid] format */ 
      fscanf(flist,"%s %d",imagelist[i],&imageid[i]);

      /* check the coadd image extension and unpack/unzip if necessary */
      check_fits_extension(imagelist[i],terapixpath,flag_verbose);

      /* get the necessary WCS info from the image header */
      get_wcs_header(imagelist[i],&img[i],flag_verbose);
    }
  }
  fclose(flist);
   
  
  /* ---------------------------------------------- */
  /* FIGURE OUT THE NEW OUTPUT EXPOSURENAME AND CCD */
  /* ---------------------------------------------- */

  /* use the first image as input */
  sprintf(tempimg,"%s",imagelist[1]);

  /* strip off the .fits */
  if(!strncmp(&(tempimg[strlen(tempimg)-5]),".fits",5)) 
    tempimg[strlen(tempimg)-5]=0;
  
  /* get the base exposure name and ccd */
  for(i=strlen(tempimg);i>0;i--) {
    if (!strncmp(&(tempimg[i]),"_",1)) 
      tempimg[i]=32;
    if (!strncmp(&(tempimg[i]),"-",1)) {
      tempimg[i]=32;
      break;
    }
  }
  sscanf(tempimg,"%s %d %d",temp1,&flag,&ccd);

  for(i=strlen(temp1);i>0;i--) {
    if (!strncmp(&(temp1[i]),"/",1)) {
      temp1[i]=32;
      break;
    }
  }
  sscanf(temp1,"%s %s",temp2,expname);
  
  sprintf(event," The base exposure is %s with CCD %02d ",expname,ccd);
  reportevt(flag_verbose,STATUS,2,event);


  /* ---------------------------------------------- */
  /* CHECK THE IMAGES IF LIE WITHIN POINTING ERROR  */
  /* ---------------------------------------------- */

  temarray=(double *)calloc(N+1,sizeof(double ));
  if (temarray==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of temarray failed");
    exit(0);
  }

  newimg=(wcshead *)calloc(N+1,sizeof(wcshead ));
  if (newimg==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of newimg failed");
    exit(0);
  }

  /* get the median values for CRVAL */
  for(i=1;i<=N;i++) temarray[i]=img[i].CRVAL1;
  mediancrval1=getmedian(temarray,N);

  for(i=1;i<=N;i++) temarray[i]=img[i].CRVAL2;
  mediancrval2=getmedian(temarray,N);

  /* filter out the pointings with > 20 arcsec */
  /* also output the list for swarp later */
  /* as well as the parent image id to a list */
  sprintf(img_inputlist,"%s/nitecombine_images_%s_%02d.list",outauxpath,expname,ccd);
  sprintf(weight_inputlist,"%s/nitecombine_weight_%s_%02d.list",outauxpath,expname,ccd);
  sprintf(imageid_list,"%s/%s_%02d_imageid.list",outauxpath,expname,ccd);
  
  foutimg=fopen(img_inputlist,"w");
  foutweight=fopen(weight_inputlist,"w");
  foutid=fopen(imageid_list,"w");

  Ngood=0;
  for(i=1;i<=N;i++) {
    if(fabs(img[i].CRVAL1-mediancrval1)*3600.0 > 20.0 || fabs(img[i].CRVAL2-mediancrval2)*3600.0 > 20.0) {
      sprintf(event," Image %s is not used: CRVAL1=%2.6f (vs median of %2.6f)  CRVAL2=%2.6f (vs median of %2.6f) ",imagelist[i],img[i].CRVAL1,mediancrval1,img[i].CRVAL2,mediancrval2);
      reportevt(flag_verbose,STATUS,1,event);
    }
    else {
      Ngood++;
      newimg[Ngood]=img[i];
      fprintf(foutimg,"%s[0]\n",imagelist[i]);
      fprintf(foutweight,"%s[2]\n",imagelist[i]);
      fprintf(foutid,"%d\n",imageid[i]);
    }
  }
  fclose(foutimg); fclose(foutweight); fclose(foutid);
  

  /* ------------------------------------------------------ */
  /* AVERAGE CRVAL,CRPIX,CD,PV MATRIX AND OUTPUT HEAD FILE  */
  /* ------------------------------------------------------ */

  /* set the .head filename */
  sprintf(headname,"%s_%02d_tmpimg.head",expname,ccd);

  /* create the .head file */
  fhead=fopen(headname,"w");
  
  /* write the first line to .head file */
  fprintf(fhead,"COMMENT\n");

  /* hardwire the NAXIS from the first image */
  fprintf(fhead,"NAXIS   = %ld\t/ Number of Axis\n",newimg[1].NAXIS);
  fprintf(fhead,"NAXIS1  = %ld\t/ Dimension of Axis1\n",newimg[1].NAXIS1);
  fprintf(fhead,"NAXIS2  = %ld\t/ Dimension of Axis1\n",newimg[1].NAXIS2);
  
  /* hardwire the CTYPE from the first image */
  fprintf(fhead,"CTYPE1  = '%s'\t/ WCS projection type for this axis\n",newimg[1].CTYPE1);
  fprintf(fhead,"CTYPE2  = '%s'\t/ WCS projection type for this axis\n",newimg[1].CTYPE2);

  /* average the CRVAL */
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CRVAL1;
  fprintf(fhead,"CRVAL1  = %2.8f\t/ World coordinate on this axis\n",calc_wcs_mean(temarray,Ngood));

  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CRVAL2;
  fprintf(fhead,"CRVAL2  = %2.8f\t/ World coordinate on this axis\n",calc_wcs_mean(temarray,Ngood));

  /* average the CRPIX */
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CRPIX1;
  fprintf(fhead,"CRPIX1  = %2.8f\t/ Reference pixel on this axis\n",calc_wcs_mean(temarray,Ngood));

  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CRPIX2;
  fprintf(fhead,"CRPIX2  = %2.8f\t/ Reference pixel on this axis\n",calc_wcs_mean(temarray,Ngood));

  /* average the CD */
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CD1_1;
  fprintf(fhead,"CD1_1   = %2.10f\t/ Linear projection matrix\n",calc_wcs_mean(temarray,Ngood));

  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CD1_2;
  fprintf(fhead,"CD1_2   = %2.10f\t/ Linear projection matrix\n",calc_wcs_mean(temarray,Ngood));

  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CD2_1;
  fprintf(fhead,"CD2_1   = %2.10f\t/ Linear projection matrix\n",calc_wcs_mean(temarray,Ngood));

  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].CD2_2;
  fprintf(fhead,"CD2_2   = %2.10f\t/ Linear projection matrix\n",calc_wcs_mean(temarray,Ngood));

  /* average PV1 */
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_0;
  fprintf(fhead,"PV1_0   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_1;
  fprintf(fhead,"PV1_1   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_2;
  fprintf(fhead,"PV1_2   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_4;
  fprintf(fhead,"PV1_4   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_5;
  fprintf(fhead,"PV1_5   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_6;
  fprintf(fhead,"PV1_6   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_7;
  fprintf(fhead,"PV1_7   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_8;
  fprintf(fhead,"PV1_8   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_9;
  fprintf(fhead,"PV1_9   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV1_10;
  fprintf(fhead,"PV1_10  = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));

  /* average PV1 */
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_0;
  fprintf(fhead,"PV2_0   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_1;
  fprintf(fhead,"PV2_1   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_2;
  fprintf(fhead,"PV2_2   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_4;
  fprintf(fhead,"PV2_4   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_5;
  fprintf(fhead,"PV2_5   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_6;
  fprintf(fhead,"PV2_6   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_7;
  fprintf(fhead,"PV2_7   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_8;
  fprintf(fhead,"PV2_8   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_9;
  fprintf(fhead,"PV2_9   = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));
  for(i=1;i<=Ngood;i++) temarray[i]=newimg[i].PV2_10;
  fprintf(fhead,"PV2_10  = %2.12f\t/ Projection distortion parameter\n",calc_wcs_mean(temarray,Ngood));

  fprintf(fhead,"END     \n");
  fclose(fhead);

  /* --------------------------------------------------- */
  /* USE SWARP TO DISTORT AND COMBINE THE COADD IMAGES   */
  /* --------------------------------------------------- */
  
  /* set the output temp fits name and .head filename */
  sprintf(tempout,"%s_%02d_tmpimg.fits",expname,ccd);
  sprintf(tempweight,"%s_%02d_tmpweight.fits",expname,ccd);

  /* construct swarp call here */
  if(flag_terapixpath) sprintf(swarpcall,"%s/swarp ",terapixpath);
  else sprintf(swarpcall,"swarp ");

  sprintf(swarpcall,"%s @%s",swarpcall,img_inputlist); 
  
  if(flag_etcpath) sprintf(swarpcall,"%s -c %s/default.swarp ",swarpcall,etcpath);
  else sprintf(swarpcall,"%s -c default.swarp ",swarpcall);

  sprintf(swarpcall,"%s -IMAGEOUT_NAME %s ",swarpcall,tempout);
  sprintf(swarpcall,"%s -WEIGHTOUT_NAME %s ",swarpcall,tempweight);
  sprintf(swarpcall,"%s -COMBINE Y ",swarpcall);
  sprintf(swarpcall,"%s -RESAMPLE Y ",swarpcall);
  sprintf(swarpcall,"%s -COMBINE_TYPE MEDIAN ",swarpcall);
  sprintf(swarpcall,"%s -SUBTRACT_BACK N",swarpcall);
  sprintf(swarpcall,"%s -DELETE_TMPFILES Y",swarpcall);
  sprintf(swarpcall,"%s -WRITE_XML N ",swarpcall);
  sprintf(swarpcall,"%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE @%s ",swarpcall,weight_inputlist);
  sprintf(swarpcall,"%s -COPY_KEYWORDS FILTER,CCDNUM ",swarpcall);
  //if(flag_nthread)
  //sprintf(swarpcall,"%s -NTHREADS %d ",swarpcall,nthread);
  if(flag_verbose==0)
    sprintf(swarpcall,"%s -VERBOSE_TYPE QUIET ",swarpcall);
  
  sprintf(event," SWARPcall: %s\n",swarpcall);
  reportevt(flag_verbose,STATUS,2,event);
  
  system(swarpcall);


  /* read in the MJD-OBS info from the first image */
  status=0;
  if(fits_open_file(&fptrtemp,imagelist[1],READONLY,&status)) {
    sprintf(event,"Image open failed: %s",imagelist[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  
  MJDOBS=get_dblkey(fptrtemp,"MJD-OBS",imagelist[1],flag_verbose);

  status=0;
  if(fits_close_file(fptrtemp,&status))  {
    sprintf(event,"Image %s close failed",imagelist[1]);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }

  /* write the MJD-OBS from first image and DES_EXT keyword to the header */
  status=0;
  if(fits_open_file(&fptr,tempout,READWRITE,&status)) {
    sprintf(event,"Image open failed: %s",tempout);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  status=0;
  if (fits_write_key_dbl(fptr,"MJD-OBS",MJDOBS,12,"MJD from first image",&status)) {
    sprintf(event,"Keyword MJD-OBS insert in %s failed",tempout);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
  }
  status=0;
  if (fits_write_key_str(fptr,"DES_EXT","IMAGE","Image extension",&status)) {
    sprintf(event,"Keyword DES_EXT insert in %s failed",tempout);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
  }
  status=0;
  if(fits_close_file(fptr,&status))  {
    sprintf(event,"Image %s close failed",tempout);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  
  status=0;
  if(fits_open_file(&fptr,tempweight,READWRITE,&status)) {
    sprintf(event,"Image open failed: %s",tempweight);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  status=0;
  if (fits_write_key_str(fptr,"DES_EXT","VARIANCE","Extension type",&status)) {
    sprintf(event,"Keyword DES_EXT insert in %s failed",tempweight);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
  }
  status=0;
  if(fits_close_file(fptr,&status))  {
    sprintf(event,"Image %s close failed",tempweight);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }
  

  /* ------------------------------------------------------- */
  /* COMBINE THE OUTPUT IMAGES AND PUT IN APPROPRIATE PLACE  */
  /* ------------------------------------------------------- */
  
  sprintf(final_outname,"%s/%s/%s_%02d_nitecmb.fits",outpath,expname,expname,ccd);

  if (mkpath(final_outname,flag_verbose)) {
    sprintf(event,"Failed to create path to file: %s",final_outname);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else {
    sprintf(event,"Created path to file %s",final_outname);
    reportevt(flag_verbose,STATUS,1,event);
  }

  /* TEMPORARY: use the BPM from first image as input */  
  if(flag_binpath) 
    sprintf(splitimagecall,"%s/splitimage %s -output temp",binpath,imagelist[1]);
  else 
    sprintf(splitimagecall,"splitimage %s ",imagelist[1]);
  
  if(!flag_verbose)
    sprintf(splitimagecall,"%s -quiet",splitimagecall);
  
  sprintf(event,"Splitimage Call: %s ",splitimagecall);
  reportevt(flag_verbose,STATUS,1,event);
  system(splitimagecall);

  /* grep the BPM imagename */
  sprintf(command,"ls temp_bpm.fits");
  pip=popen(command,"r");
  fscanf(pip,"%s",bpmname);
  pclose(pip);

  /* combine the coadd image and weight maps */
  if(flag_binpath) 
    sprintf(fitscombinecall,"%s/fitscombine %s %s %s %s -cleanup -verbose %d",
	    binpath,tempout,bpmname,tempweight,final_outname,flag_verbose);
  else 
    sprintf(fitscombinecall,"fitscombine %s %s %s %s -cleanup -verbose %d",
	    tempout,bpmname,tempweight,final_outname,flag_verbose);
  
  sprintf(event,"Fitscombine Call: %s ",fitscombinecall);
  reportevt(flag_verbose,STATUS,1,event);
  system(fitscombinecall);
  
  sprintf(checkimage,"%s",final_outname);
  fcheck=fopen(checkimage,"r");
  if (fcheck==NULL) {
    sprintf(event," Creation for nitely combined image failed: %s",final_outname);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else
    fclose(fcheck);
  
  /* move the .head to propername */
  sprintf(command,"mv %s_%02d_tmpimg.head %s_%02d_nitecmb.head",expname,ccd,expname,ccd);
  system(command);
   
  /* clean up the temp*.fits files */
  system("rm temp_im.fits temp_var.fits");

  /* free memory */
  free(imagelist); free(img); free(temarray); free(newimg); free(imageid);
  
  return(0);
}

double calc_wcs_mean(double inwcs[], int N)
{
  int i,count=0;
  double mean,sum=0.0;
  
  for(i=1;i<=N;i++) {
    if(inwcs[i]!=LARGENUM) {
      sum+=inwcs[i];
      count++;
    }
  }

  mean=sum/(double)count;

  return (mean);
}

void get_wcs_header(char *inputimage, wcshead *imgtemp, int flag_verbose)
{
  char comment[1000],event[1000];
  void printerror(),reportevt();
  int status;
  wcshead img;
  fitsfile *fptr;

  /* open the input fits image to read in necessary information */
  status=0;
  if(fits_open_file(&fptr,inputimage,READONLY,&status)) {
    sprintf(event,"Image open failed: %s",inputimage);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }

  /* extract NAXIS, and the NAXIS1/NAXIS2 */
  status=0;
  if(fits_read_key_lng(fptr,"NAXIS",&img.NAXIS,comment,&status)==KEY_NO_EXIST) { 
    print_errmessage(inputimage,"NAXIS",flag_verbose,status);
    img.NAXIS=2;
  }

  status=0;
  if(fits_read_key_lng(fptr,"NAXIS1",&img.NAXIS1,comment,&status)==KEY_NO_EXIST) {
    print_errmessage(inputimage,"NAXIS1",flag_verbose,status);
    img.NAXIS1=2048;
  }

  status=0;
  if(fits_read_key_lng(fptr,"NAXIS2",&img.NAXIS2,comment,&status)==KEY_NO_EXIST) {
    print_errmessage(inputimage,"NAXIS2",flag_verbose,status);
    img.NAXIS2=4096;
  }

  /* extract CTYPE */
  status=0;
  if(fits_read_key_str(fptr,"CTYPE1",img.CTYPE1,comment,&status)==KEY_NO_EXIST) {
    print_errmessage(inputimage,"CTYPE1",flag_verbose,status);
    sprintf(img.CTYPE1,"RA---TAN");
  }

  status=0;
  if(fits_read_key_str(fptr,"CTYPE2",img.CTYPE2,comment,&status)==KEY_NO_EXIST) {
    print_errmessage(inputimage,"CTYPE2",flag_verbose,status);
    sprintf(img.CTYPE1,"DEC--TAN");
  }

  /* extract CRVAL */
  img.CRVAL1=get_dblkey(fptr,"CRVAL1",inputimage,flag_verbose);
  img.CRVAL2=get_dblkey(fptr,"CRVAL2",inputimage,flag_verbose);
 
  /* extract CRPIX */
  img.CRPIX1=get_dblkey(fptr,"CRPIX1",inputimage,flag_verbose);
  img.CRPIX2=get_dblkey(fptr,"CRPIX2",inputimage,flag_verbose);

  /* extract CD  */
  img.CD1_1=get_dblkey(fptr,"CD1_1",inputimage,flag_verbose);
  img.CD1_2=get_dblkey(fptr,"CD1_2",inputimage,flag_verbose);
  img.CD2_1=get_dblkey(fptr,"CD2_1",inputimage,flag_verbose);
  img.CD2_2=get_dblkey(fptr,"CD2_2",inputimage,flag_verbose);
  
  /* extract PV1 (ignore PV1_3) */
  img.PV1_0=get_dblkey(fptr,"PV1_0",inputimage,flag_verbose);
  img.PV1_1=get_dblkey(fptr,"PV1_1",inputimage,flag_verbose);
  img.PV1_2=get_dblkey(fptr,"PV1_2",inputimage,flag_verbose);
  img.PV1_4=get_dblkey(fptr,"PV1_4",inputimage,flag_verbose);
  img.PV1_5=get_dblkey(fptr,"PV1_5",inputimage,flag_verbose);
  img.PV1_6=get_dblkey(fptr,"PV1_6",inputimage,flag_verbose);
  img.PV1_7=get_dblkey(fptr,"PV1_7",inputimage,flag_verbose);
  img.PV1_8=get_dblkey(fptr,"PV1_8",inputimage,flag_verbose);
  img.PV1_9=get_dblkey(fptr,"PV1_9",inputimage,flag_verbose);
  img.PV1_10=get_dblkey(fptr,"PV1_10",inputimage,flag_verbose);

  /* extract PV2 (ignore PV2_3) */
  img.PV2_0=get_dblkey(fptr,"PV2_0",inputimage,flag_verbose);
  img.PV2_1=get_dblkey(fptr,"PV2_1",inputimage,flag_verbose);
  img.PV2_2=get_dblkey(fptr,"PV2_2",inputimage,flag_verbose);
  img.PV2_4=get_dblkey(fptr,"PV2_4",inputimage,flag_verbose);
  img.PV2_5=get_dblkey(fptr,"PV2_5",inputimage,flag_verbose);
  img.PV2_6=get_dblkey(fptr,"PV2_6",inputimage,flag_verbose);
  img.PV2_7=get_dblkey(fptr,"PV2_7",inputimage,flag_verbose);
  img.PV2_8=get_dblkey(fptr,"PV2_8",inputimage,flag_verbose);
  img.PV2_9=get_dblkey(fptr,"PV2_9",inputimage,flag_verbose);
  img.PV2_10=get_dblkey(fptr,"PV2_10",inputimage,flag_verbose);

  /* close the fits image and .head file */
  status=0;
  if(fits_close_file(fptr,&status))  {
    sprintf(event,"Image %s close failed",inputimage);
    reportevt(flag_verbose,STATUS,5,event);
    printerror(status);
    exit(0);
  }

  *imgtemp=img;
}

void print_errmessage(char *image, char *key, int flag_verbose, int status)
{
  char event[1000];
  void printerror(),reportevt();
  sprintf(event,"Image %s keyword %s missing",image,key);
  reportevt(flag_verbose,STATUS,4,event);
  printerror(status);
}

double get_dblkey(fitsfile *fptr, char *keyname, char *image, int flag_verbose)
{
  char comment[1000];
  int status;
  double temp;

  status=0;
  if(fits_read_key_dbl(fptr,keyname,&temp,comment,&status)==KEY_NO_EXIST) {
    print_errmessage(image,keyname,flag_verbose,status);
    temp=LARGENUM;
  }

  return (temp);
}

double getmedian(double data[],int N)
{
  float *vecsort,median;
  int i;
  unsigned long n;
  void shell();

  //n=(unsigned long)N;
  vecsort=(float *)calloc(N+1,sizeof(float));
  
  n=0;
  for(i=1;i<=N;i++) {
    if(data[i]!=LARGENUM) {
      n++;
      vecsort[n]=(float)data[i];
    }
  }

  /* sort with N.R. subroutine */
  shell(n,vecsort);

  /* get median value */
  if(n%2)  median = vecsort[(n+1)/2];
  else median = 0.5 * (vecsort[n/2] + vecsort[(n/2)+1]);

  free(vecsort);
  return ((double)median);
}

#undef LARGENUM
