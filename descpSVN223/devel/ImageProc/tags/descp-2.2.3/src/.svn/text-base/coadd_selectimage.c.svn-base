/*
**
** coadd_selectimage.c
**
** DESCRIPTION:
**     Select the single band remap or reduced images for a given tile, and 
**     apply any filtering if necessary.
**     Output will be a list of image ID saved in ASCII file
**     
** OUTPUT FILE FORMAT:
**    ImageID  
**
** Last commit:
**     $Rev$
**     $LastChangedBy$
**     $LastChangedDate$
**
*/

#include "imageproc.h"

#define NMAX 500 /* maximum number of comma separated nites or runs */

main(argc,argv)
     int argc;
     char *argv[];
{
  char tilenamein[100],project[10],band[2],outfile[1000];
  char imagetype[10],tempname[500];
  char event[1000],line[1000];
  char input_nitelist[1000],input_runlist[1000];
  char dbcall[1000],dblogin[1000],sqlcall[1000],sqlcall1[1000],sqlHEAD[1000];
  char **tilelist,**nitelist,**runlist;
  
  int i,j,k;
  int ID,N_tile,N_nite,N_run,N_image;
  int flag_verbose=2,flag_neighbour=0,flag_output=0,flag_nitelist=0,flag_runlist=0;
  int flag_fwhm=0,flag_exptime=0;

  float FWHM,EXPTIME;
  double TILEBUFF,tile_ra,tile_dec;

  FILE *fsqlout,*fout,*pip;

  /* external functions call */
  void select_dblogin(),reportevt(),get_input(),get_input_array();
  void get_input_flt();

  /* default imagetype */
  sprintf(imagetype,"red");
  
  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <optional inputs>\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -project <project name (either BCS or DES)>\n");
    printf("       -tilename <tile string>\n");
    printf("       -band <band>\n");
    printf("       -output <output filename>\n");
    printf("    Optional Inputs:\n");
    printf("       -nite <nite#1,nite#2,...>\n");
    printf("       -run <run#1,run#2,...>\n");
    printf("       -neighbour (for 3x3 tile grid)\n");
    printf("       -moreneighbour (for 5x5 tile grid)\n");
    printf("       -remap (default imagetype is red)\n");
    printf("       -fwhm <#> (max FWHM in filtering)\n");
    printf("       -exptime <#> (min EXPTIME in filtering)\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }

  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
	sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
	flag_verbose=2;
	reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    /* required inputs */
    if (!strcmp(argv[i],"-band")) 
      get_input(argv[i],argv[i+1],"-band",band,flag_verbose);

    if (!strcmp(argv[i],"-project")) 
      get_input(argv[i],argv[i+1],"-project",project,flag_verbose);

    if (!strcmp(argv[i],"-tilename")) 
      get_input(argv[i],argv[i+1],"-tilename",tilenamein,flag_verbose);

    if (!strcmp(argv[i],"-output")) {
      get_input(argv[i],argv[i+1],"-output",outfile,flag_verbose); 
      flag_output=1;
    }

    /* option inputs */

    if (!strcmp(argv[i],"-nite")) {
      get_input(argv[i],argv[i+1],"-nite",input_nitelist,flag_verbose);
      flag_nitelist=1;
    }

    if (!strcmp(argv[i],"-run")) {
      get_input(argv[i],argv[i+1],"-run",input_runlist,flag_verbose);
      flag_runlist=1;
    }

    if (!strcmp(argv[i],"-neighbour")) flag_neighbour=1;
    if (!strcmp(argv[i],"-moreneighbour")) flag_neighbour=2;
    if (!strcmp(argv[i],"remap")) sprintf(imagetype,"remap");

    if (!strcmp(argv[i],"-fwhm")) {
      get_input_flt(argv[i],argv[i+1],"-fwhm",&FWHM,flag_verbose);
      flag_fwhm=1;
    }

    if (!strcmp(argv[i],"-fwhm")) {
      get_input_flt(argv[i],argv[i+1],"-exptime",&EXPTIME,flag_verbose);
      flag_exptime=1;
    }
  }

  /* *** Get the list of nites and/or run from inputs *** */
  if(flag_nitelist) {
    nitelist=(char **)calloc(NMAX,sizeof(char *));
    for(i=0;i<=NMAX;i++) nitelist[i]=(char *)calloc(64,sizeof(char ));
    get_input_array(input_nitelist,nitelist,&N_nite,flag_verbose);
  }

  if(flag_runlist) {
    runlist=(char **)calloc(NMAX,sizeof(char *));
    for(i=0;i<=NMAX;i++) runlist[i]=(char *)calloc(64,sizeof(char ));
    get_input_array(input_runlist,runlist,&N_run,flag_verbose);
  }

  

  /* *** Setup DB login and make generic sqlcall *** */
  select_dblogin(dblogin,DB_READWRITE);
  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < temp.sql ",dblogin);
  sprintf(sqlcall1, "${ORACLE_HOME}/bin/sqlplus -S %s < temp1.sql ",dblogin);
  sprintf(sqlHEAD,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;");

  /* *** Set up the TILEBUFF depending on how many neighbours required *** */
  switch(flag_neighbour) {
  case 0: TILEBUFF=0.001;  break; /* for 1x1 grid */
  case 1: TILEBUFF=0.800;  break; /* for 3x3 grid */
  case 2: TILEBUFF=1.600;  break; /* for 5x5 grid */
  }
  
  /* *** Find out the number of tiles *** */
  fsqlout=fopen("temp.sql", "w");
  fprintf(fsqlout,"%s\n",sqlHEAD);
  fprintf(fsqlout,"select RA,DEC ");
  fprintf(fsqlout,"from coaddtile where ");
  fprintf(fsqlout,"project='%s' ",project);
  fprintf(fsqlout," and tilename='%s'",tilenamein);
  fprintf(fsqlout,";\nexit;\n");
  fclose(fsqlout);
 
  pip=popen(sqlcall,"r");
  fscanf(pip,"%lf %lf ",&tile_ra,&tile_dec);
  pclose(pip); 
  
  /* query again to find out the neighbouring tiles */
  fsqlout=fopen("temp.sql", "w");
  fprintf(fsqlout,"%s\n",sqlHEAD);
  fprintf(fsqlout,"select count(tilename) ");
  fprintf(fsqlout,"from coaddtile where ");
  fprintf(fsqlout,"project='%s' ",project);
  fprintf(fsqlout," and (RA between %2.7f and %2.7f) and (DEC between %2.7f and %2.7f)",
	  tile_ra-TILEBUFF/cos(tile_dec*M_PI/180.0),tile_ra+TILEBUFF/cos(tile_dec*M_PI/180.0),tile_dec-TILEBUFF,tile_dec+TILEBUFF);
  fprintf(fsqlout,";\n");
  fprintf(fsqlout,"select tilename ");
  fprintf(fsqlout,"from coaddtile where ");
  fprintf(fsqlout,"project='%s' ",project);
  fprintf(fsqlout," and (RA between %2.7f and %2.7f) and (DEC between %2.7f and %2.7f)",
	  tile_ra-TILEBUFF/cos(tile_dec*M_PI/180.0),tile_ra+TILEBUFF/cos(tile_dec*M_PI/180.0),tile_dec-TILEBUFF,tile_dec+TILEBUFF);
  fprintf(fsqlout,";\nexit;\n");
  fclose(fsqlout);

  i=0;
  pip=popen(sqlcall,"r");
  while (fgets(line,1000,pip)!=NULL) {
    if (!i) {
      /* find out how many tiles */
      sscanf(line,"%d",&N_tile);

      tilelist=(char **)calloc(N_tile+1,sizeof(char *));
      for(j=1;j<=N_tile;j++)
	tilelist[j]=(char *)calloc(100,sizeof(char ));
    }
    else {
      sscanf(line,"%s",tilelist[i]);
      
      sprintf(event,"Selected tile %s ",tilelist[i]);
      reportevt(flag_verbose,STATUS,2,event);
     }
    i++;
  }
  pclose(pip);

  /* ************************** */
  /* *** Select Images Here *** */
  /* ************************** */

  /* first find out the distinct exposure names */
  fsqlout=fopen("temp1.sql", "w");
  fprintf(fsqlout,"%s\n",sqlHEAD);
  fprintf(fsqlout,"SELECT distinct(imagename) FROM image WHERE imagetype='remap' ");
  fprintf(fsqlout,"and project='%s' ",project);
  fprintf(fsqlout,"and band like '%s%%' ",band);

  fprintf(fsqlout,"and (tilename='%s' ",tilelist[1]);
  for(k=2;k<=N_tile;k++) 
    fprintf(fsqlout,"or tilename='%s' ",tilelist[k]);
  fprintf(fsqlout,") ");  
  
  if(flag_nitelist) {
    fprintf(fsqlout,"and (nite like '%s%%' ",nitelist[1]);
    for(k=2;k<=N_nite;k++) 
      fprintf(fsqlout,"or nite like '%s%%' ",nitelist[k]);
    fprintf(fsqlout,") "); 
  }

  if(flag_runlist) {
    fprintf(fsqlout,"and (run like '%s%%' ",runlist[1]);
    for(k=2;k<=N_run;k++) 
      fprintf(fsqlout,"or run like '%s%%' ",runlist[k]);
    fprintf(fsqlout,") "); 
  }
  fprintf(fsqlout,";\nexit;\n");
  fclose(fsqlout);

  /* now read in the distinct exposurename and query the imageid */

  fsqlout=fopen("temp.sql", "w");

  fprintf(fsqlout,"%s\n",sqlHEAD);
  pip=popen(sqlcall1,"r");
  while(fscanf(pip,"%s",tempname)!=EOF) {

    fprintf(fsqlout,"SELECT ID FROM image WHERE imagetype='%s' ",imagetype);
    fprintf(fsqlout,"and imagename='%s' ",tempname);
    fprintf(fsqlout,"and project='%s' ",project);
    fprintf(fsqlout,"and band like '%s%%' ",band);
 
   if(flag_fwhm) 
      fprintf(fsqlout,"and FWHM<=%2.3f ",FWHM);
    if(flag_exptime) 
      fprintf(fsqlout,"and EXPTIME>=%2.3f ",EXPTIME);
        
    if(flag_nitelist) {
      fprintf(fsqlout,"and (nite like '%s%%' ",nitelist[1]);
      for(k=2;k<=N_nite;k++) 
	fprintf(fsqlout,"or nite like '%s%%' ",nitelist[k]);
      fprintf(fsqlout,") "); 
    }
    
    if(flag_runlist) {
      fprintf(fsqlout,"and (run like '%s%%' ",runlist[1]);
      for(k=2;k<=N_run;k++) 
	fprintf(fsqlout,"or run like '%s%%' ",runlist[k]);
      fprintf(fsqlout,") "); 
    }
    fprintf(fsqlout,";\n");
  }
  fprintf(fsqlout,"exit;\n");
  fclose(fsqlout);  
  pclose(pip);

  /* output the image ID */
  N_image=0;
  if(flag_output)
     fout=fopen(outfile,"w");

  pip=popen(sqlcall,"r");
  while(fscanf(pip,"%d",&ID)!=EOF) {
    N_image++;
    if(flag_output)
      fprintf(fout,"%d\n",ID);
    else
      printf("%d\n",ID);
  }
  pclose(pip);
  if(flag_output)
    fclose(fout);

  /* output the number of images */
  sprintf(event,"Number of selected (%s) images = %d ",imagetype,N_image);
  reportevt(flag_verbose,STATUS,1,event);

  /* *** Clean up *** */
  system ("rm temp*");

  /* *** Free memory *** */
  free(tilelist);
  if(flag_nitelist) free(nitelist);
  if(flag_runlist) free(runlist);
}

#undef NMAX
