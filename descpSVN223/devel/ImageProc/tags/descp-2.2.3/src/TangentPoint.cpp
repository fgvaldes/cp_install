/*
 * TangentPoint.cpp
 *
 *  Created on: Mar 22, 2012
 *      Author: kuropat
 */

#include "TangentPoint.h"

namespace std {

TangentPoint::TangentPoint() {
	// TODO Auto-generated constructor stub
  strcpy(name_," ");
  Ra = 0.0;
  Dec = 0.0;
}
TangentPoint::TangentPoint(char *in, double ra, double dec) {
	strcpy(name_,in);
	Ra = ra;
	Dec = dec;


}
void TangentPoint::set(char *in, double ra, double dec) {
	strcpy(name_,in);
	Ra = ra;
	Dec = dec;
}
void TangentPoint::print() {
	cout<<name_<<" "<<Ra<<" "<<Dec<<endl;
}
TangentPoint::~TangentPoint() {
	// TODO Auto-generated destructor stub
}

} /* namespace std */
