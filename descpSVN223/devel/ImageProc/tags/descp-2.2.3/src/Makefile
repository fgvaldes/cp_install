################################################################################
# Makefile fo DES ImagProc codes.
#
#  $Id: arcp 996 2008-05-12 20:31:51Z dadams $
#
#  $Rev:: 996                              $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-05-12 15:31:51 #$:  # Date of last commit.
#
#  The implit rule is for every *.o object file in the OBJ list, a cooresponding
#  *.c or *.cc file exists.  Executable targets are stil explixitly defined and
#  a complete list of working targets should be maintained in the EXE variable.
#
#  A future simplification would be to group list of main targets with the same
#  command and the same dependencies--reducing the number of explicit target definitions.
#
################################################################################


################################################################################
# Install and include locations.
################################################################################

#
# 
#

# set prefix to root of svn dir if not passed.
# making it a path fixes it as an anchor for subdirectories.
ifndef PREFIX
  ifdef DES_HOME
    PREFIX=${DES_HOME}
  else
    PREFIX= $(shell (cd ..; pwd))
  endif
endif

CURDIR=$(shell pwd)

#
# working for backwards compat, locate libimsupport.a
#  Rule: if eups product use it.
#        if DES Home, use it (greg style stack)
#        n.b IMSUPPORT_DIR can be set with a setup -r .  from imsuport/trunk (or similar)

$(info IMSUPPORT_DIR $(IMSUPPORT_DIR))
ifeq ("$(IMSUPPORT_DIR)", "")
   LIBDETECT = $(wildcard $(DES_HOME)/lib/libimsupport.a)
   $(info LIBDETECT $(LIBDETECT))
   ifneq ("$(LIBDETECT)","")
	 IMSUPPORT_DIR := $(DES_HOME)
   endif
   $(info IMSUPPORT_DIR $(IMSUPPORT_DIR))
endif


ifeq ("$(IMSUPPORT_DIR)", "")
   $(warn imsupport undefined)
endif 
IMSUPPORT_LIB := ${IMSUPPORT_DIR}/lib/
IMSUPPORT_INCLUDE := ${IMSUPPORT_DIR}/include  
LIBIMSUPPORT=${IMSUPPORT_LIB}libimsupport.a


SELF_SRC= $(shell pwd)

ifndef FITSIO_BASE
  ifdef DES_PREREQ
    FITSIO_BASE=${DES_PREREQ}
  else
    FITSIO_BASE=/usr
  endif
endif


ifndef NRECIPES_BASE
  ifdef DES_PREREQ
     NRECIPES_BASE=${DES_PREREQ}
  else
    NRECIPES_BASE=/usr/local
  endif
endif

ifndef FFTW_BASE
  ifdef DES_PREREQ
    FFTW_BASE=${DES_PREREQ}
  else
    FFTW_BASE = /usr/local
  endif
endif

FITSIO_LIB = $(FITSIO_BASE)/lib
#FITSIO_INCLUDE = $(FITSIO_BASE)/cfitsio/include
FITSIO_INCLUDE = $(FITSIO_BASE)/include
CCFITS_INCLUDE = $(FITSIO_INCLUDE)/CCfits
NRECIPES_LIB = $(NRECIPES_BASE)/lib
FFTW_LIB=$(FFTW_BASE)/lib
FFTW_INCLUDE=$(FFTW_BASE)/include

#
# Oracle cleanup story
# ORACLE_INC_DIR ORACLE_LIB_DIR have to be passed in somehow
# the EUPS SETUP ORACLE_INSTANTCLIENT sets these
#

ORACLE_INC = -I ${ORACLE_INC_DIR} 
ORACLE_LIB = -L${ORACLE_LIB_DIR}/ -lclnts



ifdef DES_PREREQ
  PSFEX_INCLUDE := ${DES_PREREQ}/include/psfex ${DES_PREREQ}/include/psfex/fits ${DES_PREREQ}/include/psfex/wcs ${DES_PREREQ}/include/fits ${DES_PREREQ}/include/wcs
  SWARP_INCLUDE := ${DES_PREREQ}/include/swarp
  PSFEX_LIB := ${DES_PREREQ}/lib/psfex
  SWARP_LIB := ${DES_PREREQ}/lib/swarp
  WCS_INCLUDE := ${DES_PREREQ}/include
  WCS_LIB := ${DES_PREREQ}/lib
  PLPLOT_LIB := ${DES_PREREQ}/lib
  PLPLOT_INCLUDE := ${DES_PREREQ}/include/plplot

else
  PSFEX_INCLUDE := ../../../terapix/trunk/psfex/src
  SWARP_INCLUDE := ../../../terapix/trunk/swarp/src
  PSFEX_LIB := ../../../terapix/trunk/psfex/src
  SWARP_LIB := ../../../terapix/trunk/swarp/src

endif

LIBIMSUPPORT :=  $(IMSUPPORT_LIB)/libimsupport.a
FITSIO :=       $(FITSIO_LIB)/libcfitsio.so
NR2 :=          $(NRECIPES_LIB)/libpress2ndkr.a
NR2D :=         $(NRECIPES_LIB)/libpress2ndkrD.a
FFTW := 	$(FFTW_LIB)/libfftw3f.a
FFTW_THREADS :=	$(FFTW_LIB)/libfftw3f_threads.a
#WCS_C :=        $(DES_PREREQ)/lib/psfex/wcs/libwcs_c.a
WCS_C :=        $(DES_PREREQ)/lib/wcs/libwcs_c.a
# TPIX_FITS :=    $(DES_PREREQ)/lib/psfex/fits/libfits.a
TPIX_FITS :=    $(DES_PREREQ)/lib/fits/libfits.a
#TPIX_FITS :=    $(PSFEX_LIB)/fits/libfits.a
ORACLE :=    $(ORACLE_LIB)/libclntsh.a

BINDIR := 	$(PREFIX)/bin
LIBDIR := 	$(PREFIX)/lib

#INCLUDE :=    	$(FITSIO_INCLUDE) $(FFTW_INCLUDE) $(PSFEX_INCLUDE) $(SWARP_INCLUDE) ${WCS_INCLUDE}
#LIBS = $(FITSIO_LIB):$(NRECIPES_LIB):$(FFTW_LIB):$(PSFEX_LIB):$(SWARP_LIB):${WCS_LIB}

## Temp addition for WCSTOOLS library

DMWCS_INCLUDE := ${DES_PREREQ}/include
DMWCS_LIB := ${DES_PREREQ}/lib
DMWCS := ${DMWCS_LIB}/libwcs.a 

## Temp addition for PLPLOT library




INCLUDE :=    $(SELF_SRC) $(FITSIO_INCLUDE) $(FFTW_INCLUDE) $(PSFEX_INCLUDE) $(SWARP_INCLUDE) ${WCS_INCLUDE} ${DMWCS_INCLUDE} ${PLPLOT_INCLUDE} ${ORACLE_INC} $(CCFITS_INCLUDE) $(IMSUPPORT_INCLUDE)
LIBS = $(FITSIO_LIB):$(NRECIPES_LIB):$(FFTW_LIB):$(PSFEX_LIB):$(SWARP_LIB):${WCS_LIB}:${DMWCS_LIB}:${PLPLOT_LIB}:$(ORACLE_LIB)


# END Temp addition
#ifdef DES_PREREQ
#DATABASE:=$(LIBDIR)
#else 
DATABASE:=$(CURDIR)/Database
#endif

# ROOT libraries
ROOTCONFIG   := root-config
ROOTFLAGS    := $(shell $(ROOTCONFIG) --cflags)
ROOTLIBS     := $(shell $(ROOTCONFIG) --libs) -lMinuit2 -lTMVA -lMinuit \
		 -lXMLIO -lMLP -lTreePlayer


# We could add all the headers to this list to force rebuilds 
# if a header changes
HEADERS := fitsio.h fft.h fitswcs.h imageproc.h

# Paths to search for dependecies:
vpath %.h $(INCLUDE) 
vpath %.a $(LIBS)
vpath %.o $(LIBS)

################################################################################
# This is a default set of CC and CFLAGS settings.  they get overridden by anything
# passed in from a higher level make call
################################################################################
# gcc
CC :=		gcc
CPPC := 	g++
CPPFLAGS:=      -g -O3
CPPCOPTS :=	-c -Wall -funroll-loops -O3 -Wno-unused-variable 
FC :=		f77
CFLAGS := 	-O3 -g -Wall -Wno-unused-variable #-ftree-vectorize -ffast-math -msse2 -ftree-vectorizer-verbose=6  -DTRY2VECT
# Intel
#CC :=		icc
#CPPC := 	icpc
#CPPCOPTS :=	-O2 -w -g
#FC :=		ifort
#CFLAGS := 	-O2 -w -g

override CPPFLAGS += $(addprefix -I ,$(INCLUDE))
override CFLAGS += $(addprefix -I ,$(INCLUDE))

################################################################################
# Targets for all executables:
# Right now a target needs to be here for the executable to get built
# installed and cleaned up correctly.  The targets also need a build rule 
# defined.
# Separate lists can be defined if needed to define a alternate target, like 
# a set of related codes.
###############################################################################

#EXE_set1 = blah

EXE_REST =   imcorrect			\
        Mosaic2_crosstalk               \
	DECam_crosstalk	                \
	MegaCam_crosstalk               \
	LCOMegaCam_crosstalk	        \
	xtalk                           \
	detrend				\
	mksupersky 			\
	mkmask				\
	mkbiascor			\
	mkdarkcor			\
	mkflatcor			\
	mkphotflatcor			\
	psfnormalize 			\
	mkmanglefile 			\
	wcsexample                      \
        pix2wcs                         \
	mkbpm				\
	editbpm 			\
	fitscombine	 		\
	mkillumcor 			\
	imsim1_convert	 		\
	imreadRADEC			\
        splitimage 			\
	fitsheaderfix			\
	reset_zeropoint 		\
	grabmatches 			\
	fwhm				\
	fixradec 			\
	test_filename_resolve 		\
	test_filename_construct 	\
        mosaictile 			\
	check_hdu 			\
	check_keyword 			\
	checklist 			\
	runSExtractor 			\
	usno_input 			\
	quick_coadd 			\
	image_chi2 			\
	listconvert 			\
	coadd_calczp	 		\
	new_coadd_calczp		\
	coadd_catalog			\
	coadd_detection 		\
	coadd_fluxscale 		\
	coadd_fmatch 			\
	coadd_grabmatches 		\
	coadd_grabmatches_withtruthid	\
	coadd_meanoffset 		\
	coadd_photozp			\
	coadd_printimage		\
	coadd_select 			\
	coadd_selectimage		\
	coadd_swarp			\
	coadd_makemap			\
	copy_objects 			\
	get_FWHM 			\
	get_ellipticity 		\
	get_skybrite 			\
	get_imstats 			\
	check_remap 			\
	skycoverage 			\
	TileFinder			\
	PixToRADec			\
	RADecToPix			\
	photcal				\
	photcheck			\
	photcheck_full			\
        makeTemp                        \
        data_qual                       \
        searchAdd		        \
	get_corners			\
	cal_phot
#
# This list records programs we think are depricated.
# Very sorry if we made a mistake, doing the best we can.
#
EXE_DEPRICATED = \
	detrend				\

EXE_NOT_CP = \
	mkmanglefile 			\


EXE_REST =  		\
	roverscan                       \
        subimg                          \
	xchipfltnrm                     \
	MegaCam_crosstalk               \
	LCOMegaCam_crosstalk	        \
	xtalk                           \
	mkphotflatcor			\
	psfnormalize 			\
	wcsexample                      \
        pix2wcs                         \
	mkbpm				\
	editbpm 			\
	imsim1_convert	 		\
	imreadRADEC			\
        splitimage 			\
	fitsheaderfix			\
	reset_zeropoint 		\
	grabmatches 			\
	fixradec 			\
	test_filename_resolve 		\
	test_filename_construct 	\
        mosaictile 			\
	check_hdu 			\
	check_keyword 			\
	checklist 			\
	usno_input 			\
	quick_coadd 			\
	astrometry 			\
	QA_astrometry 			\
	image_chi2 			\
	listconvert 			\
	coadd_calczp	 		\
	new_coadd_calczp		\
	coadd_catalog			\
	coadd_detection 		\
	coadd_fluxscale 		\
	coadd_fmatch 			\
	coadd_grabmatches 		\
	coadd_meanoffset 		\
	coadd_photozp			\
	coadd_printimage		\
	coadd_select 			\
	coadd_selectimage		\
	coadd_swarp			\
	cp_coadd_swarp                  \
	coadd_makemap			\
	copy_objects 			\
	get_FWHM 			\
	get_ellipticity 		\
	get_skybrite 			\
	get_imstats 			\
	check_remap 			\
	skycoverage 			\
	TileFinder			\
	photcal				\
	photcheck			\
	photcheck_full			\
        makeTemp                        \
        data_qual                       \
        searchAdd		        \
	get_corners			\
	get_corners_from_image		\
	cal_phot                        \
	update_wcs			\
        imcopy 				\
	imreject
EXE = ${EXE_REST}


# Sample add another list:
# EXE = ${EXE_REST} ${EXE_set1}

# Sample add another target:
#.PHONY: somecodes
#somecodes: ${EXE_set1}

# Target to make all executables:
.PHONY: all
all:  ${LIBIMSUPPORT}  imdetect_exes $(EXE) 

# Imageutils.a for standalone case must already exist
# if you get here -- do this by invoking 'make standalone' 
# from one level up

standalone: ${LIBIMSUPPORT} imdetrend_exes imdetect_exes mkbpm

imdetect_exes:
	(cd imdetect; $(MAKE) CFLAGS="$(CFLAGS)" LIBIMSUPPORT=$(LIBIMSUPPORT)  $(MAKEFLAGS))



#############################################################
#############################################################
#          Begin object build rules section
#
# No object (*.o) targets should have to be explicitly defined
# unless there are special flags needed for that object.
#
#############################################################
#############################################################

# Special object targets all other obejcts will be built by
# the implicit pattern rule:
%.o : %.c $(HEADERS)
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@
%.o : %.cc
	$(CPPC) $(CPPFLAGS) $(CPPCOPTS) -c $^ -o $@




#############################################################
#############################################################
#          Begin Executable build rules section
#
#  Right now all rules are explicitly defined for all 
#  executable targets.  They have been grouped by dependencies
#  then external linkage.  At some point it may be worthwile 
#  to write a few rules that operate on a list of targets
#  that all share the same dependencies and linkage.
#
#############################################################
#############################################################

# "Solo" targets
listconvert: listconvert.o
	$(CC) $(CFLAGS) -o $@ $^

fixradec:  fixradec.o
	$(CC) $(CFLAGS) -o $@ $^ -lm
usno_input: usno_input.o
	$(CC) $(CFLAGS) -o $@ $^ -lm

check_hdu: check_hdu.o
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm
check_keyword: check_keyword.o
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm
checklist: checklist.o 
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm

photometry_mean:  photometry_mean.o 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) -lm

sexcombine: sexcombine.o 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
correcctRADEC: correcctRADEC.o 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
image_chi2: image_chi2.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
overscan:  overscan.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
quick_coadd: quick_coadd.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
coadd_zp:  coadd_zp.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
grabmatches_Imsim2_truth: grabmatches_Imsim2_truth.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
grabmatches_usnob: grabmatches_usnob.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
QA_astrometry: QA_astrometry.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
get_corners:	get_corners.o fitswcs.o
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) -lm
get_corners_from_image:	get_corners_from_image.o ${LIBIMSUPPORT}  fitswcs.o  
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(FITSIO) -lm

# Catsubs:
USNOB_convert: USNOB_convert.o catsubs.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
SExcat_convert: SExcat_convert.o catsubs.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm


# archivesubs:
test_filename_resolve:  test_filename_resolve.o  ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ -lm
test_filename_construct:  test_filename_construct.o  ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ -lm


grabmatches_cpp.o: grabmatches_cpp.cc 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

# ${LIBIMSUPPORT}
grabmatches: grabmatches.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -L$(PLPLOT_LIB) -o $@ $^ $(NR2) $(FITSIO) -lplplotd -lm
grabmatches_cpp: grabmatches_cpp.o ${LIBIMSUPPORT} 
	$(CPPC) $(CPPFLAGS)  $(ROOTLIBS) $(ROOTCFLAGS) -L$(PLPLOT_LIB) -o $@ $^ $(NR2) $(FITSIO) -lplplotd -lm
grabmatches_exposure: grabmatches_exposure.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -L$(PLPLOT_LIB) -o $@ $^ $(NR2) $(FITSIO) -lplplotd -lm
reset_zeropoint: reset_zeropoint.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
copy_objects: copy_objects.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
merge_nite_partition: merge_nite_partition.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
coadd_fmatch: coadd_fmatch.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
coadd_select: coadd_select.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
get_FWHM: get_FWHM.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
get_ellipticity: get_ellipticity.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
get_skybrite: get_skybrite.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
skycoverage: skycoverage.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
check_scamp: check_scamp.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
check_PSM: check_PSM.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
scampmkcat: scampmkcat.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
BCSfix: BCSfix.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
coadd_catalogingest: coadd_catalogingest.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm

# ${LIBIMSUPPORT} ${LIBIMSUPPORT}
runCatalog_ingest_partition: runCatalog_ingest_partition.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm


# ${LIBIMSUPPORT} 
photcal: photcal.o ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm


# ${LIBIMSUPPORT} 
mosaictile: mosaictile.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm
TNXtoTANPV: TNXtoTANPV.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm
check_remap: check_remap.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm

imreadRADEC:  imreadRADEC.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR1) $(FITSIO) -lm

editbpm: editbpm.o ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
imsim1_convert: imsim1_convert.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
splitimage: splitimage.o ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
correctRADEC: correctRADEC.o imsubs.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
fitsheaderfix: fitsheaderfix.o ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
update_wcs:update_wcs.o ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm 
imcopy:imcopy.o
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm 

roverscan: CallOverScan.o ${LIBIMSUPPORT}   
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
xchipfltnrm: xchip_flatnorm.o ${LIBIMSUPPORT}  
	$(CPPC) -o $@ $^ -L$(DES_PREREQ)/lib $(FITSIO) -lCCfits -lm
subimg: SubImg.o ${LIBIMSUPPORT}   
	$(CPPC) -o $@ $^ -L$(DES_PREREQ)/lib -lCCfits -lm
MegaCam_crosstalk: MegaCam_crosstalk.o ${LIBIMSUPPORT}   
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm
LCOMegaCam_crosstalk: LCOMegaCam_crosstalk.o ${LIBIMSUPPORT}   
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) -lm

astrometry: astrometry.o ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
binary2ascii: binary2ascii.o ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
xtalk.o: xtalk.cxx
	$(CPPC) $(CPPFLAGS) -I ${DES_PREREQ}/include/CCfits $(ROOTFLAGS) -c $^ -o $@
xchip_flatnorm.o: xchip_flatnorm.cc 
	$(CPPC) $(CPPFLAGS) $(CPPCOPTS) -I ${DES_PREREQ}/include/CCfits $(ROOTFLAGS) -c $^ -o $@
xtalk: xtalk.o
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -L ${DES_PREREQ}/lib -lCCfits -lm
# Tile finder for CP
PixTools.o: PixTools.cpp
	$(CPPC) $(CPPFLAGS) -I ${DES_PRER}/include  -c $^ -o $@
#
#TangentPoint.o: TangentPoint.cpp
#	$(CPPC) $(CPPFLAGS) -I ${DES_PRER}/include  -c $^ -o $@
TileFinder.o: TileFinder.cpp
	$(CPPC) $(CPPFLAGS) -I ${DES_PREREQ}/include/  -c $^ -o $@
PixToRADec.o: PixToRADec.cpp
	$(CPPC) $(CPPFLAGS) -I ${DES_PREREQ}/include/  -c $^ -o $@
RADecToPix.o: RADecToPix.cpp
	$(CPPC) $(CPPFLAGS) -I ${DES_PREREQ}/include/  -c $^ -o $@
#
TileFinder: TileFinder.o PixTools.o
	$(CPPC) $(CPPFLAGS)  -o $@ $^   -L ${DES_PREREQ}/lib  -lm
PixToRADec: PixToRADec.o PixTools.o
	$(CPPC) $(CPPFLAGS)  -o $@ $^   -L ${DES_PREREQ}/lib  -lm
RADecToPix: RADecToPix.o PixTools.o
	$(CPPC) $(CPPFLAGS)  -o $@ $^   -L ${DES_PREREQ}/lib  -lm
#
# ${LIBIMSUPPORT} ${LIBIMSUPPORT} 
coadd_fluxscale: coadd_fluxscale.o  ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
coadd_chi2detection: coadd_chi2detection.o  ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
coadd_meanoffset: coadd_meanoffset.o  ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm


# ${LIBIMSUPPORT}  archivesubs.o 
imreject: imreject.o ${LIBIMSUPPORT}
	$(CPPC) $(CPPFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
get_imstats: get_imstats.o ${LIBIMSUPPORT}    
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
mkphotflatcor: mkphotflatcor.o ${LIBIMSUPPORT} fitswcs.o
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) $(WCS_C) $(TPIX_FITS) -lm
mkbpm: mkbpm.o ${LIBIMSUPPORT}   
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm

# ${LIBIMSUPPORT}   archivesubs.o
photcheck: photcheck.o    ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm
photcheck_full: photcheck_full.o    ${LIBIMSUPPORT} 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm


#  ${LIBIMSUPPORT}  coadd_utils.o 
makeTemp: makeTemp.o  ${LIBIMSUPPORT}  coadd_utils.o 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm

#  ${LIBIMSUPPORT}  coadd_utils.o 
searchAdd: searchAdd.o  ${LIBIMSUPPORT}  coadd_utils.o 
	$(CC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm

# ${LIBIMSUPPORT}  fft.o psfpoly.o vignet.o
psfnormalize: psfnormalize.o ${LIBIMSUPPORT}  fft.o psfpoly.o vignet.o
	$(CC) $(CFLAGS) -o $@ $^ $(FITSIO) $(FFTW) $(FFTW_THREADS) -lpthread -lm 

# 
# Kludge here as this will, I think move to a mangle product
#
mkmanglefile: mkmanglefile.o ${LIBIMSUPPORT} fitswcs.o tdeswcs.o
	$(CC) $(CFLAGS) -o $@ $^ $(ORACLE_INC) $(ORACLE_LIB) $(WCS_C) $(TPIX_FITS)  -lm
#mkmanglefile: mkmanglefile.o ${LIBIMSUPPORT} deswcs.o fitswcs.o


mkCoaddMagList: mkCoaddMagList.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(ORACLE_LIB)
mkCoaddModelList: mkCoaddModelList.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(ORACLE_LIB)
mkCoaddSpreadModelList: mkCoaddSpreadModelList.o ${LIBIMSUPPORT}
	$(CC) $(CFLAGS) -o $@ $^ $(ORACLE_LIB)


mktilefile: mktilefile.o ${LIBIMSUPPORT} tileutils.o
	$(CC) $(CFLAGS) -o $@ $^ $(ORACLE_LIB) -L$(WCS_LIB) -lwcs
wcsexample: wcsexample.o ${LIBIMSUPPORT} fitswcs.o
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) -lm
pix2wcs: pix2wcs.o ${LIBIMSUPPORT} fitswcs.o
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) -lm

coadd_fluxscale_redimg: coadd_fluxscale_redimg.o wcssubs.o fitswcs.o  ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
coadd_fluxscale_backup: coadd_fluxscale_backup.o wcssubs.o fitswcs.o  ${LIBIMSUPPORT}  
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm

coadd_selectimage: coadd_selectimage.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
coadd_photozp: coadd_photozp.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
coadd_printimage: coadd_printimage.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm

coadd_calczp: coadd_calczp.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o 
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
coadd_calczp_test: coadd_calczp_test.o Afitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o 
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
coadd_swarp: coadd_swarp.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o   
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
cp_coadd_swarp: cp_coadd_swarp.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o   
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm

coadd_detection: coadd_detection.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o  
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
coadd_catalog: coadd_catalog.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o  
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm
coadd_makemap: coadd_makemap.o fitswcs.o  ${LIBIMSUPPORT}  coadd_utils.o  
	$(CC) $(CFLAGS) -o $@ $^ $(WCS_C) $(TPIX_FITS) $(NR2) $(FITSIO) -lm


coadd_grabmatches: coadd_grabmatches.o ${LIBIMSUPPORT}  subs_lambert.o ${LIBIMSUPPORT}
	gcc $(CFLAGS) -o coadd_grabmatches coadd_grabmatches.c subs_lambert.c ${LIBIMSUPPORT}  $(DMWCS) -lm


detrend: detrend.o detrend_subs.o ${LIBIMSUPPORT}   
	$(CPPC) $(CFLAGS) -o $@ $^ $(NR2) $(FITSIO) -lm

# data quality
SysErrFit.o: SysErrFit.cxx 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@



ShearFit.o: ShearFit.cxx 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

data_qual.o: data_qual.cc 
	$(CPPC) $(CPPFLAGS) -I ./ $(ROOTFLAGS) -c $^ -o $@

data_qual_image.o: data_qual_image.cc 
	$(CPPC) $(CPPFLAGS) -I ./ $(ROOTFLAGS) -I ${DES_PREREQ}/include/CCfits -c $^ -o $@

match_root.o: match_root.cc 
	$(CPPC) $(CPPFLAGS) -I ./  -I ${DES_PREREQ}/include/CCfits $(ROOTFLAGS) -c $^ -o $@

cal_phot.o: cal_phot.cc
	$(CPPC) $(CPPFLAGS) -I ./  -I ${DES_PREREQ}/include/CCfits $(ROOTFLAGS) -c $^ -o $@

Utils.o: Utils.h Utils.cxx 
	$(CPPC) $(CPPFLAGS) -I ./  -I ${DES_PREREQ}/include/CCfits $(ROOTFLAGS) -c Utils.cxx -o $@

data_qual: data_qual.o SysErrFit.o ShearFit.o  Object.o \
	ObjectDict.o 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -lm

data_qual_nite.o: data_qual_nite.cc 
	$(CPPC) $(CPPFLAGS) -I./ $(ROOTFLAGS) -c $^ -o $@

new_coadd_calczp.o: new_coadd_calczp.cc 
	$(CPPC) $(CPPFLAGS) -I./ $(ROOTFLAGS) -c $^ -o $@

psm_cal.o: psm_cal.cc 
	$(CPPC) $(CPPFLAGS) -I./ $(ROOTFLAGS) -c $^ -o $@

data_qual_nite: data_qual_nite.o SysErrFit.o ExtinctionMap.o Object.o \
	ObjectDict.o 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -lm \
        $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

psm_cal: psm_cal.o Object.o ObjectDict.o 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -lm \

new_coadd_calczp: new_coadd_calczp.o SysErrFit.o ExtinctionMap.o Object.o \
	ObjectDict.o 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -lm \
        $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

shear_ccd: shear_ccd.o ShearFit.o ExtinctionMap.o Object.o \
	ObjectDict.o 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -lm \
        $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

data_qual_image: data_qual_image.o ExtinctionMap.o Object.o \
	ObjectDict.o  SysErrFit.o
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -lm \
        $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

match_root: match_root.o Object.o ObjectDict.o 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) -lm \
        $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

slr_check: slr_check.o FitSLR.o ColorPoint.o \
	 ExtinctionMap.o Object.o ObjectDict.o
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

slr_2mass: slr_2mass.o FitSLR.o ColorPoint.o \
         ExtinctionMap.o Object.o ObjectDict.o
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
         -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

slr_2mass_3color: slr_2mass_3color.o FitSLR.o ColorPoint.o \
         ExtinctionMap.o Object.o ObjectDict.o
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
         -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits


find_matches:find_matches.o 
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

check_jitter:check_jitter.o 
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

check_zp:check_zp.o  Object.o 	 ExtinctionMap.o 
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits


cal_phot:cal_phot.o libDB  Utils.o 
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $@.o $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits -L$(DATABASE) -lDB Utils.o

cal_phot1:cal_phot.o Utils.o 
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o cal_phot cal_phot.o $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits -L$(DATABASE) -lDB Utils.o


slr_cal: slr_cal.o \
	 ExtinctionMap.o FitSLR.o Object.o ObjectDict.o
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

get_se: get_se.o \
	 ExtinctionMap.o FitSLR.o Object.o ObjectDict.o
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) \
	 -lm $(DMWCS) -L ${DES_PREREQ}/lib -lCCfits

coadd_drawmap: coadd_drawmap.o
	 $(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -o $@ $^ $(ROOTLIBS) 


ExtinctionMap.o: ExtinctionMap.cxx 
	$(CPPC) $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@


libDB:
	cd Database;make lib	

slr_check.o: slr_check.cc 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

slr_2mass.o: slr_2mass.cc
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

slr_2mass_3color.o: slr_2mass_3color.cc
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

shear_ccd.o: shear_ccd.cc 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

coadd_drawmap.o: coadd_drawmap.cc 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

slr_cal.o: slr_cal.cc 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

get_se.o: get_se.cc 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

find_matches.o: find_matches.cc 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

check_jitter.o: check_jitter.cc
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

check_zp.o: check_zp.cc
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

ColorPoint.o: ColorPoint.cxx 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

Object.o: Object.cxx 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

ObjectDict.o: ObjectDict.cxx  
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

ObjectDict.cxx: Object.h 
	rootcint -f $@ -c $^ LinkDef.h

FitSLR.o: FitSLR.cxx 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@

FitSLRCal.o: FitSLRCal.cxx 
	$(CPPC) -I./ $(CPPFLAGS) $(ROOTFLAGS) -c $^ -o $@




################################################################################
# Install
################################################################################
install:install_lib
	@echo "Installing to ${BINDIR}"; \
	mkdir -p ${BINDIR};\
	for i in ${EXE}; do \
	  if [ -f $$i ]; then \
	    cp $$i ${BINDIR}/$$i; \
	  fi; \
	done;
	(cd imdetect;  $(MAKE) install)
	cp  ../lib/*.a ${LIBDIR}  || true; 

################################################################################
# Install Lib
################################################################################
install_lib:
	@if [ -r $(CURDIR)/Database/libDB.so ]; then \
		echo "Installing to ${LIBDIR}"; \
		mkdir -p ${LIBDIR};\
		cp $(CURDIR)/Database/libDB.so ${LIBDIR}; \
	fi


################################################################################
# Clean-up rules.
################################################################################
clean: 
	(cd imdetect ; $(MAKE)  clean)
	@echo "Removing object files..."; \
	rm -f *.o; \
        echo "Removing executables..."; \
	for i in ${EXE}; do \
	  if [ -f $$i ]; then \
	    rm -f $$i; \
	  fi; \
	done;
	rm -f Database/*.o
	rm -f Database/*.so
	rm -f Database/DBDict.h
	rm -f Database/DBDict.cxx
	rm -f lib/*.so
	rm -f ObjectDict.cxx ObjectDict.h

clean_install:
	@echo "Removing installed executables..."; \
	for i in ${EXE}; do \
	  if [ -f ${BINDIR}/$$i ]; then \
	    rm -f ${BINDIR}/$$i; \
	  fi; \
	done;
