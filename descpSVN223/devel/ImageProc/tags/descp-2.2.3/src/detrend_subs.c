/*
/* Subroutine to read and execute basic detrending of CCD image */
/*
/*
*/

#include "imageproc.h"

#define BADPIX_BPM 1   	     /* set in bad pixel mask (hot/dead pixel/column) */
#define BADPIX_SATURATE 2    /* saturated pixel */
#define BADPIX_INTERP 4
#define BADPIX_THRESHOLD 0.25/* pixels less than this fraction of sky     */
			     /* are filtered -- helps remove failed reads */
#define BADPIX_MASK   8      /* too little signal- i.e. poor read */

/* List of control flags */
#define FLG_OUTPUT 		ctrl_flags[0]
#define FLG_OVERSCAN 		ctrl_flags[1]
#define FLG_BIAS 		ctrl_flags[2]
#define FLG_FLATTEN 		ctrl_flags[3]
#define FLG_BPM 		ctrl_flags[4]
#define FLG_PUPIL 		ctrl_flags[5]
#define FLG_ILLUMINATION 	ctrl_flags[6]
#define FLG_FRINGE 		ctrl_flags[7]
#define FLG_VARIANCE 		ctrl_flags[8]
#define FLG_INTERPOLATE		ctrl_flags[9]
#define FLG_UPDATESKYBRITE 	ctrl_flags[10]
#define FLG_UPDATESKYSIGMA 	ctrl_flags[11]
#define FLG_MEF	 		ctrl_flags[12]



void detrend(data,outputimagename,bias,flat,bpm,illumination,fringe,pupil,
	ctrl_flags,longcomment,scaleregionn,scale_interpolate,filter,
	ccdnum,flag_verbose)
	desimage *data,*bias,*flat,*bpm,*illumination,*fringe,*pupil;
	char outputimagename[],filter[],longcomment[];
	int ctrl_flags[],scaleregionn[],flag_verbose,*ccdnum;
	float scale_interpolate;
{
	int	hdunum,x,y,nfound,imtype,c,
		xmin,xmax,anynull,maskedpixels,interppixels,
		saturatepixels,mkpath(),i,keysexist,j,n,nkeys,
		scalenum,
		imnum,imoutnum,im,hdutype,flag_impupil=NO,
		flag_imoverscan,flag_imbias,flag_imflatten,
		flag_imillumination,flag_imfringe,
		seed=-15,xlow,xhi,flag_mef=NO;
	static int status=0;
	double	dbzero,dbscale,sumval;
	long	axes[2],naxes[2],pixels,npixels,bscale,bzero,bitpix,fpixel;
	char	comment[1000],imagename[1000],
		bpmname[1000],rootname[1000],varimname[1000],outputlist[1000],
		*striparchiveroot(),event[10000],
		keycard[100],keyname[10],scaleregion[100];
	float	scale,offset,gasdev(),maxsaturate,
		overscanA=0.0,overscanB=0.0,scalefactor,mode,
		*scalesort,skybrite,skysigma,thresholdval;
	void	rd_desimage(),headercheck(),printerror(),decodesection(),
		readimsections(),overscan(),retrievescale(),reportevt();
	desimage output;
	time_t	tm;
	/* list of keywords to be removed from the header after imcorrect */
	char	delkeys[100][10]={"CCDSUM","AMPSECA","AMPSECB","TRIMSEC",
		"BIASSECA","BIASSECB","GAINA","GAINB","RDNOISEA",
		"RDNOISEB","SATURATA","SATURATB",""};


	  /* ************************************** */
	  /* ***** Read Input Image Section  ****** */
	  /* ************************************** */

	  if (FLG_OUTPUT) { /* writing output to new image */
	    rd_desimage(data,READONLY,flag_verbose);  
	    sprintf(output.name,"!%s",outputimagename);
	  }
	  else  /* overwriting input image */
	    rd_desimage(data,READWRITE,flag_verbose);
	  
	  /* prepare output image */
	  output.npixels=data->npixels;
	  output.nfound=data->nfound;
	  for (i=0;i<output.nfound;i++) output.axes[i]=data->axes[i];
	  output.bitpix=FLOAT_IMG;
	  output.saturateA=data->saturateA;output.saturateB=data->saturateB;
	  output.gainA=data->gainA;output.gainB=data->gainB;
	  output.rdnoiseA=data->rdnoiseA;output.rdnoiseB=data->rdnoiseB;
	  if (output.saturateA>output.saturateB) maxsaturate=output.saturateA;
	  else maxsaturate=output.saturateB;

		  
	  /* ************************************** */
	  /* ***** READING IMAGE SECTIONS    ****** */
	  /* ************************************** */

	  if (flag_verbose==3) {
	    sprintf(event,"Scaleregion=[%d:%d,%d,%d] Filter=%s CCDNUM=%d",
	      scaleregionn[0],scaleregionn[1],scaleregionn[2],scaleregionn[3],
	      filter,*ccdnum);	
	    reportevt(flag_verbose,STATUS,1,event);
	  }
	  
	  /* retrieve basic image information from header */
	  if (fits_movabs_hdu(data->fptr,1,&hdutype,&status)) {
	    sprintf(event,"Move to HDU=1 failed: %s",data->name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* confirm correct filter and ccdnum */
	  headercheck(data,filter,ccdnum,"NOCHECK",flag_verbose);
	  /* get the DATASEC information */
	  if (fits_read_key_str(data->fptr,"DATASEC",data->datasec,
	    comment,&status) ==KEY_NO_EXIST) {
	    sprintf(event,"Keyword DATASEC not found: %s",data->name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
          }
          decodesection(data->datasec,data->datasecn,flag_verbose);

	  /* ************************************** */
          /* ****** Set up Scaling Vectors ******** */
	  /* ************************************** */
          scalenum=(scaleregionn[1]-scaleregionn[0])*
            (scaleregionn[3]-scaleregionn[2]);
          scalesort=(float *)calloc(scalenum,sizeof(float));
          if (scalesort==NULL) {
            reportevt(flag_verbose,STATUS,5,"Calloc of scalesort failed");
            exit(0);
          }
  
	  /* ************************************** */
	  /* ***** OVERSCAN and TRIM Section ****** */
	  /* ************************************** */

	  if (FLG_OVERSCAN) {
	    if (fits_read_keyword(data->fptr,"DESOSCN",comment,comment,
	      &status)==KEY_NO_EXIST) {
	      flag_imoverscan=1;
	      status=0; /* reset flag */
	      /* get the required header information */
	      /* get the BIASSEC information */          
	      if (fits_read_key_str((data->fptr),"BIASSECA",(data->biasseca),
	          comment,&status)==KEY_NO_EXIST) {
	        sprintf(event,"Keyword BIASSECA not defined: %s",data->name);
	        reportevt(flag_verbose,STATUS,5,event);
	        printerror(status);
	      }
	      decodesection((data->biasseca),(data->biassecan),flag_verbose);
    
	      /* get the BIASSEC information */
	      if (fits_read_key_str(data->fptr,"BIASSECB",data->biassecb,
	        comment,&status) ==KEY_NO_EXIST) {
	        sprintf(event,"Keyword BIASSECB not defined: %s",data->name);
	        reportevt(flag_verbose,STATUS,5,event);
	        printerror(status);
	      }
	      decodesection(data->biassecb,data->biassecbn,flag_verbose);

	     /* get the AMPSEC information */
	      if (fits_read_key_str(data->fptr,"AMPSECA",data->ampseca,
	        comment,&status)==KEY_NO_EXIST) {
	        sprintf(event,"Keyword AMPSECA not defined: %s",data->name);
	        reportevt(flag_verbose,STATUS,5,event);
	        printerror(status);
	      }
	      decodesection(data->ampseca,data->ampsecan,flag_verbose);

	      /* get the AMPSEC information */
	      if (fits_read_key_str(data->fptr,"AMPSECB",data->ampsecb,
	        comment,&status) ==KEY_NO_EXIST) {
	        sprintf(event,"Keyword AMPSECB not defined: %s",data->name);
	        reportevt(flag_verbose,STATUS,5,event);
	        printerror(status);
	      }
	      decodesection(data->ampsecb,data->ampsecbn,flag_verbose);
    
	      /* get the TRIMSEC information */
	      if (fits_read_key_str(data->fptr,"TRIMSEC",data->trimsec,
	        comment,&status) ==KEY_NO_EXIST) {
	        sprintf(event,"Keyword TRIMSEC not defined: %s",data->name);
	        reportevt(flag_verbose,STATUS,5,event);
	        printerror(status);
	      }
	      decodesection(data->trimsec,data->trimsecn,flag_verbose);
	      /* report header parameters read */
	      if (flag_verbose==3) {
                sprintf(event,"BIASSECA=%s AMPSECA=%s BIASSECB=%s AMPSECB=%s TRIMSEC=%s DATASEC=%s",
                  data->biasseca,data->ampseca,data->biassecb,data->ampsecb,
		  data->trimsec,data->datasec);
                reportevt(flag_verbose,STATUS,1,event);
	      }
	      reportevt(flag_verbose,STATUS,1,"OVERSCAN correcting");
	      /* carry out overscan */
	      overscan(data,&output,flag_verbose);
	      status=0;
	      /* update dataset within the output image */
	      sprintf(output.datasec,"[%d:%d,%d:%d]",1,output.axes[0],
	 	1,output.axes[1]);
	    }
	    else {
	      reportevt(flag_verbose,STATUS,1,"Already OVERSCAN corrected");
	      flag_imoverscan=0; 
	    }
	  }
	  else {
	    /* simply copy the data into the output array  */
	    /* note that this assumes input image has already been
	    overscan/trimmed*/
	    /* need to make this robust-- check for trimming */
	    sprintf(event,"Copying image array");
	    output.image=(float *)calloc(output.npixels,sizeof(float));
	    if (output.image==NULL) {
	      reportevt(flag_verbose,STATUS,5,"Calloc of output.image failed");
	      exit(0);
	    }
	    for (i=0;i<output.npixels;i++) output.image[i]=data->image[i];
	    if (data->mask!=NULL) {
	      sprintf(event,"%s and mask array",event);
	      output.mask=(short *)calloc(output.npixels,sizeof(short));
	      if (output.mask==NULL) {
		reportevt(flag_verbose,STATUS,5,"Calloc of output.mask failed");
	        exit(0);
	      }
	      for (i=0;i<output.npixels;i++) output.mask[i]=data->mask[i];
	    }
	    if (data->varim!=NULL) {
    	      /* set variance flag */	
	      FLG_VARIANCE=YES;  
	      sprintf(event,"%s and variance array",event);
	      output.varim=(float *)calloc(output.npixels,sizeof(float));
	      if (output.varim==NULL) {
		reportevt(flag_verbose,STATUS,5,"Calloc of output.varim failed");
	        exit(0);
	      }
	      for (i=0;i<output.npixels;i++) output.varim[i]=data->varim[i];
	    }
	    reportevt(flag_verbose,STATUS,1,event);
	  }


	  /* prepare space for variance image and initialize */
	  if (FLG_VARIANCE  && data->varim==NULL) {
	    output.varim=(float *)calloc(output.npixels,sizeof(float));
	    if (output.varim==NULL) {
	      reportevt(flag_verbose,STATUS,5,"Calloc of output.varim failed");
	      exit(0);
	    }
	    for (i=0;i<output.npixels;i++) output.varim[i]=0.0;
	  }
	  /* prepare space for the bad pixel mask and initialize */
	  if (data->mask==NULL) {
	    /* prepare image for mask == assumes mask is not yet present*/
	    output.mask=(short *)calloc(output.npixels,sizeof(short));
	    if (output.mask==NULL) {
	      reportevt(flag_verbose,STATUS,5,"Calloc of output.mask failed");
	      exit(0);
	    }
	    if (FLG_BPM) /* copy mask over */
	      for (i=0;i<output.npixels;i++) output.mask[i]=bpm->mask[i];
	    else  /* start with clean slate */
	      for (i=0;i<output.npixels;i++) output.mask[i]=0;
	  }


	
	  /* *************************************************/
	  /* ******** Confirm Correction Image Sizes  ********/
	  /* *************************************************/	  

	  /* need to make sure bias and flat are same size as image */
	  for (i=0;i<output.nfound;i++) {
	    if (output.axes[i]!=bpm->axes[i] && FLG_BPM) {
	      sprintf(event,"BPM image %s (%dX%d) different size than science image %s       (%dX%d)",
	        bpm->name,bpm->axes[0],bpm->axes[1],
		output.name,output.axes[0],output.axes[1]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    if (output.axes[i]!=bias->axes[i] && FLG_BIAS) {
	      sprintf(event,"BIAS image %s (%dX%d) different size than science image %s       (%dX%d)",
	        bias->name,bias->axes[0],bias->axes[1],
		output.name,output.axes[0],output.axes[1]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    if (output.axes[i]!=flat->axes[i] && FLG_FLATTEN) {
	      sprintf(event,"FLAT image %s (%dX%d) different size than science image %s       (%dX%d)",
	        flat->name,flat->axes[0],flat->axes[1],
		output.name,output.axes[0],output.axes[1]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    if (output.axes[i]!=fringe->axes[i] && FLG_FRINGE) {
	      sprintf(event,"FRINGE image %s (%dX%d) different size than science image %s       (%dX%d)",
	        fringe->name,fringe->axes[0],fringe->axes[1],
		output.name,output.axes[0],output.axes[1]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    if (output.axes[i]!=pupil->axes[i] && FLG_PUPIL) {
	      sprintf(event,"PUPIL GHOST image %s (%dX%d) different size than science image %s       (%dX%d)",
	        pupil->name,pupil->axes[0],pupil->axes[1],
		output.name,output.axes[0],output.axes[1]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	    if (output.axes[i]!=illumination->axes[i] && FLG_ILLUMINATION) {
	      sprintf(event,"ILLUM image %s (%dX%d) different size than science image %s       (%dX%d)",
	        illumination->name,illumination->axes[0],illumination->axes[1],
		output.name,output.axes[0], output.axes[1]);
	      reportevt(flag_verbose,STATUS,5,event);
	      exit(0);
	    }
	  }

	  /* ************************************************/
	  /* ******** Report on Upcoming Processing  ********/
	  /* ************************************************/	  

	  if (FLG_BIAS) {
	    if (fits_read_keyword(data->fptr,"DESBIAS",comment,comment,&status)==
	        KEY_NO_EXIST) {
	 	reportevt(flag_verbose,STATUS,1,"BIAS correcting");
	        flag_imbias=1;
	        status=0;
	    }
	    else {
	      reportevt(flag_verbose,STATUS,1,"Already BIAS corrected");
	      flag_imbias=0;
	    }
	  }
	  if (FLG_FLATTEN) {
	    if (fits_read_keyword(data->fptr,"DESFLAT",comment,comment,&status)==
	        KEY_NO_EXIST) {
	        flag_imflatten=1;
	        status=0;
	 	reportevt(flag_verbose,STATUS,1,"FLAT correcting");
	    }
	    else {
	      reportevt(flag_verbose,STATUS,1,"Already FLAT corrected");
	      flag_imflatten=0;
	    }
	  }
	  if (FLG_PUPIL) {
	    if (fits_read_keyword(data->fptr,"DESMKPUP",comment,comment,
		&status)==KEY_NO_EXIST) {
	        flag_impupil=1;
	        status=0;
	 	reportevt(flag_verbose,STATUS,1,"PUPIL GHOST correcting");
	    }
	    else {
	      reportevt(flag_verbose,STATUS,1,"Already PUPIL GHOST corrected");
	      flag_impupil=0;
	    }
	  }
	  
	  /* ************************************************/
	  /* ********* Mask Missing Image Sections   ********/
	  /* ************************************************/	  

          /* get scale value for image */
	  /* note that this scalefactor is pre-bias correction */
	  /* but post overscan correction */
          retrievescale(&output,scaleregionn,scalesort,flag_verbose,
           &scalefactor,&mode,&skysigma);
	  /* cycle through image masking all pixels below threshold */
	  thresholdval=scalefactor*BADPIX_THRESHOLD;
	  maskedpixels=0;
	  for (i=0;i<output.npixels;i++) {
	    if ((output.image[i]<thresholdval || output.image[i]<0.0) &&
	      !output.mask[i]) {
	      output.mask[i]+=BADPIX_MASK; /* set the masked flag */
	      maskedpixels++;
	    }
	  }
	  sprintf(event,"%d pixels masked with value below %.2f",
	    maskedpixels,thresholdval);
	  reportevt(flag_verbose,QA,1,event);
	  /* recalculate scale factor if many pixels masked */
	  if (maskedpixels>0.1*output.npixels) {
            retrievescale(&output,scaleregionn,scalesort,flag_verbose,
             &scalefactor,&mode,&skysigma);
	  }

	  /* ************************************************/
	  /* ******** BIAS, Pupil Ghost and FLATTEN  ********/
	  /* ************************************************/	  

	  if ((FLG_FLATTEN && flag_imflatten) || (FLG_BIAS && flag_imbias)
	    || (FLG_PUPIL && flag_impupil)) {
	    /* retrieve sky brightness in image */
	    scale=1.0;offset=0.0;
	    saturatepixels=0;
	    for (i=0;i<output.npixels;i++) {
	      /* grab flat field scalefactor */
	      if (FLG_FLATTEN && flag_imflatten) scale=flat->image[i];
	      /* grab bias offset */
	      if (FLG_BIAS && flag_imbias) offset=bias->image[i];
	      else offset=0;
	      /* grab pupil offset */
	      if (FLG_PUPIL && flag_impupil) 
		offset+=pupil->image[i]*scalefactor;
	      /* do math using mask  */
	      if (!output.mask[i]) {
	        if (i%output.axes[0]<1024) { /* amplifier A */
	          if (output.image[i]>output.saturateA) {
		    output.mask[i]+=BADPIX_SATURATE;
		    saturatepixels++;
		    /* boost all saturated pixels above maxsaturate */
		    if (output.image[i]<maxsaturate) 
		      output.image[i]=scale*maxsaturate+offset;
		  }
		}		
		else {/* amplifier B */
		  if (output.image[i]>output.saturateB) {
		    output.mask[i]+=BADPIX_SATURATE;
		    saturatepixels++;
		    /* boost all saturated pixels above maxsaturate */
		    if (output.image[i]<maxsaturate) 
		       output.image[i]=scale*maxsaturate+offset;
		  }
		}
	        output.image[i]=(output.image[i]-offset)/scale;
	      }
	      /* set pixel to 0.0 if it's in a bad pixel */
	      else output.image[i]=0.0;
	  }
	  sprintf(event,"Masked %d saturated pixels",saturatepixels);
	  reportevt(flag_verbose,STATUS,1,event);

	  } /* end BIAS and FLATTEN */

	  
	  /* ********************************************* */
	  /* ********** VARIANCE Image Section *********** */
	  /* ********************************************* */

	  if (FLG_VARIANCE && (FLG_BPM || FLG_BIAS || FLG_FLATTEN)) {
	    reportevt(flag_verbose,STATUS,1,
	      "Creating CCD statical variance image");
	    for (i=0;i<output.npixels;i++) 
	      if (((FLG_BPM && !output.mask[i]) || !FLG_BPM) && 
		bias->varim[i]>0.0 && flat->varim[i]>0.0 ) {
	        if (i%output.axes[0]<1024) { /* in AMP A section */	      
		  /* each image contributes Poisson noise, RdNoise */
		  /*  and BIAS image noise */
	          sumval=(output.image[i]/output.gainA+
		    Squ(output.rdnoiseA/output.gainA));
		}
		else { /* in AMP B */
		  /* each image contributes Poisson noise, RdNoise */
		  /*  and BIAS image noise */
	          sumval=(output.image[i]/output.gainB+
		    Squ(output.rdnoiseB/output.gainB));	
		}
		output.varim[i]=1.0/(sumval+1.0/bias->varim[i]+
		  1.0/flat->varim[i]);
	      }
	      else output.varim[i]=0.0;
	  }

	  /* ********************************************************** */
	  /* ******** INTERPOLATE Image and Variance Section ********** */
	  /* ********************************************************** */
	  if (FLG_INTERPOLATE && FLG_BPM) {
	    if (flag_verbose==3) 
	      sprintf(event,"Interpolating over bad pixels");
	    interppixels=0;
	    for (i=0;i<output.npixels;i++) {
	    	if (output.mask[i]==BADPIX_BPM) {/* this is bad pixel */
		  xlow=i-1;
		  while (output.mask[xlow]>0) xlow--;
		  xhi=i+1;
		  while (output.mask[xhi]>0) xhi++;
		  output.image[i]=0.5*(output.image[xlow]+output.image[xhi]);
		  /* could add noise to counteract averaging of 2 pixels */
		  /*if (FLG_VARIANCE) output.image[i]+=gasdev(&seed)*
		    sqrt(0.25*(1.0/output.varim[xlow]+1.0/output.varim[xhi]));*/
		  output.mask[i]+=BADPIX_INTERP; /* set the interpolate flag */
	          interppixels++;
		  /* now calculate weight for this pixel */
		  /* allows for non-zero weight if interpolation */
		  /* over 2 or fewer pixels */
		  if (FLG_VARIANCE && (xhi-xlow)<=3) { 
		    if (output.varim[xlow]>1.0e-10 && output.varim[xhi]>1.0e-10)
		      output.varim[i]=1.0/(0.25*(1.0/output.varim[xlow]+
		      1.0/output.image[xhi]));
		    /* else leave pixel with zero weight*/
		    /* scale weight down because this is interpolated pixel */
		    output.varim[i]/=scale_interpolate;
		  }
		}
	    }
	    if (flag_verbose==3) {
	      sprintf(event,"%s: %d pixels",event,interppixels);
	      reportevt(flag_verbose,STATUS,1,event);
	    }
	  }

	
	  /* **************************** */
	  /* **** FRINGE Correction ***** */
	  /* **************************** */

	  if (FLG_FRINGE) {
	    flag_imfringe=YES;
	    reportevt(flag_verbose,STATUS,1,"Applying Fringe correction");
	    
	    /* retrieve overall scaling from image */
	    retrievescale(&output,scaleregionn,scalesort,flag_verbose,
	      &scalefactor,&mode,&skysigma);
	    for (i=0;i<output.npixels;i++) 
	      output.image[i]-=scalefactor*fringe->image[i];
	  }	
	
	  /* ********************************** */
	  /* **** ILLUMUNATION Correction ***** */
	  /* ********************************** */

	  if (FLG_ILLUMINATION) {
	    flag_imillumination=YES;
	    if (flag_verbose && FLG_VARIANCE) 
	      reportevt(flag_verbose,STATUS,1,
		"Applying Illumination correction to image and variance image");
	   else if (flag_verbose) 
	      reportevt(flag_verbose,STATUS,1,
		"Applying Illumination correction");
	    for (i=0;i<output.npixels;i++) 
	      output.image[i]/=illumination->image[i];
	    if (FLG_VARIANCE) {
	      for (i=0;i<output.npixels;i++) 
	        output.varim[i]/=Squ(illumination->image[i]);
	    }
	  }	


	  /* *************************************** */
	  /* ********* CALCULATE SKYBRITE ********** */
	  /* *************************************** */
	  if (FLG_UPDATESKYBRITE || FLG_UPDATESKYSIGMA) {
	    /* retrieve overall scaling from image */
	    retrievescale(&output,scaleregionn,scalesort,flag_verbose,
	      &skybrite,&mode,&skysigma);
	    sprintf(event,"Image SKYBRITE is %10.4e and SKYSIGMA is %10.4e",
	      skybrite,skysigma);
	    reportevt(flag_verbose,QA,1,event);
	  }


	  /* *********************** */
	  /* **** SAVE RESULTS ***** */
	  /* *********************** */

	  if (FLG_OUTPUT) {
	    /* if writing individual images for each component */
	    /* then get these names now */
	    if (!FLG_MEF) {
	      /* strip off the ".fits" */
	      sprintf(rootname,"%s",imagename);
	      for (i=strlen(rootname);i>=0;i--) 
		if (!strncmp(rootname+i,".fits",5)) break;
	      rootname[i]=0;
	      /* create the three names */
	      sprintf(imagename,"!%s_im.fits",rootname);
	      sprintf(output.name,"%s",imagename);
	      sprintf(bpmname,"!%s_bpm.fits",rootname);
	      sprintf(varimname,"!%s_var.fits",rootname);
	    }

	    /* make sure path exists for new image */
            if (mkpath(output.name,flag_verbose)) {
              sprintf(event,"Failed to create path to file: %s",output.name+1);
              reportevt(flag_verbose,STATUS,5,event);
              exit(0);
            }
            else {
              sprintf(event,"Created path to file: %s",output.name+1);
              reportevt(flag_verbose,STATUS,1,event);
            }

	    /* create the (image) file */
            if (fits_create_file(&output.fptr,output.name,&status)) {
	      sprintf(event,"File creation failed: %s",output.name);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  
	    /* create the image */
	    if (fits_create_img(output.fptr,FLOAT_IMG,2,output.axes,&status)) {
	      sprintf(event,"Image creation failed: %s",output.name);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	      
	    /* first read the number of keywords */
	    if (fits_get_hdrspace(data->fptr,&keysexist,&j,&status)) {
	      sprintf(event,"Retrieving header failed: %s",data->name);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    sprintf(event,"Copying %d keywords into output image",
	      keysexist);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* reset to beginning of output header space */
	    if (fits_read_record(output.fptr,0,keycard,&status)) {
	      reportevt(flag_verbose,STATUS,5,
		"Reset to start of header failed");
	      printerror(status);
	    }
	    if (fits_read_record(data->fptr,0,keycard,&status)) {
	      reportevt(flag_verbose,STATUS,5,"Reading header record failed");
	      printerror(status);
	    }
	    for (j=1;j<=keysexist;j++) {
	      if (fits_read_record(data->fptr,j,keycard,&status)) {
	        sprintf(event,"Reading header record %d failed",j);
	        reportevt(flag_verbose,STATUS,5,event);
	      	printerror(status);
	      }
	      if (strncmp(keycard,"BITPIX  ",8) && 
	          strncmp(keycard,"NAXIS",5)    &&
	          strncmp(keycard,"PCOUNT  ",8) &&
	          strncmp(keycard,"EXTEND  ",8) &&
	          strncmp(keycard,"GCOUNT  ",8) &&
	          strncmp(keycard,"COMMENT   FITS (Flexible Image",30) &&
	          strncmp(keycard,"COMMENT   and Astrophysics', v",30) &&
	          strncmp(keycard,"EXTNAME ",8) &&
	          strncmp(keycard,"BSCALE  ",8) &&
	          strncmp(keycard,"BZERO   ",8) 
		  ) { 
	        if (fits_write_record(output.fptr,keycard,&status)) {
		  sprintf(event,"Writing record failed: %s",keycard);
		  reportevt(flag_verbose,STATUS,5,event);
	          printerror(status);
		}
	      }
	    }
	  }
	  else {  /* writing on top of input image */
	    /* resize image */
	    sprintf(event,"Resizing output image %s to %d X %d",
	      output.name+1,output.axes[0],output.axes[1]);
	    reportevt(flag_verbose,STATUS,1,event);
	    if (fits_resize_img(output.fptr,FLOAT_IMG,2,output.axes,&status)) {
	      sprintf(event,"Resizing of image failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);	 
	    }
	    /* change BZERO */
	    if (fits_set_bscale(output.fptr,1.0,0.0,&status)) {
	      sprintf(event,"Reading BSCALE failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    output.bzero=0;
	    if (fits_update_key_lng(output.fptr,"BZERO",output.bzero,comment,
	      &status)) {
	      sprintf(event,"Reading BZERO failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  }


	  /* write the corrected image*/
	  if (fits_write_img(output.fptr,TFLOAT,1,output.npixels,
	    output.image,&status)) {
	    sprintf(event,"Writing image failed: %s",output.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

	  /* free image array */ 
	  free(data->image);
	  free(output.image); 

	  /* Write processing history into the header */
          /* get system time */
          tm=time(NULL);
          sprintf(comment,"%s",asctime(localtime(&tm)));
          comment[strlen(comment)-1]=0;
	  if (FLG_OVERSCAN && flag_imoverscan) {
	    if (fits_write_key_str(output.fptr,"DESOSCN",comment,
	      "overscan corrected",&status)) {
	      sprintf(event,"Writing DESOSCN failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    /* update the DATASEC keyword */
	    if (fits_update_key_str(output.fptr,"DATASEC",output.datasec,
	      "Data section within image",&status)) {
	      sprintf(event,"Updating DATASEC failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  }
	  if (FLG_BIAS && flag_imbias)
	    if (fits_write_key_str(output.fptr,"DESBIAS",comment,
	      "bias subtracted",&status)) {
	      sprintf(event,"Writing DESBIAS failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  if (FLG_PUPIL && flag_impupil)
	    if (fits_write_key_str(output.fptr,"DESPUPC",comment,
	      "pupil corrected",&status)) {
	      sprintf(event,"Writing DESPUPC failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  if (FLG_FLATTEN && flag_imflatten)
	    if (fits_write_key_str(output.fptr,"DESFLAT",comment,
	      "flat fielded",&status)) {
	      sprintf(event,"Writing DESFLAT failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  if (FLG_ILLUMINATION && flag_imillumination)
	    if (fits_write_key_str(output.fptr,"DESILLUM",comment,
	      "Illumination Corrected",&status)) {
	      sprintf(event,"Writing DESILLUM failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  if (FLG_FRINGE && flag_imfringe)
	    if (fits_write_key_str(output.fptr,"DESFRING",comment,
	      "Fringe Corrected",&status)) {
	      sprintf(event,"Writing DESFRING failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  if (FLG_UPDATESKYSIGMA) 
	    if (fits_update_key_flt(output.fptr,"SKYSIGMA",skysigma,7,
	      "Sky Sigma (ADU)",&status)) {
	      sprintf(event,"Writing SKYSIGMA failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  if (FLG_UPDATESKYBRITE) 
	    if (fits_update_key_flt(output.fptr,"SKYBRITE",skybrite,7,
	      "Sky Brightness (ADU)",&status)) {
	      sprintf(event,"Writing SKYBRITE failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  if (flag_verbose) reportevt(flag_verbose,STATUS,1,longcomment);
	  if (fits_write_comment(output.fptr,longcomment,&status)) {
	    sprintf(event,"Writing longcomment failed: %s",output.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  if (fits_update_key_str(output.fptr,"DES_EXT","IMAGE",
	    "Image extension",&status)) {
	    sprintf(event,"Writing DES_EXT=IMAGE failed: %s",output.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	    
	  /* remove unneeded information from the header */
	  nkeys=0;
	  while (strlen(delkeys[nkeys])) {
	    if (fits_read_keyword(output.fptr,delkeys[nkeys],
		comment,comment,&status)==KEY_NO_EXIST) status=0;
	    else {
	      if (fits_delete_key(output.fptr,delkeys[nkeys],&status)) {
	        if (flag_verbose) {
		  sprintf(event,"Keyword %s not deleted from image %s",
		    delkeys[nkeys],output.name+1);
	          reportevt(flag_verbose,STATUS,3,event);
	        }
	        status=0;
	      }
	    }
	    nkeys++;
	  }

	  /* write new keywords */
	  if (fits_write_key(output.fptr,TFLOAT,"SATURATE",&maxsaturate,
	    "Global Saturate Value",&status)) {
	    sprintf(event,"Writing keyword SATURATE failed: %s",output.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  
	  /* update existing keywords */
	  if (fits_update_key_str(output.fptr,"OBSTYPE","red",
	    "Observation type",&status)) {
	    sprintf(event,"Updating keyword OBSTYPE failed: %s",output.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	    
	  /* close and reopen if outputting individual images */
	  if (!FLG_MEF) {
	    /* close the image file */
            if (fits_close_file(output.fptr,&status)) {
	      sprintf(event,"Closing file failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    sprintf(event,"Opening bpm image %s",bpmname+1);
	    reportevt(flag_verbose,STATUS,1,event);
	    /* open the bpm file */
            if (fits_create_file(&output.fptr,bpmname,&status)) {
	      sprintf(event,"Creating image mask file failed: %s",bpmname+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  }

	  /* now store the bad pixel mask that has been created or updated */
	  /* first create a new extension */
	  if (fits_create_img(output.fptr,USHORT_IMG,2,output.axes,&status)) {
	    sprintf(event,"Creating image mask failed: %s",bpmname+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* write the data */	  
	  if (fits_write_img(output.fptr,TUSHORT,1,output.npixels,output.mask,
	    &status)) {
	    sprintf(event,"Writing image mask failed: %s",bpmname+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* free mask image array */ 
	  free(data->mask);   
	  free(output.mask);   

	  if (fits_update_key_str(output.fptr,"DES_EXT","BPM",
	    "Extension type",&status)) {
	    reportevt(flag_verbose,STATUS,5,"Setting DES_EXT=BPM failed");
	    printerror(status);
	  }
	  	    
	  /* close and reopen if outputting individual images */
	  if (!FLG_MEF) {
	    /* close the image file */
            if (fits_close_file(output.fptr,&status)) {
	      sprintf(event,"Closing image failed: %s",output.name+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    if (flag_verbose) {
	      sprintf(event,"Opening variance image %s",varimname+1);
	      reportevt(flag_verbose,STATUS,1,event);
	    }
	    /* open the variance image */
            if (fits_create_file(&output.fptr,varimname,&status)) {
	      sprintf(event,"Creating variance image failed: %s",varimname+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  }

	  /* now store the variance image that has been created or updated */
	  /* first create a new extension */
	  if (FLG_VARIANCE) {
	    if (fits_create_img(output.fptr,FLOAT_IMG,2,output.axes,&status)) {
	      sprintf(event,"Creating variance image extention failed: %s",
		varimname+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    /* write the data */	  
	    if (fits_write_img(output.fptr,TFLOAT,1,output.npixels,output.varim,
	      &status)) {
	      sprintf(event,"Writing variance image failed: %s",varimname+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    /* free variance array */ 
	    free(data->varim);   
	    free(output.varim);   

	    if (fits_update_key_str(output.fptr,"DES_EXT","VARIANCE",
	      "Extension type",&status)) {
	      sprintf(event,"Writing DES_EXT=VARIANCE failed: %s",varimname+1);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	  }
	  	    
          /* close the corrected image */
          if (fits_close_file(output.fptr,&status)) {
	    sprintf(event,"Closing image failed: %s",output.name+1);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  /* close the input image if needed */
	  if (FLG_OUTPUT) if (fits_close_file(data->fptr,&status)) {
	    sprintf(event,"Closing input image failed: %s",data->name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }
	  sprintf(event,"Closed %s: 2D ( %d X %d )",
	    &(output.name[FLG_OUTPUT]),output.axes[0],output.axes[1]);
	  reportevt(flag_verbose,STATUS,1,event);
}

#undef BADPIX_BPM
#undef BADPIX_THRESHOLD
#undef BADPIX_SATURATE 
#undef BADPIX_INTERP 
#undef BADPIX_MASK
#undef FLG_OUTPUT
#undef FLG_OVERSCAN
#undef FLG_BIAS
#undef FLG_FLATTEN
#undef FLG_BPM
#undef FLG_PUPIL
#undef FLG_ILLUMINATION
#undef FLG_FRINGE
#undef FLG_VARIANCE
#undef FLG_INTERPOLATE
#undef FLG_UPDATESKYBRITE
#undef FLG_UPDATESKYSIGMA
#undef FLG_MEF


