#include <fstream>
#include "TSystem.h"
#include "TPRegexp.h"
#include "TPaveStats.h"
#include "TMVA/Timer.h"
#include "SysErrFit.h"
#include "TF1.h"
#include "TGaxis.h"
#include "TVirtualFitter.h"
#include "TMatrixD.h"
#include "Object.h"
#include "TEventList.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TCut.h"
#include "FitSLR.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TMath.h"
#include <string>
#include <iostream>
#include "TDirectory.h"
#include "TEventList.h"
#include "TFile.h"
#include "TH2D.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TGraph.h"
#include "TLine.h"
#include "TLatex.h"
#include "TDirectory.h"
 #include "TSQLServer.h"
 #include "TSQLStatement.h"
 #include "TOracleStatement.h"
#include "ExtinctionMap.h"
#include "kdtree++/kdtree.hpp"
#include <CCfits>
using namespace std;
using TMVA::Timer;

struct ObjPair{

  Object obs;
  Object truth;
};



using namespace CCfits;
void LoadStyle();
void report_evt(bool verbose,string type,int level,string mess);
void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol);

void plot_phot(const vector<ObjPair> &obj_pairs,
               string title="",string dir=".",
	       string which_band="",string which_mag="",
               bool debug=false,
	       double sigma=3,int N_bin=1000);

int plot_phot_all(const vector<ObjPair> &obj_pairs,
                  TPad *can,int &cur_can,
                  string title="",string dir=".",
                  string which_band="",
                  bool debug=false,
                  string true_label="true",
                  string mag_label="auto");
                  


int plot_astro(const vector<ObjPair> &obj_pairs,
               double &ra_mean,double &ra_clip_mean,
               double &ra_rms,double &ra_clip_rms,
               double &dec_mean,double &dec_clip_mean,
               double &dec_rms,double &dec_clip_rms,
               string title="",string dir=".",
	       string which_band="",
               bool debug=false,bool draw=false);

void PrintHelp();
typedef KDTree::KDTree<3,Object> KD_Tree;
typedef std::pair<KD_Tree::iterator,double> KD_Match;




class ImagePair{
public:
  ImagePair(int _id1,int _id2):id1(_id1),id2(_id2) {}
  
  bool operator==(const ImagePair& other) const {
    if( (id1==other.id1 && id2==other.id2) ||
        (id1==other.id2 && id2==other.id1)) return true;
    else return false;
  }
        
  int id1;
  int id2;
};



int main(int argc, char*argv[])
{
  TStopwatch timer;
  LoadStyle();
 char**argv_dummy;
    char char_dummy='p';
    char *char_dummy2=&char_dummy;
    argv_dummy=&char_dummy2;

    int int_dummy=1;
    //TApplication app("app",&int_dummy,argv_dummy);


  string project="DES";
  string truth_table="DC5_Truth";
  string tile="";
  string sdss_datafile("/home/rearmstr/slr_data/covey_extinction.root");
  string mag="auto";
  string true_mag="modelmag";
  bool restrict_ccd=false;
  string run="";
  string nite="";
  string band="";
  string title="";
  string dir="./";
  double sigma=3;
  int N_bin=300;
  double star_cut=0.7;
  double err_cut=1;
  int cut_points=0;
  double cut_range=-1;
  bool extinction=false;
  bool applycolor=false;
  bool print=true;
  bool remove_outliers=false;
  bool debug=false;
  string outfile="out.root";
  double tol=2;
  string loaddata="";
  bool UseClip=true;
  bool UseWeighted=false;
  double ClipTol=1e-6;
  double Thresh=2.5;
  int MaxIter=3;
  bool UseColor=true;
  bool usnob=true;
  bool standard_stars=true;
  bool standard_gals=true;
  bool use_rootfile=false;
  string star_rootfile="~/dc5_truth/dc5_stars.root";
  string gal_rootfile="~/dc5_truth/dc5_galaxies.root";
  string fitsfile="";
  bool proper_motion=false;
  bool calc_zp=true;
  bool verbose=true;
  bool draw=false;
  double max_mag=2.5;
  int imageid=-1;
  bool insert_db=false;
  string listfile="";
  string rootfile="out.root";
  vector<string> fits_list;
  vector<string> band_list;
  vector<int> id_list;
  string extinction_dir="/home/zenteno/REDSHIFT_SURVEYS/EXTINCTION/MAPS_FITS/";
  if(argc==1) {
    PrintHelp();
    exit(1);
  }
    
  for(int ii=1;ii<argc;ii++) {
    string arg=argv[ii];
    if(arg=="-tile")            {tile=argv[ii+1];ii++;}
    else if(arg=="-run")        {run=argv[ii+1];ii++;}
    else if(arg=="-project")    {project=argv[ii+1];ii++;}
    else if(arg=="-nite")       {nite=argv[ii+1];ii++;}
    else if(arg=="-band")       {band=argv[ii+1];ii++;}
    else if(arg=="-loaddata")   {loaddata=argv[ii+1];ii++;}
    else if(arg=="-list")       {listfile=argv[ii+1];ii++;}
    else if(arg=="-title")      {title=argv[ii+1];ii++;}
    else if(arg=="-dir")        {dir=argv[ii+1];ii++;}
    else if(arg=="-restrict_ccd")  {restrict_ccd=true;}
    else if(arg=="-extinction") {extinction=true;}
    else if(arg=="-debug")      {debug=true;}
    else if(arg=="-print")      {print=true;}
    else if(arg=="-draw")      {draw=true;}
    else if(arg=="-use_rootfile")      {use_rootfile=true;}
    else if(arg=="-color")      {applycolor=true;}
    else if(arg=="-insert_db")      {insert_db=true;}
    else if(arg=="-true")      {applycolor=true;}
    else if(arg=="-sdss_file")  {sdss_datafile=argv[ii+1];ii++;}
    else if(arg=="-truth_table")  {truth_table=argv[ii+1];ii++;}
    else if(arg=="-mag")        {mag=argv[ii+1];ii++;}
    else if(arg=="-true_mag")        {true_mag=argv[ii+1];ii++;}
    else if(arg=="-rootfile")        {rootfile=argv[ii+1];ii++;}
    else if(arg=="-star_cut")   {star_cut=atof(argv[ii+1]);ii++;}
    else if(arg=="-tol")        {tol=atof(argv[ii+1]);ii++;}
    else if(arg=="-sigma")      {sigma=atof(argv[ii+1]);ii++;}
    else if(arg=="-N_bin")      {N_bin=atoi(argv[ii+1]);ii++;}
    else if(arg=="-imageid")      {imageid=atoi(argv[ii+1]);ii++;}
    else if(arg=="-err_cut")    {err_cut=atof(argv[ii+1]);ii++;}
    else if(arg=="-max_mag")    {max_mag=atof(argv[ii+1]);ii++;}
    else if(arg=="-outfile")    {outfile=argv[ii+1];ii++;}
    else if(arg=="-cut_range")  {cut_range=atof(argv[ii+1]);ii++;}
    else if(arg=="-cut_points")    {cut_points=atoi(argv[ii+1]);ii++;}
    else if(arg=="-remove_outliers"){remove_outliers=true;}
    else if(arg=="-proper")     {proper_motion=true;}
    else if(arg=="-fitsfile")   {fitsfile=argv[ii+1];ii++;}
    else {
      cerr<<"Not a valid command line entry: "<<arg<<endl;
      exit(1);
    }
  }

  TFile *starfile=new TFile(star_rootfile.c_str());
  TTree *stars=(TTree*) starfile->Get("stars");
  
  TFile *galfile=new TFile(gal_rootfile.c_str());
  TTree *gals=(TTree*) galfile->Get("galaxies");
  
  // Do appropriate command line checks
  //ExtinctionMap ExtMap(extinction_dir);
  double deg_tol=tol*1./3600;
  tol*=1./3600*TMath::DegToRad();
  
  TSQLServer *db;
  string db_cmd;
  TSQLStatement *stmt;
  
  double star_ra_mean,star_dec_mean;
  double star_ra_rms,star_dec_rms;
  double star_ra_clip_mean,star_dec_clip_mean;
  double star_ra_clip_rms,star_dec_clip_rms;
  
  double gal_ra_mean,gal_dec_mean;
  double gal_ra_rms,gal_dec_rms;
  double gal_ra_clip_mean,gal_dec_clip_mean;
  double gal_ra_clip_rms,gal_dec_clip_rms;

  TTree tree("tree","");
  char band_char[10];
  tree.Branch("imageid",&imageid,"imageid/I");
  tree.Branch("band",&band_char,"band/C");
  tree.Branch("star_ra_mean",&star_ra_mean,"star_ra_mean/D");
  tree.Branch("star_ra_rms",&star_ra_rms,"star_ra_rms/D");
  tree.Branch("star_ra_clip_mean",&star_ra_clip_mean,"star_ra_clip_mean/D");
  tree.Branch("star_ra_clip_rms",&star_ra_clip_rms,"star_ra_clip_rms/D");
  tree.Branch("star_dec_mean",&star_dec_mean,"star_dec_mean/D");
  tree.Branch("star_dec_rms",&star_dec_rms,"star_dec_rms/D");
  tree.Branch("star_dec_clip_mean",&star_dec_clip_mean,"star_dec_clip_mean/D");
  tree.Branch("star_dec_clip_rms",&star_dec_clip_rms,"star_dec_clip_rms/D");
  tree.Branch("gal_ra_mean",&gal_ra_mean,"gal_ra_mean/D");
  tree.Branch("gal_ra_rms",&gal_ra_rms,"gal_ra_rms/D");
  tree.Branch("gal_ra_clip_mean",&gal_ra_clip_mean,"gal_ra_clip_mean/D");
  tree.Branch("gal_ra_clip_rms",&gal_ra_clip_rms,"gal_ra_clip_rms/D");
  tree.Branch("gal_dec_mean",&gal_dec_mean,"gal_dec_mean/D");
  tree.Branch("gal_dec_rms",&gal_dec_rms,"gal_dec_rms/D");
  tree.Branch("gal_dec_clip_mean",&gal_dec_clip_mean,"gal_dec_clip_mean/D");
  tree.Branch("gal_dec_clip_rms",&gal_dec_clip_rms,"gal_dec_clip_rms/D");
 

  if(listfile.empty()) {
    fits_list.push_back(fitsfile);
    id_list.push_back(imageid);
    band_list.push_back(band);
  }
  else {
    
    ifstream infile(listfile.c_str());

    while(infile>>fitsfile>>imageid) {
      fits_list.push_back(fitsfile);
      id_list.push_back(imageid);
      
      TString tfilename(fitsfile);
    
      band=((TObjString*)(TPRegexp("decam.*\\d-(\\w.*)-.*_cat.fits").MatchS(tfilename))->At(1))->GetString();
    band_list.push_back(band);
      
    }

  }


  for(int i=0;i<fits_list.size();i++) {

    fitsfile=fits_list[i];
    imageid=id_list[i];
    band=band_list[i];
    strcpy(band_char,band.c_str());
    cout<<"STATUS1BEG Processing file "<<fitsfile<<" with imageid "
        <<imageid<<" in band "<<band<<" STATUS1END"<<endl;

  std::auto_ptr<FITS> File(new FITS(fits_list[i])); 
  double mjd=0;
  if(proper_motion) {
    TString get_mjd=gSystem->GetFromPipe(Form("grep -P -o -a 'MJD-OBS.*\\MJD of observation' %s",fits_list[i].c_str()));
    
    TObjArray *obj = TPRegexp("MJD-OBS =\\s+(\\d.*)\\s+/").MatchS(get_mjd);

    const TString mjd_string = ((TObjString *)obj->At(1))->GetString();
    mjd=mjd_string.Atof();
  }

  ExtHDU& table = File->extension(2);

  string ra_string="ALPHAMODEL_J2000";
  string dec_string="DELTAMODEL_J2000";
  string raerr_string="";
  string decerr_string="";
  string magerr_string="MAGERR_MODEL";
  string mag_string="MAG_MODEL";
  string flag_string="FLAGS";
  string class_string="SPREAD_MODEL";

  string obs_label=mag_string.substr(4,mag_string.size());
  std::transform(obs_label.begin(), obs_label.end(), 
                 obs_label.begin(), ::tolower);
  int N=table.rows();

  vector<double> ra_vec,dec_vec,raerr_vec,decerr_vec,mag_vec,
    magerr_vec,flag_vec,class_vec;

  table.column(ra_string).read(ra_vec,0,N);
  table.column(dec_string).read(dec_vec,0,N);
  //table.column(raerr_string).read(raerr_vec,0,N);
  //table.column(decerr_string).read(decerr_vec,0,N);
  table.column(mag_string).read(mag_vec,0,N);
  table.column(magerr_string).read(magerr_vec,0,N);
  table.column(flag_string).read(flag_vec,0,N);
  table.column(class_string).read(class_vec,0,N);


  string evt=Form("Found %d catalog objects",N);
  report_evt(verbose,"STATUS",1,evt);

  vector<Object> obs_vec,truth_vec,nomatch_truth,nomatch_obs;
  KD_Tree truth_tree;
  double max_ra=-1e6,min_ra=1e6,min_dec=1e6,max_dec=-1e6;
  for(int i=0;i<N;i++) {
    if(extinction) {
      
     //  double R_g=3.793;
//       double R_r=2.751;
//       double R_i=2.086;
//       double R_z=1.479;
      
//       double EBV=ExtMap.GetExtinction(ra_vec[i],dec_vec[i]);
      
//       /* apply extinction correction in each band */
//       if(band=="g") mag_vec[i] -= R_g * EBV;
//       else if(band=="r") mag_vec[i] -= R_r * EBV;
//       else if(band=="i") mag_vec[i] -= R_i * EBV;
//       else if(band=="z") mag_vec[i] -= R_z * EBV;
      
    }
    Object ob(ra_vec[i],dec_vec[i],mag_vec[i],magerr_vec[i],band,0,0);
    
    double cd=cos(ob.Dec*TMath::DegToRad());
    ob.xyz[0]=cos(ob.RA*TMath::DegToRad())*cd;
    ob.xyz[1]=sin(ob.RA*TMath::DegToRad())*cd;
    ob.xyz[2]=sin(ob.Dec*TMath::DegToRad());

    obs_vec.push_back(ob);

    if(ob.RA>max_ra) max_ra=ob.RA;
    if(ob.Dec>max_dec) max_dec=ob.Dec;
    if(ob.RA<min_ra) min_ra=ob.RA;
    if(ob.Dec<min_dec) min_dec=ob.Dec;
  }
   
 //  string tree_cmd="ra:dec:";
//   if(band=="g") tree_cmd+="g";
//   else if(band=="r") tree_cmd+="r";
//   else if(band=="i") tree_cmd+="i";
//   else if(band=="z") tree_cmd+="z";
//   else if(band=="Y") tree_cmd+="Y";
  
  
//   TFile outf("test_truth.root","recreate");
//   TTree outt("tree","");
//   Object trutht;
//   outt.Branch("truth",&trutht);

//   string tree_cut=Form("ra>%f && ra<%f && dec>%f && dec<%f",
//                        min_ra,max_ra,min_dec,max_dec);


  if(use_rootfile) {

    double truth_ra,truth_dec;
    double mu_ra,mu_dec;
    double truth_mag;
    if(band=="g") {
      stars->SetBranchAddress("g",&truth_mag);
    }
    if(band=="r") {
      stars->SetBranchAddress("r",&truth_mag);
    }
    if(band=="i") {
      stars->SetBranchAddress("i",&truth_mag);
    }
    if(band=="z") {
      stars->SetBranchAddress("z",&truth_mag);
    }
    if(band=="Y") {
      stars->SetBranchAddress("Y",&truth_mag);
    }
    stars->SetBranchAddress("ra",&truth_ra);
    stars->SetBranchAddress("mura",&mu_ra);
    stars->SetBranchAddress("dec",&truth_dec);
    stars->SetBranchAddress("mudec",&mu_dec);
    int i=0;
    while(stars->GetEntry(i)) {
      i++;
      // give a little extra room around the edges
      if(truth_ra>max_ra+4*tol || truth_ra<min_ra-4*tol ||
         truth_dec>max_dec+4*tol || truth_dec<min_dec-4*tol) continue;
      
      Object obj(truth_ra,truth_dec,truth_mag,
                 0,band,i,0);
      obj.class_star=0;
      obj.mura=mu_ra;
      obj.mudec=mu_dec;
      
      double cd=cos(obj.Dec*TMath::DegToRad());
      obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
      obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
      obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());
      
      truth_tree.insert(obj);
      truth_vec.push_back(obj);
    }

    
    if(band=="g") {
      gals->SetBranchAddress("g",&truth_mag);
    }
    if(band=="r") {
      gals->SetBranchAddress("r",&truth_mag);
    }
    if(band=="i") {
      gals->SetBranchAddress("i",&truth_mag);
    }
    if(band=="z") {
      gals->SetBranchAddress("z",&truth_mag);
    }
    if(band=="Y") {
      gals->SetBranchAddress("Y",&truth_mag);
    }
    gals->SetBranchAddress("ra",&truth_ra);
    gals->SetBranchAddress("dec",&truth_dec);
    i=0;
    while(gals->GetEntry(i)) {
      i++;
      // give a little extra room around the edges
      if(truth_ra>max_ra+4*tol || truth_ra<min_ra-4*tol ||
         truth_dec>max_dec+4*tol || truth_dec<min_dec-4*tol) continue;
      
      Object obj(truth_ra,truth_dec,truth_mag,
                 0,band,i,0);
      obj.class_star=1;
      obj.mura=0;
      obj.mudec=0;
      double cd=cos(obj.Dec*TMath::DegToRad());
      obj.xyz[0]=cos(obj.RA*TMath::DegToRad())*cd;
      obj.xyz[1]=sin(obj.RA*TMath::DegToRad())*cd;
      obj.xyz[2]=sin(obj.Dec*TMath::DegToRad());
      
      truth_tree.insert(obj);
      truth_vec.push_back(obj);
    }
  }
  else {

  
  if(truth_table=="DC5_Truth") {
    db = TSQLServer::Connect("oracle://desdb/stdes",
                             "pipeline", "dc01user");
    
    db_cmd=Form("select ra_dbl,dec_dbl,g_mag,r_mag,i_mag,z_mag,y_mag,"
                "class,mura,mudec from DC5_Truth"
                " where (RA between %f and %f) and "
                " (DEC between %f and %f)",
                min_ra-deg_tol,max_ra+deg_tol,
                min_dec-deg_tol,max_dec+deg_tol);
  

    stmt=db->Statement(db_cmd.c_str());
    
    stmt->Process();
    stmt->StoreResult();
    
    double ra,dec;
    double g_mag,r_mag,i_mag,z_mag,Y_mag,mura,mudec;
    int star_class;
    string true_class;
    
    while(stmt->NextResultRow()) {
      ra=stmt->GetDouble(0);       
      dec=stmt->GetDouble(1);       
      g_mag=stmt->GetDouble(2);       
      r_mag=stmt->GetDouble(3);       
      i_mag=stmt->GetDouble(4);       
      z_mag=stmt->GetDouble(5);       
      Y_mag=stmt->GetDouble(6);       
      if(stmt->GetString(7)=="G") star_class=1;
      else star_class=0;

      
      double mag;
      if(band=="g") mag=g_mag;
      else if(band=="r") mag=r_mag;
      else if(band=="i") mag=i_mag;
      else if(band=="z") mag=z_mag;
      else if(band=="Y") mag=Y_mag;
      
      if(extinction) {
        
//         double R_g=3.793;
//         double R_r=2.751;
//         double R_i=2.086;
//         double R_z=1.479;
        
//         double EBV=ExtMap.GetExtinction(ra,dec);
        
//         /* apply extinction correction in each band */
//         if(band=="g") mag -= R_g * EBV;
//         else if(band=="r") mag -= R_r * EBV;
//         else if(band=="i") mag -= R_i * EBV;
//         else if(band=="z") mag -= R_z * EBV;

      }
      
      Object ob(ra,dec,mag,0,band,0,0);
      
      if(truth_table=="DC5_Truth") {
        //ob.class_star=stmt->GetInt(7);
        if(proper_motion) {
          ob.mura=stmt->GetDouble(8);
          ob.mudec=stmt->GetDouble(9);      
        }
      }

      ob.class_star=star_class;
      double cd=cos(ob.Dec*TMath::DegToRad());
      ob.xyz[0]=cos(ob.RA*TMath::DegToRad())*cd;
      ob.xyz[1]=sin(ob.RA*TMath::DegToRad())*cd;
      ob.xyz[2]=sin(ob.Dec*TMath::DegToRad());
      ob.photometric=0;
      truth_tree.insert(ob);
      truth_vec.push_back(ob);
     
    }
    db->Close();
  }
  else if( truth_table=="standards") {
    
      db = TSQLServer::Connect("oracle://desdb/stdes",
                               "pipeline", "dc01user");
      
      db_cmd=Form("select radeg,decdeg,stdmag_g,stdmag_r,stdmag_i,stdmag_z," 
                  "stdmagerr_g,stdmagerr_r,stdmagerr_i,stdmagerr_z " 
                  " FROM standard_stars "
                  " where (radeg between %f and %f) "
                  " and (decdeg between %f and %f)",
                  min_ra-deg_tol,max_ra+deg_tol,
                  min_dec-deg_tol,max_dec+deg_tol);
      if(debug) cout<<db_cmd<<endl;
      stmt=db->Statement(db_cmd.c_str());
      
      stmt->Process();
      stmt->StoreResult();
      
      double ra,dec;
      double g_mag,r_mag,i_mag,z_mag,Y_mag,mura,mudec;
      double g_magerr,r_magerr,i_magerr,z_magerr,Y_magerr;
      string true_class;
      int star_class;
      while(stmt->NextResultRow()) {
        ra=stmt->GetDouble(0);       
        dec=stmt->GetDouble(1);       
        g_mag=stmt->GetDouble(2);       
        r_mag=stmt->GetDouble(3);       
        i_mag=stmt->GetDouble(4);       
        z_mag=stmt->GetDouble(5);       
        g_magerr=stmt->GetDouble(6);       
        r_magerr=stmt->GetDouble(7);       
        i_magerr=stmt->GetDouble(8);       
        z_magerr=stmt->GetDouble(9);       

        star_class=0;
        
        
        double mag;
        if(band=="g") mag=g_mag;
        else if(band=="r") mag=r_mag;
        else if(band=="i") mag=i_mag;
        else if(band=="z") mag=z_mag;
        else if(band=="Y") mag=Y_mag;
        double magerr;
        if(band=="g")      magerr=g_magerr;
        else if(band=="r") magerr=r_magerr;
        else if(band=="i") magerr=i_magerr;
        else if(band=="z") magerr=z_magerr;
        else if(band=="Y") magerr=Y_magerr;
        
        
        if(extinction) {
          
//           double R_g=3.793;
//           double R_r=2.751;
//           double R_i=2.086;
//           double R_z=1.479;
          
//           double EBV=ExtMap.GetExtinction(ra,dec);
          
//           /* apply extinction correction in each band */
//           if(band=="g") mag -= R_g * EBV;
//           else if(band=="r") mag -= R_r * EBV;
//           else if(band=="i") mag -= R_i * EBV;
//           else if(band=="z") mag -= R_z * EBV;
          
        }
        
        Object ob(ra,dec,mag,magerr,band,0,0);
        ob.class_star=star_class;
        double cd=cos(ob.Dec*TMath::DegToRad());
        ob.xyz[0]=cos(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[1]=sin(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[2]=sin(ob.Dec*TMath::DegToRad());
        ob.photometric=0;
        truth_tree.insert(ob);
        truth_vec.push_back(ob);
      
      }


      db_cmd=Form("select ra,dec,%s_g,%s_r,%s_i,%s_z,"
                  " %serr_g,%serr_r,%serr_i,%serr_z "
                  " FROM galaxy_standards "
                  " where (ra between %f and %f) "
                  " and (dec between %f and %f)",
                  true_mag.c_str(),true_mag.c_str(),
                  true_mag.c_str(),true_mag.c_str(),
                  true_mag.c_str(),
                  true_mag.c_str(),true_mag.c_str(),
                  true_mag.c_str(),true_mag.c_str(),
                  true_mag.c_str(),
                  min_ra-deg_tol,max_ra+deg_tol,
                  min_dec-deg_tol,max_dec+deg_tol);
         
      stmt=db->Statement(db_cmd.c_str());
      
      stmt->Process();
      stmt->StoreResult();
      
      while(stmt->NextResultRow()) {
        ra=stmt->GetDouble(0);       
        dec=stmt->GetDouble(1);       
        g_mag=stmt->GetDouble(2);       
        r_mag=stmt->GetDouble(3);       
        i_mag=stmt->GetDouble(4);       
        z_mag=stmt->GetDouble(5); 
        g_magerr=stmt->GetDouble(6);       
        r_magerr=stmt->GetDouble(7);       
        i_magerr=stmt->GetDouble(8);       
        z_magerr=stmt->GetDouble(9);             
        star_class=1;
        
        
        double mag;
        if(band=="g")      mag=g_mag;
        else if(band=="r") mag=r_mag;
        else if(band=="i") mag=i_mag;
        else if(band=="z") mag=z_mag;
        else if(band=="Y") mag=Y_mag;
        
        double magerr;
        if(band=="g")      magerr=g_magerr;
        else if(band=="r") magerr=r_magerr;
        else if(band=="i") magerr=i_magerr;
        else if(band=="z") magerr=z_magerr;
        else if(band=="Y") magerr=Y_magerr;
        
        if(extinction) {
          
//           double R_g=3.793;
//           double R_r=2.751;
//           double R_i=2.086;
//           double R_z=1.479;
          
//           double EBV=ExtMap.GetExtinction(ra,dec);
          
//           /* apply extinction correction in each band */
//           if(band=="g") mag -= R_g * EBV;
//           else if(band=="r") mag -= R_r * EBV;
//           else if(band=="i") mag -= R_i * EBV;
//           else if(band=="z") mag -= R_z * EBV;

        }
        
        Object ob(ra,dec,mag,magerr,band,0,0);
        ob.class_star=star_class;
        double cd=cos(ob.Dec*TMath::DegToRad());
        ob.xyz[0]=cos(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[1]=sin(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[2]=sin(ob.Dec*TMath::DegToRad());
        ob.photometric=0;
        truth_tree.insert(ob);
        truth_vec.push_back(ob);
      
      }
      db->Close();
    }

    else if( truth_table=="usnob") {

      db = TSQLServer::Connect("oracle://desdb/stdes",
                               "pipeline", "dc01user");

      db_cmd=Form("select ra,dec,b2,r2,i2 from usnob_cat1"
                  " where (ra between %f and %f) "
                  " and (dec between %f and %f) ",
                  min_ra-deg_tol,max_ra+deg_tol,
                  min_dec-deg_tol,max_dec+deg_tol);
      if(debug) cout<<stmt<<endl;
      stmt=db->Statement(db_cmd.c_str());
      
      stmt->Process();
      stmt->StoreResult();
      
      double ra,dec;
      double g_mag,r_mag,i_mag,z_mag,Y_mag,mura,mudec;
      double b2_mag,r2_mag,i2_mag;
      double g_magerr,r_magerr,i_magerr,z_magerr,Y_magerr;
      string true_class;
      int star_class;
      while(stmt->NextResultRow()) {
        ra=stmt->GetDouble(0);       
        dec=stmt->GetDouble(1);       
        b2_mag=stmt->GetDouble(2);
        r2_mag=stmt->GetDouble(3);
        i2_mag=stmt->GetDouble(4);

        g_mag=b2_mag-0.15;
        r_mag=r2_mag+0.2;
        i_mag=i2_mag;
        z_mag=i_mag-0.025-(0.8/1.9)*(r_mag-i_mag);
        
        g_magerr=0;
        r_magerr=0;
        i_magerr=0;
        z_magerr=0;
        
        double mag;
        if(band=="g") mag=g_mag;
        else if(band=="r") mag=r_mag;
        else if(band=="i") mag=i_mag;
        else if(band=="z") mag=z_mag;
        else if(band=="Y") mag=Y_mag;
        double magerr;
        if(band=="g")      magerr=g_magerr;
        else if(band=="r") magerr=r_magerr;
        else if(band=="i") magerr=i_magerr;
        else if(band=="z") magerr=z_magerr;
        else if(band=="Y") magerr=Y_magerr;
        
        
        if(extinction) {
          
//           double R_g=3.793;
//           double R_r=2.751;
//           double R_i=2.086;
//           double R_z=1.479;
          
//           double EBV=ExtMap.GetExtinction(ra,dec);
          
//           /* apply extinction correction in each band */
//           if(band=="g") mag -= R_g * EBV;
//           else if(band=="r") mag -= R_r * EBV;
//           else if(band=="i") mag -= R_i * EBV;
//           else if(band=="z") mag -= R_z * EBV;

        }
        
        Object ob(ra,dec,mag,magerr,band,0,0);
        ob.class_star=0;
        double cd=cos(ob.Dec*TMath::DegToRad());
        ob.xyz[0]=cos(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[1]=sin(ob.RA*TMath::DegToRad())*cd;
        ob.xyz[2]=sin(ob.Dec*TMath::DegToRad());
        ob.photometric=0;
        truth_tree.insert(ob);
        truth_vec.push_back(ob);
      
      }

      db->Close();

    }
  }
  
  evt=Form("Found %d  objects from truth table between "
           "RA %0.2f %0.2f  Dec %0.2f %0.2f",
           truth_vec.size(),min_ra,max_ra,min_dec,max_dec);
  report_evt(verbose,"STATUS",1,evt);
  vector<ObjPair> pairs,gal_pairs,star_pairs;
  vector<Object>::iterator obj_iter=obs_vec.begin();
  
//   TFile tfile("test.root","recreate");
  TTree *match_tree=new TTree("match","");
  TTree *nomatch_obs_tree=new TTree("nomatch_obs","");
  TTree *nomatch_truth_tree=new TTree("nomatch_truth","");
  Object ob_obs,ob_truth;
  match_tree->Branch("obs",&ob_obs);
  nomatch_obs_tree->Branch("obs",&ob_obs);
  match_tree->Branch("truth",&ob_truth);
  nomatch_truth_tree->Branch("truth",&ob_truth);
  for(;obj_iter!=obs_vec.end(); obj_iter++) {
    
    // find closest match within tolerance
    KD_Match match=truth_tree.find_nearest(*obj_iter,tol);
    
    if(match.first!=truth_tree.end()) {
      
      
      ObjPair pa;
      pa.obs=*obj_iter;
      pa.truth=*match.first;

      if(proper_motion) {
        double deltayear=(mjd-51544)/365.25;

        pa.truth.RA+=pa.truth.mura*(0.001/3600)*deltayear/
          cos(pa.truth.Dec*TMath::DegToRad());
        pa.truth.Dec+=pa.truth.mudec*(0.001/3600)*deltayear;
      }
      // find the object in the
      // Use photometric to flag as being
      //vector<Object>::iterator ma=find(truth_vec.begin(),
      //				       truth_vec.end(),pa.truth);
      //if(ma!=truth_vec.end()) ma->photometric=1;
      pairs.push_back(pa);
      ob_obs=pa.obs;
      ob_truth=pa.truth;
      //match_tree->Fill();
    }
    else {
      nomatch_obs.push_back(*obj_iter);
      ob_obs=*obj_iter;
      //nomatch_obs_tree->Fill();
    }
  }
        
  int N_pair=pairs.size();
  evt=Form("Found %d matches with truth",N_pair);
  report_evt(verbose,"STATUS",1,evt);
 
  vector<Object>::iterator tree_iter=truth_vec.begin();
  for( ; tree_iter!=truth_vec.end(); tree_iter++) {
//     if(!tree_iter->photometric) {
//       nomatch_truth.push_back(*tree_iter);
//       ob_truth=*tree_iter;
//      nomatch_truth_tree->Fill();
    //  }
  }

  evt=Form("Found %d truth objects with no match",nomatch_truth.size());
  report_evt(verbose,"STATUS",1,evt);
  evt=Form("Found %d observed objects with no match",nomatch_obs.size());
  report_evt(verbose,"STATUS",1,evt);


//   match_tree->Write();
//   nomatch_obs_tree->Write();
//   nomatch_truth_tree->Write();



  // Calculate Zero Point
  

  

  vector<double> mag_diff,magerr_diff;
  for(int i=0;i<pairs.size();++i) {

    mag_diff.push_back(pairs[i].obs.mag-pairs[i].truth.mag);
    magerr_diff.push_back(pairs[i].obs.mag_err-pairs[i].truth.mag_err);
    
  }
  
  if( calc_zp) {
    int good,bad;
    double mean,rms;
    sigma_clip(3,2.5,mag_diff,magerr_diff,mean,
               good,bad,rms,true,false,1e-6);
    
    evt=Form("Calculed ZP=%0.3f",mean);
    report_evt(verbose,"STATUS",1,evt);
    for(int i=0;i<pairs.size();i++) pairs[i].obs.mag-=mean;
    
  }



  
   // remove pairs greater than the maximum
  int original_pairs=pairs.size();
  vector<ObjPair>::iterator iter=pairs.begin();
  while(iter!=pairs.end()) {
    
    if(fabs(iter->obs.mag-iter->truth.mag)>max_mag) pairs.erase(iter);
    else iter++;
  }
  evt=Form("Removed %d  with greater than %0.2f magnitude difference",
           original_pairs-pairs.size(),max_mag);
  report_evt(verbose,"STATUS",1,evt);

 
  // Make pairs of star/galaxy

  for(int i=0;i<pairs.size();++i) {

    if(pairs[i].truth.class_star==1) gal_pairs.push_back(pairs[i]);
    else star_pairs.push_back(pairs[i]);
  }

  evt=Form("Found %d stars and %d galaxy matches",star_pairs.size(),
           gal_pairs.size());
  report_evt(verbose,"STATUS",1,evt);

  int desired_bins=2;
  if(gal_pairs.size()>40){
    //plot_phot(gal_pairs,
    //        "Galaxies",dir,band,obs_label,debug,
    //        sigma,gal_pairs.size()/desired_bins);
  }
  if(star_pairs.size()>40){
    //plot_phot(star_pairs,
    //        "Stars",dir,band,obs_label,debug,
    //        sigma,star_pairs.size()/desired_bins);
  }

 
  if(star_pairs.size()>40){
    plot_astro(star_pairs,
               star_ra_mean,star_ra_clip_mean,
               star_ra_rms,star_ra_clip_rms,
               star_dec_mean,star_dec_clip_mean,
               star_dec_rms,star_dec_clip_rms,
               "Stars",dir,band,debug,draw
               );
  }

 
  if(gal_pairs.size()>40){
    plot_astro(gal_pairs,
               gal_ra_mean,gal_ra_clip_mean,
               gal_ra_rms,gal_ra_clip_rms,
               gal_dec_mean,gal_dec_clip_mean,
               gal_dec_rms,gal_dec_clip_rms,
               "Galaxies",dir,band,debug,draw
               );
  }

  tree.Fill();

  if(insert_db) {
      
    if(imageid==-1) {
      cout<<"STAUTS4BEG Cannot insert into database without imageid "   \
          <<"STATUS4END"<<endl;
      exit(1);
    }
    
    TSQLServer *db = TSQLServer::Connect("oracle://desdb/prdes",
                                           "des_admin", "desmgr");;
    string db_cmd=Form("insert into astrometry_qa  (imageid, "
                       "ra_clipmean_star ,ra_cliprms_star ,"
                       "ra_clipmean_gal ,ra_cliprms_gal ,"
                       "ra_mean_star ,ra_rms_star ,ra_mean_gal ,"
                       "ra_rms_gal ,dec_clipmean_star ,dec_cliprms_star ,"
                       "dec_clipmean_gal ,dec_cliprms_gal ,"
                       "dec_mean_star ,dec_rms_star ,dec_mean_gal ,"
                       "dec_rms_gal ) values "
                       "( %d,%f,%f,%f,%f,%f,%f,%f,%f,"
                       "%f,%f,%f,%f,%f,%f,%f,%f)",
                       imageid,star_ra_clip_mean,star_ra_clip_rms,
                       gal_ra_clip_mean,gal_ra_clip_rms,
                       star_ra_mean,star_ra_rms,gal_ra_mean,gal_ra_rms,
                       star_dec_clip_mean,star_dec_clip_rms,
                       gal_dec_clip_mean,gal_dec_clip_rms,
                       star_dec_mean,star_dec_rms,gal_dec_mean,gal_dec_rms);
    cout<<db_cmd<<endl;
    TSQLStatement *stmt=db->Statement(db_cmd.c_str());
    stmt->Process();
    db->Commit();
    delete stmt;
  }

  }
  


 //  plot_phot(gal_pairs,
//             "Galaxy Photometry",dir,band,debug,
//             sigma,N_bin);


  TFile tfile(rootfile.c_str(),"recreate");
  tree.Write();
   tfile.Close();
  timer.Print();
 
//   for(match_tree
 //  plot_astro(gal_pairs,pad,can_counter,
//              "Stellar Astrometry",dir,band,debug,
//              gal_ra_mean,gal_ra_sigmal,
//              gal_ra_sigmar,
//              gal_mean_dec,gal_dec_sigmal,
//              gal_dec_sigmar
//              );


//   vector<double> mag_diff_star,ra_diff_star,
//     dec_diff_star,mag_diff_gal,ra_diff_gal,
//     dec_diff_gal;
  
  
  
//   vector<ObjPair>::iterator pair_iter=pairs.begin();
//   for( ; pair_iter!=pairs.end();++pair_iter) {
//     Object obs=pair_iter->obs;
//     Object truth=pair_iter->truth;

//     double cosdec=TMath::Cos(truth.Dec*TMath::DegToRad());
//     if(truth.class_star==1) {
      
//       double deltayear=(obs.mjd-51544)/365.25;
      
//       truth.RA+=truth.mura*(0.001/3600)*deltayear/
//         cos(truth.Dec*TMath::DegToRad());
//             truth.Dec+=truth.mudec*(0.001/3600)*deltayear;
            
//             class_star=1;
//             m_ra=obs.RA;
//             m_dec=obs.Dec;
//             m_mag=obs.mag;
//             t_ra=truth.RA;
//             t_dec=truth.Dec;
//             t_mag=truth.mag;
//             sdiff_mag=(obs.mag-truth.mag);
//             sdiff_ra=(cosdec*(obs.RA-truth.RA))*3600;
//             sdiff_dec=(obs.Dec-truth.Dec)*3600;
//             //treeall->Fill();
  
//             mag_diff_star.push_back((obs.mag-truth.mag));
//             ra_diff_star.push_back((cosdec*(obs.RA-truth.RA))*3600);
//             dec_diff_star.push_back((obs.Dec-truth.Dec)*3600);
//             //cout<<obs.Dec-truth.Dec<<endl;
//           }
//           else {
//             class_star=0;
//             m_ra=obs.RA;
//             m_dec=obs.Dec;
//             m_mag=obs.mag;
//             t_ra=truth.RA;
//             t_dec=truth.Dec;
//             t_mag=truth.mag;
//             sdiff_mag=(obs.mag-truth.mag);
//             sdiff_ra=(cosdec*(obs.RA-truth.RA))*3600;
//             sdiff_dec=(obs.Dec-truth.Dec)*3600;
//             //treeall->Fill();

//             mag_diff_gal.push_back(obs.mag-truth.mag);
//             ra_diff_gal.push_back(cosdec*(obs.RA-truth.RA)*3600);
//             dec_diff_gal.push_back((obs.Dec-truth.Dec)*3600);
//           }
          
//         }
      
//         int good, bad;
//         double mean,rms;
//         vector<double> dum(mag_diff_star.size());
       
//         sigma_clip(3,2.5,mag_diff_star,dum,mean,
//                    good,bad,rms,true,false,1e-6);
//         TH1D mags(Form("mags%d",bin_counter+1),"",50,mean-5*rms,mean+5*rms);
//         for(int i=0;i<mag_diff_star.size();++i)mags.Fill(mag_diff_star[i]);
//         mags.Write();
//         photo_star->Fill(Form("%d",imageid),mean);
//         photo_star->SetBinError(bin_counter+1,rms);
//         mm_star=mean;
//         mr_star=rms;


//         sigma_clip(3,2.5,ra_diff_star,dum,mean,
//                    good,bad,rms,true,false,1e-6);
//         TH1D ras(Form("ras%d",bin_counter+1),"",50,mean-5*rms,mean+5*rms);
//         for(int i=0;i<ra_diff_star.size();++i)ras.Fill(ra_diff_star[i]);
//         ras.Write();
//         ra_star->Fill(Form("%d",imageid),mean);
//         ra_star->SetBinError(bin_counter+1,rms);
//         rm_star=mean;
//         rr_star=rms;

//         sigma_clip(3,2.5,dec_diff_star,dum,mean,
//                    good,bad,rms,true,false,1e-6);
//         TH1D decs(Form("decs%d",bin_counter+1),"",50,mean-5*rms,mean+5*rms);
//         for(int i=0;i<dec_diff_star.size();++i) decs.Fill(dec_diff_star[i]);
//         decs.Write();
//         dec_star->Fill(Form("%d",imageid),mean);
//         dec_star->SetBinError(bin_counter+1,rms);
//         dm_star=mean;
//         dr_star=rms;

//         dum.resize(mag_diff_gal.size());


//         sigma_clip(3,2.5,mag_diff_gal,dum,mean,
//                    good,bad,rms,true,false,1e-6);
//         TH1D magg(Form("magg%d",bin_counter+1),"",50,mean-5*rms,mean+5*rms);
//         for(int i=0;i<mag_diff_gal.size();++i)ras.Fill(mag_diff_gal[i]);
//         mags.Write();
//         photo_gal->Fill(Form("%d",imageid),mean);
//         photo_gal->SetBinError(bin_counter+1,rms);
//         mm_gal=mean;
//         mr_gal=rms;

//         sigma_clip(3,2.5,ra_diff_gal,dum,mean,
//                    good,bad,rms,true,false,1e-6);
//         TH1D rag(Form("rag%d",bin_counter+1),"",50,mean-5*rms,mean+5*rms);
//         for(int i=0;i<ra_diff_gal.size();++i)rag.Fill(ra_diff_gal[i]);
//         rag.Write();
//         ra_gal->Fill(Form("%d",imageid),mean);
//         ra_gal->SetBinError(bin_counter+1,rms);
//         rm_gal=mean;
//         rr_gal=rms;

//         sigma_clip(3,2.5,dec_diff_gal,dum,mean,
//                    good,bad,rms,true,false,1e-6);
//         TH1D decg(Form("decg%d",bin_counter+1),"",50,mean-5*rms,mean+5*rms);
//         for(int i=0;i<dec_diff_gal.size();++i) decg.Fill(dec_diff_gal[i]);
//         decg.Write();
//         dec_gal->Fill(Form("%d",imageid),mean);
//         dec_gal->SetBinError(bin_counter+1,rms);
//         rm_gal=mean;
//         rr_gal=rms;


        
//         tree->Fill();

       
//         bin_counter++;




//       }
//       tree->Write();
//       //treeall->Write();
//       photo_star->Write();
//       ra_star->Write();
//       dec_star->Write();
    
//       photo_gal->Write();
//       ra_gal->Write();
//       dec_gal->Write();
//       outtfile->Close();    
//     }
      
//   }
    
    
    
}


void report_evt(bool verbose,string type,int level,string mess)
{
  
  if( verbose) {
    if(type=="STATUS") {
      cout<<"STATUS"<<level<<"BEG "<<mess<<" STATUS"<<level<<"END"<<endl;
    }
    else if(type=="QA") {
      cout<<"QA"<<level<<"BEG "<<mess<<" QA"<<level<<"END"<<endl;
    }
    else {
      cout<<"STATUS5BEG Uknown event type "<<type<<endl;
    }
  }
  else {
    if(level==5) cout<<"** "<<mess<<" **"<<endl;
    else cout<<"  "<<mess<<endl;
  }
}


// ostream beg_rep(bool verbose,string type,int level)
// {
//   ostream stream;
//   if( verbose) {
//     if(type=="STATUS") {
//       stream<<"STATUS"<<level<<"BEG ";
//     }
//     else if(type=="QA") {
//       stream<<"QA"<<level<<"BEG ";
//     }
//     else {
//       stream<<"STATUS5BEG Uknown event type "<<type<<endl;
//     }
//   }
//   else {
//     if(level==5) stream<<"** ";
//     else stream<<"  ";
//   }
//   return stream;
// }

// Set style

void LoadStyle()
{


  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  TStyle* miStyle = new  TStyle("miStyle", "MI Style");

  // Colors


  //  set the background color to white
  miStyle->SetFillColor(10);


  miStyle->SetFrameFillColor(10);
  miStyle->SetCanvasColor(10);
  miStyle->SetCanvasDefH(680);
  miStyle->SetCanvasDefW(700);
  miStyle->SetPadColor(10);
  miStyle->SetTitleFillColor(0);
  miStyle->SetStatColor(10);

  //  //dont put a colored frame around the plots
  miStyle->SetFrameBorderMode(0);
  miStyle->SetCanvasBorderMode(0);
  miStyle->SetPadBorderMode(0);

  //use the primary color palette
  miStyle->SetPalette(1);

  //set the default line color for a histogram to be black
  miStyle->SetHistLineColor(kBlack);

  //set the default line color for a fit function to be red
  miStyle->SetFuncColor(kBlue);

  //make the axis labels black
  miStyle->SetLabelColor(kBlack,"xyz");

  //set the default title color to be black
  miStyle->SetTitleColor(kBlack);

  // Sizes
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);


  //set the margins
  miStyle->SetPadBottomMargin(0.16);
  miStyle->SetPadTopMargin(0.1);
  miStyle->SetPadLeftMargin(0.01);
  miStyle->SetPadRightMargin(0.01);

  //set axis label and title text sizes
  miStyle->SetLabelSize(0.04,"x");
  miStyle->SetLabelSize(0.04,"y");
  miStyle->SetTitleSize(0.05,"xyz");
  miStyle->SetTitleOffset(1.1,"x");
  miStyle->SetTitleOffset(1.3,"yz");
  miStyle->SetLabelOffset(0.012,"y");
  miStyle->SetStatFontSize(0.025);
  miStyle->SetTextSize(0.02);
  miStyle->SetTitleBorderSize(0);

  //set line widths
  miStyle->SetHistLineWidth(3);
  miStyle->SetFrameLineWidth(3);
  miStyle->SetFuncWidth(2);

  // Misc

  //align the titles to be centered
  miStyle->SetTextAlign(22);

  //set the number of divisions to show
  miStyle->SetNdivisions(506, "xy");

  //turn off xy grids
  miStyle->SetPadGridX(0);
  miStyle->SetPadGridY(0);

  //set the tick mark style
  miStyle->SetPadTickX(1);
  miStyle->SetPadTickY(1);

  //show the fit parameters in a box
  miStyle->SetOptFit(0111);
  miStyle->SetOptTitle(0);

  //turn off all other stats
  miStyle->SetOptStat(0);
  miStyle->SetStatW(0.20);
  miStyle->SetStatH(0.15);
  miStyle->SetStatX(0.94);
  miStyle->SetStatY(0.92);


  miStyle->SetFillStyle(0);

  //  // Fonts
  miStyle->SetStatFont(42);
  miStyle->SetLabelFont(42,"xyz");
  miStyle->SetTitleFont(42,"xyz");
  // miStyle->SetTextFont(40);

  //done

  miStyle->cd();


  // gROOT->ForceStyle(1);
 

}

void PrintHelp()
{
  cout<<"Usage: slr_check -tile (tile) (options)\n";
  cout<<"Options (default):\n";
  cout<<"  -project    (none)       For looping over all tiles in BCS/DES.\n";
  cout<<"  -run        (latest)     Use a specific run.   \n";
  cout<<"  -color      (no)         Apply BCS color corrections\n";
  cout<<"  -mag        (auto)       Which magnitude to use\n";
  cout<<"  -star_cut   (0.7)        Value of class_star to determine stars\n";
  cout<<"  -err_cut    (0.2)        Maximum flux error\n";
  cout<<"  -outfile    (out.root)   Output file (only if using -project)\n";
  cout<<"  -extinction (no)         Apply extinction correction\n";
  cout<<"  -image_name ((tile).png) Name of output\n";
  cout<<"  -image_dir  (./)         Name of directory for output\n";
}



void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,const double &tol)
{
  int N=array.size();
  mean=TMath::Mean(N,&array[0]);
  rms=TMath::RMS(N,&array[0]);
  good=N;
  bad=0;
  
  for(int i=0;i<N;++i) {
    if(fabs(array[i]-mean) > thresh*rms) bad++;
  }

  if(bad==0 || !clip) return;
  vector<double> new_array;
  vector<double> cur_array;

  vector<double> newerr_array;
  vector<double> curerr_array;
  copy(array.begin(),array.end(),back_inserter(cur_array));
  copy(err.begin(),err.end(),back_inserter(curerr_array));

  double prev_mean=-999;
  int iter=0;
  while(fabs(prev_mean-mean)>=tol) {
    iter++;
    
    if(!weighted) {
      mean=TMath::Mean(cur_array.size(),&cur_array[0]);
    }
    else {
      mean=TMath::Mean(cur_array.size(),&cur_array[0],&curerr_array[0]);
    }
    
    rms=TMath::RMS(cur_array.size(),&cur_array[0]);
    prev_mean=mean;
     
    new_array.clear();

    for(int i=0;i<cur_array.size();++i) {
      if(fabs(cur_array[i]-mean) < thresh*rms) {
        new_array.push_back(cur_array[i]);
        newerr_array.push_back(curerr_array[i]);
      }
    }
    
    if(!weighted) {
      mean=TMath::Mean(new_array.size(),&new_array[0]);
    }
    else {
      mean=TMath::Mean(new_array.size(),&new_array[0],&newerr_array[0]);
    }
    rms=TMath::RMS(new_array.size(),&new_array[0]);
    
    cur_array.clear();
    copy(new_array.begin(),new_array.end(),back_inserter(cur_array));
    copy(newerr_array.begin(),newerr_array.end(),
         back_inserter(curerr_array));


    int N_passed=cur_array.size();
    good=N_passed;
    bad=N-N_passed;
    if(iter>=max_iter) break;
    
  }

  

  
  return;

}


int plot_phot_all(const vector<ObjPair> &obj_pairs,
                  TPad *can,int &cur_can,
                  string title,string dir,string band,
                  bool debug,string true_label,string mag_label)
{
  
  
  // Some formatting options
  gStyle->SetOptStat(1110);
  gStyle->SetOptFit(0010);
  gStyle->SetStatFormat("4.2g"); 
  gStyle->SetFitFormat("4.2g"); 
  //gStyle->SetTitleOffset(1.6,"y");


  vector<double> mag1,mag2,diff_mag,
    mag1_err,mag2_err;
  
  TCanvas *c1=new TCanvas("c1","");
  c1->SetLeftMargin(0.16);  
  
  vector<ObjPair>::const_iterator obj_iter=obj_pairs.begin();
  
  int count=0;
  for(; obj_iter!=obj_pairs.end(); ++obj_iter) {
    
    mag1.push_back(obj_iter->obs.mag);
    mag2.push_back(obj_iter->truth.mag);
    mag1_err.push_back(obj_iter->obs.mag_err);
    mag2_err.push_back(obj_iter->truth.mag_err);
    diff_mag.push_back(mag1[count]-mag2[count]);
    count++;
  }

  double mag_mean;//=TMath::Mean(diff_mag.size(),&diff_mag[0]);
  double mag_rms;//=TMath::RMS(diff_mag.size(),&diff_mag[0]);

  vector<double> dum(diff_mag.size());
  int good,bad;
  sigma_clip(3,2.5,diff_mag,dum,mag_mean,
                   good,bad,mag_rms,true,false,1e-6);
  // Fill histogram
  double max_limit=mag_mean+10*mag_rms,min_limit=mag_mean-10*mag_rms;

  TH1D *Mag_diff=new TH1D("Mag_diff",
                          Form(";%s_{%s} - %s_{%s};Entries",
                               band.c_str(),mag_label.c_str(),
                               band.c_str(),true_label.c_str()),
                          50,min_limit,max_limit);
  
  int total=diff_mag.size();
  for(int i=0;i<diff_mag.size();++i) {
    Mag_diff->Fill(diff_mag[i]);
  }
  Mag_diff->GetXaxis()->CenterTitle();
  Mag_diff->GetYaxis()->CenterTitle();
  
  // fit to a bifurcated gaussian
  TF1 *func = new TF1("func","gaus",min_limit,max_limit);
  func->SetNpx(100000);
  func->SetParameters(100,0.0,0.1);
  func->SetParNames("Constant","Mean Fit","#sigma");
  //func->SetParLimits(2,0,10);
  //func->SetParLimits(3,0,10);
    Mag_diff->Fit(func,"RQ");
    Mag_diff->Draw();
    c1->Update();
    
    double gaus_const=func->GetParameter(0);
    double gaus_mean=func->GetParameter(1);
    double gaus_sigma=func->GetParameter(2);
    
    double cut=3*gaus_sigma;
    double min=gaus_mean-cut;
    double max=gaus_mean+cut;
    
    
    // 3sigma cut
    
    int inside=0;
    for(int i=0;i<diff_mag.size();++i) {
      if(diff_mag[i]<max && diff_mag[i]>min) inside++;
    }
    
    // calculate the outlier fraciton
    double outside=1-(1.*inside/total);


    
    // Find systematic error
    vector<double> fit_diff_mag,fit_mag1_err,fit_mag2_err;
    
    for(int i=0;i<diff_mag.size();i++) {
      if(diff_mag[i]>min && diff_mag[i]<max) {
	fit_diff_mag.push_back(diff_mag[i]);
	fit_mag1_err.push_back(mag1_err[i]);
	fit_mag2_err.push_back(mag2_err[i]);
      }
    }

    SysErrFit Mag_fit;
    double Mag_min_err;
    double Mag_min=Mag_fit.Fit(&fit_diff_mag,&fit_mag1_err,
                               Mag_min_err,&fit_mag2_err);
    if(debug)cout<<"Mag Minimization Minimum Found at = "<<Mag_min<<endl;

    
    Mag_diff->Draw();
    
    // Add text to statbox
    TPaveStats *ps_Mag = (TPaveStats*)c1->GetPrimitive("stats");
    ps_Mag->SetName("mystats_Mag");
    ps_Mag->SetX1NDC(0.675) ; 
    ps_Mag->SetX2NDC(0.976) ; 
    ps_Mag->SetY1NDC(0.334) ; 
    ps_Mag->SetY2NDC(0.871);
    TList *list_Mag = ps_Mag->GetListOfLines();
    TText *tconst_Mag = ps_Mag->GetLineWith("Constant"); 

    if(tconst_Mag) {
      TLatex *myt_Magsys = new 
        TLatex(0,0,Form("#sigma_{sys}= %2.2g",Mag_min));
      myt_Magsys->SetTextFont(tconst_Mag->GetTextFont());
      myt_Magsys->SetTextSize(tconst_Mag->GetTextSize());
      myt_Magsys->SetTextColor(2);
      list_Mag->Add(myt_Magsys);
      
      TLatex *myt_Mag = new 
        TLatex(0,0,Form("Frac > 3#sigma = %0.2f",outside));
      if(myt_Mag) {
        myt_Mag->SetTextFont(tconst_Mag->GetTextFont());
        myt_Mag->SetTextSize(tconst_Mag->GetTextSize());
        myt_Mag->SetTextColor(2);
        list_Mag->Add(myt_Mag);
      }
      
      list_Mag->Remove(tconst_Mag); 

    }
    
    ps_Mag->SetTextSize(0.053);
    ps_Mag->SetBorderSize(0);
    ps_Mag->SetTextColor(2);
    Mag_diff->SetStats(0);
    ps_Mag->Draw();
    
    
    
    // Draw top label
    TLatex *label=new TLatex;
    label->SetNDC();
    label->SetTextSize(0.1);
    label->SetTextFont(22);
    label->SetTextAlign(22);
    label->SetTextColor(kBlue);
    string Mag_label;
    Mag_label=title;
    label->DrawLatex(0.49,0.948,Mag_label.c_str());
    
    if(debug) {
      c1->Update();
      c1->WaitPrimitive();
    }
    // print out plot
    string id=title+"_"+band+"_Mag";
    string print=dir+"/"+id+".png";
    c1->Print(print.c_str());
    
    string convert="convert "+id+".eps "+id+".png";
    //gSystem->Exec(convert.c_str());
    
    convert="rm "+print;
    //gSystem->Exec(convert.c_str());
    //Mag_diff->Draw();

    
    delete c1;
    delete func;
    delete Mag_diff;

 }

void plot_phot(const vector<ObjPair> &obj_pairs,
               string title,string dir,
	       string band,string mag,
               bool debug,
	       double sigma,int N_bin)
{
  int min_entries=20; //minimum entries per bin
  int func_min=0;    // min function limit
  int func_max=1;   // max function limit
  double hist_fit_max; // max for histogram fitting the gaussian
  int func_fit=3; // this*rms is max for function fitting the gaussian
 
  // use Minuit2 to do the fitting
  // I found that it did better in some cases
  TVirtualFitter::SetDefaultFitter("Minuit2" );

   
  TCanvas *c1=new TCanvas("c1","");

  
  
  string cur_title=band+" band "+title+" ";
  	
  hist_fit_max=0.12;
  
  int N_entries = obj_pairs.size();
  
  // Sort tree into index according to true
  vector<double>  truth_mag;

  vector<ObjPair>::const_iterator obj_iter=obj_pairs.begin();
  for(; obj_iter!=obj_pairs.end(); ++obj_iter) {
    truth_mag.push_back(obj_iter->truth.mag);
  }

  vector<double> ordered_truth_mag(truth_mag.size());

  int *index = new int[N_entries];
  TMath::Sort(N_entries,&truth_mag[0],index,false);

  // Fill ordered vectors
  vector<ObjPair> ordered_pair(N_entries);
  
  for(int i=0;i<N_entries;i++) {
    ordered_pair[i]=obj_pairs[index[i]];
    ordered_truth_mag[i]=truth_mag[index[i]];
  }
  
  
  TGraph *sigma_scatter=new TGraph;  
  vector<double> bins,sys_error,tot_error,mean_error,mean_vec,frac_passed;
  int global_counter=0,sigma_counter=0;

  int bin=0;

  vector<double> full_diff,full_mean,diff_mag;
  // Loop until the end of the vectors have been reached
  bool finished=false;
  while(!finished) {

    if(debug) {
      cout<<"\nBin "<<bin<<endl;
      cout<<"**************************"<<endl;
    }
    TGraph scatter,scatter_clipped;


    // create sub list of vectors with either N_bin entries or
    // the number in a mag difference of 1
    vector<ObjPair> sub_pair(N_bin),cut_pair(N_bin);

    int current_min=global_counter;
    for(int entry=0; entry<N_bin ; entry++) {
      
      // check if at the end of the vector;
      if(global_counter  >= N_entries) {
        finished=true;
      
        break;
      }
      
      scatter.SetPoint(entry,ordered_truth_mag[global_counter],
                       ordered_pair[global_counter].obs.mag-
                       ordered_pair[global_counter].truth.mag);
      
      sub_pair[entry]=ordered_pair[global_counter];
      diff_mag.push_back(ordered_pair[global_counter].obs.mag-
                         ordered_pair[global_counter].truth.mag);

      //check if the mag difference is greater than 1 between the
      //current and first of the bin
     //  if(entry>2) {
//         //cout<<ordered_truth_mag[current_min]<<" "<<ordered_truth_mag[global_counter]<<endl;
//         if(ordered_truth_mag[global_counter]-
//            ordered_truth_mag[current_min] >1.) {
//           global_counter++;

//           break;
//         }
//       }
      
      
      global_counter++;
    }
    
    int N_actual=global_counter-current_min;
    sub_pair.resize(N_actual);
    
    if(debug) cout<<"Subset Size: "<<sub_pair.size()<<" from: "
                  <<sub_pair[0].truth.mag<<" to "
                  <<sub_pair[N_actual-1].truth.mag<<endl;

    // Make sure there at least min_entries
    if(sub_pair.size()<min_entries) {
      if(debug) {
        cout<<"Not enough total points skipping this bin. Skipping..."<<endl;
      }
      continue;
    }
     
    

    vector<double> dum(diff_mag.size());
    int good,bad;
    double mag_mean,mag_rms;
    sigma_clip(3,2.5,diff_mag,dum,mag_mean,
               good,bad,mag_rms,true,false,1e-6);
  
    
    double max_limit=mag_mean+4*mag_rms,min_limit=mag_mean-4*mag_rms;

    
    // fit to gaussian and to do  sigma clipping
    TH1D *h1=new TH1D(Form("fit%d",bin+1),"",35,min_limit,max_limit);
    for(int i=0;i<sub_pair.size();i++) {
      h1->Fill(sub_pair[i].obs.mag-sub_pair[i].truth.mag);
    }
    
    if(h1->Integral()<min_entries) {
      h1->SetName(""); // to remove warning messages
      if(debug) {
        cout<<"Not enough points for histogram for this bin.  Skipping..."<<endl;
      }
      continue;
    }
    
    double hist_mean=h1->GetMean();
    double hist_rms=h1->GetRMS();
    if(debug) cout<<"Clipped Mean "<<mag_mean<<" Clipped RMS "<<mag_rms<<endl;
    if(debug) cout<<"Hist Mean "<<hist_mean<<" RMS "<<hist_rms<<endl;
    
    TF1 *f1=new TF1("f1","gaus",min_limit,max_limit);
    
    f1->SetNpx(10000);
    f1->SetParameter(1,0);
    h1->Fit(f1,"QR");
    
    double gaus_const=f1->GetParameter(0);
    double gaus_mean=f1->GetParameter(1);
    double gaus_sigma=f1->GetParameter(2);
    if(debug) cout<<"Fit Mean "<<gaus_mean<<" Fit Sigma "<<gaus_sigma<<endl;
    // draw lines to indicate cuts
    if(debug) {
      
      TLine *hi=new TLine(gaus_mean+sigma*gaus_sigma,0,
                          gaus_mean+sigma*gaus_sigma,h1->GetMaximum());
      TLine *low=new TLine(gaus_mean-sigma*gaus_sigma,0,
                           gaus_mean-sigma*gaus_sigma,h1->GetMaximum());
      
      low->SetLineWidth(3);
      hi->SetLineWidth(3);
      hi->SetLineColor(2);
      low->SetLineColor(2);
      h1->Draw();
      hi->Draw();
      low->Draw();
    }
    
    // erase name to not have warning messages
    h1->SetName("");
    
    // cut out all values greater than sigma*gaus_sigma(from fit)
    vector<double> vec_mag1,vec_mag2,vec_diff,vec_err1,vec_err2,vec_mean;
      
    
    int clipped_counter=0;
    for(int entry=0; entry<sub_pair.size() ; entry++) {
      double diff=sub_pair[entry].obs.mag-sub_pair[entry].truth.mag;
      double mean=(sub_pair[entry].obs.mag+sub_pair[entry].truth.mag)/2.;

      full_diff.push_back(diff);
      full_mean.push_back(mean);
      if(fabs(diff-gaus_mean)<sigma*gaus_sigma) {

        vec_mag1.push_back(sub_pair[entry].obs.mag);
        vec_mag2.push_back(sub_pair[entry].truth.mag);
        vec_diff.push_back(diff);
        vec_err1.push_back(sub_pair[entry].obs.mag_err);
        vec_err2.push_back(sub_pair[entry].truth.mag_err);

        vec_mean.push_back(mean);
        cut_pair.push_back(sub_pair[entry]);
        scatter_clipped.SetPoint(clipped_counter,mean,diff);
        clipped_counter++;
        sigma_scatter->SetPoint(sigma_counter,mean,diff);
        sigma_counter++;
      }
      
    }
    

    //h1->Write();
    
    
    // calculate how many events were cut
    
    if(debug) cout<<"Good entries "<<vec_diff.size()<<endl;      
    
    if(vec_diff.size()<min_entries) {
      if(debug) cout<<"Not enough good points. Skipping..."<<endl;
      continue;
    }
    double frac=1.*vec_diff.size()/sub_pair.size();
    frac_passed.push_back(frac);
    
        
    scatter.SetName(Form("scatter%d",bin+1));
    //scatter.Write();
    
    scatter_clipped.SetName(Form("scatter_clipped%d",bin+1));
    //scatter_clipped.Write();
    
    
    SysErrFit fit;
    double err;
    double min=fit.Fit(&vec_diff,&vec_err1,err,&vec_err2);
    
    
    // Look at what the fitting is doing
    if(debug) {
      c1->Update();
      c1->WaitPrimitive();
    }
    
    
    
    // if minimization fails use 0.0 as default
    if(fabs(min)>func_max) {
      cout<<"Error: root finder failed"<<endl;
      min=0.0;
    };
    if(debug)cout<<"Systematic Error at = "<<abs(min)<<endl;
    
    // set variables
    sys_error.push_back(min);
    tot_error.push_back(gaus_sigma);
    mean_error.push_back(hist_rms/sqrt(h1->Integral()));
    mean_vec.push_back(gaus_mean);
    
    
    // get the last of the observed magnitude for binning
    if(bin==0) bins.insert(bins.end(),ordered_truth_mag.front());
    bins.insert(bins.end(),ordered_truth_mag[global_counter-1]);
    bin++;
    
  }

  cout<<"\n\nBins: ";
  for(int i=0;i<bins.size();i++) cout<<bins[i]<<" ";
  cout<<endl;
  
  TH1D *hist_sys_err=new TH1D("hist_sys_err",
                              ";mag_{mean};Systematic Error / 1000 events",
				    bins.size()-1,&bins[0]);
  TH1D *hist_tot_err=new TH1D("hist_tot_err",
                              ";mag_{mean};Systematic Error / 1000 events",
                              bins.size()-1,&bins[0]);
  TH1D *hist_mean_err=new TH1D("hist_mean_err",
                               ";mag_{true};Systematic Error / 1000 events",
                               bins.size()-1,&bins[0]);
  TH1D *hist_frac=new TH1D("hist_frac",
                           Form(";%s_{true};Fraction within 3#sigma;",
                                band.c_str()),
                           bins.size()-1,&bins[0]);
  
  double view_max=-1e6,view_min=1e6;
  for(int i=0;i<sys_error.size();i++) {
    hist_sys_err->SetBinContent(i+1,mean_vec[i]);
    hist_sys_err->SetBinError(i+1,sys_error[i]);
    
    if(mean_vec[i]-tot_error[i]<view_min) view_min=mean_vec[i]-tot_error[i];
    if(mean_vec[i]+tot_error[i]>view_max) view_max=mean_vec[i]+tot_error[i];

    if(mean_vec[i]-sys_error[i]<view_min) view_min=mean_vec[i]-sys_error[i];
    if(mean_vec[i]+sys_error[i]>view_max) view_max=mean_vec[i]+sys_error[i];

    hist_tot_err->SetBinContent(i+1,mean_vec[i]);
    hist_tot_err->SetBinError(i+1,tot_error[i]);
    
    hist_mean_err->SetBinContent(i+1,mean_vec[i]);
    hist_mean_err->SetBinError(i+1,mean_error[i]);
    
    hist_frac->SetBinContent(i+1,frac_passed[i]);
  }
  
  TLatex *label=new TLatex;
  label->SetNDC();
  label->SetTextSize(0.1);
  label->SetTextColor(kBlue);
  label->SetTextFont(22);
  label->SetTextAlign(22);
  
  // 	c1->SetLeftMargin(0.17);
  // 	hist_frac->GetYaxis()->CenterTitle();
  // 	hist_frac->GetYaxis()->SetRangeUser(0,1);
  // 	hist_frac->SetTitleOffset(1.6,"y");
  // 	hist_frac->GetXaxis()->CenterTitle();
  // 	hist_frac->Draw();
  // 	label->DrawLatex(0.49,0.948,cur_title.c_str());
  // 	c1->Update();
  // 	if(debug) c1->WaitPrimitive();
  
  string id=band+"_frac";
  string print=dir+"/"+id+".png";
  // 	if(debug)c1->WaitPrimitive();
  // 	c1->Print(print.c_str());
  // 	//hist_sys_err->Write();
  // 	//hist_frac->Write();
  label->SetTextSize(0.06);
  
  TCanvas *c2 = new TCanvas("c2","",0,0,772,802);
  TPad *pad1 = new TPad("pad1"," ",0.0,0.37,1.0,1,0);
  TPad *pad2 = new TPad("pad2"," ",0.0,0.0,1.0,0.37,0);
  
  pad1->SetBottomMargin(0.015);
  pad1->SetLeftMargin(0.15);
  pad2->SetLeftMargin(0.15);
  pad2->SetTopMargin(0.015);
  pad2->SetBottomMargin(0.3);
  pad1->SetTicks(1,0);
  
  pad1->Draw();
  pad2->Draw();
  pad1->cd();
  hist_tot_err->SetLabelOffset(1,"x");
  hist_tot_err->SetLabelSize(0.06,"y");
  
  cout<<view_min<<" "<<view_max<<endl;
  hist_tot_err->GetYaxis()->SetRangeUser(view_min-0.1,view_max+0.1);
  hist_tot_err->SetYTitle("");
  
  hist_mean_err->SetLineColor(2);
  hist_sys_err->SetLineColor(4);
  
  hist_mean_err->SetFillColor(1);
  hist_sys_err->SetFillColor(2);
  hist_tot_err->SetFillColor(4);
  
  hist_mean_err->SetLineWidth(3);
  hist_sys_err->SetLineWidth(3);
  hist_tot_err->SetLineWidth(3);
  
  
  hist_tot_err->Draw("e1");
  hist_sys_err->Draw("e1,same");  
  hist_mean_err->Draw("e1,same");
  TF1 *f3=new TF1("f3","0",0,100);
  f3->SetLineColor(15);
  f3->SetLineStyle(2);
  f3->Draw("same");
  
  label->DrawLatex(0.49,0.948,cur_title.c_str());
  
  TLatex *ty1=new TLatex;
  ty1->SetNDC();
  ty1->SetTextSize(0.07);
  ty1->SetTextFont(42);
  ty1->SetTextAngle(90);
  ty1->SetTextAlign(22);
  ty1->DrawLatex(0.031,0.5,Form("< #Delta %s_{%s}>",band.c_str(),
                                mag.c_str()));
  
  TLegend *leg=new TLegend(0.219,0.081,0.494,0.37);
  leg->SetTextFont(42);
  leg->SetTextSize(0.08);
  leg->SetBorderSize(0);
  leg->AddEntry(hist_mean_err,"#sigma_{mean}","l");
  leg->AddEntry(hist_sys_err,"#sigma_{sys}","l");
  leg->AddEntry(hist_tot_err,"#sigma_{tot}","l");
  TLegendEntry *leg_entry=leg->AddEntry(hist_frac,
                                        "Fraction < 3#sigma_{tot}","l");
  leg_entry->SetTextSize(0.06);
  leg->Draw();
  
  
  // draw fraction of values outside 3sigma
  pad1->Update();
  
  Float_t rightmax = 1;
  Float_t scale = pad1->GetUymax()/rightmax;
  hist_frac->SetLineColor(kMagenta);
  hist_frac->SetLineStyle(1);
  hist_frac->SetLineWidth(2);
  hist_frac->Scale(scale);
  hist_frac->Draw("same][");
  
  //draw an axis on the right side
  TGaxis *axis = new TGaxis(pad1->GetUxmax(),pad1->GetUymin(),
                            pad1->GetUxmax(), pad1->GetUymax(),
                            0.0001,rightmax,508,"+L");
  axis->SetLineColor(kMagenta);
  axis->SetLabelSize(0.06);
  axis->SetLabelOffset(0.012);
  axis->SetLabelFont(42);
  axis->SetLabelColor(kMagenta);
  axis->Draw();
  // 	global_file->cd();
  // 	hist_tot_err->Write();
  // 	hist_frac->Write();
  // 	global_file->Close();
  
  pad2->cd();
  TGraph *full_scatter=new TGraph;
  for(int i=0;i<full_diff.size();i++) {
    full_scatter->SetPoint(i,full_mean[i],full_diff[i]);
  }
  
    vector<double> dum(full_diff.size());
    int good,bad;
    double mean,rms;
    sigma_clip(3,2.5,full_diff,dum,mean,
               good,bad,rms,true,false,1e-6);
  TH2D *h3=new TH2D("h3","",100,bins[0],
                    bins[bins.size()-1],100,-1,1);
  
  h3->GetYaxis()->SetRangeUser(mean-10*rms,mean+10*rms);
  
  //h3->SetNdivisions(506,"y");
  h3->Draw();
  h3->SetLabelSize(0.11,"xy");
  TF1 *f2=new TF1("f2","0",0,100);
  f2->SetLineColor(1);
  f2->Draw("same");
  full_scatter->SetMarkerColor(2);
  full_scatter->SetMarkerStyle(8);
  full_scatter->SetMarkerSize(0.3);
  full_scatter->Draw("p");
  sigma_scatter->SetMarkerColor(4);
  sigma_scatter->SetMarkerStyle(8);
  sigma_scatter->SetMarkerSize(0.3);
  sigma_scatter->Draw("p");
  
  
  TLatex *tx=new TLatex;
  tx->SetNDC();
  tx->SetTextSize(0.13);
  tx->SetTextFont(42);
  tx->SetTextAlign(22);
  tx->DrawLatex(0.55,0.079,Form("<%s_{%s}>",band.c_str(),
                                mag.c_str()));
  
  TLatex *ty2=new TLatex;
  ty2->SetNDC();
  ty2->SetTextSize(0.10);
  ty2->SetTextFont(42);
  ty2->SetTextAngle(90);
  ty2->SetTextAlign(22);
  ty2->DrawLatex(0.029,0.65,Form("#Delta %s_{%s}",band.c_str(),
                                 mag.c_str()));

  
  
  TObject *dummy=0;
  TLegend *leg2=new TLegend(0.18,0.70,0.37,0.95);
  leg2->SetTextFont(42);
  leg2->SetTextSize(0.07);
  leg2->SetBorderSize(0);
  
  TLegendEntry *e1=leg2->AddEntry(dummy,"Total Sample","p");
  e1->SetMarkerSize(0.9);
  e1->SetMarkerStyle(20);
  e1->SetMarkerColor(2);
  
  TLegendEntry *e2=leg2->AddEntry(dummy,"Within 3#sigma","p");
  e2->SetMarkerSize(0.9);
  e2->SetMarkerStyle(20);
  e2->SetMarkerColor(4);
  leg2->Draw();
  
  
  c2->Write("can");
  
  id=dir+"/"+title+"_"+band+"_phot";
  print=id+".png";
  if(debug)c2->WaitPrimitive();
  c2->Print(print.c_str());
  
  
  //string convert="convert "+id+".eps "+id+".png";
  //gSystem->Exec(convert.c_str());
  
  //convert="rm "+print;
  //gSystem->Exec(convert.c_str());
  
//   pad2->cd();
//   //TH2D *h2=new TH2D("h2","",100,bins[0],
//   //bins[bins.size()-1],100,min-0.01,max+0.01);
//   TH2D *h2=new TH2D("h2","",100,bins[0],
//                     bins[bins.size()-1],100,-8,1);
  
  
  
//   //h2->SetNdivisions(506,"y");
//   h2->Draw();
//   h2->SetLabelSize(0.11,"xy");
//   TF1 *f4=new TF1("f4","0",0,100);
//   f4->SetLineColor(1);
//   f4->Draw("same");
//   full_scatter->SetMarkerColor(2);
//   full_scatter->SetMarkerStyle(0);
//   full_scatter->SetMarkerSize(0.3);
//   full_scatter->Draw("p");
//   sigma_scatter->SetMarkerColor(4);
//   sigma_scatter->SetMarkerStyle(0);
//   sigma_scatter->SetMarkerSize(0.3);
//   sigma_scatter->Draw("p");
  
  
//   tx->DrawLatex(0.55,0.079,Form("<%s_{%s}>",band.c_str(),
//                                 mag.c_str()));
//   ty2->DrawLatex(0.029,0.65,Form("#Delta %s_{%s}",band.c_str(),
//                                  mag.c_str()));
  
  
//   TLegend *leg3=new TLegend(0.18,0.36,0.375,0.62);
//   leg3->SetTextFont(42);
//   leg3->SetTextSize(0.07);
//   leg3->SetBorderSize(0);
  
  
//   e1=leg3->AddEntry(dummy,"Total Sample","p");
//   e1->SetMarkerSize(0.9);
//   e1->SetMarkerStyle(20);
//   e1->SetMarkerColor(2);
  
//   e2=leg3->AddEntry(dummy,"Within 3#sigma","p");
//   e2->SetMarkerSize(0.9);
//   e2->SetMarkerStyle(20);
//   e2->SetMarkerColor(4);
//   leg3->Draw();
  
//   print=id+"_zoomout.png";
//   c2->Print(print.c_str());
  
  
  // convert="convert "+id+"_zoomout.eps "+id+"_zoomout.png";
  //gSystem->Exec(convert.c_str());
  
  //convert="rm "+print;
  //gSystem->Exec(convert.c_str());
  
  delete f2; delete f3; delete hist_sys_err; delete hist_mean_err; delete hist_tot_err; 
  delete h3; delete hist_frac; delete c2;
  
  delete c1;
  
}






// ////////////////////////////////////////////////////////////////
// //   Functions to plot astrometry
// //
// ////////////////////////////////////////////////////////////////

double BifurGaus(double *x, double *par)
{
  double constant=par[0];
  double mean=par[1];
  double sigmaL=par[2];
  double sigmaR=par[3];
  

  
  double arg = x[0] - mean;
  double coef(0.0);

  if (arg < 0.0){
    if (TMath::Abs(sigmaL) > 1e-30) {
      coef = -0.5/(sigmaL*sigmaL);
    }
  } else {
    if (TMath::Abs(sigmaR) > 1e-30) {
      coef = -0.5/(sigmaR*sigmaR);
    }
  }

  return constant*exp(coef*arg*arg);
}



int plot_astro(const vector<ObjPair> &obj_pairs,
               double &ra_mean,double &ra_clip_mean,
               double &ra_rms,double &ra_clip_rms,
               double &dec_mean,double &dec_clip_mean,
               double &dec_rms,double &dec_clip_rms,
               string title,string dir,string band,
               bool debug,bool draw)
{

  // Some formatting options
  gStyle->SetOptStat(1110);
  gStyle->SetOptFit(0010);
  gStyle->SetStatFormat("4.2g"); 
  gStyle->SetFitFormat("4.2g"); 
  //gStyle->SetTitleOffset(1.6,"y");


  double RA,RA_true,Dec,Dec_true,x_pos,y_pos,RA_err,Dec_err;      
  char class_type;
  
  vector<double> ra1,ra2,dec1,dec2,diff_ra,diff_dec,
    ra1_err,dec1_err,ra2_err,dec2_err;  
  double conv=2.777e-4;
  
  TCanvas *c1=new TCanvas("c1","");
  c1->SetLeftMargin(0.16);  
  
 vector<ObjPair>::const_iterator obj_iter=obj_pairs.begin();
  
 int count=0;
 for(; obj_iter!=obj_pairs.end(); ++obj_iter) {

    ra1.push_back(obj_iter->obs.RA/conv);
    ra2.push_back(obj_iter->truth.RA/conv);
    ra1_err.push_back(obj_iter->obs.RA_err/conv);
    ra2_err.push_back(obj_iter->truth.RA_err/conv);

    dec1.push_back(obj_iter->obs.Dec/conv);
    dec2.push_back(obj_iter->truth.Dec/conv);
    dec1_err.push_back(obj_iter->obs.Dec_err/conv);
    dec2_err.push_back(obj_iter->truth.Dec_err/conv);
    
    double mean_dec=(dec1[count]+dec2[count])/2;

    diff_ra.push_back(cos(mean_dec*TMath::DegToRad())*(ra1[count]-ra2[count]));
    diff_dec.push_back(dec1[count]-dec2[count]);
    count++;
  }
  
  ///////////////////////////////////////////////////////////////
  // Do RA plot
    //////////////////////////////////////////////////////////////
    

 //double ra_clip_mean;//=TMath::Mean(diff_ra.size(),&diff_ra[0]);
 //double ra_clip_rms;//=TMath::RMS(diff_ra.size(),&diff_ra[0]);
  ra_mean=TMath::Mean(diff_ra.size(),&diff_ra[0]);
  ra_rms=TMath::RMS(diff_ra.size(),&diff_ra[0]);

  vector<double> dum(diff_ra.size());
  int good,bad;
  sigma_clip(3,2.5,diff_ra,dum,ra_clip_mean,
             good,bad,ra_clip_rms,true,false,1e-6);
  
  double min_cut,min,max_cut,max;
  double gaus_mean,gaus_sigmaL,gaus_sigmaR,gaus_const;
  double max_limit,min_limit;
  TH1D *RA_diff=0;
  TF1 *func=0;
  int total=diff_ra.size();
  string id,print;
  TLatex *label=0;
  if(draw) {
    // Fill RA histogram
    //  double max_limit,min_limit;
    //  max_limit=0.8;min_limit=-0.8;
    // Fill histogram
    max_limit=ra_clip_mean+12*ra_clip_rms,min_limit=ra_clip_mean-12*ra_clip_rms;
    
    RA_diff=new TH1D("RA_diff",";RA - RA_{true} (arcseconds);Entries",
                           50,min_limit,max_limit);
    

    for(int i=0;i<diff_ra.size();++i) {
      RA_diff->Fill(diff_ra[i]);
    }
    RA_diff->GetXaxis()->CenterTitle();
    RA_diff->GetYaxis()->CenterTitle();
    
    // fit to a bifurcated gaussian
    func = new TF1("bifurgaus",BifurGaus,min_limit,max_limit,4);
    func->SetNpx(100000);
    func->SetParameters(100,0.0,0.1,0.1);
    func->SetParNames("Constant","Mean Fit","#sigma_{Left}","#sigma_{Right}");
    func->SetParLimits(2,0,10);
    func->SetParLimits(3,0,10);
    RA_diff->Fit(func,"RQ");
    RA_diff->Draw();
    c1->Update();
    
    gaus_const=func->GetParameter(0);
    gaus_mean=func->GetParameter(1);
    gaus_sigmaL=func->GetParameter(2);
    gaus_sigmaR=func->GetParameter(3);
    
    
     min_cut=3*gaus_sigmaL;
     max_cut=3*gaus_sigmaR;
     min=gaus_mean-min_cut;
     max=gaus_mean+max_cut;
  }
  else {
     min_cut=3*ra_clip_rms;
     max_cut=3*ra_clip_rms;
     min=ra_clip_mean-min_cut;
     max=ra_clip_mean+max_cut;
  }
    
  // 3sigma cut
  int inside=0;
  for(int i=0;i<diff_ra.size();++i) {
    if(diff_ra[i]<max && diff_ra[i]>min) inside++;
  }
    
  // calculate the outlier fraciton
  double outside=1-(1.*inside/total);
    




  // don't have error info skip for now 
    // Find systematic error
//     vector<double> fit_diff_ra,fit_ra1_err,fit_ra2_err;
    
//     for(int i=0;i<diff_ra.size();i++) {
//       if(diff_ra[i]>min && diff_ra[i]<max) {
// 	fit_diff_ra.push_back(diff_ra[i]);
// 	fit_ra1_err.push_back(ra1_err[i]);
// 	fit_ra2_err.push_back(ra2_err[i]);
//       }
//     }

//     SysErrFit RA_fit;
//     double RA_min_err;
//     double RA_min=RA_fit.Fit(&fit_diff_ra,&fit_ra1_err,RA_min_err,&fit_ra2_err);
//     if(debug)cout<<"RA Minimization Minimum Found at = "<<RA_min<<endl;
//     if(RA_min=0.5) RA_min=0;
    



  if(draw) {  
    RA_diff->Draw();
    c1->Update();
    
    // Add text to statbox
    TPaveStats *ps_RA = (TPaveStats*)c1->GetPrimitive("stats");
    ps_RA->SetName("mystats_RA");
    ps_RA->SetX1NDC(0.673) ; 
    ps_RA->SetX2NDC(0.975) ; 
    ps_RA->SetY1NDC(0.48) ; 
    ps_RA->SetY2NDC(0.87);
    TList *list_RA = ps_RA->GetListOfLines();
    TText *tconst_RA = ps_RA->GetLineWith("Constant"); 

    if(tconst_RA) {
      // TLatex *myt_RAsys = new 
//         TLatex(0,0,Form("#sigma_{sys}= %2.2g",RA_min));
//       myt_RAsys->SetTextFont(tconst_RA->GetTextFont());
//       myt_RAsys->SetTextSize(tconst_RA->GetTextSize());
//       myt_RAsys->SetTextColor(2);
//       list_RA->Add(myt_RAsys);
      
      TLatex *myt_RA = new
        TLatex(0,0,Form("Frac > 3#sigma = %0.2f",outside));
      myt_RA->SetTextFont(tconst_RA->GetTextFont());
      myt_RA->SetTextSize(tconst_RA->GetTextSize());
      myt_RA->SetTextColor(2);
      list_RA->Add(myt_RA);

      
      list_RA->Remove(tconst_RA); 
    }

      ps_RA->SetTextSize(0.038);
      ps_RA->SetBorderSize(0);
      ps_RA->SetTextColor(2);
      RA_diff->SetStats(0);
      ps_RA->Draw();
  }
   
  cout<<"STATUS1BEG "<<title<<" RA Mean: "
      <<ra_mean<<" STATUS1END"<<endl;
  cout<<"STATUS1BEG "<<title<<" RA RMS: "
      <<ra_rms<<" STATUS1END"<<endl;
  cout<<"STATUS1BEG "<<title<<" RA Clipped Mean: "<<ra_clip_mean<<" STATUS1END"<<endl;
  cout<<"STATUS1BEG "<<title<<" RA Clipped RMS: "<<ra_clip_rms<<" STATUS1END"<<endl;
  if(draw) {
    cout<<"STATUS1BEG "<<title<<" RA Fit Mean: "<<gaus_mean<<" STATUS1END"<<endl;
    cout<<"STATUS1BEG "<<title<<" RA Fit Sigma Left: "<<gaus_sigmaL<<" STATUS1END"<<endl;
    cout<<"STATUS1BEG "<<title<<" RA Fit Sigma Right: "<<gaus_sigmaR<<" STATUS1END"<<endl;
    //cout"STATUS1BEG "<<title<<<<"Sigma Sys: "<<RA_min<<" STATUS1END"<<endl;
    cout<<"STATUS1BEG "<<title<<" RA Outside 3 sigma: "<<outside<<" STATUS1END"<<endl;
  }
  else {
    cout<<"STATUS1BEG "<<title<<" RA Outside 3 rms: "<<outside<<" STATUS1END"<<endl;
  }
    
    
  if(draw) {
  // Draw top label
    label=new TLatex;
    label->SetNDC();
    label->SetTextSize(0.1);
    label->SetTextColor(kBlue);
    label->SetTextFont(22);
    label->SetTextAlign(22);
    
    string RA_label;
    RA_label=title+" RA";
    label->DrawLatex(0.49,0.948,RA_label.c_str());
    
    if(debug) {
      c1->Update();
      c1->WaitPrimitive();
    }
    // print out plot
    id=title+"_"+band+"_RA";
    print=dir+"/"+id+".png";
    c1->Print(print.c_str());
  }
  
  c1->cd();
    
  ///////////////////////////////////////////////////////////////
  // Do Dec plot
  //////////////////////////////////////////////////////////////
  dec_mean=TMath::Mean(diff_dec.size(),&diff_dec[0]);
  dec_rms=TMath::RMS(diff_dec.size(),&diff_dec[0]);
  //double dec_clip_mean;//=TMath::Mean(diff_dec.size(),&diff_dec[0]);
  //double dec_clip_rms;//=TMath::RMS(diff_dec.size(),&diff_dec[0]);
  
  
  
  sigma_clip(3,2.5,diff_dec,dum,dec_clip_mean,
             good,bad,dec_clip_rms,true,false,1e-6);

  TH1D *Dec_diff=0;

  if(draw) {
    // Fill histogram
    max_limit=dec_clip_mean+9*dec_clip_rms,min_limit=dec_clip_mean-9*dec_clip_rms;    
    
    
  // fill
    Dec_diff=new TH1D("Dec_diff",
                      ";Dec - Dec_{true} (arcseconds);Entries",
                      50,min_limit,max_limit);
    
    Dec_diff->GetXaxis()->CenterTitle();
    Dec_diff->GetYaxis()->CenterTitle();
    for(int i=0;i<diff_dec.size();++i) {
      Dec_diff->Fill(diff_dec[i]);
    }
    
    Dec_diff->Fit(func,"QR");
    Dec_diff->Draw();
    c1->Update();
    
    
    gaus_const=func->GetParameter(0);
    gaus_mean=func->GetParameter(1);
    gaus_sigmaL=func->GetParameter(2);
    gaus_sigmaR=func->GetParameter(3);

      
    min_cut=3*gaus_sigmaL;
    max_cut=3*gaus_sigmaR;
    min=gaus_mean-min_cut;
    max=gaus_mean+max_cut;
  }
  else {
     min_cut=3*dec_clip_rms;
     max_cut=3*dec_clip_rms;
     min=ra_clip_mean-min_cut;
     max=ra_clip_mean+max_cut;
  }
    
  inside=0;
  for(int i=0;i<diff_dec.size();++i) {
    if(diff_dec[i]<max && diff_dec[i]>min) inside++;
  }
  
  outside=1-(1.*inside/total);
      


      
    
  // Find systematic error
  // vector<double> fit_diff_dec,fit_dec1_err,fit_dec2_err;
  
//   for(int i=0;i<diff_dec.size();i++) {
//       if(diff_dec[i]>min && diff_dec[i]<max) {
// 	fit_diff_dec.push_back(diff_dec[i]);
// 	fit_dec1_err.push_back(dec1_err[i]);
// 	fit_dec2_err.push_back(dec2_err[i]);
//       }
//     }

//     SysErrFit Dec_fit;
//     double Dec_min_err;
//     double Dec_min=Dec_fit.Fit(&fit_diff_dec,&fit_dec1_err,Dec_min_err,&fit_dec2_err);
//     if(debug)cout<<"Dec Minimization Minimum Found at = "<<Dec_min<<endl;
//     if(Dec_min=0.5) Dec_min=0;
  
  if(draw) {
    // Add text to statbox
    TPaveStats *ps_Dec = (TPaveStats*)c1->GetPrimitive("stats");
    ps_Dec->SetName("mystats_Dec");
    ps_Dec->SetX1NDC(0.673) ; 
    ps_Dec->SetX2NDC(0.975) ; 
    ps_Dec->SetY1NDC(0.48) ; 
    ps_Dec->SetY2NDC(0.87);
    TList *list_Dec = ps_Dec->GetListOfLines();
    TText *tconst_Dec = ps_Dec->GetLineWith("Constant"); 

    if(tconst_Dec) {
//       TLatex *myt_Decsys = new 
//         TLatex(0,0,Form("#sigma_{sys}= %2.2g",Dec_min));
//       myt_Decsys->SetTextFont(tconst_Dec->GetTextFont());
//       myt_Decsys->SetTextSize(tconst_Dec->GetTextSize());
//       myt_Decsys->SetTextColor(2);
//       list_Dec->Add(myt_Decsys);
      
      
      
    TLatex *myt_Dec = 
      new TLatex(0,0,Form("Frac > 3#sigma = %0.2f",outside));
    myt_Dec->SetTextFont(tconst_Dec->GetTextFont());
    myt_Dec->SetTextSize(tconst_Dec->GetTextSize());
    myt_Dec->SetTextColor(2);
    list_Dec->Remove(tconst_Dec); 
    list_Dec->Add(myt_Dec);
    }

      
    ps_Dec->SetTextSize(0.038);
    ps_Dec->SetBorderSize(0);
    ps_Dec->SetTextColor(2);
    Dec_diff->SetStats(0);
    ps_Dec->Draw();
  }    
      
    cout<<"STATUS1BEG "<<title<<" Dec Mean: "
        <<dec_mean<<" STATUS1END"<<endl;
    cout<<"STATUS1BEG "<<title<<" Dec RMS: "<<dec_rms<<" STATUS1END"<<endl;
    cout<<"STATUS1BEG "<<title<<" Dec Clipped Mean: "
        <<dec_clip_mean<<" STATUS1END"<<endl;
    cout<<"STATUS1BEG "<<title<<" Dec Clipped RMS: "
        <<dec_clip_rms<<" STATUS1END"<<endl;
    if(draw) {
      cout<<"STATUS1BEG "<<title<<" Dec Fit Mean: "
          <<gaus_mean<<" STATUS1END"<<endl;
      cout<<"STATUS1BEG "<<title<<" Dec Fit Sigma Left: "
          <<gaus_sigmaL<<" STATUS1END"<<endl;
      cout<<"STATUS1BEG "<<title<<" Dec Fit Sigma Right: "
          <<gaus_sigmaR<<" STATUS1END"<<endl;
      //cout<<"Sigma Sys: "<<Dec_min<<" STATUS1END"<<endl;
      cout<<"STATUS1BEG "<<title<<" Dec Outside 3 sigma: "
          <<outside<<" STATUS1END"<<endl;
    } 
    else {
      cout<<"STATUS1BEG "<<title<<" Dec Outside 3 rms: "
          <<outside<<" STATUS1END"<<endl;
    }

    if(draw) {
    string dec_label;

    dec_label=title+" Dec";

    label->DrawLatex(0.49,0.948,dec_label.c_str());
    c1->Update();

    if(debug) {
      c1->WaitPrimitive();
    }

      
    id=title+"_"+band+"_Dec";;
    print=dir+"/"+id+".png";
    c1->Print(print.c_str());
      
    //convert="convert "+id+".eps "+id+".png";
    //gSystem->Exec(convert.c_str());
      
    //convert="rm "+print;
    //gSystem->Exec(convert.c_str());

    //Dec_diff->DrawClone();
    }
      
    if(RA_diff) delete RA_diff;
    if(Dec_diff) delete Dec_diff;
    if(func) delete func;      
    delete c1;



}
  
