/* Extract sky noise distribution and compare with the expectation in the */
/* image weight map */
/*
*/

#include "imageproc.h"

#define S2N_THRESH 75

main(argc,argv)
	int argc;
	char *argv[];
{
	int	i,j,im,imnum,x,y,loc,xmin,xmax,ymin,ymax,num,delta,
		xdim,ydim,ccdnum=0,ncompare,mkpath(),
		nummasked,flag_variance=NO;
	static int status=0;
	char	comment[1000],imagename[1000],command[1000],
		longcomment[10000],scaleregion[100],filter[200]="",
		obstype[200],maskname[1000],temp[1000],event[10000],
		*striparchiveroot(),
		imtypename[6][10]={"","IMAGE","VARIANCE","MASK","SIGMA","WEIGHT"};
	float	avsigclip_sigma,srcgrowrad=0.0,avsig,sigma,
		*vecsort,scalefactor,*scalesort,maxval,*meanimage,
		val,sigmalim,mean,mode,fwhm,threshold,*imagepointer,
	 	rms,offset,maxdev,dist,
		aperture_radius=10.0,
		ran1();
	long	seed=-30;
	static desimage data;
	int	flag_verbose=1,flag_combine=NO,flag_filter=NO,
		flag_bpm=YES,hdutype,flag_srcgrowrad=NO,
		flag_rejectobjects=YES,flag_outputmasks=NO,
		flag_scale=YES,scaleregionn[4]={500,1500,1500,2500},scalenum,
		avsignum,srcgrowrad_int,valnum,dy,dx,newx,newy,
		flag_image_compare=NO,
		num_samples=1000,xc,yc;
	void	rd_desimage(),shell(),decodesection(),retrievescale(),
		headercheck(),printerror(),reportevt(),image_compare();
	double	avsigsum,val1sum,val2sum,weightsum,variancesum,
		variance_threshold=Squ(S2N_THRESH),
		immean,imvar,wtvar,numpix,
		mnnum=0.0,mnratio=0.0,mnratio2=0.0;
	short	interppix=4,maskpix=8,growpix=16,rejectpix=32;
	time_t	tm;
	fitsfile *fptr;
	FILE	*inp,*pip;

	if (argc<2) {
	  printf("get_imstats <input image> <options>\n");
	  printf("  Preprocessing Options\n");
	  printf("    -aperture_radius <#, default=10.0>\n");
	  printf("    -num_samples <#, default=1000>\n");
	  printf("    -random_seed <#, default=-30>\n");
	  printf("  Filter Options\n");
	  printf("    -avsigclip <Sigma, default=3>\n");
	  printf("  Testing Options\n");
	  printf("    -verbose <0-3>\n");
	  exit(0);
	}

	/* ******************************************* */
	/* ********  PROCESS COMMAND LINE ************ */
	/* ******************************************* */

	/* first cycle through looking for quiet flag */
	for (i=2;i<argc;i++) 
          if (!strcmp(argv[i],"-verbose")) {
            sscanf(argv[++i],"%d",&flag_verbose);
            if (flag_verbose<0 || flag_verbose>3) {
              sprintf(event,"Verbose level out of range %d . Reset to 2",
                flag_verbose);
              flag_verbose=2;
              reportevt(2,STATUS,3,event);
            }
          }

	/* check whether a FITS image */
	sprintf(imagename,"%s",argv[1]);
        if (strncmp(&(imagename[strlen(imagename)-5]),".fits",5) &&
          strncmp(&(imagename[strlen(imagename)-8]),".fits.fz",8) &&
          strncmp(&(imagename[strlen(imagename)-8]),".fits.gz",8)) {
          sprintf(event,
	    "Input image must be FITS or compressed FITS image");
          reportevt(flag_verbose,STATUS,5,event);
          exit(0);
        }

	/* open input image */
	inp=fopen(imagename,"r");
	if (inp==NULL) {
          sprintf(event,"Image not found: %s",imagename);
          reportevt(flag_verbose,STATUS,5,event);
	  exit(0);
	}
        if (fclose(inp)) {
          sprintf(event,"Input image didn't close: %s",imagename);
          reportevt(flag_verbose,STATUS,5,event);
          exit(0);
        }

	/* ******************************************* */
	/* ********  PROCESS COMMAND LINE ************ */
	/* ******************************************* */
	/* set defaults */
	flag_scale=YES;
	flag_filter=NO;
	flag_combine=MEDIAN;
	/* process command line options */
	for (i=2;i<argc;i++) {
	  if (!strcmp(argv[i],"-noscale")) flag_scale=NO;
	  if (!strcmp(argv[i],"-norejectobjects")) flag_rejectobjects=NO;
	  if (!strcmp(argv[i],"-scaleregion")) {
	    sprintf(scaleregion,"%s",argv[i+1]);
	    decodesection(scaleregion,scaleregionn,flag_verbose);
	  }
	  if (!strcmp(argv[i],"-avsigclip")) {
	    flag_filter=AVSIGCLIP;
	    sscanf(argv[i+1],"%f",&avsigclip_sigma);
	  }
	  if (!strcmp(argv[i],"-srcgrowrad")) {
	    flag_srcgrowrad=YES;
	    sscanf(argv[i+1],"%f",&srcgrowrad);
	  }
	  if (!strcmp(argv[i],"-aperture_radius")) {
	    sscanf(argv[i+1],"%f",&aperture_radius);
	  }
	  if (!strcmp(argv[i],"-num_samples")) {
	    sscanf(argv[i+1],"%d",&num_samples);
	  }
	  if (!strcmp(argv[i],"-random_seed")) {
	    sscanf(argv[i+1],"%ld",&seed);
	    if (seed>0) seed*=-1;
	  }
	}	
	
	/* ***************************************************************** */
	/* ***************************************************************** */
	/* ********************  READ Input Image ************************** */
	/* ***************************************************************** */
	/* ***************************************************************** */

	  /* read input image */
	  sprintf(data.name,"%s",imagename);
	  rd_desimage(&data,READONLY,flag_verbose);
	  xdim=data.axes[0];ydim=data.axes[1];
	  /* create a mask image if there isn't one already */
	  if (data.mask==NULL) {
	    data.mask=(short *)calloc(data.npixels,sizeof(short));
	    if (data.mask==NULL) {
	      sprintf(event,"Creation of mask pointer failed: %s",data.name);
	      reportevt(flag_verbose,STATUS,5,event);
	      printerror(status);
	    }
	    for (i=0;i<data.npixels;i++) if (fabs(data.varim[i])<1.0e-10) data.mask[i]=1;
	    else data.mask[i]=0;
	  }

	  /* close image now */
	  if (fits_close_file(data.fptr,&status)) {
	    sprintf(event,"Image close failed: %s",data.name);
	    reportevt(flag_verbose,STATUS,5,event);
	    printerror(status);
	  }

	  /* calculate the scalefactor, mode and fwhm in the scaleregion */
          /* vector for determining the scale of an image */
          if (flag_scale) { /* prepare for medianing the scale */
            scalenum=(scaleregionn[1]-scaleregionn[0]+1)*(scaleregionn[3]-
              scaleregionn[2]+1);
            scalesort=(float *)calloc(scalenum,sizeof(float));
            if (scalesort==NULL) {
              sprintf(event,"Calloc for scalesort failed");
              reportevt(flag_verbose,STATUS,5,event);
              exit(0);
            }
          }

	  if (flag_scale || flag_rejectobjects) 
	    retrievescale(&data,scaleregionn,scalesort,flag_verbose,
	      &scalefactor,&mode,&fwhm);

	  /* check scalefactor */
	  if (flag_scale) {
	    /* mask entire image if scalefactor unknown */
	    if (fabs(scalefactor)<1.0e-4) {
	      sprintf(event,"Masking entire image: %s",data.name);
	      reportevt(flag_verbose,STATUS,3,event);
	      for (i=0;i<data.npixels;i++) data.mask[i]|=maskpix;
	      scalefactor=1.0;
	    }
	  }
	  else scalefactor=1.0;

	  /* cycle through masking objects */
	  if (flag_rejectobjects) {
	    nummasked=0;
	    /* set threshold at ~12 sigma above the mode in the sky */
	    threshold=mode+5.0*fwhm;
	    for (i=0;i<data.npixels;i++) 
	      if (data.image[i]>threshold) {
	        data.mask[i]|=rejectpix;
		nummasked++;
	      }
	    sprintf(event,"Masked pixels above %.1f - %.2f percent: %s",
	      threshold,(float)(100.0*nummasked/(data.npixels)),
	      data.name);
	    reportevt(flag_verbose,QA,1,event);
	  }


	/* ********************************************** */
	/* *********  GROW RADIUS SECTION *************** */
	/* ********************************************** */

	if (flag_srcgrowrad==YES) {
	  if (flag_verbose==3) 
	    reportevt(flag_verbose,STATUS,1,
	      "Beginning source grow radius filtering"); 
	  srcgrowrad_int=srcgrowrad;
	  srcgrowrad=Squ(srcgrowrad);
	  for (im=0;im<imnum;im++) {
	    for (y=0;y<ydim;y++) 
	      for (x=0;x<xdim;x++) {
	      loc=x+y*xdim;
	      /* if bad pixel but not interpolated */
	      if (data.mask[loc] && !(data.mask[loc]&interppix) && 
		!(data.mask[loc]&growpix)) 
		for (dy=-srcgrowrad_int;dy<=srcgrowrad_int;dy++) {
		  newy=y+dy;
		  if (newy>=0 && newy<ydim) {
		    xmax=(int)sqrt(srcgrowrad-Squ(dy));
		    for (dx=-xmax;dx<=xmax;dx++) {
		      newx=x+dx;
		      if (newx>=0 && newx<xdim) {
		        data.mask[newx+newy*xdim]|=growpix;
		      }
	  	    } /* cycle through x */
		  } /* y within range? */
		} /* cycle through y */
	    } /* cycle through pixels of image */
	    /* report on how many pixels are masked */
	    nummasked=0;
	    for (i=0;i<data.npixels;i++) if (data.mask[i]) nummasked++;
	    sprintf(event,"After grow radius %.2f percent of pixels masked: %s",
	      (float)(100.0*nummasked/(data.npixels)),data.name);
	    reportevt(flag_verbose,QA,1,event);
	  } /* cycle through list of images */
	}


	/* ***************************************************************** */
	/* ***************** Examine sky statistics in Image *************** */
	/* ***************************************************************** */

	for (i=1;i<=num_samples;i++) {
	  /* reset statistics sums */
	  immean=imvar=wtvar=numpix=0.0;
	  /* choose a random position for this sample */
	  xc=xdim*ran1(&seed);if (x==xdim) x--;
	  yc=ydim*ran1(&seed);if (y==ydim) y--;
	  xmin=xc-aperture_radius;if (xmin<0) xmin=0;
	  xmax=xc+(aperture_radius+0.5);if (xmax>=xdim) xmax=xdim-1;
	  ymin=yc-aperture_radius;if (ymin<0) ymin=0;
	  ymax=yc+(aperture_radius+0.5);if (ymax>=ydim) ymax=ydim-1;
	  for (y=ymin;y<ymax;y++) for (x=xmin;x<xmax;x++) {
	    dist=sqrt(Squ(y-yc)+Squ(x-xc));
	    if (dist<aperture_radius) { /* pixel should be considered */
	      loc=x+y*xdim;
	      if (!data.mask[loc] && data.varim[loc]>0.0) {
		immean+=(data.image[loc]-scalefactor);
		imvar+=Squ(data.image[loc]-scalefactor);
		wtvar+=1.0/data.varim[loc];
		numpix+=1.0;
	      }
	    }
	  }
	  if (numpix>20.0) {
	    immean/=numpix;
	    imvar=sqrt(imvar/numpix-Squ(immean));
	    wtvar=sqrt(wtvar/numpix);
	    if (flag_verbose==3) 
	     printf(" %5d: ( %4d %4d ) Im: %10.3e ( %9.3e) Wt: %9.3e Ratio: %9.3e \n",
	      i,x,y,immean,imvar,wtvar,imvar/wtvar);
	  }
	  else immean=imvar=wtvar=0.0;
	  /* maintain uberstatistics */
	  if (numpix>20.0 && fabs(immean/imvar*sqrt(numpix))<10.0) {
	    mnratio+=(imvar/wtvar);
	    mnratio2+=Squ(imvar/wtvar);
	    mnnum+=1.0;
	  }
	}

	printf("  %d samples <Sky to Weight Map Variance Ratio> %9.3e ( %9.3e )\n",
	  (int)mnnum,mnratio/mnnum,sqrt(mnratio2/mnnum-
	  Squ(mnratio/mnnum))/sqrt(mnnum));

	return(0);

}

#undef S2N_THRESH

