/*$Id: photcheck_full.c 5081 2010-02-13 01:06:54Z desai $
**
** photcheck_full.c
**
** Last commit:
**     $Rev: 5081 $
**     $LastChangedBy: desai $
**     $LastChangedDate: 2010-02-12 18:06:54 -0700 (Fri, 12 Feb 2010) $
**
*/

#include "imageproc.h"

main(argc,argv)
     int argc;
     char *argv[];
{
  char command[1000],event[1000],line[5000],photcheck_option[1000],binpath[1000];
  char dblogin[1000],sqlcall[1000],sqlcall1[1000],sqlHEAD[1000];
  char nite[20],band[5],run[300],expname[100],project[10],tilename[100],photcheck_call[1000];

  int i;
  int flag_verbose=2,flag_coadd=0,flag_photcheck=0,flag_binpath=0,flag_redtile=0;

  FILE *pip,*fsqlout,*fsqlout1;

  void select_dblogin(),reportevt();

  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <Optional Inputs>\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -run <run> \n");
    printf("       -band <band> \n");
    printf("       -project <project> \n");
    printf("       -redtile <tilename> \n");
    printf("     For Nightly Process: \n");
    printf("       -nite <nite> \n");
    printf("     For Coadd Process: \n");
    printf("       -coadd \n");
 
    printf("    Optional Inputs:\n");
    printf("       -phocheck_option <\"Options\"> (other options for photcheck)\n");
    printf("       -binpath <path to photcheck> \n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }

  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {

    if (!strcmp(argv[i],"-run")) sprintf(run,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-band")) sprintf(band,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-project")) sprintf(project,"%s",argv[i+1]);
    if (!strcmp(argv[i],"-redtile")) {flag_redtile=1;sprintf(tilename,"%s",argv[i+1]);}
    if (!strcmp(argv[i],"-nite")) { sprintf(nite,"%s",argv[i+1]); flag_coadd=0;}
    if (!strcmp(argv[i],"-coadd")) flag_coadd=1;
    
    if (!strcmp(argv[i],"-photcheck_option")) { sprintf(photcheck_option,"%s",argv[i+1]); 
      flag_photcheck=1;} /* need to add the checking if double-quote is included */
    
    if (!strcmp(argv[i],"-binpath")) { sprintf(binpath,"%s",argv[i+1]); 
      flag_binpath=1;}
  }
  
  /* *** Setup DB login and make generic sqlcall *** */
  select_dblogin(dblogin,DB_READONLY);
  sprintf(sqlcall1, "${ORACLE_HOME}/bin/sqlplus -S %s < temp1.sql ",dblogin);
  sprintf(sqlHEAD,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;");
  
  /* ********************************** */
  /* *** Query DB and run photcheck *** */
  /* ********************************** */
  

  if(!flag_coadd) {  /* for nitely process */

    /* *** Query for exposure names *** */
    fsqlout1=fopen("temp1.sql", "w");
    fprintf(fsqlout1,"%s\n",sqlHEAD);
    if (!flag_redtile)
    fprintf(fsqlout1,"select distinct(a.exposurename) from exposure a,image b where a.id=b.exposureid and b.nite='%s' and b.band='%s' and b.run like '%s%%' and b.imagetype='red' ",nite,band,run);
    else
      fprintf(fsqlout1,"select distinct(a.exposurename) from exposure a,image b where a.id=b.exposureid and b.nite='%s' and b.band='%s' and b.run like '%s%%' and b.imagetype='remap' and b.tilename='%s'",nite,band,run,tilename);
    fprintf(fsqlout1," and a.exposurename not like '%%-0-%%';\n"); /* exclude the standards star fields at the moment */
    fprintf(fsqlout1,"exit;\n");
    fclose(fsqlout1);
    
    /* *** Run photcheck for expname *** */
    pip=popen(sqlcall1,"r");
    while(fscanf(pip,"%s",expname)!=EOF) {

      /* include path? */
      if(flag_binpath) 
	sprintf(photcheck_call,"%s/photcheck",binpath);
      else
	sprintf(photcheck_call,"photcheck");
      
      /* the rest */
      sprintf(photcheck_call,"%s -band %s",photcheck_call,band);
      sprintf(photcheck_call,"%s -project %s",photcheck_call,project);
      sprintf(photcheck_call,"%s -run %s",photcheck_call,run);
      if (flag_redtile)sprintf(photcheck_call,"%s -redtile %s",photcheck_call,tilename);
      sprintf(photcheck_call,"%s -expname %s",photcheck_call,expname);
      sprintf(photcheck_call,"%s -nite %s",photcheck_call,nite);
      if(flag_photcheck)
	sprintf(photcheck_call,"%s %s",photcheck_call,photcheck_option);
      sprintf(photcheck_call,"%s -verbose %d",photcheck_call,flag_verbose);
     
      /* output photcheck call and run photcheck */
      sprintf(event," photcheck call: %s ",photcheck_call);
      reportevt(flag_verbose,STATUS,1,event);
  
      system(photcheck_call);
    }
    pclose(pip);
  }
  else { /* for coadd */
    
    /* *** Query for tilename *** */
    fsqlout1=fopen("temp1.sql", "w");
    fprintf(fsqlout1,"%s\n",sqlHEAD);
    fprintf(fsqlout1,"select distinct(b.tilename) from coadd_objects a, coadd b where b.project='%s' and b.id=a.imageid_g and b.run like '%s%%' group by tilename having count(*)>10;\n",project,run);  
    fprintf(fsqlout1,"exit;\n");
    fclose(fsqlout1);
 
    /* *** Run photcheck for tilename *** */
    pip=popen(sqlcall1,"r");
    while(fscanf(pip,"%s",tilename)!=EOF) {

      /* include path? */
      if(flag_binpath) 
	sprintf(photcheck_call,"%s/photcheck",binpath);
      else
	sprintf(photcheck_call,"photcheck");

      /* the rest */
      sprintf(photcheck_call,"%s -band %s",photcheck_call,band);
      sprintf(photcheck_call,"%s -project %s",photcheck_call,project);
      sprintf(photcheck_call,"%s -run %s",photcheck_call,run);
      sprintf(photcheck_call,"%s -tilename %s",photcheck_call,tilename);
      if(flag_photcheck)
	sprintf(photcheck_call,"%s %s",photcheck_call,photcheck_option);
      sprintf(photcheck_call,"%s -verbose %d",photcheck_call,flag_verbose);
     
      /* output photcheck call and run photcheck */
      sprintf(event," photcheck call: %s ",photcheck_call);
      reportevt(flag_verbose,STATUS,1,event);
       system(photcheck_call);
    }
    pclose(pip); 
  }
  
  system("rm temp*.sql");

}
