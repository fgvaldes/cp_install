#include <fstream>
#include <string>
#include <iostream>

#include "TString.h"
#include "TMath.h"
#include "TStopwatch.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TApplication.h"

#include "kdtree++/kdtree.hpp"
#include "Utils.h"
#include <CCfits>



#include "Database/DatabaseInterface.h"
#include "Database/DatabaseResult.h"

using namespace std;
using namespace Db;
using namespace CCfits;

// RAG: Added to give versioning feedback 
static const char *svn_id = "$Id: cal_phot.cc 12837 2013-07-25 17:00:40Z ankitc $";

// Structure to store reference objects
struct USNObject
{
    typedef double value_type;
    double ra;
    double dec;
    double mag;
    double o_mag; // B1
    double e_mag; // R1
    double j_mag; // B2
    double f_mag; // R2
    double i_mag; // I2
    double xyz[3];
    inline double operator[](int const N) const {return xyz[N];}
};

struct ObjPair
{
    USNObject obs;
    USNObject ref;
};

typedef KDTree::KDTree<3,USNObject> Object_Tree;
typedef std::pair<Object_Tree::const_iterator,double> Object_Match;

string UseFilter(const string &filter);

int main(int argc,char *argv[])
{
    TStopwatch timer;

//     char**argv_dummy;
//     char char_dummy='p';
//     char *char_dummy2=&char_dummy;
//     argv_dummy=&char_dummy2;
//     bool debug=false;
//     int int_dummy=1;
//     TApplication app("app",&int_dummy,argv_dummy);
//     LoadStyle();

    string cat_file="";
    int id_file=-1;
    int id_exposure=-1;
    string cat_list="";
    vector<string> cat_vec;
    vector<int> id_vec;
    vector<int> exposure_vec;
    vector<int> unique_exposures_vec;
    string ref_file="";
    string out_file="";
    string band="",use_band;
    bool insert_zp=false;
    bool skip_db_transaction=false;
    bool with_exposure=false;
    double tol=1;

    string cat_ra_string="ALPHA_J2000";
    string cat_dec_string="DELTA_J2000";
    string cat_magerr_string="MAGERR_AUTO";
    string cat_mag_string="MAG_AUTO";
    string cat_flags_string="FLAGS";
    string cat_cs_string="CLASS_STAR";

    string ref_ra_string="X_WORLD";
    string ref_dec_string="Y_WORLD";
    string ref_magerr_string="MAGERR";
    string ref_mag1_string="MAG";
    string ref_mag2_string="B2MAG";
    string ref_mag3_string="I2MAG";
    bool overwrite=false;
    string outdir="";
    double min_err=0.01;
    double max_err=0.3;
    int max_flags=3;

    // RAG: Added to give versioning feedback 
    cout << svn_id << endl;

    for(int ii=1;ii<argc;ii++) 
    {
        string arg=argv[ii];
        if(arg=="-cat")            {cat_file=argv[ii+1];ii++;}
        else if(arg=="-ref")       {ref_file=argv[ii+1];ii++;}
        else if(arg=="-cat_list")  {cat_list=argv[ii+1];ii++;}
        else if(arg=="-cat_file")  {cat_file=argv[ii+1];ii++;}
        else if(arg=="-out")       {out_file=argv[ii+1];ii++;}
        else if(arg=="-ra")        {cat_ra_string=argv[ii+1];ii++;}
        else if(arg=="-dec")       {cat_dec_string=argv[ii+1];ii++;}
        else if(arg=="-mag")       {cat_mag_string=argv[ii+1];ii++;}
        else if(arg=="-magerr")    {cat_magerr_string=argv[ii+1];ii++;}
        else if(arg=="-band")      {band=argv[ii+1];ii++;}
        else if(arg=="-tol")       {tol=atof(argv[ii+1]);ii++;}
        else if(arg=="-overwrite") {overwrite=true;}
        else if(arg=="-insert_zp") {insert_zp=true;}
        else if(arg=="-exposure")  {with_exposure=true;}
        else if(arg=="-skip_db")   {skip_db_transaction=true;}
        else if(arg=="-id_file")   {id_file=atoi(argv[ii+1]);ii++;}
        else if(arg=="-outdir")    {outdir=argv[ii+1];ii++;}
        else {
            cerr<<"Not a valid command line entry: "<<arg<<endl;
            exit(1);
        }
    }

    tol*=1./3600*TMath::DegToRad();

    use_band=UseFilter(band);
    report_evt(true,"STATUS",2,Form("Using band: %s",use_band.c_str()));

    if(use_band.empty()) {
        string evt=Form("Not a valid band: ",band.c_str());
        report_evt(true,"STATUS",5,evt);
        exit(1);
    }

    if(cat_list.empty()) {
        cat_vec.push_back(cat_file);
        if(id_file>0) id_vec.push_back(id_file);
    }
    else {
        ifstream infile(cat_list.c_str());
        if(!insert_zp) {
            while(infile>>cat_file) {
                cat_vec.push_back(cat_file);
            }
        }
        else if (insert_zp && !with_exposure) {
            while(infile>>cat_file>>id_file) {
                //cout<<id_file<<endl;
                cat_vec.push_back(cat_file);
                id_vec.push_back(id_file);
            }
        }
        else if (insert_zp && with_exposure) {
            while(infile>>cat_file>>id_file>>id_exposure) {
                //cout<<id_file<<" "<<id_exposure<<endl;
                cat_vec.push_back(cat_file);
                id_vec.push_back(id_file);
                exposure_vec.push_back(id_exposure);
            }
        }
    }

    if(id_vec.empty() && insert_zp) {
        cout<<"No id given, not inserting into database"<<endl;
        insert_zp=false;
        with_exposure=false;
    }

    if (with_exposure)
    {
        int i, j;

        // since multiple exposures may be present in the list,
        // compile list of all unique exposures
        unique_exposures_vec.push_back(exposure_vec[0]);

        for(i=1;i<cat_vec.size();++i)
        {
            for(j=0;j<unique_exposures_vec.size();++j)
            {
                if (exposure_vec[i] == unique_exposures_vec[j])
                    break;
            }

            if (j == unique_exposures_vec.size()) // found new value
                unique_exposures_vec.push_back(exposure_vec[i]);
        }

        //for(j=0;j<unique_exposures_vec.size();++j)
        //    cout << unique_exposures_vec[j] << endl;

    }

    // read in the reference file

    vector<double> ref_ra_vec,ref_dec_vec,ref_mag1_vec,
        ref_magerr_vec,ref_mag2_vec,ref_mag3_vec;

    //std::auto_ptr<FITS> ref_fits(new FITS(ref_file)); 
    FITS *ref_fits=0;
    try {
        ref_fits=new FITS(ref_file);
    }
    catch (FitsException& ) {
        string evt=Form("Can't open ref file: %s",ref_file.c_str());
        report_evt(true,"STATUS",5,evt);
        exit(1);
    }

    ExtHDU& ref_table = ref_fits->extension(2);
    int N_ref=ref_table.rows();

    ref_table.column(ref_ra_string).read(ref_ra_vec,0,N_ref);
    ref_table.column(ref_dec_string).read(ref_dec_vec,0,N_ref);
    ref_table.column(ref_mag1_string).read(ref_mag1_vec,0,N_ref);
    ref_table.column(ref_mag2_string).read(ref_mag2_vec,0,N_ref);
    ref_table.column(ref_mag3_string).read(ref_mag3_vec,0,N_ref);
    ref_table.column(ref_magerr_string).read(ref_magerr_vec,0,N_ref);

    Object_Tree ref_tree;
    double max_ra=-1e6,min_ra=1e6;
    double max_dec=-1e6,min_dec=1e6;

    // load the reference stars into the kd-tree
    for(int i=0;i<N_ref;i++) 
    {
        USNObject ob;
        ob.ra=ref_ra_vec[i];
        ob.dec=ref_dec_vec[i];

        max_ra=TMath::Max(max_ra,ob.ra);
        min_ra=TMath::Min(min_ra,ob.ra);
        max_dec=TMath::Max(max_dec,ob.dec);
        min_dec=TMath::Min(min_dec,ob.dec);

        // calcualte position on sphere
        double cd=cos(ob.dec*TMath::DegToRad());
        ob.xyz[0]=cos(ob.ra*TMath::DegToRad())*cd;
        ob.xyz[1]=sin(ob.ra*TMath::DegToRad())*cd;
        ob.xyz[2]=sin(ob.dec*TMath::DegToRad());

        // currently use socalled j/f mags add more later if needed
        ob.j_mag=ref_mag1_vec[i];
        ob.f_mag=ref_mag2_vec[i];
        ob.i_mag=ref_mag3_vec[i];

        ref_tree.insert(ob);
    }

    cout<<min_ra<<" "<<max_ra<<" "<<min_dec<<" "<<max_dec<<endl;
    cout<<"Found "<<ref_tree.size()<<" objects in reference file"<<endl;

    int exp_indx=0;
    int current_exposure_id;

    vector<double> mag_diff;
    vector<double> mag_err;
    vector<string> ccat_out_name;

    double clipped_mean,clipped_rms;

    do 
    {
        mag_diff.clear();
        mag_err.clear();
        ccat_out_name.clear();
        int imgs_per_exposure = 0;

        // we are going to process one exposure at a time
        if(with_exposure) 
        {
            current_exposure_id = unique_exposures_vec[exp_indx];
            cout << "=== Processing exposure " << current_exposure_id << endl;
        }

        //TCanvas c1;
        for(int i=0;i<cat_vec.size();++i)
        {
            cat_file=cat_vec[i];
            if(insert_zp) id_file=id_vec[i];
            if(with_exposure)
            {
               // skip the file if it is from a different exposure
               if (exposure_vec[i] != current_exposure_id) continue;
               imgs_per_exposure++;
            }

            if (!with_exposure)
            {
                mag_diff.clear();
                mag_err.clear();
            }

            cout << "cat_file: " << cat_file << endl;

            // Read in the catalog file
            vector<double> cat_ra_vec,cat_dec_vec,cat_mag_vec,
                           cat_magerr_vec,cat_flags_vec,cat_cs_vec;

            // read in file
            FITS cat_fits_in(cat_file, Read);
            //FITS cat_fits("!test.fits",cat_fits_in);

            string out_name;
            //std::auto_ptr<FITS> cat_fits;
            FITS *cat_fits;
            if(overwrite)
            {
                try {
                    cat_fits=new FITS("!"+cat_file,cat_fits_in);
                    cat_fits->copy(cat_fits_in.extension(1));
                    cat_fits->copy(cat_fits_in.extension(2));
                }
                catch (FitsException& ) {
                    string evt=Form("Can't open new file: %s",cat_file.c_str());
                    report_evt(true,"STATUS",5,evt);
                    exit(1);
                }
                if (with_exposure) ccat_out_name.push_back(cat_file);
            }
            else
            {
                TString ts_name(cat_file.c_str());
                out_name=Form("%s", (ts_name.ReplaceAll("_cat.","_ccat.")).Data());

                if(out_name==cat_file) {
                    cout<<"Warning file being overwritten.  No string '_cat' was found"
                        <<" in the filename"<<endl;
                }

                if(!outdir.empty()) {
                    size_t found=out_name.find_last_of("/");
                    string base=out_name.substr(found+1);
                    out_name=outdir+"/"+base;
                }

                if (with_exposure) ccat_out_name.push_back(out_name);

                out_name="!"+out_name;
                try {
                    cat_fits=new FITS(out_name,cat_fits_in);
                }
                catch (FitsException&) {
                    string evt=Form("Can't open new file: %s",out_name.c_str());
                    report_evt(true,"STATUS",5,evt);
                    exit(1);
                }

                try {
                    cat_fits->copy(cat_fits_in.extension(1));
                    cat_fits->copy(cat_fits_in.extension(2));
                }
                catch (FitsException&) {
                    string evt="Can't copy data from old file ";
                    report_evt(true,"STATUS",5,evt);
                    exit(2);
                }
            }

            ExtHDU& cat_table = cat_fits->extension(2);
            int N_cat=cat_table.rows();

            try {
                cat_table.column(cat_ra_string).read(cat_ra_vec,0,N_cat);
                cat_table.column(cat_dec_string).read(cat_dec_vec,0,N_cat);
                cat_table.column(cat_mag_string).read(cat_mag_vec,0,N_cat);
                cat_table.column(cat_magerr_string).read(cat_magerr_vec,0,N_cat);
                cat_table.column(cat_flags_string).read(cat_flags_vec,0,N_cat);
                cat_table.column(cat_cs_string).read(cat_cs_vec,0,N_cat);
            }
            catch (FitsException&) {
                string evt="Can't read columns from file";
                report_evt(true,"STATUS",5,evt);
                exit(1);
            }

            vector<ObjPair> pair_vec;
            // Loop over catalog and match to reference
            //vector<double> mag_err;

            for(int j=0;j<N_cat;++j) 
            {
                if(cat_magerr_vec[j]>max_err) continue;

                USNObject ob;
                ob.ra=cat_ra_vec[j];
                ob.dec=cat_dec_vec[j];
                //cout<<ob.ra<<" "<<ob.dec<<endl;
                ob.mag=cat_mag_vec[j];
                // calcualte position on sphere
                double cd=cos(ob.dec*TMath::DegToRad());
                ob.xyz[0]=cos(ob.ra*TMath::DegToRad())*cd;
                ob.xyz[1]=sin(ob.ra*TMath::DegToRad())*cd;
                ob.xyz[2]=sin(ob.dec*TMath::DegToRad());

                // find closest match within tolerance
                Object_Match match=ref_tree.find_nearest(ob,tol);

                if(match.first!=ref_tree.end()) {
                    ObjPair par;
                    par.obs=ob;
                    par.ref=*match.first;

                    if(cat_magerr_vec[j]<min_err) {
                        mag_err.push_back(1./min_err);
                    } else {
                        mag_err.push_back(1./cat_magerr_vec[j]);
                    }
                    pair_vec.push_back(par);
                }
            }

            int isDefectiveImage = 0;

            cout<<"Found "<<pair_vec.size()<<" matches to reference"<<endl;

            if(pair_vec.size()==0) {
                string evt=Form("Warning: Insufficent matches found in catalog for image (ID=%d); inserting ZP=99; should not be included in stack",id_file);
                report_evt(true,"STATUS",4,evt);
                isDefectiveImage = 1;
                clipped_mean=74.0; // set to sentinal values... (25 will be added downstream) -- don't thank me... 
                clipped_rms=99.0;  // to avoid downstream processing.
            }

            if (!isDefectiveImage)
            {
                //vector<double> mag_diff;
                //mag_diff.reserve(pair_vec.size());
                vector<ObjPair>::iterator iter=pair_vec.begin();
                int jj=0;

                for(; iter!=pair_vec.end();iter++) 
                {
                    // calculate g,r,i from Frank's email
                    double g, r, i;

                    // add more tests later
                    double d=iter->ref.j_mag-iter->ref.f_mag;
                    double j1=iter->ref.j_mag;
                    double f1=iter->ref.f_mag;
                    double n1=iter->ref.i_mag;

                    // RAG 2012Aug29
                    // added test that magnitudes are less than 97 for compatibility 
                    // compatibility with NOAO instances of astrostds files (built using wcstools:scat)
                    if(j1==0 || (j1 > 97) || f1==0 || (f1 > 97) || n1==0 || (n1 > 97)) 
                    {
                        mag_err.erase(mag_err.begin()+jj);
                        continue;
                    }

                    g=0.906*j1+0.094*f1-0.05;
                    r=0.129*j1+0.871*f1+0.076;
                    i=0.173*f1+0.827*n1+0.386;

                    if(use_band=="g") mag_diff.push_back(g-iter->obs.mag);
                    else if(use_band=="r") mag_diff.push_back(r-iter->obs.mag);
                    else if(use_band=="i") mag_diff.push_back(i-iter->obs.mag);

                    jj++;
                }

                // find the mean magnitude difference
                //double mean=TMath::Mean(mag_diff.size(),&mag_diff[0]);
                //double rms=TMath::RMS(mag_diff.size(),&mag_diff[0]);

                if (!with_exposure)
                {
                    int good,bad;
                    double tol=1e-6;
                    //cout << tmp_count << " " << mag_diff.size() << " " << mag_err.size() << endl;
                    sigma_clip(100,2.5,mag_diff,mag_err,clipped_mean,
                               good,bad,clipped_rms,true,false,tol);

                    //  RAG: Added warnings and condition to suppress NaN when convergence does not occur
                    if(bad>good) {
                        string evt=Form("Warning: More values clipped from distribution than used to calculate mean (image ID=%d)",id_file);
                        report_evt(true,"STATUS",3,evt);
                    }
                    if (good < 5){
                        string evt=Form("Warning: Insufficent good values found for image (ID=%d); inserting ZP=99; should not be included in stack",id_file);
                        report_evt(true,"STATUS",4,evt);
                        clipped_mean=74.0;
                        clipped_rms=99.0;
                    }
                    cout<<"Image Mean "<<clipped_mean<<endl;
                    cout<<"Image RMS "<<clipped_rms<<endl;
                    cout<<"Image Good: "<<good<<"    Bad: "<<bad<<endl;

                    //  TH1D *h1=new TH1D("h1","",40,clipped_mean-10*clipped_rms,
                    //                       clipped_mean+10*clipped_rms);

                    //     for(int ii=0;ii<mag_diff.size();ii++) {
                    //       h1->Fill(mag_diff[ii]);
                    //     }

                    //h1->Draw();
                    //c1.Update();
                    // c1.WaitPrimitive();
                    //delete h1;

                    for(int j=0;j<cat_mag_vec.size();j++) {
                        cat_mag_vec[j]+=clipped_mean;
                    }
                }
            }

            if (!with_exposure)
            {
                if (insert_zp) {
                    // assume that original zeropoint is 25 so we don't have
                    // to query the database
                    string db_cmd=Form("insert into zeropoint (id,imageid,mag_zero,"
                               "sigma_mag_zero,source,insert_date)"
                               "values (nextval('zeropoint_seq'),%d,%f,%f,"
                               "'CALPHOT',current_timestamp)",id_file,
                               25+clipped_mean,clipped_rms);

                    if (!skip_db_transaction)
                    {
                        DatabaseInterface db;
                        DatabaseResult res;
                        cout<<db_cmd<<endl;
                        res.Process(db,db_cmd);
                        db.Commit();
                    }
                    else
                        cout << "Skipping DB transaction " << db_cmd << endl;
                }

                // rewrite catalog  -- should this be done here?
                // Update magnitudes in catalog
                cout << "Updating file " << out_name << endl;

                ValueType val=cat_table.column(cat_mag_string).type();
                size_t repeat=cat_table.column(cat_mag_string).repeat();
                string unit=cat_table.column(cat_mag_string).unit();

                cat_table.deleteColumn(cat_mag_string);
                cat_table.addColumn(val,cat_mag_string,1,unit);

                cat_table.column(cat_mag_string).write(cat_mag_vec,0);
            }

            delete cat_fits;
        }

        if (!with_exposure) break;

        // do it for the entire exposure
        int good,bad;
        double tol=1e-6;
        //cout << tmp_count << " " << mag_diff.size() << " " << mag_err.size() << endl;
        sigma_clip(100,2.5,mag_diff,mag_err,clipped_mean,
               good,bad,clipped_rms,true,false,tol);
        //cout << "IMGs per exposure: " << imgs_per_exposure << endl;
        if(bad>good) {
            string evt=Form("Warning: More values clipped from distribution than used to calculate mean (exposure ID=%d)",current_exposure_id);
            report_evt(true,"STATUS",3,evt);
            clipped_mean=74.0;
            clipped_rms=99.0;
        }
        if (good < imgs_per_exposure*2){
            string evt=Form("Warning: Insufficent good values found for exposure (ID=%d); inserting ZP=99; should not be included in stack",current_exposure_id);
            report_evt(true,"STATUS",4,evt);
            clipped_mean=74.0;
            clipped_rms=99.0;
        }

        cout<<"Exposure Mean "<<clipped_mean<<endl;
        cout<<"Exposure RMS "<<clipped_rms<<endl;
        cout<<"Exposure Good: "<<good<<"    Bad: "<<bad<<endl;

        if (insert_zp) 
        {
            int jj=0;

            for(int i=0;i<cat_vec.size();++i)
            {
                // skip the file if it is from a different exposure
                if (exposure_vec[i] != current_exposure_id) continue;

                string db_cmd=Form("insert into zeropoint (id,imageid,mag_zero,"
                               "sigma_mag_zero,source,insert_date)"
                               "values (nextval('zeropoint_seq'),%d,%f,%f,"
                               "'CALPHOT',current_timestamp)",id_vec[i],
                               25+clipped_mean,clipped_rms);

                if (!skip_db_transaction)
                {
                    DatabaseInterface db;
                    DatabaseResult res;
                    cout<<db_cmd<<endl;
                    res.Process(db,db_cmd);
                    db.Commit();
                }
                else
                    cout << "Skipping DB transaction " << db_cmd << endl;

                cout << "Updating file " << ccat_out_name[jj] << endl;

                if (clipped_rms != 99.0)
                {
                    // Update magnitudes in catalog
                    vector<double> cat_mag_vec;
                    FITS cat_fits(ccat_out_name[jj], Write);

                    ExtHDU& cat_table = cat_fits.extension(2);
                    int N_cat=cat_table.rows();

                    cat_table.column(cat_mag_string).read(cat_mag_vec,0,N_cat);

                    for(int j=0;j<cat_mag_vec.size();j++) {
                        cat_mag_vec[j]+=clipped_mean;
                    }

                    //ValueType val=cat_table.column(cat_mag_string).type();
                    //size_t repeat=cat_table.column(cat_mag_string).repeat();
                    //string unit=cat_table.column(cat_mag_string).unit();
                    //cat_table.deleteColumn(cat_mag_string);
                    //cat_table.addColumn(val,cat_mag_string,1,unit);

                    cat_table.column(cat_mag_string).write(cat_mag_vec,0);
                }

                jj++;
            }
        }

    } while (++exp_indx < unique_exposures_vec.size());

    delete ref_fits;
    timer.Print();
}

string UseFilter(const string &filter)
{
  if(filter=="g") return "g";
  else if(filter=="r") return "r";
  else if(filter=="i") return "i";
  else if(filter=="z") return "i";

  string broadband="";

  if(filter=="337") broadband="U";
  else if(filter=="390") broadband="U";
  else if(filter=="420") broadband="B";
  else if(filter=="454") broadband="B";
  else if(filter=="493") broadband="V";
  else if(filter=="527") broadband="V";
  else if(filter=="579") broadband="V";
  else if(filter=="607") broadband="R";
  else if(filter=="666") broadband="R";
  else if(filter=="705") broadband="R";
  else if(filter=="755") broadband="I";
  else if(filter=="802") broadband="I";
  else if(filter=="815") broadband="I";
  else if(filter=="823") broadband="I";
  else if(filter=="848") broadband="I";
  else if(filter=="918") broadband="I";
  else if(filter=="973") broadband="I";
  else if(filter=="B") broadband="B";
  else if(filter=="blank") broadband="V";
  else if(filter=="Bw") broadband="U";
  else if(filter=="C") broadband="B";
  else if(filter=="D51") broadband="V";
  else if(filter=="Gn") broadband="B";
  else if(filter=="g") broadband="B";
  else if(filter=="ha4") broadband="R";
  else if(filter=="ha8") broadband="R";
  else if(filter=="ha") broadband="R";
  else if(filter=="ha12") broadband="R";
  else if(filter=="ha16") broadband="R";
  else if(filter=="I") broadband="I";
  else if(filter=="i") broadband="I";
  else if(filter=="It") broadband="I";
  else if(filter=="M") broadband="V";
  else if(filter=="o2") broadband="B";
  else if(filter=="o3") broadband="V";
  else if(filter=="O3") broadband="V";
  else if(filter=="R") broadband="R";
  else if(filter=="r") broadband="R";
  else if(filter=="s2") broadband="R";
  else if(filter=="U") broadband="U";
  else if(filter=="Un") broadband="U";
  else if(filter=="u") broadband="U";
  else if(filter=="V") broadband="V";
  else if(filter=="VR") broadband="R";
  else if(filter=="wh") broadband="V";
  else if(filter=="wr475") broadband="B";
  else if(filter=="wrc3") broadband="B";
  else if(filter=="wrc4") broadband="B";
  else if(filter=="wrhe2") broadband="B";
  else if(filter=="z") broadband="I";
  else if(filter=="Us") broadband="U";
  else if(filter=="Ud") broadband="U";
  else if(filter=="Y") broadband="I";

  if(broadband=="U") return "g";
  else if(broadband=="B") return "g";
  else if(broadband=="V") return "r";
  else if(broadband=="R") return "r";
  else if(broadband=="I") return "i";
  else return "";
}


