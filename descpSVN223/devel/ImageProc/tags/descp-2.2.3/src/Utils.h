#include <string>
#include <vector>
using std::string;
using std::vector;

void Tokenize(const string& str,
              vector<string>& tokens,
              const string& delimiters = " ");

bool get_dbinfo(string which_db,string &db,string &user,
                string &passwd,string file="");


void sigma_clip(int max_iter,double thresh,
                const vector<double> &array,
                const vector<double> &err,double &mean,
                int &good, int &bad,double &rms,bool clip,
                bool weighted,double &tol);

void report_evt(bool verbose,string type,int level,string mess);

void LoadStyle();
