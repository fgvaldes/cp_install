/*$Id: coadd_calczp.c 8189 2012-07-19 19:19:21Z mtcampbe $*/
#include "imageproc.h"  
#include "nrutil.h"
#include "fitswcs.h"
#include "define.h"
#include <stdio.h>
#include <errno.h>
#include "fitscat_defs.h"

#define VERSION 1.1
#define SVN_VERSION 2000


#define TOLERANCE (2.0/3600.0) /* arcsec */
#define ACCURACY 1e-6 /* accuracy in the sigma clipping algorithm */
#define LGE 0.4342945 /* log_10(e) */
#define MAX 5000 /* Max number of matched stars from two overlapped images */
#define MOSAIC 8 
#define DECAM 62
#define Squ(x) ((x)*(x))

void helpmessage(int dummy);
void print_matrix(float **matrix, int nrow, int ncol);
void print_dmatrix(double **matrix, int nrow, int ncol);
void print_imatrix(int **matrix, int nrow, int ncol);
void print_vector(float *vector, int n);
void initialize_matrix(float **matrix, int nrow, int ncol);
void initialize_dmatrix(double **matrix, int nrow, int ncol);
void initialize_imatrix(int **matrix, int nrow, int ncol);
void initialize_vector(float *vector, int n);
void initialize_dvector(double *vector, int n);
void svd_fit(float **A, float *b, float *berr, int nrow, int ncol, float *x, float *xerr);
void iter_mean(double data[], double err[], int N, double *mean, double *rms, int *N_good, int *N_bad, int *N_iter, int flag, int flag_iter, int flag_Niterate, int Nmax_iterate, double THRESHOLD);
double getmean(double data[],double err[],int N,int flag);
double getrms(double data[],double err[],int N,int flag);
void calc_ccdratio(double **skybrite_bookeep, double **skybrite_ccdpair, double **skybriteerr_ccdpair, int ccdtotal, int Nexposure);
void format_runid(char *runid_in, char *runid_out);
/* temporary addition due to the restructure of archive */
void convert_runid(char *basedir, char *runiddesc, char *imagename, char *project, char *imgfullpath);
FILE *Popen(const char *command,const char *mode,const char *file,int line);

main(argc,argv)
     int argc;
     char *argv[];
{
  char project[100],tilenamein[300],binpath[800],etcpath[800],event[1000];
  char band[100],bandlist[1000],nitelist[1000],runidlist[1000],temp[15000];
  char selectremapfile[1000],basedir[1000],outfile[800],imgfullpath[15000];
  char command[2000],comment[800],line[5000],sqlcall[1000],combinetype[50];
  char mag1[20],magerr1[20],mag2[20],magerr2[20],image1[1000],image2[1000];
  char detector[20],ctype1[32],ctype2[32],bandtemp[5],prev[200],image[800];
  char sqlscript[1000],swarpscript[1000],runidout[1000],runidout1[1000],runidout2[1000];
  char magerr_input[100],imagetemp[1000],dblogin[500],zpout[1000],idout[800];
  char inputlist[1000],filename_fromlist[1000],name1[1000],name2[1000],temp1[1000];
  char imagelistin[1000],varlistin[1000],tilenamecheck[100],tempa[1000],tempb[1000];
  char **tilerunid,**runidnites,**nites,**runids,**bands,**nites_in,**runid_in;
  char **image_ccd_in,**image_ccd;
  char dblogin_i[500],zp_src[20];

  int flag_quiet=0,flag_proj=0,flag_tilein=0,flag_outfile=0;
  int flag_binpath=0,flag_bandlist=0,flag_nitelist=0,flag_fwhm=0;
  int flag_runidlist=0,flag_check=0,flag_selectremap=0,flag_rmscut=0;
  int flag_etcpath=0,flag_basedir=0,flag_combinetype=0,flag_magerr=0;
  int flag_detector=0,flag_magcut=0,flag_weightfit=0,flag_assginzp=0;
  int flag_Niterate=0,flag_Nstarmatch=0,flag_mean=0,flag_outzp=0;
  int flag_nostarmatch=0,flag_noskybrite=0,flag_exptime=0,flag_badzp=0;
  int flag_nophotozp=0,flag_quickcoadd=0,flag_list=0,flag_neighbour=0;
  int status=0,newcount=0,flag_checklist,flag_deltamag=0,flag_debug=0;

  int i,j,k,s,m,maxzp,flag_zpmax,endloop,jstart,len,ID,remapID;
  int nimage,nzp,nzpin,nstarmatch,nskybrite,imageid_temp,count,npix_ra,npix_dec;
  int nimage_in,N_neighbour,flag,flag_se,flag_iter,flag_weight,magtype,check_exist;
  int N_good,N_bad,N_iter,ccdtotal,Nmax_iterate,Nrun;
  int ncol,nrow,nconstrain,newcountstar=0,Nmin_starmatch,nsameredimg;
  int Nrunidnite,Nband,Nnite,Nrunid,ccdnum,Nexposure=0,track;
  int ncomma,nnite_in,nrunid_in,list_count;
  int *saveid1,*saveid2,*keepimg,*flag_checkzp;
  int **flag_mag_mean,**flag_skybrite,**flag_sameredimg;

  float class_star_lo,class_star_hi,radius,sigma,magerrin,fwhm_in,fwhm,pixscale;
  float exptime_lo,exptime_hi,exptime,magout1,magout2,magerrout1,magerrout2;
  float magrmscut,mag_base,magerr_base,fluxscale,mag_low,mag_high,pixelsize,maxmagdiff,deltamag;
  float *skybrite,*b,*berr,*x,*xerr,**A;

  double equinox_in,obsdate_in;
  double crpix1,crpix2,crval1,crval2;
  double cd1_1,cd1_2,cd2_1,cd2_2;
  double rho,rho_a,rho_b,crota2;
  double cdelt1,cdelt2;
  double xpix,ypix;
  double ra_tem,dec_tem;
  double maxra,minra,maxdec,mindec;
  double tile_ra,tile_dec,TILEBUFF;
  double mag_mean,magrms_mean,skybrite_temp,weight_temp;
  double *ra1,*ra2,*ra3,*ra4,*dec1,*dec2,*dec3,*dec4;
  double **skybrite_bookeep,**skybrite_ccdpair;
  double **skybriteerr_ccdpair,**save_skybriteerr;
  double *datain,*errin,**save_mag_mean;
  double **save_mag_rms,**save_skybrite;
  
  double pv1_0,pv1_1,pv1_2,pv1_3,pv1_4,pv1_5,pv1_6,pv1_7,pv1_8,pv1_9,pv1_10;
  double pv2_0,pv2_1,pv2_2,pv2_3,pv2_4,pv2_5,pv2_6,pv2_7,pv2_8,pv2_9,pv2_10;
  
  
  /* for wcs stuff */
  char **ctype;
  double *cd1, *cd2, *crval, *crpix, *cdelt;
  int *naxisn, naxis;
  double wcspos[2],rawpos[2];

  keystruct *key;
  catstruct *cat;
  tabstruct *tab, *imatab;
  wcsstruct *wcsin;

  float ZPLO,ZPHI;
  //int flag_tem62;

  long axes0,axes1;
  time_t curtime=time (NULL), lsttime;
  
  db_tiles      *tileinfo,*tileinfo_in,*tilelist;
  db_zeropoint  *zp, *zp_latest;

  FILE *fsqlout,*pip,*fswarp,*fswarpimg,*fswarpvar,*fzpcompare,*fidout,*flist,*flistout,*ftilelist;
  fitsfile *fptr;
  void select_dblogin(),printerror(),reportevt();
  

  QMALLOC(ctype,char *,2);
  for(i=0;i<16;i++)
    ctype[i]=(char *)calloc(20,sizeof(char));
  QMALLOC(crpix,double,2);
  QMALLOC(crval,double,2);
  QMALLOC(cd1,double,2);
  QMALLOC(cd2,double,2);
  QMALLOC(cdelt,double,2);
  QMALLOC(naxisn,int,2);

  if (argc<2) {
    printf("Usage: %s \n",argv[0]);
    printf("       -project <project name>\n");
    printf("       -tilename <tile string>\n");
    //printf("       -band <band#1,band#2,...>\n");
    printf("       -band <band>\n");
    printf("       -detector <detector> (either DECam or Mosaic2) \n");
    printf("\n");

    printf("    Option:\n");
    printf("          -debug (no ingestion to zeropoint table if set) \n");
    printf("          -basedir <basedir> (dir level before runid/...) \n");
    printf("          -binpath <binpath>\n");
    printf("          -etcpath <etcpath>\n");
    printf("          -nite <nite#1,nite#2,...>\n");
    printf("          -run <runid#1,runid#2,...>\n");
    printf("          -list <listname>\n");
    printf("          -class_star <lower_#> <upper_#> (set the range of CLASS_STAR; default is 0.99 to 1)\n");
    printf("          -sigmaclip <#> (threshold in sigma-clipping if use sigma-clipping; default is not using)\n");
    printf("          -Niterate <#> (maximum number of iteration in sigma-clipping; default is not set)\n");
    printf("          -Nstarmatch <#> (minimum number of matched stars; default is 1)\n");
    printf("          -rmscut <#> (RMS cut for the mean magnitudes in star matching; default is 0.05)\n");
    printf("          -flag <#> (upper limit of flag value from SExtractor; default is 0)\n");
    printf("          -radius <#> (radius of search in arcmin;default is 0.034)\n");
    printf("          -magtype <#> (default is 0)\n");
    printf("                       (-1 = mag_model)\n");
    printf("                       (-2 = mag_psf)\n");
    printf("                       (0 = mag_auto)\n");
    printf("                       (1-17 = mag_aper1-17)\n");
    printf("          -magerr <#> (magerr filtering; default is 0.10)\n");
    printf("          -magcut <low_mag> <high_mag> (range of magnitude cut; default is no magnitude cut applied)\n");
    printf("          -maxmagdiff <#> (maximum value for mag_difference cut; default is 6.25)\n");
    printf("          -fwhm <#> (maximum value for fhwm cut; default is not using fwhm cut)\n");
    printf("          -exptime <lower_#> <upper_#> (range of exposure cut; default is not using exposure time cut)\n");
    printf("          -weightmean (if use weighted mean; default is using unweighted mean)\n");
    printf("          -weightfit (if use weighted fit in matrix equation; default is using unweighted fit)\n");
    printf("          -weight (if use both weighted mean and weighted fit; default is using unweighted)\n");
    printf("          -combinetype <median,average,min,max,weighted,chi2,sum; default is median> \n");
    printf("          -zp_range <lower_#> <upper_#> (set the range of zeropoints that can be inserted into the database; default is 27 to 34)\n");
    printf("          -zp_src (source value in zeropoint table; default is COADD)\n");
    printf("          -outzp <output ZP filename>\n");
    printf("          -neighbour\n");
    printf("          -moreneighbour\n");
    printf("          -nostarmatch\n");
    printf("          -noskybrite\n");
    printf("          -nophotozp\n");
    printf("          -quickcoadd\n");
    printf("          -help\n");
    printf("          -version\n");
    printf("          -quiet\n");
    exit(0);
  }
  
  /* initialize */
  maxzp=endloop=ccdtotal=nrunid_in=0;

  /* set default value */
  flag_se=0;
  flag_iter=1;
  flag_weight=0;
  class_star_lo=0.99;
  class_star_hi=1.0; 
  ZPLO=27;
  ZPHI=34;
  magtype=0;
  sprintf(zp_src,"COADD");
  magrmscut=0.05;
  magerrin=0.10;
  radius=0.034;
  sigma=2.5;
  Nmin_starmatch=1;
  Nmax_iterate=3;
  maxmagdiff=6.25;
  TILEBUFF=0.8;

  sprintf(combinetype,"MEDIAN");
  nconstrain=0;
  nzp=nstarmatch=nskybrite=0;

  /* process the command line */
  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-project"))  {
      flag_proj=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -project option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {     
	sprintf(project,"%s",argv[i+1]);
	if (!strncmp(&project[0],"-",1)) {
	  printf(" ** %s error: wrong input of <project name>\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-tilename"))  {
      flag_tilein=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -tilename option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(tilenamein,"%s",argv[i+1]);
	if (!strncmp(&tilenamein[0],"-",1)) {
	  printf(" ** %s error: wrong input of <tile string>\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-band"))  {
      flag_bandlist=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -band option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(bandlist,"%s",argv[i+1]);
	sprintf(band,"%s",argv[i+1]);
	if (!strncmp(&bandlist[0],"-",1)) {
	  //printf(" ** %s error: wrong input of <band#1,band#2,...>\n",argv[0]);
	  printf(" ** %s error: wrong input of <band>\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-detector"))  {
      flag_detector=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -detector option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(detector,"%s",argv[i+1]);
	if (!strncmp(&basedir[0],"-",1)) {
	  printf(" ** %s error: wrong input of <detector>\n",argv[0]);
	  exit(1);
	}
	
	if(!strcmp(detector,"DECam")) { 
	  ccdtotal=DECAM;
	  //TILEBUFF=1.2;
	}
	else if(!strcmp(detector,"Mosaic2")) {
	  ccdtotal=MOSAIC;
	  //TILEBUFF=0.7;
	}
	else {
	  printf(" ** %s error: check the input of <detector>\n",argv[0]);
	  exit(1);
	}
      }
    }
    
    /* options */
    if (!strcmp(argv[i],"-debug"))  
      flag_debug=1;

    if (!strcmp(argv[i],"-basedir"))  {
      flag_basedir=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -basedir option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(basedir,"%s",argv[i+1]);
	if (!strncmp(&basedir[0],"-",1)) {
	  printf(" ** %s error: wrong input of <basedir>\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-binpath"))  {
      flag_binpath=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -binpath option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(binpath,"%s",argv[i+1]);
	if (!strncmp(&binpath[0],"-",1)) {
	  printf(" ** %s error: wrong input of <binpath>\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-etcpath"))  {
      flag_etcpath=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -etcpath option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(etcpath,"%s",argv[i+1]);
	if (!strncmp(&etcpath[0],"-",1)) {
	  printf(" ** %s error: wrong input of <etcpath>\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-nite"))  {
      flag_nitelist=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -nite option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(nitelist,"%s",argv[i+1]);
	if (!strncmp(&nitelist[0],"-",1)) {
	  printf(" ** %s error: wrong input of <nite#1,nite#2,...>\n",argv[0]);
	  exit(1);
	}

	ncomma=0;
	len=strlen(nitelist);
	for (j=len;j>0;j--) {
	  if (!strncmp(&(nitelist[j]),",",1)) { 
	    ncomma++;
	    nitelist[j]=32;
	  }
	}
	nnite_in=ncomma+1;	
	nites_in=(char **)calloc(nnite_in,sizeof(char *));
	for(j=0;j<nnite_in;j++) 
	  nites_in[j]=(char *)calloc(64,sizeof(char ));
	s=0;
	for(k=0;k<nnite_in;k++) {
	  sscanf(nitelist+s,"%s%[\0]",temp);
	  sprintf(nites_in[k],"%s",temp);
	  len=strlen(temp);
	  s+=len+1;
	}	
      }
    }

    if (!strcmp(argv[i],"-run"))  {
      flag_runidlist=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -run option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {    
	sprintf(runidlist,"%s",argv[i+1]);
	if (!strncmp(&runidlist[0],"-",1)) {
	  printf(" ** %s error: wrong input of <runid#1,runid#2,...>\n",argv[0]);
	  exit(1);
	}

	ncomma=0;
	len=strlen(runidlist);
	for (j=len;j>0;j--) {
	  if (!strncmp(&(runidlist[j]),",",1)) { 
	    ncomma++;
	    runidlist[j]=32;
	  }
	}
	nrunid_in=ncomma+1;
	runid_in=(char **)calloc(nrunid_in,sizeof(char *));
	for(j=0;j<nrunid_in;j++) 
	  runid_in[j]=(char *)calloc(1024,sizeof(char ));
	s=0;
	for(k=0;k<nrunid_in;k++) {
	  sscanf(runidlist+s,"%s%[\0]",temp);
	  sprintf(runid_in[k],"%s",temp);
	  len=strlen(temp);
	  s+=len+1;
	}
      }
    }

    if (!strcmp(argv[i],"-list"))  {
      flag_list=1;
      if(argv[i+1]==NULL) {    printf("       -verbose <0-3>\n");
        printf(" ** %s ERROR: input for -list option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(inputlist,"%s",argv[i+1]);
	if (!strncmp(&inputlist[0],"-",1)) {
	  printf(" ** %s error: wrong input of <listname>\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-selectremap"))  {
      flag_selectremap=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: filename for -selectremap option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(selectremapfile,"%s",argv[i+1]);
	if (!strncmp(&selectremapfile[0],"-",1)) {
	  printf(" ** %s error: wrong input of <filename> for -selectremap\n",argv[0]);
	  exit(0);
	}
      }
    }

    if (!strcmp(argv[i],"-class_star")) {
           if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -class_star option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	/*spread_model is less than 0. so undo it.
	  if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input of <lower#> <upper#> for -class_star\n",argv[0]);
	  exit(0);
	}
	*/
	sscanf(argv[i+1],"%f",&class_star_lo);
	sscanf(argv[i+2],"%f",&class_star_hi);
      }
    }

     if (!strcmp(argv[i],"-zp_range")) {
       if(argv[i+1]==NULL) {
         printf(" ** %s ERROR: value for -zp_range option is not set. Abort!\n",argv[0]);
         exit(1);
       }
      else {
        sscanf(argv[i+1],"%f",&ZPLO);
	sscanf(argv[i+2],"%f",&ZPHI);
      }
    }
     
    if (!strcmp(argv[i],"-magcut")) {
      flag_magcut=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -magcut option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input of <low_mag> <upp_mag> for -magcut\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&mag_low);
	sscanf(argv[i+2],"%f",&mag_high);
      }
    }
 
    if (!strcmp(argv[i],"-sigmaclip")) {
      flag_iter=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -sigmaclip option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -sigmaclip\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&sigma);
      }
    }

    if (!strcmp(argv[i],"-Niterate")) {
      flag_iter=1;
      flag_Niterate=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -Niterate option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -Niterate\n",argv[0]);
	  exit(1);
	}
	Nmax_iterate=atoi(argv[i+1]);
      }
    }

    if (!strcmp(argv[i],"-Nstarmatch")) {
      flag_Nstarmatch=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -Nstarmatch option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -Nstarmatch\n",argv[0]);
	  exit(1);
	}
	Nmin_starmatch=atoi(argv[i+1]);
      }
    }

    if (!strcmp(argv[i],"-rmscut")) { 
      flag_rmscut=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -rmscut option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -rmscut\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&magrmscut);
      }
    }

    if (!strcmp(argv[i],"-flag")) {
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -flag option is not set. Abort!\n",argv[0]);
        exit(0);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -flag\n",argv[0]);
	  exit(1);
	}
	flag_se=atoi(argv[i+1]);
      }
    }

    if (!strcmp(argv[i],"-radius")) {
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -radius option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -radius\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&radius);
      }
    }

    if (!strcmp(argv[i],"-magtype")) {
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -magtype option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	/*
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -magtype\n",argv[0]);
	  exit(0);
	}
*/
	magtype=atoi(argv[i+1]);
      }
    }
    
    if (!strcmp(argv[i],"-magerr")) {
      flag_magerr=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -magerr option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -magerr\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&magerrin);
      }
    }

    if (!strcmp(argv[i],"-maxmagdiff")) {
      flag_magerr=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -maxmagdiff option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -maxmagdiff\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&maxmagdiff);
      }
    }

    if (!strcmp(argv[i],"-fwhm")) { 
      flag_fwhm=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -fwhm option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input for -fwhm\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&fwhm_in);
      }
    }

    if (!strcmp(argv[i],"-exptime")) {
      flag_exptime=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: value for -exptime option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(temp,"%s",argv[i+1]);
	if (!strncmp(&temp[0],"-",1)) {
	  printf(" ** %s error: wrong input of <lower_#> <upper_#> for -exptime\n",argv[0]);
	  exit(1);
	}
	sscanf(argv[i+1],"%f",&exptime_lo);
	sscanf(argv[i+2],"%f",&exptime_hi);
      }
    }

    if (!strcmp(argv[i],"-outzp"))  {
      flag_outfile=1;
      if(argv[i+1]==NULL) {
	printf(" ** %s ERROR: filename for -outzp option is not set. Abort!\n",argv[0]);
	exit(1);
      }
      else {
	sprintf(outfile,"%s",argv[i+1]);
	if (!strncmp(&outfile[0],"-",1)) {
	  printf(" ** %s error: wrong input of <filename> for -outzp\n",argv[0]);
	  exit(1);
	}
      }
    }

    if (!strcmp(argv[i],"-combinetype"))  {
      flag_combinetype=1;
      if(argv[i+1]==NULL) {
        printf(" ** %s ERROR: input for -combinetype option is not set. Abort!\n",argv[0]);
        exit(1);
      }
      else {
	sprintf(combinetype,"%s",argv[i+1]);
	if (!strncmp(&combinetype[0],"-",1)) {
	  printf(" ** %s error: wrong input of <combinetype>\n",argv[0]);
	  exit(1);
	}
	if (!strcmp(argv[i+1],"median")) sprintf(combinetype,"MEDIAN");
	else if (!strcmp(argv[i+1],"average")) sprintf(combinetype,"AVERAGE");
	else if (!strcmp(argv[i+1],"min")) sprintf(combinetype,"MIN");
	else if (!strcmp(argv[i+1],"max")) sprintf(combinetype,"MAX");
	else if (!strcmp(argv[i+1],"weighted")) sprintf(combinetype,"WEIGHTED");
	else if (!strcmp(argv[i+1],"chi2")) sprintf(combinetype,"CHI2");
	else if (!strcmp(argv[i+1],"sum")) sprintf(combinetype,"SUM");
	else {
	  printf(" ** %s error: wrong input for <combinetype>, reset to default\n",argv[0]);
	  sprintf(combinetype,"WEIGHTED");
	}
      }
    }

    if (!strcmp(argv[i],"-weightmean")) flag_weight=1;
    if (!strcmp(argv[i],"-weightfit")) flag_weightfit=1;
    if (!strcmp(argv[i],"-weight")) {
      flag_weightfit=1; flag_weight=1;
    }
    if (!strcmp(argv[i],"-neighbour")) flag_neighbour=1;
    if (!strcmp(argv[i],"-moreneighbour")) { 
      flag_neighbour=1;
      TILEBUFF*=2.0;
    }
    if (!strcmp(argv[i],"-help")) {
      helpmessage(0);
      exit(0);
    }
    if (!strcmp(argv[i],"-version")) {
      printf("%s: Version %2.2f (SVN Version %d)\n",argv[0],VERSION,SVN_VERSION);
      exit(0);
    }
    if (!strcmp(argv[i],"-quiet")) flag_quiet=1;

    if (!strcmp(argv[i],"-nostarmatch")) flag_nostarmatch=1;
    if (!strcmp(argv[i],"-noskybrite")) flag_noskybrite=1;
    if (!strcmp(argv[i],"-nophotozp")) flag_nophotozp=1;
    if (!strcmp(argv[i],"-quickcoadd")) flag_quickcoadd=1;

     if (!strcmp(argv[i],"-zp_src"))  {
       if(argv[i+1]==NULL) {
         printf(" ** %s ERROR: filename for -zp_src option is not set. Abort!\n",argv[0]);
         exit(0);
       }
       else {
         sprintf(zp_src,"%s",argv[i+1]);
       }
     }
  }

  /* print out the time of processing */
  if(!flag_quiet)
    printf("\n ** Running %s on %s \n",argv[0],asctime(localtime (&curtime)));

  /* check the required input commands */
  if(!flag_proj) { printf(" ** %s error: -project is not set, abort!\n", argv[0]); flag_check=1; }
  if(!flag_tilein) { printf(" ** %s error: -tilename is not set, abort!\n", argv[0]); flag_check=1; }
  if(!flag_bandlist) { printf(" ** %s error: -band is not set, abort!\n", argv[0]); flag_check=1; }
  //if(!flag_basedir) { printf(" ** %s error: -basedir is not set, abort!\n", argv[0]); flag_check=1; }
  if(!flag_detector) { printf(" ** %s error: -detector is not set, abort!\n", argv[0]); flag_check=1; }
  if(flag_check) exit(0);

  /* for quickcoadd */
  if(flag_quickcoadd) {
    flag_nostarmatch=1;
    flag_noskybrite=1;
    flag_nophotozp=1;
  }

  /* printout the parameters */
  if(!flag_quiet) {
    if(!flag_quickcoadd) {
      printf(" ** Using CLASS_STAR range of %2.2f-%2.2f\n",class_star_lo,class_star_hi); 
      printf(" ** Using matching-radius of %2.4f (arcmin)\n",radius); 
      printf(" ** Using SExtractor flag of %d\n",flag_se); 
      if(flag_iter) printf(" ** Using %2.2f-sigma clipping algorithm\n",sigma);
      if(flag_Niterate) printf(" ** Using Nmax_iteration of %d\n",Nmax_iterate);
      printf(" ** Using minimum Nstarmatch of %d\n",Nmin_starmatch);
      if(flag_rmscut) printf(" ** Using %2.3f as maximum RMS in star matching calculation\n",magrmscut);
      printf(" ** Using %2.2f as maximum mag_diffrence cut\n",maxmagdiff);
      if(flag_fwhm) printf(" ** Using maximum FWHM of %2.3f\n",fwhm_in);
      if(flag_exptime) printf(" ** Using EXPTIME between %2.3f and %2.3f\n",exptime_lo,exptime_hi);
      if(flag_weight) printf(" ** Using weighted mean\n");
      else printf(" ** Using unweighted mean\n");
      if(flag_weightfit) printf(" ** Using weighted fit in SVD\n");
      else printf(" ** Using unweighted fit in SVD\n");
      printf(" ** Using magerr of %2.3f in filtering when matching stars\n",magerrin);
      if(flag_magcut) printf(" ** Using magnitude cut from %2.3f to %2.3f\n",mag_low,mag_high);
      printf(" ** Using -COMBINETYPE %s for SWarp\n",combinetype);
      printf(" ** Using zeropoint mag range %0.2f %0.2f \n",ZPLO,ZPHI);
      if(flag_neighbour) printf(" ** Using additional neighbouring tiles\n");
      if (strcmp(zp_src,"COADD"))printf(" ** Using src %s when inserting in zeropoint table\n",zp_src);
    }
  }

  /* assign the magtype */
  if (magtype==-1) {
    sprintf(mag1,"mag_model_1"); sprintf(mag2,"%s","mag_model_2");
    sprintf(magerr1,"magerr_model_1"); sprintf(magerr2,"%s","magerr_model_2");
    sprintf(magerr_input,"magerr_model");
  }
  else if (magtype == -2) {
    sprintf(mag1,"mag_psf_1"); sprintf(mag2,"%s","mag_psf_2");
    sprintf(magerr1,"magerr_psf_1"); sprintf(magerr2,"%s","magerr_psf_2");
    sprintf(magerr_input,"magerr_psf");
  }
  else if (magtype==0) {
    sprintf(mag1,"mag_auto_1"); sprintf(mag2,"%s","mag_auto_2"); 
    sprintf(magerr1,"magerr_auto_1"); sprintf(magerr2,"%s","magerr_auto_2"); 
    sprintf(magerr_input,"magerr_auto");
  }
 else {
    if (magtype>17) {
      sprintf(event,"No such magnitude:  magtype=%d",magtype);
      reportevt(3,STATUS,5,event);
    }
    sprintf(mag1,"mag_aper_%d_1",magtype); 
    sprintf(magerr1,"magerr_aper_%d_1",magtype); 
    sprintf(mag2,"mag_aper_%d_2",magtype); 
    sprintf(magerr2,"magerr_aper_%d_2",magtype); 
    sprintf(magerr_input,"magerr_aper_%d",magtype);
 }
  if(!flag_quiet) {
    if(!flag_quickcoadd) {
      if(!magtype)  printf(" ** Using mag_auto for magnitudes\n"); 
      else printf(" ** Using mag_aper_%d for magnitudes\n",magtype); 
      printf("\n");
    }
  }

  /* grab dblogin */
  select_dblogin(dblogin,DB_READONLY);
  
  /****************************************************/
  /* Find out the neighbouring tiles if set           */
  /****************************************************/

  if(flag_neighbour) {

    /* get the info for the tile as well */
    sprintf(sqlscript,"%s_tileneighbour.sql",tilenamein);
    fsqlout=fopen(sqlscript, "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    fprintf(fsqlout,"select RA,DEC ");
    fprintf(fsqlout,"from coaddtile where ");
    fprintf(fsqlout,"project='%s' ",project);
    fprintf(fsqlout," and tilename='%s' ",tilenamein);
    fprintf(fsqlout,";\n");
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);

    /* construct sql call */
    sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);

    pip=Popen(sqlcall,"r",__FILE__,__LINE__);
    fscanf(pip,"%lf %lf ",&tile_ra,&tile_dec);
    pclose(pip); 

    /* query again to find out the neighbouring tiles */
    sprintf(sqlscript,"%s_tileneighbour.sql",tilenamein);
    fsqlout=fopen(sqlscript, "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    fprintf(fsqlout,"select count(tilename) ");
    fprintf(fsqlout,"from coaddtile where ");
    fprintf(fsqlout,"project='%s' ",project);
    fprintf(fsqlout," and (RA between %2.7f and %2.7f) and (DEC between %2.7f and %2.7f)",
	    tile_ra-TILEBUFF/cos(tile_dec*M_PI/180.0),tile_ra+TILEBUFF/cos(tile_dec*M_PI/180.0),tile_dec-TILEBUFF,tile_dec+TILEBUFF);
    fprintf(fsqlout,";\n");
    fprintf(fsqlout,"select tilename,RA,DEC,NPIX_RA,NPIX_DEC,PIXELSIZE ");
    fprintf(fsqlout,"from coaddtile where ");
    fprintf(fsqlout,"project='%s' ",project);
    fprintf(fsqlout," and (RA between %2.7f and %2.7f) and (DEC between %2.7f and %2.7f)",
	    tile_ra-TILEBUFF/cos(tile_dec*M_PI/180.0),tile_ra+TILEBUFF/cos(tile_dec*M_PI/180.0),tile_dec-TILEBUFF,tile_dec+TILEBUFF);
    fprintf(fsqlout,";\n");
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);

    i=0;
    pip=Popen(sqlcall,"r",__FILE__,__LINE__);
    ftilelist=fopen("tile.list","w");
    while (fgets(line,5000,pip)!=NULL) {
      if (!i) {
      /* find out how many images */
	sscanf(line,"%d",&N_neighbour);

	tilelist=(db_tiles *)calloc(N_neighbour+1,sizeof(db_tiles));
	
	if(!flag_quiet)
	  printf(" ** The list of neighbouring tiles, include the input tile, are:\n");
      }
      else {
	sscanf(line,"%s %lg %lg %d %d %f",
	       tilelist[i].tilename,&(tilelist[i].ra),&(tilelist[i].dec),&(tilelist[i].npix_ra),&(tilelist[i].npix_dec),&(tilelist[i].pixelsize));
	fprintf(ftilelist,"%s\n",tilelist[i].tilename);
	if(!flag_quiet)
	  printf("%s\t%2.6f %2.6f\n",tilelist[i].tilename,tilelist[i].ra,tilelist[i].dec);
      }
      i++;
    }
    pclose(pip);
    fclose(ftilelist);

    if(!flag_quiet)
      printf("\n");
  }
  else {
    N_neighbour=1;
    tilelist=(db_tiles *)calloc(N_neighbour+1,sizeof(db_tiles));

    /* query again to find out the tile info */
    sprintf(sqlscript,"%s_%s.sql",tilenamein,band);
    fsqlout=fopen(sqlscript, "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    fprintf(fsqlout,"select tilename,RA,DEC,NPIX_RA,NPIX_DEC,PIXELSIZE ");
    fprintf(fsqlout,"from coaddtile where ");
    fprintf(fsqlout,"project='%s' ",project);
    fprintf(fsqlout," and tilename='%s'",tilenamein);
    fprintf(fsqlout,";\n");
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);

    /* construct sql call */
    sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);

    pip=Popen(sqlcall,"r",__FILE__,__LINE__);
    fscanf(pip,"%s %lg %lg %d %d %f",
	   tilelist[1].tilename,&(tilelist[1].ra),&(tilelist[1].dec),&(tilelist[1].npix_ra),&(tilelist[1].npix_dec),&(tilelist[1].pixelsize));
    pclose(pip);
  }

 
  /****************************************************/
  /* run DB call to get image list and info           */
  /****************************************************/

  //sprintf(sqlscript,"%s_exposure.sql",tilenamein);
  //fsqlout=fopen(sqlscript, "w");

  //fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
  //fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
  //fprintf(fsqlout,"select distinct(exposurename) from image a,exposure b where ");
  //fprintf(fsqlout,"a.project='%s' ",project);
  //fprintf(fsqlout,"and a.band like '%s%%' ",bandlist);
  //if(flag_neighbour) {
  //fprintf(fsqlout,"and (a.tilename='%s' ",tilelist[1].tilename);
  //for(k=2;k<=N_neighbour;k++) 
  //fprintf(fsqlout,"or a.tilename='%s' ",tilelist[k].tilename);
  //fprintf(fsqlout,") ");
  //}
  //else
  //fprintf(fsqlout,"and a.tilename='%s' ",tilenamein);
  //if(flag_nitelist) {
  //fprintf(fsqlout,"and (a.nite like '%s%%' ",nites_in[0]);
  //if(nnite_in>1) 
  //for(k=1;k<nnite_in;k++) fprintf(fsqlout,"or a.nite like '%s%%' ",nites_in[k]);
  //fprintf(fsqlout,") ");
  //}
  //if(flag_runidlist) {
  //fprintf(fsqlout,"and (a.run like '%s%%' ",runid_in[0]);
  //if(nrunid_in>1) 
  //for(k=1;k<nrunid_in;k++) fprintf(fsqlout,"or a.run like '%s%%' ",runid_in[k]);
  //fprintf(fsqlout,") ");
  //}
  //fprintf(fsqlout," and a.exposureid=b.id ");
  //fprintf(fsqlout,";\n");
  //fprintf(fsqlout,"exit;\n");
  //fclose(fsqlout);

  /* construct sql call */
  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);
  pip=Popen(sqlcall,"r",__FILE__,__LINE__);
   
  sprintf(sqlscript,"%s_%s_red.sql",tilenamein,band);
  fsqlout=fopen(sqlscript, "w");
  fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
  fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");

  //i=0;
  //while(fscanf(pip,"%s",imagetemp)!=EOF) {
  //i++;
  //fprintf(fsqlout,"select a.ID,a.CCD,b.EXPOSURENAME,a.NITE,a.RUN,a.EXPTIME from image a,exposure b where a.imagename like '%s_%%.fits%%' and a.imagetype='red' and a.band like '%s%%' and a.project='%s' ",imagetemp,bandlist,project);
  //if(flag_nitelist) {
  //fprintf(fsqlout,"and (a.nite like '%s%%' ",nites_in[0]);
  //if(nnite_in>1) 
  //for(k=1;k<nnite_in;k++) fprintf(fsqlout,"or a.nite like '%s%%' ",nites_in[k]);
  //fprintf(fsqlout,") ");
  //}
  //if(flag_runidlist) {
  //fprintf(fsqlout,"and (a.run like '%s%%' ",runid_in[0]);
  //if(nrunid_in>1) 
  //for(k=1;k<nrunid_in;k++) fprintf(fsqlout,"or a.run like '%s%%' ",runid_in[k]);
  //fprintf(fsqlout,") ");
  //}
  //fprintf(fsqlout," and a.exposureid=b.id ");
  //fprintf(fsqlout," order by a.id;\n");
  //}


  /*fprintf(fsqlout,"select a.PARENTID,a.CCD,b.EXPOSURENAME,a.NITE,a.RUN,a.EXPTIME from image a,exposure b,location c where a.band like '%s%%' and a.project='%s' and a.imagetype='remap' ",bandlist,project); removed Joe Mohr*/
  fprintf(fsqlout,"select a.PARENTID,a.CCD,c.EXPOSURENAME,a.NITE,a.RUN,a.EXPTIME,a.TILENAME from image a,location c where a.band like '%s%%' and a.project='%s' and a.imagetype='remap' ",bandlist,project);
  if(flag_neighbour) {
    fprintf(fsqlout,"and (a.tilename='%s' ",tilelist[1].tilename);
    for(k=2;k<=N_neighbour;k++) 
      fprintf(fsqlout,"or a.tilename='%s' ",tilelist[k].tilename);
    fprintf(fsqlout,") ");
  }
  else
    fprintf(fsqlout,"and a.tilename='%s' ",tilenamein);
  if(flag_nitelist) {
    fprintf(fsqlout,"and (a.nite like '%s%%' ",nites_in[0]);
    if(nnite_in>1) 
      for(k=1;k<nnite_in;k++) fprintf(fsqlout,"or a.nite like '%s%%' ",nites_in[k]);
    fprintf(fsqlout,") ");
  }
  if(flag_runidlist) {
    fprintf(fsqlout,"and (a.run like '%s%%' ",runid_in[0]);
    if(nrunid_in>1) 
      for(k=1;k<nrunid_in;k++) fprintf(fsqlout,"or a.run like '%s%%' ",runid_in[k]);
    fprintf(fsqlout,") ");
  }
  /*fprintf(fsqlout," and a.parentid>0 and a.exposureid=b.id "); removed by Joe Mohr */
  fprintf(fsqlout," and a.parentid=c.id and c.archivesites!='NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN' ");
  fprintf(fsqlout," order by a.parentid;\n");
  fprintf(fsqlout,"exit;\n");
  pclose(pip); fclose(fsqlout);
  
  sprintf(command, "${ORACLE_HOME}/bin/sqlplus -S %s < %s | wc -l",dblogin,sqlscript);
  pip=Popen(command,"r",__FILE__,__LINE__);
  fscanf(pip,"%d",&nimage_in);
  pclose(pip);
  
  //nimage_in=i*ccdtotal;
  //nimage_in=(i-1)*ccdtotal;

  /* memory allocation for the output and initialize keepimg */
  tileinfo_in=(db_tiles *)calloc(nimage_in+1,sizeof(db_tiles));
  keepimg=(int *)calloc(nimage_in+1,sizeof(int));
  for(j=0;j<=nimage_in;j++) keepimg[j]=1;
  image_ccd_in=(char **)calloc(nimage_in+1,sizeof(char *));
  for(j=1;j<=nimage_in;j++) image_ccd_in[j]=(char *)calloc(150,sizeof(char ));
  
  /* construct sql call */
  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);

  pip=Popen(sqlcall,"r",__FILE__,__LINE__);
  for(i=1;i<=nimage_in;i++) {
    fgets(line,5000,pip);
    sscanf(line,"%d %d %s %s %s %f %s",&(tileinfo_in[i].imageid),&(tileinfo_in[i].ccdnum),imagetemp,tileinfo_in[i].nite,tileinfo_in[i].runiddesc,&(tileinfo_in[i].exptime),&(tileinfo_in[i].tilename));
    sprintf(tileinfo_in[i].band,"%s",bandlist);
    sprintf(tileinfo_in[i].imagename,"%s/%s_%02d.fits",imagetemp,imagetemp,tileinfo_in[i].ccdnum);
    sprintf(image_ccd_in[i],"%s_%02d",imagetemp,tileinfo_in[i].ccdnum);
  }
  pclose(pip);


  /* check the input list if it has the same length */
  if(flag_list) {
    sprintf(command, "wc -l %s",inputlist);
    pip=Popen(command,"r",__FILE__,__LINE__);
    while (fgets(line,5000,pip)!=NULL)
      sscanf(line,"%d %s",&list_count,line);
    pclose(pip);

    if(list_count==nimage_in) {
      if(!flag_quiet) 
	printf(" ** Input list %s has the same number of files as in database\n",inputlist);
    }
    else 
      printf(" ** WARNING: Input list %s do bot have the same number of files as in database (%d vs %d)\n",inputlist,list_count,nimage_in);
  }

  if(flag_nitelist)
    free(nites_in); 
  if(flag_runidlist)
    free(runid_in); 

  if(!flag_quiet) printf("\n");

  /***********************************/
  /* various filtering of the images */
  /***********************************/

  /* filtering images with FWHM */
  if(flag_fwhm) {

    /* construct the sql script */
    sprintf(sqlscript,"%s_fwhm.sql",tilenamein);
    fsqlout=fopen(sqlscript, "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    for(i=1;i<=nimage_in;i++) 
      fprintf(fsqlout,"SELECT FWHM from image where id=%d;\n",tileinfo_in[i].imageid);
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);

    /* construct sql call */
    sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);

    pip=Popen(sqlcall,"r",__FILE__,__LINE__);
    for(i=1;i<=nimage_in;i++) {
      fgets(line,5000,pip);
      sscanf(line,"%f", &fwhm);
      if(fwhm > fwhm_in) { 
	keepimg[i]=0;
	if(!flag_quiet)
	  printf(" ** image %s\t%s not used: FWHM = %2.3f\n",tileinfo_in[i].runiddesc,tileinfo_in[i].imagename,fwhm);
      }
    }
    pclose(pip);
  }

  /* filtering images with EXPTIME */
  if(flag_exptime) {

    /* construct the sql script */
    sprintf(sqlscript,"%s_exptime.sql",tilenamein);
    fsqlout=fopen(sqlscript, "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    for(i=1;i<=nimage_in;i++) 
      fprintf(fsqlout,"SELECT EXPTIME from image where id=%d;\n",tileinfo_in[i].imageid);
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);

    /* construct sql call */
    sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);

    pip=Popen(sqlcall,"r",__FILE__,__LINE__);
    for(i=1;i<=nimage_in;i++) {
      fgets(line,5000,pip);
      sscanf(line,"%f", &exptime);
      if(exptime >= exptime_lo && exptime <= exptime_hi) 
	keepimg[i]=1;
      else {
	keepimg[i]=0;
	if(!flag_quiet)
	  printf(" ** image %s\t%s not used: EXPTIME = %2.3f \n",tileinfo_in[i].runiddesc,tileinfo_in[i].imagename,exptime);
      }
    }
    pclose(pip);
  }

  
  /* using the latest id/run for images in the same nite and CCD */
  for(i=1;i<nimage_in;i++) {
    
      for(j=(i+1);j<=nimage_in;j++) {
	if(!strcmp(tileinfo_in[i].imagename,tileinfo_in[j].imagename) && !strcmp(tileinfo_in[i].nite,tileinfo_in[j].nite) && tileinfo_in[i].ccdnum==tileinfo_in[j].ccdnum) {
	  
	  if(tileinfo_in[j].imageid > tileinfo_in[i].imageid)
	    keepimg[i]=0;
	} // j loop
      } 
  }

  /* check the number of images that pass the filterings */
  nimage=0;
  for(i=1;i<=nimage_in;i++) 
    if(keepimg[i]) nimage++;

  if(!flag_quiet)
    printf(" ** %d images used for co-adding (after filtering)\n\n",nimage);
  
  if(!nimage) {
    if(!flag_quiet)
      printf(" ** No images pass the filtering, abort!\n");
    exit(0);
  }
 
  /* memory allocation again */
  tileinfo=(db_tiles *)calloc(nimage+1,sizeof(db_tiles));
  tilerunid=(char **)calloc(nimage+1,sizeof(char *));
  for(j=1;j<=nimage;j++) tilerunid[j]=(char *)calloc(100,sizeof(char ));
  image_ccd=(char **)calloc(nimage+1,sizeof(char *));
  for(j=1;j<=nimage;j++) image_ccd[j]=(char *)calloc(150,sizeof(char ));

  /* input the information from the file */
  j=0;
  for(i=1;i<=nimage_in;i++) {

    if(keepimg[i]) {
      //tileinfo[j+1].id=tileinfo_in[i].id;
      sprintf(tileinfo[j+1].tilename,"%s",tileinfo_in[i].tilename);
      sprintf(tileinfo[j+1].runiddesc,"%s",tileinfo_in[i].runiddesc);
      sprintf(tileinfo[j+1].nite,"%s",tileinfo_in[i].nite);
      sprintf(tileinfo[j+1].band,"%s",tileinfo_in[i].band);
      sprintf(tileinfo[j+1].imagename,"%s",tileinfo_in[i].imagename);
      sprintf(image_ccd[j+1],"%s",image_ccd_in[i]);
      tileinfo[j+1].ccdnum=tileinfo_in[i].ccdnum;
      tileinfo[j+1].imageid=tileinfo_in[i].imageid;
      tileinfo[j+1].exptime=tileinfo_in[i].exptime;
      /* get the runid as well */
      sprintf(temp,"%s",tileinfo_in[i].runiddesc);
      len=strlen(temp);
      for (m=0;m<len;m++) 
	if (!strncmp(&(temp[m]),"_",1)) {
	  temp[m]=0;
	  break;
	}
      sscanf(temp,"%s",tilerunid[j+1]);

      /* update counter */
      j++;
    }
  }
  
  /* output the query results */
  if(!flag_quiet) {
    if(flag_neighbour) {
      printf("\tImageID\tRunIDDESC\t\tTILENAME\tImageName\n");
      for(i=1;i<=nimage;i++) 
	printf("%d\t%d\t%s\t%s\t%s\n",
	       i,tileinfo[i].imageid,tileinfo[i].runiddesc,tileinfo[i].tilename,tileinfo[i].imagename);
    }
    else {
      printf("\tImageID\tRunIDDESC\t\t\t\tImageName\n");
      for(i=1;i<=nimage;i++) 
	printf("%d\t%d\t%s\t%s\n",
	       i,tileinfo[i].imageid,tileinfo[i].runiddesc,tileinfo[i].imagename);
    }
  }


  /**************************************************************/
  /* query zeropoint table to get the latest zp for each images */
  /**************************************************************/

  /* set memory allocation */  
  zp_latest=(db_zeropoint *)calloc(nimage+1,sizeof(db_zeropoint));
  
  if(!flag_nophotozp) {

    /* construct the sql script */
    sprintf(sqlscript,"%s_%s_zp.sql",tilenamein, bandlist);
    fsqlout=fopen(sqlscript, "w");
    fprintf(fsqlout,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    //fprintf(fsqlout,"SELECT zeropoint.id,zeropoint.image_n,zeropoint.mag_zero,zeropoint.sigma_mag_zero,zeropoint.b,zeropoint.berr ");
    //fprintf(fsqlout,"FROM zeropoint,files WHERE ");
    for(i=1;i<=nimage;i++) {
        fprintf(fsqlout,"SELECT a.id,a.imageid,to_char(a.mag_zero,'99.9999'),to_char(a.SIGMA_MAG_ZERO,'99.9999') ");
        fprintf(fsqlout,"FROM zeropoint a, image b, exposure c WHERE ");
      //fprintf(fsqlout,"files.imageid=zeropoint.image_n and files.photflag=1 and "); /* temporary taken out */
      //fprintf(fsqlout,"zeropoint.imageid=%d and zeropoint.source='PSM' order by zeropoint.id;\n",tileinfo[i].imageid); /* the zeropoint table not yet include source='PSM' */
        fprintf(fsqlout,"b.exposureid=c.id and c.photflag=1 and ",tileinfo[i].imageid);
        fprintf(fsqlout,"b.id=a.imageid and a.imageid=%d and a.source='PSM' order by a.id;\n",tileinfo[i].imageid);

    }
    //fprintf(fsqlout,"zeropoint.image_n=%d ORDER BY zeropoint.image_n;\n",tileinfo[nimage].imageid);
    fprintf(fsqlout,"exit;\n");
    fclose(fsqlout);

    /* construct sql call */
    sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);
    
    /* find out how many zp returned */  
    sprintf(command, "%s | wc -l",sqlcall);
    pip=Popen(command,"r",__FILE__,__LINE__);
    while (fgets(line,5000,pip)!=NULL)
      sscanf(line,"%d %s",&nzpin,line);
    pclose(pip);
    
    /* exit if no ZP found and ask the user to use -neighbour or -moreneighbour option */
    if(nzpin==0) {
      printf("\n");
      sprintf(event," No ZP found from DB for all images; run coadd_calczp again with -neighbour or -moreneighbour option ");
      reportevt(3,STATUS,5,event);
      exit(0);
    }

    if(!flag_quiet) 
      printf("\n ** Found %d (repeated) zp from zeropoint table\n\n",nzpin);

    /* update number of constrains */
    //if(nzp)
    //nconstrain+=nzp;
    //else
    //nconstrain+=1;
  
    /* set memory allocation */  
    zp=(db_zeropoint *)calloc(nzpin+1,sizeof(db_zeropoint));

    /* initialize arrays */
    for(i=0;i<=nimage;i++) {
      zp_latest[i].mag_zero=25.0; 
      zp_latest[i].sigma_mag_zero=0.0;
    }

    /* input the zp info */
    pip=Popen(sqlcall,"r",__FILE__,__LINE__); i=1; 
    while (fgets(line,5000,pip)!=NULL) {
      sscanf(line,"%d %d %f %f",&zp[i].id,&zp[i].imageid,&zp[i].mag_zero,&zp[i].sigma_mag_zero);
      i++;
    }
    pclose(pip);

    /* loop over to find the latest zp for each images */
    nzp=0;
    for(i=1;i<=nimage;i++) {
      
      flag_zpmax=0;
      for(j=1;j<=nzpin;j++) {
	
	if(tileinfo[i].imageid == zp[j].imageid) {
	  endloop=j;
	  
	  /* first assign the maxzp with first zp */
	  if(!flag_zpmax) {
	    maxzp=zp[j].id;
	    zp_latest[i].mag_zero=zp[j].mag_zero;
	    zp_latest[i].sigma_mag_zero=zp[j].sigma_mag_zero;
	    flag_zpmax=1;
	    nzp++;
	  }
	  else { /* find the latest zp using largest id */
	    if(zp[j].id > maxzp) { 
	      maxzp=zp[j].id;
	      zp_latest[i].mag_zero=zp[j].mag_zero;
	      zp_latest[i].sigma_mag_zero=zp[j].sigma_mag_zero;
	    }
	  } 
	} // if(imageid) loop

	/* quit the j-loop after finding the latest zp */
	if(j > endloop && flag_zpmax) break;
      } // j loop
    } 

    if(!flag_quiet) {
      if(nzp) {
	printf(" ** %d images have zero-points from database\n",nzp);
	printf(" ** The (latest) zero-points for the images:\n");
	for(i=1;i<=nimage;i++) {
	  if(zp_latest[i].mag_zero!=25.0)
	    printf("%d\t%s\t%s\t%2.4f +- %2.4f\n",i,tileinfo[i].runiddesc,tileinfo[i].imagename,zp_latest[i].mag_zero,zp_latest[i].sigma_mag_zero);
	  else 
	    printf("%d\t%s\t%s\tDefault ZP is %2.0f\n",i,tileinfo[i].runiddesc,tileinfo[i].imagename,zp_latest[i].mag_zero);
	}
	printf("\n");
      }
    }
  }

  /********************************************/
  /* find the overlap images for a given tile */
  /********************************************/

  /* memory allocation */
  saveid1=(int *)calloc(nimage*(nimage-1)/2,sizeof(int));
  saveid2=(int *)calloc(nimage*(nimage-1)/2,sizeof(int));
  save_mag_mean = dmatrix(0, nimage, 0, nimage);
  initialize_dmatrix(save_mag_mean, nimage, nimage);
  save_mag_rms = dmatrix(0, nimage, 0, nimage);
  initialize_dmatrix(save_mag_rms, nimage, nimage);
  flag_mag_mean = imatrix(0, nimage, 0, nimage);
  initialize_imatrix(flag_mag_mean, nimage, nimage);

  ra1=(double *)calloc(nimage+1,sizeof(double));
  ra2=(double *)calloc(nimage+1,sizeof(double));
  ra3=(double *)calloc(nimage+1,sizeof(double));
  ra4=(double *)calloc(nimage+1,sizeof(double));
  dec1=(double *)calloc(nimage+1,sizeof(double));
  dec2=(double *)calloc(nimage+1,sizeof(double));
  dec3=(double *)calloc(nimage+1,sizeof(double));
  dec4=(double *)calloc(nimage+1,sizeof(double));

  if(!flag_nostarmatch) {
    
    if(!flag_quiet) {
      printf("\n --------------------------------------------------------------\n");
      printf("\tImage(A)                            \n");
      printf("\tImage(B)                            \n");
      printf("\tNtotal delta(mag)=Image(A)-Image(B) RMS Ngood Nbad Niter.\n");
      printf(" --------------------------------------------------------------\n");	   
    }


    /* calling database to get WCS info for images */
    sprintf(sqlscript,"%s_%s_getredimg.sql",tilenamein, bandlist);
    sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);
    
    fsqlout=fopen(sqlscript,"w");
    fprintf(fsqlout,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF ");       
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    for(i=1;i<=nimage;i++) 
      fprintf(fsqlout,"SELECT ID,EQUINOX,NAXIS1,NAXIS2,CTYPE1,CTYPE2,CRVAL1,CRVAL2,CRPIX1,CRPIX2,CD1_1,CD1_2,CD2_1,CD2_2,PV1_1,PV1_2,PV1_3,PV1_4,PV1_5,PV1_6,PV1_7,PV1_8,PV1_9,PV1_10,PV2_1,PV2_2,PV2_3,PV2_4,PV2_5,PV2_6,PV2_7,PV2_8,PV2_9,PV2_10 FROM image WHERE id=%d;\n",tileinfo[i].imageid);
    
    fprintf(fsqlout,"exit;\n ");  
    fclose(fsqlout);
    
    pip=Popen(sqlcall,"r",__FILE__,__LINE__);
    for(i=1;i<=nimage;i++) {
      
      fgets(line,5000,pip);
      sscanf(line,"%d %lg %ld %ld %s %s %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg", 
	     &imageid_temp, &equinox_in,
	     &axes0,&axes1,ctype1,ctype2,&crval1,&crval2,&crpix1,&crpix2,
	     &cd1_1,&cd1_2,&cd2_1,&cd2_2,
	     &pv1_1,&pv1_2,&pv1_3,&pv1_4,&pv1_5,&pv1_6,&pv1_7,&pv1_8,&pv1_9,&pv1_10,
	     &pv2_1,&pv2_2,&pv2_3,&pv2_4,&pv2_5,&pv2_6,&pv2_7,&pv2_8,&pv2_9,&pv2_10);
      

      if(imageid_temp == tileinfo[i].imageid) {  /* check imageid */

	sprintf(ctype[0],"%s",ctype1);
	sprintf(ctype[1],"%s",ctype2);

	naxis=2; /* hardwired at the moment */

	naxisn[0]=axes0;
	naxisn[1]=axes1;

	crval[0]=crval1;
	crval[1]=crval2;

	crpix[0]=crpix1;
	crpix[1]=crpix2;

	cd1[0]=cd1_1;
	cd1[1]=cd1_2;
	cd2[0]=cd2_1;
	cd2[1]=cd2_2;

	cdelt[0] = sqrt((cd1[0]*cd1[0]) + (cd1[1]*cd1[1]));
	cdelt[1] = sqrt((cd2[0]*cd2[0]) + (cd2[1]*cd2[1]));

	/* create a wcs structure to put the data in */
	wcsin = create_wcs(ctype,crval,crpix,cdelt,naxisn,naxis);
	
	/* fill in the info */

	obsdate_in = 0.0; /* hardwired at the moment */
	wcsin->obsdate = obsdate_in;
	wcsin->equinox = equinox_in;

	pv1_0=pv2_0=0.0; /* no entry in DB */
	
	/* PV Distortion params */
	wcsin->projp[0] = pv1_0;
	wcsin->projp[1] = pv1_1;
	wcsin->projp[2] = pv1_2;
	wcsin->projp[3] = pv1_3;
	wcsin->projp[4] = pv1_4;
	wcsin->projp[5] = pv1_5;
	wcsin->projp[6] = pv1_6;
	wcsin->projp[7] = pv1_7;
	wcsin->projp[8] = pv1_8;
	wcsin->projp[9] = pv1_9;
	wcsin->projp[10] = pv1_10;
 
	wcsin->projp[100] = pv2_0;
	wcsin->projp[101] = pv2_1;
	wcsin->projp[102] = pv2_2;
	wcsin->projp[103] = pv2_3;
	wcsin->projp[104] = pv2_4;
	wcsin->projp[105] = pv2_5;
	wcsin->projp[106] = pv2_6;
	wcsin->projp[107] = pv2_7;
	wcsin->projp[108] = pv2_8;
	wcsin->projp[109] = pv2_9;
	wcsin->projp[110] = pv2_10;

	wcsin->epoch = 2000.0; /* hardwired at the moment */
	 
	wcsin->cd[0] = cd1[0];
	wcsin->cd[1] = cd1[1];
	wcsin->cd[2] = cd2[0];
	wcsin->cd[3] = cd2[1];
	wcsin->radecsys = RDSYS_FK5;
	
	init_wcs(wcsin);
	
	sprintf(wcsin->cunit[0],"%s","deg");
	sprintf(wcsin->cunit[1],"%s","deg");
	
	/* getting the four courners here */
	rawpos[0]=0.0;
	rawpos[1]=0.0;
	raw_to_wcs(wcsin,rawpos,wcspos);
	ra1[i]=wcspos[0];
	dec1[i]=wcspos[1];

	rawpos[0]=(double)axes0;
	rawpos[1]=0.0;
	raw_to_wcs(wcsin,rawpos,wcspos);
	ra2[i]=wcspos[0];
	dec2[i]=wcspos[1];

	rawpos[0]=0.0;
	rawpos[1]=(double)axes1;
	raw_to_wcs(wcsin,rawpos,wcspos);
	ra3[i]=wcspos[0];
	dec3[i]=wcspos[1];

	rawpos[0]=(double)axes0;
	rawpos[1]=(double)axes1;
	raw_to_wcs(wcsin,rawpos,wcspos);
	ra4[i]=wcspos[0];
	dec4[i]=wcspos[1];

	//printf("%d:\t Corner1- %2.7f %2.7f Corner2- %2.7f %2.7f Corner3- %2.7f %2.7f Corner4- %2.7f %2.7f\n",
	//tileinfo[i].imageid,ra1[i],dec1[i],ra2[i],dec2[i],ra3[i],dec3[i],ra4[i],dec4[i]);
      }
      else printf(" ** %s error: image with imageid = %d does not have WCS data in Files table\n",argv[0],tileinfo[i].imageid);
    }
    pclose(pip);
  

    /* setup sql script */
    sprintf(sqlscript,"%s_%s_matchstar.sql",tilenamein, bandlist);

    sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);
  
    /* loops over the images to find the overlap images and construct sql call */
    count=0;
    for(i=1;i<=nimage-1;i++) { 

      maxra=ra1[i];
      if(ra2[i]>maxra) maxra=ra2[i];
      if(ra3[i]>maxra) maxra=ra3[i];
      if(ra4[i]>maxra) maxra=ra4[i];

      minra=ra1[i];
      if(ra2[i]<minra) minra=ra2[i];
      if(ra3[i]<minra) minra=ra3[i];
      if(ra4[i]<minra) minra=ra4[i];
      
      maxdec=dec1[i];
      if(dec2[i]>maxdec) maxdec=dec2[i];
      if(dec3[i]>maxdec) maxdec=dec3[i];
      if(dec4[i]>maxdec) maxdec=dec4[i];

      mindec=dec1[i];
      if(dec2[i]<mindec) mindec=dec2[i];
      if(dec3[i]<mindec) mindec=dec3[i];
      if(dec4[i]<mindec) mindec=dec4[i];
    
      for(j=i+1;j<=nimage;j++) {
	flag=0;

	/* compare 4 corners */
	if (ra1[j]>=minra && ra1[j]<=maxra && dec1[j]>=mindec && dec1[j]<=maxdec) flag=1;
	if (ra2[j]>=minra && ra2[j]<=maxra && dec2[j]>=mindec && dec2[j]<=maxdec) flag=2;
	if (ra3[j]>=minra && ra3[j]<=maxra && dec3[j]>=mindec && dec3[j]<=maxdec) flag=3;
	if (ra4[j]>=minra && ra4[j]<=maxra && dec4[j]>=mindec && dec4[j]<=maxdec) flag=4;
	
	/* make sure the id are not the same */
	if(tileinfo[i].imageid==tileinfo[j].imageid) flag=0;

	
	/* match two images */
	if(flag) {
	  
	  /* save the id for matched images */
	  saveid1[count]=tileinfo[i].id;
	  saveid2[count]=tileinfo[j].id;

	  /* memory allocation and initialize for data arrays */
	  datain=(double *)calloc(MAX+1,sizeof(double));
	  errin=(double *)calloc(MAX+1,sizeof(double));
	  
	  initialize_dvector(datain,MAX);
	  initialize_dvector(errin,MAX);
	  
	  /* sql script for Dora's stored procedure */
	  fsqlout=fopen(sqlscript,"w");
	  fprintf(fsqlout,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF ");       
	  fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");

	  sprintf(command," from table(fMatchImages(%d, %d, %2.3f, %2.3f, %d, %2.4f, \'%s\', %2.3f));\n", 
	  tileinfo[i].imageid,tileinfo[j].imageid,class_star_lo, class_star_hi, flag_se, radius, magerr_input, magerrin);
	  fprintf(fsqlout,"SELECT %s,%s,%s,%s %s", mag1,magerr1,mag2,magerr2,command);
	  fprintf(fsqlout,"exit;\n ");  
	  fclose(fsqlout); 

	  /* database call to get matched stars */

	  /*	  sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S pipeline/dc01user@desdb/prdes <%s",sqlscript); */
	  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);
	  pip=Popen(sqlcall, "r",__FILE__,__LINE__);
	  newcountstar=0;
	  while (fgets(line,5000,pip)!=NULL) {
	    sscanf(line,"%f %f %f %f",&magout1,&magerrout1,&magout2,&magerrout2); 


	    flag_deltamag=0;
	    deltamag=(magout1-zp_latest[i].mag_zero)-(magout2-zp_latest[j].mag_zero);
	    //deltamag=(magout1)-(magout2);
	    
	  	    
	    /* checking various conditions */
	    if(fabs(deltamag) < maxmagdiff) flag_deltamag++;
	    else flag_deltamag=0;

	    if(flag_magcut) {
	      if((magout1 >= mag_low && magout1 <= mag_high) ||  (magout2 >= mag_low && magout2 <= mag_high)) 
		flag_deltamag++;
	      else
		flag_deltamag=0;
	    }
	    else
	      flag_deltamag++;
	    
	    /* once two conditions are satisfied */
	    if(flag_deltamag==2) {
	    
	      datain[newcountstar]=deltamag;
	      errin[newcountstar] =sqrt(Squ(magerrout1)+Squ(magerrout2)+Squ(zp_latest[i].sigma_mag_zero)+Squ(zp_latest[j].sigma_mag_zero)); 
	      newcountstar++;
	      
	    }

	    if(newcountstar == MAX) {
	      printf(" ** number of matched stars exceed %d for imageid %d and %d\n",MAX,tileinfo[i].imageid,tileinfo[j].imageid);
	      exit(0);
	    }
	  }
	  pclose(pip);

	  flag_mean=1;
	  /* calculate the average and rms of mag. difference here */
	  if(flag_Nstarmatch) {
	    if(newcountstar>=Nmin_starmatch)
	      iter_mean(datain,errin,newcountstar,&mag_mean,&magrms_mean,&N_good,&N_bad,&N_iter,flag_weight,flag_iter,flag_Niterate,Nmax_iterate,sigma);
	    else
	      flag_mean=0;
	  }
	  else {
	    if(newcountstar>1)
	      iter_mean(datain,errin,newcountstar,&mag_mean,&magrms_mean,&N_good,&N_bad,&N_iter,flag_weight,flag_iter,flag_Niterate,Nmax_iterate,sigma);
	    else if(newcountstar==1){
	      mag_mean=datain[0];
	      //magrms_mean=0.0;
	      magrms_mean=errin[0];
	      N_good=1;N_bad=N_iter=0;
	    }
	    else {
	      mag_mean=magrms_mean=0.0;
	      N_good=N_bad=N_iter=0;
	      flag_mean=0;
	    }     
	  }

	  /* only apply the RMSCUT if set */
	  if(flag_rmscut) {
	    if(flag_mean) {
	      if(magrms_mean<magrmscut)
		flag_mean=1;
	      else
		flag_mean=0;
	    }
	  }

	  /* special case that none or one good stars left: not using it */
	  if(N_good<=1)
	    flag_mean=0;
	  
	  /* special case that for tiny error */
	  if(magrms_mean<ACCURACY)
	    flag_mean=0;
	  
	  /* save/output the results */
	  if(flag_mean) {
	    
	    save_mag_mean[i][j]=-mag_mean;
	    save_mag_rms[i][j]=magrms_mean;

	    flag_mag_mean[i][j]=1;
	    if(!flag_quiet) {
	      printf("%d\t%s\t%s\n",i,tileinfo[i].runiddesc,tileinfo[i].imagename);
	      printf("%d\t%s\t%s\n",j,tileinfo[j].runiddesc,tileinfo[j].imagename);
	      printf("\t%d\t%2.5f\t%2.9f\t%d\t%d\t%d\n",newcountstar,save_mag_mean[i][j],magrms_mean,N_good,N_bad,N_iter);
	      fflush(stdout);
	    }
	    newcount++;	
	  }
	    
	  count++;
	  free(datain); free(errin);
	}
      }
    }

    if(!flag_quiet) {
      printf("\n ** Initially found %d pairs overlapped images\n",count);
      printf(" ** Found %d pairs of overlapped images with common stars\n",newcount);
    }

    //nconstrain+=newcount;
  }


  /*********************************************/
  /* Obtain SKYBRITE information for each CCDs */
  /*********************************************/

  /* memory allocation and initialization */
  save_skybrite = dmatrix(0, nimage, 0, nimage);
  save_skybriteerr = dmatrix(0, nimage, 0, nimage);
  flag_skybrite = imatrix(0, nimage, 0, nimage);
  skybrite_ccdpair = dmatrix(0, ccdtotal+1, 0,ccdtotal+1);
  skybriteerr_ccdpair = dmatrix(0, ccdtotal+1, 0,ccdtotal+1);

  skybrite=(float *)calloc(nimage+1,sizeof(float)); 

  runidnites=(char **)calloc(nimage+1,sizeof(char *));
  for(j=0;j<=nimage;j++) runidnites[j]=(char *)calloc(150,sizeof(char ));
  nites=(char **)calloc(nimage+1,sizeof(char *));
  for(j=0;j<=nimage;j++) nites[j]=(char *)calloc(150,sizeof(char ));
  runids=(char **)calloc(nimage+1,sizeof(char *));
  for(j=0;j<=nimage;j++) runids[j]=(char *)calloc(150,sizeof(char ));
  bands=(char **)calloc(nimage+1,sizeof(char *));
  for(j=0;j<=nimage;j++) bands[j]=(char *)calloc(10,sizeof(char ));

  initialize_dmatrix(save_skybrite, nimage, nimage);
  initialize_vector(skybrite,nimage);
  initialize_imatrix(flag_skybrite, nimage, nimage);

  if(!flag_noskybrite) {

    /* find out the distinct runid_nites */
    Nrunidnite=1; 
    format_runid(tileinfo[1].runiddesc,runidout);
    sprintf(runidnites[1],"%s",runidout);

    for(i=2;i<=nimage;i++) {
      flag=0;
      for(j=1;j<=Nrunidnite;j++) {    
	format_runid(tileinfo[i].runiddesc,runidout);
	if(!strcmp(runidout,runidnites[j])) 
	  flag=1;
      }
      if(!flag) {
	Nrunidnite++;
	format_runid(tileinfo[i].runiddesc,runidout);
	sprintf(runidnites[Nrunidnite],"%s",runidout);
      }
    }
    
    if(!flag_quiet) {
      printf("\n ** Found %d distinct runid_nites: ",Nrunidnite);
      for(i=1;i<=Nrunidnite;i++) printf("%s ",runidnites[i]);
      printf("\n");
    }

    /* find out the distinct runid */
    Nrunid=1; 
    sprintf(runids[1],"%s",tilerunid[1]);
    
    for(i=2;i<=nimage;i++) {
      flag=0;
      for(j=1;j<=Nrunid;j++) {    
	if(!strcmp(tilerunid[i],runids[j])) 
	  flag=1;
      }
      if(!flag) {
	Nrunid++;
	sprintf(runids[Nrunid],"%s",tilerunid[i]);
      }
    }
    
    //if(!flag_quiet) {
    //printf("\n ** Found %d distinct runid: ",Nrunid);
    //for(i=1;i<=Nrunid;i++) printf("%s ",runids[i]);
    //printf("\n");
    //}

    /* find out the distinct nites */
    Nnite=1; 
    sprintf(nites[1],"%s",tileinfo[1].nite);
    
    for(i=2;i<=nimage;i++) {
      flag=0;
      for(j=1;j<=Nnite;j++) {    
	if(!strcmp(tileinfo[i].nite,nites[j])) 
	  flag=1;
      }
      if(!flag) {
	Nnite++;
	sprintf(nites[Nnite],"%s",tileinfo[i].nite);
      }
    }
    
    //if(!flag_quiet) {
    //printf("\n ** Found %d distinct nites: ",Nnite);
    //for(i=1;i<=Nnite;i++) printf("%s ",nites[i]);
    //printf("\n");
    //}

    /* find out the distinct bands */
    Nband=1; 
    sprintf(bands[1],"%s",tileinfo[1].band);
    
    for(i=2;i<=nimage;i++) {
      flag=0;
      for(j=1;j<=Nband;j++) {    
	if(!strcmp(tileinfo[i].band,bands[j])) 
	  flag=1;
      }
      if(!flag) {
	Nband++;
	sprintf(bands[Nband],"%s",tileinfo[i].band);
      }
    }

    //if(!flag_quiet) {
    //printf("\n ** Found %d distinct bands: ",Nband);
    //for(i=1;i<=Nband;i++) printf("%s ",bands[i]);
    //printf("\n");
    //}

    /* cycle through the distinct nites to get SKYBRITE values */
    sprintf(sqlscript,"%s_%s_skybrite.sql",tilenamein, bandlist);
    sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);
    
    /* database query for all exposures for a given nite/runid/band */
    fsqlout=fopen(sqlscript,"w");
    fprintf(fsqlout,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF ");
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
    for(i=1;i<=nimage;i++)
      /* only include the photometric nites */
     if (!strcmp(project,"BCS") || !strcmp(project,"SCS")) {
        fprintf(fsqlout,"SELECT image.id,image.SKYBRITE from image,exposure where image.id=%d and exposure.photflag=1 and image.exposureid=exposure.id;\n",tileinfo[i].imageid);
      }
      else {
        fprintf(fsqlout,"SELECT image.id,image.SKYBRITE from image,exposure where image.id=%d and exposure.photflag=1 and image.exposureid=exposure.id;\n",tileinfo[i].imageid);
      }
    fprintf(fsqlout,"exit;\n ");
    fclose(fsqlout);
    
    if(!flag_quiet) {
      printf("\n ** Skybrite values from DB for images from photometric nites:\n");
    
      pip=Popen(sqlcall,"r",__FILE__,__LINE__);
      while(fgets(line,5000,pip)!=NULL) {
	sscanf(line,"%d %lg",&imageid_temp,&skybrite_temp);
	for(i=1;i<=nimage;i++)
	  if(imageid_temp == tileinfo[i].imageid) {  /* check imageid */
	    printf("%d\t%s\t%s\t%2.4f(ADU)\n",i,tileinfo[i].runiddesc,tileinfo[i].imagename,skybrite_temp);
	    //break;
	  }	
      }
      pclose(pip);
    
      printf("\n");
    }

    Nrun=Nrunidnite;

    /* database query for all exposures for all nite/runid/band */
    for(i=1;i<=Nrun;i++) {

      sprintf(sqlscript,"%s_%s_%s.sql",tilenamein, bandlist,runidnites[i]);
  
      sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);

      /* count how many exposures in a given nite/runid/band */
      fsqlout=fopen(sqlscript,"w");
      fprintf(fsqlout,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF ");       
      fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
      if (!strcmp(project,"BCS") || !strcmp(project,"SCS")) {
        fprintf(fsqlout,"SELECT distinct(exposure.exposurename) from image,exposure where image.imagetype='red' ");
        fprintf(fsqlout," AND image.run like '%%%s%%' ",nites[i]); 
        for(j=1;j<=Nband;j++) 
	  fprintf(fsqlout," AND image.band='%s' ",bands[j]);
        /* only include the photometric nites */
        fprintf(fsqlout," AND image.exposureid=exposure.id ");
        fprintf(fsqlout," AND exposure.photflag=1;\n");
      }
      else {
        fprintf(fsqlout,"SELECT distinct(exposure.exposurename) from image,exposure where image.imagetype='red' ");
        //fprintf(fsqlout," AND image.run like '%s%%' ",runidnites[i]); 
        fprintf(fsqlout," AND image.run like '%%%s%%' ",nites[i]); 
        for(j=1;j<=Nband;j++) 
	  fprintf(fsqlout," AND image.band='%s' ",bands[j]);
        /* only include the photometric nites */
        fprintf(fsqlout," AND image.exposureid=exposure.id ");
        fprintf(fsqlout," AND exposure.photflag=1;\n");
      }
      fprintf(fsqlout,"exit;\n ");  
      fclose(fsqlout); 
      
      Nexposure=0;
      pip=Popen(sqlcall,"r",__FILE__,__LINE__); 
      while (fgets(line,5000,pip)!=NULL) {
	sscanf(line,"%s",temp);
	Nexposure++;
      }
      pclose(pip);
      
      printf("For %s, Nexp = %d\n", runidnites[i], Nexposure);

      if(Nexposure) {
		skybrite_bookeep = dmatrix(0,ccdtotal, 0,Nexposure); 
	initialize_dmatrix(skybrite_bookeep,ccdtotal,Nexposure);
	/* query to get the info */
	sprintf(sqlscript,"%s_%s_skybrite_%s.sql",tilenamein, bandlist,runidnites[i]);  
     
	sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);

	fsqlout=fopen(sqlscript,"w");
	fprintf(fsqlout,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF ");       
	fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
        if (!strcmp(project,"BCS") || !strcmp(project,"SCS")) {
	  fprintf(fsqlout,"SELECT exposure.exposurename,image.ccd,image.band,to_char(image.SKYBRITE,'9999.9999') from image,exposure where image.imagetype='red' ");
	  fprintf(fsqlout," AND image.run like '%%%s%%' ",nites[i]); 
	  for(j=1;j<=Nband;j++) 
	    fprintf(fsqlout," AND image.band='%s' ",bands[j]);
	  /* only include the photometric nites */
	  fprintf(fsqlout," AND image.exposureid=exposure.id");
	  fprintf(fsqlout," AND exposure.photflag=1 order by exposure.exposurename;\n");
        }
	else {
	  fprintf(fsqlout,"SELECT exposure.exposurename,image.ccd,image.band,to_char(image.SKYBRITE,'9999.9999') from image,exposure where image.imagetype='red' ");
	  fprintf(fsqlout," AND image.run like '%%%s%%' ",nites[i]); 
	  //fprintf(fsqlout," AND image.run like '%s%%' ",runidnites[i]);
	  for(j=1;j<=Nband;j++) 
	    fprintf(fsqlout," AND image.band='%s' ",bands[j]);
	  /* only include the photometric nites */
	  fprintf(fsqlout," AND image.exposureid=exposure.id ");
	  fprintf(fsqlout," AND exposure.photflag=1 order by exposure.exposurename;\n");
	}
	fprintf(fsqlout,"exit\n ");  
	fclose(fsqlout); 
	/* worry about band later */
	pip=Popen(sqlcall,"r",__FILE__,__LINE__);
	fgets(line,5000,pip); 
	sscanf(line,"%s %d %s %lg",prev,&ccdnum,bandtemp,&skybrite_temp);
	pclose(pip);
	pip=Popen(sqlcall,"r",__FILE__,__LINE__);
	j=1;
	while (fgets(line,5000,pip)!=NULL) {
	  sscanf(line,"%s %d %s %lg",temp,&ccdnum,bandtemp,&skybrite_temp);
	  if(!strcmp(temp,prev))
	    skybrite_bookeep[ccdnum][j]=skybrite_temp;
	  else {
	    j++;
	    sprintf(prev,"%s",temp);
	    skybrite_bookeep[ccdnum][j]=skybrite_temp;
	  }
	}
	pclose(pip);
	/* check if SKYBRITE=0 or not for given runid_nite */
	flag=0;
	for(j=1;j<=ccdtotal;j++) 
	  for(k=1;k<=Nexposure;k++) 
	    if(skybrite_bookeep[j][k]==0) flag=1;
       
	if(flag) {
	  if(!flag_quiet) printf(" ** No SKYBRITE ratio available for %s (SKYBRITE value=0)\n",runidnites[i]);
	}
	else {
	  /* get the median of ccdratio */      
	  calc_ccdratio(skybrite_bookeep,skybrite_ccdpair,skybriteerr_ccdpair,ccdtotal,Nexposure);
	  
	  /* print out result */
	  if(!flag_quiet) {
	    printf("\n ** SKYBRITE ratio for %s from %d exposures\n",runidnites[i],Nexposure);
	    
	    printf("--------------------------------------------------------------\n");
	    printf("\tImage(A)                            \n");
	    printf("\tImage(B)                            \n");
	    printf("\tdelta_mag=Image(A)-Image(B) \n");
	    printf(" --------------------------------------------------------------\n");
	  }
      
	  /* assign the ratio to images (worry the band match later) */  
	  track=0;
	  for(j=1;j<=nimage-1;j++) {
	    for(k=(j+1);k<=nimage;k++) {
	          
	      format_runid(tileinfo[j].runiddesc,runidout1);
	      sprintf(temp,"%s/red/%s",runidout1,tileinfo[j].imagename);
	      len=strlen(temp);
	      for (m=len;m>0;m--) 
		if (!strncmp(&(temp[m]),"/",1)) {
		  temp[m]=0;
		  break;
		}
	      sscanf(temp,"%s",image1);
	      
	      format_runid(tileinfo[k].runiddesc,runidout2);
	      sprintf(temp,"%s/red/%s",runidout2,tileinfo[k].imagename);
	      len=strlen(temp);
	      for (m=len;m>0;m--) 
		if (!strncmp(&(temp[m]),"/",1)) {
		  temp[m]=0;
		  break;
		}
	      sscanf(temp,"%s",image2);
	      
	      //sprintf(image1,"%s",image_ccd[j]);
	      //sprintf(image2,"%s",image_ccd[k]);

	      /* check if from same exposure and same runid_nite */ 	    
	      
	      if(!strcmp(image1,image2) && !strcmp(runidnites[i],runidout1) && !strcmp(runidnites[i],runidout2)  && strcmp(image_ccd[j],image_ccd[k])) {
	 
		save_skybrite[j][k]=skybrite_ccdpair[tileinfo[j].ccdnum][tileinfo[k].ccdnum];
		save_skybriteerr[j][k]=((2.5*LGE)/save_skybrite[j][k])*skybriteerr_ccdpair[tileinfo[j].ccdnum][tileinfo[k].ccdnum];
		save_skybrite[j][k]=2.5*log10(save_skybrite[j][k]);


		/* to avoid the error term to be too small */
		if(save_skybriteerr[j][k]<0.01)
		  save_skybriteerr[j][k]=0.01;

		flag_skybrite[j][k]=1;
		
		if(!flag_quiet) {
		  printf("For exposure %s\n",image1);
		  printf("%d\t%s\t%s\n",j,tileinfo[j].runiddesc,tileinfo[j].imagename);
		  printf("%d\t%s\t%s\n",k,tileinfo[k].runiddesc,tileinfo[k].imagename);
		  if(save_skybriteerr[j][k]==0.01)
		    printf("\t %2.4f +- %2.4f (pre-set error)  Fluxratio=%2.6f\n",save_skybrite[j][k],save_skybriteerr[j][k],skybrite_ccdpair[tileinfo[j].ccdnum][tileinfo[k].ccdnum]);
		  else
		    printf("\t %2.4f +- %2.4f   Fluxratio=%2.6f\n",save_skybrite[j][k],save_skybriteerr[j][k],skybrite_ccdpair[tileinfo[j].ccdnum][tileinfo[k].ccdnum]);
		}
		  
		track++;
	      
	      } 
	    } // k-loop
	  } //j-loop
	  
	  if(!track) {
	    if(!flag_quiet) 
	      printf(" ** %s warning: images are not from the same exposures for %s\n",argv[0],runidnites[i]);	    
	  }
	}
	if(!flag_quiet) printf("\n");
    
	free_dmatrix(skybrite_bookeep,0,ccdtotal,0,Nexposure);
      } // if(Nexposure)-loop
    }
  }


  /**************************************************************/
  /* check if the remap images are from the same reduced image  */
  /**************************************************************/

  flag_sameredimg = imatrix(0, nimage, 0, nimage);
  initialize_imatrix(flag_sameredimg, nimage, nimage);

  //if(flag_neighbour) {

  //nsameredimg=0;
  //for(i=1;i<nimage;i++) {
  //for(j=i+1;j<=nimage;j++) {

  //if(!strcmp(image_ccd[i],image_ccd[j])) {
  //nsameredimg++;
  //flag_sameredimg[i][j]=1;

  //}
  //  }
  //}
  //}
  //else
  nsameredimg=0;

  /*************************/
  /* construct the matrix  */
  /*************************/

  /* cases when no ZP info available, assign ZP for overlapped images as 25.0 */ 
  flag_checkzp=(int *)calloc(nimage+1,sizeof(int ));
  for(i=1;i<=nimage;i++)
    flag_checkzp[i]=0;

  if(flag_nophotozp==1 || nzp==0) {
    for(i=1;i<=nimage;i++) 
      for(j=1;j<=nimage;j++) {
	
	zp_latest[i].mag_zero=0.0; 	
	zp_latest[i].sigma_mag_zero=1.0;

	if(flag_mag_mean[i][j] && !flag_checkzp[i]) {
	  nzp++; 
	  zp_latest[i].mag_zero=25.0; 	
	  zp_latest[i].sigma_mag_zero=1.0;
	  flag_assginzp=1;
	  flag_checkzp[j]=1;
	  if(!flag_quiet)
	    printf(" ** Assing ZP=25.0 to image %d\t%s %s\n",i,tileinfo[i].runiddesc,tileinfo[i].imagename);
	  break;
	}
      }
  }

  /* get the nrow */
  for(i=1;i<=nimage;i++) {
    for(j=1;j<=nimage;j++) { 
      if(flag_mag_mean[i][j]) 
	nstarmatch++;
      if(flag_skybrite[i][j]) 
	nskybrite++;
    }
  }

  nrow=nzp+nstarmatch+nskybrite+nsameredimg;
  ncol=nimage;

  if(!flag_quiet) 
    printf(" ** Number of constrains: N(zp) = %d\t N(matched_stars) = %d\t N(skybrite) = %d\n",nzp,nstarmatch,nskybrite);


  /* memory allocation and initialization for matrix/vector eqn */
  A = matrix(0, nrow, 0, ncol);
  initialize_matrix(A, nrow, ncol);  
  
  b=(float *)calloc(nrow+1,sizeof(float));
  initialize_vector(b, nrow);

  berr=(float *)calloc(nrow+1,sizeof(float));
  for(i=0;i<=nrow;i++) berr[i]=1.0;

  x=(float *)calloc(ncol+1,sizeof(float));
  initialize_vector(x, ncol);
    
  xerr=(float *)calloc(ncol+1,sizeof(float));
  initialize_vector(xerr, ncol);

  if(!flag_quickcoadd) {

  /* !! include the weigh here, alter to Glazebrook expression? !! */
  /* assign the A matrix and b vector with overlap & SKYBRITE information */
    
    /* for the ZP info */
    if(!flag_assginzp) {
      jstart=1;
      for(i=1;i<=nzp;i++)
	for(j=jstart;j<=nimage;j++) 
	  if(zp_latest[j].mag_zero!=25.0) {
	    //if(zp_latest[j].mag_zero!=0.0 || zp_latest[j].mag_zero!=25.0) {
	    if(flag_weightfit) {
	      weight_temp=zp_latest[j].sigma_mag_zero;
	      //weight_temp=1.0;
	      A[i][j]=1.0/weight_temp;
	      b[i]=zp_latest[j].mag_zero/weight_temp;
	    }
	    else {
	      A[i][j]=1.0;
	      b[i]=zp_latest[j].mag_zero;
	    }
	    jstart=j+1;
	    break;
	  }
    }
    else {
      jstart=1;
      for(i=1;i<=nzp;i++)
	for(j=jstart;j<=nimage;j++) 
	  if(zp_latest[j].mag_zero==25.0) {
	    if(flag_weightfit) {
	      weight_temp=zp_latest[j].sigma_mag_zero;
	      //weight_temp=1.0;
	      A[i][j]=1.0/weight_temp;
	      b[i]=zp_latest[j].mag_zero/weight_temp;
	    }
	    else {
	      A[i][j]=1.0;
	      b[i]=zp_latest[j].mag_zero;
	    }
	    jstart=j+1;
	    break;
	  }
    }

    /* for the matched star info */
    if(!flag_nostarmatch) {
      k=1;
      for(i=1;i<=nimage;i++) {
	for(j=1;j<=nimage;j++) {
	
	  if(flag_mag_mean[i][j]) {
	    
	    if(flag_weightfit) {
	      weight_temp=save_mag_rms[i][j];
	      //weight_temp=1.0; 
	      A[k+nzp][i]+=1.0/weight_temp;
	      A[k+nzp][j]-=1.0/weight_temp;
	      b[k+nzp] += save_mag_mean[i][j]/weight_temp;
	    }
	    else {
	      A[k+nzp][i]+=1.0;
	      A[k+nzp][j]-=1.0;
	      b[k+nzp] += save_mag_mean[i][j];
	    }
	    
	    k++;
	  }
	}
      }
    }

    /* for the skybrite info */
    if(!flag_noskybrite) {
      k=1;
      for(i=1;i<=nimage;i++) {
	for(j=1;j<=nimage;j++) {
	  
	  if(flag_skybrite[i][j]) {
	    
	    if(flag_weightfit) {
	      //weight_temp=1.0; 
	      weight_temp=save_skybriteerr[i][j];
	      A[k+nzp+nstarmatch][i]+=1.0/weight_temp;
	      A[k+nzp+nstarmatch][j]-=1.0/weight_temp;
	      b[k+nzp+nstarmatch] +=save_skybrite[i][j]/weight_temp;
	    }
	    else {
	      A[k+nzp+nstarmatch][i]+=1.0;
	      A[k+nzp+nstarmatch][j]-=1.0;
	      b[k+nzp+nstarmatch] +=save_skybrite[i][j];
	    }

	    k++;
	  }
	}
      }
    }
    

    //print_matrix(A,nrow,ncol);
    //print_vector(b,nrow);
        
    /*************************************************/
    /* solving matrix equation and print out results */
    /*************************************************/
    
    /* SVD solver here */

    svd_fit(A,b,berr,nrow,ncol,x,xerr);

    
    /* take the average of the remapped images from the same reduced image */

    /* output result */

    if(flag_outfile)
      fidout=fopen(outfile,"w");

    //sprintf(zpout,"%s_%s_ZP.dat",tilenamein,bandlist);
    //fzpcompare=fopen(zpout,"w");

    /* insert results to the zeropoint table */
    /* first point to READWRITE database */
    select_dblogin(dblogin,DB_READWRITE);

    sprintf(sqlscript,"%s_%s_zpinsert.sql",tilenamein, bandlist);      
    sprintf(sqlcall,"${ORACLE_HOME}/bin/sqlplus -S %s < %s",dblogin,sqlscript);
    
    fsqlout=fopen(sqlscript,"w");
    fprintf(fsqlout,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF ");       
    fprintf(fsqlout,"HEAD OFF TRIMS ON LINESIZE 1000;\n");

    if(!flag_quiet) {
      if(flag_nostarmatch && flag_noskybrite) {
	printf("\n ** Input ZPs:\n");
	for(i=1;i<=nimage;i++) 
	  printf("%d\t%s\t%s\t%2.4f\n",i,tileinfo[i].runiddesc,tileinfo[i].imagename,x[i]);
      }
      else {
	printf("\n ** Results from SVD:\n");
	//fprintf(fzpcompare,"# i\tZP_cal\tZP_dir\tZP_cal-ZP_dir\tError\tImage\n");
	for(i=1;i<=nimage;i++) {
	  
	  printf("%d\t%s %s\t%2.4f +- %2.4f\tZP_dir=%2.4f\tDiff=%2.4f\n",i,tileinfo[i].runiddesc,
		 tileinfo[i].imagename,x[i],sqrt(xerr[i]),zp_latest[i].mag_zero,x[i]-zp_latest[i].mag_zero);

	  flag_badzp=0;
	  
	  /* filter out the bad ZP */
	  if(x[i]==0.0) flag_badzp=1;
	  if(x[i]==25.0) flag_badzp=1;
	  if(x[i]>ZPHI) flag_badzp=1;
	  if(x[i]<ZPLO) flag_badzp=1;

	  if(!flag_badzp) {
	    if(flag_outfile)
	      fprintf(fidout,"%d\t%2.4f\t%2.4f\n",tileinfo[i].imageid,x[i],sqrt(xerr[i]));

	    fprintf(fsqlout,"INSERT INTO zeropoint (ID,IMAGEID,MAG_ZERO,SIGMA_MAG_ZERO,SOURCE,INSERT_DATE) \n");
	    fprintf(fsqlout,"VALUES ( zeropoint_seq.nextval, %d, %.6f, %.6f,'%s',sysdate);\n",
		    tileinfo[i].imageid,x[i],sqrt(xerr[i]),zp_src);
	  }
	  else {
	    sprintf(event,"ZP for image %s (%d) not ingested, because ZP = %2.4f\n",tileinfo[i].imagename,i,x[i]);
	    reportevt(3,STATUS,4,event);
	  }

	  //if(zp_latest[i].mag_zero!=25.0) 
	  //fprintf(fzpcompare,"%d\t%2.4f\t%2.4f\t%2.4f\t%2.4f\t%s\t%s\n",i,x[i],zp_latest[i].mag_zero,x[i]-zp_latest[i].mag_zero,sqrt(xerr[i]+Squ(zp_latest[i].sigma_mag_zero)),tileinfo[i].runiddesc,tileinfo[i].imagename);
	}
      }
    }
    //fclose(fzpcompare);
  } // for flag_quickcoadd
  else {
    mag_base=25.0;
    for(i=1;i<=nimage;i++)
      x[i]=25.0;
  }
  fprintf(fsqlout,"exit;");
  fclose(fsqlout);
  
  if(flag_outfile)
    fclose(fidout);

  /* ************************************ */
  /* insert the result to zeropoint table */
  /* ************************************ */
  if(!flag_debug) {
    printf(" ** Inserting SVD solution to ZEROPOINT table...\n");
    system(sqlcall);
    printf(" ** Done insert\n");
  }

  /* clean up */
  /*system("rm *sql ");*/
 
  /********************/
  /* end of the code  */
  /********************/

  /* free memory allocation */
  free_imatrix(flag_mag_mean, 0, nimage, 0, nimage);
  free_imatrix(flag_skybrite, 0, nimage, 0, nimage);
  free_dmatrix(save_mag_mean, 0, nimage, 0, nimage);
  free_dmatrix(save_mag_rms, 0, nimage, 0, nimage);
  free_dmatrix(save_skybrite, 0, nimage, 0, nimage);
  free_dmatrix(save_skybriteerr, 0, nimage, 0, nimage);
  free_dmatrix(skybrite_ccdpair, 0, ccdtotal+1, 0, ccdtotal+1);
  free_dmatrix(skybriteerr_ccdpair, 0, ccdtotal+1, 0, ccdtotal+1);
  free_imatrix(flag_sameredimg, 0, nimage+1, 0, nimage+1);
  free_matrix(A, 0, nrow, 0, ncol);

  /* add the if statements for those arrays that really used? */
  free(tileinfo); free(tileinfo_in); free(zp); free(zp_latest);
  free(saveid1); free(saveid2); free(b); 
  free(berr); free(x); free(xerr); free(skybrite); 
  free(runidnites); free(bands); free(runids); 
  free(nites); free(tilerunid); free(keepimg);
  free(flag_checkzp);
  free(image_ccd_in); free(image_ccd);
  free(ra1); free(ra2); free(ra3); free(ra4);
  free(dec1); free(dec2); free(dec3); free(dec4);
  free(tilelist);
  
  lsttime=time (NULL);
  if(!flag_quiet) {
    //printf(" ** Using %d out of %d available remap images\n",count-1,nimage); /* the count is wrong */
    printf("\n ** Done on %s \n",asctime(localtime (&lsttime)));
  }
  return (0);
}

void print_dmatrix(double **matrix, int nrow, int ncol)
{
  int i,j;
  for(i=1; i<=nrow; i++){
      for(j=1; j<=ncol; j++)
	printf("%2.3f\t", matrix[i][j]);
      printf("\n");
    }
  printf("\n");
}

void print_imatrix(int **matrix, int nrow, int ncol)
{
  int i,j;
  for(i=1; i<=nrow; i++){
      for(j=1; j<=ncol; j++)
	printf("%d\t", matrix[i][j]);
      printf("\n");
    }
  printf("\n");
}

void print_matrix(float **matrix, int nrow, int ncol)
{
  int i,j;
  for(i=1; i<=nrow; i++){
      for(j=1; j<=ncol; j++)
	printf("%2.2f\t", matrix[i][j]);
      printf("\n");
    }
  printf("\n");
}

void print_vector(float *vector, int n)
{
  int i;
  for(i=1; i<=n; i++)
	printf("%2.5f\n", vector[i]);
  printf("\n");
}

void initialize_matrix(float **matrix, int nrow, int ncol)
{
  int i,j;
  for(i=0; i<=nrow; i++)
      for(j=0; j<=ncol; j++)
	matrix[i][j] = 0.0;
}

void initialize_dmatrix(double **matrix, int nrow, int ncol)
{
  int i,j;
  for(i=0; i<=nrow; i++)
      for(j=0; j<=ncol; j++)
	matrix[i][j] = 0.0;
}

void initialize_imatrix(int **matrix, int nrow, int ncol)
{
  int i,j;
  for(i=0; i<=nrow; i++)
      for(j=0; j<=ncol; j++)
	matrix[i][j] = 0;
}

void initialize_vector(float *vector, int n)
{
  int i;
  for(i=0; i<=n; i++)
    vector[i] = 0.0;
}

void initialize_dvector(double *vector, int n)
{
  int i;
  for(i=0; i<=n; i++)
    vector[i] = 0.0;
}

double getmean(double data[],double err[],int N,int flag)
{
  int i;
  double sum,mean,sigsq;

  sum=0.0;
  sigsq=0.0;
  for(i=0;i<N;i++) {
      sum+=data[i];
      sigsq+=1.0/(err[i]*err[i]);
  }

  if(flag==0)  /* unweighted average */  
    mean=sum/N;
  else {  /* weighted average */  
    sum = 0.0;
    for(i=0;i<N;i++)
      sum += data[i]/(err[i]*err[i]);
    mean = sum/sigsq;
  }

  return (mean);
}

double getrms(double data[],double err[],int N,int flag)
{
  int i;
  double var,s,ep,mean,rms;

  mean=getmean(data,err,N,flag);

  /* using two-pass formula as given in Numerical Recipes 14.1.8 */
  if(N>1) {
    var=ep=0.0;
    for(i=0;i<N;i++) { 
      s=data[i]-mean;
      ep+=s;
      var+=s*s;
    }
    rms=sqrt((var-ep*ep/N)/(double)(N-1));
  }
  else
    rms=0.0;

  return (rms);
}

void calc_ccdratio(double **skybrite_bookeep, double **skybrite_ccdpair, double **skybriteerr_ccdpair, int ccdtotal, int Nexposure)
{
  float median,medianerr,*vecsort;
  float sum,mean,std;
  unsigned long n;
  int i,j,k;
  void shell();

  n=(unsigned long)Nexposure;
  vecsort=(float *)calloc(Nexposure+1,sizeof(float));

  for(j=1;j<=ccdtotal;j++) {
    for(k=1;k<=ccdtotal;k++) {
      
      /* costruct the vector for ratio_jk=SKY_j/SKY_k (or ratio_jk=SKY_k/SKY_j ?) */
      sum=0.0;
      for(i=1;i<=Nexposure;i++) {
	vecsort[i]=skybrite_bookeep[j][i]/skybrite_bookeep[k][i];
	sum+=vecsort[i];
      }

      /* sort with N.R. subroutine */
      shell(n,vecsort);
    
      /* get median value */
      if(n%2)  median = vecsort[(n+1)/2];
      else median = 0.5 * (vecsort[n/2] + vecsort[(n/2)+1]);

      /* assign to skybrite_ccdpair[j][k] */
      //if(median==1.0)
      //median=0.0;
      //else
      //median=-2.5*log10(median);

      /* get medianerr value */
      mean=sum/(float)Nexposure;
      sum=0.0;
      for(i=1;i<=Nexposure;i++) 
	sum+=Squ(vecsort[i]-mean);
      std=sqrt(sum/(Nexposure-1.0));
      medianerr=1.253*std/sqrt((float)Nexposure);

      /* save results */
      skybrite_ccdpair[j][k]=(double)median;
      skybriteerr_ccdpair[j][k]=(double)medianerr;

    } // k-loop
  } // j-loop

  free(vecsort);
}

void iter_mean(double data[], double err[], int N, double *mean, double *rms, int *N_good, int *N_bad, int *N_iter, int flag, int flag_iter, int flag_Niterate, int Nmax_iterate, double THRESHOLD)
{
  int ii,Nt,Noutlier=0;
  double t_mean,t_rms,new_mean,old_mean;
  double *olddata, *olderr;
  double *newdata, *newerr;
  
  *N_good=N;
  *N_bad=0;
  *N_iter=0;
  
  /* first find the sample average and  rms */
  t_mean=getmean(data,err,N,flag);
  t_rms=getrms(data,err,N,flag);

  /* memory allocation for the newdata and newerr */
  olddata=(double *)calloc(N+1,sizeof(double));
  olderr =(double *)calloc(N+1,sizeof(double));
  newdata=(double *)calloc(N+1,sizeof(double));
  newerr =(double *)calloc(N+1,sizeof(double));

  /* first find if any outliers when doing sigma-clipping */
  for(ii=0;ii<N;ii++) {
    if((fabs(data[ii]-t_mean) > THRESHOLD*t_rms))
      Noutlier++;
  }

  if(Noutlier==0 || flag_iter==0) { /* no outliers or not using sigma-clipping, simply return the results */
    *mean=t_mean;
    *rms=t_rms;
  }
  else { /* remove outliers and begin iterative process */

    /* initiallize the olddata array and parameters */
    for(ii=0;ii<N;ii++) {
      olddata[ii]=data[ii];
      olderr[ii]=err[ii];
    }
    old_mean=t_mean;
    new_mean=0.0;
    Nt = N;
  
    /* iterative procedure until the mean converge */
    while(fabs(old_mean-new_mean) >= ACCURACY) {

      /* get the mean and rms for the old data */
      t_mean=getmean(olddata,olderr,Nt,flag);
      t_rms =getrms(olddata,olderr,Nt,flag);
      old_mean=t_mean;

      /* find the number of outliers and put data in new array without the outlier */
      *N_good=0;
      for(ii=0;ii<Nt;ii++) {
	if((fabs(olddata[ii]-t_mean) > THRESHOLD*t_rms)) 
	  *N_bad+=1;
	else {
	  newdata[*N_good]=olddata[ii];
	  newerr[*N_good]=olderr[ii];
	  *N_good+=1;
	}
      }

      /* update information */
      if(Nt != (*N_good))
	*N_iter+=1;

      Nt = (*N_good);
      t_mean=getmean(newdata,newerr,Nt,flag);
      t_rms =getrms(newdata,newerr,Nt,flag);      
      new_mean = t_mean;

      /* put the newdata array back to olddata array */
      for(ii=0;ii<Nt;ii++) {
	olddata[ii]=newdata[ii];
	olderr[ii]=newerr[ii];
      }
     
      /* quick the loop if set the Nmax_iterate */
      if(flag_Niterate && *N_iter==Nmax_iterate) break;
    }

    if((*N_good)==0 || (*N_bad)==N) {
      t_mean=0.0;
      t_rms=0.0;
    }

    *mean=t_mean;
    *rms=t_rms;
  }

  /* free memory */
  free(olddata);free(olderr);
  free(newdata);free(newerr);
}

void svd_fit(float **A, float *b, float *berr, int nrow, int ncol, float *x, float *xerr)
{
  // need double precision version ?
  int i,j;
  float wmax,wmin,**u,*w,**v,**cvm;

  void svbksb(float **u, float w[], float **v, int m, int n, float b[], float x[]);
  void svdcmp(float **a, int m, int n, float w[], float **v);
  void svdvar(float **v, int ma, float w[], float **cvm);

  u = matrix(0, nrow, 0, ncol);
  v = matrix(0, ncol, 0, ncol);
  cvm = matrix(0, ncol, 0, ncol);
  w=(float *)calloc(ncol+1,sizeof(float));

  for(i=1;i<=nrow;i++)
    for(j=1;j<=ncol;j++)
      u[i][j]=A[i][j];
  
  svdcmp(u,nrow,ncol,w,v);

  wmax=0.0;
  for(j=1;j<=ncol;j++)
    if(w[j]>wmax) wmax=w[j];
  wmin=wmax*1.0e-6;
  for(j=1;j<=ncol;j++)
    if(w[j]<wmin) w[j]=0.0;

  svbksb(u,w,v,nrow,ncol,b,x);
  
  svdvar(v,ncol,w,cvm);

  for(i=1;i<=ncol;i++) 
    for(j=1;j<=ncol;j++) 
      if(i==j) 
	xerr[i]=cvm[i][j];
      
  free_matrix(u, 0, nrow, 0, ncol);
  free_matrix(v, 0, ncol, 0, ncol);
  free_matrix(cvm, 0, ncol, 0, ncol);
  free(w);
}

void format_runid(char *runid_in, char *runid_out)
{
  char temp[1000],runid[1000],nite[1000],ccd[10];
  int j,count=0;

  sprintf(temp,"%s",runid_in);

  for (j=strlen(temp);j>0;j--) {
    if (!strncmp(&(temp[j]),"_",1)) {
      count++;
      temp[j]=32;
    }
  }

  if(count==1) {
    sscanf(temp,"%s %s",runid,nite);
    sprintf(runid_out,"%s_%s",runid,nite);
  }
  else if(count==2) {
    sscanf(temp,"%s %s %s",runid,nite,ccd);
    sprintf(runid_out,"%s_%s",runid,nite);
  }
  else {
    printf("\n  Error: Check runiddesc %s\n",runid_in);
    exit(1);
  }
    
}

void helpmessage(int dummy)
{
  printf("WORKING ON THE HELP MESSAGE\n");
  printf("Required inputs for coadd_fluxscale (the order is not critical):\n");
  
  printf("  -project <project name>\n");
  printf("      Input the project name, such as DES, BCS or SCS\n");

  printf("  -tilename <tile string>\n");
  printf("      Input the name of the tile to be coadded, e.g. BCS0516-5223\n");

  printf("  -band <band>\n");
  printf("      Input the band for the tile to be coadded\n");

  printf("  -detector <detector>\n");
  printf("      Input the detector, which should be either DECam or Mosaic2 for the CTIO Blanco 4m Telescope\n");
 
  printf("\n");
  printf("Optional inputs for coadd_fluxscale:\n");

  printf("  -basedir <basedir>\n");
  printf("      Input the base-directory before runid/... to built the absolute pathname to the remap images\n");
  printf("      On bcs.cosmology.uiuc.edu, it is /Archive/red\n");

  printf("  -binpath <binpath>\n");
  printf("      Setup the bin-path for swarp\n");

  printf("  -etcpath <etcpath>\n");
  printf("      Setup the etc-path for swarp configuration file\n");

  printf("  -nite <nite#1,nite#2,...>\n");
  printf("      Input nites for coadding with string of nite#1,nite#2,... (comma seperated). For single nite, just input nite#1\n");

  printf("  -runid <runid#1,runid#2,...>\n");
  printf("      Input runid for coadding with string of runid#1,runid#2,... (comma seperated). For single runid, just input runid#1\n");

  printf("  -class_star <lower_#> <upper_#>\n");
  printf("      Set the upper and lower values of class_star classification from SExtractor when doing star matching\n");

  printf("  -sigmaclip <#>\n");
  printf("      Set the threshold in sigma-clipping. For example, 2.5, is a common choice\n");

  printf("  -Niterate <#>\n");
  printf("      Set the maximum number of iteration in sigma-clipping\n");

  printf("  -Nstarmatch <#>\n");
  printf("      Set the minumum number of matched stars, image pairs with matched stars lower than this value will not be used in the matrix solution\n");

  printf("  -rmscut <#> \n");
  printf("      Set the maximum allowed RMS from the mean magnitudes in star matching, image pairs with RMS greater than this input value will be excluded\n");

  printf("  -flag <#>\n");
  printf("      Set the flag value from SExtractor when doing star matching\n");

  printf("  -radius <#>\n");
  printf("      Set the radius when doing star matching, in arcmin\n");

  printf("  -magtype <#>\n");
  printf("      Set the magnitude type when doing star matching, 0 is using mag_auto; 1 to 6 is using mag_aper_1 to mag_aper_17\n");

  printf("  -magcut <low_mag> <high_mag>\n");
  printf("      Set the upper and lower values of magnitude doing star matching\n");

  printf("  -maxmagdiff <#>\n");
  printf("      Set the maximum value of mag_difference for filtering out the image pairs with mag_difference greater than this value\n");

  printf("  -fwhm <#>\n");
  printf("      Set the maximum value of FWHM for filtering out the images with FWHM greater than this value\n");

  printf("  -exptime <lower_#> <upper_#>\n");
  printf("      Set the range of EXPTIME for filtering out the images\n");

  printf("  -weightmean\n");
  printf("      If calculating the weighted mean in the magnitude difference of matched stars\n");

  printf("  -weightfit \n");
  printf("      If using the wegith in matrix calculation\n");

  printf("  -weight \n");
  printf("      If using the weight calculation for both mean magnitude difference and matrix calculation\n");

  printf("  -combinetype <type>\n");
  printf("      Set the combinetype in swarp, which include: median,average,min,max,weighted,chi2,sum\n");

  //printf("  -output <coadd filename>\n");
  //printf("      Set the name of the coadded image\n");

  printf("  -neighbour\n");
  printf("      Query the additional 8 (or less) neighbouring tiles for the given input tile\n");

  printf("  -nostarmatch\n");
  printf("      If not using star matching\n");

  printf("  -noskybrite\n");
  printf("      If not using SKYBRITE information\n");

  printf("  -nophotozp\n");
  printf("      If not using photometric ZP information\n");

  printf("  -quickcoadd\n");
  printf("      Running SWarp for coaddition without any fluxscale calculation\n");

  printf("  -help\n");
  printf("      Print this help message\n");

  printf("  -version\n");
  printf("      Print current version\n");

  printf("  -quiet\n");
  printf("      Run in quiet mode\n");

}

void convert_runid(char *basedir, char *runiddesc, char *imagename, char *project, char *imgfullpath)
{
  char temp[100];
  char tempid[200],tempnite[100],tempccd[10];
  char tempidnew[200],tempnitenew[100];
  char tempidnew1[200],tempnitenew1[100];
  int j;

  for (j=strlen(runiddesc);j>0;j--) {
    if (!strncmp(&(runiddesc[j]),"_",1)) 
      runiddesc[j]=32;
  }
  sscanf(runiddesc,"%s %s %s",tempid,tempnite,tempccd);

  /* convert the <Project><Date> to <Date>000000_ format */
  for (j=strlen(tempid);j>0;j--) {
      if (!strncmp(&(tempid[j]),"B",1)) 
	tempid[j]=32;
      if (!strncmp(&(tempid[j]),"C",1)) 
	tempid[j]=32;
      if (!strncmp(&(tempid[j]),"S",1)) 
	tempid[j]=32;
  }
  sscanf(tempid,"%s %s",temp,tempidnew1);
  sprintf(tempidnew,"%s000000",tempidnew1);
  
  /* convert the <project><date> to <date> format for */
  for (j=strlen(tempnite);j>0;j--) {
    if (!strncmp(&(tempnite[j]),"b",1)) 
      tempnite[j]=32;
    if (!strncmp(&(tempnite[j]),"c",1)) 
      tempnite[j]=32;
    if (!strncmp(&(tempnite[j]),"s",1)) 
	tempnite[j]=32;
  }
  sscanf(tempnite,"%s %s",temp,tempnitenew1);
  if(!strcmp("BCS",project)) 
    sprintf(tempnitenew,"20%s",tempnitenew1);
  if(!strcmp("SCS",project))     
    sprintf(tempnitenew,"%s",tempnitenew1);
  
  sprintf(imgfullpath,"%s/%s_%s/remap/%s",basedir,tempidnew,tempnitenew,imagename);
}

FILE *Popen(const char *command,const char *mode,const char *file,int line)
{
  FILE *pipefile = NULL;
  pipefile = popen(command,mode);
  if(pipefile == NULL){
    fprintf(stderr,"Popen failed in %s on line %d: %s",file,line,strerror(errno));
    exit(1);
  }
  return(pipefile);
}

#undef ACCURACY
#undef TOLERANCE
#undef VERSION
#undef SVN_VERSION
#undef MAX
#undef MOSAIC
#undef DECAM
#undef LGE
#undef ZPLO 
#undef ZPHI 
