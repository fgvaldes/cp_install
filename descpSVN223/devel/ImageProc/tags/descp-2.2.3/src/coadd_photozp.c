/*
**
** coadd_selectimage.c
**
** DESCRIPTION:
**     Select the single band remap or reduced images for a given tile, and 
**     apply any filtering if necessary.
**     Output will be a list of image ID saved in ASCII file
**     
** OUTPUT FILE FORMAT:
**    ImageID  
**
** Last commit:
**     $Rev: 3199 $
**     $LastChangedBy: dadams $
**     $LastChangedDate: 2009-02-23 13:43:37 -0700 (Mon, 23 Feb 2009) $
**
*/

#include "imageproc.h"

main(argc,argv)
     int argc;
     char *argv[];
{
  char infile[1000],outfile[1000];
  char command[1000],event[1000],line[1000];
  char dblogin[1000],sqlcall[1000],sqlHEAD[1000];

  int *ID_array;
  int N_image,N_zpin,N_zp,i,j,endloop;
  int flag_verbose=2,flag_input=0,flag_output=0,flag_zpmax;

  float maxzp;

  db_zeropoint  *zp, *zp_latest;

  FILE *fsqlout,*fin,*fout,*pip;

  /* external functions call */
  void select_dblogin(),reportevt(),get_input();

  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <Optional Inputs>\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -input <input file from coadd_selectimage>\n");
    printf("       -output <output result filename>\n");
    printf("    Optional Inputs:\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }
  
  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    /* required inputs */
    if (!strcmp(argv[i],"-input")) {
      get_input(argv[i],argv[i+1],"-input",infile,flag_verbose); 
      flag_input=1;
    }
 
    if (!strcmp(argv[i],"-output")) {
      get_input(argv[i],argv[i+1],"-output",outfile,flag_verbose); 
      flag_output=1;
    }

    /* option inputs */
  }

  /* *** Make sure inputs and outputs are there *** */
  if(!flag_input) {
    sprintf(event,"Input file is not provided ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }

  if(!flag_output) {
    sprintf(event,"Output file is not provided ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  
  /* *** Setup DB login and make generic sqlcall *** */
  select_dblogin(dblogin,DB_READWRITE);
  sprintf(sqlcall, "${ORACLE_HOME}/bin/sqlplus -S %s < temp.sql ",dblogin);
  sprintf(sqlHEAD,"SET NEWP 0 SPA 1 PAGES 0 FEED OFF HEAD OFF TRIMS ON LINESIZE 1000;");

  /* *** Findout how many images in the input file *** */
  sprintf(command,"wc -l %s",infile);
  pip=popen(command,"r");
  fscanf(pip,"%d",&N_image);
  pclose(pip);
  sprintf(event,"Input file %s contains %d images ",infile,N_image);
  reportevt(flag_verbose,STATUS,2,event);

  /* *** Memory allocation and input data *** */
  ID_array=(int *)calloc(N_image+1,sizeof(int));
  zp_latest=(db_zeropoint *)calloc(N_image+1,sizeof(db_zeropoint));

  fin=fopen(infile,"r");
  for(i=1;i<=N_image;i++) 
    fscanf(fin,"%d",&ID_array[i]);
  fclose(fin);

  /* initialize arrays */
  for(i=0;i<=N_image;i++) {
    zp_latest[i].mag_zero=25.0; 
    zp_latest[i].sigma_mag_zero=0.0;
  }
  

  /* ****************************** */
  /* *** Query Zero-Points Here *** */
  /* ****************************** */

  fsqlout=fopen("temp.sql", "w");
  fprintf(fsqlout,"%s\n",sqlHEAD);
  for(i=1;i<=N_image;i++) {
    fprintf(fsqlout,"SELECT zeropoint.id,zeropoint.image_n,zeropoint.mag_zero,zeropoint.sigma_mag_zero  ");
    fprintf(fsqlout,"FROM zeropoint,files WHERE ");
    fprintf(fsqlout,"files.imageid=zeropoint.image_n and files.photflag=1 and ");
    fprintf(fsqlout,"zeropoint.image_n=%d order by zeropoint.id;\n",ID_array[i]);
  }
  fprintf(fsqlout,"exit;\n");
  fclose(fsqlout);

  /* find out how many zp returned */  
  sprintf(command, "%s | wc -l",sqlcall);
  pip=popen(command,"r");
  while (fgets(line,1000,pip)!=NULL)
    sscanf(line,"%d %s",&N_zpin,line);
  pclose(pip);

  /* set memory allocation */  
  zp=(db_zeropoint *)calloc(N_zpin+1,sizeof(db_zeropoint));

  /* *** Input the zp info *** */
  pip=popen(sqlcall,"r"); i=1; 
  while (fgets(line,1000,pip)!=NULL) {
    sscanf(line,"%d %d %f %f",&zp[i].id,&zp[i].imageid,&zp[i].mag_zero,&zp[i].sigma_mag_zero);
    i++;
  }
  pclose(pip);

  /* loop over to find the latest zp for each images */
  N_zp=0;
  for(i=1;i<=N_image;i++) {
    
    flag_zpmax=0;
    for(j=1;j<=N_zpin;j++) {
	
      if(ID_array[i] == zp[j].imageid) {
	endloop=j;
	  
	/* first assign the maxzp with first zp */
	if(!flag_zpmax) {
	  maxzp=zp[j].id;
	  zp_latest[i].mag_zero=zp[j].mag_zero;
	  zp_latest[i].sigma_mag_zero=zp[j].sigma_mag_zero;
	  flag_zpmax=1;
	  N_zp++;
	}
	else { /* find the latest zp using largest id */
	  if(zp[j].id > maxzp) { 
	    maxzp=zp[j].id;
	    zp_latest[i].mag_zero=zp[j].mag_zero;
	    zp_latest[i].sigma_mag_zero=zp[j].sigma_mag_zero;
	  }
	}
      } // if(imageid) loop
      
      /* quit the j-loop after finding the latest zp */
      if(j > endloop && flag_zpmax) break;
    } // j loop
  } 
  
  /* *** Output results *** */
  sprintf(event,"Number of latest ZP = %d ",N_zp);
  if(!N_zp)
    reportevt(flag_verbose,STATUS,3,event);
  else
    reportevt(flag_verbose,STATUS,2,event);
  
  fout=fopen(outfile,"w");
  for(i=1;i<=N_image;i++) 
    fprintf(fout,"%d\t%2.4f\t%2.4f\n",ID_array[i],zp_latest[i].mag_zero,zp_latest[i].sigma_mag_zero);
  fclose(fout);
    

  /* *** Clean up *** */
  system ("rm temp*");
  
  /* *** Free memory *** */
  free(ID_array); free(zp_latest); free(zp);
}
