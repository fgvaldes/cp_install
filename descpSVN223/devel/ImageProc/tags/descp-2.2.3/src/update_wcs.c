/* Code to propogate WCS keyords into an image from another image/HDU with existing (presumably relavent) WCS solution */
#include "imageproc.h"

/* desimage *data,datain,output; */

int main(argc,argv)
	int argc;
	char *argv[];
{

	int	atype,ctype,status=0,j,len,nkeys,flag_verbose=2,update_tpv=0;
	void	printerror(),reportevt();
	char	ctype1[100],ctype2[100],value[100],param[15],incoming[200],oldvalue[100],
		comment[100],comment1[100],comment2[100],oldcomment[100],event[5000];
        double  dblkey,olddblkey;
	fitsfile *fptr,*fptr_wcs;

        /* Currently supported list of doutle precision type keywords (should be present for all projections) */
        char    wcsdkeys[25][8]={"CRVAL1","CRVAL2","CRPIX1","CRPIX2","CD1_1","CD1_2","CD2_1","CD2_2",""};
        /* Currently supported list of doutle precision TPV distortion correction keywords */
        /* Note PV1_3 and PV2_3 currently not used and thus not included here */
        char    pvkeys[25][8]={"PV1_0","PV1_1","PV1_2","PV1_4","PV1_5","PV1_6","PV1_7","PV1_8","PV1_9","PV1_10",
                               "PV2_0","PV2_1","PV2_2","PV2_4","PV2_5","PV2_6","PV2_7","PV2_8","PV2_9","PV2_10",
                               ""};

	if (argc<2) {
	  printf("update_wcs <image_to_update> <image_with_wcs> <options> \n");
	  printf("  Output Options:\n");
	  printf("  -verbose <0-3>\n");
	  exit(0);
	}
	
	/* confirm that we have a fits image or a compressed fits image */
	len=strlen(argv[1])-1;
	for (j=len;j>=0;j--) 
 	  if (!strncmp(&(argv[1][j]),".fit",4)) break;
	if (j==-1) {
	  printf("  ** update_wcs requires a FITS image on input\n");
	  exit(1);
	}

	/* confirm that we have a second fits image or a compressed fits image */
	len=strlen(argv[2])-1;
	for (j=len;j>=0;j--) 
 	  if (!strncmp(&(argv[2][j]),".fit",4)) break;
	if (j==-1) {
	  printf("  ** update_wcs requires a second FITS image with an existing WCS solution\n");
	  exit(1);
	}

	/* open the first image  */
	if (fits_open_file(&fptr,argv[1],READWRITE,&status)) printerror(status);
	/* open the second image */
	if (fits_open_file(&fptr_wcs,argv[2],READONLY,&status)) printerror(status);
        /* check that a rudimemtary WCS exists in the second FITS image */
	if (fits_read_key_str(fptr_wcs,"CTYPE1",ctype1,oldcomment,&status)==KEY_NO_EXIST) {
	  status=0;
	  printf("  Rudimentary check for CTYPE1 found no evidence for a WCS being present.  Aborting\n");
          exit(1);
        }

        /* Check for type of WCS solution type, currently supports TAN, SIN, TPV */
        len=strlen(ctype1)-1;
        ctype=0;
	for (j=len;j>=0;j--) { 
 	  if (!strncmp(&(ctype1[j]),"TAN",3)) ctype=1; 
 	  if (!strncmp(&(ctype1[j]),"SIN",3)) ctype=2; 
 	  if (!strncmp(&(ctype1[j]),"TPV",3)) ctype=3; 
          if (ctype != 0) break;
        }
        if (ctype == 0) {
          printf("  Supported projection type (TAN,SIN,TPV) not found\n");
          exit(1);
        }

        /* Copy header keywords that are in common to all three projection types */
        status=0;
        if (fits_read_key_str(fptr_wcs,"CTYPE1",ctype1,comment1,&status)==KEY_NO_EXIST){
          sprintf(event,"Keyword CTYPE1 missing in: %s ",argv[2]);
          reportevt(flag_verbose,STATUS,3,event);
        }
        if (status == 0){
          if (fits_read_key_str(fptr,"CTYPE1",oldvalue,oldcomment,&status)==KEY_NO_EXIST){ 
            status=0;
            if (fits_write_key_str(fptr,"CTYPE1",ctype1,comment1,&status)) printerror(status);
          }else{ 
            if (fits_update_key_str(fptr,"CTYPE1",ctype1,comment1,&status)) printerror(status);
          }
        }

        status=0;
        if (fits_read_key_str(fptr_wcs,"CTYPE2",ctype2,comment2,&status)==KEY_NO_EXIST){
          sprintf(event,"Keyword CTYPE2 missing in: %s ",argv[2]);
          reportevt(flag_verbose,STATUS,3,event);
        }
        if (status == 0){
          if (fits_read_key_str(fptr,"CTYPE2",oldvalue,oldcomment,&status)==KEY_NO_EXIST){
            status=0;
            if (fits_write_key_str(fptr,"CTYPE2",ctype2,comment2,&status)) printerror(status);
          }else{ 
            if (fits_update_key_str(fptr,"CTYPE2",ctype2,comment2,&status)) printerror(status);
          }
        }

        /* Now that special (char type) keywords have been dealt with, loop through the numeric keywords */

        nkeys=0;
        while (strlen(wcsdkeys[nkeys])) {
          status=0;
          if (fits_read_key_dbl(fptr_wcs,wcsdkeys[nkeys],&dblkey,comment,&status)==KEY_NO_EXIST){
            sprintf(event,"Keyword %s missing in: %s ",wcsdkeys[nkeys],argv[2]);
            reportevt(flag_verbose,STATUS,3,event);
          }
          if (status == 0){
            if (fits_read_key_dbl(fptr,wcsdkeys[nkeys],&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
              status=0;
              if (fits_write_key(fptr,TDOUBLE,wcsdkeys[nkeys],&dblkey,comment,&status)) printerror(status);
            }else{ 
              if (fits_update_key(fptr,TDOUBLE,wcsdkeys[nkeys],&dblkey,comment,&status)) printerror(status);
            }
          }
          nkeys++;
        }

        /* if type is TAN test for backward compatibility, i.e., whether or not PV terms are present " */
        /* Only report instances of missing keywords for TPV projection type */

        update_tpv=0;
        if ((ctype == 3)||(ctype == 1)){

          nkeys=0;
          while (strlen(pvkeys[nkeys])) {
            status=0;
            if (fits_read_key_dbl(fptr_wcs,pvkeys[nkeys],&dblkey,comment,&status)==KEY_NO_EXIST){
              if (ctype == 3){
                sprintf(event,"Keyword %s missing in: %s ",pvkeys[nkeys],argv[2]);
                reportevt(flag_verbose,STATUS,3,event);
              }
            }
            if (status == 0){
              if (fits_read_key_dbl(fptr,pvkeys[nkeys],&olddblkey,oldcomment,&status)==KEY_NO_EXIST){
                status=0;
                if (fits_write_key(fptr,TDOUBLE,pvkeys[nkeys],&dblkey,comment,&status)) printerror(status);
              }else{ 
                if (fits_update_key(fptr,TDOUBLE,pvkeys[nkeys],&dblkey,comment,&status)) printerror(status);
              }
              if (ctype == 1) update_tpv++;
            }
            nkeys++;
          }
        }
        status=0;

        /* Now... if the original file was type TAN but TPV arguments were present update CTYPE1 and CTYPE2 */
        /* to reflect that this file indeed follows the TPV convention */

        if ((ctype == 1)&&(update_tpv > 0)){

          status=0;
          len=strlen(ctype1)-1;
          atype=0;
  	  for (j=len;j>=0;j--) { 
 	    if (!strncmp(&(ctype1[j]),"RA",2)) atype=1; 
 	    if (!strncmp(&(ctype1[j]),"DEC",3)) atype=2; 
            if (atype != 0) break;
          }
          if (atype == 1){
            if (fits_update_key_str(fptr,"CTYPE1","RA---TPV",comment1,&status)) printerror(status);
          }else{          
            if (fits_update_key_str(fptr,"CTYPE1","DEC--TPV",comment1,&status)) printerror(status);
          }

          status=0;
          len=strlen(ctype2)-1;
          atype=0;
  	  for (j=len;j>=0;j--) { 
 	    if (!strncmp(&(ctype2[j]),"RA",2)) atype=1; 
 	    if (!strncmp(&(ctype2[j]),"DEC",3)) atype=2; 
            if (atype != 0) break;
          }
          if (atype == 1){
            if (fits_update_key_str(fptr,"CTYPE2","RA---TPV",comment1,&status)) printerror(status);
          }else{          
            if (fits_update_key_str(fptr,"CTYPE2","DEC--TPV",comment1,&status)) printerror(status);
          }

        }

	/* close the files */
        status=0;
	if (fits_close_file(fptr_wcs,&status)) printerror(status);
	if (fits_close_file(fptr,&status)) printerror(status);

        exit(0);

}
