#include "PixToRADec.h"
/*
 *The program calls PixTools method to calculate RA,Dec from pixel number and given
 * nside parameter (map resolution)
 *
 * Author: Nikolay Kuropatkin    10/02/2012
 *
 */

using namespace std;



  int main(int argc, char *argv[]) {
	  	  double *radec = new double[2];
	      double *ang = new double[2];
	  	  PixTools *pt = new PixTools();

	  	  int nside;

    	  int pixel;
    	  if (argc<2) {
    	    cout<<"Usage: "<<argv[0] <<" required inputs> \n"<<endl;
    	    cout<<"    Required Inputs:"<<endl;
    	    cout<<"       <Pixel number> <nside> "<<endl;
    	    cout<<"       Output: print RA,Dec in the center of the pixel "<<endl;
    	    cout<<endl;
    	    delete(pt);
    	        exit(0);
    	  }


    	  // now go through arguments

    	   pixel = atoi(argv[1]);
    	   nside = atoi(argv[2]);
//    	   cout<<" Pix="<<pixel<<" "<<"nside="<<nside<<endl;


      	  	ang = pt->pix2ang_ring(nside,pixel);
       	  	radec = pt->PolarToRaDec(ang);
       	    cout << setiosflags(ios::fixed) << setprecision(5) << radec[0]<<","<<radec[1]<<endl;
//       	  	cout<<radec[0]<<","<<radec[1]<<endl;
       	    delete(ang);
       	    delete(radec);
       	  	delete(pt);


}

