/* makeTemp.c 

   Version History:
     1.0   --- initial version



** Last commit:
**     $Rev:  $
**     $LastChangedBy:  $
**     $LastChangedDate:  $
*/

#include "imageproc.h"

#define VERSION "1.5"
#define ZPLO 27.0
#define ZPHI 34.0

void getkeyword(fitsfile *fptr, FILE *fout, char *keyname);
void create_headfile_fits(char *reduceimage, char *headname);
void write_ZP(double ZP, char *image, int flag_verbose);
double read_ZP(char *name, int flag_verbose);

int main(int argc, char *argv[]) 
{
  char reduceimage[1500],coaddlist[1500],inputfile[1500],inputlist[1500];
  char **coaddimage,**imagelist;
  char tempfile[1500];
  char command[1000],event[1000];
  char coaddroot[1500],rootname[1500],templatename[1500],headname[1500],temp[1500],temp1[1500],temp2[1500];
  char coaddimglist[1500],coaddweightlist[1500],fluxscalelist[1500];
  char swarpcall[5000],fitscombinecall[5000];
  char tempimg[1500],tempweight[1500],checkimage[1500];
  char terapixpath[1000],binpath[1000],etcpath[1000],outpath[1000],outauxpath[1000];
  
  double magbase,fluxscale,*ZP;

  int i,j,N,Ncoadd,nthread,ncheck;
  int flag_coaddlist=0,flag_verbose=2,flag_terapixpath=0,flag_etcpath=0,flag_binpath=0,flag_outauxpath=0,flag_outpath=0,flag_nthread=0,flag_nitecmb;

  FILE *fin,*fcoaddimages,*fcoaddweight,*ffluxscale,*fcheck;
  
  int count_inputfile(),mkpath();
  void reportevt(),get_input(),check_fits_extension();

  if(argc<2) {
    printf("Usage %s (Version %s): <reduced image OR @image_list> [REQUIRED] [OPTIONS]\n",argv[0],VERSION);
    printf("         REQUIRED:\n");
    printf("            -coaddlist <coadd images list>\n"); 
    printf("            -terapixpath <full terapixpath>\n");
    printf("            -outauxpath <outauxpath>\n");
    printf("         OPTIONS\n");
    printf("            -binpath <full binpath>\n");
    printf("            -etcpath <full etcpath>\n");
    printf("            -outpath <outpath>\n");
    printf("            -nthread <0-8>\n");
    printf("            -verbose <0-3>\n");
    exit(0);
  }
  
  /* process command line to get verbose level */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    
    /* input coadd image list */
    if(!strcmp("-coaddlist",argv[i])) {
      flag_coaddlist=1;
      get_input(argv[i],argv[i+1],"-coaddlist",coaddlist,flag_verbose);
    }

    if (!strcmp(argv[i],"-terapixpath")) {
      flag_terapixpath=1;
      get_input(argv[i],argv[i+1],"-terapixpath",terapixpath,flag_verbose);
    }
 
    if (!strcmp(argv[i],"-outauxpath")) {
      flag_outauxpath=1;
      get_input(argv[i],argv[i+1],"-outauxpath",outauxpath,flag_verbose);
    }

    /* options */
    if (!strcmp(argv[i],"-outpath")) {
      flag_outpath=1;
      get_input(argv[i],argv[i+1],"-outpath",outpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-etcpath")) {
      flag_etcpath=1;
      get_input(argv[i],argv[i+1],"-etcpath",etcpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-binpath")) {
      flag_binpath=1;
      get_input(argv[i],argv[i+1],"-binpath",binpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-nthread")) {
      flag_nthread=1;
      nthread=atoi(argv[i+1]);
      if(nthread > 8 || nthread < 0) {
	sprintf(event," nthread out of range (0-8), reset to 0 ");
	reportevt(flag_verbose,STATUS,3,event);
	nthread=0;
      }
    }
  }

  /* check if the -terapixpath and -etcpath is set */
  if(!flag_terapixpath || !flag_outauxpath) {
    sprintf(event," Either -terapixpath or -outauxpath are not set ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }

  /* ----------------------------------------------------------- */
  /* CHECK INPUT COADD IMAGE LIST AND INPUT THE COADD IMAGES     */
  /* ----------------------------------------------------------- */

  if(!flag_coaddlist) {
    sprintf(event," %s Error: -coaddlist option is not set, abort\n",argv[0]);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }

  /* find out the number of lines from coaddlist */
  Ncoadd=count_inputfile(coaddlist,flag_verbose);
  sprintf(event,"Number of coadd images in %s = %d",coaddlist,Ncoadd);
  reportevt(flag_verbose,STATUS,1,event);

  /* get the coaddlist rootname */
  sprintf(temp,"%s",coaddlist);
  for(i=strlen(temp);i>0;i--) {
    if(!strncmp(&(temp[i]),"/",1)) { 
      temp[i]=32;
      break;
    }
  }
  sscanf(temp,"%s %s",temp1,coaddroot);

  /* memory allocation */
  coaddimage=(char **)calloc(Ncoadd+1,sizeof(char *));
  if (coaddimage==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of coaddimage failed");
    exit(0);
  }
  for(i=1;i<=Ncoadd;i++)
    coaddimage[i]=(char *)calloc(1500,sizeof(char ));

  ZP=(double *)calloc(Ncoadd+1,sizeof(double));
  if (ZP==NULL) {
    reportevt(flag_verbose,STATUS,5,"Calloc of ZP failed");
    exit(0);
  }

  /* input coadd images and output two lists */
  fin=fopen(coaddlist,"r");
  if (fin==NULL) {
    sprintf(event,"File %s not found",coaddlist);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else {

    for(i=1;i<=Ncoadd;i++) {
      tempfile[0]=0;
      fscanf(fin,"%s",coaddimage[i]);
   
      /* check the coadd image extension and unpack/unzip if necessary */
      check_fits_extension(coaddimage[i],terapixpath,flag_verbose);

      /* get the ZP from the coadd image header */
      ZP[i]=read_ZP(coaddimage[i],flag_verbose); /* replace with DB call later */
      
    }
    fclose(fin); 
  }

  /* get the magbase */
  for(i=1;i<=Ncoadd;i++) {
    if(ZP[i]>ZPLO && ZP[i]<ZPHI) {
      magbase=ZP[i];
      break;
    }
  }

  sprintf(event," mag_base = %2.3f ",magbase);
  reportevt(flag_verbose,STATUS,1,event);

  /* calculate the fluxscale and output the lists */
  sprintf(coaddimglist,"images_%s",coaddroot);
  sprintf(coaddweightlist,"weight_%s",coaddroot);  
  sprintf(fluxscalelist,"fluxscale_%s",coaddroot);

  fcoaddimages=fopen(coaddimglist,"w");
  fcoaddweight=fopen(coaddweightlist,"w");  
  ffluxscale=fopen(fluxscalelist,"w");

  ncheck=0;
  for(i=1;i<=Ncoadd;i++) {
    if(ZP[i]<ZPLO || ZP[i]>ZPHI) {
      sprintf(event," Coadd image discarded (ZP = %2.4f): %s",ZP[i],coaddimage[i]);
      reportevt(flag_verbose,STATUS,4,event);
    }
    else {
      fluxscale=pow(10.0,0.4*(magbase-ZP[i]));
      sprintf(event,"Name = %s & Fluxscale = %6.4f & Mag_zero = %.4f",
	      coaddimage[i],fluxscale,ZP[i]);
      reportevt(flag_verbose,QA,2,event);
      /* write to the output lists */
      fprintf(fcoaddimages,"%s[0]\n",coaddimage[i]);
      fprintf(fcoaddweight,"%s[1]\n",coaddimage[i]);
      fprintf(ffluxscale,"%2.6f\n",fluxscale);
      
      ncheck++;
    }
  }
  fclose(fcoaddimages); fclose(fcoaddweight); fclose(ffluxscale);

  if(!ncheck) {
    sprintf(event," No coadd images with ZP between %2.1f and %2.1f",ZPLO,ZPHI);
    reportevt(flag_verbose,STATUS,4,event);
    exit(1);
  }

  /* -------------------------------------- */
  /* CHECK INPUT REDUCED IMAGE OR LIST      */
  /* -------------------------------------- */

  sprintf(inputfile,"%s",argv[1]);

  if(!strncmp(&(inputfile[0]),"-",1)) {
    reportevt(flag_verbose,STATUS,5,
	      "Reduce image or image list not given? Double check command-line inputs");
    exit(0);
  }
  else if(!strncmp(&(inputfile[0]),"@",1)) {
    sprintf(temp,"%s",inputfile);
    temp[0]=32;
    sscanf(temp,"%s",inputlist);
    sprintf(event,"Input image list is %s",inputlist);
    reportevt(flag_verbose,STATUS,2,event);

    N=count_inputfile(inputlist,flag_verbose);
    sprintf(event,"Number of reduced images in %s = %d",inputlist,N);
    reportevt(flag_verbose,STATUS,1,event);

    /* memory allocation */
    imagelist=(char **)calloc(N+1,sizeof(char *));
    if (imagelist==NULL) {
      reportevt(flag_verbose,STATUS,5,"Calloc of imagelist failed");
      exit(0);
    }
    for(i=1;i<=N;i++)
      imagelist[i]=(char *)calloc(1500,sizeof(char ));
   
    /* input the reduced image to imagelist */
    fin=fopen(inputlist,"r");
    if (fin==NULL) {
      sprintf(event,"File %s not found",inputlist);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }
    else {
      for(i=1;i<=N;i++) { 
	fscanf(fin,"%s",imagelist[i]); 
   
	/* check the coadd image extension and unpack/unzip if necessary */
	check_fits_extension(imagelist[i],terapixpath,flag_verbose);
      }
      fclose(fin);
    }
  }
  else {
    N=1;

    /* memory allocation */
    imagelist=(char **)calloc(N+1,sizeof(char *));
    if (imagelist==NULL) {
      reportevt(flag_verbose,STATUS,5,"Calloc of imagelist failed");
      exit(0);
    }
    for(i=1;i<=N;i++)
      imagelist[i]=(char *)calloc(1500,sizeof(char ));

    /* assign the inputfile to the array */
    imagelist[1]=inputfile;
  }

  /* loop over the reduced images in the list (or just single image ) */
 
  for(i=1;i<=N;i++) {
    
    reduceimage[0]=0; /* initialize */
    temp[0]=0;
    sprintf(reduceimage,"%s",imagelist[i]);
  
    check_fits_extension(reduceimage,terapixpath,flag_verbose);

    /* ------------------------------------------------------- */
    /* SETUP OUTPUT TEMPLATE IMAGE NAME AND .HEAD FILENAME      */
    /* ------------------------------------------------------- */

    sprintf(event," Creating (distorted) template for image %s ",reduceimage);
    reportevt(flag_verbose,STATUS,1,event);
  
    /* take out the .fits and locate the last "/" to get the rootname */
    sprintf(temp,"%s",reduceimage);
    for(j=strlen(temp);j>0;j--) {
      if(!strncmp(&(temp[j]),".fits",5)) 
	temp[j]=0;
      if(!strncmp(&(temp[j]),"/",1)) { 
	temp[j]=32;
	break;
      }
    }
    sscanf(temp,"%s %s",temp1,temp2);
    
    /* check if it is from nitely combine or reduced image by the imagename */
    flag_nitecmb=0;
    for(j=strlen(temp2);j>0;j--) {
      if(!strncmp(&(temp2[j]),"_nitecmb",8)) {
	temp2[j]=0;
	flag_nitecmb=1;
	break;
      }
    }
    
    if(flag_nitecmb)
      sprintf(rootname,"%s_ncdistmp",temp2);
    else
      sprintf(rootname,"%s_distmp",temp2);

    /* set the output temp fits name and .head filename */
    sprintf(tempimg,"%s_tmpimg.fits",rootname);
    sprintf(tempweight,"%s_tmpweight.fits",rootname);
    sprintf(headname,"%s_tmpimg.head",rootname);
  
    /* -------------------------------------------------------------------- */
    /* CREATE THE .HEAD FILE FROM FITS HEADER  (or DB, to be added later)   */
    /* -------------------------------------------------------------------- */

    create_headfile_fits(reduceimage,headname);
    //create_headfile_db(reduceimage,headname);  /* subroutine not written yet */
  
    /* --------------------------------------------------- */
    /* USE SWARP TO DISTORT AND COMBINE THE COADD IMAGES   */
    /* --------------------------------------------------- */

    /* construct swarp call here */
    if(flag_terapixpath) sprintf(swarpcall,"%s/swarp ",terapixpath);
    else sprintf(swarpcall,"swarp ");

    sprintf(swarpcall,"%s @%s",swarpcall,coaddimglist); 
    
    if(flag_etcpath) sprintf(swarpcall,"%s -c %s/default.swarp ",swarpcall,etcpath);
    else sprintf(swarpcall,"%s -c default.swarp ",swarpcall);

    sprintf(swarpcall,"%s -IMAGEOUT_NAME %s ",swarpcall,tempimg);
    sprintf(swarpcall,"%s -WEIGHTOUT_NAME %s ",swarpcall,tempweight);
    sprintf(swarpcall,"%s -COMBINE Y ",swarpcall);
    sprintf(swarpcall,"%s -RESAMPLE Y ",swarpcall);
    sprintf(swarpcall,"%s -COMBINE_TYPE MEDIAN ",swarpcall);
    sprintf(swarpcall,"%s -SUBTRACT_BACK N",swarpcall);
    sprintf(swarpcall,"%s -DELETE_TMPFILES Y",swarpcall);
    sprintf(swarpcall,"%s -WRITE_XML N ",swarpcall);
    sprintf(swarpcall,"%s -FSCALE_DEFAULT @%s ",swarpcall,fluxscalelist);
    sprintf(swarpcall,"%s -FSCALE_KEYWORD nokey ",swarpcall);
    //sprintf(swarpcall,"%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE @%s ",swarpcall,coaddweightlist);
    if(flag_nthread)
      sprintf(swarpcall,"%s -NTHREADS %d ",swarpcall,nthread);
    if(flag_verbose==0)
      sprintf(swarpcall,"%s -VERBOSE_TYPE QUIET ",swarpcall);

    sprintf(event," SWARPcall: %s\n",swarpcall);
    reportevt(flag_verbose,STATUS,2,event);

    system(swarpcall);

    /* write the ZP back to image */
    write_ZP(magbase,tempimg,flag_verbose);

    /* ------------------------------------------------------- */
    /* COMBINE THE OUTPUT IMAGES AND PUT IN APPROPRIATE PLACE  */
    /* ------------------------------------------------------- */

    if(flag_outpath) {
      sprintf(templatename,"%s/%s.fits",outpath,rootname);

      if (mkpath(templatename,flag_verbose)) {
	sprintf(event,"Failed to create path to file: %s",templatename);
	reportevt(flag_verbose,STATUS,5,event);
	exit(0);
      }
      else {
	sprintf(event,"Created path to file %s",templatename);
	reportevt(flag_verbose,STATUS,1,event);
      }
    }
    else
      sprintf(templatename,"%s.fits",rootname);

    /* combine the coadd image and weight maps */
    if(flag_binpath) 
      sprintf(fitscombinecall,"%s/fitscombine %s %s %s -cleanup -verbose %d",
	      binpath,tempimg,tempweight,templatename,flag_verbose);
    else 
      sprintf(fitscombinecall,"fitscombine %s %s %s -cleanup -verbose %d",
	      tempimg,tempweight,templatename,flag_verbose);
    
    sprintf(event,"Fitscombine Call: %s ",fitscombinecall);
    reportevt(flag_verbose,STATUS,1,event);
    system(fitscombinecall);
    
    sprintf(checkimage,"%s",templatename);
    fcheck=fopen(checkimage,"r");
    if (fcheck==NULL) {
      sprintf(event,"Distorted template creation failed: %s",templatename);
      reportevt(flag_verbose,STATUS,5,event);
      exit(0);
    }
    else
      fclose(fcheck);

    /* move the .head to propername */
    sprintf(command,"mv %s_tmpimg.head %s.head",rootname,rootname);
    system(command);

  } /* i-loop */

  /* free memory */
  free(coaddimage); free(imagelist); free(ZP);

  return (0);
}

void create_headfile_fits(char *reduceimage, char *headname)
{
  int status;
  void printerror();

  FILE *fhead;
  fitsfile *fptr;
  
  /* create the .head file */
  fhead=fopen(headname,"w");
  
  /* write the first line to .head file */
  fprintf(fhead,"COMMENT\n");

  /* open the reduced fits image to read in necessary information */
  status=0;
  if(fits_open_file(&fptr,reduceimage,READONLY,&status)) printerror(status);

  /* get the necessary keywords */
  getkeyword(fptr,fhead,"RADECSYS");
  getkeyword(fptr,fhead,"EQUINOX");

  getkeyword(fptr,fhead,"NAXIS");
  getkeyword(fptr,fhead,"NAXIS1");
  getkeyword(fptr,fhead,"NAXIS2");

  getkeyword(fptr,fhead,"CTYPE1");
  getkeyword(fptr,fhead,"CTYPE2");

  getkeyword(fptr,fhead,"CRVAL1");
  getkeyword(fptr,fhead,"CRVAL2");

  getkeyword(fptr,fhead,"CRPIX1");
  getkeyword(fptr,fhead,"CRPIX2");

  getkeyword(fptr,fhead,"CD1_1");
  getkeyword(fptr,fhead,"CD2_1");
  getkeyword(fptr,fhead,"CD1_2");
  getkeyword(fptr,fhead,"CD2_2");
  
  getkeyword(fptr,fhead,"PV1_0");
  getkeyword(fptr,fhead,"PV1_1");
  getkeyword(fptr,fhead,"PV1_2");
  getkeyword(fptr,fhead,"PV1_3");
  getkeyword(fptr,fhead,"PV1_4");
  getkeyword(fptr,fhead,"PV1_5");
  getkeyword(fptr,fhead,"PV1_6");
  getkeyword(fptr,fhead,"PV1_7");
  getkeyword(fptr,fhead,"PV1_8");
  getkeyword(fptr,fhead,"PV1_9");
  getkeyword(fptr,fhead,"PV1_10");

  getkeyword(fptr,fhead,"PV2_0");
  getkeyword(fptr,fhead,"PV2_1");
  getkeyword(fptr,fhead,"PV2_2");
  getkeyword(fptr,fhead,"PV2_3");
  getkeyword(fptr,fhead,"PV2_4");
  getkeyword(fptr,fhead,"PV2_5");
  getkeyword(fptr,fhead,"PV2_6");
  getkeyword(fptr,fhead,"PV2_7");
  getkeyword(fptr,fhead,"PV2_8");
  getkeyword(fptr,fhead,"PV2_9");
  getkeyword(fptr,fhead,"PV2_10");

  /* close the fits image and .head file */
  status=0;
  if(fits_close_file(fptr,&status)) printerror(status);

  fprintf(fhead,"END     \n");
  fclose(fhead);
}

void getkeyword(fitsfile *fptr, FILE *fout, char *keyname)
{
  char keyvalue[200];
  int status;
  void printerror();

  status=0;
  if(!fits_read_card(fptr,keyname,keyvalue,&status)) 
    fprintf(fout,"%s\n",keyvalue);
}

double read_ZP(char *name, int flag_verbose)
{
  char event[1000],comment[1000],nameimg[1000];
  int status;
  double ZP;
  void printerror(),reportevt();
  fitsfile *fptr;

  sprintf(nameimg,"%s[0]",name);

  status=0;
  if(fits_open_file(&fptr,nameimg,READONLY,&status)) printerror(status);

  status=0;
  if(fits_read_key_dbl(fptr,"SEXMGZPT",&ZP,comment,&status)==KEY_NO_EXIST) {
    sprintf(event,"Image %s keyword SEXMGZPT missing",name);
    reportevt(flag_verbose,STATUS,4,event);
    printerror(status);
    ZP=25.0;
  }
  
  status=0;
  if(fits_close_file(fptr,&status)) printerror(status);

  return (ZP);
}

void write_ZP(double ZP, char *image, int flag_verbose)
{
  char event[1000];
  int status;
  void printerror(),reportevt();
  fitsfile *fptr;

  status=0;
  if(fits_open_file(&fptr,image,READWRITE,&status)) printerror(status);

  status=0;
  if(fits_update_key(fptr,TDOUBLE,"SEXMGZPT",&ZP,"Mag ZP from coadd",&status))  {
    sprintf(event,"Inserting keyword SEXMGZPT to %s failed ",image);
    reportevt(flag_verbose,STATUS,4,event);
    printerror(status);
    ZP=25.0;
  }
  
  status=0;
  if(fits_close_file(fptr,&status)) printerror(status);
}

#undef VERSION
#undef ZPLO 
#undef ZPHI 


