
#ifndef FITSLR_H
#define FITSLR_H
#include "TGraph.h"
#include <vector>
#include "Math/Minimizer.h"
#include <string>
#include <kdtree++/kdtree.hpp>
#include "ColorPoint.h"
using namespace std;


class FitSLR : public ROOT::Math::IMultiGenFunction {


public:
  FitSLR():MaxOutlier(0.5),
           RemoveOutlier(true),
           CutPoints(3),CutRange(0.04),MinPoints(20)
  { for(int i=0;i<4;i++) stored_color[i]=0;}

  FitSLR(int _ncolors,string _colors,bool _debug=false);  
  
    
  std::vector<double> Fit(vector<ColorPoint> &_points,
                          vector<double> &err);

  std::vector<double> Fit(vector<ColorPoint2D> &_points,
                          vector<double> &err);

  std::vector<double> Fit(vector<ColorPoint4D> &_points,
                          vector<double> &err);
  
  // Complete the IMultiGenFunction interface
  unsigned int                   NDim() const {return 3;}
  double                         DoEval(const double* p) const;
  ROOT::Math::IMultiGenFunction* Clone() const { return 0;}

  void LoadData(string filename);
  //void RemoveOutliers(bool density=false);
  void GetDiff(vector<double> &diff1,
               bool use_fit_par=true);
  void GetDiff(vector<double> &diff1,vector<double> &diff2,
               bool use_fit_par=true);
  void GetDiff(vector<double> &diff1,vector<double> &diff2,
               vector<double> &diff3,
               bool use_fit_par=true);
  void SetPoints(vector<ColorPoint> &_points) {DataPoints_3D=_points;}
  void SetPoints(vector<ColorPoint2D> &_points) {DataPoints_2D=_points;}
  void SetPoints(vector<ColorPoint4D> &_points) {DataPoints_4D=_points;}
 //  void RemoveOutliers(vector<ColorPoint> &_points,
//                       int max_iter=3,double thresh=2.5,double tol=1e-6);
  void RemoveOutliers(vector<ColorPoint4D> &CutData);
//                      

//   void RemoveOutliers(vector<ColorPoint2D> &_points,
//                       int max_iter=3,double thresh=2.5,double tol=1e-6);
//   void RemoveOutliers(vector<ColorPoint2D> &_points,
//                       const double &dist);
  void SetRemoveOutlier(const bool &w) {RemoveOutlier=w;}
  void SetSigmaCut(const double &w) {SigmaCut=w;}

  void SetMaxOutlier(const double &w) {MaxOutlier=w;}
  void SetCutRange(const double &w) {CutRange=w;}
  void SetCutPoints(const int &w) {CutPoints=w;}

  TGraph graph;
private:

  int ncolors;
  bool debug;
  vector<string> colors;
  int DataEntries;
  int CoveyEntries;

  vector<ColorPoint2D> CoveyData_2D;                 
  vector<ColorPoint>   CoveyData_3D;                 
  vector<ColorPoint4D> CoveyData_4D;                 
  vector<ColorPoint2D> DataPoints_2D;                 
  vector<ColorPoint>   DataPoints_3D;                 
  vector<ColorPoint4D> DataPoints_4D;                 

  ColorPoint2D_Tree CoveyTree_2D;
  ColorPoint_Tree   CoveyTree_3D;
  ColorPoint4D_Tree CoveyTree_4D;

  ColorPoint2D_Tree DataTree_2D;
  ColorPoint_Tree   DataTree_3D;
  ColorPoint4D_Tree DataTree_4D;

  void DoFit(vector<double> &fit_par,vector<double> &fit_err);

  double stored_color[4];

  double CutRange;
  int CutPoints;
  int MinPoints;
  double MaxOutlier;
  bool ApplyColor;
  bool RemoveOutlier;
  double SigmaCut;
  
};
  
#endif
////////////////////////////////////////////////////////////////////////
