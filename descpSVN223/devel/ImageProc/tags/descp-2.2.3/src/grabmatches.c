#include "imageproc.h" 
#include "plplot.h"
#include "plConfig.h"
#include <ctype.h>

/* possible truth tables */
#define STRIPE82 0
#define CATSIM1  1
#define IMSIM2T  2
#define USNOB2   3
#define STANDARD 4
#define CATSIM3  5
#define CATSIM4  6
#define CATSIM5 7
#define AUG09_GSN 8
#define MARCTODEG   (0.001/3600)
#define TOLERANCE (2.0/3600.0) /* arcsec */
#define PHOTO_TOLERANCE 500.0 /* use in photometry comparison, need a large number */
#define PL_DOUBLE
#define VERSION "1.3"

void plot_ast(int N, double dra[], double ddec[]);
void plot_pho(int N, double tmag[], double dmag[]);

void plot_scatter(int N, double x[], double y[],PLFLT xmin, PLFLT xmax, PLFLT ymin, PLFLT ymax, char *xlabel, char *ylabel, char *label, int just);
void plot_histogram(int N, double x[], double deltabin, PLFLT xmin, PLFLT xmax, char *xlabel);

main(argc,argv)
	int argc;
	char *argv[];
{
	char	truthtable[100],objecttable[100],sqlcall[200],tmp[10],
		band[5],oband[100],nite[100],
		truthquery[1000],command[100],dblogin[500],runid[200],
	        magin[25],magerrin[25],stemp[10];
	char    objtable_in[100],ctemp;
	float	ramin,ramax,decmin,decmax,temp,scale,deltayear=0,
	        omag,omagerr,oclass,calcdistance(),
	  truemag,mindist,inclass,err_cut=0.1,
	  *sra,*tg,*tr,*ti,*tz,*ty,*tclass,*mura,*mudec;
	double	*tra,*tdec,ora,odec,ox,oy,omjd;
	double  dist,disterr,deltamag,minmag,totaldiff,errx2,erry2; /* new addition */
	int	i,j,k,flag_quiet=0,flag_ccd=0,flag_band=2,*tmatch,flag_propermotion=0,
	        nomatches,matches,nobjects,locmatch,loclow,lochi,random,
	  ntruth,flag_nite=0,flag_noquery=0,flag_runid=0,flag_truth,flag_noobjectquery=0,
	  flag_mag=0,flag_random=0,flag_star=0,flag_galaxy=0,flag_getband=0,flag_plot,flag,ccd,
	  flag_mkrootplot,oflags,flag_notruthquery=0;
	int     itemp;
	FILE	*pip,*out,*inp,*outnm;
	unsigned long *tindex;
	void	indexx(),select_dblogin();
	void    calcdistance_err();

	double *omag_match,*tmag_match,*omagerr_match,*dmag;
	double *dra,*ddec;
	double dtemp,dra_in,ddec_in,newtra,newtdec;
	time_t startTime,endTime;
	FILE *fin;
        int full_nite=0;
	
	/* default objects table */
	sprintf(objtable_in,"OBJECTS_CURRENT");

	if (argc<5) {
	  printf("  grabmatches (ver: %s) <RAmin> <RAmax> <DECmin> <DECmax>\n",VERSION);
	  printf("  -band <g,r,i,z,Y> (default is all)\n");
          printf("  -full_nite scan over whole night (default is no)\n");
	  printf("  -ccd <# or 0(default) for all>>\n");
	  printf("  -nite <nite>\n");
	  printf("  -runid <runid>\n");
	  printf("  -truth <stripe82,catsim1,imsim2_truth,usnob2,catsim3_truth,standard_stars,catsim4_truth,catsim5_truth,aug09_gsn>\n");
	  printf("  -staronly <#> \n");
	  printf("  -galaxyonly <#> \n");
	  printf("  -objtable <object_table>\n");
	  printf("  -magtype <#> (default is 0)\n");
          printf("          (-6 = mag_petro)\n");
          printf("          (-5 = mag_iso)\n");
          printf("          (-4 = mag_psf)\n");
          printf("          (-3 = mag_spheroid)\n");
          printf("          (-2 = mag_disk)\n");
	  printf("          (-1 = mag_model)\n");
          printf("          ( 0 = mag_auto)\n");
	  printf("          ( 1-17 = mag_aper[1-17])\n");
	  printf("  -err_cut <err cut default=0.1>\n");   
	  printf("  -random <#>\n");
	  printf (" -propermotion\n");
	  printf("  -noquery\n");
	  printf("  -noobjectquery\n");
          printf("  -notruthquery\n");
	  printf(" -mkrootplot\n");
	  printf("  -quiet\n");
	  exit(0);
	}
	sscanf(argv[1],"%f",&ramin);
	sscanf(argv[2],"%f",&ramax);
	sscanf(argv[3],"%f",&decmin);
	sscanf(argv[4],"%f",&decmax);

	/* set default truth table and SExtractor flag */
	flag_truth=CATSIM5;
	flag=3;
	flag_mkrootplot=0;

	/* grab dblogin */
	select_dblogin(dblogin,DB_READONLY);

	/* make sure values are in proper order */
	if (ramax<ramin) {temp=ramin;ramin=ramax;ramax=temp;}
	if (decmax<decmin) {temp=decmin;decmin=decmax;decmax=temp;}
	
	/*  process the rest of the command line */
	for (i=5;i<argc;i++) {
	  if (!strcmp("-quiet",argv[i])) flag_quiet=1;
	  if (!strcmp("-band",argv[i])) { 
	    sprintf(band,"%s",argv[i+1]);
	    flag_getband=1;
	    if (!strcmp("g",argv[i+1])) flag_band=GBAND;
	    if (!strcmp("r",argv[i+1])) flag_band=RBAND;
	    if (!strcmp("i",argv[i+1])) flag_band=IBAND;
	    if (!strcmp("z",argv[i+1])) flag_band=ZBAND;
	    if (!strcmp("Y",argv[i+1])) flag_band=YBAND;
	  }
	  if (!strcmp("-ccd",argv[i])) {
	    flag_ccd=1;
	    sscanf(argv[i+1],"%d",&flag_ccd);
	  }
          if (!strcmp("-full_nite",argv[i])) {
	    full_nite=1;
	  }
	  if (!strcmp("-staronly",argv[i])) {
	    flag_star=1;
	    sscanf(argv[i+1],"%f",&inclass);
	  }
	  if (!strcmp("-galaxyonly",argv[i])) {
	    flag_galaxy=1;
	    sscanf(argv[i+1],"%f",&inclass);
	  }
	  if (!strcmp("-nite",argv[i])) {
	    sscanf(argv[i+1],"%s",nite);
	    flag_nite=1;
	  }
	  if (!strcmp("-magtype",argv[i])) {
	    sscanf(argv[i+1],"%d",&flag_mag);
	  }
	  if (!strcmp("-objtable",argv[i])) 
	    sscanf(argv[i+1],"%s",objtable_in);

	  if (!strcmp("-runid",argv[i])) {
	    sscanf(argv[i+1],"%s",runid);
	    flag_runid=1;
	  }
	  if (!strcmp("-err_cut",argv[i])) {
	    sscanf(argv[i+1],"%f",&err_cut);
	  }
	  if (!strcmp("-propermotion",argv[i]))flag_propermotion=1;
	  if (!strcmp("-truth",argv[i])) {
	    if (!strcmp(argv[i+1],"stripe82")) flag_truth=STRIPE82;
	    else if (!strcmp(argv[i+1],"catsim1")) flag_truth=CATSIM1;
	    else if (!strcmp(argv[i+1],"imsim2_truth")) flag_truth=IMSIM2T;
	    else if (!strcmp(argv[i+1],"usnob2")) flag_truth=USNOB2;
	    else if (!strcmp(argv[i+1],"standard_stars")) flag_truth=STANDARD;
	    else if (!strcmp(argv[i+1],"catsim3_truth")) flag_truth=CATSIM3;
	    else if (!strcmp(argv[i+1],"catsim4_truth")) flag_truth=CATSIM4;
            else if (!strcmp(argv[i+1],"aug09_gsn")) flag_truth=AUG09_GSN;
	    else if (!strcmp(argv[i+1],"catsim5_truth")) flag_truth=CATSIM5;

	    else {
	      printf("  ** Wrong input of truth table, abort!\n");
	      exit(0);
	    }
	  }
	  if (!strcmp("-random",argv[i])) {
	    flag_random=1;
	    sscanf(argv[i+1],"%d",&random);
	  }
	  if (!strcmp("-noquery",argv[i])) flag_noquery=1;
	  if (!strcmp("-noobjectquery",argv[i])) flag_noobjectquery=1;
          if (!strcmp("-notruthquery",argv[i])) flag_notruthquery=1;
	  if (!strcmp("-mkrootplot",argv[i])) flag_mkrootplot=1;
	}

	/* set input magtype */
        if(flag_mag==-1) {
          sprintf(magin,"a.mag_model"); sprintf(magerrin,"%s","a.magerr_model");
        }
        else if(flag_mag==-2) {
          sprintf(magin,"a.mag_disk"); sprintf(magerrin,"%s","a.magerr_disk");
        }
        else if(flag_mag==-3) {
          sprintf(magin,"a.mag_spheroid"); sprintf(magerrin,"%s","a.magerr_spheroid");
        }
        else if(flag_mag==-4) {
          sprintf(magin,"a.mag_psf"); sprintf(magerrin,"%s","a.magerr_psf");
        }
        else if(flag_mag==-5) {
          sprintf(magin,"a.mag_iso"); sprintf(magerrin,"%s","a.magerr_iso");
        }
        else if(flag_mag==-6) {
          sprintf(magin,"a.mag_petro"); sprintf(magerrin,"%s","a.magerr_petro");
        }
        else if(flag_mag==0) {
          sprintf(magin,"a.mag_auto"); sprintf(magerrin,"%s","a.magerr_auto");
        }
        else if(flag_mag>1) {
          sprintf(magin,"a.mag_aper_%d",flag_mag);
          sprintf(magerrin,"a.magerr_aper_%d",flag_mag);
        }
        else {
          printf(" ** %s error: wrong input of <magtype> for -magtype\n",
                 argv[0]);
          exit(0);
	}
        

	/*  set tables for comparison */
	if (flag_truth==CATSIM1) {
	  sprintf(truthtable,"CatSim1_truth");
	  sprintf(truthquery,"SELECT ra,dec,g_mag,r_mag,i_mag,z_mag,class FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RA between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DEC between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* class_star separation */
	  //if(flag_star) sprintf(truthquery,"%sand class='S'\n",truthquery);
	  //if(flag_galaxy) sprintf(truthquery,"%sand class='G'\n",truthquery);
	  /* add a cut on magnitude */
	 /*  if (flag_band==GBAND) sprintf(truthquery,"%sand g_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand r_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand i_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand z_mag<22.5 ;\n",truthquery); */
	}

	if (flag_truth==STRIPE82) {
	  sprintf(truthtable,"des_stripe82_stds_v1");
	  sprintf(truthquery,"SELECT radeg,decdeg,stdmag_g,stdmag_r,stdmag_i,stdmag_z FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RADEG between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DECDEG between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* add a cut on magnitude */
	 /*  if (flag_band==GBAND) sprintf(truthquery,"%sand stdmag_g<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand stdmag_r<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand stdmag_i<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand stdmag_z<22.5 ;\n",truthquery); */
	}

	if (flag_truth==IMSIM2T) {
	  sprintf(truthtable,"Imsim2_Truth");
	  sprintf(truthquery,"SELECT ra,dec,g_mag,r_mag,i_mag,z_mag,class FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RA between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DEC between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* class_star separation */
	  //if(flag_star) sprintf(truthquery,"%sand class='S'\n",truthquery);
	  //if(flag_galaxy) sprintf(truthquery,"%sand class='G'\n",truthquery);
	  /* add a cut on magnitude */
	 /*  if (flag_band==GBAND) sprintf(truthquery,"%sand g_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand r_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand i_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand z_mag<22.5 ;\n",truthquery); */
	}

	if (flag_truth==USNOB2) {
	  sprintf(truthtable,"USNOB_CAT1");
	  sprintf(truthquery,"SELECT ra,dec,R1,R1,R1,R1 FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RA between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DEC between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* add a cut on magnitude */
	 /*  if (flag_band==GBAND) sprintf(truthquery,"%sand R1<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand R1<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand R1<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand R1<22.5 ;\n",truthquery); */
	}

	if (flag_truth==STANDARD) {
	  sprintf(truthtable,"standard_stars");
	  sprintf(truthquery,"SELECT radeg,decdeg,stdmag_g,stdmag_r,stdmag_i,stdmag_z FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RADEG between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DECDEG between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* add a cut on magnitude */
	 /*  if (flag_band==GBAND) sprintf(truthquery,"%sand stdmag_g<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand stdmag_r<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand stdmag_i<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand stdmag_z<22.5 ;\n",truthquery); */
	}

	if (flag_truth==CATSIM3) {
	  sprintf(truthtable,"catsim3_truth");
	  sprintf(truthquery,"SELECT ra,dec,g_mag,r_mag,i_mag,z_mag,class FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RA between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DEC between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* class_star separation */
	  //if(flag_star) sprintf(truthquery,"%sand class='S'\n",truthquery);
	  //if(flag_galaxy) sprintf(truthquery,"%sand class='G'\n",truthquery);
	  /* add a cut on magnitude */
	 /*  if (flag_band==GBAND) sprintf(truthquery,"%sand g_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand r_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand i_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand z_mag<22.5 ;\n",truthquery); */
	}

	if (flag_truth==CATSIM4) {
	  sprintf(truthtable,"DC4_Truth");
	  sprintf(truthquery,"SELECT ra,dec,g_mag,r_mag,i_mag,z_mag,Y_mag,class FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RA between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DEC between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* class_star separation */
	  //if(flag_star) sprintf(truthquery,"%sand class='S'\n",truthquery);
	  //if(flag_galaxy) sprintf(truthquery,"%sand class='G'\n",truthquery);
	  /* add a cut on magnitude */
	 /*  if (flag_band==GBAND) sprintf(truthquery,"%sand g_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand r_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand i_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand z_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==YBAND) sprintf(truthquery,"%sand Y_mag<22.5 ;\n",truthquery); */
	}

	if (flag_truth==AUG09_GSN) {
	  sprintf(truthtable,"GSN_Aug09_truth");
	  sprintf(truthquery,"SELECT ra,dec,g_mag,r_mag,i_mag,z_mag,Y_mag,class FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RA between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DEC between %.7f and %.7f)\n",
	    truthquery,decmin,decmax);
	  /* class_star separation */
	  //if(flag_star) sprintf(truthquery,"%sand class='S'\n",truthquery);
	  //if(flag_galaxy) sprintf(truthquery,"%sand class='G'\n",truthquery);
	  /* add a cut on magnitude */
	  /* if (flag_band==GBAND) sprintf(truthquery,"%sand g_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand r_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand i_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand z_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==YBAND) sprintf(truthquery,"%sand Y_mag<22.5 ;\n",truthquery); */
	}



	if (flag_truth==CATSIM5) {
	  sprintf(truthtable,"DC5_Truth");
	  sprintf(truthquery,"SELECT ra_dbl,dec_dbl,g_mag,r_mag,i_mag,z_mag,Y_mag,class,nvl(mura,0),nvl(mudec,0) FROM %s\n",
	    truthtable);
	  sprintf(truthquery,"%sWHERE (RA between %.7f and %.7f)\n",
	    truthquery,ramin,ramax);
	  sprintf(truthquery,"%sand (DEC between %.7f and %.7f);\n",
	    truthquery,decmin,decmax);
	  /* class_star separation */
	  //if(flag_star) sprintf(truthquery,"%sand class='S'\n",truthquery);
	  //if(flag_galaxy) sprintf(truthquery,"%sand class='G'\n",truthquery);
	  /* add a cut on magnitude */

          /* if (flag_band==GBAND) sprintf(truthquery,"%sand g_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==RBAND) sprintf(truthquery,"%sand r_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==IBAND) sprintf(truthquery,"%sand i_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==ZBAND) sprintf(truthquery,"%sand z_mag<22.5 ;\n",truthquery); */
/* 	  if (flag_band==YBAND) sprintf(truthquery,"%sand Y_mag<22.5 ;\n",truthquery); */
	}


	/* set the object table */
	sprintf(objecttable,"%s",objtable_in);

        /* set up generic db call */
        sprintf(sqlcall,"sqlplus -S %s < grabmatches.sql > grabmatches.tmp",dblogin);
        printf("%s\n",sqlcall);

	/* construct a query */
	if (!flag_quiet && !flag_noquery) printf("  Constructing a query to return objects\n");
	out=fopen("grabmatches.sql","w");
	fprintf(out,"SET ECHO OFF NEWP 0 SPA 1 PAGES 0 FEED OFF ");
        fprintf(out,"HEAD OFF TRIMS ON LINESIZE 1000;\n");
	fprintf(out,"SET NUMWIDTH 20; \n");
        fprintf(out,"SET TERMOUT OFF;\n");

	/* TRUTH table query */
        if(!flag_notruthquery) {
          fprintf(out,"SPOOL grabmatches.truth;\n");
          fprintf(out,"%s",truthquery);
          fprintf(out,"SPOOL OFF;\n");
        }

	/* OBJECT table query */
	if (!flag_noobjectquery) {

          if(!full_nite) {
            fprintf(out,"SPOOL grabmatches.objects;\n");
            fprintf(out,"SELECT a.alphamodel_j2000,a.deltamodel_j2000,"
                    "c.mjd_obs,%s,%s,a.spread_model,a.band,"
                    "to_char(a.ERRX2_WORLD,'999.9999999999999999'),"
                    "to_char(a.ERRY2_WORLD,'999.9999999999999999'),b.ccd,"
                    "a.x_image,a.y_image,a.flags FROM %s a,IMAGE b, "
                    "exposure c WHERE ",
                    magin,magerrin,objecttable);
            fprintf(out,"(a.ra between %.7f and %.7f) ",
                    ramin,ramax);
            fprintf(out,"and (a.dec between %.7f and %.7f) ",
                    decmin,decmax);
            fprintf(out," and a.flags<%d",flag);
            if(flag_getband) {
              if(flag_band==GBAND) fprintf(out," and c.band='g' ");
              if(flag_band==RBAND) fprintf(out," and c.band='r' ");
              if(flag_band==IBAND) fprintf(out," and c.band='i' ");
              if(flag_band==ZBAND) fprintf(out," and c.band='z' ");
              if(flag_band==YBAND) fprintf(out," and c.band='Y' ");
            }
            if(flag_ccd) fprintf(out," and b.ccd=%d ",flag_ccd);
            if(flag_nite) fprintf(out," and c.nite='%s' ",nite);
            if(flag_runid) fprintf(out," and b.run = '%s_%s' ",runid,nite);
            if(flag_star) fprintf(out," and a.spread_model > %2.2f ",inclass);
            if(flag_galaxy) fprintf(out," and a.spread_model < %2.2f ",inclass);
            fprintf(out," and a.IMAGEID=b.ID ");
            fprintf(out," and b.exposureid=c.id ");
            if(flag_nite) fprintf(out, "and c.nite = b.nite ");
            if(flag_getband) fprintf(out, "and c.band = b.band ");
            if(flag_getband) fprintf(out, "and b.band = a.band ");
          }
          else {
            fprintf(out,"SPOOL grabmatches.objects;\n");
            fprintf(out,"select a.alphamodel_j2000,a.deltamodel_j2000,"
                    "%s,%s,a.spread_model,a.band,"
                    "to_char(a.ERRX2_WORLD,'999.9999999999999999'),"
                    "to_char(a.ERRY2_WORLD,'999.9999999999999999'),"
                    "a.x_image,a.y_image,a.flags "
                    "FROM %s a "
                    "WHERE a.partkey = 'TMP_%s_%s'",
                    magin,magerrin,objecttable,runid,nite);
          }            
	}
        fprintf(out,";\nSPOOL OFF;\nexit;\n");
	//fprintf(out," order by a.ALPHAMODEL_J2000;\nSPOOL OFF;\nexit;\n");
        //fprintf(out,";\nSPOOL OFF;\nexit;\n");
	fclose(out);
        
	if (!flag_quiet && !flag_noquery) {
	  printf("  Executing query of tables %s and %s\n",
		 truthtable,objecttable);
	}
	time(&startTime);
       	if (!flag_noquery) system(sqlcall);
	time(&endTime);
	printf(" Finished doing sql queries in %d seconds", (int) (endTime -startTime));
	/* count the table */
	sprintf(command,"wc grabmatches.truth");
	pip=popen(command,"r");
	fscanf(pip,"%d",&ntruth);
	pclose(pip);
	if (!ntruth) {
	  printf("  ** There are no sources found in this region in %s\n",
                 truthtable);
	  exit(0);
	}
	else if (!flag_quiet) {
	  printf("  Reading %d objects in truth tables\n",ntruth);
	}
	
        tra=(double *)calloc(ntruth,sizeof(double));
	sra=(float *)calloc(ntruth,sizeof(float));
	tdec=(double *)calloc(ntruth,sizeof(double));
	tg=(float *)calloc(ntruth,sizeof(float));
	tr=(float *)calloc(ntruth,sizeof(float));
	ti=(float *)calloc(ntruth,sizeof(float));
	tz=(float *)calloc(ntruth,sizeof(float));
	ty=(float *)calloc(ntruth,sizeof(float));
	tclass=(float *)calloc(ntruth,sizeof(float));
	tmatch=(int *)calloc(ntruth,sizeof(int));
	tindex=(unsigned long *)calloc(ntruth,sizeof(unsigned long));
	mura = (float *)calloc(ntruth,sizeof(float));
	mudec = (float *)calloc(ntruth,sizeof(float));
                
	/* read tables and start matching */
	inp=fopen("grabmatches.truth","r");
	i=0;

        while (fscanf(inp,"%lf %lf %f %f %f %f %f %s %f %f\n",tra+i,tdec+i,tg+i,
                      tr+i,ti+i,tz+i,ty+i,tmp,mura+i,mudec+i)!=EOF) {
          tindex[i]=i;sra[i]=tra[i];
          if (!strcmp(tmp,"G")) tclass[i]=0;
          else tclass[i]=1;
	  tmatch[i]=0;
	  i++;
        }
        fclose(inp);
	
	if (!flag_quiet) {
	  printf("  Indexing truth table\n");
	  fflush(stdout);
	}
	/* now sort the truth table by ra to make searches more efficient */
	indexx(ntruth,sra-1,tindex-1);
	for (i=0;i<ntruth;i++) tindex[i]-=1;
	
	/* test indexing */
/*
	for (i=0;i<10;i++) {
	  printf("(%11.7f,%11.7f) (%11.7f,%11.7f) (%11.7f,%11.7f)\n",
	    tra[tindex[i]],tdec[tindex[i]],
	    tra[tindex[i+1000]],tdec[tindex[i+1000]],
	    tra[tindex[i+20000]],tdec[tindex[i+20000]]);
	}
*/
	
        system("sort -k1,1n -k2,2n grabmatches.objects>grabmatches.objects_ordered");

	/* now read through object list finding matches */
	if (!flag_quiet) {
	  printf("  Reading %s table\n",objecttable);
	  fflush(stdout);
	}
	inp=fopen("grabmatches.objects_ordered","r");
	out=fopen("grabmatches.matches","w");
	
	if  (inp==NULL) {
	  printf("  'grabmatches.objects_ordered' empty or not found\n");
	  exit(0);
	}
	nobjects=matches=nomatches=0;
	loclow=1; k=0;
	while (1) {
          if(!full_nite) {
            if(fscanf(inp,"%lf %lf %lf %f %f %f %s %lf %lf %d %lf %lf %d",
                      &ora,&odec,&omjd,&omag,&omagerr,&oclass,oband,&errx2,&erry2,
                      &ccd,&ox,&oy,&oflags)==EOF) break;
          } else {
            if(fscanf(inp,"%lf %lf %f %f %f %s %lf %lf %lf %lf %d",
                      &ora,&odec,&omag,&omagerr,&oclass,oband,&errx2,&erry2,
                      &ox,&oy,&oflags)==EOF) break;
            ccd=-1;
            omjd=-1;
          }
          
	  nobjects++;
	  if (flag_propermotion)deltayear = (omjd - 51544) / 365.25 ;
	    
	  /*if (!flag_quiet) printf(" %d  %d matches  %.7f %.7f\n",nobjects,
	    matches,ora,odec);*/


	  if (nobjects%5000==0) if (!flag_quiet) {
	      printf("  %d matches / %d objects  %d\n",
		     matches,nobjects,loclow);
	    }

	  
	  /* find nearest neighbor */
	  scale=cos(odec*M_PI/180.0);
	  ramin=ora-TOLERANCE/scale;
	  i=loclow;lochi=-1;
	  if (loclow==0) i=1;
	  while (lochi==-1) {
	    if (tra[tindex[i]]>ramin && tra[tindex[i-1]]<=ramin) {
	      loclow=lochi=i-1;
	      if (loclow<0) loclow=lochi=0;
	    }
	    else { /* adjust the location */
	      if (tra[tindex[i]]<=ramin) i++;
	      else i--;
	    }
	    if (i==0) loclow=lochi=0;
	    if (i>=ntruth-1) loclow=lochi=ntruth-1;
	  }
	  ramax=ora+TOLERANCE/scale;
	  for (i=loclow;i<ntruth;i++) if (tra[tindex[i]]>ramax) break;
	  lochi=i; if (lochi>=ntruth) lochi=ntruth-1;
	  //mindist=Squ(TOLERANCE);
          mindist=TOLERANCE;
	  minmag=Squ(PHOTO_TOLERANCE);
	  locmatch=-1;
	  for (i=loclow;i<=lochi;i++) {

	    if (!strcmp(oband,"g")) truemag=tg[tindex[i]]; 
	    if (!strcmp(oband,"r")) truemag=tr[tindex[i]];
	    if (!strcmp(oband,"i")) truemag=ti[tindex[i]];
	    if (!strcmp(oband,"z")) truemag=tz[tindex[i]]; 
	    if (!strcmp(oband,"Y")) truemag=ty[tindex[i]]; 

	    /* if dec offset is acceptable then calculate distance */
	    if (fabs(tdec[tindex[i]]-odec)<TOLERANCE) {

	      //dist=calcdistance(tra[tindex[i]],tdec[tindex[i]],ora,odec);
	      calcdistance_err(tra[tindex[i]],tdec[tindex[i]],ora,odec,
			       errx2,erry2,&dist,&disterr);
	      deltamag=fabs(omag-truemag);

              totaldiff=Squ(dist/disterr)+Squ(deltamag/omagerr);
              
	      /* if(totaldiff<(mindist+minmag)) { */
/* 		mindist=Squ(dist/disterr); */
/* 		minmag=Squ(deltamag/omagerr); */
/* 		locmatch=i; */
/* 	      } */
	      if (dist<mindist) {
              
                mindist=dist;
                locmatch=i;
                
	      }
	    }
	  }


	  if (locmatch==-1) {
	    nomatches++;
	    /*if (nomatches%10==0) printf("-");*/

	  }
	  else {
	    tmatch[tindex[locmatch]]=1;
	    matches++;/*if (matches%10==0) printf("+");*/
	    char class;
	    if(tclass[tindex[locmatch]]) class='S';
	    else class='G';
	    
	    if (!strcmp(oband,"g")) { truemag=tg[tindex[locmatch]]; flag_band=GBAND; }
	    if (!strcmp(oband,"r")) { truemag=tr[tindex[locmatch]]; flag_band=RBAND; }
	    if (!strcmp(oband,"i")) { truemag=ti[tindex[locmatch]]; flag_band=IBAND; }
	    if (!strcmp(oband,"z")) { truemag=tz[tindex[locmatch]]; flag_band=ZBAND; }
	    if (!strcmp(oband,"Y")) { truemag=ty[tindex[locmatch]]; flag_band=YBAND; }

	    if(flag_random) {
	      if(k%random==0) {
		if(fabs(omag-truemag)<50.0)
		  {    
		    newtra=tra[tindex[locmatch]] + 
		      mura[tindex[locmatch]]*MARCTODEG*deltayear/cos(tdec[tindex[locmatch]]*M_PI/180);
		    newtdec=tdec[tindex[locmatch]] + mudec[tindex[locmatch]]*MARCTODEG*deltayear ;		
	      		  fprintf(out,"%.7f %.7f  %.4f  %.7f %.7f   %.7f %.10f %.7f %.10f %.4f %.4f   %s %.2f %d %c %d %8.2f %8.2f %d\n",
			  newtra,newtdec,
			  truemag,scale*(ora-newtra),
			  odec-newtdec,ora,sqrt(errx2),
			  odec,sqrt(erry2),omag,omagerr,oband,oclass,flag_band,
			  class,ccd,ox,oy,oflags);
		  }
	      }
	    }
	    else {
	      if(fabs(omag-truemag)<50.0)
		{
		  newtra=tra[tindex[locmatch]] + mura[tindex[locmatch]]*MARCTODEG*deltayear/cos(tdec[tindex[locmatch]]*M_PI/180);
		  newtdec=tdec[tindex[locmatch]] + mudec[tindex[locmatch]]*MARCTODEG*deltayear ;						   
		fprintf(out,"%.7f %.7f  %.4f  %.7f %.7f   %.7f %.10f %.7f %.10f %.4f %.4f   %s %.2f %d %c %d %8.2f %8.2f %d\n",
			newtra,newtdec,
			truemag,scale*(ora-newtra),
			odec-newtdec,ora,sqrt(errx2),
			odec,sqrt(erry2),omag,omagerr,oband,oclass,flag_band,
			class,ccd,ox,oy,oflags);
	        }
	    }
	  }
	  k++;
 	}
	fclose(inp);fclose(out);
	/* output all the unmatched truth table objects */
	outnm=fopen("grabmatches.truth_nomatches","w");
	for (i=0;i<ntruth;i++) {
	  if (!tmatch[i])
	    fprintf(outnm,"%.7f %.7f   %.4f %.4f %.4f %.4f %.4f    %.2f\n",
		    tra[i],tdec[i],tg[i],tr[i],ti[i],tz[i],ty[i],tclass[i]);
	}

	fclose(outnm);
	printf("  Found %d matches out of %d objects (%d objects in truth table)\n",
	       matches,nobjects,ntruth);

	if(flag_mkrootplot) {
	  char label[100] ;
	  if(flag_mag==0) sprintf(label,"auto");
          else if(flag_mag==-1) sprintf(label,"model");
          else if(flag_mag==-2) sprintf(label,"dist");
          else if(flag_mag==-3) sprintf(label,"spheroid");
          else if(flag_mag==-4) sprintf(label,"psf");
          else if(flag_mag==-5) sprintf(label,"iso");
          else if(flag_mag==-6) sprintf(label,"petro");
          else if(flag_mag>0) sprintf(label, "aper%d",flag_mag);
          if(!full_nite) {
            sprintf(command,"ImageProc/trunk/src/data_qual -inputfile grabmatches.matches -truthfile grabmatches.truth -title \'Single Epoch\' -coadd 0 -band %s -label %s -err_cut %f",oband,label,err_cut);
          }
          else {
              sprintf(command,"ImageProc/trunk/src/data_qual -inputfile grabmatches.matches -truthfile grabmatches.truth -title \'Single Epoch\' -coadd 0 -band %s -label %s -err_cut %f -algs phot,astro,comp",oband,label,err_cut);
          }
	  time(&startTime);
	  system(command);
	  time(&endTime);	  
	  printf ("Finished doing plotting and analysis in %d seconds", (int) (endTime - startTime)); 
	}



	/* ********************************* */
	/* *** PLOTTING THE RESULTS HERE *** */
	/* ********************************* */

	dra = (double *)calloc(matches,sizeof(double));
	ddec = (double *)calloc(matches,sizeof(double));
	dmag = (double *)calloc(matches,sizeof(double));
	tmag_match = (double *)calloc(matches,sizeof(double));
	omagerr_match = (double *)calloc(matches,sizeof(double));
	omag_match = (double *)calloc(matches,sizeof(double));
		

	/* read in the data */
	fin=fopen("grabmatches.matches","r");
	for(i=0;i<matches;i++) {
	  fscanf(fin,"%lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %lg %s %f %d %c %d %lg %lg",
		 &dtemp,&dtemp,&tmag_match[i],&dra_in,&ddec_in,&dtemp,&
		 dtemp,&dtemp,&dtemp,&omag_match[i],&dtemp,&omagerr_match[i],
		 stemp,&temp,&itemp,&ctemp,&itemp,&dtemp,&dtemp);
	  dmag[i]=omag_match[i]-tmag_match[i];
	  dra[i]=dra_in*3600.0; /* convert to arcsec */
	  ddec[i]=ddec_in*3600.0;
	}
	fclose(fin);

	plot_ast(matches,dra,ddec);
	plot_pho(matches,tmag_match,dmag);



	free(dra); free(ddec); free(dmag); free(tmag_match); free(omagerr_match); 
}



float calcdistance(ra1,dec1,ra2,dec2)
	double ra1,dec1,ra2,dec2;
{
	double	distance,scale;
	float	outdist;
	
	scale=cos(0.5*(dec1+dec2)*M_PI/180.0);
	distance=Squ(scale*(ra1-ra2))+Squ((dec1-dec2));
	

	/* bug..?? */
	//if (distance>1.0e-20) outdist=sqrt(distance);
	//else outdist=distance;
	//return(distance);

	return(sqrt(distance));
}

void calcdistance_err(double ra1, double dec1,double ra2,double dec2,double errx2,double erry2,double *dist,double *disterr) 
{
  double  distance,distanceerr,scale;

  scale=cos(0.5*(dec1+dec2)*M_PI/180.0);
  distance=sqrt(Squ(scale*(ra1-ra2))+Squ((dec1-dec2)));
  distanceerr=sqrt(pow(scale,4.0)*Squ((ra1-ra2))*errx2 + Squ((dec1-dec2))*erry2)/distance;

  //printf("DRA\t%2.6f\n",scale*(ra1-ra2)*3600);
  //printf("DDEC\t%2.6f\n",(dec1-dec2)*3600);
  //printf("d'= \t%2.9f\t%2.6f\n",distance,distance*3600);
  //printf("sd'= \t%2.9f\t%2.6f\n",distanceerr,distanceerr*3600);
 
  *dist=distance;
  *disterr=distanceerr;
}

void plot_ast(int N, double dra[], double ddec[])
{
  PLFLT plmin,plmax;

  /* make histogram plot */
  plsfnam("grabmatches.ast_histogram.jpeg"); /* hardwire the output name at the moment */
  plsdev("jpeg"); /* hardwire the output type at the moment */

  plscolbg(255,255,255);  /* Force the background colour to white */
  plscol0(15, 0,0,0);     /* Force the foreground colour to black */

  plstar(1,2);
  plsori(0);
  plcol0(15); 

  plmin=-(TOLERANCE*3600.0)/2.0+0.25;
  plmax= (TOLERANCE*3600.0)/2.0-0.25;
  
  plot_histogram(N,dra,0.015,plmin,plmax,"#gD RA [arcsec]");
  plot_histogram(N,ddec,0.015,plmin,plmax,"#gD DEC [arcsec]");
  
  plend(); /* close the plplot */

  /* make scatter plot */
  plsfnam("grabmatches.ast_scatter.jpeg"); /* hardwire the output name at the moment */
  plsdev("jpeg"); /* hardwire the output type at the moment */

  plscolbg(255,255,255);  /* Force the background colour to white */
  plscol0(15, 0,0,0);     /* Force the foreground colour to black */

  plinit();
  plsori(0);
  plcol0(15); 

  plmin=plmax=0.0;
  plmin=-TOLERANCE*3600.0-0.5;
  plmax= TOLERANCE*3600.0+0.5;
  
  plot_scatter(N,dra,ddec,plmin,plmax,plmin,plmax,"#gD RA [arcsec]","#gD DEC [arcsec]","",1);
    
  plend(); /* close the plplot */
}

void plot_pho(int N, double tmag[], double dmag[])
{
  PLFLT plmin,plmax;
  int i;

  /* make histogram plot */
  plsfnam("grabmatches.photometry.jpeg"); /* hardwire the output name at the moment */
  plsdev("jpeg"); /* hardwire the output type at the moment */

  plscolbg(255,255,255);  /* Force the background colour to white */
  plscol0(15, 0,0,0);     /* Force the foreground colour to black */

  plstar(1,2);
  plsori(0);
  plcol0(15); 

  plmin=plmax=18.0;
  for(i=0;i<N;i++) {
    if(tmag[i]>plmax) plmax=tmag[i];
    if(tmag[i]<plmin) plmin=tmag[i];
  }
  plmin-=0.25;
  plmax+=0.25;

  plenv(plmin, plmax, -0.15, 0.15,0,0);
  plpoin((PLINT)N,tmag,dmag,1);
  pllab("mag(true)","mag(obs) - mag(true)",""); /* labelling */

  plmin=-0.15;
  plmax=0.15; 
  plot_histogram(N,dmag,0.015,plmin,plmax,"mag(obs) - mag(true)");
  
  plend(); /* close the plplot */
}

void plot_scatter(int N, double x[], double y[],PLFLT xmin, PLFLT xmax, PLFLT ymin, PLFLT ymax, char *xlabel, char *ylabel, char *label, int just)
{
  plenv(xmin, xmax, ymin, ymax,just,0);
  plpoin((PLINT)N,x,y,1);
  pllab(xlabel,ylabel,label); /* labelling */
}

void plot_histogram(int N, double x[], double deltabin, PLFLT xmin, PLFLT xmax, char *xlabel)
{
  int nbin;
  
  /* number of bins */
  nbin=(int)((xmax-xmin)/deltabin);
  plhist((PLINT)N,x,xmin,xmax,nbin,PL_HIST_DEFAULT);
  pllab(xlabel,"Count",""); /* labelling */
}


#undef STRIPE82 
#undef CATSIM1  
#undef IMSIM2T  
#undef USNOB2   
#undef STANDARD 
#undef CATSIM3  
#undef CATSIM4  

#undef TOLERANCE
#undef PHOTO_TOLERANCE 
#undef VERSION
