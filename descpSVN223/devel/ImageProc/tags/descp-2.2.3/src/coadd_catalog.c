/*$Id: coadd_catalog.c 6645 2011-04-18 19:36:59Z mgower $*/
/**
** coadd_catalog.c
**
** DESCRIPTION:
**     Run sextractor in double image mode for cataloguing
**     
**
** NOTE:
**    Need to add PSFex stuff later
**
**
** Last commit:
**     $Rev: 6645 $
**     $LastChangedBy: mgower $
**     $LastChangedDate: 2011-04-18 12:36:59 -0700 (Mon, 18 Apr 2011) $
**
*/

#include "imageproc.h"

main(argc,argv)
     int argc;
     char *argv[];
{
  char tilename[1000],listname[1000],outpath[1000],line[5000],*position;
  char command[3000],comment[1000],event[1000],root[1000],psffile[1000],psfoutfile[1000];
  char terapixpath[1000],etcpath[1000],paramfile[2000];
  char detection_image[1000],detection_weight_image[1000],band[5],outimage[1000],newdetimage[1000];
  char imagename[1000],weightname[1000],outcat[1000],newname[1000];
  char **image;
  FILE *inp,*inp2 ;
  int i,status=0,Nimage=0,ID,len,use_fpack=0,j,flag_sersic=0;
  int flag_list=0,flag_outpath=0,flag_tilename=0,flag_detimage=0,flag_nopsf=0;
  int flag_verbose=2,flag_terapixpath=0,flag_etcpath=0,flag_psf=0;
  int flag_modelfit=0;
  float magzp;

  FILE *pip,*fin;
  fitsfile *fptr;

  /* external functions call */
  int  mkpath();
  void reportevt(),get_input(),printerror();
  
  /* *** Output usage *** */
  if (argc<2) {
    printf("Usage: %s <required inputs> <optional inputs>\n",argv[0]);
    printf("    Required Inputs:\n");
    printf("       -list <list of coadd images for creating detection image, format: COADD_IMAGE COADD_ID>  \n");
    printf("       -tilename <tilename>  \n");
    printf("       -outpath <outpath>  \n");
    printf("       -det_image <detection image>  \n");
    printf("    Optional Inputs:\n");
    printf("       -terapixpath <full terapixpath>\n");
    printf("       -etcpath <full etcpath>\n");
    printf("        -sersic\n"); 
    printf("     -nopsf\n");
    printf("       -modelfitting \n");
    //printf("       -psfex <NOT SURE WHAT GOES HERE>\n");
    printf("       -verbose <0-3>\n");
    printf("\n");
    exit(0);
  }

  /* *** Process command line *** */
  for (i=2;i<argc;i++) {
    if (!strcmp(argv[i],"-verbose")) {
      sscanf(argv[++i],"%d",&flag_verbose);
      if (flag_verbose<0 || flag_verbose>3) {
        sprintf(event,"Verbose level out of range %d. Reset to 2",flag_verbose);
        flag_verbose=2;
        reportevt(flag_verbose,STATUS,3,event);
      }
    }
  }

  /* now go through again */
  for(i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-list")) {
      flag_list=1;
      get_input(argv[i],argv[i+1],"-list",listname,flag_verbose);
    }

    if (!strcmp(argv[i],"-tilename")) {
      flag_tilename=1;
      get_input(argv[i],argv[i+1],"-tilename",tilename,flag_verbose);
    }

    if (!strcmp(argv[i],"-outpath")) {
      flag_outpath=1;
      get_input(argv[i],argv[i+1],"-outpath",outpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-sersic")) {
      flag_sersic=1;
      sprintf(event,"Doing Sersic model-fitting with Sextractor");
      reportevt(flag_verbose,STATUS,1,event);
    }


    /* options */


    if (!strcmp(argv[i],"-det_image")) {
      flag_detimage=1;
      get_input(argv[i],argv[i+1],"-det_image",detection_image,flag_verbose);
      inp=fopen(detection_image,"r");
      if (inp==NULL)
	{sprintf(newdetimage,"%s.fz",detection_image) ;
	  inp2=fopen(newdetimage,"r");
	  if (inp2==NULL) {reportevt(flag_verbose,STATUS,5,"could not open detection image"); exit(0);}
	  else
	    {

	  sprintf(command,"funpack %s",detection_image);
	  sprintf(event,"Funpack'ing coadd_det image  %s",detection_image);
	  reportevt(flag_verbose,STATUS,3,event);
	  system(command);
	    }
	}
      sprintf(detection_weight_image,"%s[1]",detection_image);
      sprintf(detection_image,"%s[0]",detection_image);
    }

    if (!strcmp(argv[i],"-terapixpath")) {
      flag_terapixpath=1;
      get_input(argv[i],argv[i+1],"-terapixpath",terapixpath,flag_verbose);
    }

    
    if (!strcmp(argv[i],"-etcpath")) {
      flag_etcpath=1;
      get_input(argv[i],argv[i+1],"-etcpath",etcpath,flag_verbose);
    }

    if (!strcmp(argv[i],"-modelfitting")){
      flag_modelfit=1;
      printf ("modelfitting flag set to 1 : doing model-fitting photometry\n");
      sprintf(paramfile,"sex.param");
    } 
    if (!strcmp(argv[i],"-nopsf")){
      flag_nopsf=1;
    } 
    //if (!strcmp(argv[i],"-psfex")) {
    //flag_psf=1;
    //get_input(argv[i],argv[i+1],"-psfex",?,flag_verbose);
    //}
  }
  if (flag_sersic)sprintf(paramfile,"sex.param_sersic");

  /* quit if required inputs are not set */
  if(!flag_tilename || !flag_list || !flag_outpath) {
    sprintf(event,"Required inputs are not set ");
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  } 

  /* get the number of images in the input list */
  sprintf(command,"wc -l %s",listname);
  pip=popen(command,"r");
  fscanf(pip,"%d",&Nimage);
  pclose(pip);

  sprintf(event," Input list %s contains %d images ",listname,Nimage);

  if(Nimage) 
    reportevt(flag_verbose,STATUS,2,event);
  else {
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }

  /* memory allocation and input the images */
  image=(char **)calloc(Nimage+1,sizeof(char *));
  for(i=1;i<=Nimage;i++)
    image[i]=(char *)calloc(1500,sizeof(char ));
  
  fin=fopen(listname,"r");
  for(i=1;i<=Nimage;i++)
    fscanf(fin,"%s %d",image[i],&ID);
  fclose(fin);
  
  /* make the output catalog path using first image */
  sprintf(outimage,"%s/%s_det.fits",outpath,tilename);
  
  if (mkpath(outimage,flag_verbose)) {
    sprintf(event,"Failed to create path %s",outpath);
    reportevt(flag_verbose,STATUS,5,event);
    exit(0);
  }
  else {
    sprintf(event,"Created path %s",outpath);
    reportevt(flag_verbose,STATUS,2,event);
  }

  /* *** Loop over image list and construct sextractor call *** */
  
  for(i=1;i<=Nimage;i++) {

    sprintf(imagename,"%s[0]",image[i]);
    sprintf(weightname,"%s[1]",image[i]);
 
    /* *** Get the band and ZP information from the image *** */

    /* open the image */
    inp=fopen(image[i],"r");
    if (inp==NULL)
      {
	sprintf(newname,"%s.fz",image[i]) ;
	inp2=fopen(newname,"r");
	if (inp2==NULL) {
	  reportevt(flag_verbose,STATUS,5,"could not open fits image"); exit(0);
	 }
      else
	{
	use_fpack=1;
	sprintf(command,"%s/funpack %s",terapixpath,newname);
	sprintf(event,"Funpack'ing coadd image  %s",newname);
	reportevt(flag_verbose,STATUS,3,event);
	system(command);
       }
      }
  
    if(fits_open_file(&fptr,image[i],READONLY,&status)) {
	sprintf(event,"Image open failed: %s",newname);
	reportevt(flag_verbose,STATUS,5,event);
	printerror(status);
	exit(0);
    }


    /* read filter info */
    status=0;
    if(fits_read_key_str(fptr,"FILTER",band,comment,&status)==KEY_NO_EXIST) {
      sprintf(event,"Image %s keyword filter missing",image[i]);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
      exit(0);
    }
    
    /* read mag_zp info */
    status=0;
    if(fits_read_key_flt(fptr,"SEXMGZPT",&magzp,comment,&status)==KEY_NO_EXIST) {
      sprintf(event,"Image %s keyword SEXMGZPT missing, using dummy ZP of 25 ",image[i]);
      reportevt(flag_verbose,STATUS,3,event);
      printerror(status);
      magzp=25.0; /* use dummy zeropoint */
    }

    /* close the image */
    status=0;
    if(fits_close_file(fptr,&status)) {
      sprintf(event,"Image %s close failed",image[i]);
      reportevt(flag_verbose,STATUS,5,event);
      printerror(status);
    }

    sprintf(event,"Image %s: band=%s ZP=%2.4f ",image[i],band,magzp);
    reportevt(flag_verbose,STATUS,2,event);

    sprintf(outcat,"%s/%s_%s_cat.fits",outpath,tilename,band);
    if (flag_modelfit)
      {
	sprintf(root,"%s",imagename);
	len=strlen(imagename);
	for (j=len;j>0;j--) if (!strncmp(&(imagename[j]),".fits",5)) {
            root[j]=0;
            break;
          }
	sprintf(psffile,"%s_psfcat.fits",root);
       sprintf(command,"%s/sex %s -c %s/default.sex -CATALOG_NAME %s -CATALOG_TYPE FITS_LDAC -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s -PARAMETERS_NAME %s/sex.param_psfex -FILTER_NAME %s/sex.conv  -STARNNW_NAME %s/sex.nnw -SATUR_LEVEL 65000 -DETECT_MINAREA 3 ",terapixpath,imagename,etcpath,psffile,weightname,etcpath,etcpath,etcpath);
        sprintf(event," First SExtractor Call %s ",command);
      reportevt(flag_verbose,STATUS,2,event);
      /*      system(command); */
           pip = popen(strcat(command,"2>&1"),"r"); 
           while (fgets(line,10000,pip)!=NULL) {
	     fputs(line,stdout);
	if (strstr(line,"Error")!=NULL) {
	  sprintf(event,"error found in first SExtractor call on image %s",imagename);
	  reportevt(flag_verbose,STATUS,5,event);
	}
	if (strstr(line,"Not enough memory")!=NULL){
	  sprintf(event,"Memory Problem in first SExtractor call on image %s",imagename);
          reportevt(flag_verbose,STATUS,5,event);
	}
      }
	   pclose(pip);
      sprintf(command,"%s/psfex %s  -c %s/default.psfex  -WRITE_XML Y  ",terapixpath,psffile,etcpath);
       sprintf(event," PSFex Call: %s ",command);
      reportevt(flag_verbose,STATUS,2,event);
            system(command); 
       }

    /* SExtractor call begin here */
    if(flag_terapixpath) sprintf(command,"%s/sex ",terapixpath);
    else sprintf(command,"sex ");

    /* run in double-image mode */
    if(flag_detimage) {
      sprintf(command,"%s %s,%s ",command,detection_image,imagename);
    }                  
    else
      {
	sprintf(command,"%s %s",command,imagename);
      }
    if (flag_nopsf) sprintf(command,"%s -SEEING_FWHM 1.2 ",command);
    if(flag_etcpath) sprintf(command,"%s -c %s/sex.config ",command,etcpath);
    else sprintf(command,"%s -c sex.config ",command);

    if(flag_etcpath) 
      sprintf(command, "%s -FILTER_NAME %s/sex.conv -STARNNW_NAME %s/sex.nnw ", 
	      command,etcpath,etcpath);
    else
      sprintf(command, "%s -FILTER_NAME sex.conv -STARNNW_NAME sex.nnw ",command);

    /* add the psf stuff here later */
    if (!flag_modelfit){
      if(flag_etcpath) 
	sprintf(command, "%s -PARAMETERS_NAME %s/sex.param_nopsfex ",command,etcpath);
      else
	sprintf(command, "%s -PARAMETERS_NAME sex.param_nopsfex ",command);
    }
    else
      {
	if(flag_etcpath) 
	  sprintf(command, "%s -PARAMETERS_NAME %s/%s",command,etcpath,paramfile);
      else
	sprintf(command, "%s -PARAMETERS_NAME %s",command,paramfile);
    }
    /* the rest */
    sprintf(command,"%s -CATALOG_TYPE FITS_1.0 -CATALOG_NAME %s ",command,outcat);
    if (flag_detimage){
    sprintf(command,"%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s,%s  -MEMORY_BUFSIZE 2048 ",command,detection_weight_image,weightname);
    }
    else
      {
	sprintf(command,"%s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s  -MEMORY_BUFSIZE 2048 ",command,weightname);
      }	
    sprintf(command,"%s -MAG_ZEROPOINT %2.4f ",command,magzp);
    if (flag_modelfit)
      { sprintf(psfoutfile,"%s_psfcat.psf",root);
	sprintf(command,"%s -PSF_NAME %s",command,psfoutfile);
      }


    if(!flag_verbose)
      sprintf(command,"%s -VERBOSE_TYPE QUIET ",command);
    else
      sprintf(command,"%s -VERBOSE_TYPE NORMAL ",command);
    
    sprintf(event," SExtractor Call: %s ",command);
    reportevt(flag_verbose,STATUS,2,event);
    
    /*    system(command); */
    pip = popen(strcat(command,"2>&1"), "r");

    while (fgets(line,1000,pip)!=NULL) {
      fputs(line,stdout);
      position = strstr(line,"Error");
      if (position!=NULL) {
	sprintf(event, "Error in second SExtractor call on image %s\n",imagename);
	reportevt(flag_verbose,STATUS,5,event);
      }
	if (strstr(line,"Not enough memory")!=NULL){
	  sprintf(event,"Memory Problem in second SExtractor call on image %s",imagename);
          reportevt(flag_verbose,STATUS,5,event);
	}
    }
    pclose(pip);
    if (use_fpack){sprintf(command,"rm %s",image[i]);
      sprintf(event,"Removing image %s",image[i]);
                   reportevt(flag_verbose,STATUS,1,event);
		   system(command);}
    use_fpack = 0;
  }

  /* free memory */
  free(image);
  return(0);
}
