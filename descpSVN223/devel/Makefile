#######################################################################
#
#  $Id: arcp 996 2008-05-12 20:31:51Z dadams $
#
#  $Rev:: 996                              $:  # Revision of last commit.
#  $LastChangedBy:: dadams                 $:  # Author of last commit. 
#  $LastChangedDate:: 2008-05-12 15:31:51 #$:  # Date of last commit.
#
#  Author: 
#         Darren Adams (dadams@ncsa.uiuc.edu)
#
# Top-Level Makefile for DESDM software stack.  Instructions on using
# this file are in the README file in this directory
#
########################################################################

# Default source locations:
ifndef BRANCH
BRANCH=branches/latest
endif

DEFAULT_DATA_BASE=Database/${BRANCH}
DEFAULT_IMAGEPROC_BASE=ImageProc/${BRANCH}
DEFAULT_IMSUPPORT_BASE=$(realpath imsupport/${BRANCH})
DEFAULT_IMDETREND_BASE=$(realpath imdetrend/${BRANCH})
DEFAULT_PHOTOZ_BASE=photoz/${BRANCH}
DEFAULT_ORCH_BASE=Orchestration/${BRANCH}
DEFAULT_APPMOD_BASE=AppModules/${BRANCH}
DEFAULT_WL_BASE=wl/${BRANCH}
DEFAULT_DIFFIMG_BASE=DiffImg/${BRANCH}
DEFAULT_COMMPIPE_BASE=CommPipe/${BRANCH}

# Install locations, assumes DES_HOME is defined:
#ifndef DES_HOME 
#$(error Must define the DES_HOME environment variable.) 
#endif
ifdef DES_HOME
  export prefix=$(DES_HOME)
endif
ifdef PREFIX
  export prefix=$(PREFIX)
endif

ifndef prefix 
$(error Must define the DES_HOME environment variable, or pass in a install PREFIX.) 
endif

export PREFIX=$(prefix)
export bindir=$(prefix)/bin
export libdir=$(prefix)/lib

################################################################################
# Include system-specific of default configuration variables
# For each SYSTEM defined her, a corresponding Makefile-${SYTSEM}.in file
# must exist, for this Makefile to function.  Undifined systems will use
# Makefile-default.in
################################################################################

THISHOST = $(shell hostname -f)
ifneq (,$(findstring cosmology, ${THISHOST}))
#  ifneq (,$(findstring bcs, ${THISHOST}))
#    SYSTEM=bcs
#  endif
#  ifneq (,$(findstring des1, ${THISHOST}))
#    SYSTEM=des1
#  endif
#  ifneq (,$(findstring des2, ${THISHOST}))
#    SYSTEM=des1
#  endif
SYSTEM=des
else
	ifneq (,$(findstring .uni-muenchen.de, ${THISHOST}))
	SYSTEM=usm
	endif
  ifneq (,$(findstring .ncsa.uiuc.edu, ${THISHOST}))
    ifneq (,$(findstring co-login, ${THISHOST}))
      SYSTEM=co
    endif
	ifneq (,$(findstring honest, ${THISHOST}))
		SYSTEM=abe
	endif
else 
    ifneq (,$(findstring loni.org, ${THISHOST}))
		SYSTEM=abe
	endif
  endif


endif
ifdef	 SYSTEM
	   INCLUDE_FILE=CONFIG/Makefile-${SYSTEM}.in
 else
	   INCLUDE_FILE=CONFIG/Makefile-default.in
 endif

include ${INCLUDE_FILE}

$(info )
$(info Using ${INCLUDE_FILE})
$(info )

ifdef CC
  export CC
endif
ifdef CFLAGS
  export CFLAGS
endif
ifdef CPPC
  export CPPC
endif
ifdef CPPCOPTS
  export CPPCOPTS
endif
ifdef MAKE_FLAGS
  export MAKE_FLAGS
  WL_BUILD_FLAGS = ${MAKE_FLAGS}
endif
WL_LIBS = ${DES_PREREQ}/lib
WL_INCLUDE = ${DES_PREREQ}/include
ifdef MKL_LIB
  WL_LIBS := ${WL_LIBS}:${MKL_LIB}
endif
ifdef MKL_INCLUDE
  WL_INCLUDE:=${WL_INCLUDE}:${MKL_INCLUDE}
endif
WL_BUILD_FLAGS = -j 4

#########################################################
# Project Source Locations.  
# Edit these locations to point to a different svn branch
# like, say "trunk"...
#########################################################
$(info )
ifdef DATA_BASE 
  $(info Using "DATA_BASE" from environment: ${DATA_BASE})
else
  DATA_BASE=${DEFAULT_DATA_BASE}
  $(info Using default value for "DATA_BASE": ${DATA_BASE} )
endif
ifdef IMAGEPROC_BASE
  $(info Using "IMAGEPROC_BASE" from environment: ${IMAGEPROC_BASE})
else
  IMAGEPROC_BASE=${DEFAULT_IMAGEPROC_BASE}
  $(info Using default value for "IMAGEPROC_BASE": ${IMAGEPROC_BASE} )
endif
ifdef IMSUPPORT_BASE
  $(info Using "IMSUPPORT_BASE" from environment: ${IMSUPPORT_BASE})
  IMSUPPORT_BASE := $(realpath $(IMSUPPORT_BASE))
else
  IMSUPPORT_BASE=${DEFAULT_IMSUPPORT_BASE}
  $(info Using default value for "IMSUPPORT_BASE": ${IMSUPPORT_BASE} )
endif

ifdef IMDETREND_BASE
  $(info Using "IMDETREND_BASE" from environment: ${IMDETREND_BASE})
  IMDETREND_BASE := $(realpath $(IMDETREND_BASE))
else
  IMDETREND_BASE=${DEFAULT_IMDETREND_BASE}
  $(info Using default value for "IMDETREND_BASE": ${IMDETREND_BASE} )
endif

ifdef COMMPIPE_BASE
  $(info Using "COMMPIPE_BASE" from environment: ${COMMPIPE_BASE})
else
  COMMPIPE_BASE=${DEFAULT_COMMPIPE_BASE}
  $(info Using default value for "COMMPIPE_BASE": ${COMMPIPE_BASE} )
endif
ifdef PHOTOZ_BASE
  $(info Using "PHOTOZ_BASE" from environment: ${PHOTOZ_BASE})
else
 PHOTOZ_BASE=${DEFAULT_PHOTOZ_BASE}
  $(info Using default value for "PHOTOZ_BASE": ${PHOTOZ_BASE})
endif
ifdef ORCH_BASE
  $(info Using "ORCH_BASE" from environment: ${ORCH_BASE})
else
  ORCH_BASE=${DEFAULT_ORCH_BASE}
  $(info Using default value for "ORCH_BASE": ${ORCH_BASE} )
endif
ifdef APPMOD_BASE
  $(info Using "APPMOD_BASE" from environment: ${APPMOD_BASE})
else
  APPMOD_BASE=${DEFAULT_APPMOD_BASE}
  $(info Using default value for "APPMOD_BASE": ${APPMOD_BASE} )
endif
ifdef WL_BASE
  $(info Using "WL_BASE" from environment: ${WL_BASE})
else
  WL_BASE=${DEFAULT_WL_BASE}
  $(info Using default value for "WL_BASE": ${WL_BASE} )
endif
ifdef DIFFIMG_BASE
  $(info Using "DIFFIMG_BASE" from environment: ${DIFFIMG_BASE})
else
  DIFFIMG_BASE=${DEFAULT_DIFFIMG_BASE}
  $(info Using default value for "DIFFIMG_BASE": ${DIFFIMG_BASE} )
endif

$(info )


# "Core" codes primarily to run on "target" processing platforms:
.PHONY: target_core
#target_core:  data appmod imageproc_perl imageproc_src diffimg photoz_src photoz_perl
target_core:  data appmod imageproc_perl imsupport imdetrend imageproc_src photoz_src photoz_perl

# Essential codes needed for the "submit" platform:
.PHONY: submit
submit: orch imageproc_perl

# Packages outside of the "core" desdm development:
.PHONY: external
external: wl

# Main build targets:
.PHONY: target
target: target_core external

# Target for all of the "core" codes:
.PHONY:all_core
all_core:allcore
.PHONY: allcore
allcore: submit target_core


cp: data appmod orch imsupport imageproc imdetrend commpipe

# Everything:
.PHONY: all
all: submit target

# Individual build targets:
.PHONY: Data
Data: data
.PHONY: data
data: clean_data $(DATA_BASE)/Build
	cd $(DATA_BASE); ./Build
	@touch .data.build
	@echo ""

$(DATA_BASE)/Build:
	cd $(DATA_BASE); perl Build.PL install_base=$(prefix)

.PHONY: AppMod
AppMod: appmod
.PHONY: appmod
appmod:
	cd $(APPMOD_BASE); $(MAKE) -e
	@touch .appmod.build
	@echo ""

.PHONY: Orchestration
Orchestration: orch
.PHONY: orch
orch: data appmod
	cd $(ORCH_BASE); $(MAKE) -e
	@touch .orch.build
	@echo ""

commpipe:clean_commpipe 
	cd $(COMMPIPE_BASE); $(MAKE) -e
	@touch .commpipe.build
	@echo ""


.PHONY: PHOTOZ
PHOTOZ:	 photoz

.PHONY: photoz
photoz:photoz_perl photoz_src
	cd $(PHOTOZ_BASE); $(MAKE) -e -i
	@touch .photoz.build
	@echo ""

.PHONY: ImageProc
ImageProc: imageproc

.PHONY: imageproc
#imageproc:imageproc_perl imageproc_src
imageproc:
	cd $(IMAGEPROC_BASE); $(MAKE) -e -i IMSUPPORT_DIR=$(IMSUPPORT_BASE)
	@touch .imageproc.build
	@echo ""

.PHONY: ImageProc_perl
ImageProc_perl: imageproc_perl
.PHONY: imageproc_perl
imageproc_perl:clean_imageproc_perl
	cd $(IMAGEPROC_BASE); $(MAKE) perl -e -i
	@touch .imageproc_perl.build
	@echo ""

.PHONY: ImageProc_src
ImageProc_src: imageproc_src
.PHONY: imageproc_src
imageproc_src: imsupport
	cd $(IMAGEPROC_BASE); $(MAKE) src -e -i IMSUPPORT_DIR=$(IMSUPPORT_BASE)
	@touch .imageproc_src.build
	@echo ""

.PHONY: imsupport
imsupport : 
	cd $(IMSUPPORT_BASE); $(MAKE)  -e -i
	@touch .imsupport.build
	@echo ""


.PHONY: imdetrend
imdetrend : 
	cd $(IMDETREND_BASE); $(MAKE)  -e -i IMSUPPORT_DIR=$(IMSUPPORT_BASE)
	@touch .imdetrend.build
	@echo ""

.PHONY:	photoz_src
PHOTOZ_src: photoz_src
.PHONY: photoz_src
photoz_src:
	cd $(PHOTOZ_BASE); $(MAKE) src -e -i
	@touch .photoz_src.build
	@echo ""
.PHONY: PHOTOZ_perl
PHOTOZ_perl: photoz_perl
.PHONY: photoz_perl
photoz_perl:clean_photoz_perl
		cd $(PHOTOZ_BASE); $(MAKE) perl -e -i
	        @touch .photoz_perl.build
	 	@echo ""


.PHONY: wl
wl:
	cd $(WL_BASE); scons CXX=$(CPPC) IMPORT_ENV=true EXTRA_LIB_PATH=$(WL_LIBS) EXTRA_INCLUDE_PATH=$(WL_INCLUDE) ${WL_BUILD_FLAGS}
	@touch .wl.build
	@echo ""

.PHONY: diffimg
diffimg:
	cd $(DIFFIMG_BASE); $(MAKE) -e -i
	@touch .diffimg.build
	@echo ""

# Install targets list.  Only install what has been built:
toinstall := $(patsubst %.build,%.install,$(wildcard .*.build))

.PHONY: install
install: $(toinstall) install_version

.PHONY: install_data
install_data:.data.install
.data.install: .data.build
	cd $(DATA_BASE); ./Build install
	@touch .data.install
	@echo -n "$(DATA_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(DATA_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

.PHONY: install_appmod
install_appmod:.appmod.install
.appmod.install: .appmod.build
	cd $(APPMOD_BASE); $(MAKE) install
	@touch .appmod.install
	@echo -n "$(APPMOD_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(APPMOD_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

.PHONY: install_orch
install_orch:.orch.install
.orch.install: .orch.build
	cd $(ORCH_BASE); $(MAKE) install
	@touch .orch.install
	@echo -n "$(ORCH_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(ORCH_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

.PHONY: install_imageproc
install_imageproc:.imageproc.install
.imageproc.install: .imageproc.build
	cd $(IMAGEPROC_BASE); $(MAKE) -e install IMSUPPORT_DIR=$(IMSUPPORT_BASE)
	@touch .imageproc.install
	@echo -n "$(IMAGEPROC_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(IMAGEPROC_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

.PHONY: install_imageproc_perl
install_imageproc_perl:.imageproc_perl.install
.imageproc_perl.install: .imageproc_perl.build
	cd $(IMAGEPROC_BASE); $(MAKE) -e install_perl
	@touch .imageproc_perl.install
	@echo -n "$(IMAGEPROC_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(IMAGEPROC_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""


.PHONY: install_imsupport
install_imsupport:.imsupport.install
.imsupport.install: .imsupport.build
	cd $(IMSUPPORT_BASE); $(MAKE) -e install
	@touch .imsupport.install
	@echo -n "$(IMSUPPORT_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(IMSUPPORT_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""


.PHONY: install_imdetrend
install_imdetrend:.imdetrend.install
.imdetrend.install: .imdetrend.build
	cd $(IMDETREND_BASE); $(MAKE) -e install
	@touch .imdetrend.install
	@echo -n "$(IMDETREND_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(IMDETREND_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""



.commpipe.install: .commpipe.build
	cd $(COMMPIPE_BASE); $(MAKE) install 
	@touch .commpipe.install
	@echo -n "$(COMMPIPE_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(COMMPIPE_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

.PHONY: install_imageproc_src
install_imageproc_src:.imageproc_src.install
.imageproc_src.install: .imageproc_src.build
	cd $(IMAGEPROC_BASE); $(MAKE) -e install_src IMSUPPORT_DIR=$(IMSUPPORT_BASE)
	@touch .imageproc_src.install
	@echo -n "$(IMAGEPROC_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(IMAGEPROC_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

.PHONY: install_photoz
install_photoz:.photoz.install
.photoz.install: .photoz.build
	cd $(PHOTOZ_BASE); $(MAKE) -e install
	@touch .photoz.install
	@echo -n "$(PHOTOZ_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(PHOTOZ_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""


.PHONY: install_photoz_src
install_photoz_src:.photoz_src.install
.photoz_src.install: .photoz_src.build
	cd $(PHOTOZ_BASE); $(MAKE) -e install_src
	@touch .photoz_src.install
	@echo -n "$(PHOTOZ_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(PHOTOZ_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""
.PHONY: install_photoz_perl
install_photoz_perl:.photoz_perl.install
.photoz_perl.install: .photoz_perl.build
	cd $(PHOTOZ_BASE); $(MAKE) -e install_perl
	@touch .photoz_perl.install
	@echo -n "$(PHOTOZ_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(PHOTOZ_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""


.PHONY: install_diffimg
install_diffimg:.diffimg.install
.diffimg.install: .diffimg.build
	cd $(DIFFIMG_BASE); $(MAKE) -e install
	@touch .diffimg.install
	@echo -n "$(DIFFIMG_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(DIFFIMG_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

.PHONY: install_version
install_version: VERSION
	cp VERSION $(DES_HOME)

.PHONY: install_wl
install_wl:.wl.install
.wl.install:.wl.build
	cd $(WL_BASE); scons install PREFIX=$(prefix)
	@touch .wl.install
	@echo -n "$(WL_BASE):" >> $(DES_HOME)/BASEVERSIONS
	@svnversion $(WL_BASE) >> $(DES_HOME)/BASEVERSIONS
	@echo ""

# Clean targets list:
toclean := $(patsubst .%.build,clean_%,$(wildcard .*.build))
.PHONY: clean_all
clean_all: clean_data clean_appmod clean_orch clean_imageproc clean_imageproc_perl clean_imageproc_src clean_photoz_perl clean_photoz_src clean_diffimg clean_wl

.PHONY: clean
#clean: $(toclean)
clean: clean_all
	rm -f *.log

.PHONY: clean_data
clean_data:
	@if [ -r $(DATA_BASE)/Build ]; then \
	cd $(DATA_BASE); ./Build realclean; \
	else \
	echo "Nothing to clean"; \
	fi
	rm -f .data.*
	@echo ""

.PHONY: clean_appmod
clean_appmod:
	#cd $(APPMOD_BASE); $(MAKE) clean
	rm -f .appmod.*
	@echo ""

.PHONY: clean_orch
clean_orch:
	cd $(ORCH_BASE); $(MAKE) clean
	rm -f .orch.*
	@echo ""

.PHONY: clean_imageproc
clean_imageproc:
	cd $(IMAGEPROC_BASE); $(MAKE) clean
	rm -f .imageproc.*
	@echo ""

.PHONY: clean_imageproc_perl
clean_imageproc_perl:
	cd $(IMAGEPROC_BASE); $(MAKE) clean_perl
	rm -f .imageproc_perl.*
	@echo ""
.PHONY: clean_commpipe
clean_commpipe:
	cd $(COMMPIPE_BASE); $(MAKE) clean
	rm -f .commpipe_perl.*
	@echo ""
.PHONY: clean_photoz_perl
clean_photoz_perl:
	cd $(PHOTOZ_BASE); $(MAKE) clean_perl
	rm -f .photoz_perl.*
	@echo ""


.PHONY: clean_imageproc_src
clean_imageproc_src:
	cd $(IMAGEPROC_BASE); $(MAKE) clean_src
	rm -f .imageproc_src.*
	@echo ""

.PHONY: clean_photoz_src
clean_photoz_src:
	cd $(PHOTOZ_BASE); $(MAKE) clean_src
	rm -f .photoz_src.*
	@echo ""

.PHONY: clean_diffimg
clean_diffimg:
	cd $(DIFFIMG_BASE); $(MAKE) clean
	rm -f .diffimg.*
	@echo ""

.PHONY: clean_wl
clean_wl: $(WL_BASE)/wl_scons.conf
	cd $(WL_BASE); scons -c IMPORT_ENV=true EXTRA_LIB_PATH=$(WL_LIBS) EXTRA_INCLUDE_PATH=$(WL_INCLUDE)
	cd $(WL_BASE); rm -rf .sconsign.dblite config.log .sconf_temp wl_scons.conf
	rm -f .wl.*
	@echo ""

