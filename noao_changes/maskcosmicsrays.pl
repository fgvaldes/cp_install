#!/usr/bin/env perl
########################################################################
#  $Id: maskcosmicsrays.pl 7602 2012-03-19 19:27:10Z ricardoc $
#
#  $Rev:: 7602                             $:  # Revision of last commit.
#  $LastChangedBy:: ricardoc               $:  # Author of last commit. 
#  $LastChangedDate:: 2012-03-19 14:27:10 #$:  # Date of last commit.
#
#  Authors: 
#         Ricardo Covarrubias (riccov@illinois.edu)
#
#  Developed at: 
#  The National Center for Supercomputing Applications (NCSA)
#
#  Copyright (C) 2007 Board of Trustees of the University of Illinois. 
#  All rights reserved.
#
#  DESCRIPTION:
#       Given a list of images, run mkbleedmask to find all bleedtrails
#       and mask them out. Input images get overwritten.
#      
#
########################################################################

use strict;
use warnings;

use Getopt::Long;
use File::Basename;
use File::Path;

# turn buffering off on STDOUT by making the filehandle hot
$|=1;

my ($image_list,$verbose,$bin_path,$detector,$quiet) = undef;
my ($crays,$cr_fract,$cr_sig2,$lowsig,$lowwt) = undef;
Getopt::Long::GetOptions(
    "list=s" => \$image_list,
    "verbose=s" => \$verbose,
    "binpath=s" => \$bin_path,
    "detector=s" => \$detector,
    "quiet" => \$quiet,
    "crays" =>  \$crays,
    "crfract=s" => \$cr_fract,
    "crsig2=s" => \$cr_sig2,
    "lowsig=s" => \$lowsig,
    "lowwt=s" => \$lowwt,
);

if (defined($quiet)) {
    $verbose = 0;
}

if (!defined($verbose)) {
    $verbose = 1;
}


my @images = ();

# get list of src files
if (defined($image_list)) {
    if (! -r $image_list) {
        print STDERR "STATUS4BEG\n" if ($verbose >= 2);
        print STDERR "Error: Cannot read image list '$image_list'\n";
        print STDERR "STATUS4END\n" if ($verbose >= 2);
        exit 1;
    }
    open FILE, "< $image_list" || die "Error: Could not open list file '$image_list'\n";
    while (my $line = <FILE>) {
        my $image_name = $line;
        chomp($image_name);
        push (@images, $image_name);
    }
    close FILE
}
else {
    print STDERR "STATUS4BEG\n" if ($verbose >= 2);
    print STDERR "Error: must provide a file containing a list of images.\n";
    print STDERR "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}


printf "  Discovered %d images\n", scalar(@images) if ($verbose > 0);


if (scalar(@images) == 0) {
    print "STATUS4BEG\n" if ($verbose >= 2);
    print "Error: 0 images to detect saturated objects\n";
    print "STATUS4END\n" if ($verbose >= 2);
    exit 1;
}


# for each image
foreach my $image (@images) {
    if ($verbose > 0) {
        print "Creating command line for image: $image\n";
    }
#    my $exposure_name = basename($image,(".fits",".fits.gz",".fits.fz"));


    # call convert program to maskcosmicsrays
    my $cmd = "";

    # include path to convert program if specified
    if (defined($bin_path)) {
        $cmd .= "$bin_path/";
    }

    $cmd .= "maskcosmics $image";

    #my $output_image = $image;

    # add arguments 
    if (defined($crays)) {
        $cmd .= " -crays";
    }
    if (defined($cr_fract)) {
        $cmd .= " -crfract $cr_fract";
    }
    if (defined($cr_sig2)) {
        $cmd .= " -crsig2 $cr_sig2";
    }
    if (defined($lowsig)) {
        $cmd .= " -lowsig $lowsig";
    }
    if (defined($lowwt)) {
        $cmd .= " -lowwt $lowwt";
    }
    if (defined($verbose)) {
        $cmd .= " -verbose $verbose";
    }
    else {
        $cmd .= "";
    }
 

    print "maskcosmicsrays command: $cmd\n" if ($verbose > 0);

    system($cmd);
    if ($? == -1) {
        print STDERR "Failed to execute: $!\n";
    }
    elsif ($? & 127) {
        printf STDERR "Child died with signal %d, %s coredump\n", ($? & 127),  ($? & 128) ? 'with' : 'without';
    }
    else {
        printf STDERR "Child exited with value %d\n", $? >> 8;
    }
}

print "\n" if ($verbose >= 0);

exit 0;
