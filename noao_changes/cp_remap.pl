#!/usr/bin/perl 

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell


#
# Modified for CP By N. Kuropatkin   02/20/2012
#
# Works with new swarp version  V2.31.1 
# for older versions require coadd.head file in working directory
# with content CD1_1   = -7.5E-5
#              CD1_2   = 0.0
#              CD2_1   = 0.0
#              CD2_2   = 7.5E-5
#
#

eval 'exec /usr/bin/perl  -S $0 ${1+"$@"}'
    if 0; # not running under some shell

use strict;
use warnings;
use Cwd;

use Carp;
use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
use File::Basename;
use File::Path;
use Astro::FITS::CFITSIO qw(:constants);
use Astro::FITS::CFITSIO qw( :longnames );
use Astro::FITS::CFITSIO qw( :shortnames );
use FindBin;
use lib ("$FindBin::Bin/../lib", "$FindBin::Bin/../lib/perl5");
use DB::EventUtils;
use DB::FileUtils;
use DB::DESUtil;
use ImageProc::SWARP qw(:all);
use ImageProc::Util qw(appendArgs readFitsHeaderKeys updateFitsHeaderKeys);
use DAF::Archive::Tools qw(getArchiveLocation);

$|=1;

# Define DES header keywords to be inserted into remap MEF:
### Create a hash, hashes are indexed by their key value. 

my %key_defs = (
  'DES_EXT' => {'dtype' => TSTRING, 'comment' => 'Image Extension'},
  'OBSTYPE' => {'dtype' => TSTRING, 'comment' => 'Observation type'}
);

my %key_defs_for_inserting_id = (
  'PARENT_ID' => {'dtype' => TSTRING, 'comment' => 'ID of the file from location table'},
);


my $verbose = 1;
my $swarp = 'swarp';
my $funpack = 'funpack';
my $fitscombine = 'fitscombine';
my $outdir = '.';

my ($help,$man,$exit);
my ($list,$prereqbin,$bindir,$stat,$image_mode,$bpm_mode,$nthread,$pixelsize,$fscaleastrovariable,$config,$cleanup)=undef;

# Grab the wrapper options and the SWARP options that are also
# handles and set by this code.  the rest of the command line
# (whatever's left) will be assumed to be additional SWARP arguments:
&Getopt::Long::Configure( 'noignorecase', 'no_autoabbrev');
$stat = Getopt::Long::GetOptions(
      'list=s' => \$list,
      'prereqbin=s' => \$prereqbin,
      'binpath|bindir=s' => \$bindir,
      'outpath|outdir|od=s' => \$outdir,
      'cleanup' => \$cleanup,
      'config|c=s' => \$config,
      'nthread=i' => \$nthread,
      'verbose|V=s' => \$verbose,
      'pixelsize|p=s' => \$pixelsize,
      'fscaleastrovariable' => \$fscaleastrovariable, 
      'help|h|?' => \$help,
      'man' => \$man
);

# Display documantation and exit if requested:
if ($help) {
  Pod::Usage::pod2usage(-verbose => 1,-exitval => 1,-output => \*STDOUT);
}
elsif ($man) {
  Pod::Usage::pod2usage(-verbose => 2,-exitval => 1,-output => \*STDOUT);
}

# Check inputs
if (!defined($list)) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2, -output => \*STDERR,
    -message => 'Must provide an input list of images.'
  );
}

if (! -r $list) {
  Pod::Usage::pod2usage (-verbose => 0, -exitval => 2,-output => \*STDERR,
    -message => "Could not read image list: '$list'."
  );
}

if (defined($bindir)) {
  $swarp = $prereqbin.'/'.$swarp;
  $funpack = $prereqbin.'/'.$funpack;
}

if (defined($bindir)) {
  $fitscombine = $bindir.'/'.$fitscombine;
}

if (! -x $swarp) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => "Cannot execute $swarp"
  );
}

if (! -x $funpack) {
  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
    -message => "Cannot execute $funpack"
  );
}

#if (! -x $fitscombine) {
#  Pod::Usage::pod2usage (-verbose => 1, -exitval => 2, -output => \*STDERR,
#    -message => "Cannot execute $fitscombine"
#  );
#}
#if (defined($cleanup)) {
#    $fitscombine .= ' -cleanup ';
#}

if (! -d $outdir) {
    eval { mkpath($outdir) };
    if ($@) {
        reportEvent($verbose, "STATUS", 5, "Error:  Could not make dir $outdir");
        exit 1;
    }
}

my $base_swarp_cmd = $swarp;
if (defined($config)) {
    if (! -r $config) {
        reportEvent($verbose, "STATUS", 4, "WARNING: Could not read config file '$config'.   Calling swarp without -c option.");
    }
    else {
        $base_swarp_cmd .= " -c $config ";
    }
}

if (defined($nthread)) {
    $base_swarp_cmd .= " -NTHREADS $nthread ";
}
	#  write the default coadd.head file in current directory
	# this file will be used then by swarp
	open HEAD, ">" , 'coadd.head' or die "\n Cannot create coadd.head file \n"; 
	print HEAD "CD1_1   = -7.5E-5 \n";
	print HEAD "CD1_2   = 0.0 \n";
	print HEAD "CD2_1   = 0.0 \n";
	print HEAD "CD2_2   = 7.5E-5 \n";
	close HEAD;

my $value_list = readSWARPInputFile($list);
my $errflag = 0;


###
# Added by Ankit to get the fileids for the files which are being used here. These ids need to be inserted in the header, so that fileingest can pick them up as parent ids.
###
my ($fileName,$fileinfoHashref,$queryVars,$allFilesHash,$allFilesDets,$finalDirName,$tmpFileName,$tmpVar,$tmpBaseName);
# Loop through all the files provided
$queryVars->{'project'} = "";
$queryVars->{'nite'} = "";
$queryVars->{'filetype'} = "";
$queryVars->{'fileclass'} = "";
foreach my $lineForIds (@$value_list) {

	$fileName = $lineForIds->[0];
	#get the meta information for this file by using filenameResolve
	#print "\n the filename $fileName";
	$tmpFileName = $fileName;
	my ($tmp_basename,$tmp_dirPath,$tmp_suffix) = fileparse($tmpFileName,('.fits','.fits.gz','.fits.fz'));
	$tmpFileName =~ s/^(.*?)\/Archive\///; 
	$fileinfoHashref = filenameResolve($tmpFileName);
	# strip out the part before Archive in the fileName and set it to 1 
	$fileName =~ s/^(.*?)\/Archive\///;
	$tmp_dirPath =~ s/^(.*?)\/Archive\///;
	$allFilesHash->{$tmp_dirPath} = 1;
	print "\n STATUS2BEG init assigning the filename $tmp_dirPath STATUS2END";
	# prepare the query variables for unique fileclass, filetype, project and nite	
	undef $tmpVar ;
	$tmpVar = $fileinfoHashref->{'PROJECT'}; 
	if($queryVars->{'project'} !~ /$tmpVar/){
		$queryVars->{'project'} .= "'".$fileinfoHashref->{'PROJECT'}."', ";
	}
	undef $tmpVar ;

	$tmpVar = $fileinfoHashref->{'FILETYPE'};
	if($queryVars->{'filetype'} !~ /$tmpVar/){
		$queryVars->{'filetype'} .= "'".$fileinfoHashref->{'FILETYPE'}."', ";
	}
	undef $tmpVar ;
	
	$tmpVar = $fileinfoHashref->{'FILECLASS'};
	if($queryVars->{'fileclass'} !~ /$tmpVar/){
		$queryVars->{'fileclass'} .= "'".$fileinfoHashref->{'FILECLASS'}."', ";
	}
	undef $tmpVar ;
	
	$tmpVar = $fileinfoHashref->{'NITE'};
	if($queryVars->{'nite'} !~ /$tmpVar/){
		$queryVars->{'nite'} .= "'".$fileinfoHashref->{'NITE'}."', ";
	}
	undef $tmpVar ;
}

	$queryVars->{'project'} = substr $queryVars->{'project'}, 0, -2;
	$queryVars->{'nite'} = substr $queryVars->{'nite'}, 0, -2;
	$queryVars->{'filetype'} = substr $queryVars->{'filetype'}, 0, -2;
	$queryVars->{'fileclass'} = substr $queryVars->{'fileclass'}, 0, -2;

#print "\n the query",Dumper($queryVars);
# run the query to get all the file information from location table
my $sqlGetIds = "select * from location where project in (".$queryVars->{'project'}.") and fileclass in (".$queryVars->{'fileclass'}.") and filetype in (".$queryVars->{'filetype'}.") and nite in (".$queryVars->{'nite'}.")";
#print "\n the sql $sqlGetIds";
my $desdbh = DB::DESUtil->new();
my $sthGetIds = $desdbh->prepare($sqlGetIds);
$sthGetIds->execute();
while (my $rowGetIds = $sthGetIds->fetchrow_hashref()){
	
	#print "\n the row ",Dumper($rowGetIds);
	$allFilesDets->{'project'} = $rowGetIds->{'project'};
        $allFilesDets->{'fileclass'} = $rowGetIds->{'fileclass'};
        $allFilesDets->{'filetype'} = $rowGetIds->{'filetype'};
        $allFilesDets->{'filename'} = $rowGetIds->{'filename'};
        $allFilesDets->{'run'} = $rowGetIds->{'run'};
        $allFilesDets->{'nite'} = $rowGetIds->{'nite'};
        $allFilesDets->{'band'} = $rowGetIds->{'band'};
        $allFilesDets->{'tilename'} = $rowGetIds->{'tilename'};
        $allFilesDets->{'exposurename'} = $rowGetIds->{'exposurename'};
        $allFilesDets->{'ccd'} = $rowGetIds->{'ccd'};
        $allFilesDets->{'archivesites'} = $rowGetIds->{'archivesites'};
	#prepare the filepath for this information
	#print "\n the archive location",Dumper($allFilesDets);
        my $finalDirName = getArchiveLocation($allFilesDets);
	
	# if this filepath exists in the previously created filesHash, it means that there is a match. Insert the file id into that hash now. Voila!
	if( defined $allFilesHash->{$finalDirName} && $allFilesHash->{$finalDirName} == 1){
		print "\n STATUS2BEG assigning id to $finalDirName: ",$rowGetIds->{'id'}," STATUS2END";
		$allFilesHash->{$finalDirName} = $rowGetIds->{'id'}; 
	}else{
		#print "\n NOT assigning id to $finalDirName: ",$rowGetIds->{'id'};
	}
}

my $linecnt = 1;
foreach my $line (@$value_list) {
    print "Starting remap work on line $linecnt\n";
    my %Args;
    my $swarp_cmd = $base_swarp_cmd;
    

#
# We have removed 2 parameters number of pixels in tile image in x and y, so check for 5 parameters not 7
#
	if (scalar(@$line) < 5) {
        reportEvent($verbose, "STATUS",5,"Error: line $linecnt does not have enough values ".Dumper($line)); 
        $linecnt++;
        next;
    }
    my $infile = $line->[0];
    my $tilename = $line->[1];
    if (!defined($pixelsize)) {
	$pixelsize = $line->[2];
    }
    my $tile_ra = $line->[3];
    my $tile_dec = $line->[4];

	print "Pixel size=",$pixelsize,"\n";

	

	
    # Get exposure from red location to use in remap location
    my ($basename,$path,$suffix) = fileparse($infile,('.fits','.fits.gz','.fits.fz'));
    $path =~ s/\/\s*$//g;
    $path =~ /\/([^\/]+)$/;
    my $exposurename = $1;
    $path = "$outdir/$exposurename";
    my $tmppath = cwd;

    my $remap_name = $basename.'_'.$tilename.'.fits';
    if (-r "$path/$remap_name") {
        reportEvent($verbose,"STATUS",4,"WARNING: Image $linecnt. Remap image already exists $path/$remap_name, removing the existing remap image");
	unlink "$path/$remap_name";

    }

    # If the input file does not exist, look for the fz compressed version.
    my $uncompressed = 0;
    if (!  -r $infile) {
        if (-r "$infile.fz") {
            print "Unpacking fz file...";
            system ("$funpack $infile.fz");
            $uncompressed = 1;
        }
        else {
            reportEvent($verbose, "STATUS", 5, "$linecnt: Cannot read input file $infile or $infile.fz");
            $errflag=1;
            $linecnt++;
            next;
        }
    }

    if (! -d $path) {
        mkdir $path;
    }

    $Args{'-CENTER_TYPE'} = 'MANUAL';
    $Args{'-PIXEL_SCALE'} = $pixelsize;
    $Args{'-PIXELSCALE_TYPE'} = 'MANUAL';
    $Args{'-CENTER'} = $tile_ra.','.$tile_dec;
	# This is necessary for swarp versions < V2.31.1 
#	$Args{'-IMAGE_SIZE'} = '40000,40000';
	#
    # This will work with swarp V2.31.1
    #
    $Args{'-IMAGE_SIZE'} = '0';
    #
    #
    $Args{'-RESAMPLE'} = 'Y';
    $Args{'-RESAMPLE_DIR'} = $tmppath;


    ##### IMAGE
    # For "image_mode", use the first HDU as input and utilize the third as the weight map 
    $swarp_cmd  = $base_swarp_cmd.' '.$infile.'[0] ';
    $swarp_cmd .= ' -WEIGHT_TYPE MAP_WEIGHT ';
    $swarp_cmd .= ' -WEIGHT_IMAGE  '.$infile.'[2] ';
    $swarp_cmd .= ' -RESAMPLE_SUFFIX _'.$tilename.'_im.fits ';
    
    $swarp_cmd = appendArgs($swarp_cmd, \%Args);
    if (defined($fscaleastrovariable)) {
        $swarp_cmd .= " -FSCALASTRO_TYPE VARIABLE ";
    }
    else {
        $swarp_cmd .= " -FSCALASTRO_TYPE NONE ";
    }
    $swarp_cmd .= ' -RESAMPLING_TYPE LANCZOS3 -COMBINE N -PIXELSCALE_TYPE MANUAL -COMBINE_TYPE WEIGHTED -COPY_KEYWORDS OBJECT,OBSERVER,CCDNUM,FILTER,OBSTYPE,FILENAME,TELRA,TELDEC,TELEQUIN,AIRMASS,DATE-OBS,EXPTIME,SATURATE,DETECTOR,TELESCOP,OBSERVAT,SKYBRITE,SKYSIGMA,PHOTFLAG,FWHM,ELLIPTIC,SCAMPCHI,SCAMPFLG,SCAMPNUM -DELETE_TMPFILES Y  -HEADER_ONLY N -VERBOSE_TYPE FULL ';
    $swarp_cmd = join(' ',$swarp_cmd, @ARGV);

    reportEvent($verbose, "STATUS", 1, "$linecnt: Running $swarp_cmd");
    $exit = runSWARP($swarp_cmd, $verbose);
    if ($exit != 0) {
        reportEvent($verbose, "STATUS", 5, "$linecnt: first swarp exited with non-zero exit code ($exit)");
        $linecnt++;
        next;
    }
    


    ##### BPM
    # For bpm use the 2nd HDU as input and no weight map  
    my $tmp_bpm;
    $tmp_bpm =  $tmppath.'/'.$basename.'_tmp_bpm.fits';
    my $imcopy_res = `imcopy $infile'[1]' $tmp_bpm`;
    my $updateWcs_res = `update_wcs $tmp_bpm $infile'[0]'`;
    
    $swarp_cmd  = $base_swarp_cmd.' '.$tmp_bpm;
    $swarp_cmd = appendArgs($swarp_cmd, \%Args);
    $swarp_cmd .= ' -WEIGHT_TYPE NONE ';
    $swarp_cmd .= ' -RESAMPLE_SUFFIX _'.$tilename.'_bpm.fits ';
    $swarp_cmd .= ' -SUBTRACT_BACK N -FSCALASTRO_TYPE NONE -RESAMPLING_TYPE NEAREST -COMBINE N -DELETE_TMPFILES Y  -HEADER_ONLY N -VERBOSE_TYPE FULL ';
    $swarp_cmd = join(' ',$swarp_cmd, @ARGV);
    reportEvent($verbose, "STATUS", 1, "$linecnt: Running $swarp_cmd");
    $exit = runSWARP($swarp_cmd, $verbose);
    if ($exit != 0) {
        reportEvent($verbose, "STATUS", 5, "$linecnt: second swarp exited with non-zero exit code ($exit)");
        $linecnt++;
        next;
    }


    ##### EXPMAP
    # Exposure map is the last HDU ([3] in red images)
    
    my $status = 0; 
    my $comment;
    my $EXPTIME;
    my $tmp = $tmppath.'/'.$basename.'_tmp_exp.fits';
    my $tmpweight = $tmppath.'/'.$basename.'_tmp_exp_'.$tilename.'_expmap.weight.fits';
    my $fptr = Astro::FITS::CFITSIO::open_file($infile,Astro::FITS::CFITSIO::READONLY(),$status);    
    ffgkey($fptr,'EXPTIME',$EXPTIME,$comment,$status); 
    #system("imcopy $infile'[1][pixi X>0 ? 0: X-X+$EXPTIME]' $tmp");
    system("imcopy $tmp_bpm'[0][pixi X>0 ? 0: X-X+$EXPTIME]' $tmp");
    $swarp_cmd = $base_swarp_cmd.' '.$tmp;
    $swarp_cmd = appendArgs($swarp_cmd, \%Args);
    $swarp_cmd .= ' -WEIGHT_TYPE NONE ';
    $swarp_cmd .= ' -RESAMPLE_SUFFIX _'.$tilename.'_expmap.fits ';
    $swarp_cmd .= ' -SUBTRACT_BACK N -FSCALASTRO_TYPE NONE -RESAMPLING_TYPE NEAREST -COMBINE N -DELETE_TMPFILES Y  -HEADER_ONLY N -VERBOSE_TYPE FULL ';
    $swarp_cmd = join(' ',$swarp_cmd, @ARGV);   
    reportEvent($verbose, "STATUS", 1, "$linecnt: Running $swarp_cmd");
    $exit = runSWARP($swarp_cmd, $verbose);    
    if ($exit != 0) {
        reportEvent($verbose, "STATUS", 5, "$linecnt: third swarp exited with non-zero exit code ($exit)");
        $linecnt++;
        next;
    }


    ##### create MEF
    my $im_name = $basename.'_'.$tilename.'_im.fits';  
    my $imvar_name = $basename.'_'.$tilename.'_im.weight.fits';   
    #my $bpm_name = $basename.'.0001_'.$tilename.'_bpm.fits';  
    my $bpm_name = $basename.'_tmp_bpm_'.$tilename.'_bpm.fits';
    #my $bpmvar_name = $basename.'.0001_'.$tilename.'_bpm.weight.fits';  
    my $bpmvar_name = $basename.'_tmp_bpm_'.$tilename.'_bpm.weight.fits';  
    my $expmap_name = $basename.'_tmp_exp_'.$tilename.'_expmap.fits';

    if (! -r "$tmppath/$im_name" ) {
        reportEvent($verbose,"STATUS",4,"Unable to read: $tmppath/$im_name");
        $errflag = 1;
    }
    if (! -r "$tmppath/$bpm_name" ) {
        reportEvent($verbose,"STATUS",4,"Unable to read: $tmppath/$bpm_name");
        $errflag = 1;
    }
    if (! -r "$tmppath/$imvar_name") {
        reportEvent($verbose,"STATUS",4,"Unable to read: $tmppath/$imvar_name");
        $errflag = 1;
    }
    if (! -r "$tmppath/$expmap_name") {
        reportEvent($verbose,"STATUS",4,"Unable to read: $tmppath/$expmap_name");
        $errflag = 1;
    }	
    if ((-r "$tmppath/$im_name") && (-r "$tmppath/$bpm_name") && (-r "$tmppath/$imvar_name") && (-r "$tmppath/$expmap_name")) {
        my $this_command = "$fitscombine $tmppath/$im_name $tmppath/$bpm_name $tmppath/$imvar_name $tmppath/$expmap_name $path/$remap_name";
        print "\n$linecnt: Combining remapped image, mask, weight and exposure map";
        print "$this_command";
        my $stat = system ("$this_command");
        if ($stat) {
            $errflag = 1;
            print STDERR "Error: $this_command exited with non-zero exit code ($stat, $infile, $tilename)\n";
        }
        else {
            # Update DES_EXT, OBSTYPE header keywords
            updateFitsHeaderKeys ({
                'fits_file' => "$path/$remap_name",
                'key_defs' => \%key_defs,
                'values' => {'DES_EXT'=>'IMAGE','OBSTYPE'=>'remap'},
                'hdu' => 1,
                'verbose' => $verbose
            });
            updateFitsHeaderKeys ({
                'fits_file' => "$path/$remap_name",
                'key_defs' => \%key_defs,
                'values' => {'DES_EXT'=>'MASK','OBSTYPE'=>'remap'},
                'hdu' => 2,
                'verbose' => $verbose
            });
            updateFitsHeaderKeys ({
                'fits_file' => "$path/$remap_name",
                'key_defs' => \%key_defs,
                'values' => {'DES_EXT'=>'WEIGHT','OBSTYPE'=>'remap'},
                'hdu' => 3,
                'verbose' => $verbose
            });
	    updateFitsHeaderKeys ({
                'fits_file' => "$path/$remap_name",
                'key_defs' => \%key_defs,
                'values' => {'DES_EXT'=>'EXPOSURE','OBSTYPE'=>'remap'},
                'hdu' => 4,
                'verbose' => $verbose
            });
	    ### 
	    # Added this additional call to updateFitsHeaderKeys to add a new keyword PARENT_ID 
	    ### 
		#print "STATUS2BEG Assigning ID ",$allFilesHash->{$infile}," to $path/$remap_name STATUS2END \n";

            #updateFitsHeaderKeys ({
            #    'fits_file' => "$path/$remap_name",
            #    'key_defs' => \%key_defs_for_inserting_id,
            #    'values' => {'PARENT_ID'=>"'".$allFilesHash->{$infile}."'"},
            #    'hdu' => 1,
            #    'verbose' => $verbose
            #});
        }
    }
    else {
        print "$linecnt: Skipping fitscombine\n";
    }

    ##### CLEANUP
    print "$linecnt: Removing intermediate files\n";
    if ($uncompressed) {
        print "Removing temporary uncompressed input file: $infile\n";
        system("rm -f $infile");
    }
    
    # Remove bpm weight map
    if ($cleanup) {
        if (-r "$tmppath/$im_name") {
            unlink "$tmppath/$im_name";
        }
        if (-r "$tmppath/$imvar_name") {
            unlink "$tmppath/$imvar_name";
        }
        if (-r "$tmppath/$bpm_name") {
            unlink "$tmppath/$bpm_name";
        }
        if (-r "$tmppath/$bpmvar_name") {
            unlink "$tmppath/$bpmvar_name";
        }
	if (-r "$tmppath/$expmap_name") {
	    unlink "$tmppath/$expmap_name";
	}
	if (-r "$tmp_bpm") {
	    unlink "$tmp_bpm";
	}
	if (-r "$tmp") {
	    unlink "$tmp";
	}
	if (-r "$tmpweight") {
	    unlink "$tmpweight";
	}
        if (! -r "$path/$remap_name") {
            rmdir "$path";
        }
    }
    $linecnt++;
}

exit $errflag;



=head1 NAME B<cp_remap.pl> - Wrapper to run TeraPix SWARP software on DESDM data sets.

=head1 SYNOPSIS

B<cp_remap.pl> -list <file list> [-bindir <bin directory with swarp>] [-- [Additional swarp options] ]

=head1 DESCRIPTION

=head1 OPTIONS

=over 4

=item B<-bindir> I<directory>

The Directory containing the Terapix B<swarp> executable.

=item B<-bpm>

Indicates that the remapping is to be done on the FITS extension containing the bpm data.

=item B<-help>, B<-h>, B<-?>

Display brief help and exit.

=item B<-image>, B<-im>

Indicates that the remapping is to be done on the FITS extension containing the image data.

=item B<-man>

Display full manual page and exit.

=item B<-verbose>, B<-V> I<Number>

Verbose level 0-5. Default is 1.  Level 2 and above will turn on status event syntax.


=back
